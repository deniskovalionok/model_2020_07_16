      Subroutine KPG_KPH(ext_deltat)
      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      Include 'KPG_KPH.fh'

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      Call KPG_KPH_ConIn
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_isili,4),R8_ipili
     &,I_uvili,I_ixili,I_ovili,
     & C8_arili,I_exili,R_urili,R_orili,R_ilili,
     & REAL(R_ulili,4),R_omili,REAL(R_apili,4),
     & R_amili,REAL(R_imili,4),I_evili,I_oxili,I_axili,I_avili
     &,L_epili,
     & L_aboli,L_adoli,L_umili,L_emili,
     & L_upili,L_opili,L_iboli,L_olili,L_irili,L_odoli,L_asili
     &,
     & L_esili,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_
     &(121),L_alili,L_(122),
     & L_elili,L_eboli,I_uxili,L_edoli,R_utili,REAL(R_erili
     &,4),L_idoli,L_erekad,
     & L_udoli,L_atakad,L_osili,L_usili,L_atili,L_itili,L_otili
     &,L_etili)
      !}

      if(L_otili.or.L_itili.or.L_etili.or.L_atili.or.L_usili.or.L_osili
     &) then      
                  I_ivili = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ivili = z'40000000'
      endif
C KPJ_vlv.fgi(  60, 177):���� ���������� �������� � ���������������� ��������,20KPJ22AA104
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_afope,4),R8_abope
     &,I_ilope,I_amope,I_elope,
     & C8_obope,I_ulope,R_idope,R_edope,R_avipe,
     & REAL(R_ivipe,4),R_exipe,REAL(R_oxipe,4),
     & R_ovipe,REAL(R_axipe,4),I_ukope,I_emope,I_olope,I_okope
     &,L_uxipe,
     & L_omope,L_opope,L_ixipe,L_uvipe,
     & L_ibope,L_ebope,L_apope,L_evipe,L_adope,L_erope,L_odope
     &,
     & L_udope,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_
     &(13),L_etipe,L_(14),
     & L_itipe,L_umope,I_imope,L_upope,R_ikope,REAL(R_ubope
     &,4),L_arope,L_otipe,
     & L_irope,L_utipe,L_efope,L_ifope,L_ofope,L_akope,L_ekope
     &,L_ufope)
      !}

      if(L_ekope.or.L_akope.or.L_ufope.or.L_ofope.or.L_ifope.or.L_efope
     &) then      
                  I_alope = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_alope = z'40000000'
      endif
C KPJ_vlv.fgi( 135, 152):���� ���������� �������� � ���������������� ��������,20KPV43AB001
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_ibupe,4),R8_ivope
     &,I_ufupe,I_ikupe,I_ofupe,
     & C8_axope,I_ekupe,R_uxope,R_oxope,R_isope,
     & REAL(R_usope,4),R_otope,REAL(R_avope,4),
     & R_atope,REAL(R_itope,4),I_efupe,I_okupe,I_akupe,I_afupe
     &,L_evope,
     & L_alupe,L_amupe,L_utope,L_etope,
     & L_uvope,L_ovope,L_ilupe,L_osope,L_ixope,L_omupe,L_abupe
     &,
     & L_ebupe,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_
     &(15),L_orope,L_(16),
     & L_urope,L_elupe,I_ukupe,L_emupe,R_udupe,REAL(R_exope
     &,4),L_imupe,L_asope,
     & L_umupe,L_esope,L_obupe,L_ubupe,L_adupe,L_idupe,L_odupe
     &,L_edupe)
      !}

      if(L_odupe.or.L_idupe.or.L_edupe.or.L_adupe.or.L_ubupe.or.L_obupe
     &) then      
                  I_ifupe = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ifupe = z'40000000'
      endif
C KPJ_vlv.fgi( 150, 177):���� ���������� �������� � ���������������� ��������,20KPV41AL001
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_uvupe,4),R8_usupe
     &,I_edare,I_udare,I_adare,
     & C8_itupe,I_odare,R_evupe,R_avupe,R_upupe,
     & REAL(R_erupe,4),R_asupe,REAL(R_isupe,4),
     & R_irupe,REAL(R_urupe,4),I_obare,I_afare,I_idare,I_ibare
     &,L_osupe,
     & L_ifare,L_ikare,L_esupe,L_orupe,
     & L_etupe,L_atupe,L_ufare,L_arupe,L_utupe,L_alare,L_ivupe
     &,
     & L_ovupe,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_
     &(17),L_apupe,L_(18),
     & L_epupe,L_ofare,I_efare,L_okare,R_ebare,REAL(R_otupe
     &,4),L_ukare,L_ipupe,
     & L_elare,L_opupe,L_axupe,L_exupe,L_ixupe,L_uxupe,L_abare
     &,L_oxupe)
      !}

      if(L_abare.or.L_uxupe.or.L_oxupe.or.L_ixupe.or.L_exupe.or.L_axupe
     &) then      
                  I_ubare = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ubare = z'40000000'
      endif
C KPJ_vlv.fgi( 150, 202):���� ���������� �������� � ���������������� ��������,20KPV12AA101
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_omomi,4),R8_okomi
     &,I_asomi,I_osomi,I_uromi,
     & C8_elomi,I_isomi,R_amomi,R_ulomi,R_odomi,
     & REAL(R_afomi,4),R_ufomi,REAL(R_ekomi,4),
     & R_efomi,REAL(R_ofomi,4),I_iromi,I_usomi,I_esomi,I_eromi
     &,L_ikomi,
     & L_etomi,L_evomi,L_akomi,L_ifomi,
     & L_alomi,L_ukomi,L_otomi,L_udomi,L_olomi,L_uvomi,L_emomi
     &,
     & L_imomi,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_
     &(135),L_edomi,L_(136),
     & L_idomi,L_itomi,I_atomi,L_ivomi,R_aromi,REAL(R_ilomi
     &,4),L_ovomi,L_irekad,
     & L_axomi,L_osakad,L_umomi,L_apomi,L_epomi,L_opomi,L_upomi
     &,L_ipomi)
      !}

      if(L_upomi.or.L_opomi.or.L_ipomi.or.L_epomi.or.L_apomi.or.L_umomi
     &) then      
                  I_oromi = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_oromi = z'40000000'
      endif
C KPJ_vlv.fgi(  60, 202):���� ���������� �������� � ���������������� ��������,20KPJ21AA104
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_odume,4),R8_oxome
     &,I_alume,I_olume,I_ukume,
     & C8_ebume,I_ilume,R_adume,R_ubume,R_otome,
     & REAL(R_avome,4),R_uvome,REAL(R_exome,4),
     & R_evome,REAL(R_ovome,4),I_ikume,I_ulume,I_elume,I_ekume
     &,L_ixome,
     & L_emume,L_epume,L_axome,L_ivome,
     & L_abume,L_uxome,L_omume,L_utome,L_obume,L_upume,L_edume
     &,
     & L_idume,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_
     &(7),L_atome,L_(8),
     & L_etome,L_imume,I_amume,L_ipume,R_akume,REAL(R_ibume
     &,4),L_opume,L_ufikad,
     & L_arume,L_itome,L_udume,L_afume,L_efume,L_ofume,L_ufume
     &,L_ifume)
      !}

      if(L_ufume.or.L_ofume.or.L_ifume.or.L_efume.or.L_afume.or.L_udume
     &) then      
                  I_okume = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_okume = z'40000000'
      endif
C KPJ_vlv.fgi( 150, 152):���� ���������� �������� � ���������������� ��������,20KPJ80AA102
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_etare,4),R8_erare
     &,I_oxare,I_ebere,I_ixare,
     & C8_urare,I_abere,R_osare,R_isare,R_emare,
     & REAL(R_omare,4),R_ipare,REAL(R_upare,4),
     & R_umare,REAL(R_epare,4),I_axare,I_ibere,I_uxare,I_uvare
     &,L_arare,
     & L_ubere,L_udere,L_opare,L_apare,
     & L_orare,L_irare,L_edere,L_imare,L_esare,L_ifere,L_usare
     &,
     & L_atare,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_
     &(19),L_ilare,L_(20),
     & L_olare,L_adere,I_obere,L_afere,R_ovare,REAL(R_asare
     &,4),L_efere,L_ulare,
     & L_ofere,L_amare,L_itare,L_otare,L_utare,L_evare,L_ivare
     &,L_avare)
      !}

      if(L_ivare.or.L_evare.or.L_avare.or.L_utare.or.L_otare.or.L_itare
     &) then      
                  I_exare = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_exare = z'40000000'
      endif
C KPJ_vlv.fgi( 121, 152):���� ���������� �������� � ���������������� ��������,20KPJ42AA101
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_at,4),R8_ar,I_ix,I_abe
     &,I_ex,
     & C8_or,I_ux,R_is,R_es,R_am,
     & REAL(R_im,4),R_ep,REAL(R_op,4),
     & R_om,REAL(R_ap,4),I_uv,I_ebe,I_ox,I_ov,L_up,
     & L_obe,L_ode,L_ip,L_um,
     & L_ir,L_er,L_ade,L_em,L_as,L_efe,L_os,
     & L_us,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_el,L_il
     &,L_ube,
     & I_ibe,L_ude,R_iv,REAL(R_ur,4),L_afe,L_ol,L_ife,L_ul
     &,
     & L_et,L_it,L_ot,L_av,L_ev,L_ut)
      !}

      if(L_ev.or.L_av.or.L_ut.or.L_ot.or.L_it.or.L_et) then
     &      
                  I_ax = ior(z'000000FF',z'04000000')    
     &                        
                else
                   I_ax = z'40000000'
      endif
C KPG_KPH_man.fgi( 195, 226):���� ���������� �������� ��������,20KPH41AA002
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_ire,4),R8_ime,I_ute
     &,I_ive,I_ote,
     & C8_ape,I_eve,R_upe,R_ope,R_ike,
     & REAL(R_uke,4),R_ole,REAL(R_ame,4),
     & R_ale,REAL(R_ile,4),I_ete,I_ove,I_ave,I_ate,L_eme,
     & L_axe,L_abi,L_ule,L_ele,
     & L_ume,L_ome,L_ixe,L_oke,L_ipe,L_obi,L_are,
     & L_ere,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_ofe
     &,L_ufe,L_exe,
     & I_uve,L_ebi,R_use,REAL(R_epe,4),L_ibi,L_ake,L_ubi,L_eke
     &,
     & L_ore,L_ure,L_ase,L_ise,L_ose,L_ese)
      !}

      if(L_ose.or.L_ise.or.L_ese.or.L_ase.or.L_ure.or.L_ore
     &) then      
                  I_ite = ior(z'000000FF',z'04000000')   
     &                         
                else
                   I_ite = z'40000000'
      endif
C KPG_KPH_man.fgi( 180, 226):���� ���������� �������� ��������,20KPH41AA001
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_umi,4),R8_uki,I_esi
     &,I_usi,I_asi,
     & C8_ili,I_osi,R_emi,R_ami,R_udi,
     & REAL(R_efi,4),R_aki,REAL(R_iki,4),
     & R_ifi,REAL(R_ufi,4),I_ori,I_ati,I_isi,I_iri,L_oki,
     & L_iti,L_ivi,L_eki,L_ofi,
     & L_eli,L_ali,L_uti,L_afi,L_uli,L_axi,L_imi,
     & L_omi,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_adi
     &,L_edi,L_oti,
     & I_eti,L_ovi,R_eri,REAL(R_oli,4),L_uvi,L_idi,L_exi,L_odi
     &,
     & L_api,L_epi,L_ipi,L_upi,L_ari,L_opi)
      !}

      if(L_ari.or.L_upi.or.L_opi.or.L_ipi.or.L_epi.or.L_api
     &) then      
                  I_uri = ior(z'000000FF',z'04000000')   
     &                         
                else
                   I_uri = z'40000000'
      endif
C KPG_KPH_man.fgi( 165, 226):���� ���������� �������� ��������,20KPH15AA002
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_elo,4),R8_efo,I_opo
     &,I_ero,I_ipo,
     & C8_ufo,I_aro,R_oko,R_iko,R_ebo,
     & REAL(R_obo,4),R_ido,REAL(R_udo,4),
     & R_ubo,REAL(R_edo,4),I_apo,I_iro,I_upo,I_umo,L_afo,
     & L_uro,L_uso,L_odo,L_ado,
     & L_ofo,L_ifo,L_eso,L_ibo,L_eko,L_ito,L_uko,
     & L_alo,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_ixi
     &,L_oxi,L_aso,
     & I_oro,L_ato,R_omo,REAL(R_ako,4),L_eto,L_uxi,L_oto,L_abo
     &,
     & L_ilo,L_olo,L_ulo,L_emo,L_imo,L_amo)
      !}

      if(L_imo.or.L_emo.or.L_amo.or.L_ulo.or.L_olo.or.L_ilo
     &) then      
                  I_epo = ior(z'000000FF',z'04000000')   
     &                         
                else
                   I_epo = z'40000000'
      endif
C KPG_KPH_man.fgi( 150, 226):���� ���������� �������� ��������,20KPH15AA001
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_ofu,4),R8_obu,I_amu
     &,I_omu,I_ulu,
     & C8_edu,I_imu,R_afu,R_udu,R_ovo,
     & REAL(R_axo,4),R_uxo,REAL(R_ebu,4),
     & R_exo,REAL(R_oxo,4),I_ilu,I_umu,I_emu,I_elu,L_ibu,
     & L_epu,L_eru,L_abu,L_ixo,
     & L_adu,L_ubu,L_opu,L_uvo,L_odu,L_uru,L_efu,
     & L_ifu,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_uto
     &,L_avo,L_ipu,
     & I_apu,L_iru,R_alu,REAL(R_idu,4),L_oru,L_evo,L_asu,L_ivo
     &,
     & L_ufu,L_aku,L_eku,L_oku,L_uku,L_iku)
      !}

      if(L_uku.or.L_oku.or.L_iku.or.L_eku.or.L_aku.or.L_ufu
     &) then      
                  I_olu = ior(z'000000FF',z'04000000')   
     &                         
                else
                   I_olu = z'40000000'
      endif
C KPG_KPH_man.fgi( 135, 226):���� ���������� �������� ��������,20KPH31AA002
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_adad,4),R8_axu,I_ikad
     &,I_alad,I_ekad,
     & C8_oxu,I_ukad,R_ibad,R_ebad,R_atu,
     & REAL(R_itu,4),R_evu,REAL(R_ovu,4),
     & R_otu,REAL(R_avu,4),I_ufad,I_elad,I_okad,I_ofad,L_uvu
     &,
     & L_olad,L_omad,L_ivu,L_utu,
     & L_ixu,L_exu,L_amad,L_etu,L_abad,L_epad,L_obad,
     & L_ubad,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_esu
     &,L_isu,L_ulad,
     & I_ilad,L_umad,R_ifad,REAL(R_uxu,4),L_apad,L_osu,L_ipad
     &,L_usu,
     & L_edad,L_idad,L_odad,L_afad,L_efad,L_udad)
      !}

      if(L_efad.or.L_afad.or.L_udad.or.L_odad.or.L_idad.or.L_edad
     &) then      
                  I_akad = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_akad = z'40000000'
      endif
C KPG_KPH_man.fgi( 120, 226):���� ���������� �������� ��������,20KPH31AA001
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_ixad,4),R8_itad,I_uded
     &,I_ifed,I_oded,
     & C8_avad,I_efed,R_uvad,R_ovad,R_irad,
     & REAL(R_urad,4),R_osad,REAL(R_atad,4),
     & R_asad,REAL(R_isad,4),I_eded,I_ofed,I_afed,I_aded,L_etad
     &,
     & L_aked,L_aled,L_usad,L_esad,
     & L_utad,L_otad,L_iked,L_orad,L_ivad,L_oled,L_axad,
     & L_exad,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_opad
     &,L_upad,L_eked,
     & I_ufed,L_eled,R_ubed,REAL(R_evad,4),L_iled,L_arad,L_uled
     &,L_erad,
     & L_oxad,L_uxad,L_abed,L_ibed,L_obed,L_ebed)
      !}

      if(L_obed.or.L_ibed.or.L_ebed.or.L_abed.or.L_uxad.or.L_oxad
     &) then      
                  I_ided = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_ided = z'40000000'
      endif
C KPG_KPH_man.fgi( 105, 226):���� ���������� �������� ��������,20KPG32AA002
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_uted,4),R8_ured,I_ebid
     &,I_ubid,I_abid,
     & C8_ised,I_obid,R_eted,R_ated,R_umed,
     & REAL(R_eped,4),R_ared,REAL(R_ired,4),
     & R_iped,REAL(R_uped,4),I_oxed,I_adid,I_ibid,I_ixed,L_ored
     &,
     & L_idid,L_ifid,L_ered,L_oped,
     & L_esed,L_ased,L_udid,L_aped,L_used,L_akid,L_ited,
     & L_oted,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_amed
     &,L_emed,L_odid,
     & I_edid,L_ofid,R_exed,REAL(R_osed,4),L_ufid,L_imed,L_ekid
     &,L_omed,
     & L_aved,L_eved,L_ived,L_uved,L_axed,L_oved)
      !}

      if(L_axed.or.L_uved.or.L_oved.or.L_ived.or.L_eved.or.L_aved
     &) then      
                  I_uxed = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_uxed = z'40000000'
      endif
C KPG_KPH_man.fgi(  90, 226):���� ���������� �������� ��������,20KPG32AA001
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_esid,4),R8_epid,I_ovid
     &,I_exid,I_ivid,
     & C8_upid,I_axid,R_orid,R_irid,R_elid,
     & REAL(R_olid,4),R_imid,REAL(R_umid,4),
     & R_ulid,REAL(R_emid,4),I_avid,I_ixid,I_uvid,I_utid,L_apid
     &,
     & L_uxid,L_ubod,L_omid,L_amid,
     & L_opid,L_ipid,L_ebod,L_ilid,L_erid,L_idod,L_urid,
     & L_asid,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_ikid
     &,L_okid,L_abod,
     & I_oxid,L_adod,R_otid,REAL(R_arid,4),L_edod,L_ukid,L_odod
     &,L_alid,
     & L_isid,L_osid,L_usid,L_etid,L_itid,L_atid)
      !}

      if(L_itid.or.L_etid.or.L_atid.or.L_usid.or.L_osid.or.L_isid
     &) then      
                  I_evid = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_evid = z'40000000'
      endif
C KPG_KPH_man.fgi(  75, 226):���� ���������� �������� ��������,20KPG12AA002
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_opod,4),R8_olod,I_atod
     &,I_otod,I_usod,
     & C8_emod,I_itod,R_apod,R_umod,R_ofod,
     & REAL(R_akod,4),R_ukod,REAL(R_elod,4),
     & R_ekod,REAL(R_okod,4),I_isod,I_utod,I_etod,I_esod,L_ilod
     &,
     & L_evod,L_exod,L_alod,L_ikod,
     & L_amod,L_ulod,L_ovod,L_ufod,L_omod,L_uxod,L_epod,
     & L_ipod,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_udod
     &,L_afod,L_ivod,
     & I_avod,L_ixod,R_asod,REAL(R_imod,4),L_oxod,L_efod,L_abud
     &,L_ifod,
     & L_upod,L_arod,L_erod,L_orod,L_urod,L_irod)
      !}

      if(L_urod.or.L_orod.or.L_irod.or.L_erod.or.L_arod.or.L_upod
     &) then      
                  I_osod = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_osod = z'40000000'
      endif
C KPG_KPH_man.fgi(  60, 226):���� ���������� �������� ��������,20KPG12AA001
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_amud,4),R8_akud,I_irud
     &,I_asud,I_erud,
     & C8_okud,I_urud,R_ilud,R_elud,R_adud,
     & REAL(R_idud,4),R_efud,REAL(R_ofud,4),
     & R_odud,REAL(R_afud,4),I_upud,I_esud,I_orud,I_opud,L_ufud
     &,
     & L_osud,L_otud,L_ifud,L_udud,
     & L_ikud,L_ekud,L_atud,L_edud,L_alud,L_evud,L_olud,
     & L_ulud,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_ebud
     &,L_ibud,L_usud,
     & I_isud,L_utud,R_ipud,REAL(R_ukud,4),L_avud,L_obud,L_ivud
     &,L_ubud,
     & L_emud,L_imud,L_omud,L_apud,L_epud,L_umud)
      !}

      if(L_epud.or.L_apud.or.L_umud.or.L_omud.or.L_imud.or.L_emud
     &) then      
                  I_arud = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_arud = z'40000000'
      endif
C KPG_KPH_man.fgi(  45, 226):���� ���������� �������� ��������,20KPH41AA006
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_ikaf,4),R8_idaf,I_umaf
     &,I_ipaf,I_omaf,
     & C8_afaf,I_epaf,R_ufaf,R_ofaf,R_ixud,
     & REAL(R_uxud,4),R_obaf,REAL(R_adaf,4),
     & R_abaf,REAL(R_ibaf,4),I_emaf,I_opaf,I_apaf,I_amaf,L_edaf
     &,
     & L_araf,L_asaf,L_ubaf,L_ebaf,
     & L_udaf,L_odaf,L_iraf,L_oxud,L_ifaf,L_osaf,L_akaf,
     & L_ekaf,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_ovud
     &,L_uvud,L_eraf,
     & I_upaf,L_esaf,R_ulaf,REAL(R_efaf,4),L_isaf,L_axud,L_usaf
     &,L_exud,
     & L_okaf,L_ukaf,L_alaf,L_ilaf,L_olaf,L_elaf)
      !}

      if(L_olaf.or.L_ilaf.or.L_elaf.or.L_alaf.or.L_ukaf.or.L_okaf
     &) then      
                  I_imaf = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_imaf = z'40000000'
      endif
C KPG_KPH_man.fgi(  30, 226):���� ���������� �������� ��������,20KPH41AA005
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_udef,4),R8_uxaf,I_elef
     &,I_ulef,I_alef,
     & C8_ibef,I_olef,R_edef,R_adef,R_utaf,
     & REAL(R_evaf,4),R_axaf,REAL(R_ixaf,4),
     & R_ivaf,REAL(R_uvaf,4),I_okef,I_amef,I_ilef,I_ikef,L_oxaf
     &,
     & L_imef,L_ipef,L_exaf,L_ovaf,
     & L_ebef,L_abef,L_umef,L_avaf,L_ubef,L_aref,L_idef,
     & L_odef,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_ataf
     &,L_etaf,L_omef,
     & I_emef,L_opef,R_ekef,REAL(R_obef,4),L_upef,L_itaf,L_eref
     &,L_otaf,
     & L_afef,L_efef,L_ifef,L_ufef,L_akef,L_ofef)
      !}

      if(L_akef.or.L_ufef.or.L_ofef.or.L_ifef.or.L_efef.or.L_afef
     &) then      
                  I_ukef = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_ukef = z'40000000'
      endif
C KPG_KPH_man.fgi( 315, 196):���� ���������� �������� ��������,20KPH10AA001
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_ebif,4),R8_evef,I_ofif
     &,I_ekif,I_ifif,
     & C8_uvef,I_akif,R_oxef,R_ixef,R_esef,
     & REAL(R_osef,4),R_itef,REAL(R_utef,4),
     & R_usef,REAL(R_etef,4),I_afif,I_ikif,I_ufif,I_udif,L_avef
     &,
     & L_ukif,L_ulif,L_otef,L_atef,
     & L_ovef,L_ivef,L_elif,L_isef,L_exef,L_imif,L_uxef,
     & L_abif,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_iref
     &,L_oref,L_alif,
     & I_okif,L_amif,R_odif,REAL(R_axef,4),L_emif,L_uref,L_omif
     &,L_asef,
     & L_ibif,L_obif,L_ubif,L_edif,L_idif,L_adif)
      !}

      if(L_idif.or.L_edif.or.L_adif.or.L_ubif.or.L_obif.or.L_ibif
     &) then      
                  I_efif = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_efif = z'40000000'
      endif
C KPG_KPH_man.fgi( 300, 196):���� ���������� �������� ��������,20KPH20AA013
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_ovif,4),R8_osif,I_adof
     &,I_odof,I_ubof,
     & C8_etif,I_idof,R_avif,R_utif,R_opif,
     & REAL(R_arif,4),R_urif,REAL(R_esif,4),
     & R_erif,REAL(R_orif,4),I_ibof,I_udof,I_edof,I_ebof,L_isif
     &,
     & L_efof,L_ekof,L_asif,L_irif,
     & L_atif,L_usif,L_ofof,L_upif,L_otif,L_ukof,L_evif,
     & L_ivif,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_umif
     &,L_apif,L_ifof,
     & I_afof,L_ikof,R_abof,REAL(R_itif,4),L_okof,L_epif,L_alof
     &,L_ipif,
     & L_uvif,L_axif,L_exif,L_oxif,L_uxif,L_ixif)
      !}

      if(L_uxif.or.L_oxif.or.L_ixif.or.L_exif.or.L_axif.or.L_uvif
     &) then      
                  I_obof = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_obof = z'40000000'
      endif
C KPG_KPH_man.fgi( 285, 196):���� ���������� �������� ��������,20KPH20AA005
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_atof,4),R8_arof,I_ixof
     &,I_abuf,I_exof,
     & C8_orof,I_uxof,R_isof,R_esof,R_amof,
     & REAL(R_imof,4),R_epof,REAL(R_opof,4),
     & R_omof,REAL(R_apof,4),I_uvof,I_ebuf,I_oxof,I_ovof,L_upof
     &,
     & L_obuf,L_oduf,L_ipof,L_umof,
     & L_irof,L_erof,L_aduf,L_emof,L_asof,L_efuf,L_osof,
     & L_usof,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_elof
     &,L_ilof,L_ubuf,
     & I_ibuf,L_uduf,R_ivof,REAL(R_urof,4),L_afuf,L_olof,L_ifuf
     &,L_ulof,
     & L_etof,L_itof,L_otof,L_avof,L_evof,L_utof)
      !}

      if(L_evof.or.L_avof.or.L_utof.or.L_otof.or.L_itof.or.L_etof
     &) then      
                  I_axof = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_axof = z'40000000'
      endif
C KPG_KPH_man.fgi( 270, 196):���� ���������� �������� ��������,20KPH13AA002
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_iruf,4),R8_imuf,I_utuf
     &,I_ivuf,I_otuf,
     & C8_apuf,I_evuf,R_upuf,R_opuf,R_ikuf,
     & REAL(R_ukuf,4),R_oluf,REAL(R_amuf,4),
     & R_aluf,REAL(R_iluf,4),I_etuf,I_ovuf,I_avuf,I_atuf,L_emuf
     &,
     & L_axuf,L_abak,L_uluf,L_eluf,
     & L_umuf,L_omuf,L_ixuf,L_okuf,L_ipuf,L_obak,L_aruf,
     & L_eruf,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_ofuf
     &,L_ufuf,L_exuf,
     & I_uvuf,L_ebak,R_usuf,REAL(R_epuf,4),L_ibak,L_akuf,L_ubak
     &,L_ekuf,
     & L_oruf,L_uruf,L_asuf,L_isuf,L_osuf,L_esuf)
      !}

      if(L_osuf.or.L_isuf.or.L_esuf.or.L_asuf.or.L_uruf.or.L_oruf
     &) then      
                  I_ituf = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_ituf = z'40000000'
      endif
C KPG_KPH_man.fgi( 255, 196):���� ���������� �������� ��������,20KPH12AA003
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_umak,4),R8_ukak,I_esak
     &,I_usak,I_asak,
     & C8_ilak,I_osak,R_emak,R_amak,R_udak,
     & REAL(R_efak,4),R_akak,REAL(R_ikak,4),
     & R_ifak,REAL(R_ufak,4),I_orak,I_atak,I_isak,I_irak,L_okak
     &,
     & L_itak,L_ivak,L_ekak,L_ofak,
     & L_elak,L_alak,L_utak,L_afak,L_ulak,L_axak,L_imak,
     & L_omak,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_adak
     &,L_edak,L_otak,
     & I_etak,L_ovak,R_erak,REAL(R_olak,4),L_uvak,L_idak,L_exak
     &,L_odak,
     & L_apak,L_epak,L_ipak,L_upak,L_arak,L_opak)
      !}

      if(L_arak.or.L_upak.or.L_opak.or.L_ipak.or.L_epak.or.L_apak
     &) then      
                  I_urak = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_urak = z'40000000'
      endif
C KPG_KPH_man.fgi( 240, 196):���� ���������� �������� ��������,20KPH11AA002
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_elek,4),R8_efek,I_opek
     &,I_erek,I_ipek,
     & C8_ufek,I_arek,R_okek,R_ikek,R_ebek,
     & REAL(R_obek,4),R_idek,REAL(R_udek,4),
     & R_ubek,REAL(R_edek,4),I_apek,I_irek,I_upek,I_umek,L_afek
     &,
     & L_urek,L_usek,L_odek,L_adek,
     & L_ofek,L_ifek,L_esek,L_ibek,L_ekek,L_itek,L_ukek,
     & L_alek,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_ixak
     &,L_oxak,L_asek,
     & I_orek,L_atek,R_omek,REAL(R_akek,4),L_etek,L_uxak,L_otek
     &,L_abek,
     & L_ilek,L_olek,L_ulek,L_emek,L_imek,L_amek)
      !}

      if(L_imek.or.L_emek.or.L_amek.or.L_ulek.or.L_olek.or.L_ilek
     &) then      
                  I_epek = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_epek = z'40000000'
      endif
C KPG_KPH_man.fgi( 225, 196):���� ���������� �������� ��������,20KPG11AA011
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_ofik,4),R8_obik,I_amik
     &,I_omik,I_ulik,
     & C8_edik,I_imik,R_afik,R_udik,R_ovek,
     & REAL(R_axek,4),R_uxek,REAL(R_ebik,4),
     & R_exek,REAL(R_oxek,4),I_ilik,I_umik,I_emik,I_elik,L_ibik
     &,
     & L_epik,L_erik,L_abik,L_ixek,
     & L_adik,L_ubik,L_opik,L_uvek,L_odik,L_urik,L_efik,
     & L_ifik,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_utek
     &,L_avek,L_ipik,
     & I_apik,L_irik,R_alik,REAL(R_idik,4),L_orik,L_evek,L_asik
     &,L_ivek,
     & L_ufik,L_akik,L_ekik,L_okik,L_ukik,L_ikik)
      !}

      if(L_ukik.or.L_okik.or.L_ikik.or.L_ekik.or.L_akik.or.L_ufik
     &) then      
                  I_olik = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_olik = z'40000000'
      endif
C KPG_KPH_man.fgi( 210, 196):���� ���������� �������� ��������,20KPG31AA002
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_adok,4),R8_axik,I_ikok
     &,I_alok,I_ekok,
     & C8_oxik,I_ukok,R_ibok,R_ebok,R_atik,
     & REAL(R_itik,4),R_evik,REAL(R_ovik,4),
     & R_otik,REAL(R_avik,4),I_ufok,I_elok,I_okok,I_ofok,L_uvik
     &,
     & L_olok,L_omok,L_ivik,L_utik,
     & L_ixik,L_exik,L_amok,L_etik,L_abok,L_epok,L_obok,
     & L_ubok,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_esik
     &,L_isik,L_ulok,
     & I_ilok,L_umok,R_ifok,REAL(R_uxik,4),L_apok,L_osik,L_ipok
     &,L_usik,
     & L_edok,L_idok,L_odok,L_afok,L_efok,L_udok)
      !}

      if(L_efok.or.L_afok.or.L_udok.or.L_odok.or.L_idok.or.L_edok
     &) then      
                  I_akok = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_akok = z'40000000'
      endif
C KPG_KPH_man.fgi( 195, 196):���� ���������� �������� ��������,20KPG21AA002
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_ixok,4),R8_itok,I_uduk
     &,I_ifuk,I_oduk,
     & C8_avok,I_efuk,R_uvok,R_ovok,R_irok,
     & REAL(R_urok,4),R_osok,REAL(R_atok,4),
     & R_asok,REAL(R_isok,4),I_eduk,I_ofuk,I_afuk,I_aduk,L_etok
     &,
     & L_akuk,L_aluk,L_usok,L_esok,
     & L_utok,L_otok,L_ikuk,L_orok,L_ivok,L_oluk,L_axok,
     & L_exok,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_opok
     &,L_upok,L_ekuk,
     & I_ufuk,L_eluk,R_ubuk,REAL(R_evok,4),L_iluk,L_arok,L_uluk
     &,L_erok,
     & L_oxok,L_uxok,L_abuk,L_ibuk,L_obuk,L_ebuk)
      !}

      if(L_obuk.or.L_ibuk.or.L_ebuk.or.L_abuk.or.L_uxok.or.L_oxok
     &) then      
                  I_iduk = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_iduk = z'40000000'
      endif
C KPG_KPH_man.fgi( 180, 196):���� ���������� �������� ��������,20KPG11AA002
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_utuk,4),R8_uruk,I_ebal
     &,I_ubal,I_abal,
     & C8_isuk,I_obal,R_etuk,R_atuk,R_umuk,
     & REAL(R_epuk,4),R_aruk,REAL(R_iruk,4),
     & R_ipuk,REAL(R_upuk,4),I_oxuk,I_adal,I_ibal,I_ixuk,L_oruk
     &,
     & L_idal,L_ifal,L_eruk,L_opuk,
     & L_esuk,L_asuk,L_udal,L_apuk,L_usuk,L_akal,L_ituk,
     & L_otuk,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_amuk
     &,L_emuk,L_odal,
     & I_edal,L_ofal,R_exuk,REAL(R_osuk,4),L_ufal,L_imuk,L_ekal
     &,L_omuk,
     & L_avuk,L_evuk,L_ivuk,L_uvuk,L_axuk,L_ovuk)
      !}

      if(L_axuk.or.L_uvuk.or.L_ovuk.or.L_ivuk.or.L_evuk.or.L_avuk
     &) then      
                  I_uxuk = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_uxuk = z'40000000'
      endif
C KPG_KPH_man.fgi( 165, 196):���� ���������� �������� ��������,20KPH14AA001
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_esal,4),R8_epal,I_oval
     &,I_exal,I_ival,
     & C8_upal,I_axal,R_oral,R_iral,R_elal,
     & REAL(R_olal,4),R_imal,REAL(R_umal,4),
     & R_ulal,REAL(R_emal,4),I_aval,I_ixal,I_uval,I_utal,L_apal
     &,
     & L_uxal,L_ubel,L_omal,L_amal,
     & L_opal,L_ipal,L_ebel,L_ilal,L_eral,L_idel,L_ural,
     & L_asal,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_ikal
     &,L_okal,L_abel,
     & I_oxal,L_adel,R_otal,REAL(R_aral,4),L_edel,L_ukal,L_odel
     &,L_alal,
     & L_isal,L_osal,L_usal,L_etal,L_ital,L_atal)
      !}

      if(L_ital.or.L_etal.or.L_atal.or.L_usal.or.L_osal.or.L_isal
     &) then      
                  I_eval = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_eval = z'40000000'
      endif
C KPG_KPH_man.fgi( 150, 196):���� ���������� �������� ��������,20KPH14AA002
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_opel,4),R8_olel,I_atel
     &,I_otel,I_usel,
     & C8_emel,I_itel,R_apel,R_umel,R_ofel,
     & REAL(R_akel,4),R_ukel,REAL(R_elel,4),
     & R_ekel,REAL(R_okel,4),I_isel,I_utel,I_etel,I_esel,L_ilel
     &,
     & L_evel,L_exel,L_alel,L_ikel,
     & L_amel,L_ulel,L_ovel,L_ufel,L_omel,L_uxel,L_epel,
     & L_ipel,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_udel
     &,L_afel,L_ivel,
     & I_avel,L_ixel,R_asel,REAL(R_imel,4),L_oxel,L_efel,L_abil
     &,L_ifel,
     & L_upel,L_arel,L_erel,L_orel,L_urel,L_irel)
      !}

      if(L_urel.or.L_orel.or.L_irel.or.L_erel.or.L_arel.or.L_upel
     &) then      
                  I_osel = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_osel = z'40000000'
      endif
C KPG_KPH_man.fgi( 135, 196):���� ���������� �������� ��������,20KPH30AA001
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_amil,4),R8_akil,I_iril
     &,I_asil,I_eril,
     & C8_okil,I_uril,R_ilil,R_elil,R_adil,
     & REAL(R_idil,4),R_efil,REAL(R_ofil,4),
     & R_odil,REAL(R_afil,4),I_upil,I_esil,I_oril,I_opil,L_ufil
     &,
     & L_osil,L_otil,L_ifil,L_udil,
     & L_ikil,L_ekil,L_atil,L_edil,L_alil,L_evil,L_olil,
     & L_ulil,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_ebil
     &,L_ibil,L_usil,
     & I_isil,L_util,R_ipil,REAL(R_ukil,4),L_avil,L_obil,L_ivil
     &,L_ubil,
     & L_emil,L_imil,L_omil,L_apil,L_epil,L_umil)
      !}

      if(L_epil.or.L_apil.or.L_umil.or.L_omil.or.L_imil.or.L_emil
     &) then      
                  I_aril = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_aril = z'40000000'
      endif
C KPG_KPH_man.fgi( 120, 196):���� ���������� �������� ��������,20KPG31AA001
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_ikol,4),R8_idol,I_umol
     &,I_ipol,I_omol,
     & C8_afol,I_epol,R_ufol,R_ofol,R_ixil,
     & REAL(R_uxil,4),R_obol,REAL(R_adol,4),
     & R_abol,REAL(R_ibol,4),I_emol,I_opol,I_apol,I_amol,L_edol
     &,
     & L_arol,L_asol,L_ubol,L_ebol,
     & L_udol,L_odol,L_irol,L_oxil,L_ifol,L_osol,L_akol,
     & L_ekol,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_ovil
     &,L_uvil,L_erol,
     & I_upol,L_esol,R_ulol,REAL(R_efol,4),L_isol,L_axil,L_usol
     &,L_exil,
     & L_okol,L_ukol,L_alol,L_ilol,L_olol,L_elol)
      !}

      if(L_olol.or.L_ilol.or.L_elol.or.L_alol.or.L_ukol.or.L_okol
     &) then      
                  I_imol = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_imol = z'40000000'
      endif
C KPG_KPH_man.fgi(  90, 196):���� ���������� �������� ��������,20KPH15AA006
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_udul,4),R8_uxol,I_elul
     &,I_ulul,I_alul,
     & C8_ibul,I_olul,R_edul,R_adul,R_utol,
     & REAL(R_evol,4),R_axol,REAL(R_ixol,4),
     & R_ivol,REAL(R_uvol,4),I_okul,I_amul,I_ilul,I_ikul,L_oxol
     &,
     & L_imul,L_ipul,L_exol,L_ovol,
     & L_ebul,L_abul,L_umul,L_avol,L_ubul,L_arul,L_idul,
     & L_odul,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_atol
     &,L_etol,L_omul,
     & I_emul,L_opul,R_ekul,REAL(R_obul,4),L_upul,L_itol,L_erul
     &,L_otol,
     & L_aful,L_eful,L_iful,L_uful,L_akul,L_oful)
      !}

      if(L_akul.or.L_uful.or.L_oful.or.L_iful.or.L_eful.or.L_aful
     &) then      
                  I_ukul = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_ukul = z'40000000'
      endif
C KPG_KPH_man.fgi(  75, 196):���� ���������� �������� ��������,20KPH15AA005
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_ebam,4),R8_evul,I_ofam
     &,I_ekam,I_ifam,
     & C8_uvul,I_akam,R_oxul,R_ixul,R_esul,
     & REAL(R_osul,4),R_itul,REAL(R_utul,4),
     & R_usul,REAL(R_etul,4),I_afam,I_ikam,I_ufam,I_udam,L_avul
     &,
     & L_ukam,L_ulam,L_otul,L_atul,
     & L_ovul,L_ivul,L_elam,L_isul,L_exul,L_imam,L_uxul,
     & L_abam,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_irul
     &,L_orul,L_alam,
     & I_okam,L_amam,R_odam,REAL(R_axul,4),L_emam,L_urul,L_omam
     &,L_asul,
     & L_ibam,L_obam,L_ubam,L_edam,L_idam,L_adam)
      !}

      if(L_idam.or.L_edam.or.L_adam.or.L_ubam.or.L_obam.or.L_ibam
     &) then      
                  I_efam = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_efam = z'40000000'
      endif
C KPG_KPH_man.fgi(  60, 196):���� ���������� �������� ��������,20KPH31AA106
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_ovam,4),R8_osam,I_adem
     &,I_odem,I_ubem,
     & C8_etam,I_idem,R_avam,R_utam,R_opam,
     & REAL(R_aram,4),R_uram,REAL(R_esam,4),
     & R_eram,REAL(R_oram,4),I_ibem,I_udem,I_edem,I_ebem,L_isam
     &,
     & L_efem,L_ekem,L_asam,L_iram,
     & L_atam,L_usam,L_ofem,L_upam,L_otam,L_ukem,L_evam,
     & L_ivam,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_umam
     &,L_apam,L_ifem,
     & I_afem,L_ikem,R_abem,REAL(R_itam,4),L_okem,L_epam,L_alem
     &,L_ipam,
     & L_uvam,L_axam,L_exam,L_oxam,L_uxam,L_ixam)
      !}

      if(L_uxam.or.L_oxam.or.L_ixam.or.L_exam.or.L_axam.or.L_uvam
     &) then      
                  I_obem = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_obem = z'40000000'
      endif
C KPG_KPH_man.fgi(  45, 196):���� ���������� �������� ��������,20KPH31AA105
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_atem,4),R8_arem,I_ixem
     &,I_abim,I_exem,
     & C8_orem,I_uxem,R_isem,R_esem,R_amem,
     & REAL(R_imem,4),R_epem,REAL(R_opem,4),
     & R_omem,REAL(R_apem,4),I_uvem,I_ebim,I_oxem,I_ovem,L_upem
     &,
     & L_obim,L_odim,L_ipem,L_umem,
     & L_irem,L_erem,L_adim,L_emem,L_asem,L_efim,L_osem,
     & L_usem,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_elem
     &,L_ilem,L_ubim,
     & I_ibim,L_udim,R_ivem,REAL(R_urem,4),L_afim,L_olem,L_ifim
     &,L_ulem,
     & L_etem,L_item,L_otem,L_avem,L_evem,L_utem)
      !}

      if(L_evem.or.L_avem.or.L_utem.or.L_otem.or.L_item.or.L_etem
     &) then      
                  I_axem = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_axem = z'40000000'
      endif
C KPG_KPH_man.fgi(  30, 196):���� ���������� �������� ��������,20KPG32AA006
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_irim,4),R8_imim,I_utim
     &,I_ivim,I_otim,
     & C8_apim,I_evim,R_upim,R_opim,R_ikim,
     & REAL(R_ukim,4),R_olim,REAL(R_amim,4),
     & R_alim,REAL(R_ilim,4),I_etim,I_ovim,I_avim,I_atim,L_emim
     &,
     & L_axim,L_abom,L_ulim,L_elim,
     & L_umim,L_omim,L_ixim,L_okim,L_ipim,L_obom,L_arim,
     & L_erim,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_ofim
     &,L_ufim,L_exim,
     & I_uvim,L_ebom,R_usim,REAL(R_epim,4),L_ibom,L_akim,L_ubom
     &,L_ekim,
     & L_orim,L_urim,L_asim,L_isim,L_osim,L_esim)
      !}

      if(L_osim.or.L_isim.or.L_esim.or.L_asim.or.L_urim.or.L_orim
     &) then      
                  I_itim = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_itim = z'40000000'
      endif
C KPG_KPH_man.fgi( 315, 166):���� ���������� �������� ��������,20KPG32AA005
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_umom,4),R8_ukom,I_esom
     &,I_usom,I_asom,
     & C8_ilom,I_osom,R_emom,R_amom,R_udom,
     & REAL(R_efom,4),R_akom,REAL(R_ikom,4),
     & R_ifom,REAL(R_ufom,4),I_orom,I_atom,I_isom,I_irom,L_okom
     &,
     & L_itom,L_ivom,L_ekom,L_ofom,
     & L_elom,L_alom,L_utom,L_afom,L_ulom,L_axom,L_imom,
     & L_omom,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_adom
     &,L_edom,L_otom,
     & I_etom,L_ovom,R_erom,REAL(R_olom,4),L_uvom,L_idom,L_exom
     &,L_odom,
     & L_apom,L_epom,L_ipom,L_upom,L_arom,L_opom)
      !}

      if(L_arom.or.L_upom.or.L_opom.or.L_ipom.or.L_epom.or.L_apom
     &) then      
                  I_urom = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_urom = z'40000000'
      endif
C KPG_KPH_man.fgi( 300, 166):���� ���������� �������� ��������,20KPH60AA019
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_elum,4),R8_efum,I_opum
     &,I_erum,I_ipum,
     & C8_ufum,I_arum,R_okum,R_ikum,R_ebum,
     & REAL(R_obum,4),R_idum,REAL(R_udum,4),
     & R_ubum,REAL(R_edum,4),I_apum,I_irum,I_upum,I_umum,L_afum
     &,
     & L_urum,L_usum,L_odum,L_adum,
     & L_ofum,L_ifum,L_esum,L_ibum,L_ekum,L_itum,L_ukum,
     & L_alum,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_ixom
     &,L_oxom,L_asum,
     & I_orum,L_atum,R_omum,REAL(R_akum,4),L_etum,L_uxom,L_otum
     &,L_abum,
     & L_ilum,L_olum,L_ulum,L_emum,L_imum,L_amum)
      !}

      if(L_imum.or.L_emum.or.L_amum.or.L_ulum.or.L_olum.or.L_ilum
     &) then      
                  I_epum = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_epum = z'40000000'
      endif
C KPG_KPH_man.fgi( 285, 166):���� ���������� �������� ��������,20KPH60AA018
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_ofap,4),R8_obap,I_amap
     &,I_omap,I_ulap,
     & C8_edap,I_imap,R_afap,R_udap,R_ovum,
     & REAL(R_axum,4),R_uxum,REAL(R_ebap,4),
     & R_exum,REAL(R_oxum,4),I_ilap,I_umap,I_emap,I_elap,L_ibap
     &,
     & L_epap,L_erap,L_abap,L_ixum,
     & L_adap,L_ubap,L_opap,L_uvum,L_odap,L_urap,L_efap,
     & L_ifap,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_utum
     &,L_avum,L_ipap,
     & I_apap,L_irap,R_alap,REAL(R_idap,4),L_orap,L_evum,L_asap
     &,L_ivum,
     & L_ufap,L_akap,L_ekap,L_okap,L_ukap,L_ikap)
      !}

      if(L_ukap.or.L_okap.or.L_ikap.or.L_ekap.or.L_akap.or.L_ufap
     &) then      
                  I_olap = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_olap = z'40000000'
      endif
C KPG_KPH_man.fgi( 270, 166):���� ���������� �������� ��������,20KPH60AA016
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_adep,4),R8_axap,I_ikep
     &,I_alep,I_ekep,
     & C8_oxap,I_ukep,R_ibep,R_ebep,R_atap,
     & REAL(R_itap,4),R_evap,REAL(R_ovap,4),
     & R_otap,REAL(R_avap,4),I_ufep,I_elep,I_okep,I_ofep,L_uvap
     &,
     & L_olep,L_omep,L_ivap,L_utap,
     & L_ixap,L_exap,L_amep,L_etap,L_abep,L_epep,L_obep,
     & L_ubep,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_esap
     &,L_isap,L_ulep,
     & I_ilep,L_umep,R_ifep,REAL(R_uxap,4),L_apep,L_osap,L_ipep
     &,L_usap,
     & L_edep,L_idep,L_odep,L_afep,L_efep,L_udep)
      !}

      if(L_efep.or.L_afep.or.L_udep.or.L_odep.or.L_idep.or.L_edep
     &) then      
                  I_akep = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_akep = z'40000000'
      endif
C KPG_KPH_man.fgi( 255, 166):���� ���������� �������� ��������,20KPH60AA017
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_ixep,4),R8_itep,I_udip
     &,I_ifip,I_odip,
     & C8_avep,I_efip,R_uvep,R_ovep,R_irep,
     & REAL(R_urep,4),R_osep,REAL(R_atep,4),
     & R_asep,REAL(R_isep,4),I_edip,I_ofip,I_afip,I_adip,L_etep
     &,
     & L_akip,L_alip,L_usep,L_esep,
     & L_utep,L_otep,L_ikip,L_orep,L_ivep,L_olip,L_axep,
     & L_exep,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_opep
     &,L_upep,L_ekip,
     & I_ufip,L_elip,R_ubip,REAL(R_evep,4),L_ilip,L_arep,L_ulip
     &,L_erep,
     & L_oxep,L_uxep,L_abip,L_ibip,L_obip,L_ebip)
      !}

      if(L_obip.or.L_ibip.or.L_ebip.or.L_abip.or.L_uxep.or.L_oxep
     &) then      
                  I_idip = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_idip = z'40000000'
      endif
C KPG_KPH_man.fgi( 240, 166):���� ���������� �������� ��������,20KPH60AA015
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_utip,4),R8_urip,I_ebop
     &,I_ubop,I_abop,
     & C8_isip,I_obop,R_etip,R_atip,R_umip,
     & REAL(R_epip,4),R_arip,REAL(R_irip,4),
     & R_ipip,REAL(R_upip,4),I_oxip,I_adop,I_ibop,I_ixip,L_orip
     &,
     & L_idop,L_ifop,L_erip,L_opip,
     & L_esip,L_asip,L_udop,L_apip,L_usip,L_akop,L_itip,
     & L_otip,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_amip
     &,L_emip,L_odop,
     & I_edop,L_ofop,R_exip,REAL(R_osip,4),L_ufop,L_imip,L_ekop
     &,L_omip,
     & L_avip,L_evip,L_ivip,L_uvip,L_axip,L_ovip)
      !}

      if(L_axip.or.L_uvip.or.L_ovip.or.L_ivip.or.L_evip.or.L_avip
     &) then      
                  I_uxip = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_uxip = z'40000000'
      endif
C KPG_KPH_man.fgi( 225, 166):���� ���������� �������� ��������,20KPG42AA001
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_esop,4),R8_epop,I_ovop
     &,I_exop,I_ivop,
     & C8_upop,I_axop,R_orop,R_irop,R_elop,
     & REAL(R_olop,4),R_imop,REAL(R_umop,4),
     & R_ulop,REAL(R_emop,4),I_avop,I_ixop,I_uvop,I_utop,L_apop
     &,
     & L_uxop,L_ubup,L_omop,L_amop,
     & L_opop,L_ipop,L_ebup,L_ilop,L_erop,L_idup,L_urop,
     & L_asop,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_ikop
     &,L_okop,L_abup,
     & I_oxop,L_adup,R_otop,REAL(R_arop,4),L_edup,L_ukop,L_odup
     &,L_alop,
     & L_isop,L_osop,L_usop,L_etop,L_itop,L_atop)
      !}

      if(L_itop.or.L_etop.or.L_atop.or.L_usop.or.L_osop.or.L_isop
     &) then      
                  I_evop = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_evop = z'40000000'
      endif
C KPG_KPH_man.fgi( 210, 166):���� ���������� �������� ��������,20KPH61AA002
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_opup,4),R8_olup,I_atup
     &,I_otup,I_usup,
     & C8_emup,I_itup,R_apup,R_umup,R_ofup,
     & REAL(R_akup,4),R_ukup,REAL(R_elup,4),
     & R_ekup,REAL(R_okup,4),I_isup,I_utup,I_etup,I_esup,L_ilup
     &,
     & L_evup,L_exup,L_alup,L_ikup,
     & L_amup,L_ulup,L_ovup,L_ufup,L_omup,L_uxup,L_epup,
     & L_ipup,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_udup
     &,L_afup,L_ivup,
     & I_avup,L_ixup,R_asup,REAL(R_imup,4),L_oxup,L_efup,L_abar
     &,L_ifup,
     & L_upup,L_arup,L_erup,L_orup,L_urup,L_irup)
      !}

      if(L_urup.or.L_orup.or.L_irup.or.L_erup.or.L_arup.or.L_upup
     &) then      
                  I_osup = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_osup = z'40000000'
      endif
C KPG_KPH_man.fgi( 195, 166):���� ���������� �������� ��������,20KPH61AA001
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_amar,4),R8_akar,I_irar
     &,I_asar,I_erar,
     & C8_okar,I_urar,R_ilar,R_elar,R_adar,
     & REAL(R_idar,4),R_efar,REAL(R_ofar,4),
     & R_odar,REAL(R_afar,4),I_upar,I_esar,I_orar,I_opar,L_ufar
     &,
     & L_osar,L_otar,L_ifar,L_udar,
     & L_ikar,L_ekar,L_atar,L_edar,L_alar,L_evar,L_olar,
     & L_ular,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_ebar
     &,L_ibar,L_usar,
     & I_isar,L_utar,R_ipar,REAL(R_ukar,4),L_avar,L_obar,L_ivar
     &,L_ubar,
     & L_emar,L_imar,L_omar,L_apar,L_epar,L_umar)
      !}

      if(L_epar.or.L_apar.or.L_umar.or.L_omar.or.L_imar.or.L_emar
     &) then      
                  I_arar = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_arar = z'40000000'
      endif
C KPG_KPH_man.fgi( 180, 166):���� ���������� �������� ��������,20KPG41AA008
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_iker,4),R8_ider,I_umer
     &,I_iper,I_omer,
     & C8_afer,I_eper,R_ufer,R_ofer,R_ixar,
     & REAL(R_uxar,4),R_ober,REAL(R_ader,4),
     & R_aber,REAL(R_iber,4),I_emer,I_oper,I_aper,I_amer,L_eder
     &,
     & L_arer,L_aser,L_uber,L_eber,
     & L_uder,L_oder,L_irer,L_oxar,L_ifer,L_oser,L_aker,
     & L_eker,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_ovar
     &,L_uvar,L_erer,
     & I_uper,L_eser,R_uler,REAL(R_efer,4),L_iser,L_axar,L_user
     &,L_exar,
     & L_oker,L_uker,L_aler,L_iler,L_oler,L_eler)
      !}

      if(L_oler.or.L_iler.or.L_eler.or.L_aler.or.L_uker.or.L_oker
     &) then      
                  I_imer = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_imer = z'40000000'
      endif
C KPG_KPH_man.fgi( 165, 166):���� ���������� �������� ��������,20KPG41AA004
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_udir,4),R8_uxer,I_elir
     &,I_ulir,I_alir,
     & C8_ibir,I_olir,R_edir,R_adir,R_uter,
     & REAL(R_ever,4),R_axer,REAL(R_ixer,4),
     & R_iver,REAL(R_uver,4),I_okir,I_amir,I_ilir,I_ikir,L_oxer
     &,
     & L_imir,L_ipir,L_exer,L_over,
     & L_ebir,L_abir,L_umir,L_aver,L_ubir,L_arir,L_idir,
     & L_odir,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_ater
     &,L_eter,L_omir,
     & I_emir,L_opir,R_ekir,REAL(R_obir,4),L_upir,L_iter,L_erir
     &,L_oter,
     & L_afir,L_efir,L_ifir,L_ufir,L_akir,L_ofir)
      !}

      if(L_akir.or.L_ufir.or.L_ofir.or.L_ifir.or.L_efir.or.L_afir
     &) then      
                  I_ukir = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_ukir = z'40000000'
      endif
C KPG_KPH_man.fgi( 150, 166):���� ���������� �������� ��������,20KPG41AA007
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_ebor,4),R8_evir,I_ofor
     &,I_ekor,I_ifor,
     & C8_uvir,I_akor,R_oxir,R_ixir,R_esir,
     & REAL(R_osir,4),R_itir,REAL(R_utir,4),
     & R_usir,REAL(R_etir,4),I_afor,I_ikor,I_ufor,I_udor,L_avir
     &,
     & L_ukor,L_ulor,L_otir,L_atir,
     & L_ovir,L_ivir,L_elor,L_isir,L_exir,L_imor,L_uxir,
     & L_abor,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_irir
     &,L_orir,L_alor,
     & I_okor,L_amor,R_odor,REAL(R_axir,4),L_emor,L_urir,L_omor
     &,L_asir,
     & L_ibor,L_obor,L_ubor,L_edor,L_idor,L_ador)
      !}

      if(L_idor.or.L_edor.or.L_ador.or.L_ubor.or.L_obor.or.L_ibor
     &) then      
                  I_efor = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_efor = z'40000000'
      endif
C KPG_KPH_man.fgi( 135, 166):���� ���������� �������� ��������,20KPG41AA003
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_ovor,4),R8_osor,I_adur
     &,I_odur,I_ubur,
     & C8_etor,I_idur,R_avor,R_utor,R_opor,
     & REAL(R_aror,4),R_uror,REAL(R_esor,4),
     & R_eror,REAL(R_oror,4),I_ibur,I_udur,I_edur,I_ebur,L_isor
     &,
     & L_efur,L_ekur,L_asor,L_iror,
     & L_ator,L_usor,L_ofur,L_upor,L_otor,L_ukur,L_evor,
     & L_ivor,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_umor
     &,L_apor,L_ifur,
     & I_afur,L_ikur,R_abur,REAL(R_itor,4),L_okur,L_epor,L_alur
     &,L_ipor,
     & L_uvor,L_axor,L_exor,L_oxor,L_uxor,L_ixor)
      !}

      if(L_uxor.or.L_oxor.or.L_ixor.or.L_exor.or.L_axor.or.L_uvor
     &) then      
                  I_obur = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_obur = z'40000000'
      endif
C KPG_KPH_man.fgi( 120, 166):���� ���������� �������� ��������,20KPH20AA011
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_atur,4),R8_arur,I_ixur
     &,I_abas,I_exur,
     & C8_orur,I_uxur,R_isur,R_esur,R_amur,
     & REAL(R_imur,4),R_epur,REAL(R_opur,4),
     & R_omur,REAL(R_apur,4),I_uvur,I_ebas,I_oxur,I_ovur,L_upur
     &,
     & L_obas,L_odas,L_ipur,L_umur,
     & L_irur,L_erur,L_adas,L_emur,L_asur,L_efas,L_osur,
     & L_usur,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_elur
     &,L_ilur,L_ubas,
     & I_ibas,L_udas,R_ivur,REAL(R_urur,4),L_afas,L_olur,L_ifas
     &,L_ulur,
     & L_etur,L_itur,L_otur,L_avur,L_evur,L_utur)
      !}

      if(L_evur.or.L_avur.or.L_utur.or.L_otur.or.L_itur.or.L_etur
     &) then      
                  I_axur = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_axur = z'40000000'
      endif
C KPG_KPH_man.fgi( 105, 166):���� ���������� �������� ��������,20KPH20AA012
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_iras,4),R8_imas,I_utas
     &,I_ivas,I_otas,
     & C8_apas,I_evas,R_upas,R_opas,R_ikas,
     & REAL(R_ukas,4),R_olas,REAL(R_amas,4),
     & R_alas,REAL(R_ilas,4),I_etas,I_ovas,I_avas,I_atas,L_emas
     &,
     & L_axas,L_abes,L_ulas,L_elas,
     & L_umas,L_omas,L_ixas,L_okas,L_ipas,L_obes,L_aras,
     & L_eras,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_ofas
     &,L_ufas,L_exas,
     & I_uvas,L_ebes,R_usas,REAL(R_epas,4),L_ibes,L_akas,L_ubes
     &,L_ekas,
     & L_oras,L_uras,L_asas,L_isas,L_osas,L_esas)
      !}

      if(L_osas.or.L_isas.or.L_esas.or.L_asas.or.L_uras.or.L_oras
     &) then      
                  I_itas = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_itas = z'40000000'
      endif
C KPG_KPH_man.fgi(  90, 166):���� ���������� �������� ��������,20KPH13AA007
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_umes,4),R8_ukes,I_eses
     &,I_uses,I_ases,
     & C8_iles,I_oses,R_emes,R_ames,R_udes,
     & REAL(R_efes,4),R_akes,REAL(R_ikes,4),
     & R_ifes,REAL(R_ufes,4),I_ores,I_ates,I_ises,I_ires,L_okes
     &,
     & L_ites,L_ives,L_ekes,L_ofes,
     & L_eles,L_ales,L_utes,L_afes,L_ules,L_axes,L_imes,
     & L_omes,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_ades
     &,L_edes,L_otes,
     & I_etes,L_oves,R_eres,REAL(R_oles,4),L_uves,L_ides,L_exes
     &,L_odes,
     & L_apes,L_epes,L_ipes,L_upes,L_ares,L_opes)
      !}

      if(L_ares.or.L_upes.or.L_opes.or.L_ipes.or.L_epes.or.L_apes
     &) then      
                  I_ures = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_ures = z'40000000'
      endif
C KPG_KPH_man.fgi(  75, 166):���� ���������� �������� ��������,20KPH13AA003
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_elis,4),R8_efis,I_opis
     &,I_eris,I_ipis,
     & C8_ufis,I_aris,R_okis,R_ikis,R_ebis,
     & REAL(R_obis,4),R_idis,REAL(R_udis,4),
     & R_ubis,REAL(R_edis,4),I_apis,I_iris,I_upis,I_umis,L_afis
     &,
     & L_uris,L_usis,L_odis,L_adis,
     & L_ofis,L_ifis,L_esis,L_ibis,L_ekis,L_itis,L_ukis,
     & L_alis,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_ixes
     &,L_oxes,L_asis,
     & I_oris,L_atis,R_omis,REAL(R_akis,4),L_etis,L_uxes,L_otis
     &,L_abis,
     & L_ilis,L_olis,L_ulis,L_emis,L_imis,L_amis)
      !}

      if(L_imis.or.L_emis.or.L_amis.or.L_ulis.or.L_olis.or.L_ilis
     &) then      
                  I_epis = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_epis = z'40000000'
      endif
C KPG_KPH_man.fgi(  60, 166):���� ���������� �������� ��������,20KPH12AA005
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_ofos,4),R8_obos,I_amos
     &,I_omos,I_ulos,
     & C8_edos,I_imos,R_afos,R_udos,R_ovis,
     & REAL(R_axis,4),R_uxis,REAL(R_ebos,4),
     & R_exis,REAL(R_oxis,4),I_ilos,I_umos,I_emos,I_elos,L_ibos
     &,
     & L_epos,L_eros,L_abos,L_ixis,
     & L_ados,L_ubos,L_opos,L_uvis,L_odos,L_uros,L_efos,
     & L_ifos,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_utis
     &,L_avis,L_ipos,
     & I_apos,L_iros,R_alos,REAL(R_idos,4),L_oros,L_evis,L_asos
     &,L_ivis,
     & L_ufos,L_akos,L_ekos,L_okos,L_ukos,L_ikos)
      !}

      if(L_ukos.or.L_okos.or.L_ikos.or.L_ekos.or.L_akos.or.L_ufos
     &) then      
                  I_olos = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_olos = z'40000000'
      endif
C KPG_KPH_man.fgi(  45, 166):���� ���������� �������� ��������,20KPH12AA004
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_adus,4),R8_axos,I_ikus
     &,I_alus,I_ekus,
     & C8_oxos,I_ukus,R_ibus,R_ebus,R_atos,
     & REAL(R_itos,4),R_evos,REAL(R_ovos,4),
     & R_otos,REAL(R_avos,4),I_ufus,I_elus,I_okus,I_ofus,L_uvos
     &,
     & L_olus,L_omus,L_ivos,L_utos,
     & L_ixos,L_exos,L_amus,L_etos,L_abus,L_epus,L_obus,
     & L_ubus,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_esos
     &,L_isos,L_ulus,
     & I_ilus,L_umus,R_ifus,REAL(R_uxos,4),L_apus,L_osos,L_ipus
     &,L_usos,
     & L_edus,L_idus,L_odus,L_afus,L_efus,L_udus)
      !}

      if(L_efus.or.L_afus.or.L_udus.or.L_odus.or.L_idus.or.L_edus
     &) then      
                  I_akus = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_akus = z'40000000'
      endif
C KPG_KPH_man.fgi(  30, 166):���� ���������� �������� ��������,20KPH51AA002
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_ixus,4),R8_itus,I_udat
     &,I_ifat,I_odat,
     & C8_avus,I_efat,R_uvus,R_ovus,R_irus,
     & REAL(R_urus,4),R_osus,REAL(R_atus,4),
     & R_asus,REAL(R_isus,4),I_edat,I_ofat,I_afat,I_adat,L_etus
     &,
     & L_akat,L_alat,L_usus,L_esus,
     & L_utus,L_otus,L_ikat,L_orus,L_ivus,L_olat,L_axus,
     & L_exus,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_opus
     &,L_upus,L_ekat,
     & I_ufat,L_elat,R_ubat,REAL(R_evus,4),L_ilat,L_arus,L_ulat
     &,L_erus,
     & L_oxus,L_uxus,L_abat,L_ibat,L_obat,L_ebat)
      !}

      if(L_obat.or.L_ibat.or.L_ebat.or.L_abat.or.L_uxus.or.L_oxus
     &) then      
                  I_idat = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_idat = z'40000000'
      endif
C KPG_KPH_man.fgi( 315, 136):���� ���������� �������� ��������,20KPH51AA003
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_utat,4),R8_urat,I_ebet
     &,I_ubet,I_abet,
     & C8_isat,I_obet,R_etat,R_atat,R_umat,
     & REAL(R_epat,4),R_arat,REAL(R_irat,4),
     & R_ipat,REAL(R_upat,4),I_oxat,I_adet,I_ibet,I_ixat,L_orat
     &,
     & L_idet,L_ifet,L_erat,L_opat,
     & L_esat,L_asat,L_udet,L_apat,L_usat,L_aket,L_itat,
     & L_otat,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_amat
     &,L_emat,L_odet,
     & I_edet,L_ofet,R_exat,REAL(R_osat,4),L_ufet,L_imat,L_eket
     &,L_omat,
     & L_avat,L_evat,L_ivat,L_uvat,L_axat,L_ovat)
      !}

      if(L_axat.or.L_uvat.or.L_ovat.or.L_ivat.or.L_evat.or.L_avat
     &) then      
                  I_uxat = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_uxat = z'40000000'
      endif
C KPG_KPH_man.fgi( 300, 136):���� ���������� �������� ��������,20KPG52AA016
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_eset,4),R8_epet,I_ovet
     &,I_exet,I_ivet,
     & C8_upet,I_axet,R_oret,R_iret,R_elet,
     & REAL(R_olet,4),R_imet,REAL(R_umet,4),
     & R_ulet,REAL(R_emet,4),I_avet,I_ixet,I_uvet,I_utet,L_apet
     &,
     & L_uxet,L_ubit,L_omet,L_amet,
     & L_opet,L_ipet,L_ebit,L_ilet,L_eret,L_idit,L_uret,
     & L_aset,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_iket
     &,L_oket,L_abit,
     & I_oxet,L_adit,R_otet,REAL(R_aret,4),L_edit,L_uket,L_odit
     &,L_alet,
     & L_iset,L_oset,L_uset,L_etet,L_itet,L_atet)
      !}

      if(L_itet.or.L_etet.or.L_atet.or.L_uset.or.L_oset.or.L_iset
     &) then      
                  I_evet = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_evet = z'40000000'
      endif
C KPG_KPH_man.fgi( 285, 136):���� ���������� �������� ��������,20KPG52AA012
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_opit,4),R8_olit,I_atit
     &,I_otit,I_usit,
     & C8_emit,I_itit,R_apit,R_umit,R_ofit,
     & REAL(R_akit,4),R_ukit,REAL(R_elit,4),
     & R_ekit,REAL(R_okit,4),I_isit,I_utit,I_etit,I_esit,L_ilit
     &,
     & L_evit,L_exit,L_alit,L_ikit,
     & L_amit,L_ulit,L_ovit,L_ufit,L_omit,L_uxit,L_epit,
     & L_ipit,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_udit
     &,L_afit,L_ivit,
     & I_avit,L_ixit,R_asit,REAL(R_imit,4),L_oxit,L_efit,L_abot
     &,L_ifit,
     & L_upit,L_arit,L_erit,L_orit,L_urit,L_irit)
      !}

      if(L_urit.or.L_orit.or.L_irit.or.L_erit.or.L_arit.or.L_upit
     &) then      
                  I_osit = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_osit = z'40000000'
      endif
C KPG_KPH_man.fgi( 270, 136):���� ���������� �������� ��������,20KPG52AA011
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_amot,4),R8_akot,I_irot
     &,I_asot,I_erot,
     & C8_okot,I_urot,R_ilot,R_elot,R_adot,
     & REAL(R_idot,4),R_efot,REAL(R_ofot,4),
     & R_odot,REAL(R_afot,4),I_upot,I_esot,I_orot,I_opot,L_ufot
     &,
     & L_osot,L_otot,L_ifot,L_udot,
     & L_ikot,L_ekot,L_atot,L_edot,L_alot,L_evot,L_olot,
     & L_ulot,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_ebot
     &,L_ibot,L_usot,
     & I_isot,L_utot,R_ipot,REAL(R_ukot,4),L_avot,L_obot,L_ivot
     &,L_ubot,
     & L_emot,L_imot,L_omot,L_apot,L_epot,L_umot)
      !}

      if(L_epot.or.L_apot.or.L_umot.or.L_omot.or.L_imot.or.L_emot
     &) then      
                  I_arot = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_arot = z'40000000'
      endif
C KPG_KPH_man.fgi( 255, 136):���� ���������� �������� ��������,20KPG52AA002
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_ikut,4),R8_idut,I_umut
     &,I_iput,I_omut,
     & C8_afut,I_eput,R_ufut,R_ofut,R_ixot,
     & REAL(R_uxot,4),R_obut,REAL(R_adut,4),
     & R_abut,REAL(R_ibut,4),I_emut,I_oput,I_aput,I_amut,L_edut
     &,
     & L_arut,L_asut,L_ubut,L_ebut,
     & L_udut,L_odut,L_irut,L_oxot,L_ifut,L_osut,L_akut,
     & L_ekut,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_ovot
     &,L_uvot,L_erut,
     & I_uput,L_esut,R_ulut,REAL(R_efut,4),L_isut,L_axot,L_usut
     &,L_exot,
     & L_okut,L_ukut,L_alut,L_ilut,L_olut,L_elut)
      !}

      if(L_olut.or.L_ilut.or.L_elut.or.L_alut.or.L_ukut.or.L_okut
     &) then      
                  I_imut = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_imut = z'40000000'
      endif
C KPG_KPH_man.fgi( 240, 136):���� ���������� �������� ��������,20KPG52AA003
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_udav,4),R8_uxut,I_elav
     &,I_ulav,I_alav,
     & C8_ibav,I_olav,R_edav,R_adav,R_utut,
     & REAL(R_evut,4),R_axut,REAL(R_ixut,4),
     & R_ivut,REAL(R_uvut,4),I_okav,I_amav,I_ilav,I_ikav,L_oxut
     &,
     & L_imav,L_ipav,L_exut,L_ovut,
     & L_ebav,L_abav,L_umav,L_avut,L_ubav,L_arav,L_idav,
     & L_odav,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_atut
     &,L_etut,L_omav,
     & I_emav,L_opav,R_ekav,REAL(R_obav,4),L_upav,L_itut,L_erav
     &,L_otut,
     & L_afav,L_efav,L_ifav,L_ufav,L_akav,L_ofav)
      !}

      if(L_akav.or.L_ufav.or.L_ofav.or.L_ifav.or.L_efav.or.L_afav
     &) then      
                  I_ukav = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_ukav = z'40000000'
      endif
C KPG_KPH_man.fgi( 225, 136):���� ���������� �������� ��������,20KPG51AA002
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_ebev,4),R8_evav,I_ofev
     &,I_ekev,I_ifev,
     & C8_uvav,I_akev,R_oxav,R_ixav,R_esav,
     & REAL(R_osav,4),R_itav,REAL(R_utav,4),
     & R_usav,REAL(R_etav,4),I_afev,I_ikev,I_ufev,I_udev,L_avav
     &,
     & L_ukev,L_ulev,L_otav,L_atav,
     & L_ovav,L_ivav,L_elev,L_isav,L_exav,L_imev,L_uxav,
     & L_abev,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_irav
     &,L_orav,L_alev,
     & I_okev,L_amev,R_odev,REAL(R_axav,4),L_emev,L_urav,L_omev
     &,L_asav,
     & L_ibev,L_obev,L_ubev,L_edev,L_idev,L_adev)
      !}

      if(L_idev.or.L_edev.or.L_adev.or.L_ubev.or.L_obev.or.L_ibev
     &) then      
                  I_efev = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_efev = z'40000000'
      endif
C KPG_KPH_man.fgi( 210, 136):���� ���������� �������� ��������,20KPG51AA003
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_ovev,4),R8_osev,I_adiv
     &,I_odiv,I_ubiv,
     & C8_etev,I_idiv,R_avev,R_utev,R_opev,
     & REAL(R_arev,4),R_urev,REAL(R_esev,4),
     & R_erev,REAL(R_orev,4),I_ibiv,I_udiv,I_ediv,I_ebiv,L_isev
     &,
     & L_efiv,L_ekiv,L_asev,L_irev,
     & L_atev,L_usev,L_ofiv,L_upev,L_otev,L_ukiv,L_evev,
     & L_ivev,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_umev
     &,L_apev,L_ifiv,
     & I_afiv,L_ikiv,R_abiv,REAL(R_itev,4),L_okiv,L_epev,L_aliv
     &,L_ipev,
     & L_uvev,L_axev,L_exev,L_oxev,L_uxev,L_ixev)
      !}

      if(L_uxev.or.L_oxev.or.L_ixev.or.L_exev.or.L_axev.or.L_uvev
     &) then      
                  I_obiv = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_obiv = z'40000000'
      endif
C KPG_KPH_man.fgi( 195, 136):���� ���������� �������� ��������,20KPH11AA011
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_ativ,4),R8_ariv,I_ixiv
     &,I_abov,I_exiv,
     & C8_oriv,I_uxiv,R_isiv,R_esiv,R_amiv,
     & REAL(R_imiv,4),R_epiv,REAL(R_opiv,4),
     & R_omiv,REAL(R_apiv,4),I_uviv,I_ebov,I_oxiv,I_oviv,L_upiv
     &,
     & L_obov,L_odov,L_ipiv,L_umiv,
     & L_iriv,L_eriv,L_adov,L_emiv,L_asiv,L_efov,L_osiv,
     & L_usiv,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_eliv
     &,L_iliv,L_ubov,
     & I_ibov,L_udov,R_iviv,REAL(R_uriv,4),L_afov,L_oliv,L_ifov
     &,L_uliv,
     & L_etiv,L_itiv,L_otiv,L_aviv,L_eviv,L_utiv)
      !}

      if(L_eviv.or.L_aviv.or.L_utiv.or.L_otiv.or.L_itiv.or.L_etiv
     &) then      
                  I_axiv = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_axiv = z'40000000'
      endif
C KPG_KPH_man.fgi( 180, 136):���� ���������� �������� ��������,20KPH10AA009
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_irov,4),R8_imov,I_utov
     &,I_ivov,I_otov,
     & C8_apov,I_evov,R_upov,R_opov,R_ikov,
     & REAL(R_ukov,4),R_olov,REAL(R_amov,4),
     & R_alov,REAL(R_ilov,4),I_etov,I_ovov,I_avov,I_atov,L_emov
     &,
     & L_axov,L_abuv,L_ulov,L_elov,
     & L_umov,L_omov,L_ixov,L_okov,L_ipov,L_obuv,L_arov,
     & L_erov,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_ofov
     &,L_ufov,L_exov,
     & I_uvov,L_ebuv,R_usov,REAL(R_epov,4),L_ibuv,L_akov,L_ubuv
     &,L_ekov,
     & L_orov,L_urov,L_asov,L_isov,L_osov,L_esov)
      !}

      if(L_osov.or.L_isov.or.L_esov.or.L_asov.or.L_urov.or.L_orov
     &) then      
                  I_itov = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_itov = z'40000000'
      endif
C KPG_KPH_man.fgi( 165, 136):���� ���������� �������� ��������,20KPH51AA006
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_umuv,4),R8_ukuv,I_esuv
     &,I_usuv,I_asuv,
     & C8_iluv,I_osuv,R_emuv,R_amuv,R_uduv,
     & REAL(R_efuv,4),R_akuv,REAL(R_ikuv,4),
     & R_ifuv,REAL(R_ufuv,4),I_oruv,I_atuv,I_isuv,I_iruv,L_okuv
     &,
     & L_ituv,L_ivuv,L_ekuv,L_ofuv,
     & L_eluv,L_aluv,L_utuv,L_afuv,L_uluv,L_axuv,L_imuv,
     & L_omuv,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_aduv
     &,L_eduv,L_otuv,
     & I_etuv,L_ovuv,R_eruv,REAL(R_oluv,4),L_uvuv,L_iduv,L_exuv
     &,L_oduv,
     & L_apuv,L_epuv,L_ipuv,L_upuv,L_aruv,L_opuv)
      !}

      if(L_aruv.or.L_upuv.or.L_opuv.or.L_ipuv.or.L_epuv.or.L_apuv
     &) then      
                  I_uruv = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_uruv = z'40000000'
      endif
C KPG_KPH_man.fgi( 150, 136):���� ���������� �������� ��������,20KPH11AA003
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_elax,4),R8_efax,I_opax
     &,I_erax,I_ipax,
     & C8_ufax,I_arax,R_okax,R_ikax,R_ebax,
     & REAL(R_obax,4),R_idax,REAL(R_udax,4),
     & R_ubax,REAL(R_edax,4),I_apax,I_irax,I_upax,I_umax,L_afax
     &,
     & L_urax,L_usax,L_odax,L_adax,
     & L_ofax,L_ifax,L_esax,L_ibax,L_ekax,L_itax,L_ukax,
     & L_alax,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_ixuv
     &,L_oxuv,L_asax,
     & I_orax,L_atax,R_omax,REAL(R_akax,4),L_etax,L_uxuv,L_otax
     &,L_abax,
     & L_ilax,L_olax,L_ulax,L_emax,L_imax,L_amax)
      !}

      if(L_imax.or.L_emax.or.L_amax.or.L_ulax.or.L_olax.or.L_ilax
     &) then      
                  I_epax = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_epax = z'40000000'
      endif
C KPG_KPH_man.fgi( 135, 136):���� ���������� �������� ��������,20KPH20AA003
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_ofex,4),R8_obex,I_amex
     &,I_omex,I_ulex,
     & C8_edex,I_imex,R_afex,R_udex,R_ovax,
     & REAL(R_axax,4),R_uxax,REAL(R_ebex,4),
     & R_exax,REAL(R_oxax,4),I_ilex,I_umex,I_emex,I_elex,L_ibex
     &,
     & L_epex,L_erex,L_abex,L_ixax,
     & L_adex,L_ubex,L_opex,L_uvax,L_odex,L_urex,L_efex,
     & L_ifex,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_utax
     &,L_avax,L_ipex,
     & I_apex,L_irex,R_alex,REAL(R_idex,4),L_orex,L_evax,L_asex
     &,L_ivax,
     & L_ufex,L_akex,L_ekex,L_okex,L_ukex,L_ikex)
      !}

      if(L_ukex.or.L_okex.or.L_ikex.or.L_ekex.or.L_akex.or.L_ufex
     &) then      
                  I_olex = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_olex = z'40000000'
      endif
C KPG_KPH_man.fgi( 105, 136):���� ���������� �������� ��������,20KPH20AA002
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_adix,4),R8_axex,I_ikix
     &,I_alix,I_ekix,
     & C8_oxex,I_ukix,R_ibix,R_ebix,R_atex,
     & REAL(R_itex,4),R_evex,REAL(R_ovex,4),
     & R_otex,REAL(R_avex,4),I_ufix,I_elix,I_okix,I_ofix,L_uvex
     &,
     & L_olix,L_omix,L_ivex,L_utex,
     & L_ixex,L_exex,L_amix,L_etex,L_abix,L_epix,L_obix,
     & L_ubix,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_esex
     &,L_isex,L_ulix,
     & I_ilix,L_umix,R_ifix,REAL(R_uxex,4),L_apix,L_osex,L_ipix
     &,L_usex,
     & L_edix,L_idix,L_odix,L_afix,L_efix,L_udix)
      !}

      if(L_efix.or.L_afix.or.L_udix.or.L_odix.or.L_idix.or.L_edix
     &) then      
                  I_akix = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_akix = z'40000000'
      endif
C KPG_KPH_man.fgi(  90, 136):���� ���������� �������� ��������,20KPH20AA001
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_ixix,4),R8_itix,I_udox
     &,I_ifox,I_odox,
     & C8_avix,I_efox,R_uvix,R_ovix,R_irix,
     & REAL(R_urix,4),R_osix,REAL(R_atix,4),
     & R_asix,REAL(R_isix,4),I_edox,I_ofox,I_afox,I_adox,L_etix
     &,
     & L_akox,L_alox,L_usix,L_esix,
     & L_utix,L_otix,L_ikox,L_orix,L_ivix,L_olox,L_axix,
     & L_exix,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_opix
     &,L_upix,L_ekox,
     & I_ufox,L_elox,R_ubox,REAL(R_evix,4),L_ilox,L_arix,L_ulox
     &,L_erix,
     & L_oxix,L_uxix,L_abox,L_ibox,L_obox,L_ebox)
      !}

      if(L_obox.or.L_ibox.or.L_ebox.or.L_abox.or.L_uxix.or.L_oxix
     &) then      
                  I_idox = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_idox = z'40000000'
      endif
C KPG_KPH_man.fgi(  75, 136):���� ���������� �������� ��������,20KPG11AA013
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_utox,4),R8_urox,I_ebux
     &,I_ubux,I_abux,
     & C8_isox,I_obux,R_etox,R_atox,R_umox,
     & REAL(R_epox,4),R_arox,REAL(R_irox,4),
     & R_ipox,REAL(R_upox,4),I_oxox,I_adux,I_ibux,I_ixox,L_orox
     &,
     & L_idux,L_ifux,L_erox,L_opox,
     & L_esox,L_asox,L_udux,L_apox,L_usox,L_akux,L_itox,
     & L_otox,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_amox
     &,L_emox,L_odux,
     & I_edux,L_ofux,R_exox,REAL(R_osox,4),L_ufux,L_imox,L_ekux
     &,L_omox,
     & L_avox,L_evox,L_ivox,L_uvox,L_axox,L_ovox)
      !}

      if(L_axox.or.L_uvox.or.L_ovox.or.L_ivox.or.L_evox.or.L_avox
     &) then      
                  I_uxox = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_uxox = z'40000000'
      endif
C KPG_KPH_man.fgi(  45, 136):���� ���������� �������� ��������,20KPG11AA015
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_esux,4),R8_epux,I_ovux
     &,I_exux,I_ivux,
     & C8_upux,I_axux,R_orux,R_irux,R_elux,
     & REAL(R_olux,4),R_imux,REAL(R_umux,4),
     & R_ulux,REAL(R_emux,4),I_avux,I_ixux,I_uvux,I_utux,L_apux
     &,
     & L_uxux,L_ubabe,L_omux,L_amux,
     & L_opux,L_ipux,L_ebabe,L_ilux,L_erux,L_idabe,L_urux
     &,
     & L_asux,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_ikux
     &,L_okux,L_ababe,
     & I_oxux,L_adabe,R_otux,REAL(R_arux,4),L_edabe,L_ukux
     &,L_odabe,L_alux,
     & L_isux,L_osux,L_usux,L_etux,L_itux,L_atux)
      !}

      if(L_itux.or.L_etux.or.L_atux.or.L_usux.or.L_osux.or.L_isux
     &) then      
                  I_evux = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_evux = z'40000000'
      endif
C KPG_KPH_man.fgi(  30, 136):���� ���������� �������� ��������,20KPG11AA014
      !{
      Call REG_MAN(deltat,REAL(R_opabe,4),R8_olabe,R_evabe
     &,R_itabe,
     & L_oxabe,R_ivabe,R_otabe,L_uxabe,R_umabe,R_omabe,
     & R_ofabe,REAL(R_akabe,4),R_ukabe,
     & REAL(R_elabe,4),R_ekabe,REAL(R_okabe,4),I_esabe,
     & L_ilabe,L_usabe,L_ovabe,L_alabe,L_ikabe,
     & L_amabe,L_ulabe,L_etabe,L_ufabe,L_imabe,L_exabe,L_epabe
     &,
     & L_ipabe,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_udabe
     &,L_afabe,L_atabe,
     & I_osabe,L_uvabe,R_asabe,REAL(R_emabe,4),L_efabe,L_ifabe
     &,L_upabe,L_arabe,
     & L_erabe,L_orabe,L_urabe,L_irabe)
      !}

      if(L_urabe.or.L_orabe.or.L_irabe.or.L_erabe.or.L_arabe.or.L_upabe
     &) then      
                  I_isabe = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_isabe = z'40000000'
      endif


      R_apabe=100*R8_olabe
C KPG_KPH_man.fgi(  91,  92):���� ���������� ������������ ��������,20KPH15AA203
      !{
      Call REG_MAN(deltat,REAL(R_ulebe,4),R8_ufebe,R_isebe
     &,R_orebe,
     & L_utebe,R_osebe,R_urebe,L_avebe,R_alebe,R_ukebe,
     & R_ubebe,REAL(R_edebe,4),R_afebe,
     & REAL(R_ifebe,4),R_idebe,REAL(R_udebe,4),I_ipebe,
     & L_ofebe,L_arebe,L_usebe,L_efebe,L_odebe,
     & L_ekebe,L_akebe,L_irebe,L_adebe,L_okebe,L_itebe,L_ilebe
     &,
     & L_olebe,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_abebe
     &,L_ebebe,L_erebe,
     & I_upebe,L_atebe,R_epebe,REAL(R_ikebe,4),L_ibebe,L_obebe
     &,L_amebe,L_emebe,
     & L_imebe,L_umebe,L_apebe,L_omebe)
      !}

      if(L_apebe.or.L_umebe.or.L_omebe.or.L_imebe.or.L_emebe.or.L_amebe
     &) then      
                  I_opebe = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_opebe = z'40000000'
      endif


      R_elebe=100*R8_ufebe
C KPG_KPH_man.fgi(  75,  92):���� ���������� ������������ ��������,20KPG41AA202
      !{
      Call REG_MAN(deltat,REAL(R_akibe,4),R8_adibe,R_opibe
     &,R_umibe,
     & L_asibe,R_upibe,R_apibe,L_esibe,R_efibe,R_afibe,
     & R_axebe,REAL(R_ixebe,4),R_ebibe,
     & REAL(R_obibe,4),R_oxebe,REAL(R_abibe,4),I_olibe,
     & L_ubibe,L_emibe,L_aribe,L_ibibe,L_uxebe,
     & L_idibe,L_edibe,L_omibe,L_exebe,L_udibe,L_oribe,L_ofibe
     &,
     & L_ufibe,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_evebe
     &,L_ivebe,L_imibe,
     & I_amibe,L_eribe,R_ilibe,REAL(R_odibe,4),L_ovebe,L_uvebe
     &,L_ekibe,L_ikibe,
     & L_okibe,L_alibe,L_elibe,L_ukibe)
      !}

      if(L_elibe.or.L_alibe.or.L_ukibe.or.L_okibe.or.L_ikibe.or.L_ekibe
     &) then      
                  I_ulibe = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ulibe = z'40000000'
      endif


      R_ifibe=100*R8_adibe
C KPG_KPH_man.fgi(  60,  92):���� ���������� ������������ ��������,20KPG41AA201
      !{
      Call REG_MAN(deltat,REAL(R_edobe,4),R8_exibe,R_ulobe
     &,R_alobe,
     & L_epobe,R_amobe,R_elobe,L_ipobe,R_ibobe,R_ebobe,
     & R_etibe,REAL(R_otibe,4),R_ivibe,
     & REAL(R_uvibe,4),R_utibe,REAL(R_evibe,4),I_ufobe,
     & L_axibe,L_ikobe,L_emobe,L_ovibe,L_avibe,
     & L_oxibe,L_ixibe,L_ukobe,L_itibe,L_abobe,L_umobe,L_ubobe
     &,
     & L_adobe,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_isibe
     &,L_osibe,L_okobe,
     & I_ekobe,L_imobe,R_ofobe,REAL(R_uxibe,4),L_usibe,L_atibe
     &,L_idobe,L_odobe,
     & L_udobe,L_efobe,L_ifobe,L_afobe)
      !}

      if(L_ifobe.or.L_efobe.or.L_afobe.or.L_udobe.or.L_odobe.or.L_idobe
     &) then      
                  I_akobe = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_akobe = z'40000000'
      endif


      R_obobe=100*R8_exibe
C KPG_KPH_man.fgi(  45,  92):���� ���������� ������������ ��������,20KPH20AA211
      !{
      Call REG_MAN(deltat,REAL(R_ixobe,4),R8_itobe,R_akube
     &,R_efube,
     & L_ilube,R_ekube,R_ifube,L_olube,R_ovobe,R_ivobe,
     & R_irobe,REAL(R_urobe,4),R_osobe,
     & REAL(R_atobe,4),R_asobe,REAL(R_isobe,4),I_adube,
     & L_etobe,L_odube,L_ikube,L_usobe,L_esobe,
     & L_utobe,L_otobe,L_afube,L_orobe,L_evobe,L_alube,L_axobe
     &,
     & L_exobe,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_opobe
     &,L_upobe,L_udube,
     & I_idube,L_okube,R_ubube,REAL(R_avobe,4),L_arobe,L_erobe
     &,L_oxobe,L_uxobe,
     & L_abube,L_ibube,L_obube,L_ebube)
      !}

      if(L_obube.or.L_ibube.or.L_ebube.or.L_abube.or.L_uxobe.or.L_oxobe
     &) then      
                  I_edube = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_edube = z'40000000'
      endif


      R_uvobe=100*R8_itobe
C KPG_KPH_man.fgi(  30,  92):���� ���������� ������������ ��������,20KPH13AA202
      !{
      Call REG_MAN(deltat,REAL(R_otube,4),R8_orube,R_edade
     &,R_ibade,
     & L_ofade,R_idade,R_obade,L_ufade,R_usube,R_osube,
     & R_omube,REAL(R_apube,4),R_upube,
     & REAL(R_erube,4),R_epube,REAL(R_opube,4),I_exube,
     & L_irube,L_uxube,L_odade,L_arube,L_ipube,
     & L_asube,L_urube,L_ebade,L_umube,L_isube,L_efade,L_etube
     &,
     & L_itube,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_ulube
     &,L_amube,L_abade,
     & I_oxube,L_udade,R_axube,REAL(R_esube,4),L_emube,L_imube
     &,L_utube,L_avube,
     & L_evube,L_ovube,L_uvube,L_ivube)
      !}

      if(L_uvube.or.L_ovube.or.L_ivube.or.L_evube.or.L_avube.or.L_utube
     &) then      
                  I_ixube = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ixube = z'40000000'
      endif


      R_atube=100*R8_orube
C KPG_KPH_man.fgi( 241,  62):���� ���������� ������������ ��������,20KPH12AA201
      !{
      Call REG_MAN(deltat,REAL(R_urade,4),R8_umade,R_ixade
     &,R_ovade,
     & L_ubede,R_oxade,R_uvade,L_adede,R_arade,R_upade,
     & R_ukade,REAL(R_elade,4),R_amade,
     & REAL(R_imade,4),R_ilade,REAL(R_ulade,4),I_itade,
     & L_omade,L_avade,L_uxade,L_emade,L_olade,
     & L_epade,L_apade,L_ivade,L_alade,L_opade,L_ibede,L_irade
     &,
     & L_orade,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_akade
     &,L_ekade,L_evade,
     & I_utade,L_abede,R_etade,REAL(R_ipade,4),L_ikade,L_okade
     &,L_asade,L_esade,
     & L_isade,L_usade,L_atade,L_osade)
      !}

      if(L_atade.or.L_usade.or.L_osade.or.L_isade.or.L_esade.or.L_asade
     &) then      
                  I_otade = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_otade = z'40000000'
      endif


      R_erade=100*R8_umade
C KPG_KPH_man.fgi( 226,  62):���� ���������� ������������ ��������,20KPH51AA202
      !{
      Call REG_MAN(deltat,REAL(R_apede,4),R8_alede,R_otede
     &,R_usede,
     & L_axede,R_utede,R_atede,L_exede,R_emede,R_amede,
     & R_afede,REAL(R_ifede,4),R_ekede,
     & REAL(R_okede,4),R_ofede,REAL(R_akede,4),I_orede,
     & L_ukede,L_esede,L_avede,L_ikede,L_ufede,
     & L_ilede,L_elede,L_osede,L_efede,L_ulede,L_ovede,L_omede
     &,
     & L_umede,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_edede
     &,L_idede,L_isede,
     & I_asede,L_evede,R_irede,REAL(R_olede,4),L_odede,L_udede
     &,L_epede,L_ipede,
     & L_opede,L_arede,L_erede,L_upede)
      !}

      if(L_erede.or.L_arede.or.L_upede.or.L_opede.or.L_ipede.or.L_epede
     &) then      
                  I_urede = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_urede = z'40000000'
      endif


      R_imede=100*R8_alede
C KPG_KPH_man.fgi( 211,  62):���� ���������� ������������ ��������,20KPH51AA201
      !{
      Call REG_MAN(deltat,REAL(R_elide,4),R8_efide,R_uride
     &,R_aride,
     & L_etide,R_aside,R_eride,L_itide,R_ikide,R_ekide,
     & R_ebide,REAL(R_obide,4),R_idide,
     & REAL(R_udide,4),R_ubide,REAL(R_edide,4),I_umide,
     & L_afide,L_ipide,L_eside,L_odide,L_adide,
     & L_ofide,L_ifide,L_upide,L_ibide,L_akide,L_uside,L_ukide
     &,
     & L_alide,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_ixede
     &,L_oxede,L_opide,
     & I_epide,L_iside,R_omide,REAL(R_ufide,4),L_uxede,L_abide
     &,L_ilide,L_olide,
     & L_ulide,L_emide,L_imide,L_amide)
      !}

      if(L_imide.or.L_emide.or.L_amide.or.L_ulide.or.L_olide.or.L_ilide
     &) then      
                  I_apide = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_apide = z'40000000'
      endif


      R_okide=100*R8_efide
C KPG_KPH_man.fgi( 196,  62):���� ���������� ������������ ��������,20KPG52AA201
      !{
      Call REG_MAN(deltat,REAL(R_ifode,4),R8_ibode,R_apode
     &,R_emode,
     & L_irode,R_epode,R_imode,L_orode,R_odode,R_idode,
     & R_ivide,REAL(R_uvide,4),R_oxide,
     & REAL(R_abode,4),R_axide,REAL(R_ixide,4),I_alode,
     & L_ebode,L_olode,L_ipode,L_uxide,L_exide,
     & L_ubode,L_obode,L_amode,L_ovide,L_edode,L_arode,L_afode
     &,
     & L_efode,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_otide
     &,L_utide,L_ulode,
     & I_ilode,L_opode,R_ukode,REAL(R_adode,4),L_avide,L_evide
     &,L_ofode,L_ufode,
     & L_akode,L_ikode,L_okode,L_ekode)
      !}

      if(L_okode.or.L_ikode.or.L_ekode.or.L_akode.or.L_ufode.or.L_ofode
     &) then      
                  I_elode = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_elode = z'40000000'
      endif


      R_udode=100*R8_ibode
C KPG_KPH_man.fgi( 181,  62):���� ���������� ������������ ��������,20KPG51AA201
      !{
      Call REG_MAN(deltat,REAL(R_obude,4),R8_ovode,R_elude
     &,R_ikude,
     & L_omude,R_ilude,R_okude,L_umude,R_uxode,R_oxode,
     & R_osode,REAL(R_atode,4),R_utode,
     & REAL(R_evode,4),R_etode,REAL(R_otode,4),I_efude,
     & L_ivode,L_ufude,L_olude,L_avode,L_itode,
     & L_axode,L_uvode,L_ekude,L_usode,L_ixode,L_emude,L_ebude
     &,
     & L_ibude,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_urode
     &,L_asode,L_akude,
     & I_ofude,L_ulude,R_afude,REAL(R_exode,4),L_esode,L_isode
     &,L_ubude,L_adude,
     & L_edude,L_odude,L_udude,L_idude)
      !}

      if(L_udude.or.L_odude.or.L_idude.or.L_edude.or.L_adude.or.L_ubude
     &) then      
                  I_ifude = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ifude = z'40000000'
      endif


      R_abude=100*R8_ovode
C KPG_KPH_man.fgi( 165,  62):���� ���������� ������������ ��������,20KPH41AA201
      !{
      Call REG_MAN(deltat,REAL(R_uvude,4),R8_usude,R_ifafe
     &,R_odafe,
     & L_ukafe,R_ofafe,R_udafe,L_alafe,R_avude,R_utude,
     & R_upude,REAL(R_erude,4),R_asude,
     & REAL(R_isude,4),R_irude,REAL(R_urude,4),I_ibafe,
     & L_osude,L_adafe,L_ufafe,L_esude,L_orude,
     & L_etude,L_atude,L_idafe,L_arude,L_otude,L_ikafe,L_ivude
     &,
     & L_ovude,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_apude
     &,L_epude,L_edafe,
     & I_ubafe,L_akafe,R_ebafe,REAL(R_itude,4),L_ipude,L_opude
     &,L_axude,L_exude,
     & L_ixude,L_uxude,L_abafe,L_oxude)
      !}

      if(L_abafe.or.L_uxude.or.L_oxude.or.L_ixude.or.L_exude.or.L_axude
     &) then      
                  I_obafe = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_obafe = z'40000000'
      endif


      R_evude=100*R8_usude
C KPG_KPH_man.fgi( 150,  62):���� ���������� ������������ ��������,20KPH41AA202
      !{
      Call REG_MAN(deltat,REAL(R_atafe,4),R8_arafe,R_obefe
     &,R_uxafe,
     & L_afefe,R_ubefe,R_abefe,L_efefe,R_esafe,R_asafe,
     & R_amafe,REAL(R_imafe,4),R_epafe,
     & REAL(R_opafe,4),R_omafe,REAL(R_apafe,4),I_ovafe,
     & L_upafe,L_exafe,L_adefe,L_ipafe,L_umafe,
     & L_irafe,L_erafe,L_oxafe,L_emafe,L_urafe,L_odefe,L_osafe
     &,
     & L_usafe,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_elafe
     &,L_ilafe,L_ixafe,
     & I_axafe,L_edefe,R_ivafe,REAL(R_orafe,4),L_olafe,L_ulafe
     &,L_etafe,L_itafe,
     & L_otafe,L_avafe,L_evafe,L_utafe)
      !}

      if(L_evafe.or.L_avafe.or.L_utafe.or.L_otafe.or.L_itafe.or.L_etafe
     &) then      
                  I_uvafe = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_uvafe = z'40000000'
      endif


      R_isafe=100*R8_arafe
C KPG_KPH_man.fgi( 135,  62):���� ���������� ������������ ��������,20KPH15AA201
      !{
      Call REG_MAN(deltat,REAL(R_erefe,4),R8_emefe,R_uvefe
     &,R_avefe,
     & L_ebife,R_axefe,R_evefe,L_ibife,R_ipefe,R_epefe,
     & R_ekefe,REAL(R_okefe,4),R_ilefe,
     & REAL(R_ulefe,4),R_ukefe,REAL(R_elefe,4),I_usefe,
     & L_amefe,L_itefe,L_exefe,L_olefe,L_alefe,
     & L_omefe,L_imefe,L_utefe,L_ikefe,L_apefe,L_uxefe,L_upefe
     &,
     & L_arefe,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_ifefe
     &,L_ofefe,L_otefe,
     & I_etefe,L_ixefe,R_osefe,REAL(R_umefe,4),L_ufefe,L_akefe
     &,L_irefe,L_orefe,
     & L_urefe,L_esefe,L_isefe,L_asefe)
      !}

      if(L_isefe.or.L_esefe.or.L_asefe.or.L_urefe.or.L_orefe.or.L_irefe
     &) then      
                  I_atefe = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_atefe = z'40000000'
      endif


      R_opefe=100*R8_emefe
C KPG_KPH_man.fgi( 120,  62):���� ���������� ������������ ��������,20KPH15AA202
      !{
      Call REG_MAN(deltat,REAL(R_imife,4),R8_ikife,R_atife
     &,R_esife,
     & L_ivife,R_etife,R_isife,L_ovife,R_olife,R_ilife,
     & R_idife,REAL(R_udife,4),R_ofife,
     & REAL(R_akife,4),R_afife,REAL(R_ifife,4),I_arife,
     & L_ekife,L_orife,L_itife,L_ufife,L_efife,
     & L_ukife,L_okife,L_asife,L_odife,L_elife,L_avife,L_amife
     &,
     & L_emife,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_obife
     &,L_ubife,L_urife,
     & I_irife,L_otife,R_upife,REAL(R_alife,4),L_adife,L_edife
     &,L_omife,L_umife,
     & L_apife,L_ipife,L_opife,L_epife)
      !}

      if(L_opife.or.L_ipife.or.L_epife.or.L_apife.or.L_umife.or.L_omife
     &) then      
                  I_erife = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_erife = z'40000000'
      endif


      R_ulife=100*R8_ikife
C KPG_KPH_man.fgi( 105,  62):���� ���������� ������������ ��������,20KPH31AA201
      !{
      Call REG_MAN(deltat,REAL(R_okofe,4),R8_odofe,R_erofe
     &,R_ipofe,
     & L_osofe,R_irofe,R_opofe,L_usofe,R_ufofe,R_ofofe,
     & R_oxife,REAL(R_abofe,4),R_ubofe,
     & REAL(R_edofe,4),R_ebofe,REAL(R_obofe,4),I_emofe,
     & L_idofe,L_umofe,L_orofe,L_adofe,L_ibofe,
     & L_afofe,L_udofe,L_epofe,L_uxife,L_ifofe,L_esofe,L_ekofe
     &,
     & L_ikofe,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_uvife
     &,L_axife,L_apofe,
     & I_omofe,L_urofe,R_amofe,REAL(R_efofe,4),L_exife,L_ixife
     &,L_ukofe,L_alofe,
     & L_elofe,L_olofe,L_ulofe,L_ilofe)
      !}

      if(L_ulofe.or.L_olofe.or.L_ilofe.or.L_elofe.or.L_alofe.or.L_ukofe
     &) then      
                  I_imofe = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_imofe = z'40000000'
      endif


      R_akofe=100*R8_odofe
C KPG_KPH_man.fgi(  90,  62):���� ���������� ������������ ��������,20KPH31AA202
      !{
      Call REG_MAN(deltat,REAL(R_udufe,4),R8_uxofe,R_imufe
     &,R_olufe,
     & L_upufe,R_omufe,R_ulufe,L_arufe,R_adufe,R_ubufe,
     & R_utofe,REAL(R_evofe,4),R_axofe,
     & REAL(R_ixofe,4),R_ivofe,REAL(R_uvofe,4),I_ikufe,
     & L_oxofe,L_alufe,L_umufe,L_exofe,L_ovofe,
     & L_ebufe,L_abufe,L_ilufe,L_avofe,L_obufe,L_ipufe,L_idufe
     &,
     & L_odufe,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_atofe
     &,L_etofe,L_elufe,
     & I_ukufe,L_apufe,R_ekufe,REAL(R_ibufe,4),L_itofe,L_otofe
     &,L_afufe,L_efufe,
     & L_ifufe,L_ufufe,L_akufe,L_ofufe)
      !}

      if(L_akufe.or.L_ufufe.or.L_ofufe.or.L_ifufe.or.L_efufe.or.L_afufe
     &) then      
                  I_okufe = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_okufe = z'40000000'
      endif


      R_edufe=100*R8_uxofe
C KPG_KPH_man.fgi(  75,  62):���� ���������� ������������ ��������,20KPG32AA201
      !{
      Call REG_MAN(deltat,REAL(R_abake,4),R8_avufe,R_okake
     &,R_ufake,
     & L_amake,R_ukake,R_akake,L_emake,R_exufe,R_axufe,
     & R_asufe,REAL(R_isufe,4),R_etufe,
     & REAL(R_otufe,4),R_osufe,REAL(R_atufe,4),I_odake,
     & L_utufe,L_efake,L_alake,L_itufe,L_usufe,
     & L_ivufe,L_evufe,L_ofake,L_esufe,L_uvufe,L_olake,L_oxufe
     &,
     & L_uxufe,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_erufe
     &,L_irufe,L_ifake,
     & I_afake,L_elake,R_idake,REAL(R_ovufe,4),L_orufe,L_urufe
     &,L_ebake,L_ibake,
     & L_obake,L_adake,L_edake,L_ubake)
      !}

      if(L_edake.or.L_adake.or.L_ubake.or.L_obake.or.L_ibake.or.L_ebake
     &) then      
                  I_udake = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_udake = z'40000000'
      endif


      R_ixufe=100*R8_avufe
C KPG_KPH_man.fgi(  60,  62):���� ���������� ������������ ��������,20KPG32AA202
      !{
      Call REG_MAN(deltat,REAL(R_evake,4),R8_esake,R_udeke
     &,R_adeke,
     & L_ekeke,R_afeke,R_edeke,L_ikeke,R_itake,R_etake,
     & R_epake,REAL(R_opake,4),R_irake,
     & REAL(R_urake,4),R_upake,REAL(R_erake,4),I_uxake,
     & L_asake,L_ibeke,L_efeke,L_orake,L_arake,
     & L_osake,L_isake,L_ubeke,L_ipake,L_atake,L_ufeke,L_utake
     &,
     & L_avake,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_imake
     &,L_omake,L_obeke,
     & I_ebeke,L_ifeke,R_oxake,REAL(R_usake,4),L_umake,L_apake
     &,L_ivake,L_ovake,
     & L_uvake,L_exake,L_ixake,L_axake)
      !}

      if(L_ixake.or.L_exake.or.L_axake.or.L_uvake.or.L_ovake.or.L_ivake
     &) then      
                  I_abeke = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_abeke = z'40000000'
      endif


      R_otake=100*R8_esake
C KPG_KPH_man.fgi(  45,  62):���� ���������� ������������ ��������,20KPG12AA201
      !{
      Call REG_MAN(deltat,REAL(R_iseke,4),R8_ipeke,R_abike
     &,R_exeke,
     & L_idike,R_ebike,R_ixeke,L_odike,R_oreke,R_ireke,
     & R_ileke,REAL(R_uleke,4),R_omeke,
     & REAL(R_apeke,4),R_ameke,REAL(R_imeke,4),I_aveke,
     & L_epeke,L_oveke,L_ibike,L_umeke,L_emeke,
     & L_upeke,L_opeke,L_axeke,L_oleke,L_ereke,L_adike,L_aseke
     &,
     & L_eseke,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_okeke
     &,L_ukeke,L_uveke,
     & I_iveke,L_obike,R_uteke,REAL(R_areke,4),L_aleke,L_eleke
     &,L_oseke,L_useke,
     & L_ateke,L_iteke,L_oteke,L_eteke)
      !}

      if(L_oteke.or.L_iteke.or.L_eteke.or.L_ateke.or.L_useke.or.L_oseke
     &) then      
                  I_eveke = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_eveke = z'40000000'
      endif


      R_ureke=100*R8_ipeke
C KPG_KPH_man.fgi(  30,  62):���� ���������� ������������ ��������,20KPG12AA202
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_opike,4),R8_olike,I_atike
     &,I_otike,I_usike,
     & C8_emike,I_itike,R_apike,R_umike,R_ofike,
     & REAL(R_akike,4),R_ukike,REAL(R_elike,4),
     & R_ekike,REAL(R_okike,4),I_isike,I_utike,I_etike,I_esike
     &,L_ilike,
     & L_evike,L_exike,L_alike,L_ikike,
     & L_amike,L_ulike,L_ovike,L_ufike,L_omike,L_uxike,L_epike
     &,
     & L_ipike,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_udike
     &,L_afike,L_ivike,
     & I_avike,L_ixike,R_asike,REAL(R_imike,4),L_oxike,L_efike
     &,L_aboke,L_ifike,
     & L_upike,L_arike,L_erike,L_orike,L_urike,L_irike)
      !}

      if(L_urike.or.L_orike.or.L_irike.or.L_erike.or.L_arike.or.L_upike
     &) then      
                  I_osike = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_osike = z'40000000'
      endif
C KPG_KPH_man.fgi( 168,  22):���� ���������� �������� ��������,20KPH40AA106
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_amoke,4),R8_akoke,I_iroke
     &,I_asoke,I_eroke,
     & C8_okoke,I_uroke,R_iloke,R_eloke,R_adoke,
     & REAL(R_idoke,4),R_efoke,REAL(R_ofoke,4),
     & R_odoke,REAL(R_afoke,4),I_upoke,I_esoke,I_oroke,I_opoke
     &,L_ufoke,
     & L_osoke,L_otoke,L_ifoke,L_udoke,
     & L_ikoke,L_ekoke,L_atoke,L_edoke,L_aloke,L_evoke,L_oloke
     &,
     & L_uloke,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_eboke
     &,L_iboke,L_usoke,
     & I_isoke,L_utoke,R_ipoke,REAL(R_ukoke,4),L_avoke,L_oboke
     &,L_ivoke,L_uboke,
     & L_emoke,L_imoke,L_omoke,L_apoke,L_epoke,L_umoke)
      !}

      if(L_epoke.or.L_apoke.or.L_umoke.or.L_omoke.or.L_imoke.or.L_emoke
     &) then      
                  I_aroke = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_aroke = z'40000000'
      endif
C KPG_KPH_man.fgi( 185,  22):���� ���������� �������� ��������,20KPH15AA103
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_ikuke,4),R8_iduke,I_umuke
     &,I_ipuke,I_omuke,
     & C8_afuke,I_epuke,R_ufuke,R_ofuke,R_ixoke,
     & REAL(R_uxoke,4),R_obuke,REAL(R_aduke,4),
     & R_abuke,REAL(R_ibuke,4),I_emuke,I_opuke,I_apuke,I_amuke
     &,L_eduke,
     & L_aruke,L_asuke,L_ubuke,L_ebuke,
     & L_uduke,L_oduke,L_iruke,L_oxoke,L_ifuke,L_osuke,L_akuke
     &,
     & L_ekuke,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_ovoke
     &,L_uvoke,L_eruke,
     & I_upuke,L_esuke,R_uluke,REAL(R_efuke,4),L_isuke,L_axoke
     &,L_usuke,L_exoke,
     & L_okuke,L_ukuke,L_aluke,L_iluke,L_oluke,L_eluke)
      !}

      if(L_oluke.or.L_iluke.or.L_eluke.or.L_aluke.or.L_ukuke.or.L_okuke
     &) then      
                  I_imuke = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_imuke = z'40000000'
      endif
C KPG_KPH_man.fgi(  95,  22):���� ���������� �������� ��������,20KPG31AA112
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_udale,4),R8_uxuke,I_elale
     &,I_ulale,I_alale,
     & C8_ibale,I_olale,R_edale,R_adale,R_utuke,
     & REAL(R_evuke,4),R_axuke,REAL(R_ixuke,4),
     & R_ivuke,REAL(R_uvuke,4),I_okale,I_amale,I_ilale,I_ikale
     &,L_oxuke,
     & L_imale,L_ipale,L_exuke,L_ovuke,
     & L_ebale,L_abale,L_umale,L_avuke,L_ubale,L_arale,L_idale
     &,
     & L_odale,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_atuke
     &,L_etuke,L_omale,
     & I_emale,L_opale,R_ekale,REAL(R_obale,4),L_upale,L_ituke
     &,L_erale,L_otuke,
     & L_afale,L_efale,L_ifale,L_ufale,L_akale,L_ofale)
      !}

      if(L_akale.or.L_ufale.or.L_ofale.or.L_ifale.or.L_efale.or.L_afale
     &) then      
                  I_ukale = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ukale = z'40000000'
      endif
C KPG_KPH_man.fgi(  80,  22):���� ���������� �������� ��������,20KPH10AA015
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_ebele,4),R8_evale,I_ofele
     &,I_ekele,I_ifele,
     & C8_uvale,I_akele,R_oxale,R_ixale,R_esale,
     & REAL(R_osale,4),R_itale,REAL(R_utale,4),
     & R_usale,REAL(R_etale,4),I_afele,I_ikele,I_ufele,I_udele
     &,L_avale,
     & L_ukele,L_ulele,L_otale,L_atale,
     & L_ovale,L_ivale,L_elele,L_isale,L_exale,L_imele,L_uxale
     &,
     & L_abele,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_irale
     &,L_orale,L_alele,
     & I_okele,L_amele,R_odele,REAL(R_axale,4),L_emele,L_urale
     &,L_omele,L_asale,
     & L_ibele,L_obele,L_ubele,L_edele,L_idele,L_adele)
      !}

      if(L_idele.or.L_edele.or.L_adele.or.L_ubele.or.L_obele.or.L_ibele
     &) then      
                  I_efele = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_efele = z'40000000'
      endif
C KPG_KPH_man.fgi( 149,  22):���� ���������� �������� ��������,20KPG21AA113
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_ovele,4),R8_osele,I_adile
     &,I_odile,I_ubile,
     & C8_etele,I_idile,R_avele,R_utele,R_opele,
     & REAL(R_arele,4),R_urele,REAL(R_esele,4),
     & R_erele,REAL(R_orele,4),I_ibile,I_udile,I_edile,I_ebile
     &,L_isele,
     & L_efile,L_ekile,L_asele,L_irele,
     & L_atele,L_usele,L_ofile,L_upele,L_otele,L_ukile,L_evele
     &,
     & L_ivele,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_umele
     &,L_apele,L_ifile,
     & I_afile,L_ikile,R_abile,REAL(R_itele,4),L_okile,L_epele
     &,L_alile,L_ipele,
     & L_uvele,L_axele,L_exele,L_oxele,L_uxele,L_ixele)
      !}

      if(L_uxele.or.L_oxele.or.L_ixele.or.L_exele.or.L_axele.or.L_uvele
     &) then      
                  I_obile = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_obile = z'40000000'
      endif
C KPG_KPH_man.fgi( 205,  22):���� ���������� �������� ��������,20KPG11AA112
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_atile,4),R8_arile,I_ixile
     &,I_abole,I_exile,
     & C8_orile,I_uxile,R_isile,R_esile,R_amile,
     & REAL(R_imile,4),R_epile,REAL(R_opile,4),
     & R_omile,REAL(R_apile,4),I_uvile,I_ebole,I_oxile,I_ovile
     &,L_upile,
     & L_obole,L_odole,L_ipile,L_umile,
     & L_irile,L_erile,L_adole,L_emile,L_asile,L_efole,L_osile
     &,
     & L_usile,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_elile
     &,L_ilile,L_ubole,
     & I_ibole,L_udole,R_ivile,REAL(R_urile,4),L_afole,L_olile
     &,L_ifole,L_ulile,
     & L_etile,L_itile,L_otile,L_avile,L_evile,L_utile)
      !}

      if(L_evile.or.L_avile.or.L_utile.or.L_otile.or.L_itile.or.L_etile
     &) then      
                  I_axile = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_axile = z'40000000'
      endif
C KPG_KPH_man.fgi( 131,  22):���� ���������� �������� ��������,20KPH40AA108
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_irole,4),R8_imole,I_utole
     &,I_ivole,I_otole,
     & C8_apole,I_evole,R_upole,R_opole,R_ikole,
     & REAL(R_ukole,4),R_olole,REAL(R_amole,4),
     & R_alole,REAL(R_ilole,4),I_etole,I_ovole,I_avole,I_atole
     &,L_emole,
     & L_axole,L_abule,L_ulole,L_elole,
     & L_umole,L_omole,L_ixole,L_okole,L_ipole,L_obule,L_arole
     &,
     & L_erole,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_ofole
     &,L_ufole,L_exole,
     & I_uvole,L_ebule,R_usole,REAL(R_epole,4),L_ibule,L_akole
     &,L_ubule,L_ekole,
     & L_orole,L_urole,L_asole,L_isole,L_osole,L_esole)
      !}

      if(L_osole.or.L_isole.or.L_esole.or.L_asole.or.L_urole.or.L_orole
     &) then      
                  I_itole = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_itole = z'40000000'
      endif
C KPG_KPH_man.fgi(  63,  22):���� ���������� �������� ��������,20KPG42AA102
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_umule,4),R8_ukule,I_esule
     &,I_usule,I_asule,
     & C8_ilule,I_osule,R_emule,R_amule,R_udule,
     & REAL(R_efule,4),R_akule,REAL(R_ikule,4),
     & R_ifule,REAL(R_ufule,4),I_orule,I_atule,I_isule,I_irule
     &,L_okule,
     & L_itule,L_ivule,L_ekule,L_ofule,
     & L_elule,L_alule,L_utule,L_afule,L_ulule,L_axule,L_imule
     &,
     & L_omule,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_adule
     &,L_edule,L_otule,
     & I_etule,L_ovule,R_erule,REAL(R_olule,4),L_uvule,L_idule
     &,L_exule,L_odule,
     & L_apule,L_epule,L_ipule,L_upule,L_arule,L_opule)
      !}

      if(L_arule.or.L_upule.or.L_opule.or.L_ipule.or.L_epule.or.L_apule
     &) then      
                  I_urule = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_urule = z'40000000'
      endif
C KPG_KPH_man.fgi(  48,  22):���� ���������� �������� ��������,20KPG60AA104
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_elame,4),R8_efame,I_opame
     &,I_erame,I_ipame,
     & C8_ufame,I_arame,R_okame,R_ikame,R_ebame,
     & REAL(R_obame,4),R_idame,REAL(R_udame,4),
     & R_ubame,REAL(R_edame,4),I_apame,I_irame,I_upame,I_umame
     &,L_afame,
     & L_urame,L_usame,L_odame,L_adame,
     & L_ofame,L_ifame,L_esame,L_ibame,L_ekame,L_itame,L_ukame
     &,
     & L_alame,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_ixule
     &,L_oxule,L_asame,
     & I_orame,L_atame,R_omame,REAL(R_akame,4),L_etame,L_uxule
     &,L_otame,L_abame,
     & L_ilame,L_olame,L_ulame,L_emame,L_imame,L_amame)
      !}

      if(L_imame.or.L_emame.or.L_amame.or.L_ulame.or.L_olame.or.L_ilame
     &) then      
                  I_epame = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_epame = z'40000000'
      endif
C KPG_KPH_man.fgi( 113,  22):���� ���������� �������� ��������,20KPG15AA114
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_ofeme,4),R8_obeme,I_ameme
     &,I_omeme,I_uleme,
     & C8_edeme,I_imeme,R_afeme,R_udeme,R_ovame,
     & REAL(R_axame,4),R_uxame,REAL(R_ebeme,4),
     & R_exame,REAL(R_oxame,4),I_ileme,I_umeme,I_ememe,I_eleme
     &,L_ibeme,
     & L_epeme,L_ereme,L_abeme,L_ixame,
     & L_ademe,L_ubeme,L_opeme,L_uvame,L_odeme,L_ureme,L_efeme
     &,
     & L_ifeme,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_utame
     &,L_avame,L_ipeme,
     & I_apeme,L_ireme,R_aleme,REAL(R_ideme,4),L_oreme,L_evame
     &,L_aseme,L_ivame,
     & L_ufeme,L_akeme,L_ekeme,L_okeme,L_ukeme,L_ikeme)
      !}

      if(L_ukeme.or.L_okeme.or.L_ikeme.or.L_ekeme.or.L_akeme.or.L_ufeme
     &) then      
                  I_oleme = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_oleme = z'40000000'
      endif
C KPG_KPH_man.fgi(  31,  22):���� ���������� �������� ��������,20RPH11AA102
      R_ibivi = R8_eseme
C KPH_sens.fgi( 130, 188):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_exevi,R_afivi,REAL(1e
     &-3,4),
     & REAL(R_uxevi,4),REAL(R_abivi,4),
     & REAL(R_axevi,4),REAL(R_uvevi,4),I_udivi,
     & REAL(R_obivi,4),L_ubivi,REAL(R_adivi,4),L_edivi,L_idivi
     &,R_ebivi,
     & REAL(R_oxevi,4),REAL(R_ixevi,4),L_odivi,REAL(R_ibivi
     &,4))
      !}
C KPH_vlv.fgi( 104,  15):���������� �������,20KPH60CP002XQ01
      R_ukuxi = R8_iseme
C KPH_sens.fgi( 130, 192):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_imuxi,4),REAL
     &(R_ukuxi,4),R_ofuxi,
     & R_omuxi,REAL(1e-3,4),REAL(R_ekuxi,4),
     & REAL(R_ikuxi,4),REAL(R_ifuxi,4),
     & REAL(R_efuxi,4),I_emuxi,REAL(R_aluxi,4),L_eluxi,
     & REAL(R_iluxi,4),L_oluxi,L_uluxi,R_okuxi,REAL(R_akuxi
     &,4),REAL(R_ufuxi,4),L_amuxi)
      !}
C KPH_vlv.fgi(  75,  15):���������� ������� ��������,20KPH20CP003XQ01
      R_odori = R8_oseme
C KPH_sens.fgi(  52, 172):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ibori,R_ekori,REAL(1,4
     &),
     & REAL(R_adori,4),REAL(R_edori,4),
     & REAL(R_ebori,4),REAL(R_abori,4),I_akori,
     & REAL(R_udori,4),L_afori,REAL(R_efori,4),L_ifori,L_ofori
     &,R_idori,
     & REAL(R_ubori,4),REAL(R_obori,4),L_ufori,REAL(R_odori
     &,4))
      !}
C KPH_vlv.fgi( 133,  28):���������� �������,20KPH30CT001XQ01
      R_ukivi = R8_useme
C KPH_sens.fgi( 287, 256):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ofivi,R_imivi,REAL(1000
     &,4),
     & REAL(R_ekivi,4),REAL(R_ikivi,4),
     & REAL(R_ifivi,4),REAL(R_efivi,4),I_emivi,
     & REAL(R_alivi,4),L_elivi,REAL(R_ilivi,4),L_olivi,L_ulivi
     &,R_okivi,
     & REAL(R_akivi,4),REAL(R_ufivi,4),L_amivi,REAL(R_ukivi
     &,4))
      !}
C KPH_vlv.fgi( 104,  28):���������� �������,20KPH30CL003XQ01
      R_adibo = R8_ateme
C KPH_sens.fgi( 130, 196):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_ofibo,4),REAL
     &(R_adibo,4),R_uxebo,
     & R_ufibo,REAL(1e-3,4),REAL(R_ibibo,4),
     & REAL(R_obibo,4),REAL(R_oxebo,4),
     & REAL(R_ixebo,4),I_ifibo,REAL(R_edibo,4),L_idibo,
     & REAL(R_odibo,4),L_udibo,L_afibo,R_ubibo,REAL(R_ebibo
     &,4),REAL(R_abibo,4),L_efibo)
      !}
C KPH_vlv.fgi(  75,  28):���������� ������� ��������,20KPH20CP001XQ01
      R_aleko = R8_eteme
C KPH_sens.fgi( 130, 200):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_omeko,4),REAL
     &(R_aleko,4),R_ufeko,
     & R_umeko,REAL(1e-6,4),REAL(R_ikeko,4),
     & REAL(R_okeko,4),REAL(R_ofeko,4),
     & REAL(R_ifeko,4),I_imeko,REAL(R_eleko,4),L_ileko,
     & REAL(R_oleko,4),L_uleko,L_ameko,R_ukeko,REAL(R_ekeko
     &,4),REAL(R_akeko,4),L_emeko)
      !}
C KPH_vlv.fgi(  46,  28):���������� ������� ��������,20KPH15CP001XQ01
      R_ureko = R8_iteme
C KPH_sens.fgi( 130, 204):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_iteko,4),REAL
     &(R_ureko,4),R_opeko,
     & R_oteko,REAL(1e-6,4),REAL(R_ereko,4),
     & REAL(R_ireko,4),REAL(R_ipeko,4),
     & REAL(R_epeko,4),I_eteko,REAL(R_aseko,4),L_eseko,
     & REAL(R_iseko,4),L_oseko,L_useko,R_oreko,REAL(R_areko
     &,4),REAL(R_upeko,4),L_ateko)
      !}
C KPH_vlv.fgi(  18,  28):���������� ������� ��������,20KPH15CP003XQ01
      R_osevi = R8_oteme
C KPH_sens.fgi( 130, 208):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_evevi,4),REAL
     &(R_osevi,4),R_irevi,
     & R_ivevi,REAL(1e-3,4),REAL(R_asevi,4),
     & REAL(R_esevi,4),REAL(R_erevi,4),
     & REAL(R_arevi,4),I_avevi,REAL(R_usevi,4),L_atevi,
     & REAL(R_etevi,4),L_itevi,L_otevi,R_isevi,REAL(R_urevi
     &,4),REAL(R_orevi,4),L_utevi)
      !}
C KPH_vlv.fgi( 133,  41):���������� ������� ��������,20KPH60CP001XQ01
      R_erivi = R8_uteme
C KPH_sens.fgi( 287, 260):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_apivi,R_usivi,REAL(1000
     &,4),
     & REAL(R_opivi,4),REAL(R_upivi,4),
     & REAL(R_umivi,4),REAL(R_omivi,4),I_osivi,
     & REAL(R_irivi,4),L_orivi,REAL(R_urivi,4),L_asivi,L_esivi
     &,R_arivi,
     & REAL(R_ipivi,4),REAL(R_epivi,4),L_isivi,REAL(R_erivi
     &,4))
      !}
C KPH_vlv.fgi( 104,  41):���������� �������,20KPH40CL003XQ01
      R_asafo = R8_aveme
C KPH_sens.fgi( 364, 268):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_upafo,R_otafo,REAL(1,4
     &),
     & REAL(R_irafo,4),REAL(R_orafo,4),
     & REAL(R_opafo,4),REAL(R_ipafo,4),I_itafo,
     & REAL(R_esafo,4),L_isafo,REAL(R_osafo,4),L_usafo,L_atafo
     &,R_urafo,
     & REAL(R_erafo,4),REAL(R_arafo,4),L_etafo,REAL(R_asafo
     &,4))
      !}
C KPH_vlv.fgi(  75,  41):���������� �������,20KPH20CQ001XQ01
      R_oxeko = R8_eveme
C KPH_sens.fgi( 130, 212):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_ediko,4),REAL
     &(R_oxeko,4),R_iveko,
     & R_idiko,REAL(1e-6,4),REAL(R_axeko,4),
     & REAL(R_exeko,4),REAL(R_eveko,4),
     & REAL(R_aveko,4),I_adiko,REAL(R_uxeko,4),L_abiko,
     & REAL(R_ebiko,4),L_ibiko,L_obiko,R_ixeko,REAL(R_uveko
     &,4),REAL(R_oveko,4),L_ubiko)
      !}
C KPH_vlv.fgi(  46,  41):���������� ������� ��������,20KPH15CP004XQ01
      R_ikiko = R8_iveme
C KPH_sens.fgi(  52, 176):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_efiko,R_amiko,REAL(1,4
     &),
     & REAL(R_ufiko,4),REAL(R_akiko,4),
     & REAL(R_afiko,4),REAL(R_udiko,4),I_uliko,
     & REAL(R_okiko,4),L_ukiko,REAL(R_aliko,4),L_eliko,L_iliko
     &,R_ekiko,
     & REAL(R_ofiko,4),REAL(R_ifiko,4),L_oliko,REAL(R_ikiko
     &,4))
      !}
C KPH_vlv.fgi(  18,  41):���������� �������,20KPH11CT001XQ01
      R_ixafo = R8_oveme
C KPH_sens.fgi( 130, 216):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_adefo,4),REAL
     &(R_ixafo,4),R_evafo,
     & R_edefo,REAL(1e-3,4),REAL(R_uvafo,4),
     & REAL(R_axafo,4),REAL(R_avafo,4),
     & REAL(R_utafo,4),I_ubefo,REAL(R_oxafo,4),L_uxafo,
     & REAL(R_abefo,4),L_ebefo,L_ibefo,R_exafo,REAL(R_ovafo
     &,4),REAL(R_ivafo,4),L_obefo)
      !}
C KPH_vlv.fgi(  75,  54):���������� ������� ��������,20KPH20CP002XQ01
      R_upiko = R8_uveme
C KPH_sens.fgi( 130, 220):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_isiko,4),REAL
     &(R_upiko,4),R_omiko,
     & R_osiko,REAL(1e-6,4),REAL(R_epiko,4),
     & REAL(R_ipiko,4),REAL(R_imiko,4),
     & REAL(R_emiko,4),I_esiko,REAL(R_ariko,4),L_eriko,
     & REAL(R_iriko,4),L_oriko,L_uriko,R_opiko,REAL(R_apiko
     &,4),REAL(R_umiko,4),L_asiko)
      !}
C KPH_vlv.fgi(  46,  54):���������� ������� ��������,20KPH11CP001XQ01
      R_oviko = R8_axeme
C KPH_sens.fgi( 287, 264):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_itiko,R_eboko,REAL(1000
     &,4),
     & REAL(R_aviko,4),REAL(R_eviko,4),
     & REAL(R_etiko,4),REAL(R_atiko,4),I_aboko,
     & REAL(R_uviko,4),L_axiko,REAL(R_exiko,4),L_ixiko,L_oxiko
     &,R_iviko,
     & REAL(R_utiko,4),REAL(R_otiko,4),L_uxiko,REAL(R_oviko
     &,4))
      !}
C KPH_vlv.fgi(  18,  54):���������� �������,20KPH11CL003XQ01
      R_olexi = R8_exeme
C KPH_sens.fgi( 130, 224):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_epexi,4),REAL
     &(R_olexi,4),R_ikexi,
     & R_ipexi,REAL(1e-3,4),REAL(R_alexi,4),
     & REAL(R_elexi,4),REAL(R_ekexi,4),
     & REAL(R_akexi,4),I_apexi,REAL(R_ulexi,4),L_amexi,
     & REAL(R_emexi,4),L_imexi,L_omexi,R_ilexi,REAL(R_ukexi
     &,4),REAL(R_okexi,4),L_umexi)
      !}
C KPH_vlv.fgi( 104,  67):���������� ������� ��������,20KPH11CP112XQ01
      R_ekefo = R8_ixeme
C KPH_sens.fgi(  52, 180):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_afefo,R_ulefo,REAL(1,4
     &),
     & REAL(R_ofefo,4),REAL(R_ufefo,4),
     & REAL(R_udefo,4),REAL(R_odefo,4),I_olefo,
     & REAL(R_ikefo,4),L_okefo,REAL(R_ukefo,4),L_alefo,L_elefo
     &,R_akefo,
     & REAL(R_ifefo,4),REAL(R_efefo,4),L_ilefo,REAL(R_ekefo
     &,4))
      !}
C KPH_vlv.fgi(  75,  67):���������� �������,20KPH20CT003XQ01
      R_afoko = R8_oxeme
C KPH_sens.fgi( 208, 244):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_uboko,R_okoko,REAL(1,4
     &),
     & REAL(R_idoko,4),REAL(R_odoko,4),
     & REAL(R_oboko,4),REAL(R_iboko,4),I_ikoko,
     & REAL(R_efoko,4),L_ifoko,REAL(R_ofoko,4),L_ufoko,L_akoko
     &,R_udoko,
     & REAL(R_edoko,4),REAL(R_adoko,4),L_ekoko,REAL(R_afoko
     &,4))
      !}
C KPH_vlv.fgi(  46,  67):���������� �������,20KPH11CF001XQ01
      L_ekato=R_udoko.lt.R0_e
C KPG_KPH_blk.fgi(  46, 139):���������� <
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_emato,4),L_osato,L_usato
     &,R8_ufato,C30_elato,
     & L_oboto,L_idoto,L_ipoto,I_orato,I_urato,R_olato,R_ilato
     &,
     & R_ubato,REAL(R_edato,4),R_afato,
     & REAL(R_ifato,4),R_idato,REAL(R_udato,4),I_upato,
     & I_asato,I_irato,I_erato,L_ofato,L_esato,L_otato,L_efato
     &,
     & L_odato,L_okato,L_ikato,L_atato,L_adato,L_alato,
     & L_evato,L_isato,L_ulato,L_amato,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_ekato,L_akato,L_utato,R_opato,REAL(R_ukato,4),L_avato
     &,L_ivato,
     & L_imato,L_omato,L_umato,L_epato,L_ipato,L_apato)
      !}

      if(L_ipato.or.L_epato.or.L_apato.or.L_umato.or.L_omato.or.L_imato
     &) then      
                  I_arato = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_arato = z'40000000'
      endif
C KPH_vlv.fgi( 258, 179):���� ���������� �������� ��������,20KPH10AA103
      R_imoko = R8_uxeme
C KPH_sens.fgi(  52, 184):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_eloko,R_aroko,REAL(1,4
     &),
     & REAL(R_uloko,4),REAL(R_amoko,4),
     & REAL(R_aloko,4),REAL(R_ukoko,4),I_upoko,
     & REAL(R_omoko,4),L_umoko,REAL(R_apoko,4),L_epoko,L_ipoko
     &,R_emoko,
     & REAL(R_oloko,4),REAL(R_iloko,4),L_opoko,REAL(R_imoko
     &,4))
      !}
C KPH_vlv.fgi(  18,  67):���������� �������,20KPH11CT003XQ01
      R_ovivi = R8_abime
C KPH_sens.fgi( 130, 228):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_ebovi,4),REAL
     &(R_ovivi,4),R_itivi,
     & R_ibovi,REAL(1e-6,4),REAL(R_avivi,4),
     & REAL(R_evivi,4),REAL(R_etivi,4),
     & REAL(R_ativi,4),I_abovi,REAL(R_uvivi,4),L_axivi,
     & REAL(R_exivi,4),L_ixivi,L_oxivi,R_ivivi,REAL(R_utivi
     &,4),REAL(R_otivi,4),L_uxivi)
      !}
C KPH_vlv.fgi( 133,  80):���������� ������� ��������,20KPH31CP001XQ01
      R_isexi = R8_ebime
C KPH_sens.fgi(  52, 188):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_erexi,R_avexi,REAL(1,4
     &),
     & REAL(R_urexi,4),REAL(R_asexi,4),
     & REAL(R_arexi,4),REAL(R_upexi,4),I_utexi,
     & REAL(R_osexi,4),L_usexi,REAL(R_atexi,4),L_etexi,L_itexi
     &,R_esexi,
     & REAL(R_orexi,4),REAL(R_irexi,4),L_otexi,REAL(R_isexi
     &,4))
      !}
C KPH_vlv.fgi( 104,  80):���������� �������,20KPH13CT002XQ01
      R_usoko = R8_ibime
C KPH_sens.fgi( 130, 232):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_ivoko,4),REAL
     &(R_usoko,4),R_oroko,
     & R_ovoko,REAL(1e-3,4),REAL(R_esoko,4),
     & REAL(R_isoko,4),REAL(R_iroko,4),
     & REAL(R_eroko,4),I_evoko,REAL(R_atoko,4),L_etoko,
     & REAL(R_itoko,4),L_otoko,L_utoko,R_osoko,REAL(R_asoko
     &,4),REAL(R_uroko,4),L_avoko)
      !}
C KPH_vlv.fgi(  46,  80):���������� ������� ��������,20KPH11CP003XQ01
      R_uxexi = R8_obime
C KPH_sens.fgi( 130, 236):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_idixi,4),REAL
     &(R_uxexi,4),R_ovexi,
     & R_odixi,REAL(1e-3,4),REAL(R_exexi,4),
     & REAL(R_ixexi,4),REAL(R_ivexi,4),
     & REAL(R_evexi,4),I_edixi,REAL(R_abixi,4),L_ebixi,
     & REAL(R_ibixi,4),L_obixi,L_ubixi,R_oxexi,REAL(R_axexi
     &,4),REAL(R_uvexi,4),L_adixi)
      !}
C KPH_vlv.fgi( 104,  93):���������� ������� ��������,20KPH11CP111XQ01
      R_urifo = R8_ubime
C KPH_sens.fgi( 130, 240):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_itifo,4),REAL
     &(R_urifo,4),R_opifo,
     & R_otifo,REAL(1e-6,4),REAL(R_erifo,4),
     & REAL(R_irifo,4),REAL(R_ipifo,4),
     & REAL(R_epifo,4),I_etifo,REAL(R_asifo,4),L_esifo,
     & REAL(R_isifo,4),L_osifo,L_usifo,R_orifo,REAL(R_arifo
     &,4),REAL(R_upifo,4),L_atifo)
      !}
C KPH_vlv.fgi(  75,  93):���������� ������� ��������,20KPH15CP002XQ01
      R_iruko = R8_adime
C KPH_sens.fgi( 208, 248):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_epuko,R_atuko,REAL(1,4
     &),
     & REAL(R_upuko,4),REAL(R_aruko,4),
     & REAL(R_apuko,4),REAL(R_umuko,4),I_usuko,
     & REAL(R_oruko,4),L_uruko,REAL(R_asuko,4),L_esuko,L_isuko
     &,R_eruko,
     & REAL(R_opuko,4),REAL(R_ipuko,4),L_osuko,REAL(R_iruko
     &,4))
      !}
C KPH_vlv.fgi(  18,  93):���������� �������,20KPH11CF002XQ01
      R_aduvi = R8_edime
C KPH_sens.fgi(  52, 192):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_uxovi,R_ofuvi,REAL(1,4
     &),
     & REAL(R_ibuvi,4),REAL(R_obuvi,4),
     & REAL(R_oxovi,4),REAL(R_ixovi,4),I_ifuvi,
     & REAL(R_eduvi,4),L_iduvi,REAL(R_oduvi,4),L_uduvi,L_afuvi
     &,R_ubuvi,
     & REAL(R_ebuvi,4),REAL(R_abuvi,4),L_efuvi,REAL(R_aduvi
     &,4))
      !}
C KPH_vlv.fgi( 133,  93):���������� �������,20KPH31CT004XQ01
      R_epovi = R8_idime
C KPH_sens.fgi(  52, 196):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_amovi,R_urovi,REAL(1,4
     &),
     & REAL(R_omovi,4),REAL(R_umovi,4),
     & REAL(R_ulovi,4),REAL(R_olovi,4),I_orovi,
     & REAL(R_ipovi,4),L_opovi,REAL(R_upovi,4),L_arovi,L_erovi
     &,R_apovi,
     & REAL(R_imovi,4),REAL(R_emovi,4),L_irovi,REAL(R_epovi
     &,4))
      !}
C KPH_vlv.fgi( 133,  54):���������� �������,20KPH31CT003XQ01
      R_iluvi = R8_odime
C KPH_sens.fgi(  52, 200):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ekuvi,R_apuvi,REAL(1,4
     &),
     & REAL(R_ukuvi,4),REAL(R_aluvi,4),
     & REAL(R_akuvi,4),REAL(R_ufuvi,4),I_umuvi,
     & REAL(R_oluvi,4),L_uluvi,REAL(R_amuvi,4),L_emuvi,L_imuvi
     &,R_eluvi,
     & REAL(R_okuvi,4),REAL(R_ikuvi,4),L_omuvi,REAL(R_iluvi
     &,4))
      !}
C KPH_vlv.fgi( 133, 106):���������� �������,20KPH31CT002XQ01
      R_otovi = R8_udime
C KPH_sens.fgi(  52, 204):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_isovi,R_exovi,REAL(1,4
     &),
     & REAL(R_atovi,4),REAL(R_etovi,4),
     & REAL(R_esovi,4),REAL(R_asovi,4),I_axovi,
     & REAL(R_utovi,4),L_avovi,REAL(R_evovi,4),L_ivovi,L_ovovi
     &,R_itovi,
     & REAL(R_usovi,4),REAL(R_osovi,4),L_uvovi,REAL(R_otovi
     &,4))
      !}
C KPH_vlv.fgi( 133,  67):���������� �������,20KPH31CT001XQ01
      R_okixi = R8_afime
C KPH_sens.fgi( 208, 252):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ifixi,R_emixi,REAL(1,4
     &),
     & REAL(R_akixi,4),REAL(R_ekixi,4),
     & REAL(R_efixi,4),REAL(R_afixi,4),I_amixi,
     & REAL(R_ukixi,4),L_alixi,REAL(R_elixi,4),L_ilixi,L_olixi
     &,R_ikixi,
     & REAL(R_ufixi,4),REAL(R_ofixi,4),L_ulixi,REAL(R_okixi
     &,4))
      !}
C KPH_vlv.fgi( 104, 106):���������� �������,20KPH13CF001XQ01
      R_itofo = R8_efime
C KPH_sens.fgi(  52, 208):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_esofo,R_axofo,REAL(1,4
     &),
     & REAL(R_usofo,4),REAL(R_atofo,4),
     & REAL(R_asofo,4),REAL(R_urofo,4),I_uvofo,
     & REAL(R_otofo,4),L_utofo,REAL(R_avofo,4),L_evofo,L_ivofo
     &,R_etofo,
     & REAL(R_osofo,4),REAL(R_isofo,4),L_ovofo,REAL(R_itofo
     &,4))
      !}
C KPH_vlv.fgi(  75, 106):���������� �������,20KPH14CT003XQ01
      R_uvuko = R8_ifime
C KPH_sens.fgi( 130, 244):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_ibalo,4),REAL
     &(R_uvuko,4),R_otuko,
     & R_obalo,REAL(1e-6,4),REAL(R_evuko,4),
     & REAL(R_ivuko,4),REAL(R_ituko,4),
     & REAL(R_etuko,4),I_ebalo,REAL(R_axuko,4),L_exuko,
     & REAL(R_ixuko,4),L_oxuko,L_uxuko,R_ovuko,REAL(R_avuko
     &,4),REAL(R_utuko,4),L_abalo)
      !}
C KPH_vlv.fgi(  46, 106):���������� ������� ��������,20KPH11CP002XQ01
      R_amori = R8_ofime
C KPH_sens.fgi( 130, 248):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_opori,4),REAL
     &(R_amori,4),R_ukori,
     & R_upori,REAL(1e-6,4),REAL(R_ilori,4),
     & REAL(R_olori,4),REAL(R_okori,4),
     & REAL(R_ikori,4),I_ipori,REAL(R_emori,4),L_imori,
     & REAL(R_omori,4),L_umori,L_apori,R_ulori,REAL(R_elori
     &,4),REAL(R_alori,4),L_epori)
      !}
C KPH_vlv.fgi( 162, 119):���������� ������� ��������,20KPH41CP001XQ01
      R_ifovi = R8_ufime
C KPH_sens.fgi( 130, 252):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_alovi,4),REAL
     &(R_ifovi,4),R_edovi,
     & R_elovi,REAL(1e-6,4),REAL(R_udovi,4),
     & REAL(R_afovi,4),REAL(R_adovi,4),
     & REAL(R_ubovi,4),I_ukovi,REAL(R_ofovi,4),L_ufovi,
     & REAL(R_akovi,4),L_ekovi,L_ikovi,R_efovi,REAL(R_odovi
     &,4),REAL(R_idovi,4),L_okovi)
      !}
C KPH_vlv.fgi( 133, 119):���������� ������� ��������,20KPH31CP003XQ01
      R_arixi = R8_akime
C KPH_sens.fgi(  52, 212):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_umixi,R_osixi,REAL(1,4
     &),
     & REAL(R_ipixi,4),REAL(R_opixi,4),
     & REAL(R_omixi,4),REAL(R_imixi,4),I_isixi,
     & REAL(R_erixi,4),L_irixi,REAL(R_orixi,4),L_urixi,L_asixi
     &,R_upixi,
     & REAL(R_epixi,4),REAL(R_apixi,4),L_esixi,REAL(R_arixi
     &,4))
      !}
C KPH_vlv.fgi( 104, 119):���������� �������,20KPH61CT001XQ01
      R_ubufo = R8_ekime
C KPH_sens.fgi(  52, 216):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_oxofo,R_ifufo,REAL(1,4
     &),
     & REAL(R_ebufo,4),REAL(R_ibufo,4),
     & REAL(R_ixofo,4),REAL(R_exofo,4),I_efufo,
     & REAL(R_adufo,4),L_edufo,REAL(R_idufo,4),L_odufo,L_udufo
     &,R_obufo,
     & REAL(R_abufo,4),REAL(R_uxofo,4),L_afufo,REAL(R_ubufo
     &,4))
      !}
C KPH_vlv.fgi(  75, 119):���������� �������,20KPH14CT001XQ01
      R_apalo = R8_ikime
C KPH_sens.fgi(  52, 220):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ulalo,R_oralo,REAL(1,4
     &),
     & REAL(R_imalo,4),REAL(R_omalo,4),
     & REAL(R_olalo,4),REAL(R_ilalo,4),I_iralo,
     & REAL(R_epalo,4),L_ipalo,REAL(R_opalo,4),L_upalo,L_aralo
     &,R_umalo,
     & REAL(R_emalo,4),REAL(R_amalo,4),L_eralo,REAL(R_apalo
     &,4))
      !}
C KPH_vlv.fgi(  46, 119):���������� �������,20KPH11CT002XQ01
      R_italo = R8_okime
C KPH_sens.fgi( 208, 256):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_esalo,R_axalo,REAL(1,4
     &),
     & REAL(R_usalo,4),REAL(R_atalo,4),
     & REAL(R_asalo,4),REAL(R_uralo,4),I_uvalo,
     & REAL(R_otalo,4),L_utalo,REAL(R_avalo,4),L_evalo,L_ivalo
     &,R_etalo,
     & REAL(R_osalo,4),REAL(R_isalo,4),L_ovalo,REAL(R_italo
     &,4))
      !}
C KPH_vlv.fgi(  18, 119):���������� �������,20KPH51CF002XQ01
      R_usori = R8_ukime
C KPH_sens.fgi( 130, 256):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_ivori,4),REAL
     &(R_usori,4),R_orori,
     & R_ovori,REAL(1e-6,4),REAL(R_esori,4),
     & REAL(R_isori,4),REAL(R_irori,4),
     & REAL(R_erori,4),I_evori,REAL(R_atori,4),L_etori,
     & REAL(R_itori,4),L_otori,L_utori,R_osori,REAL(R_asori
     &,4),REAL(R_urori,4),L_avori)
      !}
C KPH_vlv.fgi( 162, 132):���������� ������� ��������,20KPH31CP002XQ01
      R_ivixi = R8_alime
C KPH_sens.fgi( 130, 260):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_aboxi,4),REAL
     &(R_ivixi,4),R_etixi,
     & R_eboxi,REAL(1e-6,4),REAL(R_utixi,4),
     & REAL(R_avixi,4),REAL(R_atixi,4),
     & REAL(R_usixi,4),I_uxixi,REAL(R_ovixi,4),L_uvixi,
     & REAL(R_axixi,4),L_exixi,L_ixixi,R_evixi,REAL(R_otixi
     &,4),REAL(R_itixi,4),L_oxixi)
      !}
C KPH_vlv.fgi( 104, 132):���������� ������� ��������,20KPH40CP001XQ01
      R_elufo = R8_elime
C KPH_sens.fgi( 287, 268):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_akufo,R_umufo,REAL(1000
     &,4),
     & REAL(R_okufo,4),REAL(R_ukufo,4),
     & REAL(R_ufufo,4),REAL(R_ofufo,4),I_omufo,
     & REAL(R_ilufo,4),L_olufo,REAL(R_ulufo,4),L_amufo,L_emufo
     &,R_alufo,
     & REAL(R_ikufo,4),REAL(R_ekufo,4),L_imufo,REAL(R_elufo
     &,4))
      !}
C KPH_vlv.fgi(  75, 132):���������� �������,20KPH14CL005XQ01
      R_ofaso = R8_ilime
C KPH_sens.fgi( 208, 260):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_idaso,R_elaso,REAL(1,4
     &),
     & REAL(R_afaso,4),REAL(R_efaso,4),
     & REAL(R_edaso,4),REAL(R_adaso,4),I_alaso,
     & REAL(R_ufaso,4),L_akaso,REAL(R_ekaso,4),L_ikaso,L_okaso
     &,R_ifaso,
     & REAL(R_udaso,4),REAL(R_odaso,4),L_ukaso,REAL(R_ofaso
     &,4))
      !}
C KPH_vlv.fgi(  46, 132):���������� �������,20KPH10CF002XQ01
      R_imiro = R8_olime
C KPH_sens.fgi( 208, 264):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_eliro,R_ariro,REAL(1,4
     &),
     & REAL(R_uliro,4),REAL(R_amiro,4),
     & REAL(R_aliro,4),REAL(R_ukiro,4),I_upiro,
     & REAL(R_omiro,4),L_umiro,REAL(R_apiro,4),L_epiro,L_ipiro
     &,R_emiro,
     & REAL(R_oliro,4),REAL(R_iliro,4),L_opiro,REAL(R_imiro
     &,4))
      !}
C KPH_vlv.fgi(  18, 132):���������� �������,20KPH12CF001XQ01
      R_oburi = R8_ulime
C KPH_sens.fgi( 130, 264):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_efuri,4),REAL
     &(R_oburi,4),R_ixori,
     & R_ifuri,REAL(1e-6,4),REAL(R_aburi,4),
     & REAL(R_eburi,4),REAL(R_exori,4),
     & REAL(R_axori,4),I_afuri,REAL(R_uburi,4),L_aduri,
     & REAL(R_eduri,4),L_iduri,L_oduri,R_iburi,REAL(R_uxori
     &,4),REAL(R_oxori,4),L_uduri)
      !}
C KPH_vlv.fgi( 162, 145):���������� ������� ��������,20KPH31CP004XQ01
      R_efoxi = R8_amime
C KPH_sens.fgi( 364, 272):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_adoxi,R_ukoxi,REAL(1,4
     &),
     & REAL(R_odoxi,4),REAL(R_udoxi,4),
     & REAL(R_uboxi,4),REAL(R_oboxi,4),I_okoxi,
     & REAL(R_ifoxi,4),L_ofoxi,REAL(R_ufoxi,4),L_akoxi,L_ekoxi
     &,R_afoxi,
     & REAL(R_idoxi,4),REAL(R_edoxi,4),L_ikoxi,REAL(R_efoxi
     &,4))
      !}
C KPH_vlv.fgi( 104, 145):���������� �������,20KPH13CQ001XQ01
      R_orufo = R8_emime
C KPH_sens.fgi( 287, 272):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ipufo,R_etufo,REAL(1000
     &,4),
     & REAL(R_arufo,4),REAL(R_erufo,4),
     & REAL(R_epufo,4),REAL(R_apufo,4),I_atufo,
     & REAL(R_urufo,4),L_asufo,REAL(R_esufo,4),L_isufo,L_osufo
     &,R_irufo,
     & REAL(R_upufo,4),REAL(R_opufo,4),L_usufo,REAL(R_orufo
     &,4))
      !}
C KPH_vlv.fgi(  75, 145):���������� �������,20KPH14CL006XQ01
      R_ileso = R8_imime
C KPH_sens.fgi( 208, 268):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ekeso,R_apeso,REAL(1,4
     &),
     & REAL(R_ukeso,4),REAL(R_aleso,4),
     & REAL(R_akeso,4),REAL(R_ufeso,4),I_umeso,
     & REAL(R_oleso,4),L_uleso,REAL(R_ameso,4),L_emeso,L_imeso
     &,R_eleso,
     & REAL(R_okeso,4),REAL(R_ikeso,4),L_omeso,REAL(R_ileso
     &,4))
      !}
C KPH_vlv.fgi(  46, 145):���������� �������,20KPH10CF001XQ01
      R_usiro = R8_omime
C KPH_sens.fgi( 208, 272):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_oriro,R_iviro,REAL(1,4
     &),
     & REAL(R_esiro,4),REAL(R_isiro,4),
     & REAL(R_iriro,4),REAL(R_eriro,4),I_eviro,
     & REAL(R_atiro,4),L_etiro,REAL(R_itiro,4),L_otiro,L_utiro
     &,R_osiro,
     & REAL(R_asiro,4),REAL(R_uriro,4),L_aviro,REAL(R_usiro
     &,4))
      !}
C KPH_vlv.fgi(  18, 145):���������� �������,20KPH20CF011XQ01
      R_iluri = R8_umime
C KPH_sens.fgi( 130, 268):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_apuri,4),REAL
     &(R_iluri,4),R_ekuri,
     & R_epuri,REAL(1e-6,4),REAL(R_ukuri,4),
     & REAL(R_aluri,4),REAL(R_akuri,4),
     & REAL(R_ufuri,4),I_umuri,REAL(R_oluri,4),L_uluri,
     & REAL(R_amuri,4),L_emuri,L_imuri,R_eluri,REAL(R_okuri
     &,4),REAL(R_ikuri,4),L_omuri)
      !}
C KPH_vlv.fgi( 162, 158):���������� ������� ��������,20KPH41CP003XQ01
      R_omoxi = R8_apime
C KPH_sens.fgi(  52, 224):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_iloxi,R_eroxi,REAL(1,4
     &),
     & REAL(R_amoxi,4),REAL(R_emoxi,4),
     & REAL(R_eloxi,4),REAL(R_aloxi,4),I_aroxi,
     & REAL(R_umoxi,4),L_apoxi,REAL(R_epoxi,4),L_ipoxi,L_opoxi
     &,R_imoxi,
     & REAL(R_uloxi,4),REAL(R_oloxi,4),L_upoxi,REAL(R_omoxi
     &,4))
      !}
C KPH_vlv.fgi( 104, 158):���������� �������,20KPH20CT001XQ01
      R_axufo = R8_epime
C KPH_sens.fgi(  52, 228):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_utufo,R_obako,REAL(1,4
     &),
     & REAL(R_ivufo,4),REAL(R_ovufo,4),
     & REAL(R_otufo,4),REAL(R_itufo,4),I_ibako,
     & REAL(R_exufo,4),L_ixufo,REAL(R_oxufo,4),L_uxufo,L_abako
     &,R_uvufo,
     & REAL(R_evufo,4),REAL(R_avufo,4),L_ebako,REAL(R_axufo
     &,4))
      !}
C KPH_vlv.fgi(  75, 158):���������� �������,20KPH14CT002XQ01
      R_ebeto = R8_ipime
C KPH_sens.fgi( 364, 276):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_axato,R_udeto,REAL(1,4
     &),
     & REAL(R_oxato,4),REAL(R_uxato,4),
     & REAL(R_uvato,4),REAL(R_ovato,4),I_odeto,
     & REAL(R_ibeto,4),L_obeto,REAL(R_ubeto,4),L_adeto,L_edeto
     &,R_abeto,
     & REAL(R_ixato,4),REAL(R_exato,4),L_ideto,REAL(R_ebeto
     &,4))
      !}
C KPH_vlv.fgi(  46, 158):���������� �������,20KPH10CQ001XQ01
      R_ureso = R8_opime
C KPH_sens.fgi(  52, 232):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_opeso,R_iteso,REAL(1,4
     &),
     & REAL(R_ereso,4),REAL(R_ireso,4),
     & REAL(R_ipeso,4),REAL(R_epeso,4),I_eteso,
     & REAL(R_aseso,4),L_eseso,REAL(R_iseso,4),L_oseso,L_useso
     &,R_oreso,
     & REAL(R_areso,4),REAL(R_upeso,4),L_ateso,REAL(R_ureso
     &,4))
      !}
C KPH_vlv.fgi(  18, 158):���������� �������,20KPH10CT001XQ01
      R_esuri = R8_upime
C KPH_sens.fgi( 130, 272):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_uturi,4),REAL
     &(R_esuri,4),R_aruri,
     & R_avuri,REAL(1e-6,4),REAL(R_oruri,4),
     & REAL(R_ururi,4),REAL(R_upuri,4),
     & REAL(R_opuri,4),I_oturi,REAL(R_isuri,4),L_osuri,
     & REAL(R_usuri,4),L_aturi,L_eturi,R_asuri,REAL(R_iruri
     &,4),REAL(R_eruri,4),L_ituri)
      !}
C KPH_vlv.fgi( 162, 171):���������� ������� ��������,20KPH41CP002XQ01
      R_ofaxi = R8_arime
C KPH_sens.fgi(  52, 236):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_idaxi,R_elaxi,REAL(1,4
     &),
     & REAL(R_afaxi,4),REAL(R_efaxi,4),
     & REAL(R_edaxi,4),REAL(R_adaxi,4),I_alaxi,
     & REAL(R_ufaxi,4),L_akaxi,REAL(R_ekaxi,4),L_ikaxi,L_okaxi
     &,R_ifaxi,
     & REAL(R_udaxi,4),REAL(R_odaxi,4),L_ukaxi,REAL(R_ofaxi
     &,4))
      !}
C KPH_vlv.fgi( 133, 158):���������� �������,20KPH41CT004XQ01
      R_uruvi = R8_erime
C KPH_sens.fgi(  52, 240):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_opuvi,R_ituvi,REAL(1,4
     &),
     & REAL(R_eruvi,4),REAL(R_iruvi,4),
     & REAL(R_ipuvi,4),REAL(R_epuvi,4),I_etuvi,
     & REAL(R_asuvi,4),L_esuvi,REAL(R_isuvi,4),L_osuvi,L_usuvi
     &,R_oruvi,
     & REAL(R_aruvi,4),REAL(R_upuvi,4),L_atuvi,REAL(R_uruvi
     &,4))
      !}
C KPH_vlv.fgi( 133, 132):���������� �������,20KPH41CT003XQ01
      R_apaxi = R8_irime
C KPH_sens.fgi(  52, 244):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ulaxi,R_oraxi,REAL(1,4
     &),
     & REAL(R_imaxi,4),REAL(R_omaxi,4),
     & REAL(R_olaxi,4),REAL(R_ilaxi,4),I_iraxi,
     & REAL(R_epaxi,4),L_ipaxi,REAL(R_opaxi,4),L_upaxi,L_araxi
     &,R_umaxi,
     & REAL(R_emaxi,4),REAL(R_amaxi,4),L_eraxi,REAL(R_apaxi
     &,4))
      !}
C KPH_vlv.fgi( 133, 171):���������� �������,20KPH41CT002XQ01
      R_exuvi = R8_orime
C KPH_sens.fgi(  52, 248):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_avuvi,R_ubaxi,REAL(1,4
     &),
     & REAL(R_ovuvi,4),REAL(R_uvuvi,4),
     & REAL(R_utuvi,4),REAL(R_otuvi,4),I_obaxi,
     & REAL(R_ixuvi,4),L_oxuvi,REAL(R_uxuvi,4),L_abaxi,L_ebaxi
     &,R_axuvi,
     & REAL(R_ivuvi,4),REAL(R_evuvi,4),L_ibaxi,REAL(R_exuvi
     &,4))
      !}
C KPH_vlv.fgi( 133, 145):���������� �������,20KPH41CT001XQ01
      R_atoxi = R8_urime
C KPH_sens.fgi( 287, 276):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_uroxi,R_ovoxi,REAL(1000
     &,4),
     & REAL(R_isoxi,4),REAL(R_osoxi,4),
     & REAL(R_oroxi,4),REAL(R_iroxi,4),I_ivoxi,
     & REAL(R_etoxi,4),L_itoxi,REAL(R_otoxi,4),L_utoxi,L_avoxi
     &,R_usoxi,
     & REAL(R_esoxi,4),REAL(R_asoxi,4),L_evoxi,REAL(R_atoxi
     &,4))
      !}
C KPH_vlv.fgi( 104, 171):���������� �������,20KPH20CL003XQ01
      R_ibuxi = R8_asime
C KPH_sens.fgi( 208, 276):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_exoxi,R_afuxi,REAL(1,4
     &),
     & REAL(R_uxoxi,4),REAL(R_abuxi,4),
     & REAL(R_axoxi,4),REAL(R_uvoxi,4),I_uduxi,
     & REAL(R_obuxi,4),L_ubuxi,REAL(R_aduxi,4),L_eduxi,L_iduxi
     &,R_ebuxi,
     & REAL(R_oxoxi,4),REAL(R_ixoxi,4),L_oduxi,REAL(R_ibuxi
     &,4))
      !}
C KPH_vlv.fgi( 104, 184):���������� �������,20KPH20CF001XQ01
      R_ototo = R8_esime
C KPH_sens.fgi(  52, 252):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_isoto,R_exoto,REAL(1,4
     &),
     & REAL(R_atoto,4),REAL(R_etoto,4),
     & REAL(R_esoto,4),REAL(R_asoto,4),I_axoto,
     & REAL(R_utoto,4),L_avoto,REAL(R_evoto,4),L_ivoto,L_ovoto
     &,R_itoto,
     & REAL(R_usoto,4),REAL(R_osoto,4),L_uvoto,REAL(R_ototo
     &,4))
      !}
C KPH_vlv.fgi(  18, 171):���������� �������,20KPH12CT001XQ01
      R_emevi = R8_isime
C KPH_sens.fgi(  52, 256):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_alevi,R_upevi,REAL(1,4
     &),
     & REAL(R_olevi,4),REAL(R_ulevi,4),
     & REAL(R_ukevi,4),REAL(R_okevi,4),I_opevi,
     & REAL(R_imevi,4),L_omevi,REAL(R_umevi,4),L_apevi,L_epevi
     &,R_amevi,
     & REAL(R_ilevi,4),REAL(R_elevi,4),L_ipevi,REAL(R_emevi
     &,4))
      !}
C KPH_vlv.fgi( 162, 184):���������� �������,20KPH13CT001XQ01
      R_itaxi = R8_osime
C KPH_sens.fgi( 130, 276):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_axaxi,4),REAL
     &(R_itaxi,4),R_esaxi,
     & R_exaxi,REAL(1e-6,4),REAL(R_usaxi,4),
     & REAL(R_ataxi,4),REAL(R_asaxi,4),
     & REAL(R_uraxi,4),I_uvaxi,REAL(R_otaxi,4),L_utaxi,
     & REAL(R_avaxi,4),L_evaxi,L_ivaxi,R_etaxi,REAL(R_osaxi
     &,4),REAL(R_isaxi,4),L_ovaxi)
      !}
C KPH_vlv.fgi( 133, 184):���������� ������� ��������,20KPH41CP004XQ01
      R_etako = R8_usime
C KPH_sens.fgi(  52, 260):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_asako,R_uvako,REAL(1,4
     &),
     & REAL(R_osako,4),REAL(R_usako,4),
     & REAL(R_urako,4),REAL(R_orako,4),I_ovako,
     & REAL(R_itako,4),L_otako,REAL(R_utako,4),L_avako,L_evako
     &,R_atako,
     & REAL(R_isako,4),REAL(R_esako,4),L_ivako,REAL(R_etako
     &,4))
      !}
C KPH_vlv.fgi(  46,  15):���������� �������,20KPH15CT004XQ01
      R_ifako = R8_atime
C KPH_sens.fgi(  52, 264):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_edako,R_alako,REAL(1,4
     &),
     & REAL(R_udako,4),REAL(R_afako,4),
     & REAL(R_adako,4),REAL(R_ubako,4),I_ukako,
     & REAL(R_ofako,4),L_ufako,REAL(R_akako,4),L_ekako,L_ikako
     &,R_efako,
     & REAL(R_odako,4),REAL(R_idako,4),L_okako,REAL(R_ifako
     &,4))
      !}
C KPH_vlv.fgi(  75, 171):���������� �������,20KPH15CT003XQ01
      R_obeko = R8_etime
C KPH_sens.fgi(  52, 268):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ixako,R_efeko,REAL(1,4
     &),
     & REAL(R_abeko,4),REAL(R_ebeko,4),
     & REAL(R_exako,4),REAL(R_axako,4),I_afeko,
     & REAL(R_ubeko,4),L_adeko,REAL(R_edeko,4),L_ideko,L_odeko
     &,R_ibeko,
     & REAL(R_uxako,4),REAL(R_oxako,4),L_udeko,REAL(R_obeko
     &,4))
      !}
C KPH_vlv.fgi(  18,  15):���������� �������,20KPH15CT002XQ01
      R_umako = R8_itime
C KPH_sens.fgi(  52, 272):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_olako,R_irako,REAL(1,4
     &),
     & REAL(R_emako,4),REAL(R_imako,4),
     & REAL(R_ilako,4),REAL(R_elako,4),I_erako,
     & REAL(R_apako,4),L_epako,REAL(R_ipako,4),L_opako,L_upako
     &,R_omako,
     & REAL(R_amako,4),REAL(R_ulako,4),L_arako,REAL(R_umako
     &,4))
      !}
C KPH_vlv.fgi(  75, 184):���������� �������,20KPH15CT001XQ01
      R_ururo = R8_otime
C KPH_sens.fgi(  52, 276):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_opuro,R_ituro,REAL(1,4
     &),
     & REAL(R_eruro,4),REAL(R_iruro,4),
     & REAL(R_ipuro,4),REAL(R_epuro,4),I_eturo,
     & REAL(R_asuro,4),L_esuro,REAL(R_isuro,4),L_osuro,L_usuro
     &,R_oruro,
     & REAL(R_aruro,4),REAL(R_upuro,4),L_aturo,REAL(R_ururo
     &,4))
      !}
C KPH_vlv.fgi(  18, 184):���������� �������,20KPH20CT012XQ01
      R_ofovo = R8_utime
C KPG_sens.fgi( 129, 208):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_idovo,R_elovo,REAL(1,4
     &),
     & REAL(R_afovo,4),REAL(R_efovo,4),
     & REAL(R_edovo,4),REAL(R_adovo,4),I_alovo,
     & REAL(R_ufovo,4),L_akovo,REAL(R_ekovo,4),L_ikovo,L_okovo
     &,R_ifovo,
     & REAL(R_udovo,4),REAL(R_odovo,4),L_ukovo,REAL(R_ofovo
     &,4))
      !}
C KPG_vlv.fgi(  48,  51):���������� �������,20KPG12CT004XQ01
      R_urivo = R8_avime
C KPG_sens.fgi( 129, 212):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_opivo,R_itivo,REAL(1,4
     &),
     & REAL(R_erivo,4),REAL(R_irivo,4),
     & REAL(R_ipivo,4),REAL(R_epivo,4),I_etivo,
     & REAL(R_asivo,4),L_esivo,REAL(R_isivo,4),L_osivo,L_usivo
     &,R_orivo,
     & REAL(R_arivo,4),REAL(R_upivo,4),L_ativo,REAL(R_urivo
     &,4))
      !}
C KPG_vlv.fgi(  48,  38):���������� �������,20KPG12CT003XQ01
      R_apovo = R8_evime
C KPG_sens.fgi( 129, 216):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ulovo,R_orovo,REAL(1,4
     &),
     & REAL(R_imovo,4),REAL(R_omovo,4),
     & REAL(R_olovo,4),REAL(R_ilovo,4),I_irovo,
     & REAL(R_epovo,4),L_ipovo,REAL(R_opovo,4),L_upovo,L_arovo
     &,R_umovo,
     & REAL(R_emovo,4),REAL(R_amovo,4),L_erovo,REAL(R_apovo
     &,4))
      !}
C KPG_vlv.fgi(  48,  64):���������� �������,20KPG12CT002XQ01
      R_efexo = R8_ivime
C KPG_sens.fgi( 206, 204):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_ukexo,4),REAL
     &(R_efexo,4),R_adexo,
     & R_alexo,REAL(1e-6,4),REAL(R_odexo,4),
     & REAL(R_udexo,4),REAL(R_ubexo,4),
     & REAL(R_obexo,4),I_okexo,REAL(R_ifexo,4),L_ofexo,
     & REAL(R_ufexo,4),L_akexo,L_ekexo,R_afexo,REAL(R_idexo
     &,4),REAL(R_edexo,4),L_ikexo)
      !}
C KPG_vlv.fgi(  20,  38):���������� ������� ��������,20KPG32CP001XQ01
      R_utexo = R8_ovime
C KPG_sens.fgi( 206, 208):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_ixexo,4),REAL
     &(R_utexo,4),R_osexo,
     & R_oxexo,REAL(1e-6,4),REAL(R_etexo,4),
     & REAL(R_itexo,4),REAL(R_isexo,4),
     & REAL(R_esexo,4),I_exexo,REAL(R_avexo,4),L_evexo,
     & REAL(R_ivexo,4),L_ovexo,L_uvexo,R_otexo,REAL(R_atexo
     &,4),REAL(R_usexo,4),L_axexo)
      !}
C KPG_vlv.fgi(  76,  64):���������� ������� ��������,20KPG32CP002XQ01
      R_exivo = R8_uvime
C KPG_sens.fgi( 129, 220):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_avivo,R_ubovo,REAL(1,4
     &),
     & REAL(R_ovivo,4),REAL(R_uvivo,4),
     & REAL(R_utivo,4),REAL(R_otivo,4),I_obovo,
     & REAL(R_ixivo,4),L_oxivo,REAL(R_uxivo,4),L_abovo,L_ebovo
     &,R_axivo,
     & REAL(R_ivivo,4),REAL(R_evivo,4),L_ibovo,REAL(R_exivo
     &,4))
      !}
C KPG_vlv.fgi(  20,  51):���������� �������,20KPG12CT001XQ01
      L_(4)=R_axivo.gt.R0_i
C KPG_KPH_blk.fgi(  46, 150):���������� >
      R_apexo = R8_axime
C KPG_sens.fgi( 206, 212):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_orexo,4),REAL
     &(R_apexo,4),R_ulexo,
     & R_urexo,REAL(1e-6,4),REAL(R_imexo,4),
     & REAL(R_omexo,4),REAL(R_olexo,4),
     & REAL(R_ilexo,4),I_irexo,REAL(R_epexo,4),L_ipexo,
     & REAL(R_opexo,4),L_upexo,L_arexo,R_umexo,REAL(R_emexo
     &,4),REAL(R_amexo,4),L_erexo)
      !}
C KPG_vlv.fgi(  76,  51):���������� ������� ��������,20KPG32CP003XQ01
      R_odixo = R8_exime
C KPG_sens.fgi( 206, 216):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_ekixo,4),REAL
     &(R_odixo,4),R_ibixo,
     & R_ikixo,REAL(1e-6,4),REAL(R_adixo,4),
     & REAL(R_edixo,4),REAL(R_ebixo,4),
     & REAL(R_abixo,4),I_akixo,REAL(R_udixo,4),L_afixo,
     & REAL(R_efixo,4),L_ifixo,L_ofixo,R_idixo,REAL(R_ubixo
     &,4),REAL(R_obixo,4),L_ufixo)
      !}
C KPG_vlv.fgi(  76,  77):���������� ������� ��������,20KPG32CP004XQ01
      R_eduvo = R8_ixime
C KPG_sens.fgi( 206, 220):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_ufuvo,4),REAL
     &(R_eduvo,4),R_abuvo,
     & R_akuvo,REAL(1e-6,4),REAL(R_obuvo,4),
     & REAL(R_ubuvo,4),REAL(R_uxovo,4),
     & REAL(R_oxovo,4),I_ofuvo,REAL(R_iduvo,4),L_oduvo,
     & REAL(R_uduvo,4),L_afuvo,L_efuvo,R_aduvo,REAL(R_ibuvo
     &,4),REAL(R_ebuvo,4),L_ifuvo)
      !}
C KPG_vlv.fgi(  48,  77):���������� ������� ��������,20KPG12CP003XQ01
      R_usuvo = R8_oxime
C KPG_sens.fgi( 206, 224):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_ivuvo,4),REAL
     &(R_usuvo,4),R_oruvo,
     & R_ovuvo,REAL(1e-6,4),REAL(R_esuvo,4),
     & REAL(R_isuvo,4),REAL(R_iruvo,4),
     & REAL(R_eruvo,4),I_evuvo,REAL(R_atuvo,4),L_etuvo,
     & REAL(R_ituvo,4),L_otuvo,L_utuvo,R_osuvo,REAL(R_asuvo
     &,4),REAL(R_uruvo,4),L_avuvo)
      !}
C KPG_vlv.fgi(  20,  90):���������� ������� ��������,20KPG12CP004XQ01
      R_obaxo = R8_uxime
C KPG_sens.fgi( 129, 224):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ixuvo,R_efaxo,REAL(1,4
     &),
     & REAL(R_abaxo,4),REAL(R_ebaxo,4),
     & REAL(R_exuvo,4),REAL(R_axuvo,4),I_afaxo,
     & REAL(R_ubaxo,4),L_adaxo,REAL(R_edaxo,4),L_idaxo,L_odaxo
     &,R_ibaxo,
     & REAL(R_uxuvo,4),REAL(R_oxuvo,4),L_udaxo,REAL(R_obaxo
     &,4))
      !}
C KPG_vlv.fgi(  48, 103):���������� �������,20KPG32CT003XQ01
      R_alaxo = R8_abome
C KPG_sens.fgi( 129, 228):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ufaxo,R_omaxo,REAL(1,4
     &),
     & REAL(R_ikaxo,4),REAL(R_okaxo,4),
     & REAL(R_ofaxo,4),REAL(R_ifaxo,4),I_imaxo,
     & REAL(R_elaxo,4),L_ilaxo,REAL(R_olaxo,4),L_ulaxo,L_amaxo
     &,R_ukaxo,
     & REAL(R_ekaxo,4),REAL(R_akaxo,4),L_emaxo,REAL(R_alaxo
     &,4))
      !}
C KPG_vlv.fgi(  20, 103):���������� �������,20KPG32CT001XQ01
      R_eboxo = R8_ebome
C KPG_sens.fgi( 285, 256):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_axixo,R_udoxo,REAL(1,4
     &),
     & REAL(R_oxixo,4),REAL(R_uxixo,4),
     & REAL(R_uvixo,4),REAL(R_ovixo,4),I_odoxo,
     & REAL(R_iboxo,4),L_oboxo,REAL(R_uboxo,4),L_adoxo,L_edoxo
     &,R_aboxo,
     & REAL(R_ixixo,4),REAL(R_exixo,4),L_idoxo,REAL(R_eboxo
     &,4))
      !}
C KPG_vlv.fgi(  76, 116):���������� �������,20KPG32CQ001XQ01
      R_uvaxo = R8_ibome
C KPG_sens.fgi( 129, 232):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_otaxo,R_ibexo,REAL(1,4
     &),
     & REAL(R_evaxo,4),REAL(R_ivaxo,4),
     & REAL(R_itaxo,4),REAL(R_etaxo,4),I_ebexo,
     & REAL(R_axaxo,4),L_exaxo,REAL(R_ixaxo,4),L_oxaxo,L_uxaxo
     &,R_ovaxo,
     & REAL(R_avaxo,4),REAL(R_utaxo,4),L_abexo,REAL(R_uvaxo
     &,4))
      !}
C KPG_vlv.fgi(  48, 116):���������� �������,20KPG32CT002XQ01
      R_iraxo = R8_obome
C KPG_sens.fgi( 129, 236):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_epaxo,R_ataxo,REAL(1,4
     &),
     & REAL(R_upaxo,4),REAL(R_araxo,4),
     & REAL(R_apaxo,4),REAL(R_umaxo,4),I_usaxo,
     & REAL(R_oraxo,4),L_uraxo,REAL(R_asaxo,4),L_esaxo,L_isaxo
     &,R_eraxo,
     & REAL(R_opaxo,4),REAL(R_ipaxo,4),L_osaxo,REAL(R_iraxo
     &,4))
      !}
C KPG_vlv.fgi(  20, 116):���������� �������,20KPG32CT004XQ01
      R_okoxo = R8_ubome
C KPG_sens.fgi( 361, 244):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ifoxo,R_emoxo,REAL(1,4
     &),
     & REAL(R_akoxo,4),REAL(R_ekoxo,4),
     & REAL(R_efoxo,4),REAL(R_afoxo,4),I_amoxo,
     & REAL(R_ukoxo,4),L_aloxo,REAL(R_eloxo,4),L_iloxo,L_oloxo
     &,R_ikoxo,
     & REAL(R_ufoxo,4),REAL(R_ofoxo,4),L_uloxo,REAL(R_okoxo
     &,4))
      !}
C KPG_vlv.fgi(  76, 130):���������� �������,20KPG32CF001XQ01
      R_ivoxo = R8_adome
C KPG_sens.fgi( 361, 248):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_etoxo,R_abuxo,REAL(1,4
     &),
     & REAL(R_utoxo,4),REAL(R_avoxo,4),
     & REAL(R_atoxo,4),REAL(R_usoxo,4),I_uxoxo,
     & REAL(R_ovoxo,4),L_uvoxo,REAL(R_axoxo,4),L_exoxo,L_ixoxo
     &,R_evoxo,
     & REAL(R_otoxo,4),REAL(R_itoxo,4),L_oxoxo,REAL(R_ivoxo
     &,4))
      !}
C KPG_vlv.fgi(  48, 130):���������� �������,20KPG43CF001XQ01
      R_omuxo = R8_edome
C KPG_sens.fgi( 206, 232):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_eruxo,4),REAL
     &(R_omuxo,4),R_iluxo,
     & R_iruxo,REAL(1e-3,4),REAL(R_amuxo,4),
     & REAL(R_emuxo,4),REAL(R_eluxo,4),
     & REAL(R_aluxo,4),I_aruxo,REAL(R_umuxo,4),L_apuxo,
     & REAL(R_epuxo,4),L_ipuxo,L_opuxo,R_imuxo,REAL(R_uluxo
     &,4),REAL(R_oluxo,4),L_upuxo)
      !}
C KPG_vlv.fgi(  76, 143):���������� ������� ��������,20KPG42CP001XQ01
      R_uduxo = R8_idome
C KPG_sens.fgi( 206, 228):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_ikuxo,4),REAL
     &(R_uduxo,4),R_obuxo,
     & R_okuxo,REAL(1e-6,4),REAL(R_eduxo,4),
     & REAL(R_iduxo,4),REAL(R_ibuxo,4),
     & REAL(R_ebuxo,4),I_ekuxo,REAL(R_afuxo,4),L_efuxo,
     & REAL(R_ifuxo,4),L_ofuxo,L_ufuxo,R_oduxo,REAL(R_aduxo
     &,4),REAL(R_ubuxo,4),L_akuxo)
      !}
C KPG_vlv.fgi(  20, 130):���������� ������� ��������,20KPG43CP001XQ01
      R_ifebu = R8_odome
C KPG_sens.fgi( 361, 252):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_edebu,R_alebu,REAL(1,4
     &),
     & REAL(R_udebu,4),REAL(R_afebu,4),
     & REAL(R_adebu,4),REAL(R_ubebu,4),I_ukebu,
     & REAL(R_ofebu,4),L_ufebu,REAL(R_akebu,4),L_ekebu,L_ikebu
     &,R_efebu,
     & REAL(R_odebu,4),REAL(R_idebu,4),L_okebu,REAL(R_ifebu
     &,4))
      !}
C KPG_vlv.fgi(  76, 156):���������� �������,20KPG60CF001XQ01
      R_umebu = R8_udome
C KPG_sens.fgi( 285, 260):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_olebu,R_irebu,REAL(1,4
     &),
     & REAL(R_emebu,4),REAL(R_imebu,4),
     & REAL(R_ilebu,4),REAL(R_elebu,4),I_erebu,
     & REAL(R_apebu,4),L_epebu,REAL(R_ipebu,4),L_opebu,L_upebu
     &,R_omebu,
     & REAL(R_amebu,4),REAL(R_ulebu,4),L_arebu,REAL(R_umebu
     &,4))
      !}
C KPG_vlv.fgi(  48, 156):���������� �������,20KPG60CQ002XQ01
      R_obibu = R8_afome
C KPG_sens.fgi( 129, 240):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ixebu,R_efibu,REAL(1,4
     &),
     & REAL(R_abibu,4),REAL(R_ebibu,4),
     & REAL(R_exebu,4),REAL(R_axebu,4),I_afibu,
     & REAL(R_ubibu,4),L_adibu,REAL(R_edibu,4),L_idibu,L_odibu
     &,R_ibibu,
     & REAL(R_uxebu,4),REAL(R_oxebu,4),L_udibu,REAL(R_obibu
     &,4))
      !}
C KPG_vlv.fgi(  76, 169):���������� �������,20KPG61CT001XQ01
      R_iribu = R8_efome
C KPG_sens.fgi( 129, 244):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_epibu,R_atibu,REAL(1,4
     &),
     & REAL(R_upibu,4),REAL(R_aribu,4),
     & REAL(R_apibu,4),REAL(R_umibu,4),I_usibu,
     & REAL(R_oribu,4),L_uribu,REAL(R_asibu,4),L_esibu,L_isibu
     &,R_eribu,
     & REAL(R_opibu,4),REAL(R_ipibu,4),L_osibu,REAL(R_iribu
     &,4))
      !}
C KPG_vlv.fgi(  20, 169):���������� �������,20KPG60CT001XQ01
      R_alibu = R8_ifome
C KPG_sens.fgi( 206, 236):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ufibu,R_omibu,REAL(1e
     &-3,4),
     & REAL(R_ikibu,4),REAL(R_okibu,4),
     & REAL(R_ofibu,4),REAL(R_ifibu,4),I_imibu,
     & REAL(R_elibu,4),L_ilibu,REAL(R_olibu,4),L_ulibu,L_amibu
     &,R_ukibu,
     & REAL(R_ekibu,4),REAL(R_akibu,4),L_emibu,REAL(R_alibu
     &,4))
      !}
C KPG_vlv.fgi(  48, 169):���������� �������,20KPG61CP001XQ01
      R_uvibu = R8_ofome
C KPG_sens.fgi( 206, 240):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_otibu,R_ibobu,REAL(1e
     &-3,4),
     & REAL(R_evibu,4),REAL(R_ivibu,4),
     & REAL(R_itibu,4),REAL(R_etibu,4),I_ebobu,
     & REAL(R_axibu,4),L_exibu,REAL(R_ixibu,4),L_oxibu,L_uxibu
     &,R_ovibu,
     & REAL(R_avibu,4),REAL(R_utibu,4),L_abobu,REAL(R_uvibu
     &,4))
      !}
C KPG_vlv.fgi(  76, 182):���������� �������,20KPG60CP001XQ01
      R_ixodu = R8_ufome
C KPG_sens.fgi( 361, 256):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_evodu,R_adudu,REAL(1000
     &,4),
     & REAL(R_uvodu,4),REAL(R_axodu,4),
     & REAL(R_avodu,4),REAL(R_utodu,4),I_ubudu,
     & REAL(R_oxodu,4),L_uxodu,REAL(R_abudu,4),L_ebudu,L_ibudu
     &,R_exodu,
     & REAL(R_ovodu,4),REAL(R_ivodu,4),L_obudu,REAL(R_ixodu
     &,4))
      !}
C KPG_vlv.fgi(  48, 182):���������� �������,20KPG52CF001XQ01
      R_opixu = R8_akome
C KPG_sens.fgi( 129, 248):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_imixu,R_esixu,REAL(1,4
     &),
     & REAL(R_apixu,4),REAL(R_epixu,4),
     & REAL(R_emixu,4),REAL(R_amixu,4),I_asixu,
     & REAL(R_upixu,4),L_arixu,REAL(R_erixu,4),L_irixu,L_orixu
     &,R_ipixu,
     & REAL(R_umixu,4),REAL(R_omixu,4),L_urixu,REAL(R_opixu
     &,4))
      !}
C KPG_vlv.fgi(  76, 195):���������� �������,20KPG31CT001XQ01
      R_avixu = R8_ekome
C KPG_sens.fgi( 206, 244):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_oxixu,4),REAL
     &(R_avixu,4),R_usixu,
     & R_uxixu,REAL(1e-6,4),REAL(R_itixu,4),
     & REAL(R_otixu,4),REAL(R_osixu,4),
     & REAL(R_isixu,4),I_ixixu,REAL(R_evixu,4),L_ivixu,
     & REAL(R_ovixu,4),L_uvixu,L_axixu,R_utixu,REAL(R_etixu
     &,4),REAL(R_atixu,4),L_exixu)
      !}
C KPG_vlv.fgi(  48, 195):���������� ������� ��������,20KPG21CP002XQ01
      R_isabad = R8_ikome
C KPG_sens.fgi( 129, 252):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_erabad,R_avabad,REAL(1
     &,4),
     & REAL(R_urabad,4),REAL(R_asabad,4),
     & REAL(R_arabad,4),REAL(R_upabad,4),I_utabad,
     & REAL(R_osabad,4),L_usabad,REAL(R_atabad,4),L_etabad
     &,L_itabad,R_esabad,
     & REAL(R_orabad,4),REAL(R_irabad,4),L_otabad,REAL(R_isabad
     &,4))
      !}
C KPG_vlv.fgi(  76, 208):���������� �������,20KPG41CT004XQ01
      R_udufad = R8_okome
C KPG_sens.fgi( 206, 248):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_ikufad,4),REAL
     &(R_udufad,4),R_obufad,
     & R_okufad,REAL(1e-6,4),REAL(R_edufad,4),
     & REAL(R_idufad,4),REAL(R_ibufad,4),
     & REAL(R_ebufad,4),I_ekufad,REAL(R_afufad,4),L_efufad
     &,
     & REAL(R_ifufad,4),L_ofufad,L_ufufad,R_odufad,REAL(R_adufad
     &,4),REAL(R_ubufad,4),L_akufad)
      !}
C KPG_vlv.fgi(  48, 208):���������� ������� ��������,20KPG41CP002XQ01
      R_uxabad = R8_ukome
C KPG_sens.fgi( 129, 256):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ovabad,R_idebad,REAL(1
     &,4),
     & REAL(R_exabad,4),REAL(R_ixabad,4),
     & REAL(R_ivabad,4),REAL(R_evabad,4),I_edebad,
     & REAL(R_abebad,4),L_ebebad,REAL(R_ibebad,4),L_obebad
     &,L_ubebad,R_oxabad,
     & REAL(R_axabad,4),REAL(R_uvabad,4),L_adebad,REAL(R_uxabad
     &,4))
      !}
C KPG_vlv.fgi(  76, 221):���������� �������,20KPG11CT011XQ01
      R_ubedad = R8_alome
C KPG_sens.fgi( 206, 252):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_ifedad,4),REAL
     &(R_ubedad,4),R_oxadad,
     & R_ofedad,REAL(1e-6,4),REAL(R_ebedad,4),
     & REAL(R_ibedad,4),REAL(R_ixadad,4),
     & REAL(R_exadad,4),I_efedad,REAL(R_adedad,4),L_ededad
     &,
     & REAL(R_idedad,4),L_odedad,L_udedad,R_obedad,REAL(R_abedad
     &,4),REAL(R_uxadad,4),L_afedad)
      !}
C KPG_vlv.fgi(  48, 221):���������� ������� ��������,20KPG41CP001XQ01
      L_(6)=R_obedad.lt.R0_al
C KPG_KPH_blk.fgi( 135, 405):���������� <
      R_ekebad = R8_elome
C KPG_sens.fgi( 361, 260):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_afebad,R_ulebad,REAL(1
     &,4),
     & REAL(R_ofebad,4),REAL(R_ufebad,4),
     & REAL(R_udebad,4),REAL(R_odebad,4),I_olebad,
     & REAL(R_ikebad,4),L_okebad,REAL(R_ukebad,4),L_alebad
     &,L_elebad,R_akebad,
     & REAL(R_ifebad,4),REAL(R_efebad,4),L_ilebad,REAL(R_ekebad
     &,4))
      !}
C KPG_vlv.fgi(  76, 234):���������� �������,20KPG11CF011XQ01
      R_oledad = R8_ilome
C KPG_sens.fgi( 361, 264):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ikedad,R_epedad,REAL(1
     &,4),
     & REAL(R_aledad,4),REAL(R_eledad,4),
     & REAL(R_ekedad,4),REAL(R_akedad,4),I_apedad,
     & REAL(R_uledad,4),L_amedad,REAL(R_emedad,4),L_imedad
     &,L_omedad,R_iledad,
     & REAL(R_ukedad,4),REAL(R_okedad,4),L_umedad,REAL(R_oledad
     &,4))
      !}
C KPG_vlv.fgi(  48, 234):���������� �������,20KPG41CF002XQ01
      R_akuxu = R8_olome
C KPG_sens.fgi( 361, 268):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_uduxu,R_oluxu,REAL(1,4
     &),
     & REAL(R_ifuxu,4),REAL(R_ofuxu,4),
     & REAL(R_oduxu,4),REAL(R_iduxu,4),I_iluxu,
     & REAL(R_ekuxu,4),L_ikuxu,REAL(R_okuxu,4),L_ukuxu,L_aluxu
     &,R_ufuxu,
     & REAL(R_efuxu,4),REAL(R_afuxu,4),L_eluxu,REAL(R_akuxu
     &,4))
      !}
C KPG_vlv.fgi(  20, 234):���������� �������,20KPG51CF001XQ01
      R_opebad = R8_ulome
C KPG_sens.fgi( 206, 256):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_esebad,4),REAL
     &(R_opebad,4),R_imebad,
     & R_isebad,REAL(1e-3,4),REAL(R_apebad,4),
     & REAL(R_epebad,4),REAL(R_emebad,4),
     & REAL(R_amebad,4),I_asebad,REAL(R_upebad,4),L_arebad
     &,
     & REAL(R_erebad,4),L_irebad,L_orebad,R_ipebad,REAL(R_umebad
     &,4),REAL(R_omebad,4),L_urebad)
      !}
C KPG_vlv.fgi(  76, 247):���������� ������� ��������,20KPG11CP012XQ01
      R_adefad = R8_amome
C KPG_sens.fgi( 361, 272):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_uxafad,R_ofefad,REAL(1
     &,4),
     & REAL(R_ibefad,4),REAL(R_obefad,4),
     & REAL(R_oxafad,4),REAL(R_ixafad,4),I_ifefad,
     & REAL(R_edefad,4),L_idefad,REAL(R_odefad,4),L_udefad
     &,L_afefad,R_ubefad,
     & REAL(R_ebefad,4),REAL(R_abefad,4),L_efefad,REAL(R_adefad
     &,4))
      !}
C KPG_vlv.fgi(  48, 247):���������� �������,20KPG41CF001XQ01
      R_ipuxu = R8_emome
C KPG_sens.fgi( 206, 260):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_asuxu,4),REAL
     &(R_ipuxu,4),R_emuxu,
     & R_esuxu,REAL(1e-6,4),REAL(R_umuxu,4),
     & REAL(R_apuxu,4),REAL(R_amuxu,4),
     & REAL(R_uluxu,4),I_uruxu,REAL(R_opuxu,4),L_upuxu,
     & REAL(R_aruxu,4),L_eruxu,L_iruxu,R_epuxu,REAL(R_omuxu
     &,4),REAL(R_imuxu,4),L_oruxu)
      !}
C KPG_vlv.fgi(  20, 247):���������� ������� ��������,20KPG11CP011XQ01
      R_ivebad = R8_imome
C KPG_sens.fgi( 206, 264):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_abibad,4),REAL
     &(R_ivebad,4),R_etebad,
     & R_ebibad,REAL(1e-6,4),REAL(R_utebad,4),
     & REAL(R_avebad,4),REAL(R_atebad,4),
     & REAL(R_usebad,4),I_uxebad,REAL(R_ovebad,4),L_uvebad
     &,
     & REAL(R_axebad,4),L_exebad,L_ixebad,R_evebad,REAL(R_otebad
     &,4),REAL(R_itebad,4),L_oxebad)
      !}
C KPG_vlv.fgi(  76, 260):���������� ������� ��������,20KPG41CP003XQ01
      R_evuxu = R8_omome
C KPG_sens.fgi( 129, 260):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_atuxu,R_uxuxu,REAL(1,4
     &),
     & REAL(R_otuxu,4),REAL(R_utuxu,4),
     & REAL(R_usuxu,4),REAL(R_osuxu,4),I_oxuxu,
     & REAL(R_ivuxu,4),L_ovuxu,REAL(R_uvuxu,4),L_axuxu,L_exuxu
     &,R_avuxu,
     & REAL(R_ituxu,4),REAL(R_etuxu,4),L_ixuxu,REAL(R_evuxu
     &,4))
      !}
C KPG_vlv.fgi(  20, 260):���������� �������,20KPG41CT003XQ01
      R_asedad = R8_umome
C KPG_sens.fgi( 129, 264):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_upedad,R_otedad,REAL(1
     &,4),
     & REAL(R_iredad,4),REAL(R_oredad,4),
     & REAL(R_opedad,4),REAL(R_ipedad,4),I_itedad,
     & REAL(R_esedad,4),L_isedad,REAL(R_osedad,4),L_usedad
     &,L_atedad,R_uredad,
     & REAL(R_eredad,4),REAL(R_aredad,4),L_etedad,REAL(R_asedad
     &,4))
      !}
C KPG_vlv.fgi(  48, 273):���������� �������,20KPG41CT002XQ01
      R_apome = R8_epome
C KPG_sens.fgi( 285, 264):��������
      R_omibad = R8_ipome
C KPG_sens.fgi( 285, 268):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ilibad,R_eribad,REAL(1
     &,4),
     & REAL(R_amibad,4),REAL(R_emibad,4),
     & REAL(R_elibad,4),REAL(R_alibad,4),I_aribad,
     & REAL(R_umibad,4),L_apibad,REAL(R_epibad,4),L_ipibad
     &,L_opibad,R_imibad,
     & REAL(R_ulibad,4),REAL(R_olibad,4),L_upibad,REAL(R_omibad
     &,4))
      !}
C KPG_vlv.fgi(  76, 286):���������� �������,20KPG11CQ011XQ01
      R_ivofad = R8_opome
C KPG_sens.fgi( 129, 268):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_etofad,R_abufad,REAL(1
     &,4),
     & REAL(R_utofad,4),REAL(R_avofad,4),
     & REAL(R_atofad,4),REAL(R_usofad,4),I_uxofad,
     & REAL(R_ovofad,4),L_uvofad,REAL(R_axofad,4),L_exofad
     &,L_ixofad,R_evofad,
     & REAL(R_otofad,4),REAL(R_itofad,4),L_oxofad,REAL(R_ivofad
     &,4))
      !}
C KPG_vlv.fgi(  48, 286):���������� �������,20KPG41CT001XQ01
      R_amabad = R8_upome
C KPG_sens.fgi( 361, 276):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ukabad,R_opabad,REAL(1
     &,4),
     & REAL(R_ilabad,4),REAL(R_olabad,4),
     & REAL(R_okabad,4),REAL(R_ikabad,4),I_ipabad,
     & REAL(R_emabad,4),L_imabad,REAL(R_omabad,4),L_umabad
     &,L_apabad,R_ulabad,
     & REAL(R_elabad,4),REAL(R_alabad,4),L_epabad,REAL(R_amabad
     &,4))
      !}
C KPG_vlv.fgi(  20, 286):���������� �������,20KPG41CF003XQ01
      R_imixo = R8_arome
C KPG_sens.fgi( 285, 272):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_elixo,R_arixo,REAL(1,4
     &),
     & REAL(R_ulixo,4),REAL(R_amixo,4),
     & REAL(R_alixo,4),REAL(R_ukixo,4),I_upixo,
     & REAL(R_omixo,4),L_umixo,REAL(R_apixo,4),L_epixo,L_ipixo
     &,R_emixo,
     & REAL(R_olixo,4),REAL(R_ilixo,4),L_opixo,REAL(R_imixo
     &,4))
      !}
C KPG_vlv.fgi(  76,  90):���������� �������,20KPG12CQ002XQ01
      R_uxexu = R8_erome
C KPG_sens.fgi( 129, 272):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ovexu,R_idixu,REAL(1,4
     &),
     & REAL(R_exexu,4),REAL(R_ixexu,4),
     & REAL(R_ivexu,4),REAL(R_evexu,4),I_edixu,
     & REAL(R_abixu,4),L_ebixu,REAL(R_ibixu,4),L_obixu,L_ubixu
     &,R_oxexu,
     & REAL(R_axexu,4),REAL(R_uvexu,4),L_adixu,REAL(R_uxexu
     &,4))
      !}
C KPG_vlv.fgi(  20, 221):���������� �������,20KPG21CT001XQ01
      R_ubabu = R8_irome
C KPG_sens.fgi( 206, 268):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_ifabu,4),REAL
     &(R_ubabu,4),R_oxuxo,
     & R_ofabu,REAL(1e-6,4),REAL(R_ebabu,4),
     & REAL(R_ibabu,4),REAL(R_ixuxo,4),
     & REAL(R_exuxo,4),I_efabu,REAL(R_adabu,4),L_edabu,
     & REAL(R_idabu,4),L_odabu,L_udabu,R_obabu,REAL(R_ababu
     &,4),REAL(R_uxuxo,4),L_afabu)
      !}
C KPG_vlv.fgi(  20, 143):���������� ������� ��������,20KPG21CP001XQ01
      R_usixo = R8_orome
C KPG_sens.fgi( 285, 276):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_orixo,R_ivixo,REAL(1,4
     &),
     & REAL(R_esixo,4),REAL(R_isixo,4),
     & REAL(R_irixo,4),REAL(R_erixo,4),I_evixo,
     & REAL(R_atixo,4),L_etixo,REAL(R_itixo,4),L_otixo,L_utixo
     &,R_osixo,
     & REAL(R_asixo,4),REAL(R_urixo,4),L_avixo,REAL(R_usixo
     &,4))
      !}
C KPG_vlv.fgi(  76, 103):���������� �������,20KPG12CQ001XQ01
      R_amuvo = R8_urome
C KPG_sens.fgi( 206, 272):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_opuvo,4),REAL
     &(R_amuvo,4),R_ukuvo,
     & R_upuvo,REAL(1e-6,4),REAL(R_iluvo,4),
     & REAL(R_oluvo,4),REAL(R_okuvo,4),
     & REAL(R_ikuvo,4),I_ipuvo,REAL(R_emuvo,4),L_imuvo,
     & REAL(R_omuvo,4),L_umuvo,L_apuvo,R_uluvo,REAL(R_eluvo
     &,4),REAL(R_aluvo,4),L_epuvo)
      !}
C KPG_vlv.fgi(  20,  77):���������� ������� ��������,20KPG12CP002XQ01
      R_itovo = R8_asome
C KPG_sens.fgi( 206, 276):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_axovo,4),REAL
     &(R_itovo,4),R_esovo,
     & R_exovo,REAL(1e-6,4),REAL(R_usovo,4),
     & REAL(R_atovo,4),REAL(R_asovo,4),
     & REAL(R_urovo,4),I_uvovo,REAL(R_otovo,4),L_utovo,
     & REAL(R_avovo,4),L_evovo,L_ivovo,R_etovo,REAL(R_osovo
     &,4),REAL(R_isovo,4),L_ovovo)
      !}
C KPG_vlv.fgi(  20,  64):���������� ������� ��������,20KPG12CP001XQ01
      R_udoxu = R8_esome
C KPG_sens.fgi( 129, 276):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_oboxu,R_ikoxu,REAL(1,4
     &),
     & REAL(R_edoxu,4),REAL(R_idoxu,4),
     & REAL(R_iboxu,4),REAL(R_eboxu,4),I_ekoxu,
     & REAL(R_afoxu,4),L_efoxu,REAL(R_ifoxu,4),L_ofoxu,L_ufoxu
     &,R_odoxu,
     & REAL(R_adoxu,4),REAL(R_uboxu,4),L_akoxu,REAL(R_udoxu
     &,4))
      !}
C KPG_vlv.fgi(  20, 195):���������� �������,20KPG11CT001XQ01
      L_(5)=R_odoxu.gt.R0_o
C KPG_KPH_blk.fgi(  46, 157):���������� >
      L_urodad = L_(5).OR.L_(4)
C KPG_KPH_blk.fgi(  55, 156):���
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_utodad,4),L_edudad,L_idudad
     &,R8_irodad,C30_usodad,
     & L_alafad,L_ulafad,L_utafad,I_ebudad,I_ibudad,R_etodad
     &,R_atodad,
     & R_imodad,REAL(R_umodad,4),R_opodad,
     & REAL(R_arodad,4),R_apodad,REAL(R_ipodad,4),I_ixodad
     &,
     & I_obudad,I_abudad,I_uxodad,L_erodad,L_ubudad,L_efudad
     &,L_upodad,
     & L_epodad,L_esodad,L_asodad,L_odudad,L_omodad,L_osodad
     &,
     & L_ufudad,L_adudad,L_itodad,L_otodad,REAL(R8_irufad
     &,8),REAL(1.0,4),R8_ixufad,
     & L_urodad,L_orodad,L_ifudad,R_exodad,REAL(R_isodad,4
     &),L_ofudad,L_akudad,
     & L_avodad,L_evodad,L_ivodad,L_uvodad,L_axodad,L_ovodad
     &)
      !}

      if(L_axodad.or.L_uvodad.or.L_ovodad.or.L_ivodad.or.L_evodad.or.L_a
     &vodad) then      
                  I_oxodad = ior(z'000000FF',z'04000000')
     &                            
                else
                   I_oxodad = z'40000000'
      endif
C KPG_vlv.fgi( 103, 256):���� ���������� �������� ��������,20KPG11AA113
      R_ekixu = R8_isome
C KPG_sens.fgi(  52, 268):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_afixu,R_ulixu,REAL(1000
     &,4),
     & REAL(R_ofixu,4),REAL(R_ufixu,4),
     & REAL(R_udixu,4),REAL(R_odixu,4),I_olixu,
     & REAL(R_ikixu,4),L_okixu,REAL(R_ukixu,4),L_alixu,L_elixu
     &,R_akixu,
     & REAL(R_ifixu,4),REAL(R_efixu,4),L_ilixu,REAL(R_ekixu
     &,4))
      !}
C KPG_vlv.fgi(  20, 182):���������� �������,20KPG31CL003XQ01
      R_isexu = R8_osome
C KPG_sens.fgi(  52, 272):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_erexu,R_avexu,REAL(1000
     &,4),
     & REAL(R_urexu,4),REAL(R_asexu,4),
     & REAL(R_arexu,4),REAL(R_upexu,4),I_utexu,
     & REAL(R_osexu,4),L_usexu,REAL(R_atexu,4),L_etexu,L_itexu
     &,R_esexu,
     & REAL(R_orexu,4),REAL(R_irexu,4),L_otexu,REAL(R_isexu
     &,4))
      !}
C KPG_vlv.fgi(  20, 208):���������� �������,20KPG21CL003XQ01
      R_aroxo = R8_usome
C KPG_sens.fgi(  52, 276):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_umoxo,R_osoxo,REAL(1000
     &,4),
     & REAL(R_ipoxo,4),REAL(R_opoxo,4),
     & REAL(R_omoxo,4),REAL(R_imoxo,4),I_isoxo,
     & REAL(R_eroxo,4),L_iroxo,REAL(R_oroxo,4),L_uroxo,L_asoxo
     &,R_upoxo,
     & REAL(R_epoxo,4),REAL(R_apoxo,4),L_esoxo,REAL(R_aroxo
     &,4))
      !}
C KPG_vlv.fgi(  48,  90):���������� �������,20KPG11CL003XQ01
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_utepi,4),L_edipi,L_idipi
     &,R8_arepi,C30_osepi,
     & L_asepi,L_usepi,L_udipi,I_ebipi,I_ibipi,R_etepi,R_atepi
     &,
     & R_amepi,REAL(R_imepi,4),R_epepi,
     & REAL(R_opepi,4),R_omepi,REAL(R_apepi,4),I_ixepi,
     & I_obipi,I_abipi,I_uxepi,L_upepi,L_ubipi,L_ifipi,L_ipepi
     &,
     & L_umepi,L_urepi,L_orepi,L_odipi,L_emepi,L_isepi,
     & L_akipi,L_adipi,L_itepi,L_otepi,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_irepi,L_erepi,L_ofipi,R_exepi,REAL(R_esepi,4),L_ufipi
     &,L_ekipi,
     & L_avepi,L_evepi,L_ivepi,L_uvepi,L_axepi,L_ovepi)
      !}

      if(L_axepi.or.L_uvepi.or.L_ovepi.or.L_ivepi.or.L_evepi.or.L_avepi
     &) then      
                  I_oxepi = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_oxepi = z'40000000'
      endif
C KPJ_vlv.fgi(  15, 229):���� ���������� �������� ��������,20KPJ70AA109
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_ufasi,4),L_epasi,L_ipasi
     &,R8_ibasi,C30_udasi,
     & L_isero,L_etero,L_efiro,I_emasi,I_imasi,R_efasi,R_afasi
     &,
     & R_ivuri,REAL(R_uvuri,4),R_oxuri,
     & REAL(R_abasi,4),R_axuri,REAL(R_ixuri,4),I_ilasi,
     & I_omasi,I_amasi,I_ulasi,L_ebasi,L_umasi,L_erasi,L_uxuri
     &,
     & L_exuri,L_edasi,L_adasi,L_opasi,L_ovuri,L_odasi,
     & L_urasi,L_apasi,L_ifasi,L_ofasi,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_ubasi,L_obasi,L_irasi,R_elasi,REAL(R_idasi,4),L_orasi
     &,L_asasi,
     & L_akasi,L_ekasi,L_ikasi,L_ukasi,L_alasi,L_okasi)
      !}

      if(L_alasi.or.L_ukasi.or.L_okasi.or.L_ikasi.or.L_ekasi.or.L_akasi
     &) then      
                  I_olasi = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_olasi = z'40000000'
      endif
C KPH_vlv.fgi( 303,  59):���� ���������� �������� ��������,20KPH51AA111
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_obesi,4),L_alesi,L_elesi
     &,R8_evasi,C30_oxasi,
     & L_isero,L_etero,L_efiro,I_akesi,I_ekesi,R_abesi,R_uxasi
     &,
     & R_esasi,REAL(R_osasi,4),R_itasi,
     & REAL(R_utasi,4),R_usasi,REAL(R_etasi,4),I_efesi,
     & I_ikesi,I_ufesi,I_ofesi,L_avasi,L_okesi,L_amesi,L_otasi
     &,
     & L_atasi,L_axasi,L_uvasi,L_ilesi,L_isasi,L_ixasi,
     & L_omesi,L_ukesi,L_ebesi,L_ibesi,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_ovasi,L_ivasi,L_emesi,R_afesi,REAL(R_exasi,4),L_imesi
     &,L_umesi,
     & L_ubesi,L_adesi,L_edesi,L_odesi,L_udesi,L_idesi)
      !}

      if(L_udesi.or.L_odesi.or.L_idesi.or.L_edesi.or.L_adesi.or.L_ubesi
     &) then      
                  I_ifesi = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ifesi = z'40000000'
      endif
C KPH_vlv.fgi( 318,  83):���� ���������� �������� ��������,20KPH50AA111
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_ivesi,4),L_udisi,L_afisi
     &,R8_asesi,C30_itesi,
     & L_oboto,L_idoto,L_ipoto,I_ubisi,I_adisi,R_utesi,R_otesi
     &,
     & R_apesi,REAL(R_ipesi,4),R_eresi,
     & REAL(R_oresi,4),R_opesi,REAL(R_aresi,4),I_abisi,
     & I_edisi,I_obisi,I_ibisi,L_uresi,L_idisi,L_ufisi,L_iresi
     &,
     & L_upesi,L_usesi,L_osesi,L_efisi,L_epesi,L_etesi,
     & L_ikisi,L_odisi,L_avesi,L_evesi,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_isesi,L_esesi,L_akisi,R_uxesi,REAL(R_atesi,4),L_ekisi
     &,L_okisi,
     & L_ovesi,L_uvesi,L_axesi,L_ixesi,L_oxesi,L_exesi)
      !}

      if(L_oxesi.or.L_ixesi.or.L_exesi.or.L_axesi.or.L_uvesi.or.L_ovesi
     &) then      
                  I_ebisi = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ebisi = z'40000000'
      endif
C KPH_vlv.fgi( 303,  83):���� ���������� �������� ��������,20KPH10AA111
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_esisi,4),L_oxisi,L_uxisi
     &,R8_umisi,C30_erisi,
     & L_oxubo,L_ibado,L_imado,I_ovisi,I_uvisi,R_orisi,R_irisi
     &,
     & R_ukisi,REAL(R_elisi,4),R_amisi,
     & REAL(R_imisi,4),R_ilisi,REAL(R_ulisi,4),I_utisi,
     & I_axisi,I_ivisi,I_evisi,L_omisi,L_exisi,L_obosi,L_emisi
     &,
     & L_olisi,L_opisi,L_ipisi,L_abosi,L_alisi,L_arisi,
     & L_edosi,L_ixisi,L_urisi,L_asisi,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_episi,L_apisi,L_ubosi,R_otisi,REAL(R_upisi,4),L_adosi
     &,L_idosi,
     & L_isisi,L_osisi,L_usisi,L_etisi,L_itisi,L_atisi)
      !}

      if(L_itisi.or.L_etisi.or.L_atisi.or.L_usisi.or.L_osisi.or.L_isisi
     &) then      
                  I_avisi = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_avisi = z'40000000'
      endif
C KPH_vlv.fgi( 318, 107):���� ���������� �������� ��������,20KPH30AA111
      Call KPG_KPH_1(ext_deltat)
      End
      Subroutine KPG_KPH_1(ext_deltat)
      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      Include 'KPG_KPH.fh'

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      !{
      Call DSHUTTER_MAN(deltat,REAL(R_aposi,4),L_itosi,L_otosi
     &,R8_okosi,C30_amosi,
     & L_oxubo,L_ibado,L_imado,I_isosi,I_ososi,R_imosi,R_emosi
     &,
     & R_odosi,REAL(R_afosi,4),R_ufosi,
     & REAL(R_ekosi,4),R_efosi,REAL(R_ofosi,4),I_orosi,
     & I_usosi,I_esosi,I_asosi,L_ikosi,L_atosi,L_ivosi,L_akosi
     &,
     & L_ifosi,L_ilosi,L_elosi,L_utosi,L_udosi,L_ulosi,
     & L_axosi,L_etosi,L_omosi,L_umosi,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_alosi,L_ukosi,L_ovosi,R_irosi,REAL(R_olosi,4),L_uvosi
     &,L_exosi,
     & L_eposi,L_iposi,L_oposi,L_arosi,L_erosi,L_uposi)
      !}

      if(L_erosi.or.L_arosi.or.L_uposi.or.L_oposi.or.L_iposi.or.L_eposi
     &) then      
                  I_urosi = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_urosi = z'40000000'
      endif
C KPH_vlv.fgi( 303, 107):���� ���������� �������� ��������,20KPH31AA101
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_ukusi,4),L_erusi,L_irusi
     &,R8_idusi,C30_ufusi,
     & L_oxubo,L_ibado,L_imado,I_epusi,I_ipusi,R_ekusi,R_akusi
     &,
     & R_ixosi,REAL(R_uxosi,4),R_obusi,
     & REAL(R_adusi,4),R_abusi,REAL(R_ibusi,4),I_imusi,
     & I_opusi,I_apusi,I_umusi,L_edusi,L_upusi,L_esusi,L_ubusi
     &,
     & L_ebusi,L_efusi,L_afusi,L_orusi,L_oxosi,L_ofusi,
     & L_ususi,L_arusi,L_ikusi,L_okusi,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_udusi,L_odusi,L_isusi,R_emusi,REAL(R_ifusi,4),L_osusi
     &,L_atusi,
     & L_alusi,L_elusi,L_ilusi,L_ulusi,L_amusi,L_olusi)
      !}

      if(L_amusi.or.L_ulusi.or.L_olusi.or.L_ilusi.or.L_elusi.or.L_alusi
     &) then      
                  I_omusi = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_omusi = z'40000000'
      endif
C KPH_vlv.fgi( 318, 131):���� ���������� �������� ��������,20KPH31AA102
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_odati,4),L_amati,L_emati
     &,R8_exusi,C30_obati,
     & L_oxubo,L_ibado,L_imado,I_alati,I_elati,R_adati,R_ubati
     &,
     & R_etusi,REAL(R_otusi,4),R_ivusi,
     & REAL(R_uvusi,4),R_utusi,REAL(R_evusi,4),I_ekati,
     & I_ilati,I_ukati,I_okati,L_axusi,L_olati,L_apati,L_ovusi
     &,
     & L_avusi,L_abati,L_uxusi,L_imati,L_itusi,L_ibati,
     & L_opati,L_ulati,L_edati,L_idati,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_oxusi,L_ixusi,L_epati,R_akati,REAL(R_ebati,4),L_ipati
     &,L_upati,
     & L_udati,L_afati,L_efati,L_ofati,L_ufati,L_ifati)
      !}

      if(L_ufati.or.L_ofati.or.L_ifati.or.L_efati.or.L_afati.or.L_udati
     &) then      
                  I_ikati = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ikati = z'40000000'
      endif
C KPH_vlv.fgi( 303, 131):���� ���������� �������� ��������,20KPH30AA107
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_ixati,4),L_ufeti,L_aketi
     &,R8_atati,C30_ivati,
     & L_oxubo,L_ibado,L_imado,I_udeti,I_afeti,R_uvati,R_ovati
     &,
     & R_arati,REAL(R_irati,4),R_esati,
     & REAL(R_osati,4),R_orati,REAL(R_asati,4),I_adeti,
     & I_efeti,I_odeti,I_ideti,L_usati,L_ifeti,L_uketi,L_isati
     &,
     & L_urati,L_utati,L_otati,L_eketi,L_erati,L_evati,
     & L_ileti,L_ofeti,L_axati,L_exati,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_itati,L_etati,L_aleti,R_ubeti,REAL(R_avati,4),L_eleti
     &,L_oleti,
     & L_oxati,L_uxati,L_abeti,L_ibeti,L_obeti,L_ebeti)
      !}

      if(L_obeti.or.L_ibeti.or.L_ebeti.or.L_abeti.or.L_uxati.or.L_oxati
     &) then      
                  I_edeti = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_edeti = z'40000000'
      endif
C KPH_vlv.fgi( 318, 155):���� ���������� �������� ��������,20KPH31AA103
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_eteti,4),L_obiti,L_ubiti
     &,R8_upeti,C30_eseti,
     & L_ikodo,L_elodo,L_etodo,I_oxeti,I_uxeti,R_oseti,R_iseti
     &,
     & R_uleti,REAL(R_emeti,4),R_apeti,
     & REAL(R_ipeti,4),R_imeti,REAL(R_umeti,4),I_uveti,
     & I_abiti,I_ixeti,I_exeti,L_opeti,L_ebiti,L_oditi,L_epeti
     &,
     & L_ometi,L_oreti,L_ireti,L_aditi,L_ameti,L_aseti,
     & L_efiti,L_ibiti,L_useti,L_ateti,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_ereti,L_areti,L_uditi,R_oveti,REAL(R_ureti,4),L_afiti
     &,L_ifiti,
     & L_iteti,L_oteti,L_uteti,L_eveti,L_iveti,L_aveti)
      !}

      if(L_iveti.or.L_eveti.or.L_aveti.or.L_uteti.or.L_oteti.or.L_iteti
     &) then      
                  I_axeti = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_axeti = z'40000000'
      endif
C KPH_vlv.fgi( 303, 155):���� ���������� �������� ��������,20KPH41AA101
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_ariti,4),L_iviti,L_oviti
     &,R8_oliti,C30_apiti,
     & L_ikodo,L_elodo,L_etodo,I_ititi,I_otiti,R_ipiti,R_epiti
     &,
     & R_ofiti,REAL(R_akiti,4),R_ukiti,
     & REAL(R_eliti,4),R_ekiti,REAL(R_okiti,4),I_ositi,
     & I_utiti,I_etiti,I_atiti,L_iliti,L_aviti,L_ixiti,L_aliti
     &,
     & L_ikiti,L_imiti,L_emiti,L_uviti,L_ufiti,L_umiti,
     & L_aboti,L_eviti,L_opiti,L_upiti,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_amiti,L_uliti,L_oxiti,R_isiti,REAL(R_omiti,4),L_uxiti
     &,L_eboti,
     & L_eriti,L_iriti,L_oriti,L_asiti,L_esiti,L_uriti)
      !}

      if(L_esiti.or.L_asiti.or.L_uriti.or.L_oriti.or.L_iriti.or.L_eriti
     &) then      
                  I_usiti = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_usiti = z'40000000'
      endif
C KPH_vlv.fgi( 318, 179):���� ���������� �������� ��������,20KPH41AA102
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_uloti,4),L_esoti,L_isoti
     &,R8_ifoti,C30_ukoti,
     & L_oxubo,L_ibado,L_imado,I_eroti,I_iroti,R_eloti,R_aloti
     &,
     & R_iboti,REAL(R_uboti,4),R_odoti,
     & REAL(R_afoti,4),R_adoti,REAL(R_idoti,4),I_ipoti,
     & I_oroti,I_aroti,I_upoti,L_efoti,L_uroti,L_etoti,L_udoti
     &,
     & L_edoti,L_ekoti,L_akoti,L_osoti,L_oboti,L_okoti,
     & L_utoti,L_asoti,L_iloti,L_oloti,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_ufoti,L_ofoti,L_itoti,R_epoti,REAL(R_ikoti,4),L_ototi
     &,L_avoti,
     & L_amoti,L_emoti,L_imoti,L_umoti,L_apoti,L_omoti)
      !}

      if(L_apoti.or.L_umoti.or.L_omoti.or.L_imoti.or.L_emoti.or.L_amoti
     &) then      
                  I_opoti = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_opoti = z'40000000'
      endif
C KPH_vlv.fgi( 303, 179):���� ���������� �������� ��������,20KPH30AA105
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_ofuti,4),L_aputi,L_eputi
     &,R8_ebuti,C30_oduti,
     & L_oboto,L_idoto,L_ipoto,I_amuti,I_emuti,R_afuti,R_uduti
     &,
     & R_evoti,REAL(R_ovoti,4),R_ixoti,
     & REAL(R_uxoti,4),R_uvoti,REAL(R_exoti,4),I_eluti,
     & I_imuti,I_uluti,I_oluti,L_abuti,L_omuti,L_aruti,L_oxoti
     &,
     & L_axoti,L_aduti,L_ubuti,L_iputi,L_ivoti,L_iduti,
     & L_oruti,L_umuti,L_efuti,L_ifuti,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_obuti,L_ibuti,L_eruti,R_aluti,REAL(R_eduti,4),L_iruti
     &,L_uruti,
     & L_ufuti,L_akuti,L_ekuti,L_okuti,L_ukuti,L_ikuti)
      !}

      if(L_ukuti.or.L_okuti.or.L_ikuti.or.L_ekuti.or.L_akuti.or.L_ufuti
     &) then      
                  I_iluti = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_iluti = z'40000000'
      endif
C KPH_vlv.fgi( 288,  35):���� ���������� �������� ��������,20KPH11AA113
      R_edexi = R_uduti
C KPH_sens.fgi(  58, 128):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_abexi,R_ufexi,REAL(1,4
     &),
     & REAL(R_obexi,4),REAL(R_ubexi,4),
     & REAL(R_uxaxi,4),REAL(R_oxaxi,4),I_ofexi,
     & REAL(R_idexi,4),L_odexi,REAL(R_udexi,4),L_afexi,L_efexi
     &,R_adexi,
     & REAL(R_ibexi,4),REAL(R_ebexi,4),L_ifexi,REAL(R_edexi
     &,4))
      !}
C KPH_vlv.fgi( 104,  54):���������� �������,20KPH11AA113XQ01
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_ibavi,4),L_ukavi,L_alavi
     &,R8_avuti,C30_ixuti,
     & L_oboto,L_idoto,L_ipoto,I_ufavi,I_akavi,R_uxuti,R_oxuti
     &,
     & R_asuti,REAL(R_isuti,4),R_etuti,
     & REAL(R_otuti,4),R_osuti,REAL(R_atuti,4),I_afavi,
     & I_ekavi,I_ofavi,I_ifavi,L_ututi,L_ikavi,L_ulavi,L_ituti
     &,
     & L_usuti,L_uvuti,L_ovuti,L_elavi,L_esuti,L_exuti,
     & L_imavi,L_okavi,L_abavi,L_ebavi,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_ivuti,L_evuti,L_amavi,R_udavi,REAL(R_axuti,4),L_emavi
     &,L_omavi,
     & L_obavi,L_ubavi,L_adavi,L_idavi,L_odavi,L_edavi)
      !}

      if(L_odavi.or.L_idavi.or.L_edavi.or.L_adavi.or.L_ubavi.or.L_obavi
     &) then      
                  I_efavi = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_efavi = z'40000000'
      endif
C KPH_vlv.fgi( 273,  35):���� ���������� �������� ��������,20KPH11AA111
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_evavi,4),L_odevi,L_udevi
     &,R8_uravi,C30_etavi,
     & L_oboto,L_idoto,L_ipoto,I_obevi,I_ubevi,R_otavi,R_itavi
     &,
     & R_umavi,REAL(R_epavi,4),R_aravi,
     & REAL(R_iravi,4),R_ipavi,REAL(R_upavi,4),I_uxavi,
     & I_adevi,I_ibevi,I_ebevi,L_oravi,L_edevi,L_ofevi,L_eravi
     &,
     & L_opavi,L_osavi,L_isavi,L_afevi,L_apavi,L_atavi,
     & L_ekevi,L_idevi,L_utavi,L_avavi,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_esavi,L_asavi,L_ufevi,R_oxavi,REAL(R_usavi,4),L_akevi
     &,L_ikevi,
     & L_ivavi,L_ovavi,L_uvavi,L_exavi,L_ixavi,L_axavi)
      !}

      if(L_ixavi.or.L_exavi.or.L_axavi.or.L_uvavi.or.L_ovavi.or.L_ivavi
     &) then      
                  I_abevi = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_abevi = z'40000000'
      endif
C KPH_vlv.fgi( 258,  35):���� ���������� �������� ��������,20KPH11AA112
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_ivuxi,4),L_udabo,L_afabo
     &,R8_asuxi,C30_ituxi,
     & L_iraso,L_esaso,L_edeso,I_ubabo,I_adabo,R_utuxi,R_otuxi
     &,
     & R_apuxi,REAL(R_ipuxi,4),R_eruxi,
     & REAL(R_oruxi,4),R_opuxi,REAL(R_aruxi,4),I_ababo,
     & I_edabo,I_obabo,I_ibabo,L_uruxi,L_idabo,L_ufabo,L_iruxi
     &,
     & L_upuxi,L_usuxi,L_osuxi,L_efabo,L_epuxi,L_etuxi,
     & L_ikabo,L_odabo,L_avuxi,L_evuxi,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_isuxi,L_esuxi,L_akabo,R_uxuxi,REAL(R_atuxi,4),L_ekabo
     &,L_okabo,
     & L_ovuxi,L_uvuxi,L_axuxi,L_ixuxi,L_oxuxi,L_exuxi)
      !}

      if(L_oxuxi.or.L_ixuxi.or.L_exuxi.or.L_axuxi.or.L_uvuxi.or.L_ovuxi
     &) then      
                  I_ebabo = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ebabo = z'40000000'
      endif
C KPH_vlv.fgi( 243,  35):���� ���������� �������� ��������,20KPH20AA101
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_oribo,4),L_axibo,L_exibo
     &,R8_emibo,C30_opibo,
     & L_oxubo,L_ibado,L_imado,I_avibo,I_evibo,R_aribo,R_upibo
     &,
     & R_ekibo,REAL(R_okibo,4),R_ilibo,
     & REAL(R_ulibo,4),R_ukibo,REAL(R_elibo,4),I_etibo,
     & I_ivibo,I_utibo,I_otibo,L_amibo,L_ovibo,L_abobo,L_olibo
     &,
     & L_alibo,L_apibo,L_umibo,L_ixibo,L_ikibo,L_ipibo,
     & L_obobo,L_uvibo,L_eribo,L_iribo,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_omibo,L_imibo,L_ebobo,R_atibo,REAL(R_epibo,4),L_ibobo
     &,L_ubobo,
     & L_uribo,L_asibo,L_esibo,L_osibo,L_usibo,L_isibo)
      !}

      if(L_usibo.or.L_osibo.or.L_isibo.or.L_esibo.or.L_asibo.or.L_uribo
     &) then      
                  I_itibo = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_itibo = z'40000000'
      endif
C KPH_vlv.fgi( 193,  35):���� ���������� �������� ��������,20KPH30AA104
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_imobo,4),L_usobo,L_atobo
     &,R8_akobo,C30_ilobo,
     & L_oxubo,L_ibado,L_imado,I_urobo,I_asobo,R_ulobo,R_olobo
     &,
     & R_adobo,REAL(R_idobo,4),R_efobo,
     & REAL(R_ofobo,4),R_odobo,REAL(R_afobo,4),I_arobo,
     & I_esobo,I_orobo,I_irobo,L_ufobo,L_isobo,L_utobo,L_ifobo
     &,
     & L_udobo,L_ukobo,L_okobo,L_etobo,L_edobo,L_elobo,
     & L_ivobo,L_osobo,L_amobo,L_emobo,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_ikobo,L_ekobo,L_avobo,R_upobo,REAL(R_alobo,4),L_evobo
     &,L_ovobo,
     & L_omobo,L_umobo,L_apobo,L_ipobo,L_opobo,L_epobo)
      !}

      if(L_opobo.or.L_ipobo.or.L_epobo.or.L_apobo.or.L_umobo.or.L_omobo
     &) then      
                  I_erobo = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_erobo = z'40000000'
      endif
C KPH_vlv.fgi( 288,  59):���� ���������� �������� ��������,20KPH30AA103
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_ixado,4),L_ufedo,L_akedo
     &,R8_atado,C30_ivado,
     & L_ikodo,L_elodo,L_etodo,I_udedo,I_afedo,R_uvado,R_ovado
     &,
     & R_arado,REAL(R_irado,4),R_esado,
     & REAL(R_osado,4),R_orado,REAL(R_asado,4),I_adedo,
     & I_efedo,I_odedo,I_idedo,L_usado,L_ifedo,L_ukedo,L_isado
     &,
     & L_urado,L_utado,L_otado,L_ekedo,L_erado,L_evado,
     & L_iledo,L_ofedo,L_axado,L_exado,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_itado,L_etado,L_aledo,R_ubedo,REAL(R_avado,4),L_eledo
     &,L_oledo,
     & L_oxado,L_uxado,L_abedo,L_ibedo,L_obedo,L_ebedo)
      !}

      if(L_obedo.or.L_ibedo.or.L_ebedo.or.L_abedo.or.L_uxado.or.L_oxado
     &) then      
                  I_ededo = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ededo = z'40000000'
      endif
C KPH_vlv.fgi( 243,  59):���� ���������� �������� ��������,20KPH40AA104
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_arido,4),L_ivido,L_ovido
     &,R8_olido,C30_apido,
     & L_ikodo,L_elodo,L_etodo,I_itido,I_otido,R_ipido,R_epido
     &,
     & R_ofido,REAL(R_akido,4),R_ukido,
     & REAL(R_elido,4),R_ekido,REAL(R_okido,4),I_osido,
     & I_utido,I_etido,I_atido,L_ilido,L_avido,L_ixido,L_alido
     &,
     & L_ikido,L_imido,L_emido,L_uvido,L_ufido,L_umido,
     & L_abodo,L_evido,L_opido,L_upido,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_amido,L_ulido,L_oxido,R_isido,REAL(R_omido,4),L_uxido
     &,L_ebodo,
     & L_erido,L_irido,L_orido,L_asido,L_esido,L_urido)
      !}

      if(L_esido.or.L_asido.or.L_urido.or.L_orido.or.L_irido.or.L_erido
     &) then      
                  I_usido = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_usido = z'40000000'
      endif
C KPH_vlv.fgi( 210,  59):���� ���������� �������� ��������,20KPH40AA102
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_emodo,4),L_osodo,L_usodo
     &,R8_ifodo,C30_alodo,
     & L_ikodo,L_elodo,L_etodo,I_orodo,I_urodo,R_olodo,R_ilodo
     &,
     & R_ibodo,REAL(R_ubodo,4),R_ododo,
     & REAL(R_afodo,4),R_adodo,REAL(R_idodo,4),I_upodo,
     & I_asodo,I_irodo,I_erodo,L_efodo,L_esodo,L_utodo,L_udodo
     &,
     & L_edodo,L_ekodo,L_akodo,L_atodo,L_obodo,L_ukodo,
     & L_ivodo,L_isodo,L_ulodo,L_amodo,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_ufodo,L_ofodo,L_avodo,R_opodo,REAL(R_okodo,4),L_evodo
     &,L_ovodo,
     & L_imodo,L_omodo,L_umodo,L_epodo,L_ipodo,L_apodo)
      !}

      if(L_ipodo.or.L_epodo.or.L_apodo.or.L_umodo.or.L_omodo.or.L_imodo
     &) then      
                  I_arodo = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_arodo = z'40000000'
      endif
C KPH_vlv.fgi( 193,  59):���� ���������� �������� ��������,20KPH40AA101
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_ekudo,4),L_opudo,L_upudo
     &,R8_ubudo,C30_efudo,
     & L_iraso,L_esaso,L_edeso,I_omudo,I_umudo,R_ofudo,R_ifudo
     &,
     & R_uvodo,REAL(R_exodo,4),R_abudo,
     & REAL(R_ibudo,4),R_ixodo,REAL(R_uxodo,4),I_uludo,
     & I_apudo,I_imudo,I_emudo,L_obudo,L_epudo,L_orudo,L_ebudo
     &,
     & L_oxodo,L_odudo,L_idudo,L_arudo,L_axodo,L_afudo,
     & L_esudo,L_ipudo,L_ufudo,L_akudo,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_edudo,L_adudo,L_urudo,R_oludo,REAL(R_ududo,4),L_asudo
     &,L_isudo,
     & L_ikudo,L_okudo,L_ukudo,L_eludo,L_iludo,L_aludo)
      !}

      if(L_iludo.or.L_eludo.or.L_aludo.or.L_ukudo.or.L_okudo.or.L_ikudo
     &) then      
                  I_amudo = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_amudo = z'40000000'
      endif
C KPH_vlv.fgi( 288,  83):���� ���������� �������� ��������,20KPH20AA102
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_adafo,4),L_ilafo,L_olafo
     &,R8_ovudo,C30_abafo,
     & L_iraso,L_esaso,L_edeso,I_ikafo,I_okafo,R_ibafo,R_ebafo
     &,
     & R_osudo,REAL(R_atudo,4),R_utudo,
     & REAL(R_evudo,4),R_etudo,REAL(R_otudo,4),I_ofafo,
     & I_ukafo,I_ekafo,I_akafo,L_ivudo,L_alafo,L_imafo,L_avudo
     &,
     & L_itudo,L_ixudo,L_exudo,L_ulafo,L_usudo,L_uxudo,
     & L_apafo,L_elafo,L_obafo,L_ubafo,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_axudo,L_uvudo,L_omafo,R_ifafo,REAL(R_oxudo,4),L_umafo
     &,L_epafo,
     & L_edafo,L_idafo,L_odafo,L_afafo,L_efafo,L_udafo)
      !}

      if(L_efafo.or.L_afafo.or.L_udafo.or.L_odafo.or.L_idafo.or.L_edafo
     &) then      
                  I_ufafo = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ufafo = z'40000000'
      endif
C KPH_vlv.fgi( 273,  83):���� ���������� �������� ��������,20KPH20AA103
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_itefo,4),L_ubifo,L_adifo
     &,R8_arefo,C30_isefo,
     & L_iraso,L_esaso,L_edeso,I_uxefo,I_abifo,R_usefo,R_osefo
     &,
     & R_amefo,REAL(R_imefo,4),R_epefo,
     & REAL(R_opefo,4),R_omefo,REAL(R_apefo,4),I_axefo,
     & I_ebifo,I_oxefo,I_ixefo,L_upefo,L_ibifo,L_udifo,L_ipefo
     &,
     & L_umefo,L_urefo,L_orefo,L_edifo,L_emefo,L_esefo,
     & L_ififo,L_obifo,L_atefo,L_etefo,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_irefo,L_erefo,L_afifo,R_uvefo,REAL(R_asefo,4),L_efifo
     &,L_ofifo,
     & L_otefo,L_utefo,L_avefo,L_ivefo,L_ovefo,L_evefo)
      !}

      if(L_ovefo.or.L_ivefo.or.L_evefo.or.L_avefo.or.L_utefo.or.L_otefo
     &) then      
                  I_exefo = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_exefo = z'40000000'
      endif
C KPH_vlv.fgi( 258,  83):���� ���������� �������� ��������,20KPH20AA201
      R_ilifo = R_osefo
C KPH_sens.fgi(  58, 132):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ekifo,R_apifo,REAL(1,4
     &),
     & REAL(R_ukifo,4),REAL(R_alifo,4),
     & REAL(R_akifo,4),REAL(R_ufifo,4),I_umifo,
     & REAL(R_olifo,4),L_ulifo,REAL(R_amifo,4),L_emifo,L_imifo
     &,R_elifo,
     & REAL(R_okifo,4),REAL(R_ikifo,4),L_omifo,REAL(R_ilifo
     &,4))
      !}
C KPH_vlv.fgi(  75,  80):���������� �������,20KPH20AA201XQ01
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_ifofo,4),L_umofo,L_apofo
     &,R8_abofo,C30_idofo,
     & L_oboto,L_idoto,L_ipoto,I_ulofo,I_amofo,R_udofo,R_odofo
     &,
     & R_avifo,REAL(R_ivifo,4),R_exifo,
     & REAL(R_oxifo,4),R_ovifo,REAL(R_axifo,4),I_alofo,
     & I_emofo,I_olofo,I_ilofo,L_uxifo,L_imofo,L_upofo,L_ixifo
     &,
     & L_uvifo,L_ubofo,L_obofo,L_epofo,L_evifo,L_edofo,
     & L_irofo,L_omofo,L_afofo,L_efofo,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_ibofo,L_ebofo,L_arofo,R_ukofo,REAL(R_adofo,4),L_erofo
     &,L_orofo,
     & L_ofofo,L_ufofo,L_akofo,L_ikofo,L_okofo,L_ekofo)
      !}

      if(L_okofo.or.L_ikofo.or.L_ekofo.or.L_akofo.or.L_ufofo.or.L_ofofo
     &) then      
                  I_elofo = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_elofo = z'40000000'
      endif
C KPH_vlv.fgi( 243,  83):���� ���������� �������� ��������,20KPH14AA108
      !{
      Call DAT_ANA_HANDLER(deltat,R_ufuko,R_omuko,REAL(1,4
     &),
     & REAL(R_ikuko,4),REAL(R_okuko,4),
     & REAL(R_ofuko,4),REAL(R_ifuko,4),I_imuko,
     & REAL(R_eluko,4),L_iluko,REAL(R_oluko,4),L_uluko,L_amuko
     &,R_ukuko,
     & REAL(R_ekuko,4),REAL(R_akuko,4),L_emuko,REAL(R_aluko
     &,4))
      !}
C KPH_vlv.fgi(  46,  93):���������� �������,20KPH51CF001XQ01
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_okelo,4),L_arelo,L_erelo
     &,R8_edelo,C30_ofelo,
     & L_oboto,L_idoto,L_ipoto,I_apelo,I_epelo,R_akelo,R_ufelo
     &,
     & R_exalo,REAL(R_oxalo,4),R_ibelo,
     & REAL(R_ubelo,4),R_uxalo,REAL(R_ebelo,4),I_emelo,
     & I_ipelo,I_umelo,I_omelo,L_adelo,L_opelo,L_aselo,L_obelo
     &,
     & L_abelo,L_afelo,L_udelo,L_irelo,L_ixalo,L_ifelo,
     & L_oselo,L_upelo,L_ekelo,L_ikelo,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_odelo,L_idelo,L_eselo,R_amelo,REAL(R_efelo,4),L_iselo
     &,L_uselo,
     & L_ukelo,L_alelo,L_elelo,L_olelo,L_ulelo,L_ilelo)
      !}

      if(L_ulelo.or.L_olelo.or.L_ilelo.or.L_elelo.or.L_alelo.or.L_ukelo
     &) then      
                  I_imelo = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_imelo = z'40000000'
      endif
C KPH_vlv.fgi( 227,  83):���� ���������� �������� ��������,20KPH14AA107
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_idilo,4),L_ulilo,L_amilo
     &,R8_axelo,C30_ibilo,
     & L_oboto,L_idoto,L_ipoto,I_ukilo,I_alilo,R_ubilo,R_obilo
     &,
     & R_atelo,REAL(R_itelo,4),R_evelo,
     & REAL(R_ovelo,4),R_otelo,REAL(R_avelo,4),I_akilo,
     & I_elilo,I_okilo,I_ikilo,L_uvelo,L_ililo,L_umilo,L_ivelo
     &,
     & L_utelo,L_uxelo,L_oxelo,L_emilo,L_etelo,L_ebilo,
     & L_ipilo,L_olilo,L_adilo,L_edilo,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_ixelo,L_exelo,L_apilo,R_ufilo,REAL(R_abilo,4),L_epilo
     &,L_opilo,
     & L_odilo,L_udilo,L_afilo,L_ifilo,L_ofilo,L_efilo)
      !}

      if(L_ofilo.or.L_ifilo.or.L_efilo.or.L_afilo.or.L_udilo.or.L_odilo
     &) then      
                  I_ekilo = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ekilo = z'40000000'
      endif
C KPH_vlv.fgi( 210,  83):���� ���������� �������� ��������,20KPH14AA106
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_exilo,4),L_ofolo,L_ufolo
     &,R8_usilo,C30_evilo,
     & L_oboto,L_idoto,L_ipoto,I_odolo,I_udolo,R_ovilo,R_ivilo
     &,
     & R_upilo,REAL(R_erilo,4),R_asilo,
     & REAL(R_isilo,4),R_irilo,REAL(R_urilo,4),I_ubolo,
     & I_afolo,I_idolo,I_edolo,L_osilo,L_efolo,L_okolo,L_esilo
     &,
     & L_orilo,L_otilo,L_itilo,L_akolo,L_arilo,L_avilo,
     & L_elolo,L_ifolo,L_uvilo,L_axilo,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_etilo,L_atilo,L_ukolo,R_obolo,REAL(R_utilo,4),L_alolo
     &,L_ilolo,
     & L_ixilo,L_oxilo,L_uxilo,L_ebolo,L_ibolo,L_abolo)
      !}

      if(L_ibolo.or.L_ebolo.or.L_abolo.or.L_uxilo.or.L_oxilo.or.L_ixilo
     &) then      
                  I_adolo = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_adolo = z'40000000'
      endif
C KPH_vlv.fgi( 193,  83):���� ���������� �������� ��������,20KPH14AA105
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_atolo,4),L_ibulo,L_obulo
     &,R8_opolo,C30_asolo,
     & L_oboto,L_idoto,L_ipoto,I_ixolo,I_oxolo,R_isolo,R_esolo
     &,
     & R_ololo,REAL(R_amolo,4),R_umolo,
     & REAL(R_epolo,4),R_emolo,REAL(R_omolo,4),I_ovolo,
     & I_uxolo,I_exolo,I_axolo,L_ipolo,L_abulo,L_idulo,L_apolo
     &,
     & L_imolo,L_irolo,L_erolo,L_ubulo,L_ulolo,L_urolo,
     & L_afulo,L_ebulo,L_osolo,L_usolo,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_arolo,L_upolo,L_odulo,R_ivolo,REAL(R_orolo,4),L_udulo
     &,L_efulo,
     & L_etolo,L_itolo,L_otolo,L_avolo,L_evolo,L_utolo)
      !}

      if(L_evolo.or.L_avolo.or.L_utolo.or.L_otolo.or.L_itolo.or.L_etolo
     &) then      
                  I_uvolo = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_uvolo = z'40000000'
      endif
C KPH_vlv.fgi( 288, 107):���� ���������� �������� ��������,20KPH14AA104
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_upulo,4),L_evulo,L_ivulo
     &,R8_ilulo,C30_umulo,
     & L_oboto,L_idoto,L_ipoto,I_etulo,I_itulo,R_epulo,R_apulo
     &,
     & R_ifulo,REAL(R_ufulo,4),R_okulo,
     & REAL(R_alulo,4),R_akulo,REAL(R_ikulo,4),I_isulo,
     & I_otulo,I_atulo,I_usulo,L_elulo,L_utulo,L_exulo,L_ukulo
     &,
     & L_ekulo,L_emulo,L_amulo,L_ovulo,L_ofulo,L_omulo,
     & L_uxulo,L_avulo,L_ipulo,L_opulo,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_ululo,L_olulo,L_ixulo,R_esulo,REAL(R_imulo,4),L_oxulo
     &,L_abamo,
     & L_arulo,L_erulo,L_irulo,L_urulo,L_asulo,L_orulo)
      !}

      if(L_asulo.or.L_urulo.or.L_orulo.or.L_irulo.or.L_erulo.or.L_arulo
     &) then      
                  I_osulo = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_osulo = z'40000000'
      endif
C KPH_vlv.fgi( 273, 107):���� ���������� �������� ��������,20KPH14AA103
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_olamo,4),L_asamo,L_esamo
     &,R8_efamo,C30_okamo,
     & L_oboto,L_idoto,L_ipoto,I_aramo,I_eramo,R_alamo,R_ukamo
     &,
     & R_ebamo,REAL(R_obamo,4),R_idamo,
     & REAL(R_udamo,4),R_ubamo,REAL(R_edamo,4),I_epamo,
     & I_iramo,I_upamo,I_opamo,L_afamo,L_oramo,L_atamo,L_odamo
     &,
     & L_adamo,L_akamo,L_ufamo,L_isamo,L_ibamo,L_ikamo,
     & L_otamo,L_uramo,L_elamo,L_ilamo,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_ofamo,L_ifamo,L_etamo,R_apamo,REAL(R_ekamo,4),L_itamo
     &,L_utamo,
     & L_ulamo,L_amamo,L_emamo,L_omamo,L_umamo,L_imamo)
      !}

      if(L_umamo.or.L_omamo.or.L_imamo.or.L_emamo.or.L_amamo.or.L_ulamo
     &) then      
                  I_ipamo = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ipamo = z'40000000'
      endif
C KPH_vlv.fgi( 258, 107):���� ���������� �������� ��������,20KPH14AA102
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_ifemo,4),L_umemo,L_apemo
     &,R8_abemo,C30_idemo,
     & L_oboto,L_idoto,L_ipoto,I_ulemo,I_amemo,R_udemo,R_odemo
     &,
     & R_avamo,REAL(R_ivamo,4),R_examo,
     & REAL(R_oxamo,4),R_ovamo,REAL(R_axamo,4),I_alemo,
     & I_ememo,I_olemo,I_ilemo,L_uxamo,L_imemo,L_upemo,L_ixamo
     &,
     & L_uvamo,L_ubemo,L_obemo,L_epemo,L_evamo,L_edemo,
     & L_iremo,L_omemo,L_afemo,L_efemo,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_ibemo,L_ebemo,L_aremo,R_ukemo,REAL(R_ademo,4),L_eremo
     &,L_oremo,
     & L_ofemo,L_ufemo,L_akemo,L_ikemo,L_okemo,L_ekemo)
      !}

      if(L_okemo.or.L_ikemo.or.L_ekemo.or.L_akemo.or.L_ufemo.or.L_ofemo
     &) then      
                  I_elemo = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_elemo = z'40000000'
      endif
C KPH_vlv.fgi( 243, 107):���� ���������� �������� ��������,20KPH14AA101
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_ebimo,4),L_okimo,L_ukimo
     &,R8_utemo,C30_exemo,
     & L_oboto,L_idoto,L_ipoto,I_ofimo,I_ufimo,R_oxemo,R_ixemo
     &,
     & R_uremo,REAL(R_esemo,4),R_atemo,
     & REAL(R_itemo,4),R_isemo,REAL(R_usemo,4),I_udimo,
     & I_akimo,I_ifimo,I_efimo,L_otemo,L_ekimo,L_olimo,L_etemo
     &,
     & L_osemo,L_ovemo,L_ivemo,L_alimo,L_asemo,L_axemo,
     & L_emimo,L_ikimo,L_uxemo,L_abimo,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_evemo,L_avemo,L_ulimo,R_odimo,REAL(R_uvemo,4),L_amimo
     &,L_imimo,
     & L_ibimo,L_obimo,L_ubimo,L_edimo,L_idimo,L_adimo)
      !}

      if(L_idimo.or.L_edimo.or.L_adimo.or.L_ubimo.or.L_obimo.or.L_ibimo
     &) then      
                  I_afimo = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_afimo = z'40000000'
      endif
C KPH_vlv.fgi( 227, 107):���� ���������� �������� ��������,20KPH11AA101
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_avimo,4),L_idomo,L_odomo
     &,R8_orimo,C30_atimo,
     & L_oboto,L_idoto,L_ipoto,I_ibomo,I_obomo,R_itimo,R_etimo
     &,
     & R_omimo,REAL(R_apimo,4),R_upimo,
     & REAL(R_erimo,4),R_epimo,REAL(R_opimo,4),I_oximo,
     & I_ubomo,I_ebomo,I_abomo,L_irimo,L_adomo,L_ifomo,L_arimo
     &,
     & L_ipimo,L_isimo,L_esimo,L_udomo,L_umimo,L_usimo,
     & L_akomo,L_edomo,L_otimo,L_utimo,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_asimo,L_urimo,L_ofomo,R_iximo,REAL(R_osimo,4),L_ufomo
     &,L_ekomo,
     & L_evimo,L_ivimo,L_ovimo,L_aximo,L_eximo,L_uvimo)
      !}

      if(L_eximo.or.L_aximo.or.L_uvimo.or.L_ovimo.or.L_ivimo.or.L_evimo
     &) then      
                  I_uximo = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_uximo = z'40000000'
      endif
C KPH_vlv.fgi( 210, 107):���� ���������� �������� ��������,20KPH15AA102
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_uromo,4),L_exomo,L_ixomo
     &,R8_imomo,C30_upomo,
     & L_oboto,L_idoto,L_ipoto,I_evomo,I_ivomo,R_eromo,R_aromo
     &,
     & R_ikomo,REAL(R_ukomo,4),R_olomo,
     & REAL(R_amomo,4),R_alomo,REAL(R_ilomo,4),I_itomo,
     & I_ovomo,I_avomo,I_utomo,L_emomo,L_uvomo,L_ebumo,L_ulomo
     &,
     & L_elomo,L_epomo,L_apomo,L_oxomo,L_okomo,L_opomo,
     & L_ubumo,L_axomo,L_iromo,L_oromo,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_umomo,L_omomo,L_ibumo,R_etomo,REAL(R_ipomo,4),L_obumo
     &,L_adumo,
     & L_asomo,L_esomo,L_isomo,L_usomo,L_atomo,L_osomo)
      !}

      if(L_atomo.or.L_usomo.or.L_osomo.or.L_isomo.or.L_esomo.or.L_asomo
     &) then      
                  I_otomo = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_otomo = z'40000000'
      endif
C KPH_vlv.fgi( 193, 107):���� ���������� �������� ��������,20KPH15AA101
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_omumo,4),L_atumo,L_etumo
     &,R8_ekumo,C30_olumo,
     & L_oboto,L_idoto,L_ipoto,I_asumo,I_esumo,R_amumo,R_ulumo
     &,
     & R_edumo,REAL(R_odumo,4),R_ifumo,
     & REAL(R_ufumo,4),R_udumo,REAL(R_efumo,4),I_erumo,
     & I_isumo,I_urumo,I_orumo,L_akumo,L_osumo,L_avumo,L_ofumo
     &,
     & L_afumo,L_alumo,L_ukumo,L_itumo,L_idumo,L_ilumo,
     & L_ovumo,L_usumo,L_emumo,L_imumo,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_okumo,L_ikumo,L_evumo,R_arumo,REAL(R_elumo,4),L_ivumo
     &,L_uvumo,
     & L_umumo,L_apumo,L_epumo,L_opumo,L_upumo,L_ipumo)
      !}

      if(L_upumo.or.L_opumo.or.L_ipumo.or.L_epumo.or.L_apumo.or.L_umumo
     &) then      
                  I_irumo = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_irumo = z'40000000'
      endif
C KPH_vlv.fgi( 288, 131):���� ���������� �������� ��������,20KPH15AA107
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_ikapo,4),L_upapo,L_arapo
     &,R8_adapo,C30_ifapo,
     & L_oboto,L_idoto,L_ipoto,I_umapo,I_apapo,R_ufapo,R_ofapo
     &,
     & R_axumo,REAL(R_ixumo,4),R_ebapo,
     & REAL(R_obapo,4),R_oxumo,REAL(R_abapo,4),I_amapo,
     & I_epapo,I_omapo,I_imapo,L_ubapo,L_ipapo,L_urapo,L_ibapo
     &,
     & L_uxumo,L_udapo,L_odapo,L_erapo,L_exumo,L_efapo,
     & L_isapo,L_opapo,L_akapo,L_ekapo,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_idapo,L_edapo,L_asapo,R_ulapo,REAL(R_afapo,4),L_esapo
     &,L_osapo,
     & L_okapo,L_ukapo,L_alapo,L_ilapo,L_olapo,L_elapo)
      !}

      if(L_olapo.or.L_ilapo.or.L_elapo.or.L_alapo.or.L_ukapo.or.L_okapo
     &) then      
                  I_emapo = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_emapo = z'40000000'
      endif
C KPH_vlv.fgi( 288, 155):���� ���������� �������� ��������,20KPH15AA106
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_edepo,4),L_olepo,L_ulepo
     &,R8_uvapo,C30_ebepo,
     & L_oboto,L_idoto,L_ipoto,I_okepo,I_ukepo,R_obepo,R_ibepo
     &,
     & R_usapo,REAL(R_etapo,4),R_avapo,
     & REAL(R_ivapo,4),R_itapo,REAL(R_utapo,4),I_ufepo,
     & I_alepo,I_ikepo,I_ekepo,L_ovapo,L_elepo,L_omepo,L_evapo
     &,
     & L_otapo,L_oxapo,L_ixapo,L_amepo,L_atapo,L_abepo,
     & L_epepo,L_ilepo,L_ubepo,L_adepo,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_exapo,L_axapo,L_umepo,R_ofepo,REAL(R_uxapo,4),L_apepo
     &,L_ipepo,
     & L_idepo,L_odepo,L_udepo,L_efepo,L_ifepo,L_afepo)
      !}

      if(L_ifepo.or.L_efepo.or.L_afepo.or.L_udepo.or.L_odepo.or.L_idepo
     &) then      
                  I_akepo = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_akepo = z'40000000'
      endif
C KPH_vlv.fgi( 288, 179):���� ���������� �������� ��������,20KPH15AA105
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_axepo,4),L_ifipo,L_ofipo
     &,R8_osepo,C30_avepo,
     & L_oboto,L_idoto,L_ipoto,I_idipo,I_odipo,R_ivepo,R_evepo
     &,
     & R_opepo,REAL(R_arepo,4),R_urepo,
     & REAL(R_esepo,4),R_erepo,REAL(R_orepo,4),I_obipo,
     & I_udipo,I_edipo,I_adipo,L_isepo,L_afipo,L_ikipo,L_asepo
     &,
     & L_irepo,L_itepo,L_etepo,L_ufipo,L_upepo,L_utepo,
     & L_alipo,L_efipo,L_ovepo,L_uvepo,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_atepo,L_usepo,L_okipo,R_ibipo,REAL(R_otepo,4),L_ukipo
     &,L_elipo,
     & L_exepo,L_ixepo,L_oxepo,L_abipo,L_ebipo,L_uxepo)
      !}

      if(L_ebipo.or.L_abipo.or.L_uxepo.or.L_oxepo.or.L_ixepo.or.L_exepo
     &) then      
                  I_ubipo = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ubipo = z'40000000'
      endif
C KPH_vlv.fgi( 273, 131):���� ���������� �������� ��������,20KPH15AA104
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_usipo,4),L_ebopo,L_ibopo
     &,R8_ipipo,C30_uripo,
     & L_oboto,L_idoto,L_ipoto,I_exipo,I_ixipo,R_esipo,R_asipo
     &,
     & R_ilipo,REAL(R_ulipo,4),R_omipo,
     & REAL(R_apipo,4),R_amipo,REAL(R_imipo,4),I_ivipo,
     & I_oxipo,I_axipo,I_uvipo,L_epipo,L_uxipo,L_edopo,L_umipo
     &,
     & L_emipo,L_eripo,L_aripo,L_obopo,L_olipo,L_oripo,
     & L_udopo,L_abopo,L_isipo,L_osipo,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_upipo,L_opipo,L_idopo,R_evipo,REAL(R_iripo,4),L_odopo
     &,L_afopo,
     & L_atipo,L_etipo,L_itipo,L_utipo,L_avipo,L_otipo)
      !}

      if(L_avipo.or.L_utipo.or.L_otipo.or.L_itipo.or.L_etipo.or.L_atipo
     &) then      
                  I_ovipo = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ovipo = z'40000000'
      endif
C KPH_vlv.fgi( 273, 155):���� ���������� �������� ��������,20KPH11AA202
      R_ofalo = R_asipo
C KPH_sens.fgi(  58, 140):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_idalo,R_elalo,REAL(1,4
     &),
     & REAL(R_afalo,4),REAL(R_efalo,4),
     & REAL(R_edalo,4),REAL(R_adalo,4),I_alalo,
     & REAL(R_ufalo,4),L_akalo,REAL(R_ekalo,4),L_ikalo,L_okalo
     &,R_ifalo,
     & REAL(R_udalo,4),REAL(R_odalo,4),L_ukalo,REAL(R_ofalo
     &,4))
      !}
C KPH_vlv.fgi(  18, 106):���������� �������,20KPH11AA202XQ01
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_opopo,4),L_avopo,L_evopo
     &,R8_elopo,C30_omopo,
     & L_oboto,L_idoto,L_ipoto,I_atopo,I_etopo,R_apopo,R_umopo
     &,
     & R_efopo,REAL(R_ofopo,4),R_ikopo,
     & REAL(R_ukopo,4),R_ufopo,REAL(R_ekopo,4),I_esopo,
     & I_itopo,I_usopo,I_osopo,L_alopo,L_otopo,L_axopo,L_okopo
     &,
     & L_akopo,L_amopo,L_ulopo,L_ivopo,L_ifopo,L_imopo,
     & L_oxopo,L_utopo,L_epopo,L_ipopo,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_olopo,L_ilopo,L_exopo,R_asopo,REAL(R_emopo,4),L_ixopo
     &,L_uxopo,
     & L_upopo,L_aropo,L_eropo,L_oropo,L_uropo,L_iropo)
      !}

      if(L_uropo.or.L_oropo.or.L_iropo.or.L_eropo.or.L_aropo.or.L_upopo
     &) then      
                  I_isopo = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_isopo = z'40000000'
      endif
C KPH_vlv.fgi( 273, 179):���� ���������� �������� ��������,20KPH11AA201
      R_obuko = R_umopo
C KPH_sens.fgi(  58, 136):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ixoko,R_efuko,REAL(1,4
     &),
     & REAL(R_abuko,4),REAL(R_ebuko,4),
     & REAL(R_exoko,4),REAL(R_axoko,4),I_afuko,
     & REAL(R_ubuko,4),L_aduko,REAL(R_eduko,4),L_iduko,L_oduko
     &,R_ibuko,
     & REAL(R_uxoko,4),REAL(R_oxoko,4),L_uduko,REAL(R_obuko
     &,4))
      !}
C KPH_vlv.fgi(  18,  80):���������� �������,20KPH11AA201XQ01
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_ilupo,4),L_urupo,L_asupo
     &,R8_afupo,C30_ikupo,
     & L_oboto,L_idoto,L_ipoto,I_upupo,I_arupo,R_ukupo,R_okupo
     &,
     & R_abupo,REAL(R_ibupo,4),R_edupo,
     & REAL(R_odupo,4),R_obupo,REAL(R_adupo,4),I_apupo,
     & I_erupo,I_opupo,I_ipupo,L_udupo,L_irupo,L_usupo,L_idupo
     &,
     & L_ubupo,L_ufupo,L_ofupo,L_esupo,L_ebupo,L_ekupo,
     & L_itupo,L_orupo,L_alupo,L_elupo,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_ifupo,L_efupo,L_atupo,R_umupo,REAL(R_akupo,4),L_etupo
     &,L_otupo,
     & L_olupo,L_ulupo,L_amupo,L_imupo,L_omupo,L_emupo)
      !}

      if(L_omupo.or.L_imupo.or.L_emupo.or.L_amupo.or.L_ulupo.or.L_olupo
     &) then      
                  I_epupo = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_epupo = z'40000000'
      endif
C KPH_vlv.fgi( 258, 131):���� ���������� �������� ��������,20KPH11AA102
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_efaro,4),L_omaro,L_umaro
     &,R8_uxupo,C30_edaro,
     & L_isero,L_etero,L_efiro,I_olaro,I_ularo,R_odaro,R_idaro
     &,
     & R_utupo,REAL(R_evupo,4),R_axupo,
     & REAL(R_ixupo,4),R_ivupo,REAL(R_uvupo,4),I_ukaro,
     & I_amaro,I_ilaro,I_elaro,L_oxupo,L_emaro,L_oparo,L_exupo
     &,
     & L_ovupo,L_obaro,L_ibaro,L_aparo,L_avupo,L_adaro,
     & L_eraro,L_imaro,L_udaro,L_afaro,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_ebaro,L_abaro,L_uparo,R_okaro,REAL(R_ubaro,4),L_araro
     &,L_iraro,
     & L_ifaro,L_ofaro,L_ufaro,L_ekaro,L_ikaro,L_akaro)
      !}

      if(L_ikaro.or.L_ekaro.or.L_akaro.or.L_ufaro.or.L_ofaro.or.L_ifaro
     &) then      
                  I_alaro = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_alaro = z'40000000'
      endif
C KPH_vlv.fgi( 243, 131):���� ���������� �������� ��������,20KPH51AA103
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_abero,4),L_ikero,L_okero
     &,R8_otaro,C30_axaro,
     & L_isero,L_etero,L_efiro,I_ifero,I_ofero,R_ixaro,R_exaro
     &,
     & R_oraro,REAL(R_asaro,4),R_usaro,
     & REAL(R_etaro,4),R_esaro,REAL(R_osaro,4),I_odero,
     & I_ufero,I_efero,I_afero,L_itaro,L_akero,L_ilero,L_ataro
     &,
     & L_isaro,L_ivaro,L_evaro,L_ukero,L_uraro,L_uvaro,
     & L_amero,L_ekero,L_oxaro,L_uxaro,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_avaro,L_utaro,L_olero,R_idero,REAL(R_ovaro,4),L_ulero
     &,L_emero,
     & L_ebero,L_ibero,L_obero,L_adero,L_edero,L_ubero)
      !}

      if(L_edero.or.L_adero.or.L_ubero.or.L_obero.or.L_ibero.or.L_ebero
     &) then      
                  I_udero = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_udero = z'40000000'
      endif
C KPH_vlv.fgi( 227, 131):���� ���������� �������� ��������,20KPH51AA104
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_evero,4),L_odiro,L_udiro
     &,R8_irero,C30_atero,
     & L_isero,L_etero,L_efiro,I_obiro,I_ubiro,R_otero,R_itero
     &,
     & R_imero,REAL(R_umero,4),R_opero,
     & REAL(R_arero,4),R_apero,REAL(R_ipero,4),I_uxero,
     & I_adiro,I_ibiro,I_ebiro,L_erero,L_ediro,L_ufiro,L_upero
     &,
     & L_epero,L_esero,L_asero,L_afiro,L_omero,L_usero,
     & L_ikiro,L_idiro,L_utero,L_avero,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_urero,L_orero,L_akiro,R_oxero,REAL(R_osero,4),L_ekiro
     &,L_okiro,
     & L_ivero,L_overo,L_uvero,L_exero,L_ixero,L_axero)
      !}

      if(L_ixero.or.L_exero.or.L_axero.or.L_uvero.or.L_overo.or.L_ivero
     &) then      
                  I_abiro = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_abiro = z'40000000'
      endif
C KPH_vlv.fgi( 210, 131):���� ���������� �������� ��������,20KPH51AA102
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_akoro,4),L_iporo,L_oporo
     &,R8_oboro,C30_aforo,
     & L_oboto,L_idoto,L_ipoto,I_imoro,I_omoro,R_iforo,R_eforo
     &,
     & R_oviro,REAL(R_axiro,4),R_uxiro,
     & REAL(R_eboro,4),R_exiro,REAL(R_oxiro,4),I_oloro,
     & I_umoro,I_emoro,I_amoro,L_iboro,L_aporo,L_iroro,L_aboro
     &,
     & L_ixiro,L_idoro,L_edoro,L_uporo,L_uviro,L_udoro,
     & L_asoro,L_eporo,L_oforo,L_uforo,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_adoro,L_uboro,L_ororo,R_iloro,REAL(R_odoro,4),L_uroro
     &,L_esoro,
     & L_ekoro,L_ikoro,L_okoro,L_aloro,L_eloro,L_ukoro)
      !}

      if(L_eloro.or.L_aloro.or.L_ukoro.or.L_okoro.or.L_ikoro.or.L_ekoro
     &) then      
                  I_uloro = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_uloro = z'40000000'
      endif
C KPH_vlv.fgi( 193, 131):���� ���������� �������� ��������,20KPH10AA102
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_uburo,4),L_eluro,L_iluro
     &,R8_ivoro,C30_uxoro,
     & L_oboto,L_idoto,L_ipoto,I_ekuro,I_ikuro,R_eburo,R_aburo
     &,
     & R_isoro,REAL(R_usoro,4),R_otoro,
     & REAL(R_avoro,4),R_atoro,REAL(R_itoro,4),I_ifuro,
     & I_okuro,I_akuro,I_ufuro,L_evoro,L_ukuro,L_emuro,L_utoro
     &,
     & L_etoro,L_exoro,L_axoro,L_oluro,L_osoro,L_oxoro,
     & L_umuro,L_aluro,L_iburo,L_oburo,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_uvoro,L_ovoro,L_imuro,R_efuro,REAL(R_ixoro,4),L_omuro
     &,L_apuro,
     & L_aduro,L_eduro,L_iduro,L_uduro,L_afuro,L_oduro)
      !}

      if(L_afuro.or.L_uduro.or.L_oduro.or.L_iduro.or.L_eduro.or.L_aduro
     &) then      
                  I_ofuro = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ofuro = z'40000000'
      endif
C KPH_vlv.fgi( 258, 155):���� ���������� �������� ��������,20KPH10AA201
      R_aduto = R_aburo
C KPH_sens.fgi(  58, 148):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_uxoto,R_ofuto,REAL(1,4
     &),
     & REAL(R_ibuto,4),REAL(R_obuto,4),
     & REAL(R_oxoto,4),REAL(R_ixoto,4),I_ifuto,
     & REAL(R_eduto,4),L_iduto,REAL(R_oduto,4),L_uduto,L_afuto
     &,R_ubuto,
     & REAL(R_ebuto,4),REAL(R_abuto,4),L_efuto,REAL(R_aduto
     &,4))
      !}
C KPH_vlv.fgi(  46, 184):���������� �������,20KPH10AA201XQ01
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_etaso,4),L_obeso,L_ubeso
     &,R8_ipaso,C30_asaso,
     & L_iraso,L_esaso,L_edeso,I_oxaso,I_uxaso,R_osaso,R_isaso
     &,
     & R_ilaso,REAL(R_ulaso,4),R_omaso,
     & REAL(R_apaso,4),R_amaso,REAL(R_imaso,4),I_uvaso,
     & I_abeso,I_ixaso,I_exaso,L_epaso,L_ebeso,L_udeso,L_umaso
     &,
     & L_emaso,L_eraso,L_araso,L_adeso,L_olaso,L_uraso,
     & L_ifeso,L_ibeso,L_usaso,L_ataso,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_upaso,L_opaso,L_afeso,R_ovaso,REAL(R_oraso,4),L_efeso
     &,L_ofeso,
     & L_itaso,L_otaso,L_utaso,L_evaso,L_ivaso,L_avaso)
      !}

      if(L_ivaso.or.L_evaso.or.L_avaso.or.L_utaso.or.L_otaso.or.L_itaso
     &) then      
                  I_axaso = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_axaso = z'40000000'
      endif
C KPH_vlv.fgi( 243, 155):���� ���������� �������� ��������,20KPH20AA111
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_afiso,4),L_imiso,L_omiso
     &,R8_oxeso,C30_adiso,
     & L_oboto,L_idoto,L_ipoto,I_iliso,I_oliso,R_idiso,R_ediso
     &,
     & R_oteso,REAL(R_aveso,4),R_uveso,
     & REAL(R_exeso,4),R_eveso,REAL(R_oveso,4),I_okiso,
     & I_uliso,I_eliso,I_aliso,L_ixeso,L_amiso,L_ipiso,L_axeso
     &,
     & L_iveso,L_ibiso,L_ebiso,L_umiso,L_uteso,L_ubiso,
     & L_ariso,L_emiso,L_odiso,L_udiso,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_abiso,L_uxeso,L_opiso,R_ikiso,REAL(R_obiso,4),L_upiso
     &,L_eriso,
     & L_efiso,L_ifiso,L_ofiso,L_akiso,L_ekiso,L_ufiso)
      !}

      if(L_ekiso.or.L_akiso.or.L_ufiso.or.L_ofiso.or.L_ifiso.or.L_efiso
     &) then      
                  I_ukiso = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ukiso = z'40000000'
      endif
C KPH_vlv.fgi( 227, 155):���� ���������� �������� ��������,20KPH10AA202
      R_exuro = R_ediso
C KPH_sens.fgi(  58, 144):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_avuro,R_ubaso,REAL(1,4
     &),
     & REAL(R_ovuro,4),REAL(R_uvuro,4),
     & REAL(R_uturo,4),REAL(R_oturo,4),I_obaso,
     & REAL(R_ixuro,4),L_oxuro,REAL(R_uxuro,4),L_abaso,L_ebaso
     &,R_axuro,
     & REAL(R_ivuro,4),REAL(R_evuro,4),L_ibaso,REAL(R_exuro
     &,4))
      !}
C KPH_vlv.fgi(  46, 171):���������� �������,20KPH10AA202XQ01
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_uxiso,4),L_ekoso,L_ikoso
     &,R8_itiso,C30_uviso,
     & L_oboto,L_idoto,L_ipoto,I_efoso,I_ifoso,R_exiso,R_axiso
     &,
     & R_iriso,REAL(R_uriso,4),R_osiso,
     & REAL(R_atiso,4),R_asiso,REAL(R_isiso,4),I_idoso,
     & I_ofoso,I_afoso,I_udoso,L_etiso,L_ufoso,L_eloso,L_usiso
     &,
     & L_esiso,L_eviso,L_aviso,L_okoso,L_oriso,L_oviso,
     & L_uloso,L_akoso,L_ixiso,L_oxiso,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_utiso,L_otiso,L_iloso,R_edoso,REAL(R_iviso,4),L_oloso
     &,L_amoso,
     & L_aboso,L_eboso,L_iboso,L_uboso,L_adoso,L_oboso)
      !}

      if(L_adoso.or.L_uboso.or.L_oboso.or.L_iboso.or.L_eboso.or.L_aboso
     &) then      
                  I_odoso = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_odoso = z'40000000'
      endif
C KPH_vlv.fgi( 210, 155):���� ���������� �������� ��������,20KPH10AA104
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_otoso,4),L_aduso,L_eduso
     &,R8_eroso,C30_ososo,
     & L_oboto,L_idoto,L_ipoto,I_abuso,I_ebuso,R_atoso,R_usoso
     &,
     & R_emoso,REAL(R_omoso,4),R_iposo,
     & REAL(R_uposo,4),R_umoso,REAL(R_eposo,4),I_exoso,
     & I_ibuso,I_uxoso,I_oxoso,L_aroso,L_obuso,L_afuso,L_oposo
     &,
     & L_aposo,L_asoso,L_uroso,L_iduso,L_imoso,L_isoso,
     & L_ofuso,L_ubuso,L_etoso,L_itoso,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_oroso,L_iroso,L_efuso,R_axoso,REAL(R_esoso,4),L_ifuso
     &,L_ufuso,
     & L_utoso,L_avoso,L_evoso,L_ovoso,L_uvoso,L_ivoso)
      !}

      if(L_uvoso.or.L_ovoso.or.L_ivoso.or.L_evoso.or.L_avoso.or.L_utoso
     &) then      
                  I_ixoso = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ixoso = z'40000000'
      endif
C KPH_vlv.fgi( 193, 155):���� ���������� �������� ��������,20KPH10AA101
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_iruso,4),L_uvuso,L_axuso
     &,R8_amuso,C30_ipuso,
     & L_oboto,L_idoto,L_ipoto,I_utuso,I_avuso,R_upuso,R_opuso
     &,
     & R_akuso,REAL(R_ikuso,4),R_eluso,
     & REAL(R_oluso,4),R_okuso,REAL(R_aluso,4),I_atuso,
     & I_evuso,I_otuso,I_ituso,L_uluso,L_ivuso,L_uxuso,L_iluso
     &,
     & L_ukuso,L_umuso,L_omuso,L_exuso,L_ekuso,L_epuso,
     & L_ibato,L_ovuso,L_aruso,L_eruso,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_imuso,L_emuso,L_abato,R_ususo,REAL(R_apuso,4),L_ebato
     &,L_obato,
     & L_oruso,L_uruso,L_asuso,L_isuso,L_osuso,L_esuso)
      !}

      if(L_osuso.or.L_isuso.or.L_esuso.or.L_asuso.or.L_uruso.or.L_oruso
     &) then      
                  I_etuso = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_etuso = z'40000000'
      endif
C KPH_vlv.fgi( 243, 179):���� ���������� �������� ��������,20KPH12AA102
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_ipeto,4),L_uteto,L_aveto
     &,R8_aleto,C30_imeto,
     & L_oboto,L_idoto,L_ipoto,I_useto,I_ateto,R_umeto,R_ometo
     &,
     & R_afeto,REAL(R_ifeto,4),R_eketo,
     & REAL(R_oketo,4),R_ofeto,REAL(R_aketo,4),I_aseto,
     & I_eteto,I_oseto,I_iseto,L_uketo,L_iteto,L_uveto,L_iketo
     &,
     & L_ufeto,L_uleto,L_oleto,L_eveto,L_efeto,L_emeto,
     & L_ixeto,L_oteto,L_apeto,L_epeto,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_ileto,L_eleto,L_axeto,R_ureto,REAL(R_ameto,4),L_exeto
     &,L_oxeto,
     & L_opeto,L_upeto,L_areto,L_ireto,L_oreto,L_ereto)
      !}

      if(L_oreto.or.L_ireto.or.L_ereto.or.L_areto.or.L_upeto.or.L_opeto
     &) then      
                  I_eseto = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_eseto = z'40000000'
      endif
C KPH_vlv.fgi( 227, 179):���� ���������� �������� ��������,20KPH12AA101
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_elito,4),L_orito,L_urito
     &,R8_udito,C30_ekito,
     & L_oboto,L_idoto,L_ipoto,I_opito,I_upito,R_okito,R_ikito
     &,
     & R_uxeto,REAL(R_ebito,4),R_adito,
     & REAL(R_idito,4),R_ibito,REAL(R_ubito,4),I_umito,
     & I_arito,I_ipito,I_epito,L_odito,L_erito,L_osito,L_edito
     &,
     & L_obito,L_ofito,L_ifito,L_asito,L_abito,L_akito,
     & L_etito,L_irito,L_ukito,L_alito,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_efito,L_afito,L_usito,R_omito,REAL(R_ufito,4),L_atito
     &,L_itito,
     & L_ilito,L_olito,L_ulito,L_emito,L_imito,L_amito)
      !}

      if(L_imito.or.L_emito.or.L_amito.or.L_ulito.or.L_olito.or.L_ilito
     &) then      
                  I_apito = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_apito = z'40000000'
      endif
C KPH_vlv.fgi( 210, 179):���� ���������� �������� ��������,20KPH10AA107
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_ifoto,4),L_umoto,L_apoto
     &,R8_oxito,C30_edoto,
     & L_oboto,L_idoto,L_ipoto,I_uloto,I_amoto,R_udoto,R_odoto
     &,
     & R_otito,REAL(R_avito,4),R_uvito,
     & REAL(R_exito,4),R_evito,REAL(R_ovito,4),I_aloto,
     & I_emoto,I_oloto,I_iloto,L_ixito,L_imoto,L_aroto,L_axito
     &,
     & L_ivito,L_iboto,L_eboto,L_epoto,L_utito,L_adoto,
     & L_oroto,L_omoto,L_afoto,L_efoto,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_aboto,L_uxito,L_eroto,R_ukoto,REAL(R_uboto,4),L_iroto
     &,L_uroto,
     & L_ofoto,L_ufoto,L_akoto,L_ikoto,L_okoto,L_ekoto)
      !}

      if(L_okoto.or.L_ikoto.or.L_ekoto.or.L_akoto.or.L_ufoto.or.L_ofoto
     &) then      
                  I_eloto = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_eloto = z'40000000'
      endif
C KPH_vlv.fgi( 193, 179):���� ���������� �������� ��������,20KPH10AA106
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_amobu,4),L_isobu,L_osobu
     &,R8_ofobu,C30_alobu,
     & L_ifodu,L_ekodu,L_esodu,I_irobu,I_orobu,R_ilobu,R_elobu
     &,
     & R_obobu,REAL(R_adobu,4),R_udobu,
     & REAL(R_efobu,4),R_edobu,REAL(R_odobu,4),I_opobu,
     & I_urobu,I_erobu,I_arobu,L_ifobu,L_asobu,L_itobu,L_afobu
     &,
     & L_idobu,L_ikobu,L_ekobu,L_usobu,L_ubobu,L_ukobu,
     & L_avobu,L_esobu,L_olobu,L_ulobu,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_akobu,L_ufobu,L_otobu,R_ipobu,REAL(R_okobu,4),L_utobu
     &,L_evobu,
     & L_emobu,L_imobu,L_omobu,L_apobu,L_epobu,L_umobu)
      !}

      if(L_epobu.or.L_apobu.or.L_umobu.or.L_omobu.or.L_imobu.or.L_emobu
     &) then      
                  I_upobu = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_upobu = z'40000000'
      endif
C KPG_vlv.fgi( 187, 160):���� ���������� �������� ��������,20KPG61AA201
      R_etebu = R_elobu
C KPG_sens.fgi(  48, 179):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_asebu,R_uvebu,REAL(1,4
     &),
     & REAL(R_osebu,4),REAL(R_usebu,4),
     & REAL(R_urebu,4),REAL(R_orebu,4),I_ovebu,
     & REAL(R_itebu,4),L_otebu,REAL(R_utebu,4),L_avebu,L_evebu
     &,R_atebu,
     & REAL(R_isebu,4),REAL(R_esebu,4),L_ivebu,REAL(R_etebu
     &,4))
      !}
C KPG_vlv.fgi(  20, 156):���������� �������,20KPG61AA201XQ01
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_ufubu,4),L_epubu,L_ipubu
     &,R8_ibubu,C30_udubu,
     & L_ifodu,L_ekodu,L_esodu,I_emubu,I_imubu,R_efubu,R_afubu
     &,
     & R_ivobu,REAL(R_uvobu,4),R_oxobu,
     & REAL(R_abubu,4),R_axobu,REAL(R_ixobu,4),I_ilubu,
     & I_omubu,I_amubu,I_ulubu,L_ebubu,L_umubu,L_erubu,L_uxobu
     &,
     & L_exobu,L_edubu,L_adubu,L_opubu,L_ovobu,L_odubu,
     & L_urubu,L_apubu,L_ifubu,L_ofubu,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_ububu,L_obubu,L_irubu,R_elubu,REAL(R_idubu,4),L_orubu
     &,L_asubu,
     & L_akubu,L_ekubu,L_ikubu,L_ukubu,L_alubu,L_okubu)
      !}

      if(L_alubu.or.L_ukubu.or.L_okubu.or.L_ikubu.or.L_ekubu.or.L_akubu
     &) then      
                  I_olubu = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_olubu = z'40000000'
      endif
C KPG_vlv.fgi( 187, 184):���� ���������� �������� ��������,20KPG61AA102
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_obadu,4),L_aladu,L_eladu
     &,R8_evubu,C30_oxubu,
     & L_ifodu,L_ekodu,L_esodu,I_akadu,I_ekadu,R_abadu,R_uxubu
     &,
     & R_esubu,REAL(R_osubu,4),R_itubu,
     & REAL(R_utubu,4),R_usubu,REAL(R_etubu,4),I_efadu,
     & I_ikadu,I_ufadu,I_ofadu,L_avubu,L_okadu,L_amadu,L_otubu
     &,
     & L_atubu,L_axubu,L_uvubu,L_iladu,L_isubu,L_ixubu,
     & L_omadu,L_ukadu,L_ebadu,L_ibadu,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_ovubu,L_ivubu,L_emadu,R_afadu,REAL(R_exubu,4),L_imadu
     &,L_umadu,
     & L_ubadu,L_adadu,L_edadu,L_odadu,L_udadu,L_idadu)
      !}

      if(L_udadu.or.L_odadu.or.L_idadu.or.L_edadu.or.L_adadu.or.L_ubadu
     &) then      
                  I_ifadu = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ifadu = z'40000000'
      endif
C KPG_vlv.fgi( 187, 208):���� ���������� �������� ��������,20KPG61AA101
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_ivadu,4),L_udedu,L_afedu
     &,R8_asadu,C30_itadu,
     & L_ifodu,L_ekodu,L_esodu,I_ubedu,I_adedu,R_utadu,R_otadu
     &,
     & R_apadu,REAL(R_ipadu,4),R_eradu,
     & REAL(R_oradu,4),R_opadu,REAL(R_aradu,4),I_abedu,
     & I_ededu,I_obedu,I_ibedu,L_uradu,L_idedu,L_ufedu,L_iradu
     &,
     & L_upadu,L_usadu,L_osadu,L_efedu,L_epadu,L_etadu,
     & L_ikedu,L_odedu,L_avadu,L_evadu,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_isadu,L_esadu,L_akedu,R_uxadu,REAL(R_atadu,4),L_ekedu
     &,L_okedu,
     & L_ovadu,L_uvadu,L_axadu,L_ixadu,L_oxadu,L_exadu)
      !}

      if(L_oxadu.or.L_ixadu.or.L_exadu.or.L_axadu.or.L_uvadu.or.L_ovadu
     &) then      
                  I_ebedu = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ebedu = z'40000000'
      endif
C KPG_vlv.fgi( 187, 232):���� ���������� �������� ��������,20KPG60AA102
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_esedu,4),L_oxedu,L_uxedu
     &,R8_umedu,C30_eredu,
     & L_ifodu,L_ekodu,L_esodu,I_ovedu,I_uvedu,R_oredu,R_iredu
     &,
     & R_ukedu,REAL(R_eledu,4),R_amedu,
     & REAL(R_imedu,4),R_iledu,REAL(R_uledu,4),I_utedu,
     & I_axedu,I_ivedu,I_evedu,L_omedu,L_exedu,L_obidu,L_emedu
     &,
     & L_oledu,L_opedu,L_ipedu,L_abidu,L_aledu,L_aredu,
     & L_edidu,L_ixedu,L_uredu,L_asedu,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_epedu,L_apedu,L_ubidu,R_otedu,REAL(R_upedu,4),L_adidu
     &,L_ididu,
     & L_isedu,L_osedu,L_usedu,L_etedu,L_itedu,L_atedu)
      !}

      if(L_itedu.or.L_etedu.or.L_atedu.or.L_usedu.or.L_osedu.or.L_isedu
     &) then      
                  I_avedu = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_avedu = z'40000000'
      endif
C KPG_vlv.fgi( 187, 256):���� ���������� �������� ��������,20KPG60AA105
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_apidu,4),L_itidu,L_otidu
     &,R8_okidu,C30_amidu,
     & L_ifodu,L_ekodu,L_esodu,I_isidu,I_osidu,R_imidu,R_emidu
     &,
     & R_odidu,REAL(R_afidu,4),R_ufidu,
     & REAL(R_ekidu,4),R_efidu,REAL(R_ofidu,4),I_oridu,
     & I_usidu,I_esidu,I_asidu,L_ikidu,L_atidu,L_ividu,L_akidu
     &,
     & L_ifidu,L_ilidu,L_elidu,L_utidu,L_udidu,L_ulidu,
     & L_axidu,L_etidu,L_omidu,L_umidu,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_alidu,L_ukidu,L_ovidu,R_iridu,REAL(R_olidu,4),L_uvidu
     &,L_exidu,
     & L_epidu,L_ipidu,L_opidu,L_aridu,L_eridu,L_upidu)
      !}

      if(L_eridu.or.L_aridu.or.L_upidu.or.L_opidu.or.L_ipidu.or.L_epidu
     &) then      
                  I_uridu = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_uridu = z'40000000'
      endif
C KPG_vlv.fgi( 187, 280):���� ���������� �������� ��������,20KPG60AA103
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_elodu,4),L_orodu,L_urodu
     &,R8_idodu,C30_akodu,
     & L_ifodu,L_ekodu,L_esodu,I_opodu,I_upodu,R_okodu,R_ikodu
     &,
     & R_ixidu,REAL(R_uxidu,4),R_obodu,
     & REAL(R_adodu,4),R_abodu,REAL(R_ibodu,4),I_umodu,
     & I_arodu,I_ipodu,I_epodu,L_edodu,L_erodu,L_usodu,L_ubodu
     &,
     & L_ebodu,L_efodu,L_afodu,L_asodu,L_oxidu,L_ufodu,
     & L_itodu,L_irodu,L_ukodu,L_alodu,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_udodu,L_ododu,L_atodu,R_omodu,REAL(R_ofodu,4),L_etodu
     &,L_otodu,
     & L_ilodu,L_olodu,L_ulodu,L_emodu,L_imodu,L_amodu)
      !}

      if(L_imodu.or.L_emodu.or.L_amodu.or.L_ulodu.or.L_olodu.or.L_ilodu
     &) then      
                  I_apodu = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_apodu = z'40000000'
      endif
C KPG_vlv.fgi( 173,  16):���� ���������� �������� ��������,20KPG60AA101
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_omudu,4),L_atudu,L_etudu
     &,R8_ekudu,C30_oludu,
     & L_idofad,L_efofad,L_erofad,I_asudu,I_esudu,R_amudu
     &,R_uludu,
     & R_edudu,REAL(R_odudu,4),R_ifudu,
     & REAL(R_ufudu,4),R_ududu,REAL(R_efudu,4),I_erudu,
     & I_isudu,I_urudu,I_orudu,L_akudu,L_osudu,L_avudu,L_ofudu
     &,
     & L_afudu,L_aludu,L_ukudu,L_itudu,L_idudu,L_iludu,
     & L_ovudu,L_usudu,L_emudu,L_imudu,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_okudu,L_ikudu,L_evudu,R_arudu,REAL(R_eludu,4),L_ivudu
     &,L_uvudu,
     & L_umudu,L_apudu,L_epudu,L_opudu,L_upudu,L_ipudu)
      !}

      if(L_upudu.or.L_opudu.or.L_ipudu.or.L_epudu.or.L_apudu.or.L_umudu
     &) then      
                  I_irudu = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_irudu = z'40000000'
      endif
C KPG_vlv.fgi( 173,  40):���� ���������� �������� ��������,20KPG42AA101
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_opofu,4),L_avofu,L_evofu
     &,R8_elofu,C30_omofu,
     & L_alafad,L_ulafad,L_utafad,I_atofu,I_etofu,R_apofu
     &,R_umofu,
     & R_efofu,REAL(R_ofofu,4),R_ikofu,
     & REAL(R_ukofu,4),R_ufofu,REAL(R_ekofu,4),I_esofu,
     & I_itofu,I_usofu,I_osofu,L_alofu,L_otofu,L_axofu,L_okofu
     &,
     & L_akofu,L_amofu,L_ulofu,L_ivofu,L_ifofu,L_imofu,
     & L_oxofu,L_utofu,L_epofu,L_ipofu,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_olofu,L_ilofu,L_exofu,R_asofu,REAL(R_emofu,4),L_ixofu
     &,L_uxofu,
     & L_upofu,L_arofu,L_erofu,L_orofu,L_urofu,L_irofu)
      !}

      if(L_urofu.or.L_orofu.or.L_irofu.or.L_erofu.or.L_arofu.or.L_upofu
     &) then      
                  I_isofu = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_isofu = z'40000000'
      endif
C KPG_vlv.fgi( 103,  16):���� ���������� �������� ��������,20KPG11AA101
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_ilufu,4),L_urufu,L_asufu
     &,R8_afufu,C30_ikufu,
     & L_ufolu,L_okolu,L_osolu,I_upufu,I_arufu,R_ukufu,R_okufu
     &,
     & R_abufu,REAL(R_ibufu,4),R_edufu,
     & REAL(R_odufu,4),R_obufu,REAL(R_adufu,4),I_apufu,
     & I_erufu,I_opufu,I_ipufu,L_udufu,L_irufu,L_usufu,L_idufu
     &,
     & L_ubufu,L_ufufu,L_ofufu,L_esufu,L_ebufu,L_ekufu,
     & L_itufu,L_orufu,L_alufu,L_elufu,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_ifufu,L_efufu,L_atufu,R_umufu,REAL(R_akufu,4),L_etufu
     &,L_otufu,
     & L_olufu,L_ulufu,L_amufu,L_imufu,L_omufu,L_emufu)
      !}

      if(L_omufu.or.L_imufu.or.L_emufu.or.L_amufu.or.L_ulufu.or.L_olufu
     &) then      
                  I_epufu = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_epufu = z'40000000'
      endif
C KPG_vlv.fgi( 173, 160):���� ���������� �������� ��������,20KPG21AA121
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_efaku,4),L_omaku,L_umaku
     &,R8_uxufu,C30_edaku,
     & L_idofad,L_efofad,L_erofad,I_olaku,I_ulaku,R_odaku
     &,R_idaku,
     & R_utufu,REAL(R_evufu,4),R_axufu,
     & REAL(R_ixufu,4),R_ivufu,REAL(R_uvufu,4),I_ukaku,
     & I_amaku,I_ilaku,I_elaku,L_oxufu,L_emaku,L_opaku,L_exufu
     &,
     & L_ovufu,L_obaku,L_ibaku,L_apaku,L_avufu,L_adaku,
     & L_eraku,L_imaku,L_udaku,L_afaku,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_ebaku,L_abaku,L_upaku,R_okaku,REAL(R_ubaku,4),L_araku
     &,L_iraku,
     & L_ifaku,L_ofaku,L_ufaku,L_ekaku,L_ikaku,L_akaku)
      !}

      if(L_ikaku.or.L_ekaku.or.L_akaku.or.L_ufaku.or.L_ofaku.or.L_ifaku
     &) then      
                  I_alaku = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_alaku = z'40000000'
      endif
C KPG_vlv.fgi( 159, 136):���� ���������� �������� ��������,20KPG43AA102
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_uteku,4),L_ediku,L_idiku
     &,R8_ireku,C30_useku,
     & L_odaxu,L_ifaxu,L_iraxu,I_ebiku,I_ibiku,R_eteku,R_ateku
     &,
     & R_imeku,REAL(R_umeku,4),R_opeku,
     & REAL(R_areku,4),R_apeku,REAL(R_ipeku,4),I_ixeku,
     & I_obiku,I_abiku,I_uxeku,L_ereku,L_ubiku,L_efiku,L_upeku
     &,
     & L_epeku,L_eseku,L_aseku,L_odiku,L_omeku,L_oseku,
     & L_ufiku,L_adiku,L_iteku,L_oteku,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_ureku,L_oreku,L_ifiku,R_exeku,REAL(R_iseku,4),L_ofiku
     &,L_akiku,
     & L_aveku,L_eveku,L_iveku,L_uveku,L_axeku,L_oveku)
      !}

      if(L_axeku.or.L_uveku.or.L_oveku.or.L_iveku.or.L_eveku.or.L_aveku
     &) then      
                  I_oxeku = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_oxeku = z'40000000'
      endif
C KPG_vlv.fgi( 131, 136):���� ���������� �������� ��������,20KPG31AA111
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_imoku,4),L_usoku,L_atoku
     &,R8_akoku,C30_iloku,
     & L_ufolu,L_okolu,L_osolu,I_uroku,I_asoku,R_uloku,R_oloku
     &,
     & R_adoku,REAL(R_idoku,4),R_efoku,
     & REAL(R_ofoku,4),R_odoku,REAL(R_afoku,4),I_aroku,
     & I_esoku,I_oroku,I_iroku,L_ufoku,L_isoku,L_utoku,L_ifoku
     &,
     & L_udoku,L_ukoku,L_okoku,L_etoku,L_edoku,L_eloku,
     & L_ivoku,L_osoku,L_amoku,L_emoku,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_ikoku,L_ekoku,L_avoku,R_upoku,REAL(R_aloku,4),L_evoku
     &,L_ovoku,
     & L_omoku,L_umoku,L_apoku,L_ipoku,L_opoku,L_epoku)
      !}

      if(L_opoku.or.L_ipoku.or.L_epoku.or.L_apoku.or.L_umoku.or.L_omoku
     &) then      
                  I_eroku = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_eroku = z'40000000'
      endif
C KPG_vlv.fgi( 103, 136):���� ���������� �������� ��������,20KPG21AA107
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_oselu,4),L_abilu,L_ebilu
     &,R8_epelu,C30_orelu,
     & L_ufolu,L_okolu,L_osolu,I_axelu,I_exelu,R_aselu,R_urelu
     &,
     & R_elelu,REAL(R_olelu,4),R_imelu,
     & REAL(R_umelu,4),R_ulelu,REAL(R_emelu,4),I_evelu,
     & I_ixelu,I_uvelu,I_ovelu,L_apelu,L_oxelu,L_adilu,L_omelu
     &,
     & L_amelu,L_arelu,L_upelu,L_ibilu,L_ilelu,L_irelu,
     & L_odilu,L_uxelu,L_eselu,L_iselu,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_opelu,L_ipelu,L_edilu,R_avelu,REAL(R_erelu,4),L_idilu
     &,L_udilu,
     & L_uselu,L_atelu,L_etelu,L_otelu,L_utelu,L_itelu)
      !}

      if(L_utelu.or.L_otelu.or.L_itelu.or.L_etelu.or.L_atelu.or.L_uselu
     &) then      
                  I_ivelu = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ivelu = z'40000000'
      endif
C KPG_vlv.fgi( 145,  40):���� ���������� �������� ��������,20KPG21AA103
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_ofulu,4),L_apulu,L_epulu
     &,R8_ebulu,C30_odulu,
     & L_idofad,L_efofad,L_erofad,I_amulu,I_emulu,R_afulu
     &,R_udulu,
     & R_evolu,REAL(R_ovolu,4),R_ixolu,
     & REAL(R_uxolu,4),R_uvolu,REAL(R_exolu,4),I_elulu,
     & I_imulu,I_ululu,I_olulu,L_abulu,L_omulu,L_arulu,L_oxolu
     &,
     & L_axolu,L_adulu,L_ubulu,L_ipulu,L_ivolu,L_idulu,
     & L_orulu,L_umulu,L_efulu,L_ifulu,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_obulu,L_ibulu,L_erulu,R_alulu,REAL(R_edulu,4),L_irulu
     &,L_urulu,
     & L_ufulu,L_akulu,L_ekulu,L_okulu,L_ukulu,L_ikulu)
      !}

      if(L_ukulu.or.L_okulu.or.L_ikulu.or.L_ekulu.or.L_akulu.or.L_ufulu
     &) then      
                  I_ilulu = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ilulu = z'40000000'
      endif
C KPG_vlv.fgi( 103,  40):���� ���������� �������� ��������,20KPG42AA201
      R_ituxo = R_udulu
C KPG_sens.fgi(  48, 175):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_esuxo,R_axuxo,REAL(1,4
     &),
     & REAL(R_usuxo,4),REAL(R_atuxo,4),
     & REAL(R_asuxo,4),REAL(R_uruxo,4),I_uvuxo,
     & REAL(R_otuxo,4),L_utuxo,REAL(R_avuxo,4),L_evuxo,L_ivuxo
     &,R_etuxo,
     & REAL(R_osuxo,4),REAL(R_isuxo,4),L_ovuxo,REAL(R_ituxo
     &,4))
      !}
C KPG_vlv.fgi(  48, 143):���������� �������,20KPG42AA201XQ01
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_ibamu,4),L_ukamu,L_alamu
     &,R8_avulu,C30_ixulu,
     & L_idofad,L_efofad,L_erofad,I_ufamu,I_akamu,R_uxulu
     &,R_oxulu,
     & R_asulu,REAL(R_isulu,4),R_etulu,
     & REAL(R_otulu,4),R_osulu,REAL(R_atulu,4),I_afamu,
     & I_ekamu,I_ofamu,I_ifamu,L_utulu,L_ikamu,L_ulamu,L_itulu
     &,
     & L_usulu,L_uvulu,L_ovulu,L_elamu,L_esulu,L_exulu,
     & L_imamu,L_okamu,L_abamu,L_ebamu,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_ivulu,L_evulu,L_amamu,R_udamu,REAL(R_axulu,4),L_emamu
     &,L_omamu,
     & L_obamu,L_ubamu,L_adamu,L_idamu,L_odamu,L_edamu)
      !}

      if(L_odamu.or.L_idamu.or.L_edamu.or.L_adamu.or.L_ubamu.or.L_obamu
     &) then      
                  I_efamu = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_efamu = z'40000000'
      endif
C KPG_vlv.fgi( 173, 184):���� ���������� �������� ��������,20KPG43AA101
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_asemu,4),L_ixemu,L_oxemu
     &,R8_omemu,C30_aremu,
     & L_odaxu,L_ifaxu,L_iraxu,I_ivemu,I_ovemu,R_iremu,R_eremu
     &,
     & R_okemu,REAL(R_alemu,4),R_ulemu,
     & REAL(R_ememu,4),R_elemu,REAL(R_olemu,4),I_otemu,
     & I_uvemu,I_evemu,I_avemu,L_imemu,L_axemu,L_ibimu,L_amemu
     &,
     & L_ilemu,L_ipemu,L_epemu,L_uxemu,L_ukemu,L_upemu,
     & L_adimu,L_exemu,L_oremu,L_uremu,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_apemu,L_umemu,L_obimu,R_itemu,REAL(R_opemu,4),L_ubimu
     &,L_edimu,
     & L_esemu,L_isemu,L_osemu,L_atemu,L_etemu,L_usemu)
      !}

      if(L_etemu.or.L_atemu.or.L_usemu.or.L_osemu.or.L_isemu.or.L_esemu
     &) then      
                  I_utemu = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_utemu = z'40000000'
      endif
C KPG_vlv.fgi( 173, 208):���� ���������� �������� ��������,20KPG31AA106
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_okomu,4),L_aromu,L_eromu
     &,R8_edomu,C30_ofomu,
     & L_odaxu,L_ifaxu,L_iraxu,I_apomu,I_epomu,R_akomu,R_ufomu
     &,
     & R_eximu,REAL(R_oximu,4),R_ibomu,
     & REAL(R_ubomu,4),R_uximu,REAL(R_ebomu,4),I_emomu,
     & I_ipomu,I_umomu,I_omomu,L_adomu,L_opomu,L_asomu,L_obomu
     &,
     & L_abomu,L_afomu,L_udomu,L_iromu,L_iximu,L_ifomu,
     & L_osomu,L_upomu,L_ekomu,L_ikomu,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_odomu,L_idomu,L_esomu,R_amomu,REAL(R_efomu,4),L_isomu
     &,L_usomu,
     & L_ukomu,L_alomu,L_elomu,L_olomu,L_ulomu,L_ilomu)
      !}

      if(L_ulomu.or.L_olomu.or.L_ilomu.or.L_elomu.or.L_alomu.or.L_ukomu
     &) then      
                  I_imomu = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_imomu = z'40000000'
      endif
C KPG_vlv.fgi( 173, 232):���� ���������� �������� ��������,20KPG31AA105
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_upepu,4),L_evepu,L_ivepu
     &,R8_ilepu,C30_umepu,
     & L_odaxu,L_ifaxu,L_iraxu,I_etepu,I_itepu,R_epepu,R_apepu
     &,
     & R_ifepu,REAL(R_ufepu,4),R_okepu,
     & REAL(R_alepu,4),R_akepu,REAL(R_ikepu,4),I_isepu,
     & I_otepu,I_atepu,I_usepu,L_elepu,L_utepu,L_exepu,L_ukepu
     &,
     & L_ekepu,L_emepu,L_amepu,L_ovepu,L_ofepu,L_omepu,
     & L_uxepu,L_avepu,L_ipepu,L_opepu,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_ulepu,L_olepu,L_ixepu,R_esepu,REAL(R_imepu,4),L_oxepu
     &,L_abipu,
     & L_arepu,L_erepu,L_irepu,L_urepu,L_asepu,L_orepu)
      !}

      if(L_asepu.or.L_urepu.or.L_orepu.or.L_irepu.or.L_erepu.or.L_arepu
     &) then      
                  I_osepu = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_osepu = z'40000000'
      endif
C KPG_vlv.fgi( 117, 160):���� ���������� �������� ��������,20KPG31AA102
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_olipu,4),L_asipu,L_esipu
     &,R8_efipu,C30_okipu,
     & L_alafad,L_ulafad,L_utafad,I_aripu,I_eripu,R_alipu
     &,R_ukipu,
     & R_ebipu,REAL(R_obipu,4),R_idipu,
     & REAL(R_udipu,4),R_ubipu,REAL(R_edipu,4),I_epipu,
     & I_iripu,I_upipu,I_opipu,L_afipu,L_oripu,L_atipu,L_odipu
     &,
     & L_adipu,L_akipu,L_ufipu,L_isipu,L_ibipu,L_ikipu,
     & L_otipu,L_uripu,L_elipu,L_ilipu,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_ofipu,L_ifipu,L_etipu,R_apipu,REAL(R_ekipu,4),L_itipu
     &,L_utipu,
     & L_ulipu,L_amipu,L_emipu,L_omipu,L_umipu,L_imipu)
      !}

      if(L_umipu.or.L_omipu.or.L_imipu.or.L_emipu.or.L_amipu.or.L_ulipu
     &) then      
                  I_ipipu = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ipipu = z'40000000'
      endif
C KPG_vlv.fgi( 103, 160):���� ���������� �������� ��������,20KPG12AA110
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_ifopu,4),L_umopu,L_apopu
     &,R8_abopu,C30_idopu,
     & L_alafad,L_ulafad,L_utafad,I_ulopu,I_amopu,R_udopu
     &,R_odopu,
     & R_avipu,REAL(R_ivipu,4),R_exipu,
     & REAL(R_oxipu,4),R_ovipu,REAL(R_axipu,4),I_alopu,
     & I_emopu,I_olopu,I_ilopu,L_uxipu,L_imopu,L_upopu,L_ixipu
     &,
     & L_uvipu,L_ubopu,L_obopu,L_epopu,L_evipu,L_edopu,
     & L_iropu,L_omopu,L_afopu,L_efopu,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_ibopu,L_ebopu,L_aropu,R_ukopu,REAL(R_adopu,4),L_eropu
     &,L_oropu,
     & L_ofopu,L_ufopu,L_akopu,L_ikopu,L_okopu,L_ekopu)
      !}

      if(L_okopu.or.L_ikopu.or.L_ekopu.or.L_akopu.or.L_ufopu.or.L_ofopu
     &) then      
                  I_elopu = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_elopu = z'40000000'
      endif
C KPG_vlv.fgi( 145, 184):���� ���������� �������� ��������,20KPG12AA109
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_ebupu,4),L_okupu,L_ukupu
     &,R8_utopu,C30_exopu,
     & L_alafad,L_ulafad,L_utafad,I_ofupu,I_ufupu,R_oxopu
     &,R_ixopu,
     & R_uropu,REAL(R_esopu,4),R_atopu,
     & REAL(R_itopu,4),R_isopu,REAL(R_usopu,4),I_udupu,
     & I_akupu,I_ifupu,I_efupu,L_otopu,L_ekupu,L_olupu,L_etopu
     &,
     & L_osopu,L_ovopu,L_ivopu,L_alupu,L_asopu,L_axopu,
     & L_emupu,L_ikupu,L_uxopu,L_abupu,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_evopu,L_avopu,L_ulupu,R_odupu,REAL(R_uvopu,4),L_amupu
     &,L_imupu,
     & L_ibupu,L_obupu,L_ubupu,L_edupu,L_idupu,L_adupu)
      !}

      if(L_idupu.or.L_edupu.or.L_adupu.or.L_ubupu.or.L_obupu.or.L_ibupu
     &) then      
                  I_afupu = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_afupu = z'40000000'
      endif
C KPG_vlv.fgi( 131, 184):���� ���������� �������� ��������,20KPG12AA108
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_avupu,4),L_idaru,L_odaru
     &,R8_orupu,C30_atupu,
     & L_alafad,L_ulafad,L_utafad,I_ibaru,I_obaru,R_itupu
     &,R_etupu,
     & R_omupu,REAL(R_apupu,4),R_upupu,
     & REAL(R_erupu,4),R_epupu,REAL(R_opupu,4),I_oxupu,
     & I_ubaru,I_ebaru,I_abaru,L_irupu,L_adaru,L_ifaru,L_arupu
     &,
     & L_ipupu,L_isupu,L_esupu,L_udaru,L_umupu,L_usupu,
     & L_akaru,L_edaru,L_otupu,L_utupu,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_asupu,L_urupu,L_ofaru,R_ixupu,REAL(R_osupu,4),L_ufaru
     &,L_ekaru,
     & L_evupu,L_ivupu,L_ovupu,L_axupu,L_exupu,L_uvupu)
      !}

      if(L_exupu.or.L_axupu.or.L_uvupu.or.L_ovupu.or.L_ivupu.or.L_evupu
     &) then      
                  I_uxupu = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_uxupu = z'40000000'
      endif
C KPG_vlv.fgi( 117, 184):���� ���������� �������� ��������,20KPG12AA102
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_uraru,4),L_exaru,L_ixaru
     &,R8_imaru,C30_uparu,
     & L_alafad,L_ulafad,L_utafad,I_evaru,I_ivaru,R_eraru
     &,R_araru,
     & R_ikaru,REAL(R_ukaru,4),R_olaru,
     & REAL(R_amaru,4),R_alaru,REAL(R_ilaru,4),I_itaru,
     & I_ovaru,I_avaru,I_utaru,L_emaru,L_uvaru,L_eberu,L_ularu
     &,
     & L_elaru,L_eparu,L_aparu,L_oxaru,L_okaru,L_oparu,
     & L_uberu,L_axaru,L_iraru,L_oraru,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_umaru,L_omaru,L_iberu,R_etaru,REAL(R_iparu,4),L_oberu
     &,L_aderu,
     & L_asaru,L_esaru,L_isaru,L_usaru,L_ataru,L_osaru)
      !}

      if(L_ataru.or.L_usaru.or.L_osaru.or.L_isaru.or.L_esaru.or.L_asaru
     &) then      
                  I_otaru = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_otaru = z'40000000'
      endif
C KPG_vlv.fgi( 103, 184):���� ���������� �������� ��������,20KPG12AA101
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_omeru,4),L_ateru,L_eteru
     &,R8_ekeru,C30_oleru,
     & L_alafad,L_ulafad,L_utafad,I_aseru,I_eseru,R_ameru
     &,R_uleru,
     & R_ederu,REAL(R_oderu,4),R_iferu,
     & REAL(R_uferu,4),R_uderu,REAL(R_eferu,4),I_ereru,
     & I_iseru,I_ureru,I_oreru,L_akeru,L_oseru,L_averu,L_oferu
     &,
     & L_aferu,L_aleru,L_ukeru,L_iteru,L_ideru,L_ileru,
     & L_overu,L_useru,L_emeru,L_imeru,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_okeru,L_ikeru,L_everu,R_areru,REAL(R_eleru,4),L_iveru
     &,L_uveru,
     & L_umeru,L_aperu,L_eperu,L_operu,L_uperu,L_iperu)
      !}

      if(L_uperu.or.L_operu.or.L_iperu.or.L_eperu.or.L_aperu.or.L_umeru
     &) then      
                  I_ireru = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ireru = z'40000000'
      endif
C KPG_vlv.fgi( 145, 208):���� ���������� �������� ��������,20KPG12AA106
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_ikiru,4),L_upiru,L_ariru
     &,R8_adiru,C30_ifiru,
     & L_alafad,L_ulafad,L_utafad,I_umiru,I_apiru,R_ufiru
     &,R_ofiru,
     & R_axeru,REAL(R_ixeru,4),R_ebiru,
     & REAL(R_obiru,4),R_oxeru,REAL(R_abiru,4),I_amiru,
     & I_epiru,I_omiru,I_imiru,L_ubiru,L_ipiru,L_uriru,L_ibiru
     &,
     & L_uxeru,L_udiru,L_odiru,L_eriru,L_exeru,L_efiru,
     & L_isiru,L_opiru,L_akiru,L_ekiru,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_idiru,L_ediru,L_asiru,R_uliru,REAL(R_afiru,4),L_esiru
     &,L_osiru,
     & L_okiru,L_ukiru,L_aliru,L_iliru,L_oliru,L_eliru)
      !}

      if(L_oliru.or.L_iliru.or.L_eliru.or.L_aliru.or.L_ukiru.or.L_okiru
     &) then      
                  I_emiru = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_emiru = z'40000000'
      endif
C KPG_vlv.fgi( 131, 208):���� ���������� �������� ��������,20KPG12AA105
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_edoru,4),L_oloru,L_uloru
     &,R8_uviru,C30_eboru,
     & L_alafad,L_ulafad,L_utafad,I_okoru,I_ukoru,R_oboru
     &,R_iboru,
     & R_usiru,REAL(R_etiru,4),R_aviru,
     & REAL(R_iviru,4),R_itiru,REAL(R_utiru,4),I_uforu,
     & I_aloru,I_ikoru,I_ekoru,L_oviru,L_eloru,L_omoru,L_eviru
     &,
     & L_otiru,L_oxiru,L_ixiru,L_amoru,L_atiru,L_aboru,
     & L_eporu,L_iloru,L_uboru,L_adoru,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_exiru,L_axiru,L_umoru,R_oforu,REAL(R_uxiru,4),L_aporu
     &,L_iporu,
     & L_idoru,L_odoru,L_udoru,L_eforu,L_iforu,L_aforu)
      !}

      if(L_iforu.or.L_eforu.or.L_aforu.or.L_udoru.or.L_odoru.or.L_idoru
     &) then      
                  I_akoru = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_akoru = z'40000000'
      endif
C KPG_vlv.fgi( 117, 208):���� ���������� �������� ��������,20KPG12AA104
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_axoru,4),L_ifuru,L_ofuru
     &,R8_osoru,C30_avoru,
     & L_alafad,L_ulafad,L_utafad,I_iduru,I_oduru,R_ivoru
     &,R_evoru,
     & R_oporu,REAL(R_aroru,4),R_uroru,
     & REAL(R_esoru,4),R_eroru,REAL(R_ororu,4),I_oburu,
     & I_uduru,I_eduru,I_aduru,L_isoru,L_afuru,L_ikuru,L_asoru
     &,
     & L_iroru,L_itoru,L_etoru,L_ufuru,L_uporu,L_utoru,
     & L_aluru,L_efuru,L_ovoru,L_uvoru,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_atoru,L_usoru,L_okuru,R_iburu,REAL(R_otoru,4),L_ukuru
     &,L_eluru,
     & L_exoru,L_ixoru,L_oxoru,L_aburu,L_eburu,L_uxoru)
      !}

      if(L_eburu.or.L_aburu.or.L_uxoru.or.L_oxoru.or.L_ixoru.or.L_exoru
     &) then      
                  I_uburu = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_uburu = z'40000000'
      endif
C KPG_vlv.fgi( 103, 208):���� ���������� �������� ��������,20KPG12AA103
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_usuru,4),L_ebasu,L_ibasu
     &,R8_ipuru,C30_ururu,
     & L_alafad,L_ulafad,L_utafad,I_exuru,I_ixuru,R_esuru
     &,R_asuru,
     & R_iluru,REAL(R_uluru,4),R_omuru,
     & REAL(R_apuru,4),R_amuru,REAL(R_imuru,4),I_ivuru,
     & I_oxuru,I_axuru,I_uvuru,L_epuru,L_uxuru,L_edasu,L_umuru
     &,
     & L_emuru,L_eruru,L_aruru,L_obasu,L_oluru,L_oruru,
     & L_udasu,L_abasu,L_isuru,L_osuru,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_upuru,L_opuru,L_idasu,R_evuru,REAL(R_iruru,4),L_odasu
     &,L_afasu,
     & L_aturu,L_eturu,L_ituru,L_uturu,L_avuru,L_oturu)
      !}

      if(L_avuru.or.L_uturu.or.L_oturu.or.L_ituru.or.L_eturu.or.L_aturu
     &) then      
                  I_ovuru = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ovuru = z'40000000'
      endif
C KPG_vlv.fgi( 159, 232):���� ���������� �������� ��������,20KPG12AA107
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_ilesu,4),L_uresu,L_asesu
     &,R8_afesu,C30_ikesu,
     & L_odaxu,L_ifaxu,L_iraxu,I_upesu,I_aresu,R_ukesu,R_okesu
     &,
     & R_abesu,REAL(R_ibesu,4),R_edesu,
     & REAL(R_odesu,4),R_obesu,REAL(R_adesu,4),I_apesu,
     & I_eresu,I_opesu,I_ipesu,L_udesu,L_iresu,L_usesu,L_idesu
     &,
     & L_ubesu,L_ufesu,L_ofesu,L_esesu,L_ebesu,L_ekesu,
     & L_itesu,L_oresu,L_alesu,L_elesu,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_ifesu,L_efesu,L_atesu,R_umesu,REAL(R_akesu,4),L_etesu
     &,L_otesu,
     & L_olesu,L_ulesu,L_amesu,L_imesu,L_omesu,L_emesu)
      !}

      if(L_omesu.or.L_imesu.or.L_emesu.or.L_amesu.or.L_ulesu.or.L_olesu
     &) then      
                  I_epesu = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_epesu = z'40000000'
      endif
C KPG_vlv.fgi( 173, 280):���� ���������� �������� ��������,20KPG32AA102
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_efisu,4),L_omisu,L_umisu
     &,R8_uxesu,C30_edisu,
     & L_odaxu,L_ifaxu,L_iraxu,I_olisu,I_ulisu,R_odisu,R_idisu
     &,
     & R_utesu,REAL(R_evesu,4),R_axesu,
     & REAL(R_ixesu,4),R_ivesu,REAL(R_uvesu,4),I_ukisu,
     & I_amisu,I_ilisu,I_elisu,L_oxesu,L_emisu,L_opisu,L_exesu
     &,
     & L_ovesu,L_obisu,L_ibisu,L_apisu,L_avesu,L_adisu,
     & L_erisu,L_imisu,L_udisu,L_afisu,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_ebisu,L_abisu,L_upisu,R_okisu,REAL(R_ubisu,4),L_arisu
     &,L_irisu,
     & L_ifisu,L_ofisu,L_ufisu,L_ekisu,L_ikisu,L_akisu)
      !}

      if(L_ikisu.or.L_ekisu.or.L_akisu.or.L_ufisu.or.L_ofisu.or.L_ifisu
     &) then      
                  I_alisu = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_alisu = z'40000000'
      endif
C KPG_vlv.fgi( 173,  88):���� ���������� �������� ��������,20KPG32AA101
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_abosu,4),L_ikosu,L_okosu
     &,R8_otisu,C30_axisu,
     & L_odaxu,L_ifaxu,L_iraxu,I_ifosu,I_ofosu,R_ixisu,R_exisu
     &,
     & R_orisu,REAL(R_asisu,4),R_usisu,
     & REAL(R_etisu,4),R_esisu,REAL(R_osisu,4),I_odosu,
     & I_ufosu,I_efosu,I_afosu,L_itisu,L_akosu,L_ilosu,L_atisu
     &,
     & L_isisu,L_ivisu,L_evisu,L_ukosu,L_urisu,L_uvisu,
     & L_amosu,L_ekosu,L_oxisu,L_uxisu,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_avisu,L_utisu,L_olosu,R_idosu,REAL(R_ovisu,4),L_ulosu
     &,L_emosu,
     & L_ebosu,L_ibosu,L_obosu,L_adosu,L_edosu,L_ubosu)
      !}

      if(L_edosu.or.L_adosu.or.L_ubosu.or.L_obosu.or.L_ibosu.or.L_ebosu
     &) then      
                  I_udosu = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_udosu = z'40000000'
      endif
C KPG_vlv.fgi( 159,  64):���� ���������� �������� ��������,20KPG32AA106
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_utosu,4),L_edusu,L_idusu
     &,R8_irosu,C30_usosu,
     & L_odaxu,L_ifaxu,L_iraxu,I_ebusu,I_ibusu,R_etosu,R_atosu
     &,
     & R_imosu,REAL(R_umosu,4),R_oposu,
     & REAL(R_arosu,4),R_aposu,REAL(R_iposu,4),I_ixosu,
     & I_obusu,I_abusu,I_uxosu,L_erosu,L_ubusu,L_efusu,L_uposu
     &,
     & L_eposu,L_esosu,L_asosu,L_odusu,L_omosu,L_ososu,
     & L_ufusu,L_adusu,L_itosu,L_otosu,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_urosu,L_orosu,L_ifusu,R_exosu,REAL(R_isosu,4),L_ofusu
     &,L_akusu,
     & L_avosu,L_evosu,L_ivosu,L_uvosu,L_axosu,L_ovosu)
      !}

      if(L_axosu.or.L_uvosu.or.L_ovosu.or.L_ivosu.or.L_evosu.or.L_avosu
     &) then      
                  I_oxosu = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_oxosu = z'40000000'
      endif
C KPG_vlv.fgi( 145,  64):���� ���������� �������� ��������,20KPG32AA105
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_orusu,4),L_axusu,L_exusu
     &,R8_emusu,C30_opusu,
     & L_odaxu,L_ifaxu,L_iraxu,I_avusu,I_evusu,R_arusu,R_upusu
     &,
     & R_ekusu,REAL(R_okusu,4),R_ilusu,
     & REAL(R_ulusu,4),R_ukusu,REAL(R_elusu,4),I_etusu,
     & I_ivusu,I_utusu,I_otusu,L_amusu,L_ovusu,L_abatu,L_olusu
     &,
     & L_alusu,L_apusu,L_umusu,L_ixusu,L_ikusu,L_ipusu,
     & L_obatu,L_uvusu,L_erusu,L_irusu,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_omusu,L_imusu,L_ebatu,R_atusu,REAL(R_epusu,4),L_ibatu
     &,L_ubatu,
     & L_urusu,L_asusu,L_esusu,L_osusu,L_ususu,L_isusu)
      !}

      if(L_ususu.or.L_osusu.or.L_isusu.or.L_esusu.or.L_asusu.or.L_urusu
     &) then      
                  I_itusu = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_itusu = z'40000000'
      endif
C KPG_vlv.fgi( 131,  64):���� ���������� �������� ��������,20KPG32AA104
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_imatu,4),L_usatu,L_atatu
     &,R8_akatu,C30_ilatu,
     & L_odaxu,L_ifaxu,L_iraxu,I_uratu,I_asatu,R_ulatu,R_olatu
     &,
     & R_adatu,REAL(R_idatu,4),R_efatu,
     & REAL(R_ofatu,4),R_odatu,REAL(R_afatu,4),I_aratu,
     & I_esatu,I_oratu,I_iratu,L_ufatu,L_isatu,L_utatu,L_ifatu
     &,
     & L_udatu,L_ukatu,L_okatu,L_etatu,L_edatu,L_elatu,
     & L_ivatu,L_osatu,L_amatu,L_ematu,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_ikatu,L_ekatu,L_avatu,R_upatu,REAL(R_alatu,4),L_evatu
     &,L_ovatu,
     & L_omatu,L_umatu,L_apatu,L_ipatu,L_opatu,L_epatu)
      !}

      if(L_opatu.or.L_ipatu.or.L_epatu.or.L_apatu.or.L_umatu.or.L_omatu
     &) then      
                  I_eratu = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_eratu = z'40000000'
      endif
C KPG_vlv.fgi( 117,  64):���� ���������� �������� ��������,20KPG32AA103
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_eketu,4),L_opetu,L_upetu
     &,R8_ubetu,C30_efetu,
     & L_odaxu,L_ifaxu,L_iraxu,I_ometu,I_umetu,R_ofetu,R_ifetu
     &,
     & R_uvatu,REAL(R_exatu,4),R_abetu,
     & REAL(R_ibetu,4),R_ixatu,REAL(R_uxatu,4),I_uletu,
     & I_apetu,I_imetu,I_emetu,L_obetu,L_epetu,L_oretu,L_ebetu
     &,
     & L_oxatu,L_odetu,L_idetu,L_aretu,L_axatu,L_afetu,
     & L_esetu,L_ipetu,L_ufetu,L_aketu,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_edetu,L_adetu,L_uretu,R_oletu,REAL(R_udetu,4),L_asetu
     &,L_isetu,
     & L_iketu,L_oketu,L_uketu,L_eletu,L_iletu,L_aletu)
      !}

      if(L_iletu.or.L_eletu.or.L_aletu.or.L_uketu.or.L_oketu.or.L_iketu
     &) then      
                  I_ametu = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ametu = z'40000000'
      endif
C KPG_vlv.fgi( 103,  64):���� ���������� �������� ��������,20KPG31AA116
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_aditu,4),L_ilitu,L_olitu
     &,R8_ovetu,C30_abitu,
     & L_odaxu,L_ifaxu,L_iraxu,I_ikitu,I_okitu,R_ibitu,R_ebitu
     &,
     & R_osetu,REAL(R_atetu,4),R_utetu,
     & REAL(R_evetu,4),R_etetu,REAL(R_otetu,4),I_ofitu,
     & I_ukitu,I_ekitu,I_akitu,L_ivetu,L_alitu,L_imitu,L_avetu
     &,
     & L_itetu,L_ixetu,L_exetu,L_ulitu,L_usetu,L_uxetu,
     & L_apitu,L_elitu,L_obitu,L_ubitu,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_axetu,L_uvetu,L_omitu,R_ifitu,REAL(R_oxetu,4),L_umitu
     &,L_epitu,
     & L_editu,L_iditu,L_oditu,L_afitu,L_efitu,L_uditu)
      !}

      if(L_efitu.or.L_afitu.or.L_uditu.or.L_oditu.or.L_iditu.or.L_editu
     &) then      
                  I_ufitu = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ufitu = z'40000000'
      endif
C KPG_vlv.fgi( 173, 112):���� ���������� �������� ��������,20KPG31AA115
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_osotu,4),L_abutu,L_ebutu
     &,R8_epotu,C30_orotu,
     & L_alafad,L_ulafad,L_utafad,I_axotu,I_exotu,R_asotu
     &,R_urotu,
     & R_elotu,REAL(R_olotu,4),R_imotu,
     & REAL(R_umotu,4),R_ulotu,REAL(R_emotu,4),I_evotu,
     & I_ixotu,I_uvotu,I_ovotu,L_apotu,L_oxotu,L_adutu,L_omotu
     &,
     & L_amotu,L_arotu,L_upotu,L_ibutu,L_ilotu,L_irotu,
     & L_odutu,L_uxotu,L_esotu,L_isotu,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_opotu,L_ipotu,L_edutu,R_avotu,REAL(R_erotu,4),L_idutu
     &,L_udutu,
     & L_usotu,L_atotu,L_etotu,L_ototu,L_utotu,L_itotu)
      !}

      if(L_utotu.or.L_ototu.or.L_itotu.or.L_etotu.or.L_atotu.or.L_usotu
     &) then      
                  I_ivotu = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ivotu = z'40000000'
      endif
C KPG_vlv.fgi( 145,  88):���� ���������� �������� ��������,20KPG11AA108
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_uxevu,4),L_ekivu,L_ikivu
     &,R8_itevu,C30_uvevu,
     & L_alafad,L_ulafad,L_utafad,I_efivu,I_ifivu,R_exevu
     &,R_axevu,
     & R_irevu,REAL(R_urevu,4),R_osevu,
     & REAL(R_atevu,4),R_asevu,REAL(R_isevu,4),I_idivu,
     & I_ofivu,I_afivu,I_udivu,L_etevu,L_ufivu,L_elivu,L_usevu
     &,
     & L_esevu,L_evevu,L_avevu,L_okivu,L_orevu,L_ovevu,
     & L_ulivu,L_akivu,L_ixevu,L_oxevu,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_utevu,L_otevu,L_ilivu,R_edivu,REAL(R_ivevu,4),L_olivu
     &,L_amivu,
     & L_abivu,L_ebivu,L_ibivu,L_ubivu,L_adivu,L_obivu)
      !}

      if(L_adivu.or.L_ubivu.or.L_obivu.or.L_ibivu.or.L_ebivu.or.L_abivu
     &) then      
                  I_odivu = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_odivu = z'40000000'
      endif
C KPG_vlv.fgi( 173, 136):���� ���������� �������� ��������,20KPG11AA104
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_irovu,4),L_uvovu,L_axovu
     &,R8_amovu,C30_ipovu,
     & L_alafad,L_ulafad,L_utafad,I_utovu,I_avovu,R_upovu
     &,R_opovu,
     & R_akovu,REAL(R_ikovu,4),R_elovu,
     & REAL(R_olovu,4),R_okovu,REAL(R_alovu,4),I_atovu,
     & I_evovu,I_otovu,I_itovu,L_ulovu,L_ivovu,L_uxovu,L_ilovu
     &,
     & L_ukovu,L_umovu,L_omovu,L_exovu,L_ekovu,L_epovu,
     & L_ibuvu,L_ovovu,L_arovu,L_erovu,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_imovu,L_emovu,L_abuvu,R_usovu,REAL(R_apovu,4),L_ebuvu
     &,L_obuvu,
     & L_orovu,L_urovu,L_asovu,L_isovu,L_osovu,L_esovu)
      !}

      if(L_osovu.or.L_isovu.or.L_esovu.or.L_asovu.or.L_urovu.or.L_orovu
     &) then      
                  I_etovu = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_etovu = z'40000000'
      endif
C KPG_vlv.fgi( 145, 112):���� ���������� �������� ��������,20KPG11AA102
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_ikaxu,4),L_upaxu,L_araxu
     &,R8_obaxu,C30_efaxu,
     & L_odaxu,L_ifaxu,L_iraxu,I_umaxu,I_apaxu,R_ufaxu,R_ofaxu
     &,
     & R_ovuvu,REAL(R_axuvu,4),R_uxuvu,
     & REAL(R_ebaxu,4),R_exuvu,REAL(R_oxuvu,4),I_amaxu,
     & I_epaxu,I_omaxu,I_imaxu,L_ibaxu,L_ipaxu,L_asaxu,L_abaxu
     &,
     & L_ixuvu,L_idaxu,L_edaxu,L_eraxu,L_uvuvu,L_afaxu,
     & L_osaxu,L_opaxu,L_akaxu,L_ekaxu,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_adaxu,L_ubaxu,L_esaxu,R_ulaxu,REAL(R_udaxu,4),L_isaxu
     &,L_usaxu,
     & L_okaxu,L_ukaxu,L_alaxu,L_ilaxu,L_olaxu,L_elaxu)
      !}

      if(L_olaxu.or.L_ilaxu.or.L_elaxu.or.L_alaxu.or.L_ukaxu.or.L_okaxu
     &) then      
                  I_emaxu = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_emaxu = z'40000000'
      endif
C KPG_vlv.fgi( 117, 112):���� ���������� �������� ��������,20KPG32AA111
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_idexu,4),L_ulexu,L_amexu
     &,R8_axaxu,C30_ibexu,
     & L_alafad,L_ulafad,L_utafad,I_ukexu,I_alexu,R_ubexu
     &,R_obexu,
     & R_ataxu,REAL(R_itaxu,4),R_evaxu,
     & REAL(R_ovaxu,4),R_otaxu,REAL(R_avaxu,4),I_akexu,
     & I_elexu,I_okexu,I_ikexu,L_uvaxu,L_ilexu,L_umexu,L_ivaxu
     &,
     & L_utaxu,L_uxaxu,L_oxaxu,L_emexu,L_etaxu,L_ebexu,
     & L_ipexu,L_olexu,L_adexu,L_edexu,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_ixaxu,L_exaxu,L_apexu,R_ufexu,REAL(R_abexu,4),L_epexu
     &,L_opexu,
     & L_odexu,L_udexu,L_afexu,L_ifexu,L_ofexu,L_efexu)
      !}

      if(L_ofexu.or.L_ifexu.or.L_efexu.or.L_afexu.or.L_udexu.or.L_odexu
     &) then      
                  I_ekexu = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ekexu = z'40000000'
      endif
C KPG_vlv.fgi( 103, 112):���� ���������� �������� ��������,20KPG11AA001
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_asoxu,4),L_ixoxu,L_oxoxu
     &,R8_omoxu,C30_aroxu,
     & L_apubad,L_upubad,L_uxubad,I_ivoxu,I_ovoxu,R_iroxu
     &,R_eroxu,
     & R_okoxu,REAL(R_aloxu,4),R_uloxu,
     & REAL(R_emoxu,4),R_eloxu,REAL(R_oloxu,4),I_otoxu,
     & I_uvoxu,I_evoxu,I_avoxu,L_imoxu,L_axoxu,L_ibuxu,L_amoxu
     &,
     & L_iloxu,L_ipoxu,L_epoxu,L_uxoxu,L_ukoxu,L_upoxu,
     & L_aduxu,L_exoxu,L_oroxu,L_uroxu,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_apoxu,L_umoxu,L_obuxu,R_itoxu,REAL(R_opoxu,4),L_ubuxu
     &,L_eduxu,
     & L_esoxu,L_isoxu,L_osoxu,L_atoxu,L_etoxu,L_usoxu)
      !}

      if(L_etoxu.or.L_atoxu.or.L_usoxu.or.L_osoxu.or.L_isoxu.or.L_esoxu
     &) then      
                  I_utoxu = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_utoxu = z'40000000'
      endif
C KPG_vlv.fgi( 131, 232):���� ���������� �������� ��������,20KPG52AA101
      !{
      Call DAT_ANA_HANDLER(deltat,R_ibabad,R_ekabad,REAL(1000
     &,4),
     & REAL(R_adabad,4),REAL(R_edabad,4),
     & REAL(R_ebabad,4),REAL(R_ababad,4),I_akabad,
     & REAL(R_udabad,4),L_afabad,REAL(R_efabad,4),L_ifabad
     &,L_ofabad,R_idabad,
     & REAL(R_ubabad,4),REAL(R_obabad,4),L_ufabad,REAL(R_odabad
     &,4))
      !}
C KPG_vlv.fgi(  20, 273):���������� �������,20KPG41CQ001XQ01
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_uxibad,4),L_ekobad,L_ikobad
     &,R8_itibad,C30_uvibad,
     & L_alafad,L_ulafad,L_utafad,I_efobad,I_ifobad,R_exibad
     &,R_axibad,
     & R_iribad,REAL(R_uribad,4),R_osibad,
     & REAL(R_atibad,4),R_asibad,REAL(R_isibad,4),I_idobad
     &,
     & I_ofobad,I_afobad,I_udobad,L_etibad,L_ufobad,L_elobad
     &,L_usibad,
     & L_esibad,L_evibad,L_avibad,L_okobad,L_oribad,L_ovibad
     &,
     & L_ulobad,L_akobad,L_ixibad,L_oxibad,REAL(R8_irufad
     &,8),REAL(1.0,4),R8_ixufad,
     & L_utibad,L_otibad,L_ilobad,R_edobad,REAL(R_ivibad,4
     &),L_olobad,L_amobad,
     & L_abobad,L_ebobad,L_ibobad,L_ubobad,L_adobad,L_obobad
     &)
      !}

      if(L_adobad.or.L_ubobad.or.L_obobad.or.L_ibobad.or.L_ebobad.or.L_a
     &bobad) then      
                  I_odobad = ior(z'000000FF',z'04000000')
     &                            
                else
                   I_odobad = z'40000000'
      endif
C KPG_vlv.fgi( 117, 232):���� ���������� �������� ��������,20KPG11AA211
      R_efibad = R_axibad
C KPG_sens.fgi(  48, 187):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_adibad,R_ukibad,REAL(1
     &,4),
     & REAL(R_odibad,4),REAL(R_udibad,4),
     & REAL(R_ubibad,4),REAL(R_obibad,4),I_okibad,
     & REAL(R_ifibad,4),L_ofibad,REAL(R_ufibad,4),L_akibad
     &,L_ekibad,R_afibad,
     & REAL(R_idibad,4),REAL(R_edibad,4),L_ikibad,REAL(R_efibad
     &,4))
      !}
C KPG_vlv.fgi(  76, 273):���������� �������,20KPG11AA211XQ01
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_otobad,4),L_adubad,L_edubad
     &,R8_erobad,C30_osobad,
     & L_idofad,L_efofad,L_erofad,I_abubad,I_ebubad,R_atobad
     &,R_usobad,
     & R_emobad,REAL(R_omobad,4),R_ipobad,
     & REAL(R_upobad,4),R_umobad,REAL(R_epobad,4),I_exobad
     &,
     & I_ibubad,I_uxobad,I_oxobad,L_arobad,L_obubad,L_afubad
     &,L_opobad,
     & L_apobad,L_asobad,L_urobad,L_idubad,L_imobad,L_isobad
     &,
     & L_ofubad,L_ububad,L_etobad,L_itobad,REAL(R8_irufad
     &,8),REAL(1.0,4),R8_ixufad,
     & L_orobad,L_irobad,L_efubad,R_axobad,REAL(R_esobad,4
     &),L_ifubad,L_ufubad,
     & L_utobad,L_avobad,L_evobad,L_ovobad,L_uvobad,L_ivobad
     &)
      !}

      if(L_uvobad.or.L_ovobad.or.L_ivobad.or.L_evobad.or.L_avobad.or.L_u
     &tobad) then      
                  I_ixobad = ior(z'000000FF',z'04000000')
     &                            
                else
                   I_ixobad = z'40000000'
      endif
C KPG_vlv.fgi( 103, 232):���� ���������� �������� ��������,20KPG41AA104
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_urubad,4),L_exubad,L_ixubad
     &,R8_amubad,C30_opubad,
     & L_apubad,L_upubad,L_uxubad,I_evubad,I_ivubad,R_erubad
     &,R_arubad,
     & R_akubad,REAL(R_ikubad,4),R_elubad,
     & REAL(R_olubad,4),R_okubad,REAL(R_alubad,4),I_itubad
     &,
     & I_ovubad,I_avubad,I_utubad,L_ulubad,L_uvubad,L_ibadad
     &,L_ilubad,
     & L_ukubad,L_umubad,L_omubad,L_oxubad,L_ekubad,L_ipubad
     &,
     & L_adadad,L_axubad,L_irubad,L_orubad,REAL(R8_irufad
     &,8),REAL(1.0,4),R8_ixufad,
     & L_imubad,L_emubad,L_obadad,R_etubad,REAL(R_epubad,4
     &),L_ubadad,L_edadad,
     & L_asubad,L_esubad,L_isubad,L_usubad,L_atubad,L_osubad
     &)
      !}

      if(L_atubad.or.L_usubad.or.L_osubad.or.L_isubad.or.L_esubad.or.L_a
     &subad) then      
                  I_otubad = ior(z'000000FF',z'04000000')
     &                            
                else
                   I_otubad = z'40000000'
      endif
C KPG_vlv.fgi( 159, 256):���� ���������� �������� ��������,20KPG51AA103
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_umadad,4),L_etadad,L_itadad
     &,R8_ikadad,C30_uladad,
     & L_idofad,L_efofad,L_erofad,I_esadad,I_isadad,R_emadad
     &,R_amadad,
     & R_idadad,REAL(R_udadad,4),R_ofadad,
     & REAL(R_akadad,4),R_afadad,REAL(R_ifadad,4),I_iradad
     &,
     & I_osadad,I_asadad,I_uradad,L_ekadad,L_usadad,L_evadad
     &,L_ufadad,
     & L_efadad,L_eladad,L_aladad,L_otadad,L_odadad,L_oladad
     &,
     & L_uvadad,L_atadad,L_imadad,L_omadad,REAL(R8_irufad
     &,8),REAL(1.0,4),R8_ixufad,
     & L_ukadad,L_okadad,L_ivadad,R_eradad,REAL(R_iladad,4
     &),L_ovadad,L_axadad,
     & L_apadad,L_epadad,L_ipadad,L_upadad,L_aradad,L_opadad
     &)
      !}

      if(L_aradad.or.L_upadad.or.L_opadad.or.L_ipadad.or.L_epadad.or.L_a
     &padad) then      
                  I_oradad = ior(z'000000FF',z'04000000')
     &                            
                else
                   I_oradad = z'40000000'
      endif
C KPG_vlv.fgi( 145, 256):���� ���������� �������� ��������,20KPG41AA203
      R_omufad = R_amadad
C KPG_sens.fgi(  48, 183):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ilufad,R_erufad,REAL(1
     &,4),
     & REAL(R_amufad,4),REAL(R_emufad,4),
     & REAL(R_elufad,4),REAL(R_alufad,4),I_arufad,
     & REAL(R_umufad,4),L_apufad,REAL(R_epufad,4),L_ipufad
     &,L_opufad,R_imufad,
     & REAL(R_ulufad,4),REAL(R_olufad,4),L_upufad,REAL(R_omufad
     &,4))
      !}
C KPG_vlv.fgi(  48, 260):���������� �������,20KPG41AA203XQ01
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_efidad,4),L_omidad,L_umidad
     &,R8_uxedad,C30_edidad,
     & L_alafad,L_ulafad,L_utafad,I_olidad,I_ulidad,R_odidad
     &,R_ididad,
     & R_utedad,REAL(R_evedad,4),R_axedad,
     & REAL(R_ixedad,4),R_ivedad,REAL(R_uvedad,4),I_ukidad
     &,
     & I_amidad,I_ilidad,I_elidad,L_oxedad,L_emidad,L_opidad
     &,L_exedad,
     & L_ovedad,L_obidad,L_ibidad,L_apidad,L_avedad,L_adidad
     &,
     & L_eridad,L_imidad,L_udidad,L_afidad,REAL(R8_irufad
     &,8),REAL(1.0,4),R8_ixufad,
     & L_ebidad,L_abidad,L_upidad,R_okidad,REAL(R_ubidad,4
     &),L_aridad,L_iridad,
     & L_ifidad,L_ofidad,L_ufidad,L_ekidad,L_ikidad,L_akidad
     &)
      !}

      if(L_ikidad.or.L_ekidad.or.L_akidad.or.L_ufidad.or.L_ofidad.or.L_i
     &fidad) then      
                  I_alidad = ior(z'000000FF',z'04000000')
     &                            
                else
                   I_alidad = z'40000000'
      endif
C KPG_vlv.fgi( 131, 256):���� ���������� �������� ��������,20KPG11AA122
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_abodad,4),L_ikodad,L_okodad
     &,R8_otidad,C30_axidad,
     & L_alafad,L_ulafad,L_utafad,I_ifodad,I_ofodad,R_ixidad
     &,R_exidad,
     & R_oridad,REAL(R_asidad,4),R_usidad,
     & REAL(R_etidad,4),R_esidad,REAL(R_osidad,4),I_ododad
     &,
     & I_ufodad,I_efodad,I_afodad,L_itidad,L_akodad,L_ilodad
     &,L_atidad,
     & L_isidad,L_ividad,L_evidad,L_ukodad,L_uridad,L_uvidad
     &,
     & L_amodad,L_ekodad,L_oxidad,L_uxidad,REAL(R8_irufad
     &,8),REAL(1.0,4),R8_ixufad,
     & L_avidad,L_utidad,L_olodad,R_idodad,REAL(R_ovidad,4
     &),L_ulodad,L_emodad,
     & L_ebodad,L_ibodad,L_obodad,L_adodad,L_edodad,L_ubodad
     &)
      !}

      if(L_edodad.or.L_adodad.or.L_ubodad.or.L_obodad.or.L_ibodad.or.L_e
     &bodad) then      
                  I_udodad = ior(z'000000FF',z'04000000')
     &                            
                else
                   I_udodad = z'40000000'
      endif
C KPG_vlv.fgi( 117, 256):���� ���������� �������� ��������,20KPG11AA121
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_orudad,4),L_axudad,L_exudad
     &,R8_emudad,C30_opudad,
     & L_alafad,L_ulafad,L_utafad,I_avudad,I_evudad,R_arudad
     &,R_upudad,
     & R_ekudad,REAL(R_okudad,4),R_iludad,
     & REAL(R_uludad,4),R_ukudad,REAL(R_eludad,4),I_etudad
     &,
     & I_ivudad,I_utudad,I_otudad,L_amudad,L_ovudad,L_abafad
     &,L_oludad,
     & L_aludad,L_apudad,L_umudad,L_ixudad,L_ikudad,L_ipudad
     &,
     & L_obafad,L_uvudad,L_erudad,L_irudad,REAL(R8_irufad
     &,8),REAL(1.0,4),R8_ixufad,
     & L_omudad,L_imudad,L_ebafad,R_atudad,REAL(R_epudad,4
     &),L_ibafad,L_ubafad,
     & L_urudad,L_asudad,L_esudad,L_osudad,L_usudad,L_isudad
     &)
      !}

      if(L_usudad.or.L_osudad.or.L_isudad.or.L_esudad.or.L_asudad.or.L_u
     &rudad) then      
                  I_itudad = ior(z'000000FF',z'04000000')
     &                            
                else
                   I_itudad = z'40000000'
      endif
C KPG_vlv.fgi( 145, 280):���� ���������� �������� ��������,20KPG11AA114
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_umafad,4),L_etafad,L_itafad
     &,R8_akafad,C30_olafad,
     & L_alafad,L_ulafad,L_utafad,I_esafad,I_isafad,R_emafad
     &,R_amafad,
     & R_adafad,REAL(R_idafad,4),R_efafad,
     & REAL(R_ofafad,4),R_odafad,REAL(R_afafad,4),I_irafad
     &,
     & I_osafad,I_asafad,I_urafad,L_ufafad,L_usafad,L_ivafad
     &,L_ifafad,
     & L_udafad,L_ukafad,L_okafad,L_otafad,L_edafad,L_ilafad
     &,
     & L_axafad,L_atafad,L_imafad,L_omafad,REAL(R8_irufad
     &,8),REAL(1.0,4),R8_ixufad,
     & L_ikafad,L_ekafad,L_ovafad,R_erafad,REAL(R_elafad,4
     &),L_uvafad,L_exafad,
     & L_apafad,L_epafad,L_ipafad,L_upafad,L_arafad,L_opafad
     &)
      !}

      if(L_arafad.or.L_upafad.or.L_opafad.or.L_ipafad.or.L_epafad.or.L_a
     &pafad) then      
                  I_orafad = ior(z'000000FF',z'04000000')
     &                            
                else
                   I_orafad = z'40000000'
      endif
C KPG_vlv.fgi( 159, 280):���� ���������� �������� ��������,20KPG11AA115
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_erefad,4),L_ovefad,L_uvefad
     &,R8_ulefad,C30_epefad,
     & L_idofad,L_efofad,L_erofad,I_otefad,I_utefad,R_opefad
     &,R_ipefad,
     & R_ufefad,REAL(R_ekefad,4),R_alefad,
     & REAL(R_ilefad,4),R_ikefad,REAL(R_ukefad,4),I_usefad
     &,
     & I_avefad,I_itefad,I_etefad,L_olefad,L_evefad,L_oxefad
     &,L_elefad,
     & L_okefad,L_omefad,L_imefad,L_axefad,L_akefad,L_apefad
     &,
     & L_ebifad,L_ivefad,L_upefad,L_arefad,REAL(R8_irufad
     &,8),REAL(1.0,4),R8_ixufad,
     & L_emefad,L_amefad,L_uxefad,R_osefad,REAL(R_umefad,4
     &),L_abifad,L_ibifad,
     & L_irefad,L_orefad,L_urefad,L_esefad,L_isefad,L_asefad
     &)
      !}

      if(L_isefad.or.L_esefad.or.L_asefad.or.L_urefad.or.L_orefad.or.L_i
     &refad) then      
                  I_atefad = ior(z'000000FF',z'04000000')
     &                            
                else
                   I_atefad = z'40000000'
      endif
C KPG_vlv.fgi( 131, 280):���� ���������� �������� ��������,20KPG41AA103
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_amifad,4),L_isifad,L_osifad
     &,R8_ofifad,C30_alifad,
     & L_idofad,L_efofad,L_erofad,I_irifad,I_orifad,R_ilifad
     &,R_elifad,
     & R_obifad,REAL(R_adifad,4),R_udifad,
     & REAL(R_efifad,4),R_edifad,REAL(R_odifad,4),I_opifad
     &,
     & I_urifad,I_erifad,I_arifad,L_ififad,L_asifad,L_itifad
     &,L_afifad,
     & L_idifad,L_ikifad,L_ekifad,L_usifad,L_ubifad,L_ukifad
     &,
     & L_avifad,L_esifad,L_olifad,L_ulifad,REAL(R8_irufad
     &,8),REAL(1.0,4),R8_ixufad,
     & L_akifad,L_ufifad,L_otifad,R_ipifad,REAL(R_okifad,4
     &),L_utifad,L_evifad,
     & L_emifad,L_imifad,L_omifad,L_apifad,L_epifad,L_umifad
     &)
      !}

      if(L_epifad.or.L_apifad.or.L_umifad.or.L_omifad.or.L_imifad.or.L_e
     &mifad) then      
                  I_upifad = ior(z'000000FF',z'04000000')
     &                            
                else
                   I_upifad = z'40000000'
      endif
C KPG_vlv.fgi( 117, 280):���� ���������� �������� ��������,20KPG41AA102
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_ekofad,4),L_opofad,L_upofad
     &,R8_ibofad,C30_afofad,
     & L_idofad,L_efofad,L_erofad,I_omofad,I_umofad,R_ofofad
     &,R_ifofad,
     & R_ivifad,REAL(R_uvifad,4),R_oxifad,
     & REAL(R_abofad,4),R_axifad,REAL(R_ixifad,4),I_ulofad
     &,
     & I_apofad,I_imofad,I_emofad,L_ebofad,L_epofad,L_urofad
     &,L_uxifad,
     & L_exifad,L_edofad,L_adofad,L_arofad,L_ovifad,L_udofad
     &,
     & L_isofad,L_ipofad,L_ufofad,L_akofad,REAL(R8_irufad
     &,8),REAL(1.0,4),R8_ixufad,
     & L_ubofad,L_obofad,L_asofad,R_olofad,REAL(R_odofad,4
     &),L_esofad,L_osofad,
     & L_ikofad,L_okofad,L_ukofad,L_elofad,L_ilofad,L_alofad
     &)
      !}

      if(L_ilofad.or.L_elofad.or.L_alofad.or.L_ukofad.or.L_okofad.or.L_i
     &kofad) then      
                  I_amofad = ior(z'000000FF',z'04000000')
     &                            
                else
                   I_amofad = z'40000000'
      endif
C KPG_vlv.fgi( 103, 280):���� ���������� �������� ��������,20KPG41AA101
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_obakad,4),L_alakad,L_elakad
     &,R8_otufad,C30_exufad,
     & L_ovufad,L_oxufad,L_olakad,I_akakad,I_ekakad,R_abakad
     &,R_uxufad,
     & R_orufad,REAL(R_asufad,4),R_usufad,
     & REAL(R_etufad,4),R_esufad,REAL(R_osufad,4),I_efakad
     &,
     & I_ikakad,I_ufakad,I_ofakad,L_itufad,L_okakad,L_emakad
     &,L_atufad,
     & L_isufad,L_ivufad,L_evufad,L_ilakad,L_urufad,L_axufad
     &,
     & L_umakad,L_ukakad,L_ebakad,L_ibakad,REAL(R8_irufad
     &,8),REAL(1.0,4),R8_ixufad,
     & L_avufad,L_utufad,L_imakad,R_afakad,REAL(R_uvufad,4
     &),L_omakad,L_apakad,
     & L_ubakad,L_adakad,L_edakad,L_odakad,L_udakad,L_idakad
     &)
      !}

      if(L_udakad.or.L_odakad.or.L_idakad.or.L_edakad.or.L_adakad.or.L_u
     &bakad) then      
                  I_ifakad = ior(z'000000FF',z'04000000')
     &                            
                else
                   I_ifakad = z'40000000'
      endif
C KPF_vlv.fgi(  15,  42):���� ���������� �������� ��������,20KPF40AA111
      L_(165) = (.NOT.L_epakad)
C KPG_KPH_logic.fgi( 432,  72):���
      L_(166) = (.NOT.L_ipakad)
C KPG_KPH_logic.fgi( 432,  88):���
      L_arakad=R_upakad.gt.R0_opakad
C KPG_KPH_logic.fgi( 262, 105):���������� >
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_erere,4),R8_imere
     &,I_otere,I_evere,I_itere,
     & C8_umere,I_avere,R_opere,R_ipere,R_ikere,
     & REAL(R_ukere,4),R_olere,REAL(R_amere,4),
     & R_alere,REAL(R_ilere,4),I_atere,I_ivere,I_utere,I_usere
     &,L_emere,
     & L_uvere,L_uxere,L_ulere,L_elere,
     & L_omere,L_arakad,L_exere,L_okere,L_epere,L_ibire,L_upere
     &,
     & L_arere,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_
     &(21),L_ufere,L_(22),
     & L_akere,L_axere,I_overe,L_abire,R_osere,REAL(R_apere
     &,4),L_ebire,L_edokad,
     & L_obire,L_ekere,L_irere,L_orere,L_urere,L_esere,L_isere
     &,L_asere)
      !}

      if(L_isere.or.L_esere.or.L_asere.or.L_urere.or.L_orere.or.L_irere
     &) then      
                  I_etere = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_etere = z'40000000'
      endif
C KPJ_vlv.fgi( 136, 177):���� ���������� �������� � ���������������� ��������,20KPV46AB001
      L_elokad = (.NOT.L_odokad).OR.L_idokad
C KPG_KPH_logic.fgi(  48, 146):���
      L_erakad=L_elokad
C KPG_KPH_logic.fgi(  76, 188):������,20KPJ50AA102_ulubop
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_ipebi,4),R8_olebi
     &,I_usebi,I_itebi,I_osebi,
     & C8_amebi,I_etebi,R_umebi,R_omebi,R_ofebi,
     & REAL(R_akebi,4),R_ukebi,REAL(R_elebi,4),
     & R_ekebi,REAL(R_okebi,4),I_esebi,I_otebi,I_atebi,I_asebi
     &,L_ilebi,
     & L_avebi,L_axebi,L_alebi,L_ikebi,
     & L_ulebi,L_erakad,L_ivebi,L_ufebi,L_imebi,L_oxebi,L_apebi
     &,
     & L_epebi,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_
     &(77),L_afebi,L_(78),
     & L_efebi,L_evebi,I_utebi,L_exebi,R_urebi,REAL(R_emebi
     &,4),L_ixebi,L_amokad,
     & L_uxebi,L_ifebi,L_opebi,L_upebi,L_arebi,L_irebi,L_orebi
     &,L_erebi)
      !}

      if(L_orebi.or.L_irebi.or.L_erebi.or.L_arebi.or.L_upebi.or.L_opebi
     &) then      
                  I_isebi = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_isebi = z'40000000'
      endif
C KPJ_vlv.fgi(  30, 102):���� ���������� �������� � ���������������� ��������,20KPJ50AA102
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_ovuxe,4),R8_usuxe
     &,I_adabi,I_odabi,I_ubabi,
     & C8_etuxe,I_idabi,R_avuxe,R_utuxe,R_upuxe,
     & REAL(R_eruxe,4),R_asuxe,REAL(R_isuxe,4),
     & R_iruxe,REAL(R_uruxe,4),I_ibabi,I_udabi,I_edabi,I_ebabi
     &,L_osuxe,
     & L_efabi,L_ekabi,L_esuxe,L_oruxe,
     & L_atuxe,L_elokad,L_ofabi,L_aruxe,L_otuxe,L_ukabi,L_evuxe
     &,
     & L_ivuxe,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_
     &(73),L_epuxe,L_(74),
     & L_ipuxe,L_ifabi,I_afabi,L_ikabi,R_ababi,REAL(R_ituxe
     &,4),L_okabi,L_olokad,
     & L_alabi,L_opuxe,L_uvuxe,L_axuxe,L_exuxe,L_oxuxe,L_uxuxe
     &,L_ixuxe)
      !}

      if(L_uxuxe.or.L_oxuxe.or.L_ixuxe.or.L_exuxe.or.L_axuxe.or.L_uvuxe
     &) then      
                  I_obabi = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_obabi = z'40000000'
      endif
C KPJ_vlv.fgi(  60, 102):���� ���������� �������� � ���������������� ��������,20KPJ51AA102
      L_(167)=R_evakad.gt.R0_avakad
C KPG_KPH_logic.fgi( 347, 131):���������� >
      L_(168)=R_ovakad.gt.R0_ivakad
C KPG_KPH_logic.fgi( 347, 153):���������� >
      L_(177)=I_uvekad.ne.0
C KPG_KPH_logic.fgi( 346, 262):��������� 1->LO
      L_(178)=I_axekad.ne.0
C KPG_KPH_logic.fgi( 346, 273):��������� 1->LO
      L_ovekad=(L_(178).or.L_ovekad).and..not.(L_(177))
      L_(176)=.not.L_ovekad
C KPG_KPH_logic.fgi( 359, 271):RS �������
      L_evekad=L_ovekad
C KPG_KPH_logic.fgi( 376, 273):������,20KPJ20_AVR_option1
      L_(173)=L_evekad.and..not.L0_usekad
      L0_usekad=L_evekad
C KPG_KPH_logic.fgi( 346, 143):������������  �� 1 ���
      L_etekad = L_(173).OR.L_arekad.OR.L_upekad.OR.L_(167
     &)
C KPG_KPH_logic.fgi( 359, 140):���
      L_ivekad=(L_(177).or.L_ivekad).and..not.(L_(178))
      L_(175)=.not.L_ivekad
C KPG_KPH_logic.fgi( 359, 260):RS �������
      L_avekad=L_ivekad
C KPG_KPH_logic.fgi( 376, 262):������,20KPJ20_AVR_option2
      L_(174)=L_avekad.and..not.L0_atekad
      L0_atekad=L_avekad
C KPG_KPH_logic.fgi( 346, 165):������������  �� 1 ���
      L_itekad = L_(174).OR.L_arekad.OR.L_upekad.OR.L_(168
     &)
C KPG_KPH_logic.fgi( 359, 162):���
      L_okikad=L_isikad
C KPG_KPH_logic.fgi( 284, 268):������,20KPJ61AA102_ulubop
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_ivaxe,4),R8_osaxe
     &,I_ubexe,I_idexe,I_obexe,
     & C8_ataxe,I_edexe,R_utaxe,R_otaxe,R_opaxe,
     & REAL(R_araxe,4),R_uraxe,REAL(R_esaxe,4),
     & R_eraxe,REAL(R_oraxe,4),I_ebexe,I_odexe,I_adexe,I_abexe
     &,L_isaxe,
     & L_afexe,L_akexe,L_asaxe,L_iraxe,
     & L_usaxe,L_okikad,L_ifexe,L_upaxe,L_itaxe,L_okexe,L_avaxe
     &,
     & L_evaxe,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_
     &(67),L_apaxe,L_(68),
     & L_epaxe,L_efexe,I_udexe,L_ekexe,R_uxaxe,REAL(R_etaxe
     &,4),L_ikexe,L_akikad,
     & L_ukexe,L_ipaxe,L_ovaxe,L_uvaxe,L_axaxe,L_ixaxe,L_oxaxe
     &,L_exaxe)
      !}

      if(L_oxaxe.or.L_ixaxe.or.L_exaxe.or.L_axaxe.or.L_uvaxe.or.L_ovaxe
     &) then      
                  I_ibexe = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ibexe = z'40000000'
      endif
C KPJ_vlv.fgi( 105, 102):���� ���������� �������� � ���������������� ��������,20KPJ61AA102
      L_ukikad=L_esikad
C KPG_KPH_logic.fgi( 284, 261):������,20KPJ61AA101_ulubop
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_isexe,4),R8_opexe
     &,I_uvexe,I_ixexe,I_ovexe,
     & C8_arexe,I_exexe,R_urexe,R_orexe,R_olexe,
     & REAL(R_amexe,4),R_umexe,REAL(R_epexe,4),
     & R_emexe,REAL(R_omexe,4),I_evexe,I_oxexe,I_axexe,I_avexe
     &,L_ipexe,
     & L_abixe,L_adixe,L_apexe,L_imexe,
     & L_upexe,L_ukikad,L_ibixe,L_ulexe,L_irexe,L_odixe,L_asexe
     &,
     & L_esexe,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_
     &(69),L_alexe,L_(70),
     & L_elexe,L_ebixe,I_uxexe,L_edixe,R_utexe,REAL(R_erexe
     &,4),L_idixe,L_ekikad,
     & L_udixe,L_ilexe,L_osexe,L_usexe,L_atexe,L_itexe,L_otexe
     &,L_etexe)
      !}

      if(L_otexe.or.L_itexe.or.L_etexe.or.L_atexe.or.L_usexe.or.L_osexe
     &) then      
                  I_ivexe = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ivexe = z'40000000'
      endif
C KPJ_vlv.fgi(  90, 102):���� ���������� �������� � ���������������� ��������,20KPJ61AA101
      L_(179) = L_ekikad.OR.L_akikad
C KPG_KPH_logic.fgi( 254, 249):���
      L_(180) = L_ufikad.AND.L_(179)
C KPG_KPH_logic.fgi( 258, 252):�
      L_ikikad = L_(180).OR.L_ofikad
C KPG_KPH_logic.fgi( 264, 251):���
      L_alikad=L_elikad
C KPG_KPH_logic.fgi( 284, 275):������,20KPJ80AA101_ulubop
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_opixe,4),R8_ulixe
     &,I_atixe,I_otixe,I_usixe,
     & C8_emixe,I_itixe,R_apixe,R_umixe,R_ufixe,
     & REAL(R_ekixe,4),R_alixe,REAL(R_ilixe,4),
     & R_ikixe,REAL(R_ukixe,4),I_isixe,I_utixe,I_etixe,I_esixe
     &,L_olixe,
     & L_evixe,L_exixe,L_elixe,L_okixe,
     & L_amixe,L_alikad,L_ovixe,L_akixe,L_omixe,L_uxixe,L_epixe
     &,
     & L_ipixe,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_
     &(71),L_afixe,L_(72),
     & L_efixe,L_ivixe,I_avixe,L_ixixe,R_asixe,REAL(R_imixe
     &,4),L_oxixe,L_ifixe,
     & L_aboxe,L_ofixe,L_upixe,L_arixe,L_erixe,L_orixe,L_urixe
     &,L_irixe)
      !}

      if(L_urixe.or.L_orixe.or.L_irixe.or.L_erixe.or.L_arixe.or.L_upixe
     &) then      
                  I_osixe = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_osixe = z'40000000'
      endif
C KPJ_vlv.fgi(  75, 102):���� ���������� �������� � ���������������� ��������,20KPJ80AA101
      L_amikad=L_esikad
C KPG_KPH_logic.fgi( 198, 268):������,20KPJ82AA102_ulubop
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_ifuve,4),R8_obuve
     &,I_uluve,I_imuve,I_oluve,
     & C8_aduve,I_emuve,R_uduve,R_oduve,R_ovove,
     & REAL(R_axove,4),R_uxove,REAL(R_ebuve,4),
     & R_exove,REAL(R_oxove,4),I_eluve,I_omuve,I_amuve,I_aluve
     &,L_ibuve,
     & L_apuve,L_aruve,L_abuve,L_ixove,
     & L_ubuve,L_amikad,L_ipuve,L_uvove,L_iduve,L_oruve,L_afuve
     &,
     & L_efuve,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_
     &(63),L_avove,L_(64),
     & L_evove,L_epuve,I_umuve,L_eruve,R_ukuve,REAL(R_eduve
     &,4),L_iruve,L_ilikad,
     & L_uruve,L_ivove,L_ofuve,L_ufuve,L_akuve,L_ikuve,L_okuve
     &,L_ekuve)
      !}

      if(L_okuve.or.L_ikuve.or.L_ekuve.or.L_akuve.or.L_ufuve.or.L_ofuve
     &) then      
                  I_iluve = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_iluve = z'40000000'
      endif
C KPJ_vlv.fgi( 135, 102):���� ���������� �������� � ���������������� ��������,20KPJ82AA102
      L_emikad=L_isikad
C KPG_KPH_logic.fgi( 198, 274):������,20KPJ82AA101_ulubop
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_ibaxe,4),R8_ovuve
     &,I_ufaxe,I_ikaxe,I_ofaxe,
     & C8_axuve,I_ekaxe,R_uxuve,R_oxuve,R_osuve,
     & REAL(R_atuve,4),R_utuve,REAL(R_evuve,4),
     & R_etuve,REAL(R_otuve,4),I_efaxe,I_okaxe,I_akaxe,I_afaxe
     &,L_ivuve,
     & L_alaxe,L_amaxe,L_avuve,L_ituve,
     & L_uvuve,L_emikad,L_ilaxe,L_usuve,L_ixuve,L_omaxe,L_abaxe
     &,
     & L_ebaxe,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_
     &(65),L_asuve,L_(66),
     & L_esuve,L_elaxe,I_ukaxe,L_emaxe,R_udaxe,REAL(R_exuve
     &,4),L_imaxe,L_olikad,
     & L_umaxe,L_isuve,L_obaxe,L_ubaxe,L_adaxe,L_idaxe,L_odaxe
     &,L_edaxe)
      !}

      if(L_odaxe.or.L_idaxe.or.L_edaxe.or.L_adaxe.or.L_ubaxe.or.L_obaxe
     &) then      
                  I_ifaxe = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ifaxe = z'40000000'
      endif
C KPJ_vlv.fgi( 120, 102):���� ���������� �������� � ���������������� ��������,20KPJ82AA101
      L_ulikad = L_isikad.OR.L_esikad.OR.L_olikad.OR.L_ilikad
C KPG_KPH_logic.fgi( 177, 258):���
      L_imokad=L_omokad
C KPG_KPH_logic.fgi(  76, 194):������,20KPJ50AA101_ulubop
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_olibi,4),R8_ufibi
     &,I_aribi,I_oribi,I_upibi,
     & C8_ekibi,I_iribi,R_alibi,R_ukibi,R_ubibi,
     & REAL(R_edibi,4),R_afibi,REAL(R_ifibi,4),
     & R_idibi,REAL(R_udibi,4),I_ipibi,I_uribi,I_eribi,I_epibi
     &,L_ofibi,
     & L_esibi,L_etibi,L_efibi,L_odibi,
     & L_akibi,L_imokad,L_osibi,L_adibi,L_okibi,L_utibi,L_elibi
     &,
     & L_ilibi,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_
     &(79),L_abibi,L_(80),
     & L_ebibi,L_isibi,I_asibi,L_itibi,R_apibi,REAL(R_ikibi
     &,4),L_otibi,L_ibibi,
     & L_avibi,L_obibi,L_ulibi,L_amibi,L_emibi,L_omibi,L_umibi
     &,L_imibi)
      !}

      if(L_umibi.or.L_omibi.or.L_imibi.or.L_emibi.or.L_amibi.or.L_ulibi
     &) then      
                  I_opibi = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_opibi = z'40000000'
      endif
C KPJ_vlv.fgi(  15, 102):���� ���������� �������� � ���������������� ��������,20KPJ50AA101
      C30_epokad = '������������������'
C KPG_KPH_logic.fgi(  74, 264):��������� ���������� CH20 (CH30) (�������)
      C30_ipokad = '������������'
C KPG_KPH_logic.fgi(  98, 268):��������� ���������� CH20 (CH30) (�������)
      C30_opokad = '������'
C KPG_KPH_logic.fgi(  92, 263):��������� ���������� CH20 (CH30) (�������)
      C30_erokad = '�� ������'
C KPG_KPH_logic.fgi(  74, 266):��������� ���������� CH20 (CH30) (�������)
      I_(2) = z'01000007'
C KPG_KPH_logic.fgi( 106, 234):��������� ������������� IN (�������)
      I_(1) = z'0100000A'
C KPG_KPH_logic.fgi( 106, 232):��������� ������������� IN (�������)
      I_(4) = z'01000007'
C KPG_KPH_logic.fgi( 106, 244):��������� ������������� IN (�������)
      I_(3) = z'0100000A'
C KPG_KPH_logic.fgi( 106, 242):��������� ������������� IN (�������)
      I_(6) = z'01000007'
C KPG_KPH_logic.fgi( 106, 254):��������� ������������� IN (�������)
      I_(5) = z'0100000A'
C KPG_KPH_logic.fgi( 106, 252):��������� ������������� IN (�������)
      L_(192)=.true.
C KPG_KPH_logic.fgi(  44, 240):��������� ���������� (�������)
      L0_osokad=R0_atokad.ne.R0_usokad
      R0_usokad=R0_atokad
C KPG_KPH_logic.fgi(  31, 233):���������� ������������� ������
      if(L0_osokad) then
         L_(189)=L_(192)
      else
         L_(189)=.false.
      endif
C KPG_KPH_logic.fgi(  48, 239):���� � ������������� �������
      L_(3)=L_(189)
C KPG_KPH_logic.fgi(  48, 239):������-�������: ���������� ��� �������������� ������
      L_(193)=.true.
C KPG_KPH_logic.fgi(  44, 257):��������� ���������� (�������)
      L0_otokad=R0_avokad.ne.R0_utokad
      R0_utokad=R0_avokad
C KPG_KPH_logic.fgi(  31, 250):���������� ������������� ������
      if(L0_otokad) then
         L_(190)=L_(193)
      else
         L_(190)=.false.
      endif
C KPG_KPH_logic.fgi(  48, 256):���� � ������������� �������
      L_(2)=L_(190)
C KPG_KPH_logic.fgi(  48, 256):������-�������: ���������� ��� �������������� ������
      L_(187) = L_(2).OR.L_(3)
C KPG_KPH_logic.fgi(  58, 269):���
      L_(194)=.true.
C KPG_KPH_logic.fgi(  44, 273):��������� ���������� (�������)
      L0_ovokad=R0_axokad.ne.R0_uvokad
      R0_uvokad=R0_axokad
C KPG_KPH_logic.fgi(  31, 266):���������� ������������� ������
      if(L0_ovokad) then
         L_(191)=L_(194)
      else
         L_(191)=.false.
      endif
C KPG_KPH_logic.fgi(  48, 272):���� � ������������� �������
      L_(1)=L_(191)
C KPG_KPH_logic.fgi(  48, 272):������-�������: ���������� ��� �������������� ������
      L_(183) = L_(1).OR.L_(2)
C KPG_KPH_logic.fgi(  58, 236):���
      L_isokad=(L_(3).or.L_isokad).and..not.(L_(183))
      L_(184)=.not.L_isokad
C KPG_KPH_logic.fgi(  66, 238):RS �������
      L_esokad=L_isokad
C KPG_KPH_logic.fgi(  84, 240):������,KPG_KPH_tech_mode
      if(L_isokad) then
         I_orokad=I_(1)
      else
         I_orokad=I_(2)
      endif
C KPG_KPH_logic.fgi( 109, 232):���� RE IN LO CH7
      L_(185) = L_(1).OR.L_(3)
C KPG_KPH_logic.fgi(  58, 253):���
      L_itokad=(L_(2).or.L_itokad).and..not.(L_(185))
      L_(186)=.not.L_itokad
C KPG_KPH_logic.fgi(  66, 255):RS �������
      L_etokad=L_itokad
C KPG_KPH_logic.fgi(  84, 257):������,KPG_KPH_ruch_mode
      if(L_itokad) then
         I_urokad=I_(3)
      else
         I_urokad=I_(4)
      endif
C KPG_KPH_logic.fgi( 109, 242):���� RE IN LO CH7
      L_ivokad=(L_(1).or.L_ivokad).and..not.(L_(187))
      L_(188)=.not.L_ivokad
C KPG_KPH_logic.fgi(  66, 271):RS �������
      L_evokad=L_ivokad
C KPG_KPH_logic.fgi(  84, 273):������,KPG_KPH_avt_mode
      if(L_ivokad) then
         I_asokad=I_(5)
      else
         I_asokad=I_(6)
      endif
C KPG_KPH_logic.fgi( 109, 252):���� RE IN LO CH7
      if(L_ivokad) then
         C30_arokad=C30_epokad
      else
         C30_arokad=C30_erokad
      endif
C KPG_KPH_logic.fgi(  78, 264):���� RE IN LO CH20
      if(L_itokad) then
         C30_upokad=C30_opokad
      else
         C30_upokad=C30_arokad
      endif
C KPG_KPH_logic.fgi(  96, 263):���� RE IN LO CH20
      if(L_isokad) then
         C30_irokad=C30_ipokad
      else
         C30_irokad=C30_upokad
      endif
C KPG_KPH_logic.fgi( 105, 262):���� RE IN LO CH20
      L_(221) = (.NOT.L_exokad)
C KPG_KPH_lamp.fgi( 120,  28):�
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ulolad,L_imolad,R_emolad
     &,
     & REAL(R_omolad,4),L_(221),L_ololad,I_amolad)
      !}
C KPG_KPH_lamp.fgi( 134,  28):���������� ������� ���������,20KPG41AG002XH18
      L_(222) = (.NOT.L_ixokad)
C KPG_KPH_lamp.fgi( 120,  41):�
      !{
      Call DAT_DISCR_HANDLER(deltat,I_esolad,L_usolad,R_osolad
     &,
     & REAL(R_atolad,4),L_(222),L_asolad,I_isolad)
      !}
C KPG_KPH_lamp.fgi( 134,  40):���������� ������� ���������,20KPG41AG001XH18
      L_(195)=R8_omilad.gt.R0_emilad
C KPG_KPH_lamp.fgi(  48, 126):���������� >
      !{
      Call DAT_DISCR_HANDLER(deltat,I_uxokad,L_ibukad,R_ebukad
     &,
     & REAL(R_obukad,4),L_(195),L_oxokad,I_abukad)
      !}
C KPG_KPH_lamp.fgi(  68, 126):���������� ������� ���������,20KPH30CL001XQ01
      L_edubo=L_oxokad
C KPG_KPH_blk.fgi(  64, 201):������,20KPH30AA102_ulucl
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_ekubo,4),L_opubo,L_upubo
     &,R8_ububo,C30_efubo,
     & L_oxubo,L_ibado,L_imado,I_omubo,I_umubo,R_ofubo,R_ifubo
     &,
     & R_uvobo,REAL(R_exobo,4),R_abubo,
     & REAL(R_ibubo,4),R_ixobo,REAL(R_uxobo,4),I_ulubo,
     & I_apubo,I_imubo,I_emubo,L_obubo,L_epubo,L_orubo,L_ebubo
     &,
     & L_oxobo,L_odubo,L_idubo,L_arubo,L_axobo,L_afubo,
     & L_esubo,L_ipubo,L_ufubo,L_akubo,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_edubo,L_adubo,L_urubo,R_olubo,REAL(R_udubo,4),L_asubo
     &,L_isubo,
     & L_ikubo,L_okubo,L_ukubo,L_elubo,L_ilubo,L_alubo)
      !}

      if(L_ilubo.or.L_elubo.or.L_alubo.or.L_ukubo.or.L_okubo.or.L_ikubo
     &) then      
                  I_amubo = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_amubo = z'40000000'
      endif
C KPH_vlv.fgi( 273,  59):���� ���������� �������� ��������,20KPH30AA102
      L_axubo=L_oxokad
C KPG_KPH_blk.fgi(  64, 196):������,20KPH30AA101_ulucl
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_idado,4),L_ulado,L_amado
     &,R8_ovubo,C30_ebado,
     & L_oxubo,L_ibado,L_imado,I_ukado,I_alado,R_ubado,R_obado
     &,
     & R_osubo,REAL(R_atubo,4),R_utubo,
     & REAL(R_evubo,4),R_etubo,REAL(R_otubo,4),I_akado,
     & I_elado,I_okado,I_ikado,L_ivubo,L_ilado,L_apado,L_avubo
     &,
     & L_itubo,L_ixubo,L_exubo,L_emado,L_usubo,L_abado,
     & L_opado,L_olado,L_adado,L_edado,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_axubo,L_uvubo,L_epado,R_ufado,REAL(R_uxubo,4),L_ipado
     &,L_upado,
     & L_odado,L_udado,L_afado,L_ifado,L_ofado,L_efado)
      !}

      if(L_ofado.or.L_ifado.or.L_efado.or.L_afado.or.L_udado.or.L_odado
     &) then      
                  I_ekado = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ekado = z'40000000'
      endif
C KPH_vlv.fgi( 258,  59):���� ���������� �������� ��������,20KPH30AA101
      L_(196)=R8_omilad.lt.R0_imilad
C KPG_KPH_lamp.fgi(  48, 137):���������� <
      !{
      Call DAT_DISCR_HANDLER(deltat,I_idukad,L_afukad,R_udukad
     &,
     & REAL(R_efukad,4),L_(196),L_edukad,I_odukad)
      !}
C KPG_KPH_lamp.fgi(  68, 136):���������� ������� ���������,20KPH30CL002XQ01
      L_(197)=R8_epilad.gt.R0_umilad
C KPG_KPH_lamp.fgi(  48, 148):���������� >
      !{
      Call DAT_DISCR_HANDLER(deltat,I_akukad,L_okukad,R_ikukad
     &,
     & REAL(R_ukukad,4),L_(197),L_ufukad,I_ekukad)
      !}
C KPG_KPH_lamp.fgi(  68, 148):���������� ������� ���������,20KPG52CL002XQ01
      L_(198)=R8_epilad.lt.R0_apilad
C KPG_KPH_lamp.fgi(  48, 159):���������� <
      !{
      Call DAT_DISCR_HANDLER(deltat,I_olukad,L_emukad,R_amukad
     &,
     & REAL(R_imukad,4),L_(198),L_ilukad,I_ulukad)
      !}
C KPG_KPH_lamp.fgi(  68, 158):���������� ������� ���������,20KPG52CL003XQ01
      L_(199)=R8_opilad.gt.R0_ipilad
C KPG_KPH_lamp.fgi(  48, 170):���������� >
      !{
      Call DAT_DISCR_HANDLER(deltat,I_epukad,L_upukad,R_opukad
     &,
     & REAL(R_arukad,4),L_(199),L_apukad,I_ipukad)
      !}
C KPG_KPH_lamp.fgi(  68, 170):���������� ������� ���������,20KPG52CL001XQ01
      L_(200)=R8_arilad.gt.R0_upilad
C KPG_KPH_lamp.fgi(  48, 181):���������� >
      !{
      Call DAT_DISCR_HANDLER(deltat,I_urukad,L_isukad,R_esukad
     &,
     & REAL(R_osukad,4),L_(200),L_orukad,I_asukad)
      !}
C KPG_KPH_lamp.fgi(  68, 180):���������� ������� ���������,20KPG51CL001XQ01
      L_(201)=R8_orilad.gt.R0_erilad
C KPG_KPH_lamp.fgi(  48, 192):���������� >
      !{
      Call DAT_DISCR_HANDLER(deltat,I_itukad,L_avukad,R_utukad
     &,
     & REAL(R_evukad,4),L_(201),L_etukad,I_otukad)
      !}
C KPG_KPH_lamp.fgi(  68, 192):���������� ������� ���������,20KPH40CL001XQ01
      L_u=L_etukad
C KPG_KPH_blk.fgi(  64, 174):������,20KPH4440AA106_ulucl
      L_eredo=L_etukad
C KPG_KPH_blk.fgi(  64, 169):������,20KPH40AA103_ulucl
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_etedo,4),L_obido,L_ubido
     &,R8_upedo,C30_esedo,
     & L_ikodo,L_elodo,L_etodo,I_oxedo,I_uxedo,R_osedo,R_isedo
     &,
     & R_uledo,REAL(R_emedo,4),R_apedo,
     & REAL(R_ipedo,4),R_imedo,REAL(R_umedo,4),I_uvedo,
     & I_abido,I_ixedo,I_exedo,L_opedo,L_ebido,L_odido,L_epedo
     &,
     & L_omedo,L_oredo,L_iredo,L_adido,L_amedo,L_asedo,
     & L_efido,L_ibido,L_usedo,L_atedo,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_eredo,L_aredo,L_udido,R_ovedo,REAL(R_uredo,4),L_afido
     &,L_ifido,
     & L_itedo,L_otedo,L_utedo,L_evedo,L_ivedo,L_avedo)
      !}

      if(L_ivedo.or.L_evedo.or.L_avedo.or.L_utedo.or.L_otedo.or.L_itedo
     &) then      
                  I_axedo = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_axedo = z'40000000'
      endif
C KPH_vlv.fgi( 227,  59):���� ���������� �������� ��������,20KPH40AA103
      L_(202)=R8_orilad.lt.R0_irilad
C KPG_KPH_lamp.fgi(  48, 203):���������� <
      !{
      Call DAT_DISCR_HANDLER(deltat,I_axukad,L_oxukad,R_ixukad
     &,
     & REAL(R_uxukad,4),L_(202),L_uvukad,I_exukad)
      !}
C KPG_KPH_lamp.fgi(  68, 202):���������� ������� ���������,20KPH40CL002XQ01
      L_(203)=R8_esilad.gt.R0_urilad
C KPG_KPH_lamp.fgi(  48, 214):���������� >
      !{
      Call DAT_DISCR_HANDLER(deltat,I_obalad,L_edalad,R_adalad
     &,
     & REAL(R_idalad,4),L_(203),L_ibalad,I_ubalad)
      !}
C KPG_KPH_lamp.fgi(  68, 214):���������� ������� ���������,20KPH11CL111XQ01
      L_(204)=R8_esilad.lt.R0_asilad
C KPG_KPH_lamp.fgi(  48, 225):���������� <
      !{
      Call DAT_DISCR_HANDLER(deltat,I_efalad,L_ufalad,R_ofalad
     &,
     & REAL(R_akalad,4),L_(204),L_afalad,I_ifalad)
      !}
C KPG_KPH_lamp.fgi(  68, 224):���������� ������� ���������,20KPH11CL112XQ01
      L_(205)=R8_usilad.gt.R0_isilad
C KPG_KPH_lamp.fgi(  48, 236):���������� >
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ukalad,L_ilalad,R_elalad
     &,
     & REAL(R_olalad,4),L_(205),L_okalad,I_alalad)
      !}
C KPG_KPH_lamp.fgi(  68, 236):���������� ������� ���������,20KPH14CL002XQ01
      L_(206)=R8_usilad.lt.R0_osilad
C KPG_KPH_lamp.fgi(  48, 247):���������� <
      !{
      Call DAT_DISCR_HANDLER(deltat,I_imalad,L_apalad,R_umalad
     &,
     & REAL(R_epalad,4),L_(206),L_emalad,I_omalad)
      !}
C KPG_KPH_lamp.fgi(  68, 246):���������� ������� ���������,20KPH14CL004XQ01
      L_(207)=R8_itilad.gt.R0_atilad
C KPG_KPH_lamp.fgi(  48, 258):���������� >
      !{
      Call DAT_DISCR_HANDLER(deltat,I_aralad,L_oralad,R_iralad
     &,
     & REAL(R_uralad,4),L_(207),L_upalad,I_eralad)
      !}
C KPG_KPH_lamp.fgi(  68, 258):���������� ������� ���������,20KPH14CL001XQ01
      L_(208)=R8_itilad.lt.R0_etilad
C KPG_KPH_lamp.fgi(  48, 269):���������� <
      !{
      Call DAT_DISCR_HANDLER(deltat,I_osalad,L_etalad,R_atalad
     &,
     & REAL(R_italad,4),L_(208),L_isalad,I_usalad)
      !}
C KPG_KPH_lamp.fgi(  68, 268):���������� ������� ���������,20KPH14CL003XQ01
      L_(209)=R8_axilad.lt.R0_otilad
C KPG_KPH_lamp.fgi(  48, 324):���������� <
      !{
      Call DAT_DISCR_HANDLER(deltat,I_olelad,L_emelad,R_amelad
     &,
     & REAL(R_imelad,4),L_(209),L_ilelad,I_ulelad)
      !}
C KPG_KPH_lamp.fgi(  68, 324):���������� ������� ���������,20KPH11CL005XQ01
      L_(210)=R8_axilad.gt.R0_utilad
C KPG_KPH_lamp.fgi(  48, 302):���������� >
      !{
      Call DAT_DISCR_HANDLER(deltat,I_idelad,L_afelad,R_udelad
     &,
     & REAL(R_efelad,4),L_(210),L_edelad,I_odelad)
      !}
C KPG_KPH_lamp.fgi(  68, 302):���������� ������� ���������,20KPH11CL002XQ01
      L_(211)=R8_ivilad.gt.R0_avilad
C KPG_KPH_lamp.fgi(  48, 280):���������� >
      !{
      Call DAT_DISCR_HANDLER(deltat,I_evalad,L_uvalad,R_ovalad
     &,
     & REAL(R_axalad,4),L_(211),L_avalad,I_ivalad)
      !}
C KPG_KPH_lamp.fgi(  68, 280):���������� ������� ���������,20KPH20CL001XQ01
      L_(212)=R8_ivilad.lt.R0_evilad
C KPG_KPH_lamp.fgi(  48, 291):���������� <
      !{
      Call DAT_DISCR_HANDLER(deltat,I_uxalad,L_ibelad,R_ebelad
     &,
     & REAL(R_obelad,4),L_(212),L_oxalad,I_abelad)
      !}
C KPG_KPH_lamp.fgi(  68, 290):���������� ������� ���������,20KPH20CL002XQ01
      L_(213)=R8_axilad.gt.R0_ovilad
C KPG_KPH_lamp.fgi(  48, 313):���������� >
      !{
      Call DAT_DISCR_HANDLER(deltat,I_akelad,L_okelad,R_ikelad
     &,
     & REAL(R_ukelad,4),L_(213),L_ufelad,I_ekelad)
      !}
C KPG_KPH_lamp.fgi(  68, 312):���������� ������� ���������,20KPH11CL001XQ01
      L_(214)=R8_axilad.lt.R0_uvilad
C KPG_KPH_lamp.fgi(  48, 335):���������� <
      !{
      Call DAT_DISCR_HANDLER(deltat,I_epelad,L_upelad,R_opelad
     &,
     & REAL(R_arelad,4),L_(214),L_apelad,I_ipelad)
      !}
C KPG_KPH_lamp.fgi(  68, 334):���������� ������� ���������,20KPH11CL004XQ01
      L_(215)=R8_oxilad.gt.R0_exilad
C KPG_KPH_lamp.fgi(  48, 346):���������� >
      !{
      Call DAT_DISCR_HANDLER(deltat,I_urelad,L_iselad,R_eselad
     &,
     & REAL(R_oselad,4),L_(215),L_orelad,I_aselad)
      !}
C KPG_KPH_lamp.fgi(  68, 346):���������� ������� ���������,20KPG31CL001XQ01
      L_ukimu=L_orelad
C KPG_KPH_blk.fgi(  64, 248):������,20KPG31AA109_ulucl
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_umimu,4),L_etimu,L_itimu
     &,R8_ikimu,C30_ulimu,
     & L_odaxu,L_ifaxu,L_iraxu,I_esimu,I_isimu,R_emimu,R_amimu
     &,
     & R_idimu,REAL(R_udimu,4),R_ofimu,
     & REAL(R_akimu,4),R_afimu,REAL(R_ifimu,4),I_irimu,
     & I_osimu,I_asimu,I_urimu,L_ekimu,L_usimu,L_evimu,L_ufimu
     &,
     & L_efimu,L_elimu,L_alimu,L_otimu,L_odimu,L_olimu,
     & L_uvimu,L_atimu,L_imimu,L_omimu,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_ukimu,L_okimu,L_ivimu,R_erimu,REAL(R_ilimu,4),L_ovimu
     &,L_aximu,
     & L_apimu,L_epimu,L_ipimu,L_upimu,L_arimu,L_opimu)
      !}

      if(L_arimu.or.L_upimu.or.L_opimu.or.L_ipimu.or.L_epimu.or.L_apimu
     &) then      
                  I_orimu = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_orimu = z'40000000'
      endif
C KPG_vlv.fgi( 159, 184):���� ���������� �������� ��������,20KPG31AA109
      L_arapu=L_orelad
C KPG_KPH_blk.fgi(  64, 243):������,20KPG31AA103_ulucl
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_atapu,4),L_ibepu,L_obepu
     &,R8_opapu,C30_asapu,
     & L_odaxu,L_ifaxu,L_iraxu,I_ixapu,I_oxapu,R_isapu,R_esapu
     &,
     & R_olapu,REAL(R_amapu,4),R_umapu,
     & REAL(R_epapu,4),R_emapu,REAL(R_omapu,4),I_ovapu,
     & I_uxapu,I_exapu,I_axapu,L_ipapu,L_abepu,L_idepu,L_apapu
     &,
     & L_imapu,L_irapu,L_erapu,L_ubepu,L_ulapu,L_urapu,
     & L_afepu,L_ebepu,L_osapu,L_usapu,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_arapu,L_upapu,L_odepu,R_ivapu,REAL(R_orapu,4),L_udepu
     &,L_efepu,
     & L_etapu,L_itapu,L_otapu,L_avapu,L_evapu,L_utapu)
      !}

      if(L_evapu.or.L_avapu.or.L_utapu.or.L_otapu.or.L_itapu.or.L_etapu
     &) then      
                  I_uvapu = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_uvapu = z'40000000'
      endif
C KPG_vlv.fgi( 131, 160):���� ���������� �������� ��������,20KPG31AA103
      L_etumu=L_orelad
C KPG_KPH_blk.fgi(  64, 238):������,20KPG31AA101_ulucl
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_exumu,4),L_ofapu,L_ufapu
     &,R8_usumu,C30_evumu,
     & L_odaxu,L_ifaxu,L_iraxu,I_odapu,I_udapu,R_ovumu,R_ivumu
     &,
     & R_upumu,REAL(R_erumu,4),R_asumu,
     & REAL(R_isumu,4),R_irumu,REAL(R_urumu,4),I_ubapu,
     & I_afapu,I_idapu,I_edapu,L_osumu,L_efapu,L_okapu,L_esumu
     &,
     & L_orumu,L_otumu,L_itumu,L_akapu,L_arumu,L_avumu,
     & L_elapu,L_ifapu,L_uvumu,L_axumu,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_etumu,L_atumu,L_ukapu,R_obapu,REAL(R_utumu,4),L_alapu
     &,L_ilapu,
     & L_ixumu,L_oxumu,L_uxumu,L_ebapu,L_ibapu,L_abapu)
      !}

      if(L_ibapu.or.L_ebapu.or.L_abapu.or.L_uxumu.or.L_oxumu.or.L_ixumu
     &) then      
                  I_adapu = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_adapu = z'40000000'
      endif
C KPG_vlv.fgi( 145, 160):���� ���������� �������� ��������,20KPG31AA101
      L_ixomu=L_orelad
C KPG_KPH_blk.fgi(  64, 233):������,20KPG31AA104_ulucl
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_idumu,4),L_ulumu,L_amumu
     &,R8_axomu,C30_ibumu,
     & L_odaxu,L_ifaxu,L_iraxu,I_ukumu,I_alumu,R_ubumu,R_obumu
     &,
     & R_atomu,REAL(R_itomu,4),R_evomu,
     & REAL(R_ovomu,4),R_otomu,REAL(R_avomu,4),I_akumu,
     & I_elumu,I_okumu,I_ikumu,L_uvomu,L_ilumu,L_umumu,L_ivomu
     &,
     & L_utomu,L_uxomu,L_oxomu,L_emumu,L_etomu,L_ebumu,
     & L_ipumu,L_olumu,L_adumu,L_edumu,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_ixomu,L_exomu,L_apumu,R_ufumu,REAL(R_abumu,4),L_epumu
     &,L_opumu,
     & L_odumu,L_udumu,L_afumu,L_ifumu,L_ofumu,L_efumu)
      !}

      if(L_ofumu.or.L_ifumu.or.L_efumu.or.L_afumu.or.L_udumu.or.L_odumu
     &) then      
                  I_ekumu = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ekumu = z'40000000'
      endif
C KPG_vlv.fgi( 159, 208):���� ���������� �������� ��������,20KPG31AA104
      L_esamu=L_orelad
C KPG_KPH_blk.fgi(  64, 228):������,20KPG31AA107_ulucl
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_evamu,4),L_odemu,L_udemu
     &,R8_uramu,C30_etamu,
     & L_odaxu,L_ifaxu,L_iraxu,I_obemu,I_ubemu,R_otamu,R_itamu
     &,
     & R_umamu,REAL(R_epamu,4),R_aramu,
     & REAL(R_iramu,4),R_ipamu,REAL(R_upamu,4),I_uxamu,
     & I_ademu,I_ibemu,I_ebemu,L_oramu,L_edemu,L_ofemu,L_eramu
     &,
     & L_opamu,L_osamu,L_isamu,L_afemu,L_apamu,L_atamu,
     & L_ekemu,L_idemu,L_utamu,L_avamu,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_esamu,L_asamu,L_ufemu,R_oxamu,REAL(R_usamu,4),L_akemu
     &,L_ikemu,
     & L_ivamu,L_ovamu,L_uvamu,L_examu,L_ixamu,L_axamu)
      !}

      if(L_ixamu.or.L_examu.or.L_axamu.or.L_uvamu.or.L_ovamu.or.L_ivamu
     &) then      
                  I_abemu = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_abemu = z'40000000'
      endif
C KPG_vlv.fgi( 159, 160):���� ���������� �������� ��������,20KPG31AA107
      L_imabu=L_orelad
C KPG_KPH_blk.fgi(  64, 223):������,20KPG31AA108_ulucl
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_irabu,4),L_uvabu,L_axabu
     &,R8_amabu,C30_ipabu,
     & L_odaxu,L_ifaxu,L_iraxu,I_utabu,I_avabu,R_upabu,R_opabu
     &,
     & R_akabu,REAL(R_ikabu,4),R_elabu,
     & REAL(R_olabu,4),R_okabu,REAL(R_alabu,4),I_atabu,
     & I_evabu,I_otabu,I_itabu,L_ulabu,L_ivabu,L_uxabu,L_ilabu
     &,
     & L_ukabu,L_umabu,L_omabu,L_exabu,L_ekabu,L_epabu,
     & L_ibebu,L_ovabu,L_arabu,L_erabu,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_imabu,L_emabu,L_abebu,R_usabu,REAL(R_apabu,4),L_ebebu
     &,L_obebu,
     & L_orabu,L_urabu,L_asabu,L_isabu,L_osabu,L_esabu)
      !}

      if(L_osabu.or.L_isabu.or.L_esabu.or.L_asabu.or.L_urabu.or.L_orabu
     &) then      
                  I_etabu = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_etabu = z'40000000'
      endif
C KPG_vlv.fgi( 187, 136):���� ���������� �������� ��������,20KPG31AA108
      L_(216)=R8_oxilad.lt.R0_ixilad
C KPG_KPH_lamp.fgi(  48, 357):���������� <
      !{
      Call DAT_DISCR_HANDLER(deltat,I_itelad,L_avelad,R_utelad
     &,
     & REAL(R_evelad,4),L_(216),L_etelad,I_otelad)
      !}
C KPG_KPH_lamp.fgi(  68, 356):���������� ������� ���������,20KPG31CL002XQ01
      L_(217)=R8_ebolad.gt.R0_uxilad
C KPG_KPH_lamp.fgi(  48, 368):���������� >
      !{
      Call DAT_DISCR_HANDLER(deltat,I_axelad,L_oxelad,R_ixelad
     &,
     & REAL(R_uxelad,4),L_(217),L_uvelad,I_exelad)
      !}
C KPG_KPH_lamp.fgi(  68, 368):���������� ������� ���������,20KPG21CL001XQ01
      L_axuku=L_uvelad
C KPG_KPH_blk.fgi(  64, 320):������,20KPG21AA105_ulucl
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_adalu,4),L_ilalu,L_olalu
     &,R8_ovuku,C30_abalu,
     & L_ufolu,L_okolu,L_osolu,I_ikalu,I_okalu,R_ibalu,R_ebalu
     &,
     & R_osuku,REAL(R_atuku,4),R_utuku,
     & REAL(R_evuku,4),R_etuku,REAL(R_otuku,4),I_ofalu,
     & I_ukalu,I_ekalu,I_akalu,L_ivuku,L_alalu,L_imalu,L_avuku
     &,
     & L_ituku,L_ixuku,L_exuku,L_ulalu,L_usuku,L_uxuku,
     & L_apalu,L_elalu,L_obalu,L_ubalu,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_axuku,L_uvuku,L_omalu,R_ifalu,REAL(R_oxuku,4),L_umalu
     &,L_epalu,
     & L_edalu,L_idalu,L_odalu,L_afalu,L_efalu,L_udalu)
      !}

      if(L_efalu.or.L_afalu.or.L_udalu.or.L_odalu.or.L_idalu.or.L_edalu
     &) then      
                  I_ufalu = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ufalu = z'40000000'
      endif
C KPG_vlv.fgi( 173,  64):���� ���������� �������� ��������,20KPG21AA105
      L_usalu=L_uvelad
C KPG_KPH_blk.fgi(  64, 315):������,20KPG21AA109_ulucl
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_uvalu,4),L_efelu,L_ifelu
     &,R8_isalu,C30_utalu,
     & L_ufolu,L_okolu,L_osolu,I_edelu,I_idelu,R_evalu,R_avalu
     &,
     & R_ipalu,REAL(R_upalu,4),R_oralu,
     & REAL(R_asalu,4),R_aralu,REAL(R_iralu,4),I_ibelu,
     & I_odelu,I_adelu,I_ubelu,L_esalu,L_udelu,L_ekelu,L_uralu
     &,
     & L_eralu,L_etalu,L_atalu,L_ofelu,L_opalu,L_otalu,
     & L_ukelu,L_afelu,L_ivalu,L_ovalu,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_usalu,L_osalu,L_ikelu,R_ebelu,REAL(R_italu,4),L_okelu
     &,L_alelu,
     & L_axalu,L_exalu,L_ixalu,L_uxalu,L_abelu,L_oxalu)
      !}

      if(L_abelu.or.L_uxalu.or.L_oxalu.or.L_ixalu.or.L_exalu.or.L_axalu
     &) then      
                  I_obelu = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_obelu = z'40000000'
      endif
C KPG_vlv.fgi( 159,  40):���� ���������� �������� ��������,20KPG21AA109
      L_atefu=L_uvelad
C KPG_KPH_blk.fgi(  64, 310):������,20KPG21AA112_ulucl
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_axefu,4),L_ififu,L_ofifu
     &,R8_osefu,C30_avefu,
     & L_ufolu,L_okolu,L_osolu,I_idifu,I_odifu,R_ivefu,R_evefu
     &,
     & R_opefu,REAL(R_arefu,4),R_urefu,
     & REAL(R_esefu,4),R_erefu,REAL(R_orefu,4),I_obifu,
     & I_udifu,I_edifu,I_adifu,L_isefu,L_afifu,L_ikifu,L_asefu
     &,
     & L_irefu,L_itefu,L_etefu,L_ufifu,L_upefu,L_utefu,
     & L_alifu,L_efifu,L_ovefu,L_uvefu,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_atefu,L_usefu,L_okifu,R_ibifu,REAL(R_otefu,4),L_ukifu
     &,L_elifu,
     & L_exefu,L_ixefu,L_oxefu,L_abifu,L_ebifu,L_uxefu)
      !}

      if(L_ebifu.or.L_abifu.or.L_uxefu.or.L_oxefu.or.L_ixefu.or.L_exefu
     &) then      
                  I_ubifu = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ubifu = z'40000000'
      endif
C KPG_vlv.fgi( 131,  16):���� ���������� �������� ��������,20KPG21AA112
      L_ililu=L_uvelad
C KPG_KPH_blk.fgi(  64, 285):������,20KPG21AA101_ulucl
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_ipilu,4),L_utilu,L_avilu
     &,R8_alilu,C30_imilu,
     & L_ufolu,L_okolu,L_osolu,I_usilu,I_atilu,R_umilu,R_omilu
     &,
     & R_afilu,REAL(R_ifilu,4),R_ekilu,
     & REAL(R_okilu,4),R_ofilu,REAL(R_akilu,4),I_asilu,
     & I_etilu,I_osilu,I_isilu,L_ukilu,L_itilu,L_uvilu,L_ikilu
     &,
     & L_ufilu,L_ulilu,L_olilu,L_evilu,L_efilu,L_emilu,
     & L_ixilu,L_otilu,L_apilu,L_epilu,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_ililu,L_elilu,L_axilu,R_urilu,REAL(R_amilu,4),L_exilu
     &,L_oxilu,
     & L_opilu,L_upilu,L_arilu,L_irilu,L_orilu,L_erilu)
      !}

      if(L_orilu.or.L_irilu.or.L_erilu.or.L_arilu.or.L_upilu.or.L_opilu
     &) then      
                  I_esilu = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_esilu = z'40000000'
      endif
C KPG_vlv.fgi( 131,  40):���� ���������� �������� ��������,20KPG21AA101
      L_efolu=L_uvelad
C KPG_KPH_blk.fgi(  64, 280):������,20KPG21AA102_ulucl
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_ololu,4),L_asolu,L_esolu
     &,R8_udolu,C30_ikolu,
     & L_ufolu,L_okolu,L_osolu,I_arolu,I_erolu,R_alolu,R_ukolu
     &,
     & R_uxilu,REAL(R_ebolu,4),R_adolu,
     & REAL(R_idolu,4),R_ibolu,REAL(R_ubolu,4),I_epolu,
     & I_irolu,I_upolu,I_opolu,L_odolu,L_orolu,L_etolu,L_edolu
     &,
     & L_obolu,L_ofolu,L_ifolu,L_isolu,L_abolu,L_ekolu,
     & L_utolu,L_urolu,L_elolu,L_ilolu,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_efolu,L_afolu,L_itolu,R_apolu,REAL(R_akolu,4),L_otolu
     &,L_avolu,
     & L_ulolu,L_amolu,L_emolu,L_omolu,L_umolu,L_imolu)
      !}

      if(L_umolu.or.L_omolu.or.L_imolu.or.L_emolu.or.L_amolu.or.L_ulolu
     &) then      
                  I_ipolu = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ipolu = z'40000000'
      endif
C KPG_vlv.fgi( 117,  40):���� ���������� �������� ��������,20KPG21AA102
      L_omiku=L_uvelad
C KPG_KPH_blk.fgi(  64, 290):������,20KPG21AA108_ulucl
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_oriku,4),L_axiku,L_exiku
     &,R8_emiku,C30_opiku,
     & L_ufolu,L_okolu,L_osolu,I_aviku,I_eviku,R_ariku,R_upiku
     &,
     & R_ekiku,REAL(R_okiku,4),R_iliku,
     & REAL(R_uliku,4),R_ukiku,REAL(R_eliku,4),I_etiku,
     & I_iviku,I_utiku,I_otiku,L_amiku,L_oviku,L_aboku,L_oliku
     &,
     & L_aliku,L_apiku,L_umiku,L_ixiku,L_ikiku,L_ipiku,
     & L_oboku,L_uviku,L_eriku,L_iriku,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_omiku,L_imiku,L_eboku,R_atiku,REAL(R_epiku,4),L_iboku
     &,L_uboku,
     & L_uriku,L_asiku,L_esiku,L_osiku,L_usiku,L_isiku)
      !}

      if(L_usiku.or.L_osiku.or.L_isiku.or.L_esiku.or.L_asiku.or.L_uriku
     &) then      
                  I_itiku = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_itiku = z'40000000'
      endif
C KPG_vlv.fgi( 117, 136):���� ���������� �������� ��������,20KPG21AA108
      L_eduku=L_uvelad
C KPG_KPH_blk.fgi(  64, 295):������,20KPG21AA106_ulucl
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_ekuku,4),L_opuku,L_upuku
     &,R8_ubuku,C30_efuku,
     & L_ufolu,L_okolu,L_osolu,I_omuku,I_umuku,R_ofuku,R_ifuku
     &,
     & R_uvoku,REAL(R_exoku,4),R_abuku,
     & REAL(R_ibuku,4),R_ixoku,REAL(R_uxoku,4),I_uluku,
     & I_apuku,I_imuku,I_emuku,L_obuku,L_epuku,L_oruku,L_ebuku
     &,
     & L_oxoku,L_oduku,L_iduku,L_aruku,L_axoku,L_afuku,
     & L_esuku,L_ipuku,L_ufuku,L_akuku,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_eduku,L_aduku,L_uruku,R_oluku,REAL(R_uduku,4),L_asuku
     &,L_isuku,
     & L_ikuku,L_okuku,L_ukuku,L_eluku,L_iluku,L_aluku)
      !}

      if(L_iluku.or.L_eluku.or.L_aluku.or.L_ukuku.or.L_okuku.or.L_ikuku
     &) then      
                  I_amuku = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_amuku = z'40000000'
      endif
C KPG_vlv.fgi( 145, 232):���� ���������� �������� ��������,20KPG21AA106
      L_idafu=L_uvelad
C KPG_KPH_blk.fgi(  64, 300):������,20KPG21AA104_ulucl
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_ikafu,4),L_upafu,L_arafu
     &,R8_adafu,C30_ifafu,
     & L_ufolu,L_okolu,L_osolu,I_umafu,I_apafu,R_ufafu,R_ofafu
     &,
     & R_axudu,REAL(R_ixudu,4),R_ebafu,
     & REAL(R_obafu,4),R_oxudu,REAL(R_abafu,4),I_amafu,
     & I_epafu,I_omafu,I_imafu,L_ubafu,L_ipafu,L_urafu,L_ibafu
     &,
     & L_uxudu,L_udafu,L_odafu,L_erafu,L_exudu,L_efafu,
     & L_isafu,L_opafu,L_akafu,L_ekafu,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_idafu,L_edafu,L_asafu,R_ulafu,REAL(R_afafu,4),L_esafu
     &,L_osafu,
     & L_okafu,L_ukafu,L_alafu,L_ilafu,L_olafu,L_elafu)
      !}

      if(L_olafu.or.L_ilafu.or.L_elafu.or.L_alafu.or.L_ukafu.or.L_okafu
     &) then      
                  I_emafu = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_emafu = z'40000000'
      endif
C KPG_vlv.fgi( 159,  16):���� ���������� �������� ��������,20KPG21AA104
      L_upifu=L_uvelad
C KPG_KPH_blk.fgi(  64, 305):������,20KPG21AA111_ulucl
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_usifu,4),L_ebofu,L_ibofu
     &,R8_ipifu,C30_urifu,
     & L_ufolu,L_okolu,L_osolu,I_exifu,I_ixifu,R_esifu,R_asifu
     &,
     & R_ilifu,REAL(R_ulifu,4),R_omifu,
     & REAL(R_apifu,4),R_amifu,REAL(R_imifu,4),I_ivifu,
     & I_oxifu,I_axifu,I_uvifu,L_epifu,L_uxifu,L_edofu,L_umifu
     &,
     & L_emifu,L_erifu,L_arifu,L_obofu,L_olifu,L_orifu,
     & L_udofu,L_abofu,L_isifu,L_osifu,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_upifu,L_opifu,L_idofu,R_evifu,REAL(R_irifu,4),L_odofu
     &,L_afofu,
     & L_atifu,L_etifu,L_itifu,L_utifu,L_avifu,L_otifu)
      !}

      if(L_avifu.or.L_utifu.or.L_otifu.or.L_itifu.or.L_etifu.or.L_atifu
     &) then      
                  I_ovifu = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ovifu = z'40000000'
      endif
C KPG_vlv.fgi( 117,  16):���� ���������� �������� ��������,20KPG21AA111
      L_(218)=R8_ebolad.lt.R0_abolad
C KPG_KPH_lamp.fgi(  48, 379):���������� <
      !{
      Call DAT_DISCR_HANDLER(deltat,I_obilad,L_edilad,R_adilad
     &,
     & REAL(R_idilad,4),L_(218),L_ibilad,I_ubilad)
      !}
C KPG_KPH_lamp.fgi(  68, 378):���������� ������� ���������,20KPG21CL002XQ01
      L_(219)=R8_ubolad.gt.R0_ibolad
C KPG_KPH_lamp.fgi(  48, 390):���������� >
      !{
      Call DAT_DISCR_HANDLER(deltat,I_efilad,L_ufilad,R_ofilad
     &,
     & REAL(R_akilad,4),L_(219),L_afilad,I_ifilad)
      !}
C KPG_KPH_lamp.fgi(  68, 390):���������� ������� ���������,20KPG11CL001XQ01
      L_abevu=L_afilad
C KPG_KPH_blk.fgi(  64, 402):������,20KPG11AA105_ulucl
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_afevu,4),L_imevu,L_omevu
     &,R8_oxavu,C30_adevu,
     & L_alafad,L_ulafad,L_utafad,I_ilevu,I_olevu,R_idevu
     &,R_edevu,
     & R_otavu,REAL(R_avavu,4),R_uvavu,
     & REAL(R_exavu,4),R_evavu,REAL(R_ovavu,4),I_okevu,
     & I_ulevu,I_elevu,I_alevu,L_ixavu,L_amevu,L_ipevu,L_axavu
     &,
     & L_ivavu,L_ibevu,L_ebevu,L_umevu,L_utavu,L_ubevu,
     & L_arevu,L_emevu,L_odevu,L_udevu,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_abevu,L_uxavu,L_opevu,R_ikevu,REAL(R_obevu,4),L_upevu
     &,L_erevu,
     & L_efevu,L_ifevu,L_ofevu,L_akevu,L_ekevu,L_ufevu)
      !}

      if(L_ekevu.or.L_akevu.or.L_ufevu.or.L_ofevu.or.L_ifevu.or.L_efevu
     &) then      
                  I_ukevu = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ukevu = z'40000000'
      endif
C KPG_vlv.fgi( 103,  88):���� ���������� �������� ��������,20KPG11AA105
      L_usitu=L_afilad
C KPG_KPH_blk.fgi(  64, 397):������,20KPG11AA109_ulucl
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_uvitu,4),L_efotu,L_ifotu
     &,R8_isitu,C30_utitu,
     & L_alafad,L_ulafad,L_utafad,I_edotu,I_idotu,R_evitu
     &,R_avitu,
     & R_ipitu,REAL(R_upitu,4),R_oritu,
     & REAL(R_asitu,4),R_aritu,REAL(R_iritu,4),I_ibotu,
     & I_odotu,I_adotu,I_ubotu,L_esitu,L_udotu,L_ekotu,L_uritu
     &,
     & L_eritu,L_etitu,L_atitu,L_ofotu,L_opitu,L_otitu,
     & L_ukotu,L_afotu,L_ivitu,L_ovitu,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_usitu,L_ositu,L_ikotu,R_ebotu,REAL(R_ititu,4),L_okotu
     &,L_alotu,
     & L_axitu,L_exitu,L_ixitu,L_uxitu,L_abotu,L_oxitu)
      !}

      if(L_abotu.or.L_uxitu.or.L_oxitu.or.L_ixitu.or.L_exitu.or.L_axitu
     &) then      
                  I_obotu = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_obotu = z'40000000'
      endif
C KPG_vlv.fgi( 159,  88):���� ���������� �������� ��������,20KPG11AA109
      L_efavu=L_afilad
C KPG_KPH_blk.fgi(  64, 392):������,20KPG11AA106_ulucl
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_elavu,4),L_oravu,L_uravu
     &,R8_udavu,C30_ekavu,
     & L_alafad,L_ulafad,L_utafad,I_opavu,I_upavu,R_okavu
     &,R_ikavu,
     & R_uxutu,REAL(R_ebavu,4),R_adavu,
     & REAL(R_idavu,4),R_ibavu,REAL(R_ubavu,4),I_umavu,
     & I_aravu,I_ipavu,I_epavu,L_odavu,L_eravu,L_osavu,L_edavu
     &,
     & L_obavu,L_ofavu,L_ifavu,L_asavu,L_abavu,L_akavu,
     & L_etavu,L_iravu,L_ukavu,L_alavu,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_efavu,L_afavu,L_usavu,R_omavu,REAL(R_ufavu,4),L_atavu
     &,L_itavu,
     & L_ilavu,L_olavu,L_ulavu,L_emavu,L_imavu,L_amavu)
      !}

      if(L_imavu.or.L_emavu.or.L_amavu.or.L_ulavu.or.L_olavu.or.L_ilavu
     &) then      
                  I_apavu = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_apavu = z'40000000'
      endif
C KPG_vlv.fgi( 117,  88):���� ���������� �������� ��������,20KPG11AA106
      L_ilutu=L_afilad
C KPG_KPH_blk.fgi(  64, 387):������,20KPG11AA107_ulucl
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_iputu,4),L_ututu,L_avutu
     &,R8_alutu,C30_imutu,
     & L_alafad,L_ulafad,L_utafad,I_usutu,I_atutu,R_umutu
     &,R_omutu,
     & R_afutu,REAL(R_ifutu,4),R_ekutu,
     & REAL(R_okutu,4),R_ofutu,REAL(R_akutu,4),I_asutu,
     & I_etutu,I_osutu,I_isutu,L_ukutu,L_itutu,L_uvutu,L_ikutu
     &,
     & L_ufutu,L_ulutu,L_olutu,L_evutu,L_efutu,L_emutu,
     & L_ixutu,L_otutu,L_aputu,L_eputu,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_ilutu,L_elutu,L_axutu,R_urutu,REAL(R_amutu,4),L_exutu
     &,L_oxutu,
     & L_oputu,L_uputu,L_arutu,L_irutu,L_orutu,L_erutu)
      !}

      if(L_orutu.or.L_irutu.or.L_erutu.or.L_arutu.or.L_uputu.or.L_oputu
     &) then      
                  I_esutu = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_esutu = z'40000000'
      endif
C KPG_vlv.fgi( 131,  88):���� ���������� �������� ��������,20KPG11AA107
      L_olasu=L_afilad
C KPG_KPH_blk.fgi(  64, 382):������,20KPG11AA111_ulucl
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_opasu,4),L_avasu,L_evasu
     &,R8_elasu,C30_omasu,
     & L_alafad,L_ulafad,L_utafad,I_atasu,I_etasu,R_apasu
     &,R_umasu,
     & R_efasu,REAL(R_ofasu,4),R_ikasu,
     & REAL(R_ukasu,4),R_ufasu,REAL(R_ekasu,4),I_esasu,
     & I_itasu,I_usasu,I_osasu,L_alasu,L_otasu,L_axasu,L_okasu
     &,
     & L_akasu,L_amasu,L_ulasu,L_ivasu,L_ifasu,L_imasu,
     & L_oxasu,L_utasu,L_epasu,L_ipasu,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_olasu,L_ilasu,L_exasu,R_asasu,REAL(R_emasu,4),L_ixasu
     &,L_uxasu,
     & L_upasu,L_arasu,L_erasu,L_orasu,L_urasu,L_irasu)
      !}

      if(L_urasu.or.L_orasu.or.L_irasu.or.L_erasu.or.L_arasu.or.L_upasu
     &) then      
                  I_isasu = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_isasu = z'40000000'
      endif
C KPG_vlv.fgi( 173, 256):���� ���������� �������� ��������,20KPG11AA111
      L_ud=L_afilad
C KPG_KPH_blk.fgi(  64, 377):������,20KPG11AA1022_ulucl
      L_orivu=L_afilad
C KPG_KPH_blk.fgi(  64, 372):������,20KPG11AA103_ulucl
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_otivu,4),L_adovu,L_edovu
     &,R8_erivu,C30_osivu,
     & L_alafad,L_ulafad,L_utafad,I_abovu,I_ebovu,R_ativu
     &,R_usivu,
     & R_emivu,REAL(R_omivu,4),R_ipivu,
     & REAL(R_upivu,4),R_umivu,REAL(R_epivu,4),I_exivu,
     & I_ibovu,I_uxivu,I_oxivu,L_arivu,L_obovu,L_afovu,L_opivu
     &,
     & L_apivu,L_asivu,L_urivu,L_idovu,L_imivu,L_isivu,
     & L_ofovu,L_ubovu,L_etivu,L_itivu,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_orivu,L_irivu,L_efovu,R_axivu,REAL(R_esivu,4),L_ifovu
     &,L_ufovu,
     & L_utivu,L_avivu,L_evivu,L_ovivu,L_uvivu,L_ivivu)
      !}

      if(L_uvivu.or.L_ovivu.or.L_ivivu.or.L_evivu.or.L_avivu.or.L_utivu
     &) then      
                  I_ixivu = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ixivu = z'40000000'
      endif
C KPG_vlv.fgi( 159, 112):���� ���������� �������� ��������,20KPG11AA103
      L_(220)=R8_ubolad.lt.R0_obolad
C KPG_KPH_lamp.fgi(  48, 401):���������� <
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ukilad,L_ililad,R_elilad
     &,
     & REAL(R_olilad,4),L_(220),L_okilad,I_alilad)
      !}
C KPG_KPH_lamp.fgi(  68, 400):���������� ������� ���������,20KPG11CL002XQ01
      !{
      Call DAT_DISCR_HANDLER(deltat,I_edolad,L_udolad,R_odolad
     &,
     & REAL(R_afolad,4),L_efolad,L_adolad,I_idolad)
      !}
C KPG_KPH_lamp.fgi( 180,  40):���������� ������� ���������,20KPG61AT001XH17
      !{
      Call DAT_DISCR_HANDLER(deltat,I_akolad,L_okolad,R_ikolad
     &,
     & REAL(R_ukolad,4),L_alolad,L_ufolad,I_ekolad)
      !}
C KPG_KPH_lamp.fgi( 180,  52):���������� ������� ���������,20KPG61AT001XH15
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ipolad,L_arolad,R_upolad
     &,
     & REAL(R_erolad,4),L_irolad,L_epolad,I_opolad)
      !}
C KPG_KPH_lamp.fgi( 156,  52):���������� ������� ���������,20KPG41AG002XH17
      !{
      Call DAT_DISCR_HANDLER(deltat,I_utolad,L_ivolad,R_evolad
     &,
     & REAL(R_ovolad,4),L_uvolad,L_otolad,I_avolad)
      !}
C KPG_KPH_lamp.fgi( 134,  52):���������� ������� ���������,20KPG41AG001XH17
      !{
      Call DAT_DISCR_HANDLER(deltat,I_oxolad,L_ebulad,R_abulad
     &,
     & REAL(R_ibulad,4),L_obulad,L_ixolad,I_uxolad)
      !}
C KPG_KPH_lamp.fgi( 133, 106):���������� ������� ���������,20KPG32CL004XQ01
      !{
      Call DAT_DISCR_HANDLER(deltat,I_idulad,L_afulad,R_udulad
     &,
     & REAL(R_efulad,4),L_ifulad,L_edulad,I_odulad)
      !}
C KPG_KPH_lamp.fgi( 276, 161):���������� ������� ���������,20KPJ70CL004XQ01
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ekulad,L_ukulad,R_okulad
     &,
     & REAL(R_alulad,4),L_elulad,L_akulad,I_ikulad)
      !}
C KPG_KPH_lamp.fgi( 276, 172):���������� ������� ���������,20KPF40CL004XQ01
      !{
      Call DAT_DISCR_HANDLER(deltat,I_amulad,L_omulad,R_imulad
     &,
     & REAL(R_umulad,4),L_apulad,L_ululad,I_emulad)
      !}
C KPG_KPH_lamp.fgi( 133, 117):���������� ������� ���������,20KPG31CL004XQ01
      !{
      Call DAT_DISCR_HANDLER(deltat,I_upulad,L_irulad,R_erulad
     &,
     & REAL(R_orulad,4),L_urulad,L_opulad,I_arulad)
      !}
C KPG_KPH_lamp.fgi( 133, 128):���������� ������� ���������,20KPG11CL004XQ01
      !{
      Call DAT_DISCR_HANDLER(deltat,I_osulad,L_etulad,R_atulad
     &,
     & REAL(R_itulad,4),L_otulad,L_isulad,I_usulad)
      !}
C KPG_KPH_lamp.fgi( 240, 117):���������� ������� ���������,20KPH51CL004XQ01
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ivulad,L_axulad,R_uvulad
     &,
     & REAL(R_exulad,4),L_ixulad,L_evulad,I_ovulad)
      !}
C KPG_KPH_lamp.fgi( 240, 128):���������� ������� ���������,20KPH50CL004XQ01
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ebamad,L_ubamad,R_obamad
     &,
     & REAL(R_adamad,4),L_edamad,L_abamad,I_ibamad)
      !}
C KPG_KPH_lamp.fgi( 240, 139):���������� ������� ���������,20KPH30CL004XQ01
      !{
      Call DAT_DISCR_HANDLER(deltat,I_afamad,L_ofamad,R_ifamad
     &,
     & REAL(R_ufamad,4),L_akamad,L_udamad,I_efamad)
      !}
C KPG_KPH_lamp.fgi( 240, 150):���������� ������� ���������,20KPH10CL004XQ01
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ukamad,L_ilamad,R_elamad
     &,
     & REAL(R_olamad,4),L_ulamad,L_okamad,I_alamad)
      !}
C KPG_KPH_lamp.fgi( 214, 150):���������� ������� ���������,20KPH20CL005XQ01
      !{
      Call DAT_DISCR_HANDLER(deltat,I_omamad,L_epamad,R_apamad
     &,
     & REAL(R_ipamad,4),L_opamad,L_imamad,I_umamad)
      !}
C KPG_KPH_lamp.fgi( 188, 172):���������� ������� ���������,20KPH51CL001XQ01
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_idapi,4),R8_oxumi
     &,I_ukapi,I_ilapi,I_okapi,
     & C8_abapi,I_elapi,R_ubapi,R_obapi,R_otumi,
     & REAL(R_avumi,4),R_uvumi,REAL(R_exumi,4),
     & R_evumi,REAL(R_ovumi,4),I_ekapi,I_olapi,I_alapi,I_akapi
     &,L_ixumi,
     & L_amapi,L_apapi,L_axumi,L_ivumi,
     & L_uxumi,L_alekad,L_imapi,L_utumi,L_ibapi,L_opapi,L_adapi
     &,
     & L_edapi,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_
     &(139),L_etumi,L_(140),
     & L_itumi,L_emapi,I_ulapi,L_epapi,R_ufapi,REAL(R_ebapi
     &,4),L_ipapi,L_imekad,
     & L_upapi,L_usakad,L_odapi,L_udapi,L_afapi,L_ifapi,L_ofapi
     &,L_efapi)
      !}

      if(L_ofapi.or.L_ifapi.or.L_efapi.or.L_afapi.or.L_udapi.or.L_odapi
     &) then      
                  I_ikapi = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ikapi = z'40000000'
      endif
C KPJ_vlv.fgi(  30, 202):���� ���������� �������� � ���������������� ��������,20KPJ21AA102
C label 1315  try1315=try1315-1
      L_idekad=L_imekad
C KPG_KPH_logic.fgi( 466, 159):������,20KPJ11AA104_ulubop
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_avave,4),R8_esave
     &,I_ibeve,I_adeve,I_ebeve,
     & C8_osave,I_ubeve,R_itave,R_etave,R_epave,
     & REAL(R_opave,4),R_irave,REAL(R_urave,4),
     & R_upave,REAL(R_erave,4),I_uxave,I_edeve,I_obeve,I_oxave
     &,L_asave,
     & L_odeve,L_ofeve,L_orave,L_arave,
     & L_isave,L_idekad,L_afeve,L_ipave,L_atave,L_ekeve,L_otave
     &,
     & L_utave,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_
     &(55),L_omave,L_(56),
     & L_umave,L_udeve,I_ideve,L_ufeve,R_ixave,REAL(R_usave
     &,4),L_akeve,L_elekad,
     & L_ikeve,L_apave,L_evave,L_ivave,L_ovave,L_axave,L_exave
     &,L_uvave)
      !}

      if(L_exave.or.L_axave.or.L_uvave.or.L_ovave.or.L_ivave.or.L_evave
     &) then      
                  I_abeve = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_abeve = z'40000000'
      endif
C KPJ_vlv.fgi(  60,  77):���� ���������� �������� � ���������������� ��������,20KPJ11AA104
      L_alekad=L_elekad
C KPG_KPH_logic.fgi( 466, 172):������,20KPJ21AA102_ulubop
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_utudi,4),R8_asudi
     &,I_ebafi,I_ubafi,I_abafi,
     & C8_isudi,I_obafi,R_etudi,R_atudi,R_apudi,
     & REAL(R_ipudi,4),R_erudi,REAL(R_orudi,4),
     & R_opudi,REAL(R_arudi,4),I_oxudi,I_adafi,I_ibafi,I_ixudi
     &,L_urudi,
     & L_idafi,L_ifafi,L_irudi,L_upudi,
     & L_esudi,L_ibokad,L_udafi,L_epudi,L_usudi,L_akafi,L_itudi
     &,
     & L_otudi,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_
     &(97),L_imudi,L_(98),
     & L_omudi,L_odafi,I_edafi,L_ofafi,R_exudi,REAL(R_osudi
     &,4),L_ufafi,L_umudi,
     & L_ekafi,L_ofokad,L_avudi,L_evudi,L_ivudi,L_uvudi,L_axudi
     &,L_ovudi)
      !}

      if(L_axudi.or.L_uvudi.or.L_ovudi.or.L_ivudi.or.L_evudi.or.L_avudi
     &) then      
                  I_uxudi = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_uxudi = z'40000000'
      endif
C KPJ_vlv.fgi(  15, 127):���� ���������� �������� � ���������������� ��������,20KPJ30AA101
C label 1327  try1327=try1327-1
      L_okokad = L_ukokad.OR.L_ekokad.OR.L_ofokad
C KPG_KPH_logic.fgi( 173, 156):���
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_afodi,4),R8_ebodi
     &,I_ilodi,I_amodi,I_elodi,
     & C8_obodi,I_ulodi,R_idodi,R_edodi,R_evidi,
     & REAL(R_ovidi,4),R_ixidi,REAL(R_uxidi,4),
     & R_uvidi,REAL(R_exidi,4),I_ukodi,I_emodi,I_olodi,I_okodi
     &,L_abodi,
     & L_omodi,L_opodi,L_oxidi,L_axidi,
     & L_ibodi,L_okokad,L_apodi,L_ividi,L_adodi,L_erodi,L_ododi
     &,
     & L_udodi,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_
     &(93),L_utidi,L_(94),
     & L_avidi,L_umodi,I_imodi,L_upodi,R_ikodi,REAL(R_ubodi
     &,4),L_arodi,L_axakad,
     & L_irodi,L_akokad,L_efodi,L_ifodi,L_ofodi,L_akodi,L_ekodi
     &,L_ufodi)
      !}

      if(L_ekodi.or.L_akodi.or.L_ufodi.or.L_ofodi.or.L_ifodi.or.L_efodi
     &) then      
                  I_alodi = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_alodi = z'40000000'
      endif
C KPJ_vlv.fgi(  45, 127):���� ���������� �������� � ���������������� ��������,20KPJ30AA103
      L_axikad=L_udokad
C KPG_KPH_logic.fgi( 196,   9):������,20KPJ30AA109_ulubop
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_ofobi,4),R8_ubobi
     &,I_amobi,I_omobi,I_ulobi,
     & C8_edobi,I_imobi,R_afobi,R_udobi,R_uvibi,
     & REAL(R_exibi,4),R_abobi,REAL(R_ibobi,4),
     & R_ixibi,REAL(R_uxibi,4),I_ilobi,I_umobi,I_emobi,I_elobi
     &,L_obobi,
     & L_epobi,L_erobi,L_ebobi,L_oxibi,
     & L_adobi,L_axikad,L_opobi,L_axibi,L_odobi,L_urobi,L_efobi
     &,
     & L_ifobi,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_
     &(81),L_evibi,L_(82),
     & L_ivibi,L_ipobi,I_apobi,L_irobi,R_alobi,REAL(R_idobi
     &,4),L_orobi,L_ovibi,
     & L_asobi,L_exikad,L_ufobi,L_akobi,L_ekobi,L_okobi,L_ukobi
     &,L_ikobi)
      !}

      if(L_ukobi.or.L_okobi.or.L_ikobi.or.L_ekobi.or.L_akobi.or.L_ufobi
     &) then      
                  I_olobi = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_olobi = z'40000000'
      endif
C KPJ_vlv.fgi( 135, 127):���� ���������� �������� � ���������������� ��������,20KPJ30AA109
      L_obokad = L_ofokad.OR.L_ekokad.OR.L_ixikad.OR.L_exikad
C KPG_KPH_logic.fgi( 174,  25):���
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_ibubi,4),R8_ovobi
     &,I_ufubi,I_ikubi,I_ofubi,
     & C8_axobi,I_ekubi,R_uxobi,R_oxobi,R_osobi,
     & REAL(R_atobi,4),R_utobi,REAL(R_evobi,4),
     & R_etobi,REAL(R_otobi,4),I_efubi,I_okubi,I_akubi,I_afubi
     &,L_ivobi,
     & L_alubi,L_amubi,L_avobi,L_itobi,
     & L_uvobi,L_obokad,L_ilubi,L_usobi,L_ixobi,L_omubi,L_abubi
     &,
     & L_ebubi,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_
     &(83),L_esobi,L_(84),
     & L_isobi,L_elubi,I_ukubi,L_emubi,R_udubi,REAL(R_exobi
     &,4),L_imubi,L_oxikad,
     & L_umubi,L_udokad,L_obubi,L_ububi,L_adubi,L_idubi,L_odubi
     &,L_edubi)
      !}

      if(L_odubi.or.L_idubi.or.L_edubi.or.L_adubi.or.L_ububi.or.L_obubi
     &) then      
                  I_ifubi = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ifubi = z'40000000'
      endif
C KPJ_vlv.fgi( 120, 127):���� ���������� �������� � ���������������� ��������,20KPJ30AA108
C sav1=L_axikad
C KPG_KPH_logic.fgi( 196,   9):recalc:������,20KPJ30AA109_ulubop
C if(sav1.ne.L_axikad .and. try1352.gt.0) goto 1352
      L_ubokad = L_ofokad.OR.L_ekokad.OR.L_oxikad
C KPG_KPH_logic.fgi( 174,  40):���
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_ipedi,4),R8_oledi
     &,I_usedi,I_itedi,I_osedi,
     & C8_amedi,I_etedi,R_umedi,R_omedi,R_ofedi,
     & REAL(R_akedi,4),R_ukedi,REAL(R_eledi,4),
     & R_ekedi,REAL(R_okedi,4),I_esedi,I_otedi,I_atedi,I_asedi
     &,L_iledi,
     & L_avedi,L_axedi,L_aledi,L_ikedi,
     & L_uledi,L_ubokad,L_ivedi,L_ufedi,L_imedi,L_oxedi,L_apedi
     &,
     & L_epedi,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_
     &(89),L_afedi,L_(90),
     & L_efedi,L_evedi,I_utedi,L_exedi,R_uredi,REAL(R_emedi
     &,4),L_ixedi,L_uxikad,
     & L_uxedi,L_ifedi,L_opedi,L_upedi,L_aredi,L_iredi,L_oredi
     &,L_eredi)
      !}

      if(L_oredi.or.L_iredi.or.L_eredi.or.L_aredi.or.L_upedi.or.L_opedi
     &) then      
                  I_isedi = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_isedi = z'40000000'
      endif
C KPJ_vlv.fgi(  75, 127):���� ���������� �������� � ���������������� ��������,20KPJ30AA105
      L_ebokad = L_ofokad.OR.L_ekokad.OR.L_uxikad.OR.L_afokad
C KPG_KPH_logic.fgi( 173,  98):���
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_isadi,4),R8_opadi
     &,I_uvadi,I_ixadi,I_ovadi,
     & C8_aradi,I_exadi,R_uradi,R_oradi,R_oladi,
     & REAL(R_amadi,4),R_umadi,REAL(R_epadi,4),
     & R_emadi,REAL(R_omadi,4),I_evadi,I_oxadi,I_axadi,I_avadi
     &,L_ipadi,
     & L_abedi,L_adedi,L_apadi,L_imadi,
     & L_upadi,L_ebokad,L_ibedi,L_uladi,L_iradi,L_odedi,L_asadi
     &,
     & L_esadi,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_
     &(87),L_aladi,L_(88),
     & L_eladi,L_ebedi,I_uxadi,L_ededi,R_utadi,REAL(R_eradi
     &,4),L_idedi,L_iladi,
     & L_udedi,L_efokad,L_osadi,L_usadi,L_atadi,L_itadi,L_otadi
     &,L_etadi)
      !}

      if(L_otadi.or.L_itadi.or.L_etadi.or.L_atadi.or.L_usadi.or.L_osadi
     &) then      
                  I_ivadi = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ivadi = z'40000000'
      endif
C KPJ_vlv.fgi(  90, 127):���� ���������� �������� � ���������������� ��������,20KPJ30AA106
      L_abokad = L_ofokad.OR.L_ekokad.OR.L_uxikad.OR.L_efokad.OR.L_ufoka
     &d.OR.(.NOT.L_odokad).OR.L_idokad.OR.(.NOT.L_edokad).OR.L_adokad
C KPG_KPH_logic.fgi( 176,  74):���
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_ivubi,4),R8_osubi
     &,I_ubadi,I_idadi,I_obadi,
     & C8_atubi,I_edadi,R_utubi,R_otubi,R_opubi,
     & REAL(R_arubi,4),R_urubi,REAL(R_esubi,4),
     & R_erubi,REAL(R_orubi,4),I_ebadi,I_odadi,I_adadi,I_abadi
     &,L_isubi,
     & L_afadi,L_akadi,L_asubi,L_irubi,
     & L_usubi,L_abokad,L_ifadi,L_upubi,L_itubi,L_okadi,L_avubi
     &,
     & L_evubi,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_
     &(85),L_apubi,L_(86),
     & L_epubi,L_efadi,I_udadi,L_ekadi,R_uxubi,REAL(R_etubi
     &,4),L_ikadi,L_ipubi,
     & L_ukadi,L_afokad,L_ovubi,L_uvubi,L_axubi,L_ixubi,L_oxubi
     &,L_exubi)
      !}

      if(L_oxubi.or.L_ixubi.or.L_exubi.or.L_axubi.or.L_uvubi.or.L_ovubi
     &) then      
                  I_ibadi = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ibadi = z'40000000'
      endif
C KPJ_vlv.fgi( 105, 127):���� ���������� �������� � ���������������� ��������,20KPJ30AA107
      L_ikokad = L_ekokad.OR.L_ofokad.OR.L_afokad.OR.(.NOT.L_odokad
     &).OR.L_idokad.OR.(.NOT.L_edokad).OR.L_adokad
C KPG_KPH_logic.fgi( 175, 138):���
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_elidi,4),R8_ifidi
     &,I_opidi,I_eridi,I_ipidi,
     & C8_ufidi,I_aridi,R_okidi,R_ikidi,R_ibidi,
     & REAL(R_ubidi,4),R_odidi,REAL(R_afidi,4),
     & R_adidi,REAL(R_ididi,4),I_apidi,I_iridi,I_upidi,I_umidi
     &,L_efidi,
     & L_uridi,L_usidi,L_udidi,L_edidi,
     & L_ofidi,L_ikokad,L_esidi,L_obidi,L_ekidi,L_itidi,L_ukidi
     &,
     & L_alidi,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_
     &(91),L_abidi,L_(92),
     & L_ebidi,L_asidi,I_oridi,L_atidi,R_omidi,REAL(R_akidi
     &,4),L_etidi,L_uxakad,
     & L_otidi,L_ufokad,L_ilidi,L_olidi,L_ulidi,L_emidi,L_imidi
     &,L_amidi)
      !}

      if(L_imidi.or.L_emidi.or.L_amidi.or.L_ulidi.or.L_olidi.or.L_ilidi
     &) then      
                  I_epidi = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_epidi = z'40000000'
      endif
C KPJ_vlv.fgi(  60, 127):���� ���������� �������� � ���������������� ��������,20KPJ30AA104
      L_alokad = L_ukokad.OR.L_akokad.OR.L_ufokad.OR.L_ofokad.OR.L_ifoka
     &d.OR.L_efokad.OR.L_afokad.OR.L_udokad
C KPG_KPH_logic.fgi( 177, 187):���
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_uxodi,4),R8_avodi
     &,I_efudi,I_ufudi,I_afudi,
     & C8_ivodi,I_ofudi,R_exodi,R_axodi,R_asodi,
     & REAL(R_isodi,4),R_etodi,REAL(R_otodi,4),
     & R_osodi,REAL(R_atodi,4),I_odudi,I_akudi,I_ifudi,I_idudi
     &,L_utodi,
     & L_ikudi,L_iludi,L_itodi,L_usodi,
     & L_evodi,L_alokad,L_ukudi,L_esodi,L_uvodi,L_amudi,L_ixodi
     &,
     & L_oxodi,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_
     &(95),L_orodi,L_(96),
     & L_urodi,L_okudi,I_ekudi,L_oludi,R_edudi,REAL(R_ovodi
     &,4),L_uludi,L_abekad,
     & L_emudi,L_ekokad,L_abudi,L_ebudi,L_ibudi,L_ubudi,L_adudi
     &,L_obudi)
      !}

      if(L_adudi.or.L_ubudi.or.L_obudi.or.L_ibudi.or.L_ebudi.or.L_abudi
     &) then      
                  I_ududi = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ududi = z'40000000'
      endif
C KPJ_vlv.fgi(  30, 127):���� ���������� �������� � ���������������� ��������,20KPJ30AA102
C sav1=L_okokad
      L_okokad = L_ukokad.OR.L_ekokad.OR.L_ofokad
C KPG_KPH_logic.fgi( 173, 156):recalc:���
C if(sav1.ne.L_okokad .and. try1342.gt.0) goto 1342
C sav1=L_obokad
      L_obokad = L_ofokad.OR.L_ekokad.OR.L_ixikad.OR.L_exikad
C KPG_KPH_logic.fgi( 174,  25):recalc:���
C if(sav1.ne.L_obokad .and. try1356.gt.0) goto 1356
C sav1=L_abokad
      L_abokad = L_ofokad.OR.L_ekokad.OR.L_uxikad.OR.L_efokad.OR.L_ufoka
     &d.OR.(.NOT.L_odokad).OR.L_idokad.OR.(.NOT.L_edokad).OR.L_adokad
C KPG_KPH_logic.fgi( 176,  74):recalc:���
C if(sav1.ne.L_abokad .and. try1382.gt.0) goto 1382
C sav1=L_ebokad
      L_ebokad = L_ofokad.OR.L_ekokad.OR.L_uxikad.OR.L_afokad
C KPG_KPH_logic.fgi( 173,  98):recalc:���
C if(sav1.ne.L_ebokad .and. try1374.gt.0) goto 1374
C sav1=L_ubokad
      L_ubokad = L_ofokad.OR.L_ekokad.OR.L_oxikad
C KPG_KPH_logic.fgi( 174,  40):recalc:���
C if(sav1.ne.L_ubokad .and. try1366.gt.0) goto 1366
C sav1=L_ikokad
      L_ikokad = L_ekokad.OR.L_ofokad.OR.L_afokad.OR.(.NOT.L_odokad
     &).OR.L_idokad.OR.(.NOT.L_edokad).OR.L_adokad
C KPG_KPH_logic.fgi( 175, 138):recalc:���
C if(sav1.ne.L_ikokad .and. try1392.gt.0) goto 1392
      L_ibokad = L_ekokad.OR.L_afikad
C KPG_KPH_logic.fgi( 173, 111):���
      L_ixakad = L_ekokad.OR.L_ufokad
C KPG_KPH_logic.fgi( 433, 224):���
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_amuki,4),R8_ekuki
     &,I_iruki,I_asuki,I_eruki,
     & C8_okuki,I_uruki,R_iluki,R_eluki,R_eduki,
     & REAL(R_oduki,4),R_ifuki,REAL(R_ufuki,4),
     & R_uduki,REAL(R_efuki,4),I_upuki,I_esuki,I_oruki,I_opuki
     &,L_akuki,
     & L_osuki,L_otuki,L_ofuki,L_afuki,
     & L_ikuki,L_ixakad,L_atuki,L_iduki,L_aluki,L_evuki,L_oluki
     &,
     & L_uluki,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_
     &(113),L_obuki,L_(114),
     & L_ubuki,L_usuki,I_isuki,L_utuki,R_ipuki,REAL(R_ukuki
     &,4),L_avuki,L_irakad,
     & L_ivuki,L_aduki,L_emuki,L_imuki,L_omuki,L_apuki,L_epuki
     &,L_umuki)
      !}

      if(L_epuki.or.L_apuki.or.L_umuki.or.L_omuki.or.L_imuki.or.L_emuki
     &) then      
                  I_aruki = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_aruki = z'40000000'
      endif
C KPJ_vlv.fgi( 136, 202):���� ���������� �������� � ���������������� ��������,20KPJ20AA101
      L_opikad=L_upikad
C KPG_KPH_logic.fgi(  74,  39):������,20KPJ11AA108_ulubop
C label 1441  try1441=try1441-1
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_apite,4),R8_elite
     &,I_isite,I_atite,I_esite,
     & C8_olite,I_usite,R_imite,R_emite,R_efite,
     & REAL(R_ofite,4),R_ikite,REAL(R_ukite,4),
     & R_ufite,REAL(R_ekite,4),I_urite,I_etite,I_osite,I_orite
     &,L_alite,
     & L_otite,L_ovite,L_okite,L_akite,
     & L_ilite,L_opikad,L_avite,L_ifite,L_amite,L_exite,L_omite
     &,
     & L_umite,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_
     &(47),L_udite,L_(48),
     & L_afite,L_utite,I_itite,L_uvite,R_irite,REAL(R_ulite
     &,4),L_axite,L_asikad,
     & L_ixite,L_ipikad,L_epite,L_ipite,L_opite,L_arite,L_erite
     &,L_upite)
      !}

      if(L_erite.or.L_arite.or.L_upite.or.L_opite.or.L_ipite.or.L_epite
     &) then      
                  I_asite = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_asite = z'40000000'
      endif
C KPJ_vlv.fgi( 120,  77):���� ���������� �������� � ���������������� ��������,20KPJ11AA108
      L_epikad=L_ipikad
C KPG_KPH_logic.fgi(  74,  32):������,20KPJ11AA107_ulubop
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_ukote,4),R8_afote
     &,I_epote,I_upote,I_apote,
     & C8_ifote,I_opote,R_ekote,R_akote,R_abote,
     & REAL(R_ibote,4),R_edote,REAL(R_odote,4),
     & R_obote,REAL(R_adote,4),I_omote,I_arote,I_ipote,I_imote
     &,L_udote,
     & L_irote,L_isote,L_idote,L_ubote,
     & L_efote,L_epikad,L_urote,L_ebote,L_ufote,L_atote,L_ikote
     &,
     & L_okote,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_
     &(49),L_oxite,L_(50),
     & L_uxite,L_orote,I_erote,L_osote,R_emote,REAL(R_ofote
     &,4),L_usote,L_orikad,
     & L_etote,L_upikad,L_alote,L_elote,L_ilote,L_ulote,L_amote
     &,L_olote)
      !}

      if(L_amote.or.L_ulote.or.L_olote.or.L_ilote.or.L_elote.or.L_alote
     &) then      
                  I_umote = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_umote = z'40000000'
      endif
C KPJ_vlv.fgi( 105,  77):���� ���������� �������� � ���������������� ��������,20KPJ11AA107
C sav1=L_opikad
C KPG_KPH_logic.fgi(  74,  39):recalc:������,20KPJ11AA108_ulubop
C if(sav1.ne.L_opikad .and. try1441.gt.0) goto 1441
      L_irikad=L_orikad
C KPG_KPH_logic.fgi(  74,  57):������,20KPJ11AA110_ulubop
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_avate,4),R8_esate
     &,I_ibete,I_adete,I_ebete,
     & C8_osate,I_ubete,R_itate,R_etate,R_epate,
     & REAL(R_opate,4),R_irate,REAL(R_urate,4),
     & R_upate,REAL(R_erate,4),I_uxate,I_edete,I_obete,I_oxate
     &,L_asate,
     & L_odete,L_ofete,L_orate,L_arate,
     & L_isate,L_irikad,L_afete,L_ipate,L_atate,L_ekete,L_otate
     &,
     & L_utate,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_
     &(43),L_imate,L_(44),
     & L_omate,L_udete,I_idete,L_ufete,R_ixate,REAL(R_usate
     &,4),L_akete,L_umate,
     & L_ikete,L_apate,L_evate,L_ivate,L_ovate,L_axate,L_exate
     &,L_uvate)
      !}

      if(L_exate.or.L_axate.or.L_uvate.or.L_ovate.or.L_ivate.or.L_evate
     &) then      
                  I_abete = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_abete = z'40000000'
      endif
C KPJ_vlv.fgi( 150,  77):���� ���������� �������� � ���������������� ��������,20KPJ11AA110
      L_uvikad = L_isikad.OR.L_asikad
C KPG_KPH_logic.fgi(  47, 114):���
      L_ovikad=L_uvikad
C KPG_KPH_logic.fgi(  74, 114):������,20KPJ11AA101_ulubop
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_ilove,4),R8_ofove
     &,I_upove,I_irove,I_opove,
     & C8_akove,I_erove,R_ukove,R_okove,R_obove,
     & REAL(R_adove,4),R_udove,REAL(R_efove,4),
     & R_edove,REAL(R_odove,4),I_epove,I_orove,I_arove,I_apove
     &,L_ifove,
     & L_asove,L_atove,L_afove,L_idove,
     & L_ufove,L_ovikad,L_isove,L_ubove,L_ikove,L_otove,L_alove
     &,
     & L_elove,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_
     &(61),L_uxive,L_(62),
     & L_above,L_esove,I_urove,L_etove,R_umove,REAL(R_ekove
     &,4),L_itove,L_ebove,
     & L_utove,L_ibove,L_olove,L_ulove,L_amove,L_imove,L_omove
     &,L_emove)
      !}

      if(L_omove.or.L_imove.or.L_emove.or.L_amove.or.L_ulove.or.L_olove
     &) then      
                  I_ipove = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ipove = z'40000000'
      endif
C KPJ_vlv.fgi(  15,  77):���� ���������� �������� � ���������������� ��������,20KPJ11AA101
      L_ivikad=L_uvikad
C KPG_KPH_logic.fgi(  74, 110):������,20KPJ11AA102_ulubop
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_epive,4),R8_ilive
     &,I_osive,I_etive,I_isive,
     & C8_ulive,I_ative,R_omive,R_imive,R_ifive,
     & REAL(R_ufive,4),R_okive,REAL(R_alive,4),
     & R_akive,REAL(R_ikive,4),I_asive,I_itive,I_usive,I_urive
     &,L_elive,
     & L_utive,L_uvive,L_ukive,L_ekive,
     & L_olive,L_ivikad,L_evive,L_ofive,L_emive,L_ixive,L_umive
     &,
     & L_apive,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_
     &(59),L_odive,L_(60),
     & L_udive,L_avive,I_otive,L_axive,R_orive,REAL(R_amive
     &,4),L_exive,L_afive,
     & L_oxive,L_efive,L_ipive,L_opive,L_upive,L_erive,L_irive
     &,L_arive)
      !}

      if(L_irive.or.L_erive.or.L_arive.or.L_upive.or.L_opive.or.L_ipive
     &) then      
                  I_esive = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_esive = z'40000000'
      endif
C KPJ_vlv.fgi(  30,  77):���� ���������� �������� � ���������������� ��������,20KPJ11AA102
      L_evikad=L_uvikad
C KPG_KPH_logic.fgi(  74, 106):������,20KPJ11AA103_ulubop
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_aseve,4),R8_epeve
     &,I_iveve,I_axeve,I_eveve,
     & C8_opeve,I_uveve,R_ireve,R_ereve,R_eleve,
     & REAL(R_oleve,4),R_imeve,REAL(R_umeve,4),
     & R_uleve,REAL(R_emeve,4),I_uteve,I_exeve,I_oveve,I_oteve
     &,L_apeve,
     & L_oxeve,L_obive,L_omeve,L_ameve,
     & L_ipeve,L_evikad,L_abive,L_ileve,L_areve,L_edive,L_oreve
     &,
     & L_ureve,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_
     &(57),L_okeve,L_(58),
     & L_ukeve,L_uxeve,I_ixeve,L_ubive,R_iteve,REAL(R_upeve
     &,4),L_adive,L_ibikad,
     & L_idive,L_aleve,L_eseve,L_iseve,L_oseve,L_ateve,L_eteve
     &,L_useve)
      !}

      if(L_eteve.or.L_ateve.or.L_useve.or.L_oseve.or.L_iseve.or.L_eseve
     &) then      
                  I_aveve = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_aveve = z'40000000'
      endif
C KPJ_vlv.fgi(  45,  77):���� ���������� �������� � ���������������� ��������,20KPJ11AA103
      L_avikad=L_uvikad
C KPG_KPH_logic.fgi(  74, 102):������,20KPJ11AA105_ulubop
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_abave,4),R8_evute
     &,I_ifave,I_akave,I_efave,
     & C8_ovute,I_ufave,R_ixute,R_exute,R_esute,
     & REAL(R_osute,4),R_itute,REAL(R_utute,4),
     & R_usute,REAL(R_etute,4),I_udave,I_ekave,I_ofave,I_odave
     &,L_avute,
     & L_okave,L_olave,L_otute,L_atute,
     & L_ivute,L_avikad,L_alave,L_isute,L_axute,L_emave,L_oxute
     &,
     & L_uxute,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_
     &(53),L_orute,L_(54),
     & L_urute,L_ukave,I_ikave,L_ulave,R_idave,REAL(R_uvute
     &,4),L_amave,L_ifekad,
     & L_imave,L_asute,L_ebave,L_ibave,L_obave,L_adave,L_edave
     &,L_ubave)
      !}

      if(L_edave.or.L_adave.or.L_ubave.or.L_obave.or.L_ibave.or.L_ebave
     &) then      
                  I_afave = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_afave = z'40000000'
      endif
C KPJ_vlv.fgi(  75,  77):���� ���������� �������� � ���������������� ��������,20KPJ11AA105
      L_utikad=L_uvikad
C KPG_KPH_logic.fgi(  74,  98):������,20KPJ11AA106_ulubop
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_afute,4),R8_ebute
     &,I_ilute,I_amute,I_elute,
     & C8_obute,I_ulute,R_idute,R_edute,R_evote,
     & REAL(R_ovote,4),R_ixote,REAL(R_uxote,4),
     & R_uvote,REAL(R_exote,4),I_ukute,I_emute,I_olute,I_okute
     &,L_abute,
     & L_omute,L_opute,L_oxote,L_axote,
     & L_ibute,L_utikad,L_apute,L_ivote,L_adute,L_erute,L_odute
     &,
     & L_udute,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_
     &(51),L_itote,L_(52),
     & L_otote,L_umute,I_imute,L_upute,R_ikute,REAL(R_ubute
     &,4),L_arute,L_utote,
     & L_irute,L_avote,L_efute,L_ifute,L_ofute,L_akute,L_ekute
     &,L_ufute)
      !}

      if(L_ekute.or.L_akute.or.L_ufute.or.L_ofute.or.L_ifute.or.L_efute
     &) then      
                  I_alute = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_alute = z'40000000'
      endif
C KPJ_vlv.fgi(  90,  77):���� ���������� �������� � ���������������� ��������,20KPJ11AA106
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_esete,4),R8_ipete
     &,I_ovete,I_exete,I_ivete,
     & C8_upete,I_axete,R_orete,R_irete,R_ilete,
     & REAL(R_ulete,4),R_omete,REAL(R_apete,4),
     & R_amete,REAL(R_imete,4),I_avete,I_ixete,I_uvete,I_utete
     &,L_epete,
     & L_uxete,L_ubite,L_umete,L_emete,
     & L_opete,L_uvikad,L_ebite,L_olete,L_erete,L_idite,L_urete
     &,
     & L_asete,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_
     &(45),L_okete,L_(46),
     & L_ukete,L_abite,I_oxete,L_adite,R_otete,REAL(R_arete
     &,4),L_edite,L_alete,
     & L_odite,L_elete,L_isete,L_osete,L_usete,L_etete,L_itete
     &,L_atete)
      !}

      if(L_itete.or.L_etete.or.L_atete.or.L_usete.or.L_osete.or.L_isete
     &) then      
                  I_evete = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_evete = z'40000000'
      endif
C KPJ_vlv.fgi( 135,  77):���� ���������� �������� � ���������������� ��������,20KPJ11AA109
      L_umikad=L_apikad
C KPG_KPH_logic.fgi(  74,  21):������,20KPJ12AA108_ulubop
C label 1480  try1480=try1480-1
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_idure,4),R8_oxore
     &,I_ukure,I_ilure,I_okure,
     & C8_abure,I_elure,R_ubure,R_obure,R_otore,
     & REAL(R_avore,4),R_uvore,REAL(R_exore,4),
     & R_evore,REAL(R_ovore,4),I_ekure,I_olure,I_alure,I_akure
     &,L_ixore,
     & L_amure,L_apure,L_axore,L_ivore,
     & L_uxore,L_umikad,L_imure,L_utore,L_ibure,L_opure,L_adure
     &,
     & L_edure,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_
     &(27),L_etore,L_(28),
     & L_itore,L_emure,I_ulure,L_epure,R_ufure,REAL(R_ebure
     &,4),L_ipure,L_urikad,
     & L_upure,L_omikad,L_odure,L_udure,L_afure,L_ifure,L_ofure
     &,L_efure)
      !}

      if(L_ofure.or.L_ifure.or.L_efure.or.L_afure.or.L_udure.or.L_odure
     &) then      
                  I_ikure = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ikure = z'40000000'
      endif
C KPJ_vlv.fgi( 120,  52):���� ���������� �������� � ���������������� ��������,20KPJ12AA108
      L_imikad=L_omikad
C KPG_KPH_logic.fgi(  74,  14):������,20KPJ12AA107_ulubop
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_exure,4),R8_iture
     &,I_odase,I_efase,I_idase,
     & C8_uture,I_afase,R_ovure,R_ivure,R_irure,
     & REAL(R_urure,4),R_osure,REAL(R_ature,4),
     & R_asure,REAL(R_isure,4),I_adase,I_ifase,I_udase,I_ubase
     &,L_eture,
     & L_ufase,L_ukase,L_usure,L_esure,
     & L_oture,L_imikad,L_ekase,L_orure,L_evure,L_ilase,L_uvure
     &,
     & L_axure,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_
     &(29),L_arure,L_(30),
     & L_erure,L_akase,I_ofase,L_alase,R_obase,REAL(R_avure
     &,4),L_elase,L_erikad,
     & L_olase,L_apikad,L_ixure,L_oxure,L_uxure,L_ebase,L_ibase
     &,L_abase)
      !}

      if(L_ibase.or.L_ebase.or.L_abase.or.L_uxure.or.L_oxure.or.L_ixure
     &) then      
                  I_edase = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_edase = z'40000000'
      endif
C KPJ_vlv.fgi( 105,  52):���� ���������� �������� � ���������������� ��������,20KPJ12AA107
C sav1=L_umikad
C KPG_KPH_logic.fgi(  74,  21):recalc:������,20KPJ12AA108_ulubop
C if(sav1.ne.L_umikad .and. try1480.gt.0) goto 1480
      L_arikad=L_erikad
C KPG_KPH_logic.fgi(  74,  49):������,20KPJ12AA110_ulubop
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_imire,4),R8_okire
     &,I_urire,I_isire,I_orire,
     & C8_alire,I_esire,R_ulire,R_olire,R_odire,
     & REAL(R_afire,4),R_ufire,REAL(R_ekire,4),
     & R_efire,REAL(R_ofire,4),I_erire,I_osire,I_asire,I_arire
     &,L_ikire,
     & L_atire,L_avire,L_akire,L_ifire,
     & L_ukire,L_arikad,L_itire,L_udire,L_ilire,L_ovire,L_amire
     &,
     & L_emire,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_
     &(23),L_ubire,L_(24),
     & L_adire,L_etire,I_usire,L_evire,R_upire,REAL(R_elire
     &,4),L_ivire,L_edire,
     & L_uvire,L_idire,L_omire,L_umire,L_apire,L_ipire,L_opire
     &,L_epire)
      !}

      if(L_opire.or.L_ipire.or.L_epire.or.L_apire.or.L_umire.or.L_omire
     &) then      
                  I_irire = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_irire = z'40000000'
      endif
C KPJ_vlv.fgi( 150,  52):���� ���������� �������� � ���������������� ��������,20KPJ12AA110
      L_otikad = L_esikad.OR.L_urikad
C KPG_KPH_logic.fgi(  47,  86):���
      L_itikad=L_otikad
C KPG_KPH_logic.fgi(  74,  86):������,20KPJ12AA101_ulubop
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_uxuse,4),R8_avuse
     &,I_efate,I_ufate,I_afate,
     & C8_ivuse,I_ofate,R_exuse,R_axuse,R_asuse,
     & REAL(R_isuse,4),R_etuse,REAL(R_otuse,4),
     & R_osuse,REAL(R_atuse,4),I_odate,I_akate,I_ifate,I_idate
     &,L_utuse,
     & L_ikate,L_ilate,L_ituse,L_ususe,
     & L_evuse,L_itikad,L_ukate,L_esuse,L_uvuse,L_amate,L_ixuse
     &,
     & L_oxuse,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_
     &(41),L_eruse,L_(42),
     & L_iruse,L_okate,I_ekate,L_olate,R_edate,REAL(R_ovuse
     &,4),L_ulate,L_oruse,
     & L_emate,L_uruse,L_abate,L_ebate,L_ibate,L_ubate,L_adate
     &,L_obate)
      !}

      if(L_adate.or.L_ubate.or.L_obate.or.L_ibate.or.L_ebate.or.L_abate
     &) then      
                  I_udate = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_udate = z'40000000'
      endif
C KPJ_vlv.fgi(  15,  52):���� ���������� �������� � ���������������� ��������,20KPJ12AA101
      L_etikad=L_otikad
C KPG_KPH_logic.fgi(  74,  82):������,20KPJ12AA102_ulubop
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_oduse,4),R8_uxose
     &,I_aluse,I_oluse,I_ukuse,
     & C8_ebuse,I_iluse,R_aduse,R_ubuse,R_utose,
     & REAL(R_evose,4),R_axose,REAL(R_ixose,4),
     & R_ivose,REAL(R_uvose,4),I_ikuse,I_uluse,I_eluse,I_ekuse
     &,L_oxose,
     & L_emuse,L_epuse,L_exose,L_ovose,
     & L_abuse,L_etikad,L_omuse,L_avose,L_obuse,L_upuse,L_eduse
     &,
     & L_iduse,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_
     &(39),L_atose,L_(40),
     & L_etose,L_imuse,I_amuse,L_ipuse,R_akuse,REAL(R_ibuse
     &,4),L_opuse,L_itose,
     & L_aruse,L_otose,L_uduse,L_afuse,L_efuse,L_ofuse,L_ufuse
     &,L_ifuse)
      !}

      if(L_ufuse.or.L_ofuse.or.L_ifuse.or.L_efuse.or.L_afuse.or.L_uduse
     &) then      
                  I_okuse = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_okuse = z'40000000'
      endif
C KPJ_vlv.fgi(  30,  52):���� ���������� �������� � ���������������� ��������,20KPJ12AA102
      L_atikad=L_otikad
C KPG_KPH_logic.fgi(  74,  78):������,20KPJ12AA103_ulubop
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_ikose,4),R8_odose
     &,I_umose,I_ipose,I_omose,
     & C8_afose,I_epose,R_ufose,R_ofose,R_oxise,
     & REAL(R_abose,4),R_ubose,REAL(R_edose,4),
     & R_ebose,REAL(R_obose,4),I_emose,I_opose,I_apose,I_amose
     &,L_idose,
     & L_arose,L_asose,L_adose,L_ibose,
     & L_udose,L_atikad,L_irose,L_uxise,L_ifose,L_osose,L_akose
     &,
     & L_ekose,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_
     &(37),L_axise,L_(38),
     & L_exise,L_erose,I_upose,L_esose,R_ulose,REAL(R_efose
     &,4),L_isose,L_ebikad,
     & L_usose,L_ixise,L_okose,L_ukose,L_alose,L_ilose,L_olose
     &,L_elose)
      !}

      if(L_olose.or.L_ilose.or.L_elose.or.L_alose.or.L_ukose.or.L_okose
     &) then      
                  I_imose = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_imose = z'40000000'
      endif
C KPJ_vlv.fgi(  45,  52):���� ���������� �������� � ���������������� ��������,20KPJ12AA103
      L_usikad=L_otikad
C KPG_KPH_logic.fgi(  74,  74):������,20KPJ12AA105_ulubop
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_irese,4),R8_omese
     &,I_utese,I_ivese,I_otese,
     & C8_apese,I_evese,R_upese,R_opese,R_okese,
     & REAL(R_alese,4),R_ulese,REAL(R_emese,4),
     & R_elese,REAL(R_olese,4),I_etese,I_ovese,I_avese,I_atese
     &,L_imese,
     & L_axese,L_abise,L_amese,L_ilese,
     & L_umese,L_usikad,L_ixese,L_ukese,L_ipese,L_obise,L_arese
     &,
     & L_erese,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_
     &(33),L_akese,L_(34),
     & L_ekese,L_exese,I_uvese,L_ebise,R_usese,REAL(R_epese
     &,4),L_ibise,L_odekad,
     & L_ubise,L_ikese,L_orese,L_urese,L_asese,L_isese,L_osese
     &,L_esese)
      !}

      if(L_osese.or.L_isese.or.L_esese.or.L_asese.or.L_urese.or.L_orese
     &) then      
                  I_itese = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_itese = z'40000000'
      endif
C KPJ_vlv.fgi(  75,  52):���� ���������� �������� � ���������������� ��������,20KPJ12AA105
      L_osikad=L_otikad
C KPG_KPH_logic.fgi(  74,  70):������,20KPJ12AA106_ulubop
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_itase,4),R8_orase
     &,I_uxase,I_ibese,I_oxase,
     & C8_asase,I_ebese,R_usase,R_osase,R_omase,
     & REAL(R_apase,4),R_upase,REAL(R_erase,4),
     & R_epase,REAL(R_opase,4),I_exase,I_obese,I_abese,I_axase
     &,L_irase,
     & L_adese,L_afese,L_arase,L_ipase,
     & L_urase,L_osikad,L_idese,L_umase,L_isase,L_ofese,L_atase
     &,
     & L_etase,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_
     &(31),L_ulase,L_(32),
     & L_amase,L_edese,I_ubese,L_efese,R_uvase,REAL(R_esase
     &,4),L_ifese,L_emase,
     & L_ufese,L_imase,L_otase,L_utase,L_avase,L_ivase,L_ovase
     &,L_evase)
      !}

      if(L_ovase.or.L_ivase.or.L_evase.or.L_avase.or.L_utase.or.L_otase
     &) then      
                  I_ixase = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ixase = z'40000000'
      endif
C KPJ_vlv.fgi(  90,  52):���� ���������� �������� � ���������������� ��������,20KPJ12AA106
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_okore,4),R8_udore
     &,I_apore,I_opore,I_umore,
     & C8_efore,I_ipore,R_akore,R_ufore,R_uxire,
     & REAL(R_ebore,4),R_adore,REAL(R_idore,4),
     & R_ibore,REAL(R_ubore,4),I_imore,I_upore,I_epore,I_emore
     &,L_odore,
     & L_erore,L_esore,L_edore,L_obore,
     & L_afore,L_otikad,L_orore,L_abore,L_ofore,L_usore,L_ekore
     &,
     & L_ikore,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_
     &(25),L_axire,L_(26),
     & L_exire,L_irore,I_arore,L_isore,R_amore,REAL(R_ifore
     &,4),L_osore,L_ixire,
     & L_atore,L_oxire,L_ukore,L_alore,L_elore,L_olore,L_ulore
     &,L_ilore)
      !}

      if(L_ulore.or.L_olore.or.L_ilore.or.L_elore.or.L_alore.or.L_ukore
     &) then      
                  I_omore = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_omore = z'40000000'
      endif
C KPJ_vlv.fgi( 135,  52):���� ���������� �������� � ���������������� ��������,20KPJ12AA109
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_orimi,4),R8_umimi
     &,I_avimi,I_ovimi,I_utimi,
     & C8_epimi,I_ivimi,R_arimi,R_upimi,R_ukimi,
     & REAL(R_elimi,4),R_amimi,REAL(R_imimi,4),
     & R_ilimi,REAL(R_ulimi,4),I_itimi,I_uvimi,I_evimi,I_etimi
     &,L_omimi,
     & L_eximi,L_ebomi,L_emimi,L_olimi,
     & L_apimi,L_ikekad,L_oximi,L_alimi,L_opimi,L_ubomi,L_erimi
     &,
     & L_irimi,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_
     &(133),L_ikimi,L_(134),
     & L_okimi,L_iximi,I_aximi,L_ibomi,R_atimi,REAL(R_ipimi
     &,4),L_obomi,L_epekad,
     & L_adomi,L_ubekad,L_urimi,L_asimi,L_esimi,L_osimi,L_usimi
     &,L_isimi)
      !}

      if(L_usimi.or.L_osimi.or.L_isimi.or.L_esimi.or.L_asimi.or.L_urimi
     &) then      
                  I_otimi = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_otimi = z'40000000'
      endif
C KPJ_vlv.fgi(  75, 202):���� ���������� �������� � ���������������� ��������,20KPJ21AA105
C label 1519  try1519=try1519-1
      L_exakad = L_ubekad.OR.L_efekad.OR.L_afekad.OR.L_axakad
C KPG_KPH_logic.fgi( 433, 211):���
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_etape,4),R8_irape
     &,I_oxape,I_ebepe,I_ixape,
     & C8_urape,I_abepe,R_osape,R_isape,R_imape,
     & REAL(R_umape,4),R_opape,REAL(R_arape,4),
     & R_apape,REAL(R_ipape,4),I_axape,I_ibepe,I_uxape,I_uvape
     &,L_erape,
     & L_ubepe,L_udepe,L_upape,L_epape,
     & L_orape,L_exakad,L_edepe,L_omape,L_esape,L_ifepe,L_usape
     &,
     & L_atape,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_
     &(11),L_amape,L_(12),
     & L_emape,L_adepe,I_obepe,L_afepe,R_ovape,REAL(R_asape
     &,4),L_efepe,L_omekad,
     & L_ofepe,L_udekad,L_itape,L_otape,L_utape,L_evape,L_ivape
     &,L_avape)
      !}

      if(L_ivape.or.L_evape.or.L_avape.or.L_utape.or.L_otape.or.L_itape
     &) then      
                  I_exape = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_exape = z'40000000'
      endif
C KPJ_vlv.fgi( 121, 202):���� ���������� �������� � ���������������� ��������,20KPJ21AA108
      L_ebekad = L_ubekad.OR.L_efekad.OR.L_udekad.OR.L_abekad.OR.L_uxaka
     &d
C KPG_KPH_logic.fgi( 433, 269):���
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_abemi,4),R8_evami
     &,I_ifemi,I_akemi,I_efemi,
     & C8_ovami,I_ufemi,R_ixami,R_exami,R_esami,
     & REAL(R_osami,4),R_itami,REAL(R_utami,4),
     & R_usami,REAL(R_etami,4),I_udemi,I_ekemi,I_ofemi,I_odemi
     &,L_avami,
     & L_okemi,L_olemi,L_otami,L_atami,
     & L_ivami,L_ebekad,L_alemi,L_isami,L_axami,L_ememi,L_oxami
     &,
     & L_uxami,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_
     &(129),L_urami,L_(130),
     & L_asami,L_ukemi,I_ikemi,L_ulemi,R_idemi,REAL(R_uvami
     &,4),L_amemi,L_umekad,
     & L_imemi,L_afekad,L_ebemi,L_ibemi,L_obemi,L_ademi,L_edemi
     &,L_ubemi)
      !}

      if(L_edemi.or.L_ademi.or.L_ubemi.or.L_obemi.or.L_ibemi.or.L_ebemi
     &) then      
                  I_afemi = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_afemi = z'40000000'
      endif
C KPJ_vlv.fgi( 105, 202):���� ���������� �������� � ���������������� ��������,20KPJ21AA107
      L_adekad = L_ubekad.OR.L_afekad.OR.L_udekad
C KPG_KPH_logic.fgi( 344,  31):���
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_utemi,4),R8_asemi
     &,I_ebimi,I_ubimi,I_abimi,
     & C8_isemi,I_obimi,R_etemi,R_atemi,R_apemi,
     & REAL(R_ipemi,4),R_eremi,REAL(R_oremi,4),
     & R_opemi,REAL(R_aremi,4),I_oxemi,I_adimi,I_ibimi,I_ixemi
     &,L_uremi,
     & L_idimi,L_ifimi,L_iremi,L_upemi,
     & L_esemi,L_adekad,L_udimi,L_epemi,L_usemi,L_akimi,L_itemi
     &,
     & L_otemi,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_
     &(131),L_omemi,L_(132),
     & L_umemi,L_odimi,I_edimi,L_ofimi,R_exemi,REAL(R_osemi
     &,4),L_ufimi,L_apekad,
     & L_ekimi,L_efekad,L_avemi,L_evemi,L_ivemi,L_uvemi,L_axemi
     &,L_ovemi)
      !}

      if(L_axemi.or.L_uvemi.or.L_ovemi.or.L_ivemi.or.L_evemi.or.L_avemi
     &) then      
                  I_uxemi = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_uxemi = z'40000000'
      endif
C KPJ_vlv.fgi(  90, 202):���� ���������� �������� � ���������������� ��������,20KPJ21AA106
C sav1=L_exakad
      L_exakad = L_ubekad.OR.L_efekad.OR.L_afekad.OR.L_axakad
C KPG_KPH_logic.fgi( 433, 211):recalc:���
C if(sav1.ne.L_exakad .and. try1528.gt.0) goto 1528
C sav1=L_ebekad
      L_ebekad = L_ubekad.OR.L_efekad.OR.L_udekad.OR.L_abekad.OR.L_uxaka
     &d
C KPG_KPH_logic.fgi( 433, 269):recalc:���
C if(sav1.ne.L_ebekad .and. try1540.gt.0) goto 1540
      L_ikekad = L_efekad.OR.L_afekad.OR.L_udekad.OR.L_ifekad.OR.L_odeka
     &d
C KPG_KPH_logic.fgi( 344,  79):���
      L_(170) = L_imekad.AND.L_epekad.AND.L_apekad.AND.L_umekad.AND.L_om
     &ekad
C KPG_KPH_logic.fgi( 342, 228):�
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_ixapi,4),R8_otapi
     &,I_udepi,I_ifepi,I_odepi,
     & C8_avapi,I_efepi,R_uvapi,R_ovapi,R_orapi,
     & REAL(R_asapi,4),R_usapi,REAL(R_etapi,4),
     & R_esapi,REAL(R_osapi,4),I_edepi,I_ofepi,I_afepi,I_adepi
     &,L_itapi,
     & L_akepi,L_alepi,L_atapi,L_isapi,
     & L_utapi,L_urakad,L_ikepi,L_urapi,L_ivapi,L_olepi,L_axapi
     &,
     & L_exapi,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_
     &(141),L_arapi,L_(142),
     & L_erapi,L_ekepi,I_ufepi,L_elepi,R_ubepi,REAL(R_evapi
     &,4),L_ilepi,L_irapi,
     & L_ulepi,L_esakad,L_oxapi,L_uxapi,L_abepi,L_ibepi,L_obepi
     &,L_ebepi)
      !}

      if(L_obepi.or.L_ibepi.or.L_ebepi.or.L_abepi.or.L_uxapi.or.L_oxapi
     &) then      
                  I_idepi = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_idepi = z'40000000'
      endif
C KPJ_vlv.fgi(  15, 202):���� ���������� �������� � ���������������� ��������,20KPJ21AA101
C label 1571  try1571=try1571-1
      L_utakad = L_itakad.OR.L_usakad.OR.L_afekad.OR.L_osakad.OR.L_esaka
     &d
C KPG_KPH_logic.fgi( 434, 140):���
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_okumi,4),R8_udumi
     &,I_apumi,I_opumi,I_umumi,
     & C8_efumi,I_ipumi,R_akumi,R_ufumi,R_uxomi,
     & REAL(R_ebumi,4),R_adumi,REAL(R_idumi,4),
     & R_ibumi,REAL(R_ubumi,4),I_imumi,I_upumi,I_epumi,I_emumi
     &,L_odumi,
     & L_erumi,L_esumi,L_edumi,L_obumi,
     & L_afumi,L_utakad,L_orumi,L_abumi,L_ofumi,L_usumi,L_ekumi
     &,
     & L_ikumi,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_
     &(137),L_exomi,L_(138),
     & L_ixomi,L_irumi,I_arumi,L_isumi,R_amumi,REAL(R_ifumi
     &,4),L_osumi,L_oxomi,
     & L_atumi,L_opekad,L_ukumi,L_alumi,L_elumi,L_olumi,L_ulumi
     &,L_ilumi)
      !}

      if(L_ulumi.or.L_olumi.or.L_ilumi.or.L_elumi.or.L_alumi.or.L_ukumi
     &) then      
                  I_omumi = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_omumi = z'40000000'
      endif
C KPJ_vlv.fgi(  45, 202):���� ���������� �������� � ���������������� ��������,20KPJ21AA103
      L_urakad = L_opekad.OR.L_irakad.OR.L_(166)
C KPG_KPH_logic.fgi( 442,  94):���
      L_utekad = L_avekad.OR.L_irekad.OR.L_arekad.OR.L_upekad.OR.L_opeka
     &d.OR.L_(170)
C KPG_KPH_logic.fgi( 353, 246):���
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_eluli,4),R8_ifuli
     &,I_opuli,I_eruli,I_ipuli,
     & C8_ufuli,I_aruli,R_okuli,R_ikuli,R_ibuli,
     & REAL(R_ubuli,4),R_oduli,REAL(R_afuli,4),
     & R_aduli,REAL(R_iduli,4),I_apuli,I_iruli,I_upuli,I_umuli
     &,L_efuli,
     & L_uruli,L_usuli,L_uduli,L_eduli,
     & L_ofuli,L_okekad,L_esuli,L_obuli,L_ekuli,L_ituli,L_ukuli
     &,
     & L_aluli,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_
     &(125),L_abuli,L_(126),
     & L_ebuli,L_asuli,I_oruli,L_atuli,R_omuli,REAL(R_akuli
     &,4),L_etuli,L_ilekad,
     & L_otuli,L_etakad,L_iluli,L_oluli,L_ululi,L_emuli,L_imuli
     &,L_amuli)
      !}

      if(L_imuli.or.L_emuli.or.L_amuli.or.L_ululi.or.L_oluli.or.L_iluli
     &) then      
                  I_epuli = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_epuli = z'40000000'
      endif
C KPJ_vlv.fgi(  30, 177):���� ���������� �������� � ���������������� ��������,20KPJ22AA102
C label 1588  try1588=try1588-1
      L_edekad=L_ilekad
C KPG_KPH_logic.fgi( 466, 153):������,20KPJ12AA104_ulubop
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_imise,4),R8_okise
     &,I_urise,I_isise,I_orise,
     & C8_alise,I_esise,R_ulise,R_olise,R_odise,
     & REAL(R_afise,4),R_ufise,REAL(R_ekise,4),
     & R_efise,REAL(R_ofise,4),I_erise,I_osise,I_asise,I_arise
     &,L_ikise,
     & L_atise,L_avise,L_akise,L_ifise,
     & L_ukise,L_edekad,L_itise,L_udise,L_ilise,L_ovise,L_amise
     &,
     & L_emise,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_
     &(35),L_adise,L_(36),
     & L_edise,L_etise,I_usise,L_evise,R_upise,REAL(R_elise
     &,4),L_ivise,L_ukekad,
     & L_uvise,L_idise,L_omise,L_umise,L_apise,L_ipise,L_opise
     &,L_epise)
      !}

      if(L_opise.or.L_ipise.or.L_epise.or.L_apise.or.L_umise.or.L_omise
     &) then      
                  I_irise = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_irise = z'40000000'
      endif
C KPJ_vlv.fgi(  60,  52):���� ���������� �������� � ���������������� ��������,20KPJ12AA104
      L_okekad=L_ukekad
C KPG_KPH_logic.fgi( 466, 166):������,20KPJ22AA102_ulubop
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_iveli,4),R8_oseli
     &,I_ubili,I_idili,I_obili,
     & C8_ateli,I_edili,R_uteli,R_oteli,R_opeli,
     & REAL(R_areli,4),R_ureli,REAL(R_eseli,4),
     & R_ereli,REAL(R_oreli,4),I_ebili,I_odili,I_adili,I_abili
     &,L_iseli,
     & L_afili,L_akili,L_aseli,L_ireli,
     & L_useli,L_ekekad,L_ifili,L_upeli,L_iteli,L_okili,L_aveli
     &,
     & L_eveli,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_
     &(119),L_epeli,L_(120),
     & L_ipeli,L_efili,I_udili,L_ekili,R_uxeli,REAL(R_eteli
     &,4),L_ikili,L_emekad,
     & L_ukili,L_ibekad,L_oveli,L_uveli,L_axeli,L_ixeli,L_oxeli
     &,L_exeli)
      !}

      if(L_oxeli.or.L_ixeli.or.L_exeli.or.L_axeli.or.L_uveli.or.L_oveli
     &) then      
                  I_ibili = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ibili = z'40000000'
      endif
C KPJ_vlv.fgi(  75, 177):���� ���������� �������� � ���������������� ��������,20KPJ22AA105
C label 1600  try1600=try1600-1
      L_uvakad = L_ibekad.OR.L_akekad.OR.L_ufekad.OR.L_axakad
C KPG_KPH_logic.fgi( 433, 191):���
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_ixume,4),R8_otume
     &,I_udape,I_ifape,I_odape,
     & C8_avume,I_efape,R_uvume,R_ovume,R_orume,
     & REAL(R_asume,4),R_usume,REAL(R_etume,4),
     & R_esume,REAL(R_osume,4),I_edape,I_ofape,I_afape,I_adape
     &,L_itume,
     & L_akape,L_alape,L_atume,L_isume,
     & L_utume,L_uvakad,L_ikape,L_urume,L_ivume,L_olape,L_axume
     &,
     & L_exume,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_
     &(9),L_erume,L_(10),
     & L_irume,L_ekape,I_ufape,L_elape,R_ubape,REAL(R_evume
     &,4),L_ilape,L_olekad,
     & L_ulape,L_ofekad,L_oxume,L_uxume,L_abape,L_ibape,L_obape
     &,L_ebape)
      !}

      if(L_obape.or.L_ibape.or.L_ebape.or.L_abape.or.L_uxume.or.L_oxume
     &) then      
                  I_idape = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_idape = z'40000000'
      endif
C KPJ_vlv.fgi( 121, 177):���� ���������� �������� � ���������������� ��������,20KPJ22AA108
      L_oxakad = L_ibekad.OR.L_akekad.OR.L_ofekad.OR.L_abekad.OR.L_uxaka
     &d
C KPG_KPH_logic.fgi( 433, 246):���
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_ufali,4),R8_adali
     &,I_emali,I_umali,I_amali,
     & C8_idali,I_omali,R_efali,R_afali,R_axuki,
     & REAL(R_ixuki,4),R_ebali,REAL(R_obali,4),
     & R_oxuki,REAL(R_abali,4),I_olali,I_apali,I_imali,I_ilali
     &,L_ubali,
     & L_ipali,L_irali,L_ibali,L_uxuki,
     & L_edali,L_oxakad,L_upali,L_exuki,L_udali,L_asali,L_ifali
     &,
     & L_ofali,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_
     &(115),L_ovuki,L_(116),
     & L_uvuki,L_opali,I_epali,L_orali,R_elali,REAL(R_odali
     &,4),L_urali,L_ulekad,
     & L_esali,L_ufekad,L_akali,L_ekali,L_ikali,L_ukali,L_alali
     &,L_okali)
      !}

      if(L_alali.or.L_ukali.or.L_okali.or.L_ikali.or.L_ekali.or.L_akali
     &) then      
                  I_ulali = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ulali = z'40000000'
      endif
C KPJ_vlv.fgi( 105, 177):���� ���������� �������� � ���������������� ��������,20KPJ22AA107
      L_obekad = L_ibekad.OR.L_ufekad.OR.L_ofekad
C KPG_KPH_logic.fgi( 344,  17):���
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_obeli,4),R8_uvali
     &,I_akeli,I_okeli,I_ufeli,
     & C8_exali,I_ikeli,R_abeli,R_uxali,R_usali,
     & REAL(R_etali,4),R_avali,REAL(R_ivali,4),
     & R_itali,REAL(R_utali,4),I_ifeli,I_ukeli,I_ekeli,I_efeli
     &,L_ovali,
     & L_eleli,L_emeli,L_evali,L_otali,
     & L_axali,L_obekad,L_oleli,L_atali,L_oxali,L_umeli,L_ebeli
     &,
     & L_ibeli,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_
     &(117),L_isali,L_(118),
     & L_osali,L_ileli,I_aleli,L_imeli,R_afeli,REAL(R_ixali
     &,4),L_omeli,L_amekad,
     & L_apeli,L_akekad,L_ubeli,L_adeli,L_edeli,L_odeli,L_udeli
     &,L_ideli)
      !}

      if(L_udeli.or.L_odeli.or.L_ideli.or.L_edeli.or.L_adeli.or.L_ubeli
     &) then      
                  I_ofeli = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ofeli = z'40000000'
      endif
C KPJ_vlv.fgi(  90, 177):���� ���������� �������� � ���������������� ��������,20KPJ22AA106
C sav1=L_oxakad
      L_oxakad = L_ibekad.OR.L_akekad.OR.L_ofekad.OR.L_abekad.OR.L_uxaka
     &d
C KPG_KPH_logic.fgi( 433, 246):recalc:���
C if(sav1.ne.L_oxakad .and. try1621.gt.0) goto 1621
C sav1=L_uvakad
      L_uvakad = L_ibekad.OR.L_akekad.OR.L_ufekad.OR.L_axakad
C KPG_KPH_logic.fgi( 433, 191):recalc:���
C if(sav1.ne.L_uvakad .and. try1609.gt.0) goto 1609
      L_ekekad = L_akekad.OR.L_ufekad.OR.L_ofekad.OR.L_ifekad.OR.L_odeka
     &d
C KPG_KPH_logic.fgi( 344,  53):���
      L_(169) = L_ilekad.AND.L_emekad.AND.L_amekad.AND.L_ulekad.AND.L_ol
     &ekad
C KPG_KPH_logic.fgi( 342, 186):�
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_efami,4),R8_ibami
     &,I_olami,I_emami,I_ilami,
     & C8_ubami,I_amami,R_odami,R_idami,R_ivuli,
     & REAL(R_uvuli,4),R_oxuli,REAL(R_abami,4),
     & R_axuli,REAL(R_ixuli,4),I_alami,I_imami,I_ulami,I_ukami
     &,L_ebami,
     & L_umami,L_upami,L_uxuli,L_exuli,
     & L_obami,L_orakad,L_epami,L_ovuli,L_edami,L_irami,L_udami
     &,
     & L_afami,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_
     &(127),L_utuli,L_(128),
     & L_avuli,L_apami,I_omami,L_arami,R_okami,REAL(R_adami
     &,4),L_erami,L_evuli,
     & L_orami,L_asakad,L_ifami,L_ofami,L_ufami,L_ekami,L_ikami
     &,L_akami)
      !}

      if(L_ikami.or.L_ekami.or.L_akami.or.L_ufami.or.L_ofami.or.L_ifami
     &) then      
                  I_elami = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_elami = z'40000000'
      endif
C KPJ_vlv.fgi(  15, 177):���� ���������� �������� � ���������������� ��������,20KPJ22AA101
C label 1652  try1652=try1652-1
      L_otakad = L_isakad.OR.L_etakad.OR.L_ufekad.OR.L_atakad.OR.L_asaka
     &d
C KPG_KPH_logic.fgi( 434, 116):���
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_ipoli,4),R8_ololi
     &,I_usoli,I_itoli,I_osoli,
     & C8_amoli,I_etoli,R_umoli,R_omoli,R_ofoli,
     & REAL(R_akoli,4),R_ukoli,REAL(R_eloli,4),
     & R_ekoli,REAL(R_okoli,4),I_esoli,I_otoli,I_atoli,I_asoli
     &,L_iloli,
     & L_avoli,L_axoli,L_aloli,L_ikoli,
     & L_uloli,L_otakad,L_ivoli,L_ufoli,L_imoli,L_oxoli,L_apoli
     &,
     & L_epoli,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_
     &(123),L_afoli,L_(124),
     & L_efoli,L_evoli,I_utoli,L_exoli,R_uroli,REAL(R_emoli
     &,4),L_ixoli,L_ifoli,
     & L_uxoli,L_ipekad,L_opoli,L_upoli,L_aroli,L_iroli,L_oroli
     &,L_eroli)
      !}

      if(L_oroli.or.L_iroli.or.L_eroli.or.L_aroli.or.L_upoli.or.L_opoli
     &) then      
                  I_isoli = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_isoli = z'40000000'
      endif
C KPJ_vlv.fgi(  45, 177):���� ���������� �������� � ���������������� ��������,20KPJ22AA103
      L_orakad = L_ipekad.OR.L_irakad.OR.L_(165)
C KPG_KPH_logic.fgi( 442,  78):���
      L_otekad = L_evekad.OR.L_erekad.OR.L_arekad.OR.L_upekad.OR.L_ipeka
     &d.OR.L_(169)
C KPG_KPH_logic.fgi( 353, 204):���
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_okifi,4),R8_udifi
     &,I_apifi,I_opifi,I_umifi,
     & C8_efifi,I_ipifi,R_akifi,R_ufifi,R_uxefi,
     & REAL(R_ebifi,4),R_adifi,REAL(R_idifi,4),
     & R_ibifi,REAL(R_ubifi,4),I_imifi,I_upifi,I_epifi,I_emifi
     &,L_odifi,
     & L_erifi,L_esifi,L_edifi,L_obifi,
     & L_afifi,L_ixekad,L_orifi,L_abifi,L_ofifi,L_usifi,L_ekifi
     &,
     & L_ikifi,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_
     &(103),L_exefi,L_(104),
     & L_ixefi,L_irifi,I_arifi,L_isifi,R_amifi,REAL(R_ififi
     &,4),L_osifi,L_oxefi,
     & L_atifi,L_oxekad,L_ukifi,L_alifi,L_elifi,L_olifi,L_ulifi
     &,L_ilifi)
      !}

      if(L_ulifi.or.L_olifi.or.L_ilifi.or.L_elifi.or.L_alifi.or.L_ukifi
     &) then      
                  I_omifi = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_omifi = z'40000000'
      endif
C KPJ_vlv.fgi(  75, 152):���� ���������� �������� � ���������������� ��������,20KPJ70AA105
C label 1669  try1669=try1669-1
      L_idikad=L_odikad
C KPG_KPH_logic.fgi( 282, 172):������,20KPJ70AA107_ulubop
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_urafi,4),R8_apafi
     &,I_evafi,I_uvafi,I_avafi,
     & C8_ipafi,I_ovafi,R_erafi,R_arafi,R_alafi,
     & REAL(R_ilafi,4),R_emafi,REAL(R_omafi,4),
     & R_olafi,REAL(R_amafi,4),I_otafi,I_axafi,I_ivafi,I_itafi
     &,L_umafi,
     & L_ixafi,L_ibefi,L_imafi,L_ulafi,
     & L_epafi,L_idikad,L_uxafi,L_elafi,L_upafi,L_adefi,L_irafi
     &,
     & L_orafi,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_
     &(99),L_ikafi,L_(100),
     & L_okafi,L_oxafi,I_exafi,L_obefi,R_etafi,REAL(R_opafi
     &,4),L_ubefi,L_ukafi,
     & L_edefi,L_edikad,L_asafi,L_esafi,L_isafi,L_usafi,L_atafi
     &,L_osafi)
      !}

      if(L_atafi.or.L_usafi.or.L_osafi.or.L_isafi.or.L_esafi.or.L_asafi
     &) then      
                  I_utafi = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_utafi = z'40000000'
      endif
C KPJ_vlv.fgi( 105, 152):���� ���������� �������� � ���������������� ��������,20KPJ70AA107
      L_udikad = L_edikad.OR.L_oxekad
C KPG_KPH_logic.fgi( 254, 183):���
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_omefi,4),R8_ukefi
     &,I_asefi,I_osefi,I_urefi,
     & C8_elefi,I_isefi,R_amefi,R_ulefi,R_udefi,
     & REAL(R_efefi,4),R_akefi,REAL(R_ikefi,4),
     & R_ifefi,REAL(R_ufefi,4),I_irefi,I_usefi,I_esefi,I_erefi
     &,L_okefi,
     & L_etefi,L_evefi,L_ekefi,L_ofefi,
     & L_alefi,L_udikad,L_otefi,L_afefi,L_olefi,L_uvefi,L_emefi
     &,
     & L_imefi,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_
     &(101),L_idefi,L_(102),
     & L_odefi,L_itefi,I_atefi,L_ivefi,R_arefi,REAL(R_ilefi
     &,4),L_ovefi,L_exekad,
     & L_axefi,L_odikad,L_umefi,L_apefi,L_epefi,L_opefi,L_upefi
     &,L_ipefi)
      !}

      if(L_upefi.or.L_opefi.or.L_ipefi.or.L_epefi.or.L_apefi.or.L_umefi
     &) then      
                  I_orefi = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_orefi = z'40000000'
      endif
C KPJ_vlv.fgi(  90, 152):���� ���������� �������� � ���������������� ��������,20KPJ70AA106
C sav1=L_idikad
C KPG_KPH_logic.fgi( 282, 172):recalc:������,20KPJ70AA107_ulubop
C if(sav1.ne.L_idikad .and. try1672.gt.0) goto 1672
      L_ixekad=L_odikad
C KPG_KPH_logic.fgi( 282, 132):������,20KPJ70AA105_ulubop
      L_ifikad = L_afikad.OR.L_exekad
C KPG_KPH_logic.fgi( 258, 193):���
      L_efikad=L_ifikad
C KPG_KPH_logic.fgi( 282, 193):������,20KPJ70AA101_ulubop
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_asaki,4),R8_epaki
     &,I_ivaki,I_axaki,I_evaki,
     & C8_opaki,I_uvaki,R_iraki,R_eraki,R_elaki,
     & REAL(R_olaki,4),R_imaki,REAL(R_umaki,4),
     & R_ulaki,REAL(R_emaki,4),I_utaki,I_exaki,I_ovaki,I_otaki
     &,L_apaki,
     & L_oxaki,L_obeki,L_omaki,L_amaki,
     & L_ipaki,L_efikad,L_abeki,L_ilaki,L_araki,L_edeki,L_oraki
     &,
     & L_uraki,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_
     &(111),L_ikaki,L_(112),
     & L_okaki,L_uxaki,I_ixaki,L_ubeki,R_itaki,REAL(R_upaki
     &,4),L_adeki,L_ukaki,
     & L_ideki,L_alaki,L_esaki,L_isaki,L_osaki,L_ataki,L_etaki
     &,L_usaki)
      !}

      if(L_etaki.or.L_ataki.or.L_usaki.or.L_osaki.or.L_isaki.or.L_esaki
     &) then      
                  I_avaki = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_avaki = z'40000000'
      endif
C KPJ_vlv.fgi(  15, 152):���� ���������� �������� � ���������������� ��������,20KPJ70AA101
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_uxofi,4),R8_avofi
     &,I_efufi,I_ufufi,I_afufi,
     & C8_ivofi,I_ofufi,R_exofi,R_axofi,R_asofi,
     & REAL(R_isofi,4),R_etofi,REAL(R_otofi,4),
     & R_osofi,REAL(R_atofi,4),I_odufi,I_akufi,I_ifufi,I_idufi
     &,L_utofi,
     & L_ikufi,L_ilufi,L_itofi,L_usofi,
     & L_evofi,L_ifikad,L_ukufi,L_esofi,L_uvofi,L_amufi,L_ixofi
     &,
     & L_oxofi,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_
     &(107),L_erofi,L_(108),
     & L_irofi,L_okufi,I_ekufi,L_olufi,R_edufi,REAL(R_ovofi
     &,4),L_ulufi,L_orofi,
     & L_emufi,L_urofi,L_abufi,L_ebufi,L_ibufi,L_ubufi,L_adufi
     &,L_obufi)
      !}

      if(L_adufi.or.L_ubufi.or.L_obufi.or.L_ibufi.or.L_ebufi.or.L_abufi
     &) then      
                  I_udufi = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_udufi = z'40000000'
      endif
C KPJ_vlv.fgi(  45, 152):���� ���������� �������� � ���������������� ��������,20KPJ70AA103
      Call PUMP2_HANDLER(deltat,I_iroki,I_eroki,R_amoki,
     & REAL(R_imoki,4),R_iloki,REAL(R_uloki,4),L_utoki,
     & L_atoki,L_oxoki,L_uxoki,L_upoki,L_esoki,L_osoki,L_isoki
     &,L_usoki,L_otoki,
     & L_eloki,L_emoki,L_utekad,L_oloki,L_axoki,
     & L_exoki,L_itekad,L_urekad,L_osekad,I_oroki,R_evoki
     &,R_ivoki,L_ibuki,
     & L_ekivo,L_ixoki,REAL(R8_irufad,8),L_itoki,REAL(R8_etoki
     &,8),R_abuki,
     & REAL(R_uroki,4),R_asoki,REAL(R8_opoki,8),R_ipoki,R8_ixufad
     &,R_ebuki,R8_etoki,
     & REAL(R_omoki,4),REAL(R_umoki,4))
C KPJ_vlv.fgi(  30, 230):���������� ���������� ������� 2,20KPJ21AT001
C label 1694  try1694=try1694-1
      L0_esekad=(L_osekad.or.L0_esekad).and..not.(L_asekad
     &)
      L_(172)=.not.L0_esekad
C KPG_KPH_logic.fgi( 348, 118):RS �������
      L_asekad = L0_esekad.AND.L_avekad
C KPG_KPH_logic.fgi( 358, 119):�
      Call PUMP2_HANDLER(deltat,I_udiki,I_odiki,R_ixeki,
     & REAL(R_uxeki,4),R_uveki,REAL(R_exeki,4),L_eliki,
     & L_ikiki,L_apiki,L_epiki,L_ediki,L_ofiki,L_akiki,L_ufiki
     &,L_ekiki,L_aliki,
     & L_oveki,L_oxeki,L_otekad,L_axeki,L_imiki,
     & L_omiki,L_etekad,L_asekad,L_isekad,I_afiki,R_oliki
     &,R_uliki,L_upiki,
     & L_ekivo,L_umiki,REAL(R8_irufad,8),L_ukiki,REAL(R8_okiki
     &,8),R_ipiki,
     & REAL(R_efiki,4),R_ifiki,REAL(R8_adiki,8),R_ubiki,R8_ixufad
     &,R_opiki,R8_okiki,
     & REAL(R_abiki,4),REAL(R_ebiki,4))
C KPJ_vlv.fgi(  45, 230):���������� ���������� ������� 2,20KPJ22AT001
C sav1=R_opiki
C sav2=R_ipiki
C sav3=L_eliki
C sav4=L_aliki
C sav5=L_ikiki
C sav6=R8_okiki
C sav7=R_ifiki
C sav8=I_afiki
C sav9=I_udiki
C sav10=I_odiki
C sav11=I_idiki
C sav12=L_isekad
C sav13=L_ediki
C sav14=R_ubiki
C sav15=L_oxeki
C sav16=L_axeki
      Call PUMP2_HANDLER(deltat,I_udiki,I_odiki,R_ixeki,
     & REAL(R_uxeki,4),R_uveki,REAL(R_exeki,4),L_eliki,
     & L_ikiki,L_apiki,L_epiki,L_ediki,L_ofiki,L_akiki,L_ufiki
     &,L_ekiki,L_aliki,
     & L_oveki,L_oxeki,L_otekad,L_axeki,L_imiki,
     & L_omiki,L_etekad,L_asekad,L_isekad,I_afiki,R_oliki
     &,R_uliki,L_upiki,
     & L_ekivo,L_umiki,REAL(R8_irufad,8),L_ukiki,REAL(R8_okiki
     &,8),R_ipiki,
     & REAL(R_efiki,4),R_ifiki,REAL(R8_adiki,8),R_ubiki,R8_ixufad
     &,R_opiki,R8_okiki,
     & REAL(R_abiki,4),REAL(R_ebiki,4))
C KPJ_vlv.fgi(  45, 230):recalc:���������� ���������� ������� 2,20KPJ22AT001
C if(sav1.ne.R_opiki .and. try1703.gt.0) goto 1703
C if(sav2.ne.R_ipiki .and. try1703.gt.0) goto 1703
C if(sav3.ne.L_eliki .and. try1703.gt.0) goto 1703
C if(sav4.ne.L_aliki .and. try1703.gt.0) goto 1703
C if(sav5.ne.L_ikiki .and. try1703.gt.0) goto 1703
C if(sav6.ne.R8_okiki .and. try1703.gt.0) goto 1703
C if(sav7.ne.R_ifiki .and. try1703.gt.0) goto 1703
C if(sav8.ne.I_afiki .and. try1703.gt.0) goto 1703
C if(sav9.ne.I_udiki .and. try1703.gt.0) goto 1703
C if(sav10.ne.I_odiki .and. try1703.gt.0) goto 1703
C if(sav11.ne.I_idiki .and. try1703.gt.0) goto 1703
C if(sav12.ne.L_isekad .and. try1703.gt.0) goto 1703
C if(sav13.ne.L_ediki .and. try1703.gt.0) goto 1703
C if(sav14.ne.R_ubiki .and. try1703.gt.0) goto 1703
C if(sav15.ne.L_oxeki .and. try1703.gt.0) goto 1703
C if(sav16.ne.L_axeki .and. try1703.gt.0) goto 1703
      L0_orekad=(L_isekad.or.L0_orekad).and..not.(L_urekad
     &)
      L_(171)=.not.L0_orekad
C KPG_KPH_logic.fgi( 348, 103):RS �������
      L_urekad = L0_orekad.AND.L_evekad
C KPG_KPH_logic.fgi( 358, 104):�
C sav1=R_ebuki
C sav2=R_abuki
C sav3=L_utoki
C sav4=L_otoki
C sav5=L_atoki
C sav6=R8_etoki
C sav7=R_asoki
C sav8=I_oroki
C sav9=I_iroki
C sav10=I_eroki
C sav11=I_aroki
C sav12=L_osekad
C sav13=L_upoki
C sav14=R_ipoki
C sav15=L_emoki
C sav16=L_oloki
      Call PUMP2_HANDLER(deltat,I_iroki,I_eroki,R_amoki,
     & REAL(R_imoki,4),R_iloki,REAL(R_uloki,4),L_utoki,
     & L_atoki,L_oxoki,L_uxoki,L_upoki,L_esoki,L_osoki,L_isoki
     &,L_usoki,L_otoki,
     & L_eloki,L_emoki,L_utekad,L_oloki,L_axoki,
     & L_exoki,L_itekad,L_urekad,L_osekad,I_oroki,R_evoki
     &,R_ivoki,L_ibuki,
     & L_ekivo,L_ixoki,REAL(R8_irufad,8),L_itoki,REAL(R8_etoki
     &,8),R_abuki,
     & REAL(R_uroki,4),R_asoki,REAL(R8_opoki,8),R_ipoki,R8_ixufad
     &,R_ebuki,R8_etoki,
     & REAL(R_omoki,4),REAL(R_umoki,4))
C KPJ_vlv.fgi(  30, 230):recalc:���������� ���������� ������� 2,20KPJ21AT001
C if(sav1.ne.R_ebuki .and. try1694.gt.0) goto 1694
C if(sav2.ne.R_abuki .and. try1694.gt.0) goto 1694
C if(sav3.ne.L_utoki .and. try1694.gt.0) goto 1694
C if(sav4.ne.L_otoki .and. try1694.gt.0) goto 1694
C if(sav5.ne.L_atoki .and. try1694.gt.0) goto 1694
C if(sav6.ne.R8_etoki .and. try1694.gt.0) goto 1694
C if(sav7.ne.R_asoki .and. try1694.gt.0) goto 1694
C if(sav8.ne.I_oroki .and. try1694.gt.0) goto 1694
C if(sav9.ne.I_iroki .and. try1694.gt.0) goto 1694
C if(sav10.ne.I_eroki .and. try1694.gt.0) goto 1694
C if(sav11.ne.I_aroki .and. try1694.gt.0) goto 1694
C if(sav12.ne.L_osekad .and. try1694.gt.0) goto 1694
C if(sav13.ne.L_upoki .and. try1694.gt.0) goto 1694
C if(sav14.ne.R_ipoki .and. try1694.gt.0) goto 1694
C if(sav15.ne.L_emoki .and. try1694.gt.0) goto 1694
C if(sav16.ne.L_oloki .and. try1694.gt.0) goto 1694
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_utufi,4),R8_asufi
     &,I_ebaki,I_ubaki,I_abaki,
     & C8_isufi,I_obaki,R_etufi,R_atufi,R_apufi,
     & REAL(R_ipufi,4),R_erufi,REAL(R_orufi,4),
     & R_opufi,REAL(R_arufi,4),I_oxufi,I_adaki,I_ibaki,I_ixufi
     &,L_urufi,
     & L_idaki,L_ifaki,L_irufi,L_upufi,
     & L_esufi,L_adikad,L_udaki,L_epufi,L_usufi,L_akaki,L_itufi
     &,
     & L_otufi,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_
     &(109),L_imufi,L_(110),
     & L_omufi,L_odaki,I_edaki,L_ofaki,R_exufi,REAL(R_osufi
     &,4),L_ufaki,L_umufi,
     & L_ekaki,L_uxekad,L_avufi,L_evufi,L_ivufi,L_uvufi,L_axufi
     &,L_ovufi)
      !}

      if(L_axufi.or.L_uvufi.or.L_ovufi.or.L_ivufi.or.L_evufi.or.L_avufi
     &) then      
                  I_uxufi = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_uxufi = z'40000000'
      endif
C KPJ_vlv.fgi(  30, 152):���� ���������� �������� � ���������������� ��������,20KPJ70AA102
C label 1712  try1712=try1712-1
      L_abikad = L_afikad.OR.L_uxekad
C KPG_KPH_logic.fgi( 254, 142):���
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_odofi,4),R8_uxifi
     &,I_alofi,I_olofi,I_ukofi,
     & C8_ebofi,I_ilofi,R_adofi,R_ubofi,R_utifi,
     & REAL(R_evifi,4),R_axifi,REAL(R_ixifi,4),
     & R_ivifi,REAL(R_uvifi,4),I_ikofi,I_ulofi,I_elofi,I_ekofi
     &,L_oxifi,
     & L_emofi,L_epofi,L_exifi,L_ovifi,
     & L_abofi,L_abikad,L_omofi,L_avifi,L_obofi,L_upofi,L_edofi
     &,
     & L_idofi,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_
     &(105),L_etifi,L_(106),
     & L_itifi,L_imofi,I_amofi,L_ipofi,R_akofi,REAL(R_ibofi
     &,4),L_opofi,L_otifi,
     & L_arofi,L_obikad,L_udofi,L_afofi,L_efofi,L_ofofi,L_ufofi
     &,L_ifofi)
      !}

      if(L_ufofi.or.L_ofofi.or.L_ifofi.or.L_efofi.or.L_afofi.or.L_udofi
     &) then      
                  I_okofi = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_okofi = z'40000000'
      endif
C KPJ_vlv.fgi(  60, 152):���� ���������� �������� � ���������������� ��������,20KPJ70AA104
      L_adikad = L_ubikad.OR.L_obikad.OR.L_ibikad.OR.L_ebikad
C KPG_KPH_logic.fgi( 257, 161):���
      L_apokad = L_umokad.OR.L_elokad
C KPG_KPH_logic.fgi(  59, 180):���
C label 1724  try1724=try1724-1
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_isabi,4),R8_opabi
     &,I_uvabi,I_ixabi,I_ovabi,
     & C8_arabi,I_exabi,R_urabi,R_orabi,R_olabi,
     & REAL(R_amabi,4),R_umabi,REAL(R_epabi,4),
     & R_emabi,REAL(R_omabi,4),I_evabi,I_oxabi,I_axabi,I_avabi
     &,L_ipabi,
     & L_abebi,L_adebi,L_apabi,L_imabi,
     & L_upabi,L_apokad,L_ibebi,L_ulabi,L_irabi,L_odebi,L_asabi
     &,
     & L_esabi,REAL(R8_irufad,8),REAL(1.0,4),R8_ixufad,L_
     &(75),L_elabi,L_(76),
     & L_ilabi,L_ebebi,I_uxabi,L_edebi,R_utabi,REAL(R_erabi
     &,4),L_idebi,L_ulokad,
     & L_udebi,L_umokad,L_osabi,L_usabi,L_atabi,L_itabi,L_otabi
     &,L_etabi)
      !}

      if(L_otabi.or.L_itabi.or.L_etabi.or.L_atabi.or.L_usabi.or.L_osabi
     &) then      
                  I_ivabi = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ivabi = z'40000000'
      endif
C KPJ_vlv.fgi(  45, 102):���� ���������� �������� � ���������������� ��������,20KPJ51AA101
C sav1=L_apokad
      L_apokad = L_umokad.OR.L_elokad
C KPG_KPH_logic.fgi(  59, 180):recalc:���
C if(sav1.ne.L_apokad .and. try1724.gt.0) goto 1724
      L_(181) = L_ulokad.OR.L_olokad
C KPG_KPH_logic.fgi(  44, 161):���
      L_(182) = L_amokad.AND.L_(181)
C KPG_KPH_logic.fgi(  48, 164):�
      L_emokad = L_(182).OR.L_ilokad.OR.L_elokad
C KPG_KPH_logic.fgi(  59, 162):���
      Call PUMP2_HANDLER(deltat,I_axiki,I_uviki,R_isiki,
     & REAL(R_usiki,4),R_uriki,REAL(R_esiki,4),L_idoki,
     & L_oboki,L_ekoki,L_ikoki,L_eviki,L_uxiki,L_eboki,L_aboki
     &,L_iboki,L_edoki,
     & L_oriki,L_osiki,L_iriki,L_asiki,L_ofoki,
     & L_ufoki,L_eriki,L_ariki,L_iviki,I_exiki,R_udoki,R_afoki
     &,L_aloki,
     & L_ekivo,L_akoki,REAL(R8_irufad,8),L_adoki,REAL(R8_uboki
     &,8),R_okoki,
     & REAL(R_ixiki,4),R_oxiki,REAL(R8_aviki,8),R_utiki,R8_ixufad
     &,R_ukoki,R8_uboki,
     & REAL(R_atiki,4),REAL(R_etiki,4))
C KPJ_vlv.fgi(  60, 230):���������� ���������� ������� 2,20KPJ23AP001
C label 1739  try1739=try1739-1
C sav1=R_ukoki
C sav2=R_okoki
C sav3=L_idoki
C sav4=L_edoki
C sav5=L_oboki
C sav6=R8_uboki
C sav7=R_oxiki
C sav8=I_exiki
C sav9=I_axiki
C sav10=I_uviki
C sav11=I_oviki
C sav12=L_iviki
C sav13=L_eviki
C sav14=R_utiki
C sav15=L_osiki
C sav16=L_asiki
      Call PUMP2_HANDLER(deltat,I_axiki,I_uviki,R_isiki,
     & REAL(R_usiki,4),R_uriki,REAL(R_esiki,4),L_idoki,
     & L_oboki,L_ekoki,L_ikoki,L_eviki,L_uxiki,L_eboki,L_aboki
     &,L_iboki,L_edoki,
     & L_oriki,L_osiki,L_iriki,L_asiki,L_ofoki,
     & L_ufoki,L_eriki,L_ariki,L_iviki,I_exiki,R_udoki,R_afoki
     &,L_aloki,
     & L_ekivo,L_akoki,REAL(R8_irufad,8),L_adoki,REAL(R8_uboki
     &,8),R_okoki,
     & REAL(R_ixiki,4),R_oxiki,REAL(R8_aviki,8),R_utiki,R8_ixufad
     &,R_ukoki,R8_uboki,
     & REAL(R_atiki,4),REAL(R_etiki,4))
C KPJ_vlv.fgi(  60, 230):recalc:���������� ���������� ������� 2,20KPJ23AP001
C if(sav1.ne.R_ukoki .and. try1739.gt.0) goto 1739
C if(sav2.ne.R_okoki .and. try1739.gt.0) goto 1739
C if(sav3.ne.L_idoki .and. try1739.gt.0) goto 1739
C if(sav4.ne.L_edoki .and. try1739.gt.0) goto 1739
C if(sav5.ne.L_oboki .and. try1739.gt.0) goto 1739
C if(sav6.ne.R8_uboki .and. try1739.gt.0) goto 1739
C if(sav7.ne.R_oxiki .and. try1739.gt.0) goto 1739
C if(sav8.ne.I_exiki .and. try1739.gt.0) goto 1739
C if(sav9.ne.I_axiki .and. try1739.gt.0) goto 1739
C if(sav10.ne.I_uviki .and. try1739.gt.0) goto 1739
C if(sav11.ne.I_oviki .and. try1739.gt.0) goto 1739
C if(sav12.ne.L_iviki .and. try1739.gt.0) goto 1739
C if(sav13.ne.L_eviki .and. try1739.gt.0) goto 1739
C if(sav14.ne.R_utiki .and. try1739.gt.0) goto 1739
C if(sav15.ne.L_osiki .and. try1739.gt.0) goto 1739
C if(sav16.ne.L_asiki .and. try1739.gt.0) goto 1739
      Call PUMP2_HANDLER(deltat,I_imeki,I_emeki,R_ufeki,
     & REAL(R_ekeki,4),R_efeki,REAL(R_ofeki,4),L_ureki,
     & L_areki,L_oteki,L_uteki,L_oleki,L_epeki,L_opeki,L_ipeki
     &,L_upeki,L_oreki,
     & L_afeki,L_akeki,L_ikikad,L_ifeki,L_ateki,
     & L_eteki,L_udeki,L_odeki,L_uleki,I_omeki,R_eseki,R_iseki
     &,L_iveki,
     & L_ekivo,L_iteki,REAL(R8_irufad,8),L_ireki,REAL(R8_ereki
     &,8),R_aveki,
     & REAL(R_umeki,4),R_apeki,REAL(R8_ileki,8),R_eleki,R8_ixufad
     &,R_eveki,R8_ereki,
     & REAL(R_ikeki,4),REAL(R_okeki,4))
C KPJ_vlv.fgi(  75, 230):���������� ���������� ������� 2,20KPJ61AP001
C label 1740  try1740=try1740-1
C sav1=R_eveki
C sav2=R_aveki
C sav3=L_ureki
C sav4=L_oreki
C sav5=L_areki
C sav6=R8_ereki
C sav7=R_apeki
C sav8=I_omeki
C sav9=I_imeki
C sav10=I_emeki
C sav11=I_ameki
C sav12=L_uleki
C sav13=L_oleki
C sav14=R_eleki
C sav15=L_akeki
C sav16=L_ifeki
      Call PUMP2_HANDLER(deltat,I_imeki,I_emeki,R_ufeki,
     & REAL(R_ekeki,4),R_efeki,REAL(R_ofeki,4),L_ureki,
     & L_areki,L_oteki,L_uteki,L_oleki,L_epeki,L_opeki,L_ipeki
     &,L_upeki,L_oreki,
     & L_afeki,L_akeki,L_ikikad,L_ifeki,L_ateki,
     & L_eteki,L_udeki,L_odeki,L_uleki,I_omeki,R_eseki,R_iseki
     &,L_iveki,
     & L_ekivo,L_iteki,REAL(R8_irufad,8),L_ireki,REAL(R8_ereki
     &,8),R_aveki,
     & REAL(R_umeki,4),R_apeki,REAL(R8_ileki,8),R_eleki,R8_ixufad
     &,R_eveki,R8_ereki,
     & REAL(R_ikeki,4),REAL(R_okeki,4))
C KPJ_vlv.fgi(  75, 230):recalc:���������� ���������� ������� 2,20KPJ61AP001
C if(sav1.ne.R_eveki .and. try1740.gt.0) goto 1740
C if(sav2.ne.R_aveki .and. try1740.gt.0) goto 1740
C if(sav3.ne.L_ureki .and. try1740.gt.0) goto 1740
C if(sav4.ne.L_oreki .and. try1740.gt.0) goto 1740
C if(sav5.ne.L_areki .and. try1740.gt.0) goto 1740
C if(sav6.ne.R8_ereki .and. try1740.gt.0) goto 1740
C if(sav7.ne.R_apeki .and. try1740.gt.0) goto 1740
C if(sav8.ne.I_omeki .and. try1740.gt.0) goto 1740
C if(sav9.ne.I_imeki .and. try1740.gt.0) goto 1740
C if(sav10.ne.I_emeki .and. try1740.gt.0) goto 1740
C if(sav11.ne.I_ameki .and. try1740.gt.0) goto 1740
C if(sav12.ne.L_uleki .and. try1740.gt.0) goto 1740
C if(sav13.ne.L_oleki .and. try1740.gt.0) goto 1740
C if(sav14.ne.R_eleki .and. try1740.gt.0) goto 1740
C if(sav15.ne.L_akeki .and. try1740.gt.0) goto 1740
C if(sav16.ne.L_ifeki .and. try1740.gt.0) goto 1740
      L_uk = L_okilad.AND.(.NOT.L_usuvu)
C KPG_KPH_blk.fgi(  41, 364):�
C label 1741  try1741=try1741-1
      L_ekuvu=L_uk
C KPG_KPH_blk.fgi(  64, 364):������,20KPG11AA110_ulucl
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_emuvu,4),L_osuvu,L_usuvu
     &,R8_ufuvu,C30_eluvu,
     & L_alafad,L_ulafad,L_utafad,I_oruvu,I_uruvu,R_oluvu
     &,R_iluvu,
     & R_ubuvu,REAL(R_eduvu,4),R_afuvu,
     & REAL(R_ifuvu,4),R_iduvu,REAL(R_uduvu,4),I_upuvu,
     & I_asuvu,I_iruvu,I_eruvu,L_ofuvu,L_esuvu,L_otuvu,L_efuvu
     &,
     & L_oduvu,L_okuvu,L_ikuvu,L_atuvu,L_aduvu,L_aluvu,
     & L_evuvu,L_isuvu,L_uluvu,L_amuvu,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_ekuvu,L_akuvu,L_utuvu,R_opuvu,REAL(R_ukuvu,4),L_avuvu
     &,L_ivuvu,
     & L_imuvu,L_omuvu,L_umuvu,L_epuvu,L_ipuvu,L_apuvu)
      !}

      if(L_ipuvu.or.L_epuvu.or.L_apuvu.or.L_umuvu.or.L_omuvu.or.L_imuvu
     &) then      
                  I_aruvu = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_aruvu = z'40000000'
      endif
C KPG_vlv.fgi( 131, 112):���� ���������� �������� ��������,20KPG11AA110
C sav1=L_uk
      L_uk = L_okilad.AND.(.NOT.L_usuvu)
C KPG_KPH_blk.fgi(  41, 364):recalc:�
C if(sav1.ne.L_uk .and. try1741.gt.0) goto 1741
      L_ok=L_uk
C KPG_KPH_blk.fgi(  64, 359):������,20KPG12AP001_uluoff1
      Call PUMP2_HANDLER(deltat,I_aduxe,I_ubuxe,R_ivoxe,
     & REAL(R_uvoxe,4),R_utoxe,REAL(R_evoxe,4),L_ikuxe,
     & L_ofuxe,L_emuxe,L_imuxe,L_ebuxe,L_uduxe,L_efuxe,L_afuxe
     &,L_ifuxe,L_ekuxe,
     & L_otoxe,L_ovoxe,L_emokad,L_avoxe,L_oluxe,
     & L_uluxe,L_itoxe,L_etoxe,L_ibuxe,I_eduxe,R_ukuxe,R_aluxe
     &,L_apuxe,
     & L_ekivo,L_amuxe,REAL(R8_irufad,8),L_akuxe,REAL(R8_ufuxe
     &,8),R_omuxe,
     & REAL(R_iduxe,4),R_oduxe,REAL(R8_abuxe,8),R_uxoxe,R8_ixufad
     &,R_umuxe,R8_ufuxe,
     & REAL(R_axoxe,4),REAL(R_exoxe,4))
C KPJ_vlv.fgi(  90, 230):���������� ���������� ������� 2,20KPJ62AP001
C label 1755  try1755=try1755-1
C sav1=R_umuxe
C sav2=R_omuxe
C sav3=L_ikuxe
C sav4=L_ekuxe
C sav5=L_ofuxe
C sav6=R8_ufuxe
C sav7=R_oduxe
C sav8=I_eduxe
C sav9=I_aduxe
C sav10=I_ubuxe
C sav11=I_obuxe
C sav12=L_ibuxe
C sav13=L_ebuxe
C sav14=R_uxoxe
C sav15=L_ovoxe
C sav16=L_avoxe
      Call PUMP2_HANDLER(deltat,I_aduxe,I_ubuxe,R_ivoxe,
     & REAL(R_uvoxe,4),R_utoxe,REAL(R_evoxe,4),L_ikuxe,
     & L_ofuxe,L_emuxe,L_imuxe,L_ebuxe,L_uduxe,L_efuxe,L_afuxe
     &,L_ifuxe,L_ekuxe,
     & L_otoxe,L_ovoxe,L_emokad,L_avoxe,L_oluxe,
     & L_uluxe,L_itoxe,L_etoxe,L_ibuxe,I_eduxe,R_ukuxe,R_aluxe
     &,L_apuxe,
     & L_ekivo,L_amuxe,REAL(R8_irufad,8),L_akuxe,REAL(R8_ufuxe
     &,8),R_omuxe,
     & REAL(R_iduxe,4),R_oduxe,REAL(R8_abuxe,8),R_uxoxe,R8_ixufad
     &,R_umuxe,R8_ufuxe,
     & REAL(R_axoxe,4),REAL(R_exoxe,4))
C KPJ_vlv.fgi(  90, 230):recalc:���������� ���������� ������� 2,20KPJ62AP001
C if(sav1.ne.R_umuxe .and. try1755.gt.0) goto 1755
C if(sav2.ne.R_omuxe .and. try1755.gt.0) goto 1755
C if(sav3.ne.L_ikuxe .and. try1755.gt.0) goto 1755
C if(sav4.ne.L_ekuxe .and. try1755.gt.0) goto 1755
C if(sav5.ne.L_ofuxe .and. try1755.gt.0) goto 1755
C if(sav6.ne.R8_ufuxe .and. try1755.gt.0) goto 1755
C if(sav7.ne.R_oduxe .and. try1755.gt.0) goto 1755
C if(sav8.ne.I_eduxe .and. try1755.gt.0) goto 1755
C if(sav9.ne.I_aduxe .and. try1755.gt.0) goto 1755
C if(sav10.ne.I_ubuxe .and. try1755.gt.0) goto 1755
C if(sav11.ne.I_obuxe .and. try1755.gt.0) goto 1755
C if(sav12.ne.L_ibuxe .and. try1755.gt.0) goto 1755
C if(sav13.ne.L_ebuxe .and. try1755.gt.0) goto 1755
C if(sav14.ne.R_uxoxe .and. try1755.gt.0) goto 1755
C if(sav15.ne.L_ovoxe .and. try1755.gt.0) goto 1755
C if(sav16.ne.L_avoxe .and. try1755.gt.0) goto 1755
      L_ef = L_etelad.AND.(.NOT.L_okeku)
C KPG_KPH_blk.fgi(  41, 274):�
C label 1756  try1756=try1756-1
      L_avaku=L_ef
C KPG_KPH_blk.fgi(  64, 274):������,20KPG31AA110_ulucl
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_abeku,4),L_ikeku,L_okeku
     &,R8_otaku,C30_axaku,
     & L_odaxu,L_ifaxu,L_iraxu,I_ifeku,I_ofeku,R_ixaku,R_exaku
     &,
     & R_oraku,REAL(R_asaku,4),R_usaku,
     & REAL(R_etaku,4),R_esaku,REAL(R_osaku,4),I_odeku,
     & I_ufeku,I_efeku,I_afeku,L_itaku,L_akeku,L_ileku,L_ataku
     &,
     & L_isaku,L_ivaku,L_evaku,L_ukeku,L_uraku,L_uvaku,
     & L_ameku,L_ekeku,L_oxaku,L_uxaku,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_avaku,L_utaku,L_oleku,R_ideku,REAL(R_ovaku,4),L_uleku
     &,L_emeku,
     & L_ebeku,L_ibeku,L_obeku,L_adeku,L_edeku,L_ubeku)
      !}

      if(L_edeku.or.L_adeku.or.L_ubeku.or.L_obeku.or.L_ibeku.or.L_ebeku
     &) then      
                  I_udeku = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_udeku = z'40000000'
      endif
C KPG_vlv.fgi( 145, 136):���� ���������� �������� ��������,20KPG31AA110
C sav1=L_ef
      L_ef = L_etelad.AND.(.NOT.L_okeku)
C KPG_KPH_blk.fgi(  41, 274):recalc:�
C if(sav1.ne.L_ef .and. try1756.gt.0) goto 1756
      L_uf=L_ef
C KPG_KPH_blk.fgi(  64, 269):������,20KPG12AP001_uluoff3
      L_af=L_ef
C KPG_KPH_blk.fgi(  64, 259):������,20KPG32AP001_uluoff2
      L_of=L_ef
C KPG_KPH_blk.fgi(  64, 264):������,20KPG12AP002_uluoff3
      L_ik = L_ibilad.AND.(.NOT.L_ulefu)
C KPG_KPH_blk.fgi(  41, 346):�
C label 1778  try1778=try1778-1
      L_exafu=L_ik
C KPG_KPH_blk.fgi(  64, 346):������,20KPG21AA110_ulucl
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_edefu,4),L_olefu,L_ulefu
     &,R8_uvafu,C30_ebefu,
     & L_ufolu,L_okolu,L_osolu,I_okefu,I_ukefu,R_obefu,R_ibefu
     &,
     & R_usafu,REAL(R_etafu,4),R_avafu,
     & REAL(R_ivafu,4),R_itafu,REAL(R_utafu,4),I_ufefu,
     & I_alefu,I_ikefu,I_ekefu,L_ovafu,L_elefu,L_omefu,L_evafu
     &,
     & L_otafu,L_oxafu,L_ixafu,L_amefu,L_atafu,L_abefu,
     & L_epefu,L_ilefu,L_ubefu,L_adefu,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_exafu,L_axafu,L_umefu,R_ofefu,REAL(R_uxafu,4),L_apefu
     &,L_ipefu,
     & L_idefu,L_odefu,L_udefu,L_efefu,L_ifefu,L_afefu)
      !}

      if(L_ifefu.or.L_efefu.or.L_afefu.or.L_udefu.or.L_odefu.or.L_idefu
     &) then      
                  I_akefu = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_akefu = z'40000000'
      endif
C KPG_vlv.fgi( 145,  16):���� ���������� �������� ��������,20KPG21AA110
C sav1=L_ik
      L_ik = L_ibilad.AND.(.NOT.L_ulefu)
C KPG_KPH_blk.fgi(  41, 346):recalc:�
C if(sav1.ne.L_ik .and. try1778.gt.0) goto 1778
      L_ek=L_ik
C KPG_KPH_blk.fgi(  64, 341):������,20KPG12AP001_uluoff2
      L_ubavo = L_ok.OR.L_ek.OR.L_uf
C KPG_KPH_blk.fgi( 123, 379):���
      L_if=L_ik
C KPG_KPH_blk.fgi(  64, 331):������,20KPG32AP001_uluoff1
      L_isevo = L_if.OR.L_af
C KPG_KPH_blk.fgi( 123, 351):���
      L_ovavo = L_ik.OR.L_ef
C KPG_KPH_blk.fgi( 123, 342):���
      L_ak=L_ik
C KPG_KPH_blk.fgi(  64, 336):������,20KPG12AP002_uluoff2
      L_akuto = L_uk.OR.L_ak.OR.L_of
C KPG_KPH_blk.fgi( 123, 364):���
      Call PUMP2_HANDLER(deltat,I_aloxe,I_ukoxe,R_idoxe,
     & REAL(R_udoxe,4),R_uboxe,REAL(R_edoxe,4),L_ipoxe,
     & L_omoxe,L_esoxe,L_isoxe,L_ekoxe,L_uloxe,L_emoxe,L_amoxe
     &,L_imoxe,L_epoxe,
     & L_oboxe,L_odoxe,L_ulikad,L_adoxe,L_oroxe,
     & L_uroxe,L_iboxe,L_eboxe,L_ikoxe,I_eloxe,R_upoxe,R_aroxe
     &,L_atoxe,
     & L_ekivo,L_asoxe,REAL(R8_irufad,8),L_apoxe,REAL(R8_umoxe
     &,8),R_osoxe,
     & REAL(R_iloxe,4),R_oloxe,REAL(R8_akoxe,8),R_ufoxe,R8_ixufad
     &,R_usoxe,R8_umoxe,
     & REAL(R_afoxe,4),REAL(R_efoxe,4))
C KPJ_vlv.fgi( 105, 230):���������� ���������� ������� 2,20KPJ82AP001
C label 1812  try1812=try1812-1
C sav1=R_usoxe
C sav2=R_osoxe
C sav3=L_ipoxe
C sav4=L_epoxe
C sav5=L_omoxe
C sav6=R8_umoxe
C sav7=R_oloxe
C sav8=I_eloxe
C sav9=I_aloxe
C sav10=I_ukoxe
C sav11=I_okoxe
C sav12=L_ikoxe
C sav13=L_ekoxe
C sav14=R_ufoxe
C sav15=L_odoxe
C sav16=L_adoxe
      Call PUMP2_HANDLER(deltat,I_aloxe,I_ukoxe,R_idoxe,
     & REAL(R_udoxe,4),R_uboxe,REAL(R_edoxe,4),L_ipoxe,
     & L_omoxe,L_esoxe,L_isoxe,L_ekoxe,L_uloxe,L_emoxe,L_amoxe
     &,L_imoxe,L_epoxe,
     & L_oboxe,L_odoxe,L_ulikad,L_adoxe,L_oroxe,
     & L_uroxe,L_iboxe,L_eboxe,L_ikoxe,I_eloxe,R_upoxe,R_aroxe
     &,L_atoxe,
     & L_ekivo,L_asoxe,REAL(R8_irufad,8),L_apoxe,REAL(R8_umoxe
     &,8),R_osoxe,
     & REAL(R_iloxe,4),R_oloxe,REAL(R8_akoxe,8),R_ufoxe,R8_ixufad
     &,R_usoxe,R8_umoxe,
     & REAL(R_afoxe,4),REAL(R_efoxe,4))
C KPJ_vlv.fgi( 105, 230):recalc:���������� ���������� ������� 2,20KPJ82AP001
C if(sav1.ne.R_usoxe .and. try1812.gt.0) goto 1812
C if(sav2.ne.R_osoxe .and. try1812.gt.0) goto 1812
C if(sav3.ne.L_ipoxe .and. try1812.gt.0) goto 1812
C if(sav4.ne.L_epoxe .and. try1812.gt.0) goto 1812
C if(sav5.ne.L_omoxe .and. try1812.gt.0) goto 1812
C if(sav6.ne.R8_umoxe .and. try1812.gt.0) goto 1812
C if(sav7.ne.R_oloxe .and. try1812.gt.0) goto 1812
C if(sav8.ne.I_eloxe .and. try1812.gt.0) goto 1812
C if(sav9.ne.I_aloxe .and. try1812.gt.0) goto 1812
C if(sav10.ne.I_ukoxe .and. try1812.gt.0) goto 1812
C if(sav11.ne.I_okoxe .and. try1812.gt.0) goto 1812
C if(sav12.ne.L_ikoxe .and. try1812.gt.0) goto 1812
C if(sav13.ne.L_ekoxe .and. try1812.gt.0) goto 1812
C if(sav14.ne.R_ufoxe .and. try1812.gt.0) goto 1812
C if(sav15.ne.L_odoxe .and. try1812.gt.0) goto 1812
C if(sav16.ne.L_adoxe .and. try1812.gt.0) goto 1812
      L_od = L_edukad.AND.(.NOT.L_otebo)
C KPG_KPH_blk.fgi(  41, 217):�
C label 1813  try1813=try1813-1
      L_alebo=L_od
C KPG_KPH_blk.fgi(  64, 217):������,20KPH30AA106_ulucl
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_apebo,4),L_itebo,L_otebo
     &,R8_okebo,C30_amebo,
     & L_oxubo,L_ibado,L_imado,I_isebo,I_osebo,R_imebo,R_emebo
     &,
     & R_odebo,REAL(R_afebo,4),R_ufebo,
     & REAL(R_ekebo,4),R_efebo,REAL(R_ofebo,4),I_orebo,
     & I_usebo,I_esebo,I_asebo,L_ikebo,L_atebo,L_ivebo,L_akebo
     &,
     & L_ifebo,L_ilebo,L_elebo,L_utebo,L_udebo,L_ulebo,
     & L_axebo,L_etebo,L_omebo,L_umebo,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_alebo,L_ukebo,L_ovebo,R_irebo,REAL(R_olebo,4),L_uvebo
     &,L_exebo,
     & L_epebo,L_ipebo,L_opebo,L_arebo,L_erebo,L_upebo)
      !}

      if(L_erebo.or.L_arebo.or.L_upebo.or.L_opebo.or.L_ipebo.or.L_epebo
     &) then      
                  I_urebo = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_urebo = z'40000000'
      endif
C KPH_vlv.fgi( 210,  35):���� ���������� �������� ��������,20KPH30AA106
C sav1=L_od
      L_od = L_edukad.AND.(.NOT.L_otebo)
C KPG_KPH_blk.fgi(  41, 217):recalc:�
C if(sav1.ne.L_od .and. try1813.gt.0) goto 1813
      L_id=L_od
C KPG_KPH_blk.fgi(  64, 212):������,20KPH31AP001_uluoff1
      L_exopi=L_id
C KPG_KPH_blk.fgi( 134, 212):������,20KPH31AP001_uluoff
      L_idopi=L_od
C KPG_KPH_blk.fgi( 134, 207):������,20KPH31AP002_uluoff
      L_ed = L_uvukad.AND.(.NOT.L_uxabo)
C KPG_KPH_blk.fgi(  41, 190):�
C label 1829  try1829=try1829-1
      L_epabo=L_ed
C KPG_KPH_blk.fgi(  64, 190):������,20KPH40AA105_ulucl
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_esabo,4),L_oxabo,L_uxabo
     &,R8_umabo,C30_erabo,
     & L_ikodo,L_elodo,L_etodo,I_ovabo,I_uvabo,R_orabo,R_irabo
     &,
     & R_ukabo,REAL(R_elabo,4),R_amabo,
     & REAL(R_imabo,4),R_ilabo,REAL(R_ulabo,4),I_utabo,
     & I_axabo,I_ivabo,I_evabo,L_omabo,L_exabo,L_obebo,L_emabo
     &,
     & L_olabo,L_opabo,L_ipabo,L_abebo,L_alabo,L_arabo,
     & L_edebo,L_ixabo,L_urabo,L_asabo,REAL(R8_irufad,8),REAL
     &(1.0,4),R8_ixufad,
     & L_epabo,L_apabo,L_ubebo,R_otabo,REAL(R_upabo,4),L_adebo
     &,L_idebo,
     & L_isabo,L_osabo,L_usabo,L_etabo,L_itabo,L_atabo)
      !}

      if(L_itabo.or.L_etabo.or.L_atabo.or.L_usabo.or.L_osabo.or.L_isabo
     &) then      
                  I_avabo = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_avabo = z'40000000'
      endif
C KPH_vlv.fgi( 227,  35):���� ���������� �������� ��������,20KPH40AA105
C sav1=L_ed
      L_ed = L_uvukad.AND.(.NOT.L_uxabo)
C KPG_KPH_blk.fgi(  41, 190):recalc:�
C if(sav1.ne.L_ed .and. try1829.gt.0) goto 1829
      L_ad=L_ed
C KPG_KPH_blk.fgi(  64, 185):������,20KPH41AP001_uluoff1
      L_upari=L_ad
C KPG_KPH_blk.fgi( 134, 185):������,20KPH41AP001_uluoff
      L_atupi=L_ed
C KPG_KPH_blk.fgi( 134, 180):������,20KPH41AP002_uluoff
      Call PUMP2_HANDLER(deltat,I_alipe,I_ukipe,R_idipe,
     & REAL(R_udipe,4),R_ubipe,REAL(R_edipe,4),L_ipipe,
     & L_omipe,L_esipe,L_isipe,L_ekipe,L_ulipe,L_emipe,L_amipe
     &,L_imipe,L_epipe,
     & L_obipe,L_odipe,L_ibipe,L_adipe,L_oripe,
     & L_uripe,L_ebipe,L_abipe,L_ikipe,I_elipe,R_upipe,R_aripe
     &,L_atipe,
     & L_ekivo,L_asipe,REAL(R8_irufad,8),L_apipe,REAL(R8_umipe
     &,8),R_osipe,
     & REAL(R_ilipe,4),R_olipe,REAL(R8_akipe,8),R_ufipe,R8_ixufad
     &,R_usipe,R8_umipe,
     & REAL(R_afipe,4),REAL(R_efipe,4))
C KPJ_vlv.fgi( 121, 230):���������� ���������� ������� 2,20KPV15AN001
C label 1845  try1845=try1845-1
C sav1=R_usipe
C sav2=R_osipe
C sav3=L_ipipe
C sav4=L_epipe
C sav5=L_omipe
C sav6=R8_umipe
C sav7=R_olipe
C sav8=I_elipe
C sav9=I_alipe
C sav10=I_ukipe
C sav11=I_okipe
C sav12=L_ikipe
C sav13=L_ekipe
C sav14=R_ufipe
C sav15=L_odipe
C sav16=L_adipe
      Call PUMP2_HANDLER(deltat,I_alipe,I_ukipe,R_idipe,
     & REAL(R_udipe,4),R_ubipe,REAL(R_edipe,4),L_ipipe,
     & L_omipe,L_esipe,L_isipe,L_ekipe,L_ulipe,L_emipe,L_amipe
     &,L_imipe,L_epipe,
     & L_obipe,L_odipe,L_ibipe,L_adipe,L_oripe,
     & L_uripe,L_ebipe,L_abipe,L_ikipe,I_elipe,R_upipe,R_aripe
     &,L_atipe,
     & L_ekivo,L_asipe,REAL(R8_irufad,8),L_apipe,REAL(R8_umipe
     &,8),R_osipe,
     & REAL(R_ilipe,4),R_olipe,REAL(R8_akipe,8),R_ufipe,R8_ixufad
     &,R_usipe,R8_umipe,
     & REAL(R_afipe,4),REAL(R_efipe,4))
C KPJ_vlv.fgi( 121, 230):recalc:���������� ���������� ������� 2,20KPV15AN001
C if(sav1.ne.R_usipe .and. try1845.gt.0) goto 1845
C if(sav2.ne.R_osipe .and. try1845.gt.0) goto 1845
C if(sav3.ne.L_ipipe .and. try1845.gt.0) goto 1845
C if(sav4.ne.L_epipe .and. try1845.gt.0) goto 1845
C if(sav5.ne.L_omipe .and. try1845.gt.0) goto 1845
C if(sav6.ne.R8_umipe .and. try1845.gt.0) goto 1845
C if(sav7.ne.R_olipe .and. try1845.gt.0) goto 1845
C if(sav8.ne.I_elipe .and. try1845.gt.0) goto 1845
C if(sav9.ne.I_alipe .and. try1845.gt.0) goto 1845
C if(sav10.ne.I_ukipe .and. try1845.gt.0) goto 1845
C if(sav11.ne.I_okipe .and. try1845.gt.0) goto 1845
C if(sav12.ne.L_ikipe .and. try1845.gt.0) goto 1845
C if(sav13.ne.L_ekipe .and. try1845.gt.0) goto 1845
C if(sav14.ne.R_ufipe .and. try1845.gt.0) goto 1845
C if(sav15.ne.L_odipe .and. try1845.gt.0) goto 1845
C if(sav16.ne.L_adipe .and. try1845.gt.0) goto 1845
      Call PUMP2_HANDLER(deltat,I_upepe,I_opepe,R_elepe,
     & REAL(R_olepe,4),R_okepe,REAL(R_alepe,4),L_etepe,
     & L_isepe,L_axepe,L_exepe,L_apepe,L_orepe,L_asepe,L_urepe
     &,L_esepe,L_atepe,
     & L_ikepe,L_ilepe,L_ekepe,L_ukepe,L_ivepe,
     & L_ovepe,L_akepe,L_ufepe,L_epepe,I_arepe,R_otepe,R_utepe
     &,L_uxepe,
     & L_ekivo,L_uvepe,REAL(R8_irufad,8),L_usepe,REAL(R8_osepe
     &,8),R_ixepe,
     & REAL(R_erepe,4),R_irepe,REAL(R8_umepe,8),R_omepe,R8_ixufad
     &,R_oxepe,R8_osepe,
     & REAL(R_ulepe,4),REAL(R_amepe,4))
C KPJ_vlv.fgi( 137, 230):���������� ���������� ������� 2,20KPV15AN002
C label 1846  try1846=try1846-1
C sav1=R_oxepe
C sav2=R_ixepe
C sav3=L_etepe
C sav4=L_atepe
C sav5=L_isepe
C sav6=R8_osepe
C sav7=R_irepe
C sav8=I_arepe
C sav9=I_upepe
C sav10=I_opepe
C sav11=I_ipepe
C sav12=L_epepe
C sav13=L_apepe
C sav14=R_omepe
C sav15=L_ilepe
C sav16=L_ukepe
      Call PUMP2_HANDLER(deltat,I_upepe,I_opepe,R_elepe,
     & REAL(R_olepe,4),R_okepe,REAL(R_alepe,4),L_etepe,
     & L_isepe,L_axepe,L_exepe,L_apepe,L_orepe,L_asepe,L_urepe
     &,L_esepe,L_atepe,
     & L_ikepe,L_ilepe,L_ekepe,L_ukepe,L_ivepe,
     & L_ovepe,L_akepe,L_ufepe,L_epepe,I_arepe,R_otepe,R_utepe
     &,L_uxepe,
     & L_ekivo,L_uvepe,REAL(R8_irufad,8),L_usepe,REAL(R8_osepe
     &,8),R_ixepe,
     & REAL(R_erepe,4),R_irepe,REAL(R8_umepe,8),R_omepe,R8_ixufad
     &,R_oxepe,R8_osepe,
     & REAL(R_ulepe,4),REAL(R_amepe,4))
C KPJ_vlv.fgi( 137, 230):recalc:���������� ���������� ������� 2,20KPV15AN002
C if(sav1.ne.R_oxepe .and. try1846.gt.0) goto 1846
C if(sav2.ne.R_ixepe .and. try1846.gt.0) goto 1846
C if(sav3.ne.L_etepe .and. try1846.gt.0) goto 1846
C if(sav4.ne.L_atepe .and. try1846.gt.0) goto 1846
C if(sav5.ne.L_isepe .and. try1846.gt.0) goto 1846
C if(sav6.ne.R8_osepe .and. try1846.gt.0) goto 1846
C if(sav7.ne.R_irepe .and. try1846.gt.0) goto 1846
C if(sav8.ne.I_arepe .and. try1846.gt.0) goto 1846
C if(sav9.ne.I_upepe .and. try1846.gt.0) goto 1846
C if(sav10.ne.I_opepe .and. try1846.gt.0) goto 1846
C if(sav11.ne.I_ipepe .and. try1846.gt.0) goto 1846
C if(sav12.ne.L_epepe .and. try1846.gt.0) goto 1846
C if(sav13.ne.L_apepe .and. try1846.gt.0) goto 1846
C if(sav14.ne.R_omepe .and. try1846.gt.0) goto 1846
C if(sav15.ne.L_ilepe .and. try1846.gt.0) goto 1846
C if(sav16.ne.L_ukepe .and. try1846.gt.0) goto 1846
      Call PUMP_HANDLER(deltat,C30_uvevo,I_ibivo,L_odaxu,L_ifaxu
     &,L_iraxu,
     & I_obivo,I_ebivo,R_otevo,REAL(R_avevo,4),
     & R_atevo,REAL(R_itevo,4),I_ubivo,L_ikivo,L_ifivo,L_emivo
     &,
     & L_imivo,L_oxevo,L_odivo,L_afivo,L_udivo,L_efivo,L_akivo
     &,L_usevo,
     & L_utevo,L_osevo,L_etevo,L_olivo,L_(164),
     & L_ulivo,L_(163),L_isevo,L_esevo,L_uxevo,I_adivo,R_ukivo
     &,R_alivo,
     & L_apivo,L_ekivo,L_amivo,REAL(R8_irufad,8),L_ufivo,
     & REAL(R8_ofivo,8),R_omivo,REAL(R_edivo,4),R_idivo,REAL
     &(R8_ixevo,8),R_exevo,
     & R8_ixufad,R_umivo,R8_ofivo,REAL(R_evevo,4),REAL(R_ivevo
     &,4))
C KPG_vlv.fgi( 209, 281):���������� ���������� �������,20KPG32AP001
C label 1847  try1847=try1847-1
C sav1=R_umivo
C sav2=R_omivo
C sav3=L_ikivo
C sav4=L_akivo
C sav5=L_ifivo
C sav6=R8_ofivo
C sav7=R_idivo
C sav8=I_adivo
C sav9=I_ubivo
C sav10=I_obivo
C sav11=I_ibivo
C sav12=I_ebivo
C sav13=I_abivo
C sav14=L_uxevo
C sav15=L_oxevo
C sav16=R_exevo
C sav17=C30_uvevo
C sav18=L_utevo
C sav19=L_etevo
      Call PUMP_HANDLER(deltat,C30_uvevo,I_ibivo,L_odaxu,L_ifaxu
     &,L_iraxu,
     & I_obivo,I_ebivo,R_otevo,REAL(R_avevo,4),
     & R_atevo,REAL(R_itevo,4),I_ubivo,L_ikivo,L_ifivo,L_emivo
     &,
     & L_imivo,L_oxevo,L_odivo,L_afivo,L_udivo,L_efivo,L_akivo
     &,L_usevo,
     & L_utevo,L_osevo,L_etevo,L_olivo,L_(164),
     & L_ulivo,L_(163),L_isevo,L_esevo,L_uxevo,I_adivo,R_ukivo
     &,R_alivo,
     & L_apivo,L_ekivo,L_amivo,REAL(R8_irufad,8),L_ufivo,
     & REAL(R8_ofivo,8),R_omivo,REAL(R_edivo,4),R_idivo,REAL
     &(R8_ixevo,8),R_exevo,
     & R8_ixufad,R_umivo,R8_ofivo,REAL(R_evevo,4),REAL(R_ivevo
     &,4))
C KPG_vlv.fgi( 209, 281):recalc:���������� ���������� �������,20KPG32AP001
C if(sav1.ne.R_umivo .and. try1847.gt.0) goto 1847
C if(sav2.ne.R_omivo .and. try1847.gt.0) goto 1847
C if(sav3.ne.L_ikivo .and. try1847.gt.0) goto 1847
C if(sav4.ne.L_akivo .and. try1847.gt.0) goto 1847
C if(sav5.ne.L_ifivo .and. try1847.gt.0) goto 1847
C if(sav6.ne.R8_ofivo .and. try1847.gt.0) goto 1847
C if(sav7.ne.R_idivo .and. try1847.gt.0) goto 1847
C if(sav8.ne.I_adivo .and. try1847.gt.0) goto 1847
C if(sav9.ne.I_ubivo .and. try1847.gt.0) goto 1847
C if(sav10.ne.I_obivo .and. try1847.gt.0) goto 1847
C if(sav11.ne.I_ibivo .and. try1847.gt.0) goto 1847
C if(sav12.ne.I_ebivo .and. try1847.gt.0) goto 1847
C if(sav13.ne.I_abivo .and. try1847.gt.0) goto 1847
C if(sav14.ne.L_uxevo .and. try1847.gt.0) goto 1847
C if(sav15.ne.L_oxevo .and. try1847.gt.0) goto 1847
C if(sav16.ne.R_exevo .and. try1847.gt.0) goto 1847
C if(sav17.ne.C30_uvevo .and. try1847.gt.0) goto 1847
C if(sav18.ne.L_utevo .and. try1847.gt.0) goto 1847
C if(sav19.ne.L_etevo .and. try1847.gt.0) goto 1847
      Call PUMP_HANDLER(deltat,C30_adevo,I_ofevo,L_odaxu,L_ifaxu
     &,L_iraxu,
     & I_ufevo,I_ifevo,R_uxavo,REAL(R_ebevo,4),
     & R_exavo,REAL(R_oxavo,4),I_akevo,L_imevo,L_olevo,L_erevo
     &,
     & L_irevo,L_udevo,L_ukevo,L_elevo,L_alevo,L_ilevo,L_emevo
     &,L_axavo,
     & L_abevo,L_uvavo,L_ixavo,L_opevo,L_(162),
     & L_upevo,L_(161),L_ovavo,L_ivavo,L_afevo,I_ekevo,R_umevo
     &,R_apevo,
     & L_asevo,L_ekivo,L_arevo,REAL(R8_irufad,8),L_amevo,
     & REAL(R8_ulevo,8),R_orevo,REAL(R_ikevo,4),R_okevo,REAL
     &(R8_odevo,8),R_idevo,
     & R8_ixufad,R_urevo,R8_ulevo,REAL(R_ibevo,4),REAL(R_obevo
     &,4))
C KPG_vlv.fgi( 222, 281):���������� ���������� �������,20KPG32AP002
C label 1848  try1848=try1848-1
C sav1=R_urevo
C sav2=R_orevo
C sav3=L_imevo
C sav4=L_emevo
C sav5=L_olevo
C sav6=R8_ulevo
C sav7=R_okevo
C sav8=I_ekevo
C sav9=I_akevo
C sav10=I_ufevo
C sav11=I_ofevo
C sav12=I_ifevo
C sav13=I_efevo
C sav14=L_afevo
C sav15=L_udevo
C sav16=R_idevo
C sav17=C30_adevo
C sav18=L_abevo
C sav19=L_ixavo
      Call PUMP_HANDLER(deltat,C30_adevo,I_ofevo,L_odaxu,L_ifaxu
     &,L_iraxu,
     & I_ufevo,I_ifevo,R_uxavo,REAL(R_ebevo,4),
     & R_exavo,REAL(R_oxavo,4),I_akevo,L_imevo,L_olevo,L_erevo
     &,
     & L_irevo,L_udevo,L_ukevo,L_elevo,L_alevo,L_ilevo,L_emevo
     &,L_axavo,
     & L_abevo,L_uvavo,L_ixavo,L_opevo,L_(162),
     & L_upevo,L_(161),L_ovavo,L_ivavo,L_afevo,I_ekevo,R_umevo
     &,R_apevo,
     & L_asevo,L_ekivo,L_arevo,REAL(R8_irufad,8),L_amevo,
     & REAL(R8_ulevo,8),R_orevo,REAL(R_ikevo,4),R_okevo,REAL
     &(R8_odevo,8),R_idevo,
     & R8_ixufad,R_urevo,R8_ulevo,REAL(R_ibevo,4),REAL(R_obevo
     &,4))
C KPG_vlv.fgi( 222, 281):recalc:���������� ���������� �������,20KPG32AP002
C if(sav1.ne.R_urevo .and. try1848.gt.0) goto 1848
C if(sav2.ne.R_orevo .and. try1848.gt.0) goto 1848
C if(sav3.ne.L_imevo .and. try1848.gt.0) goto 1848
C if(sav4.ne.L_emevo .and. try1848.gt.0) goto 1848
C if(sav5.ne.L_olevo .and. try1848.gt.0) goto 1848
C if(sav6.ne.R8_ulevo .and. try1848.gt.0) goto 1848
C if(sav7.ne.R_okevo .and. try1848.gt.0) goto 1848
C if(sav8.ne.I_ekevo .and. try1848.gt.0) goto 1848
C if(sav9.ne.I_akevo .and. try1848.gt.0) goto 1848
C if(sav10.ne.I_ufevo .and. try1848.gt.0) goto 1848
C if(sav11.ne.I_ofevo .and. try1848.gt.0) goto 1848
C if(sav12.ne.I_ifevo .and. try1848.gt.0) goto 1848
C if(sav13.ne.I_efevo .and. try1848.gt.0) goto 1848
C if(sav14.ne.L_afevo .and. try1848.gt.0) goto 1848
C if(sav15.ne.L_udevo .and. try1848.gt.0) goto 1848
C if(sav16.ne.R_idevo .and. try1848.gt.0) goto 1848
C if(sav17.ne.C30_adevo .and. try1848.gt.0) goto 1848
C if(sav18.ne.L_abevo .and. try1848.gt.0) goto 1848
C if(sav19.ne.L_ixavo .and. try1848.gt.0) goto 1848
      Call PUMP_HANDLER(deltat,C30_uliri,I_ipiri,L_oboto,L_idoto
     &,L_ipoto,
     & I_opiri,I_epiri,R_okiri,REAL(R_aliri,4),
     & R_akiri,REAL(R_ikiri,4),I_upiri,L_etiri,L_isiri,L_axiri
     &,
     & L_exiri,L_omiri,L_oriri,L_asiri,L_uriri,L_esiri,L_atiri
     &,L_ufiri,
     & L_ukiri,L_ofiri,L_ekiri,L_iviri,L_(156),
     & L_oviri,L_(155),L_ifiri,L_efiri,L_umiri,I_ariri,R_otiri
     &,R_utiri,
     & L_uxiri,L_ekivo,L_uviri,REAL(R8_irufad,8),L_usiri,
     & REAL(R8_osiri,8),R_ixiri,REAL(R_eriri,4),R_iriri,REAL
     &(R8_imiri,8),R_emiri,
     & R8_ixufad,R_oxiri,R8_osiri,REAL(R_eliri,4),REAL(R_iliri
     &,4))
C KPH_vlv.fgi( 344, 180):���������� ���������� �������,20KPH15AP002
C label 1849  try1849=try1849-1
C sav1=R_oxiri
C sav2=R_ixiri
C sav3=L_etiri
C sav4=L_atiri
C sav5=L_isiri
C sav6=R8_osiri
C sav7=R_iriri
C sav8=I_ariri
C sav9=I_upiri
C sav10=I_opiri
C sav11=I_ipiri
C sav12=I_epiri
C sav13=I_apiri
C sav14=L_umiri
C sav15=L_omiri
C sav16=R_emiri
C sav17=C30_uliri
C sav18=L_ukiri
C sav19=L_ekiri
      Call PUMP_HANDLER(deltat,C30_uliri,I_ipiri,L_oboto,L_idoto
     &,L_ipoto,
     & I_opiri,I_epiri,R_okiri,REAL(R_aliri,4),
     & R_akiri,REAL(R_ikiri,4),I_upiri,L_etiri,L_isiri,L_axiri
     &,
     & L_exiri,L_omiri,L_oriri,L_asiri,L_uriri,L_esiri,L_atiri
     &,L_ufiri,
     & L_ukiri,L_ofiri,L_ekiri,L_iviri,L_(156),
     & L_oviri,L_(155),L_ifiri,L_efiri,L_umiri,I_ariri,R_otiri
     &,R_utiri,
     & L_uxiri,L_ekivo,L_uviri,REAL(R8_irufad,8),L_usiri,
     & REAL(R8_osiri,8),R_ixiri,REAL(R_eriri,4),R_iriri,REAL
     &(R8_imiri,8),R_emiri,
     & R8_ixufad,R_oxiri,R8_osiri,REAL(R_eliri,4),REAL(R_iliri
     &,4))
C KPH_vlv.fgi( 344, 180):recalc:���������� ���������� �������,20KPH15AP002
C if(sav1.ne.R_oxiri .and. try1849.gt.0) goto 1849
C if(sav2.ne.R_ixiri .and. try1849.gt.0) goto 1849
C if(sav3.ne.L_etiri .and. try1849.gt.0) goto 1849
C if(sav4.ne.L_atiri .and. try1849.gt.0) goto 1849
C if(sav5.ne.L_isiri .and. try1849.gt.0) goto 1849
C if(sav6.ne.R8_osiri .and. try1849.gt.0) goto 1849
C if(sav7.ne.R_iriri .and. try1849.gt.0) goto 1849
C if(sav8.ne.I_ariri .and. try1849.gt.0) goto 1849
C if(sav9.ne.I_upiri .and. try1849.gt.0) goto 1849
C if(sav10.ne.I_opiri .and. try1849.gt.0) goto 1849
C if(sav11.ne.I_ipiri .and. try1849.gt.0) goto 1849
C if(sav12.ne.I_epiri .and. try1849.gt.0) goto 1849
C if(sav13.ne.I_apiri .and. try1849.gt.0) goto 1849
C if(sav14.ne.L_umiri .and. try1849.gt.0) goto 1849
C if(sav15.ne.L_omiri .and. try1849.gt.0) goto 1849
C if(sav16.ne.R_emiri .and. try1849.gt.0) goto 1849
C if(sav17.ne.C30_uliri .and. try1849.gt.0) goto 1849
C if(sav18.ne.L_ukiri .and. try1849.gt.0) goto 1849
C if(sav19.ne.L_ekiri .and. try1849.gt.0) goto 1849
      Call PUMP_HANDLER(deltat,C30_odupi,I_ekupi,L_oxubo,L_ibado
     &,L_imado,
     & I_ikupi,I_akupi,R_ibupi,REAL(R_ubupi,4),
     & R_uxopi,REAL(R_ebupi,4),I_okupi,L_apupi,L_emupi,L_urupi
     &,
     & L_asupi,L_ifupi,L_ilupi,L_ulupi,L_olupi,L_amupi,L_umupi
     &,L_oxopi,
     & L_obupi,L_ixopi,L_abupi,L_erupi,L_(148),
     & L_irupi,L_(147),L_exopi,L_axopi,L_ofupi,I_ukupi,R_ipupi
     &,R_opupi,
     & L_osupi,L_ekivo,L_orupi,REAL(R8_irufad,8),L_omupi,
     & REAL(R8_imupi,8),R_esupi,REAL(R_alupi,4),R_elupi,REAL
     &(R8_efupi,8),R_afupi,
     & R8_ixufad,R_isupi,R8_imupi,REAL(R_adupi,4),REAL(R_edupi
     &,4))
C KPH_vlv.fgi( 344, 156):���������� ���������� �������,20KPH31AP001
C label 1850  try1850=try1850-1
C sav1=R_isupi
C sav2=R_esupi
C sav3=L_apupi
C sav4=L_umupi
C sav5=L_emupi
C sav6=R8_imupi
C sav7=R_elupi
C sav8=I_ukupi
C sav9=I_okupi
C sav10=I_ikupi
C sav11=I_ekupi
C sav12=I_akupi
C sav13=I_ufupi
C sav14=L_ofupi
C sav15=L_ifupi
C sav16=R_afupi
C sav17=C30_odupi
C sav18=L_obupi
C sav19=L_abupi
      Call PUMP_HANDLER(deltat,C30_odupi,I_ekupi,L_oxubo,L_ibado
     &,L_imado,
     & I_ikupi,I_akupi,R_ibupi,REAL(R_ubupi,4),
     & R_uxopi,REAL(R_ebupi,4),I_okupi,L_apupi,L_emupi,L_urupi
     &,
     & L_asupi,L_ifupi,L_ilupi,L_ulupi,L_olupi,L_amupi,L_umupi
     &,L_oxopi,
     & L_obupi,L_ixopi,L_abupi,L_erupi,L_(148),
     & L_irupi,L_(147),L_exopi,L_axopi,L_ofupi,I_ukupi,R_ipupi
     &,R_opupi,
     & L_osupi,L_ekivo,L_orupi,REAL(R8_irufad,8),L_omupi,
     & REAL(R8_imupi,8),R_esupi,REAL(R_alupi,4),R_elupi,REAL
     &(R8_efupi,8),R_afupi,
     & R8_ixufad,R_isupi,R8_imupi,REAL(R_adupi,4),REAL(R_edupi
     &,4))
C KPH_vlv.fgi( 344, 156):recalc:���������� ���������� �������,20KPH31AP001
C if(sav1.ne.R_isupi .and. try1850.gt.0) goto 1850
C if(sav2.ne.R_esupi .and. try1850.gt.0) goto 1850
C if(sav3.ne.L_apupi .and. try1850.gt.0) goto 1850
C if(sav4.ne.L_umupi .and. try1850.gt.0) goto 1850
C if(sav5.ne.L_emupi .and. try1850.gt.0) goto 1850
C if(sav6.ne.R8_imupi .and. try1850.gt.0) goto 1850
C if(sav7.ne.R_elupi .and. try1850.gt.0) goto 1850
C if(sav8.ne.I_ukupi .and. try1850.gt.0) goto 1850
C if(sav9.ne.I_okupi .and. try1850.gt.0) goto 1850
C if(sav10.ne.I_ikupi .and. try1850.gt.0) goto 1850
C if(sav11.ne.I_ekupi .and. try1850.gt.0) goto 1850
C if(sav12.ne.I_akupi .and. try1850.gt.0) goto 1850
C if(sav13.ne.I_ufupi .and. try1850.gt.0) goto 1850
C if(sav14.ne.L_ofupi .and. try1850.gt.0) goto 1850
C if(sav15.ne.L_ifupi .and. try1850.gt.0) goto 1850
C if(sav16.ne.R_afupi .and. try1850.gt.0) goto 1850
C if(sav17.ne.C30_odupi .and. try1850.gt.0) goto 1850
C if(sav18.ne.L_obupi .and. try1850.gt.0) goto 1850
C if(sav19.ne.L_abupi .and. try1850.gt.0) goto 1850
      Call PUMP_HANDLER(deltat,C30_areri,I_oseri,L_oboto,L_idoto
     &,L_ipoto,
     & I_useri,I_iseri,R_umeri,REAL(R_eperi,4),
     & R_emeri,REAL(R_omeri,4),I_ateri,L_ixeri,L_overi,L_ediri
     &,
     & L_idiri,L_ureri,L_uteri,L_everi,L_averi,L_iveri,L_exeri
     &,L_ameri,
     & L_aperi,L_uleri,L_imeri,L_obiri,L_(154),
     & L_ubiri,L_(153),L_oleri,L_ileri,L_aseri,I_eteri,R_uxeri
     &,R_abiri,
     & L_afiri,L_ekivo,L_adiri,REAL(R8_irufad,8),L_axeri,
     & REAL(R8_uveri,8),R_odiri,REAL(R_iteri,4),R_oteri,REAL
     &(R8_oreri,8),R_ireri,
     & R8_ixufad,R_udiri,R8_uveri,REAL(R_iperi,4),REAL(R_operi
     &,4))
C KPH_vlv.fgi( 358, 180):���������� ���������� �������,20KPH15AP001
C label 1851  try1851=try1851-1
C sav1=R_udiri
C sav2=R_odiri
C sav3=L_ixeri
C sav4=L_exeri
C sav5=L_overi
C sav6=R8_uveri
C sav7=R_oteri
C sav8=I_eteri
C sav9=I_ateri
C sav10=I_useri
C sav11=I_oseri
C sav12=I_iseri
C sav13=I_eseri
C sav14=L_aseri
C sav15=L_ureri
C sav16=R_ireri
C sav17=C30_areri
C sav18=L_aperi
C sav19=L_imeri
      Call PUMP_HANDLER(deltat,C30_areri,I_oseri,L_oboto,L_idoto
     &,L_ipoto,
     & I_useri,I_iseri,R_umeri,REAL(R_eperi,4),
     & R_emeri,REAL(R_omeri,4),I_ateri,L_ixeri,L_overi,L_ediri
     &,
     & L_idiri,L_ureri,L_uteri,L_everi,L_averi,L_iveri,L_exeri
     &,L_ameri,
     & L_aperi,L_uleri,L_imeri,L_obiri,L_(154),
     & L_ubiri,L_(153),L_oleri,L_ileri,L_aseri,I_eteri,R_uxeri
     &,R_abiri,
     & L_afiri,L_ekivo,L_adiri,REAL(R8_irufad,8),L_axeri,
     & REAL(R8_uveri,8),R_odiri,REAL(R_iteri,4),R_oteri,REAL
     &(R8_oreri,8),R_ireri,
     & R8_ixufad,R_udiri,R8_uveri,REAL(R_iperi,4),REAL(R_operi
     &,4))
C KPH_vlv.fgi( 358, 180):recalc:���������� ���������� �������,20KPH15AP001
C if(sav1.ne.R_udiri .and. try1851.gt.0) goto 1851
C if(sav2.ne.R_odiri .and. try1851.gt.0) goto 1851
C if(sav3.ne.L_ixeri .and. try1851.gt.0) goto 1851
C if(sav4.ne.L_exeri .and. try1851.gt.0) goto 1851
C if(sav5.ne.L_overi .and. try1851.gt.0) goto 1851
C if(sav6.ne.R8_uveri .and. try1851.gt.0) goto 1851
C if(sav7.ne.R_oteri .and. try1851.gt.0) goto 1851
C if(sav8.ne.I_eteri .and. try1851.gt.0) goto 1851
C if(sav9.ne.I_ateri .and. try1851.gt.0) goto 1851
C if(sav10.ne.I_useri .and. try1851.gt.0) goto 1851
C if(sav11.ne.I_oseri .and. try1851.gt.0) goto 1851
C if(sav12.ne.I_iseri .and. try1851.gt.0) goto 1851
C if(sav13.ne.I_eseri .and. try1851.gt.0) goto 1851
C if(sav14.ne.L_aseri .and. try1851.gt.0) goto 1851
C if(sav15.ne.L_ureri .and. try1851.gt.0) goto 1851
C if(sav16.ne.R_ireri .and. try1851.gt.0) goto 1851
C if(sav17.ne.C30_areri .and. try1851.gt.0) goto 1851
C if(sav18.ne.L_aperi .and. try1851.gt.0) goto 1851
C if(sav19.ne.L_imeri .and. try1851.gt.0) goto 1851
      Call PUMP_HANDLER(deltat,C30_ukopi,I_imopi,L_oxubo,L_ibado
     &,L_imado,
     & I_omopi,I_emopi,R_ofopi,REAL(R_akopi,4),
     & R_afopi,REAL(R_ifopi,4),I_umopi,L_esopi,L_iropi,L_avopi
     &,
     & L_evopi,L_olopi,L_opopi,L_aropi,L_upopi,L_eropi,L_asopi
     &,L_udopi,
     & L_ufopi,L_odopi,L_efopi,L_itopi,L_(146),
     & L_otopi,L_(145),L_idopi,L_edopi,L_ulopi,I_apopi,R_osopi
     &,R_usopi,
     & L_uvopi,L_ekivo,L_utopi,REAL(R8_irufad,8),L_uropi,
     & REAL(R8_oropi,8),R_ivopi,REAL(R_epopi,4),R_ipopi,REAL
     &(R8_ilopi,8),R_elopi,
     & R8_ixufad,R_ovopi,R8_oropi,REAL(R_ekopi,4),REAL(R_ikopi
     &,4))
C KPH_vlv.fgi( 358, 156):���������� ���������� �������,20KPH31AP002
C label 1852  try1852=try1852-1
C sav1=R_ovopi
C sav2=R_ivopi
C sav3=L_esopi
C sav4=L_asopi
C sav5=L_iropi
C sav6=R8_oropi
C sav7=R_ipopi
C sav8=I_apopi
C sav9=I_umopi
C sav10=I_omopi
C sav11=I_imopi
C sav12=I_emopi
C sav13=I_amopi
C sav14=L_ulopi
C sav15=L_olopi
C sav16=R_elopi
C sav17=C30_ukopi
C sav18=L_ufopi
C sav19=L_efopi
      Call PUMP_HANDLER(deltat,C30_ukopi,I_imopi,L_oxubo,L_ibado
     &,L_imado,
     & I_omopi,I_emopi,R_ofopi,REAL(R_akopi,4),
     & R_afopi,REAL(R_ifopi,4),I_umopi,L_esopi,L_iropi,L_avopi
     &,
     & L_evopi,L_olopi,L_opopi,L_aropi,L_upopi,L_eropi,L_asopi
     &,L_udopi,
     & L_ufopi,L_odopi,L_efopi,L_itopi,L_(146),
     & L_otopi,L_(145),L_idopi,L_edopi,L_ulopi,I_apopi,R_osopi
     &,R_usopi,
     & L_uvopi,L_ekivo,L_utopi,REAL(R8_irufad,8),L_uropi,
     & REAL(R8_oropi,8),R_ivopi,REAL(R_epopi,4),R_ipopi,REAL
     &(R8_ilopi,8),R_elopi,
     & R8_ixufad,R_ovopi,R8_oropi,REAL(R_ekopi,4),REAL(R_ikopi
     &,4))
C KPH_vlv.fgi( 358, 156):recalc:���������� ���������� �������,20KPH31AP002
C if(sav1.ne.R_ovopi .and. try1852.gt.0) goto 1852
C if(sav2.ne.R_ivopi .and. try1852.gt.0) goto 1852
C if(sav3.ne.L_esopi .and. try1852.gt.0) goto 1852
C if(sav4.ne.L_asopi .and. try1852.gt.0) goto 1852
C if(sav5.ne.L_iropi .and. try1852.gt.0) goto 1852
C if(sav6.ne.R8_oropi .and. try1852.gt.0) goto 1852
C if(sav7.ne.R_ipopi .and. try1852.gt.0) goto 1852
C if(sav8.ne.I_apopi .and. try1852.gt.0) goto 1852
C if(sav9.ne.I_umopi .and. try1852.gt.0) goto 1852
C if(sav10.ne.I_omopi .and. try1852.gt.0) goto 1852
C if(sav11.ne.I_imopi .and. try1852.gt.0) goto 1852
C if(sav12.ne.I_emopi .and. try1852.gt.0) goto 1852
C if(sav13.ne.I_amopi .and. try1852.gt.0) goto 1852
C if(sav14.ne.L_ulopi .and. try1852.gt.0) goto 1852
C if(sav15.ne.L_olopi .and. try1852.gt.0) goto 1852
C if(sav16.ne.R_elopi .and. try1852.gt.0) goto 1852
C if(sav17.ne.C30_ukopi .and. try1852.gt.0) goto 1852
C if(sav18.ne.L_ufopi .and. try1852.gt.0) goto 1852
C if(sav19.ne.L_efopi .and. try1852.gt.0) goto 1852
      Call PUMP_HANDLER(deltat,C30_ekavo,I_ulavo,L_alafad
     &,L_ulafad,L_utafad,
     & I_amavo,I_olavo,R_afavo,REAL(R_ifavo,4),
     & R_idavo,REAL(R_udavo,4),I_emavo,L_oravo,L_upavo,L_itavo
     &,
     & L_otavo,L_alavo,L_apavo,L_ipavo,L_epavo,L_opavo,L_iravo
     &,L_edavo,
     & L_efavo,L_adavo,L_odavo,L_usavo,L_(160),
     & L_atavo,L_(159),L_ubavo,L_obavo,L_elavo,I_imavo,R_asavo
     &,R_esavo,
     & L_evavo,L_ekivo,L_etavo,REAL(R8_irufad,8),L_eravo,
     & REAL(R8_aravo,8),R_utavo,REAL(R_omavo,4),R_umavo,REAL
     &(R8_ukavo,8),R_okavo,
     & R8_ixufad,R_avavo,R8_aravo,REAL(R_ofavo,4),REAL(R_ufavo
     &,4))
C KPG_vlv.fgi( 235, 281):���������� ���������� �������,20KPG12AP001
C label 1853  try1853=try1853-1
C sav1=R_avavo
C sav2=R_utavo
C sav3=L_oravo
C sav4=L_iravo
C sav5=L_upavo
C sav6=R8_aravo
C sav7=R_umavo
C sav8=I_imavo
C sav9=I_emavo
C sav10=I_amavo
C sav11=I_ulavo
C sav12=I_olavo
C sav13=I_ilavo
C sav14=L_elavo
C sav15=L_alavo
C sav16=R_okavo
C sav17=C30_ekavo
C sav18=L_efavo
C sav19=L_odavo
      Call PUMP_HANDLER(deltat,C30_ekavo,I_ulavo,L_alafad
     &,L_ulafad,L_utafad,
     & I_amavo,I_olavo,R_afavo,REAL(R_ifavo,4),
     & R_idavo,REAL(R_udavo,4),I_emavo,L_oravo,L_upavo,L_itavo
     &,
     & L_otavo,L_alavo,L_apavo,L_ipavo,L_epavo,L_opavo,L_iravo
     &,L_edavo,
     & L_efavo,L_adavo,L_odavo,L_usavo,L_(160),
     & L_atavo,L_(159),L_ubavo,L_obavo,L_elavo,I_imavo,R_asavo
     &,R_esavo,
     & L_evavo,L_ekivo,L_etavo,REAL(R8_irufad,8),L_eravo,
     & REAL(R8_aravo,8),R_utavo,REAL(R_omavo,4),R_umavo,REAL
     &(R8_ukavo,8),R_okavo,
     & R8_ixufad,R_avavo,R8_aravo,REAL(R_ofavo,4),REAL(R_ufavo
     &,4))
C KPG_vlv.fgi( 235, 281):recalc:���������� ���������� �������,20KPG12AP001
C if(sav1.ne.R_avavo .and. try1853.gt.0) goto 1853
C if(sav2.ne.R_utavo .and. try1853.gt.0) goto 1853
C if(sav3.ne.L_oravo .and. try1853.gt.0) goto 1853
C if(sav4.ne.L_iravo .and. try1853.gt.0) goto 1853
C if(sav5.ne.L_upavo .and. try1853.gt.0) goto 1853
C if(sav6.ne.R8_aravo .and. try1853.gt.0) goto 1853
C if(sav7.ne.R_umavo .and. try1853.gt.0) goto 1853
C if(sav8.ne.I_imavo .and. try1853.gt.0) goto 1853
C if(sav9.ne.I_emavo .and. try1853.gt.0) goto 1853
C if(sav10.ne.I_amavo .and. try1853.gt.0) goto 1853
C if(sav11.ne.I_ulavo .and. try1853.gt.0) goto 1853
C if(sav12.ne.I_olavo .and. try1853.gt.0) goto 1853
C if(sav13.ne.I_ilavo .and. try1853.gt.0) goto 1853
C if(sav14.ne.L_elavo .and. try1853.gt.0) goto 1853
C if(sav15.ne.L_alavo .and. try1853.gt.0) goto 1853
C if(sav16.ne.R_okavo .and. try1853.gt.0) goto 1853
C if(sav17.ne.C30_ekavo .and. try1853.gt.0) goto 1853
C if(sav18.ne.L_efavo .and. try1853.gt.0) goto 1853
C if(sav19.ne.L_odavo .and. try1853.gt.0) goto 1853
      Call PUMP_HANDLER(deltat,C30_etari,I_uvari,L_ikodo,L_elodo
     &,L_etodo,
     & I_axari,I_ovari,R_asari,REAL(R_isari,4),
     & R_irari,REAL(R_urari,4),I_exari,L_oderi,L_uberi,L_ikeri
     &,
     & L_okeri,L_avari,L_aberi,L_iberi,L_eberi,L_oberi,L_ideri
     &,L_erari,
     & L_esari,L_arari,L_orari,L_uferi,L_(152),
     & L_akeri,L_(151),L_upari,L_opari,L_evari,I_ixari,R_aferi
     &,R_eferi,
     & L_eleri,L_ekivo,L_ekeri,REAL(R8_irufad,8),L_ederi,
     & REAL(R8_aderi,8),R_ukeri,REAL(R_oxari,4),R_uxari,REAL
     &(R8_utari,8),R_otari,
     & R8_ixufad,R_aleri,R8_aderi,REAL(R_osari,4),REAL(R_usari
     &,4))
C KPH_vlv.fgi( 372, 180):���������� ���������� �������,20KPH41AP001
C label 1854  try1854=try1854-1
C sav1=R_aleri
C sav2=R_ukeri
C sav3=L_oderi
C sav4=L_ideri
C sav5=L_uberi
C sav6=R8_aderi
C sav7=R_uxari
C sav8=I_ixari
C sav9=I_exari
C sav10=I_axari
C sav11=I_uvari
C sav12=I_ovari
C sav13=I_ivari
C sav14=L_evari
C sav15=L_avari
C sav16=R_otari
C sav17=C30_etari
C sav18=L_esari
C sav19=L_orari
      Call PUMP_HANDLER(deltat,C30_etari,I_uvari,L_ikodo,L_elodo
     &,L_etodo,
     & I_axari,I_ovari,R_asari,REAL(R_isari,4),
     & R_irari,REAL(R_urari,4),I_exari,L_oderi,L_uberi,L_ikeri
     &,
     & L_okeri,L_avari,L_aberi,L_iberi,L_eberi,L_oberi,L_ideri
     &,L_erari,
     & L_esari,L_arari,L_orari,L_uferi,L_(152),
     & L_akeri,L_(151),L_upari,L_opari,L_evari,I_ixari,R_aferi
     &,R_eferi,
     & L_eleri,L_ekivo,L_ekeri,REAL(R8_irufad,8),L_ederi,
     & REAL(R8_aderi,8),R_ukeri,REAL(R_oxari,4),R_uxari,REAL
     &(R8_utari,8),R_otari,
     & R8_ixufad,R_aleri,R8_aderi,REAL(R_osari,4),REAL(R_usari
     &,4))
C KPH_vlv.fgi( 372, 180):recalc:���������� ���������� �������,20KPH41AP001
C if(sav1.ne.R_aleri .and. try1854.gt.0) goto 1854
C if(sav2.ne.R_ukeri .and. try1854.gt.0) goto 1854
C if(sav3.ne.L_oderi .and. try1854.gt.0) goto 1854
C if(sav4.ne.L_ideri .and. try1854.gt.0) goto 1854
C if(sav5.ne.L_uberi .and. try1854.gt.0) goto 1854
C if(sav6.ne.R8_aderi .and. try1854.gt.0) goto 1854
C if(sav7.ne.R_uxari .and. try1854.gt.0) goto 1854
C if(sav8.ne.I_ixari .and. try1854.gt.0) goto 1854
C if(sav9.ne.I_exari .and. try1854.gt.0) goto 1854
C if(sav10.ne.I_axari .and. try1854.gt.0) goto 1854
C if(sav11.ne.I_uvari .and. try1854.gt.0) goto 1854
C if(sav12.ne.I_ovari .and. try1854.gt.0) goto 1854
C if(sav13.ne.I_ivari .and. try1854.gt.0) goto 1854
C if(sav14.ne.L_evari .and. try1854.gt.0) goto 1854
C if(sav15.ne.L_avari .and. try1854.gt.0) goto 1854
C if(sav16.ne.R_otari .and. try1854.gt.0) goto 1854
C if(sav17.ne.C30_etari .and. try1854.gt.0) goto 1854
C if(sav18.ne.L_esari .and. try1854.gt.0) goto 1854
C if(sav19.ne.L_orari .and. try1854.gt.0) goto 1854
      Call PUMP_HANDLER(deltat,C30_apipi,I_oripi,L_isero,L_etero
     &,L_efiro,
     & I_uripi,I_iripi,R_ulipi,REAL(R_emipi,4),
     & R_elipi,REAL(R_olipi,4),I_asipi,L_ivipi,L_otipi,L_ebopi
     &,
     & L_ibopi,L_upipi,L_usipi,L_etipi,L_atipi,L_itipi,L_evipi
     &,L_alipi,
     & L_amipi,L_ukipi,L_ilipi,L_oxipi,L_(144),
     & L_uxipi,L_(143),L_okipi,L_ikipi,L_aripi,I_esipi,R_uvipi
     &,R_axipi,
     & L_adopi,L_ekivo,L_abopi,REAL(R8_irufad,8),L_avipi,
     & REAL(R8_utipi,8),R_obopi,REAL(R_isipi,4),R_osipi,REAL
     &(R8_opipi,8),R_ipipi,
     & R8_ixufad,R_ubopi,R8_utipi,REAL(R_imipi,4),REAL(R_omipi
     &,4))
C KPH_vlv.fgi( 372, 156):���������� ���������� �������,20KPH51BB001
C label 1855  try1855=try1855-1
C sav1=R_ubopi
C sav2=R_obopi
C sav3=L_ivipi
C sav4=L_evipi
C sav5=L_otipi
C sav6=R8_utipi
C sav7=R_osipi
C sav8=I_esipi
C sav9=I_asipi
C sav10=I_uripi
C sav11=I_oripi
C sav12=I_iripi
C sav13=I_eripi
C sav14=L_aripi
C sav15=L_upipi
C sav16=R_ipipi
C sav17=C30_apipi
C sav18=L_amipi
C sav19=L_ilipi
      Call PUMP_HANDLER(deltat,C30_apipi,I_oripi,L_isero,L_etero
     &,L_efiro,
     & I_uripi,I_iripi,R_ulipi,REAL(R_emipi,4),
     & R_elipi,REAL(R_olipi,4),I_asipi,L_ivipi,L_otipi,L_ebopi
     &,
     & L_ibopi,L_upipi,L_usipi,L_etipi,L_atipi,L_itipi,L_evipi
     &,L_alipi,
     & L_amipi,L_ukipi,L_ilipi,L_oxipi,L_(144),
     & L_uxipi,L_(143),L_okipi,L_ikipi,L_aripi,I_esipi,R_uvipi
     &,R_axipi,
     & L_adopi,L_ekivo,L_abopi,REAL(R8_irufad,8),L_avipi,
     & REAL(R8_utipi,8),R_obopi,REAL(R_isipi,4),R_osipi,REAL
     &(R8_opipi,8),R_ipipi,
     & R8_ixufad,R_ubopi,R8_utipi,REAL(R_imipi,4),REAL(R_omipi
     &,4))
C KPH_vlv.fgi( 372, 156):recalc:���������� ���������� �������,20KPH51BB001
C if(sav1.ne.R_ubopi .and. try1855.gt.0) goto 1855
C if(sav2.ne.R_obopi .and. try1855.gt.0) goto 1855
C if(sav3.ne.L_ivipi .and. try1855.gt.0) goto 1855
C if(sav4.ne.L_evipi .and. try1855.gt.0) goto 1855
C if(sav5.ne.L_otipi .and. try1855.gt.0) goto 1855
C if(sav6.ne.R8_utipi .and. try1855.gt.0) goto 1855
C if(sav7.ne.R_osipi .and. try1855.gt.0) goto 1855
C if(sav8.ne.I_esipi .and. try1855.gt.0) goto 1855
C if(sav9.ne.I_asipi .and. try1855.gt.0) goto 1855
C if(sav10.ne.I_uripi .and. try1855.gt.0) goto 1855
C if(sav11.ne.I_oripi .and. try1855.gt.0) goto 1855
C if(sav12.ne.I_iripi .and. try1855.gt.0) goto 1855
C if(sav13.ne.I_eripi .and. try1855.gt.0) goto 1855
C if(sav14.ne.L_aripi .and. try1855.gt.0) goto 1855
C if(sav15.ne.L_upipi .and. try1855.gt.0) goto 1855
C if(sav16.ne.R_ipipi .and. try1855.gt.0) goto 1855
C if(sav17.ne.C30_apipi .and. try1855.gt.0) goto 1855
C if(sav18.ne.L_amipi .and. try1855.gt.0) goto 1855
C if(sav19.ne.L_ilipi .and. try1855.gt.0) goto 1855
      Call PUMP_HANDLER(deltat,C30_ixupi,I_adari,L_ikodo,L_elodo
     &,L_etodo,
     & I_edari,I_ubari,R_evupi,REAL(R_ovupi,4),
     & R_otupi,REAL(R_avupi,4),I_idari,L_ukari,L_akari,L_omari
     &,
     & L_umari,L_ebari,L_efari,L_ofari,L_ifari,L_ufari,L_okari
     &,L_itupi,
     & L_ivupi,L_etupi,L_utupi,L_amari,L_(150),
     & L_emari,L_(149),L_atupi,L_usupi,L_ibari,I_odari,R_elari
     &,R_ilari,
     & L_ipari,L_ekivo,L_imari,REAL(R8_irufad,8),L_ikari,
     & REAL(R8_ekari,8),R_apari,REAL(R_udari,4),R_afari,REAL
     &(R8_abari,8),R_uxupi,
     & R8_ixufad,R_epari,R8_ekari,REAL(R_uvupi,4),REAL(R_axupi
     &,4))
C KPH_vlv.fgi( 386, 180):���������� ���������� �������,20KPH41AP002
C label 1856  try1856=try1856-1
C sav1=R_epari
C sav2=R_apari
C sav3=L_ukari
C sav4=L_okari
C sav5=L_akari
C sav6=R8_ekari
C sav7=R_afari
C sav8=I_odari
C sav9=I_idari
C sav10=I_edari
C sav11=I_adari
C sav12=I_ubari
C sav13=I_obari
C sav14=L_ibari
C sav15=L_ebari
C sav16=R_uxupi
C sav17=C30_ixupi
C sav18=L_ivupi
C sav19=L_utupi
      Call PUMP_HANDLER(deltat,C30_ixupi,I_adari,L_ikodo,L_elodo
     &,L_etodo,
     & I_edari,I_ubari,R_evupi,REAL(R_ovupi,4),
     & R_otupi,REAL(R_avupi,4),I_idari,L_ukari,L_akari,L_omari
     &,
     & L_umari,L_ebari,L_efari,L_ofari,L_ifari,L_ufari,L_okari
     &,L_itupi,
     & L_ivupi,L_etupi,L_utupi,L_amari,L_(150),
     & L_emari,L_(149),L_atupi,L_usupi,L_ibari,I_odari,R_elari
     &,R_ilari,
     & L_ipari,L_ekivo,L_imari,REAL(R8_irufad,8),L_ikari,
     & REAL(R8_ekari,8),R_apari,REAL(R_udari,4),R_afari,REAL
     &(R8_abari,8),R_uxupi,
     & R8_ixufad,R_epari,R8_ekari,REAL(R_uvupi,4),REAL(R_axupi
     &,4))
C KPH_vlv.fgi( 386, 180):recalc:���������� ���������� �������,20KPH41AP002
C if(sav1.ne.R_epari .and. try1856.gt.0) goto 1856
C if(sav2.ne.R_apari .and. try1856.gt.0) goto 1856
C if(sav3.ne.L_ukari .and. try1856.gt.0) goto 1856
C if(sav4.ne.L_okari .and. try1856.gt.0) goto 1856
C if(sav5.ne.L_akari .and. try1856.gt.0) goto 1856
C if(sav6.ne.R8_ekari .and. try1856.gt.0) goto 1856
C if(sav7.ne.R_afari .and. try1856.gt.0) goto 1856
C if(sav8.ne.I_odari .and. try1856.gt.0) goto 1856
C if(sav9.ne.I_idari .and. try1856.gt.0) goto 1856
C if(sav10.ne.I_edari .and. try1856.gt.0) goto 1856
C if(sav11.ne.I_adari .and. try1856.gt.0) goto 1856
C if(sav12.ne.I_ubari .and. try1856.gt.0) goto 1856
C if(sav13.ne.I_obari .and. try1856.gt.0) goto 1856
C if(sav14.ne.L_ibari .and. try1856.gt.0) goto 1856
C if(sav15.ne.L_ebari .and. try1856.gt.0) goto 1856
C if(sav16.ne.R_uxupi .and. try1856.gt.0) goto 1856
C if(sav17.ne.C30_ixupi .and. try1856.gt.0) goto 1856
C if(sav18.ne.L_ivupi .and. try1856.gt.0) goto 1856
C if(sav19.ne.L_utupi .and. try1856.gt.0) goto 1856
      Call PUMP_HANDLER(deltat,C30_imuto,I_aruto,L_alafad
     &,L_ulafad,L_utafad,
     & I_eruto,I_uputo,R_eluto,REAL(R_oluto,4),
     & R_okuto,REAL(R_aluto,4),I_iruto,L_ututo,L_atuto,L_oxuto
     &,
     & L_uxuto,L_eputo,L_esuto,L_osuto,L_isuto,L_usuto,L_otuto
     &,L_ikuto,
     & L_iluto,L_ekuto,L_ukuto,L_axuto,L_(158),
     & L_exuto,L_(157),L_akuto,L_ufuto,L_iputo,I_oruto,R_evuto
     &,R_ivuto,
     & L_ibavo,L_ekivo,L_ixuto,REAL(R8_irufad,8),L_ituto,
     & REAL(R8_etuto,8),R_abavo,REAL(R_uruto,4),R_asuto,REAL
     &(R8_aputo,8),R_umuto,
     & R8_ixufad,R_ebavo,R8_etuto,REAL(R_uluto,4),REAL(R_amuto
     &,4))
C KPG_vlv.fgi( 248, 281):���������� ���������� �������,20KPG12AP002
C label 1857  try1857=try1857-1
C sav1=R_ebavo
C sav2=R_abavo
C sav3=L_ututo
C sav4=L_otuto
C sav5=L_atuto
C sav6=R8_etuto
C sav7=R_asuto
C sav8=I_oruto
C sav9=I_iruto
C sav10=I_eruto
C sav11=I_aruto
C sav12=I_uputo
C sav13=I_oputo
C sav14=L_iputo
C sav15=L_eputo
C sav16=R_umuto
C sav17=C30_imuto
C sav18=L_iluto
C sav19=L_ukuto
      Call PUMP_HANDLER(deltat,C30_imuto,I_aruto,L_alafad
     &,L_ulafad,L_utafad,
     & I_eruto,I_uputo,R_eluto,REAL(R_oluto,4),
     & R_okuto,REAL(R_aluto,4),I_iruto,L_ututo,L_atuto,L_oxuto
     &,
     & L_uxuto,L_eputo,L_esuto,L_osuto,L_isuto,L_usuto,L_otuto
     &,L_ikuto,
     & L_iluto,L_ekuto,L_ukuto,L_axuto,L_(158),
     & L_exuto,L_(157),L_akuto,L_ufuto,L_iputo,I_oruto,R_evuto
     &,R_ivuto,
     & L_ibavo,L_ekivo,L_ixuto,REAL(R8_irufad,8),L_ituto,
     & REAL(R8_etuto,8),R_abavo,REAL(R_uruto,4),R_asuto,REAL
     &(R8_aputo,8),R_umuto,
     & R8_ixufad,R_ebavo,R8_etuto,REAL(R_uluto,4),REAL(R_amuto
     &,4))
C KPG_vlv.fgi( 248, 281):recalc:���������� ���������� �������,20KPG12AP002
C if(sav1.ne.R_ebavo .and. try1857.gt.0) goto 1857
C if(sav2.ne.R_abavo .and. try1857.gt.0) goto 1857
C if(sav3.ne.L_ututo .and. try1857.gt.0) goto 1857
C if(sav4.ne.L_otuto .and. try1857.gt.0) goto 1857
C if(sav5.ne.L_atuto .and. try1857.gt.0) goto 1857
C if(sav6.ne.R8_etuto .and. try1857.gt.0) goto 1857
C if(sav7.ne.R_asuto .and. try1857.gt.0) goto 1857
C if(sav8.ne.I_oruto .and. try1857.gt.0) goto 1857
C if(sav9.ne.I_iruto .and. try1857.gt.0) goto 1857
C if(sav10.ne.I_eruto .and. try1857.gt.0) goto 1857
C if(sav11.ne.I_aruto .and. try1857.gt.0) goto 1857
C if(sav12.ne.I_uputo .and. try1857.gt.0) goto 1857
C if(sav13.ne.I_oputo .and. try1857.gt.0) goto 1857
C if(sav14.ne.L_iputo .and. try1857.gt.0) goto 1857
C if(sav15.ne.L_eputo .and. try1857.gt.0) goto 1857
C if(sav16.ne.R_umuto .and. try1857.gt.0) goto 1857
C if(sav17.ne.C30_imuto .and. try1857.gt.0) goto 1857
C if(sav18.ne.L_iluto .and. try1857.gt.0) goto 1857
C if(sav19.ne.L_ukuto .and. try1857.gt.0) goto 1857
      End
