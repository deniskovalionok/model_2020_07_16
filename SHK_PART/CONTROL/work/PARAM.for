      Subroutine PARAM(ext_deltat)
      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      Include 'PARAM.fh'

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      I_(2) = 0
C ASU_SIO.fgi( 106, 239):��������� ������������� IN (�������)
      I_(1) = 1
C ASU_SIO.fgi( 106, 237):��������� ������������� IN (�������)
      I_(3) = 2
C ASU_SIO.fgi( 106, 227):��������� ������������� IN (�������)
      I_(6) = 0
C ASU_SIO.fgi( 107, 288):��������� ������������� IN (�������)
      I_(5) = 1
C ASU_SIO.fgi( 107, 286):��������� ������������� IN (�������)
      I_(8) = 0
C ASU_SIO.fgi( 106, 221):��������� ������������� IN (�������)
      I_(7) = 1
C ASU_SIO.fgi( 106, 219):��������� ������������� IN (�������)
      R_(3) = 0.0
C ASU_SIO.fgi(  53, 215):��������� (RE4) (�������)
      R_(2) = -1.0
C ASU_SIO.fgi(  53, 213):��������� (RE4) (�������)
      I_(9) = 1
C ASU_SIO.fgi(  29, 208):��������� ������������� IN (�������)
      L_(12)=I_(9).eq.I_if
C ASU_SIO.fgi(  36, 207):���������� �������������
      R_(6) = 0.0
C ASU_SIO.fgi(  53, 228):��������� (RE4) (�������)
      R_(5) = 1.0
C ASU_SIO.fgi(  53, 226):��������� (RE4) (�������)
      I_(10) = 1
C ASU_SIO.fgi(  29, 221):��������� ������������� IN (�������)
      L_(13)=I_(10).eq.I_ef
C ASU_SIO.fgi(  36, 220):���������� �������������
      I_(12) = 0
C ASU_SIO.fgi( 107, 270):��������� ������������� IN (�������)
      I_(11) = 1
C ASU_SIO.fgi( 107, 268):��������� ������������� IN (�������)
      I_(13) = 2
C ASU_SIO.fgi( 107, 276):��������� ������������� IN (�������)
      R_(10) = 0.0
C ASU_SIO.fgi(  54, 264):��������� (RE4) (�������)
      R_(9) = -1.0
C ASU_SIO.fgi(  54, 262):��������� (RE4) (�������)
      I_(15) = 1
C ASU_SIO.fgi(  30, 257):��������� ������������� IN (�������)
      L_(19)=I_(15).eq.I_ul
C ASU_SIO.fgi(  37, 256):���������� �������������
      R_(13) = 0.0
C ASU_SIO.fgi(  54, 277):��������� (RE4) (�������)
      R_(12) = 1.0
C ASU_SIO.fgi(  54, 275):��������� (RE4) (�������)
      I_(16) = 1
C ASU_SIO.fgi(  30, 270):��������� ������������� IN (�������)
      L_(20)=I_(16).eq.I_ol
C ASU_SIO.fgi(  37, 269):���������� �������������
      R_ap = R8_um
C sens_init_2.fgi( 250, 292):��������
      R_ip = R8_ep
C sens_init_2.fgi( 250, 295):��������
      R_up = R8_op
C sens_init_2.fgi( 250, 280):��������
      R_er = R8_ar
C sens_init_2.fgi( 250, 283):��������
      R_or = R8_ir
C sens_init_2.fgi( 250, 286):��������
      R_as = R8_ur
C sens_init_2.fgi( 250, 289):��������
      R_es = R8_is
C sens_init_1.fgi( 380, 152):��������
      R_os = R8_us
C sens_init_1.fgi( 380, 156):��������
      R_at = R8_et
C sens_init_1.fgi( 380, 160):��������
      R_it = R8_ot
C sens_init_1.fgi( 380, 164):��������
      R_ut = R8_av
C sens_init_1.fgi( 380, 168):��������
      R_ev = R8_iv
C sens_init_1.fgi( 380, 172):��������
      R_ov=R_uv
C sens_init_1.fgi( 312, 260):������,20KPA02CG003XQ01_input
      R_ax = R8_ex
C sens_init_1.fgi( 299, 191):��������
      R_ix = R8_ox
C sens_init_1.fgi( 299, 196):��������
      R_ux = R8_abe
C sens_init_1.fgi( 299, 200):��������
      R_ebe = R8_ibe
C sens_init_1.fgi( 299, 205):��������
      R_idi = R8_odi
C sens_init_1.fgi( 380, 188):��������
      R_ofi = R8_ufi
C sens_init_1.fgi( 380, 200):��������
      R_iki = R8_oki
C sens_init_1.fgi( 380, 210):��������
      R_udi = R8_afi
C sens_init_1.fgi( 380, 192):��������
      R_aki = R8_eki
C sens_init_1.fgi( 380, 205):��������
      R_esi = R8_isi
C sens_init_1.fgi(  46,  62):��������
      R_obe = R8_ube
C sens_init_1.fgi( 299, 211):��������
      R_ade = R8_ede
C sens_init_1.fgi( 299, 216):��������
      R_ide = R8_ode
C sens_init_1.fgi( 299, 220):��������
      R_(15) = 1
C sens_init_1.fgi( 134,   6):��������� (RE4) (�������)
      R_ude = R8_afe * R_(15)
C sens_init_1.fgi( 137,   8):����������
      R_(16) = 1
C sens_init_1.fgi( 134,  12):��������� (RE4) (�������)
      R_efe = R8_ife * R_(16)
C sens_init_1.fgi( 137,  14):����������
      R_(17) = 1
C sens_init_1.fgi(  46,  86):��������� (RE4) (�������)
      R_ofe = R8_ufe * R_(17)
C sens_init_1.fgi(  49,  88):����������
      R_ake = R8_eke
C sens_init_1.fgi(  46,  92):��������
      R_(31)=abs(R8_uvi)
C sens_init_1.fgi(  46, 105):���������� ��������
      R_ovi = R_(31)
C sens_init_1.fgi(  53, 105):��������
      R_(32)=abs(R8_ibo)
C sens_init_1.fgi(  46, 123):���������� ��������
      R_ebo = R_(32)
C sens_init_1.fgi(  53, 123):��������
      R_(18) = 1
C sens_init_1.fgi( 134,  19):��������� (RE4) (�������)
      R_ike = R8_oke * R_(18)
C sens_init_1.fgi( 137,  21):����������
      R_(19) = 1
C sens_init_1.fgi( 134,  31):��������� (RE4) (�������)
      R_uke = R8_ale * R_(19)
C sens_init_1.fgi( 137,  33):����������
      R_(20) = 1
C sens_init_1.fgi( 134,  37):��������� (RE4) (�������)
      R_ele = R8_ile * R_(20)
C sens_init_1.fgi( 137,  39):����������
      R_(21) = 1
C sens_init_1.fgi( 134,  25):��������� (RE4) (�������)
      R_ole = R8_ule * R_(21)
C sens_init_1.fgi( 137,  27):����������
      R_ari = R8_eri
C sens_init_1.fgi( 299, 168):��������
      R_(22) = 0.001
C sens_init_1.fgi( 377, 178):��������� (RE4) (�������)
      R_obi = R8_ubi * R_(22)
C sens_init_1.fgi( 380, 180):����������
      R_(23) = 0.001
C sens_init_1.fgi( 296, 128):��������� (RE4) (�������)
      R_oli = R8_uli * R_(23)
C sens_init_1.fgi( 299, 130):����������
      R_(24) = 0.001
C sens_init_1.fgi( 296, 133):��������� (RE4) (�������)
      R_ami = R8_emi * R_(24)
C sens_init_1.fgi( 299, 135):����������
      R_(25) = 1
C sens_init_1.fgi(  46,  95):��������� (RE4) (�������)
      R_uti = R8_avi * R_(25)
C sens_init_1.fgi(  49,  97):����������
      R_(26) = 0.000001
C sens_init_1.fgi(  46, 108):��������� (RE4) (�������)
      R_axi = R8_exi * R_(26)
C sens_init_1.fgi(  49, 111):����������
      R_ame = R8_eme
C sens_init_1.fgi( 299, 249):��������
      R_ime = R8_ome
C sens_init_1.fgi( 299, 244):��������
      R_ume = R8_ape
C sens_init_1.fgi( 299, 239):��������
      R_epe = R8_ipe
C sens_init_1.fgi( 299, 233):��������
      R_ope = R8_upe
C sens_init_1.fgi( 299, 229):��������
      R_are = R8_ere
C sens_init_1.fgi( 299, 225):��������
      R_ire=R_ore
C sens_init_1.fgi( 312, 263):������,20FDB25CG001XQ01_input
      R_ure=R_ase
C sens_init_1.fgi( 312, 266):������,20FDB24CG001XQ01_input
      R_ese=R_ise
C sens_init_1.fgi( 312, 269):������,20FDB23CG001XQ01_input
      R_ose=R_use
C sens_init_1.fgi( 312, 272):������,20FDB22CG001XQ01_input
      R_ate=R_ete
C sens_init_1.fgi( 312, 275):������,20FDB21CG001XQ01_input
      R_ite=R_ote
C sens_init_1.fgi( 312, 278):������,20FDB41CG001XQ01_input
      R_ute=R_ave
C sens_init_1.fgi( 312, 281):������,20FDA41CG001XQ01_input
      R_eve=R_ive
C sens_init_1.fgi( 312, 290):������,20KPA01CG001XQ01_input
      R_ove=R_uve
C sens_init_1.fgi( 312, 284):������,20KPA01CG003XQ01_input
      R_axe=R_exe
C sens_init_1.fgi( 312, 287):������,20KPA01CG002XQ01_input
      R_ixe=R_oxe
C sens_init_1.fgi( 235, 248):������,20FDA51CG001XQ01_input
      R_uxe = R8_abi
C sens_init_1.fgi( 211, 286):��������
      R_ebi = R8_ibi
C sens_init_1.fgi( 211, 290):��������
      R_adi = R8_edi
C sens_init_1.fgi( 380, 184):��������
      R_efi = R8_ifi
C sens_init_1.fgi( 380, 196):��������
      R_(27) = 13553
C sens_init_1.fgi( 380, 214):��������� (RE4) (�������)
      R_uki = R8_ali * R_(27)
C sens_init_1.fgi( 383, 216):����������
      R_(28) = 13553
C sens_init_1.fgi( 380, 220):��������� (RE4) (�������)
      R_eli = R8_ili * R_(28)
C sens_init_1.fgi( 383, 222):����������
      R_imi = R8_omi
C sens_init_1.fgi( 299, 145):��������
      R_umi = R8_api
C sens_init_1.fgi( 299, 156):��������
      R_epi = R8_ipi
C sens_init_1.fgi( 299, 160):��������
      R_opi = R8_upi
C sens_init_1.fgi( 299, 164):��������
      R_iri = R8_ori
C sens_init_1.fgi( 299, 172):��������
      R_(29) = 13553
C sens_init_1.fgi(  46,  70):��������� (RE4) (�������)
      R_ati = R8_eti * R_(29)
C sens_init_1.fgi(  49,  72):����������
      R_(30) = 13553
C sens_init_1.fgi(  46,  76):��������� (RE4) (�������)
      R_iti = R8_oti * R_(30)
C sens_init_1.fgi(  49,  78):����������
      R_uri = R8_asi
C sens_init_1.fgi(  46,  58):��������
      R_osi = R8_usi
C sens_init_1.fgi(  46,  66):��������
      R_evi = R8_ivi
C sens_init_1.fgi(  46, 101):��������
      R_ixi = R8_oxi
C sens_init_1.fgi(  46, 115):��������
      R_uxi = R8_abo
C sens_init_1.fgi(  46, 119):��������
      if(L0_af) then
         R_(4)=R_(5)
      else
         R_(4)=R_(6)
      endif
C ASU_SIO.fgi(  56, 227):���� RE IN LO CH7
C label 263  try263=try263-1
      if(L0_ud) then
         R_(1)=R_(2)
      else
         R_(1)=R_(3)
      endif
C ASU_SIO.fgi(  56, 214):���� RE IN LO CH7
      R_(7) = R_(4) + R_(1)
C ASU_SIO.fgi(  64, 220):��������
      R0_ak=R0_ak+deltat/R0_uf*R_(7)
      if(R0_ak.gt.R0_of) then
         R0_ak=R0_of
      elseif(R0_ak.lt.R0_ek) then
         R0_ak=R0_ek
      endif
C ASU_SIO.fgi(  77, 218):����������
      L_(10)=R0_ak.lt.R0_ed
C ASU_SIO.fgi(  98, 216):���������� <
      L_(11)=R0_ak.gt.R0_od
C ASU_SIO.fgi(  98, 224):���������� >
      L_(8) = L_(10).OR.L_(11)
C ASU_SIO.fgi(  86, 197):���
      L0_af=L_(13).or.(L0_af.and..not.(L_(8)))
      L_(9)=.not.L0_af
C ASU_SIO.fgi(  46, 218):RS �������
      L0_ud=L_(12).or.(L0_ud.and..not.(L_(8)))
      L_(7)=.not.L0_ud
C ASU_SIO.fgi(  46, 205):RS �������
      if(L_(10)) then
         I_ad=I_(7)
      else
         I_ad=I_(8)
      endif
C ASU_SIO.fgi( 109, 219):���� RE IN LO CH7
      L_(1)=R0_ak.lt.R0_e
C ASU_SIO.fgi(  98, 230):���������� <
      L_(3)=R0_ak.gt.R0_i
C ASU_SIO.fgi(  98, 236):���������� >
      L_(2) = L_(3).AND.L_(1)
C ASU_SIO.fgi( 103, 233):�
      if(L_(2)) then
         I_(4)=I_(1)
      else
         I_(4)=I_(2)
      endif
C ASU_SIO.fgi( 109, 237):���� RE IN LO CH7
      if(L_(11)) then
         I_id=I_(3)
      else
         I_id=I_(4)
      endif
C ASU_SIO.fgi( 109, 227):���� RE IN LO CH7
      if(L0_il) then
         R_(11)=R_(12)
      else
         R_(11)=R_(13)
      endif
C ASU_SIO.fgi(  57, 276):���� RE IN LO CH7
C label 300  try300=try300-1
      if(L0_el) then
         R_(8)=R_(9)
      else
         R_(8)=R_(10)
      endif
C ASU_SIO.fgi(  57, 263):���� RE IN LO CH7
      R_(14) = R_(11) + R_(8)
C ASU_SIO.fgi(  65, 269):��������
      R0_im=R0_im+deltat/R0_em*R_(14)
      if(R0_im.gt.R0_am) then
         R0_im=R0_am
      elseif(R0_im.lt.R0_om) then
         R0_im=R0_om
      endif
C ASU_SIO.fgi(  78, 267):����������
      L_(17)=R0_im.lt.R0_ok
C ASU_SIO.fgi(  99, 265):���������� <
      L_(18)=R0_im.gt.R0_al
C ASU_SIO.fgi(  99, 273):���������� >
      L_(15) = L_(17).OR.L_(18)
C ASU_SIO.fgi(  87, 246):���
      L0_il=L_(20).or.(L0_il.and..not.(L_(15)))
      L_(16)=.not.L0_il
C ASU_SIO.fgi(  47, 267):RS �������
      L0_el=L_(19).or.(L0_el.and..not.(L_(15)))
      L_(14)=.not.L0_el
C ASU_SIO.fgi(  47, 254):RS �������
      if(L_(17)) then
         I_ik=I_(11)
      else
         I_ik=I_(12)
      endif
C ASU_SIO.fgi( 110, 268):���� RE IN LO CH7
      L_(5)=R0_im.lt.R0_o
C ASU_SIO.fgi(  99, 279):���������� <
      L_(6)=R0_im.gt.R0_u
C ASU_SIO.fgi(  99, 285):���������� >
      L_(4) = L_(6).AND.L_(5)
C ASU_SIO.fgi( 104, 282):�
      if(L_(4)) then
         I_(14)=I_(5)
      else
         I_(14)=I_(6)
      endif
C ASU_SIO.fgi( 110, 286):���� RE IN LO CH7
      if(L_(18)) then
         I_uk=I_(13)
      else
         I_uk=I_(14)
      endif
C ASU_SIO.fgi( 110, 276):���� RE IN LO CH7
      End
