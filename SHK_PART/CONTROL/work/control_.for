       SUBROUTINE SPW_Init_tab(iTabVer)
       integer*4 iTabVer
       TYPE TSubr
         sequence
         integer*4 addr
         integer*4 period
         integer*4 phase
         integer*4 object
         integer*4 allowed
         integer*4 ncall
         real*8    time
         integer*4 individualScale
         integer*4 rezerv(2),flags,CPUTime(2)
         character(LEN=32) name
       END TYPE TSubr

       TYPE(TSubr) SPW_Table_Sub(5)
       Common/SPW_Table_Sub/ SPW_Table_Sub
       external PARAM
       Data SPW_Table_Sub(1)/TSubr(0,1,0,2,0,0,0.,0,0,8,0,
     &   'PARAM')/
       external KPA
       Data SPW_Table_Sub(2)/TSubr(0,1,0,3,0,0,0.,0,0,8,0,
     &   'KPA')/
       external KPG_KPH
       Data SPW_Table_Sub(3)/TSubr(0,1,0,4,0,0,0.,0,0,8,0,
     &   'KPG_KPH')/
       external SRG
       Data SPW_Table_Sub(4)/TSubr(0,1,0,5,0,0,0.,0,0,8,0,
     &   'SRG')/
       external DAA_DAB
       Data SPW_Table_Sub(5)/TSubr(0,1,0,6,0,0,0.,0,0,8,0,
     &   'DAA_DAB')/

       LOGICAL*1 SPW_0(0:1791)
       Common/SPW_0/ SPW_0 !
       LOGICAL*1 SPW_1(0:6143)
       Common/SPW_1/ SPW_1 !
       CHARACTER*1 SPW_2(0:2559)
       Common/SPW_2/ SPW_2 !
       LOGICAL*1 SPW_3(0:49151)
       Common/SPW_3/ SPW_3 !
       CHARACTER*1 SPW_4(0:6143)
       Common/SPW_4/ SPW_4 !
       LOGICAL*1 SPW_5(0:57343)
       Common/SPW_5/ SPW_5 !
       CHARACTER*1 SPW_6(0:2559)
       Common/SPW_6/ SPW_6 !
       LOGICAL*1 SPW_7(0:383)
       Common/SPW_7/ SPW_7 !
       LOGICAL*1 SPW_8(0:31)
       Common/SPW_8/ SPW_8 !
       LOGICAL*1 SPW_9(0:15)
       Common/SPW_9/ SPW_9 !
       LOGICAL*1 SPW_10(0:15)
       Common/SPW_10/ SPW_10 !

       real*4 info_kwant
       integer*4 info_siz
       common/spw_info/ info_siz,info_kwant
       !MS$ATTRIBUTES DLLEXPORT::spw_info
       Data info_siz/8/,info_kwant/0.125/
       integer*4 dispStateHdr
       integer*4 stepCounter
       real*8    subInfo(20)
       common/spw_dispState/ dispStateHdr,stepCounter,subInfo

       character*(32) ver(6)
       integer*4 nver,versiz
       common/spw_versions/ nver,versiz,ver
       !MS$ATTRIBUTES DLLEXPORT::spw_versions
       Data nver/6/,versiz/32/
       Data ver(1)/''/
       Data ver(2)/'v_DF386A6A_PARAM'/
       Data ver(3)/'v_722B6FA2_KPA'/
       Data ver(4)/'v_74849DFD_KPG_KPH'/
       Data ver(5)/'v_70F100EB_SRG'/
       Data ver(6)/'v_F851A3FE_DAA_DAB'/

       CALL SPW_SET_SIGN(865693333,0,-253658662)
       iTabVer=iTabVer*1000+1088

       CALL SPW_SET_UIDTR(687795771,-1015539916)
       CALL SPW_SET_KWANT(REAL(0.125))
       CALL SPW_SET_NCMN(11)
       CALL SPW_ADD_CMN(LOC(SPW_0),1706)
       CALL SPW_ADD_CMN(LOC(SPW_1),5586)
       CALL SPW_ADD_CMN(LOC(SPW_2),2072)
       CALL SPW_ADD_CMN(LOC(SPW_3),48259)
       CALL SPW_ADD_CMN(LOC(SPW_4),5954)
       CALL SPW_ADD_CMN(LOC(SPW_5),52342)
       CALL SPW_ADD_CMN(LOC(SPW_6),2170)
       CALL SPW_ADD_CMN(LOC(SPW_7),350)
       CALL SPW_ADD_CMN(LOC(SPW_8),32)
       CALL SPW_ADD_CMN(LOC(SPW_9),1)
       CALL SPW_ADD_CMN(LOC(SPW_10),16)
       CALL SPW_SET_NAMES(77705,
     +    'control_.vpt',
     +    'control_.C3781734.vpt')
       SPW_Table_Sub(1)%addr=LOC(PARAM)
       SPW_Table_Sub(1)%allowed=LOC(SPW_0)+1221
       SPW_Table_Sub(1)%individualScale=LOC(SPW_0)+512
       SPW_Table_Sub(2)%addr=LOC(KPA)
       SPW_Table_Sub(2)%allowed=LOC(SPW_1)+5084
       SPW_Table_Sub(2)%individualScale=LOC(SPW_1)+128
       SPW_Table_Sub(3)%addr=LOC(KPG_KPH)
       SPW_Table_Sub(3)%allowed=LOC(SPW_3)+48258
       SPW_Table_Sub(3)%individualScale=LOC(SPW_3)+3856
       SPW_Table_Sub(4)%addr=LOC(SRG)
       SPW_Table_Sub(4)%allowed=LOC(SPW_5)+52340
       SPW_Table_Sub(4)%individualScale=LOC(SPW_5)+4704
       SPW_Table_Sub(5)%addr=LOC(DAA_DAB)
       SPW_Table_Sub(5)%allowed=LOC(SPW_7)+348
       SPW_Table_Sub(5)%individualScale=LOC(SPW_7)+0
       CALL SPW_SET_SUB_LST(5,SPW_Table_Sub,stepCounter)
       end

       subroutine spw_initDisp
       integer*4 dispStateHdr
       integer*4 stepCounter
       real*8    subInfo(20)
       common/spw_dispState/ dispStateHdr,stepCounter,subInfo
       stepCounter=0
       subInfo=0
       end

       subroutine KPG_KPH_ConInQ
       end

       subroutine KPG_KPH_ConIn
       LOGICAL*1 SPW_0(0:1791)
       Common/SPW_0/ SPW_0 !
       integer*4 SPW_0_4(0:447)
       equivalence(SPW_0,SPW_0_4)
       LOGICAL*1 SPW_1(0:6143)
       Common/SPW_1/ SPW_1 !
       integer*4 SPW_1_4(0:1535)
       equivalence(SPW_1,SPW_1_4)
        Call KPG_KPH_ConInQ
!beg �������:20KPJ11AA101
        SPW_0(1466)=.false.	!L_(62) O
!end �������:20KPJ11AA101
!beg �������:20KPJ11AA102
        SPW_0(1464)=.false.	!L_(60) O
!end �������:20KPJ11AA102
!beg �������:20KPJ11AA103
        SPW_0(1462)=.false.	!L_(58) O
!end �������:20KPJ11AA103
!beg �������:20KPJ11AA104
        SPW_0(1460)=.false.	!L_(56) O
!end �������:20KPJ11AA104
!beg �������:20KPJ11AA105
        SPW_0(1458)=.false.	!L_(54) O
!end �������:20KPJ11AA105
!beg �������:20KPJ11AA106
        SPW_0(1456)=.false.	!L_(52) O
!end �������:20KPJ11AA106
!beg �������:20KPJ11AA107
        SPW_0(1454)=.false.	!L_(50) O
!end �������:20KPJ11AA107
!beg �������:20KPJ11AA108
        SPW_0(1452)=.false.	!L_(48) O
!end �������:20KPJ11AA108
!beg �������:20KPJ11AA109
        SPW_0(1450)=.false.	!L_(46) O
!end �������:20KPJ11AA109
!beg �������:20KPJ11AA110
        SPW_0(1448)=.false.	!L_(44) O
!end �������:20KPJ11AA110
!beg �������:20KPJ42AA101
        SPW_0(1423)=.false.	!L_(19) O
!end �������:20KPJ42AA101
!beg �������:20KPJ70AA101
        SPW_0(1515)=.false.	!L_(111) O
!end �������:20KPJ70AA101
!beg �������:20KPJ70AA102
        SPW_0(1513)=.false.	!L_(109) O
!end �������:20KPJ70AA102
!beg �������:20KPJ80AA101
        SPW_0(1476)=.false.	!L_(72) O
!end �������:20KPJ80AA101
!beg �������:20KPJ70AA103
        SPW_0(1511)=.false.	!L_(107) O
!end �������:20KPJ70AA103
!beg �������:20KPJ80AA102
        SPW_0(1412)=.false.	!L_(8) O
!end �������:20KPJ80AA102
!beg �������:20KPJ70AA104
        SPW_0(1509)=.false.	!L_(105) O
!end �������:20KPJ70AA104
!beg �������:20KPJ70AA105
        SPW_0(1507)=.false.	!L_(103) O
!end �������:20KPJ70AA105
!beg �������:20KPJ70AA106
        SPW_0(1505)=.false.	!L_(101) O
!end �������:20KPJ70AA106
!beg �������:20KPJ70AA107
        SPW_0(1503)=.false.	!L_(99) O
!end �������:20KPJ70AA107
!beg �������:20KPJ11AA101
        SPW_0(1465)=.false.	!L_(61) O
!end �������:20KPJ11AA101
!beg �������:20KPJ21AA101
        SPW_0(1546)=.false.	!L_(142) O
!end �������:20KPJ21AA101
!beg �������:20KPJ11AA102
        SPW_0(1463)=.false.	!L_(59) O
!end �������:20KPJ11AA102
!beg �������:20KPJ21AA102
        SPW_0(1544)=.false.	!L_(140) O
!end �������:20KPJ21AA102
!beg �������:20KPJ11AA103
        SPW_0(1461)=.false.	!L_(57) O
!end �������:20KPJ11AA103
!beg �������:20KPJ21AA103
        SPW_0(1542)=.false.	!L_(138) O
!end �������:20KPJ21AA103
!beg �������:20KPJ11AA104
        SPW_0(1459)=.false.	!L_(55) O
!end �������:20KPJ11AA104
!beg �������:20KPJ21AA104
        SPW_0(1540)=.false.	!L_(136) O
!end �������:20KPJ21AA104
!beg �������:20KPJ11AA105
        SPW_0(1457)=.false.	!L_(53) O
!end �������:20KPJ11AA105
!beg �������:20KPJ21AA105
        SPW_0(1538)=.false.	!L_(134) O
!end �������:20KPJ21AA105
!beg �������:20KPJ11AA106
        SPW_0(1455)=.false.	!L_(51) O
!end �������:20KPJ11AA106
!beg �������:20KPJ21AA106
        SPW_0(1536)=.false.	!L_(132) O
!end �������:20KPJ21AA106
!beg �������:20KPJ11AA107
        SPW_0(1453)=.false.	!L_(49) O
!end �������:20KPJ11AA107
!beg �������:20KPJ21AA107
        SPW_0(1534)=.false.	!L_(130) O
!end �������:20KPJ21AA107
!beg �������:20KPJ11AA108
        SPW_0(1451)=.false.	!L_(47) O
!end �������:20KPJ11AA108
!beg �������:20KPJ11AA109
        SPW_0(1449)=.false.	!L_(45) O
!end �������:20KPJ11AA109
!beg �������:20KPJ21AA108
        SPW_0(1416)=.false.	!L_(12) O
!end �������:20KPJ21AA108
!beg �������:20KPJ11AA110
        SPW_0(1447)=.false.	!L_(43) O
!end �������:20KPJ11AA110
!beg ��������:20KPH31AP001
        SPW_0(1551)=.false.	!L_(147) O
!end ��������:20KPH31AP001
!beg ��������:20KPH31AP002
        SPW_0(1549)=.false.	!L_(145) O
!end ��������:20KPH31AP002
!beg �������:20KPJ80AA101
        SPW_0(1475)=.false.	!L_(71) O
!end �������:20KPJ80AA101
!beg �������:20KPJ80AA102
        SPW_0(1411)=.false.	!L_(7) O
!end �������:20KPJ80AA102
!beg ���������:20KPH15AP001
        SPW_0(1558)=.false.	!L_(154) O
!end ���������:20KPH15AP001
!beg ���������:20KPH15AP002
        SPW_0(1560)=.false.	!L_(156) O
!end ���������:20KPH15AP002
!beg �������:20KPJ21AA101
        SPW_0(1545)=.false.	!L_(141) O
!end �������:20KPJ21AA101
!beg �������:20KPJ21AA102
        SPW_0(1543)=.false.	!L_(139) O
!end �������:20KPJ21AA102
!beg �������:20KPJ21AA103
        SPW_0(1541)=.false.	!L_(137) O
!end �������:20KPJ21AA103
!beg �������:20KPJ21AA104
        SPW_0(1539)=.false.	!L_(135) O
!end �������:20KPJ21AA104
!beg �������:20KPJ21AA105
        SPW_0(1537)=.false.	!L_(133) O
!end �������:20KPJ21AA105
!beg �������:20KPJ21AA106
        SPW_0(1535)=.false.	!L_(131) O
!end �������:20KPJ21AA106
!beg �������:20KPJ21AA107
        SPW_0(1533)=.false.	!L_(129) O
!end �������:20KPJ21AA107
!beg �������:20KPJ21AA108
        SPW_0(1415)=.false.	!L_(11) O
!end �������:20KPJ21AA108
!beg �������:20KPV12AA101
        SPW_0(1422)=.false.	!L_(18) O
!end �������:20KPV12AA101
!beg ��������:20KPH41AP001
        SPW_0(1555)=.false.	!L_(151) O
!end ��������:20KPH41AP001
!beg ��������:20KPH41AP002
        SPW_0(1553)=.false.	!L_(149) O
!end ��������:20KPH41AP002
!beg �������:20KPV12AA101
        SPW_0(1421)=.false.	!L_(17) O
!end �������:20KPV12AA101
!beg �������:20KPJ82AA101
        SPW_0(1470)=.false.	!L_(66) O
!end �������:20KPJ82AA101
!beg �������:20KPJ82AA102
        SPW_0(1468)=.false.	!L_(64) O
!end �������:20KPJ82AA102
!beg �������:20KPJ51AA101
        SPW_0(1480)=.false.	!L_(76) O
!end �������:20KPJ51AA101
!beg �������:20KPJ51AA102
        SPW_0(1478)=.false.	!L_(74) O
!end �������:20KPJ51AA102
!beg ���������:20KPG12AP001
        SPW_0(1564)=.false.	!L_(160) O
!end ���������:20KPG12AP001
!beg ���������:20KPG12AP002
        SPW_0(1562)=.false.	!L_(158) O
!end ���������:20KPG12AP002
!beg ���������:20KPH51BB001
        SPW_0(1548)=.false.	!L_(144) O
!end ���������:20KPH51BB001
!beg �������:20KPJ20AA101
        SPW_0(1518)=.false.	!L_(114) O
!end �������:20KPJ20AA101
!beg �������:20KPJ82AA101
        SPW_0(1469)=.false.	!L_(65) O
!end �������:20KPJ82AA101
!beg �������:20KPJ82AA102
        SPW_0(1467)=.false.	!L_(63) O
!end �������:20KPJ82AA102
!beg �������:20KPJ51AA101
        SPW_0(1479)=.false.	!L_(75) O
!end �������:20KPJ51AA101
!beg �������:20KPJ51AA102
        SPW_0(1477)=.false.	!L_(73) O
!end �������:20KPJ51AA102
!beg �������:20KPJ61AA101
        SPW_0(1474)=.false.	!L_(70) O
!end �������:20KPJ61AA101
!beg �������:20KPJ61AA102
        SPW_0(1472)=.false.	!L_(68) O
!end �������:20KPJ61AA102
!beg ��������:20KPH15AP001
        SPW_0(1557)=.false.	!L_(153) O
!end ��������:20KPH15AP001
!beg ��������:20KPH15AP002
        SPW_0(1559)=.false.	!L_(155) O
!end ��������:20KPH15AP002
!beg �������:20KPV41AL001
        SPW_0(1420)=.false.	!L_(16) O
!end �������:20KPV41AL001
!beg �������:20KPJ20AA101
        SPW_0(1517)=.false.	!L_(113) O
!end �������:20KPJ20AA101
!beg �������:20KPJ30AA101
        SPW_0(1502)=.false.	!L_(98) O
!end �������:20KPJ30AA101
!beg �������:20KPJ30AA102
        SPW_0(1500)=.false.	!L_(96) O
!end �������:20KPJ30AA102
!beg �������:20KPJ30AA103
        SPW_0(1498)=.false.	!L_(94) O
!end �������:20KPJ30AA103
!beg �������:20KPJ30AA104
        SPW_0(1496)=.false.	!L_(92) O
!end �������:20KPJ30AA104
!beg �������:20KPJ30AA105
        SPW_0(1494)=.false.	!L_(90) O
!end �������:20KPJ30AA105
!beg �������:20KPJ30AA106
        SPW_0(1492)=.false.	!L_(88) O
!end �������:20KPJ30AA106
!beg �������:20KPJ30AA107
        SPW_0(1490)=.false.	!L_(86) O
!end �������:20KPJ30AA107
!beg �������:20KPJ30AA108
        SPW_0(1488)=.false.	!L_(84) O
!end �������:20KPJ30AA108
!beg �������:20KPJ30AA109
        SPW_0(1486)=.false.	!L_(82) O
!end �������:20KPJ30AA109
!beg �������:20KPJ61AA101
        SPW_0(1473)=.false.	!L_(69) O
!end �������:20KPJ61AA101
!beg �������:20KPJ61AA102
        SPW_0(1471)=.false.	!L_(67) O
!end �������:20KPJ61AA102
!beg �������:20KPV41AL001
        SPW_0(1419)=.false.	!L_(15) O
!end �������:20KPV41AL001
!beg �������:20KPJ12AA101
        SPW_0(1446)=.false.	!L_(42) O
!end �������:20KPJ12AA101
!beg �������:20KPJ12AA102
        SPW_0(1444)=.false.	!L_(40) O
!end �������:20KPJ12AA102
!beg �������:20KPJ12AA103
        SPW_0(1442)=.false.	!L_(38) O
!end �������:20KPJ12AA103
!beg �������:20KPJ12AA104
        SPW_0(1440)=.false.	!L_(36) O
!end �������:20KPJ12AA104
!beg �������:20KPJ12AA105
        SPW_0(1438)=.false.	!L_(34) O
!end �������:20KPJ12AA105
!beg �������:20KPJ12AA106
        SPW_0(1436)=.false.	!L_(32) O
!end �������:20KPJ12AA106
!beg �������:20KPJ12AA107
        SPW_0(1434)=.false.	!L_(30) O
!end �������:20KPJ12AA107
!beg ���������:20KPG32AP001
        SPW_0(1568)=.false.	!L_(164) O
!end ���������:20KPG32AP001
!beg �������:20KPJ12AA108
        SPW_0(1432)=.false.	!L_(28) O
!end �������:20KPJ12AA108
!beg ���������:20KPG32AP002
        SPW_0(1566)=.false.	!L_(162) O
!end ���������:20KPG32AP002
!beg �������:20KPJ12AA109
        SPW_0(1430)=.false.	!L_(26) O
!end �������:20KPJ12AA109
!beg �������:20KPJ30AA101
        SPW_0(1501)=.false.	!L_(97) O
!end �������:20KPJ30AA101
!beg �������:20KPJ30AA102
        SPW_0(1499)=.false.	!L_(95) O
!end �������:20KPJ30AA102
!beg �������:20KPJ30AA103
        SPW_0(1497)=.false.	!L_(93) O
!end �������:20KPJ30AA103
!beg �������:20KPJ30AA104
        SPW_0(1495)=.false.	!L_(91) O
!end �������:20KPJ30AA104
!beg �������:20KPJ30AA105
        SPW_0(1493)=.false.	!L_(89) O
!end �������:20KPJ30AA105
!beg �������:20KPJ30AA106
        SPW_0(1491)=.false.	!L_(87) O
!end �������:20KPJ30AA106
!beg �������:20KPJ30AA107
        SPW_0(1489)=.false.	!L_(85) O
!end �������:20KPJ30AA107
!beg �������:20KPJ12AA110
        SPW_0(1428)=.false.	!L_(24) O
!end �������:20KPJ12AA110
!beg �������:20KPJ30AA108
        SPW_0(1487)=.false.	!L_(83) O
!end �������:20KPJ30AA108
!beg �������:20KPJ30AA109
        SPW_0(1485)=.false.	!L_(81) O
!end �������:20KPJ30AA109
!beg �������:20KPV46AB001
        SPW_0(1426)=.false.	!L_(22) O
!end �������:20KPV46AB001
!beg �������:20KPV43AB001
        SPW_0(1418)=.false.	!L_(14) O
!end �������:20KPV43AB001
!beg �������:20KPJ12AA101
        SPW_0(1445)=.false.	!L_(41) O
!end �������:20KPJ12AA101
!beg �������:20KPJ22AA101
        SPW_0(1532)=.false.	!L_(128) O
!end �������:20KPJ22AA101
!beg �������:20KPJ12AA102
        SPW_0(1443)=.false.	!L_(39) O
!end �������:20KPJ12AA102
!beg �������:20KPJ22AA102
        SPW_0(1530)=.false.	!L_(126) O
!end �������:20KPJ22AA102
!beg �������:20KPJ12AA103
        SPW_0(1441)=.false.	!L_(37) O
!end �������:20KPJ12AA103
!beg �������:20KPJ22AA103
        SPW_0(1528)=.false.	!L_(124) O
!end �������:20KPJ22AA103
!beg �������:20KPJ12AA104
        SPW_0(1439)=.false.	!L_(35) O
!end �������:20KPJ12AA104
!beg �������:20KPJ22AA104
        SPW_0(1526)=.false.	!L_(122) O
!end �������:20KPJ22AA104
!beg �������:20KPJ12AA105
        SPW_0(1437)=.false.	!L_(33) O
!end �������:20KPJ12AA105
!beg �������:20KPJ22AA105
        SPW_0(1524)=.false.	!L_(120) O
!end �������:20KPJ22AA105
!beg �������:20KPJ12AA106
        SPW_0(1435)=.false.	!L_(31) O
!end �������:20KPJ12AA106
!beg �������:20KPJ22AA106
        SPW_0(1522)=.false.	!L_(118) O
!end �������:20KPJ22AA106
!beg �������:20KPJ12AA107
        SPW_0(1433)=.false.	!L_(29) O
!end �������:20KPJ12AA107
!beg �������:20KPJ22AA107
        SPW_0(1520)=.false.	!L_(116) O
!end �������:20KPJ22AA107
!beg �������:20KPJ12AA108
        SPW_0(1431)=.false.	!L_(27) O
!end �������:20KPJ12AA108
!beg �������:20KPJ12AA109
        SPW_0(1429)=.false.	!L_(25) O
!end �������:20KPJ12AA109
!beg �������:20KPJ22AA108
        SPW_0(1414)=.false.	!L_(10) O
!end �������:20KPJ22AA108
!beg �������:20KPJ50AA101
        SPW_0(1484)=.false.	!L_(80) O
!end �������:20KPJ50AA101
!beg �������:20KPJ50AA102
        SPW_0(1482)=.false.	!L_(78) O
!end �������:20KPJ50AA102
!beg �������:20KPJ12AA110
        SPW_0(1427)=.false.	!L_(23) O
!end �������:20KPJ12AA110
!beg ��������:20KPG12AP001
        SPW_0(1563)=.false.	!L_(159) O
!end ��������:20KPG12AP001
!beg ��������:20KPG12AP002
        SPW_0(1561)=.false.	!L_(157) O
!end ��������:20KPG12AP002
!beg ��������:20KPH51BB001
        SPW_0(1547)=.false.	!L_(143) O
!end ��������:20KPH51BB001
!beg �������:20KPV46AB001
        SPW_0(1425)=.false.	!L_(21) O
!end �������:20KPV46AB001
!beg ���������:20KPH31AP001
        SPW_0(1552)=.false.	!L_(148) O
!end ���������:20KPH31AP001
!beg ���������:20KPH31AP002
        SPW_0(1550)=.false.	!L_(146) O
!end ���������:20KPH31AP002
!beg �������:20KPV43AB001
        SPW_0(1417)=.false.	!L_(13) O
!end �������:20KPV43AB001
!beg �������:20KPJ22AA101
        SPW_0(1531)=.false.	!L_(127) O
!end �������:20KPJ22AA101
!beg �������:20KPJ22AA102
        SPW_0(1529)=.false.	!L_(125) O
!end �������:20KPJ22AA102
!beg �������:20KPJ22AA103
        SPW_0(1527)=.false.	!L_(123) O
!end �������:20KPJ22AA103
!beg �������:20KPJ22AA104
        SPW_0(1525)=.false.	!L_(121) O
!end �������:20KPJ22AA104
!beg �������:20KPJ22AA105
        SPW_0(1523)=.false.	!L_(119) O
!end �������:20KPJ22AA105
!beg �������:20KPJ22AA106
        SPW_0(1521)=.false.	!L_(117) O
!end �������:20KPJ22AA106
!beg �������:20KPJ22AA107
        SPW_0(1519)=.false.	!L_(115) O
!end �������:20KPJ22AA107
!beg �������:20KPJ22AA108
        SPW_0(1413)=.false.	!L_(9) O
!end �������:20KPJ22AA108
!beg �������:20KPJ50AA101
        SPW_0(1483)=.false.	!L_(79) O
!end �������:20KPJ50AA101
!beg �������:20KPJ50AA102
        SPW_0(1481)=.false.	!L_(77) O
!end �������:20KPJ50AA102
!beg ���������:20KPH41AP001
        SPW_0(1556)=.false.	!L_(152) O
!end ���������:20KPH41AP001
!beg ���������:20KPH41AP002
        SPW_0(1554)=.false.	!L_(150) O
!end ���������:20KPH41AP002
!beg �������:20KPJ42AA101
        SPW_0(1424)=.false.	!L_(20) O
!end �������:20KPJ42AA101
!beg �������:20KPJ70AA101
        SPW_0(1516)=.false.	!L_(112) O
!end �������:20KPJ70AA101
!beg �������:20KPJ70AA102
        SPW_0(1514)=.false.	!L_(110) O
!end �������:20KPJ70AA102
!beg �������:20KPJ70AA103
        SPW_0(1512)=.false.	!L_(108) O
!end �������:20KPJ70AA103
!beg �������:20KPJ70AA104
        SPW_0(1510)=.false.	!L_(106) O
!end �������:20KPJ70AA104
!beg �������:20KPJ70AA105
        SPW_0(1508)=.false.	!L_(104) O
!end �������:20KPJ70AA105
!beg �������:20KPJ70AA106
        SPW_0(1506)=.false.	!L_(102) O
!end �������:20KPJ70AA106
!beg �������:20KPJ70AA107
        SPW_0(1504)=.false.	!L_(100) O
!end �������:20KPJ70AA107
!beg ��������:20KPG32AP001
        SPW_0(1567)=.false.	!L_(163) O
!end ��������:20KPG32AP001
!beg ��������:20KPG32AP002
        SPW_0(1565)=.false.	!L_(161) O
!end ��������:20KPG32AP002
       end

       subroutine SRG_ConInQ
       end

       subroutine SRG_ConIn
       LOGICAL*1 SPW_0(0:1791)
       Common/SPW_0/ SPW_0 !
       integer*4 SPW_0_4(0:447)
       equivalence(SPW_0,SPW_0_4)
       LOGICAL*1 SPW_1(0:6143)
       Common/SPW_1/ SPW_1 !
       integer*4 SPW_1_4(0:1535)
       equivalence(SPW_1,SPW_1_4)
        Call SRG_ConInQ
!beg �������:20SRG10AA262
        SPW_1(5407)=.false.	!L_(323) O
!end �������:20SRG10AA262
!beg �������:20SRG10AA263
        SPW_1(5405)=.false.	!L_(321) O
!end �������:20SRG10AA263
!beg �������:20SRG10AA264
        SPW_1(5403)=.false.	!L_(319) O
!end �������:20SRG10AA264
!beg �������:20SRG10AA265
        SPW_1(5401)=.false.	!L_(317) O
!end �������:20SRG10AA265
!beg �������:20SRG10AA266
        SPW_1(5399)=.false.	!L_(315) O
!end �������:20SRG10AA266
!beg �������:20SRG10AA267
        SPW_1(5397)=.false.	!L_(313) O
!end �������:20SRG10AA267
!beg �������:20SRG10AA268
        SPW_1(5395)=.false.	!L_(311) O
!end �������:20SRG10AA268
!beg �������:20SRG10AA269
        SPW_1(5393)=.false.	!L_(309) O
!end �������:20SRG10AA269
!beg �������:20SRG10AA270
        SPW_1(5391)=.false.	!L_(307) O
!end �������:20SRG10AA270
!beg �������:20SRG10AA271
        SPW_1(5389)=.false.	!L_(305) O
!end �������:20SRG10AA271
!beg �������:20SRG10AA272
        SPW_1(5387)=.false.	!L_(303) O
!end �������:20SRG10AA272
!beg �������:20SRG10AA273
        SPW_1(5385)=.false.	!L_(301) O
!end �������:20SRG10AA273
!beg �������:20SRG10AA274
        SPW_1(5383)=.false.	!L_(299) O
!end �������:20SRG10AA274
!beg �������:20SRG10AA275
        SPW_1(5381)=.false.	!L_(297) O
!end �������:20SRG10AA275
!beg �������:20SRG10AA276
        SPW_1(5245)=.false.	!L_(161) O
!end �������:20SRG10AA276
!beg �������:20SRG10AA277
        SPW_1(5243)=.false.	!L_(159) O
!end �������:20SRG10AA277
!beg �������:20SRG10AA278
        SPW_1(5267)=.false.	!L_(183) O
!end �������:20SRG10AA278
!beg �������:20SRG10AA279
        SPW_1(5263)=.false.	!L_(179) O
!end �������:20SRG10AA279
!beg �������:20SRG10AA280
        SPW_1(5241)=.false.	!L_(157) O
!end �������:20SRG10AA280
!beg �������:20SRG10AA281
        SPW_1(5259)=.false.	!L_(175) O
!end �������:20SRG10AA281
!beg �������:20SRG10AA282
        SPW_1(5247)=.false.	!L_(163) O
!end �������:20SRG10AA282
!beg �������:20SRG10AA283
        SPW_1(5281)=.false.	!L_(197) O
!end �������:20SRG10AA283
!beg �������:20SRG10AA284
        SPW_1(5277)=.false.	!L_(193) O
!end �������:20SRG10AA284
!beg �������:20SRG10AA285
        SPW_1(5273)=.false.	!L_(189) O
!end �������:20SRG10AA285
!beg �������:20SRG10AA286
        SPW_1(5257)=.false.	!L_(173) O
!end �������:20SRG10AA286
!beg �������:20SRG10AA287
        SPW_1(5279)=.false.	!L_(195) O
!end �������:20SRG10AA287
!beg �������:20SRG10AA288
        SPW_1(5275)=.false.	!L_(191) O
!end �������:20SRG10AA288
!beg �������:20SRG10AA289
        SPW_1(5271)=.false.	!L_(187) O
!end �������:20SRG10AA289
!beg �������:20SRG10AA290
        SPW_1(5255)=.false.	!L_(171) O
!end �������:20SRG10AA290
!beg �������:20SRG10AA291
        SPW_1(5269)=.false.	!L_(185) O
!end �������:20SRG10AA291
!beg �������:20SRG10AA292
        SPW_1(5265)=.false.	!L_(181) O
!end �������:20SRG10AA292
!beg �������:20SRG10AA293
        SPW_1(5261)=.false.	!L_(177) O
!end �������:20SRG10AA293
!beg �������:20SRG10AA294
        SPW_1(5253)=.false.	!L_(169) O
!end �������:20SRG10AA294
!beg �������:20SRG10AA295
        SPW_1(5239)=.false.	!L_(155) O
!end �������:20SRG10AA295
!beg �������:20SRG10AA296
        SPW_1(5237)=.false.	!L_(153) O
!end �������:20SRG10AA296
!beg �������:20SRG10AA297
        SPW_1(5235)=.false.	!L_(151) O
!end �������:20SRG10AA297
!beg �������:20SRG10AA298
        SPW_1(5233)=.false.	!L_(149) O
!end �������:20SRG10AA298
!beg �������:20SRG10AA299
        SPW_1(5231)=.false.	!L_(147) O
!end �������:20SRG10AA299
!beg �������:20SRG10AA241
        SPW_1(5420)=.false.	!L_(336) O
!end �������:20SRG10AA241
!beg �������:20SRG10AA242
        SPW_1(5418)=.false.	!L_(334) O
!end �������:20SRG10AA242
!beg �������:20SRG10AA243
        SPW_1(5416)=.false.	!L_(332) O
!end �������:20SRG10AA243
!beg �������:20SRG10AA244
        SPW_1(5414)=.false.	!L_(330) O
!end �������:20SRG10AA244
!beg �������:20SRG10AA245
        SPW_1(5412)=.false.	!L_(328) O
!end �������:20SRG10AA245
!beg �������:20SRG10AA246
        SPW_1(5378)=.false.	!L_(294) O
!end �������:20SRG10AA246
!beg �������:20SRG10AA247
        SPW_1(5376)=.false.	!L_(292) O
!end �������:20SRG10AA247
!beg �������:20SRG10AA248
        SPW_1(5374)=.false.	!L_(290) O
!end �������:20SRG10AA248
!beg �������:20SRG10AA249
        SPW_1(5372)=.false.	!L_(288) O
!end �������:20SRG10AA249
!beg �������:20SRG10AA250
        SPW_1(5370)=.false.	!L_(286) O
!end �������:20SRG10AA250
!beg �������:20SRG10AA251
        SPW_1(5368)=.false.	!L_(284) O
!end �������:20SRG10AA251
!beg �������:20SRG10AA252
        SPW_1(5366)=.false.	!L_(282) O
!end �������:20SRG10AA252
!beg �������:20SRG10AA253
        SPW_1(5364)=.false.	!L_(280) O
!end �������:20SRG10AA253
!beg �������:20SRG10AA254
        SPW_1(5362)=.false.	!L_(278) O
!end �������:20SRG10AA254
!beg �������:20SRG10AA255
        SPW_1(5360)=.false.	!L_(276) O
!end �������:20SRG10AA255
!beg �������:20SRG10AA256
        SPW_1(5358)=.false.	!L_(274) O
!end �������:20SRG10AA256
!beg �������:20SRG10AA257
        SPW_1(5356)=.false.	!L_(272) O
!end �������:20SRG10AA257
!beg �������:20SRG10AA258
        SPW_1(5354)=.false.	!L_(270) O
!end �������:20SRG10AA258
!beg �������:20SRG10AA259
        SPW_1(5352)=.false.	!L_(268) O
!end �������:20SRG10AA259
!beg �������:20SRG10AA260
        SPW_1(5350)=.false.	!L_(266) O
!end �������:20SRG10AA260
!beg �������:20SRG10AA261
        SPW_1(5408)=.false.	!L_(324) O
!end �������:20SRG10AA261
!beg �������:20SRG10AA262
        SPW_1(5406)=.false.	!L_(322) O
!end �������:20SRG10AA262
!beg �������:20SRG10AA263
        SPW_1(5404)=.false.	!L_(320) O
!end �������:20SRG10AA263
!beg �������:20SRG10AA264
        SPW_1(5402)=.false.	!L_(318) O
!end �������:20SRG10AA264
!beg �������:20SRG10AA265
        SPW_1(5400)=.false.	!L_(316) O
!end �������:20SRG10AA265
!beg �������:20SRG10AA266
        SPW_1(5398)=.false.	!L_(314) O
!end �������:20SRG10AA266
!beg �������:20SRG10AA267
        SPW_1(5396)=.false.	!L_(312) O
!end �������:20SRG10AA267
!beg �������:20SRG10AA268
        SPW_1(5394)=.false.	!L_(310) O
!end �������:20SRG10AA268
!beg �������:20SRG10AA269
        SPW_1(5392)=.false.	!L_(308) O
!end �������:20SRG10AA269
!beg �������:20SRG10AA270
        SPW_1(5390)=.false.	!L_(306) O
!end �������:20SRG10AA270
!beg �������:20SRG10AA271
        SPW_1(5388)=.false.	!L_(304) O
!end �������:20SRG10AA271
!beg �������:20SRG10AA272
        SPW_1(5386)=.false.	!L_(302) O
!end �������:20SRG10AA272
!beg �������:20SRG10AA273
        SPW_1(5384)=.false.	!L_(300) O
!end �������:20SRG10AA273
!beg �������:20SRG10AA274
        SPW_1(5382)=.false.	!L_(298) O
!end �������:20SRG10AA274
!beg �������:20SRG10AA275
        SPW_1(5380)=.false.	!L_(296) O
!end �������:20SRG10AA275
!beg �������:20SRG10AA276
        SPW_1(5244)=.false.	!L_(160) O
!end �������:20SRG10AA276
!beg �������:20SRG10AA277
        SPW_1(5242)=.false.	!L_(158) O
!end �������:20SRG10AA277
!beg �������:20SRG10AA278
        SPW_1(5266)=.false.	!L_(182) O
!end �������:20SRG10AA278
!beg �������:20SRG10AA279
        SPW_1(5262)=.false.	!L_(178) O
!end �������:20SRG10AA279
!beg �������:20SRG10AA280
        SPW_1(5240)=.false.	!L_(156) O
!end �������:20SRG10AA280
!beg �������:20SRG10AA281
        SPW_1(5258)=.false.	!L_(174) O
!end �������:20SRG10AA281
!beg �������:20SRG10AA282
        SPW_1(5246)=.false.	!L_(162) O
!end �������:20SRG10AA282
!beg �������:20SRG10AA283
        SPW_1(5280)=.false.	!L_(196) O
!end �������:20SRG10AA283
!beg �������:20SRG10AA284
        SPW_1(5276)=.false.	!L_(192) O
!end �������:20SRG10AA284
!beg �������:20SRG10AA285
        SPW_1(5272)=.false.	!L_(188) O
!end �������:20SRG10AA285
!beg �������:20SRG10AA286
        SPW_1(5256)=.false.	!L_(172) O
!end �������:20SRG10AA286
!beg �������:20SRG10AA287
        SPW_1(5278)=.false.	!L_(194) O
!end �������:20SRG10AA287
!beg �������:20SRG10AA288
        SPW_1(5274)=.false.	!L_(190) O
!end �������:20SRG10AA288
!beg �������:20SRG10AA289
        SPW_1(5270)=.false.	!L_(186) O
!end �������:20SRG10AA289
!beg �������:20SRG10AA290
        SPW_1(5254)=.false.	!L_(170) O
!end �������:20SRG10AA290
!beg �������:20SRG10AA291
        SPW_1(5268)=.false.	!L_(184) O
!end �������:20SRG10AA291
!beg �������:20SRG10AA292
        SPW_1(5264)=.false.	!L_(180) O
!end �������:20SRG10AA292
!beg �������:20SRG10AA293
        SPW_1(5260)=.false.	!L_(176) O
!end �������:20SRG10AA293
!beg �������:20SRG10AA294
        SPW_1(5252)=.false.	!L_(168) O
!end �������:20SRG10AA294
!beg �������:20SRG10AA295
        SPW_1(5238)=.false.	!L_(154) O
!end �������:20SRG10AA295
!beg �������:20SRG10AA296
        SPW_1(5236)=.false.	!L_(152) O
!end �������:20SRG10AA296
!beg �������:20SRG10AA297
        SPW_1(5234)=.false.	!L_(150) O
!end �������:20SRG10AA297
!beg �������:20SRG10AA298
        SPW_1(5232)=.false.	!L_(148) O
!end �������:20SRG10AA298
!beg �������:20SRG10AA299
        SPW_1(5230)=.false.	!L_(146) O
!end �������:20SRG10AA299
!beg �������:20SRG10AA300
        SPW_1(5229)=.false.	!L_(145) O
!end �������:20SRG10AA300
!beg �������:20SRG10AA301
        SPW_1(5227)=.false.	!L_(143) O
!end �������:20SRG10AA301
!beg �������:20SRG10AA302
        SPW_1(5225)=.false.	!L_(141) O
!end �������:20SRG10AA302
!beg �������:20SRG10AA303
        SPW_1(5223)=.false.	!L_(139) O
!end �������:20SRG10AA303
!beg �������:20SRG10AA304
        SPW_1(5221)=.false.	!L_(137) O
!end �������:20SRG10AA304
!beg �������:20SRG10AA305
        SPW_1(5219)=.false.	!L_(135) O
!end �������:20SRG10AA305
!beg �������:20SRG10AA306
        SPW_1(5217)=.false.	!L_(133) O
!end �������:20SRG10AA306
!beg �������:20SRG10AA307
        SPW_1(5215)=.false.	!L_(131) O
!end �������:20SRG10AA307
!beg �������:20SRG10AA308
        SPW_1(5213)=.false.	!L_(129) O
!end �������:20SRG10AA308
!beg �������:20SRG10AA309
        SPW_1(5211)=.false.	!L_(127) O
!end �������:20SRG10AA309
!beg �������:20SRG10AA310
        SPW_1(5349)=.false.	!L_(265) O
!end �������:20SRG10AA310
!beg �������:20SRG10AA311
        SPW_1(5345)=.false.	!L_(261) O
!end �������:20SRG10AA311
!beg �������:20SRG10AA312
        SPW_1(5341)=.false.	!L_(257) O
!end �������:20SRG10AA312
!beg �������:20SRG10AA313
        SPW_1(5301)=.false.	!L_(217) O
!end �������:20SRG10AA313
!beg �������:20SRG10AA314
        SPW_1(5347)=.false.	!L_(263) O
!end �������:20SRG10AA314
!beg �������:20SRG10AA315
        SPW_1(5319)=.false.	!L_(235) O
!end �������:20SRG10AA315
!beg �������:20SRG10AA316
        SPW_1(5249)=.false.	!L_(165) O
!end �������:20SRG10AA316
!beg �������:20SRG10AA317
        SPW_1(5315)=.false.	!L_(231) O
!end �������:20SRG10AA317
!beg �������:20SRG10AA318
        SPW_1(5291)=.false.	!L_(207) O
!end �������:20SRG10AA318
!beg �������:20SRG10AA319
        SPW_1(5313)=.false.	!L_(229) O
!end �������:20SRG10AA319
!beg �������:20SRG10AA320
        SPW_1(5309)=.false.	!L_(225) O
!end �������:20SRG10AA320
!beg �������:20SRG10AA321
        SPW_1(5305)=.false.	!L_(221) O
!end �������:20SRG10AA321
!beg ���������:20SRG10AN021
        SPW_1(5111)=.false.	!L_(27) O
!end ���������:20SRG10AN021
!beg �������:20SRG10AA322
        SPW_1(5289)=.false.	!L_(205) O
!end �������:20SRG10AA322
!beg �������:20SRG10AA323
        SPW_1(5311)=.false.	!L_(227) O
!end �������:20SRG10AA323
!beg �������:20SRG10AA324
        SPW_1(5307)=.false.	!L_(223) O
!end �������:20SRG10AA324
!beg �������:20SRG10AA325
        SPW_1(5303)=.false.	!L_(219) O
!end �������:20SRG10AA325
!beg �������:20SRG10AA326
        SPW_1(5283)=.false.	!L_(199) O
!end �������:20SRG10AA326
!beg �������:20SRG10AA327
        SPW_1(5287)=.false.	!L_(203) O
!end �������:20SRG10AA327
!beg �������:20SRG10AA328
        SPW_1(5285)=.false.	!L_(201) O
!end �������:20SRG10AA328
!beg �������:20SRG10AA329
        SPW_1(5339)=.false.	!L_(255) O
!end �������:20SRG10AA329
!beg �������:20SRG10AA330
        SPW_1(5299)=.false.	!L_(215) O
!end �������:20SRG10AA330
!beg �������:20SRG10AA331
        SPW_1(5337)=.false.	!L_(253) O
!end �������:20SRG10AA331
!beg �������:20SRG10AA332
        SPW_1(5333)=.false.	!L_(249) O
!end �������:20SRG10AA332
!beg �������:20SRG10AA333
        SPW_1(5329)=.false.	!L_(245) O
!end �������:20SRG10AA333
!beg �������:20SRG10AA334
        SPW_1(5297)=.false.	!L_(213) O
!end �������:20SRG10AA334
!beg �������:20SRG10AA335
        SPW_1(5335)=.false.	!L_(251) O
!end �������:20SRG10AA335
!beg �������:20SRG10AA336
        SPW_1(5331)=.false.	!L_(247) O
!end �������:20SRG10AA336
!beg �������:20SRG10AA337
        SPW_1(5327)=.false.	!L_(243) O
!end �������:20SRG10AA337
!beg �������:20SRG10AA338
        SPW_1(5295)=.false.	!L_(211) O
!end �������:20SRG10AA338
!beg �������:20SRG10AA339
        SPW_1(5325)=.false.	!L_(241) O
!end �������:20SRG10AA339
!beg �������:20SRG10AA340
        SPW_1(5321)=.false.	!L_(237) O
!end �������:20SRG10AA340
!beg �������:20SRG10AA341
        SPW_1(5317)=.false.	!L_(233) O
!end �������:20SRG10AA341
!beg �������:20SRG10AA342
        SPW_1(5293)=.false.	!L_(209) O
!end �������:20SRG10AA342
!beg �������:20SRG10AA343
        SPW_1(5323)=.false.	!L_(239) O
!end �������:20SRG10AA343
!beg �������:20SRG10AA300
        SPW_1(5228)=.false.	!L_(144) O
!end �������:20SRG10AA300
!beg �������:20SRG10AA301
        SPW_1(5226)=.false.	!L_(142) O
!end �������:20SRG10AA301
!beg �������:20SRG10AA302
        SPW_1(5224)=.false.	!L_(140) O
!end �������:20SRG10AA302
!beg �������:20SRG10AA303
        SPW_1(5222)=.false.	!L_(138) O
!end �������:20SRG10AA303
!beg �������:20SRG10AA304
        SPW_1(5220)=.false.	!L_(136) O
!end �������:20SRG10AA304
!beg �������:20SRG10AA305
        SPW_1(5218)=.false.	!L_(134) O
!end �������:20SRG10AA305
!beg �������:20SRG10AA306
        SPW_1(5216)=.false.	!L_(132) O
!end �������:20SRG10AA306
!beg �������:20SRG10AA307
        SPW_1(5214)=.false.	!L_(130) O
!end �������:20SRG10AA307
!beg �������:20SRG10AA308
        SPW_1(5212)=.false.	!L_(128) O
!end �������:20SRG10AA308
!beg �������:20SRG10AA309
        SPW_1(5210)=.false.	!L_(126) O
!end �������:20SRG10AA309
!beg �������:20SRG10AA310
        SPW_1(5348)=.false.	!L_(264) O
!end �������:20SRG10AA310
!beg �������:20SRG10AA311
        SPW_1(5344)=.false.	!L_(260) O
!end �������:20SRG10AA311
!beg �������:20SRG10AA312
        SPW_1(5340)=.false.	!L_(256) O
!end �������:20SRG10AA312
!beg �������:20SRG10AA313
        SPW_1(5300)=.false.	!L_(216) O
!end �������:20SRG10AA313
!beg �������:20SRG10AA314
        SPW_1(5346)=.false.	!L_(262) O
!end �������:20SRG10AA314
!beg �������:20SRG10AA315
        SPW_1(5318)=.false.	!L_(234) O
!end �������:20SRG10AA315
!beg �������:20SRG10AA316
        SPW_1(5248)=.false.	!L_(164) O
!end �������:20SRG10AA316
!beg �������:20SRG10AA317
        SPW_1(5314)=.false.	!L_(230) O
!end �������:20SRG10AA317
!beg �������:20SRG10AA318
        SPW_1(5290)=.false.	!L_(206) O
!end �������:20SRG10AA318
!beg �������:20SRG10AA319
        SPW_1(5312)=.false.	!L_(228) O
!end �������:20SRG10AA319
!beg �������:20SRG10AA320
        SPW_1(5308)=.false.	!L_(224) O
!end �������:20SRG10AA320
!beg �������:20SRG10AA321
        SPW_1(5304)=.false.	!L_(220) O
!end �������:20SRG10AA321
!beg �������:20SRG10AA322
        SPW_1(5288)=.false.	!L_(204) O
!end �������:20SRG10AA322
!beg �������:20SRG10AA323
        SPW_1(5310)=.false.	!L_(226) O
!end �������:20SRG10AA323
!beg �������:20SRG10AA324
        SPW_1(5306)=.false.	!L_(222) O
!end �������:20SRG10AA324
!beg �������:20SRG10AA325
        SPW_1(5302)=.false.	!L_(218) O
!end �������:20SRG10AA325
!beg �������:20SRG10AA326
        SPW_1(5282)=.false.	!L_(198) O
!end �������:20SRG10AA326
!beg �������:20SRG10AA327
        SPW_1(5286)=.false.	!L_(202) O
!end �������:20SRG10AA327
!beg �������:20SRG10AA328
        SPW_1(5284)=.false.	!L_(200) O
!end �������:20SRG10AA328
!beg �������:20SRG10AA329
        SPW_1(5338)=.false.	!L_(254) O
!end �������:20SRG10AA329
!beg �������:20SRG10AA330
        SPW_1(5298)=.false.	!L_(214) O
!end �������:20SRG10AA330
!beg �������:20SRG10AA331
        SPW_1(5336)=.false.	!L_(252) O
!end �������:20SRG10AA331
!beg �������:20SRG10AA332
        SPW_1(5332)=.false.	!L_(248) O
!end �������:20SRG10AA332
!beg �������:20SRG10AA333
        SPW_1(5328)=.false.	!L_(244) O
!end �������:20SRG10AA333
!beg �������:20SRG10AA334
        SPW_1(5296)=.false.	!L_(212) O
!end �������:20SRG10AA334
!beg �������:20SRG10AA335
        SPW_1(5334)=.false.	!L_(250) O
!end �������:20SRG10AA335
!beg �������:20SRG10AA336
        SPW_1(5330)=.false.	!L_(246) O
!end �������:20SRG10AA336
!beg �������:20SRG10AA337
        SPW_1(5326)=.false.	!L_(242) O
!end �������:20SRG10AA337
!beg �������:20SRG10AA338
        SPW_1(5294)=.false.	!L_(210) O
!end �������:20SRG10AA338
!beg �������:20SRG10AA339
        SPW_1(5324)=.false.	!L_(240) O
!end �������:20SRG10AA339
!beg �������:20SRG10AA340
        SPW_1(5320)=.false.	!L_(236) O
!end �������:20SRG10AA340
!beg �������:20SRG10AA341
        SPW_1(5316)=.false.	!L_(232) O
!end �������:20SRG10AA341
!beg �������:20SRG10AA342
        SPW_1(5292)=.false.	!L_(208) O
!end �������:20SRG10AA342
!beg �������:20SRG10AA343
        SPW_1(5322)=.false.	!L_(238) O
!end �������:20SRG10AA343
!beg ��������:20SRG10AN021
        SPW_1(5110)=.false.	!L_(26) O
!end ��������:20SRG10AN021
!beg �������:20SRG10AA100
        SPW_1(5207)=.false.	!L_(123) O
!end �������:20SRG10AA100
!beg �������:20SRG10AA101
        SPW_1(5209)=.false.	!L_(125) O
!end �������:20SRG10AA101
!beg �������:20SRG10AA102
        SPW_1(5203)=.false.	!L_(119) O
!end �������:20SRG10AA102
!beg �������:20SRG10AA103
        SPW_1(5201)=.false.	!L_(117) O
!end �������:20SRG10AA103
!beg �������:20SRG10AA104
        SPW_1(5199)=.false.	!L_(115) O
!end �������:20SRG10AA104
!beg �������:20SRG10AA105
        SPW_1(5197)=.false.	!L_(113) O
!end �������:20SRG10AA105
!beg �������:20SRG10AA106
        SPW_1(5195)=.false.	!L_(111) O
!end �������:20SRG10AA106
!beg �������:20SRG10AA107
        SPW_1(5193)=.false.	!L_(109) O
!end �������:20SRG10AA107
!beg �������:20SRG10AA108
        SPW_1(5191)=.false.	!L_(107) O
!end �������:20SRG10AA108
!beg �������:20SRG10AA109
        SPW_1(5189)=.false.	!L_(105) O
!end �������:20SRG10AA109
!beg �������:20SRG10AA110
        SPW_1(5187)=.false.	!L_(103) O
!end �������:20SRG10AA110
!beg �������:20SRG10AA111
        SPW_1(5185)=.false.	!L_(101) O
!end �������:20SRG10AA111
!beg �������:20SRG10AA112
        SPW_1(5183)=.false.	!L_(99) O
!end �������:20SRG10AA112
!beg �������:20SRG10AA113
        SPW_1(5181)=.false.	!L_(97) O
!end �������:20SRG10AA113
!beg �������:20SRG10AA114
        SPW_1(5179)=.false.	!L_(95) O
!end �������:20SRG10AA114
!beg �������:20SRG10AA115
        SPW_1(5177)=.false.	!L_(93) O
!end �������:20SRG10AA115
!beg �������:20SRG10AA116
        SPW_1(5175)=.false.	!L_(91) O
!end �������:20SRG10AA116
!beg �������:20SRG10AA117
        SPW_1(5173)=.false.	!L_(89) O
!end �������:20SRG10AA117
!beg �������:20SRG10AA118
        SPW_1(5171)=.false.	!L_(87) O
!end �������:20SRG10AA118
!beg �������:20SRG10AA119
        SPW_1(5169)=.false.	!L_(85) O
!end �������:20SRG10AA119
!beg �������:20SRG10AA120
        SPW_1(5167)=.false.	!L_(83) O
!end �������:20SRG10AA120
!beg �������:20SRG10AA121
        SPW_1(5165)=.false.	!L_(81) O
!end �������:20SRG10AA121
!beg �������:20SRG10AA122
        SPW_1(5163)=.false.	!L_(79) O
!end �������:20SRG10AA122
!beg �������:20SRG10AA123
        SPW_1(5161)=.false.	!L_(77) O
!end �������:20SRG10AA123
!beg �������:20SRG10AA124
        SPW_1(5159)=.false.	!L_(75) O
!end �������:20SRG10AA124
!beg �������:20SRG10AA125
        SPW_1(5157)=.false.	!L_(73) O
!end �������:20SRG10AA125
!beg �������:20SRG10AA126
        SPW_1(5153)=.false.	!L_(69) O
!end �������:20SRG10AA126
!beg �������:20SRG10AA127
        SPW_1(5151)=.false.	!L_(67) O
!end �������:20SRG10AA127
!beg �������:20SRG10AA128
        SPW_1(5143)=.false.	!L_(59) O
!end �������:20SRG10AA128
!beg �������:20SRG10AA129
        SPW_1(5141)=.false.	!L_(57) O
!end �������:20SRG10AA129
!beg �������:20SRG10AA130
        SPW_1(5139)=.false.	!L_(55) O
!end �������:20SRG10AA130
!beg �������:20SRG10AA131
        SPW_1(5137)=.false.	!L_(53) O
!end �������:20SRG10AA131
!beg �������:20SRG10AA132
        SPW_1(5135)=.false.	!L_(51) O
!end �������:20SRG10AA132
!beg �������:20SRG10AA133
        SPW_1(5133)=.false.	!L_(49) O
!end �������:20SRG10AA133
!beg �������:20SRG10AA134
        SPW_1(5205)=.false.	!L_(121) O
!end �������:20SRG10AA134
!beg �������:20SRG10AA135
        SPW_1(5155)=.false.	!L_(71) O
!end �������:20SRG10AA135
!beg �������:20SRG10AA136
        SPW_1(5149)=.false.	!L_(65) O
!end �������:20SRG10AA136
!beg �������:20SRG10AA137
        SPW_1(5147)=.false.	!L_(63) O
!end �������:20SRG10AA137
!beg �������:20SRG10AA138
        SPW_1(5145)=.false.	!L_(61) O
!end �������:20SRG10AA138
!beg �������:20SRG10AA139
        SPW_1(5131)=.false.	!L_(47) O
!end �������:20SRG10AA139
!beg �������:20SRG10AA140
        SPW_1(5129)=.false.	!L_(45) O
!end �������:20SRG10AA140
!beg �������:20SRG10AA141
        SPW_1(5127)=.false.	!L_(43) O
!end �������:20SRG10AA141
!beg �������:20SRG10AA142
        SPW_1(5125)=.false.	!L_(41) O
!end �������:20SRG10AA142
!beg �������:20SRG10AA143
        SPW_1(5123)=.false.	!L_(39) O
!end �������:20SRG10AA143
!beg �������:20SRG10AA144
        SPW_1(5121)=.false.	!L_(37) O
!end �������:20SRG10AA144
!beg �������:20SRG10AA145
        SPW_1(5119)=.false.	!L_(35) O
!end �������:20SRG10AA145
!beg �������:20SRG10AA146
        SPW_1(5117)=.false.	!L_(33) O
!end �������:20SRG10AA146
!beg �������:20SRG10AA147
        SPW_1(5115)=.false.	!L_(31) O
!end �������:20SRG10AA147
!beg �������:20SRG10AA148
        SPW_1(5113)=.false.	!L_(29) O
!end �������:20SRG10AA148
!beg �������:20SRG10AA100
        SPW_1(5206)=.false.	!L_(122) O
!end �������:20SRG10AA100
!beg �������:20SRG10AA101
        SPW_1(5208)=.false.	!L_(124) O
!end �������:20SRG10AA101
!beg �������:20SRG10AA102
        SPW_1(5202)=.false.	!L_(118) O
!end �������:20SRG10AA102
!beg �������:20SRG10AA103
        SPW_1(5200)=.false.	!L_(116) O
!end �������:20SRG10AA103
!beg �������:20SRG10AA104
        SPW_1(5198)=.false.	!L_(114) O
!end �������:20SRG10AA104
!beg �������:20SRG10AA105
        SPW_1(5196)=.false.	!L_(112) O
!end �������:20SRG10AA105
!beg �������:20SRG10AA106
        SPW_1(5194)=.false.	!L_(110) O
!end �������:20SRG10AA106
!beg �������:20SRG10AA107
        SPW_1(5192)=.false.	!L_(108) O
!end �������:20SRG10AA107
!beg �������:20SRG10AA108
        SPW_1(5190)=.false.	!L_(106) O
!end �������:20SRG10AA108
!beg �������:20SRG10AA109
        SPW_1(5188)=.false.	!L_(104) O
!end �������:20SRG10AA109
!beg �������:20SRG10AA171
        SPW_1(5411)=.false.	!L_(327) O
!end �������:20SRG10AA171
!beg �������:20SRG10AA172
        SPW_1(5251)=.false.	!L_(167) O
!end �������:20SRG10AA172
!beg �������:20SRG10AA173
        SPW_1(5343)=.false.	!L_(259) O
!end �������:20SRG10AA173
!beg �������:20SRG10AA110
        SPW_1(5186)=.false.	!L_(102) O
!end �������:20SRG10AA110
!beg �������:20SRG10AA111
        SPW_1(5184)=.false.	!L_(100) O
!end �������:20SRG10AA111
!beg �������:20SRG10AA112
        SPW_1(5182)=.false.	!L_(98) O
!end �������:20SRG10AA112
!beg �������:20SRG10AA113
        SPW_1(5180)=.false.	!L_(96) O
!end �������:20SRG10AA113
!beg �������:20SRG10AA114
        SPW_1(5178)=.false.	!L_(94) O
!end �������:20SRG10AA114
!beg �������:20SRG10AA115
        SPW_1(5176)=.false.	!L_(92) O
!end �������:20SRG10AA115
!beg �������:20SRG10AA116
        SPW_1(5174)=.false.	!L_(90) O
!end �������:20SRG10AA116
!beg �������:20SRG10AA117
        SPW_1(5172)=.false.	!L_(88) O
!end �������:20SRG10AA117
!beg �������:20SRG10AA118
        SPW_1(5170)=.false.	!L_(86) O
!end �������:20SRG10AA118
!beg �������:20SRG10AA119
        SPW_1(5168)=.false.	!L_(84) O
!end �������:20SRG10AA119
!beg �������:20SRG10AA120
        SPW_1(5166)=.false.	!L_(82) O
!end �������:20SRG10AA120
!beg �������:20SRG10AA121
        SPW_1(5164)=.false.	!L_(80) O
!end �������:20SRG10AA121
!beg �������:20SRG10AA122
        SPW_1(5162)=.false.	!L_(78) O
!end �������:20SRG10AA122
!beg �������:20SRG10AA123
        SPW_1(5160)=.false.	!L_(76) O
!end �������:20SRG10AA123
!beg �������:20SRG10AA124
        SPW_1(5158)=.false.	!L_(74) O
!end �������:20SRG10AA124
!beg �������:20SRG10AA125
        SPW_1(5156)=.false.	!L_(72) O
!end �������:20SRG10AA125
!beg �������:20SRG10AA126
        SPW_1(5152)=.false.	!L_(68) O
!end �������:20SRG10AA126
!beg �������:20SRG10AA127
        SPW_1(5150)=.false.	!L_(66) O
!end �������:20SRG10AA127
!beg �������:20SRG10AA128
        SPW_1(5142)=.false.	!L_(58) O
!end �������:20SRG10AA128
!beg �������:20SRG10AA129
        SPW_1(5140)=.false.	!L_(56) O
!end �������:20SRG10AA129
!beg �������:20SRG10AA130
        SPW_1(5138)=.false.	!L_(54) O
!end �������:20SRG10AA130
!beg �������:20SRG10AA131
        SPW_1(5136)=.false.	!L_(52) O
!end �������:20SRG10AA131
!beg �������:20SRG10AA132
        SPW_1(5134)=.false.	!L_(50) O
!end �������:20SRG10AA132
!beg �������:20SRG10AA133
        SPW_1(5132)=.false.	!L_(48) O
!end �������:20SRG10AA133
!beg �������:20SRG10AA134
        SPW_1(5204)=.false.	!L_(120) O
!end �������:20SRG10AA134
!beg �������:20SRG10AA135
        SPW_1(5154)=.false.	!L_(70) O
!end �������:20SRG10AA135
!beg �������:20SRG10AA136
        SPW_1(5148)=.false.	!L_(64) O
!end �������:20SRG10AA136
!beg �������:20SRG10AA137
        SPW_1(5146)=.false.	!L_(62) O
!end �������:20SRG10AA137
!beg �������:20SRG10AA138
        SPW_1(5144)=.false.	!L_(60) O
!end �������:20SRG10AA138
!beg �������:20SRG10AA139
        SPW_1(5130)=.false.	!L_(46) O
!end �������:20SRG10AA139
!beg �������:20SRG10AA140
        SPW_1(5128)=.false.	!L_(44) O
!end �������:20SRG10AA140
!beg �������:20SRG10AA141
        SPW_1(5126)=.false.	!L_(42) O
!end �������:20SRG10AA141
!beg �������:20SRG10AA142
        SPW_1(5124)=.false.	!L_(40) O
!end �������:20SRG10AA142
!beg �������:20SRG10AA143
        SPW_1(5122)=.false.	!L_(38) O
!end �������:20SRG10AA143
!beg �������:20SRG10AA144
        SPW_1(5120)=.false.	!L_(36) O
!end �������:20SRG10AA144
!beg �������:20SRG10AA145
        SPW_1(5118)=.false.	!L_(34) O
!end �������:20SRG10AA145
!beg �������:20SRG10AA146
        SPW_1(5116)=.false.	!L_(32) O
!end �������:20SRG10AA146
!beg �������:20SRG10AA147
        SPW_1(5114)=.false.	!L_(30) O
!end �������:20SRG10AA147
!beg �������:20SRG10AA148
        SPW_1(5112)=.false.	!L_(28) O
!end �������:20SRG10AA148
!beg �������:20SRG10AA171
        SPW_1(5410)=.false.	!L_(326) O
!end �������:20SRG10AA171
!beg �������:20SRG10AA172
        SPW_1(5250)=.false.	!L_(166) O
!end �������:20SRG10AA172
!beg �������:20SRG10AA173
        SPW_1(5342)=.false.	!L_(258) O
!end �������:20SRG10AA173
!beg �������:20SRG10AA241
        SPW_1(5421)=.false.	!L_(337) O
!end �������:20SRG10AA241
!beg �������:20SRG10AA242
        SPW_1(5419)=.false.	!L_(335) O
!end �������:20SRG10AA242
!beg �������:20SRG10AA243
        SPW_1(5417)=.false.	!L_(333) O
!end �������:20SRG10AA243
!beg �������:20SRG10AA244
        SPW_1(5415)=.false.	!L_(331) O
!end �������:20SRG10AA244
!beg �������:20SRG10AA245
        SPW_1(5413)=.false.	!L_(329) O
!end �������:20SRG10AA245
!beg �������:20SRG10AA246
        SPW_1(5379)=.false.	!L_(295) O
!end �������:20SRG10AA246
!beg �������:20SRG10AA247
        SPW_1(5377)=.false.	!L_(293) O
!end �������:20SRG10AA247
!beg �������:20SRG10AA248
        SPW_1(5375)=.false.	!L_(291) O
!end �������:20SRG10AA248
!beg �������:20SRG10AA249
        SPW_1(5373)=.false.	!L_(289) O
!end �������:20SRG10AA249
!beg �������:20SRG10AA250
        SPW_1(5371)=.false.	!L_(287) O
!end �������:20SRG10AA250
!beg �������:20SRG10AA251
        SPW_1(5369)=.false.	!L_(285) O
!end �������:20SRG10AA251
!beg �������:20SRG10AA252
        SPW_1(5367)=.false.	!L_(283) O
!end �������:20SRG10AA252
!beg �������:20SRG10AA253
        SPW_1(5365)=.false.	!L_(281) O
!end �������:20SRG10AA253
!beg �������:20SRG10AA254
        SPW_1(5363)=.false.	!L_(279) O
!end �������:20SRG10AA254
!beg �������:20SRG10AA255
        SPW_1(5361)=.false.	!L_(277) O
!end �������:20SRG10AA255
!beg �������:20SRG10AA256
        SPW_1(5359)=.false.	!L_(275) O
!end �������:20SRG10AA256
!beg �������:20SRG10AA257
        SPW_1(5357)=.false.	!L_(273) O
!end �������:20SRG10AA257
!beg �������:20SRG10AA258
        SPW_1(5355)=.false.	!L_(271) O
!end �������:20SRG10AA258
!beg �������:20SRG10AA259
        SPW_1(5353)=.false.	!L_(269) O
!end �������:20SRG10AA259
!beg �������:20SRG10AA260
        SPW_1(5351)=.false.	!L_(267) O
!end �������:20SRG10AA260
!beg �������:20SRG10AA261
        SPW_1(5409)=.false.	!L_(325) O
!end �������:20SRG10AA261
       end
