      Subroutine SRG(ext_deltat)
      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      Include 'SRG.fh'

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      Call SRG_ConIn
      R_(190)=R0_ukeru
C SRG_logic.fgi( 511, 364):pre: ������������  �� T
      R_(193)=R0_aperu
C SRG_logic.fgi( 508, 410):pre: ������������  �� T
      R_(194)=R0_abiru
C SRG_logic.fgi( 500, 467):pre: ������������  �� T
      R_(189)=R0_oferu
C SRG_logic.fgi( 487, 340):pre: ������������  �� T
      R_(188)=R0_ederu
C SRG_logic.fgi( 260, 312):pre: ������������  �� T
      R_(187)=R0_uvaru
C SRG_logic.fgi( 259, 282):pre: ������������  �� T
      R_(186)=R0_otaru
C SRG_logic.fgi( 260, 251):pre: ������������  �� T
      R_(185)=R0_esaru
C SRG_logic.fgi( 259, 237):pre: ������������  �� T
      R_(178)=R0_ovupu
C SRG_logic.fgi( 517, 279):pre: ������������  �� T
      R_(177)=R0_itupu
C SRG_logic.fgi( 516, 265):pre: ������������  �� T
      R_(180)=R0_edaru
C SRG_logic.fgi( 517, 312):pre: ������������  �� T
      R_(179)=R0_ixupu
C SRG_logic.fgi( 516, 298):pre: ������������  �� T
      R_(182)=R0_ikaru
C SRG_logic.fgi( 388, 279):pre: ������������  �� T
      R_(181)=R0_efaru
C SRG_logic.fgi( 387, 265):pre: ������������  �� T
      R_(184)=R0_aparu
C SRG_logic.fgi( 388, 312):pre: ������������  �� T
      R_(183)=R0_elaru
C SRG_logic.fgi( 387, 298):pre: ������������  �� T
      R_(200)=R0_uviru
C SRG_logic.fgi( 379, 467):pre: ������������  �� T
      R_(195)=R0_idiru
C SRG_logic.fgi( 366, 340):pre: ������������  �� T
      R_(196)=R0_ofiru
C SRG_logic.fgi( 390, 364):pre: ������������  �� T
      R_(199)=R0_uliru
C SRG_logic.fgi( 387, 410):pre: ������������  �� T
      R_(202)=R0_idoru
C SRG_logic.fgi( 271, 364):pre: ������������  �� T
      R_(201)=R0_eboru
C SRG_logic.fgi( 247, 340):pre: ������������  �� T
      R_(205)=R0_okoru
C SRG_logic.fgi( 268, 410):pre: ������������  �� T
      R_(206)=R0_otoru
C SRG_logic.fgi( 267, 467):pre: ������������  �� T
      R_(207)=R0_imotu
C SRG_logic.fgi(  53, 457):pre: ������������  �� T
      !{
      Call KLAPAN_MAN(deltat,REAL(R_osoki,4),R8_ipoki,I_uvoki
     &,C8_eroki,I_exoki,R_asoki,
     & R_uroki,R_iloki,REAL(R_uloki,4),R_omoki,
     & REAL(R_apoki,4),R_amoki,REAL(R_imoki,4),I_ivoki,
     & I_ixoki,I_axoki,I_evoki,L_epoki,L_uxoki,L_ubuki,L_umoki
     &,
     & L_emoki,REAL(R_iroki,4),L_upoki,L_opoki,L_ebuki,
     & L_oloki,L_oroki,L_iduki,L_esoki,L_isoki,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(258),L_okoki,L_(259),L_ukoki
     &,L_abuki,I_oxoki,L_aduki,
     & R_avoki,REAL(R_aroki,4),L_eduki,L_aloki,L_oduki,L_eloki
     &,L_usoki,L_atoki,
     & L_etoki,L_otoki,L_utoki,L_itoki)
      !}

      if(L_utoki.or.L_otoki.or.L_itoki.or.L_etoki.or.L_atoki.or.L_usoki
     &) then      
                  I_ovoki = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ovoki = z'40000000'
      endif
C SRG_vlv.fgi( 634, 629):���� ���������� ��������,20SRG10AA173
      !{
      Call KLAPAN_MAN(deltat,REAL(R_omase,4),R8_ikase,I_urase
     &,C8_elase,I_esase,R_amase,
     & R_ulase,R_idase,REAL(R_udase,4),R_ofase,
     & REAL(R_akase,4),R_afase,REAL(R_ifase,4),I_irase,
     & I_isase,I_asase,I_erase,L_ekase,L_usase,L_utase,L_ufase
     &,
     & L_efase,REAL(R_ilase,4),L_ukase,L_okase,L_etase,
     & L_odase,L_olase,L_ivase,L_emase,L_imase,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(166),L_obase,L_(167),L_ubase
     &,L_atase,I_osase,L_avase,
     & R_arase,REAL(R_alase,4),L_evase,L_adase,L_ovase,L_edase
     &,L_umase,L_apase,
     & L_epase,L_opase,L_upase,L_ipase)
      !}

      if(L_upase.or.L_opase.or.L_ipase.or.L_epase.or.L_apase.or.L_umase
     &) then      
                  I_orase = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_orase = z'40000000'
      endif
C SRG_vlv.fgi( 464, 330):���� ���������� ��������,20SRG10AA172
      !{
      Call KLAPAN_MAN(deltat,REAL(R_aboko,4),R8_utiko,I_efoko
     &,C8_oviko,I_ofoko,R_ixiko,
     & R_exiko,R_uriko,REAL(R_esiko,4),R_atiko,
     & REAL(R_itiko,4),R_isiko,REAL(R_usiko,4),I_udoko,
     & I_ufoko,I_ifoko,I_odoko,L_otiko,L_ekoko,L_eloko,L_etiko
     &,
     & L_osiko,REAL(R_uviko,4),L_eviko,L_aviko,L_okoko,
     & L_asiko,L_axiko,L_uloko,L_oxiko,L_uxiko,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(326),L_ariko,L_(327),L_eriko
     &,L_ikoko,I_akoko,L_iloko,
     & R_idoko,REAL(R_iviko,4),L_oloko,L_iriko,L_amoko,L_oriko
     &,L_eboko,L_iboko,
     & L_oboko,L_adoko,L_edoko,L_uboko)
      !}

      if(L_edoko.or.L_adoko.or.L_uboko.or.L_oboko.or.L_iboko.or.L_eboko
     &) then      
                  I_afoko = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_afoko = z'40000000'
      endif
C SRG_vlv.fgi( 435, 605):���� ���������� ��������,20SRG10AA171
      !{
      Call KLAPAN_MAN(deltat,REAL(R_obam,4),R8_ivul,I_ufam
     &,C8_exul,I_ekam,R_abam,
     & R_uxul,R_isul,REAL(R_usul,4),R_otul,
     & REAL(R_avul,4),R_atul,REAL(R_itul,4),I_ifam,
     & I_ikam,I_akam,I_efam,L_evul,L_ukam,L_ulam,L_utul,
     & L_etul,REAL(R_ixul,4),L_uvul,L_ovul,L_elam,
     & L_osul,L_oxul,L_imam,L_ebam,L_ibam,REAL(R8_elomu,8
     &),
     & REAL(1.0,4),R8_aromu,L_(28),L_orul,L_(29),L_urul,L_alam
     &,I_okam,L_amam,
     & R_afam,REAL(R_axul,4),L_emam,L_asul,L_omam,L_esul,L_ubam
     &,L_adam,
     & L_edam,L_odam,L_udam,L_idam)
      !}

      if(L_udam.or.L_odam.or.L_idam.or.L_edam.or.L_adam.or.L_ubam
     &) then      
                  I_ofam = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_ofam = z'40000000'
      endif
C SRG_vlv.fgi( 255, 698):���� ���������� ��������,20SRG10AA148
      !{
      Call KLAPAN_MAN(deltat,REAL(R_uvam,4),R8_osam,I_adem
     &,C8_itam,I_idem,R_evam,
     & R_avam,R_opam,REAL(R_aram,4),R_uram,
     & REAL(R_esam,4),R_eram,REAL(R_oram,4),I_obem,
     & I_odem,I_edem,I_ibem,L_isam,L_afem,L_akem,L_asam,
     & L_iram,REAL(R_otam,4),L_atam,L_usam,L_ifem,
     & L_upam,L_utam,L_okem,L_ivam,L_ovam,REAL(R8_elomu,8
     &),
     & REAL(1.0,4),R8_aromu,L_(30),L_umam,L_(31),L_apam,L_efem
     &,I_udem,L_ekem,
     & R_ebem,REAL(R_etam,4),L_ikem,L_epam,L_ukem,L_ipam,L_axam
     &,L_exam,
     & L_ixam,L_uxam,L_abem,L_oxam)
      !}

      if(L_abem.or.L_uxam.or.L_oxam.or.L_ixam.or.L_exam.or.L_axam
     &) then      
                  I_ubem = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_ubem = z'40000000'
      endif
C SRG_vlv.fgi( 241, 698):���� ���������� ��������,20SRG10AA147
      !{
      Call KLAPAN_MAN(deltat,REAL(R_atem,4),R8_upem,I_exem
     &,C8_orem,I_oxem,R_isem,
     & R_esem,R_ulem,REAL(R_emem,4),R_apem,
     & REAL(R_ipem,4),R_imem,REAL(R_umem,4),I_uvem,
     & I_uxem,I_ixem,I_ovem,L_opem,L_ebim,L_edim,L_epem,
     & L_omem,REAL(R_urem,4),L_erem,L_arem,L_obim,
     & L_amem,L_asem,L_udim,L_osem,L_usem,REAL(R8_elomu,8
     &),
     & REAL(1.0,4),R8_aromu,L_(32),L_alem,L_(33),L_elem,L_ibim
     &,I_abim,L_idim,
     & R_ivem,REAL(R_irem,4),L_odim,L_ilem,L_afim,L_olem,L_etem
     &,L_item,
     & L_otem,L_avem,L_evem,L_utem)
      !}

      if(L_evem.or.L_avem.or.L_utem.or.L_otem.or.L_item.or.L_etem
     &) then      
                  I_axem = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_axem = z'40000000'
      endif
C SRG_vlv.fgi( 227, 698):���� ���������� ��������,20SRG10AA146
      !{
      Call KLAPAN_MAN(deltat,REAL(R_erim,4),R8_amim,I_itim
     &,C8_umim,I_utim,R_opim,
     & R_ipim,R_akim,REAL(R_ikim,4),R_elim,
     & REAL(R_olim,4),R_okim,REAL(R_alim,4),I_atim,
     & I_avim,I_otim,I_usim,L_ulim,L_ivim,L_ixim,L_ilim,
     & L_ukim,REAL(R_apim,4),L_imim,L_emim,L_uvim,
     & L_ekim,L_epim,L_abom,L_upim,L_arim,REAL(R8_elomu,8
     &),
     & REAL(1.0,4),R8_aromu,L_(34),L_efim,L_(35),L_ifim,L_ovim
     &,I_evim,L_oxim,
     & R_osim,REAL(R_omim,4),L_uxim,L_ofim,L_ebom,L_ufim,L_irim
     &,L_orim,
     & L_urim,L_esim,L_isim,L_asim)
      !}

      if(L_isim.or.L_esim.or.L_asim.or.L_urim.or.L_orim.or.L_irim
     &) then      
                  I_etim = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_etim = z'40000000'
      endif
C SRG_vlv.fgi( 535, 723):���� ���������� ��������,20SRG10AA145
      !{
      Call KLAPAN_MAN(deltat,REAL(R_imom,4),R8_ekom,I_orom
     &,C8_alom,I_asom,R_ulom,
     & R_olom,R_edom,REAL(R_odom,4),R_ifom,
     & REAL(R_ufom,4),R_udom,REAL(R_efom,4),I_erom,
     & I_esom,I_urom,I_arom,L_akom,L_osom,L_otom,L_ofom,
     & L_afom,REAL(R_elom,4),L_okom,L_ikom,L_atom,
     & L_idom,L_ilom,L_evom,L_amom,L_emom,REAL(R8_elomu,8
     &),
     & REAL(1.0,4),R8_aromu,L_(36),L_ibom,L_(37),L_obom,L_usom
     &,I_isom,L_utom,
     & R_upom,REAL(R_ukom,4),L_avom,L_ubom,L_ivom,L_adom,L_omom
     &,L_umom,
     & L_apom,L_ipom,L_opom,L_epom)
      !}

      if(L_opom.or.L_ipom.or.L_epom.or.L_apom.or.L_umom.or.L_omom
     &) then      
                  I_irom = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_irom = z'40000000'
      endif
C SRG_vlv.fgi( 521, 723):���� ���������� ��������,20SRG10AA144
      !{
      Call KLAPAN_MAN(deltat,REAL(R_okum,4),R8_idum,I_umum
     &,C8_efum,I_epum,R_akum,
     & R_ufum,R_ixom,REAL(R_uxom,4),R_obum,
     & REAL(R_adum,4),R_abum,REAL(R_ibum,4),I_imum,
     & I_ipum,I_apum,I_emum,L_edum,L_upum,L_urum,L_ubum,
     & L_ebum,REAL(R_ifum,4),L_udum,L_odum,L_erum,
     & L_oxom,L_ofum,L_isum,L_ekum,L_ikum,REAL(R8_elomu,8
     &),
     & REAL(1.0,4),R8_aromu,L_(38),L_ovom,L_(39),L_uvom,L_arum
     &,I_opum,L_asum,
     & R_amum,REAL(R_afum,4),L_esum,L_axom,L_osum,L_exom,L_ukum
     &,L_alum,
     & L_elum,L_olum,L_ulum,L_ilum)
      !}

      if(L_ulum.or.L_olum.or.L_ilum.or.L_elum.or.L_alum.or.L_ukum
     &) then      
                  I_omum = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_omum = z'40000000'
      endif
C SRG_vlv.fgi( 507, 723):���� ���������� ��������,20SRG10AA143
      !{
      Call KLAPAN_MAN(deltat,REAL(R_udap,4),R8_oxum,I_alap
     &,C8_ibap,I_ilap,R_edap,
     & R_adap,R_otum,REAL(R_avum,4),R_uvum,
     & REAL(R_exum,4),R_evum,REAL(R_ovum,4),I_okap,
     & I_olap,I_elap,I_ikap,L_ixum,L_amap,L_apap,L_axum,
     & L_ivum,REAL(R_obap,4),L_abap,L_uxum,L_imap,
     & L_utum,L_ubap,L_opap,L_idap,L_odap,REAL(R8_elomu,8
     &),
     & REAL(1.0,4),R8_aromu,L_(40),L_usum,L_(41),L_atum,L_emap
     &,I_ulap,L_epap,
     & R_ekap,REAL(R_ebap,4),L_ipap,L_etum,L_upap,L_itum,L_afap
     &,L_efap,
     & L_ifap,L_ufap,L_akap,L_ofap)
      !}

      if(L_akap.or.L_ufap.or.L_ofap.or.L_ifap.or.L_efap.or.L_afap
     &) then      
                  I_ukap = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_ukap = z'40000000'
      endif
C SRG_vlv.fgi( 493, 723):���� ���������� ��������,20SRG10AA142
      !{
      Call KLAPAN_MAN(deltat,REAL(R_abep,4),R8_utap,I_efep
     &,C8_ovap,I_ofep,R_ixap,
     & R_exap,R_urap,REAL(R_esap,4),R_atap,
     & REAL(R_itap,4),R_isap,REAL(R_usap,4),I_udep,
     & I_ufep,I_ifep,I_odep,L_otap,L_ekep,L_elep,L_etap,
     & L_osap,REAL(R_uvap,4),L_evap,L_avap,L_okep,
     & L_asap,L_axap,L_ulep,L_oxap,L_uxap,REAL(R8_elomu,8
     &),
     & REAL(1.0,4),R8_aromu,L_(42),L_arap,L_(43),L_erap,L_ikep
     &,I_akep,L_ilep,
     & R_idep,REAL(R_ivap,4),L_olep,L_irap,L_amep,L_orap,L_ebep
     &,L_ibep,
     & L_obep,L_adep,L_edep,L_ubep)
      !}

      if(L_edep.or.L_adep.or.L_ubep.or.L_obep.or.L_ibep.or.L_ebep
     &) then      
                  I_afep = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_afep = z'40000000'
      endif
C SRG_vlv.fgi( 479, 723):���� ���������� ��������,20SRG10AA141
      !{
      Call KLAPAN_MAN(deltat,REAL(R_evep,4),R8_asep,I_ibip
     &,C8_usep,I_ubip,R_otep,
     & R_itep,R_apep,REAL(R_ipep,4),R_erep,
     & REAL(R_orep,4),R_opep,REAL(R_arep,4),I_abip,
     & I_adip,I_obip,I_uxep,L_urep,L_idip,L_ifip,L_irep,
     & L_upep,REAL(R_atep,4),L_isep,L_esep,L_udip,
     & L_epep,L_etep,L_akip,L_utep,L_avep,REAL(R8_elomu,8
     &),
     & REAL(1.0,4),R8_aromu,L_(44),L_emep,L_(45),L_imep,L_odip
     &,I_edip,L_ofip,
     & R_oxep,REAL(R_osep,4),L_ufip,L_omep,L_ekip,L_umep,L_ivep
     &,L_ovep,
     & L_uvep,L_exep,L_ixep,L_axep)
      !}

      if(L_ixep.or.L_exep.or.L_axep.or.L_uvep.or.L_ovep.or.L_ivep
     &) then      
                  I_ebip = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_ebip = z'40000000'
      endif
C SRG_vlv.fgi( 465, 723):���� ���������� ��������,20SRG10AA140
      !{
      Call KLAPAN_MAN(deltat,REAL(R_isip,4),R8_epip,I_ovip
     &,C8_arip,I_axip,R_urip,
     & R_orip,R_elip,REAL(R_olip,4),R_imip,
     & REAL(R_umip,4),R_ulip,REAL(R_emip,4),I_evip,
     & I_exip,I_uvip,I_avip,L_apip,L_oxip,L_obop,L_omip,
     & L_amip,REAL(R_erip,4),L_opip,L_ipip,L_abop,
     & L_ilip,L_irip,L_edop,L_asip,L_esip,REAL(R8_elomu,8
     &),
     & REAL(1.0,4),R8_aromu,L_(46),L_ikip,L_(47),L_okip,L_uxip
     &,I_ixip,L_ubop,
     & R_utip,REAL(R_upip,4),L_adop,L_ukip,L_idop,L_alip,L_osip
     &,L_usip,
     & L_atip,L_itip,L_otip,L_etip)
      !}

      if(L_otip.or.L_itip.or.L_etip.or.L_atip.or.L_usip.or.L_osip
     &) then      
                  I_ivip = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_ivip = z'40000000'
      endif
C SRG_vlv.fgi( 451, 723):���� ���������� ��������,20SRG10AA139
      !{
      Call KLAPAN_MAN(deltat,REAL(R_elas,4),R8_afas,I_ipas
     &,C8_ufas,I_upas,R_okas,
     & R_ikas,R_abas,REAL(R_ibas,4),R_edas,
     & REAL(R_odas,4),R_obas,REAL(R_adas,4),I_apas,
     & I_aras,I_opas,I_umas,L_udas,L_iras,L_isas,L_idas,
     & L_ubas,REAL(R_akas,4),L_ifas,L_efas,L_uras,
     & L_ebas,L_ekas,L_atas,L_ukas,L_alas,REAL(R8_elomu,8
     &),
     & REAL(1.0,4),R8_aromu,L_(60),L_exur,L_(61),L_ixur,L_oras
     &,I_eras,L_osas,
     & R_omas,REAL(R_ofas,4),L_usas,L_oxur,L_etas,L_uxur,L_ilas
     &,L_olas,
     & L_ulas,L_emas,L_imas,L_amas)
      !}

      if(L_imas.or.L_emas.or.L_amas.or.L_ulas.or.L_olas.or.L_ilas
     &) then      
                  I_epas = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_epas = z'40000000'
      endif
C SRG_vlv.fgi( 437, 723):���� ���������� ��������,20SRG10AA138
      !{
      Call KLAPAN_MAN(deltat,REAL(R_ifes,4),R8_ebes,I_oles
     &,C8_ades,I_ames,R_udes,
     & R_odes,R_evas,REAL(R_ovas,4),R_ixas,
     & REAL(R_uxas,4),R_uvas,REAL(R_exas,4),I_eles,
     & I_emes,I_ules,I_ales,L_abes,L_omes,L_opes,L_oxas,
     & L_axas,REAL(R_edes,4),L_obes,L_ibes,L_apes,
     & L_ivas,L_ides,L_eres,L_afes,L_efes,REAL(R8_elomu,8
     &),
     & REAL(1.0,4),R8_aromu,L_(62),L_itas,L_(63),L_otas,L_umes
     &,I_imes,L_upes,
     & R_ukes,REAL(R_ubes,4),L_ares,L_utas,L_ires,L_avas,L_ofes
     &,L_ufes,
     & L_akes,L_ikes,L_okes,L_ekes)
      !}

      if(L_okes.or.L_ikes.or.L_ekes.or.L_akes.or.L_ufes.or.L_ofes
     &) then      
                  I_iles = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_iles = z'40000000'
      endif
C SRG_vlv.fgi( 423, 723):���� ���������� ��������,20SRG10AA137
      !{
      Call KLAPAN_MAN(deltat,REAL(R_uvis,4),R8_osis,I_ados
     &,C8_itis,I_idos,R_evis,
     & R_avis,R_opis,REAL(R_aris,4),R_uris,
     & REAL(R_esis,4),R_eris,REAL(R_oris,4),I_obos,
     & I_odos,I_edos,I_ibos,L_isis,L_afos,L_akos,L_asis,
     & L_iris,REAL(R_otis,4),L_atis,L_usis,L_ifos,
     & L_upis,L_utis,L_okos,L_ivis,L_ovis,REAL(R8_elomu,8
     &),
     & REAL(1.0,4),R8_aromu,L_(64),L_umis,L_(65),L_apis,L_efos
     &,I_udos,L_ekos,
     & R_ebos,REAL(R_etis,4),L_ikos,L_epis,L_ukos,L_ipis,L_axis
     &,L_exis,
     & L_ixis,L_uxis,L_abos,L_oxis)
      !}

      if(L_abos.or.L_uxis.or.L_oxis.or.L_ixis.or.L_exis.or.L_axis
     &) then      
                  I_ubos = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_ubos = z'40000000'
      endif
C SRG_vlv.fgi( 409, 723):���� ���������� ��������,20SRG10AA136
      !{
      Call KLAPAN_MAN(deltat,REAL(R_ulat,4),R8_ofat,I_arat
     &,C8_ikat,I_irat,R_elat,
     & R_alat,R_obat,REAL(R_adat,4),R_udat,
     & REAL(R_efat,4),R_edat,REAL(R_odat,4),I_opat,
     & I_orat,I_erat,I_ipat,L_ifat,L_asat,L_atat,L_afat,
     & L_idat,REAL(R_okat,4),L_akat,L_ufat,L_isat,
     & L_ubat,L_ukat,L_otat,L_ilat,L_olat,REAL(R8_elomu,8
     &),
     & REAL(1.0,4),R8_aromu,L_(70),L_uxus,L_(71),L_abat,L_esat
     &,I_urat,L_etat,
     & R_epat,REAL(R_ekat,4),L_itat,L_ebat,L_utat,L_ibat,L_amat
     &,L_emat,
     & L_imat,L_umat,L_apat,L_omat)
      !}

      if(L_apat.or.L_umat.or.L_omat.or.L_imat.or.L_emat.or.L_amat
     &) then      
                  I_upat = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_upat = z'40000000'
      endif
C SRG_vlv.fgi( 395, 723):���� ���������� ��������,20SRG10AA135
      !{
      Call KLAPAN_MAN(deltat,REAL(R_obede,4),R8_ivade,I_ufede
     &,C8_exade,I_ekede,R_abede,
     & R_uxade,R_isade,REAL(R_usade,4),R_otade,
     & REAL(R_avade,4),R_atade,REAL(R_itade,4),I_ifede,
     & I_ikede,I_akede,I_efede,L_evade,L_ukede,L_ulede,L_utade
     &,
     & L_etade,REAL(R_ixade,4),L_uvade,L_ovade,L_elede,
     & L_osade,L_oxade,L_imede,L_ebede,L_ibede,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(120),L_orade,L_(121),L_urade
     &,L_alede,I_okede,L_amede,
     & R_afede,REAL(R_axade,4),L_emede,L_asade,L_omede,L_esade
     &,L_ubede,L_adede,
     & L_edede,L_odede,L_udede,L_idede)
      !}

      if(L_udede.or.L_odede.or.L_idede.or.L_edede.or.L_adede.or.L_ubede
     &) then      
                  I_ofede = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ofede = z'40000000'
      endif
C SRG_vlv.fgi( 381, 723):���� ���������� ��������,20SRG10AA134
      !{
      Call KLAPAN_MAN(deltat,REAL(R_opop,4),R8_ilop,I_usop
     &,C8_emop,I_etop,R_apop,
     & R_umop,R_ifop,REAL(R_ufop,4),R_okop,
     & REAL(R_alop,4),R_akop,REAL(R_ikop,4),I_isop,
     & I_itop,I_atop,I_esop,L_elop,L_utop,L_uvop,L_ukop,
     & L_ekop,REAL(R_imop,4),L_ulop,L_olop,L_evop,
     & L_ofop,L_omop,L_ixop,L_epop,L_ipop,REAL(R8_elomu,8
     &),
     & REAL(1.0,4),R8_aromu,L_(48),L_odop,L_(49),L_udop,L_avop
     &,I_otop,L_axop,
     & R_asop,REAL(R_amop,4),L_exop,L_afop,L_oxop,L_efop,L_upop
     &,L_arop,
     & L_erop,L_orop,L_urop,L_irop)
      !}

      if(L_urop.or.L_orop.or.L_irop.or.L_erop.or.L_arop.or.L_upop
     &) then      
                  I_osop = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_osop = z'40000000'
      endif
C SRG_vlv.fgi( 367, 723):���� ���������� ��������,20SRG10AA133
      !{
      Call KLAPAN_MAN(deltat,REAL(R_ulup,4),R8_ofup,I_arup
     &,C8_ikup,I_irup,R_elup,
     & R_alup,R_obup,REAL(R_adup,4),R_udup,
     & REAL(R_efup,4),R_edup,REAL(R_odup,4),I_opup,
     & I_orup,I_erup,I_ipup,L_ifup,L_asup,L_atup,L_afup,
     & L_idup,REAL(R_okup,4),L_akup,L_ufup,L_isup,
     & L_ubup,L_ukup,L_otup,L_ilup,L_olup,REAL(R8_elomu,8
     &),
     & REAL(1.0,4),R8_aromu,L_(50),L_uxop,L_(51),L_abup,L_esup
     &,I_urup,L_etup,
     & R_epup,REAL(R_ekup,4),L_itup,L_ebup,L_utup,L_ibup,L_amup
     &,L_emup,
     & L_imup,L_umup,L_apup,L_omup)
      !}

      if(L_apup.or.L_umup.or.L_omup.or.L_imup.or.L_emup.or.L_amup
     &) then      
                  I_upup = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_upup = z'40000000'
      endif
C SRG_vlv.fgi( 353, 723):���� ���������� ��������,20SRG10AA132
      !{
      Call KLAPAN_MAN(deltat,REAL(R_eder,4),R8_axar,I_iker
     &,C8_uxar,I_uker,R_ober,
     & R_iber,R_atar,REAL(R_itar,4),R_evar,
     & REAL(R_ovar,4),R_otar,REAL(R_avar,4),I_aker,
     & I_aler,I_oker,I_ufer,L_uvar,L_iler,L_imer,L_ivar,
     & L_utar,REAL(R_aber,4),L_ixar,L_exar,L_uler,
     & L_etar,L_eber,L_aper,L_uber,L_ader,REAL(R8_elomu,8
     &),
     & REAL(1.0,4),R8_aromu,L_(52),L_esar,L_(53),L_isar,L_oler
     &,I_eler,L_omer,
     & R_ofer,REAL(R_oxar,4),L_umer,L_osar,L_eper,L_usar,L_ider
     &,L_oder,
     & L_uder,L_efer,L_ifer,L_afer)
      !}

      if(L_ifer.or.L_efer.or.L_afer.or.L_uder.or.L_oder.or.L_ider
     &) then      
                  I_eker = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_eker = z'40000000'
      endif
C SRG_vlv.fgi( 339, 723):���� ���������� ��������,20SRG10AA131
      !{
      Call KLAPAN_MAN(deltat,REAL(R_ixer,4),R8_eter,I_odir
     &,C8_aver,I_afir,R_uver,
     & R_over,R_erer,REAL(R_orer,4),R_iser,
     & REAL(R_user,4),R_urer,REAL(R_eser,4),I_edir,
     & I_efir,I_udir,I_adir,L_ater,L_ofir,L_okir,L_oser,
     & L_aser,REAL(R_ever,4),L_oter,L_iter,L_akir,
     & L_irer,L_iver,L_elir,L_axer,L_exer,REAL(R8_elomu,8
     &),
     & REAL(1.0,4),R8_aromu,L_(54),L_iper,L_(55),L_oper,L_ufir
     &,I_ifir,L_ukir,
     & R_ubir,REAL(R_uter,4),L_alir,L_uper,L_ilir,L_arer,L_oxer
     &,L_uxer,
     & L_abir,L_ibir,L_obir,L_ebir)
      !}

      if(L_obir.or.L_ibir.or.L_ebir.or.L_abir.or.L_uxer.or.L_oxer
     &) then      
                  I_idir = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_idir = z'40000000'
      endif
C SRG_vlv.fgi( 325, 723):���� ���������� ��������,20SRG10AA130
      !{
      Call KLAPAN_MAN(deltat,REAL(R_uror,4),R8_omor,I_avor
     &,C8_ipor,I_ivor,R_eror,
     & R_aror,R_okor,REAL(R_alor,4),R_ulor,
     & REAL(R_emor,4),R_elor,REAL(R_olor,4),I_otor,
     & I_ovor,I_evor,I_itor,L_imor,L_axor,L_abur,L_amor,
     & L_ilor,REAL(R_opor,4),L_apor,L_umor,L_ixor,
     & L_ukor,L_upor,L_obur,L_iror,L_oror,REAL(R8_elomu,8
     &),
     & REAL(1.0,4),R8_aromu,L_(56),L_ufor,L_(57),L_akor,L_exor
     &,I_uvor,L_ebur,
     & R_etor,REAL(R_epor,4),L_ibur,L_ekor,L_ubur,L_ikor,L_asor
     &,L_esor,
     & L_isor,L_usor,L_ator,L_osor)
      !}

      if(L_ator.or.L_usor.or.L_osor.or.L_isor.or.L_esor.or.L_asor
     &) then      
                  I_utor = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_utor = z'40000000'
      endif
C SRG_vlv.fgi( 311, 723):���� ���������� ��������,20SRG10AA129
      !{
      Call KLAPAN_MAN(deltat,REAL(R_apur,4),R8_ukur,I_esur
     &,C8_olur,I_osur,R_imur,
     & R_emur,R_udur,REAL(R_efur,4),R_akur,
     & REAL(R_ikur,4),R_ifur,REAL(R_ufur,4),I_urur,
     & I_usur,I_isur,I_orur,L_okur,L_etur,L_evur,L_ekur,
     & L_ofur,REAL(R_ulur,4),L_elur,L_alur,L_otur,
     & L_afur,L_amur,L_uvur,L_omur,L_umur,REAL(R8_elomu,8
     &),
     & REAL(1.0,4),R8_aromu,L_(58),L_adur,L_(59),L_edur,L_itur
     &,I_atur,L_ivur,
     & R_irur,REAL(R_ilur,4),L_ovur,L_idur,L_axur,L_odur,L_epur
     &,L_ipur,
     & L_opur,L_arur,L_erur,L_upur)
      !}

      if(L_erur.or.L_arur.or.L_upur.or.L_opur.or.L_ipur.or.L_epur
     &) then      
                  I_asur = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_asur = z'40000000'
      endif
C SRG_vlv.fgi( 297, 723):���� ���������� ��������,20SRG10AA128
      !{
      Call KLAPAN_MAN(deltat,REAL(R_ikafi,4),R8_edafi,I_omafi
     &,C8_afafi,I_apafi,R_ufafi,
     & R_ofafi,R_exudi,REAL(R_oxudi,4),R_ibafi,
     & REAL(R_ubafi,4),R_uxudi,REAL(R_ebafi,4),I_emafi,
     & I_epafi,I_umafi,I_amafi,L_adafi,L_opafi,L_orafi,L_obafi
     &,
     & L_abafi,REAL(R_efafi,4),L_odafi,L_idafi,L_arafi,
     & L_ixudi,L_ifafi,L_esafi,L_akafi,L_ekafi,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(238),L_ivudi,L_(239),L_ovudi
     &,L_upafi,I_ipafi,L_urafi,
     & R_ulafi,REAL(R_udafi,4),L_asafi,L_uvudi,L_isafi,L_axudi
     &,L_okafi,L_ukafi,
     & L_alafi,L_ilafi,L_olafi,L_elafi)
      !}

      if(L_olafi.or.L_ilafi.or.L_elafi.or.L_alafi.or.L_ukafi.or.L_okafi
     &) then      
                  I_imafi = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_imafi = z'40000000'
      endif
C SRG_vlv.fgi( 690, 653):���� ���������� ��������,20SRG10AA343
      !{
      Call KLAPAN_MAN(deltat,REAL(R_ivixe,4),R8_esixe,I_oboxe
     &,C8_atixe,I_adoxe,R_utixe,
     & R_otixe,R_epixe,REAL(R_opixe,4),R_irixe,
     & REAL(R_urixe,4),R_upixe,REAL(R_erixe,4),I_eboxe,
     & I_edoxe,I_uboxe,I_aboxe,L_asixe,L_odoxe,L_ofoxe,L_orixe
     &,
     & L_arixe,REAL(R_etixe,4),L_osixe,L_isixe,L_afoxe,
     & L_ipixe,L_itixe,L_ekoxe,L_avixe,L_evixe,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(208),L_imixe,L_(209),L_omixe
     &,L_udoxe,I_idoxe,L_ufoxe,
     & R_uxixe,REAL(R_usixe,4),L_akoxe,L_umixe,L_ikoxe,L_apixe
     &,L_ovixe,L_uvixe,
     & L_axixe,L_ixixe,L_oxixe,L_exixe)
      !}

      if(L_oxixe.or.L_ixixe.or.L_exixe.or.L_axixe.or.L_uvixe.or.L_ovixe
     &) then      
                  I_iboxe = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_iboxe = z'40000000'
      endif
C SRG_vlv.fgi( 676, 581):���� ���������� ��������,20SRG10AA342
      !{
      Call KLAPAN_MAN(deltat,REAL(R_otidi,4),R8_iridi,I_uxidi
     &,C8_esidi,I_ebodi,R_atidi,
     & R_usidi,R_imidi,REAL(R_umidi,4),R_opidi,
     & REAL(R_aridi,4),R_apidi,REAL(R_ipidi,4),I_ixidi,
     & I_ibodi,I_abodi,I_exidi,L_eridi,L_ubodi,L_udodi,L_upidi
     &,
     & L_epidi,REAL(R_isidi,4),L_uridi,L_oridi,L_edodi,
     & L_omidi,L_osidi,L_ifodi,L_etidi,L_itidi,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(232),L_olidi,L_(233),L_ulidi
     &,L_adodi,I_obodi,L_afodi,
     & R_axidi,REAL(R_asidi,4),L_efodi,L_amidi,L_ofodi,L_emidi
     &,L_utidi,L_avidi,
     & L_evidi,L_ovidi,L_uvidi,L_ividi)
      !}

      if(L_uvidi.or.L_ovidi.or.L_ividi.or.L_evidi.or.L_avidi.or.L_utidi
     &) then      
                  I_oxidi = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_oxidi = z'40000000'
      endif
C SRG_vlv.fgi( 676, 605):���� ���������� ��������,20SRG10AA341
      !{
      Call KLAPAN_MAN(deltat,REAL(R_emudi,4),R8_akudi,I_irudi
     &,C8_ukudi,I_urudi,R_oludi,
     & R_iludi,R_adudi,REAL(R_idudi,4),R_efudi,
     & REAL(R_ofudi,4),R_odudi,REAL(R_afudi,4),I_arudi,
     & I_asudi,I_orudi,I_upudi,L_ufudi,L_isudi,L_itudi,L_ifudi
     &,
     & L_ududi,REAL(R_aludi,4),L_ikudi,L_ekudi,L_usudi,
     & L_edudi,L_eludi,L_avudi,L_uludi,L_amudi,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(236),L_ebudi,L_(237),L_ibudi
     &,L_osudi,I_esudi,L_otudi,
     & R_opudi,REAL(R_okudi,4),L_utudi,L_obudi,L_evudi,L_ubudi
     &,L_imudi,L_omudi,
     & L_umudi,L_epudi,L_ipudi,L_apudi)
      !}

      if(L_ipudi.or.L_epudi.or.L_apudi.or.L_umudi.or.L_omudi.or.L_imudi
     &) then      
                  I_erudi = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_erudi = z'40000000'
      endif
C SRG_vlv.fgi( 676, 629):���� ���������� ��������,20SRG10AA340
      !{
      Call KLAPAN_MAN(deltat,REAL(R_odefi,4),R8_ixafi,I_ukefi
     &,C8_ebefi,I_elefi,R_adefi,
     & R_ubefi,R_itafi,REAL(R_utafi,4),R_ovafi,
     & REAL(R_axafi,4),R_avafi,REAL(R_ivafi,4),I_ikefi,
     & I_ilefi,I_alefi,I_ekefi,L_exafi,L_ulefi,L_umefi,L_uvafi
     &,
     & L_evafi,REAL(R_ibefi,4),L_uxafi,L_oxafi,L_emefi,
     & L_otafi,L_obefi,L_ipefi,L_edefi,L_idefi,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(240),L_osafi,L_(241),L_usafi
     &,L_amefi,I_olefi,L_apefi,
     & R_akefi,REAL(R_abefi,4),L_epefi,L_atafi,L_opefi,L_etafi
     &,L_udefi,L_afefi,
     & L_efefi,L_ofefi,L_ufefi,L_ifefi)
      !}

      if(L_ufefi.or.L_ofefi.or.L_ifefi.or.L_efefi.or.L_afefi.or.L_udefi
     &) then      
                  I_okefi = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_okefi = z'40000000'
      endif
C SRG_vlv.fgi( 676, 653):���� ���������� ��������,20SRG10AA339
      !{
      Call KLAPAN_MAN(deltat,REAL(R_osoxe,4),R8_ipoxe,I_uvoxe
     &,C8_eroxe,I_exoxe,R_asoxe,
     & R_uroxe,R_iloxe,REAL(R_uloxe,4),R_omoxe,
     & REAL(R_apoxe,4),R_amoxe,REAL(R_imoxe,4),I_ivoxe,
     & I_ixoxe,I_axoxe,I_evoxe,L_epoxe,L_uxoxe,L_ubuxe,L_umoxe
     &,
     & L_emoxe,REAL(R_iroxe,4),L_upoxe,L_opoxe,L_ebuxe,
     & L_oloxe,L_oroxe,L_iduxe,L_esoxe,L_isoxe,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(210),L_okoxe,L_(211),L_ukoxe
     &,L_abuxe,I_oxoxe,L_aduxe,
     & R_avoxe,REAL(R_aroxe,4),L_eduxe,L_aloxe,L_oduxe,L_eloxe
     &,L_usoxe,L_atoxe,
     & L_etoxe,L_otoxe,L_utoxe,L_itoxe)
      !}

      if(L_utoxe.or.L_otoxe.or.L_itoxe.or.L_etoxe.or.L_atoxe.or.L_usoxe
     &) then      
                  I_ovoxe = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ovoxe = z'40000000'
      endif
C SRG_vlv.fgi( 662, 581):���� ���������� ��������,20SRG10AA338
      !{
      Call KLAPAN_MAN(deltat,REAL(R_uxefi,4),R8_otefi,I_afifi
     &,C8_ivefi,I_ififi,R_exefi,
     & R_axefi,R_orefi,REAL(R_asefi,4),R_usefi,
     & REAL(R_etefi,4),R_esefi,REAL(R_osefi,4),I_odifi,
     & I_ofifi,I_efifi,I_idifi,L_itefi,L_akifi,L_alifi,L_atefi
     &,
     & L_isefi,REAL(R_ovefi,4),L_avefi,L_utefi,L_ikifi,
     & L_urefi,L_uvefi,L_olifi,L_ixefi,L_oxefi,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(242),L_upefi,L_(243),L_arefi
     &,L_ekifi,I_ufifi,L_elifi,
     & R_edifi,REAL(R_evefi,4),L_ilifi,L_erefi,L_ulifi,L_irefi
     &,L_abifi,L_ebifi,
     & L_ibifi,L_ubifi,L_adifi,L_obifi)
      !}

      if(L_adifi.or.L_ubifi.or.L_obifi.or.L_ibifi.or.L_ebifi.or.L_abifi
     &) then      
                  I_udifi = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_udifi = z'40000000'
      endif
C SRG_vlv.fgi( 662, 605):���� ���������� ��������,20SRG10AA337
      !{
      Call KLAPAN_MAN(deltat,REAL(R_esofi,4),R8_apofi,I_ivofi
     &,C8_upofi,I_uvofi,R_orofi,
     & R_irofi,R_alofi,REAL(R_ilofi,4),R_emofi,
     & REAL(R_omofi,4),R_olofi,REAL(R_amofi,4),I_avofi,
     & I_axofi,I_ovofi,I_utofi,L_umofi,L_ixofi,L_ibufi,L_imofi
     &,
     & L_ulofi,REAL(R_arofi,4),L_ipofi,L_epofi,L_uxofi,
     & L_elofi,L_erofi,L_adufi,L_urofi,L_asofi,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(246),L_ekofi,L_(247),L_ikofi
     &,L_oxofi,I_exofi,L_obufi,
     & R_otofi,REAL(R_opofi,4),L_ubufi,L_okofi,L_edufi,L_ukofi
     &,L_isofi,L_osofi,
     & L_usofi,L_etofi,L_itofi,L_atofi)
      !}

      if(L_itofi.or.L_etofi.or.L_atofi.or.L_usofi.or.L_osofi.or.L_isofi
     &) then      
                  I_evofi = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_evofi = z'40000000'
      endif
C SRG_vlv.fgi( 662, 629):���� ���������� ��������,20SRG10AA336
      !{
      Call KLAPAN_MAN(deltat,REAL(R_olaki,4),R8_ifaki,I_upaki
     &,C8_ekaki,I_eraki,R_alaki,
     & R_ukaki,R_ibaki,REAL(R_ubaki,4),R_odaki,
     & REAL(R_afaki,4),R_adaki,REAL(R_idaki,4),I_ipaki,
     & I_iraki,I_araki,I_epaki,L_efaki,L_uraki,L_usaki,L_udaki
     &,
     & L_edaki,REAL(R_ikaki,4),L_ufaki,L_ofaki,L_esaki,
     & L_obaki,L_okaki,L_itaki,L_elaki,L_ilaki,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(250),L_oxufi,L_(251),L_uxufi
     &,L_asaki,I_oraki,L_ataki,
     & R_apaki,REAL(R_akaki,4),L_etaki,L_abaki,L_otaki,L_ebaki
     &,L_ulaki,L_amaki,
     & L_emaki,L_omaki,L_umaki,L_imaki)
      !}

      if(L_umaki.or.L_omaki.or.L_imaki.or.L_emaki.or.L_amaki.or.L_ulaki
     &) then      
                  I_opaki = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_opaki = z'40000000'
      endif
C SRG_vlv.fgi( 662, 653):���� ���������� ��������,20SRG10AA335
      !{
      Call KLAPAN_MAN(deltat,REAL(R_upuxe,4),R8_oluxe,I_atuxe
     &,C8_imuxe,I_ituxe,R_epuxe,
     & R_apuxe,R_ofuxe,REAL(R_akuxe,4),R_ukuxe,
     & REAL(R_eluxe,4),R_ekuxe,REAL(R_okuxe,4),I_osuxe,
     & I_otuxe,I_etuxe,I_isuxe,L_iluxe,L_avuxe,L_axuxe,L_aluxe
     &,
     & L_ikuxe,REAL(R_omuxe,4),L_amuxe,L_uluxe,L_ivuxe,
     & L_ufuxe,L_umuxe,L_oxuxe,L_ipuxe,L_opuxe,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(212),L_uduxe,L_(213),L_afuxe
     &,L_evuxe,I_utuxe,L_exuxe,
     & R_esuxe,REAL(R_emuxe,4),L_ixuxe,L_efuxe,L_uxuxe,L_ifuxe
     &,L_aruxe,L_eruxe,
     & L_iruxe,L_uruxe,L_asuxe,L_oruxe)
      !}

      if(L_asuxe.or.L_uruxe.or.L_oruxe.or.L_iruxe.or.L_eruxe.or.L_aruxe
     &) then      
                  I_usuxe = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_usuxe = z'40000000'
      endif
C SRG_vlv.fgi( 648, 581):���� ���������� ��������,20SRG10AA334
      !{
      Call KLAPAN_MAN(deltat,REAL(R_avifi,4),R8_urifi,I_ebofi
     &,C8_osifi,I_obofi,R_itifi,
     & R_etifi,R_umifi,REAL(R_epifi,4),R_arifi,
     & REAL(R_irifi,4),R_ipifi,REAL(R_upifi,4),I_uxifi,
     & I_ubofi,I_ibofi,I_oxifi,L_orifi,L_edofi,L_efofi,L_erifi
     &,
     & L_opifi,REAL(R_usifi,4),L_esifi,L_asifi,L_odofi,
     & L_apifi,L_atifi,L_ufofi,L_otifi,L_utifi,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(244),L_amifi,L_(245),L_emifi
     &,L_idofi,I_adofi,L_ifofi,
     & R_ixifi,REAL(R_isifi,4),L_ofofi,L_imifi,L_akofi,L_omifi
     &,L_evifi,L_ivifi,
     & L_ovifi,L_axifi,L_exifi,L_uvifi)
      !}

      if(L_exifi.or.L_axifi.or.L_uvifi.or.L_ovifi.or.L_ivifi.or.L_evifi
     &) then      
                  I_abofi = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_abofi = z'40000000'
      endif
C SRG_vlv.fgi( 648, 605):���� ���������� ��������,20SRG10AA333
      !{
      Call KLAPAN_MAN(deltat,REAL(R_ipufi,4),R8_elufi,I_osufi
     &,C8_amufi,I_atufi,R_umufi,
     & R_omufi,R_efufi,REAL(R_ofufi,4),R_ikufi,
     & REAL(R_ukufi,4),R_ufufi,REAL(R_ekufi,4),I_esufi,
     & I_etufi,I_usufi,I_asufi,L_alufi,L_otufi,L_ovufi,L_okufi
     &,
     & L_akufi,REAL(R_emufi,4),L_olufi,L_ilufi,L_avufi,
     & L_ifufi,L_imufi,L_exufi,L_apufi,L_epufi,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(248),L_idufi,L_(249),L_odufi
     &,L_utufi,I_itufi,L_uvufi,
     & R_urufi,REAL(R_ulufi,4),L_axufi,L_udufi,L_ixufi,L_afufi
     &,L_opufi,L_upufi,
     & L_arufi,L_irufi,L_orufi,L_erufi)
      !}

      if(L_orufi.or.L_irufi.or.L_erufi.or.L_arufi.or.L_upufi.or.L_opufi
     &) then      
                  I_isufi = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_isufi = z'40000000'
      endif
C SRG_vlv.fgi( 648, 629):���� ���������� ��������,20SRG10AA332
      !{
      Call KLAPAN_MAN(deltat,REAL(R_ufeki,4),R8_obeki,I_ameki
     &,C8_ideki,I_imeki,R_efeki,
     & R_afeki,R_ovaki,REAL(R_axaki,4),R_uxaki,
     & REAL(R_ebeki,4),R_exaki,REAL(R_oxaki,4),I_oleki,
     & I_omeki,I_emeki,I_ileki,L_ibeki,L_apeki,L_areki,L_abeki
     &,
     & L_ixaki,REAL(R_odeki,4),L_adeki,L_ubeki,L_ipeki,
     & L_uvaki,L_udeki,L_oreki,L_ifeki,L_ofeki,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(252),L_utaki,L_(253),L_avaki
     &,L_epeki,I_umeki,L_ereki,
     & R_eleki,REAL(R_edeki,4),L_ireki,L_evaki,L_ureki,L_ivaki
     &,L_akeki,L_ekeki,
     & L_ikeki,L_ukeki,L_aleki,L_okeki)
      !}

      if(L_aleki.or.L_ukeki.or.L_okeki.or.L_ikeki.or.L_ekeki.or.L_akeki
     &) then      
                  I_uleki = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_uleki = z'40000000'
      endif
C SRG_vlv.fgi( 648, 653):���� ���������� ��������,20SRG10AA331
      !{
      Call KLAPAN_MAN(deltat,REAL(R_amabi,4),R8_ufabi,I_erabi
     &,C8_okabi,I_orabi,R_ilabi,
     & R_elabi,R_ubabi,REAL(R_edabi,4),R_afabi,
     & REAL(R_ifabi,4),R_idabi,REAL(R_udabi,4),I_upabi,
     & I_urabi,I_irabi,I_opabi,L_ofabi,L_esabi,L_etabi,L_efabi
     &,
     & L_odabi,REAL(R_ukabi,4),L_ekabi,L_akabi,L_osabi,
     & L_adabi,L_alabi,L_utabi,L_olabi,L_ulabi,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(214),L_ababi,L_(215),L_ebabi
     &,L_isabi,I_asabi,L_itabi,
     & R_ipabi,REAL(R_ikabi,4),L_otabi,L_ibabi,L_avabi,L_obabi
     &,L_emabi,L_imabi,
     & L_omabi,L_apabi,L_epabi,L_umabi)
      !}

      if(L_epabi.or.L_apabi.or.L_umabi.or.L_omabi.or.L_imabi.or.L_emabi
     &) then      
                  I_arabi = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_arabi = z'40000000'
      endif
C SRG_vlv.fgi( 634, 581):���� ���������� ��������,20SRG10AA330
      !{
      Call KLAPAN_MAN(deltat,REAL(R_adiki,4),R8_uveki,I_ekiki
     &,C8_oxeki,I_okiki,R_ibiki,
     & R_ebiki,R_useki,REAL(R_eteki,4),R_aveki,
     & REAL(R_iveki,4),R_iteki,REAL(R_uteki,4),I_ufiki,
     & I_ukiki,I_ikiki,I_ofiki,L_oveki,L_eliki,L_emiki,L_eveki
     &,
     & L_oteki,REAL(R_uxeki,4),L_exeki,L_axeki,L_oliki,
     & L_ateki,L_abiki,L_umiki,L_obiki,L_ubiki,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(254),L_aseki,L_(255),L_eseki
     &,L_iliki,I_aliki,L_imiki,
     & R_ifiki,REAL(R_ixeki,4),L_omiki,L_iseki,L_apiki,L_oseki
     &,L_ediki,L_idiki,
     & L_odiki,L_afiki,L_efiki,L_udiki)
      !}

      if(L_efiki.or.L_afiki.or.L_udiki.or.L_odiki.or.L_idiki.or.L_ediki
     &) then      
                  I_akiki = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_akiki = z'40000000'
      endif
C SRG_vlv.fgi( 634, 605):���� ���������� ��������,20SRG10AA329
      !{
      Call KLAPAN_MAN(deltat,REAL(R_amali,4),R8_ufali,I_erali
     &,C8_okali,I_orali,R_ilali,
     & R_elali,R_ubali,REAL(R_edali,4),R_afali,
     & REAL(R_ifali,4),R_idali,REAL(R_udali,4),I_upali,
     & I_urali,I_irali,I_opali,L_ofali,L_esali,L_etali,L_efali
     &,
     & L_odali,REAL(R_ukali,4),L_ekali,L_akali,L_osali,
     & L_adali,L_alali,L_utali,L_olali,L_ulali,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(262),L_abali,L_(263),L_ebali
     &,L_isali,I_asali,L_itali,
     & R_ipali,REAL(R_ikali,4),L_otali,L_ibali,L_avali,L_obali
     &,L_emali,L_imali,
     & L_omali,L_apali,L_epali,L_umali)
      !}

      if(L_epali.or.L_apali.or.L_umali.or.L_omali.or.L_imali.or.L_emali
     &) then      
                  I_arali = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_arali = z'40000000'
      endif
C SRG_vlv.fgi( 634, 653):���� ���������� ��������,20SRG10AA314
      !{
      Call KLAPAN_MAN(deltat,REAL(R_upuki,4),R8_oluki,I_atuki
     &,C8_imuki,I_ituki,R_epuki,
     & R_apuki,R_ofuki,REAL(R_akuki,4),R_ukuki,
     & REAL(R_eluki,4),R_ekuki,REAL(R_okuki,4),I_osuki,
     & I_otuki,I_etuki,I_isuki,L_iluki,L_avuki,L_axuki,L_aluki
     &,
     & L_ikuki,REAL(R_omuki,4),L_amuki,L_uluki,L_ivuki,
     & L_ufuki,L_umuki,L_oxuki,L_ipuki,L_opuki,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(260),L_uduki,L_(261),L_afuki
     &,L_evuki,I_utuki,L_exuki,
     & R_esuki,REAL(R_emuki,4),L_ixuki,L_efuki,L_uxuki,L_ifuki
     &,L_aruki,L_eruki,
     & L_iruki,L_uruki,L_asuki,L_oruki)
      !}

      if(L_asuki.or.L_uruki.or.L_oruki.or.L_iruki.or.L_eruki.or.L_aruki
     &) then      
                  I_usuki = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_usuki = z'40000000'
      endif
C SRG_vlv.fgi( 620, 629):���� ���������� ��������,20SRG10AA311
      !{
      Call KLAPAN_MAN(deltat,REAL(R_opode,4),R8_ilode,I_usode
     &,C8_emode,I_etode,R_apode,
     & R_umode,R_ifode,REAL(R_ufode,4),R_okode,
     & REAL(R_alode,4),R_akode,REAL(R_ikode,4),I_isode,
     & I_itode,I_atode,I_esode,L_elode,L_utode,L_uvode,L_ukode
     &,
     & L_ekode,REAL(R_imode,4),L_ulode,L_olode,L_evode,
     & L_ofode,L_omode,L_ixode,L_epode,L_ipode,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(126),L_odode,L_(127),L_udode
     &,L_avode,I_otode,L_axode,
     & R_asode,REAL(R_amode,4),L_exode,L_afode,L_oxode,L_efode
     &,L_upode,L_arode,
     & L_erode,L_orode,L_urode,L_irode)
      !}

      if(L_urode.or.L_orode.or.L_irode.or.L_erode.or.L_arode.or.L_upode
     &) then      
                  I_osode = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_osode = z'40000000'
      endif
C SRG_vlv.fgi( 534, 354):���� ���������� ��������,20SRG10AA309
      !{
      Call KLAPAN_MAN(deltat,REAL(R_ulude,4),R8_ofude,I_arude
     &,C8_ikude,I_irude,R_elude,
     & R_alude,R_obude,REAL(R_adude,4),R_udude,
     & REAL(R_efude,4),R_edude,REAL(R_odude,4),I_opude,
     & I_orude,I_erude,I_ipude,L_ifude,L_asude,L_atude,L_afude
     &,
     & L_idude,REAL(R_okude,4),L_akude,L_ufude,L_isude,
     & L_ubude,L_ukude,L_otude,L_ilude,L_olude,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(128),L_uxode,L_(129),L_abude
     &,L_esude,I_urude,L_etude,
     & R_epude,REAL(R_ekude,4),L_itude,L_ebude,L_utude,L_ibude
     &,L_amude,L_emude,
     & L_imude,L_umude,L_apude,L_omude)
      !}

      if(L_apude.or.L_umude.or.L_omude.or.L_imude.or.L_emude.or.L_amude
     &) then      
                  I_upude = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_upude = z'40000000'
      endif
C SRG_vlv.fgi( 534, 378):���� ���������� ��������,20SRG10AA308
      !{
      Call KLAPAN_MAN(deltat,REAL(R_akafe,4),R8_ubafe,I_emafe
     &,C8_odafe,I_omafe,R_ifafe,
     & R_efafe,R_uvude,REAL(R_exude,4),R_abafe,
     & REAL(R_ibafe,4),R_ixude,REAL(R_uxude,4),I_ulafe,
     & I_umafe,I_imafe,I_olafe,L_obafe,L_epafe,L_erafe,L_ebafe
     &,
     & L_oxude,REAL(R_udafe,4),L_edafe,L_adafe,L_opafe,
     & L_axude,L_afafe,L_urafe,L_ofafe,L_ufafe,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(130),L_avude,L_(131),L_evude
     &,L_ipafe,I_apafe,L_irafe,
     & R_ilafe,REAL(R_idafe,4),L_orafe,L_ivude,L_asafe,L_ovude
     &,L_ekafe,L_ikafe,
     & L_okafe,L_alafe,L_elafe,L_ukafe)
      !}

      if(L_elafe.or.L_alafe.or.L_ukafe.or.L_okafe.or.L_ikafe.or.L_ekafe
     &) then      
                  I_amafe = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_amafe = z'40000000'
      endif
C SRG_vlv.fgi( 534, 402):���� ���������� ��������,20SRG10AA307
      !{
      Call KLAPAN_MAN(deltat,REAL(R_edefe,4),R8_axafe,I_ikefe
     &,C8_uxafe,I_ukefe,R_obefe,
     & R_ibefe,R_atafe,REAL(R_itafe,4),R_evafe,
     & REAL(R_ovafe,4),R_otafe,REAL(R_avafe,4),I_akefe,
     & I_alefe,I_okefe,I_ufefe,L_uvafe,L_ilefe,L_imefe,L_ivafe
     &,
     & L_utafe,REAL(R_abefe,4),L_ixafe,L_exafe,L_ulefe,
     & L_etafe,L_ebefe,L_apefe,L_ubefe,L_adefe,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(132),L_esafe,L_(133),L_isafe
     &,L_olefe,I_elefe,L_omefe,
     & R_ofefe,REAL(R_oxafe,4),L_umefe,L_osafe,L_epefe,L_usafe
     &,L_idefe,L_odefe,
     & L_udefe,L_efefe,L_ifefe,L_afefe)
      !}

      if(L_ifefe.or.L_efefe.or.L_afefe.or.L_udefe.or.L_odefe.or.L_idefe
     &) then      
                  I_ekefe = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ekefe = z'40000000'
      endif
C SRG_vlv.fgi( 520, 330):���� ���������� ��������,20SRG10AA306
      !{
      Call KLAPAN_MAN(deltat,REAL(R_ixefe,4),R8_etefe,I_odife
     &,C8_avefe,I_afife,R_uvefe,
     & R_ovefe,R_erefe,REAL(R_orefe,4),R_isefe,
     & REAL(R_usefe,4),R_urefe,REAL(R_esefe,4),I_edife,
     & I_efife,I_udife,I_adife,L_atefe,L_ofife,L_okife,L_osefe
     &,
     & L_asefe,REAL(R_evefe,4),L_otefe,L_itefe,L_akife,
     & L_irefe,L_ivefe,L_elife,L_axefe,L_exefe,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(134),L_ipefe,L_(135),L_opefe
     &,L_ufife,I_ifife,L_ukife,
     & R_ubife,REAL(R_utefe,4),L_alife,L_upefe,L_ilife,L_arefe
     &,L_oxefe,L_uxefe,
     & L_abife,L_ibife,L_obife,L_ebife)
      !}

      if(L_obife.or.L_ibife.or.L_ebife.or.L_abife.or.L_uxefe.or.L_oxefe
     &) then      
                  I_idife = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_idife = z'40000000'
      endif
C SRG_vlv.fgi( 520, 354):���� ���������� ��������,20SRG10AA305
      !{
      Call KLAPAN_MAN(deltat,REAL(R_otife,4),R8_irife,I_uxife
     &,C8_esife,I_ebofe,R_atife,
     & R_usife,R_imife,REAL(R_umife,4),R_opife,
     & REAL(R_arife,4),R_apife,REAL(R_ipife,4),I_ixife,
     & I_ibofe,I_abofe,I_exife,L_erife,L_ubofe,L_udofe,L_upife
     &,
     & L_epife,REAL(R_isife,4),L_urife,L_orife,L_edofe,
     & L_omife,L_osife,L_ifofe,L_etife,L_itife,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(136),L_olife,L_(137),L_ulife
     &,L_adofe,I_obofe,L_afofe,
     & R_axife,REAL(R_asife,4),L_efofe,L_amife,L_ofofe,L_emife
     &,L_utife,L_avife,
     & L_evife,L_ovife,L_uvife,L_ivife)
      !}

      if(L_uvife.or.L_ovife.or.L_ivife.or.L_evife.or.L_avife.or.L_utife
     &) then      
                  I_oxife = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_oxife = z'40000000'
      endif
C SRG_vlv.fgi( 520, 378):���� ���������� ��������,20SRG10AA304
      !{
      Call KLAPAN_MAN(deltat,REAL(R_urofe,4),R8_omofe,I_avofe
     &,C8_ipofe,I_ivofe,R_erofe,
     & R_arofe,R_okofe,REAL(R_alofe,4),R_ulofe,
     & REAL(R_emofe,4),R_elofe,REAL(R_olofe,4),I_otofe,
     & I_ovofe,I_evofe,I_itofe,L_imofe,L_axofe,L_abufe,L_amofe
     &,
     & L_ilofe,REAL(R_opofe,4),L_apofe,L_umofe,L_ixofe,
     & L_ukofe,L_upofe,L_obufe,L_irofe,L_orofe,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(138),L_ufofe,L_(139),L_akofe
     &,L_exofe,I_uvofe,L_ebufe,
     & R_etofe,REAL(R_epofe,4),L_ibufe,L_ekofe,L_ubufe,L_ikofe
     &,L_asofe,L_esofe,
     & L_isofe,L_usofe,L_atofe,L_osofe)
      !}

      if(L_atofe.or.L_usofe.or.L_osofe.or.L_isofe.or.L_esofe.or.L_asofe
     &) then      
                  I_utofe = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_utofe = z'40000000'
      endif
C SRG_vlv.fgi( 520, 402):���� ���������� ��������,20SRG10AA303
      !{
      Call KLAPAN_MAN(deltat,REAL(R_apufe,4),R8_ukufe,I_esufe
     &,C8_olufe,I_osufe,R_imufe,
     & R_emufe,R_udufe,REAL(R_efufe,4),R_akufe,
     & REAL(R_ikufe,4),R_ifufe,REAL(R_ufufe,4),I_urufe,
     & I_usufe,I_isufe,I_orufe,L_okufe,L_etufe,L_evufe,L_ekufe
     &,
     & L_ofufe,REAL(R_ulufe,4),L_elufe,L_alufe,L_otufe,
     & L_afufe,L_amufe,L_uvufe,L_omufe,L_umufe,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(140),L_adufe,L_(141),L_edufe
     &,L_itufe,I_atufe,L_ivufe,
     & R_irufe,REAL(R_ilufe,4),L_ovufe,L_idufe,L_axufe,L_odufe
     &,L_epufe,L_ipufe,
     & L_opufe,L_arufe,L_erufe,L_upufe)
      !}

      if(L_erufe.or.L_arufe.or.L_upufe.or.L_opufe.or.L_ipufe.or.L_epufe
     &) then      
                  I_asufe = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_asufe = z'40000000'
      endif
C SRG_vlv.fgi( 506, 330):���� ���������� ��������,20SRG10AA302
      !{
      Call KLAPAN_MAN(deltat,REAL(R_elake,4),R8_afake,I_ipake
     &,C8_ufake,I_upake,R_okake,
     & R_ikake,R_abake,REAL(R_ibake,4),R_edake,
     & REAL(R_odake,4),R_obake,REAL(R_adake,4),I_apake,
     & I_arake,I_opake,I_umake,L_udake,L_irake,L_isake,L_idake
     &,
     & L_ubake,REAL(R_akake,4),L_ifake,L_efake,L_urake,
     & L_ebake,L_ekake,L_atake,L_ukake,L_alake,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(142),L_exufe,L_(143),L_ixufe
     &,L_orake,I_erake,L_osake,
     & R_omake,REAL(R_ofake,4),L_usake,L_oxufe,L_etake,L_uxufe
     &,L_ilake,L_olake,
     & L_ulake,L_emake,L_imake,L_amake)
      !}

      if(L_imake.or.L_emake.or.L_amake.or.L_ulake.or.L_olake.or.L_ilake
     &) then      
                  I_epake = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_epake = z'40000000'
      endif
C SRG_vlv.fgi( 506, 354):���� ���������� ��������,20SRG10AA301
      !{
      Call KLAPAN_MAN(deltat,REAL(R_ifeke,4),R8_ebeke,I_oleke
     &,C8_adeke,I_ameke,R_udeke,
     & R_odeke,R_evake,REAL(R_ovake,4),R_ixake,
     & REAL(R_uxake,4),R_uvake,REAL(R_exake,4),I_eleke,
     & I_emeke,I_uleke,I_aleke,L_abeke,L_omeke,L_opeke,L_oxake
     &,
     & L_axake,REAL(R_edeke,4),L_obeke,L_ibeke,L_apeke,
     & L_ivake,L_ideke,L_ereke,L_afeke,L_efeke,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(144),L_itake,L_(145),L_otake
     &,L_umeke,I_imeke,L_upeke,
     & R_ukeke,REAL(R_ubeke,4),L_areke,L_utake,L_ireke,L_avake
     &,L_ofeke,L_ufeke,
     & L_akeke,L_ikeke,L_okeke,L_ekeke)
      !}

      if(L_okeke.or.L_ikeke.or.L_ekeke.or.L_akeke.or.L_ufeke.or.L_ofeke
     &) then      
                  I_ileke = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ileke = z'40000000'
      endif
C SRG_vlv.fgi( 506, 378):���� ���������� ��������,20SRG10AA300
      !{
      Call KLAPAN_MAN(deltat,REAL(R_udiko,4),R8_oxeko,I_aliko
     &,C8_ibiko,I_iliko,R_ediko,
     & R_adiko,R_oteko,REAL(R_aveko,4),R_uveko,
     & REAL(R_exeko,4),R_eveko,REAL(R_oveko,4),I_okiko,
     & I_oliko,I_eliko,I_ikiko,L_ixeko,L_amiko,L_apiko,L_axeko
     &,
     & L_iveko,REAL(R_obiko,4),L_abiko,L_uxeko,L_imiko,
     & L_uteko,L_ubiko,L_opiko,L_idiko,L_odiko,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(324),L_useko,L_(325),L_ateko
     &,L_emiko,I_uliko,L_epiko,
     & R_ekiko,REAL(R_ebiko,4),L_ipiko,L_eteko,L_upiko,L_iteko
     &,L_afiko,L_efiko,
     & L_ifiko,L_ufiko,L_akiko,L_ofiko)
      !}

      if(L_akiko.or.L_ufiko.or.L_ofiko.or.L_ifiko.or.L_efiko.or.L_afiko
     &) then      
                  I_ukiko = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ukiko = z'40000000'
      endif
C SRG_vlv.fgi( 449, 653):���� ���������� ��������,20SRG10AA261
      !{
      Call KLAPAN_MAN(deltat,REAL(R_evoko,4),R8_asoko,I_ibuko
     &,C8_usoko,I_ubuko,R_otoko,
     & R_itoko,R_apoko,REAL(R_ipoko,4),R_eroko,
     & REAL(R_oroko,4),R_opoko,REAL(R_aroko,4),I_abuko,
     & I_aduko,I_obuko,I_uxoko,L_uroko,L_iduko,L_ifuko,L_iroko
     &,
     & L_upoko,REAL(R_atoko,4),L_isoko,L_esoko,L_uduko,
     & L_epoko,L_etoko,L_akuko,L_utoko,L_avoko,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(328),L_emoko,L_(329),L_imoko
     &,L_oduko,I_eduko,L_ofuko,
     & R_oxoko,REAL(R_osoko,4),L_ufuko,L_omoko,L_ekuko,L_umoko
     &,L_ivoko,L_ovoko,
     & L_uvoko,L_exoko,L_ixoko,L_axoko)
      !}

      if(L_ixoko.or.L_exoko.or.L_axoko.or.L_uvoko.or.L_ovoko.or.L_ivoko
     &) then      
                  I_ebuko = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ebuko = z'40000000'
      endif
C SRG_vlv.fgi( 421, 605):���� ���������� ��������,20SRG10AA245
      !{
      Call KLAPAN_MAN(deltat,REAL(R_okelo,4),R8_idelo,I_umelo
     &,C8_efelo,I_epelo,R_akelo,
     & R_ufelo,R_ixalo,REAL(R_uxalo,4),R_obelo,
     & REAL(R_adelo,4),R_abelo,REAL(R_ibelo,4),I_imelo,
     & I_ipelo,I_apelo,I_emelo,L_edelo,L_upelo,L_urelo,L_ubelo
     &,
     & L_ebelo,REAL(R_ifelo,4),L_udelo,L_odelo,L_erelo,
     & L_oxalo,L_ofelo,L_iselo,L_ekelo,L_ikelo,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(334),L_ovalo,L_(335),L_uvalo
     &,L_arelo,I_opelo,L_aselo,
     & R_amelo,REAL(R_afelo,4),L_eselo,L_axalo,L_oselo,L_exalo
     &,L_ukelo,L_alelo,
     & L_elelo,L_olelo,L_ulelo,L_ilelo)
      !}

      if(L_ulelo.or.L_olelo.or.L_ilelo.or.L_elelo.or.L_alelo.or.L_ukelo
     &) then      
                  I_omelo = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_omelo = z'40000000'
      endif
C SRG_vlv.fgi( 435, 653):���� ���������� ��������,20SRG10AA242
      !{
      Call KLAPAN_MAN(deltat,REAL(R_obike,4),R8_iveke,I_ufike
     &,C8_exeke,I_ekike,R_abike,
     & R_uxeke,R_iseke,REAL(R_useke,4),R_oteke,
     & REAL(R_aveke,4),R_ateke,REAL(R_iteke,4),I_ifike,
     & I_ikike,I_akike,I_efike,L_eveke,L_ukike,L_ulike,L_uteke
     &,
     & L_eteke,REAL(R_ixeke,4),L_uveke,L_oveke,L_elike,
     & L_oseke,L_oxeke,L_imike,L_ebike,L_ibike,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(146),L_oreke,L_(147),L_ureke
     &,L_alike,I_okike,L_amike,
     & R_afike,REAL(R_axeke,4),L_emike,L_aseke,L_omike,L_eseke
     &,L_ubike,L_adike,
     & L_edike,L_odike,L_udike,L_idike)
      !}

      if(L_udike.or.L_odike.or.L_idike.or.L_edike.or.L_adike.or.L_ubike
     &) then      
                  I_ofike = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ofike = z'40000000'
      endif
C SRG_vlv.fgi( 506, 402):���� ���������� ��������,20SRG10AA299
      !{
      Call KLAPAN_MAN(deltat,REAL(R_uvike,4),R8_osike,I_adoke
     &,C8_itike,I_idoke,R_evike,
     & R_avike,R_opike,REAL(R_arike,4),R_urike,
     & REAL(R_esike,4),R_erike,REAL(R_orike,4),I_oboke,
     & I_odoke,I_edoke,I_iboke,L_isike,L_afoke,L_akoke,L_asike
     &,
     & L_irike,REAL(R_otike,4),L_atike,L_usike,L_ifoke,
     & L_upike,L_utike,L_okoke,L_ivike,L_ovike,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(148),L_umike,L_(149),L_apike
     &,L_efoke,I_udoke,L_ekoke,
     & R_eboke,REAL(R_etike,4),L_ikoke,L_epike,L_ukoke,L_ipike
     &,L_axike,L_exike,
     & L_ixike,L_uxike,L_aboke,L_oxike)
      !}

      if(L_aboke.or.L_uxike.or.L_oxike.or.L_ixike.or.L_exike.or.L_axike
     &) then      
                  I_uboke = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_uboke = z'40000000'
      endif
C SRG_vlv.fgi( 492, 330):���� ���������� ��������,20SRG10AA298
      !{
      Call KLAPAN_MAN(deltat,REAL(R_atoke,4),R8_upoke,I_exoke
     &,C8_oroke,I_oxoke,R_isoke,
     & R_esoke,R_uloke,REAL(R_emoke,4),R_apoke,
     & REAL(R_ipoke,4),R_imoke,REAL(R_umoke,4),I_uvoke,
     & I_uxoke,I_ixoke,I_ovoke,L_opoke,L_ebuke,L_eduke,L_epoke
     &,
     & L_omoke,REAL(R_uroke,4),L_eroke,L_aroke,L_obuke,
     & L_amoke,L_asoke,L_uduke,L_osoke,L_usoke,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(150),L_aloke,L_(151),L_eloke
     &,L_ibuke,I_abuke,L_iduke,
     & R_ivoke,REAL(R_iroke,4),L_oduke,L_iloke,L_afuke,L_oloke
     &,L_etoke,L_itoke,
     & L_otoke,L_avoke,L_evoke,L_utoke)
      !}

      if(L_evoke.or.L_avoke.or.L_utoke.or.L_otoke.or.L_itoke.or.L_etoke
     &) then      
                  I_axoke = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_axoke = z'40000000'
      endif
C SRG_vlv.fgi( 492, 354):���� ���������� ��������,20SRG10AA297
      !{
      Call KLAPAN_MAN(deltat,REAL(R_eruke,4),R8_amuke,I_ituke
     &,C8_umuke,I_utuke,R_opuke,
     & R_ipuke,R_akuke,REAL(R_ikuke,4),R_eluke,
     & REAL(R_oluke,4),R_okuke,REAL(R_aluke,4),I_atuke,
     & I_avuke,I_otuke,I_usuke,L_uluke,L_ivuke,L_ixuke,L_iluke
     &,
     & L_ukuke,REAL(R_apuke,4),L_imuke,L_emuke,L_uvuke,
     & L_ekuke,L_epuke,L_abale,L_upuke,L_aruke,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(152),L_efuke,L_(153),L_ifuke
     &,L_ovuke,I_evuke,L_oxuke,
     & R_osuke,REAL(R_omuke,4),L_uxuke,L_ofuke,L_ebale,L_ufuke
     &,L_iruke,L_oruke,
     & L_uruke,L_esuke,L_isuke,L_asuke)
      !}

      if(L_isuke.or.L_esuke.or.L_asuke.or.L_uruke.or.L_oruke.or.L_iruke
     &) then      
                  I_etuke = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_etuke = z'40000000'
      endif
C SRG_vlv.fgi( 492, 378):���� ���������� ��������,20SRG10AA296
      !{
      Call KLAPAN_MAN(deltat,REAL(R_imale,4),R8_ekale,I_orale
     &,C8_alale,I_asale,R_ulale,
     & R_olale,R_edale,REAL(R_odale,4),R_ifale,
     & REAL(R_ufale,4),R_udale,REAL(R_efale,4),I_erale,
     & I_esale,I_urale,I_arale,L_akale,L_osale,L_otale,L_ofale
     &,
     & L_afale,REAL(R_elale,4),L_okale,L_ikale,L_atale,
     & L_idale,L_ilale,L_evale,L_amale,L_emale,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(154),L_ibale,L_(155),L_obale
     &,L_usale,I_isale,L_utale,
     & R_upale,REAL(R_ukale,4),L_avale,L_ubale,L_ivale,L_adale
     &,L_omale,L_umale,
     & L_apale,L_ipale,L_opale,L_epale)
      !}

      if(L_opale.or.L_ipale.or.L_epale.or.L_apale.or.L_umale.or.L_omale
     &) then      
                  I_irale = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_irale = z'40000000'
      endif
C SRG_vlv.fgi( 492, 402):���� ���������� ��������,20SRG10AA295
      !{
      Call KLAPAN_MAN(deltat,REAL(R_okele,4),R8_idele,I_umele
     &,C8_efele,I_epele,R_akele,
     & R_ufele,R_ixale,REAL(R_uxale,4),R_obele,
     & REAL(R_adele,4),R_abele,REAL(R_ibele,4),I_imele,
     & I_ipele,I_apele,I_emele,L_edele,L_upele,L_urele,L_ubele
     &,
     & L_ebele,REAL(R_ifele,4),L_udele,L_odele,L_erele,
     & L_oxale,L_ofele,L_isele,L_ekele,L_ikele,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(156),L_ovale,L_(157),L_uvale
     &,L_arele,I_opele,L_asele,
     & R_amele,REAL(R_afele,4),L_esele,L_axale,L_osele,L_exale
     &,L_ukele,L_alele,
     & L_elele,L_olele,L_ulele,L_ilele)
      !}

      if(L_ulele.or.L_olele.or.L_ilele.or.L_elele.or.L_alele.or.L_ukele
     &) then      
                  I_omele = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_omele = z'40000000'
      endif
C SRG_vlv.fgi( 478, 330):���� ���������� ��������,20SRG10AA280
      !{
      Call KLAPAN_MAN(deltat,REAL(R_udile,4),R8_oxele,I_alile
     &,C8_ibile,I_ilile,R_edile,
     & R_adile,R_otele,REAL(R_avele,4),R_uvele,
     & REAL(R_exele,4),R_evele,REAL(R_ovele,4),I_okile,
     & I_olile,I_elile,I_ikile,L_ixele,L_amile,L_apile,L_axele
     &,
     & L_ivele,REAL(R_obile,4),L_abile,L_uxele,L_imile,
     & L_utele,L_ubile,L_opile,L_idile,L_odile,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(158),L_usele,L_(159),L_atele
     &,L_emile,I_ulile,L_epile,
     & R_ekile,REAL(R_ebile,4),L_ipile,L_etele,L_upile,L_itele
     &,L_afile,L_efile,
     & L_ifile,L_ufile,L_akile,L_ofile)
      !}

      if(L_akile.or.L_ufile.or.L_ofile.or.L_ifile.or.L_efile.or.L_afile
     &) then      
                  I_ukile = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ukile = z'40000000'
      endif
C SRG_vlv.fgi( 478, 354):���� ���������� ��������,20SRG10AA277
      !{
      Call KLAPAN_MAN(deltat,REAL(R_akado,4),R8_ubado,I_emado
     &,C8_odado,I_omado,R_ifado,
     & R_efado,R_uvubo,REAL(R_exubo,4),R_abado,
     & REAL(R_ibado,4),R_ixubo,REAL(R_uxubo,4),I_ulado,
     & I_umado,I_imado,I_olado,L_obado,L_epado,L_erado,L_ebado
     &,
     & L_oxubo,REAL(R_udado,4),L_edado,L_adado,L_opado,
     & L_axubo,L_afado,L_urado,L_ofado,L_ufado,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(296),L_avubo,L_(297),L_evubo
     &,L_ipado,I_apado,L_irado,
     & R_ilado,REAL(R_idado,4),L_orado,L_ivubo,L_asado,L_ovubo
     &,L_ekado,L_ikado,
     & L_okado,L_alado,L_elado,L_ukado)
      !}

      if(L_elado.or.L_alado.or.L_ukado.or.L_okado.or.L_ikado.or.L_ekado
     &) then      
                  I_amado = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_amado = z'40000000'
      endif
C SRG_vlv.fgi( 505, 629):���� ���������� ��������,20SRG10AA275
      !{
      Call KLAPAN_MAN(deltat,REAL(R_ededo,4),R8_axado,I_ikedo
     &,C8_uxado,I_ukedo,R_obedo,
     & R_ibedo,R_atado,REAL(R_itado,4),R_evado,
     & REAL(R_ovado,4),R_otado,REAL(R_avado,4),I_akedo,
     & I_aledo,I_okedo,I_ufedo,L_uvado,L_iledo,L_imedo,L_ivado
     &,
     & L_utado,REAL(R_abedo,4),L_ixado,L_exado,L_uledo,
     & L_etado,L_ebedo,L_apedo,L_ubedo,L_adedo,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(298),L_esado,L_(299),L_isado
     &,L_oledo,I_eledo,L_omedo,
     & R_ofedo,REAL(R_oxado,4),L_umedo,L_osado,L_epedo,L_usado
     &,L_idedo,L_odedo,
     & L_udedo,L_efedo,L_ifedo,L_afedo)
      !}

      if(L_ifedo.or.L_efedo.or.L_afedo.or.L_udedo.or.L_odedo.or.L_idedo
     &) then      
                  I_ekedo = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ekedo = z'40000000'
      endif
C SRG_vlv.fgi( 519, 653):���� ���������� ��������,20SRG10AA274
      !{
      Call KLAPAN_MAN(deltat,REAL(R_ixedo,4),R8_etedo,I_odido
     &,C8_avedo,I_afido,R_uvedo,
     & R_ovedo,R_eredo,REAL(R_oredo,4),R_isedo,
     & REAL(R_usedo,4),R_uredo,REAL(R_esedo,4),I_edido,
     & I_efido,I_udido,I_adido,L_atedo,L_ofido,L_okido,L_osedo
     &,
     & L_asedo,REAL(R_evedo,4),L_otedo,L_itedo,L_akido,
     & L_iredo,L_ivedo,L_elido,L_axedo,L_exedo,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(300),L_ipedo,L_(301),L_opedo
     &,L_ufido,I_ifido,L_ukido,
     & R_ubido,REAL(R_utedo,4),L_alido,L_upedo,L_ilido,L_aredo
     &,L_oxedo,L_uxedo,
     & L_abido,L_ibido,L_obido,L_ebido)
      !}

      if(L_obido.or.L_ibido.or.L_ebido.or.L_abido.or.L_uxedo.or.L_oxedo
     &) then      
                  I_idido = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_idido = z'40000000'
      endif
C SRG_vlv.fgi( 505, 653):���� ���������� ��������,20SRG10AA273
      !{
      Call KLAPAN_MAN(deltat,REAL(R_otido,4),R8_irido,I_uxido
     &,C8_esido,I_ebodo,R_atido,
     & R_usido,R_imido,REAL(R_umido,4),R_opido,
     & REAL(R_arido,4),R_apido,REAL(R_ipido,4),I_ixido,
     & I_ibodo,I_abodo,I_exido,L_erido,L_ubodo,L_udodo,L_upido
     &,
     & L_epido,REAL(R_isido,4),L_urido,L_orido,L_edodo,
     & L_omido,L_osido,L_ifodo,L_etido,L_itido,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(302),L_olido,L_(303),L_ulido
     &,L_adodo,I_obodo,L_afodo,
     & R_axido,REAL(R_asido,4),L_efodo,L_amido,L_ofodo,L_emido
     &,L_utido,L_avido,
     & L_evido,L_ovido,L_uvido,L_ivido)
      !}

      if(L_uvido.or.L_ovido.or.L_ivido.or.L_evido.or.L_avido.or.L_utido
     &) then      
                  I_oxido = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_oxido = z'40000000'
      endif
C SRG_vlv.fgi( 491, 605):���� ���������� ��������,20SRG10AA272
      !{
      Call KLAPAN_MAN(deltat,REAL(R_urodo,4),R8_omodo,I_avodo
     &,C8_ipodo,I_ivodo,R_erodo,
     & R_arodo,R_okodo,REAL(R_alodo,4),R_ulodo,
     & REAL(R_emodo,4),R_elodo,REAL(R_olodo,4),I_otodo,
     & I_ovodo,I_evodo,I_itodo,L_imodo,L_axodo,L_abudo,L_amodo
     &,
     & L_ilodo,REAL(R_opodo,4),L_apodo,L_umodo,L_ixodo,
     & L_ukodo,L_upodo,L_obudo,L_irodo,L_orodo,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(304),L_ufodo,L_(305),L_akodo
     &,L_exodo,I_uvodo,L_ebudo,
     & R_etodo,REAL(R_epodo,4),L_ibudo,L_ekodo,L_ubudo,L_ikodo
     &,L_asodo,L_esodo,
     & L_isodo,L_usodo,L_atodo,L_osodo)
      !}

      if(L_atodo.or.L_usodo.or.L_osodo.or.L_isodo.or.L_esodo.or.L_asodo
     &) then      
                  I_utodo = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_utodo = z'40000000'
      endif
C SRG_vlv.fgi( 477, 605):���� ���������� ��������,20SRG10AA271
      !{
      Call KLAPAN_MAN(deltat,REAL(R_apudo,4),R8_ukudo,I_esudo
     &,C8_oludo,I_osudo,R_imudo,
     & R_emudo,R_ududo,REAL(R_efudo,4),R_akudo,
     & REAL(R_ikudo,4),R_ifudo,REAL(R_ufudo,4),I_urudo,
     & I_usudo,I_isudo,I_orudo,L_okudo,L_etudo,L_evudo,L_ekudo
     &,
     & L_ofudo,REAL(R_uludo,4),L_eludo,L_aludo,L_otudo,
     & L_afudo,L_amudo,L_uvudo,L_omudo,L_umudo,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(306),L_adudo,L_(307),L_edudo
     &,L_itudo,I_atudo,L_ivudo,
     & R_irudo,REAL(R_iludo,4),L_ovudo,L_idudo,L_axudo,L_odudo
     &,L_epudo,L_ipudo,
     & L_opudo,L_arudo,L_erudo,L_upudo)
      !}

      if(L_erudo.or.L_arudo.or.L_upudo.or.L_opudo.or.L_ipudo.or.L_epudo
     &) then      
                  I_asudo = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_asudo = z'40000000'
      endif
C SRG_vlv.fgi( 491, 629):���� ���������� ��������,20SRG10AA270
      !{
      Call KLAPAN_MAN(deltat,REAL(R_elafo,4),R8_afafo,I_ipafo
     &,C8_ufafo,I_upafo,R_okafo,
     & R_ikafo,R_abafo,REAL(R_ibafo,4),R_edafo,
     & REAL(R_odafo,4),R_obafo,REAL(R_adafo,4),I_apafo,
     & I_arafo,I_opafo,I_umafo,L_udafo,L_irafo,L_isafo,L_idafo
     &,
     & L_ubafo,REAL(R_akafo,4),L_ifafo,L_efafo,L_urafo,
     & L_ebafo,L_ekafo,L_atafo,L_ukafo,L_alafo,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(308),L_exudo,L_(309),L_ixudo
     &,L_orafo,I_erafo,L_osafo,
     & R_omafo,REAL(R_ofafo,4),L_usafo,L_oxudo,L_etafo,L_uxudo
     &,L_ilafo,L_olafo,
     & L_ulafo,L_emafo,L_imafo,L_amafo)
      !}

      if(L_imafo.or.L_emafo.or.L_amafo.or.L_ulafo.or.L_olafo.or.L_ilafo
     &) then      
                  I_epafo = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_epafo = z'40000000'
      endif
C SRG_vlv.fgi( 477, 629):���� ���������� ��������,20SRG10AA269
      !{
      Call KLAPAN_MAN(deltat,REAL(R_ifefo,4),R8_ebefo,I_olefo
     &,C8_adefo,I_amefo,R_udefo,
     & R_odefo,R_evafo,REAL(R_ovafo,4),R_ixafo,
     & REAL(R_uxafo,4),R_uvafo,REAL(R_exafo,4),I_elefo,
     & I_emefo,I_ulefo,I_alefo,L_abefo,L_omefo,L_opefo,L_oxafo
     &,
     & L_axafo,REAL(R_edefo,4),L_obefo,L_ibefo,L_apefo,
     & L_ivafo,L_idefo,L_erefo,L_afefo,L_efefo,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(310),L_itafo,L_(311),L_otafo
     &,L_umefo,I_imefo,L_upefo,
     & R_ukefo,REAL(R_ubefo,4),L_arefo,L_utafo,L_irefo,L_avafo
     &,L_ofefo,L_ufefo,
     & L_akefo,L_ikefo,L_okefo,L_ekefo)
      !}

      if(L_okefo.or.L_ikefo.or.L_ekefo.or.L_akefo.or.L_ufefo.or.L_ofefo
     &) then      
                  I_ilefo = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ilefo = z'40000000'
      endif
C SRG_vlv.fgi( 491, 653):���� ���������� ��������,20SRG10AA268
      !{
      Call KLAPAN_MAN(deltat,REAL(R_obifo,4),R8_ivefo,I_ufifo
     &,C8_exefo,I_ekifo,R_abifo,
     & R_uxefo,R_isefo,REAL(R_usefo,4),R_otefo,
     & REAL(R_avefo,4),R_atefo,REAL(R_itefo,4),I_ififo,
     & I_ikifo,I_akifo,I_efifo,L_evefo,L_ukifo,L_ulifo,L_utefo
     &,
     & L_etefo,REAL(R_ixefo,4),L_uvefo,L_ovefo,L_elifo,
     & L_osefo,L_oxefo,L_imifo,L_ebifo,L_ibifo,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(312),L_orefo,L_(313),L_urefo
     &,L_alifo,I_okifo,L_amifo,
     & R_afifo,REAL(R_axefo,4),L_emifo,L_asefo,L_omifo,L_esefo
     &,L_ubifo,L_adifo,
     & L_edifo,L_odifo,L_udifo,L_idifo)
      !}

      if(L_udifo.or.L_odifo.or.L_idifo.or.L_edifo.or.L_adifo.or.L_ubifo
     &) then      
                  I_ofifo = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ofifo = z'40000000'
      endif
C SRG_vlv.fgi( 477, 653):���� ���������� ��������,20SRG10AA267
      !{
      Call KLAPAN_MAN(deltat,REAL(R_uvifo,4),R8_osifo,I_adofo
     &,C8_itifo,I_idofo,R_evifo,
     & R_avifo,R_opifo,REAL(R_arifo,4),R_urifo,
     & REAL(R_esifo,4),R_erifo,REAL(R_orifo,4),I_obofo,
     & I_odofo,I_edofo,I_ibofo,L_isifo,L_afofo,L_akofo,L_asifo
     &,
     & L_irifo,REAL(R_otifo,4),L_atifo,L_usifo,L_ifofo,
     & L_upifo,L_utifo,L_okofo,L_ivifo,L_ovifo,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(314),L_umifo,L_(315),L_apifo
     &,L_efofo,I_udofo,L_ekofo,
     & R_ebofo,REAL(R_etifo,4),L_ikofo,L_epifo,L_ukofo,L_ipifo
     &,L_axifo,L_exifo,
     & L_ixifo,L_uxifo,L_abofo,L_oxifo)
      !}

      if(L_abofo.or.L_uxifo.or.L_oxifo.or.L_ixifo.or.L_exifo.or.L_axifo
     &) then      
                  I_ubofo = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ubofo = z'40000000'
      endif
C SRG_vlv.fgi( 463, 605):���� ���������� ��������,20SRG10AA266
      !{
      Call KLAPAN_MAN(deltat,REAL(R_atofo,4),R8_upofo,I_exofo
     &,C8_orofo,I_oxofo,R_isofo,
     & R_esofo,R_ulofo,REAL(R_emofo,4),R_apofo,
     & REAL(R_ipofo,4),R_imofo,REAL(R_umofo,4),I_uvofo,
     & I_uxofo,I_ixofo,I_ovofo,L_opofo,L_ebufo,L_edufo,L_epofo
     &,
     & L_omofo,REAL(R_urofo,4),L_erofo,L_arofo,L_obufo,
     & L_amofo,L_asofo,L_udufo,L_osofo,L_usofo,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(316),L_alofo,L_(317),L_elofo
     &,L_ibufo,I_abufo,L_idufo,
     & R_ivofo,REAL(R_irofo,4),L_odufo,L_ilofo,L_afufo,L_olofo
     &,L_etofo,L_itofo,
     & L_otofo,L_avofo,L_evofo,L_utofo)
      !}

      if(L_evofo.or.L_avofo.or.L_utofo.or.L_otofo.or.L_itofo.or.L_etofo
     &) then      
                  I_axofo = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_axofo = z'40000000'
      endif
C SRG_vlv.fgi( 449, 605):���� ���������� ��������,20SRG10AA265
      !{
      Call KLAPAN_MAN(deltat,REAL(R_erufo,4),R8_amufo,I_itufo
     &,C8_umufo,I_utufo,R_opufo,
     & R_ipufo,R_akufo,REAL(R_ikufo,4),R_elufo,
     & REAL(R_olufo,4),R_okufo,REAL(R_alufo,4),I_atufo,
     & I_avufo,I_otufo,I_usufo,L_ulufo,L_ivufo,L_ixufo,L_ilufo
     &,
     & L_ukufo,REAL(R_apufo,4),L_imufo,L_emufo,L_uvufo,
     & L_ekufo,L_epufo,L_abako,L_upufo,L_arufo,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(318),L_efufo,L_(319),L_ifufo
     &,L_ovufo,I_evufo,L_oxufo,
     & R_osufo,REAL(R_omufo,4),L_uxufo,L_ofufo,L_ebako,L_ufufo
     &,L_irufo,L_orufo,
     & L_urufo,L_esufo,L_isufo,L_asufo)
      !}

      if(L_isufo.or.L_esufo.or.L_asufo.or.L_urufo.or.L_orufo.or.L_irufo
     &) then      
                  I_etufo = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_etufo = z'40000000'
      endif
C SRG_vlv.fgi( 463, 629):���� ���������� ��������,20SRG10AA264
      !{
      Call KLAPAN_MAN(deltat,REAL(R_imako,4),R8_ekako,I_orako
     &,C8_alako,I_asako,R_ulako,
     & R_olako,R_edako,REAL(R_odako,4),R_ifako,
     & REAL(R_ufako,4),R_udako,REAL(R_efako,4),I_erako,
     & I_esako,I_urako,I_arako,L_akako,L_osako,L_otako,L_ofako
     &,
     & L_afako,REAL(R_elako,4),L_okako,L_ikako,L_atako,
     & L_idako,L_ilako,L_evako,L_amako,L_emako,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(320),L_ibako,L_(321),L_obako
     &,L_usako,I_isako,L_utako,
     & R_upako,REAL(R_ukako,4),L_avako,L_ubako,L_ivako,L_adako
     &,L_omako,L_umako,
     & L_apako,L_ipako,L_opako,L_epako)
      !}

      if(L_opako.or.L_ipako.or.L_epako.or.L_apako.or.L_umako.or.L_omako
     &) then      
                  I_irako = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_irako = z'40000000'
      endif
C SRG_vlv.fgi( 449, 629):���� ���������� ��������,20SRG10AA263
      !{
      Call KLAPAN_MAN(deltat,REAL(R_okeko,4),R8_ideko,I_umeko
     &,C8_efeko,I_epeko,R_akeko,
     & R_ufeko,R_ixako,REAL(R_uxako,4),R_obeko,
     & REAL(R_adeko,4),R_abeko,REAL(R_ibeko,4),I_imeko,
     & I_ipeko,I_apeko,I_emeko,L_edeko,L_upeko,L_ureko,L_ubeko
     &,
     & L_ebeko,REAL(R_ifeko,4),L_udeko,L_odeko,L_ereko,
     & L_oxako,L_ofeko,L_iseko,L_ekeko,L_ikeko,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(322),L_ovako,L_(323),L_uvako
     &,L_areko,I_opeko,L_aseko,
     & R_ameko,REAL(R_afeko,4),L_eseko,L_axako,L_oseko,L_exako
     &,L_ukeko,L_aleko,
     & L_eleko,L_oleko,L_uleko,L_ileko)
      !}

      if(L_uleko.or.L_oleko.or.L_ileko.or.L_eleko.or.L_aleko.or.L_ukeko
     &) then      
                  I_omeko = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_omeko = z'40000000'
      endif
C SRG_vlv.fgi( 463, 653):���� ���������� ��������,20SRG10AA262
      R_imimo = R8_e
C SRG_init_1.fgi( 224,  37):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_elimo,R_erimo,REAL(16.666
     &,4),
     & REAL(R_imimo,4),I_arimo,REAL(R_ulimo,4),
     & REAL(R_amimo,4),REAL(R_alimo,4),
     & REAL(R_ukimo,4),REAL(R_umimo,4),L_apimo,REAL(R_epimo
     &,4),L_ipimo,
     & L_opimo,R_emimo,REAL(R_olimo,4),REAL(R_ilimo,4),L_upimo
     &)
      !}
C SRG_vlv.fgi(  84, 602):���������� ������� ��� 2,20SRG10CF031XQ01
      R_atimo = R8_i
C SRG_init_1.fgi( 138,  37):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_urimo,R_uvimo,REAL(16.666
     &,4),
     & REAL(R_atimo,4),I_ovimo,REAL(R_isimo,4),
     & REAL(R_osimo,4),REAL(R_orimo,4),
     & REAL(R_irimo,4),REAL(R_itimo,4),L_otimo,REAL(R_utimo
     &,4),L_avimo,
     & L_evimo,R_usimo,REAL(R_esimo,4),REAL(R_asimo,4),L_ivimo
     &)
      !}
C SRG_vlv.fgi(  84, 616):���������� ������� ��� 2,20SRG10CF030XQ01
      R_obomo = R8_o
C SRG_init_1.fgi(  50,  37):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_iximo,R_ifomo,REAL(16.666
     &,4),
     & REAL(R_obomo,4),I_efomo,REAL(R_abomo,4),
     & REAL(R_ebomo,4),REAL(R_eximo,4),
     & REAL(R_aximo,4),REAL(R_adomo,4),L_edomo,REAL(R_idomo
     &,4),L_odomo,
     & L_udomo,R_ibomo,REAL(R_uximo,4),REAL(R_oximo,4),L_afomo
     &)
      !}
C SRG_vlv.fgi(  84, 630):���������� ������� ��� 2,20SRG10CF029XQ01
      R_asili = R8_u
C SRG_init_1.fgi( 224,  41):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_upili,R_utili,REAL(16.666
     &,4),
     & REAL(R_asili,4),I_otili,REAL(R_irili,4),
     & REAL(R_orili,4),REAL(R_opili,4),
     & REAL(R_ipili,4),REAL(R_isili,4),L_osili,REAL(R_usili
     &,4),L_atili,
     & L_etili,R_urili,REAL(R_erili,4),REAL(R_arili,4),L_itili
     &)
      !}
C SRG_vlv.fgi( 700, 517):���������� ������� ��� 2,20SRG10CF061XQ01
      R_esore = R8_ad
C SRG_init_1.fgi( 138,  41):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_arore,R_avore,REAL(16.666
     &,4),
     & REAL(R_esore,4),I_utore,REAL(R_orore,4),
     & REAL(R_urore,4),REAL(R_upore,4),
     & REAL(R_opore,4),REAL(R_osore,4),L_usore,REAL(R_atore
     &,4),L_etore,
     & L_itore,R_asore,REAL(R_irore,4),REAL(R_erore,4),L_otore
     &)
      !}
C SRG_vlv.fgi( 428, 290):���������� ������� ��� 2,20SRG10CF056XQ01
      R_ubori = R8_ed
C SRG_init_1.fgi(  50,  41):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_oxiri,R_ofori,REAL(16.666
     &,4),
     & REAL(R_ubori,4),I_ifori,REAL(R_ebori,4),
     & REAL(R_ibori,4),REAL(R_ixiri,4),
     & REAL(R_exiri,4),REAL(R_edori,4),L_idori,REAL(R_odori
     &,4),L_udori,
     & L_afori,R_obori,REAL(R_abori,4),REAL(R_uxiri,4),L_efori
     &)
      !}
C SRG_vlv.fgi( 502, 517):���������� ������� ��� 2,20SRG10CF051XQ01
      R_opipo = R8_id
C SRG_init_1.fgi(  50,  45):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_imipo,R_isipo,REAL(1
     &,4),
     & REAL(R_opipo,4),I_esipo,REAL(R_apipo,4),
     & REAL(R_epipo,4),REAL(R_emipo,4),
     & REAL(R_amipo,4),REAL(R_aripo,4),L_eripo,REAL(R_iripo
     &,4),L_oripo,
     & L_uripo,R_ipipo,REAL(R_umipo,4),REAL(R_omipo,4),L_asipo
     &)
      !}
C SRG_vlv.fgi( 112, 224):���������� ������� ��� 2,20SRG10CQ018XQ01
      Call REG_RAS_MAN(deltat,.TRUE.,L_oxof,REAL(R_ixof,4
     &),REAL(R_osof,4),R_abuf,
     & REAL(R_urof,4),R_uxof,R_obuf,R_ibuf,R_asof,REAL(R_isof
     &,4),
     & R_opof,REAL(R_arof,4),R_erof,REAL(R_orof,4),L_esof
     &,
     & R_aduf,R_eduf,L_ebuf,L_upof,L_irof,I_ovof,R_iduf,R_ivof
     &,
     & R_ubuf,R8_evof,REAL(100000,4),REAL(R_ipipo,4),REAL
     &(R_usof,4),
     & REAL(R_exof,4),REAL(R_axof,4),REAL(R_uvof,4))
C SRG_vlv.fgi( 723, 329):���������� ���������� �������,20SRG10AA218
      R_udapo = R8_od
C SRG_init_1.fgi(  50,  49):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_obapo,R_okapo,REAL(1e
     &-4,4),
     & REAL(R_udapo,4),I_ikapo,REAL(R_edapo,4),
     & REAL(R_idapo,4),REAL(R_ibapo,4),
     & REAL(R_ebapo,4),REAL(R_efapo,4),L_ifapo,REAL(R_ofapo
     &,4),L_ufapo,
     & L_akapo,R_odapo,REAL(R_adapo,4),REAL(R_ubapo,4),L_ekapo
     &)
      !}
C SRG_vlv.fgi( 366, 630):���������� ������� ��� 2,20SRG10CQ015XQ04
      R_imapo = R8_ud
C SRG_init_1.fgi(  50,  53):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_elapo,R_erapo,REAL(1e
     &-4,4),
     & REAL(R_imapo,4),I_arapo,REAL(R_ulapo,4),
     & REAL(R_amapo,4),REAL(R_alapo,4),
     & REAL(R_ukapo,4),REAL(R_umapo,4),L_apapo,REAL(R_epapo
     &,4),L_ipapo,
     & L_opapo,R_emapo,REAL(R_olapo,4),REAL(R_ilapo,4),L_upapo
     &)
      !}
C SRG_vlv.fgi( 366, 644):���������� ������� ��� 2,20SRG10CQ015XQ03
      R_atapo = R8_af
C SRG_init_1.fgi(  50,  57):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_urapo,R_uvapo,REAL(1e
     &-4,4),
     & REAL(R_atapo,4),I_ovapo,REAL(R_isapo,4),
     & REAL(R_osapo,4),REAL(R_orapo,4),
     & REAL(R_irapo,4),REAL(R_itapo,4),L_otapo,REAL(R_utapo
     &,4),L_avapo,
     & L_evapo,R_usapo,REAL(R_esapo,4),REAL(R_asapo,4),L_ivapo
     &)
      !}
C SRG_vlv.fgi( 366, 658):���������� ������� ��� 2,20SRG10CQ015XQ01
      R_imopo = R8_ef
C SRG_init_1.fgi(  50,  61):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_elopo,R_eropo,REAL(1
     &,4),
     & REAL(R_imopo,4),I_aropo,REAL(R_ulopo,4),
     & REAL(R_amopo,4),REAL(R_alopo,4),
     & REAL(R_ukopo,4),REAL(R_umopo,4),L_apopo,REAL(R_epopo
     &,4),L_ipopo,
     & L_opopo,R_emopo,REAL(R_olopo,4),REAL(R_ilopo,4),L_upopo
     &)
      !}
C SRG_vlv.fgi( 112, 266):���������� ������� ��� 2,20SRG10CM015XQ01
      R_ixepo = R8_if
C SRG_init_1.fgi( 224,  45):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_evepo,R_edipo,REAL(1
     &,4),
     & REAL(R_ixepo,4),I_adipo,REAL(R_uvepo,4),
     & REAL(R_axepo,4),REAL(R_avepo,4),
     & REAL(R_utepo,4),REAL(R_uxepo,4),L_abipo,REAL(R_ebipo
     &,4),L_ibipo,
     & L_obipo,R_exepo,REAL(R_ovepo,4),REAL(R_ivepo,4),L_ubipo
     &)
      !}
C SRG_vlv.fgi( 112, 196):���������� ������� ��� 2,20SRG10CQ020XQ01
      Call REG_RAS_MAN(deltat,.TRUE.,L_usif,REAL(R_osif,4
     &),REAL(R_umif,4),R_etif,
     & REAL(R_amif,4),R_atif,R_utif,R_otif,R_emif,REAL(R_omif
     &,4),
     & R_ukif,REAL(R_elif,4),R_ilif,REAL(R_ulif,4),L_imif
     &,
     & R_evif,R_ivif,L_itif,L_alif,L_olif,I_urif,R_ovif,R_orif
     &,
     & R_avif,R8_irif,REAL(100000,4),REAL(R_exepo,4),REAL
     &(R_apif,4),
     & REAL(R_isif,4),REAL(R_esif,4),REAL(R_asif,4))
C SRG_vlv.fgi( 724, 277):���������� ���������� �������,20SRG10AA222
      R_elomo = R8_of
C SRG_init_1.fgi( 224,  49):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_akomo,R_apomo,REAL(1e
     &-4,4),
     & REAL(R_elomo,4),I_umomo,REAL(R_okomo,4),
     & REAL(R_ukomo,4),REAL(R_ufomo,4),
     & REAL(R_ofomo,4),REAL(R_olomo,4),L_ulomo,REAL(R_amomo
     &,4),L_emomo,
     & L_imomo,R_alomo,REAL(R_ikomo,4),REAL(R_ekomo,4),L_omomo
     &)
      !}
C SRG_vlv.fgi( 366, 546):���������� ������� ��� 2,20SRG10CQ017XQ04
      R_uromo = R8_uf
C SRG_init_1.fgi( 224,  53):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_opomo,R_otomo,REAL(1e
     &-4,4),
     & REAL(R_uromo,4),I_itomo,REAL(R_eromo,4),
     & REAL(R_iromo,4),REAL(R_ipomo,4),
     & REAL(R_epomo,4),REAL(R_esomo,4),L_isomo,REAL(R_osomo
     &,4),L_usomo,
     & L_atomo,R_oromo,REAL(R_aromo,4),REAL(R_upomo,4),L_etomo
     &)
      !}
C SRG_vlv.fgi( 366, 560):���������� ������� ��� 2,20SRG10CQ017XQ03
      R_ixomo = R8_ak
C SRG_init_1.fgi( 224,  57):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_evomo,R_edumo,REAL(1e
     &-4,4),
     & REAL(R_ixomo,4),I_adumo,REAL(R_uvomo,4),
     & REAL(R_axomo,4),REAL(R_avomo,4),
     & REAL(R_utomo,4),REAL(R_uxomo,4),L_abumo,REAL(R_ebumo
     &,4),L_ibumo,
     & L_obumo,R_exomo,REAL(R_ovomo,4),REAL(R_ivomo,4),L_ubumo
     &)
      !}
C SRG_vlv.fgi( 366, 574):���������� ������� ��� 2,20SRG10CQ017XQ01
      R_evipo = R8_ek
C SRG_init_1.fgi( 224,  61):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_atipo,R_abopo,REAL(1
     &,4),
     & REAL(R_evipo,4),I_uxipo,REAL(R_otipo,4),
     & REAL(R_utipo,4),REAL(R_usipo,4),
     & REAL(R_osipo,4),REAL(R_ovipo,4),L_uvipo,REAL(R_axipo
     &,4),L_exipo,
     & L_ixipo,R_avipo,REAL(R_itipo,4),REAL(R_etipo,4),L_oxipo
     &)
      !}
C SRG_vlv.fgi( 112, 238):���������� ������� ��� 2,20SRG10CM017XQ01
      R_akipo = R8_ik
C SRG_init_1.fgi( 138,  45):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_udipo,R_ulipo,REAL(1
     &,4),
     & REAL(R_akipo,4),I_olipo,REAL(R_ifipo,4),
     & REAL(R_ofipo,4),REAL(R_odipo,4),
     & REAL(R_idipo,4),REAL(R_ikipo,4),L_okipo,REAL(R_ukipo
     &,4),L_alipo,
     & L_elipo,R_ufipo,REAL(R_efipo,4),REAL(R_afipo,4),L_ilipo
     &)
      !}
C SRG_vlv.fgi( 112, 210):���������� ������� ��� 2,20SRG10CQ019XQ01
      Call REG_RAS_MAN(deltat,.TRUE.,L_elof,REAL(R_alof,4
     &),REAL(R_edof,4),R_olof,
     & REAL(R_ibof,4),R_ilof,R_emof,R_amof,R_obof,REAL(R_adof
     &,4),
     & R_exif,REAL(R_oxif,4),R_uxif,REAL(R_ebof,4),L_ubof
     &,
     & R_omof,R_umof,L_ulof,L_ixif,L_abof,I_ekof,R_apof,R_akof
     &,
     & R_imof,R8_ufof,REAL(100000,4),REAL(R_ufipo,4),REAL
     &(R_idof,4),
     & REAL(R_ukof,4),REAL(R_okof,4),REAL(R_ikof,4))
C SRG_vlv.fgi( 723, 303):���������� ���������� �������,20SRG10AA220
      R_akumo = R8_ok
C SRG_init_1.fgi( 138,  49):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_udumo,R_ulumo,REAL(1e
     &-4,4),
     & REAL(R_akumo,4),I_olumo,REAL(R_ifumo,4),
     & REAL(R_ofumo,4),REAL(R_odumo,4),
     & REAL(R_idumo,4),REAL(R_ikumo,4),L_okumo,REAL(R_ukumo
     &,4),L_alumo,
     & L_elumo,R_ufumo,REAL(R_efumo,4),REAL(R_afumo,4),L_ilumo
     &)
      !}
C SRG_vlv.fgi( 366, 588):���������� ������� ��� 2,20SRG10CQ016XQ04
      R_opumo = R8_uk
C SRG_init_1.fgi( 138,  53):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_imumo,R_isumo,REAL(1
     &,4),
     & REAL(R_opumo,4),I_esumo,REAL(R_apumo,4),
     & REAL(R_epumo,4),REAL(R_emumo,4),
     & REAL(R_amumo,4),REAL(R_arumo,4),L_erumo,REAL(R_irumo
     &,4),L_orumo,
     & L_urumo,R_ipumo,REAL(R_umumo,4),REAL(R_omumo,4),L_asumo
     &)
      !}
C SRG_vlv.fgi( 366, 602):���������� ������� ��� 2,20SRG10CQ016XQ03
      R_evumo = R8_al
C SRG_init_1.fgi( 138,  57):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_atumo,R_abapo,REAL(1e
     &-4,4),
     & REAL(R_evumo,4),I_uxumo,REAL(R_otumo,4),
     & REAL(R_utumo,4),REAL(R_usumo,4),
     & REAL(R_osumo,4),REAL(R_ovumo,4),L_uvumo,REAL(R_axumo
     &,4),L_exumo,
     & L_ixumo,R_avumo,REAL(R_itumo,4),REAL(R_etumo,4),L_oxumo
     &)
      !}
C SRG_vlv.fgi( 366, 616):���������� ������� ��� 2,20SRG10CQ016XQ01
      R_udopo = R8_el
C SRG_init_1.fgi( 138,  61):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_obopo,R_okopo,REAL(1
     &,4),
     & REAL(R_udopo,4),I_ikopo,REAL(R_edopo,4),
     & REAL(R_idopo,4),REAL(R_ibopo,4),
     & REAL(R_ebopo,4),REAL(R_efopo,4),L_ifopo,REAL(R_ofopo
     &,4),L_ufopo,
     & L_akopo,R_odopo,REAL(R_adopo,4),REAL(R_ubopo,4),L_ekopo
     &)
      !}
C SRG_vlv.fgi( 112, 252):���������� ������� ��� 2,20SRG10CM016XQ01
      R_aroli = R8_il
C SRG_init_1.fgi( 224,  65):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_umoli,R_usoli,REAL(1
     &,4),
     & REAL(R_aroli,4),I_osoli,REAL(R_ipoli,4),
     & REAL(R_opoli,4),REAL(R_omoli,4),
     & REAL(R_imoli,4),REAL(R_iroli,4),L_oroli,REAL(R_uroli
     &,4),L_asoli,
     & L_esoli,R_upoli,REAL(R_epoli,4),REAL(R_apoli,4),L_isoli
     &)
      !}
C SRG_vlv.fgi( 676, 439):���������� ������� ��� 2,20SRG10CU045XQ02
      R_ikoli = R8_ol
C SRG_init_1.fgi( 224,  69):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_efoli,R_emoli,REAL(60
     &,4),
     & REAL(R_ikoli,4),I_amoli,REAL(R_ufoli,4),
     & REAL(R_akoli,4),REAL(R_afoli,4),
     & REAL(R_udoli,4),REAL(R_ukoli,4),L_aloli,REAL(R_eloli
     &,4),L_iloli,
     & L_ololi,R_ekoli,REAL(R_ofoli,4),REAL(R_ifoli,4),L_uloli
     &)
      !}
C SRG_vlv.fgi( 700, 439):���������� ������� ��� 2,20SRG10CU045XQ01
      R_efuli = R8_ul
C SRG_init_1.fgi( 224,  73):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_aduli,R_aluli,REAL(1
     &,4),
     & REAL(R_efuli,4),I_ukuli,REAL(R_oduli,4),
     & REAL(R_uduli,4),REAL(R_ubuli,4),
     & REAL(R_obuli,4),REAL(R_ofuli,4),L_ufuli,REAL(R_akuli
     &,4),L_ekuli,
     & L_ikuli,R_afuli,REAL(R_iduli,4),REAL(R_eduli,4),L_okuli
     &)
      !}
C SRG_vlv.fgi( 676, 452):���������� ������� ��� 2,20SRG10CU044XQ02
      R_ovoli = R8_am
C SRG_init_1.fgi( 224,  77):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_itoli,R_ibuli,REAL(60
     &,4),
     & REAL(R_ovoli,4),I_ebuli,REAL(R_avoli,4),
     & REAL(R_evoli,4),REAL(R_etoli,4),
     & REAL(R_atoli,4),REAL(R_axoli,4),L_exoli,REAL(R_ixoli
     &,4),L_oxoli,
     & L_uxoli,R_ivoli,REAL(R_utoli,4),REAL(R_otoli,4),L_abuli
     &)
      !}
C SRG_vlv.fgi( 700, 452):���������� ������� ��� 2,20SRG10CU044XQ01
      R_ituli = R8_em
C SRG_init_1.fgi( 224,  81):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_esuli,R_exuli,REAL(1
     &,4),
     & REAL(R_ituli,4),I_axuli,REAL(R_usuli,4),
     & REAL(R_atuli,4),REAL(R_asuli,4),
     & REAL(R_uruli,4),REAL(R_utuli,4),L_avuli,REAL(R_evuli
     &,4),L_ivuli,
     & L_ovuli,R_etuli,REAL(R_osuli,4),REAL(R_isuli,4),L_uvuli
     &)
      !}
C SRG_vlv.fgi( 676, 465):���������� ������� ��� 2,20SRG10CU043XQ02
      R_umuli = R8_im
C SRG_init_1.fgi( 224,  85):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_oluli,R_oruli,REAL(60
     &,4),
     & REAL(R_umuli,4),I_iruli,REAL(R_emuli,4),
     & REAL(R_imuli,4),REAL(R_iluli,4),
     & REAL(R_eluli,4),REAL(R_epuli,4),L_ipuli,REAL(R_opuli
     &,4),L_upuli,
     & L_aruli,R_omuli,REAL(R_amuli,4),REAL(R_ululi,4),L_eruli
     &)
      !}
C SRG_vlv.fgi( 700, 465):���������� ������� ��� 2,20SRG10CU043XQ01
      R_olami = R8_om
C SRG_init_1.fgi( 224,  89):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ikami,R_ipami,REAL(1
     &,4),
     & REAL(R_olami,4),I_epami,REAL(R_alami,4),
     & REAL(R_elami,4),REAL(R_ekami,4),
     & REAL(R_akami,4),REAL(R_amami,4),L_emami,REAL(R_imami
     &,4),L_omami,
     & L_umami,R_ilami,REAL(R_ukami,4),REAL(R_okami,4),L_apami
     &)
      !}
C SRG_vlv.fgi( 676, 478):���������� ������� ��� 2,20SRG10CU042XQ02
      R_adami = R8_um
C SRG_init_1.fgi( 224,  93):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_uxuli,R_ufami,REAL(60
     &,4),
     & REAL(R_adami,4),I_ofami,REAL(R_ibami,4),
     & REAL(R_obami,4),REAL(R_oxuli,4),
     & REAL(R_ixuli,4),REAL(R_idami,4),L_odami,REAL(R_udami
     &,4),L_afami,
     & L_efami,R_ubami,REAL(R_ebami,4),REAL(R_abami,4),L_ifami
     &)
      !}
C SRG_vlv.fgi( 700, 478):���������� ������� ��� 2,20SRG10CU042XQ01
      R_uxami = R8_ap
C SRG_init_1.fgi( 224,  97):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ovami,R_odemi,REAL(1
     &,4),
     & REAL(R_uxami,4),I_idemi,REAL(R_exami,4),
     & REAL(R_ixami,4),REAL(R_ivami,4),
     & REAL(R_evami,4),REAL(R_ebemi,4),L_ibemi,REAL(R_obemi
     &,4),L_ubemi,
     & L_ademi,R_oxami,REAL(R_axami,4),REAL(R_uvami,4),L_edemi
     &)
      !}
C SRG_vlv.fgi( 676, 491):���������� ������� ��� 2,20SRG10CU041XQ02
      R_esami = R8_ep
C SRG_init_1.fgi( 224, 101):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_arami,R_avami,REAL(60
     &,4),
     & REAL(R_esami,4),I_utami,REAL(R_orami,4),
     & REAL(R_urami,4),REAL(R_upami,4),
     & REAL(R_opami,4),REAL(R_osami,4),L_usami,REAL(R_atami
     &,4),L_etami,
     & L_itami,R_asami,REAL(R_irami,4),REAL(R_erami,4),L_otami
     &)
      !}
C SRG_vlv.fgi( 700, 491):���������� ������� ��� 2,20SRG10CU041XQ01
      R_aremi = R8_ip
C SRG_init_1.fgi( 224, 105):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_umemi,R_usemi,REAL(1
     &,4),
     & REAL(R_aremi,4),I_osemi,REAL(R_ipemi,4),
     & REAL(R_opemi,4),REAL(R_omemi,4),
     & REAL(R_imemi,4),REAL(R_iremi,4),L_oremi,REAL(R_uremi
     &,4),L_asemi,
     & L_esemi,R_upemi,REAL(R_epemi,4),REAL(R_apemi,4),L_isemi
     &)
      !}
C SRG_vlv.fgi( 626, 439):���������� ������� ��� 2,20SRG10CU040XQ02
      R_ikemi = R8_op
C SRG_init_1.fgi( 224, 109):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_efemi,R_ememi,REAL(60
     &,4),
     & REAL(R_ikemi,4),I_amemi,REAL(R_ufemi,4),
     & REAL(R_akemi,4),REAL(R_afemi,4),
     & REAL(R_udemi,4),REAL(R_ukemi,4),L_alemi,REAL(R_elemi
     &,4),L_ilemi,
     & L_olemi,R_ekemi,REAL(R_ofemi,4),REAL(R_ifemi,4),L_ulemi
     &)
      !}
C SRG_vlv.fgi( 650, 439):���������� ������� ��� 2,20SRG10CU040XQ01
      R_efimi = R8_up
C SRG_init_1.fgi( 224, 113):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_adimi,R_alimi,REAL(1
     &,4),
     & REAL(R_efimi,4),I_ukimi,REAL(R_odimi,4),
     & REAL(R_udimi,4),REAL(R_ubimi,4),
     & REAL(R_obimi,4),REAL(R_ofimi,4),L_ufimi,REAL(R_akimi
     &,4),L_ekimi,
     & L_ikimi,R_afimi,REAL(R_idimi,4),REAL(R_edimi,4),L_okimi
     &)
      !}
C SRG_vlv.fgi( 626, 452):���������� ������� ��� 2,20SRG10CU039XQ02
      R_ovemi = R8_ar
C SRG_init_1.fgi( 224, 117):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_itemi,R_ibimi,REAL(60
     &,4),
     & REAL(R_ovemi,4),I_ebimi,REAL(R_avemi,4),
     & REAL(R_evemi,4),REAL(R_etemi,4),
     & REAL(R_atemi,4),REAL(R_axemi,4),L_exemi,REAL(R_ixemi
     &,4),L_oxemi,
     & L_uxemi,R_ivemi,REAL(R_utemi,4),REAL(R_otemi,4),L_abimi
     &)
      !}
C SRG_vlv.fgi( 650, 452):���������� ������� ��� 2,20SRG10CU039XQ01
      R_itimi = R8_er
C SRG_init_1.fgi( 224, 121):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_esimi,R_eximi,REAL(1
     &,4),
     & REAL(R_itimi,4),I_aximi,REAL(R_usimi,4),
     & REAL(R_atimi,4),REAL(R_asimi,4),
     & REAL(R_urimi,4),REAL(R_utimi,4),L_avimi,REAL(R_evimi
     &,4),L_ivimi,
     & L_ovimi,R_etimi,REAL(R_osimi,4),REAL(R_isimi,4),L_uvimi
     &)
      !}
C SRG_vlv.fgi( 626, 465):���������� ������� ��� 2,20SRG10CU038XQ02
      R_umimi = R8_ir
C SRG_init_1.fgi( 224, 125):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_olimi,R_orimi,REAL(60
     &,4),
     & REAL(R_umimi,4),I_irimi,REAL(R_emimi,4),
     & REAL(R_imimi,4),REAL(R_ilimi,4),
     & REAL(R_elimi,4),REAL(R_epimi,4),L_ipimi,REAL(R_opimi
     &,4),L_upimi,
     & L_arimi,R_omimi,REAL(R_amimi,4),REAL(R_ulimi,4),L_erimi
     &)
      !}
C SRG_vlv.fgi( 650, 465):���������� ������� ��� 2,20SRG10CU038XQ01
      R_olomi = R8_or
C SRG_init_1.fgi( 224, 129):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ikomi,R_ipomi,REAL(1
     &,4),
     & REAL(R_olomi,4),I_epomi,REAL(R_alomi,4),
     & REAL(R_elomi,4),REAL(R_ekomi,4),
     & REAL(R_akomi,4),REAL(R_amomi,4),L_emomi,REAL(R_imomi
     &,4),L_omomi,
     & L_umomi,R_ilomi,REAL(R_ukomi,4),REAL(R_okomi,4),L_apomi
     &)
      !}
C SRG_vlv.fgi( 626, 478):���������� ������� ��� 2,20SRG10CU037XQ02
      R_adomi = R8_ur
C SRG_init_1.fgi( 224, 133):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_uximi,R_ufomi,REAL(60
     &,4),
     & REAL(R_adomi,4),I_ofomi,REAL(R_ibomi,4),
     & REAL(R_obomi,4),REAL(R_oximi,4),
     & REAL(R_iximi,4),REAL(R_idomi,4),L_odomi,REAL(R_udomi
     &,4),L_afomi,
     & L_efomi,R_ubomi,REAL(R_ebomi,4),REAL(R_abomi,4),L_ifomi
     &)
      !}
C SRG_vlv.fgi( 650, 478):���������� ������� ��� 2,20SRG10CU037XQ01
      R_uxomi = R8_as
C SRG_init_1.fgi( 224, 137):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ovomi,R_odumi,REAL(1
     &,4),
     & REAL(R_uxomi,4),I_idumi,REAL(R_exomi,4),
     & REAL(R_ixomi,4),REAL(R_ivomi,4),
     & REAL(R_evomi,4),REAL(R_ebumi,4),L_ibumi,REAL(R_obumi
     &,4),L_ubumi,
     & L_adumi,R_oxomi,REAL(R_axomi,4),REAL(R_uvomi,4),L_edumi
     &)
      !}
C SRG_vlv.fgi( 626, 491):���������� ������� ��� 2,20SRG10CU036XQ02
      R_esomi = R8_es
C SRG_init_1.fgi( 224, 141):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_aromi,R_avomi,REAL(60
     &,4),
     & REAL(R_esomi,4),I_utomi,REAL(R_oromi,4),
     & REAL(R_uromi,4),REAL(R_upomi,4),
     & REAL(R_opomi,4),REAL(R_osomi,4),L_usomi,REAL(R_atomi
     &,4),L_etomi,
     & L_itomi,R_asomi,REAL(R_iromi,4),REAL(R_eromi,4),L_otomi
     &)
      !}
C SRG_vlv.fgi( 650, 491):���������� ������� ��� 2,20SRG10CU036XQ01
      R_arumi = R8_is
C SRG_init_1.fgi( 224, 145):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_umumi,R_usumi,REAL(1
     &,4),
     & REAL(R_arumi,4),I_osumi,REAL(R_ipumi,4),
     & REAL(R_opumi,4),REAL(R_omumi,4),
     & REAL(R_imumi,4),REAL(R_irumi,4),L_orumi,REAL(R_urumi
     &,4),L_asumi,
     & L_esumi,R_upumi,REAL(R_epumi,4),REAL(R_apumi,4),L_isumi
     &)
      !}
C SRG_vlv.fgi( 700, 504):���������� ������� ��� 2,20SRG10CU035XQ02
      R_ikumi = R8_os
C SRG_init_1.fgi( 224, 149):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_efumi,R_emumi,REAL(60
     &,4),
     & REAL(R_ikumi,4),I_amumi,REAL(R_ufumi,4),
     & REAL(R_akumi,4),REAL(R_afumi,4),
     & REAL(R_udumi,4),REAL(R_ukumi,4),L_alumi,REAL(R_elumi
     &,4),L_ilumi,
     & L_olumi,R_ekumi,REAL(R_ofumi,4),REAL(R_ifumi,4),L_ulumi
     &)
      !}
C SRG_vlv.fgi( 726, 504):���������� ������� ��� 2,20SRG10CU035XQ01
      R_efapi = R8_us
C SRG_init_1.fgi( 224, 153):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_adapi,R_alapi,REAL(1
     &,4),
     & REAL(R_efapi,4),I_ukapi,REAL(R_odapi,4),
     & REAL(R_udapi,4),REAL(R_ubapi,4),
     & REAL(R_obapi,4),REAL(R_ofapi,4),L_ufapi,REAL(R_akapi
     &,4),L_ekapi,
     & L_ikapi,R_afapi,REAL(R_idapi,4),REAL(R_edapi,4),L_okapi
     &)
      !}
C SRG_vlv.fgi( 726, 517):���������� ������� ��� 2,20SRG10CU034XQ02
      R_ovumi = R8_at
C SRG_init_1.fgi( 224, 157):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_itumi,R_ibapi,REAL(60
     &,4),
     & REAL(R_ovumi,4),I_ebapi,REAL(R_avumi,4),
     & REAL(R_evumi,4),REAL(R_etumi,4),
     & REAL(R_atumi,4),REAL(R_axumi,4),L_exumi,REAL(R_ixumi
     &,4),L_oxumi,
     & L_uxumi,R_ivumi,REAL(R_utumi,4),REAL(R_otumi,4),L_abapi
     &)
      !}
C SRG_vlv.fgi( 726, 452):���������� ������� ��� 2,20SRG10CU034XQ01
      R_itapi = R8_et
C SRG_init_1.fgi( 224, 161):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_esapi,R_exapi,REAL(1
     &,4),
     & REAL(R_itapi,4),I_axapi,REAL(R_usapi,4),
     & REAL(R_atapi,4),REAL(R_asapi,4),
     & REAL(R_urapi,4),REAL(R_utapi,4),L_avapi,REAL(R_evapi
     &,4),L_ivapi,
     & L_ovapi,R_etapi,REAL(R_osapi,4),REAL(R_isapi,4),L_uvapi
     &)
      !}
C SRG_vlv.fgi( 726, 530):���������� ������� ��� 2,20SRG10CU033XQ02
      R_umapi = R8_it
C SRG_init_1.fgi( 224, 165):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_olapi,R_orapi,REAL(60
     &,4),
     & REAL(R_umapi,4),I_irapi,REAL(R_emapi,4),
     & REAL(R_imapi,4),REAL(R_ilapi,4),
     & REAL(R_elapi,4),REAL(R_epapi,4),L_ipapi,REAL(R_opapi
     &,4),L_upapi,
     & L_arapi,R_omapi,REAL(R_amapi,4),REAL(R_ulapi,4),L_erapi
     &)
      !}
C SRG_vlv.fgi( 726, 465):���������� ������� ��� 2,20SRG10CU033XQ01
      R_olepi = R8_ot
C SRG_init_1.fgi( 224, 169):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ikepi,R_ipepi,REAL(1
     &,4),
     & REAL(R_olepi,4),I_epepi,REAL(R_alepi,4),
     & REAL(R_elepi,4),REAL(R_ekepi,4),
     & REAL(R_akepi,4),REAL(R_amepi,4),L_emepi,REAL(R_imepi
     &,4),L_omepi,
     & L_umepi,R_ilepi,REAL(R_ukepi,4),REAL(R_okepi,4),L_apepi
     &)
      !}
C SRG_vlv.fgi( 726, 543):���������� ������� ��� 2,20SRG10CU032XQ02
      R_adepi = R8_ut
C SRG_init_1.fgi( 224, 173):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_uxapi,R_ufepi,REAL(60
     &,4),
     & REAL(R_adepi,4),I_ofepi,REAL(R_ibepi,4),
     & REAL(R_obepi,4),REAL(R_oxapi,4),
     & REAL(R_ixapi,4),REAL(R_idepi,4),L_odepi,REAL(R_udepi
     &,4),L_afepi,
     & L_efepi,R_ubepi,REAL(R_ebepi,4),REAL(R_abepi,4),L_ifepi
     &)
      !}
C SRG_vlv.fgi( 726, 478):���������� ������� ��� 2,20SRG10CU032XQ01
      R_uxepi = R8_av
C SRG_init_1.fgi( 224, 177):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ovepi,R_odipi,REAL(1
     &,4),
     & REAL(R_uxepi,4),I_idipi,REAL(R_exepi,4),
     & REAL(R_ixepi,4),REAL(R_ivepi,4),
     & REAL(R_evepi,4),REAL(R_ebipi,4),L_ibipi,REAL(R_obipi
     &,4),L_ubipi,
     & L_adipi,R_oxepi,REAL(R_axepi,4),REAL(R_uvepi,4),L_edipi
     &)
      !}
C SRG_vlv.fgi( 700, 556):���������� ������� ��� 2,20SRG10CU031XQ02
      R_esepi = R8_ev
C SRG_init_1.fgi( 224, 181):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_arepi,R_avepi,REAL(60
     &,4),
     & REAL(R_esepi,4),I_utepi,REAL(R_orepi,4),
     & REAL(R_urepi,4),REAL(R_upepi,4),
     & REAL(R_opepi,4),REAL(R_osepi,4),L_usepi,REAL(R_atepi
     &,4),L_etepi,
     & L_itepi,R_asepi,REAL(R_irepi,4),REAL(R_erepi,4),L_otepi
     &)
      !}
C SRG_vlv.fgi( 726, 491):���������� ������� ��� 2,20SRG10CU031XQ01
      R_axape = R8_iv
C SRG_init_1.fgi( 138,  65):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_utape,R_ubepe,REAL(1
     &,4),
     & REAL(R_axape,4),I_obepe,REAL(R_ivape,4),
     & REAL(R_ovape,4),REAL(R_otape,4),
     & REAL(R_itape,4),REAL(R_ixape,4),L_oxape,REAL(R_uxape
     &,4),L_abepe,
     & L_ebepe,R_uvape,REAL(R_evape,4),REAL(R_avape,4),L_ibepe
     &)
      !}
C SRG_vlv.fgi( 428, 185):���������� ������� ��� 2,20SRG10CU030XQ02
      R_ebime = R8_ov
C SRG_init_1.fgi( 138,  69):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_axeme,R_afime,REAL(60
     &,4),
     & REAL(R_ebime,4),I_udime,REAL(R_oxeme,4),
     & REAL(R_uxeme,4),REAL(R_uveme,4),
     & REAL(R_oveme,4),REAL(R_obime,4),L_ubime,REAL(R_adime
     &,4),L_edime,
     & L_idime,R_abime,REAL(R_ixeme,4),REAL(R_exeme,4),L_odime
     &)
      !}
C SRG_vlv.fgi( 452, 185):���������� ������� ��� 2,20SRG10CU030XQ01
      R_ofepe = R8_uv
C SRG_init_1.fgi( 138,  73):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_idepe,R_ilepe,REAL(1
     &,4),
     & REAL(R_ofepe,4),I_elepe,REAL(R_afepe,4),
     & REAL(R_efepe,4),REAL(R_edepe,4),
     & REAL(R_adepe,4),REAL(R_akepe,4),L_ekepe,REAL(R_ikepe
     &,4),L_okepe,
     & L_ukepe,R_ifepe,REAL(R_udepe,4),REAL(R_odepe,4),L_alepe
     &)
      !}
C SRG_vlv.fgi( 428, 198):���������� ������� ��� 2,20SRG10CU029XQ02
      R_ukime = R8_ax
C SRG_init_1.fgi( 138,  77):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ofime,R_omime,REAL(60
     &,4),
     & REAL(R_ukime,4),I_imime,REAL(R_ekime,4),
     & REAL(R_ikime,4),REAL(R_ifime,4),
     & REAL(R_efime,4),REAL(R_elime,4),L_ilime,REAL(R_olime
     &,4),L_ulime,
     & L_amime,R_okime,REAL(R_akime,4),REAL(R_ufime,4),L_emime
     &)
      !}
C SRG_vlv.fgi( 452, 198):���������� ������� ��� 2,20SRG10CU029XQ01
      R_epepe = R8_ex
C SRG_init_1.fgi( 138,  81):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_amepe,R_asepe,REAL(1
     &,4),
     & REAL(R_epepe,4),I_urepe,REAL(R_omepe,4),
     & REAL(R_umepe,4),REAL(R_ulepe,4),
     & REAL(R_olepe,4),REAL(R_opepe,4),L_upepe,REAL(R_arepe
     &,4),L_erepe,
     & L_irepe,R_apepe,REAL(R_imepe,4),REAL(R_emepe,4),L_orepe
     &)
      !}
C SRG_vlv.fgi( 428, 211):���������� ������� ��� 2,20SRG10CU028XQ02
      R_irime = R8_ix
C SRG_init_1.fgi( 138,  85):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_epime,R_etime,REAL(60
     &,4),
     & REAL(R_irime,4),I_atime,REAL(R_upime,4),
     & REAL(R_arime,4),REAL(R_apime,4),
     & REAL(R_umime,4),REAL(R_urime,4),L_asime,REAL(R_esime
     &,4),L_isime,
     & L_osime,R_erime,REAL(R_opime,4),REAL(R_ipime,4),L_usime
     &)
      !}
C SRG_vlv.fgi( 452, 211):���������� ������� ��� 2,20SRG10CU028XQ01
      R_utepe = R8_ox
C SRG_init_1.fgi( 138,  89):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_osepe,R_oxepe,REAL(1
     &,4),
     & REAL(R_utepe,4),I_ixepe,REAL(R_etepe,4),
     & REAL(R_itepe,4),REAL(R_isepe,4),
     & REAL(R_esepe,4),REAL(R_evepe,4),L_ivepe,REAL(R_ovepe
     &,4),L_uvepe,
     & L_axepe,R_otepe,REAL(R_atepe,4),REAL(R_usepe,4),L_exepe
     &)
      !}
C SRG_vlv.fgi( 428, 224):���������� ������� ��� 2,20SRG10CU027XQ02
      R_axime = R8_ux
C SRG_init_1.fgi( 138,  93):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_utime,R_ubome,REAL(60
     &,4),
     & REAL(R_axime,4),I_obome,REAL(R_ivime,4),
     & REAL(R_ovime,4),REAL(R_otime,4),
     & REAL(R_itime,4),REAL(R_ixime,4),L_oxime,REAL(R_uxime
     &,4),L_abome,
     & L_ebome,R_uvime,REAL(R_evime,4),REAL(R_avime,4),L_ibome
     &)
      !}
C SRG_vlv.fgi( 452, 224):���������� ������� ��� 2,20SRG10CU027XQ01
      R_idipe = R8_abe
C SRG_init_1.fgi( 138,  97):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ebipe,R_ekipe,REAL(1
     &,4),
     & REAL(R_idipe,4),I_akipe,REAL(R_ubipe,4),
     & REAL(R_adipe,4),REAL(R_abipe,4),
     & REAL(R_uxepe,4),REAL(R_udipe,4),L_afipe,REAL(R_efipe
     &,4),L_ifipe,
     & L_ofipe,R_edipe,REAL(R_obipe,4),REAL(R_ibipe,4),L_ufipe
     &)
      !}
C SRG_vlv.fgi( 428, 237):���������� ������� ��� 2,20SRG10CU026XQ02
      R_ofome = R8_ebe
C SRG_init_1.fgi( 138, 101):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_idome,R_ilome,REAL(60
     &,4),
     & REAL(R_ofome,4),I_elome,REAL(R_afome,4),
     & REAL(R_efome,4),REAL(R_edome,4),
     & REAL(R_adome,4),REAL(R_akome,4),L_ekome,REAL(R_ikome
     &,4),L_okome,
     & L_ukome,R_ifome,REAL(R_udome,4),REAL(R_odome,4),L_alome
     &)
      !}
C SRG_vlv.fgi( 452, 237):���������� ������� ��� 2,20SRG10CU026XQ01
      R_amipe = R8_ibe
C SRG_init_1.fgi( 138, 105):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ukipe,R_upipe,REAL(1
     &,4),
     & REAL(R_amipe,4),I_opipe,REAL(R_ilipe,4),
     & REAL(R_olipe,4),REAL(R_okipe,4),
     & REAL(R_ikipe,4),REAL(R_imipe,4),L_omipe,REAL(R_umipe
     &,4),L_apipe,
     & L_epipe,R_ulipe,REAL(R_elipe,4),REAL(R_alipe,4),L_ipipe
     &)
      !}
C SRG_vlv.fgi( 428, 250):���������� ������� ��� 2,20SRG10CU025XQ02
      R_epome = R8_obe
C SRG_init_1.fgi( 138, 109):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_amome,R_asome,REAL(60
     &,4),
     & REAL(R_epome,4),I_urome,REAL(R_omome,4),
     & REAL(R_umome,4),REAL(R_ulome,4),
     & REAL(R_olome,4),REAL(R_opome,4),L_upome,REAL(R_arome
     &,4),L_erome,
     & L_irome,R_apome,REAL(R_imome,4),REAL(R_emome,4),L_orome
     &)
      !}
C SRG_vlv.fgi( 452, 250):���������� ������� ��� 2,20SRG10CU025XQ01
      R_osipe = R8_ube
C SRG_init_1.fgi( 138, 113):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_iripe,R_ivipe,REAL(1
     &,4),
     & REAL(R_osipe,4),I_evipe,REAL(R_asipe,4),
     & REAL(R_esipe,4),REAL(R_eripe,4),
     & REAL(R_aripe,4),REAL(R_atipe,4),L_etipe,REAL(R_itipe
     &,4),L_otipe,
     & L_utipe,R_isipe,REAL(R_uripe,4),REAL(R_oripe,4),L_avipe
     &)
      !}
C SRG_vlv.fgi( 428, 263):���������� ������� ��� 2,20SRG10CU024XQ02
      R_utome = R8_ade
C SRG_init_1.fgi( 138, 117):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_osome,R_oxome,REAL(60
     &,4),
     & REAL(R_utome,4),I_ixome,REAL(R_etome,4),
     & REAL(R_itome,4),REAL(R_isome,4),
     & REAL(R_esome,4),REAL(R_evome,4),L_ivome,REAL(R_ovome
     &,4),L_uvome,
     & L_axome,R_otome,REAL(R_atome,4),REAL(R_usome,4),L_exome
     &)
      !}
C SRG_vlv.fgi( 452, 263):���������� ������� ��� 2,20SRG10CU024XQ01
      R_ebope = R8_ede
C SRG_init_1.fgi( 138, 121):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_axipe,R_afope,REAL(1
     &,4),
     & REAL(R_ebope,4),I_udope,REAL(R_oxipe,4),
     & REAL(R_uxipe,4),REAL(R_uvipe,4),
     & REAL(R_ovipe,4),REAL(R_obope,4),L_ubope,REAL(R_adope
     &,4),L_edope,
     & L_idope,R_abope,REAL(R_ixipe,4),REAL(R_exipe,4),L_odope
     &)
      !}
C SRG_vlv.fgi( 428, 276):���������� ������� ��� 2,20SRG10CU023XQ02
      R_idume = R8_ide
C SRG_init_1.fgi( 138, 125):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ebume,R_ekume,REAL(60
     &,4),
     & REAL(R_idume,4),I_akume,REAL(R_ubume,4),
     & REAL(R_adume,4),REAL(R_abume,4),
     & REAL(R_uxome,4),REAL(R_udume,4),L_afume,REAL(R_efume
     &,4),L_ifume,
     & L_ofume,R_edume,REAL(R_obume,4),REAL(R_ibume,4),L_ufume
     &)
      !}
C SRG_vlv.fgi( 452, 276):���������� ������� ��� 2,20SRG10CU023XQ01
      R_ukope = R8_ode
C SRG_init_1.fgi( 138, 129):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ofope,R_omope,REAL(1
     &,4),
     & REAL(R_ukope,4),I_imope,REAL(R_ekope,4),
     & REAL(R_ikope,4),REAL(R_ifope,4),
     & REAL(R_efope,4),REAL(R_elope,4),L_ilope,REAL(R_olope
     &,4),L_ulope,
     & L_amope,R_okope,REAL(R_akope,4),REAL(R_ufope,4),L_emope
     &)
      !}
C SRG_vlv.fgi( 476, 198):���������� ������� ��� 2,20SRG10CU022XQ02
      R_amume = R8_ude
C SRG_init_1.fgi( 138, 133):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ukume,R_upume,REAL(60
     &,4),
     & REAL(R_amume,4),I_opume,REAL(R_ilume,4),
     & REAL(R_olume,4),REAL(R_okume,4),
     & REAL(R_ikume,4),REAL(R_imume,4),L_omume,REAL(R_umume
     &,4),L_apume,
     & L_epume,R_ulume,REAL(R_elume,4),REAL(R_alume,4),L_ipume
     &)
      !}
C SRG_vlv.fgi( 502, 198):���������� ������� ��� 2,20SRG10CU022XQ01
      R_irope = R8_afe
C SRG_init_1.fgi( 138, 137):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_epope,R_etope,REAL(1
     &,4),
     & REAL(R_irope,4),I_atope,REAL(R_upope,4),
     & REAL(R_arope,4),REAL(R_apope,4),
     & REAL(R_umope,4),REAL(R_urope,4),L_asope,REAL(R_esope
     &,4),L_isope,
     & L_osope,R_erope,REAL(R_opope,4),REAL(R_ipope,4),L_usope
     &)
      !}
C SRG_vlv.fgi( 476, 211):���������� ������� ��� 2,20SRG10CU021XQ02
      R_osume = R8_efe
C SRG_init_1.fgi( 138, 141):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_irume,R_ivume,REAL(60
     &,4),
     & REAL(R_osume,4),I_evume,REAL(R_asume,4),
     & REAL(R_esume,4),REAL(R_erume,4),
     & REAL(R_arume,4),REAL(R_atume,4),L_etume,REAL(R_itume
     &,4),L_otume,
     & L_utume,R_isume,REAL(R_urume,4),REAL(R_orume,4),L_avume
     &)
      !}
C SRG_vlv.fgi( 502, 211):���������� ������� ��� 2,20SRG10CU021XQ01
      R_axope = R8_ife
C SRG_init_1.fgi( 138, 145):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_utope,R_ubupe,REAL(1
     &,4),
     & REAL(R_axope,4),I_obupe,REAL(R_ivope,4),
     & REAL(R_ovope,4),REAL(R_otope,4),
     & REAL(R_itope,4),REAL(R_ixope,4),L_oxope,REAL(R_uxope
     &,4),L_abupe,
     & L_ebupe,R_uvope,REAL(R_evope,4),REAL(R_avope,4),L_ibupe
     &)
      !}
C SRG_vlv.fgi( 476, 224):���������� ������� ��� 2,20SRG10CU020XQ02
      R_ebape = R8_ofe
C SRG_init_1.fgi( 138, 149):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_axume,R_afape,REAL(60
     &,4),
     & REAL(R_ebape,4),I_udape,REAL(R_oxume,4),
     & REAL(R_uxume,4),REAL(R_uvume,4),
     & REAL(R_ovume,4),REAL(R_obape,4),L_ubape,REAL(R_adape
     &,4),L_edape,
     & L_idape,R_abape,REAL(R_ixume,4),REAL(R_exume,4),L_odape
     &)
      !}
C SRG_vlv.fgi( 502, 224):���������� ������� ��� 2,20SRG10CU020XQ01
      R_ofupe = R8_ufe
C SRG_init_1.fgi( 138, 153):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_idupe,R_ilupe,REAL(1
     &,4),
     & REAL(R_ofupe,4),I_elupe,REAL(R_afupe,4),
     & REAL(R_efupe,4),REAL(R_edupe,4),
     & REAL(R_adupe,4),REAL(R_akupe,4),L_ekupe,REAL(R_ikupe
     &,4),L_okupe,
     & L_ukupe,R_ifupe,REAL(R_udupe,4),REAL(R_odupe,4),L_alupe
     &)
      !}
C SRG_vlv.fgi( 476, 237):���������� ������� ��� 2,20SRG10CU019XQ02
      R_ukape = R8_ake
C SRG_init_1.fgi( 138, 157):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ofape,R_omape,REAL(60
     &,4),
     & REAL(R_ukape,4),I_imape,REAL(R_ekape,4),
     & REAL(R_ikape,4),REAL(R_ifape,4),
     & REAL(R_efape,4),REAL(R_elape,4),L_ilape,REAL(R_olape
     &,4),L_ulape,
     & L_amape,R_okape,REAL(R_akape,4),REAL(R_ufape,4),L_emape
     &)
      !}
C SRG_vlv.fgi( 502, 237):���������� ������� ��� 2,20SRG10CU019XQ01
      R_epupe = R8_eke
C SRG_init_1.fgi( 138, 161):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_amupe,R_asupe,REAL(1
     &,4),
     & REAL(R_epupe,4),I_urupe,REAL(R_omupe,4),
     & REAL(R_umupe,4),REAL(R_ulupe,4),
     & REAL(R_olupe,4),REAL(R_opupe,4),L_upupe,REAL(R_arupe
     &,4),L_erupe,
     & L_irupe,R_apupe,REAL(R_imupe,4),REAL(R_emupe,4),L_orupe
     &)
      !}
C SRG_vlv.fgi( 476, 250):���������� ������� ��� 2,20SRG10CU018XQ02
      R_irape = R8_ike
C SRG_init_1.fgi( 138, 165):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_epape,R_etape,REAL(60
     &,4),
     & REAL(R_irape,4),I_atape,REAL(R_upape,4),
     & REAL(R_arape,4),REAL(R_apape,4),
     & REAL(R_umape,4),REAL(R_urape,4),L_asape,REAL(R_esape
     &,4),L_isape,
     & L_osape,R_erape,REAL(R_opape,4),REAL(R_ipape,4),L_usape
     &)
      !}
C SRG_vlv.fgi( 502, 250):���������� ������� ��� 2,20SRG10CU018XQ01
      R_idare = R8_oke
C SRG_init_1.fgi( 138, 169):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ebare,R_ekare,REAL(1
     &,4),
     & REAL(R_idare,4),I_akare,REAL(R_ubare,4),
     & REAL(R_adare,4),REAL(R_abare,4),
     & REAL(R_uxupe,4),REAL(R_udare,4),L_afare,REAL(R_efare
     &,4),L_ifare,
     & L_ofare,R_edare,REAL(R_obare,4),REAL(R_ibare,4),L_ufare
     &)
      !}
C SRG_vlv.fgi( 476, 263):���������� ������� ��� 2,20SRG10CU017XQ02
      R_utupe = R8_uke
C SRG_init_1.fgi( 138, 173):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_osupe,R_oxupe,REAL(60
     &,4),
     & REAL(R_utupe,4),I_ixupe,REAL(R_etupe,4),
     & REAL(R_itupe,4),REAL(R_isupe,4),
     & REAL(R_esupe,4),REAL(R_evupe,4),L_ivupe,REAL(R_ovupe
     &,4),L_uvupe,
     & L_axupe,R_otupe,REAL(R_atupe,4),REAL(R_usupe,4),L_exupe
     &)
      !}
C SRG_vlv.fgi( 502, 263):���������� ������� ��� 2,20SRG10CU017XQ01
      R_osare = R8_ale
C SRG_init_1.fgi( 138, 177):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_irare,R_ivare,REAL(1
     &,4),
     & REAL(R_osare,4),I_evare,REAL(R_asare,4),
     & REAL(R_esare,4),REAL(R_erare,4),
     & REAL(R_arare,4),REAL(R_atare,4),L_etare,REAL(R_itare
     &,4),L_otare,
     & L_utare,R_isare,REAL(R_urare,4),REAL(R_orare,4),L_avare
     &)
      !}
C SRG_vlv.fgi( 476, 276):���������� ������� ��� 2,20SRG10CU016XQ02
      R_amare = R8_ele
C SRG_init_1.fgi( 138, 181):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ukare,R_upare,REAL(60
     &,4),
     & REAL(R_amare,4),I_opare,REAL(R_ilare,4),
     & REAL(R_olare,4),REAL(R_okare,4),
     & REAL(R_ikare,4),REAL(R_imare,4),L_omare,REAL(R_umare
     &,4),L_apare,
     & L_epare,R_ulare,REAL(R_elare,4),REAL(R_alare,4),L_ipare
     &)
      !}
C SRG_vlv.fgi( 502, 276):���������� ������� ��� 2,20SRG10CU016XQ01
      R_okuri = R8_ile
C SRG_init_1.fgi(  50,  65):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ifuri,R_imuri,REAL(1
     &,4),
     & REAL(R_okuri,4),I_emuri,REAL(R_akuri,4),
     & REAL(R_ekuri,4),REAL(R_efuri,4),
     & REAL(R_afuri,4),REAL(R_aluri,4),L_eluri,REAL(R_iluri
     &,4),L_oluri,
     & L_uluri,R_ikuri,REAL(R_ufuri,4),REAL(R_ofuri,4),L_amuri
     &)
      !}
C SRG_vlv.fgi( 478, 439):���������� ������� ��� 2,20SRG10CU015XQ02
      R_aburi = R8_ole
C SRG_init_1.fgi(  50,  69):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_uvori,R_uduri,REAL(60
     &,4),
     & REAL(R_aburi,4),I_oduri,REAL(R_ixori,4),
     & REAL(R_oxori,4),REAL(R_ovori,4),
     & REAL(R_ivori,4),REAL(R_iburi,4),L_oburi,REAL(R_uburi
     &,4),L_aduri,
     & L_eduri,R_uxori,REAL(R_exori,4),REAL(R_axori,4),L_iduri
     &)
      !}
C SRG_vlv.fgi( 502, 439):���������� ������� ��� 2,20SRG10CU015XQ01
      R_uvuri = R8_ule
C SRG_init_1.fgi(  50,  73):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_oturi,R_obasi,REAL(1
     &,4),
     & REAL(R_uvuri,4),I_ibasi,REAL(R_evuri,4),
     & REAL(R_ivuri,4),REAL(R_ituri,4),
     & REAL(R_eturi,4),REAL(R_exuri,4),L_ixuri,REAL(R_oxuri
     &,4),L_uxuri,
     & L_abasi,R_ovuri,REAL(R_avuri,4),REAL(R_uturi,4),L_ebasi
     &)
      !}
C SRG_vlv.fgi( 478, 452):���������� ������� ��� 2,20SRG10CU014XQ02
      R_eruri = R8_ame
C SRG_init_1.fgi(  50,  77):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_apuri,R_aturi,REAL(60
     &,4),
     & REAL(R_eruri,4),I_usuri,REAL(R_opuri,4),
     & REAL(R_upuri,4),REAL(R_umuri,4),
     & REAL(R_omuri,4),REAL(R_oruri,4),L_ururi,REAL(R_asuri
     &,4),L_esuri,
     & L_isuri,R_aruri,REAL(R_ipuri,4),REAL(R_epuri,4),L_osuri
     &)
      !}
C SRG_vlv.fgi( 502, 452):���������� ������� ��� 2,20SRG10CU014XQ01
      R_apasi = R8_eme
C SRG_init_1.fgi(  50,  81):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ulasi,R_urasi,REAL(1
     &,4),
     & REAL(R_apasi,4),I_orasi,REAL(R_imasi,4),
     & REAL(R_omasi,4),REAL(R_olasi,4),
     & REAL(R_ilasi,4),REAL(R_ipasi,4),L_opasi,REAL(R_upasi
     &,4),L_arasi,
     & L_erasi,R_umasi,REAL(R_emasi,4),REAL(R_amasi,4),L_irasi
     &)
      !}
C SRG_vlv.fgi( 478, 465):���������� ������� ��� 2,20SRG10CU013XQ02
      R_ifasi = R8_ime
C SRG_init_1.fgi(  50,  85):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_edasi,R_elasi,REAL(60
     &,4),
     & REAL(R_ifasi,4),I_alasi,REAL(R_udasi,4),
     & REAL(R_afasi,4),REAL(R_adasi,4),
     & REAL(R_ubasi,4),REAL(R_ufasi,4),L_akasi,REAL(R_ekasi
     &,4),L_ikasi,
     & L_okasi,R_efasi,REAL(R_odasi,4),REAL(R_idasi,4),L_ukasi
     &)
      !}
C SRG_vlv.fgi( 502, 465):���������� ������� ��� 2,20SRG10CU013XQ01
      R_edesi = R8_ome
C SRG_init_1.fgi(  50,  89):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_abesi,R_akesi,REAL(1
     &,4),
     & REAL(R_edesi,4),I_ufesi,REAL(R_obesi,4),
     & REAL(R_ubesi,4),REAL(R_uxasi,4),
     & REAL(R_oxasi,4),REAL(R_odesi,4),L_udesi,REAL(R_afesi
     &,4),L_efesi,
     & L_ifesi,R_adesi,REAL(R_ibesi,4),REAL(R_ebesi,4),L_ofesi
     &)
      !}
C SRG_vlv.fgi( 478, 478):���������� ������� ��� 2,20SRG10CU012XQ02
      R_otasi = R8_ume
C SRG_init_1.fgi(  50,  93):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_isasi,R_ixasi,REAL(60
     &,4),
     & REAL(R_otasi,4),I_exasi,REAL(R_atasi,4),
     & REAL(R_etasi,4),REAL(R_esasi,4),
     & REAL(R_asasi,4),REAL(R_avasi,4),L_evasi,REAL(R_ivasi
     &,4),L_ovasi,
     & L_uvasi,R_itasi,REAL(R_usasi,4),REAL(R_osasi,4),L_axasi
     &)
      !}
C SRG_vlv.fgi( 502, 478):���������� ������� ��� 2,20SRG10CU012XQ01
      R_isesi = R8_ape
C SRG_init_1.fgi(  50,  97):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_eresi,R_evesi,REAL(1
     &,4),
     & REAL(R_isesi,4),I_avesi,REAL(R_uresi,4),
     & REAL(R_asesi,4),REAL(R_aresi,4),
     & REAL(R_upesi,4),REAL(R_usesi,4),L_atesi,REAL(R_etesi
     &,4),L_itesi,
     & L_otesi,R_esesi,REAL(R_oresi,4),REAL(R_iresi,4),L_utesi
     &)
      !}
C SRG_vlv.fgi( 478, 491):���������� ������� ��� 2,20SRG10CU011XQ02
      R_ulesi = R8_epe
C SRG_init_1.fgi(  50, 101):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_okesi,R_opesi,REAL(60
     &,4),
     & REAL(R_ulesi,4),I_ipesi,REAL(R_elesi,4),
     & REAL(R_ilesi,4),REAL(R_ikesi,4),
     & REAL(R_ekesi,4),REAL(R_emesi,4),L_imesi,REAL(R_omesi
     &,4),L_umesi,
     & L_apesi,R_olesi,REAL(R_alesi,4),REAL(R_ukesi,4),L_epesi
     &)
      !}
C SRG_vlv.fgi( 502, 491):���������� ������� ��� 2,20SRG10CU011XQ01
      R_okisi = R8_ipe
C SRG_init_1.fgi(  50, 105):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ifisi,R_imisi,REAL(1
     &,4),
     & REAL(R_okisi,4),I_emisi,REAL(R_akisi,4),
     & REAL(R_ekisi,4),REAL(R_efisi,4),
     & REAL(R_afisi,4),REAL(R_alisi,4),L_elisi,REAL(R_ilisi
     &,4),L_olisi,
     & L_ulisi,R_ikisi,REAL(R_ufisi,4),REAL(R_ofisi,4),L_amisi
     &)
      !}
C SRG_vlv.fgi( 428, 439):���������� ������� ��� 2,20SRG10CU010XQ02
      R_abisi = R8_ope
C SRG_init_1.fgi(  50, 109):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_uvesi,R_udisi,REAL(60
     &,4),
     & REAL(R_abisi,4),I_odisi,REAL(R_ixesi,4),
     & REAL(R_oxesi,4),REAL(R_ovesi,4),
     & REAL(R_ivesi,4),REAL(R_ibisi,4),L_obisi,REAL(R_ubisi
     &,4),L_adisi,
     & L_edisi,R_uxesi,REAL(R_exesi,4),REAL(R_axesi,4),L_idisi
     &)
      !}
C SRG_vlv.fgi( 452, 439):���������� ������� ��� 2,20SRG10CU010XQ01
      R_uvisi = R8_upe
C SRG_init_1.fgi(  50, 113):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_otisi,R_obosi,REAL(1
     &,4),
     & REAL(R_uvisi,4),I_ibosi,REAL(R_evisi,4),
     & REAL(R_ivisi,4),REAL(R_itisi,4),
     & REAL(R_etisi,4),REAL(R_exisi,4),L_ixisi,REAL(R_oxisi
     &,4),L_uxisi,
     & L_abosi,R_ovisi,REAL(R_avisi,4),REAL(R_utisi,4),L_ebosi
     &)
      !}
C SRG_vlv.fgi( 428, 452):���������� ������� ��� 2,20SRG10CU009XQ02
      R_erisi = R8_are
C SRG_init_1.fgi(  50, 117):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_apisi,R_atisi,REAL(60
     &,4),
     & REAL(R_erisi,4),I_usisi,REAL(R_opisi,4),
     & REAL(R_upisi,4),REAL(R_umisi,4),
     & REAL(R_omisi,4),REAL(R_orisi,4),L_urisi,REAL(R_asisi
     &,4),L_esisi,
     & L_isisi,R_arisi,REAL(R_ipisi,4),REAL(R_episi,4),L_osisi
     &)
      !}
C SRG_vlv.fgi( 452, 452):���������� ������� ��� 2,20SRG10CU009XQ01
      R_aposi = R8_ere
C SRG_init_1.fgi(  50, 121):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ulosi,R_urosi,REAL(1
     &,4),
     & REAL(R_aposi,4),I_orosi,REAL(R_imosi,4),
     & REAL(R_omosi,4),REAL(R_olosi,4),
     & REAL(R_ilosi,4),REAL(R_iposi,4),L_oposi,REAL(R_uposi
     &,4),L_arosi,
     & L_erosi,R_umosi,REAL(R_emosi,4),REAL(R_amosi,4),L_irosi
     &)
      !}
C SRG_vlv.fgi( 428, 465):���������� ������� ��� 2,20SRG10CU008XQ02
      R_ifosi = R8_ire
C SRG_init_1.fgi(  50, 125):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_edosi,R_elosi,REAL(60
     &,4),
     & REAL(R_ifosi,4),I_alosi,REAL(R_udosi,4),
     & REAL(R_afosi,4),REAL(R_adosi,4),
     & REAL(R_ubosi,4),REAL(R_ufosi,4),L_akosi,REAL(R_ekosi
     &,4),L_ikosi,
     & L_okosi,R_efosi,REAL(R_odosi,4),REAL(R_idosi,4),L_ukosi
     &)
      !}
C SRG_vlv.fgi( 452, 465):���������� ������� ��� 2,20SRG10CU008XQ01
      R_edusi = R8_ore
C SRG_init_1.fgi(  50, 129):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_abusi,R_akusi,REAL(1
     &,4),
     & REAL(R_edusi,4),I_ufusi,REAL(R_obusi,4),
     & REAL(R_ubusi,4),REAL(R_uxosi,4),
     & REAL(R_oxosi,4),REAL(R_odusi,4),L_udusi,REAL(R_afusi
     &,4),L_efusi,
     & L_ifusi,R_adusi,REAL(R_ibusi,4),REAL(R_ebusi,4),L_ofusi
     &)
      !}
C SRG_vlv.fgi( 428, 478):���������� ������� ��� 2,20SRG10CU007XQ02
      R_otosi = R8_ure
C SRG_init_1.fgi(  50, 133):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_isosi,R_ixosi,REAL(60
     &,4),
     & REAL(R_otosi,4),I_exosi,REAL(R_atosi,4),
     & REAL(R_etosi,4),REAL(R_esosi,4),
     & REAL(R_asosi,4),REAL(R_avosi,4),L_evosi,REAL(R_ivosi
     &,4),L_ovosi,
     & L_uvosi,R_itosi,REAL(R_usosi,4),REAL(R_ososi,4),L_axosi
     &)
      !}
C SRG_vlv.fgi( 452, 478):���������� ������� ��� 2,20SRG10CU007XQ01
      R_isusi = R8_ase
C SRG_init_1.fgi(  50, 137):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_erusi,R_evusi,REAL(1
     &,4),
     & REAL(R_isusi,4),I_avusi,REAL(R_urusi,4),
     & REAL(R_asusi,4),REAL(R_arusi,4),
     & REAL(R_upusi,4),REAL(R_ususi,4),L_atusi,REAL(R_etusi
     &,4),L_itusi,
     & L_otusi,R_esusi,REAL(R_orusi,4),REAL(R_irusi,4),L_utusi
     &)
      !}
C SRG_vlv.fgi( 428, 491):���������� ������� ��� 2,20SRG10CU006XQ02
      R_ulusi = R8_ese
C SRG_init_1.fgi(  50, 141):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_okusi,R_opusi,REAL(60
     &,4),
     & REAL(R_ulusi,4),I_ipusi,REAL(R_elusi,4),
     & REAL(R_ilusi,4),REAL(R_ikusi,4),
     & REAL(R_ekusi,4),REAL(R_emusi,4),L_imusi,REAL(R_omusi
     &,4),L_umusi,
     & L_apusi,R_olusi,REAL(R_alusi,4),REAL(R_ukusi,4),L_epusi
     &)
      !}
C SRG_vlv.fgi( 452, 491):���������� ������� ��� 2,20SRG10CU006XQ01
      R_okati = R8_ise
C SRG_init_1.fgi(  50, 145):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ifati,R_imati,REAL(1
     &,4),
     & REAL(R_okati,4),I_emati,REAL(R_akati,4),
     & REAL(R_ekati,4),REAL(R_efati,4),
     & REAL(R_afati,4),REAL(R_alati,4),L_elati,REAL(R_ilati
     &,4),L_olati,
     & L_ulati,R_ikati,REAL(R_ufati,4),REAL(R_ofati,4),L_amati
     &)
      !}
C SRG_vlv.fgi( 502, 504):���������� ������� ��� 2,20SRG10CU005XQ02
      R_abati = R8_ose
C SRG_init_1.fgi(  50, 149):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_uvusi,R_udati,REAL(60
     &,4),
     & REAL(R_abati,4),I_odati,REAL(R_ixusi,4),
     & REAL(R_oxusi,4),REAL(R_ovusi,4),
     & REAL(R_ivusi,4),REAL(R_ibati,4),L_obati,REAL(R_ubati
     &,4),L_adati,
     & L_edati,R_uxusi,REAL(R_exusi,4),REAL(R_axusi,4),L_idati
     &)
      !}
C SRG_vlv.fgi( 528, 504):���������� ������� ��� 2,20SRG10CU005XQ01
      R_uvati = R8_use
C SRG_init_1.fgi(  50, 153):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_otati,R_obeti,REAL(1
     &,4),
     & REAL(R_uvati,4),I_ibeti,REAL(R_evati,4),
     & REAL(R_ivati,4),REAL(R_itati,4),
     & REAL(R_etati,4),REAL(R_exati,4),L_ixati,REAL(R_oxati
     &,4),L_uxati,
     & L_abeti,R_ovati,REAL(R_avati,4),REAL(R_utati,4),L_ebeti
     &)
      !}
C SRG_vlv.fgi( 528, 517):���������� ������� ��� 2,20SRG10CU004XQ02
      R_erati = R8_ate
C SRG_init_1.fgi(  50, 157):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_apati,R_atati,REAL(60
     &,4),
     & REAL(R_erati,4),I_usati,REAL(R_opati,4),
     & REAL(R_upati,4),REAL(R_umati,4),
     & REAL(R_omati,4),REAL(R_orati,4),L_urati,REAL(R_asati
     &,4),L_esati,
     & L_isati,R_arati,REAL(R_ipati,4),REAL(R_epati,4),L_osati
     &)
      !}
C SRG_vlv.fgi( 528, 452):���������� ������� ��� 2,20SRG10CU004XQ01
      R_apeti = R8_ete
C SRG_init_1.fgi(  50, 161):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_uleti,R_ureti,REAL(1
     &,4),
     & REAL(R_apeti,4),I_oreti,REAL(R_imeti,4),
     & REAL(R_ometi,4),REAL(R_oleti,4),
     & REAL(R_ileti,4),REAL(R_ipeti,4),L_opeti,REAL(R_upeti
     &,4),L_areti,
     & L_ereti,R_umeti,REAL(R_emeti,4),REAL(R_ameti,4),L_ireti
     &)
      !}
C SRG_vlv.fgi( 528, 530):���������� ������� ��� 2,20SRG10CU003XQ02
      R_ifeti = R8_ite
C SRG_init_1.fgi(  50, 165):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_edeti,R_eleti,REAL(60
     &,4),
     & REAL(R_ifeti,4),I_aleti,REAL(R_udeti,4),
     & REAL(R_afeti,4),REAL(R_adeti,4),
     & REAL(R_ubeti,4),REAL(R_ufeti,4),L_aketi,REAL(R_eketi
     &,4),L_iketi,
     & L_oketi,R_efeti,REAL(R_odeti,4),REAL(R_ideti,4),L_uketi
     &)
      !}
C SRG_vlv.fgi( 528, 465):���������� ������� ��� 2,20SRG10CU003XQ01
      R_editi = R8_ote
C SRG_init_1.fgi(  50, 169):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_abiti,R_akiti,REAL(1
     &,4),
     & REAL(R_editi,4),I_ufiti,REAL(R_obiti,4),
     & REAL(R_ubiti,4),REAL(R_uxeti,4),
     & REAL(R_oxeti,4),REAL(R_oditi,4),L_uditi,REAL(R_afiti
     &,4),L_efiti,
     & L_ifiti,R_aditi,REAL(R_ibiti,4),REAL(R_ebiti,4),L_ofiti
     &)
      !}
C SRG_vlv.fgi( 528, 543):���������� ������� ��� 2,20SRG10CU002XQ02
      R_oteti = R8_ute
C SRG_init_1.fgi(  50, 173):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_iseti,R_ixeti,REAL(60
     &,4),
     & REAL(R_oteti,4),I_exeti,REAL(R_ateti,4),
     & REAL(R_eteti,4),REAL(R_eseti,4),
     & REAL(R_aseti,4),REAL(R_aveti,4),L_eveti,REAL(R_iveti
     &,4),L_oveti,
     & L_uveti,R_iteti,REAL(R_useti,4),REAL(R_oseti,4),L_axeti
     &)
      !}
C SRG_vlv.fgi( 528, 478):���������� ������� ��� 2,20SRG10CU002XQ01
      R_isiti = R8_ave
C SRG_init_1.fgi(  50, 177):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_eriti,R_eviti,REAL(1
     &,4),
     & REAL(R_isiti,4),I_aviti,REAL(R_uriti,4),
     & REAL(R_asiti,4),REAL(R_ariti,4),
     & REAL(R_upiti,4),REAL(R_usiti,4),L_atiti,REAL(R_etiti
     &,4),L_ititi,
     & L_otiti,R_esiti,REAL(R_oriti,4),REAL(R_iriti,4),L_utiti
     &)
      !}
C SRG_vlv.fgi( 528, 556):���������� ������� ��� 2,20SRG10CU001XQ02
      R_uliti = R8_eve
C SRG_init_1.fgi(  50, 181):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_okiti,R_opiti,REAL(60
     &,4),
     & REAL(R_uliti,4),I_ipiti,REAL(R_eliti,4),
     & REAL(R_iliti,4),REAL(R_ikiti,4),
     & REAL(R_ekiti,4),REAL(R_emiti,4),L_imiti,REAL(R_omiti
     &,4),L_umiti,
     & L_apiti,R_oliti,REAL(R_aliti,4),REAL(R_ukiti,4),L_epiti
     &)
      !}
C SRG_vlv.fgi( 528, 491):���������� ������� ��� 2,20SRG10CU001XQ01
      R_ikipi = R8_ive
C SRG_init_1.fgi( 224, 185):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_amipi,4),REAL
     &(R_ikipi,4),R_efipi,
     & R_emipi,REAL(1e-3,4),REAL(R_ufipi,4),
     & REAL(R_akipi,4),REAL(R_afipi,4),
     & REAL(R_udipi,4),I_ulipi,REAL(R_okipi,4),L_ukipi,
     & REAL(R_alipi,4),L_elipi,L_ilipi,R_ekipi,REAL(R_ofipi
     &,4),REAL(R_ifipi,4),L_olipi)
      !}
C SRG_vlv.fgi( 700, 543):���������� ������� ��������,20SRG10CP074XQ01
      R_osile = R8_ove
C SRG_init_1.fgi( 138, 185):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_evile,4),REAL
     &(R_osile,4),R_irile,
     & R_ivile,REAL(1e-3,4),REAL(R_asile,4),
     & REAL(R_esile,4),REAL(R_erile,4),
     & REAL(R_arile,4),I_avile,REAL(R_usile,4),L_atile,
     & REAL(R_etile,4),L_itile,L_otile,R_isile,REAL(R_urile
     &,4),REAL(R_orile,4),L_utile)
      !}
C SRG_vlv.fgi( 550, 262):���������� ������� ��������,20SRG10CP066XQ01
      R_aboti = R8_uve
C SRG_init_1.fgi(  50, 185):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_odoti,4),REAL
     &(R_aboti,4),R_uviti,
     & R_udoti,REAL(1e-3,4),REAL(R_ixiti,4),
     & REAL(R_oxiti,4),REAL(R_oviti,4),
     & REAL(R_iviti,4),I_idoti,REAL(R_eboti,4),L_iboti,
     & REAL(R_oboti,4),L_uboti,L_adoti,R_uxiti,REAL(R_exiti
     &,4),REAL(R_axiti,4),L_edoti)
      !}
C SRG_vlv.fgi( 502, 556):���������� ������� ��������,20SRG10CP058XQ01
      R_evolo = R8_axe
C SRG_init_1.fgi( 224, 189):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_atolo,R_abulo,REAL(60
     &,4),
     & REAL(R_evolo,4),I_uxolo,REAL(R_otolo,4),
     & REAL(R_utolo,4),REAL(R_usolo,4),
     & REAL(R_osolo,4),REAL(R_ovolo,4),L_uvolo,REAL(R_axolo
     &,4),L_exolo,
     & L_ixolo,R_avolo,REAL(R_itolo,4),REAL(R_etolo,4),L_oxolo
     &)
      !}
C SRG_vlv.fgi(  84, 434):���������� ������� ��� 2,20SRG10CU103XQ02
      R_udulo = R8_exe
C SRG_init_1.fgi( 224, 193):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_obulo,R_okulo,REAL(1
     &,4),
     & REAL(R_udulo,4),I_ikulo,REAL(R_edulo,4),
     & REAL(R_idulo,4),REAL(R_ibulo,4),
     & REAL(R_ebulo,4),REAL(R_efulo,4),L_ifulo,REAL(R_ofulo
     &,4),L_ufulo,
     & L_akulo,R_odulo,REAL(R_adulo,4),REAL(R_ubulo,4),L_ekulo
     &)
      !}
C SRG_vlv.fgi(  84, 448):���������� ������� ��� 2,20SRG10CU103XQ01
      R_imulo = R8_ixe
C SRG_init_1.fgi( 138, 189):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_elulo,R_erulo,REAL(60
     &,4),
     & REAL(R_imulo,4),I_arulo,REAL(R_ululo,4),
     & REAL(R_amulo,4),REAL(R_alulo,4),
     & REAL(R_ukulo,4),REAL(R_umulo,4),L_apulo,REAL(R_epulo
     &,4),L_ipulo,
     & L_opulo,R_emulo,REAL(R_olulo,4),REAL(R_ilulo,4),L_upulo
     &)
      !}
C SRG_vlv.fgi(  84, 462):���������� ������� ��� 2,20SRG10CU102XQ02
      R_atulo = R8_oxe
C SRG_init_1.fgi( 138, 193):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_urulo,R_uvulo,REAL(1
     &,4),
     & REAL(R_atulo,4),I_ovulo,REAL(R_isulo,4),
     & REAL(R_osulo,4),REAL(R_orulo,4),
     & REAL(R_irulo,4),REAL(R_itulo,4),L_otulo,REAL(R_utulo
     &,4),L_avulo,
     & L_evulo,R_usulo,REAL(R_esulo,4),REAL(R_asulo,4),L_ivulo
     &)
      !}
C SRG_vlv.fgi(  84, 476):���������� ������� ��� 2,20SRG10CU102XQ01
      R_atopo = R8_uxe
C SRG_init_1.fgi( 312, 273):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_uropo,R_uvopo,REAL(1e
     &-3,4),
     & REAL(R_atopo,4),I_ovopo,REAL(R_isopo,4),
     & REAL(R_osopo,4),REAL(R_oropo,4),
     & REAL(R_iropo,4),REAL(R_itopo,4),L_otopo,REAL(R_utopo
     &,4),L_avopo,
     & L_evopo,R_usopo,REAL(R_esopo,4),REAL(R_asopo,4),L_ivopo
     &)
      !}
C SRG_vlv.fgi( 225, 602):���������� ������� ��� 2,20SRG10CP039XQ01
      R_obupo = R8_abi
C SRG_init_1.fgi( 312, 277):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ixopo,R_ifupo,REAL(1e
     &-3,4),
     & REAL(R_obupo,4),I_efupo,REAL(R_abupo,4),
     & REAL(R_ebupo,4),REAL(R_exopo,4),
     & REAL(R_axopo,4),REAL(R_adupo,4),L_edupo,REAL(R_idupo
     &,4),L_odupo,
     & L_udupo,R_ibupo,REAL(R_uxopo,4),REAL(R_oxopo,4),L_afupo
     &)
      !}
C SRG_vlv.fgi( 225, 616):���������� ������� ��� 2,20SRG10CP038XQ01
      R_esori = R8_ebi
C SRG_init_1.fgi( 312, 281):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_utori,4),REAL
     &(R_esori,4),R_arori,
     & R_avori,REAL(-1,4),REAL(R_orori,4),
     & REAL(R_urori,4),REAL(R_upori,4),
     & REAL(R_opori,4),I_otori,REAL(R_isori,4),L_osori,
     & REAL(R_usori,4),L_atori,L_etori,R_asori,REAL(R_irori
     &,4),REAL(R_erori,4),L_itori)
      !}
C SRG_vlv.fgi( 502, 543):���������� ������� ��������,20SRG10CP057XQ01
      R_obamo = R8_ibi
C SRG_init_1.fgi(  50, 189):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ixulo,R_ifamo,REAL(60
     &,4),
     & REAL(R_obamo,4),I_efamo,REAL(R_abamo,4),
     & REAL(R_ebamo,4),REAL(R_exulo,4),
     & REAL(R_axulo,4),REAL(R_adamo,4),L_edamo,REAL(R_idamo
     &,4),L_odamo,
     & L_udamo,R_ibamo,REAL(R_uxulo,4),REAL(R_oxulo,4),L_afamo
     &)
      !}
C SRG_vlv.fgi(  84, 490):���������� ������� ��� 2,20SRG10CU101XQ02
      R_elamo = R8_obi
C SRG_init_1.fgi(  50, 193):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_akamo,R_apamo,REAL(1
     &,4),
     & REAL(R_elamo,4),I_umamo,REAL(R_okamo,4),
     & REAL(R_ukamo,4),REAL(R_ufamo,4),
     & REAL(R_ofamo,4),REAL(R_olamo,4),L_ulamo,REAL(R_amamo
     &,4),L_emamo,
     & L_imamo,R_alamo,REAL(R_ikamo,4),REAL(R_ekamo,4),L_omamo
     &)
      !}
C SRG_vlv.fgi(  84, 504):���������� ������� ��� 2,20SRG10CU101XQ01
      R_urilo = R8_ubi
C SRG_init_1.fgi( 224, 197):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_opilo,R_otilo,REAL(1
     &,4),
     & REAL(R_urilo,4),I_itilo,REAL(R_erilo,4),
     & REAL(R_irilo,4),REAL(R_ipilo,4),
     & REAL(R_epilo,4),REAL(R_esilo,4),L_isilo,REAL(R_osilo
     &,4),L_usilo,
     & L_atilo,R_orilo,REAL(R_arilo,4),REAL(R_upilo,4),L_etilo
     &)
      !}
C SRG_vlv.fgi( 338, 588):���������� ������� ��� 2,20SRG10CP034XQ01
      R_ixilo = R8_adi
C SRG_init_1.fgi( 224, 201):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_evilo,R_edolo,REAL(1
     &,4),
     & REAL(R_ixilo,4),I_adolo,REAL(R_uvilo,4),
     & REAL(R_axilo,4),REAL(R_avilo,4),
     & REAL(R_utilo,4),REAL(R_uxilo,4),L_abolo,REAL(R_ebolo
     &,4),L_ibolo,
     & L_obolo,R_exilo,REAL(R_ovilo,4),REAL(R_ivilo,4),L_ubolo
     &)
      !}
C SRG_vlv.fgi( 338, 602):���������� ������� ��� 2,20SRG10CP033XQ01
      R_akolo = R8_edi
C SRG_init_1.fgi( 138, 197):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_udolo,R_ulolo,REAL(1
     &,4),
     & REAL(R_akolo,4),I_ololo,REAL(R_ifolo,4),
     & REAL(R_ofolo,4),REAL(R_odolo,4),
     & REAL(R_idolo,4),REAL(R_ikolo,4),L_okolo,REAL(R_ukolo
     &,4),L_alolo,
     & L_elolo,R_ufolo,REAL(R_efolo,4),REAL(R_afolo,4),L_ilolo
     &)
      !}
C SRG_vlv.fgi( 338, 616):���������� ������� ��� 2,20SRG10CP032XQ01
      R_opolo = R8_idi
C SRG_init_1.fgi( 138, 201):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_imolo,R_isolo,REAL(1
     &,4),
     & REAL(R_opolo,4),I_esolo,REAL(R_apolo,4),
     & REAL(R_epolo,4),REAL(R_emolo,4),
     & REAL(R_amolo,4),REAL(R_arolo,4),L_erolo,REAL(R_irolo
     &,4),L_orolo,
     & L_urolo,R_ipolo,REAL(R_umolo,4),REAL(R_omolo,4),L_asolo
     &)
      !}
C SRG_vlv.fgi( 338, 630):���������� ������� ��� 2,20SRG10CP031XQ01
      R_urepo = R8_odi
C SRG_init_1.fgi(  50, 197):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_opepo,R_otepo,REAL(1
     &,4),
     & REAL(R_urepo,4),I_itepo,REAL(R_erepo,4),
     & REAL(R_irepo,4),REAL(R_ipepo,4),
     & REAL(R_epepo,4),REAL(R_esepo,4),L_isepo,REAL(R_osepo
     &,4),L_usepo,
     & L_atepo,R_orepo,REAL(R_arepo,4),REAL(R_upepo,4),L_etepo
     &)
      !}
C SRG_vlv.fgi( 338, 644):���������� ������� ��� 2,20SRG10CP030XQ01
      R_elepo = R8_udi
C SRG_init_1.fgi(  50, 201):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_akepo,R_apepo,REAL(1
     &,4),
     & REAL(R_elepo,4),I_umepo,REAL(R_okepo,4),
     & REAL(R_ukepo,4),REAL(R_ufepo,4),
     & REAL(R_ofepo,4),REAL(R_olepo,4),L_ulepo,REAL(R_amepo
     &,4),L_emepo,
     & L_imepo,R_alepo,REAL(R_ikepo,4),REAL(R_ekepo,4),L_omepo
     &)
      !}
C SRG_vlv.fgi( 338, 658):���������� ������� ��� 2,20SRG10CP029XQ01
      R_uramo = R8_afi
C SRG_init_1.fgi( 224, 205):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_opamo,R_otamo,REAL(1
     &,4),
     & REAL(R_uramo,4),I_itamo,REAL(R_eramo,4),
     & REAL(R_iramo,4),REAL(R_ipamo,4),
     & REAL(R_epamo,4),REAL(R_esamo,4),L_isamo,REAL(R_osamo
     &,4),L_usamo,
     & L_atamo,R_oramo,REAL(R_aramo,4),REAL(R_upamo,4),L_etamo
     &)
      !}
C SRG_vlv.fgi(  84, 518):���������� ������� ��� 2,20SRG10CT020XQ01
      R_ixamo = R8_efi
C SRG_init_1.fgi( 224, 209):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_evamo,R_edemo,REAL(1
     &,4),
     & REAL(R_ixamo,4),I_ademo,REAL(R_uvamo,4),
     & REAL(R_axamo,4),REAL(R_avamo,4),
     & REAL(R_utamo,4),REAL(R_uxamo,4),L_abemo,REAL(R_ebemo
     &,4),L_ibemo,
     & L_obemo,R_examo,REAL(R_ovamo,4),REAL(R_ivamo,4),L_ubemo
     &)
      !}
C SRG_vlv.fgi(  84, 532):���������� ������� ��� 2,20SRG10CT017XQ01
      R_akemo = R8_ifi
C SRG_init_1.fgi( 138, 205):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_udemo,R_ulemo,REAL(1
     &,4),
     & REAL(R_akemo,4),I_olemo,REAL(R_ifemo,4),
     & REAL(R_ofemo,4),REAL(R_odemo,4),
     & REAL(R_idemo,4),REAL(R_ikemo,4),L_okemo,REAL(R_ukemo
     &,4),L_alemo,
     & L_elemo,R_ufemo,REAL(R_efemo,4),REAL(R_afemo,4),L_ilemo
     &)
      !}
C SRG_vlv.fgi(  84, 546):���������� ������� ��� 2,20SRG10CT019XQ01
      R_opemo = R8_ofi
C SRG_init_1.fgi( 138, 209):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_imemo,R_isemo,REAL(1
     &,4),
     & REAL(R_opemo,4),I_esemo,REAL(R_apemo,4),
     & REAL(R_epemo,4),REAL(R_ememo,4),
     & REAL(R_amemo,4),REAL(R_aremo,4),L_eremo,REAL(R_iremo
     &,4),L_oremo,
     & L_uremo,R_ipemo,REAL(R_umemo,4),REAL(R_omemo,4),L_asemo
     &)
      !}
C SRG_vlv.fgi(  84, 560):���������� ������� ��� 2,20SRG10CT016XQ01
      R_evemo = R8_ufi
C SRG_init_1.fgi(  50, 205):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_atemo,R_abimo,REAL(1
     &,4),
     & REAL(R_evemo,4),I_uxemo,REAL(R_otemo,4),
     & REAL(R_utemo,4),REAL(R_usemo,4),
     & REAL(R_osemo,4),REAL(R_ovemo,4),L_uvemo,REAL(R_axemo
     &,4),L_exemo,
     & L_ixemo,R_avemo,REAL(R_itemo,4),REAL(R_etemo,4),L_oxemo
     &)
      !}
C SRG_vlv.fgi(  84, 574):���������� ������� ��� 2,20SRG10CT018XQ01
      R_udimo = R8_aki
C SRG_init_1.fgi(  50, 209):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_obimo,R_okimo,REAL(1
     &,4),
     & REAL(R_udimo,4),I_ikimo,REAL(R_edimo,4),
     & REAL(R_idimo,4),REAL(R_ibimo,4),
     & REAL(R_ebimo,4),REAL(R_efimo,4),L_ifimo,REAL(R_ofimo
     &,4),L_ufimo,
     & L_akimo,R_odimo,REAL(R_adimo,4),REAL(R_ubimo,4),L_ekimo
     &)
      !}
C SRG_vlv.fgi(  84, 588):���������� ������� ��� 2,20SRG10CT015XQ01
      R_eripi = R8_eki
C SRG_init_1.fgi( 224, 213):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_apipi,R_atipi,REAL(1
     &,4),
     & REAL(R_eripi,4),I_usipi,REAL(R_opipi,4),
     & REAL(R_upipi,4),REAL(R_umipi,4),
     & REAL(R_omipi,4),REAL(R_oripi,4),L_uripi,REAL(R_asipi
     &,4),L_esipi,
     & L_isipi,R_aripi,REAL(R_ipipi,4),REAL(R_epipi,4),L_osipi
     &)
      !}
C SRG_vlv.fgi( 650, 504):���������� ������� ��� 2,20SRG10CT059XQ01
      R_ibole = R8_iki
C SRG_init_1.fgi( 138, 213):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_exile,R_efole,REAL(1
     &,4),
     & REAL(R_ibole,4),I_afole,REAL(R_uxile,4),
     & REAL(R_abole,4),REAL(R_axile,4),
     & REAL(R_uvile,4),REAL(R_ubole,4),L_adole,REAL(R_edole
     &,4),L_idole,
     & L_odole,R_ebole,REAL(R_oxile,4),REAL(R_ixile,4),L_udole
     &)
      !}
C SRG_vlv.fgi( 526, 290):���������� ������� ��� 2,20SRG10CT056XQ01
      R_ukoti = R8_oki
C SRG_init_1.fgi(  50, 213):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ofoti,R_omoti,REAL(1
     &,4),
     & REAL(R_ukoti,4),I_imoti,REAL(R_ekoti,4),
     & REAL(R_ikoti,4),REAL(R_ifoti,4),
     & REAL(R_efoti,4),REAL(R_eloti,4),L_iloti,REAL(R_oloti
     &,4),L_uloti,
     & L_amoti,R_okoti,REAL(R_akoti,4),REAL(R_ufoti,4),L_emoti
     &)
      !}
C SRG_vlv.fgi( 452, 504):���������� ������� ��� 2,20SRG10CT053XQ01
      R_eteli = R8_uki
C SRG_init_1.fgi( 224, 217):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_aseli,R_axeli,REAL(1
     &,4),
     & REAL(R_eteli,4),I_uveli,REAL(R_oseli,4),
     & REAL(R_useli,4),REAL(R_ureli,4),
     & REAL(R_oreli,4),REAL(R_oteli,4),L_uteli,REAL(R_aveli
     &,4),L_eveli,
     & L_iveli,R_ateli,REAL(R_iseli,4),REAL(R_eseli,4),L_oveli
     &)
      !}
C SRG_vlv.fgi( 750, 556):���������� ������� ��� 2,20SRG10CT058XQ02
      R_ubili = R8_uki
C SRG_init_1.fgi( 224, 221):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_oxeli,R_ofili,REAL(1
     &,4),
     & REAL(R_ubili,4),I_ifili,REAL(R_ebili,4),
     & REAL(R_ibili,4),REAL(R_ixeli,4),
     & REAL(R_exeli,4),REAL(R_edili,4),L_idili,REAL(R_odili
     &,4),L_udili,
     & L_afili,R_obili,REAL(R_abili,4),REAL(R_uxeli,4),L_efili
     &)
      !}
C SRG_vlv.fgi( 726, 556):���������� ������� ��� 2,20SRG10CT058XQ01
      R_ilili = R8_ali
C SRG_init_1.fgi( 224, 225):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ekili,R_epili,REAL(1
     &,4),
     & REAL(R_ilili,4),I_apili,REAL(R_ukili,4),
     & REAL(R_alili,4),REAL(R_akili,4),
     & REAL(R_ufili,4),REAL(R_ulili,4),L_amili,REAL(R_emili
     &,4),L_imili,
     & L_omili,R_elili,REAL(R_okili,4),REAL(R_ikili,4),L_umili
     &)
      !}
C SRG_vlv.fgi( 726, 439):���������� ������� ��� 2,20SRG10CT057XQ02
      R_edupi = R8_ali
C SRG_init_1.fgi( 224, 229):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_abupi,R_akupi,REAL(1
     &,4),
     & REAL(R_edupi,4),I_ufupi,REAL(R_obupi,4),
     & REAL(R_ubupi,4),REAL(R_uxopi,4),
     & REAL(R_oxopi,4),REAL(R_odupi,4),L_udupi,REAL(R_afupi
     &,4),L_efupi,
     & L_ifupi,R_adupi,REAL(R_ibupi,4),REAL(R_ebupi,4),L_ofupi
     &)
      !}
C SRG_vlv.fgi( 676, 556):���������� ������� ��� 2,20SRG10CT057XQ01
      R_uxore = R8_eli
C SRG_init_1.fgi( 138, 217):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ovore,R_odure,REAL(1
     &,4),
     & REAL(R_uxore,4),I_idure,REAL(R_exore,4),
     & REAL(R_ixore,4),REAL(R_ivore,4),
     & REAL(R_evore,4),REAL(R_ebure,4),L_ibure,REAL(R_obure
     &,4),L_ubure,
     & L_adure,R_oxore,REAL(R_axore,4),REAL(R_uvore,4),L_edure
     &)
      !}
C SRG_vlv.fgi( 526, 303):���������� ������� ��� 2,20SRG10CT055XQ02
      R_ikure = R8_eli
C SRG_init_1.fgi( 138, 221):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_efure,R_emure,REAL(1
     &,4),
     & REAL(R_ikure,4),I_amure,REAL(R_ufure,4),
     & REAL(R_akure,4),REAL(R_afure,4),
     & REAL(R_udure,4),REAL(R_ukure,4),L_alure,REAL(R_elure
     &,4),L_ilure,
     & L_olure,R_ekure,REAL(R_ofure,4),REAL(R_ifure,4),L_ulure
     &)
      !}
C SRG_vlv.fgi( 502, 303):���������� ������� ��� 2,20SRG10CT055XQ01
      R_arure = R8_ili
C SRG_init_1.fgi( 138, 225):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_umure,R_usure,REAL(1
     &,4),
     & REAL(R_arure,4),I_osure,REAL(R_ipure,4),
     & REAL(R_opure,4),REAL(R_omure,4),
     & REAL(R_imure,4),REAL(R_irure,4),L_orure,REAL(R_urure
     &,4),L_asure,
     & L_esure,R_upure,REAL(R_epure,4),REAL(R_apure,4),L_isure
     &)
      !}
C SRG_vlv.fgi( 476, 303):���������� ������� ��� 2,20SRG10CT054XQ02
      R_ovure = R8_ili
C SRG_init_1.fgi( 138, 229):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_iture,R_ibase,REAL(1
     &,4),
     & REAL(R_ovure,4),I_ebase,REAL(R_avure,4),
     & REAL(R_evure,4),REAL(R_eture,4),
     & REAL(R_ature,4),REAL(R_axure,4),L_exure,REAL(R_ixure
     &,4),L_oxure,
     & L_uxure,R_ivure,REAL(R_uture,4),REAL(R_oture,4),L_abase
     &)
      !}
C SRG_vlv.fgi( 452, 303):���������� ������� ��� 2,20SRG10CT054XQ01
      R_afiri = R8_oli
C SRG_init_1.fgi(  50, 217):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ubiri,R_ukiri,REAL(1
     &,4),
     & REAL(R_afiri,4),I_okiri,REAL(R_idiri,4),
     & REAL(R_odiri,4),REAL(R_obiri,4),
     & REAL(R_ibiri,4),REAL(R_ifiri,4),L_ofiri,REAL(R_ufiri
     &,4),L_akiri,
     & L_ekiri,R_udiri,REAL(R_ediri,4),REAL(R_adiri,4),L_ikiri
     &)
      !}
C SRG_vlv.fgi( 552, 543):���������� ������� ��� 2,20SRG10CT052XQ02
      R_omiri = R8_oli
C SRG_init_1.fgi(  50, 221):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_iliri,R_iriri,REAL(1
     &,4),
     & REAL(R_omiri,4),I_eriri,REAL(R_amiri,4),
     & REAL(R_emiri,4),REAL(R_eliri,4),
     & REAL(R_aliri,4),REAL(R_apiri,4),L_epiri,REAL(R_ipiri
     &,4),L_opiri,
     & L_upiri,R_imiri,REAL(R_uliri,4),REAL(R_oliri,4),L_ariri
     &)
      !}
C SRG_vlv.fgi( 552, 556):���������� ������� ��� 2,20SRG10CT052XQ01
      R_etiri = R8_uli
C SRG_init_1.fgi(  50, 225):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_asiri,R_axiri,REAL(1
     &,4),
     & REAL(R_etiri,4),I_uviri,REAL(R_osiri,4),
     & REAL(R_usiri,4),REAL(R_uriri,4),
     & REAL(R_oriri,4),REAL(R_otiri,4),L_utiri,REAL(R_aviri
     &,4),L_eviri,
     & L_iviri,R_atiri,REAL(R_isiri,4),REAL(R_esiri,4),L_oviri
     &)
      !}
C SRG_vlv.fgi( 528, 439):���������� ������� ��� 2,20SRG10CT051XQ02
      R_atuti = R8_uli
C SRG_init_1.fgi(  50, 229):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_uruti,R_uvuti,REAL(1
     &,4),
     & REAL(R_atuti,4),I_ovuti,REAL(R_isuti,4),
     & REAL(R_osuti,4),REAL(R_oruti,4),
     & REAL(R_iruti,4),REAL(R_ituti,4),L_otuti,REAL(R_ututi
     &,4),L_avuti,
     & L_evuti,R_usuti,REAL(R_esuti,4),REAL(R_asuti,4),L_ivuti
     &)
      !}
C SRG_vlv.fgi( 478, 556):���������� ������� ��� 2,20SRG10CT051XQ01
      R_akaro = R8_ami
C SRG_init_1.fgi( 224, 233):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_udaro,R_ularo,REAL(1
     &,4),
     & REAL(R_akaro,4),I_olaro,REAL(R_ifaro,4),
     & REAL(R_ofaro,4),REAL(R_odaro,4),
     & REAL(R_idaro,4),REAL(R_ikaro,4),L_okaro,REAL(R_ukaro
     &,4),L_alaro,
     & L_elaro,R_ufaro,REAL(R_efaro,4),REAL(R_afaro,4),L_ilaro
     &)
      !}
C SRG_vlv.fgi( 281, 546):���������� ������� ��� 2,20SRG10GC410XQ03
      R_oparo = R8_ami
C SRG_init_1.fgi( 224, 237):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_imaro,R_isaro,REAL(1
     &,4),
     & REAL(R_oparo,4),I_esaro,REAL(R_aparo,4),
     & REAL(R_eparo,4),REAL(R_emaro,4),
     & REAL(R_amaro,4),REAL(R_araro,4),L_eraro,REAL(R_iraro
     &,4),L_oraro,
     & L_uraro,R_iparo,REAL(R_umaro,4),REAL(R_omaro,4),L_asaro
     &)
      !}
C SRG_vlv.fgi( 281, 560):���������� ������� ��� 2,20SRG10GC410XQ02
      R_evaro = R8_ami
C SRG_init_1.fgi( 224, 241):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ataro,R_abero,REAL(1
     &,4),
     & REAL(R_evaro,4),I_uxaro,REAL(R_otaro,4),
     & REAL(R_utaro,4),REAL(R_usaro,4),
     & REAL(R_osaro,4),REAL(R_ovaro,4),L_uvaro,REAL(R_axaro
     &,4),L_exaro,
     & L_ixaro,R_avaro,REAL(R_itaro,4),REAL(R_etaro,4),L_oxaro
     &)
      !}
C SRG_vlv.fgi( 281, 574):���������� ������� ��� 2,20SRG10GC410XQ01
      R_obiro = R8_emi
C SRG_init_1.fgi( 138, 233):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ixero,R_ifiro,REAL(1
     &,4),
     & REAL(R_obiro,4),I_efiro,REAL(R_abiro,4),
     & REAL(R_ebiro,4),REAL(R_exero,4),
     & REAL(R_axero,4),REAL(R_adiro,4),L_ediro,REAL(R_idiro
     &,4),L_odiro,
     & L_udiro,R_ibiro,REAL(R_uxero,4),REAL(R_oxero,4),L_afiro
     &)
      !}
C SRG_vlv.fgi( 281, 588):���������� ������� ��� 2,20SRG10GC409XQ03
      R_eliro = R8_emi
C SRG_init_1.fgi( 138, 237):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_akiro,R_apiro,REAL(1
     &,4),
     & REAL(R_eliro,4),I_umiro,REAL(R_okiro,4),
     & REAL(R_ukiro,4),REAL(R_ufiro,4),
     & REAL(R_ofiro,4),REAL(R_oliro,4),L_uliro,REAL(R_amiro
     &,4),L_emiro,
     & L_imiro,R_aliro,REAL(R_ikiro,4),REAL(R_ekiro,4),L_omiro
     &)
      !}
C SRG_vlv.fgi( 281, 602):���������� ������� ��� 2,20SRG10GC409XQ02
      R_uriro = R8_emi
C SRG_init_1.fgi( 138, 241):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_opiro,R_otiro,REAL(1
     &,4),
     & REAL(R_uriro,4),I_itiro,REAL(R_eriro,4),
     & REAL(R_iriro,4),REAL(R_ipiro,4),
     & REAL(R_epiro,4),REAL(R_esiro,4),L_isiro,REAL(R_osiro
     &,4),L_usiro,
     & L_atiro,R_oriro,REAL(R_ariro,4),REAL(R_upiro,4),L_etiro
     &)
      !}
C SRG_vlv.fgi( 281, 616):���������� ������� ��� 2,20SRG10GC409XQ01
      R_evoro = R8_imi
C SRG_init_1.fgi(  50, 233):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_atoro,R_aburo,REAL(1
     &,4),
     & REAL(R_evoro,4),I_uxoro,REAL(R_otoro,4),
     & REAL(R_utoro,4),REAL(R_usoro,4),
     & REAL(R_osoro,4),REAL(R_ovoro,4),L_uvoro,REAL(R_axoro
     &,4),L_exoro,
     & L_ixoro,R_avoro,REAL(R_itoro,4),REAL(R_etoro,4),L_oxoro
     &)
      !}
C SRG_vlv.fgi( 281, 630):���������� ������� ��� 2,20SRG10GC408XQ03
      R_uduro = R8_imi
C SRG_init_1.fgi(  50, 237):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_oburo,R_okuro,REAL(1
     &,4),
     & REAL(R_uduro,4),I_ikuro,REAL(R_eduro,4),
     & REAL(R_iduro,4),REAL(R_iburo,4),
     & REAL(R_eburo,4),REAL(R_efuro,4),L_ifuro,REAL(R_ofuro
     &,4),L_ufuro,
     & L_akuro,R_oduro,REAL(R_aduro,4),REAL(R_uburo,4),L_ekuro
     &)
      !}
C SRG_vlv.fgi( 281, 644):���������� ������� ��� 2,20SRG10GC408XQ02
      R_imuro = R8_imi
C SRG_init_1.fgi(  50, 241):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_eluro,R_eruro,REAL(1
     &,4),
     & REAL(R_imuro,4),I_aruro,REAL(R_uluro,4),
     & REAL(R_amuro,4),REAL(R_aluro,4),
     & REAL(R_ukuro,4),REAL(R_umuro,4),L_apuro,REAL(R_epuro
     &,4),L_ipuro,
     & L_opuro,R_emuro,REAL(R_oluro,4),REAL(R_iluro,4),L_upuro
     &)
      !}
C SRG_vlv.fgi( 281, 658):���������� ������� ��� 2,20SRG10GC408XQ01
      R_ibari = R8_omi
C SRG_init_1.fgi( 224, 245):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_exupi,R_efari,REAL(16.666
     &,4),
     & REAL(R_ibari,4),I_afari,REAL(R_uxupi,4),
     & REAL(R_abari,4),REAL(R_axupi,4),
     & REAL(R_uvupi,4),REAL(R_ubari,4),L_adari,REAL(R_edari
     &,4),L_idari,
     & L_odari,R_ebari,REAL(R_oxupi,4),REAL(R_ixupi,4),L_udari
     &)
      !}
C SRG_vlv.fgi( 650, 517):���������� ������� ��� 2,20SRG10CF065XQ01
      Call REG_RAS_MAN(deltat,.TRUE.,L_epad,REAL(R_apad,4
     &),REAL(R_ekad,4),R_opad,
     & REAL(R_ifad,4),R_ipad,R_erad,R_arad,R_ofad,REAL(R_akad
     &,4),
     & R_edad,REAL(R_odad,4),R_udad,REAL(R_efad,4),L_ufad
     &,
     & R_orad,R_urad,L_upad,L_idad,L_afad,I_emad,R_asad,R_amad
     &,
     & R_irad,R8_ulad,REAL(100,4),REAL(R_ebari,4),REAL(R_ikad
     &,4),
     & REAL(R_umad,4),REAL(R_omad,4),REAL(R_imad,4))
C SRG_vlv.fgi( 879, 365):���������� ���������� �������,20SRG10DF065
      R_ukari = R8_umi
C SRG_init_1.fgi( 224, 249):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ufari,R_omari,REAL(16.666
     &,4),
     & REAL(R_ukari,4),I_imari,REAL(R_ikari,4),
     & REAL(R_okari,4),REAL(R_ofari,4),
     & REAL(R_ifari,4),REAL(R_elari,4),L_ilari,REAL(R_olari
     &,4),L_ulari,
     & L_amari,R_omeru,REAL(R_ekari,4),REAL(R_akari,4),L_emari
     &)
      !}
C SRG_vlv.fgi( 650, 530):���������� ������� ��� 2,20SRG10CF064XQ01
      R_(192) = R_umeru + (-R_omeru)
C SRG_logic.fgi( 473, 351):��������
      R_(191)=abs(R_(192))
C SRG_logic.fgi( 480, 351):���������� ��������
      L_(430)=R_(191).lt.R0_imeru
C SRG_logic.fgi( 490, 351):���������� <
      Call REG_RAS_MAN(deltat,.TRUE.,L_eded,REAL(R_aded,4
     &),REAL(R_evad,4),R_oded,
     & REAL(R_itad,4),R_ided,R_efed,R_afed,R_otad,REAL(R_avad
     &,4),
     & R_esad,REAL(R_osad,4),R_usad,REAL(R_etad,4),L_utad
     &,
     & R_ofed,R_ufed,L_uded,L_isad,L_atad,I_ebed,R_aked,R_abed
     &,
     & R_ifed,R8_uxad,REAL(100,4),REAL(R_omeru,4),REAL(R_ivad
     &,4),
     & REAL(R_ubed,4),REAL(R_obed,4),REAL(R_ibed,4))
C SRG_vlv.fgi( 852, 365):���������� ���������� �������,20SRG10DF064
      R_irari = R8_api
C SRG_init_1.fgi( 224, 253):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_epari,R_etari,REAL(16.666
     &,4),
     & REAL(R_irari,4),I_atari,REAL(R_upari,4),
     & REAL(R_arari,4),REAL(R_apari,4),
     & REAL(R_umari,4),REAL(R_urari,4),L_asari,REAL(R_esari
     &,4),L_isari,
     & L_osari,R_erari,REAL(R_opari,4),REAL(R_ipari,4),L_usari
     &)
      !}
C SRG_vlv.fgi( 650, 543):���������� ������� ��� 2,20SRG10CF063XQ01
      Call REG_RAS_MAN(deltat,.TRUE.,L_esed,REAL(R_ased,4
     &),REAL(R_emed,4),R_osed,
     & REAL(R_iled,4),R_ised,R_eted,R_ated,R_oled,REAL(R_amed
     &,4),
     & R_eked,REAL(R_oked,4),R_uked,REAL(R_eled,4),L_uled
     &,
     & R_oted,R_uted,L_used,L_iked,L_aled,I_ered,R_aved,R_ared
     &,
     & R_ited,R8_uped,REAL(100,4),REAL(R_erari,4),REAL(R_imed
     &,4),
     & REAL(R_ured,4),REAL(R_ored,4),REAL(R_ired,4))
C SRG_vlv.fgi( 826, 365):���������� ���������� �������,20SRG10DF063
      R_axari = R8_epi
C SRG_init_1.fgi( 224, 257):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_utari,R_uberi,REAL(16.666
     &,4),
     & REAL(R_axari,4),I_oberi,REAL(R_ivari,4),
     & REAL(R_ovari,4),REAL(R_otari,4),
     & REAL(R_itari,4),REAL(R_ixari,4),L_oxari,REAL(R_uxari
     &,4),L_aberi,
     & L_eberi,R_uvari,REAL(R_evari,4),REAL(R_avari,4),L_iberi
     &)
      !}
C SRG_vlv.fgi( 650, 556):���������� ������� ��� 2,20SRG10CF062XQ01
      Call REG_RAS_MAN(deltat,.TRUE.,L_ekid,REAL(R_akid,4
     &),REAL(R_ebid,4),R_okid,
     & REAL(R_ixed,4),R_ikid,R_elid,R_alid,R_oxed,REAL(R_abid
     &,4),
     & R_eved,REAL(R_oved,4),R_uved,REAL(R_exed,4),L_uxed
     &,
     & R_olid,R_ulid,L_ukid,L_ived,L_axed,I_efid,R_amid,R_afid
     &,
     & R_ilid,R8_udid,REAL(100,4),REAL(R_uvari,4),REAL(R_ibid
     &,4),
     & REAL(R_ufid,4),REAL(R_ofid,4),REAL(R_ifid,4))
C SRG_vlv.fgi( 799, 365):���������� ���������� �������,20SRG10DF062
      R_alole = R8_ipi
C SRG_init_1.fgi( 138, 245):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ufole,R_umole,REAL(16.666
     &,4),
     & REAL(R_alole,4),I_omole,REAL(R_ikole,4),
     & REAL(R_okole,4),REAL(R_ofole,4),
     & REAL(R_ifole,4),REAL(R_ilole,4),L_olole,REAL(R_ulole
     &,4),L_amole,
     & L_emole,R_ukole,REAL(R_ekole,4),REAL(R_akole,4),L_imole
     &)
      !}
C SRG_vlv.fgi( 550, 276):���������� ������� ��� 2,20SRG10CF060XQ01
      Call REG_RAS_MAN(deltat,.TRUE.,L_evid,REAL(R_avid,4
     &),REAL(R_erid,4),R_ovid,
     & REAL(R_ipid,4),R_ivid,R_exid,R_axid,R_opid,REAL(R_arid
     &,4),
     & R_emid,REAL(R_omid,4),R_umid,REAL(R_epid,4),L_upid
     &,
     & R_oxid,R_uxid,L_uvid,L_imid,L_apid,I_etid,R_abod,R_atid
     &,
     & R_ixid,R8_usid,REAL(100,4),REAL(R_ukole,4),REAL(R_irid
     &,4),
     & REAL(R_utid,4),REAL(R_otid,4),REAL(R_itid,4))
C SRG_vlv.fgi( 879, 384):���������� ���������� �������,20SRG10DF060
      R_ifove = R8_opi
C SRG_init_1.fgi( 138, 249):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_idove,R_elove,REAL(16.666
     &,4),
     & REAL(R_ifove,4),I_alove,REAL(R_afove,4),
     & REAL(R_efove,4),REAL(R_edove,4),
     & REAL(R_adove,4),REAL(R_ufove,4),L_akove,REAL(R_ekove
     &,4),L_ikove,
     & L_okove,R_iliru,REAL(R_udove,4),REAL(R_odove,4),L_ukove
     &)
      !}
C SRG_vlv.fgi( 428, 303):���������� ������� ��� 2,20SRG10CF059XQ01
      R_(198) = R_oliru + (-R_iliru)
C SRG_logic.fgi( 352, 351):��������
      R_(197)=abs(R_(198))
C SRG_logic.fgi( 359, 351):���������� ��������
      L_(440)=R_(197).lt.R0_eliru
C SRG_logic.fgi( 369, 351):���������� <
      Call REG_RAS_MAN(deltat,.TRUE.,L_emod,REAL(R_amod,4
     &),REAL(R_efod,4),R_omod,
     & REAL(R_idod,4),R_imod,R_epod,R_apod,R_odod,REAL(R_afod
     &,4),
     & R_ebod,REAL(R_obod,4),R_ubod,REAL(R_edod,4),L_udod
     &,
     & R_opod,R_upod,L_umod,L_ibod,L_adod,I_elod,R_arod,R_alod
     &,
     & R_ipod,R8_ukod,REAL(100,4),REAL(R_iliru,4),REAL(R_ifod
     &,4),
     & REAL(R_ulod,4),REAL(R_olod,4),REAL(R_ilod,4))
C SRG_vlv.fgi( 852, 384):���������� ���������� �������,20SRG10DF059
      R_orole = R8_upi
C SRG_init_1.fgi( 138, 253):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ipole,R_itole,REAL(16.666
     &,4),
     & REAL(R_orole,4),I_etole,REAL(R_arole,4),
     & REAL(R_erole,4),REAL(R_epole,4),
     & REAL(R_apole,4),REAL(R_asole,4),L_esole,REAL(R_isole
     &,4),L_osole,
     & L_usole,R_irole,REAL(R_upole,4),REAL(R_opole,4),L_atole
     &)
      !}
C SRG_vlv.fgi( 550, 290):���������� ������� ��� 2,20SRG10CF058XQ01
      Call REG_RAS_MAN(deltat,.TRUE.,L_ebud,REAL(R_abud,4
     &),REAL(R_etod,4),R_obud,
     & REAL(R_isod,4),R_ibud,R_edud,R_adud,R_osod,REAL(R_atod
     &,4),
     & R_erod,REAL(R_orod,4),R_urod,REAL(R_esod,4),L_usod
     &,
     & R_odud,R_udud,L_ubud,L_irod,L_asod,I_exod,R_afud,R_axod
     &,
     & R_idud,R8_uvod,REAL(100,4),REAL(R_irole,4),REAL(R_itod
     &,4),
     & REAL(R_uxod,4),REAL(R_oxod,4),REAL(R_ixod,4))
C SRG_vlv.fgi( 826, 384):���������� ���������� �������,20SRG10DF058
      R_exole = R8_ari
C SRG_init_1.fgi( 138, 257):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_avole,R_adule,REAL(16.666
     &,4),
     & REAL(R_exole,4),I_ubule,REAL(R_ovole,4),
     & REAL(R_uvole,4),REAL(R_utole,4),
     & REAL(R_otole,4),REAL(R_oxole,4),L_uxole,REAL(R_abule
     &,4),L_ebule,
     & L_ibule,R_axole,REAL(R_ivole,4),REAL(R_evole,4),L_obule
     &)
      !}
C SRG_vlv.fgi( 550, 303):���������� ������� ��� 2,20SRG10CF057XQ01
      Call REG_RAS_MAN(deltat,.TRUE.,L_erud,REAL(R_arud,4
     &),REAL(R_elud,4),R_orud,
     & REAL(R_ikud,4),R_irud,R_esud,R_asud,R_okud,REAL(R_alud
     &,4),
     & R_efud,REAL(R_ofud,4),R_ufud,REAL(R_ekud,4),L_ukud
     &,
     & R_osud,R_usud,L_urud,L_ifud,L_akud,I_epud,R_atud,R_apud
     &,
     & R_isud,R8_umud,REAL(100,4),REAL(R_axole,4),REAL(R_ilud
     &,4),
     & REAL(R_upud,4),REAL(R_opud,4),REAL(R_ipud,4))
C SRG_vlv.fgi( 799, 384):���������� ���������� �������,20SRG10DF057
      R_esavi = R8_eri
C SRG_init_1.fgi(  50, 245):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_aravi,R_avavi,REAL(16.666
     &,4),
     & REAL(R_esavi,4),I_utavi,REAL(R_oravi,4),
     & REAL(R_uravi,4),REAL(R_upavi,4),
     & REAL(R_opavi,4),REAL(R_osavi,4),L_usavi,REAL(R_atavi
     &,4),L_etavi,
     & L_itavi,R_asavi,REAL(R_iravi,4),REAL(R_eravi,4),L_otavi
     &)
      !}
C SRG_vlv.fgi( 452, 517):���������� ������� ��� 2,20SRG10CF055XQ01
      Call REG_RAS_MAN(deltat,.TRUE.,L_efaf,REAL(R_afaf,4
     &),REAL(R_exud,4),R_ofaf,
     & REAL(R_ivud,4),R_ifaf,R_ekaf,R_akaf,R_ovud,REAL(R_axud
     &,4),
     & R_etud,REAL(R_otud,4),R_utud,REAL(R_evud,4),L_uvud
     &,
     & R_okaf,R_ukaf,L_ufaf,L_itud,L_avud,I_edaf,R_alaf,R_adaf
     &,
     & R_ikaf,R8_ubaf,REAL(100,4),REAL(R_asavi,4),REAL(R_ixud
     &,4),
     & REAL(R_udaf,4),REAL(R_odaf,4),REAL(R_idaf,4))
C SRG_vlv.fgi( 879, 405):���������� ���������� �������,20SRG10DF055
      R_oxavi = R8_iri
C SRG_init_1.fgi(  50, 249):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ovavi,R_idevi,REAL(16.666
     &,4),
     & REAL(R_oxavi,4),I_edevi,REAL(R_exavi,4),
     & REAL(R_ixavi,4),REAL(R_ivavi,4),
     & REAL(R_evavi,4),REAL(R_abevi,4),L_ebevi,REAL(R_ibevi
     &,4),L_obevi,
     & L_ubevi,R_ekoru,REAL(R_axavi,4),REAL(R_uvavi,4),L_adevi
     &)
      !}
C SRG_vlv.fgi( 452, 530):���������� ������� ��� 2,20SRG10CF054XQ01
      R_(204) = R_ikoru + (-R_ekoru)
C SRG_logic.fgi( 233, 351):��������
      R_(203)=abs(R_(204))
C SRG_logic.fgi( 240, 351):���������� ��������
      L_(450)=R_(203).lt.R0_akoru
C SRG_logic.fgi( 250, 351):���������� <
      Call REG_RAS_MAN(deltat,.TRUE.,L_ipef,REAL(R_epef,4
     &),REAL(R_ikef,4),R_upef,
     & REAL(R_ofef,4),R_opef,R_iref,R_eref,R_ufef,REAL(R_ekef
     &,4),
     & R_idef,REAL(R_udef,4),R_afef,REAL(R_ifef,4),L_akef
     &,
     & R_uref,R_asef,L_aref,L_odef,L_efef,I_imef,R_esef,R_emef
     &,
     & R_oref,R8_amef,REAL(100,4),REAL(R_ekoru,4),REAL(R_okef
     &,4),
     & REAL(R_apef,4),REAL(R_umef,4),REAL(R_omef,4))
C SRG_vlv.fgi( 852, 405):���������� ���������� �������,20SRG10DF054
      R_ekevi = R8_ori
C SRG_init_1.fgi(  50, 253):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_afevi,R_amevi,REAL(16.666
     &,4),
     & REAL(R_ekevi,4),I_ulevi,REAL(R_ofevi,4),
     & REAL(R_ufevi,4),REAL(R_udevi,4),
     & REAL(R_odevi,4),REAL(R_okevi,4),L_ukevi,REAL(R_alevi
     &,4),L_elevi,
     & L_ilevi,R_akevi,REAL(R_ifevi,4),REAL(R_efevi,4),L_olevi
     &)
      !}
C SRG_vlv.fgi( 452, 543):���������� ������� ��� 2,20SRG10CF053XQ01
      Call REG_RAS_MAN(deltat,.TRUE.,L_idif,REAL(R_edif,4
     &),REAL(R_ivef,4),R_udif,
     & REAL(R_otef,4),R_odif,R_ifif,R_efif,R_utef,REAL(R_evef
     &,4),
     & R_isef,REAL(R_usef,4),R_atef,REAL(R_itef,4),L_avef
     &,
     & R_ufif,R_akif,L_afif,L_osef,L_etef,I_ibif,R_ekif,R_ebif
     &,
     & R_ofif,R8_abif,REAL(100,4),REAL(R_akevi,4),REAL(R_ovef
     &,4),
     & REAL(R_adif,4),REAL(R_ubif,4),REAL(R_obif,4))
C SRG_vlv.fgi( 826, 405):���������� ���������� �������,20SRG10DF053
      R_upevi = R8_uri
C SRG_init_1.fgi(  50, 257):��������
      Call SRG_1(ext_deltat)
      End
      Subroutine SRG_1(ext_deltat)
      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      Include 'SRG.fh'

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      !{
      Call DAT_ANA_2_HANDLER(deltat,R_omevi,R_osevi,REAL(16.666
     &,4),
     & REAL(R_upevi,4),I_isevi,REAL(R_epevi,4),
     & REAL(R_ipevi,4),REAL(R_imevi,4),
     & REAL(R_emevi,4),REAL(R_erevi,4),L_irevi,REAL(R_orevi
     &,4),L_urevi,
     & L_asevi,R_opevi,REAL(R_apevi,4),REAL(R_umevi,4),L_esevi
     &)
      !}
C SRG_vlv.fgi( 452, 556):���������� ������� ��� 2,20SRG10CF052XQ01
      Call REG_RAS_MAN(deltat,.TRUE.,L_omul,REAL(R_imul,4
     &),REAL(R_oful,4),R_apul,
     & REAL(R_udul,4),R_umul,R_opul,R_ipul,R_aful,REAL(R_iful
     &,4),
     & R_obul,REAL(R_adul,4),R_edul,REAL(R_odul,4),L_eful
     &,
     & R_arul,R_erul,L_epul,L_ubul,L_idul,I_olul,R_irul,R_ilul
     &,
     & R_upul,R8_elul,REAL(100,4),REAL(R_opevi,4),REAL(R_uful
     &,4),
     & REAL(R_emul,4),REAL(R_amul,4),REAL(R_ulul,4))
C SRG_vlv.fgi( 799, 405):���������� ���������� �������,20SRG10DF052
      R_oxili = R8_asi
C SRG_init_1.fgi( 224, 261):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_edoli,4),REAL
     &(R_oxili,4),R_ivili,
     & R_idoli,REAL(1e-3,4),REAL(R_axili,4),
     & REAL(R_exili,4),REAL(R_evili,4),
     & REAL(R_avili,4),I_adoli,REAL(R_uxili,4),L_aboli,
     & REAL(R_eboli,4),L_iboli,L_oboli,R_ixili,REAL(R_uvili
     &,4),REAL(R_ovili,4),L_uboli)
      !}
C SRG_vlv.fgi( 700, 530):���������� ������� ��������,20SRG10CP069XQ01
      R_ulupi = R8_esi
C SRG_init_1.fgi( 224, 265):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_ipupi,4),REAL
     &(R_ulupi,4),R_okupi,
     & R_opupi,REAL(1e-3,4),REAL(R_elupi,4),
     & REAL(R_ilupi,4),REAL(R_ikupi,4),
     & REAL(R_ekupi,4),I_epupi,REAL(R_amupi,4),L_emupi,
     & REAL(R_imupi,4),L_omupi,L_umupi,R_olupi,REAL(R_alupi
     &,4),REAL(R_ukupi,4),L_apupi)
      !}
C SRG_vlv.fgi( 626, 504):���������� ������� ��������,20SRG10CP068XQ01
      R_osupi = R8_isi
C SRG_init_1.fgi( 224, 269):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_evupi,4),REAL
     &(R_osupi,4),R_irupi,
     & R_ivupi,REAL(1e-3,4),REAL(R_asupi,4),
     & REAL(R_esupi,4),REAL(R_erupi,4),
     & REAL(R_arupi,4),I_avupi,REAL(R_usupi,4),L_atupi,
     & REAL(R_etupi,4),L_itupi,L_otupi,R_isupi,REAL(R_urupi
     &,4),REAL(R_orupi,4),L_utupi)
      !}
C SRG_vlv.fgi( 626, 517):���������� ������� ��������,20SRG10CP067XQ01
      R_ilore = R8_osi
C SRG_init_1.fgi( 138, 261):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_apore,4),REAL
     &(R_ilore,4),R_ekore,
     & R_epore,REAL(1e-3,4),REAL(R_ukore,4),
     & REAL(R_alore,4),REAL(R_akore,4),
     & REAL(R_ufore,4),I_umore,REAL(R_olore,4),L_ulore,
     & REAL(R_amore,4),L_emore,L_imore,R_elore,REAL(R_okore
     &,4),REAL(R_ikore,4),L_omore)
      !}
C SRG_vlv.fgi( 452, 290):���������� ������� ��������,20SRG10CP061XQ01
      R_efame = R8_usi
C SRG_init_1.fgi( 138, 265):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_ukame,4),REAL
     &(R_efame,4),R_adame,
     & R_alame,REAL(1e-3,4),REAL(R_odame,4),
     & REAL(R_udame,4),REAL(R_ubame,4),
     & REAL(R_obame,4),I_okame,REAL(R_ifame,4),L_ofame,
     & REAL(R_ufame,4),L_akame,L_ekame,R_afame,REAL(R_idame
     &,4),REAL(R_edame,4),L_ikame)
      !}
C SRG_vlv.fgi( 502, 290):���������� ������� ��������,20SRG10CP060XQ01
      R_apame = R8_ati
C SRG_init_1.fgi( 138, 269):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_orame,4),REAL
     &(R_apame,4),R_ulame,
     & R_urame,REAL(1e-3,4),REAL(R_imame,4),
     & REAL(R_omame,4),REAL(R_olame,4),
     & REAL(R_ilame,4),I_irame,REAL(R_epame,4),L_ipame,
     & REAL(R_opame,4),L_upame,L_arame,R_umame,REAL(R_emame
     &,4),REAL(R_amame,4),L_erame)
      !}
C SRG_vlv.fgi( 476, 290):���������� ������� ��������,20SRG10CP059XQ01
      R_ilori = R8_eti
C SRG_init_1.fgi(  50, 261):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_apori,4),REAL
     &(R_ilori,4),R_ekori,
     & R_epori,REAL(1e-3,4),REAL(R_ukori,4),
     & REAL(R_alori,4),REAL(R_akori,4),
     & REAL(R_ufori,4),I_umori,REAL(R_olori,4),L_ulori,
     & REAL(R_amori,4),L_emori,L_imori,R_elori,REAL(R_okori
     &,4),REAL(R_ikori,4),L_omori)
      !}
C SRG_vlv.fgi( 502, 530):���������� ������� ��������,20SRG10CP053XQ01
      R_obavi = R8_iti
C SRG_init_1.fgi(  50, 265):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_efavi,4),REAL
     &(R_obavi,4),R_ixuti,
     & R_ifavi,REAL(1e-3,4),REAL(R_abavi,4),
     & REAL(R_ebavi,4),REAL(R_exuti,4),
     & REAL(R_axuti,4),I_afavi,REAL(R_ubavi,4),L_adavi,
     & REAL(R_edavi,4),L_idavi,L_odavi,R_ibavi,REAL(R_uxuti
     &,4),REAL(R_oxuti,4),L_udavi)
      !}
C SRG_vlv.fgi( 428, 504):���������� ������� ��������,20SRG10CP052XQ01
      R_ilavi = R8_oti
C SRG_init_1.fgi(  50, 269):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_apavi,4),REAL
     &(R_ilavi,4),R_ekavi,
     & R_epavi,REAL(1e-3,4),REAL(R_ukavi,4),
     & REAL(R_alavi,4),REAL(R_akavi,4),
     & REAL(R_ufavi,4),I_umavi,REAL(R_olavi,4),L_ulavi,
     & REAL(R_amavi,4),L_emavi,L_imavi,R_elavi,REAL(R_okavi
     &,4),REAL(R_ikavi,4),L_omavi)
      !}
C SRG_vlv.fgi( 428, 517):���������� ������� ��������,20SRG10CP051XQ01
      R_oferi = R8_uti
C SRG_init_1.fgi( 224, 273):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_eleri,4),REAL
     &(R_oferi,4),R_ideri,
     & R_ileri,REAL(1e-6,4),REAL(R_aferi,4),
     & REAL(R_eferi,4),REAL(R_ederi,4),
     & REAL(R_aderi,4),I_aleri,REAL(R_uferi,4),L_akeri,
     & REAL(R_ekeri,4),L_ikeri,L_okeri,R_iferi,REAL(R_uderi
     &,4),REAL(R_oderi,4),L_ukeri)
      !}
C SRG_vlv.fgi( 626, 530):���������� ������� ��������,20SRG10CP072XQ01
      R_iperi = R8_avi
C SRG_init_1.fgi( 224, 277):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_aseri,4),REAL
     &(R_iperi,4),R_emeri,
     & R_eseri,REAL(1e-6,4),REAL(R_umeri,4),
     & REAL(R_aperi,4),REAL(R_ameri,4),
     & REAL(R_uleri,4),I_ureri,REAL(R_operi,4),L_uperi,
     & REAL(R_areri,4),L_ereri,L_ireri,R_eperi,REAL(R_omeri
     &,4),REAL(R_imeri,4),L_oreri)
      !}
C SRG_vlv.fgi( 626, 543):���������� ������� ��������,20SRG10CP071XQ01
      R_everi = R8_evi
C SRG_init_1.fgi( 224, 281):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_uxeri,4),REAL
     &(R_everi,4),R_ateri,
     & R_abiri,REAL(1e-6,4),REAL(R_oteri,4),
     & REAL(R_uteri,4),REAL(R_useri,4),
     & REAL(R_oseri,4),I_oxeri,REAL(R_iveri,4),L_overi,
     & REAL(R_uveri,4),L_axeri,L_exeri,R_averi,REAL(R_iteri
     &,4),REAL(R_eteri,4),L_ixeri)
      !}
C SRG_vlv.fgi( 626, 556):���������� ������� ��������,20SRG10CP070XQ01
      R_ufule = R8_ivi
C SRG_init_1.fgi( 138, 273):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_ilule,4),REAL
     &(R_ufule,4),R_odule,
     & R_olule,REAL(1e-6,4),REAL(R_efule,4),
     & REAL(R_ifule,4),REAL(R_idule,4),
     & REAL(R_edule,4),I_elule,REAL(R_akule,4),L_ekule,
     & REAL(R_ikule,4),L_okule,L_ukule,R_ofule,REAL(R_afule
     &,4),REAL(R_udule,4),L_alule)
      !}
C SRG_vlv.fgi( 526, 198):���������� ������� ��������,20SRG10CP064XQ01
      R_opule = R8_ovi
C SRG_init_1.fgi( 138, 277):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_esule,4),REAL
     &(R_opule,4),R_imule,
     & R_isule,REAL(1e-6,4),REAL(R_apule,4),
     & REAL(R_epule,4),REAL(R_emule,4),
     & REAL(R_amule,4),I_asule,REAL(R_upule,4),L_arule,
     & REAL(R_erule,4),L_irule,L_orule,R_ipule,REAL(R_umule
     &,4),REAL(R_omule,4),L_urule)
      !}
C SRG_vlv.fgi( 526, 211):���������� ������� ��������,20SRG10CP063XQ01
      R_ivule = R8_uvi
C SRG_init_1.fgi( 138, 281):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_abame,4),REAL
     &(R_ivule,4),R_etule,
     & R_ebame,REAL(1e-6,4),REAL(R_utule,4),
     & REAL(R_avule,4),REAL(R_atule,4),
     & REAL(R_usule,4),I_uxule,REAL(R_ovule,4),L_uvule,
     & REAL(R_axule,4),L_exule,L_ixule,R_evule,REAL(R_otule
     &,4),REAL(R_itule,4),L_oxule)
      !}
C SRG_vlv.fgi( 526, 224):���������� ������� ��������,20SRG10CP062XQ01
      R_ivevi = R8_axi
C SRG_init_1.fgi(  50, 273):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_abivi,4),REAL
     &(R_ivevi,4),R_etevi,
     & R_ebivi,REAL(1e-6,4),REAL(R_utevi,4),
     & REAL(R_avevi,4),REAL(R_atevi,4),
     & REAL(R_usevi,4),I_uxevi,REAL(R_ovevi,4),L_uvevi,
     & REAL(R_axevi,4),L_exevi,L_ixevi,R_evevi,REAL(R_otevi
     &,4),REAL(R_itevi,4),L_oxevi)
      !}
C SRG_vlv.fgi( 428, 530):���������� ������� ��������,20SRG10CP056XQ01
      R_efivi = R8_exi
C SRG_init_1.fgi(  50, 277):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_ukivi,4),REAL
     &(R_efivi,4),R_adivi,
     & R_alivi,REAL(1e-6,4),REAL(R_odivi,4),
     & REAL(R_udivi,4),REAL(R_ubivi,4),
     & REAL(R_obivi,4),I_okivi,REAL(R_ifivi,4),L_ofivi,
     & REAL(R_ufivi,4),L_akivi,L_ekivi,R_afivi,REAL(R_idivi
     &,4),REAL(R_edivi,4),L_ikivi)
      !}
C SRG_vlv.fgi( 428, 543):���������� ������� ��������,20SRG10CP055XQ01
      R_apivi = R8_ixi
C SRG_init_1.fgi(  50, 281):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_orivi,4),REAL
     &(R_apivi,4),R_ulivi,
     & R_urivi,REAL(1e-6,4),REAL(R_imivi,4),
     & REAL(R_omivi,4),REAL(R_olivi,4),
     & REAL(R_ilivi,4),I_irivi,REAL(R_epivi,4),L_ipivi,
     & REAL(R_opivi,4),L_upivi,L_arivi,R_umivi,REAL(R_emivi
     &,4),REAL(R_amivi,4),L_erivi)
      !}
C SRG_vlv.fgi( 428, 556):���������� ������� ��������,20SRG10CP054XQ01
      R_(2) = 0.0
C SRG_temp_pss.fgi( 247, 167):��������� (RE4) (�������)
      R_(1) = 1.0
C SRG_temp_pss.fgi( 247, 165):��������� (RE4) (�������)
      R_(8) = 400.0
C SRG_temp_pss.fgi( 247, 174):��������� (RE4) (�������)
      R_(9) = 10.0
C SRG_temp_pss.fgi( 247, 176):��������� (RE4) (�������)
      R_(10) = R_(9) * R_(8)
C SRG_temp_pss.fgi( 261, 176):����������
      R_(5)=0.0
C SRG_temp_pss.fgi( 266, 178):��������� ������������ ����
      !Constant L_(14) = SRG_temp_pssClJ196 /.false./
      L_(14)=L0_ado
C SRG_temp_pss.fgi( 262, 151):��������� ���. ������������� 
      R_(7) = 0.0
C SRG_temp_pss.fgi( 233, 159):��������� (RE4) (�������)
      R_(4) = 20.0
C SRG_temp_pss.fgi( 271, 181):��������� (RE4) (�������)
      R_(3) = 2000.0
C SRG_temp_pss.fgi( 271, 184):��������� (RE4) (�������)
      R_(12) = 0.0
C SRG_temp_pss.fgi( 244, 221):��������� (RE4) (�������)
      R_(11) = 1.0
C SRG_temp_pss.fgi( 244, 219):��������� (RE4) (�������)
      R_(18) = 400.0
C SRG_temp_pss.fgi( 244, 228):��������� (RE4) (�������)
      R_(19) = 10.0
C SRG_temp_pss.fgi( 244, 230):��������� (RE4) (�������)
      R_(20) = R_(19) * R_(18)
C SRG_temp_pss.fgi( 258, 230):����������
      R_(15)=0.0
C SRG_temp_pss.fgi( 263, 232):��������� ������������ ����
      !Constant L_(16) = SRG_temp_pssClJ161 /.false./
      L_(16)=L0_efo
C SRG_temp_pss.fgi( 259, 205):��������� ���. ������������� 
      R_(17) = 0.0
C SRG_temp_pss.fgi( 230, 213):��������� (RE4) (�������)
      R_(14) = 20.0
C SRG_temp_pss.fgi( 268, 235):��������� (RE4) (�������)
      R_(13) = 2000.0
C SRG_temp_pss.fgi( 268, 238):��������� (RE4) (�������)
      R_(22) = 0.0
C SRG_temp_pss.fgi( 243, 269):��������� (RE4) (�������)
      R_(21) = 1.0
C SRG_temp_pss.fgi( 243, 267):��������� (RE4) (�������)
      R_(28) = 400.0
C SRG_temp_pss.fgi( 243, 276):��������� (RE4) (�������)
      R_(29) = 10.0
C SRG_temp_pss.fgi( 243, 278):��������� (RE4) (�������)
      R_(30) = R_(29) * R_(28)
C SRG_temp_pss.fgi( 257, 278):����������
      R_(25)=0.0
C SRG_temp_pss.fgi( 262, 280):��������� ������������ ����
      !Constant L_(18) = SRG_temp_pssClJ126 /.false./
      L_(18)=L0_iko
C SRG_temp_pss.fgi( 258, 253):��������� ���. ������������� 
      R_(27) = 0.0
C SRG_temp_pss.fgi( 229, 261):��������� (RE4) (�������)
      R_(24) = 20.0
C SRG_temp_pss.fgi( 267, 283):��������� (RE4) (�������)
      R_(23) = 2000.0
C SRG_temp_pss.fgi( 267, 286):��������� (RE4) (�������)
      R_(32) = 0.0
C SRG_temp_pss.fgi(  78, 169):��������� (RE4) (�������)
      R_(31) = 1.0
C SRG_temp_pss.fgi(  78, 167):��������� (RE4) (�������)
      R_(38) = 400.0
C SRG_temp_pss.fgi(  78, 176):��������� (RE4) (�������)
      R_(39) = 10.0
C SRG_temp_pss.fgi(  78, 178):��������� (RE4) (�������)
      R_(40) = R_(39) * R_(38)
C SRG_temp_pss.fgi(  92, 178):����������
      R_(35)=0.0
C SRG_temp_pss.fgi(  97, 180):��������� ������������ ����
      !Constant L_(20) = SRG_temp_pssClJ91 /.false./
      L_(20)=L0_olo
C SRG_temp_pss.fgi(  93, 153):��������� ���. ������������� 
      R_(37) = 0.0
C SRG_temp_pss.fgi(  64, 161):��������� (RE4) (�������)
      R_(34) = 20.0
C SRG_temp_pss.fgi( 102, 183):��������� (RE4) (�������)
      R_(33) = 2000.0
C SRG_temp_pss.fgi( 102, 186):��������� (RE4) (�������)
      R_(42) = 0.0
C SRG_temp_pss.fgi(  75, 223):��������� (RE4) (�������)
      R_(41) = 1.0
C SRG_temp_pss.fgi(  75, 221):��������� (RE4) (�������)
      R_(48) = 400.0
C SRG_temp_pss.fgi(  75, 230):��������� (RE4) (�������)
      R_(49) = 10.0
C SRG_temp_pss.fgi(  75, 232):��������� (RE4) (�������)
      R_(50) = R_(49) * R_(48)
C SRG_temp_pss.fgi(  89, 232):����������
      R_(45)=0.0
C SRG_temp_pss.fgi(  94, 234):��������� ������������ ����
      !Constant L_(22) = SRG_temp_pssClJ56 /.false./
      L_(22)=L0_umo
C SRG_temp_pss.fgi(  90, 207):��������� ���. ������������� 
      R_(47) = 0.0
C SRG_temp_pss.fgi(  61, 215):��������� (RE4) (�������)
      R_(44) = 20.0
C SRG_temp_pss.fgi(  99, 237):��������� (RE4) (�������)
      R_(43) = 2000.0
C SRG_temp_pss.fgi(  99, 240):��������� (RE4) (�������)
      R_(52) = 0.0
C SRG_temp_pss.fgi(  74, 271):��������� (RE4) (�������)
      R_(51) = 1.0
C SRG_temp_pss.fgi(  74, 269):��������� (RE4) (�������)
      R_(58) = 400.0
C SRG_temp_pss.fgi(  74, 278):��������� (RE4) (�������)
      R_(59) = 10.0
C SRG_temp_pss.fgi(  74, 280):��������� (RE4) (�������)
      R_(60) = R_(59) * R_(58)
C SRG_temp_pss.fgi(  88, 280):����������
      R_(55)=0.0
C SRG_temp_pss.fgi(  93, 282):��������� ������������ ����
      !Constant L_(24) = SRG_temp_pssClJ21 /.false./
      L_(24)=L0_aro
C SRG_temp_pss.fgi(  89, 255):��������� ���. ������������� 
      R_(57) = 0.0
C SRG_temp_pss.fgi(  60, 263):��������� (RE4) (�������)
      R_(54) = 20.0
C SRG_temp_pss.fgi(  98, 285):��������� (RE4) (�������)
      R_(53) = 2000.0
C SRG_temp_pss.fgi(  98, 288):��������� (RE4) (�������)
      R_elefu = R8_aso
C SRG_init.fgi(  48, 361):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_akefu,R_apefu,REAL(1
     &,4),
     & REAL(R_elefu,4),I_umefu,REAL(R_okefu,4),
     & REAL(R_ukefu,4),REAL(R_ufefu,4),
     & REAL(R_ofefu,4),REAL(R_olefu,4),L_ulefu,REAL(R_amefu
     &,4),L_emefu,
     & L_imefu,R_alefu,REAL(R_ikefu,4),REAL(R_ekefu,4),L_omefu
     &)
      !}
C SRG_vlv.fgi( 112, 658):���������� ������� ��� 2,20SRG10CQ001XQ01
      Call REG_RAS_MAN(deltat,.TRUE.,L_utak,REAL(R_otak,4
     &),REAL(R_upak,4),R_evak,
     & REAL(R_apak,4),R_avak,R_uvak,R_ovak,R_epak,REAL(R_opak
     &,4),
     & R_ulak,REAL(R_emak,4),R_imak,REAL(R_umak,4),L_ipak
     &,
     & R_exak,R_ixak,L_ivak,L_amak,L_omak,I_usak,R_oxak,R_osak
     &,
     & R_axak,R8_isak,REAL(100000,4),REAL(R_alefu,4),REAL
     &(R_arak,4),
     & REAL(R_itak,4),REAL(R_etak,4),REAL(R_atak,4))
C SRG_vlv.fgi( 723, 405):���������� ���������� �������,20SRG10AA215
      R_obefu = R8_eso
C SRG_init.fgi(  48, 365):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ixafu,R_ifefu,REAL(1
     &,4),
     & REAL(R_obefu,4),I_efefu,REAL(R_abefu,4),
     & REAL(R_ebefu,4),REAL(R_exafu,4),
     & REAL(R_axafu,4),REAL(R_adefu,4),L_edefu,REAL(R_idefu
     &,4),L_odefu,
     & L_udefu,R_ibefu,REAL(R_uxafu,4),REAL(R_oxafu,4),L_afefu
     &)
      !}
C SRG_vlv.fgi( 112, 644):���������� ������� ��� 2,20SRG10CM001XQ01
      R_(61) = 0.06
C SRG_init.fgi( 125, 131):��������� (RE4) (�������)
      R_(62) = R8_iso * R_(61)
C SRG_init.fgi( 132, 133):����������
      R_ilaso = R_(62)
C SRG_init.fgi( 140, 133):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ekaso,R_epaso,REAL(1
     &,4),
     & REAL(R_ilaso,4),I_apaso,REAL(R_ukaso,4),
     & REAL(R_alaso,4),REAL(R_akaso,4),
     & REAL(R_ufaso,4),REAL(R_ulaso,4),L_amaso,REAL(R_emaso
     &,4),L_imaso,
     & L_omaso,R_elaso,REAL(R_okaso,4),REAL(R_ikaso,4),L_umaso
     &)
      !}
C SRG_vlv.fgi(  26, 476):���������� ������� ��� 2,20SRG10CF027XQ01
      R_ubaso = R8_oso
C SRG_init.fgi( 140, 138):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_oxuro,R_ofaso,REAL(1
     &,4),
     & REAL(R_ubaso,4),I_ifaso,REAL(R_ebaso,4),
     & REAL(R_ibaso,4),REAL(R_ixuro,4),
     & REAL(R_exuro,4),REAL(R_edaso,4),L_idaso,REAL(R_odaso
     &,4),L_udaso,
     & L_afaso,R_obaso,REAL(R_abaso,4),REAL(R_uxuro,4),L_efaso
     &)
      !}
C SRG_vlv.fgi(  55, 476):���������� ������� ��� 2,20SRG10CF028XQ01
      R_asaso = R8_uso
C SRG_init.fgi( 140, 146):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_upaso,R_utaso,REAL(1
     &,4),
     & REAL(R_asaso,4),I_otaso,REAL(R_iraso,4),
     & REAL(R_oraso,4),REAL(R_opaso,4),
     & REAL(R_ipaso,4),REAL(R_isaso,4),L_osaso,REAL(R_usaso
     &,4),L_ataso,
     & L_etaso,R_uraso,REAL(R_eraso,4),REAL(R_araso,4),L_itaso
     &)
      !}
C SRG_vlv.fgi( 169, 476):���������� ������� ��� 2,20SRG10CP028XQ01
      R_eveso = R8_ato
C SRG_init.fgi( 140, 142):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ateso,R_abiso,REAL(1
     &,4),
     & REAL(R_eveso,4),I_uxeso,REAL(R_oteso,4),
     & REAL(R_uteso,4),REAL(R_useso,4),
     & REAL(R_oseso,4),REAL(R_oveso,4),L_uveso,REAL(R_axeso
     &,4),L_exeso,
     & L_ixeso,R_aveso,REAL(R_iteso,4),REAL(R_eteso,4),L_oxeso
     &)
      !}
C SRG_vlv.fgi( 141, 476):���������� ������� ��� 2,20SRG10CT014XQ01
      R_exaso = R8_eto
C SRG_init.fgi( 140, 150):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_ubeso,4),REAL
     &(R_exaso,4),R_evaso,
     & R_adeso,REAL(1,4),REAL(R_uvaso,4),
     & REAL(R_axaso,4),REAL(R_atitu,4),
     & REAL(R_avaso,4),I_obeso,REAL(R_ixaso,4),L_oxaso,
     & REAL(R_uxaso,4),L_abeso,L_ebeso,R_etitu,REAL(R_ovaso
     &,4),REAL(R_ivaso,4),L_ibeso)
      !}
C SRG_vlv.fgi( 197, 476):���������� ������� ��������,20SRG10CP027XQ01
      L_usitu=R_etitu.gt.R_atitu
C SRG_logic.fgi( 143, 378):���������� >
      L_(456)=R_etitu.lt.R_atitu
C SRG_logic.fgi(  57, 277):���������� <
      Call REG_RAS_MAN(deltat,.TRUE.,L_aluru,REAL(R_ukuru
     &,4),REAL(R_aduru,4),R_iluru,
     & REAL(R_eburu,4),R_eluru,R_amuru,R_uluru,R_iburu,REAL
     &(R_uburu,4),
     & R_axoru,REAL(R_ixoru,4),R_oxoru,REAL(R_aburu,4),L_oburu
     &,
     & R_imuru,R_omuru,L_oluru,L_exoru,L_uxoru,I_akuru,R_umuru
     &,R_ufuru,
     & R_emuru,R8_ofuru,REAL(100000,4),REAL(R_etitu,4),REAL
     &(R_eduru,4),
     & REAL(R_okuru,4),REAL(R_ikuru,4),REAL(R_ekuru,4))
C SRG_logic.fgi( 223, 584):���������� ���������� �������,20SRG10DF028
      R_aturo = R8_ito
C SRG_init.fgi( 140, 154):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_ovuro,4),REAL
     &(R_aturo,4),R_ururo,
     & R_uvuro,REAL(1,4),REAL(R_isuro,4),
     & REAL(R_osuro,4),REAL(R_oruro,4),
     & REAL(R_iruro,4),I_ivuro,REAL(R_eturo,4),L_ituro,
     & REAL(R_oturo,4),L_uturo,L_avuro,R_usuro,REAL(R_esuro
     &,4),REAL(R_asuro,4),L_evuro)
      !}
C SRG_vlv.fgi( 225, 630):���������� ������� ��������,20SRG10CP037XQ01
      R_(63) = 0.06
C SRG_init.fgi( 125, 170):��������� (RE4) (�������)
      R_(64) = R8_oto * R_(63)
C SRG_init.fgi( 132, 172):����������
      R_imiso = R_(64)
C SRG_init.fgi( 140, 172):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_eliso,R_eriso,REAL(1
     &,4),
     & REAL(R_imiso,4),I_ariso,REAL(R_uliso,4),
     & REAL(R_amiso,4),REAL(R_aliso,4),
     & REAL(R_ukiso,4),REAL(R_umiso,4),L_apiso,REAL(R_episo
     &,4),L_ipiso,
     & L_opiso,R_emiso,REAL(R_oliso,4),REAL(R_iliso,4),L_upiso
     &)
      !}
C SRG_vlv.fgi(  26, 490):���������� ������� ��� 2,20SRG10CF025XQ01
      R_udiso = R8_uto
C SRG_init.fgi( 140, 177):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_obiso,R_okiso,REAL(1
     &,4),
     & REAL(R_udiso,4),I_ikiso,REAL(R_ediso,4),
     & REAL(R_idiso,4),REAL(R_ibiso,4),
     & REAL(R_ebiso,4),REAL(R_efiso,4),L_ifiso,REAL(R_ofiso
     &,4),L_ufiso,
     & L_akiso,R_odiso,REAL(R_adiso,4),REAL(R_ubiso,4),L_ekiso
     &)
      !}
C SRG_vlv.fgi(  55, 490):���������� ������� ��� 2,20SRG10CF026XQ01
      R_uboso = R8_avo
C SRG_init.fgi( 140, 185):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_oxiso,R_ofoso,REAL(1
     &,4),
     & REAL(R_uboso,4),I_ifoso,REAL(R_eboso,4),
     & REAL(R_iboso,4),REAL(R_ixiso,4),
     & REAL(R_exiso,4),REAL(R_edoso,4),L_idoso,REAL(R_odoso
     &,4),L_udoso,
     & L_afoso,R_oboso,REAL(R_aboso,4),REAL(R_uxiso,4),L_efoso
     &)
      !}
C SRG_vlv.fgi( 169, 490):���������� ������� ��� 2,20SRG10CP026XQ01
      R_akuso = R8_evo
C SRG_init.fgi( 140, 181):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_uduso,R_uluso,REAL(1
     &,4),
     & REAL(R_akuso,4),I_oluso,REAL(R_ifuso,4),
     & REAL(R_ofuso,4),REAL(R_oduso,4),
     & REAL(R_iduso,4),REAL(R_ikuso,4),L_okuso,REAL(R_ukuso
     &,4),L_aluso,
     & L_eluso,R_ufuso,REAL(R_efuso,4),REAL(R_afuso,4),L_iluso
     &)
      !}
C SRG_vlv.fgi( 141, 490):���������� ������� ��� 2,20SRG10CT013XQ01
      R_aloso = R8_ivo
C SRG_init.fgi( 140, 189):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_omoso,4),REAL
     &(R_aloso,4),R_akoso,
     & R_umoso,REAL(1,4),REAL(R_okoso,4),
     & REAL(R_ukoso,4),REAL(R_otitu,4),
     & REAL(R_ufoso,4),I_imoso,REAL(R_eloso,4),L_iloso,
     & REAL(R_oloso,4),L_uloso,L_amoso,R_utitu,REAL(R_ikoso
     &,4),REAL(R_ekoso,4),L_emoso)
      !}
C SRG_vlv.fgi( 197, 490):���������� ������� ��������,20SRG10CP025XQ01
      L_ititu=R_utitu.gt.R_otitu
C SRG_logic.fgi( 143, 384):���������� >
      L_(457)=R_utitu.lt.R_otitu
C SRG_logic.fgi(  57, 291):���������� <
      Call REG_RAS_MAN(deltat,.TRUE.,L_axuru,REAL(R_uvuru
     &,4),REAL(R_asuru,4),R_ixuru,
     & REAL(R_eruru,4),R_exuru,R_abasu,R_uxuru,R_iruru,REAL
     &(R_ururu,4),
     & R_apuru,REAL(R_ipuru,4),R_opuru,REAL(R_aruru,4),L_oruru
     &,
     & R_ibasu,R_obasu,L_oxuru,L_epuru,L_upuru,I_avuru,R_ubasu
     &,R_uturu,
     & R_ebasu,R8_oturu,REAL(100000,4),REAL(R_utitu,4),REAL
     &(R_esuru,4),
     & REAL(R_ovuru,4),REAL(R_ivuru,4),REAL(R_evuru,4))
C SRG_logic.fgi( 199, 584):���������� ���������� �������,20SRG10DF026
      R_atiso = R8_ovo
C SRG_init.fgi( 140, 193):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_oviso,4),REAL
     &(R_atiso,4),R_uriso,
     & R_uviso,REAL(1,4),REAL(R_isiso,4),
     & REAL(R_osiso,4),REAL(R_oriso,4),
     & REAL(R_iriso,4),I_iviso,REAL(R_etiso,4),L_itiso,
     & REAL(R_otiso,4),L_utiso,L_aviso,R_usiso,REAL(R_esiso
     &,4),REAL(R_asiso,4),L_eviso)
      !}
C SRG_vlv.fgi( 225, 644):���������� ������� ��������,20SRG10CP036XQ01
      R_(65) = 0.06
C SRG_init.fgi( 125, 209):��������� (RE4) (�������)
      R_(66) = R8_uvo * R_(65)
C SRG_init.fgi( 132, 211):����������
      R_evuso = R_(66)
C SRG_init.fgi( 140, 211):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_atuso,R_abato,REAL(1
     &,4),
     & REAL(R_evuso,4),I_uxuso,REAL(R_otuso,4),
     & REAL(R_utuso,4),REAL(R_ususo,4),
     & REAL(R_osuso,4),REAL(R_ovuso,4),L_uvuso,REAL(R_axuso
     &,4),L_exuso,
     & L_ixuso,R_avuso,REAL(R_ituso,4),REAL(R_etuso,4),L_oxuso
     &)
      !}
C SRG_vlv.fgi(  26, 504):���������� ������� ��� 2,20SRG10CF023XQ01
      R_opuso = R8_axo
C SRG_init.fgi( 140, 216):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_imuso,R_isuso,REAL(1
     &,4),
     & REAL(R_opuso,4),I_esuso,REAL(R_apuso,4),
     & REAL(R_epuso,4),REAL(R_emuso,4),
     & REAL(R_amuso,4),REAL(R_aruso,4),L_eruso,REAL(R_iruso
     &,4),L_oruso,
     & L_uruso,R_ipuso,REAL(R_umuso,4),REAL(R_omuso,4),L_asuso
     &)
      !}
C SRG_vlv.fgi(  55, 504):���������� ������� ��� 2,20SRG10CF024XQ01
      R_udato = R8_exo
C SRG_init.fgi( 140, 224):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_obato,R_okato,REAL(1
     &,4),
     & REAL(R_udato,4),I_ikato,REAL(R_edato,4),
     & REAL(R_idato,4),REAL(R_ibato,4),
     & REAL(R_ebato,4),REAL(R_efato,4),L_ifato,REAL(R_ofato
     &,4),L_ufato,
     & L_akato,R_odato,REAL(R_adato,4),REAL(R_ubato,4),L_ekato
     &)
      !}
C SRG_vlv.fgi( 169, 504):���������� ������� ��� 2,20SRG10CP024XQ01
      R_ixo = R8_oxo
C SRG_init.fgi( 140, 220):��������
      R_amato = R8_uxo
C SRG_init.fgi( 140, 228):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_opato,4),REAL
     &(R_amato,4),R_alato,
     & R_upato,REAL(1,4),REAL(R_olato,4),
     & REAL(R_ulato,4),REAL(R_evitu,4),
     & REAL(R_ukato,4),I_ipato,REAL(R_emato,4),L_imato,
     & REAL(R_omato,4),L_umato,L_apato,R_ivitu,REAL(R_ilato
     &,4),REAL(R_elato,4),L_epato)
      !}
C SRG_vlv.fgi( 197, 504):���������� ������� ��������,20SRG10CP023XQ01
      L_avitu=R_ivitu.gt.R_evitu
C SRG_logic.fgi( 143, 390):���������� >
      L_(458)=R_ivitu.lt.R_evitu
C SRG_logic.fgi(  57, 305):���������� <
      Call REG_RAS_MAN(deltat,.TRUE.,L_apasu,REAL(R_umasu
     &,4),REAL(R_akasu,4),R_ipasu,
     & REAL(R_efasu,4),R_epasu,R_arasu,R_upasu,R_ifasu,REAL
     &(R_ufasu,4),
     & R_adasu,REAL(R_idasu,4),R_odasu,REAL(R_afasu,4),L_ofasu
     &,
     & R_irasu,R_orasu,L_opasu,L_edasu,L_udasu,I_amasu,R_urasu
     &,R_ulasu,
     & R_erasu,R8_olasu,REAL(100000,4),REAL(R_ivitu,4),REAL
     &(R_ekasu,4),
     & REAL(R_omasu,4),REAL(R_imasu,4),REAL(R_emasu,4))
C SRG_logic.fgi( 176, 497):���������� ���������� �������,20SRG10DF024
      R_(67) = 0.06
C SRG_init.fgi( 125, 244):��������� (RE4) (�������)
      R_(68) = R8_abu * R_(67)
C SRG_init.fgi( 132, 246):����������
      R_avito = R_(68)
C SRG_init.fgi( 140, 246):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_usito,R_uxito,REAL(1
     &,4),
     & REAL(R_avito,4),I_oxito,REAL(R_itito,4),
     & REAL(R_otito,4),REAL(R_osito,4),
     & REAL(R_isito,4),REAL(R_ivito,4),L_ovito,REAL(R_uvito
     &,4),L_axito,
     & L_exito,R_utito,REAL(R_etito,4),REAL(R_atito,4),L_ixito
     &)
      !}
C SRG_vlv.fgi(  26, 518):���������� ������� ��� 2,20SRG10CF021XQ01
      R_ipito = R8_ebu
C SRG_init.fgi( 140, 251):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_emito,R_esito,REAL(1
     &,4),
     & REAL(R_ipito,4),I_asito,REAL(R_umito,4),
     & REAL(R_apito,4),REAL(R_amito,4),
     & REAL(R_ulito,4),REAL(R_upito,4),L_arito,REAL(R_erito
     &,4),L_irito,
     & L_orito,R_epito,REAL(R_omito,4),REAL(R_imito,4),L_urito
     &)
      !}
C SRG_vlv.fgi(  55, 518):���������� ������� ��� 2,20SRG10CF022XQ01
      R_odoto = R8_ibu
C SRG_init.fgi( 140, 259):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_iboto,R_ikoto,REAL(1
     &,4),
     & REAL(R_odoto,4),I_ekoto,REAL(R_adoto,4),
     & REAL(R_edoto,4),REAL(R_eboto,4),
     & REAL(R_aboto,4),REAL(R_afoto,4),L_efoto,REAL(R_ifoto
     &,4),L_ofoto,
     & L_ufoto,R_idoto,REAL(R_uboto,4),REAL(R_oboto,4),L_akoto
     &)
      !}
C SRG_vlv.fgi( 169, 518):���������� ������� ��� 2,20SRG10CP022XQ01
      R_obu = R8_ubu
C SRG_init.fgi( 140, 255):��������
      R_uloto = R8_adu
C SRG_init.fgi( 140, 263):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_ipoto,4),REAL
     &(R_uloto,4),R_ukoto,
     & R_opoto,REAL(1,4),REAL(R_iloto,4),
     & REAL(R_oloto,4),REAL(R_uvitu,4),
     & REAL(R_okoto,4),I_epoto,REAL(R_amoto,4),L_emoto,
     & REAL(R_imoto,4),L_omoto,L_umoto,R_axitu,REAL(R_eloto
     &,4),REAL(R_aloto,4),L_apoto)
      !}
C SRG_vlv.fgi( 197, 518):���������� ������� ��������,20SRG10CP021XQ01
      L_ovitu=R_axitu.gt.R_uvitu
C SRG_logic.fgi( 143, 396):���������� >
      L_(459)=R_axitu.lt.R_uvitu
C SRG_logic.fgi(  57, 319):���������� <
      Call REG_RAS_MAN(deltat,.TRUE.,L_adesu,REAL(R_ubesu
     &,4),REAL(R_avasu,4),R_idesu,
     & REAL(R_etasu,4),R_edesu,R_afesu,R_udesu,R_itasu,REAL
     &(R_utasu,4),
     & R_asasu,REAL(R_isasu,4),R_osasu,REAL(R_atasu,4),L_otasu
     &,
     & R_ifesu,R_ofesu,L_odesu,L_esasu,L_usasu,I_abesu,R_ufesu
     &,R_uxasu,
     & R_efesu,R8_oxasu,REAL(100000,4),REAL(R_axitu,4),REAL
     &(R_evasu,4),
     & REAL(R_obesu,4),REAL(R_ibesu,4),REAL(R_ebesu,4))
C SRG_logic.fgi( 152, 497):���������� ���������� �������,20SRG10DF022
      R_(69) = 0.06
C SRG_init.fgi( 125, 279):��������� (RE4) (�������)
      R_(70) = R8_edu * R_(69)
C SRG_init.fgi( 132, 281):����������
      R_apavo = R_(70)
C SRG_init.fgi( 140, 281):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ulavo,R_uravo,REAL(1
     &,4),
     & REAL(R_apavo,4),I_oravo,REAL(R_imavo,4),
     & REAL(R_omavo,4),REAL(R_olavo,4),
     & REAL(R_ilavo,4),REAL(R_ipavo,4),L_opavo,REAL(R_upavo
     &,4),L_aravo,
     & L_eravo,R_umavo,REAL(R_emavo,4),REAL(R_amavo,4),L_iravo
     &)
      !}
C SRG_vlv.fgi(  26, 532):���������� ������� ��� 2,20SRG10CF019XQ01
      R_ifavo = R8_idu
C SRG_init.fgi( 140, 286):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_edavo,R_elavo,REAL(1
     &,4),
     & REAL(R_ifavo,4),I_alavo,REAL(R_udavo,4),
     & REAL(R_afavo,4),REAL(R_adavo,4),
     & REAL(R_ubavo,4),REAL(R_ufavo,4),L_akavo,REAL(R_ekavo
     &,4),L_ikavo,
     & L_okavo,R_efavo,REAL(R_odavo,4),REAL(R_idavo,4),L_ukavo
     &)
      !}
C SRG_vlv.fgi(  55, 532):���������� ������� ��� 2,20SRG10CF020XQ01
      R_iruto = R8_odu
C SRG_init.fgi( 140, 294):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_eputo,R_etuto,REAL(1
     &,4),
     & REAL(R_iruto,4),I_atuto,REAL(R_uputo,4),
     & REAL(R_aruto,4),REAL(R_aputo,4),
     & REAL(R_umuto,4),REAL(R_uruto,4),L_asuto,REAL(R_esuto
     &,4),L_isuto,
     & L_osuto,R_eruto,REAL(R_oputo,4),REAL(R_iputo,4),L_usuto
     &)
      !}
C SRG_vlv.fgi( 169, 532):���������� ������� ��� 2,20SRG10CP020XQ01
      R_udu = R8_afu
C SRG_init.fgi( 140, 290):��������
      R_ovuto = R8_efu
C SRG_init.fgi( 140, 298):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_ebavo,4),REAL
     &(R_ovuto,4),R_otuto,
     & R_ibavo,REAL(1,4),REAL(R_evuto,4),
     & REAL(R_ivuto,4),REAL(R_ixitu,4),
     & REAL(R_ituto,4),I_abavo,REAL(R_uvuto,4),L_axuto,
     & REAL(R_exuto,4),L_ixuto,L_oxuto,R_oxitu,REAL(R_avuto
     &,4),REAL(R_ututo,4),L_uxuto)
      !}
C SRG_vlv.fgi( 197, 532):���������� ������� ��������,20SRG10CP019XQ01
      L_exitu=R_oxitu.gt.R_ixitu
C SRG_logic.fgi( 143, 402):���������� >
      L_(460)=R_oxitu.lt.R_ixitu
C SRG_logic.fgi(  57, 333):���������� <
      Call REG_RAS_MAN(deltat,.TRUE.,L_asesu,REAL(R_uresu
     &,4),REAL(R_amesu,4),R_isesu,
     & REAL(R_elesu,4),R_esesu,R_atesu,R_usesu,R_ilesu,REAL
     &(R_ulesu,4),
     & R_akesu,REAL(R_ikesu,4),R_okesu,REAL(R_alesu,4),L_olesu
     &,
     & R_itesu,R_otesu,L_osesu,L_ekesu,L_ukesu,I_aresu,R_utesu
     &,R_upesu,
     & R_etesu,R8_opesu,REAL(100000,4),REAL(R_oxitu,4),REAL
     &(R_emesu,4),
     & REAL(R_oresu,4),REAL(R_iresu,4),REAL(R_eresu,4))
C SRG_logic.fgi( 176, 514):���������� ���������� �������,20SRG10DF020
      R_(71) = 0.06
C SRG_init.fgi( 125, 314):��������� (RE4) (�������)
      R_(72) = R8_ifu * R_(71)
C SRG_init.fgi( 132, 316):����������
      R_arivo = R_(72)
C SRG_init.fgi( 140, 316):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_umivo,R_usivo,REAL(1
     &,4),
     & REAL(R_arivo,4),I_osivo,REAL(R_ipivo,4),
     & REAL(R_opivo,4),REAL(R_omivo,4),
     & REAL(R_imivo,4),REAL(R_irivo,4),L_orivo,REAL(R_urivo
     &,4),L_asivo,
     & L_esivo,R_upivo,REAL(R_epivo,4),REAL(R_apivo,4),L_isivo
     &)
      !}
C SRG_vlv.fgi(  26, 546):���������� ������� ��� 2,20SRG10CF017XQ01
      R_ikivo = R8_ofu
C SRG_init.fgi( 140, 321):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_efivo,R_emivo,REAL(1
     &,4),
     & REAL(R_ikivo,4),I_amivo,REAL(R_ufivo,4),
     & REAL(R_akivo,4),REAL(R_afivo,4),
     & REAL(R_udivo,4),REAL(R_ukivo,4),L_alivo,REAL(R_elivo
     &,4),L_ilivo,
     & L_olivo,R_ekivo,REAL(R_ofivo,4),REAL(R_ifivo,4),L_ulivo
     &)
      !}
C SRG_vlv.fgi(  55, 546):���������� ������� ��� 2,20SRG10CF018XQ01
      R_isevo = R8_ufu
C SRG_init.fgi( 140, 329):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_erevo,R_evevo,REAL(1
     &,4),
     & REAL(R_isevo,4),I_avevo,REAL(R_urevo,4),
     & REAL(R_asevo,4),REAL(R_arevo,4),
     & REAL(R_upevo,4),REAL(R_usevo,4),L_atevo,REAL(R_etevo
     &,4),L_itevo,
     & L_otevo,R_esevo,REAL(R_orevo,4),REAL(R_irevo,4),L_utevo
     &)
      !}
C SRG_vlv.fgi( 169, 546):���������� ������� ��� 2,20SRG10CP018XQ01
      R_aku = R8_eku
C SRG_init.fgi( 140, 325):��������
      R_oxevo = R8_iku
C SRG_init.fgi( 140, 333):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_edivo,4),REAL
     &(R_oxevo,4),R_ovevo,
     & R_idivo,REAL(1,4),REAL(R_exevo,4),
     & REAL(R_ixevo,4),REAL(R_abotu,4),
     & REAL(R_ivevo,4),I_adivo,REAL(R_uxevo,4),L_abivo,
     & REAL(R_ebivo,4),L_ibivo,L_obivo,R_ebotu,REAL(R_axevo
     &,4),REAL(R_uvevo,4),L_ubivo)
      !}
C SRG_vlv.fgi( 197, 546):���������� ������� ��������,20SRG10CP017XQ01
      L_uxitu=R_ebotu.gt.R_abotu
C SRG_logic.fgi( 143, 408):���������� >
      L_(461)=R_ebotu.lt.R_abotu
C SRG_logic.fgi(  57, 347):���������� <
      Call REG_RAS_MAN(deltat,.TRUE.,L_akisu,REAL(R_ufisu
     &,4),REAL(R_abisu,4),R_ikisu,
     & REAL(R_exesu,4),R_ekisu,R_alisu,R_ukisu,R_ixesu,REAL
     &(R_uxesu,4),
     & R_avesu,REAL(R_ivesu,4),R_ovesu,REAL(R_axesu,4),L_oxesu
     &,
     & R_ilisu,R_olisu,L_okisu,L_evesu,L_uvesu,I_afisu,R_ulisu
     &,R_udisu,
     & R_elisu,R8_odisu,REAL(100000,4),REAL(R_ebotu,4),REAL
     &(R_ebisu,4),
     & REAL(R_ofisu,4),REAL(R_ifisu,4),REAL(R_efisu,4))
C SRG_logic.fgi( 152, 514):���������� ���������� �������,20SRG10DF018
      R_(73) = 0.06
C SRG_init.fgi( 125, 367):��������� (RE4) (�������)
      R_(74) = R8_oku * R_(73)
C SRG_init.fgi( 132, 369):����������
      R_esuvo = R_(74)
C SRG_init.fgi( 140, 369):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_aruvo,R_avuvo,REAL(1
     &,4),
     & REAL(R_esuvo,4),I_utuvo,REAL(R_oruvo,4),
     & REAL(R_uruvo,4),REAL(R_upuvo,4),
     & REAL(R_opuvo,4),REAL(R_osuvo,4),L_usuvo,REAL(R_atuvo
     &,4),L_etuvo,
     & L_ituvo,R_asuvo,REAL(R_iruvo,4),REAL(R_eruvo,4),L_otuvo
     &)
      !}
C SRG_vlv.fgi(  26, 560):���������� ������� ��� 2,20SRG10CF015XQ01
      R_oluvo = R8_uku
C SRG_init.fgi( 140, 374):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ikuvo,R_ipuvo,REAL(1
     &,4),
     & REAL(R_oluvo,4),I_epuvo,REAL(R_aluvo,4),
     & REAL(R_eluvo,4),REAL(R_ekuvo,4),
     & REAL(R_akuvo,4),REAL(R_amuvo,4),L_emuvo,REAL(R_imuvo
     &,4),L_omuvo,
     & L_umuvo,R_iluvo,REAL(R_ukuvo,4),REAL(R_okuvo,4),L_apuvo
     &)
      !}
C SRG_vlv.fgi(  55, 560):���������� ������� ��� 2,20SRG10CF016XQ01
      R_uxuvo = R8_alu
C SRG_init.fgi( 140, 382):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ovuvo,R_odaxo,REAL(1
     &,4),
     & REAL(R_uxuvo,4),I_idaxo,REAL(R_exuvo,4),
     & REAL(R_ixuvo,4),REAL(R_ivuvo,4),
     & REAL(R_evuvo,4),REAL(R_ebaxo,4),L_ibaxo,REAL(R_obaxo
     &,4),L_ubaxo,
     & L_adaxo,R_oxuvo,REAL(R_axuvo,4),REAL(R_uvuvo,4),L_edaxo
     &)
      !}
C SRG_vlv.fgi( 169, 560):���������� ������� ��� 2,20SRG10CP016XQ01
      R_elu = R8_ilu
C SRG_init.fgi( 140, 378):��������
      R_akaxo = R8_olu
C SRG_init.fgi( 140, 386):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_olaxo,4),REAL
     &(R_akaxo,4),R_afaxo,
     & R_ulaxo,REAL(1,4),REAL(R_ofaxo,4),
     & REAL(R_ufaxo,4),REAL(R_obotu,4),
     & REAL(R_udaxo,4),I_ilaxo,REAL(R_ekaxo,4),L_ikaxo,
     & REAL(R_okaxo,4),L_ukaxo,L_alaxo,R_ubotu,REAL(R_ifaxo
     &,4),REAL(R_efaxo,4),L_elaxo)
      !}
C SRG_vlv.fgi( 197, 560):���������� ������� ��������,20SRG10CP015XQ01
      L_ibotu=R_ubotu.gt.R_obotu
C SRG_logic.fgi( 143, 414):���������� >
      L_(462)=R_ubotu.lt.R_obotu
C SRG_logic.fgi(  57, 361):���������� <
      Call REG_RAS_MAN(deltat,.TRUE.,L_avisu,REAL(R_utisu
     &,4),REAL(R_arisu,4),R_ivisu,
     & REAL(R_episu,4),R_evisu,R_axisu,R_uvisu,R_ipisu,REAL
     &(R_upisu,4),
     & R_amisu,REAL(R_imisu,4),R_omisu,REAL(R_apisu,4),L_opisu
     &,
     & R_ixisu,R_oxisu,L_ovisu,L_emisu,L_umisu,I_atisu,R_uxisu
     &,R_usisu,
     & R_exisu,R8_osisu,REAL(100000,4),REAL(R_ubotu,4),REAL
     &(R_erisu,4),
     & REAL(R_otisu,4),REAL(R_itisu,4),REAL(R_etisu,4))
C SRG_logic.fgi( 176, 531):���������� ���������� �������,20SRG10DF016
      R_(75) = 0.06
C SRG_init.fgi(  33, 139):��������� (RE4) (�������)
      R_(76) = R8_ulu * R_(75)
C SRG_init.fgi(  40, 141):����������
      R_elixo = R_(76)
C SRG_init.fgi(  48, 141):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_akixo,R_apixo,REAL(1
     &,4),
     & REAL(R_elixo,4),I_umixo,REAL(R_okixo,4),
     & REAL(R_ukixo,4),REAL(R_ufixo,4),
     & REAL(R_ofixo,4),REAL(R_olixo,4),L_ulixo,REAL(R_amixo
     &,4),L_emixo,
     & L_imixo,R_alixo,REAL(R_ikixo,4),REAL(R_ekixo,4),L_omixo
     &)
      !}
C SRG_vlv.fgi(  26, 574):���������� ������� ��� 2,20SRG10CF013XQ01
      R_obixo = R8_amu
C SRG_init.fgi(  48, 146):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ixexo,R_ifixo,REAL(1
     &,4),
     & REAL(R_obixo,4),I_efixo,REAL(R_abixo,4),
     & REAL(R_ebixo,4),REAL(R_exexo,4),
     & REAL(R_axexo,4),REAL(R_adixo,4),L_edixo,REAL(R_idixo
     &,4),L_odixo,
     & L_udixo,R_ibixo,REAL(R_uxexo,4),REAL(R_oxexo,4),L_afixo
     &)
      !}
C SRG_vlv.fgi(  55, 574):���������� ������� ��� 2,20SRG10CF014XQ01
      R_omexo = R8_emu
C SRG_init.fgi(  48, 154):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ilexo,R_irexo,REAL(1
     &,4),
     & REAL(R_omexo,4),I_erexo,REAL(R_amexo,4),
     & REAL(R_emexo,4),REAL(R_elexo,4),
     & REAL(R_alexo,4),REAL(R_apexo,4),L_epexo,REAL(R_ipexo
     &,4),L_opexo,
     & L_upexo,R_imexo,REAL(R_ulexo,4),REAL(R_olexo,4),L_arexo
     &)
      !}
C SRG_vlv.fgi( 169, 574):���������� ������� ��� 2,20SRG10CP014XQ01
      R_imu = R8_omu
C SRG_init.fgi(  48, 150):��������
      R_usexo = R8_umu
C SRG_init.fgi(  48, 158):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_ivexo,4),REAL
     &(R_usexo,4),R_urexo,
     & R_ovexo,REAL(1,4),REAL(R_isexo,4),
     & REAL(R_osexo,4),REAL(R_edotu,4),
     & REAL(R_orexo,4),I_evexo,REAL(R_atexo,4),L_etexo,
     & REAL(R_itexo,4),L_otexo,L_utexo,R_idotu,REAL(R_esexo
     &,4),REAL(R_asexo,4),L_avexo)
      !}
C SRG_vlv.fgi( 197, 574):���������� ������� ��������,20SRG10CP013XQ01
      L_adotu=R_idotu.gt.R_edotu
C SRG_logic.fgi( 143, 420):���������� >
      L_(463)=R_idotu.lt.R_edotu
C SRG_logic.fgi(  57, 375):���������� <
      Call REG_RAS_MAN(deltat,.TRUE.,L_amosu,REAL(R_ulosu
     &,4),REAL(R_afosu,4),R_imosu,
     & REAL(R_edosu,4),R_emosu,R_aposu,R_umosu,R_idosu,REAL
     &(R_udosu,4),
     & R_abosu,REAL(R_ibosu,4),R_obosu,REAL(R_adosu,4),L_odosu
     &,
     & R_iposu,R_oposu,L_omosu,L_ebosu,L_ubosu,I_alosu,R_uposu
     &,R_ukosu,
     & R_eposu,R8_okosu,REAL(100000,4),REAL(R_idotu,4),REAL
     &(R_efosu,4),
     & REAL(R_olosu,4),REAL(R_ilosu,4),REAL(R_elosu,4))
C SRG_logic.fgi( 152, 531):���������� ���������� �������,20SRG10DF014
      R_(77) = 0.06
C SRG_init.fgi(  33, 174):��������� (RE4) (�������)
      R_(78) = R8_apu * R_(77)
C SRG_init.fgi(  40, 176):����������
      R_obabu = R_(78)
C SRG_init.fgi(  48, 176):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ixuxo,R_ifabu,REAL(1
     &,4),
     & REAL(R_obabu,4),I_efabu,REAL(R_ababu,4),
     & REAL(R_ebabu,4),REAL(R_exuxo,4),
     & REAL(R_axuxo,4),REAL(R_adabu,4),L_edabu,REAL(R_idabu
     &,4),L_odabu,
     & L_udabu,R_ibabu,REAL(R_uxuxo,4),REAL(R_oxuxo,4),L_afabu
     &)
      !}
C SRG_vlv.fgi(  26, 588):���������� ������� ��� 2,20SRG10CF011XQ01
      R_atuxo = R8_epu
C SRG_init.fgi(  48, 181):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_uruxo,R_uvuxo,REAL(1
     &,4),
     & REAL(R_atuxo,4),I_ovuxo,REAL(R_isuxo,4),
     & REAL(R_osuxo,4),REAL(R_oruxo,4),
     & REAL(R_iruxo,4),REAL(R_ituxo,4),L_otuxo,REAL(R_utuxo
     &,4),L_avuxo,
     & L_evuxo,R_usuxo,REAL(R_esuxo,4),REAL(R_asuxo,4),L_ivuxo
     &)
      !}
C SRG_vlv.fgi(  55, 588):���������� ������� ��� 2,20SRG10CF012XQ01
      R_orabu = R8_ipu
C SRG_init.fgi(  48, 189):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ipabu,R_itabu,REAL(1
     &,4),
     & REAL(R_orabu,4),I_etabu,REAL(R_arabu,4),
     & REAL(R_erabu,4),REAL(R_epabu,4),
     & REAL(R_apabu,4),REAL(R_asabu,4),L_esabu,REAL(R_isabu
     &,4),L_osabu,
     & L_usabu,R_irabu,REAL(R_upabu,4),REAL(R_opabu,4),L_atabu
     &)
      !}
C SRG_vlv.fgi( 169, 588):���������� ������� ��� 2,20SRG10CP012XQ01
      R_opu = R8_upu
C SRG_init.fgi(  48, 185):��������
      R_ukabu = R8_aru
C SRG_init.fgi(  48, 193):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_imabu,4),REAL
     &(R_ukabu,4),R_ufabu,
     & R_omabu,REAL(1,4),REAL(R_ikabu,4),
     & REAL(R_okabu,4),REAL(R_udotu,4),
     & REAL(R_ofabu,4),I_emabu,REAL(R_alabu,4),L_elabu,
     & REAL(R_ilabu,4),L_olabu,L_ulabu,R_afotu,REAL(R_ekabu
     &,4),REAL(R_akabu,4),L_amabu)
      !}
C SRG_vlv.fgi( 197, 588):���������� ������� ��������,20SRG10CP011XQ01
      L_odotu=R_afotu.gt.R_udotu
C SRG_logic.fgi( 143, 426):���������� >
      L_(464)=R_afotu.lt.R_udotu
C SRG_logic.fgi(  57, 389):���������� <
      Call REG_RAS_MAN(deltat,.TRUE.,L_abusu,REAL(R_uxosu
     &,4),REAL(R_atosu,4),R_ibusu,
     & REAL(R_esosu,4),R_ebusu,R_adusu,R_ubusu,R_isosu,REAL
     &(R_usosu,4),
     & R_arosu,REAL(R_irosu,4),R_orosu,REAL(R_asosu,4),L_ososu
     &,
     & R_idusu,R_odusu,L_obusu,L_erosu,L_urosu,I_axosu,R_udusu
     &,R_uvosu,
     & R_edusu,R8_ovosu,REAL(100000,4),REAL(R_afotu,4),REAL
     &(R_etosu,4),
     & REAL(R_oxosu,4),REAL(R_ixosu,4),REAL(R_exosu,4))
C SRG_logic.fgi( 176, 549):���������� ���������� �������,20SRG10DF012
      R_(79) = 0.06
C SRG_init.fgi(  33, 209):��������� (RE4) (�������)
      R_(80) = R8_eru * R_(79)
C SRG_init.fgi(  40, 211):����������
      R_amibu = R_(80)
C SRG_init.fgi(  48, 211):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ukibu,R_upibu,REAL(1
     &,4),
     & REAL(R_amibu,4),I_opibu,REAL(R_ilibu,4),
     & REAL(R_olibu,4),REAL(R_okibu,4),
     & REAL(R_ikibu,4),REAL(R_imibu,4),L_omibu,REAL(R_umibu
     &,4),L_apibu,
     & L_epibu,R_ulibu,REAL(R_elibu,4),REAL(R_alibu,4),L_ipibu
     &)
      !}
C SRG_vlv.fgi(  26, 602):���������� ������� ��� 2,20SRG10CF009XQ01
      R_osibu = R8_iru
C SRG_init.fgi(  48, 216):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_iribu,R_ivibu,REAL(1
     &,4),
     & REAL(R_osibu,4),I_evibu,REAL(R_asibu,4),
     & REAL(R_esibu,4),REAL(R_eribu,4),
     & REAL(R_aribu,4),REAL(R_atibu,4),L_etibu,REAL(R_itibu
     &,4),L_otibu,
     & L_utibu,R_isibu,REAL(R_uribu,4),REAL(R_oribu,4),L_avibu
     &)
      !}
C SRG_vlv.fgi(  55, 602):���������� ������� ��� 2,20SRG10CF010XQ01
      R_avebu = R8_oru
C SRG_init.fgi(  48, 224):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_usebu,R_uxebu,REAL(1
     &,4),
     & REAL(R_avebu,4),I_oxebu,REAL(R_itebu,4),
     & REAL(R_otebu,4),REAL(R_osebu,4),
     & REAL(R_isebu,4),REAL(R_ivebu,4),L_ovebu,REAL(R_uvebu
     &,4),L_axebu,
     & L_exebu,R_utebu,REAL(R_etebu,4),REAL(R_atebu,4),L_ixebu
     &)
      !}
C SRG_vlv.fgi( 169, 602):���������� ������� ��� 2,20SRG10CP010XQ01
      R_uru = R8_asu
C SRG_init.fgi(  48, 220):��������
      R_edibu = R8_esu
C SRG_init.fgi(  48, 228):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_ufibu,4),REAL
     &(R_edibu,4),R_ebibu,
     & R_akibu,REAL(1,4),REAL(R_ubibu,4),
     & REAL(R_adibu,4),REAL(R_ifotu,4),
     & REAL(R_abibu,4),I_ofibu,REAL(R_idibu,4),L_odibu,
     & REAL(R_udibu,4),L_afibu,L_efibu,R_ofotu,REAL(R_obibu
     &,4),REAL(R_ibibu,4),L_ifibu)
      !}
C SRG_vlv.fgi( 197, 602):���������� ������� ��������,20SRG10CP009XQ01
      L_efotu=R_ofotu.gt.R_ifotu
C SRG_logic.fgi( 143, 432):���������� >
      L_(465)=R_ofotu.lt.R_ifotu
C SRG_logic.fgi(  57, 403):���������� <
      Call REG_RAS_MAN(deltat,.TRUE.,L_arusu,REAL(R_upusu
     &,4),REAL(R_alusu,4),R_irusu,
     & REAL(R_ekusu,4),R_erusu,R_asusu,R_urusu,R_ikusu,REAL
     &(R_ukusu,4),
     & R_afusu,REAL(R_ifusu,4),R_ofusu,REAL(R_akusu,4),L_okusu
     &,
     & R_isusu,R_osusu,L_orusu,L_efusu,L_ufusu,I_apusu,R_ususu
     &,R_umusu,
     & R_esusu,R8_omusu,REAL(100000,4),REAL(R_ofotu,4),REAL
     &(R_elusu,4),
     & REAL(R_opusu,4),REAL(R_ipusu,4),REAL(R_epusu,4))
C SRG_logic.fgi( 152, 549):���������� ���������� �������,20SRG10DF010
      R_(81) = 0.06
C SRG_init.fgi(  33, 244):��������� (RE4) (�������)
      R_(82) = R8_isu * R_(81)
C SRG_init.fgi(  40, 246):����������
      R_ofubu = R_(82)
C SRG_init.fgi(  48, 246):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_idubu,R_ilubu,REAL(1
     &,4),
     & REAL(R_ofubu,4),I_elubu,REAL(R_afubu,4),
     & REAL(R_efubu,4),REAL(R_edubu,4),
     & REAL(R_adubu,4),REAL(R_akubu,4),L_ekubu,REAL(R_ikubu
     &,4),L_okubu,
     & L_ukubu,R_ifubu,REAL(R_udubu,4),REAL(R_odubu,4),L_alubu
     &)
      !}
C SRG_vlv.fgi(  26, 616):���������� ������� ��� 2,20SRG10CF007XQ01
      R_axobu = R8_osu
C SRG_init.fgi(  48, 251):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_utobu,R_ububu,REAL(1
     &,4),
     & REAL(R_axobu,4),I_obubu,REAL(R_ivobu,4),
     & REAL(R_ovobu,4),REAL(R_otobu,4),
     & REAL(R_itobu,4),REAL(R_ixobu,4),L_oxobu,REAL(R_uxobu
     &,4),L_abubu,
     & L_ebubu,R_uvobu,REAL(R_evobu,4),REAL(R_avobu,4),L_ibubu
     &)
      !}
C SRG_vlv.fgi(  55, 616):���������� ������� ��� 2,20SRG10CF008XQ01
      R_epubu = R8_usu
C SRG_init.fgi(  48, 259):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_amubu,R_asubu,REAL(1
     &,4),
     & REAL(R_epubu,4),I_urubu,REAL(R_omubu,4),
     & REAL(R_umubu,4),REAL(R_ulubu,4),
     & REAL(R_olubu,4),REAL(R_opubu,4),L_upubu,REAL(R_arubu
     &,4),L_erubu,
     & L_irubu,R_apubu,REAL(R_imubu,4),REAL(R_emubu,4),L_orubu
     &)
      !}
C SRG_vlv.fgi( 169, 616):���������� ������� ��� 2,20SRG10CP008XQ01
      R_atu = R8_etu
C SRG_init.fgi(  48, 255):��������
      R_itubu = R8_itu
C SRG_init.fgi(  48, 263):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_axubu,4),REAL
     &(R_itubu,4),R_isubu,
     & R_exubu,REAL(1,4),REAL(R_atubu,4),
     & REAL(R_etubu,4),REAL(R_akotu,4),
     & REAL(R_esubu,4),I_uvubu,REAL(R_otubu,4),L_utubu,
     & REAL(R_avubu,4),L_evubu,L_ivubu,R_ekotu,REAL(R_usubu
     &,4),REAL(R_osubu,4),L_ovubu)
      !}
C SRG_vlv.fgi( 197, 616):���������� ������� ��������,20SRG10CP007XQ01
      L_ufotu=R_ekotu.gt.R_akotu
C SRG_logic.fgi( 143, 438):���������� >
      L_(466)=R_ekotu.lt.R_akotu
C SRG_logic.fgi(  57, 417):���������� <
      Call REG_RAS_MAN(deltat,.TRUE.,L_afatu,REAL(R_udatu
     &,4),REAL(R_axusu,4),R_ifatu,
     & REAL(R_evusu,4),R_efatu,R_akatu,R_ufatu,R_ivusu,REAL
     &(R_uvusu,4),
     & R_atusu,REAL(R_itusu,4),R_otusu,REAL(R_avusu,4),L_ovusu
     &,
     & R_ikatu,R_okatu,L_ofatu,L_etusu,L_utusu,I_adatu,R_ukatu
     &,R_ubatu,
     & R_ekatu,R8_obatu,REAL(100000,4),REAL(R_ekotu,4),REAL
     &(R_exusu,4),
     & REAL(R_odatu,4),REAL(R_idatu,4),REAL(R_edatu,4))
C SRG_logic.fgi( 176, 567):���������� ���������� �������,20SRG10DF008
      R_(83) = 0.06
C SRG_init.fgi(  33, 279):��������� (RE4) (�������)
      R_(84) = R8_otu * R_(83)
C SRG_init.fgi(  40, 281):����������
      R_okedu = R_(84)
C SRG_init.fgi(  48, 281):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ifedu,R_imedu,REAL(1
     &,4),
     & REAL(R_okedu,4),I_emedu,REAL(R_akedu,4),
     & REAL(R_ekedu,4),REAL(R_efedu,4),
     & REAL(R_afedu,4),REAL(R_aledu,4),L_eledu,REAL(R_iledu
     &,4),L_oledu,
     & L_uledu,R_ikedu,REAL(R_ufedu,4),REAL(R_ofedu,4),L_amedu
     &)
      !}
C SRG_vlv.fgi(  26, 630):���������� ������� ��� 2,20SRG10CF005XQ01
      R_abedu = R8_utu
C SRG_init.fgi(  48, 286):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_uvadu,R_udedu,REAL(1
     &,4),
     & REAL(R_abedu,4),I_odedu,REAL(R_ixadu,4),
     & REAL(R_oxadu,4),REAL(R_ovadu,4),
     & REAL(R_ivadu,4),REAL(R_ibedu,4),L_obedu,REAL(R_ubedu
     &,4),L_adedu,
     & L_ededu,R_uxadu,REAL(R_exadu,4),REAL(R_axadu,4),L_idedu
     &)
      !}
C SRG_vlv.fgi(  55, 630):���������� ������� ��� 2,20SRG10CF006XQ01
      R_eredu = R8_avu
C SRG_init.fgi(  48, 294):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_apedu,R_atedu,REAL(1
     &,4),
     & REAL(R_eredu,4),I_usedu,REAL(R_opedu,4),
     & REAL(R_upedu,4),REAL(R_umedu,4),
     & REAL(R_omedu,4),REAL(R_oredu,4),L_uredu,REAL(R_asedu
     &,4),L_esedu,
     & L_isedu,R_aredu,REAL(R_ipedu,4),REAL(R_epedu,4),L_osedu
     &)
      !}
C SRG_vlv.fgi( 169, 630):���������� ������� ��� 2,20SRG10CP006XQ01
      R_evu = R8_ivu
C SRG_init.fgi(  48, 290):��������
      R_afidu = R8_ovu
C SRG_init.fgi(  48, 298):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_okidu,4),REAL
     &(R_afidu,4),R_adidu,
     & R_ukidu,REAL(1,4),REAL(R_odidu,4),
     & REAL(R_udidu,4),REAL(R_okotu,4),
     & REAL(R_ubidu,4),I_ikidu,REAL(R_efidu,4),L_ifidu,
     & REAL(R_ofidu,4),L_ufidu,L_akidu,R_ukotu,REAL(R_ididu
     &,4),REAL(R_edidu,4),L_ekidu)
      !}
C SRG_vlv.fgi( 197, 630):���������� ������� ��������,20SRG10CP005XQ01
      L_ikotu=R_ukotu.gt.R_okotu
C SRG_logic.fgi( 143, 444):���������� >
      L_(467)=R_ukotu.lt.R_okotu
C SRG_logic.fgi(  57, 431):���������� <
      Call REG_RAS_MAN(deltat,.TRUE.,L_atatu,REAL(R_usatu
     &,4),REAL(R_apatu,4),R_itatu,
     & REAL(R_ematu,4),R_etatu,R_avatu,R_utatu,R_imatu,REAL
     &(R_umatu,4),
     & R_alatu,REAL(R_ilatu,4),R_olatu,REAL(R_amatu,4),L_omatu
     &,
     & R_ivatu,R_ovatu,L_otatu,L_elatu,L_ulatu,I_asatu,R_uvatu
     &,R_uratu,
     & R_evatu,R8_oratu,REAL(100000,4),REAL(R_ukotu,4),REAL
     &(R_epatu,4),
     & REAL(R_osatu,4),REAL(R_isatu,4),REAL(R_esatu,4))
C SRG_logic.fgi( 152, 567):���������� ���������� �������,20SRG10DF006
      R_(85) = 0.06
C SRG_init.fgi(  33, 314):��������� (RE4) (�������)
      R_(86) = R8_uvu * R_(85)
C SRG_init.fgi(  40, 316):����������
      R_adodu = R_(86)
C SRG_init.fgi(  48, 316):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_uxidu,R_ufodu,REAL(1
     &,4),
     & REAL(R_adodu,4),I_ofodu,REAL(R_ibodu,4),
     & REAL(R_obodu,4),REAL(R_oxidu,4),
     & REAL(R_ixidu,4),REAL(R_idodu,4),L_ododu,REAL(R_udodu
     &,4),L_afodu,
     & L_efodu,R_ubodu,REAL(R_ebodu,4),REAL(R_abodu,4),L_ifodu
     &)
      !}
C SRG_vlv.fgi(  26, 644):���������� ������� ��� 2,20SRG10CF003XQ01
      R_olodu = R8_axu
C SRG_init.fgi(  48, 321):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ikodu,R_ipodu,REAL(1
     &,4),
     & REAL(R_olodu,4),I_epodu,REAL(R_alodu,4),
     & REAL(R_elodu,4),REAL(R_ekodu,4),
     & REAL(R_akodu,4),REAL(R_amodu,4),L_emodu,REAL(R_imodu
     &,4),L_omodu,
     & L_umodu,R_ilodu,REAL(R_ukodu,4),REAL(R_okodu,4),L_apodu
     &)
      !}
C SRG_vlv.fgi(  55, 644):���������� ������� ��� 2,20SRG10CF004XQ01
      R_esodu = R8_exu
C SRG_init.fgi(  48, 329):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_arodu,R_avodu,REAL(1
     &,4),
     & REAL(R_esodu,4),I_utodu,REAL(R_orodu,4),
     & REAL(R_urodu,4),REAL(R_upodu,4),
     & REAL(R_opodu,4),REAL(R_osodu,4),L_usodu,REAL(R_atodu
     &,4),L_etodu,
     & L_itodu,R_asodu,REAL(R_irodu,4),REAL(R_erodu,4),L_otodu
     &)
      !}
C SRG_vlv.fgi( 169, 644):���������� ������� ��� 2,20SRG10CP004XQ01
      R_ixu = R8_oxu
C SRG_init.fgi(  48, 325):��������
      R_ixodu = R8_uxu
C SRG_init.fgi(  48, 333):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_adudu,4),REAL
     &(R_ixodu,4),R_ivodu,
     & R_edudu,REAL(1,4),REAL(R_axodu,4),
     & REAL(R_exodu,4),REAL(R_elotu,4),
     & REAL(R_evodu,4),I_ubudu,REAL(R_oxodu,4),L_uxodu,
     & REAL(R_abudu,4),L_ebudu,L_ibudu,R_ilotu,REAL(R_uvodu
     &,4),REAL(R_ovodu,4),L_obudu)
      !}
C SRG_vlv.fgi( 197, 644):���������� ������� ��������,20SRG10CP003XQ01
      L_alotu=R_ilotu.gt.R_elotu
C SRG_logic.fgi( 143, 450):���������� >
      L_(468)=R_ilotu.lt.R_elotu
C SRG_logic.fgi(  57, 445):���������� <
      Call REG_RAS_MAN(deltat,.TRUE.,L_aletu,REAL(R_uketu
     &,4),REAL(R_adetu,4),R_iletu,
     & REAL(R_ebetu,4),R_eletu,R_ametu,R_uletu,R_ibetu,REAL
     &(R_ubetu,4),
     & R_axatu,REAL(R_ixatu,4),R_oxatu,REAL(R_abetu,4),L_obetu
     &,
     & R_imetu,R_ometu,L_oletu,L_exatu,L_uxatu,I_aketu,R_umetu
     &,R_ufetu,
     & R_emetu,R8_ofetu,REAL(100000,4),REAL(R_ilotu,4),REAL
     &(R_edetu,4),
     & REAL(R_oketu,4),REAL(R_iketu,4),REAL(R_eketu,4))
C SRG_logic.fgi( 176, 584):���������� ���������� �������,20SRG10DF004
      R_(87) = 0.06
C SRG_init.fgi(  33, 367):��������� (RE4) (�������)
      R_(88) = R8_abad * R_(87)
C SRG_init.fgi(  40, 369):����������
      R_ofapu = R_(88)
C SRG_init.fgi(  48, 369):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_idapu,R_ilapu,REAL(1
     &,4),
     & REAL(R_ofapu,4),I_elapu,REAL(R_afapu,4),
     & REAL(R_efapu,4),REAL(R_edapu,4),
     & REAL(R_adapu,4),REAL(R_akapu,4),L_ekapu,REAL(R_ikapu
     &,4),L_okapu,
     & L_ukapu,R_ifapu,REAL(R_udapu,4),REAL(R_odapu,4),L_alapu
     &)
      !}
C SRG_vlv.fgi(  26, 658):���������� ������� ��� 2,20SRG10CF001XQ01
      R_afafu = R8_ebad
C SRG_init.fgi(  48, 374):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ubafu,R_ukafu,REAL(1
     &,4),
     & REAL(R_afafu,4),I_okafu,REAL(R_idafu,4),
     & REAL(R_odafu,4),REAL(R_obafu,4),
     & REAL(R_ibafu,4),REAL(R_ifafu,4),L_ofafu,REAL(R_ufafu
     &,4),L_akafu,
     & L_ekafu,R_udafu,REAL(R_edafu,4),REAL(R_adafu,4),L_ikafu
     &)
      !}
C SRG_vlv.fgi(  55, 658):���������� ������� ��� 2,20SRG10CF002XQ01
      R_atafu = R8_ibad
C SRG_init.fgi(  48, 382):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_urafu,R_uvafu,REAL(1
     &,4),
     & REAL(R_atafu,4),I_ovafu,REAL(R_isafu,4),
     & REAL(R_osafu,4),REAL(R_orafu,4),
     & REAL(R_irafu,4),REAL(R_itafu,4),L_otafu,REAL(R_utafu
     &,4),L_avafu,
     & L_evafu,R_usafu,REAL(R_esafu,4),REAL(R_asafu,4),L_ivafu
     &)
      !}
C SRG_vlv.fgi( 169, 658):���������� ������� ��� 2,20SRG10CP002XQ01
      R_okumu = R8_obad
C SRG_init.fgi(  48, 378):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ifumu,R_imumu,REAL(1
     &,4),
     & REAL(R_okumu,4),I_emumu,REAL(R_akumu,4),
     & REAL(R_ekumu,4),REAL(R_efumu,4),
     & REAL(R_afumu,4),REAL(R_alumu,4),L_elumu,REAL(R_ilumu
     &,4),L_olumu,
     & L_ulumu,R_ikumu,REAL(R_ufumu,4),REAL(R_ofumu,4),L_amumu
     &)
      !}
C SRG_vlv.fgi( 141, 658):���������� ������� ��� 2,20SRG10CT001XQ01
      R_emafu = R8_ubad
C SRG_init.fgi(  48, 386):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_upafu,4),REAL
     &(R_emafu,4),R_elafu,
     & R_arafu,REAL(1,4),REAL(R_ulafu,4),
     & REAL(R_amafu,4),REAL(R_ulotu,4),
     & REAL(R_alafu,4),I_opafu,REAL(R_imafu,4),L_omafu,
     & REAL(R_umafu,4),L_apafu,L_epafu,R_amotu,REAL(R_olafu
     &,4),REAL(R_ilafu,4),L_ipafu)
      !}
C SRG_vlv.fgi( 197, 658):���������� ������� ��������,20SRG10CP001XQ01
      L_olotu=R_amotu.gt.R_ulotu
C SRG_logic.fgi( 143, 456):���������� >
      L_(469)=R_amotu.lt.R_ulotu
C SRG_logic.fgi(  57, 465):���������� <
      Call REG_RAS_MAN(deltat,.TRUE.,L_axetu,REAL(R_uvetu
     &,4),REAL(R_asetu,4),R_ixetu,
     & REAL(R_eretu,4),R_exetu,R_abitu,R_uxetu,R_iretu,REAL
     &(R_uretu,4),
     & R_apetu,REAL(R_ipetu,4),R_opetu,REAL(R_aretu,4),L_oretu
     &,
     & R_ibitu,R_obitu,L_oxetu,L_epetu,L_upetu,I_avetu,R_ubitu
     &,R_utetu,
     & R_ebitu,R8_otetu,REAL(100000,4),REAL(R_amotu,4),REAL
     &(R_esetu,4),
     & REAL(R_ovetu,4),REAL(R_ivetu,4),REAL(R_evetu,4))
C SRG_logic.fgi( 152, 584):���������� ���������� �������,20SRG10DF002
      R_erumu = R8_adad
C SRG_init.fgi(  48, 390):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_usumu,4),REAL
     &(R_erumu,4),R_apumu,
     & R_atumu,REAL(1,4),REAL(R_opumu,4),
     & REAL(R_upumu,4),REAL(R_umumu,4),
     & REAL(R_omumu,4),I_osumu,REAL(R_irumu,4),L_orumu,
     & REAL(R_urumu,4),L_asumu,L_esumu,R_arumu,REAL(R_ipumu
     &,4),REAL(R_epumu,4),L_isumu)
      !}
C SRG_vlv.fgi( 225, 658):���������� ������� ��������,20SRG10CP035XQ01
      !��������� R_(89) = SRG_vlvC?? /100.0/
      R_(89)=R0_ikif
C SRG_vlv.fgi( 716, 265):���������
      R_okif = R8_irif * R_(89)
C SRG_vlv.fgi( 724, 266):����������
      !��������� R_(90) = SRG_vlvC?? /100.0/
      R_(90)=R0_uvif
C SRG_vlv.fgi( 715, 291):���������
      R_axif = R8_ufof * R_(90)
C SRG_vlv.fgi( 723, 292):����������
      !��������� R_(91) = SRG_vlvC?? /100.0/
      R_(91)=R0_epof
C SRG_vlv.fgi( 715, 317):���������
      R_ipof = R8_evof * R_(91)
C SRG_vlv.fgi( 723, 318):����������
      !��������� R_(92) = SRG_vlvC?? /100.0/
      R_(92)=R0_oduf
C SRG_vlv.fgi( 715, 344):���������
      !��������� R_(93) = SRG_vlvC?? /100.0/
      R_(93)=R0_atuf
C SRG_vlv.fgi( 715, 369):���������
      !��������� R_(94) = SRG_vlvC?? /100.0/
      R_(94)=R0_ilak
C SRG_vlv.fgi( 715, 393):���������
      R_olak = R8_isak * R_(94)
C SRG_vlv.fgi( 723, 394):����������
      !��������� R_(95) = SRG_vlvC?? /100.0/
      R_(95)=R0_uxak
C SRG_vlv.fgi( 664, 241):���������
      !��������� R_(96) = SRG_vlvC?? /100.0/
      R_(96)=R0_erek
C SRG_vlv.fgi( 664, 266):���������
      !��������� R_(97) = SRG_vlvC?? /100.0/
      R_(97)=R0_ofik
C SRG_vlv.fgi( 664, 292):���������
      !��������� R_(98) = SRG_vlvC?? /100.0/
      R_(98)=R0_avik
C SRG_vlv.fgi( 664, 318):���������
      !��������� R_(99) = SRG_vlvC?? /100.0/
      R_(99)=R0_imok
C SRG_vlv.fgi( 664, 345):���������
      !��������� R_(100) = SRG_vlvC?? /100.0/
      R_(100)=R0_ubuk
C SRG_vlv.fgi( 664, 370):���������
      !��������� R_(101) = SRG_vlvC?? /100.0/
      R_(101)=R0_esuk
C SRG_vlv.fgi( 664, 394):���������
      !��������� R_(102) = SRG_vlvC?? /100.0/
      R_(102)=R0_okal
C SRG_vlv.fgi( 616, 242):���������
      !��������� R_(103) = SRG_vlvC?? /100.0/
      R_(103)=R0_axal
C SRG_vlv.fgi( 615, 267):���������
      !��������� R_(104) = SRG_vlvC?? /100.0/
      R_(104)=R0_ipel
C SRG_vlv.fgi( 614, 293):���������
      !��������� R_(105) = SRG_vlvC?? /100.0/
      R_(105)=R0_udil
C SRG_vlv.fgi( 614, 319):���������
      !��������� R_(106) = SRG_vlvC?? /100.0/
      R_(106)=R0_etil
C SRG_vlv.fgi( 614, 346):���������
      !��������� R_(107) = SRG_vlvC?? /100.0/
      R_(107)=R0_olol
C SRG_vlv.fgi( 614, 371):���������
      !��������� R_(108) = SRG_vlvC?? /100.0/
      R_(108)=R0_abul
C SRG_vlv.fgi( 614, 395):���������
      R_ebul = R8_ibul * R_(108)
C SRG_vlv.fgi( 622, 396):����������
      !{
      Call REG_MAN(deltat,REAL(R_ufar,4),R8_ubar,R_ipar,R_omar
     &,
     & L_urar,R_opar,R_umar,L_asar,R_afar,R_udar,
     & R_uvup,REAL(R_exup,4),R_abar,
     & REAL(R_ibar,4),R_ixup,REAL(R_uxup,4),I_ilar,
     & L_obar,L_amar,L_upar,L_ebar,L_oxup,
     & L_edar,L_adar,L_imar,L_axup,L_odar,L_irar,L_ifar,
     & L_ofar,REAL(R8_elomu,8),REAL(1.0,4),R8_aromu,L_avup
     &,L_evup,L_emar,
     & I_ular,L_arar,R_elar,REAL(R_idar,4),L_ivup,L_ovup,L_akar
     &,L_ekar,
     & L_ikar,L_ukar,L_alar,L_okar)
      !}

      if(L_alar.or.L_ukar.or.L_okar.or.L_ikar.or.L_ekar.or.L_akar
     &) then      
                  I_olar = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_olar = z'40000000'
      endif


      R_efar=100*R8_ubar
C SRG_vlv.fgi( 535, 775):���� ���������� ������������ ��������,20SRG10AA223
      !{
      Call REG_MAN(deltat,REAL(R_itir,4),R8_irir,R_ador,R_ebor
     &,
     & L_ifor,R_edor,R_ibor,L_ofor,R_osir,R_isir,
     & R_imir,REAL(R_umir,4),R_opir,
     & REAL(R_arir,4),R_apir,REAL(R_ipir,4),I_axir,
     & L_erir,L_oxir,L_idor,L_upir,L_epir,
     & L_urir,L_orir,L_abor,L_omir,L_esir,L_afor,L_atir,
     & L_etir,REAL(R8_elomu,8),REAL(1.0,4),R8_aromu,L_olir
     &,L_ulir,L_uxir,
     & I_ixir,L_odor,R_uvir,REAL(R_asir,4),L_amir,L_emir,L_otir
     &,L_utir,
     & L_avir,L_ivir,L_ovir,L_evir)
      !}

      if(L_ovir.or.L_ivir.or.L_evir.or.L_avir.or.L_utir.or.L_otir
     &) then      
                  I_exir = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_exir = z'40000000'
      endif


      R_usir=100*R8_irir
C SRG_vlv.fgi( 507, 775):���� ���������� ������������ ��������,20SRG10AA221
      !{
      Call REG_MAN(deltat,REAL(R_ibis,4),R8_ives,R_alis,R_ekis
     &,
     & L_imis,R_elis,R_ikis,L_omis,R_oxes,R_ixes,
     & R_ises,REAL(R_uses,4),R_otes,
     & REAL(R_aves,4),R_ates,REAL(R_ites,4),I_afis,
     & L_eves,L_ofis,L_ilis,L_utes,L_etes,
     & L_uves,L_oves,L_akis,L_oses,L_exes,L_amis,L_abis,
     & L_ebis,REAL(R8_elomu,8),REAL(1.0,4),R8_aromu,L_ores
     &,L_ures,L_ufis,
     & I_ifis,L_olis,R_udis,REAL(R_axes,4),L_ases,L_eses,L_obis
     &,L_ubis,
     & L_adis,L_idis,L_odis,L_edis)
      !}

      if(L_odis.or.L_idis.or.L_edis.or.L_adis.or.L_ubis.or.L_obis
     &) then      
                  I_efis = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_efis = z'40000000'
      endif


      R_uxes=100*R8_ives
C SRG_vlv.fgi( 479, 775):���� ���������� ������������ ��������,20SRG10AA219
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_osame,R_oxame,REAL(1
     &,4),
     & REAL(R_utame,4),I_ixame,REAL(R_etame,4),
     & REAL(R_itame,4),REAL(R_isame,4),
     & REAL(R_esame,4),REAL(R_evame,4),L_ivame,REAL(R_ovame
     &,4),L_uvame,
     & L_axame,R_otame,REAL(R_atame,4),REAL(R_usame,4),L_exame
     &)
      !}
C SRG_vlv.fgi( 526, 237):���������� ������� ��� 2,20SRG10GT004XQ02
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ebeme,R_ekeme,REAL(1
     &,4),
     & REAL(R_ideme,4),I_akeme,REAL(R_ubeme,4),
     & REAL(R_ademe,4),REAL(R_abeme,4),
     & REAL(R_uxame,4),REAL(R_udeme,4),L_afeme,REAL(R_efeme
     &,4),L_ifeme,
     & L_ofeme,R_edeme,REAL(R_obeme,4),REAL(R_ibeme,4),L_ufeme
     &)
      !}
C SRG_vlv.fgi( 526, 250):���������� ������� ��� 2,20SRG10GT004XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ukeme,R_upeme,REAL(1
     &,4),
     & REAL(R_ameme,4),I_opeme,REAL(R_ileme,4),
     & REAL(R_oleme,4),REAL(R_okeme,4),
     & REAL(R_ikeme,4),REAL(R_imeme,4),L_omeme,REAL(R_umeme
     &,4),L_apeme,
     & L_epeme,R_uleme,REAL(R_eleme,4),REAL(R_aleme,4),L_ipeme
     &)
      !}
C SRG_vlv.fgi( 526, 263):���������� ������� ��� 2,20SRG10GT003XQ02
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ireme,R_iveme,REAL(1
     &,4),
     & REAL(R_oseme,4),I_eveme,REAL(R_aseme,4),
     & REAL(R_eseme,4),REAL(R_ereme,4),
     & REAL(R_areme,4),REAL(R_ateme,4),L_eteme,REAL(R_iteme
     &,4),L_oteme,
     & L_uteme,R_iseme,REAL(R_ureme,4),REAL(R_oreme,4),L_aveme
     &)
      !}
C SRG_vlv.fgi( 526, 276):���������� ������� ��� 2,20SRG10GT003XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_otipi,R_obopi,REAL(1
     &,4),
     & REAL(R_uvipi,4),I_ibopi,REAL(R_evipi,4),
     & REAL(R_ivipi,4),REAL(R_itipi,4),
     & REAL(R_etipi,4),REAL(R_exipi,4),L_ixipi,REAL(R_oxipi
     &,4),L_uxipi,
     & L_abopi,R_ovipi,REAL(R_avipi,4),REAL(R_utipi,4),L_ebopi
     &)
      !}
C SRG_vlv.fgi( 676, 504):���������� ������� ��� 2,20SRG10GT006XQ02
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_edopi,R_elopi,REAL(1
     &,4),
     & REAL(R_ifopi,4),I_alopi,REAL(R_udopi,4),
     & REAL(R_afopi,4),REAL(R_adopi,4),
     & REAL(R_ubopi,4),REAL(R_ufopi,4),L_akopi,REAL(R_ekopi
     &,4),L_ikopi,
     & L_okopi,R_efopi,REAL(R_odopi,4),REAL(R_idopi,4),L_ukopi
     &)
      !}
C SRG_vlv.fgi( 676, 517):���������� ������� ��� 2,20SRG10GT006XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ulopi,R_uropi,REAL(1
     &,4),
     & REAL(R_apopi,4),I_oropi,REAL(R_imopi,4),
     & REAL(R_omopi,4),REAL(R_olopi,4),
     & REAL(R_ilopi,4),REAL(R_ipopi,4),L_opopi,REAL(R_upopi
     &,4),L_aropi,
     & L_eropi,R_umopi,REAL(R_emopi,4),REAL(R_amopi,4),L_iropi
     &)
      !}
C SRG_vlv.fgi( 676, 530):���������� ������� ��� 2,20SRG10GT005XQ02
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_isopi,R_ixopi,REAL(1
     &,4),
     & REAL(R_otopi,4),I_exopi,REAL(R_atopi,4),
     & REAL(R_etopi,4),REAL(R_esopi,4),
     & REAL(R_asopi,4),REAL(R_avopi,4),L_evopi,REAL(R_ivopi
     &,4),L_ovopi,
     & L_uvopi,R_itopi,REAL(R_usopi,4),REAL(R_osopi,4),L_axopi
     &)
      !}
C SRG_vlv.fgi( 676, 543):���������� ������� ��� 2,20SRG10GT005XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ixapo,R_ifepo,REAL(1
     &,4),
     & REAL(R_obepo,4),I_efepo,REAL(R_abepo,4),
     & REAL(R_ebepo,4),REAL(R_exapo,4),
     & REAL(R_axapo,4),REAL(R_adepo,4),L_edepo,REAL(R_idepo
     &,4),L_odepo,
     & L_udepo,R_ibepo,REAL(R_uxapo,4),REAL(R_oxapo,4),L_afepo
     &)
      !}
C SRG_vlv.fgi(  84, 644):���������� ������� ��� 2,20SRG10CT021XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_akupo,R_apupo,REAL(1
     &,4),
     & REAL(R_elupo,4),I_umupo,REAL(R_okupo,4),
     & REAL(R_ukupo,4),REAL(R_ufupo,4),
     & REAL(R_ofupo,4),REAL(R_olupo,4),L_ulupo,REAL(R_amupo
     &,4),L_emupo,
     & L_imupo,R_alupo,REAL(R_ikupo,4),REAL(R_ekupo,4),L_omupo
     &)
      !}
C SRG_vlv.fgi( 309, 546):���������� ������� ��� 2,20SRG10GC410XQ06
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_opupo,R_otupo,REAL(1
     &,4),
     & REAL(R_urupo,4),I_itupo,REAL(R_erupo,4),
     & REAL(R_irupo,4),REAL(R_ipupo,4),
     & REAL(R_epupo,4),REAL(R_esupo,4),L_isupo,REAL(R_osupo
     &,4),L_usupo,
     & L_atupo,R_orupo,REAL(R_arupo,4),REAL(R_upupo,4),L_etupo
     &)
      !}
C SRG_vlv.fgi( 309, 560):���������� ������� ��� 2,20SRG10GC410XQ05
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_evupo,R_edaro,REAL(1
     &,4),
     & REAL(R_ixupo,4),I_adaro,REAL(R_uvupo,4),
     & REAL(R_axupo,4),REAL(R_avupo,4),
     & REAL(R_utupo,4),REAL(R_uxupo,4),L_abaro,REAL(R_ebaro
     &,4),L_ibaro,
     & L_obaro,R_exupo,REAL(R_ovupo,4),REAL(R_ivupo,4),L_ubaro
     &)
      !}
C SRG_vlv.fgi( 309, 574):���������� ������� ��� 2,20SRG10GC410XQ04
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_obero,R_okero,REAL(1
     &,4),
     & REAL(R_udero,4),I_ikero,REAL(R_edero,4),
     & REAL(R_idero,4),REAL(R_ibero,4),
     & REAL(R_ebero,4),REAL(R_efero,4),L_ifero,REAL(R_ofero
     &,4),L_ufero,
     & L_akero,R_odero,REAL(R_adero,4),REAL(R_ubero,4),L_ekero
     &)
      !}
C SRG_vlv.fgi( 309, 588):���������� ������� ��� 2,20SRG10GC409XQ06
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_elero,R_erero,REAL(1
     &,4),
     & REAL(R_imero,4),I_arero,REAL(R_ulero,4),
     & REAL(R_amero,4),REAL(R_alero,4),
     & REAL(R_ukero,4),REAL(R_umero,4),L_apero,REAL(R_epero
     &,4),L_ipero,
     & L_opero,R_emero,REAL(R_olero,4),REAL(R_ilero,4),L_upero
     &)
      !}
C SRG_vlv.fgi( 309, 602):���������� ������� ��� 2,20SRG10GC409XQ05
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_urero,R_uvero,REAL(1
     &,4),
     & REAL(R_atero,4),I_overo,REAL(R_isero,4),
     & REAL(R_osero,4),REAL(R_orero,4),
     & REAL(R_irero,4),REAL(R_itero,4),L_otero,REAL(R_utero
     &,4),L_avero,
     & L_evero,R_usero,REAL(R_esero,4),REAL(R_asero,4),L_ivero
     &)
      !}
C SRG_vlv.fgi( 309, 616):���������� ������� ��� 2,20SRG10GC409XQ04
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_eviro,R_edoro,REAL(1
     &,4),
     & REAL(R_ixiro,4),I_adoro,REAL(R_uviro,4),
     & REAL(R_axiro,4),REAL(R_aviro,4),
     & REAL(R_utiro,4),REAL(R_uxiro,4),L_aboro,REAL(R_eboro
     &,4),L_iboro,
     & L_oboro,R_exiro,REAL(R_oviro,4),REAL(R_iviro,4),L_uboro
     &)
      !}
C SRG_vlv.fgi( 309, 630):���������� ������� ��� 2,20SRG10GC408XQ06
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_udoro,R_uloro,REAL(1
     &,4),
     & REAL(R_akoro,4),I_oloro,REAL(R_iforo,4),
     & REAL(R_oforo,4),REAL(R_odoro,4),
     & REAL(R_idoro,4),REAL(R_ikoro,4),L_okoro,REAL(R_ukoro
     &,4),L_aloro,
     & L_eloro,R_uforo,REAL(R_eforo,4),REAL(R_aforo,4),L_iloro
     &)
      !}
C SRG_vlv.fgi( 309, 644):���������� ������� ��� 2,20SRG10GC408XQ05
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_imoro,R_isoro,REAL(1
     &,4),
     & REAL(R_oporo,4),I_esoro,REAL(R_aporo,4),
     & REAL(R_eporo,4),REAL(R_emoro,4),
     & REAL(R_amoro,4),REAL(R_aroro,4),L_eroro,REAL(R_iroro
     &,4),L_ororo,
     & L_uroro,R_iporo,REAL(R_umoro,4),REAL(R_omoro,4),L_asoro
     &)
      !}
C SRG_vlv.fgi( 309, 658):���������� ������� ��� 2,20SRG10GC408XQ04
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_udeso,R_uleso,REAL(1
     &,4),
     & REAL(R_akeso,4),I_oleso,REAL(R_ifeso,4),
     & REAL(R_ofeso,4),REAL(R_odeso,4),
     & REAL(R_ideso,4),REAL(R_ikeso,4),L_okeso,REAL(R_ukeso
     &,4),L_aleso,
     & L_eleso,R_ufeso,REAL(R_efeso,4),REAL(R_afeso,4),L_ileso
     &)
      !}
C SRG_vlv.fgi( 112, 280):���������� ������� ��� 2,20SRG10CM014XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_imeso,R_iseso,REAL(1
     &,4),
     & REAL(R_opeso,4),I_eseso,REAL(R_apeso,4),
     & REAL(R_epeso,4),REAL(R_emeso,4),
     & REAL(R_ameso,4),REAL(R_areso,4),L_ereso,REAL(R_ireso
     &,4),L_oreso,
     & L_ureso,R_ipeso,REAL(R_umeso,4),REAL(R_omeso,4),L_aseso
     &)
      !}
C SRG_vlv.fgi( 112, 294):���������� ������� ��� 2,20SRG10CQ014XQ01
      Call REG_RAS_MAN(deltat,.TRUE.,L_emek,REAL(R_amek,4
     &),REAL(R_efek,4),R_omek,
     & REAL(R_idek,4),R_imek,R_epek,R_apek,R_odek,REAL(R_afek
     &,4),
     & R_ebek,REAL(R_obek,4),R_ubek,REAL(R_edek,4),L_udek
     &,
     & R_opek,R_upek,L_umek,L_ibek,L_adek,I_elek,R_arek,R_alek
     &,
     & R_ipek,R8_ukek,REAL(100000,4),REAL(R_ipeso,4),REAL
     &(R_ifek,4),
     & REAL(R_ulek,4),REAL(R_olek,4),REAL(R_ilek,4))
C SRG_vlv.fgi( 672, 253):���������� ���������� �������,20SRG10AA214
      R_abek = R8_ukek * R_(95)
C SRG_vlv.fgi( 672, 242):����������
      Call REG_RAS_MAN(deltat,.TRUE.,L_aruf,REAL(R_upuf,4
     &),REAL(R_aluf,4),R_iruf,
     & REAL(R_ekuf,4),R_eruf,R_asuf,R_uruf,R_ikuf,REAL(R_ukuf
     &,4),
     & R_afuf,REAL(R_ifuf,4),R_ofuf,REAL(R_akuf,4),L_okuf
     &,
     & R_isuf,R_osuf,L_oruf,L_efuf,L_ufuf,I_apuf,R_usuf,R_umuf
     &,
     & R_esuf,R8_omuf,REAL(100000,4),REAL(R_ipeso,4),REAL
     &(R_eluf,4),
     & REAL(R_opuf,4),REAL(R_ipuf,4),REAL(R_epuf,4))
C SRG_vlv.fgi( 723, 356):���������� ���������� �������,20SRG10AA217
      R_uduf = R8_omuf * R_(92)
C SRG_vlv.fgi( 723, 345):����������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_oposo,R_otoso,REAL(1
     &,4),
     & REAL(R_uroso,4),I_itoso,REAL(R_eroso,4),
     & REAL(R_iroso,4),REAL(R_iposo,4),
     & REAL(R_eposo,4),REAL(R_esoso,4),L_isoso,REAL(R_ososo
     &,4),L_usoso,
     & L_atoso,R_oroso,REAL(R_aroso,4),REAL(R_uposo,4),L_etoso
     &)
      !}
C SRG_vlv.fgi( 112, 308):���������� ������� ��� 2,20SRG10CM013XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_evoso,R_eduso,REAL(1
     &,4),
     & REAL(R_ixoso,4),I_aduso,REAL(R_uvoso,4),
     & REAL(R_axoso,4),REAL(R_avoso,4),
     & REAL(R_utoso,4),REAL(R_uxoso,4),L_abuso,REAL(R_ebuso
     &,4),L_ibuso,
     & L_obuso,R_exoso,REAL(R_ovoso,4),REAL(R_ivoso,4),L_ubuso
     &)
      !}
C SRG_vlv.fgi( 112, 322):���������� ������� ��� 2,20SRG10CQ013XQ01
      Call REG_RAS_MAN(deltat,.TRUE.,L_ifak,REAL(R_efak,4
     &),REAL(R_ixuf,4),R_ufak,
     & REAL(R_ovuf,4),R_ofak,R_ikak,R_ekak,R_uvuf,REAL(R_exuf
     &,4),
     & R_ituf,REAL(R_utuf,4),R_avuf,REAL(R_ivuf,4),L_axuf
     &,
     & R_ukak,R_alak,L_akak,L_otuf,L_evuf,I_idak,R_elak,R_edak
     &,
     & R_okak,R8_adak,REAL(100000,4),REAL(R_exoso,4),REAL
     &(R_oxuf,4),
     & REAL(R_afak,4),REAL(R_udak,4),REAL(R_odak,4))
C SRG_vlv.fgi( 723, 381):���������� ���������� �������,20SRG10AA216
      R_etuf = R8_adak * R_(93)
C SRG_vlv.fgi( 723, 370):����������
      Call REG_RAS_MAN(deltat,.TRUE.,L_obik,REAL(R_ibik,4
     &),REAL(R_otek,4),R_adik,
     & REAL(R_usek,4),R_ubik,R_odik,R_idik,R_atek,REAL(R_itek
     &,4),
     & R_orek,REAL(R_asek,4),R_esek,REAL(R_osek,4),L_etek
     &,
     & R_afik,R_efik,L_edik,L_urek,L_isek,I_oxek,R_ifik,R_ixek
     &,
     & R_udik,R8_exek,REAL(100000,4),REAL(R_exoso,4),REAL
     &(R_utek,4),
     & REAL(R_ebik,4),REAL(R_abik,4),REAL(R_uxek,4))
C SRG_vlv.fgi( 672, 278):���������� ���������� �������,20SRG10AA213
      R_irek = R8_exek * R_(96)
C SRG_vlv.fgi( 672, 267):����������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_orato,R_ovato,REAL(1
     &,4),
     & REAL(R_usato,4),I_ivato,REAL(R_esato,4),
     & REAL(R_isato,4),REAL(R_irato,4),
     & REAL(R_erato,4),REAL(R_etato,4),L_itato,REAL(R_otato
     &,4),L_utato,
     & L_avato,R_osato,REAL(R_asato,4),REAL(R_urato,4),L_evato
     &)
      !}
C SRG_vlv.fgi( 112, 336):���������� ������� ��� 2,20SRG10CM012XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_exato,R_efeto,REAL(1
     &,4),
     & REAL(R_ibeto,4),I_afeto,REAL(R_uxato,4),
     & REAL(R_abeto,4),REAL(R_axato,4),
     & REAL(R_uvato,4),REAL(R_ubeto,4),L_adeto,REAL(R_edeto
     &,4),L_ideto,
     & L_odeto,R_ebeto,REAL(R_oxato,4),REAL(R_ixato,4),L_udeto
     &)
      !}
C SRG_vlv.fgi( 112, 350):���������� ������� ��� 2,20SRG10CQ012XQ01
      Call REG_RAS_MAN(deltat,.TRUE.,L_asik,REAL(R_urik,4
     &),REAL(R_amik,4),R_isik,
     & REAL(R_elik,4),R_esik,R_atik,R_usik,R_ilik,REAL(R_ulik
     &,4),
     & R_akik,REAL(R_ikik,4),R_okik,REAL(R_alik,4),L_olik
     &,
     & R_itik,R_otik,L_osik,L_ekik,L_ukik,I_arik,R_utik,R_upik
     &,
     & R_etik,R8_opik,REAL(100000,4),REAL(R_ebeto,4),REAL
     &(R_emik,4),
     & REAL(R_orik,4),REAL(R_irik,4),REAL(R_erik,4))
C SRG_vlv.fgi( 672, 304):���������� ���������� �������,20SRG10AA212
      R_ufik = R8_opik * R_(97)
C SRG_vlv.fgi( 672, 293):����������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ufeto,R_umeto,REAL(1
     &,4),
     & REAL(R_aleto,4),I_ometo,REAL(R_iketo,4),
     & REAL(R_oketo,4),REAL(R_ofeto,4),
     & REAL(R_ifeto,4),REAL(R_ileto,4),L_oleto,REAL(R_uleto
     &,4),L_ameto,
     & L_emeto,R_uketo,REAL(R_eketo,4),REAL(R_aketo,4),L_imeto
     &)
      !}
C SRG_vlv.fgi( 141, 504):���������� ������� ��� 2,20SRG10CT012XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ipeto,R_iteto,REAL(1
     &,4),
     & REAL(R_oreto,4),I_eteto,REAL(R_areto,4),
     & REAL(R_ereto,4),REAL(R_epeto,4),
     & REAL(R_apeto,4),REAL(R_aseto,4),L_eseto,REAL(R_iseto
     &,4),L_oseto,
     & L_useto,R_ireto,REAL(R_upeto,4),REAL(R_opeto,4),L_ateto
     &)
      !}
C SRG_vlv.fgi( 253, 546):���������� ������� ��� 2,20SRG10CW009XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_aveto,R_adito,REAL(1
     &,4),
     & REAL(R_exeto,4),I_ubito,REAL(R_oveto,4),
     & REAL(R_uveto,4),REAL(R_uteto,4),
     & REAL(R_oteto,4),REAL(R_oxeto,4),L_uxeto,REAL(R_abito
     &,4),L_ebito,
     & L_ibito,R_axeto,REAL(R_iveto,4),REAL(R_eveto,4),L_obito
     &)
      !}
C SRG_vlv.fgi( 253, 560):���������� ������� ��� 2,20SRG10CW008XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_odito,R_olito,REAL(1
     &,4),
     & REAL(R_ufito,4),I_ilito,REAL(R_efito,4),
     & REAL(R_ifito,4),REAL(R_idito,4),
     & REAL(R_edito,4),REAL(R_ekito,4),L_ikito,REAL(R_okito
     &,4),L_ukito,
     & L_alito,R_ofito,REAL(R_afito,4),REAL(R_udito,4),L_elito
     &)
      !}
C SRG_vlv.fgi( 253, 574):���������� ������� ��� 2,20SRG10CW007XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_iroto,R_ivoto,REAL(1
     &,4),
     & REAL(R_osoto,4),I_evoto,REAL(R_asoto,4),
     & REAL(R_esoto,4),REAL(R_eroto,4),
     & REAL(R_aroto,4),REAL(R_atoto,4),L_etoto,REAL(R_itoto
     &,4),L_ototo,
     & L_utoto,R_isoto,REAL(R_uroto,4),REAL(R_oroto,4),L_avoto
     &)
      !}
C SRG_vlv.fgi( 112, 364):���������� ������� ��� 2,20SRG10CM011XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_axoto,R_afuto,REAL(1
     &,4),
     & REAL(R_ebuto,4),I_uduto,REAL(R_oxoto,4),
     & REAL(R_uxoto,4),REAL(R_uvoto,4),
     & REAL(R_ovoto,4),REAL(R_obuto,4),L_ubuto,REAL(R_aduto
     &,4),L_eduto,
     & L_iduto,R_abuto,REAL(R_ixoto,4),REAL(R_exoto,4),L_oduto
     &)
      !}
C SRG_vlv.fgi( 112, 378):���������� ������� ��� 2,20SRG10CQ011XQ01
      Call REG_RAS_MAN(deltat,.TRUE.,L_ikok,REAL(R_ekok,4
     &),REAL(R_ibok,4),R_ukok,
     & REAL(R_oxik,4),R_okok,R_ilok,R_elok,R_uxik,REAL(R_ebok
     &,4),
     & R_ivik,REAL(R_uvik,4),R_axik,REAL(R_ixik,4),L_abok
     &,
     & R_ulok,R_amok,L_alok,L_ovik,L_exik,I_ifok,R_emok,R_efok
     &,
     & R_olok,R8_afok,REAL(100000,4),REAL(R_abuto,4),REAL
     &(R_obok,4),
     & REAL(R_akok,4),REAL(R_ufok,4),REAL(R_ofok,4))
C SRG_vlv.fgi( 672, 330):���������� ���������� �������,20SRG10AA211
      R_evik = R8_afok * R_(98)
C SRG_vlv.fgi( 672, 319):����������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ofuto,R_omuto,REAL(1
     &,4),
     & REAL(R_ukuto,4),I_imuto,REAL(R_ekuto,4),
     & REAL(R_ikuto,4),REAL(R_ifuto,4),
     & REAL(R_efuto,4),REAL(R_eluto,4),L_iluto,REAL(R_oluto
     &,4),L_uluto,
     & L_amuto,R_okuto,REAL(R_akuto,4),REAL(R_ufuto,4),L_emuto
     &)
      !}
C SRG_vlv.fgi( 141, 518):���������� ������� ��� 2,20SRG10CT011XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_isavo,R_ixavo,REAL(1
     &,4),
     & REAL(R_otavo,4),I_exavo,REAL(R_atavo,4),
     & REAL(R_etavo,4),REAL(R_esavo,4),
     & REAL(R_asavo,4),REAL(R_avavo,4),L_evavo,REAL(R_ivavo
     &,4),L_ovavo,
     & L_uvavo,R_itavo,REAL(R_usavo,4),REAL(R_osavo,4),L_axavo
     &)
      !}
C SRG_vlv.fgi( 112, 392):���������� ������� ��� 2,20SRG10CM010XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_abevo,R_akevo,REAL(1
     &,4),
     & REAL(R_edevo,4),I_ufevo,REAL(R_obevo,4),
     & REAL(R_ubevo,4),REAL(R_uxavo,4),
     & REAL(R_oxavo,4),REAL(R_odevo,4),L_udevo,REAL(R_afevo
     &,4),L_efevo,
     & L_ifevo,R_adevo,REAL(R_ibevo,4),REAL(R_ebevo,4),L_ofevo
     &)
      !}
C SRG_vlv.fgi( 112, 406):���������� ������� ��� 2,20SRG10CQ010XQ01
      Call REG_RAS_MAN(deltat,.TRUE.,L_uvok,REAL(R_ovok,4
     &),REAL(R_urok,4),R_exok,
     & REAL(R_arok,4),R_axok,R_uxok,R_oxok,R_erok,REAL(R_orok
     &,4),
     & R_umok,REAL(R_epok,4),R_ipok,REAL(R_upok,4),L_irok
     &,
     & R_ebuk,R_ibuk,L_ixok,L_apok,L_opok,I_utok,R_obuk,R_otok
     &,
     & R_abuk,R8_itok,REAL(100000,4),REAL(R_adevo,4),REAL
     &(R_asok,4),
     & REAL(R_ivok,4),REAL(R_evok,4),REAL(R_avok,4))
C SRG_vlv.fgi( 672, 357):���������� ���������� �������,20SRG10AA210
      R_omok = R8_itok * R_(99)
C SRG_vlv.fgi( 672, 346):����������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_okevo,R_opevo,REAL(1
     &,4),
     & REAL(R_ulevo,4),I_ipevo,REAL(R_elevo,4),
     & REAL(R_ilevo,4),REAL(R_ikevo,4),
     & REAL(R_ekevo,4),REAL(R_emevo,4),L_imevo,REAL(R_omevo
     &,4),L_umevo,
     & L_apevo,R_olevo,REAL(R_alevo,4),REAL(R_ukevo,4),L_epevo
     &)
      !}
C SRG_vlv.fgi( 141, 532):���������� ������� ��� 2,20SRG10CT010XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_itivo,R_ibovo,REAL(1
     &,4),
     & REAL(R_ovivo,4),I_ebovo,REAL(R_avivo,4),
     & REAL(R_evivo,4),REAL(R_etivo,4),
     & REAL(R_ativo,4),REAL(R_axivo,4),L_exivo,REAL(R_ixivo
     &,4),L_oxivo,
     & L_uxivo,R_ivivo,REAL(R_utivo,4),REAL(R_otivo,4),L_abovo
     &)
      !}
C SRG_vlv.fgi( 253, 588):���������� ������� ��� 2,20SRG10CW006XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_adovo,R_alovo,REAL(1
     &,4),
     & REAL(R_efovo,4),I_ukovo,REAL(R_odovo,4),
     & REAL(R_udovo,4),REAL(R_ubovo,4),
     & REAL(R_obovo,4),REAL(R_ofovo,4),L_ufovo,REAL(R_akovo
     &,4),L_ekovo,
     & L_ikovo,R_afovo,REAL(R_idovo,4),REAL(R_edovo,4),L_okovo
     &)
      !}
C SRG_vlv.fgi( 253, 602):���������� ������� ��� 2,20SRG10CW005XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_olovo,R_orovo,REAL(1
     &,4),
     & REAL(R_umovo,4),I_irovo,REAL(R_emovo,4),
     & REAL(R_imovo,4),REAL(R_ilovo,4),
     & REAL(R_elovo,4),REAL(R_epovo,4),L_ipovo,REAL(R_opovo
     &,4),L_upovo,
     & L_arovo,R_omovo,REAL(R_amovo,4),REAL(R_ulovo,4),L_erovo
     &)
      !}
C SRG_vlv.fgi( 112, 420):���������� ������� ��� 2,20SRG10CM009XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_esovo,R_exovo,REAL(1
     &,4),
     & REAL(R_itovo,4),I_axovo,REAL(R_usovo,4),
     & REAL(R_atovo,4),REAL(R_asovo,4),
     & REAL(R_urovo,4),REAL(R_utovo,4),L_avovo,REAL(R_evovo
     &,4),L_ivovo,
     & L_ovovo,R_etovo,REAL(R_osovo,4),REAL(R_isovo,4),L_uvovo
     &)
      !}
C SRG_vlv.fgi( 112, 434):���������� ������� ��� 2,20SRG10CQ009XQ01
      Call REG_RAS_MAN(deltat,.TRUE.,L_epuk,REAL(R_apuk,4
     &),REAL(R_ekuk,4),R_opuk,
     & REAL(R_ifuk,4),R_ipuk,R_eruk,R_aruk,R_ofuk,REAL(R_akuk
     &,4),
     & R_eduk,REAL(R_oduk,4),R_uduk,REAL(R_efuk,4),L_ufuk
     &,
     & R_oruk,R_uruk,L_upuk,L_iduk,L_afuk,I_emuk,R_asuk,R_amuk
     &,
     & R_iruk,R8_uluk,REAL(100000,4),REAL(R_etovo,4),REAL
     &(R_ikuk,4),
     & REAL(R_umuk,4),REAL(R_omuk,4),REAL(R_imuk,4))
C SRG_vlv.fgi( 672, 382):���������� ���������� �������,20SRG10AA209
      R_aduk = R8_uluk * R_(100)
C SRG_vlv.fgi( 672, 371):����������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_uxovo,R_ufuvo,REAL(1
     &,4),
     & REAL(R_aduvo,4),I_ofuvo,REAL(R_ibuvo,4),
     & REAL(R_obuvo,4),REAL(R_oxovo,4),
     & REAL(R_ixovo,4),REAL(R_iduvo,4),L_oduvo,REAL(R_uduvo
     &,4),L_afuvo,
     & L_efuvo,R_ubuvo,REAL(R_ebuvo,4),REAL(R_abuvo,4),L_ifuvo
     &)
      !}
C SRG_vlv.fgi( 141, 546):���������� ������� ��� 2,20SRG10CT009XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_omaxo,R_osaxo,REAL(1
     &,4),
     & REAL(R_upaxo,4),I_isaxo,REAL(R_epaxo,4),
     & REAL(R_ipaxo,4),REAL(R_imaxo,4),
     & REAL(R_emaxo,4),REAL(R_eraxo,4),L_iraxo,REAL(R_oraxo
     &,4),L_uraxo,
     & L_asaxo,R_opaxo,REAL(R_apaxo,4),REAL(R_umaxo,4),L_esaxo
     &)
      !}
C SRG_vlv.fgi( 112, 448):���������� ������� ��� 2,20SRG10CM008XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_etaxo,R_ebexo,REAL(1
     &,4),
     & REAL(R_ivaxo,4),I_abexo,REAL(R_utaxo,4),
     & REAL(R_avaxo,4),REAL(R_ataxo,4),
     & REAL(R_usaxo,4),REAL(R_uvaxo,4),L_axaxo,REAL(R_exaxo
     &,4),L_ixaxo,
     & L_oxaxo,R_evaxo,REAL(R_otaxo,4),REAL(R_itaxo,4),L_uxaxo
     &)
      !}
C SRG_vlv.fgi( 112, 462):���������� ������� ��� 2,20SRG10CQ008XQ01
      Call REG_RAS_MAN(deltat,.TRUE.,L_odal,REAL(R_idal,4
     &),REAL(R_ovuk,4),R_afal,
     & REAL(R_utuk,4),R_udal,R_ofal,R_ifal,R_avuk,REAL(R_ivuk
     &,4),
     & R_osuk,REAL(R_atuk,4),R_etuk,REAL(R_otuk,4),L_evuk
     &,
     & R_akal,R_ekal,L_efal,L_usuk,L_ituk,I_obal,R_ikal,R_ibal
     &,
     & R_ufal,R8_ebal,REAL(100000,4),REAL(R_evaxo,4),REAL
     &(R_uvuk,4),
     & REAL(R_edal,4),REAL(R_adal,4),REAL(R_ubal,4))
C SRG_vlv.fgi( 672, 406):���������� ���������� �������,20SRG10AA208
      R_isuk = R8_ebal * R_(101)
C SRG_vlv.fgi( 672, 395):����������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ubexo,R_ukexo,REAL(1
     &,4),
     & REAL(R_afexo,4),I_okexo,REAL(R_idexo,4),
     & REAL(R_odexo,4),REAL(R_obexo,4),
     & REAL(R_ibexo,4),REAL(R_ifexo,4),L_ofexo,REAL(R_ufexo
     &,4),L_akexo,
     & L_ekexo,R_udexo,REAL(R_edexo,4),REAL(R_adexo,4),L_ikexo
     &)
      !}
C SRG_vlv.fgi( 141, 560):���������� ������� ��� 2,20SRG10CT008XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_opixo,R_otixo,REAL(1
     &,4),
     & REAL(R_urixo,4),I_itixo,REAL(R_erixo,4),
     & REAL(R_irixo,4),REAL(R_ipixo,4),
     & REAL(R_epixo,4),REAL(R_esixo,4),L_isixo,REAL(R_osixo
     &,4),L_usixo,
     & L_atixo,R_orixo,REAL(R_arixo,4),REAL(R_upixo,4),L_etixo
     &)
      !}
C SRG_vlv.fgi( 253, 616):���������� ������� ��� 2,20SRG10CW004XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_evixo,R_edoxo,REAL(1
     &,4),
     & REAL(R_ixixo,4),I_adoxo,REAL(R_uvixo,4),
     & REAL(R_axixo,4),REAL(R_avixo,4),
     & REAL(R_utixo,4),REAL(R_uxixo,4),L_aboxo,REAL(R_eboxo
     &,4),L_iboxo,
     & L_oboxo,R_exixo,REAL(R_ovixo,4),REAL(R_ivixo,4),L_uboxo
     &)
      !}
C SRG_vlv.fgi( 253, 630):���������� ������� ��� 2,20SRG10CW003XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_udoxo,R_uloxo,REAL(1
     &,4),
     & REAL(R_akoxo,4),I_oloxo,REAL(R_ifoxo,4),
     & REAL(R_ofoxo,4),REAL(R_odoxo,4),
     & REAL(R_idoxo,4),REAL(R_ikoxo,4),L_okoxo,REAL(R_ukoxo
     &,4),L_aloxo,
     & L_eloxo,R_ufoxo,REAL(R_efoxo,4),REAL(R_afoxo,4),L_iloxo
     &)
      !}
C SRG_vlv.fgi( 253, 644):���������� ������� ��� 2,20SRG10CW002XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_imoxo,R_isoxo,REAL(1
     &,4),
     & REAL(R_opoxo,4),I_esoxo,REAL(R_apoxo,4),
     & REAL(R_epoxo,4),REAL(R_emoxo,4),
     & REAL(R_amoxo,4),REAL(R_aroxo,4),L_eroxo,REAL(R_iroxo
     &,4),L_oroxo,
     & L_uroxo,R_ipoxo,REAL(R_umoxo,4),REAL(R_omoxo,4),L_asoxo
     &)
      !}
C SRG_vlv.fgi( 253, 658):���������� ������� ��� 2,20SRG10CW001XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_atoxo,R_abuxo,REAL(1
     &,4),
     & REAL(R_evoxo,4),I_uxoxo,REAL(R_otoxo,4),
     & REAL(R_utoxo,4),REAL(R_usoxo,4),
     & REAL(R_osoxo,4),REAL(R_ovoxo,4),L_uvoxo,REAL(R_axoxo
     &,4),L_exoxo,
     & L_ixoxo,R_avoxo,REAL(R_itoxo,4),REAL(R_etoxo,4),L_oxoxo
     &)
      !}
C SRG_vlv.fgi( 112, 476):���������� ������� ��� 2,20SRG10CM007XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_obuxo,R_okuxo,REAL(1
     &,4),
     & REAL(R_uduxo,4),I_ikuxo,REAL(R_eduxo,4),
     & REAL(R_iduxo,4),REAL(R_ibuxo,4),
     & REAL(R_ebuxo,4),REAL(R_efuxo,4),L_ifuxo,REAL(R_ofuxo
     &,4),L_ufuxo,
     & L_akuxo,R_oduxo,REAL(R_aduxo,4),REAL(R_ubuxo,4),L_ekuxo
     &)
      !}
C SRG_vlv.fgi( 112, 490):���������� ������� ��� 2,20SRG10CQ007XQ01
      Call REG_RAS_MAN(deltat,.TRUE.,L_atal,REAL(R_usal,4
     &),REAL(R_apal,4),R_ital,
     & REAL(R_emal,4),R_etal,R_aval,R_utal,R_imal,REAL(R_umal
     &,4),
     & R_alal,REAL(R_ilal,4),R_olal,REAL(R_amal,4),L_omal
     &,
     & R_ival,R_oval,L_otal,L_elal,L_ulal,I_asal,R_uval,R_ural
     &,
     & R_eval,R8_oral,REAL(100000,4),REAL(R_oduxo,4),REAL
     &(R_epal,4),
     & REAL(R_osal,4),REAL(R_isal,4),REAL(R_esal,4))
C SRG_vlv.fgi( 624, 254):���������� ���������� �������,20SRG10AA207
      R_ukal = R8_oral * R_(102)
C SRG_vlv.fgi( 624, 243):����������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_eluxo,R_eruxo,REAL(1
     &,4),
     & REAL(R_imuxo,4),I_aruxo,REAL(R_uluxo,4),
     & REAL(R_amuxo,4),REAL(R_aluxo,4),
     & REAL(R_ukuxo,4),REAL(R_umuxo,4),L_apuxo,REAL(R_epuxo
     &,4),L_ipuxo,
     & L_opuxo,R_emuxo,REAL(R_oluxo,4),REAL(R_iluxo,4),L_upuxo
     &)
      !}
C SRG_vlv.fgi( 141, 574):���������� ������� ��� 2,20SRG10CT007XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_avabu,R_adebu,REAL(1
     &,4),
     & REAL(R_exabu,4),I_ubebu,REAL(R_ovabu,4),
     & REAL(R_uvabu,4),REAL(R_utabu,4),
     & REAL(R_otabu,4),REAL(R_oxabu,4),L_uxabu,REAL(R_abebu
     &,4),L_ebebu,
     & L_ibebu,R_axabu,REAL(R_ivabu,4),REAL(R_evabu,4),L_obebu
     &)
      !}
C SRG_vlv.fgi( 112, 504):���������� ������� ��� 2,20SRG10CM006XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_odebu,R_olebu,REAL(1
     &,4),
     & REAL(R_ufebu,4),I_ilebu,REAL(R_efebu,4),
     & REAL(R_ifebu,4),REAL(R_idebu,4),
     & REAL(R_edebu,4),REAL(R_ekebu,4),L_ikebu,REAL(R_okebu
     &,4),L_ukebu,
     & L_alebu,R_ofebu,REAL(R_afebu,4),REAL(R_udebu,4),L_elebu
     &)
      !}
C SRG_vlv.fgi( 112, 518):���������� ������� ��� 2,20SRG10CQ006XQ01
      Call REG_RAS_MAN(deltat,.TRUE.,L_ilel,REAL(R_elel,4
     &),REAL(R_idel,4),R_ulel,
     & REAL(R_obel,4),R_olel,R_imel,R_emel,R_ubel,REAL(R_edel
     &,4),
     & R_ixal,REAL(R_uxal,4),R_abel,REAL(R_ibel,4),L_adel
     &,
     & R_umel,R_apel,L_amel,L_oxal,L_ebel,I_ikel,R_epel,R_ekel
     &,
     & R_omel,R8_akel,REAL(100000,4),REAL(R_ofebu,4),REAL
     &(R_odel,4),
     & REAL(R_alel,4),REAL(R_ukel,4),REAL(R_okel,4))
C SRG_vlv.fgi( 623, 279):���������� ���������� �������,20SRG10AA206
      R_exal = R8_akel * R_(103)
C SRG_vlv.fgi( 623, 268):����������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_emebu,R_esebu,REAL(1
     &,4),
     & REAL(R_ipebu,4),I_asebu,REAL(R_umebu,4),
     & REAL(R_apebu,4),REAL(R_amebu,4),
     & REAL(R_ulebu,4),REAL(R_upebu,4),L_arebu,REAL(R_erebu
     &,4),L_irebu,
     & L_orebu,R_epebu,REAL(R_omebu,4),REAL(R_imebu,4),L_urebu
     &)
      !}
C SRG_vlv.fgi( 141, 588):���������� ������� ��� 2,20SRG10CT006XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_axibu,R_afobu,REAL(1
     &,4),
     & REAL(R_ebobu,4),I_udobu,REAL(R_oxibu,4),
     & REAL(R_uxibu,4),REAL(R_uvibu,4),
     & REAL(R_ovibu,4),REAL(R_obobu,4),L_ubobu,REAL(R_adobu
     &,4),L_edobu,
     & L_idobu,R_abobu,REAL(R_ixibu,4),REAL(R_exibu,4),L_odobu
     &)
      !}
C SRG_vlv.fgi( 112, 532):���������� ������� ��� 2,20SRG10CM005XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ofobu,R_omobu,REAL(1
     &,4),
     & REAL(R_ukobu,4),I_imobu,REAL(R_ekobu,4),
     & REAL(R_ikobu,4),REAL(R_ifobu,4),
     & REAL(R_efobu,4),REAL(R_elobu,4),L_ilobu,REAL(R_olobu
     &,4),L_ulobu,
     & L_amobu,R_okobu,REAL(R_akobu,4),REAL(R_ufobu,4),L_emobu
     &)
      !}
C SRG_vlv.fgi( 112, 546):���������� ������� ��� 2,20SRG10CQ005XQ01
      Call REG_RAS_MAN(deltat,.TRUE.,L_uxel,REAL(R_oxel,4
     &),REAL(R_usel,4),R_ebil,
     & REAL(R_asel,4),R_abil,R_ubil,R_obil,R_esel,REAL(R_osel
     &,4),
     & R_upel,REAL(R_erel,4),R_irel,REAL(R_urel,4),L_isel
     &,
     & R_edil,R_idil,L_ibil,L_arel,L_orel,I_uvel,R_odil,R_ovel
     &,
     & R_adil,R8_ivel,REAL(100000,4),REAL(R_okobu,4),REAL
     &(R_atel,4),
     & REAL(R_ixel,4),REAL(R_exel,4),REAL(R_axel,4))
C SRG_vlv.fgi( 622, 305):���������� ���������� �������,20SRG10AA205
      R_opel = R8_ivel * R_(104)
C SRG_vlv.fgi( 622, 294):����������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_epobu,R_etobu,REAL(1
     &,4),
     & REAL(R_irobu,4),I_atobu,REAL(R_upobu,4),
     & REAL(R_arobu,4),REAL(R_apobu,4),
     & REAL(R_umobu,4),REAL(R_urobu,4),L_asobu,REAL(R_esobu
     &,4),L_isobu,
     & L_osobu,R_erobu,REAL(R_opobu,4),REAL(R_ipobu,4),L_usobu
     &)
      !}
C SRG_vlv.fgi( 141, 602):���������� ������� ��� 2,20SRG10CT005XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_abadu,R_akadu,REAL(1
     &,4),
     & REAL(R_edadu,4),I_ufadu,REAL(R_obadu,4),
     & REAL(R_ubadu,4),REAL(R_uxubu,4),
     & REAL(R_oxubu,4),REAL(R_odadu,4),L_udadu,REAL(R_afadu
     &,4),L_efadu,
     & L_ifadu,R_adadu,REAL(R_ibadu,4),REAL(R_ebadu,4),L_ofadu
     &)
      !}
C SRG_vlv.fgi( 141, 616):���������� ������� ��� 2,20SRG10CT004XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_okadu,R_opadu,REAL(1
     &,4),
     & REAL(R_uladu,4),I_ipadu,REAL(R_eladu,4),
     & REAL(R_iladu,4),REAL(R_ikadu,4),
     & REAL(R_ekadu,4),REAL(R_emadu,4),L_imadu,REAL(R_omadu
     &,4),L_umadu,
     & L_apadu,R_oladu,REAL(R_aladu,4),REAL(R_ukadu,4),L_epadu
     &)
      !}
C SRG_vlv.fgi( 112, 560):���������� ������� ��� 2,20SRG10CM004XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_eradu,R_evadu,REAL(1
     &,4),
     & REAL(R_isadu,4),I_avadu,REAL(R_uradu,4),
     & REAL(R_asadu,4),REAL(R_aradu,4),
     & REAL(R_upadu,4),REAL(R_usadu,4),L_atadu,REAL(R_etadu
     &,4),L_itadu,
     & L_otadu,R_esadu,REAL(R_oradu,4),REAL(R_iradu,4),L_utadu
     &)
      !}
C SRG_vlv.fgi( 112, 574):���������� ������� ��� 2,20SRG10CQ004XQ01
      Call REG_RAS_MAN(deltat,.TRUE.,L_eril,REAL(R_aril,4
     &),REAL(R_elil,4),R_oril,
     & REAL(R_ikil,4),R_iril,R_esil,R_asil,R_okil,REAL(R_alil
     &,4),
     & R_efil,REAL(R_ofil,4),R_ufil,REAL(R_ekil,4),L_ukil
     &,
     & R_osil,R_usil,L_uril,L_ifil,L_akil,I_epil,R_atil,R_apil
     &,
     & R_isil,R8_umil,REAL(100000,4),REAL(R_esadu,4),REAL
     &(R_ilil,4),
     & REAL(R_upil,4),REAL(R_opil,4),REAL(R_ipil,4))
C SRG_vlv.fgi( 622, 331):���������� ���������� �������,20SRG10AA204
      R_afil = R8_umil * R_(105)
C SRG_vlv.fgi( 622, 320):����������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_otedu,R_obidu,REAL(1
     &,4),
     & REAL(R_uvedu,4),I_ibidu,REAL(R_evedu,4),
     & REAL(R_ivedu,4),REAL(R_itedu,4),
     & REAL(R_etedu,4),REAL(R_exedu,4),L_ixedu,REAL(R_oxedu
     &,4),L_uxedu,
     & L_abidu,R_ovedu,REAL(R_avedu,4),REAL(R_utedu,4),L_ebidu
     &)
      !}
C SRG_vlv.fgi( 141, 630):���������� ������� ��� 2,20SRG10CT003XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_olidu,R_oridu,REAL(1
     &,4),
     & REAL(R_umidu,4),I_iridu,REAL(R_emidu,4),
     & REAL(R_imidu,4),REAL(R_ilidu,4),
     & REAL(R_elidu,4),REAL(R_epidu,4),L_ipidu,REAL(R_opidu
     &,4),L_upidu,
     & L_aridu,R_omidu,REAL(R_amidu,4),REAL(R_ulidu,4),L_eridu
     &)
      !}
C SRG_vlv.fgi( 112, 588):���������� ������� ��� 2,20SRG10CM003XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_esidu,R_exidu,REAL(1
     &,4),
     & REAL(R_itidu,4),I_axidu,REAL(R_usidu,4),
     & REAL(R_atidu,4),REAL(R_asidu,4),
     & REAL(R_uridu,4),REAL(R_utidu,4),L_avidu,REAL(R_evidu
     &,4),L_ividu,
     & L_ovidu,R_etidu,REAL(R_osidu,4),REAL(R_isidu,4),L_uvidu
     &)
      !}
C SRG_vlv.fgi( 112, 602):���������� ������� ��� 2,20SRG10CQ003XQ01
      Call REG_RAS_MAN(deltat,.TRUE.,L_ofol,REAL(R_ifol,4
     &),REAL(R_oxil,4),R_akol,
     & REAL(R_uvil,4),R_ufol,R_okol,R_ikol,R_axil,REAL(R_ixil
     &,4),
     & R_otil,REAL(R_avil,4),R_evil,REAL(R_ovil,4),L_exil
     &,
     & R_alol,R_elol,L_ekol,L_util,L_ivil,I_odol,R_ilol,R_idol
     &,
     & R_ukol,R8_edol,REAL(100000,4),REAL(R_etidu,4),REAL
     &(R_uxil,4),
     & REAL(R_efol,4),REAL(R_afol,4),REAL(R_udol,4))
C SRG_vlv.fgi( 622, 358):���������� ���������� �������,20SRG10AA203
      R_itil = R8_edol * R_(106)
C SRG_vlv.fgi( 622, 347):����������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_afudu,R_amudu,REAL(1
     &,4),
     & REAL(R_ekudu,4),I_uludu,REAL(R_ofudu,4),
     & REAL(R_ufudu,4),REAL(R_ududu,4),
     & REAL(R_odudu,4),REAL(R_okudu,4),L_ukudu,REAL(R_aludu
     &,4),L_eludu,
     & L_iludu,R_akudu,REAL(R_ifudu,4),REAL(R_efudu,4),L_oludu
     &)
      !}
C SRG_vlv.fgi( 141, 644):���������� ������� ��� 2,20SRG10CT002XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_omudu,R_osudu,REAL(1
     &,4),
     & REAL(R_upudu,4),I_isudu,REAL(R_epudu,4),
     & REAL(R_ipudu,4),REAL(R_imudu,4),
     & REAL(R_emudu,4),REAL(R_erudu,4),L_irudu,REAL(R_orudu
     &,4),L_urudu,
     & L_asudu,R_opudu,REAL(R_apudu,4),REAL(R_umudu,4),L_esudu
     &)
      !}
C SRG_vlv.fgi( 112, 616):���������� ������� ��� 2,20SRG10CM002XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_etudu,R_ebafu,REAL(1
     &,4),
     & REAL(R_ivudu,4),I_abafu,REAL(R_utudu,4),
     & REAL(R_avudu,4),REAL(R_atudu,4),
     & REAL(R_usudu,4),REAL(R_uvudu,4),L_axudu,REAL(R_exudu
     &,4),L_ixudu,
     & L_oxudu,R_evudu,REAL(R_otudu,4),REAL(R_itudu,4),L_uxudu
     &)
      !}
C SRG_vlv.fgi( 112, 630):���������� ������� ��� 2,20SRG10CQ002XQ01
      Call REG_RAS_MAN(deltat,.TRUE.,L_avol,REAL(R_utol,4
     &),REAL(R_arol,4),R_ivol,
     & REAL(R_epol,4),R_evol,R_axol,R_uvol,R_ipol,REAL(R_upol
     &,4),
     & R_amol,REAL(R_imol,4),R_omol,REAL(R_apol,4),L_opol
     &,
     & R_ixol,R_oxol,L_ovol,L_emol,L_umol,I_atol,R_uxol,R_usol
     &,
     & R_exol,R8_osol,REAL(100000,4),REAL(R_evudu,4),REAL
     &(R_erol,4),
     & REAL(R_otol,4),REAL(R_itol,4),REAL(R_etol,4))
C SRG_vlv.fgi( 622, 383):���������� ���������� �������,20SRG10AA202
      R_ulol = R8_osol * R_(107)
C SRG_vlv.fgi( 622, 372):����������
      !{
      Call BOX_HANDLER(deltat,L_emamu,
     & R_amamu,REAL(R_imamu,4),
     & L_apamu,R_umamu,
     & REAL(R_epamu,4),L_ilamu,R_elamu,
     & REAL(R_olamu,4),L_ipamu,I_ukamu,L_omamu,L_ulamu,
     & I_alamu)
      !}
C SRG_vlv.fgi( 684, 712):���������� �����,20SRG10_BOX14
      !{
      Call BOX_HANDLER(deltat,L_asamu,
     & R_uramu,REAL(R_esamu,4),
     & L_usamu,R_osamu,
     & REAL(R_atamu,4),L_eramu,R_aramu,
     & REAL(R_iramu,4),L_etamu,I_opamu,L_isamu,L_oramu,
     & I_upamu)
      !}
C SRG_vlv.fgi( 657, 712):���������� �����,20SRG10_BOX13
      !{
      Call BOX_HANDLER(deltat,L_uvamu,
     & R_ovamu,REAL(R_axamu,4),
     & L_oxamu,R_ixamu,
     & REAL(R_uxamu,4),L_avamu,R_utamu,
     & REAL(R_evamu,4),L_abemu,I_itamu,L_examu,L_ivamu,
     & I_otamu)
      !}
C SRG_vlv.fgi( 620, 712):���������� �����,20SRG10_BOX12
      !{
      Call BOX_HANDLER(deltat,L_odemu,
     & R_idemu,REAL(R_udemu,4),
     & L_ifemu,R_efemu,
     & REAL(R_ofemu,4),L_ubemu,R_obemu,
     & REAL(R_ademu,4),L_ufemu,I_ebemu,L_afemu,L_edemu,
     & I_ibemu)
      !}
C SRG_vlv.fgi( 586, 712):���������� �����,20SRG10_BOX11
      !{
      Call BOX_HANDLER(deltat,L_ilemu,
     & R_elemu,REAL(R_olemu,4),
     & L_ememu,R_amemu,
     & REAL(R_imemu,4),L_okemu,R_ikemu,
     & REAL(R_ukemu,4),L_omemu,I_akemu,L_ulemu,L_alemu,
     & I_ekemu)
      !}
C SRG_vlv.fgi( 711, 742):���������� �����,20SRG10_BOX10
      !{
      Call BOX_HANDLER(deltat,L_eremu,
     & R_aremu,REAL(R_iremu,4),
     & L_asemu,R_uremu,
     & REAL(R_esemu,4),L_ipemu,R_epemu,
     & REAL(R_opemu,4),L_isemu,I_umemu,L_oremu,L_upemu,
     & I_apemu)
      !}
C SRG_vlv.fgi( 684, 742):���������� �����,20SRG10_BOX9
      !{
      Call BOX_HANDLER(deltat,L_avemu,
     & R_utemu,REAL(R_evemu,4),
     & L_uvemu,R_ovemu,
     & REAL(R_axemu,4),L_etemu,R_atemu,
     & REAL(R_itemu,4),L_exemu,I_osemu,L_ivemu,L_otemu,
     & I_usemu)
      !}
C SRG_vlv.fgi( 657, 742):���������� �����,20SRG10_BOX8
      !{
      Call BOX_HANDLER(deltat,L_ubimu,
     & R_obimu,REAL(R_adimu,4),
     & L_odimu,R_idimu,
     & REAL(R_udimu,4),L_abimu,R_uxemu,
     & REAL(R_ebimu,4),L_afimu,I_ixemu,L_edimu,L_ibimu,
     & I_oxemu)
      !}
C SRG_vlv.fgi( 620, 742):���������� �����,20SRG10_BOX7
      !{
      Call BOX_HANDLER(deltat,L_okimu,
     & R_ikimu,REAL(R_ukimu,4),
     & L_ilimu,R_elimu,
     & REAL(R_olimu,4),L_ufimu,R_ofimu,
     & REAL(R_akimu,4),L_ulimu,I_efimu,L_alimu,L_ekimu,
     & I_ifimu)
      !}
C SRG_vlv.fgi( 586, 742):���������� �����,20SRG10_BOX6
      !{
      Call BOX_HANDLER(deltat,L_ipimu,
     & R_epimu,REAL(R_opimu,4),
     & L_erimu,R_arimu,
     & REAL(R_irimu,4),L_omimu,R_imimu,
     & REAL(R_umimu,4),L_orimu,I_amimu,L_upimu,L_apimu,
     & I_emimu)
      !}
C SRG_vlv.fgi( 711, 772):���������� �����,20SRG10_BOX5
      !{
      Call BOX_HANDLER(deltat,L_etimu,
     & R_atimu,REAL(R_itimu,4),
     & L_avimu,R_utimu,
     & REAL(R_evimu,4),L_isimu,R_esimu,
     & REAL(R_osimu,4),L_ivimu,I_urimu,L_otimu,L_usimu,
     & I_asimu)
      !}
C SRG_vlv.fgi( 684, 772):���������� �����,20SRG10_BOX4
      !{
      Call BOX_HANDLER(deltat,L_abomu,
     & R_uximu,REAL(R_ebomu,4),
     & L_ubomu,R_obomu,
     & REAL(R_adomu,4),L_eximu,R_aximu,
     & REAL(R_iximu,4),L_edomu,I_ovimu,L_ibomu,L_oximu,
     & I_uvimu)
      !}
C SRG_vlv.fgi( 657, 772):���������� �����,20SRG10_BOX3
      !{
      Call BOX_HANDLER(deltat,L_ufomu,
     & R_ofomu,REAL(R_akomu,4),
     & L_okomu,R_ikomu,
     & REAL(R_ukomu,4),L_afomu,R_udomu,
     & REAL(R_efomu,4),L_alomu,I_idomu,L_ekomu,L_ifomu,
     & I_odomu)
      !}
C SRG_vlv.fgi( 620, 772):���������� �����,20SRG10_BOX2
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_utumu,R_ubapu,REAL(1
     &,4),
     & REAL(R_axumu,4),I_obapu,REAL(R_ivumu,4),
     & REAL(R_ovumu,4),REAL(R_otumu,4),
     & REAL(R_itumu,4),REAL(R_ixumu,4),L_oxumu,REAL(R_uxumu
     &,4),L_abapu,
     & L_ebapu,R_uvumu,REAL(R_evumu,4),REAL(R_avumu,4),L_ibapu
     &)
      !}
C SRG_vlv.fgi(  84, 658):���������� ������� ��� 2,20SRG10CP040XQ01
      !{
      Call BOX_HANDLER(deltat,L_apapu,
     & R_umapu,REAL(R_epapu,4),
     & L_upapu,R_opapu,
     & REAL(R_arapu,4),L_emapu,R_amapu,
     & REAL(R_imapu,4),L_erapu,I_olapu,L_ipapu,L_omapu,
     & I_ulapu)
      !}
C SRG_vlv.fgi( 586, 772):���������� �����,20SRG10_BOX1
      !�����. �������� I0_orapu = SRG_logicC?? /z'01000003'
C /
C SRG_logic.fgi( 287, 257):��������� �����
      !�����. �������� I0_urapu = SRG_logicC?? /z'01000010'
C /
C SRG_logic.fgi( 287, 259):��������� �����
      !�����. �������� I0_esapu = SRG_logicC?? /z'01000003'
C /
C SRG_logic.fgi( 287, 269):��������� �����
      !�����. �������� I0_isapu = SRG_logicC?? /z'01000010'
C /
C SRG_logic.fgi( 287, 267):��������� �����
      !�����. �������� I0_atapu = SRG_logicC?? /z'01000003'
C /
C SRG_logic.fgi( 287, 292):��������� �����
      !�����. �������� I0_usapu = SRG_logicC?? /z'01000010'
C /
C SRG_logic.fgi( 287, 290):��������� �����
      R_(111) = 220.0
C SRG_logic.fgi( 130, 156):��������� (RE4) (�������)
      R_(109) = 400.0
C SRG_logic.fgi( 132, 163):��������� (RE4) (�������)
      R_(110) = 0.0
C SRG_logic.fgi( 132, 165):��������� (RE4) (�������)
      R_(112) = 0.0
C SRG_logic.fgi( 130, 158):��������� (RE4) (�������)
      R_(113) = 400.0
C SRG_logic.fgi( 122, 184):��������� (RE4) (�������)
      R_(114) = 0.0
C SRG_logic.fgi( 122, 186):��������� (RE4) (�������)
      R_(115) = 220.0
C SRG_logic.fgi( 120, 178):��������� (RE4) (�������)
      R_(116) = 0.0
C SRG_logic.fgi( 120, 180):��������� (RE4) (�������)
      L0_ixapu=R0_ivapu.ne.R0_evapu
      R0_evapu=R0_ivapu
C SRG_logic.fgi( 345,  70):���������� ������������� ������
      L0_exapu=R0_uvapu.ne.R0_ovapu
      R0_ovapu=R0_uvapu
C SRG_logic.fgi( 345,  81):���������� ������������� ������
      L0_oxapu=L0_exapu.or.(L0_oxapu.and..not.(L0_ixapu))
      L_(338)=.not.L0_oxapu
C SRG_logic.fgi( 373,  76):RS �������
      I_(1) = 6
C SRG_logic.fgi( 383,  81):��������� ������������� IN (�������)
      I_(2) = 0
C SRG_logic.fgi( 383,  83):��������� ������������� IN (�������)
      if(L0_oxapu) then
         I_axapu=I_(1)
      else
         I_axapu=I_(2)
      endif
C SRG_logic.fgi( 388,  81):���� RE IN LO CH7
      L_(15)=I_axapu.ne.I0_odo
C SRG_temp_pss.fgi( 243, 146):���������� �������������
      if(L_(15)) then
         R8_ubo=R_(1)
      else
         R8_ubo=R_(2)
      endif
C SRG_temp_pss.fgi( 250, 166):���� RE IN LO CH7
      R8_abo=R8_ubo
C SRG_temp_pss.fgi( 260, 170):������,PSS3_B02_K
      if(L_(15)) then
         R_(6)=R8_udo
      else
         R_(6)=R_(7)
      endif
C SRG_temp_pss.fgi( 250, 158):���� RE IN LO CH7
      if(L_(14)) then
         R8_ido=R_(5)
      elseif(L_(14)) then
         R8_ido=R8_ido
      else
         R8_ido=R8_ido+deltat/R_(10)*R_(6)
      endif
      if(R8_ido.gt.R_(3)) then
         R8_ido=R_(3)
      elseif(R8_ido.lt.R_(4)) then
         R8_ido=R_(4)
      endif
C SRG_temp_pss.fgi( 275, 159):����������
      R8_edo=R8_ido
C SRG_temp_pss.fgi( 309, 159):������,PSS3_B02_W_intTt
      L_(339)=I_amepu.ne.0
C SRG_logic.fgi( 363,  93):��������� 1->LO
      !��������� R_(119) = SRG_logicC?? /1900/
      R_(119)=R0_uxapu
C SRG_logic.fgi( 367, 117):���������
      R_(120) = (-R8_adepu) + R_(119)
C SRG_logic.fgi( 376, 118):��������
      !��������� R_(117) = SRG_logicC?? /0.006/
      R_(117)=R0_abepu
C SRG_logic.fgi( 387, 116):���������
      R_(124) = R_(120) * R_(117)
C SRG_logic.fgi( 390, 117):����������
      !��������� R_(121) = SRG_logicC?? /0/
      R_(121)=R0_ebepu
C SRG_logic.fgi( 399, 102):���������
      !��������� R_(118) = SRG_logicC?? /100000/
      R_(118)=R0_ibepu
C SRG_logic.fgi( 429, 115):���������
      !��������� R_(122) = SRG_logicC?? /0.05/
      R_(122)=R0_obepu
C SRG_logic.fgi( 399, 110):���������
      !��������� R_(123) = SRG_logicC?? /1.0/
      R_(123)=R0_ubepu
C SRG_logic.fgi( 399, 112):���������
      Call pid_tuned(deltat,L_afepu,R8_odepu,REAL(R_(121)
     &,4),REAL(R_idepu,4),R_ifepu,
     & REAL(R_udepu,4),REAL(R_edepu,4),REAL(R_(122),4),R_efepu
     &,REAL(R_(123),4),R_ufepu,R_ofepu,
     & REAL(R_(124),4))
C SRG_logic.fgi( 412, 110):��������� ����������� ������������,20SRG10AC006_REG
      R_(125) = R8_odepu * R_(118)
C SRG_logic.fgi( 432, 116):����������
      R_(126) = 0.0
C SRG_logic.fgi( 441, 117):��������� (RE4) (�������)
      L0_olepu=R0_okepu.ne.R0_ikepu
      R0_ikepu=R0_okepu
C SRG_logic.fgi( 345, 129):���������� ������������� ������
      L0_ilepu=R0_alepu.ne.R0_ukepu
      R0_ukepu=R0_alepu
C SRG_logic.fgi( 345, 140):���������� ������������� ������
      L0_ulepu=L0_ilepu.or.(L0_ulepu.and..not.(L0_olepu))
      L_(341)=.not.L0_ulepu
C SRG_logic.fgi( 373, 135):RS �������
      I_(3) = 5
C SRG_logic.fgi( 383, 140):��������� ������������� IN (�������)
      I_(4) = 0
C SRG_logic.fgi( 383, 142):��������� ������������� IN (�������)
      if(L0_ulepu) then
         I_elepu=I_(3)
      else
         I_elepu=I_(4)
      endif
C SRG_logic.fgi( 388, 140):���� RE IN LO CH7
      L_(21)=I_elepu.ne.I0_emo
C SRG_temp_pss.fgi(  74, 148):���������� �������������
      if(L_(21)) then
         R_(36)=R8_imo
      else
         R_(36)=R_(37)
      endif
C SRG_temp_pss.fgi(  81, 160):���� RE IN LO CH7
      if(L_(20)) then
         R8_amo=R_(35)
      elseif(L_(20)) then
         R8_amo=R8_amo
      else
         R8_amo=R8_amo+deltat/R_(40)*R_(36)
      endif
      if(R8_amo.gt.R_(33)) then
         R8_amo=R_(33)
      elseif(R8_amo.lt.R_(34)) then
         R8_amo=R_(34)
      endif
C SRG_temp_pss.fgi( 106, 161):����������
      R8_ulo=R8_amo
C SRG_temp_pss.fgi( 140, 161):������,PSS3_B01_W_intTt
      if(L_(21)) then
         R8_ilo=R_(31)
      else
         R8_ilo=R_(32)
      endif
C SRG_temp_pss.fgi(  81, 168):���� RE IN LO CH7
      R8_ebo=R8_ilo
C SRG_temp_pss.fgi(  91, 172):������,PSS3_B01_K
      L_(342)=I_amepu.ne.0
C SRG_logic.fgi( 363, 152):��������� 1->LO
      !��������� R_(129) = SRG_logicC?? /1900/
      R_(129)=R0_emepu
C SRG_logic.fgi( 367, 176):���������
      R_(130) = (-R8_ipepu) + R_(129)
C SRG_logic.fgi( 376, 177):��������
      !��������� R_(127) = SRG_logicC?? /0.006/
      R_(127)=R0_imepu
C SRG_logic.fgi( 387, 175):���������
      R_(134) = R_(130) * R_(127)
C SRG_logic.fgi( 390, 176):����������
      !��������� R_(131) = SRG_logicC?? /0/
      R_(131)=R0_omepu
C SRG_logic.fgi( 399, 161):���������
      !��������� R_(128) = SRG_logicC?? /100000/
      R_(128)=R0_umepu
C SRG_logic.fgi( 429, 174):���������
      !��������� R_(132) = SRG_logicC?? /0.05/
      R_(132)=R0_apepu
C SRG_logic.fgi( 399, 169):���������
      !��������� R_(133) = SRG_logicC?? /1.0/
      R_(133)=R0_epepu
C SRG_logic.fgi( 399, 171):���������
      Call pid_tuned(deltat,L_irepu,R8_arepu,REAL(R_(131)
     &,4),REAL(R_upepu,4),R_urepu,
     & REAL(R_erepu,4),REAL(R_opepu,4),REAL(R_(132),4),R_orepu
     &,REAL(R_(133),4),R_esepu,R_asepu,
     & REAL(R_(134),4))
C SRG_logic.fgi( 412, 169):��������� ����������� ������������,20SRG10AC005_REG
      R_(135) = R8_arepu * R_(128)
C SRG_logic.fgi( 432, 175):����������
      R_(136) = 0.0
C SRG_logic.fgi( 441, 176):��������� (RE4) (�������)
      L0_avepu=R0_atepu.ne.R0_usepu
      R0_usepu=R0_atepu
C SRG_logic.fgi( 182,  70):���������� ������������� ������
      L0_utepu=R0_itepu.ne.R0_etepu
      R0_etepu=R0_itepu
C SRG_logic.fgi( 182,  81):���������� ������������� ������
      L0_evepu=L0_utepu.or.(L0_evepu.and..not.(L0_avepu))
      L_(344)=.not.L0_evepu
C SRG_logic.fgi( 210,  76):RS �������
      I_(5) = 4
C SRG_logic.fgi( 220,  81):��������� ������������� IN (�������)
      I_(6) = 0
C SRG_logic.fgi( 220,  83):��������� ������������� IN (�������)
      if(L0_evepu) then
         I_otepu=I_(5)
      else
         I_otepu=I_(6)
      endif
C SRG_logic.fgi( 225,  81):���� RE IN LO CH7
      L_(17)=I_otepu.ne.I0_ufo
C SRG_temp_pss.fgi( 240, 200):���������� �������������
      if(L_(17)) then
         R_(16)=R8_ako
      else
         R_(16)=R_(17)
      endif
C SRG_temp_pss.fgi( 247, 212):���� RE IN LO CH7
      if(L_(16)) then
         R8_ofo=R_(15)
      elseif(L_(16)) then
         R8_ofo=R8_ofo
      else
         R8_ofo=R8_ofo+deltat/R_(20)*R_(16)
      endif
      if(R8_ofo.gt.R_(13)) then
         R8_ofo=R_(13)
      elseif(R8_ofo.lt.R_(14)) then
         R8_ofo=R_(14)
      endif
C SRG_temp_pss.fgi( 272, 213):����������
      R8_ifo=R8_ofo
C SRG_temp_pss.fgi( 306, 213):������,PSS2_B02_W_intTt
      if(L_(17)) then
         R8_afo=R_(11)
      else
         R8_afo=R_(12)
      endif
C SRG_temp_pss.fgi( 247, 220):���� RE IN LO CH7
      R8_uxi=R8_afo
C SRG_temp_pss.fgi( 257, 224):������,PSS2_B02_K
      L_(345)=I_okipu.ne.0
C SRG_logic.fgi( 200,  93):��������� 1->LO
      !��������� R_(139) = SRG_logicC?? /1900/
      R_(139)=R0_ivepu
C SRG_logic.fgi( 204, 117):���������
      R_(140) = (-R8_oxepu) + R_(139)
C SRG_logic.fgi( 213, 118):��������
      !��������� R_(137) = SRG_logicC?? /0.006/
      R_(137)=R0_ovepu
C SRG_logic.fgi( 224, 116):���������
      R_(144) = R_(140) * R_(137)
C SRG_logic.fgi( 227, 117):����������
      !��������� R_(141) = SRG_logicC?? /0/
      R_(141)=R0_uvepu
C SRG_logic.fgi( 236, 102):���������
      !��������� R_(138) = SRG_logicC?? /100000/
      R_(138)=R0_axepu
C SRG_logic.fgi( 266, 115):���������
      !��������� R_(142) = SRG_logicC?? /0.05/
      R_(142)=R0_exepu
C SRG_logic.fgi( 236, 110):���������
      !��������� R_(143) = SRG_logicC?? /1.0/
      R_(143)=R0_ixepu
C SRG_logic.fgi( 236, 112):���������
      Call pid_tuned(deltat,L_obipu,R8_ebipu,REAL(R_(141)
     &,4),REAL(R_abipu,4),R_adipu,
     & REAL(R_ibipu,4),REAL(R_uxepu,4),REAL(R_(142),4),R_ubipu
     &,REAL(R_(143),4),R_idipu,R_edipu,
     & REAL(R_(144),4))
C SRG_logic.fgi( 249, 110):��������� ����������� ������������,20SRG10AC004_REG
      R_(145) = R8_ebipu * R_(138)
C SRG_logic.fgi( 269, 116):����������
      R_(146) = 0.0
C SRG_logic.fgi( 278, 117):��������� (RE4) (�������)
      L0_ekipu=R0_efipu.ne.R0_afipu
      R0_afipu=R0_efipu
C SRG_logic.fgi( 182, 129):���������� ������������� ������
      L0_akipu=R0_ofipu.ne.R0_ifipu
      R0_ifipu=R0_ofipu
C SRG_logic.fgi( 182, 140):���������� ������������� ������
      L0_ikipu=L0_akipu.or.(L0_ikipu.and..not.(L0_ekipu))
      L_(347)=.not.L0_ikipu
C SRG_logic.fgi( 210, 135):RS �������
      I_(7) = 3
C SRG_logic.fgi( 220, 140):��������� ������������� IN (�������)
      I_(8) = 0
C SRG_logic.fgi( 220, 142):��������� ������������� IN (�������)
      if(L0_ikipu) then
         I_ufipu=I_(7)
      else
         I_ufipu=I_(8)
      endif
C SRG_logic.fgi( 225, 140):���� RE IN LO CH7
      L_(23)=I_ufipu.ne.I0_ipo
C SRG_temp_pss.fgi(  71, 202):���������� �������������
      if(L_(23)) then
         R8_omo=R_(41)
      else
         R8_omo=R_(42)
      endif
C SRG_temp_pss.fgi(  78, 222):���� RE IN LO CH7
      R8_oxi=R8_omo
C SRG_temp_pss.fgi(  88, 226):������,PSS2_B01_K
      if(L_(23)) then
         R_(46)=R8_opo
      else
         R_(46)=R_(47)
      endif
C SRG_temp_pss.fgi(  78, 214):���� RE IN LO CH7
      if(L_(22)) then
         R8_epo=R_(45)
      elseif(L_(22)) then
         R8_epo=R8_epo
      else
         R8_epo=R8_epo+deltat/R_(50)*R_(46)
      endif
      if(R8_epo.gt.R_(43)) then
         R8_epo=R_(43)
      elseif(R8_epo.lt.R_(44)) then
         R8_epo=R_(44)
      endif
C SRG_temp_pss.fgi( 103, 215):����������
      R8_apo=R8_epo
C SRG_temp_pss.fgi( 137, 215):������,PSS2_B01_W_intTt
      L_(348)=I_okipu.ne.0
C SRG_logic.fgi( 200, 152):��������� 1->LO
      !��������� R_(149) = SRG_logicC?? /1900/
      R_(149)=R0_ukipu
C SRG_logic.fgi( 204, 176):���������
      R_(150) = (-R8_amipu) + R_(149)
C SRG_logic.fgi( 213, 177):��������
      !��������� R_(147) = SRG_logicC?? /0.006/
      R_(147)=R0_alipu
C SRG_logic.fgi( 224, 175):���������
      R_(154) = R_(150) * R_(147)
C SRG_logic.fgi( 227, 176):����������
      !��������� R_(151) = SRG_logicC?? /0/
      R_(151)=R0_elipu
C SRG_logic.fgi( 236, 161):���������
      !��������� R_(148) = SRG_logicC?? /100000/
      R_(148)=R0_ilipu
C SRG_logic.fgi( 266, 174):���������
      !��������� R_(152) = SRG_logicC?? /0.05/
      R_(152)=R0_olipu
C SRG_logic.fgi( 236, 169):���������
      !��������� R_(153) = SRG_logicC?? /1.0/
      R_(153)=R0_ulipu
C SRG_logic.fgi( 236, 171):���������
      Call pid_tuned(deltat,L_apipu,R8_omipu,REAL(R_(151)
     &,4),REAL(R_imipu,4),R_ipipu,
     & REAL(R_umipu,4),REAL(R_emipu,4),REAL(R_(152),4),R_epipu
     &,REAL(R_(153),4),R_upipu,R_opipu,
     & REAL(R_(154),4))
C SRG_logic.fgi( 249, 169):��������� ����������� ������������,20SRG10AC003_REG
      R_(155) = R8_omipu * R_(148)
C SRG_logic.fgi( 269, 175):����������
      R_(156) = 0.0
C SRG_logic.fgi( 278, 176):��������� (RE4) (�������)
      L0_osipu=R0_oripu.ne.R0_iripu
      R0_iripu=R0_oripu
C SRG_logic.fgi(  23,  71):���������� ������������� ������
      L0_isipu=R0_asipu.ne.R0_uripu
      R0_uripu=R0_asipu
C SRG_logic.fgi(  23,  82):���������� ������������� ������
      L0_usipu=L0_isipu.or.(L0_usipu.and..not.(L0_osipu))
      L_(350)=.not.L0_usipu
C SRG_logic.fgi(  51,  77):RS �������
      I_(9) = 2
C SRG_logic.fgi(  61,  82):��������� ������������� IN (�������)
      I_(10) = 0
C SRG_logic.fgi(  61,  84):��������� ������������� IN (�������)
      if(L0_usipu) then
         I_esipu=I_(9)
      else
         I_esipu=I_(10)
      endif
C SRG_logic.fgi(  66,  82):���� RE IN LO CH7
      L_(19)=I_esipu.ne.I0_alo
C SRG_temp_pss.fgi( 239, 248):���������� �������������
      if(L_(19)) then
         R8_eko=R_(21)
      else
         R8_eko=R_(22)
      endif
C SRG_temp_pss.fgi( 246, 268):���� RE IN LO CH7
      R8_ibo=R8_eko
C SRG_temp_pss.fgi( 256, 272):������,PSS1_B02_K
      if(L_(19)) then
         R_(26)=R8_elo
      else
         R_(26)=R_(27)
      endif
C SRG_temp_pss.fgi( 246, 260):���� RE IN LO CH7
      if(L_(18)) then
         R8_uko=R_(25)
      elseif(L_(18)) then
         R8_uko=R8_uko
      else
         R8_uko=R8_uko+deltat/R_(30)*R_(26)
      endif
      if(R8_uko.gt.R_(23)) then
         R8_uko=R_(23)
      elseif(R8_uko.lt.R_(24)) then
         R8_uko=R_(24)
      endif
C SRG_temp_pss.fgi( 271, 261):����������
      R8_oko=R8_uko
C SRG_temp_pss.fgi( 305, 261):������,PSS1_B02_W_intTt
      L_(351)=I_efopu.ne.0
C SRG_logic.fgi(  41,  94):��������� 1->LO
      !��������� R_(159) = SRG_logicC?? /1900/
      R_(159)=R0_atipu
C SRG_logic.fgi(  45, 118):���������
      R_(160) = (-R8_evipu) + R_(159)
C SRG_logic.fgi(  54, 119):��������
      !��������� R_(157) = SRG_logicC?? /0.006/
      R_(157)=R0_etipu
C SRG_logic.fgi(  65, 117):���������
      R_(164) = R_(160) * R_(157)
C SRG_logic.fgi(  68, 118):����������
      !��������� R_(161) = SRG_logicC?? /0/
      R_(161)=R0_itipu
C SRG_logic.fgi(  77, 103):���������
      !��������� R_(158) = SRG_logicC?? /100000/
      R_(158)=R0_otipu
C SRG_logic.fgi( 107, 116):���������
      !��������� R_(162) = SRG_logicC?? /0.05/
      R_(162)=R0_utipu
C SRG_logic.fgi(  77, 111):���������
      !��������� R_(163) = SRG_logicC?? /1.0/
      R_(163)=R0_avipu
C SRG_logic.fgi(  77, 113):���������
      Call pid_tuned(deltat,L_exipu,R8_uvipu,REAL(R_(161)
     &,4),REAL(R_ovipu,4),R_oxipu,
     & REAL(R_axipu,4),REAL(R_ivipu,4),REAL(R_(162),4),R_ixipu
     &,REAL(R_(163),4),R_abopu,R_uxipu,
     & REAL(R_(164),4))
C SRG_logic.fgi(  90, 111):��������� ����������� ������������,20SRG10AC002_REG
      R_(165) = R8_uvipu * R_(158)
C SRG_logic.fgi( 110, 117):����������
      R_(166) = 0.0
C SRG_logic.fgi( 119, 118):��������� (RE4) (�������)
      L0_udopu=R0_ubopu.ne.R0_obopu
      R0_obopu=R0_ubopu
C SRG_logic.fgi(  23, 130):���������� ������������� ������
      L0_odopu=R0_edopu.ne.R0_adopu
      R0_adopu=R0_edopu
C SRG_logic.fgi(  23, 141):���������� ������������� ������
      L0_afopu=L0_odopu.or.(L0_afopu.and..not.(L0_udopu))
      L_(353)=.not.L0_afopu
C SRG_logic.fgi(  51, 136):RS �������
      I_(11) = 1
C SRG_logic.fgi(  61, 141):��������� ������������� IN (�������)
      I_(12) = 0
C SRG_logic.fgi(  61, 143):��������� ������������� IN (�������)
      if(L0_afopu) then
         I_idopu=I_(11)
      else
         I_idopu=I_(12)
      endif
C SRG_logic.fgi(  66, 141):���� RE IN LO CH7
      L_(25)=I_idopu.ne.I0_oro
C SRG_temp_pss.fgi(  70, 250):���������� �������������
      if(L_(25)) then
         R_(56)=R8_uro
      else
         R_(56)=R_(57)
      endif
C SRG_temp_pss.fgi(  77, 262):���� RE IN LO CH7
      if(L_(24)) then
         R8_iro=R_(55)
      elseif(L_(24)) then
         R8_iro=R8_iro
      else
         R8_iro=R8_iro+deltat/R_(60)*R_(56)
      endif
      if(R8_iro.gt.R_(53)) then
         R8_iro=R_(53)
      elseif(R8_iro.lt.R_(54)) then
         R8_iro=R_(54)
      endif
C SRG_temp_pss.fgi( 102, 263):����������
      R8_ero=R8_iro
C SRG_temp_pss.fgi( 136, 263):������,PSS1_B01_W_intTt
      if(L_(25)) then
         R8_upo=R_(51)
      else
         R8_upo=R_(52)
      endif
C SRG_temp_pss.fgi(  77, 270):���� RE IN LO CH7
      R8_obo=R8_upo
C SRG_temp_pss.fgi(  87, 274):������,PSS1_B01_K
      L_(354)=I_efopu.ne.0
C SRG_logic.fgi(  41, 150):��������� 1->LO
      !��������� R_(169) = SRG_logicC?? /1900/
      R_(169)=R0_ifopu
C SRG_logic.fgi(  45, 174):���������
      R_(170) = (-R8_okopu) + R_(169)
C SRG_logic.fgi(  54, 175):��������
      !��������� R_(167) = SRG_logicC?? /0.006/
      R_(167)=R0_ofopu
C SRG_logic.fgi(  65, 173):���������
      R_(174) = R_(170) * R_(167)
C SRG_logic.fgi(  68, 174):����������
      !��������� R_(171) = SRG_logicC?? /0/
      R_(171)=R0_ufopu
C SRG_logic.fgi(  77, 159):���������
      !��������� R_(168) = SRG_logicC?? /100000/
      R_(168)=R0_akopu
C SRG_logic.fgi( 107, 172):���������
      !��������� R_(172) = SRG_logicC?? /0.05/
      R_(172)=R0_ekopu
C SRG_logic.fgi(  77, 167):���������
      !��������� R_(173) = SRG_logicC?? /1.0/
      R_(173)=R0_ikopu
C SRG_logic.fgi(  77, 169):���������
      Call pid_tuned(deltat,L_olopu,R8_elopu,REAL(R_(171)
     &,4),REAL(R_alopu,4),R_amopu,
     & REAL(R_ilopu,4),REAL(R_ukopu,4),REAL(R_(172),4),R_ulopu
     &,REAL(R_(173),4),R_imopu,R_emopu,
     & REAL(R_(174),4))
C SRG_logic.fgi(  90, 167):��������� ����������� ������������,20SRG10AC001_REG
      R_(175) = R8_elopu * R_(168)
C SRG_logic.fgi( 110, 173):����������
      R_(176) = 0.0
C SRG_logic.fgi( 118, 174):��������� (RE4) (�������)
      I_(13) = z'01000003'
C SRG_logic.fgi( 543, 521):��������� ������������� IN (�������)
      I_(14) = z'01000009'
C SRG_logic.fgi( 543, 519):��������� ������������� IN (�������)
      I_(15) = z'01000003'
C SRG_logic.fgi( 495, 521):��������� ������������� IN (�������)
      I_(16) = z'01000009'
C SRG_logic.fgi( 495, 519):��������� ������������� IN (�������)
      L0_ivopu=R0_aropu.ne.R0_upopu
      R0_upopu=R0_aropu
C SRG_logic.fgi( 481, 536):���������� ������������� ������
      L0_ovopu=R0_iropu.ne.R0_eropu
      R0_eropu=R0_iropu
C SRG_logic.fgi( 481, 553):���������� ������������� ������
      L0_uvopu=R0_uropu.ne.R0_oropu
      R0_oropu=R0_uropu
C SRG_logic.fgi( 481, 571):���������� ������������� ������
      C30_asopu = '������������'
C SRG_logic.fgi( 535, 570):��������� ���������� CH20 (CH30) (�������)
      C30_esopu = '������'
C SRG_logic.fgi( 529, 565):��������� ���������� CH20 (CH30) (�������)
      C30_usopu = '��������������'
C SRG_logic.fgi( 514, 566):��������� ���������� CH20 (CH30) (�������)
      C30_atopu = '�� ������'
C SRG_logic.fgi( 514, 568):��������� ���������� CH20 (CH30) (�������)
      I_(17) = z'01000003'
C SRG_logic.fgi( 545, 532):��������� ������������� IN (�������)
      I_(18) = z'01000009'
C SRG_logic.fgi( 545, 530):��������� ������������� IN (�������)
      L_(365)=.true.
C SRG_logic.fgi( 494, 543):��������� ���������� (�������)
      if(L0_ivopu) then
         L_(362)=L_(365)
      else
         L_(362)=.false.
      endif
C SRG_logic.fgi( 498, 542):���� � ������������� �������
      L_(13)=L_(362)
C SRG_logic.fgi( 498, 542):������-�������: ���������� ��� �������������� ������
      L_(366)=.true.
C SRG_logic.fgi( 494, 558):��������� ���������� (�������)
      if(L0_ovopu) then
         L_(363)=L_(366)
      else
         L_(363)=.false.
      endif
C SRG_logic.fgi( 498, 557):���� � ������������� �������
      L_(12)=L_(363)
C SRG_logic.fgi( 498, 557):������-�������: ���������� ��� �������������� ������
      L_(360) = L_(12).OR.L_(13)
C SRG_logic.fgi( 508, 574):���
      L_(367)=.true.
C SRG_logic.fgi( 494, 578):��������� ���������� (�������)
      if(L0_uvopu) then
         L_(364)=L_(367)
      else
         L_(364)=.false.
      endif
C SRG_logic.fgi( 498, 577):���� � ������������� �������
      L_(11)=L_(364)
C SRG_logic.fgi( 498, 577):������-�������: ���������� ��� �������������� ������
      L_(356) = L_(11).OR.L_(12)
C SRG_logic.fgi( 508, 539):���
      L_utopu=(L_(13).or.L_utopu).and..not.(L_(356))
      L_(357)=.not.L_utopu
C SRG_logic.fgi( 516, 541):RS �������
      L_ipopu=L_utopu
C SRG_logic.fgi( 535, 543):������,SRG10_PSS3_tech_mode
      if(L_ipopu) then
         I_apopu=I_(14)
      else
         I_apopu=I_(13)
      endif
C SRG_logic.fgi( 546, 519):���� RE IN LO CH7
      L_(358) = L_(11).OR.L_(13)
C SRG_logic.fgi( 508, 554):���
      L_avopu=(L_(12).or.L_avopu).and..not.(L_(358))
      L_(359)=.not.L_avopu
C SRG_logic.fgi( 516, 556):RS �������
      L_etopu=L_avopu
C SRG_logic.fgi( 535, 558):������,SRG10_PSS3_ruch_mode
      if(L_etopu) then
         I_otopu=I_(18)
      else
         I_otopu=I_(17)
      endif
C SRG_logic.fgi( 548, 530):���� RE IN LO CH7
      L_evopu=(L_(11).or.L_evopu).and..not.(L_(360))
      L_(361)=.not.L_evopu
C SRG_logic.fgi( 516, 576):RS �������
      L_opopu=L_evopu
C SRG_logic.fgi( 537, 578):������,SRG10_PSS3_automatic_mode
      if(L_opopu) then
         I_epopu=I_(16)
      else
         I_epopu=I_(15)
      endif
C SRG_logic.fgi( 498, 519):���� RE IN LO CH7
      if(L_evopu) then
         C30_osopu=C30_usopu
      else
         C30_osopu=C30_atopu
      endif
C SRG_logic.fgi( 518, 566):���� RE IN LO CH20
      if(L_avopu) then
         C30_isopu=C30_esopu
      else
         C30_isopu=C30_osopu
      endif
C SRG_logic.fgi( 533, 565):���� RE IN LO CH20
      if(L_utopu) then
         C30_itopu=C30_asopu
      else
         C30_itopu=C30_isopu
      endif
C SRG_logic.fgi( 542, 564):���� RE IN LO CH20
      I_(19) = z'01000003'
C SRG_logic.fgi( 431, 521):��������� ������������� IN (�������)
      I_(20) = z'01000009'
C SRG_logic.fgi( 431, 519):��������� ������������� IN (�������)
      I_(21) = z'01000003'
C SRG_logic.fgi( 383, 521):��������� ������������� IN (�������)
      I_(22) = z'01000009'
C SRG_logic.fgi( 383, 519):��������� ������������� IN (�������)
      L0_ikupu=R0_abupu.ne.R0_uxopu
      R0_uxopu=R0_abupu
C SRG_logic.fgi( 369, 536):���������� ������������� ������
      L0_okupu=R0_ibupu.ne.R0_ebupu
      R0_ebupu=R0_ibupu
C SRG_logic.fgi( 369, 553):���������� ������������� ������
      L0_ukupu=R0_ubupu.ne.R0_obupu
      R0_obupu=R0_ubupu
C SRG_logic.fgi( 369, 571):���������� ������������� ������
      C30_adupu = '������������'
C SRG_logic.fgi( 423, 570):��������� ���������� CH20 (CH30) (�������)
      C30_edupu = '������'
C SRG_logic.fgi( 417, 565):��������� ���������� CH20 (CH30) (�������)
      C30_udupu = '��������������'
C SRG_logic.fgi( 402, 566):��������� ���������� CH20 (CH30) (�������)
      C30_afupu = '�� ������'
C SRG_logic.fgi( 402, 568):��������� ���������� CH20 (CH30) (�������)
      I_(23) = z'01000003'
C SRG_logic.fgi( 433, 532):��������� ������������� IN (�������)
      I_(24) = z'01000009'
C SRG_logic.fgi( 433, 530):��������� ������������� IN (�������)
      L_(377)=.true.
C SRG_logic.fgi( 382, 543):��������� ���������� (�������)
      if(L0_ikupu) then
         L_(374)=L_(377)
      else
         L_(374)=.false.
      endif
C SRG_logic.fgi( 386, 542):���� � ������������� �������
      L_(10)=L_(374)
C SRG_logic.fgi( 386, 542):������-�������: ���������� ��� �������������� ������
      L_(378)=.true.
C SRG_logic.fgi( 382, 558):��������� ���������� (�������)
      if(L0_okupu) then
         L_(375)=L_(378)
      else
         L_(375)=.false.
      endif
C SRG_logic.fgi( 386, 557):���� � ������������� �������
      L_(9)=L_(375)
C SRG_logic.fgi( 386, 557):������-�������: ���������� ��� �������������� ������
      L_(372) = L_(9).OR.L_(10)
C SRG_logic.fgi( 396, 574):���
      L_(379)=.true.
C SRG_logic.fgi( 382, 578):��������� ���������� (�������)
      if(L0_ukupu) then
         L_(376)=L_(379)
      else
         L_(376)=.false.
      endif
C SRG_logic.fgi( 386, 577):���� � ������������� �������
      L_(8)=L_(376)
C SRG_logic.fgi( 386, 577):������-�������: ���������� ��� �������������� ������
      L_(368) = L_(8).OR.L_(9)
C SRG_logic.fgi( 396, 539):���
      L_ufupu=(L_(10).or.L_ufupu).and..not.(L_(368))
      L_(369)=.not.L_ufupu
C SRG_logic.fgi( 404, 541):RS �������
      L_ixopu=L_ufupu
C SRG_logic.fgi( 423, 543):������,SRG10_PSS2_tech_mode
      if(L_ixopu) then
         I_axopu=I_(20)
      else
         I_axopu=I_(19)
      endif
C SRG_logic.fgi( 434, 519):���� RE IN LO CH7
      L_(370) = L_(8).OR.L_(10)
C SRG_logic.fgi( 396, 554):���
      L_akupu=(L_(9).or.L_akupu).and..not.(L_(370))
      L_(371)=.not.L_akupu
C SRG_logic.fgi( 404, 556):RS �������
      L_efupu=L_akupu
C SRG_logic.fgi( 423, 558):������,SRG10_PSS2_ruch_mode
      if(L_efupu) then
         I_ofupu=I_(24)
      else
         I_ofupu=I_(23)
      endif
C SRG_logic.fgi( 436, 530):���� RE IN LO CH7
      L_ekupu=(L_(8).or.L_ekupu).and..not.(L_(372))
      L_(373)=.not.L_ekupu
C SRG_logic.fgi( 404, 576):RS �������
      L_oxopu=L_ekupu
C SRG_logic.fgi( 425, 578):������,SRG10_PSS2_automatic_mode
      if(L_oxopu) then
         I_exopu=I_(22)
      else
         I_exopu=I_(21)
      endif
C SRG_logic.fgi( 386, 519):���� RE IN LO CH7
      if(L_ekupu) then
         C30_odupu=C30_udupu
      else
         C30_odupu=C30_afupu
      endif
C SRG_logic.fgi( 406, 566):���� RE IN LO CH20
      if(L_akupu) then
         C30_idupu=C30_edupu
      else
         C30_idupu=C30_odupu
      endif
C SRG_logic.fgi( 421, 565):���� RE IN LO CH20
      if(L_ufupu) then
         C30_ifupu=C30_adupu
      else
         C30_ifupu=C30_idupu
      endif
C SRG_logic.fgi( 430, 564):���� RE IN LO CH20
      I_(25) = z'01000003'
C SRG_logic.fgi( 320, 521):��������� ������������� IN (�������)
      I_(26) = z'01000009'
C SRG_logic.fgi( 320, 519):��������� ������������� IN (�������)
      I_(27) = z'01000003'
C SRG_logic.fgi( 272, 521):��������� ������������� IN (�������)
      I_(28) = z'01000009'
C SRG_logic.fgi( 272, 519):��������� ������������� IN (�������)
      L0_isupu=R0_amupu.ne.R0_ulupu
      R0_ulupu=R0_amupu
C SRG_logic.fgi( 258, 536):���������� ������������� ������
      L0_osupu=R0_imupu.ne.R0_emupu
      R0_emupu=R0_imupu
C SRG_logic.fgi( 258, 553):���������� ������������� ������
      L0_usupu=R0_umupu.ne.R0_omupu
      R0_omupu=R0_umupu
C SRG_logic.fgi( 258, 571):���������� ������������� ������
      C30_apupu = '������������'
C SRG_logic.fgi( 312, 570):��������� ���������� CH20 (CH30) (�������)
      C30_epupu = '������'
C SRG_logic.fgi( 306, 565):��������� ���������� CH20 (CH30) (�������)
      C30_upupu = '��������������'
C SRG_logic.fgi( 291, 566):��������� ���������� CH20 (CH30) (�������)
      C30_arupu = '�� ������'
C SRG_logic.fgi( 291, 568):��������� ���������� CH20 (CH30) (�������)
      I_(29) = z'01000003'
C SRG_logic.fgi( 322, 532):��������� ������������� IN (�������)
      I_(30) = z'01000009'
C SRG_logic.fgi( 322, 530):��������� ������������� IN (�������)
      L_(389)=.true.
C SRG_logic.fgi( 271, 543):��������� ���������� (�������)
      if(L0_isupu) then
         L_(386)=L_(389)
      else
         L_(386)=.false.
      endif
C SRG_logic.fgi( 275, 542):���� � ������������� �������
      L_(7)=L_(386)
C SRG_logic.fgi( 275, 542):������-�������: ���������� ��� �������������� ������
      L_(390)=.true.
C SRG_logic.fgi( 271, 558):��������� ���������� (�������)
      if(L0_osupu) then
         L_(387)=L_(390)
      else
         L_(387)=.false.
      endif
C SRG_logic.fgi( 275, 557):���� � ������������� �������
      L_(6)=L_(387)
C SRG_logic.fgi( 275, 557):������-�������: ���������� ��� �������������� ������
      L_(384) = L_(6).OR.L_(7)
C SRG_logic.fgi( 285, 574):���
      L_(391)=.true.
C SRG_logic.fgi( 271, 578):��������� ���������� (�������)
      if(L0_usupu) then
         L_(388)=L_(391)
      else
         L_(388)=.false.
      endif
C SRG_logic.fgi( 275, 577):���� � ������������� �������
      L_(5)=L_(388)
C SRG_logic.fgi( 275, 577):������-�������: ���������� ��� �������������� ������
      L_(380) = L_(5).OR.L_(6)
C SRG_logic.fgi( 285, 539):���
      L_urupu=(L_(7).or.L_urupu).and..not.(L_(380))
      L_(381)=.not.L_urupu
C SRG_logic.fgi( 293, 541):RS �������
      L_ilupu=L_urupu
C SRG_logic.fgi( 312, 543):������,SRG10_PSS1_tech_mode
      if(L_ilupu) then
         I_alupu=I_(26)
      else
         I_alupu=I_(25)
      endif
C SRG_logic.fgi( 323, 519):���� RE IN LO CH7
      L_(382) = L_(5).OR.L_(7)
C SRG_logic.fgi( 285, 554):���
      L_asupu=(L_(6).or.L_asupu).and..not.(L_(382))
      L_(383)=.not.L_asupu
C SRG_logic.fgi( 293, 556):RS �������
      L_erupu=L_asupu
C SRG_logic.fgi( 312, 558):������,SRG10_PSS1_ruch_mode
      if(L_erupu) then
         I_orupu=I_(30)
      else
         I_orupu=I_(29)
      endif
C SRG_logic.fgi( 325, 530):���� RE IN LO CH7
      L_esupu=(L_(5).or.L_esupu).and..not.(L_(384))
      L_(385)=.not.L_esupu
C SRG_logic.fgi( 293, 576):RS �������
      L_olupu=L_esupu
C SRG_logic.fgi( 314, 578):������,SRG10_PSS1_automatic_mode
      if(L_olupu) then
         I_elupu=I_(28)
      else
         I_elupu=I_(27)
      endif
C SRG_logic.fgi( 275, 519):���� RE IN LO CH7
      if(L_esupu) then
         C30_opupu=C30_upupu
      else
         C30_opupu=C30_arupu
      endif
C SRG_logic.fgi( 295, 566):���� RE IN LO CH20
      if(L_asupu) then
         C30_ipupu=C30_epupu
      else
         C30_ipupu=C30_opupu
      endif
C SRG_logic.fgi( 310, 565):���� RE IN LO CH20
      if(L_urupu) then
         C30_irupu=C30_apupu
      else
         C30_irupu=C30_ipupu
      endif
C SRG_logic.fgi( 319, 564):���� RE IN LO CH20
      I_(31) = z'01C0C0C0'
C SRG_logic.fgi( 545, 262):��������� ������������� IN (�������)
      I_(32) = z'0100FFFF'
C SRG_logic.fgi( 545, 264):��������� ������������� IN (�������)
      I_(34) = z'01C0C0C0'
C SRG_logic.fgi( 545, 298):��������� ������������� IN (�������)
      I_(33) = z'0100FFFF'
C SRG_logic.fgi( 545, 296):��������� ������������� IN (�������)
      L_(397)=I_ovaru.ne.0
C SRG_logic.fgi( 488, 279):��������� 1->LO
      L_(403)=I_uberu.ne.0
C SRG_logic.fgi( 488, 312):��������� 1->LO
      I_(35) = z'01C0C0C0'
C SRG_logic.fgi( 416, 262):��������� ������������� IN (�������)
      I_(36) = z'0100FFFF'
C SRG_logic.fgi( 416, 264):��������� ������������� IN (�������)
      I_(38) = z'01C0C0C0'
C SRG_logic.fgi( 416, 298):��������� ������������� IN (�������)
      I_(37) = z'0100FFFF'
C SRG_logic.fgi( 416, 296):��������� ������������� IN (�������)
      L_(409)=I_ovaru.ne.0
C SRG_logic.fgi( 359, 279):��������� 1->LO
      L_(415)=I_aderu.ne.0
C SRG_logic.fgi( 359, 312):��������� 1->LO
      I_(39) = z'01C0C0C0'
C SRG_logic.fgi( 288, 234):��������� ������������� IN (�������)
      I_(40) = z'0100FFFF'
C SRG_logic.fgi( 288, 236):��������� ������������� IN (�������)
      I_(42) = z'01C0C0C0'
C SRG_logic.fgi( 288, 282):��������� ������������� IN (�������)
      I_(41) = z'0100FFFF'
C SRG_logic.fgi( 288, 280):��������� ������������� IN (�������)
      L_(420)=I_ovaru.ne.0
C SRG_logic.fgi( 231, 251):��������� 1->LO
      L_(425)=I_iferu.ne.0
C SRG_logic.fgi( 231, 312):��������� 1->LO
      L_(435)=I_ediru.ne.0
C SRG_logic.fgi( 471, 467):��������� 1->LO
      L_(445)=I_aboru.ne.0
C SRG_logic.fgi( 350, 467):��������� 1->LO
      L_(455)=I_uvoru.ne.0
C SRG_logic.fgi( 231, 467):��������� 1->LO
      I_(43) = z'01000003'
C SRG_logic.fgi(  66, 514):��������� ������������� IN (�������)
      I_(44) = z'01000009'
C SRG_logic.fgi(  66, 512):��������� ������������� IN (�������)
      C30_opotu = '������������������'
C SRG_logic.fgi(  96, 573):��������� ���������� CH20 (CH30) (�������)
      L_(470)=.true.
C SRG_logic.fgi(  45, 528):��������� ���������� (�������)
      L0_erotu=R0_orotu.ne.R0_irotu
      R0_irotu=R0_orotu
C SRG_logic.fgi(  32, 521):���������� ������������� ������
      if(L0_erotu) then
         L_(477)=L_(470)
      else
         L_(477)=.false.
      endif
C SRG_logic.fgi(  49, 527):���� � ������������� �������
      L_(4)=L_(477)
C SRG_logic.fgi(  49, 527):������-�������: ���������� ��� �������������� ������
      I_(45) = z'01000003'
C SRG_logic.fgi( 102, 503):��������� ������������� IN (�������)
      I_(46) = z'01000009'
C SRG_logic.fgi( 102, 501):��������� ������������� IN (�������)
      I_(47) = z'01000003'
C SRG_logic.fgi(  62, 503):��������� ������������� IN (�������)
      I_(48) = z'01000009'
C SRG_logic.fgi(  62, 501):��������� ������������� IN (�������)
      L0_ibutu=R0_usotu.ne.R0_osotu
      R0_osotu=R0_usotu
C SRG_logic.fgi(  32, 536):���������� ������������� ������
      L0_obutu=R0_etotu.ne.R0_atotu
      R0_atotu=R0_etotu
C SRG_logic.fgi(  32, 553):���������� ������������� ������
      L0_ubutu=R0_ototu.ne.R0_itotu
      R0_itotu=R0_ototu
C SRG_logic.fgi(  32, 571):���������� ������������� ������
      C30_avotu = '������������'
C SRG_logic.fgi(  86, 570):��������� ���������� CH20 (CH30) (�������)
      C30_evotu = '������'
C SRG_logic.fgi(  80, 565):��������� ���������� CH20 (CH30) (�������)
      C30_uvotu = '��������������'
C SRG_logic.fgi(  65, 566):��������� ���������� CH20 (CH30) (�������)
      C30_axotu = '�� ������'
C SRG_logic.fgi(  65, 568):��������� ���������� CH20 (CH30) (�������)
      I_(49) = z'01000003'
C SRG_logic.fgi( 104, 514):��������� ������������� IN (�������)
      I_(50) = z'01000009'
C SRG_logic.fgi( 104, 512):��������� ������������� IN (�������)
      L_(483)=.true.
C SRG_logic.fgi(  45, 543):��������� ���������� (�������)
      if(L0_ibutu) then
         L_(480)=L_(483)
      else
         L_(480)=.false.
      endif
C SRG_logic.fgi(  49, 542):���� � ������������� �������
      L_(3)=L_(480)
C SRG_logic.fgi(  49, 542):������-�������: ���������� ��� �������������� ������
      L_(484)=.true.
C SRG_logic.fgi(  45, 558):��������� ���������� (�������)
      if(L0_obutu) then
         L_(481)=L_(484)
      else
         L_(481)=.false.
      endif
C SRG_logic.fgi(  49, 557):���� � ������������� �������
      L_(2)=L_(481)
C SRG_logic.fgi(  49, 557):������-�������: ���������� ��� �������������� ������
      L_(478) = L_(2).OR.L_(3).OR.L_(4)
C SRG_logic.fgi(  59, 574):���
      L_(485)=.true.
C SRG_logic.fgi(  45, 578):��������� ���������� (�������)
      if(L0_ubutu) then
         L_(482)=L_(485)
      else
         L_(482)=.false.
      endif
C SRG_logic.fgi(  49, 577):���� � ������������� �������
      L_(1)=L_(482)
C SRG_logic.fgi(  49, 577):������-�������: ���������� ��� �������������� ������
      L_(473) = L_(1).OR.L_(2).OR.L_(4)
C SRG_logic.fgi(  59, 539):���
      L_uxotu=(L_(3).or.L_uxotu).and..not.(L_(473))
      L_(474)=.not.L_uxotu
C SRG_logic.fgi(  67, 541):RS �������
      L_esotu=L_uxotu
C SRG_logic.fgi(  83, 543):������,SRG1_tech_mode
      if(L_esotu) then
         I_urotu=I_(46)
      else
         I_urotu=I_(45)
      endif
C SRG_logic.fgi( 105, 501):���� RE IN LO CH7
      L_(471) = L_(1).OR.L_(3).OR.L_(2)
C SRG_logic.fgi(  59, 524):���
      L_arotu=(L_(4).or.L_arotu).and..not.(L_(471))
      L_(472)=.not.L_arotu
C SRG_logic.fgi(  67, 526):RS �������
      L_upotu=L_arotu
C SRG_logic.fgi(  83, 528):������,SRG1_avt_mode
      if(L_upotu.and..not.L0_apotu) then
         R0_imotu=R0_omotu
      else
         R0_imotu=max(R_(207)-deltat,0.0)
      endif
      L_umotu=R0_imotu.gt.0.0
      L0_apotu=L_upotu
C SRG_logic.fgi(  53, 457):������������  �� T
      L_emotu=L_umotu
C SRG_logic.fgi(  80, 453):������,20SRG10AA100YA21
      !{
      Call KLAPAN_MAN(deltat,REAL(R_ovede,4),R8_isede,I_ubide
     &,C8_etede,I_edide,R_avede,
     & R_utede,R_ipede,REAL(R_upede,4),R_orede,
     & REAL(R_asede,4),R_arede,REAL(R_irede,4),I_ibide,
     & I_idide,I_adide,I_ebide,L_esede,L_udide,L_ufide,L_urede
     &,
     & L_erede,REAL(R_itede,4),L_usede,L_osede,L_efide,
     & L_opede,L_otede,L_ikide,L_evede,L_ivede,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(122),L_umede,L_(123),L_emotu
     &,L_afide,I_odide,L_akide,
     & R_abide,REAL(R_atede,4),L_ekide,L_apede,L_okide,L_epede
     &,L_uvede,L_axede,
     & L_exede,L_oxede,L_uxede,L_ixede)
      !}

      if(L_uxede.or.L_oxede.or.L_ixede.or.L_exede.or.L_axede.or.L_uvede
     &) then      
                  I_obide = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_obide = z'40000000'
      endif
C SRG_vlv.fgi( 227, 749):���� ���������� ��������,20SRG10AA100
      L_ositu=L_umotu
C SRG_logic.fgi(  80, 449):������,20SRG10AA201YA21
      L_asitu=L_umotu
C SRG_logic.fgi(  80, 435):������,20SRG10AA202YA21
      L_iritu=L_umotu
C SRG_logic.fgi(  80, 421):������,20SRG10AA203YA21
      L_upitu=L_umotu
C SRG_logic.fgi(  80, 407):������,20SRG10AA204YA21
      L_epitu=L_umotu
C SRG_logic.fgi(  80, 393):������,20SRG10AA205YA21
      L_omitu=L_umotu
C SRG_logic.fgi(  80, 379):������,20SRG10AA206YA21
      L_amitu=L_umotu
C SRG_logic.fgi(  80, 365):������,20SRG10AA207YA21
      L_ilitu=L_umotu
C SRG_logic.fgi(  80, 351):������,20SRG10AA208YA21
      L_ukitu=L_umotu
C SRG_logic.fgi(  80, 337):������,20SRG10AA209YA21
      L_okitu = L_(460).OR.L_umotu
C SRG_logic.fgi(  68, 332):���
      !{
      Call KLAPAN_MAN(deltat,REAL(R_afev,4),R8_uxav,I_elev
     &,C8_obev,I_olev,R_idev,
     & R_edev,R_utav,REAL(R_evav,4),R_axav,
     & REAL(R_ixav,4),R_ivav,REAL(R_uvav,4),I_ukev,
     & I_ulev,I_ilev,I_okev,L_oxav,L_emev,L_epev,L_exav,
     & L_ovav,REAL(R_ubev,4),L_ebev,L_abev,L_omev,
     & L_avav,L_adev,L_upev,L_odev,L_udev,REAL(R8_elomu,8
     &),
     & REAL(1.0,4),R8_aromu,L_(84),L_okitu,L_(85),L_exitu
     &,L_imev,I_amev,L_ipev,
     & R_ikev,REAL(R_ibev,4),L_opev,L_itav,L_arev,L_otav,L_efev
     &,L_ifev,
     & L_ofev,L_akev,L_ekev,L_ufev)
      !}

      if(L_ekev.or.L_akev.or.L_ufev.or.L_ofev.or.L_ifev.or.L_efev
     &) then      
                  I_alev = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_alev = z'40000000'
      endif
C SRG_vlv.fgi( 493, 749):���� ���������� ��������,20SRG10AA119
      L_ekitu=L_umotu
C SRG_logic.fgi(  80, 323):������,20SRG10AA210YA21
      L_akitu = L_(459).OR.L_umotu
C SRG_logic.fgi(  68, 318):���
      !{
      Call KLAPAN_MAN(deltat,REAL(R_eput,4),R8_alut,I_isut
     &,C8_ulut,I_usut,R_omut,
     & R_imut,R_afut,REAL(R_ifut,4),R_ekut,
     & REAL(R_okut,4),R_ofut,REAL(R_akut,4),I_asut,
     & I_atut,I_osut,I_urut,L_ukut,L_itut,L_ivut,L_ikut,
     & L_ufut,REAL(R_amut,4),L_ilut,L_elut,L_utut,
     & L_efut,L_emut,L_axut,L_umut,L_aput,REAL(R8_elomu,8
     &),
     & REAL(1.0,4),R8_aromu,L_(80),L_akitu,L_(81),L_ovitu
     &,L_otut,I_etut,L_ovut,
     & R_orut,REAL(R_olut,4),L_uvut,L_odut,L_exut,L_udut,L_iput
     &,L_oput,
     & L_uput,L_erut,L_irut,L_arut)
      !}

      if(L_irut.or.L_erut.or.L_arut.or.L_uput.or.L_oput.or.L_iput
     &) then      
                  I_esut = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_esut = z'40000000'
      endif
C SRG_vlv.fgi( 521, 749):���� ���������� ��������,20SRG10AA121
      L_ofitu=L_umotu
C SRG_logic.fgi(  80, 309):������,20SRG10AA211YA21
      L_ifitu = L_(458).OR.L_umotu
C SRG_logic.fgi(  68, 304):���
      !{
      Call KLAPAN_MAN(deltat,REAL(R_ivit,4),R8_esit,I_obot
     &,C8_atit,I_adot,R_utit,
     & R_otit,R_epit,REAL(R_opit,4),R_irit,
     & REAL(R_urit,4),R_upit,REAL(R_erit,4),I_ebot,
     & I_edot,I_ubot,I_abot,L_asit,L_odot,L_ofot,L_orit,
     & L_arit,REAL(R_etit,4),L_osit,L_isit,L_afot,
     & L_ipit,L_itit,L_ekot,L_avit,L_evit,REAL(R8_elomu,8
     &),
     & REAL(1.0,4),R8_aromu,L_(76),L_ifitu,L_(77),L_avitu
     &,L_udot,I_idot,L_ufot,
     & R_uxit,REAL(R_usit,4),L_akot,L_umit,L_ikot,L_apit,L_ovit
     &,L_uvit,
     & L_axit,L_ixit,L_oxit,L_exit)
      !}

      if(L_oxit.or.L_ixit.or.L_exit.or.L_axit.or.L_uvit.or.L_ovit
     &) then      
                  I_ibot = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_ibot = z'40000000'
      endif
C SRG_vlv.fgi( 227, 723):���� ���������� ��������,20SRG10AA123
      L_afitu=L_umotu
C SRG_logic.fgi(  80, 295):������,20SRG10AA212YA21
      L_uditu = L_(457).OR.L_umotu
C SRG_logic.fgi(  68, 290):���
      !{
      Call KLAPAN_MAN(deltat,REAL(R_ofet,4),R8_ibet,I_ulet
     &,C8_edet,I_emet,R_afet,
     & R_udet,R_ivat,REAL(R_uvat,4),R_oxat,
     & REAL(R_abet,4),R_axat,REAL(R_ixat,4),I_ilet,
     & I_imet,I_amet,I_elet,L_ebet,L_umet,L_upet,L_uxat,
     & L_exat,REAL(R_idet,4),L_ubet,L_obet,L_epet,
     & L_ovat,L_odet,L_iret,L_efet,L_ifet,REAL(R8_elomu,8
     &),
     & REAL(1.0,4),R8_aromu,L_(72),L_uditu,L_(73),L_ititu
     &,L_apet,I_omet,L_aret,
     & R_alet,REAL(R_adet,4),L_eret,L_avat,L_oret,L_evat,L_ufet
     &,L_aket,
     & L_eket,L_oket,L_uket,L_iket)
      !}

      if(L_uket.or.L_oket.or.L_iket.or.L_eket.or.L_aket.or.L_ufet
     &) then      
                  I_olet = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_olet = z'40000000'
      endif
C SRG_vlv.fgi( 255, 723):���� ���������� ��������,20SRG10AA125
      L_iditu=L_umotu
C SRG_logic.fgi(  80, 281):������,20SRG10AA213YA21
      L_editu = L_(456).OR.L_umotu
C SRG_logic.fgi(  68, 276):���
      !{
      Call KLAPAN_MAN(deltat,REAL(R_osos,4),R8_ipos,I_uvos
     &,C8_eros,I_exos,R_asos,
     & R_uros,R_ilos,REAL(R_ulos,4),R_omos,
     & REAL(R_apos,4),R_amos,REAL(R_imos,4),I_ivos,
     & I_ixos,I_axos,I_evos,L_epos,L_uxos,L_ubus,L_umos,
     & L_emos,REAL(R_iros,4),L_upos,L_opos,L_ebus,
     & L_olos,L_oros,L_idus,L_esos,L_isos,REAL(R8_elomu,8
     &),
     & REAL(1.0,4),R8_aromu,L_(66),L_editu,L_(67),L_usitu
     &,L_abus,I_oxos,L_adus,
     & R_avos,REAL(R_aros,4),L_edus,L_alos,L_odus,L_elos,L_usos
     &,L_atos,
     & L_etos,L_otos,L_utos,L_itos)
      !}

      if(L_utos.or.L_otos.or.L_itos.or.L_etos.or.L_atos.or.L_usos
     &) then      
                  I_ovos = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_ovos = z'40000000'
      endif
C SRG_vlv.fgi( 283, 723):���� ���������� ��������,20SRG10AA127
      L_aditu=L_umotu
C SRG_logic.fgi(  80, 271):������,20SRG10AA126YA21
      !{
      Call KLAPAN_MAN(deltat,REAL(R_opus,4),R8_ilus,I_usus
     &,C8_emus,I_etus,R_apus,
     & R_umus,R_ifus,REAL(R_ufus,4),R_okus,
     & REAL(R_alus,4),R_akus,REAL(R_ikus,4),I_isus,
     & I_itus,I_atus,I_esus,L_elus,L_utus,L_uvus,L_ukus,
     & L_ekus,REAL(R_imus,4),L_ulus,L_olus,L_evus,
     & L_ofus,L_omus,L_ixus,L_epus,L_ipus,REAL(R8_elomu,8
     &),
     & REAL(1.0,4),R8_aromu,L_(68),L_udus,L_(69),L_aditu,L_avus
     &,I_otus,L_axus,
     & R_asus,REAL(R_amus,4),L_exus,L_afus,L_oxus,L_efus,L_upus
     &,L_arus,
     & L_erus,L_orus,L_urus,L_irus)
      !}

      if(L_urus.or.L_orus.or.L_irus.or.L_erus.or.L_arus.or.L_upus
     &) then      
                  I_osus = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_osus = z'40000000'
      endif
C SRG_vlv.fgi( 269, 723):���� ���������� ��������,20SRG10AA126
      L_oditu=L_umotu
C SRG_logic.fgi(  80, 285):������,20SRG10AA124YA21
      !{
      Call KLAPAN_MAN(deltat,REAL(R_obit,4),R8_ivet,I_ufit
     &,C8_exet,I_ekit,R_abit,
     & R_uxet,R_iset,REAL(R_uset,4),R_otet,
     & REAL(R_avet,4),R_atet,REAL(R_itet,4),I_ifit,
     & I_ikit,I_akit,I_efit,L_evet,L_ukit,L_ulit,L_utet,
     & L_etet,REAL(R_ixet,4),L_uvet,L_ovet,L_elit,
     & L_oset,L_oxet,L_imit,L_ebit,L_ibit,REAL(R8_elomu,8
     &),
     & REAL(1.0,4),R8_aromu,L_(74),L_uret,L_(75),L_oditu,L_alit
     &,I_okit,L_amit,
     & R_afit,REAL(R_axet,4),L_emit,L_aset,L_omit,L_eset,L_ubit
     &,L_adit,
     & L_edit,L_odit,L_udit,L_idit)
      !}

      if(L_udit.or.L_odit.or.L_idit.or.L_edit.or.L_adit.or.L_ubit
     &) then      
                  I_ofit = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_ofit = z'40000000'
      endif
C SRG_vlv.fgi( 241, 723):���� ���������� ��������,20SRG10AA124
      L_efitu=L_umotu
C SRG_logic.fgi(  80, 299):������,20SRG10AA122YA21
      !{
      Call KLAPAN_MAN(deltat,REAL(R_isot,4),R8_epot,I_ovot
     &,C8_arot,I_axot,R_urot,
     & R_orot,R_elot,REAL(R_olot,4),R_imot,
     & REAL(R_umot,4),R_ulot,REAL(R_emot,4),I_evot,
     & I_exot,I_uvot,I_avot,L_apot,L_oxot,L_obut,L_omot,
     & L_amot,REAL(R_erot,4),L_opot,L_ipot,L_abut,
     & L_ilot,L_irot,L_edut,L_asot,L_esot,REAL(R8_elomu,8
     &),
     & REAL(1.0,4),R8_aromu,L_(78),L_okot,L_(79),L_efitu,L_uxot
     &,I_ixot,L_ubut,
     & R_utot,REAL(R_upot,4),L_adut,L_ukot,L_idut,L_alot,L_osot
     &,L_usot,
     & L_atot,L_itot,L_otot,L_etot)
      !}

      if(L_otot.or.L_itot.or.L_etot.or.L_atot.or.L_usot.or.L_osot
     &) then      
                  I_ivot = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_ivot = z'40000000'
      endif
C SRG_vlv.fgi( 535, 749):���� ���������� ��������,20SRG10AA122
      L_ufitu=L_umotu
C SRG_logic.fgi(  80, 313):������,20SRG10AA120YA21
      !{
      Call KLAPAN_MAN(deltat,REAL(R_elav,4),R8_afav,I_ipav
     &,C8_ufav,I_upav,R_okav,
     & R_ikav,R_abav,REAL(R_ibav,4),R_edav,
     & REAL(R_odav,4),R_obav,REAL(R_adav,4),I_apav,
     & I_arav,I_opav,I_umav,L_udav,L_irav,L_isav,L_idav,
     & L_ubav,REAL(R_akav,4),L_ifav,L_efav,L_urav,
     & L_ebav,L_ekav,L_atav,L_ukav,L_alav,REAL(R8_elomu,8
     &),
     & REAL(1.0,4),R8_aromu,L_(82),L_ixut,L_(83),L_ufitu,L_orav
     &,I_erav,L_osav,
     & R_omav,REAL(R_ofav,4),L_usav,L_oxut,L_etav,L_uxut,L_ilav
     &,L_olav,
     & L_ulav,L_emav,L_imav,L_amav)
      !}

      if(L_imav.or.L_emav.or.L_amav.or.L_ulav.or.L_olav.or.L_ilav
     &) then      
                  I_epav = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_epav = z'40000000'
      endif
C SRG_vlv.fgi( 507, 749):���� ���������� ��������,20SRG10AA120
      L_ikitu=L_umotu
C SRG_logic.fgi(  80, 327):������,20SRG10AA118YA21
      !{
      Call KLAPAN_MAN(deltat,REAL(R_abiv,4),R8_utev,I_efiv
     &,C8_ovev,I_ofiv,R_ixev,
     & R_exev,R_urev,REAL(R_esev,4),R_atev,
     & REAL(R_itev,4),R_isev,REAL(R_usev,4),I_udiv,
     & I_ufiv,I_ifiv,I_odiv,L_otev,L_ekiv,L_eliv,L_etev,
     & L_osev,REAL(R_uvev,4),L_evev,L_avev,L_okiv,
     & L_asev,L_axev,L_uliv,L_oxev,L_uxev,REAL(R8_elomu,8
     &),
     & REAL(1.0,4),R8_aromu,L_(86),L_erev,L_(87),L_ikitu,L_ikiv
     &,I_akiv,L_iliv,
     & R_idiv,REAL(R_ivev,4),L_oliv,L_irev,L_amiv,L_orev,L_ebiv
     &,L_ibiv,
     & L_obiv,L_adiv,L_ediv,L_ubiv)
      !}

      if(L_ediv.or.L_adiv.or.L_ubiv.or.L_obiv.or.L_ibiv.or.L_ebiv
     &) then      
                  I_afiv = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_afiv = z'40000000'
      endif
C SRG_vlv.fgi( 479, 749):���� ���������� ��������,20SRG10AA118
      L_alitu=L_umotu
C SRG_logic.fgi(  80, 341):������,20SRG10AA116YA21
      !{
      Call KLAPAN_MAN(deltat,REAL(R_urov,4),R8_omov,I_avov
     &,C8_ipov,I_ivov,R_erov,
     & R_arov,R_okov,REAL(R_alov,4),R_ulov,
     & REAL(R_emov,4),R_elov,REAL(R_olov,4),I_otov,
     & I_ovov,I_evov,I_itov,L_imov,L_axov,L_abuv,L_amov,
     & L_ilov,REAL(R_opov,4),L_apov,L_umov,L_ixov,
     & L_ukov,L_upov,L_obuv,L_irov,L_orov,REAL(R8_elomu,8
     &),
     & REAL(1.0,4),R8_aromu,L_(90),L_akov,L_(91),L_alitu,L_exov
     &,I_uvov,L_ebuv,
     & R_etov,REAL(R_epov,4),L_ibuv,L_ekov,L_ubuv,L_ikov,L_asov
     &,L_esov,
     & L_isov,L_usov,L_atov,L_osov)
      !}

      if(L_atov.or.L_usov.or.L_osov.or.L_isov.or.L_esov.or.L_asov
     &) then      
                  I_utov = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_utov = z'40000000'
      endif
C SRG_vlv.fgi( 451, 749):���� ���������� ��������,20SRG10AA116
      L_elitu = L_(461).OR.L_umotu
C SRG_logic.fgi(  68, 346):���
      !{
      Call KLAPAN_MAN(deltat,REAL(R_utiv,4),R8_oriv,I_abov
     &,C8_isiv,I_ibov,R_etiv,
     & R_ativ,R_omiv,REAL(R_apiv,4),R_upiv,
     & REAL(R_eriv,4),R_epiv,REAL(R_opiv,4),I_oxiv,
     & I_obov,I_ebov,I_ixiv,L_iriv,L_adov,L_afov,L_ariv,
     & L_ipiv,REAL(R_osiv,4),L_asiv,L_uriv,L_idov,
     & L_umiv,L_usiv,L_ofov,L_itiv,L_otiv,REAL(R8_elomu,8
     &),
     & REAL(1.0,4),R8_aromu,L_(88),L_elitu,L_(89),L_uxitu
     &,L_edov,I_ubov,L_efov,
     & R_exiv,REAL(R_esiv,4),L_ifov,L_emiv,L_ufov,L_imiv,L_aviv
     &,L_eviv,
     & L_iviv,L_uviv,L_axiv,L_oviv)
      !}

      if(L_axiv.or.L_uviv.or.L_oviv.or.L_iviv.or.L_eviv.or.L_aviv
     &) then      
                  I_uxiv = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_uxiv = z'40000000'
      endif
C SRG_vlv.fgi( 465, 749):���� ���������� ��������,20SRG10AA117
      L_olitu=L_umotu
C SRG_logic.fgi(  80, 355):������,20SRG10AA114YA21
      !{
      Call KLAPAN_MAN(deltat,REAL(R_okax,4),R8_idax,I_umax
     &,C8_efax,I_epax,R_akax,
     & R_ufax,R_ixuv,REAL(R_uxuv,4),R_obax,
     & REAL(R_adax,4),R_abax,REAL(R_ibax,4),I_imax,
     & I_ipax,I_apax,I_emax,L_edax,L_upax,L_urax,L_ubax,
     & L_ebax,REAL(R_ifax,4),L_udax,L_odax,L_erax,
     & L_oxuv,L_ofax,L_isax,L_ekax,L_ikax,REAL(R8_elomu,8
     &),
     & REAL(1.0,4),R8_aromu,L_(94),L_uvuv,L_(95),L_olitu,L_arax
     &,I_opax,L_asax,
     & R_amax,REAL(R_afax,4),L_esax,L_axuv,L_osax,L_exuv,L_ukax
     &,L_alax,
     & L_elax,L_olax,L_ulax,L_ilax)
      !}

      if(L_ulax.or.L_olax.or.L_ilax.or.L_elax.or.L_alax.or.L_ukax
     &) then      
                  I_omax = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_omax = z'40000000'
      endif
C SRG_vlv.fgi( 423, 749):���� ���������� ��������,20SRG10AA114
      L_ulitu = L_(462).OR.L_umotu
C SRG_logic.fgi(  68, 360):���
      !{
      Call KLAPAN_MAN(deltat,REAL(R_omuv,4),R8_ikuv,I_uruv
     &,C8_eluv,I_esuv,R_amuv,
     & R_uluv,R_iduv,REAL(R_uduv,4),R_ofuv,
     & REAL(R_akuv,4),R_afuv,REAL(R_ifuv,4),I_iruv,
     & I_isuv,I_asuv,I_eruv,L_ekuv,L_usuv,L_utuv,L_ufuv,
     & L_efuv,REAL(R_iluv,4),L_ukuv,L_okuv,L_etuv,
     & L_oduv,L_oluv,L_ivuv,L_emuv,L_imuv,REAL(R8_elomu,8
     &),
     & REAL(1.0,4),R8_aromu,L_(92),L_ulitu,L_(93),L_ibotu
     &,L_atuv,I_osuv,L_avuv,
     & R_aruv,REAL(R_aluv,4),L_evuv,L_aduv,L_ovuv,L_eduv,L_umuv
     &,L_apuv,
     & L_epuv,L_opuv,L_upuv,L_ipuv)
      !}

      if(L_upuv.or.L_opuv.or.L_ipuv.or.L_epuv.or.L_apuv.or.L_umuv
     &) then      
                  I_oruv = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_oruv = z'40000000'
      endif
C SRG_vlv.fgi( 437, 749):���� ���������� ��������,20SRG10AA115
      L_emitu=L_umotu
C SRG_logic.fgi(  80, 369):������,20SRG10AA112YA21
      !{
      Call KLAPAN_MAN(deltat,REAL(R_ixex,4),R8_etex,I_odix
     &,C8_avex,I_afix,R_uvex,
     & R_ovex,R_erex,REAL(R_orex,4),R_isex,
     & REAL(R_usex,4),R_urex,REAL(R_esex,4),I_edix,
     & I_efix,I_udix,I_adix,L_atex,L_ofix,L_okix,L_osex,
     & L_asex,REAL(R_evex,4),L_otex,L_itex,L_akix,
     & L_irex,L_ivex,L_elix,L_axex,L_exex,REAL(R8_elomu,8
     &),
     & REAL(1.0,4),R8_aromu,L_(98),L_opex,L_(99),L_emitu,L_ufix
     &,I_ifix,L_ukix,
     & R_ubix,REAL(R_utex,4),L_alix,L_upex,L_ilix,L_arex,L_oxex
     &,L_uxex,
     & L_abix,L_ibix,L_obix,L_ebix)
      !}

      if(L_obix.or.L_ibix.or.L_ebix.or.L_abix.or.L_uxex.or.L_oxex
     &) then      
                  I_idix = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_idix = z'40000000'
      endif
C SRG_vlv.fgi( 395, 749):���� ���������� ��������,20SRG10AA112
      L_imitu = L_(463).OR.L_umotu
C SRG_logic.fgi(  68, 374):���
      !{
      Call KLAPAN_MAN(deltat,REAL(R_idex,4),R8_exax,I_okex
     &,C8_abex,I_alex,R_ubex,
     & R_obex,R_etax,REAL(R_otax,4),R_ivax,
     & REAL(R_uvax,4),R_utax,REAL(R_evax,4),I_ekex,
     & I_elex,I_ukex,I_akex,L_axax,L_olex,L_omex,L_ovax,
     & L_avax,REAL(R_ebex,4),L_oxax,L_ixax,L_amex,
     & L_itax,L_ibex,L_epex,L_adex,L_edex,REAL(R8_elomu,8
     &),
     & REAL(1.0,4),R8_aromu,L_(96),L_imitu,L_(97),L_adotu
     &,L_ulex,I_ilex,L_umex,
     & R_ufex,REAL(R_uxax,4),L_apex,L_usax,L_ipex,L_atax,L_odex
     &,L_udex,
     & L_afex,L_ifex,L_ofex,L_efex)
      !}

      if(L_ofex.or.L_ifex.or.L_efex.or.L_afex.or.L_udex.or.L_odex
     &) then      
                  I_ikex = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_ikex = z'40000000'
      endif
C SRG_vlv.fgi( 409, 749):���� ���������� ��������,20SRG10AA113
      L_umitu=L_umotu
C SRG_logic.fgi(  80, 383):������,20SRG10AA110YA21
      !{
      Call KLAPAN_MAN(deltat,REAL(R_erox,4),R8_amox,I_itox
     &,C8_umox,I_utox,R_opox,
     & R_ipox,R_akox,REAL(R_ikox,4),R_elox,
     & REAL(R_olox,4),R_okox,REAL(R_alox,4),I_atox,
     & I_avox,I_otox,I_usox,L_ulox,L_ivox,L_ixox,L_ilox,
     & L_ukox,REAL(R_apox,4),L_imox,L_emox,L_uvox,
     & L_ekox,L_epox,L_abux,L_upox,L_arox,REAL(R8_elomu,8
     &),
     & REAL(1.0,4),R8_aromu,L_(102),L_ifox,L_(103),L_umitu
     &,L_ovox,I_evox,L_oxox,
     & R_osox,REAL(R_omox,4),L_uxox,L_ofox,L_ebux,L_ufox,L_irox
     &,L_orox,
     & L_urox,L_esox,L_isox,L_asox)
      !}

      if(L_isox.or.L_esox.or.L_asox.or.L_urox.or.L_orox.or.L_irox
     &) then      
                  I_etox = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_etox = z'40000000'
      endif
C SRG_vlv.fgi( 367, 749):���� ���������� ��������,20SRG10AA110
      L_apitu = L_(464).OR.L_umotu
C SRG_logic.fgi(  68, 388):���
      !{
      Call KLAPAN_MAN(deltat,REAL(R_etix,4),R8_arix,I_ixix
     &,C8_urix,I_uxix,R_osix,
     & R_isix,R_amix,REAL(R_imix,4),R_epix,
     & REAL(R_opix,4),R_omix,REAL(R_apix,4),I_axix,
     & I_abox,I_oxix,I_uvix,L_upix,L_ibox,L_idox,L_ipix,
     & L_umix,REAL(R_asix,4),L_irix,L_erix,L_ubox,
     & L_emix,L_esix,L_afox,L_usix,L_atix,REAL(R8_elomu,8
     &),
     & REAL(1.0,4),R8_aromu,L_(100),L_apitu,L_(101),L_odotu
     &,L_obox,I_ebox,L_odox,
     & R_ovix,REAL(R_orix,4),L_udox,L_olix,L_efox,L_ulix,L_itix
     &,L_otix,
     & L_utix,L_evix,L_ivix,L_avix)
      !}

      if(L_ivix.or.L_evix.or.L_avix.or.L_utix.or.L_otix.or.L_itix
     &) then      
                  I_exix = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_exix = z'40000000'
      endif
C SRG_vlv.fgi( 381, 749):���� ���������� ��������,20SRG10AA111
      L_ipitu=L_umotu
C SRG_logic.fgi(  80, 397):������,20SRG10AA108YA21
      !{
      Call KLAPAN_MAN(deltat,REAL(R_akabe,4),R8_ubabe,I_emabe
     &,C8_odabe,I_omabe,R_ifabe,
     & R_efabe,R_uvux,REAL(R_exux,4),R_ababe,
     & REAL(R_ibabe,4),R_ixux,REAL(R_uxux,4),I_ulabe,
     & I_umabe,I_imabe,I_olabe,L_obabe,L_epabe,L_erabe,L_ebabe
     &,
     & L_oxux,REAL(R_udabe,4),L_edabe,L_adabe,L_opabe,
     & L_axux,L_afabe,L_urabe,L_ofabe,L_ufabe,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(106),L_evux,L_(107),L_ipitu
     &,L_ipabe,I_apabe,L_irabe,
     & R_ilabe,REAL(R_idabe,4),L_orabe,L_ivux,L_asabe,L_ovux
     &,L_ekabe,L_ikabe,
     & L_okabe,L_alabe,L_elabe,L_ukabe)
      !}

      if(L_elabe.or.L_alabe.or.L_ukabe.or.L_okabe.or.L_ikabe.or.L_ekabe
     &) then      
                  I_amabe = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_amabe = z'40000000'
      endif
C SRG_vlv.fgi( 339, 749):���� ���������� ��������,20SRG10AA108
      L_opitu = L_(465).OR.L_umotu
C SRG_logic.fgi(  68, 402):���
      !{
      Call KLAPAN_MAN(deltat,REAL(R_amux,4),R8_ufux,I_erux
     &,C8_okux,I_orux,R_ilux,
     & R_elux,R_ubux,REAL(R_edux,4),R_afux,
     & REAL(R_ifux,4),R_idux,REAL(R_udux,4),I_upux,
     & I_urux,I_irux,I_opux,L_ofux,L_esux,L_etux,L_efux,
     & L_odux,REAL(R_ukux,4),L_ekux,L_akux,L_osux,
     & L_adux,L_alux,L_utux,L_olux,L_ulux,REAL(R8_elomu,8
     &),
     & REAL(1.0,4),R8_aromu,L_(104),L_opitu,L_(105),L_efotu
     &,L_isux,I_asux,L_itux,
     & R_ipux,REAL(R_ikux,4),L_otux,L_ibux,L_avux,L_obux,L_emux
     &,L_imux,
     & L_omux,L_apux,L_epux,L_umux)
      !}

      if(L_epux.or.L_apux.or.L_umux.or.L_omux.or.L_imux.or.L_emux
     &) then      
                  I_arux = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_arux = z'40000000'
      endif
C SRG_vlv.fgi( 353, 749):���� ���������� ��������,20SRG10AA109
      L_aritu=L_umotu
C SRG_logic.fgi(  80, 411):������,20SRG10AA106YA21
      !{
      Call KLAPAN_MAN(deltat,REAL(R_uvebe,4),R8_osebe,I_adibe
     &,C8_itebe,I_idibe,R_evebe,
     & R_avebe,R_opebe,REAL(R_arebe,4),R_urebe,
     & REAL(R_esebe,4),R_erebe,REAL(R_orebe,4),I_obibe,
     & I_odibe,I_edibe,I_ibibe,L_isebe,L_afibe,L_akibe,L_asebe
     &,
     & L_irebe,REAL(R_otebe,4),L_atebe,L_usebe,L_ifibe,
     & L_upebe,L_utebe,L_okibe,L_ivebe,L_ovebe,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(110),L_apebe,L_(111),L_aritu
     &,L_efibe,I_udibe,L_ekibe,
     & R_ebibe,REAL(R_etebe,4),L_ikibe,L_epebe,L_ukibe,L_ipebe
     &,L_axebe,L_exebe,
     & L_ixebe,L_uxebe,L_abibe,L_oxebe)
      !}

      if(L_abibe.or.L_uxebe.or.L_oxebe.or.L_ixebe.or.L_exebe.or.L_axebe
     &) then      
                  I_ubibe = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ubibe = z'40000000'
      endif
C SRG_vlv.fgi( 311, 749):���� ���������� ��������,20SRG10AA106
      L_eritu = L_(466).OR.L_umotu
C SRG_logic.fgi(  68, 416):���
      !{
      Call KLAPAN_MAN(deltat,REAL(R_ubebe,4),R8_ovabe,I_akebe
     &,C8_ixabe,I_ikebe,R_ebebe,
     & R_abebe,R_osabe,REAL(R_atabe,4),R_utabe,
     & REAL(R_evabe,4),R_etabe,REAL(R_otabe,4),I_ofebe,
     & I_okebe,I_ekebe,I_ifebe,L_ivabe,L_alebe,L_amebe,L_avabe
     &,
     & L_itabe,REAL(R_oxabe,4),L_axabe,L_uvabe,L_ilebe,
     & L_usabe,L_uxabe,L_omebe,L_ibebe,L_obebe,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(108),L_eritu,L_(109),L_ufotu
     &,L_elebe,I_ukebe,L_emebe,
     & R_efebe,REAL(R_exabe,4),L_imebe,L_esabe,L_umebe,L_isabe
     &,L_adebe,L_edebe,
     & L_idebe,L_udebe,L_afebe,L_odebe)
      !}

      if(L_afebe.or.L_udebe.or.L_odebe.or.L_idebe.or.L_edebe.or.L_adebe
     &) then      
                  I_ufebe = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ufebe = z'40000000'
      endif
C SRG_vlv.fgi( 325, 749):���� ���������� ��������,20SRG10AA107
      L_oritu=L_umotu
C SRG_logic.fgi(  80, 425):������,20SRG10AA104YA21
      !{
      Call KLAPAN_MAN(deltat,REAL(R_opobe,4),R8_ilobe,I_usobe
     &,C8_emobe,I_etobe,R_apobe,
     & R_umobe,R_ifobe,REAL(R_ufobe,4),R_okobe,
     & REAL(R_alobe,4),R_akobe,REAL(R_ikobe,4),I_isobe,
     & I_itobe,I_atobe,I_esobe,L_elobe,L_utobe,L_uvobe,L_ukobe
     &,
     & L_ekobe,REAL(R_imobe,4),L_ulobe,L_olobe,L_evobe,
     & L_ofobe,L_omobe,L_ixobe,L_epobe,L_ipobe,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(114),L_udobe,L_(115),L_oritu
     &,L_avobe,I_otobe,L_axobe,
     & R_asobe,REAL(R_amobe,4),L_exobe,L_afobe,L_oxobe,L_efobe
     &,L_upobe,L_arobe,
     & L_erobe,L_orobe,L_urobe,L_irobe)
      !}

      if(L_urobe.or.L_orobe.or.L_irobe.or.L_erobe.or.L_arobe.or.L_upobe
     &) then      
                  I_osobe = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_osobe = z'40000000'
      endif
C SRG_vlv.fgi( 283, 749):���� ���������� ��������,20SRG10AA104
      L_uritu = L_(467).OR.L_umotu
C SRG_logic.fgi(  68, 430):���
      !{
      Call KLAPAN_MAN(deltat,REAL(R_osibe,4),R8_ipibe,I_uvibe
     &,C8_eribe,I_exibe,R_asibe,
     & R_uribe,R_ilibe,REAL(R_ulibe,4),R_omibe,
     & REAL(R_apibe,4),R_amibe,REAL(R_imibe,4),I_ivibe,
     & I_ixibe,I_axibe,I_evibe,L_epibe,L_uxibe,L_ubobe,L_umibe
     &,
     & L_emibe,REAL(R_iribe,4),L_upibe,L_opibe,L_ebobe,
     & L_olibe,L_oribe,L_idobe,L_esibe,L_isibe,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(112),L_uritu,L_(113),L_ikotu
     &,L_abobe,I_oxibe,L_adobe,
     & R_avibe,REAL(R_aribe,4),L_edobe,L_alibe,L_odobe,L_elibe
     &,L_usibe,L_atibe,
     & L_etibe,L_otibe,L_utibe,L_itibe)
      !}

      if(L_utibe.or.L_otibe.or.L_itibe.or.L_etibe.or.L_atibe.or.L_usibe
     &) then      
                  I_ovibe = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ovibe = z'40000000'
      endif
C SRG_vlv.fgi( 297, 749):���� ���������� ��������,20SRG10AA105
      L_esitu=L_umotu
C SRG_logic.fgi(  80, 439):������,20SRG10AA102YA21
      !{
      Call KLAPAN_MAN(deltat,REAL(R_ifade,4),R8_ebade,I_olade
     &,C8_adade,I_amade,R_udade,
     & R_odade,R_evube,REAL(R_ovube,4),R_ixube,
     & REAL(R_uxube,4),R_uvube,REAL(R_exube,4),I_elade,
     & I_emade,I_ulade,I_alade,L_abade,L_omade,L_opade,L_oxube
     &,
     & L_axube,REAL(R_edade,4),L_obade,L_ibade,L_apade,
     & L_ivube,L_idade,L_erade,L_afade,L_efade,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(118),L_otube,L_(119),L_esitu
     &,L_umade,I_imade,L_upade,
     & R_ukade,REAL(R_ubade,4),L_arade,L_utube,L_irade,L_avube
     &,L_ofade,L_ufade,
     & L_akade,L_ikade,L_okade,L_ekade)
      !}

      if(L_okade.or.L_ikade.or.L_ekade.or.L_akade.or.L_ufade.or.L_ofade
     &) then      
                  I_ilade = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ilade = z'40000000'
      endif
C SRG_vlv.fgi( 255, 749):���� ���������� ��������,20SRG10AA102
      L_isitu = L_(468).OR.L_umotu
C SRG_logic.fgi(  68, 444):���
      !{
      Call KLAPAN_MAN(deltat,REAL(R_ilube,4),R8_efube,I_opube
     &,C8_akube,I_arube,R_ukube,
     & R_okube,R_ebube,REAL(R_obube,4),R_idube,
     & REAL(R_udube,4),R_ubube,REAL(R_edube,4),I_epube,
     & I_erube,I_upube,I_apube,L_afube,L_orube,L_osube,L_odube
     &,
     & L_adube,REAL(R_ekube,4),L_ofube,L_ifube,L_asube,
     & L_ibube,L_ikube,L_etube,L_alube,L_elube,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(116),L_isitu,L_(117),L_alotu
     &,L_urube,I_irube,L_usube,
     & R_umube,REAL(R_ufube,4),L_atube,L_uxobe,L_itube,L_abube
     &,L_olube,L_ulube,
     & L_amube,L_imube,L_omube,L_emube)
      !}

      if(L_omube.or.L_imube.or.L_emube.or.L_amube.or.L_ulube.or.L_olube
     &) then      
                  I_ipube = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ipube = z'40000000'
      endif
C SRG_vlv.fgi( 269, 749):���� ���������� ��������,20SRG10AA103
      L_epotu = L_(469).OR.L_umotu
C SRG_logic.fgi(  68, 458):���
      !{
      Call KLAPAN_MAN(deltat,REAL(R_iside,4),R8_epide,I_ovide
     &,C8_aride,I_axide,R_uride,
     & R_oride,R_elide,REAL(R_olide,4),R_imide,
     & REAL(R_umide,4),R_ulide,REAL(R_emide,4),I_evide,
     & I_exide,I_uvide,I_avide,L_apide,L_oxide,L_obode,L_omide
     &,
     & L_amide,REAL(R_eride,4),L_opide,L_ipide,L_abode,
     & L_ilide,L_iride,L_edode,L_aside,L_eside,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(124),L_epotu,L_(125),L_olotu
     &,L_uxide,I_ixide,L_ubode,
     & R_utide,REAL(R_upide,4),L_adode,L_ukide,L_idode,L_alide
     &,L_oside,L_uside,
     & L_atide,L_itide,L_otide,L_etide)
      !}

      if(L_otide.or.L_itide.or.L_etide.or.L_atide.or.L_uside.or.L_oside
     &) then      
                  I_ivide = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ivide = z'40000000'
      endif
C SRG_vlv.fgi( 241, 749):���� ���������� ��������,20SRG10AA101
      if(L_upotu) then
         I_ipotu=I_(44)
      else
         I_ipotu=I_(43)
      endif
C SRG_logic.fgi(  69, 512):���� RE IN LO CH7
      L_(475) = L_(1).OR.L_(3).OR.L_(4)
C SRG_logic.fgi(  59, 554):���
      L_abutu=(L_(2).or.L_abutu).and..not.(L_(475))
      L_(476)=.not.L_abutu
C SRG_logic.fgi(  67, 556):RS �������
      L_exotu=L_abutu
C SRG_logic.fgi(  83, 558):������,SRG1_ruch_mode
      if(L_exotu) then
         I_oxotu=I_(50)
      else
         I_oxotu=I_(49)
      endif
C SRG_logic.fgi( 107, 512):���� RE IN LO CH7
      L_ebutu=(L_(1).or.L_ebutu).and..not.(L_(478))
      L_(479)=.not.L_ebutu
C SRG_logic.fgi(  67, 576):RS �������
      L_isotu=L_ebutu
C SRG_logic.fgi(  85, 578):������,SRG1_automatic_mode
      if(L_isotu) then
         I_asotu=I_(48)
      else
         I_asotu=I_(47)
      endif
C SRG_logic.fgi(  65, 501):���� RE IN LO CH7
      if(L_ebutu) then
         C30_ovotu=C30_uvotu
      else
         C30_ovotu=C30_axotu
      endif
C SRG_logic.fgi(  69, 566):���� RE IN LO CH20
      if(L_abutu) then
         C30_ivotu=C30_evotu
      else
         C30_ivotu=C30_ovotu
      endif
C SRG_logic.fgi(  84, 565):���� RE IN LO CH20
      if(L_uxotu) then
         C30_utotu=C30_avotu
      else
         C30_utotu=C30_ivotu
      endif
C SRG_logic.fgi(  93, 564):���� RE IN LO CH20
      if(L_arotu) then
         C30_ixotu=C30_opotu
      else
         C30_ixotu=C30_utotu
      endif
C SRG_logic.fgi( 103, 563):���� RE IN LO CH20
      L_(449) = L_uforu.AND.L_oforu.AND.L_iforu
C SRG_logic.fgi( 232, 366):�
C label 2903  try2903=try2903-1
      L0_ovoru=(L_(455).or.L0_ovoru).and..not.(L_ivoru)
      L_(454)=.not.L0_ovoru
C SRG_logic.fgi( 247, 465):RS �������
      if(L0_ovoru.and..not.L0_evoru) then
         R0_otoru=R0_utoru
      else
         R0_otoru=max(R_(206)-deltat,0.0)
      endif
      L_avoru=R0_otoru.gt.0.0
      L0_evoru=L0_ovoru
C SRG_logic.fgi( 267, 467):������������  �� T
      L_itoru=L_avoru
C SRG_logic.fgi( 288, 467):������,20SRG10AA249YA21
      !{
      Call KLAPAN_MAN(deltat,REAL(R_obibo,4),R8_ivebo,I_ufibo
     &,C8_exebo,I_ekibo,R_abibo,
     & R_uxebo,R_isebo,REAL(R_usebo,4),R_otebo,
     & REAL(R_avebo,4),R_atebo,REAL(R_itebo,4),I_ifibo,
     & I_ikibo,I_akibo,I_efibo,L_evebo,L_ukibo,L_ulibo,L_utebo
     &,
     & L_etebo,REAL(R_ixebo,4),L_uvebo,L_ovebo,L_elibo,
     & L_osebo,L_oxebo,L_imibo,L_ebibo,L_ibibo,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(288),L_asebo,L_(289),L_itoru
     &,L_alibo,I_okibo,L_amibo,
     & R_afibo,REAL(R_axebo,4),L_emibo,L_esebo,L_omibo,L_oporu
     &,L_ubibo,L_adibo,
     & L_edibo,L_odibo,L_udibo,L_idibo)
      !}

      if(L_udibo.or.L_odibo.or.L_idibo.or.L_edibo.or.L_adibo.or.L_ubibo
     &) then      
                  I_ofibo = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ofibo = z'40000000'
      endif
C SRG_vlv.fgi( 421, 581):���� ���������� ��������,20SRG10AA249
      L_etoru=L_avoru
C SRG_logic.fgi( 288, 463):������,20SRG10AA250YA21
      !{
      Call KLAPAN_MAN(deltat,REAL(R_ufebo,4),R8_obebo,I_amebo
     &,C8_idebo,I_imebo,R_efebo,
     & R_afebo,R_ovabo,REAL(R_axabo,4),R_uxabo,
     & REAL(R_ebebo,4),R_exabo,REAL(R_oxabo,4),I_olebo,
     & I_omebo,I_emebo,I_ilebo,L_ibebo,L_apebo,L_arebo,L_abebo
     &,
     & L_ixabo,REAL(R_odebo,4),L_adebo,L_ubebo,L_ipebo,
     & L_uvabo,L_udebo,L_orebo,L_ifebo,L_ofebo,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(286),L_evabo,L_(287),L_etoru
     &,L_epebo,I_umebo,L_erebo,
     & R_elebo,REAL(R_edebo,4),L_irebo,L_ivabo,L_urebo,L_iporu
     &,L_akebo,L_ekebo,
     & L_ikebo,L_ukebo,L_alebo,L_okebo)
      !}

      if(L_alebo.or.L_ukebo.or.L_okebo.or.L_ikebo.or.L_ekebo.or.L_akebo
     &) then      
                  I_ulebo = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ulebo = z'40000000'
      endif
C SRG_vlv.fgi( 435, 581):���� ���������� ��������,20SRG10AA250
      L_atoru=L_avoru
C SRG_logic.fgi( 288, 459):������,20SRG10AA251YA21
      !{
      Call KLAPAN_MAN(deltat,REAL(R_amabo,4),R8_ufabo,I_erabo
     &,C8_okabo,I_orabo,R_ilabo,
     & R_elabo,R_ubabo,REAL(R_edabo,4),R_afabo,
     & REAL(R_ifabo,4),R_idabo,REAL(R_udabo,4),I_upabo,
     & I_urabo,I_irabo,I_opabo,L_ofabo,L_esabo,L_etabo,L_efabo
     &,
     & L_odabo,REAL(R_ukabo,4),L_ekabo,L_akabo,L_osabo,
     & L_adabo,L_alabo,L_utabo,L_olabo,L_ulabo,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(284),L_ibabo,L_(285),L_atoru
     &,L_isabo,I_asabo,L_itabo,
     & R_ipabo,REAL(R_ikabo,4),L_otabo,L_obabo,L_avabo,L_eporu
     &,L_emabo,L_imabo,
     & L_omabo,L_apabo,L_epabo,L_umabo)
      !}

      if(L_epabo.or.L_apabo.or.L_umabo.or.L_omabo.or.L_imabo.or.L_emabo
     &) then      
                  I_arabo = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_arabo = z'40000000'
      endif
C SRG_vlv.fgi( 449, 581):���� ���������� ��������,20SRG10AA251
      L_usoru=L_avoru
C SRG_logic.fgi( 288, 455):������,20SRG10AA252YA21
      !{
      Call KLAPAN_MAN(deltat,REAL(R_eruxi,4),R8_amuxi,I_ituxi
     &,C8_umuxi,I_utuxi,R_opuxi,
     & R_ipuxi,R_akuxi,REAL(R_ikuxi,4),R_eluxi,
     & REAL(R_oluxi,4),R_okuxi,REAL(R_aluxi,4),I_atuxi,
     & I_avuxi,I_otuxi,I_usuxi,L_uluxi,L_ivuxi,L_ixuxi,L_iluxi
     &,
     & L_ukuxi,REAL(R_apuxi,4),L_imuxi,L_emuxi,L_uvuxi,
     & L_ekuxi,L_epuxi,L_ababo,L_upuxi,L_aruxi,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(282),L_ofuxi,L_(283),L_usoru
     &,L_ovuxi,I_evuxi,L_oxuxi,
     & R_osuxi,REAL(R_omuxi,4),L_uxuxi,L_ufuxi,L_ebabo,L_aporu
     &,L_iruxi,L_oruxi,
     & L_uruxi,L_esuxi,L_isuxi,L_asuxi)
      !}

      if(L_isuxi.or.L_esuxi.or.L_asuxi.or.L_uruxi.or.L_oruxi.or.L_iruxi
     &) then      
                  I_etuxi = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_etuxi = z'40000000'
      endif
C SRG_vlv.fgi( 463, 581):���� ���������� ��������,20SRG10AA252
      L_osoru=L_avoru
C SRG_logic.fgi( 288, 451):������,20SRG10AA253YA21
      !{
      Call KLAPAN_MAN(deltat,REAL(R_itoxi,4),R8_eroxi,I_oxoxi
     &,C8_asoxi,I_abuxi,R_usoxi,
     & R_osoxi,R_emoxi,REAL(R_omoxi,4),R_ipoxi,
     & REAL(R_upoxi,4),R_umoxi,REAL(R_epoxi,4),I_exoxi,
     & I_ebuxi,I_uxoxi,I_axoxi,L_aroxi,L_obuxi,L_oduxi,L_opoxi
     &,
     & L_apoxi,REAL(R_esoxi,4),L_oroxi,L_iroxi,L_aduxi,
     & L_imoxi,L_isoxi,L_efuxi,L_atoxi,L_etoxi,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(280),L_uloxi,L_(281),L_osoru
     &,L_ubuxi,I_ibuxi,L_uduxi,
     & R_uvoxi,REAL(R_uroxi,4),L_afuxi,L_amoxi,L_ifuxi,L_umoru
     &,L_otoxi,L_utoxi,
     & L_avoxi,L_ivoxi,L_ovoxi,L_evoxi)
      !}

      if(L_ovoxi.or.L_ivoxi.or.L_evoxi.or.L_avoxi.or.L_utoxi.or.L_otoxi
     &) then      
                  I_ixoxi = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ixoxi = z'40000000'
      endif
C SRG_vlv.fgi( 477, 581):���� ���������� ��������,20SRG10AA253
      L_isoru=L_avoru
C SRG_logic.fgi( 288, 447):������,20SRG10AA254YA21
      !{
      Call KLAPAN_MAN(deltat,REAL(R_oxixi,4),R8_itixi,I_udoxi
     &,C8_evixi,I_efoxi,R_axixi,
     & R_uvixi,R_irixi,REAL(R_urixi,4),R_osixi,
     & REAL(R_atixi,4),R_asixi,REAL(R_isixi,4),I_idoxi,
     & I_ifoxi,I_afoxi,I_edoxi,L_etixi,L_ufoxi,L_ukoxi,L_usixi
     &,
     & L_esixi,REAL(R_ivixi,4),L_utixi,L_otixi,L_ekoxi,
     & L_orixi,L_ovixi,L_iloxi,L_exixi,L_ixixi,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(278),L_arixi,L_(279),L_isoru
     &,L_akoxi,I_ofoxi,L_aloxi,
     & R_adoxi,REAL(R_avixi,4),L_eloxi,L_erixi,L_oloxi,L_omoru
     &,L_uxixi,L_aboxi,
     & L_eboxi,L_oboxi,L_uboxi,L_iboxi)
      !}

      if(L_uboxi.or.L_oboxi.or.L_iboxi.or.L_eboxi.or.L_aboxi.or.L_uxixi
     &) then      
                  I_odoxi = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_odoxi = z'40000000'
      endif
C SRG_vlv.fgi( 491, 581):���� ���������� ��������,20SRG10AA254
      L_esoru=L_avoru
C SRG_logic.fgi( 288, 443):������,20SRG10AA255YA21
      !{
      Call KLAPAN_MAN(deltat,REAL(R_udixi,4),R8_oxexi,I_alixi
     &,C8_ibixi,I_ilixi,R_edixi,
     & R_adixi,R_otexi,REAL(R_avexi,4),R_uvexi,
     & REAL(R_exexi,4),R_evexi,REAL(R_ovexi,4),I_okixi,
     & I_olixi,I_elixi,I_ikixi,L_ixexi,L_amixi,L_apixi,L_axexi
     &,
     & L_ivexi,REAL(R_obixi,4),L_abixi,L_uxexi,L_imixi,
     & L_utexi,L_ubixi,L_opixi,L_idixi,L_odixi,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(276),L_etexi,L_(277),L_esoru
     &,L_emixi,I_ulixi,L_epixi,
     & R_ekixi,REAL(R_ebixi,4),L_ipixi,L_itexi,L_upixi,L_imoru
     &,L_afixi,L_efixi,
     & L_ifixi,L_ufixi,L_akixi,L_ofixi)
      !}

      if(L_akixi.or.L_ufixi.or.L_ofixi.or.L_ifixi.or.L_efixi.or.L_afixi
     &) then      
                  I_ukixi = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ukixi = z'40000000'
      endif
C SRG_vlv.fgi( 505, 581):���� ���������� ��������,20SRG10AA255
      L_asoru=L_avoru
C SRG_logic.fgi( 288, 439):������,20SRG10AA256YA21
      !{
      Call KLAPAN_MAN(deltat,REAL(R_alexi,4),R8_udexi,I_epexi
     &,C8_ofexi,I_opexi,R_ikexi,
     & R_ekexi,R_uxaxi,REAL(R_ebexi,4),R_adexi,
     & REAL(R_idexi,4),R_ibexi,REAL(R_ubexi,4),I_umexi,
     & I_upexi,I_ipexi,I_omexi,L_odexi,L_erexi,L_esexi,L_edexi
     &,
     & L_obexi,REAL(R_ufexi,4),L_efexi,L_afexi,L_orexi,
     & L_abexi,L_akexi,L_usexi,L_okexi,L_ukexi,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(274),L_ixaxi,L_(275),L_asoru
     &,L_irexi,I_arexi,L_isexi,
     & R_imexi,REAL(R_ifexi,4),L_osexi,L_oxaxi,L_atexi,L_emoru
     &,L_elexi,L_ilexi,
     & L_olexi,L_amexi,L_emexi,L_ulexi)
      !}

      if(L_emexi.or.L_amexi.or.L_ulexi.or.L_olexi.or.L_ilexi.or.L_elexi
     &) then      
                  I_apexi = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_apexi = z'40000000'
      endif
C SRG_vlv.fgi( 533, 653):���� ���������� ��������,20SRG10AA256
      L_uroru=L_avoru
C SRG_logic.fgi( 288, 435):������,20SRG10AA257YA21
      !{
      Call KLAPAN_MAN(deltat,REAL(R_epaxi,4),R8_alaxi,I_isaxi
     &,C8_ulaxi,I_usaxi,R_omaxi,
     & R_imaxi,R_afaxi,REAL(R_ifaxi,4),R_ekaxi,
     & REAL(R_okaxi,4),R_ofaxi,REAL(R_akaxi,4),I_asaxi,
     & I_ataxi,I_osaxi,I_uraxi,L_ukaxi,L_itaxi,L_ivaxi,L_ikaxi
     &,
     & L_ufaxi,REAL(R_amaxi,4),L_ilaxi,L_elaxi,L_utaxi,
     & L_efaxi,L_emaxi,L_axaxi,L_umaxi,L_apaxi,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(272),L_odaxi,L_(273),L_uroru
     &,L_otaxi,I_etaxi,L_ovaxi,
     & R_oraxi,REAL(R_olaxi,4),L_uvaxi,L_udaxi,L_exaxi,L_amoru
     &,L_ipaxi,L_opaxi,
     & L_upaxi,L_eraxi,L_iraxi,L_araxi)
      !}

      if(L_iraxi.or.L_eraxi.or.L_araxi.or.L_upaxi.or.L_opaxi.or.L_ipaxi
     &) then      
                  I_esaxi = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_esaxi = z'40000000'
      endif
C SRG_vlv.fgi( 533, 629):���� ���������� ��������,20SRG10AA257
      L_ororu=L_avoru
C SRG_logic.fgi( 288, 431):������,20SRG10AA258YA21
      !{
      Call KLAPAN_MAN(deltat,REAL(R_isuvi,4),R8_epuvi,I_ovuvi
     &,C8_aruvi,I_axuvi,R_uruvi,
     & R_oruvi,R_eluvi,REAL(R_oluvi,4),R_imuvi,
     & REAL(R_umuvi,4),R_uluvi,REAL(R_emuvi,4),I_evuvi,
     & I_exuvi,I_uvuvi,I_avuvi,L_apuvi,L_oxuvi,L_obaxi,L_omuvi
     &,
     & L_amuvi,REAL(R_eruvi,4),L_opuvi,L_ipuvi,L_abaxi,
     & L_iluvi,L_iruvi,L_edaxi,L_asuvi,L_esuvi,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(270),L_ukuvi,L_(271),L_ororu
     &,L_uxuvi,I_ixuvi,L_ubaxi,
     & R_utuvi,REAL(R_upuvi,4),L_adaxi,L_aluvi,L_idaxi,L_uloru
     &,L_osuvi,L_usuvi,
     & L_atuvi,L_ituvi,L_otuvi,L_etuvi)
      !}

      if(L_otuvi.or.L_ituvi.or.L_etuvi.or.L_atuvi.or.L_usuvi.or.L_osuvi
     &) then      
                  I_ivuvi = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ivuvi = z'40000000'
      endif
C SRG_vlv.fgi( 533, 605):���� ���������� ��������,20SRG10AA258
      L_iroru=L_avoru
C SRG_logic.fgi( 288, 427):������,20SRG10AA259YA21
      !{
      Call KLAPAN_MAN(deltat,REAL(R_ovovi,4),R8_isovi,I_ubuvi
     &,C8_etovi,I_eduvi,R_avovi,
     & R_utovi,R_ipovi,REAL(R_upovi,4),R_orovi,
     & REAL(R_asovi,4),R_arovi,REAL(R_irovi,4),I_ibuvi,
     & I_iduvi,I_aduvi,I_ebuvi,L_esovi,L_uduvi,L_ufuvi,L_urovi
     &,
     & L_erovi,REAL(R_itovi,4),L_usovi,L_osovi,L_efuvi,
     & L_opovi,L_otovi,L_ikuvi,L_evovi,L_ivovi,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(268),L_apovi,L_(269),L_iroru
     &,L_afuvi,I_oduvi,L_akuvi,
     & R_abuvi,REAL(R_atovi,4),L_ekuvi,L_epovi,L_okuvi,L_oloru
     &,L_uvovi,L_axovi,
     & L_exovi,L_oxovi,L_uxovi,L_ixovi)
      !}

      if(L_uxovi.or.L_oxovi.or.L_ixovi.or.L_exovi.or.L_axovi.or.L_uvovi
     &) then      
                  I_obuvi = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_obuvi = z'40000000'
      endif
C SRG_vlv.fgi( 519, 581):���� ���������� ��������,20SRG10AA259
      !{
      Call KLAPAN_MAN(deltat,REAL(R_ubovi,4),R8_ovivi,I_akovi
     &,C8_ixivi,I_ikovi,R_ebovi,
     & R_abovi,R_osivi,REAL(R_ativi,4),R_utivi,
     & REAL(R_evivi,4),R_etivi,REAL(R_otivi,4),I_ofovi,
     & I_okovi,I_ekovi,I_ifovi,L_ivivi,L_alovi,L_amovi,L_avivi
     &,
     & L_itivi,REAL(R_oxivi,4),L_axivi,L_uvivi,L_ilovi,
     & L_usivi,L_uxivi,L_omovi,L_ibovi,L_obovi,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(266),L_esivi,L_(267),L_avoru
     &,L_elovi,I_ukovi,L_emovi,
     & R_efovi,REAL(R_exivi,4),L_imovi,L_isivi,L_umovi,L_iloru
     &,L_adovi,L_edovi,
     & L_idovi,L_udovi,L_afovi,L_odovi)
      !}

      if(L_afovi.or.L_udovi.or.L_odovi.or.L_idovi.or.L_edovi.or.L_adovi
     &) then      
                  I_ufovi = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ufovi = z'40000000'
      endif
C SRG_vlv.fgi( 534, 581):���� ���������� ��������,20SRG10AA260
      L_(453) = L_oporu.AND.L_iporu.AND.L_eporu.AND.L_aporu.AND.L_umoru.
     &AND.L_omoru.AND.L_imoru.AND.L_emoru.AND.L_amoru.AND.L_uloru.AND.L_
     &oloru.AND.L_iloru
C SRG_logic.fgi( 241, 411):�
      L0_eloru=(L0_ovoru.or.L0_eloru).and..not.(L_ivoru)
      L_(451)=.not.L0_eloru
C SRG_logic.fgi( 252, 407):RS �������
      L_(452) = L_(453).AND.L0_eloru
C SRG_logic.fgi( 260, 410):�
      if(L_(452).and..not.L0_aloru) then
         R0_okoru=R0_ukoru
      else
         R0_okoru=max(R_(205)-deltat,0.0)
      endif
      L_eroru=R0_okoru.gt.0.0
      L0_aloru=L_(452)
C SRG_logic.fgi( 268, 410):������������  �� T
      L_uporu=L_eroru
C SRG_logic.fgi( 288, 406):������,20SRG10AA243YA21
      !{
      Call KLAPAN_MAN(deltat,REAL(R_imalo,4),R8_ekalo,I_oralo
     &,C8_alalo,I_asalo,R_ulalo,
     & R_olalo,R_edalo,REAL(R_odalo,4),R_ifalo,
     & REAL(R_ufalo,4),R_udalo,REAL(R_efalo,4),I_eralo,
     & I_esalo,I_uralo,I_aralo,L_akalo,L_osalo,L_otalo,L_ofalo
     &,
     & L_afalo,REAL(R_elalo,4),L_okalo,L_ikalo,L_atalo,
     & L_idalo,L_ilalo,L_evalo,L_amalo,L_emalo,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(332),L_udoru,L_(333),L_uporu
     &,L_usalo,I_isalo,L_utalo,
     & R_upalo,REAL(R_ukalo,4),L_avalo,L_uboru,L_ivalo,L_oforu
     &,L_omalo,L_umalo,
     & L_apalo,L_ipalo,L_opalo,L_epalo)
      !}

      if(L_opalo.or.L_ipalo.or.L_epalo.or.L_apalo.or.L_umalo.or.L_omalo
     &) then      
                  I_iralo = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_iralo = z'40000000'
      endif
C SRG_vlv.fgi( 421, 629):���� ���������� ��������,20SRG10AA243
      L_(446) = L_adoru.AND.L_uboru
C SRG_logic.fgi( 232, 340):�
      if(L_(446).and..not.L0_oboru) then
         R0_eboru=R0_iboru
      else
         R0_eboru=max(R_(201)-deltat,0.0)
      endif
      L_ivoru=R0_eboru.gt.0.0
      L0_oboru=L_(446)
C SRG_logic.fgi( 247, 340):������������  �� T
      L0_eforu=(L0_ovoru.or.L0_eforu).and..not.(L_ivoru)
      L_(447)=.not.L0_eforu
C SRG_logic.fgi( 255, 362):RS �������
      L_(448) = L_(449).AND.L0_eforu.AND.L_(450)
C SRG_logic.fgi( 263, 364):�
      if(L_(448).and..not.L0_aforu) then
         R0_idoru=R0_odoru
      else
         R0_idoru=max(R_(202)-deltat,0.0)
      endif
      L_udoru=R0_idoru.gt.0.0
      L0_aforu=L_(448)
C SRG_logic.fgi( 271, 364):������������  �� T
      L_edoru=L_udoru
C SRG_logic.fgi( 290, 364):������,20SRG10AA241YA21
      L_aroru=L_eroru
C SRG_logic.fgi( 288, 410):������,20SRG10AA241YA22
      !{
      Call KLAPAN_MAN(deltat,REAL(R_adilo,4),R8_uvelo,I_ekilo
     &,C8_oxelo,I_okilo,R_ibilo,
     & R_ebilo,R_uselo,REAL(R_etelo,4),R_avelo,
     & REAL(R_ivelo,4),R_itelo,REAL(R_utelo,4),I_ufilo,
     & I_ukilo,I_ikilo,I_ofilo,L_ovelo,L_elilo,L_emilo,L_evelo
     &,
     & L_otelo,REAL(R_uxelo,4),L_exelo,L_axelo,L_olilo,
     & L_atelo,L_abilo,L_umilo,L_obilo,L_ubilo,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(336),L_aroru,L_(337),L_edoru
     &,L_ililo,I_alilo,L_imilo,
     & R_ifilo,REAL(R_ixelo,4),L_omilo,L_uforu,L_apilo,L_adoru
     &,L_edilo,L_idilo,
     & L_odilo,L_afilo,L_efilo,L_udilo)
      !}

      if(L_efilo.or.L_afilo.or.L_udilo.or.L_odilo.or.L_idilo.or.L_edilo
     &) then      
                  I_akilo = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_akilo = z'40000000'
      endif
C SRG_vlv.fgi( 421, 653):���� ���������� ��������,20SRG10AA241
C sav1=L_(446)
      L_(446) = L_adoru.AND.L_uboru
C SRG_logic.fgi( 232, 340):recalc:�
C if(sav1.ne.L_(446) .and. try2987.gt.0) goto 2987
      !{
      Call KLAPAN_MAN(deltat,REAL(R_asuko,4),R8_umuko,I_evuko
     &,C8_opuko,I_ovuko,R_iruko,
     & R_eruko,R_ukuko,REAL(R_eluko,4),R_amuko,
     & REAL(R_imuko,4),R_iluko,REAL(R_uluko,4),I_utuko,
     & I_uvuko,I_ivuko,I_otuko,L_omuko,L_exuko,L_ebalo,L_emuko
     &,
     & L_oluko,REAL(R_upuko,4),L_epuko,L_apuko,L_oxuko,
     & L_aluko,L_aruko,L_ubalo,L_oruko,L_uruko,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(330),L_ikuko,L_(331),L_eroru
     &,L_ixuko,I_axuko,L_ibalo,
     & R_ituko,REAL(R_ipuko,4),L_obalo,L_okuko,L_adalo,L_iforu
     &,L_esuko,L_isuko,
     & L_osuko,L_atuko,L_etuko,L_usuko)
      !}

      if(L_etuko.or.L_atuko.or.L_usuko.or.L_osuko.or.L_isuko.or.L_esuko
     &) then      
                  I_avuko = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_avuko = z'40000000'
      endif
C SRG_vlv.fgi( 435, 629):���� ���������� ��������,20SRG10AA244
C sav1=L_(449)
      L_(449) = L_uforu.AND.L_oforu.AND.L_iforu
C SRG_logic.fgi( 232, 366):recalc:�
C if(sav1.ne.L_(449) .and. try2903.gt.0) goto 2903
      L0_omopu=L_ivoru.or.(L0_omopu.and..not.(L_(354)))
      L_(355)=.not.L0_omopu
C SRG_logic.fgi(  51, 152):RS �������
      if(L0_omopu) then
         R8_umopu=R_(175)
      else
         R8_umopu=R_(176)
      endif
C SRG_logic.fgi( 121, 173):���� RE IN LO CH7
      if(L0_omopu) then
         R_avapu=R_(115)
      else
         R_avapu=R_(116)
      endif
C SRG_logic.fgi( 123, 179):���� RE IN LO CH7
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_iluti,R_eruti,REAL(1
     &,4),
     & REAL(R_avapu,4),I_aruti,REAL(R_amuti,4),
     & REAL(R_emuti,4),REAL(R_eluti,4),
     & REAL(R_aluti,4),REAL(R_umuti,4),L_aputi,REAL(R_eputi
     &,4),L_iputi,
     & L_oputi,R_imuti,REAL(R_uluti,4),REAL(R_oluti,4),L_uputi
     &)
      !}
C SRG_vlv.fgi( 478, 543):���������� ������� ��� 2,20SRG10GT001XQ01
      if(L0_omopu) then
         R_otapu=R_(111)
      else
         R_otapu=R_(112)
      endif
C SRG_logic.fgi( 133, 157):���� RE IN LO CH7
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ototi,R_ibuti,REAL(1
     &,4),
     & REAL(R_otapu,4),I_ebuti,REAL(R_evoti,4),
     & REAL(R_ivoti,4),REAL(R_itoti,4),
     & REAL(R_etoti,4),REAL(R_axoti,4),L_exoti,REAL(R_ixoti
     &,4),L_oxoti,
     & L_uxoti,R_ovoti,REAL(R_avoti,4),REAL(R_utoti,4),L_abuti
     &)
      !}
C SRG_vlv.fgi( 478, 517):���������� ������� ��� 2,20SRG10GT002XQ01
      if(L0_omopu) then
         R_utapu=R_(113)
      else
         R_utapu=R_(114)
      endif
C SRG_logic.fgi( 125, 185):���� RE IN LO CH7
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_aduti,R_ukuti,REAL(1
     &,4),
     & REAL(R_utapu,4),I_okuti,REAL(R_oduti,4),
     & REAL(R_uduti,4),REAL(R_ubuti,4),
     & REAL(R_obuti,4),REAL(R_ifuti,4),L_ofuti,REAL(R_ufuti
     &,4),L_akuti,
     & L_ekuti,R_afuti,REAL(R_iduti,4),REAL(R_eduti,4),L_ikuti
     &)
      !}
C SRG_vlv.fgi( 478, 530):���������� ������� ��� 2,20SRG10GT001XQ02
      if(L0_omopu) then
         R_itapu=R_(109)
      else
         R_itapu=R_(110)
      endif
C SRG_logic.fgi( 135, 164):���� RE IN LO CH7
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_epoti,R_atoti,REAL(1
     &,4),
     & REAL(R_itapu,4),I_usoti,REAL(R_upoti,4),
     & REAL(R_aroti,4),REAL(R_apoti,4),
     & REAL(R_umoti,4),REAL(R_oroti,4),L_uroti,REAL(R_asoti
     &,4),L_esoti,
     & L_isoti,R_eroti,REAL(R_opoti,4),REAL(R_ipoti,4),L_osoti
     &)
      !}
C SRG_vlv.fgi( 478, 504):���������� ������� ��� 2,20SRG10GT002XQ02
      L0_ebopu=L_ivoru.or.(L0_ebopu.and..not.(L_(351)))
      L_(352)=.not.L0_ebopu
C SRG_logic.fgi(  51,  96):RS �������
      if(L0_ebopu) then
         R8_ibopu=R_(165)
      else
         R8_ibopu=R_(166)
      endif
C SRG_logic.fgi( 121, 117):���� RE IN LO CH7
      Call PUMP2_HANDLER(deltat,I_osomu,I_isomu,R_umomu,
     & REAL(R_epomu,4),R_emomu,REAL(R_omomu,4),L_exomu,
     & L_evomu,L_adumu,L_edumu,L_uromu,L_itomu,L_utomu,L_otomu
     &,L_avomu,L_uvomu,
     & L_amomu,L_apomu,L_ulomu,L_imomu,L_ibumu,
     & L_obumu,L_olomu,L_ilomu,L_asomu,I_usomu,R_oxomu,R_uxomu
     &,L_udumu,
     & L_axomu,L_ubumu,REAL(R8_elomu,8),L_ovomu,REAL(R8_ivomu
     &,8),R_idumu,
     & REAL(R_atomu,4),R_etomu,REAL(R8_oromu,8),R_iromu,R8_aromu
     &,R_odumu,R8_ivomu,
     & REAL(R_ipomu,4),REAL(R_opomu,4))
C SRG_vlv.fgi(  37, 773):���������� ���������� ������� 2,20SRG10AN001
C label 3050  try3050=try3050-1
C sav1=R_odumu
C sav2=R_idumu
C sav3=L_exomu
C sav4=L_uvomu
C sav5=L_evomu
C sav6=R8_ivomu
C sav7=R_etomu
C sav8=I_usomu
C sav9=I_osomu
C sav10=I_isomu
C sav11=I_esomu
C sav12=L_asomu
C sav13=L_uromu
C sav14=R_iromu
C sav15=L_apomu
C sav16=L_imomu
      Call PUMP2_HANDLER(deltat,I_osomu,I_isomu,R_umomu,
     & REAL(R_epomu,4),R_emomu,REAL(R_omomu,4),L_exomu,
     & L_evomu,L_adumu,L_edumu,L_uromu,L_itomu,L_utomu,L_otomu
     &,L_avomu,L_uvomu,
     & L_amomu,L_apomu,L_ulomu,L_imomu,L_ibumu,
     & L_obumu,L_olomu,L_ilomu,L_asomu,I_usomu,R_oxomu,R_uxomu
     &,L_udumu,
     & L_axomu,L_ubumu,REAL(R8_elomu,8),L_ovomu,REAL(R8_ivomu
     &,8),R_idumu,
     & REAL(R_atomu,4),R_etomu,REAL(R8_oromu,8),R_iromu,R8_aromu
     &,R_odumu,R8_ivomu,
     & REAL(R_ipomu,4),REAL(R_opomu,4))
C SRG_vlv.fgi(  37, 773):recalc:���������� ���������� ������� 2,20SRG10AN001
C if(sav1.ne.R_odumu .and. try3050.gt.0) goto 3050
C if(sav2.ne.R_idumu .and. try3050.gt.0) goto 3050
C if(sav3.ne.L_exomu .and. try3050.gt.0) goto 3050
C if(sav4.ne.L_uvomu .and. try3050.gt.0) goto 3050
C if(sav5.ne.L_evomu .and. try3050.gt.0) goto 3050
C if(sav6.ne.R8_ivomu .and. try3050.gt.0) goto 3050
C if(sav7.ne.R_etomu .and. try3050.gt.0) goto 3050
C if(sav8.ne.I_usomu .and. try3050.gt.0) goto 3050
C if(sav9.ne.I_osomu .and. try3050.gt.0) goto 3050
C if(sav10.ne.I_isomu .and. try3050.gt.0) goto 3050
C if(sav11.ne.I_esomu .and. try3050.gt.0) goto 3050
C if(sav12.ne.L_asomu .and. try3050.gt.0) goto 3050
C if(sav13.ne.L_uromu .and. try3050.gt.0) goto 3050
C if(sav14.ne.R_iromu .and. try3050.gt.0) goto 3050
C if(sav15.ne.L_apomu .and. try3050.gt.0) goto 3050
C if(sav16.ne.L_imomu .and. try3050.gt.0) goto 3050
      Call PUMP2_HANDLER(deltat,I_ofiku,I_ifiku,R_abiku,
     & REAL(R_ibiku,4),R_ixeku,REAL(R_uxeku,4),L_amiku,
     & L_eliku,L_upiku,L_ariku,L_udiku,L_ikiku,L_ukiku,L_okiku
     &,L_aliku,L_uliku,
     & L_exeku,L_ebiku,L_axeku,L_oxeku,L_epiku,
     & L_ipiku,L_uveku,L_oveku,L_afiku,I_ufiku,R_imiku,R_omiku
     &,L_oriku,
     & L_axomu,L_opiku,REAL(R8_elomu,8),L_oliku,REAL(R8_iliku
     &,8),R_eriku,
     & REAL(R_akiku,4),R_ekiku,REAL(R8_odiku,8),R_idiku,R8_aromu
     &,R_iriku,R8_iliku,
     & REAL(R_obiku,4),REAL(R_ubiku,4))
C SRG_vlv.fgi(  37, 747):���������� ���������� ������� 2,20SRG10AN012
C label 3051  try3051=try3051-1
C sav1=R_iriku
C sav2=R_eriku
C sav3=L_amiku
C sav4=L_uliku
C sav5=L_eliku
C sav6=R8_iliku
C sav7=R_ekiku
C sav8=I_ufiku
C sav9=I_ofiku
C sav10=I_ifiku
C sav11=I_efiku
C sav12=L_afiku
C sav13=L_udiku
C sav14=R_idiku
C sav15=L_ebiku
C sav16=L_oxeku
      Call PUMP2_HANDLER(deltat,I_ofiku,I_ifiku,R_abiku,
     & REAL(R_ibiku,4),R_ixeku,REAL(R_uxeku,4),L_amiku,
     & L_eliku,L_upiku,L_ariku,L_udiku,L_ikiku,L_ukiku,L_okiku
     &,L_aliku,L_uliku,
     & L_exeku,L_ebiku,L_axeku,L_oxeku,L_epiku,
     & L_ipiku,L_uveku,L_oveku,L_afiku,I_ufiku,R_imiku,R_omiku
     &,L_oriku,
     & L_axomu,L_opiku,REAL(R8_elomu,8),L_oliku,REAL(R8_iliku
     &,8),R_eriku,
     & REAL(R_akiku,4),R_ekiku,REAL(R8_odiku,8),R_idiku,R8_aromu
     &,R_iriku,R8_iliku,
     & REAL(R_obiku,4),REAL(R_ubiku,4))
C SRG_vlv.fgi(  37, 747):recalc:���������� ���������� ������� 2,20SRG10AN012
C if(sav1.ne.R_iriku .and. try3051.gt.0) goto 3051
C if(sav2.ne.R_eriku .and. try3051.gt.0) goto 3051
C if(sav3.ne.L_amiku .and. try3051.gt.0) goto 3051
C if(sav4.ne.L_uliku .and. try3051.gt.0) goto 3051
C if(sav5.ne.L_eliku .and. try3051.gt.0) goto 3051
C if(sav6.ne.R8_iliku .and. try3051.gt.0) goto 3051
C if(sav7.ne.R_ekiku .and. try3051.gt.0) goto 3051
C if(sav8.ne.I_ufiku .and. try3051.gt.0) goto 3051
C if(sav9.ne.I_ofiku .and. try3051.gt.0) goto 3051
C if(sav10.ne.I_ifiku .and. try3051.gt.0) goto 3051
C if(sav11.ne.I_efiku .and. try3051.gt.0) goto 3051
C if(sav12.ne.L_afiku .and. try3051.gt.0) goto 3051
C if(sav13.ne.L_udiku .and. try3051.gt.0) goto 3051
C if(sav14.ne.R_idiku .and. try3051.gt.0) goto 3051
C if(sav15.ne.L_ebiku .and. try3051.gt.0) goto 3051
C if(sav16.ne.L_oxeku .and. try3051.gt.0) goto 3051
      Call PUMP2_HANDLER(deltat,I_ovulu,I_ivulu,R_asulu,
     & REAL(R_isulu,4),R_irulu,REAL(R_urulu,4),L_adamu,
     & L_ebamu,L_ufamu,L_akamu,L_utulu,L_ixulu,L_uxulu,L_oxulu
     &,L_abamu,L_ubamu,
     & L_erulu,L_esulu,L_arulu,L_orulu,L_efamu,
     & L_ifamu,L_upulu,L_opulu,L_avulu,I_uvulu,R_idamu,R_odamu
     &,L_okamu,
     & L_axomu,L_ofamu,REAL(R8_elomu,8),L_obamu,REAL(R8_ibamu
     &,8),R_ekamu,
     & REAL(R_axulu,4),R_exulu,REAL(R8_otulu,8),R_itulu,R8_aromu
     &,R_ikamu,R8_ibamu,
     & REAL(R_osulu,4),REAL(R_usulu,4))
C SRG_vlv.fgi(  50, 773):���������� ���������� ������� 2,20SRG10AN002
C label 3052  try3052=try3052-1
C sav1=R_ikamu
C sav2=R_ekamu
C sav3=L_adamu
C sav4=L_ubamu
C sav5=L_ebamu
C sav6=R8_ibamu
C sav7=R_exulu
C sav8=I_uvulu
C sav9=I_ovulu
C sav10=I_ivulu
C sav11=I_evulu
C sav12=L_avulu
C sav13=L_utulu
C sav14=R_itulu
C sav15=L_esulu
C sav16=L_orulu
      Call PUMP2_HANDLER(deltat,I_ovulu,I_ivulu,R_asulu,
     & REAL(R_isulu,4),R_irulu,REAL(R_urulu,4),L_adamu,
     & L_ebamu,L_ufamu,L_akamu,L_utulu,L_ixulu,L_uxulu,L_oxulu
     &,L_abamu,L_ubamu,
     & L_erulu,L_esulu,L_arulu,L_orulu,L_efamu,
     & L_ifamu,L_upulu,L_opulu,L_avulu,I_uvulu,R_idamu,R_odamu
     &,L_okamu,
     & L_axomu,L_ofamu,REAL(R8_elomu,8),L_obamu,REAL(R8_ibamu
     &,8),R_ekamu,
     & REAL(R_axulu,4),R_exulu,REAL(R8_otulu,8),R_itulu,R8_aromu
     &,R_ikamu,R8_ibamu,
     & REAL(R_osulu,4),REAL(R_usulu,4))
C SRG_vlv.fgi(  50, 773):recalc:���������� ���������� ������� 2,20SRG10AN002
C if(sav1.ne.R_ikamu .and. try3052.gt.0) goto 3052
C if(sav2.ne.R_ekamu .and. try3052.gt.0) goto 3052
C if(sav3.ne.L_adamu .and. try3052.gt.0) goto 3052
C if(sav4.ne.L_ubamu .and. try3052.gt.0) goto 3052
C if(sav5.ne.L_ebamu .and. try3052.gt.0) goto 3052
C if(sav6.ne.R8_ibamu .and. try3052.gt.0) goto 3052
C if(sav7.ne.R_exulu .and. try3052.gt.0) goto 3052
C if(sav8.ne.I_uvulu .and. try3052.gt.0) goto 3052
C if(sav9.ne.I_ovulu .and. try3052.gt.0) goto 3052
C if(sav10.ne.I_ivulu .and. try3052.gt.0) goto 3052
C if(sav11.ne.I_evulu .and. try3052.gt.0) goto 3052
C if(sav12.ne.L_avulu .and. try3052.gt.0) goto 3052
C if(sav13.ne.L_utulu .and. try3052.gt.0) goto 3052
C if(sav14.ne.R_itulu .and. try3052.gt.0) goto 3052
C if(sav15.ne.L_esulu .and. try3052.gt.0) goto 3052
C if(sav16.ne.L_orulu .and. try3052.gt.0) goto 3052
      Call PUMP2_HANDLER(deltat,I_imeku,I_emeku,R_ufeku,
     & REAL(R_ekeku,4),R_efeku,REAL(R_ofeku,4),L_ureku,
     & L_areku,L_oteku,L_uteku,L_oleku,L_epeku,L_opeku,L_ipeku
     &,L_upeku,L_oreku,
     & L_afeku,L_akeku,L_udeku,L_ifeku,L_ateku,
     & L_eteku,L_odeku,L_ideku,L_uleku,I_omeku,R_eseku,R_iseku
     &,L_iveku,
     & L_axomu,L_iteku,REAL(R8_elomu,8),L_ireku,REAL(R8_ereku
     &,8),R_aveku,
     & REAL(R_umeku,4),R_apeku,REAL(R8_ileku,8),R_eleku,R8_aromu
     &,R_eveku,R8_ereku,
     & REAL(R_ikeku,4),REAL(R_okeku,4))
C SRG_vlv.fgi(  50, 747):���������� ���������� ������� 2,20SRG10AN013
C label 3053  try3053=try3053-1
C sav1=R_eveku
C sav2=R_aveku
C sav3=L_ureku
C sav4=L_oreku
C sav5=L_areku
C sav6=R8_ereku
C sav7=R_apeku
C sav8=I_omeku
C sav9=I_imeku
C sav10=I_emeku
C sav11=I_ameku
C sav12=L_uleku
C sav13=L_oleku
C sav14=R_eleku
C sav15=L_akeku
C sav16=L_ifeku
      Call PUMP2_HANDLER(deltat,I_imeku,I_emeku,R_ufeku,
     & REAL(R_ekeku,4),R_efeku,REAL(R_ofeku,4),L_ureku,
     & L_areku,L_oteku,L_uteku,L_oleku,L_epeku,L_opeku,L_ipeku
     &,L_upeku,L_oreku,
     & L_afeku,L_akeku,L_udeku,L_ifeku,L_ateku,
     & L_eteku,L_odeku,L_ideku,L_uleku,I_omeku,R_eseku,R_iseku
     &,L_iveku,
     & L_axomu,L_iteku,REAL(R8_elomu,8),L_ireku,REAL(R8_ereku
     &,8),R_aveku,
     & REAL(R_umeku,4),R_apeku,REAL(R8_ileku,8),R_eleku,R8_aromu
     &,R_eveku,R8_ereku,
     & REAL(R_ikeku,4),REAL(R_okeku,4))
C SRG_vlv.fgi(  50, 747):recalc:���������� ���������� ������� 2,20SRG10AN013
C if(sav1.ne.R_eveku .and. try3053.gt.0) goto 3053
C if(sav2.ne.R_aveku .and. try3053.gt.0) goto 3053
C if(sav3.ne.L_ureku .and. try3053.gt.0) goto 3053
C if(sav4.ne.L_oreku .and. try3053.gt.0) goto 3053
C if(sav5.ne.L_areku .and. try3053.gt.0) goto 3053
C if(sav6.ne.R8_ereku .and. try3053.gt.0) goto 3053
C if(sav7.ne.R_apeku .and. try3053.gt.0) goto 3053
C if(sav8.ne.I_omeku .and. try3053.gt.0) goto 3053
C if(sav9.ne.I_imeku .and. try3053.gt.0) goto 3053
C if(sav10.ne.I_emeku .and. try3053.gt.0) goto 3053
C if(sav11.ne.I_ameku .and. try3053.gt.0) goto 3053
C if(sav12.ne.L_uleku .and. try3053.gt.0) goto 3053
C if(sav13.ne.L_oleku .and. try3053.gt.0) goto 3053
C if(sav14.ne.R_eleku .and. try3053.gt.0) goto 3053
C if(sav15.ne.L_akeku .and. try3053.gt.0) goto 3053
C if(sav16.ne.L_ifeku .and. try3053.gt.0) goto 3053
      Call PUMP2_HANDLER(deltat,I_idulu,I_edulu,R_uvolu,
     & REAL(R_exolu,4),R_evolu,REAL(R_ovolu,4),L_ukulu,
     & L_akulu,L_omulu,L_umulu,L_obulu,L_efulu,L_ofulu,L_ifulu
     &,L_ufulu,L_okulu,
     & L_avolu,L_axolu,L_utolu,L_ivolu,L_amulu,
     & L_emulu,L_otolu,L_itolu,L_ubulu,I_odulu,R_elulu,R_ilulu
     &,L_ipulu,
     & L_axomu,L_imulu,REAL(R8_elomu,8),L_ikulu,REAL(R8_ekulu
     &,8),R_apulu,
     & REAL(R_udulu,4),R_afulu,REAL(R8_ibulu,8),R_ebulu,R8_aromu
     &,R_epulu,R8_ekulu,
     & REAL(R_ixolu,4),REAL(R_oxolu,4))
C SRG_vlv.fgi(  64, 773):���������� ���������� ������� 2,20SRG10AN003
C label 3054  try3054=try3054-1
C sav1=R_epulu
C sav2=R_apulu
C sav3=L_ukulu
C sav4=L_okulu
C sav5=L_akulu
C sav6=R8_ekulu
C sav7=R_afulu
C sav8=I_odulu
C sav9=I_idulu
C sav10=I_edulu
C sav11=I_adulu
C sav12=L_ubulu
C sav13=L_obulu
C sav14=R_ebulu
C sav15=L_axolu
C sav16=L_ivolu
      Call PUMP2_HANDLER(deltat,I_idulu,I_edulu,R_uvolu,
     & REAL(R_exolu,4),R_evolu,REAL(R_ovolu,4),L_ukulu,
     & L_akulu,L_omulu,L_umulu,L_obulu,L_efulu,L_ofulu,L_ifulu
     &,L_ufulu,L_okulu,
     & L_avolu,L_axolu,L_utolu,L_ivolu,L_amulu,
     & L_emulu,L_otolu,L_itolu,L_ubulu,I_odulu,R_elulu,R_ilulu
     &,L_ipulu,
     & L_axomu,L_imulu,REAL(R8_elomu,8),L_ikulu,REAL(R8_ekulu
     &,8),R_apulu,
     & REAL(R_udulu,4),R_afulu,REAL(R8_ibulu,8),R_ebulu,R8_aromu
     &,R_epulu,R8_ekulu,
     & REAL(R_ixolu,4),REAL(R_oxolu,4))
C SRG_vlv.fgi(  64, 773):recalc:���������� ���������� ������� 2,20SRG10AN003
C if(sav1.ne.R_epulu .and. try3054.gt.0) goto 3054
C if(sav2.ne.R_apulu .and. try3054.gt.0) goto 3054
C if(sav3.ne.L_ukulu .and. try3054.gt.0) goto 3054
C if(sav4.ne.L_okulu .and. try3054.gt.0) goto 3054
C if(sav5.ne.L_akulu .and. try3054.gt.0) goto 3054
C if(sav6.ne.R8_ekulu .and. try3054.gt.0) goto 3054
C if(sav7.ne.R_afulu .and. try3054.gt.0) goto 3054
C if(sav8.ne.I_odulu .and. try3054.gt.0) goto 3054
C if(sav9.ne.I_idulu .and. try3054.gt.0) goto 3054
C if(sav10.ne.I_edulu .and. try3054.gt.0) goto 3054
C if(sav11.ne.I_adulu .and. try3054.gt.0) goto 3054
C if(sav12.ne.L_ubulu .and. try3054.gt.0) goto 3054
C if(sav13.ne.L_obulu .and. try3054.gt.0) goto 3054
C if(sav14.ne.R_ebulu .and. try3054.gt.0) goto 3054
C if(sav15.ne.L_axolu .and. try3054.gt.0) goto 3054
C if(sav16.ne.L_ivolu .and. try3054.gt.0) goto 3054
      Call PUMP2_HANDLER(deltat,I_esaku,I_asaku,R_omaku,
     & REAL(R_apaku,4),R_amaku,REAL(R_imaku,4),L_ovaku,
     & L_utaku,L_ibeku,L_obeku,L_iraku,L_ataku,L_itaku,L_etaku
     &,L_otaku,L_ivaku,
     & L_ulaku,L_umaku,L_olaku,L_emaku,L_uxaku,
     & L_abeku,L_ilaku,L_elaku,L_oraku,I_isaku,R_axaku,R_exaku
     &,L_edeku,
     & L_axomu,L_ebeku,REAL(R8_elomu,8),L_evaku,REAL(R8_avaku
     &,8),R_ubeku,
     & REAL(R_osaku,4),R_usaku,REAL(R8_eraku,8),R_araku,R8_aromu
     &,R_adeku,R8_avaku,
     & REAL(R_epaku,4),REAL(R_ipaku,4))
C SRG_vlv.fgi(  64, 747):���������� ���������� ������� 2,20SRG10AN014
C label 3055  try3055=try3055-1
C sav1=R_adeku
C sav2=R_ubeku
C sav3=L_ovaku
C sav4=L_ivaku
C sav5=L_utaku
C sav6=R8_avaku
C sav7=R_usaku
C sav8=I_isaku
C sav9=I_esaku
C sav10=I_asaku
C sav11=I_uraku
C sav12=L_oraku
C sav13=L_iraku
C sav14=R_araku
C sav15=L_umaku
C sav16=L_emaku
      Call PUMP2_HANDLER(deltat,I_esaku,I_asaku,R_omaku,
     & REAL(R_apaku,4),R_amaku,REAL(R_imaku,4),L_ovaku,
     & L_utaku,L_ibeku,L_obeku,L_iraku,L_ataku,L_itaku,L_etaku
     &,L_otaku,L_ivaku,
     & L_ulaku,L_umaku,L_olaku,L_emaku,L_uxaku,
     & L_abeku,L_ilaku,L_elaku,L_oraku,I_isaku,R_axaku,R_exaku
     &,L_edeku,
     & L_axomu,L_ebeku,REAL(R8_elomu,8),L_evaku,REAL(R8_avaku
     &,8),R_ubeku,
     & REAL(R_osaku,4),R_usaku,REAL(R8_eraku,8),R_araku,R8_aromu
     &,R_adeku,R8_avaku,
     & REAL(R_epaku,4),REAL(R_ipaku,4))
C SRG_vlv.fgi(  64, 747):recalc:���������� ���������� ������� 2,20SRG10AN014
C if(sav1.ne.R_adeku .and. try3055.gt.0) goto 3055
C if(sav2.ne.R_ubeku .and. try3055.gt.0) goto 3055
C if(sav3.ne.L_ovaku .and. try3055.gt.0) goto 3055
C if(sav4.ne.L_ivaku .and. try3055.gt.0) goto 3055
C if(sav5.ne.L_utaku .and. try3055.gt.0) goto 3055
C if(sav6.ne.R8_avaku .and. try3055.gt.0) goto 3055
C if(sav7.ne.R_usaku .and. try3055.gt.0) goto 3055
C if(sav8.ne.I_isaku .and. try3055.gt.0) goto 3055
C if(sav9.ne.I_esaku .and. try3055.gt.0) goto 3055
C if(sav10.ne.I_asaku .and. try3055.gt.0) goto 3055
C if(sav11.ne.I_uraku .and. try3055.gt.0) goto 3055
C if(sav12.ne.L_oraku .and. try3055.gt.0) goto 3055
C if(sav13.ne.L_iraku .and. try3055.gt.0) goto 3055
C if(sav14.ne.R_araku .and. try3055.gt.0) goto 3055
C if(sav15.ne.L_umaku .and. try3055.gt.0) goto 3055
C if(sav16.ne.L_emaku .and. try3055.gt.0) goto 3055
      Call PUMP2_HANDLER(deltat,I_elolu,I_alolu,R_odolu,
     & REAL(R_afolu,4),R_adolu,REAL(R_idolu,4),L_opolu,
     & L_umolu,L_isolu,L_osolu,L_ikolu,L_amolu,L_imolu,L_emolu
     &,L_omolu,L_ipolu,
     & L_ubolu,L_udolu,L_obolu,L_edolu,L_urolu,
     & L_asolu,L_ibolu,L_ebolu,L_okolu,I_ilolu,R_arolu,R_erolu
     &,L_etolu,
     & L_axomu,L_esolu,REAL(R8_elomu,8),L_epolu,REAL(R8_apolu
     &,8),R_usolu,
     & REAL(R_ololu,4),R_ulolu,REAL(R8_ekolu,8),R_akolu,R8_aromu
     &,R_atolu,R8_apolu,
     & REAL(R_efolu,4),REAL(R_ifolu,4))
C SRG_vlv.fgi(  78, 773):���������� ���������� ������� 2,20SRG10AN004
C label 3056  try3056=try3056-1
C sav1=R_atolu
C sav2=R_usolu
C sav3=L_opolu
C sav4=L_ipolu
C sav5=L_umolu
C sav6=R8_apolu
C sav7=R_ulolu
C sav8=I_ilolu
C sav9=I_elolu
C sav10=I_alolu
C sav11=I_ukolu
C sav12=L_okolu
C sav13=L_ikolu
C sav14=R_akolu
C sav15=L_udolu
C sav16=L_edolu
      Call PUMP2_HANDLER(deltat,I_elolu,I_alolu,R_odolu,
     & REAL(R_afolu,4),R_adolu,REAL(R_idolu,4),L_opolu,
     & L_umolu,L_isolu,L_osolu,L_ikolu,L_amolu,L_imolu,L_emolu
     &,L_omolu,L_ipolu,
     & L_ubolu,L_udolu,L_obolu,L_edolu,L_urolu,
     & L_asolu,L_ibolu,L_ebolu,L_okolu,I_ilolu,R_arolu,R_erolu
     &,L_etolu,
     & L_axomu,L_esolu,REAL(R8_elomu,8),L_epolu,REAL(R8_apolu
     &,8),R_usolu,
     & REAL(R_ololu,4),R_ulolu,REAL(R8_ekolu,8),R_akolu,R8_aromu
     &,R_atolu,R8_apolu,
     & REAL(R_efolu,4),REAL(R_ifolu,4))
C SRG_vlv.fgi(  78, 773):recalc:���������� ���������� ������� 2,20SRG10AN004
C if(sav1.ne.R_atolu .and. try3056.gt.0) goto 3056
C if(sav2.ne.R_usolu .and. try3056.gt.0) goto 3056
C if(sav3.ne.L_opolu .and. try3056.gt.0) goto 3056
C if(sav4.ne.L_ipolu .and. try3056.gt.0) goto 3056
C if(sav5.ne.L_umolu .and. try3056.gt.0) goto 3056
C if(sav6.ne.R8_apolu .and. try3056.gt.0) goto 3056
C if(sav7.ne.R_ulolu .and. try3056.gt.0) goto 3056
C if(sav8.ne.I_ilolu .and. try3056.gt.0) goto 3056
C if(sav9.ne.I_elolu .and. try3056.gt.0) goto 3056
C if(sav10.ne.I_alolu .and. try3056.gt.0) goto 3056
C if(sav11.ne.I_ukolu .and. try3056.gt.0) goto 3056
C if(sav12.ne.L_okolu .and. try3056.gt.0) goto 3056
C if(sav13.ne.L_ikolu .and. try3056.gt.0) goto 3056
C if(sav14.ne.R_akolu .and. try3056.gt.0) goto 3056
C if(sav15.ne.L_udolu .and. try3056.gt.0) goto 3056
C if(sav16.ne.L_edolu .and. try3056.gt.0) goto 3056
      Call PUMP2_HANDLER(deltat,I_axufu,I_uvufu,R_isufu,
     & REAL(R_usufu,4),R_urufu,REAL(R_esufu,4),L_idaku,
     & L_obaku,L_ekaku,L_ikaku,L_evufu,L_uxufu,L_ebaku,L_abaku
     &,L_ibaku,L_edaku,
     & L_orufu,L_osufu,L_irufu,L_asufu,L_ofaku,
     & L_ufaku,L_erufu,L_arufu,L_ivufu,I_exufu,R_udaku,R_afaku
     &,L_alaku,
     & L_axomu,L_akaku,REAL(R8_elomu,8),L_adaku,REAL(R8_ubaku
     &,8),R_okaku,
     & REAL(R_ixufu,4),R_oxufu,REAL(R8_avufu,8),R_utufu,R8_aromu
     &,R_ukaku,R8_ubaku,
     & REAL(R_atufu,4),REAL(R_etufu,4))
C SRG_vlv.fgi(  78, 747):���������� ���������� ������� 2,20SRG10AN015
C label 3057  try3057=try3057-1
C sav1=R_ukaku
C sav2=R_okaku
C sav3=L_idaku
C sav4=L_edaku
C sav5=L_obaku
C sav6=R8_ubaku
C sav7=R_oxufu
C sav8=I_exufu
C sav9=I_axufu
C sav10=I_uvufu
C sav11=I_ovufu
C sav12=L_ivufu
C sav13=L_evufu
C sav14=R_utufu
C sav15=L_osufu
C sav16=L_asufu
      Call PUMP2_HANDLER(deltat,I_axufu,I_uvufu,R_isufu,
     & REAL(R_usufu,4),R_urufu,REAL(R_esufu,4),L_idaku,
     & L_obaku,L_ekaku,L_ikaku,L_evufu,L_uxufu,L_ebaku,L_abaku
     &,L_ibaku,L_edaku,
     & L_orufu,L_osufu,L_irufu,L_asufu,L_ofaku,
     & L_ufaku,L_erufu,L_arufu,L_ivufu,I_exufu,R_udaku,R_afaku
     &,L_alaku,
     & L_axomu,L_akaku,REAL(R8_elomu,8),L_adaku,REAL(R8_ubaku
     &,8),R_okaku,
     & REAL(R_ixufu,4),R_oxufu,REAL(R8_avufu,8),R_utufu,R8_aromu
     &,R_ukaku,R8_ubaku,
     & REAL(R_atufu,4),REAL(R_etufu,4))
C SRG_vlv.fgi(  78, 747):recalc:���������� ���������� ������� 2,20SRG10AN015
C if(sav1.ne.R_ukaku .and. try3057.gt.0) goto 3057
C if(sav2.ne.R_okaku .and. try3057.gt.0) goto 3057
C if(sav3.ne.L_idaku .and. try3057.gt.0) goto 3057
C if(sav4.ne.L_edaku .and. try3057.gt.0) goto 3057
C if(sav5.ne.L_obaku .and. try3057.gt.0) goto 3057
C if(sav6.ne.R8_ubaku .and. try3057.gt.0) goto 3057
C if(sav7.ne.R_oxufu .and. try3057.gt.0) goto 3057
C if(sav8.ne.I_exufu .and. try3057.gt.0) goto 3057
C if(sav9.ne.I_axufu .and. try3057.gt.0) goto 3057
C if(sav10.ne.I_uvufu .and. try3057.gt.0) goto 3057
C if(sav11.ne.I_ovufu .and. try3057.gt.0) goto 3057
C if(sav12.ne.L_ivufu .and. try3057.gt.0) goto 3057
C if(sav13.ne.L_evufu .and. try3057.gt.0) goto 3057
C if(sav14.ne.R_utufu .and. try3057.gt.0) goto 3057
C if(sav15.ne.L_osufu .and. try3057.gt.0) goto 3057
C if(sav16.ne.L_asufu .and. try3057.gt.0) goto 3057
      Call PUMP2_HANDLER(deltat,I_arilu,I_upilu,R_ililu,
     & REAL(R_ulilu,4),R_ukilu,REAL(R_elilu,4),L_itilu,
     & L_osilu,L_exilu,L_ixilu,L_epilu,L_urilu,L_esilu,L_asilu
     &,L_isilu,L_etilu,
     & L_okilu,L_olilu,L_ikilu,L_alilu,L_ovilu,
     & L_uvilu,L_ekilu,L_akilu,L_ipilu,I_erilu,R_utilu,R_avilu
     &,L_abolu,
     & L_axomu,L_axilu,REAL(R8_elomu,8),L_atilu,REAL(R8_usilu
     &,8),R_oxilu,
     & REAL(R_irilu,4),R_orilu,REAL(R8_apilu,8),R_umilu,R8_aromu
     &,R_uxilu,R8_usilu,
     & REAL(R_amilu,4),REAL(R_emilu,4))
C SRG_vlv.fgi(  92, 773):���������� ���������� ������� 2,20SRG10AN005
C label 3058  try3058=try3058-1
C sav1=R_uxilu
C sav2=R_oxilu
C sav3=L_itilu
C sav4=L_etilu
C sav5=L_osilu
C sav6=R8_usilu
C sav7=R_orilu
C sav8=I_erilu
C sav9=I_arilu
C sav10=I_upilu
C sav11=I_opilu
C sav12=L_ipilu
C sav13=L_epilu
C sav14=R_umilu
C sav15=L_olilu
C sav16=L_alilu
      Call PUMP2_HANDLER(deltat,I_arilu,I_upilu,R_ililu,
     & REAL(R_ulilu,4),R_ukilu,REAL(R_elilu,4),L_itilu,
     & L_osilu,L_exilu,L_ixilu,L_epilu,L_urilu,L_esilu,L_asilu
     &,L_isilu,L_etilu,
     & L_okilu,L_olilu,L_ikilu,L_alilu,L_ovilu,
     & L_uvilu,L_ekilu,L_akilu,L_ipilu,I_erilu,R_utilu,R_avilu
     &,L_abolu,
     & L_axomu,L_axilu,REAL(R8_elomu,8),L_atilu,REAL(R8_usilu
     &,8),R_oxilu,
     & REAL(R_irilu,4),R_orilu,REAL(R8_apilu,8),R_umilu,R8_aromu
     &,R_uxilu,R8_usilu,
     & REAL(R_amilu,4),REAL(R_emilu,4))
C SRG_vlv.fgi(  92, 773):recalc:���������� ���������� ������� 2,20SRG10AN005
C if(sav1.ne.R_uxilu .and. try3058.gt.0) goto 3058
C if(sav2.ne.R_oxilu .and. try3058.gt.0) goto 3058
C if(sav3.ne.L_itilu .and. try3058.gt.0) goto 3058
C if(sav4.ne.L_etilu .and. try3058.gt.0) goto 3058
C if(sav5.ne.L_osilu .and. try3058.gt.0) goto 3058
C if(sav6.ne.R8_usilu .and. try3058.gt.0) goto 3058
C if(sav7.ne.R_orilu .and. try3058.gt.0) goto 3058
C if(sav8.ne.I_erilu .and. try3058.gt.0) goto 3058
C if(sav9.ne.I_arilu .and. try3058.gt.0) goto 3058
C if(sav10.ne.I_upilu .and. try3058.gt.0) goto 3058
C if(sav11.ne.I_opilu .and. try3058.gt.0) goto 3058
C if(sav12.ne.L_ipilu .and. try3058.gt.0) goto 3058
C if(sav13.ne.L_epilu .and. try3058.gt.0) goto 3058
C if(sav14.ne.R_umilu .and. try3058.gt.0) goto 3058
C if(sav15.ne.L_olilu .and. try3058.gt.0) goto 3058
C if(sav16.ne.L_alilu .and. try3058.gt.0) goto 3058
      Call PUMP2_HANDLER(deltat,I_udufu,I_odufu,R_exofu,
     & REAL(R_oxofu,4),R_ovofu,REAL(R_axofu,4),L_elufu,
     & L_ikufu,L_apufu,L_epufu,L_adufu,L_ofufu,L_akufu,L_ufufu
     &,L_ekufu,L_alufu,
     & L_ivofu,L_ixofu,L_evofu,L_uvofu,L_imufu,
     & L_omufu,L_avofu,L_utofu,L_edufu,I_afufu,R_olufu,R_ulufu
     &,L_upufu,
     & L_axomu,L_umufu,REAL(R8_elomu,8),L_ukufu,REAL(R8_okufu
     &,8),R_ipufu,
     & REAL(R_efufu,4),R_ifufu,REAL(R8_ubufu,8),R_obufu,R8_aromu
     &,R_opufu,R8_okufu,
     & REAL(R_uxofu,4),REAL(R_abufu,4))
C SRG_vlv.fgi(  92, 747):���������� ���������� ������� 2,20SRG10AN016
C label 3059  try3059=try3059-1
C sav1=R_opufu
C sav2=R_ipufu
C sav3=L_elufu
C sav4=L_alufu
C sav5=L_ikufu
C sav6=R8_okufu
C sav7=R_ifufu
C sav8=I_afufu
C sav9=I_udufu
C sav10=I_odufu
C sav11=I_idufu
C sav12=L_edufu
C sav13=L_adufu
C sav14=R_obufu
C sav15=L_ixofu
C sav16=L_uvofu
      Call PUMP2_HANDLER(deltat,I_udufu,I_odufu,R_exofu,
     & REAL(R_oxofu,4),R_ovofu,REAL(R_axofu,4),L_elufu,
     & L_ikufu,L_apufu,L_epufu,L_adufu,L_ofufu,L_akufu,L_ufufu
     &,L_ekufu,L_alufu,
     & L_ivofu,L_ixofu,L_evofu,L_uvofu,L_imufu,
     & L_omufu,L_avofu,L_utofu,L_edufu,I_afufu,R_olufu,R_ulufu
     &,L_upufu,
     & L_axomu,L_umufu,REAL(R8_elomu,8),L_ukufu,REAL(R8_okufu
     &,8),R_ipufu,
     & REAL(R_efufu,4),R_ifufu,REAL(R8_ubufu,8),R_obufu,R8_aromu
     &,R_opufu,R8_okufu,
     & REAL(R_uxofu,4),REAL(R_abufu,4))
C SRG_vlv.fgi(  92, 747):recalc:���������� ���������� ������� 2,20SRG10AN016
C if(sav1.ne.R_opufu .and. try3059.gt.0) goto 3059
C if(sav2.ne.R_ipufu .and. try3059.gt.0) goto 3059
C if(sav3.ne.L_elufu .and. try3059.gt.0) goto 3059
C if(sav4.ne.L_alufu .and. try3059.gt.0) goto 3059
C if(sav5.ne.L_ikufu .and. try3059.gt.0) goto 3059
C if(sav6.ne.R8_okufu .and. try3059.gt.0) goto 3059
C if(sav7.ne.R_ifufu .and. try3059.gt.0) goto 3059
C if(sav8.ne.I_afufu .and. try3059.gt.0) goto 3059
C if(sav9.ne.I_udufu .and. try3059.gt.0) goto 3059
C if(sav10.ne.I_odufu .and. try3059.gt.0) goto 3059
C if(sav11.ne.I_idufu .and. try3059.gt.0) goto 3059
C if(sav12.ne.L_edufu .and. try3059.gt.0) goto 3059
C if(sav13.ne.L_adufu .and. try3059.gt.0) goto 3059
C if(sav14.ne.R_obufu .and. try3059.gt.0) goto 3059
C if(sav15.ne.L_ixofu .and. try3059.gt.0) goto 3059
C if(sav16.ne.L_uvofu .and. try3059.gt.0) goto 3059
      Call PUMP2_HANDLER(deltat,I_utelu,I_otelu,R_erelu,
     & REAL(R_orelu,4),R_opelu,REAL(R_arelu,4),L_ebilu,
     & L_ixelu,L_afilu,L_efilu,L_atelu,L_ovelu,L_axelu,L_uvelu
     &,L_exelu,L_abilu,
     & L_ipelu,L_irelu,L_epelu,L_upelu,L_idilu,
     & L_odilu,L_apelu,L_umelu,L_etelu,I_avelu,R_obilu,R_ubilu
     &,L_ufilu,
     & L_axomu,L_udilu,REAL(R8_elomu,8),L_uxelu,REAL(R8_oxelu
     &,8),R_ifilu,
     & REAL(R_evelu,4),R_ivelu,REAL(R8_uselu,8),R_oselu,R8_aromu
     &,R_ofilu,R8_oxelu,
     & REAL(R_urelu,4),REAL(R_aselu,4))
C SRG_vlv.fgi( 106, 773):���������� ���������� ������� 2,20SRG10AN006
C label 3060  try3060=try3060-1
C sav1=R_ofilu
C sav2=R_ifilu
C sav3=L_ebilu
C sav4=L_abilu
C sav5=L_ixelu
C sav6=R8_oxelu
C sav7=R_ivelu
C sav8=I_avelu
C sav9=I_utelu
C sav10=I_otelu
C sav11=I_itelu
C sav12=L_etelu
C sav13=L_atelu
C sav14=R_oselu
C sav15=L_irelu
C sav16=L_upelu
      Call PUMP2_HANDLER(deltat,I_utelu,I_otelu,R_erelu,
     & REAL(R_orelu,4),R_opelu,REAL(R_arelu,4),L_ebilu,
     & L_ixelu,L_afilu,L_efilu,L_atelu,L_ovelu,L_axelu,L_uvelu
     &,L_exelu,L_abilu,
     & L_ipelu,L_irelu,L_epelu,L_upelu,L_idilu,
     & L_odilu,L_apelu,L_umelu,L_etelu,I_avelu,R_obilu,R_ubilu
     &,L_ufilu,
     & L_axomu,L_udilu,REAL(R8_elomu,8),L_uxelu,REAL(R8_oxelu
     &,8),R_ifilu,
     & REAL(R_evelu,4),R_ivelu,REAL(R8_uselu,8),R_oselu,R8_aromu
     &,R_ofilu,R8_oxelu,
     & REAL(R_urelu,4),REAL(R_aselu,4))
C SRG_vlv.fgi( 106, 773):recalc:���������� ���������� ������� 2,20SRG10AN006
C if(sav1.ne.R_ofilu .and. try3060.gt.0) goto 3060
C if(sav2.ne.R_ifilu .and. try3060.gt.0) goto 3060
C if(sav3.ne.L_ebilu .and. try3060.gt.0) goto 3060
C if(sav4.ne.L_abilu .and. try3060.gt.0) goto 3060
C if(sav5.ne.L_ixelu .and. try3060.gt.0) goto 3060
C if(sav6.ne.R8_oxelu .and. try3060.gt.0) goto 3060
C if(sav7.ne.R_ivelu .and. try3060.gt.0) goto 3060
C if(sav8.ne.I_avelu .and. try3060.gt.0) goto 3060
C if(sav9.ne.I_utelu .and. try3060.gt.0) goto 3060
C if(sav10.ne.I_otelu .and. try3060.gt.0) goto 3060
C if(sav11.ne.I_itelu .and. try3060.gt.0) goto 3060
C if(sav12.ne.L_etelu .and. try3060.gt.0) goto 3060
C if(sav13.ne.L_atelu .and. try3060.gt.0) goto 3060
C if(sav14.ne.R_oselu .and. try3060.gt.0) goto 3060
C if(sav15.ne.L_irelu .and. try3060.gt.0) goto 3060
C if(sav16.ne.L_upelu .and. try3060.gt.0) goto 3060
      Call PUMP2_HANDLER(deltat,I_olofu,I_ilofu,R_afofu,
     & REAL(R_ifofu,4),R_idofu,REAL(R_udofu,4),L_arofu,
     & L_epofu,L_usofu,L_atofu,L_ukofu,L_imofu,L_umofu,L_omofu
     &,L_apofu,L_upofu,
     & L_edofu,L_efofu,L_adofu,L_odofu,L_esofu,
     & L_isofu,L_ubofu,L_obofu,L_alofu,I_ulofu,R_irofu,R_orofu
     &,L_otofu,
     & L_axomu,L_osofu,REAL(R8_elomu,8),L_opofu,REAL(R8_ipofu
     &,8),R_etofu,
     & REAL(R_amofu,4),R_emofu,REAL(R8_okofu,8),R_ikofu,R8_aromu
     &,R_itofu,R8_ipofu,
     & REAL(R_ofofu,4),REAL(R_ufofu,4))
C SRG_vlv.fgi( 106, 747):���������� ���������� ������� 2,20SRG10AN017
C label 3061  try3061=try3061-1
C sav1=R_itofu
C sav2=R_etofu
C sav3=L_arofu
C sav4=L_upofu
C sav5=L_epofu
C sav6=R8_ipofu
C sav7=R_emofu
C sav8=I_ulofu
C sav9=I_olofu
C sav10=I_ilofu
C sav11=I_elofu
C sav12=L_alofu
C sav13=L_ukofu
C sav14=R_ikofu
C sav15=L_efofu
C sav16=L_odofu
      Call PUMP2_HANDLER(deltat,I_olofu,I_ilofu,R_afofu,
     & REAL(R_ifofu,4),R_idofu,REAL(R_udofu,4),L_arofu,
     & L_epofu,L_usofu,L_atofu,L_ukofu,L_imofu,L_umofu,L_omofu
     &,L_apofu,L_upofu,
     & L_edofu,L_efofu,L_adofu,L_odofu,L_esofu,
     & L_isofu,L_ubofu,L_obofu,L_alofu,I_ulofu,R_irofu,R_orofu
     &,L_otofu,
     & L_axomu,L_osofu,REAL(R8_elomu,8),L_opofu,REAL(R8_ipofu
     &,8),R_etofu,
     & REAL(R_amofu,4),R_emofu,REAL(R8_okofu,8),R_ikofu,R8_aromu
     &,R_itofu,R8_ipofu,
     & REAL(R_ofofu,4),REAL(R_ufofu,4))
C SRG_vlv.fgi( 106, 747):recalc:���������� ���������� ������� 2,20SRG10AN017
C if(sav1.ne.R_itofu .and. try3061.gt.0) goto 3061
C if(sav2.ne.R_etofu .and. try3061.gt.0) goto 3061
C if(sav3.ne.L_arofu .and. try3061.gt.0) goto 3061
C if(sav4.ne.L_upofu .and. try3061.gt.0) goto 3061
C if(sav5.ne.L_epofu .and. try3061.gt.0) goto 3061
C if(sav6.ne.R8_ipofu .and. try3061.gt.0) goto 3061
C if(sav7.ne.R_emofu .and. try3061.gt.0) goto 3061
C if(sav8.ne.I_ulofu .and. try3061.gt.0) goto 3061
C if(sav9.ne.I_olofu .and. try3061.gt.0) goto 3061
C if(sav10.ne.I_ilofu .and. try3061.gt.0) goto 3061
C if(sav11.ne.I_elofu .and. try3061.gt.0) goto 3061
C if(sav12.ne.L_alofu .and. try3061.gt.0) goto 3061
C if(sav13.ne.L_ukofu .and. try3061.gt.0) goto 3061
C if(sav14.ne.R_ikofu .and. try3061.gt.0) goto 3061
C if(sav15.ne.L_efofu .and. try3061.gt.0) goto 3061
C if(sav16.ne.L_odofu .and. try3061.gt.0) goto 3061
      Call PUMP2_HANDLER(deltat,I_obelu,I_ibelu,R_avalu,
     & REAL(R_ivalu,4),R_italu,REAL(R_utalu,4),L_akelu,
     & L_efelu,L_ulelu,L_amelu,L_uxalu,L_idelu,L_udelu,L_odelu
     &,L_afelu,L_ufelu,
     & L_etalu,L_evalu,L_atalu,L_otalu,L_elelu,
     & L_ilelu,L_usalu,L_osalu,L_abelu,I_ubelu,R_ikelu,R_okelu
     &,L_omelu,
     & L_axomu,L_olelu,REAL(R8_elomu,8),L_ofelu,REAL(R8_ifelu
     &,8),R_emelu,
     & REAL(R_adelu,4),R_edelu,REAL(R8_oxalu,8),R_ixalu,R8_aromu
     &,R_imelu,R8_ifelu,
     & REAL(R_ovalu,4),REAL(R_uvalu,4))
C SRG_vlv.fgi( 120, 773):���������� ���������� ������� 2,20SRG10AN007
C label 3062  try3062=try3062-1
C sav1=R_imelu
C sav2=R_emelu
C sav3=L_akelu
C sav4=L_ufelu
C sav5=L_efelu
C sav6=R8_ifelu
C sav7=R_edelu
C sav8=I_ubelu
C sav9=I_obelu
C sav10=I_ibelu
C sav11=I_ebelu
C sav12=L_abelu
C sav13=L_uxalu
C sav14=R_ixalu
C sav15=L_evalu
C sav16=L_otalu
      Call PUMP2_HANDLER(deltat,I_obelu,I_ibelu,R_avalu,
     & REAL(R_ivalu,4),R_italu,REAL(R_utalu,4),L_akelu,
     & L_efelu,L_ulelu,L_amelu,L_uxalu,L_idelu,L_udelu,L_odelu
     &,L_afelu,L_ufelu,
     & L_etalu,L_evalu,L_atalu,L_otalu,L_elelu,
     & L_ilelu,L_usalu,L_osalu,L_abelu,I_ubelu,R_ikelu,R_okelu
     &,L_omelu,
     & L_axomu,L_olelu,REAL(R8_elomu,8),L_ofelu,REAL(R8_ifelu
     &,8),R_emelu,
     & REAL(R_adelu,4),R_edelu,REAL(R8_oxalu,8),R_ixalu,R8_aromu
     &,R_imelu,R8_ifelu,
     & REAL(R_ovalu,4),REAL(R_uvalu,4))
C SRG_vlv.fgi( 120, 773):recalc:���������� ���������� ������� 2,20SRG10AN007
C if(sav1.ne.R_imelu .and. try3062.gt.0) goto 3062
C if(sav2.ne.R_emelu .and. try3062.gt.0) goto 3062
C if(sav3.ne.L_akelu .and. try3062.gt.0) goto 3062
C if(sav4.ne.L_ufelu .and. try3062.gt.0) goto 3062
C if(sav5.ne.L_efelu .and. try3062.gt.0) goto 3062
C if(sav6.ne.R8_ifelu .and. try3062.gt.0) goto 3062
C if(sav7.ne.R_edelu .and. try3062.gt.0) goto 3062
C if(sav8.ne.I_ubelu .and. try3062.gt.0) goto 3062
C if(sav9.ne.I_obelu .and. try3062.gt.0) goto 3062
C if(sav10.ne.I_ibelu .and. try3062.gt.0) goto 3062
C if(sav11.ne.I_ebelu .and. try3062.gt.0) goto 3062
C if(sav12.ne.L_abelu .and. try3062.gt.0) goto 3062
C if(sav13.ne.L_uxalu .and. try3062.gt.0) goto 3062
C if(sav14.ne.R_ixalu .and. try3062.gt.0) goto 3062
C if(sav15.ne.L_evalu .and. try3062.gt.0) goto 3062
C if(sav16.ne.L_otalu .and. try3062.gt.0) goto 3062
      Call PUMP2_HANDLER(deltat,I_irifu,I_erifu,R_ulifu,
     & REAL(R_emifu,4),R_elifu,REAL(R_olifu,4),L_utifu,
     & L_atifu,L_oxifu,L_uxifu,L_opifu,L_esifu,L_osifu,L_isifu
     &,L_usifu,L_otifu,
     & L_alifu,L_amifu,L_ukifu,L_ilifu,L_axifu,
     & L_exifu,L_okifu,L_ikifu,L_upifu,I_orifu,R_evifu,R_ivifu
     &,L_ibofu,
     & L_axomu,L_ixifu,REAL(R8_elomu,8),L_itifu,REAL(R8_etifu
     &,8),R_abofu,
     & REAL(R_urifu,4),R_asifu,REAL(R8_ipifu,8),R_epifu,R8_aromu
     &,R_ebofu,R8_etifu,
     & REAL(R_imifu,4),REAL(R_omifu,4))
C SRG_vlv.fgi( 120, 747):���������� ���������� ������� 2,20SRG10AN018
C label 3063  try3063=try3063-1
C sav1=R_ebofu
C sav2=R_abofu
C sav3=L_utifu
C sav4=L_otifu
C sav5=L_atifu
C sav6=R8_etifu
C sav7=R_asifu
C sav8=I_orifu
C sav9=I_irifu
C sav10=I_erifu
C sav11=I_arifu
C sav12=L_upifu
C sav13=L_opifu
C sav14=R_epifu
C sav15=L_amifu
C sav16=L_ilifu
      Call PUMP2_HANDLER(deltat,I_irifu,I_erifu,R_ulifu,
     & REAL(R_emifu,4),R_elifu,REAL(R_olifu,4),L_utifu,
     & L_atifu,L_oxifu,L_uxifu,L_opifu,L_esifu,L_osifu,L_isifu
     &,L_usifu,L_otifu,
     & L_alifu,L_amifu,L_ukifu,L_ilifu,L_axifu,
     & L_exifu,L_okifu,L_ikifu,L_upifu,I_orifu,R_evifu,R_ivifu
     &,L_ibofu,
     & L_axomu,L_ixifu,REAL(R8_elomu,8),L_itifu,REAL(R8_etifu
     &,8),R_abofu,
     & REAL(R_urifu,4),R_asifu,REAL(R8_ipifu,8),R_epifu,R8_aromu
     &,R_ebofu,R8_etifu,
     & REAL(R_imifu,4),REAL(R_omifu,4))
C SRG_vlv.fgi( 120, 747):recalc:���������� ���������� ������� 2,20SRG10AN018
C if(sav1.ne.R_ebofu .and. try3063.gt.0) goto 3063
C if(sav2.ne.R_abofu .and. try3063.gt.0) goto 3063
C if(sav3.ne.L_utifu .and. try3063.gt.0) goto 3063
C if(sav4.ne.L_otifu .and. try3063.gt.0) goto 3063
C if(sav5.ne.L_atifu .and. try3063.gt.0) goto 3063
C if(sav6.ne.R8_etifu .and. try3063.gt.0) goto 3063
C if(sav7.ne.R_asifu .and. try3063.gt.0) goto 3063
C if(sav8.ne.I_orifu .and. try3063.gt.0) goto 3063
C if(sav9.ne.I_irifu .and. try3063.gt.0) goto 3063
C if(sav10.ne.I_erifu .and. try3063.gt.0) goto 3063
C if(sav11.ne.I_arifu .and. try3063.gt.0) goto 3063
C if(sav12.ne.L_upifu .and. try3063.gt.0) goto 3063
C if(sav13.ne.L_opifu .and. try3063.gt.0) goto 3063
C if(sav14.ne.R_epifu .and. try3063.gt.0) goto 3063
C if(sav15.ne.L_amifu .and. try3063.gt.0) goto 3063
C if(sav16.ne.L_ilifu .and. try3063.gt.0) goto 3063
      Call PUMP2_HANDLER(deltat,I_ikalu,I_ekalu,R_ubalu,
     & REAL(R_edalu,4),R_ebalu,REAL(R_obalu,4),L_umalu,
     & L_amalu,L_oralu,L_uralu,L_ofalu,L_elalu,L_olalu,L_ilalu
     &,L_ulalu,L_omalu,
     & L_abalu,L_adalu,L_uxuku,L_ibalu,L_aralu,
     & L_eralu,L_oxuku,L_ixuku,L_ufalu,I_okalu,R_epalu,R_ipalu
     &,L_isalu,
     & L_axomu,L_iralu,REAL(R8_elomu,8),L_imalu,REAL(R8_emalu
     &,8),R_asalu,
     & REAL(R_ukalu,4),R_alalu,REAL(R8_ifalu,8),R_efalu,R8_aromu
     &,R_esalu,R8_emalu,
     & REAL(R_idalu,4),REAL(R_odalu,4))
C SRG_vlv.fgi( 134, 773):���������� ���������� ������� 2,20SRG10AN008
C label 3064  try3064=try3064-1
C sav1=R_esalu
C sav2=R_asalu
C sav3=L_umalu
C sav4=L_omalu
C sav5=L_amalu
C sav6=R8_emalu
C sav7=R_alalu
C sav8=I_okalu
C sav9=I_ikalu
C sav10=I_ekalu
C sav11=I_akalu
C sav12=L_ufalu
C sav13=L_ofalu
C sav14=R_efalu
C sav15=L_adalu
C sav16=L_ibalu
      Call PUMP2_HANDLER(deltat,I_ikalu,I_ekalu,R_ubalu,
     & REAL(R_edalu,4),R_ebalu,REAL(R_obalu,4),L_umalu,
     & L_amalu,L_oralu,L_uralu,L_ofalu,L_elalu,L_olalu,L_ilalu
     &,L_ulalu,L_omalu,
     & L_abalu,L_adalu,L_uxuku,L_ibalu,L_aralu,
     & L_eralu,L_oxuku,L_ixuku,L_ufalu,I_okalu,R_epalu,R_ipalu
     &,L_isalu,
     & L_axomu,L_iralu,REAL(R8_elomu,8),L_imalu,REAL(R8_emalu
     &,8),R_asalu,
     & REAL(R_ukalu,4),R_alalu,REAL(R8_ifalu,8),R_efalu,R8_aromu
     &,R_esalu,R8_emalu,
     & REAL(R_idalu,4),REAL(R_odalu,4))
C SRG_vlv.fgi( 134, 773):recalc:���������� ���������� ������� 2,20SRG10AN008
C if(sav1.ne.R_esalu .and. try3064.gt.0) goto 3064
C if(sav2.ne.R_asalu .and. try3064.gt.0) goto 3064
C if(sav3.ne.L_umalu .and. try3064.gt.0) goto 3064
C if(sav4.ne.L_omalu .and. try3064.gt.0) goto 3064
C if(sav5.ne.L_amalu .and. try3064.gt.0) goto 3064
C if(sav6.ne.R8_emalu .and. try3064.gt.0) goto 3064
C if(sav7.ne.R_alalu .and. try3064.gt.0) goto 3064
C if(sav8.ne.I_okalu .and. try3064.gt.0) goto 3064
C if(sav9.ne.I_ikalu .and. try3064.gt.0) goto 3064
C if(sav10.ne.I_ekalu .and. try3064.gt.0) goto 3064
C if(sav11.ne.I_akalu .and. try3064.gt.0) goto 3064
C if(sav12.ne.L_ufalu .and. try3064.gt.0) goto 3064
C if(sav13.ne.L_ofalu .and. try3064.gt.0) goto 3064
C if(sav14.ne.R_efalu .and. try3064.gt.0) goto 3064
C if(sav15.ne.L_adalu .and. try3064.gt.0) goto 3064
C if(sav16.ne.L_ibalu .and. try3064.gt.0) goto 3064
      Call PUMP2_HANDLER(deltat,I_evefu,I_avefu,R_orefu,
     & REAL(R_asefu,4),R_arefu,REAL(R_irefu,4),L_obifu,
     & L_uxefu,L_ififu,L_ofifu,L_itefu,L_axefu,L_ixefu,L_exefu
     &,L_oxefu,L_ibifu,
     & L_upefu,L_urefu,L_opefu,L_erefu,L_udifu,
     & L_afifu,L_ipefu,L_epefu,L_otefu,I_ivefu,R_adifu,R_edifu
     &,L_ekifu,
     & L_axomu,L_efifu,REAL(R8_elomu,8),L_ebifu,REAL(R8_abifu
     &,8),R_ufifu,
     & REAL(R_ovefu,4),R_uvefu,REAL(R8_etefu,8),R_atefu,R8_aromu
     &,R_akifu,R8_abifu,
     & REAL(R_esefu,4),REAL(R_isefu,4))
C SRG_vlv.fgi( 134, 747):���������� ���������� ������� 2,20SRG10AN019
C label 3065  try3065=try3065-1
C sav1=R_akifu
C sav2=R_ufifu
C sav3=L_obifu
C sav4=L_ibifu
C sav5=L_uxefu
C sav6=R8_abifu
C sav7=R_uvefu
C sav8=I_ivefu
C sav9=I_evefu
C sav10=I_avefu
C sav11=I_utefu
C sav12=L_otefu
C sav13=L_itefu
C sav14=R_atefu
C sav15=L_urefu
C sav16=L_erefu
      Call PUMP2_HANDLER(deltat,I_evefu,I_avefu,R_orefu,
     & REAL(R_asefu,4),R_arefu,REAL(R_irefu,4),L_obifu,
     & L_uxefu,L_ififu,L_ofifu,L_itefu,L_axefu,L_ixefu,L_exefu
     &,L_oxefu,L_ibifu,
     & L_upefu,L_urefu,L_opefu,L_erefu,L_udifu,
     & L_afifu,L_ipefu,L_epefu,L_otefu,I_ivefu,R_adifu,R_edifu
     &,L_ekifu,
     & L_axomu,L_efifu,REAL(R8_elomu,8),L_ebifu,REAL(R8_abifu
     &,8),R_ufifu,
     & REAL(R_ovefu,4),R_uvefu,REAL(R8_etefu,8),R_atefu,R8_aromu
     &,R_akifu,R8_abifu,
     & REAL(R_esefu,4),REAL(R_isefu,4))
C SRG_vlv.fgi( 134, 747):recalc:���������� ���������� ������� 2,20SRG10AN019
C if(sav1.ne.R_akifu .and. try3065.gt.0) goto 3065
C if(sav2.ne.R_ufifu .and. try3065.gt.0) goto 3065
C if(sav3.ne.L_obifu .and. try3065.gt.0) goto 3065
C if(sav4.ne.L_ibifu .and. try3065.gt.0) goto 3065
C if(sav5.ne.L_uxefu .and. try3065.gt.0) goto 3065
C if(sav6.ne.R8_abifu .and. try3065.gt.0) goto 3065
C if(sav7.ne.R_uvefu .and. try3065.gt.0) goto 3065
C if(sav8.ne.I_ivefu .and. try3065.gt.0) goto 3065
C if(sav9.ne.I_evefu .and. try3065.gt.0) goto 3065
C if(sav10.ne.I_avefu .and. try3065.gt.0) goto 3065
C if(sav11.ne.I_utefu .and. try3065.gt.0) goto 3065
C if(sav12.ne.L_otefu .and. try3065.gt.0) goto 3065
C if(sav13.ne.L_itefu .and. try3065.gt.0) goto 3065
C if(sav14.ne.R_atefu .and. try3065.gt.0) goto 3065
C if(sav15.ne.L_urefu .and. try3065.gt.0) goto 3065
C if(sav16.ne.L_erefu .and. try3065.gt.0) goto 3065
      Call PUMP2_HANDLER(deltat,I_epuku,I_apuku,R_okuku,
     & REAL(R_aluku,4),R_akuku,REAL(R_ikuku,4),L_osuku,
     & L_uruku,L_ivuku,L_ovuku,L_imuku,L_aruku,L_iruku,L_eruku
     &,L_oruku,L_isuku,
     & L_ufuku,L_ukuku,L_ofuku,L_ekuku,L_utuku,
     & L_avuku,L_ifuku,L_efuku,L_omuku,I_ipuku,R_atuku,R_etuku
     &,L_exuku,
     & L_axomu,L_evuku,REAL(R8_elomu,8),L_esuku,REAL(R8_asuku
     &,8),R_uvuku,
     & REAL(R_opuku,4),R_upuku,REAL(R8_emuku,8),R_amuku,R8_aromu
     &,R_axuku,R8_asuku,
     & REAL(R_eluku,4),REAL(R_iluku,4))
C SRG_vlv.fgi( 148, 773):���������� ���������� ������� 2,20SRG10AN009
C label 3066  try3066=try3066-1
C sav1=R_axuku
C sav2=R_uvuku
C sav3=L_osuku
C sav4=L_isuku
C sav5=L_uruku
C sav6=R8_asuku
C sav7=R_upuku
C sav8=I_ipuku
C sav9=I_epuku
C sav10=I_apuku
C sav11=I_umuku
C sav12=L_omuku
C sav13=L_imuku
C sav14=R_amuku
C sav15=L_ukuku
C sav16=L_ekuku
      Call PUMP2_HANDLER(deltat,I_epuku,I_apuku,R_okuku,
     & REAL(R_aluku,4),R_akuku,REAL(R_ikuku,4),L_osuku,
     & L_uruku,L_ivuku,L_ovuku,L_imuku,L_aruku,L_iruku,L_eruku
     &,L_oruku,L_isuku,
     & L_ufuku,L_ukuku,L_ofuku,L_ekuku,L_utuku,
     & L_avuku,L_ifuku,L_efuku,L_omuku,I_ipuku,R_atuku,R_etuku
     &,L_exuku,
     & L_axomu,L_evuku,REAL(R8_elomu,8),L_esuku,REAL(R8_asuku
     &,8),R_uvuku,
     & REAL(R_opuku,4),R_upuku,REAL(R8_emuku,8),R_amuku,R8_aromu
     &,R_axuku,R8_asuku,
     & REAL(R_eluku,4),REAL(R_iluku,4))
C SRG_vlv.fgi( 148, 773):recalc:���������� ���������� ������� 2,20SRG10AN009
C if(sav1.ne.R_axuku .and. try3066.gt.0) goto 3066
C if(sav2.ne.R_uvuku .and. try3066.gt.0) goto 3066
C if(sav3.ne.L_osuku .and. try3066.gt.0) goto 3066
C if(sav4.ne.L_isuku .and. try3066.gt.0) goto 3066
C if(sav5.ne.L_uruku .and. try3066.gt.0) goto 3066
C if(sav6.ne.R8_asuku .and. try3066.gt.0) goto 3066
C if(sav7.ne.R_upuku .and. try3066.gt.0) goto 3066
C if(sav8.ne.I_ipuku .and. try3066.gt.0) goto 3066
C if(sav9.ne.I_epuku .and. try3066.gt.0) goto 3066
C if(sav10.ne.I_apuku .and. try3066.gt.0) goto 3066
C if(sav11.ne.I_umuku .and. try3066.gt.0) goto 3066
C if(sav12.ne.L_omuku .and. try3066.gt.0) goto 3066
C if(sav13.ne.L_imuku .and. try3066.gt.0) goto 3066
C if(sav14.ne.R_amuku .and. try3066.gt.0) goto 3066
C if(sav15.ne.L_ukuku .and. try3066.gt.0) goto 3066
C if(sav16.ne.L_ekuku .and. try3066.gt.0) goto 3066
      Call PUMP2_HANDLER(deltat,I_atoku,I_usoku,R_ipoku,
     & REAL(R_upoku,4),R_umoku,REAL(R_epoku,4),L_ixoku,
     & L_ovoku,L_eduku,L_iduku,L_esoku,L_utoku,L_evoku,L_avoku
     &,L_ivoku,L_exoku,
     & L_omoku,L_opoku,L_imoku,L_apoku,L_obuku,
     & L_ubuku,L_emoku,L_amoku,L_isoku,I_etoku,R_uxoku,R_abuku
     &,L_afuku,
     & L_axomu,L_aduku,REAL(R8_elomu,8),L_axoku,REAL(R8_uvoku
     &,8),R_oduku,
     & REAL(R_itoku,4),R_otoku,REAL(R8_asoku,8),R_uroku,R8_aromu
     &,R_uduku,R8_uvoku,
     & REAL(R_aroku,4),REAL(R_eroku,4))
C SRG_vlv.fgi( 162, 773):���������� ���������� ������� 2,20SRG10AN010
C label 3067  try3067=try3067-1
C sav1=R_uduku
C sav2=R_oduku
C sav3=L_ixoku
C sav4=L_exoku
C sav5=L_ovoku
C sav6=R8_uvoku
C sav7=R_otoku
C sav8=I_etoku
C sav9=I_atoku
C sav10=I_usoku
C sav11=I_osoku
C sav12=L_isoku
C sav13=L_esoku
C sav14=R_uroku
C sav15=L_opoku
C sav16=L_apoku
      Call PUMP2_HANDLER(deltat,I_atoku,I_usoku,R_ipoku,
     & REAL(R_upoku,4),R_umoku,REAL(R_epoku,4),L_ixoku,
     & L_ovoku,L_eduku,L_iduku,L_esoku,L_utoku,L_evoku,L_avoku
     &,L_ivoku,L_exoku,
     & L_omoku,L_opoku,L_imoku,L_apoku,L_obuku,
     & L_ubuku,L_emoku,L_amoku,L_isoku,I_etoku,R_uxoku,R_abuku
     &,L_afuku,
     & L_axomu,L_aduku,REAL(R8_elomu,8),L_axoku,REAL(R8_uvoku
     &,8),R_oduku,
     & REAL(R_itoku,4),R_otoku,REAL(R8_asoku,8),R_uroku,R8_aromu
     &,R_uduku,R8_uvoku,
     & REAL(R_aroku,4),REAL(R_eroku,4))
C SRG_vlv.fgi( 162, 773):recalc:���������� ���������� ������� 2,20SRG10AN010
C if(sav1.ne.R_uduku .and. try3067.gt.0) goto 3067
C if(sav2.ne.R_oduku .and. try3067.gt.0) goto 3067
C if(sav3.ne.L_ixoku .and. try3067.gt.0) goto 3067
C if(sav4.ne.L_exoku .and. try3067.gt.0) goto 3067
C if(sav5.ne.L_ovoku .and. try3067.gt.0) goto 3067
C if(sav6.ne.R8_uvoku .and. try3067.gt.0) goto 3067
C if(sav7.ne.R_otoku .and. try3067.gt.0) goto 3067
C if(sav8.ne.I_etoku .and. try3067.gt.0) goto 3067
C if(sav9.ne.I_atoku .and. try3067.gt.0) goto 3067
C if(sav10.ne.I_usoku .and. try3067.gt.0) goto 3067
C if(sav11.ne.I_osoku .and. try3067.gt.0) goto 3067
C if(sav12.ne.L_isoku .and. try3067.gt.0) goto 3067
C if(sav13.ne.L_esoku .and. try3067.gt.0) goto 3067
C if(sav14.ne.R_uroku .and. try3067.gt.0) goto 3067
C if(sav15.ne.L_opoku .and. try3067.gt.0) goto 3067
C if(sav16.ne.L_apoku .and. try3067.gt.0) goto 3067
      Call PUMP2_HANDLER(deltat,I_uxiku,I_oxiku,R_etiku,
     & REAL(R_otiku,4),R_osiku,REAL(R_atiku,4),L_efoku,
     & L_idoku,L_aloku,L_eloku,L_axiku,L_oboku,L_adoku,L_uboku
     &,L_edoku,L_afoku,
     & L_isiku,L_itiku,L_esiku,L_usiku,L_ikoku,
     & L_okoku,L_asiku,L_uriku,L_exiku,I_aboku,R_ofoku,R_ufoku
     &,L_uloku,
     & L_axomu,L_ukoku,REAL(R8_elomu,8),L_udoku,REAL(R8_odoku
     &,8),R_iloku,
     & REAL(R_eboku,4),R_iboku,REAL(R8_uviku,8),R_oviku,R8_aromu
     &,R_oloku,R8_odoku,
     & REAL(R_utiku,4),REAL(R_aviku,4))
C SRG_vlv.fgi( 176, 773):���������� ���������� ������� 2,20SRG10AN011
C label 3068  try3068=try3068-1
C sav1=R_oloku
C sav2=R_iloku
C sav3=L_efoku
C sav4=L_afoku
C sav5=L_idoku
C sav6=R8_odoku
C sav7=R_iboku
C sav8=I_aboku
C sav9=I_uxiku
C sav10=I_oxiku
C sav11=I_ixiku
C sav12=L_exiku
C sav13=L_axiku
C sav14=R_oviku
C sav15=L_itiku
C sav16=L_usiku
      Call PUMP2_HANDLER(deltat,I_uxiku,I_oxiku,R_etiku,
     & REAL(R_otiku,4),R_osiku,REAL(R_atiku,4),L_efoku,
     & L_idoku,L_aloku,L_eloku,L_axiku,L_oboku,L_adoku,L_uboku
     &,L_edoku,L_afoku,
     & L_isiku,L_itiku,L_esiku,L_usiku,L_ikoku,
     & L_okoku,L_asiku,L_uriku,L_exiku,I_aboku,R_ofoku,R_ufoku
     &,L_uloku,
     & L_axomu,L_ukoku,REAL(R8_elomu,8),L_udoku,REAL(R8_odoku
     &,8),R_iloku,
     & REAL(R_eboku,4),R_iboku,REAL(R8_uviku,8),R_oviku,R8_aromu
     &,R_oloku,R8_odoku,
     & REAL(R_utiku,4),REAL(R_aviku,4))
C SRG_vlv.fgi( 176, 773):recalc:���������� ���������� ������� 2,20SRG10AN011
C if(sav1.ne.R_oloku .and. try3068.gt.0) goto 3068
C if(sav2.ne.R_iloku .and. try3068.gt.0) goto 3068
C if(sav3.ne.L_efoku .and. try3068.gt.0) goto 3068
C if(sav4.ne.L_afoku .and. try3068.gt.0) goto 3068
C if(sav5.ne.L_idoku .and. try3068.gt.0) goto 3068
C if(sav6.ne.R8_odoku .and. try3068.gt.0) goto 3068
C if(sav7.ne.R_iboku .and. try3068.gt.0) goto 3068
C if(sav8.ne.I_aboku .and. try3068.gt.0) goto 3068
C if(sav9.ne.I_uxiku .and. try3068.gt.0) goto 3068
C if(sav10.ne.I_oxiku .and. try3068.gt.0) goto 3068
C if(sav11.ne.I_ixiku .and. try3068.gt.0) goto 3068
C if(sav12.ne.L_exiku .and. try3068.gt.0) goto 3068
C if(sav13.ne.L_axiku .and. try3068.gt.0) goto 3068
C if(sav14.ne.R_oviku .and. try3068.gt.0) goto 3068
C if(sav15.ne.L_itiku .and. try3068.gt.0) goto 3068
C if(sav16.ne.L_usiku .and. try3068.gt.0) goto 3068
      !{
      Call KLAPAN_MAN(deltat,REAL(R_urive,4),R8_omive,I_avive
     &,C8_ipive,I_ivive,R_erive,
     & R_arive,R_okive,REAL(R_alive,4),R_ulive,
     & REAL(R_emive,4),R_elive,REAL(R_olive,4),I_otive,
     & I_ovive,I_evive,I_itive,L_imive,L_axive,L_above,L_amive
     &,
     & L_ilive,REAL(R_opive,4),L_apive,L_umive,L_ixive,
     & L_ukive,L_upive,L_obove,L_irive,L_orive,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(196),L_ekive,L_(197),L_oviru
     &,L_exive,I_uvive,L_ebove,
     & R_etive,REAL(R_epive,4),L_ibove,L_ikive,L_ubove,L_uriru
     &,L_asive,L_esive,
     & L_isive,L_usive,L_ative,L_osive)
      !}

      if(L_ative.or.L_usive.or.L_osive.or.L_isive.or.L_esive.or.L_asive
     &) then      
                  I_utive = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_utive = z'40000000'
      endif
C SRG_vlv.fgi( 422, 402):���� ���������� ��������,20SRG10AA283
C label 3069  try3069=try3069-1
      L_iviru=L_exiru
C SRG_logic.fgi( 398, 463):������,20SRG10AA284YA21
      !{
      Call KLAPAN_MAN(deltat,REAL(R_ebeve,4),R8_avave,I_ifeve
     &,C8_uvave,I_ufeve,R_oxave,
     & R_ixave,R_asave,REAL(R_isave,4),R_etave,
     & REAL(R_otave,4),R_osave,REAL(R_atave,4),I_afeve,
     & I_akeve,I_ofeve,I_udeve,L_utave,L_ikeve,L_ileve,L_itave
     &,
     & L_usave,REAL(R_axave,4),L_ivave,L_evave,L_ukeve,
     & L_esave,L_exave,L_ameve,L_uxave,L_abeve,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(192),L_orave,L_(193),L_iviru
     &,L_okeve,I_ekeve,L_oleve,
     & R_odeve,REAL(R_ovave,4),L_uleve,L_urave,L_emeve,L_oriru
     &,L_ibeve,L_obeve,
     & L_ubeve,L_edeve,L_ideve,L_adeve)
      !}

      if(L_ideve.or.L_edeve.or.L_adeve.or.L_ubeve.or.L_obeve.or.L_ibeve
     &) then      
                  I_efeve = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_efeve = z'40000000'
      endif
C SRG_vlv.fgi( 422, 378):���� ���������� ��������,20SRG10AA284
      L_eviru=L_exiru
C SRG_logic.fgi( 398, 459):������,20SRG10AA285YA21
      !{
      Call KLAPAN_MAN(deltat,REAL(R_olute,4),R8_ifute,I_upute
     &,C8_ekute,I_erute,R_alute,
     & R_ukute,R_ibute,REAL(R_ubute,4),R_odute,
     & REAL(R_afute,4),R_adute,REAL(R_idute,4),I_ipute,
     & I_irute,I_arute,I_epute,L_efute,L_urute,L_usute,L_udute
     &,
     & L_edute,REAL(R_ikute,4),L_ufute,L_ofute,L_esute,
     & L_obute,L_okute,L_itute,L_elute,L_ilute,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(188),L_abute,L_(189),L_eviru
     &,L_asute,I_orute,L_atute,
     & R_apute,REAL(R_akute,4),L_etute,L_ebute,L_otute,L_iriru
     &,L_ulute,L_amute,
     & L_emute,L_omute,L_umute,L_imute)
      !}

      if(L_umute.or.L_omute.or.L_imute.or.L_emute.or.L_amute.or.L_ulute
     &) then      
                  I_opute = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_opute = z'40000000'
      endif
C SRG_vlv.fgi( 422, 354):���� ���������� ��������,20SRG10AA285
      L_aviru=L_exiru
C SRG_logic.fgi( 398, 455):������,20SRG10AA286YA21
      !{
      Call KLAPAN_MAN(deltat,REAL(R_axise,4),R8_usise,I_edose
     &,C8_otise,I_odose,R_ivise,
     & R_evise,R_upise,REAL(R_erise,4),R_asise,
     & REAL(R_isise,4),R_irise,REAL(R_urise,4),I_ubose,
     & I_udose,I_idose,I_obose,L_osise,L_efose,L_ekose,L_esise
     &,
     & L_orise,REAL(R_utise,4),L_etise,L_atise,L_ofose,
     & L_arise,L_avise,L_ukose,L_ovise,L_uvise,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(172),L_ipise,L_(173),L_aviru
     &,L_ifose,I_afose,L_ikose,
     & R_ibose,REAL(R_itise,4),L_okose,L_opise,L_alose,L_eriru
     &,L_exise,L_ixise,
     & L_oxise,L_abose,L_ebose,L_uxise)
      !}

      if(L_ebose.or.L_abose.or.L_uxise.or.L_oxise.or.L_ixise.or.L_exise
     &) then      
                  I_adose = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_adose = z'40000000'
      endif
C SRG_vlv.fgi( 422, 330):���� ���������� ��������,20SRG10AA286
      L_utiru=L_exiru
C SRG_logic.fgi( 398, 451):������,20SRG10AA287YA21
      !{
      Call KLAPAN_MAN(deltat,REAL(R_aveve,4),R8_ureve,I_ebive
     &,C8_oseve,I_obive,R_iteve,
     & R_eteve,R_umeve,REAL(R_epeve,4),R_areve,
     & REAL(R_ireve,4),R_ipeve,REAL(R_upeve,4),I_uxeve,
     & I_ubive,I_ibive,I_oxeve,L_oreve,L_edive,L_efive,L_ereve
     &,
     & L_opeve,REAL(R_useve,4),L_eseve,L_aseve,L_odive,
     & L_apeve,L_ateve,L_ufive,L_oteve,L_uteve,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(194),L_imeve,L_(195),L_utiru
     &,L_idive,I_adive,L_ifive,
     & R_ixeve,REAL(R_iseve,4),L_ofive,L_omeve,L_akive,L_ariru
     &,L_eveve,L_iveve,
     & L_oveve,L_axeve,L_exeve,L_uveve)
      !}

      if(L_exeve.or.L_axeve.or.L_uveve.or.L_oveve.or.L_iveve.or.L_eveve
     &) then      
                  I_abive = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_abive = z'40000000'
      endif
C SRG_vlv.fgi( 436, 402):���� ���������� ��������,20SRG10AA287
      L_otiru=L_exiru
C SRG_logic.fgi( 398, 447):������,20SRG10AA288YA21
      !{
      Call KLAPAN_MAN(deltat,REAL(R_ifave,4),R8_ebave,I_olave
     &,C8_adave,I_amave,R_udave,
     & R_odave,R_evute,REAL(R_ovute,4),R_ixute,
     & REAL(R_uxute,4),R_uvute,REAL(R_exute,4),I_elave,
     & I_emave,I_ulave,I_alave,L_abave,L_omave,L_opave,L_oxute
     &,
     & L_axute,REAL(R_edave,4),L_obave,L_ibave,L_apave,
     & L_ivute,L_idave,L_erave,L_afave,L_efave,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(190),L_utute,L_(191),L_otiru
     &,L_umave,I_imave,L_upave,
     & R_ukave,REAL(R_ubave,4),L_arave,L_avute,L_irave,L_upiru
     &,L_ofave,L_ufave,
     & L_akave,L_ikave,L_okave,L_ekave)
      !}

      if(L_okave.or.L_ikave.or.L_ekave.or.L_akave.or.L_ufave.or.L_ofave
     &) then      
                  I_ilave = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ilave = z'40000000'
      endif
C SRG_vlv.fgi( 436, 378):���� ���������� ��������,20SRG10AA288
      L_itiru=L_exiru
C SRG_logic.fgi( 398, 443):������,20SRG10AA289YA21
      !{
      Call KLAPAN_MAN(deltat,REAL(R_upote,4),R8_olote,I_atote
     &,C8_imote,I_itote,R_epote,
     & R_apote,R_ofote,REAL(R_akote,4),R_ukote,
     & REAL(R_elote,4),R_ekote,REAL(R_okote,4),I_osote,
     & I_otote,I_etote,I_isote,L_ilote,L_avote,L_axote,L_alote
     &,
     & L_ikote,REAL(R_omote,4),L_amote,L_ulote,L_ivote,
     & L_ufote,L_umote,L_oxote,L_ipote,L_opote,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(186),L_efote,L_(187),L_itiru
     &,L_evote,I_utote,L_exote,
     & R_esote,REAL(R_emote,4),L_ixote,L_ifote,L_uxote,L_opiru
     &,L_arote,L_erote,
     & L_irote,L_urote,L_asote,L_orote)
      !}

      if(L_asote.or.L_urote.or.L_orote.or.L_irote.or.L_erote.or.L_arote
     &) then      
                  I_usote = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_usote = z'40000000'
      endif
C SRG_vlv.fgi( 436, 354):���� ���������� ��������,20SRG10AA289
      L_etiru=L_exiru
C SRG_logic.fgi( 398, 439):������,20SRG10AA290YA21
      !{
      Call KLAPAN_MAN(deltat,REAL(R_edise,4),R8_axese,I_ikise
     &,C8_uxese,I_ukise,R_obise,
     & R_ibise,R_atese,REAL(R_itese,4),R_evese,
     & REAL(R_ovese,4),R_otese,REAL(R_avese,4),I_akise,
     & I_alise,I_okise,I_ufise,L_uvese,L_ilise,L_imise,L_ivese
     &,
     & L_utese,REAL(R_abise,4),L_ixese,L_exese,L_ulise,
     & L_etese,L_ebise,L_apise,L_ubise,L_adise,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(170),L_osese,L_(171),L_etiru
     &,L_olise,I_elise,L_omise,
     & R_ofise,REAL(R_oxese,4),L_umise,L_usese,L_epise,L_ipiru
     &,L_idise,L_odise,
     & L_udise,L_efise,L_ifise,L_afise)
      !}

      if(L_ifise.or.L_efise.or.L_afise.or.L_udise.or.L_odise.or.L_idise
     &) then      
                  I_ekise = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ekise = z'40000000'
      endif
C SRG_vlv.fgi( 436, 330):���� ���������� ��������,20SRG10AA290
      L_atiru=L_exiru
C SRG_logic.fgi( 398, 435):������,20SRG10AA291YA21
      !{
      Call KLAPAN_MAN(deltat,REAL(R_atite,4),R8_upite,I_exite
     &,C8_orite,I_oxite,R_isite,
     & R_esite,R_ulite,REAL(R_emite,4),R_apite,
     & REAL(R_ipite,4),R_imite,REAL(R_umite,4),I_uvite,
     & I_uxite,I_ixite,I_ovite,L_opite,L_ebote,L_edote,L_epite
     &,
     & L_omite,REAL(R_urite,4),L_erite,L_arite,L_obote,
     & L_amite,L_asite,L_udote,L_osite,L_usite,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(184),L_ilite,L_(185),L_atiru
     &,L_ibote,I_abote,L_idote,
     & R_ivite,REAL(R_irite,4),L_odote,L_olite,L_afote,L_epiru
     &,L_etite,L_itite,
     & L_otite,L_avite,L_evite,L_utite)
      !}

      if(L_evite.or.L_avite.or.L_utite.or.L_otite.or.L_itite.or.L_etite
     &) then      
                  I_axite = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_axite = z'40000000'
      endif
C SRG_vlv.fgi( 450, 402):���� ���������� ��������,20SRG10AA291
      L_usiru=L_exiru
C SRG_logic.fgi( 398, 431):������,20SRG10AA292YA21
      !{
      Call KLAPAN_MAN(deltat,REAL(R_udete,4),R8_oxate,I_alete
     &,C8_ibete,I_ilete,R_edete,
     & R_adete,R_otate,REAL(R_avate,4),R_uvate,
     & REAL(R_exate,4),R_evate,REAL(R_ovate,4),I_okete,
     & I_olete,I_elete,I_ikete,L_ixate,L_amete,L_apete,L_axate
     &,
     & L_ivate,REAL(R_obete,4),L_abete,L_uxate,L_imete,
     & L_utate,L_ubete,L_opete,L_idete,L_odete,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(180),L_etate,L_(181),L_usiru
     &,L_emete,I_ulete,L_epete,
     & R_ekete,REAL(R_ebete,4),L_ipete,L_itate,L_upete,L_apiru
     &,L_afete,L_efete,
     & L_ifete,L_ufete,L_akete,L_ofete)
      !}

      if(L_akete.or.L_ufete.or.L_ofete.or.L_ifete.or.L_efete.or.L_afete
     &) then      
                  I_ukete = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ukete = z'40000000'
      endif
C SRG_vlv.fgi( 450, 378):���� ���������� ��������,20SRG10AA292
      L_osiru=L_exiru
C SRG_logic.fgi( 398, 427):������,20SRG10AA293YA21
      !{
      Call KLAPAN_MAN(deltat,REAL(R_epuse,4),R8_aluse,I_isuse
     &,C8_uluse,I_ususe,R_omuse,
     & R_imuse,R_afuse,REAL(R_ifuse,4),R_ekuse,
     & REAL(R_okuse,4),R_ofuse,REAL(R_akuse,4),I_asuse,
     & I_atuse,I_osuse,I_uruse,L_ukuse,L_ituse,L_ivuse,L_ikuse
     &,
     & L_ufuse,REAL(R_amuse,4),L_iluse,L_eluse,L_utuse,
     & L_efuse,L_emuse,L_axuse,L_umuse,L_apuse,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(176),L_oduse,L_(177),L_osiru
     &,L_otuse,I_etuse,L_ovuse,
     & R_oruse,REAL(R_oluse,4),L_uvuse,L_uduse,L_exuse,L_umiru
     &,L_ipuse,L_opuse,
     & L_upuse,L_eruse,L_iruse,L_aruse)
      !}

      if(L_iruse.or.L_eruse.or.L_aruse.or.L_upuse.or.L_opuse.or.L_ipuse
     &) then      
                  I_esuse = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_esuse = z'40000000'
      endif
C SRG_vlv.fgi( 450, 354):���� ���������� ��������,20SRG10AA293
      !{
      Call KLAPAN_MAN(deltat,REAL(R_ikese,4),R8_edese,I_omese
     &,C8_afese,I_apese,R_ufese,
     & R_ofese,R_exase,REAL(R_oxase,4),R_ibese,
     & REAL(R_ubese,4),R_uxase,REAL(R_ebese,4),I_emese,
     & I_epese,I_umese,I_amese,L_adese,L_opese,L_orese,L_obese
     &,
     & L_abese,REAL(R_efese,4),L_odese,L_idese,L_arese,
     & L_ixase,L_ifese,L_esese,L_akese,L_ekese,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(168),L_uvase,L_(169),L_exiru
     &,L_upese,I_ipese,L_urese,
     & R_ulese,REAL(R_udese,4),L_asese,L_axase,L_isese,L_omiru
     &,L_okese,L_ukese,
     & L_alese,L_ilese,L_olese,L_elese)
      !}

      if(L_olese.or.L_ilese.or.L_elese.or.L_alese.or.L_ukese.or.L_okese
     &) then      
                  I_imese = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_imese = z'40000000'
      endif
C SRG_vlv.fgi( 450, 330):���� ���������� ��������,20SRG10AA294
      L_(443) = L_uriru.AND.L_oriru.AND.L_iriru.AND.L_eriru.AND.L_ariru.
     &AND.L_upiru.AND.L_opiru.AND.L_ipiru.AND.L_epiru.AND.L_apiru.AND.L_
     &umiru.AND.L_omiru
C SRG_logic.fgi( 360, 411):�
      L0_imiru=(L0_uxiru.or.L0_imiru).and..not.(L_oxiru)
      L_(441)=.not.L0_imiru
C SRG_logic.fgi( 371, 407):RS �������
      L_(442) = L_(443).AND.L0_imiru
C SRG_logic.fgi( 379, 410):�
      if(L_(442).and..not.L0_emiru) then
         R0_uliru=R0_amiru
      else
         R0_uliru=max(R_(199)-deltat,0.0)
      endif
      L_isiru=R0_uliru.gt.0.0
      L0_emiru=L_(442)
C SRG_logic.fgi( 387, 410):������������  �� T
      L_asiru=L_isiru
C SRG_logic.fgi( 406, 406):������,20SRG10AA278YA21
      !{
      Call KLAPAN_MAN(deltat,REAL(R_exete,4),R8_atete,I_idite
     &,C8_utete,I_udite,R_ovete,
     & R_ivete,R_arete,REAL(R_irete,4),R_esete,
     & REAL(R_osete,4),R_orete,REAL(R_asete,4),I_adite,
     & I_afite,I_odite,I_ubite,L_usete,L_ifite,L_ikite,L_isete
     &,
     & L_urete,REAL(R_avete,4),L_itete,L_etete,L_ufite,
     & L_erete,L_evete,L_alite,L_uvete,L_axete,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(182),L_akiru,L_(183),L_asiru
     &,L_ofite,I_efite,L_okite,
     & R_obite,REAL(R_otete,4),L_ukite,L_afiru,L_elite,L_ukiru
     &,L_ixete,L_oxete,
     & L_uxete,L_ebite,L_ibite,L_abite)
      !}

      if(L_ibite.or.L_ebite.or.L_abite.or.L_uxete.or.L_oxete.or.L_ixete
     &) then      
                  I_edite = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_edite = z'40000000'
      endif
C SRG_vlv.fgi( 464, 402):���� ���������� ��������,20SRG10AA278
      !{
      Call KLAPAN_MAN(deltat,REAL(R_alate,4),R8_udate,I_epate
     &,C8_ofate,I_opate,R_ikate,
     & R_ekate,R_uxuse,REAL(R_ebate,4),R_adate,
     & REAL(R_idate,4),R_ibate,REAL(R_ubate,4),I_umate,
     & I_upate,I_ipate,I_omate,L_odate,L_erate,L_esate,L_edate
     &,
     & L_obate,REAL(R_ufate,4),L_efate,L_afate,L_orate,
     & L_abate,L_akate,L_usate,L_okate,L_ukate,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(178),L_ixuse,L_(179),L_isiru
     &,L_irate,I_arate,L_isate,
     & R_imate,REAL(R_ifate,4),L_osate,L_oxuse,L_atate,L_okiru
     &,L_elate,L_ilate,
     & L_olate,L_amate,L_emate,L_ulate)
      !}

      if(L_emate.or.L_amate.or.L_ulate.or.L_olate.or.L_ilate.or.L_elate
     &) then      
                  I_apate = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_apate = z'40000000'
      endif
C SRG_vlv.fgi( 464, 378):���� ���������� ��������,20SRG10AA279
      L_(439) = L_aliru.AND.L_ukiru.AND.L_okiru
C SRG_logic.fgi( 351, 366):�
      L0_ikiru=(L0_uxiru.or.L0_ikiru).and..not.(L_oxiru)
      L_(437)=.not.L0_ikiru
C SRG_logic.fgi( 374, 362):RS �������
      L_(438) = L_(439).AND.L0_ikiru.AND.L_(440)
C SRG_logic.fgi( 382, 364):�
      if(L_(438).and..not.L0_ekiru) then
         R0_ofiru=R0_ufiru
      else
         R0_ofiru=max(R_(196)-deltat,0.0)
      endif
      L_akiru=R0_ofiru.gt.0.0
      L0_ekiru=L_(438)
C SRG_logic.fgi( 390, 364):������������  �� T
      L_ifiru=L_akiru
C SRG_logic.fgi( 410, 364):������,20SRG10AA276YA21
      L_esiru=L_isiru
C SRG_logic.fgi( 406, 410):������,20SRG10AA276YA22
      !{
      Call KLAPAN_MAN(deltat,REAL(R_ufere,4),R8_obere,I_amere
     &,C8_idere,I_imere,R_efere,
     & R_afere,R_ovare,REAL(R_axare,4),R_uxare,
     & REAL(R_ebere,4),R_exare,REAL(R_oxare,4),I_olere,
     & I_omere,I_emere,I_ilere,L_ibere,L_apere,L_arere,L_abere
     &,
     & L_ixare,REAL(R_odere,4),L_adere,L_ubere,L_ipere,
     & L_uvare,L_udere,L_orere,L_ifere,L_ofere,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(160),L_esiru,L_(161),L_ifiru
     &,L_epere,I_umere,L_erere,
     & R_elere,REAL(R_edere,4),L_irere,L_aliru,L_urere,L_efiru
     &,L_akere,L_ekere,
     & L_ikere,L_ukere,L_alere,L_okere)
      !}

      if(L_alere.or.L_ukere.or.L_okere.or.L_ikere.or.L_ekere.or.L_akere
     &) then      
                  I_ulere = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ulere = z'40000000'
      endif
C SRG_vlv.fgi( 478, 378):���� ���������� ��������,20SRG10AA276
C sav1=L_(439)
      L_(439) = L_aliru.AND.L_ukiru.AND.L_okiru
C SRG_logic.fgi( 351, 366):recalc:�
C if(sav1.ne.L_(439) .and. try3135.gt.0) goto 3135
      L_(436) = L_efiru.AND.L_afiru
C SRG_logic.fgi( 351, 340):�
      if(L_(436).and..not.L0_udiru) then
         R0_idiru=R0_odiru
      else
         R0_idiru=max(R_(195)-deltat,0.0)
      endif
      L_oxiru=R0_idiru.gt.0.0
      L0_udiru=L_(436)
C SRG_logic.fgi( 366, 340):������������  �� T
      L0_uxiru=(L_(445).or.L0_uxiru).and..not.(L_oxiru)
      L_(444)=.not.L0_uxiru
C SRG_logic.fgi( 366, 465):RS �������
      if(L0_uxiru.and..not.L0_ixiru) then
         R0_uviru=R0_axiru
      else
         R0_uviru=max(R_(200)-deltat,0.0)
      endif
      L_exiru=R0_uviru.gt.0.0
      L0_ixiru=L0_uxiru
C SRG_logic.fgi( 379, 467):������������  �� T
C sav1=L_osiru
C SRG_logic.fgi( 398, 427):recalc:������,20SRG10AA293YA21
C if(sav1.ne.L_osiru .and. try3108.gt.0) goto 3108
C sav1=L_usiru
C SRG_logic.fgi( 398, 431):recalc:������,20SRG10AA292YA21
C if(sav1.ne.L_usiru .and. try3104.gt.0) goto 3104
C sav1=L_atiru
C SRG_logic.fgi( 398, 435):recalc:������,20SRG10AA291YA21
C if(sav1.ne.L_atiru .and. try3100.gt.0) goto 3100
C sav1=L_etiru
C SRG_logic.fgi( 398, 439):recalc:������,20SRG10AA290YA21
C if(sav1.ne.L_etiru .and. try3096.gt.0) goto 3096
C sav1=L_itiru
C SRG_logic.fgi( 398, 443):recalc:������,20SRG10AA289YA21
C if(sav1.ne.L_itiru .and. try3092.gt.0) goto 3092
C sav1=L_otiru
C SRG_logic.fgi( 398, 447):recalc:������,20SRG10AA288YA21
C if(sav1.ne.L_otiru .and. try3088.gt.0) goto 3088
C sav1=L_utiru
C SRG_logic.fgi( 398, 451):recalc:������,20SRG10AA287YA21
C if(sav1.ne.L_utiru .and. try3084.gt.0) goto 3084
C sav1=L_aviru
C SRG_logic.fgi( 398, 455):recalc:������,20SRG10AA286YA21
C if(sav1.ne.L_aviru .and. try3080.gt.0) goto 3080
C sav1=L_eviru
C SRG_logic.fgi( 398, 459):recalc:������,20SRG10AA285YA21
C if(sav1.ne.L_eviru .and. try3076.gt.0) goto 3076
C sav1=L_iviru
C SRG_logic.fgi( 398, 463):recalc:������,20SRG10AA284YA21
C if(sav1.ne.L_iviru .and. try3072.gt.0) goto 3072
      L_oviru=L_exiru
C SRG_logic.fgi( 398, 467):������,20SRG10AA283YA21
      L0_aripu=L_oxiru.or.(L0_aripu.and..not.(L_(348)))
      L_(349)=.not.L0_aripu
C SRG_logic.fgi( 210, 154):RS �������
      if(L0_aripu) then
         R8_eripu=R_(155)
      else
         R8_eripu=R_(156)
      endif
C SRG_logic.fgi( 280, 175):���� RE IN LO CH7
      L0_odipu=L_oxiru.or.(L0_odipu.and..not.(L_(345)))
      L_(346)=.not.L0_odipu
C SRG_logic.fgi( 210,  95):RS �������
      if(L0_odipu) then
         R8_udipu=R_(145)
      else
         R8_udipu=R_(146)
      endif
C SRG_logic.fgi( 280, 116):���� RE IN LO CH7
      L_etupu = L_oderu.OR.L_iparu.OR.L_odaru
C SRG_logic.fgi( 226, 220):���
C label 3195  try3195=try3195-1
      L_imaru=L_omaru
C SRG_logic.fgi( 412, 279):������,20SRG10AA281YA21
      !{
      Call KLAPAN_MAN(deltat,REAL(R_ebire,4),R8_avere,I_ifire
     &,C8_uvere,I_ufire,R_oxere,
     & R_ixere,R_asere,REAL(R_isere,4),R_etere,
     & REAL(R_otere,4),R_osere,REAL(R_atere,4),I_afire,
     & I_akire,I_ofire,I_udire,L_utere,L_ikire,L_ilire,L_itere
     &,
     & L_usere,REAL(R_axere,4),L_ivere,L_evere,L_ukire,
     & L_esere,L_exere,L_amire,L_uxere,L_abire,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(162),L_omaru,L_(163),L_iparu
     &,L_okire,I_ekire,L_olire,
     & R_odire,REAL(R_overe,4),L_ulire,L_akaru,L_emire,L_amaru
     &,L_ibire,L_obire,
     & L_ubire,L_edire,L_idire,L_adire)
      !}

      if(L_idire.or.L_edire.or.L_adire.or.L_ubire.or.L_obire.or.L_ibire
     &) then      
                  I_efire = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_efire = z'40000000'
      endif
C SRG_vlv.fgi( 478, 402):���� ���������� ��������,20SRG10AA282
      L_(412) = L_emaru.AND.L_uxaru.AND.L_amaru
C SRG_logic.fgi( 360, 299):�
      L_(411) = L_(412).AND.L0_ularu
C SRG_logic.fgi( 379, 298):�
      if(L_(411).and..not.L0_olaru) then
         R0_elaru=R0_ilaru
      else
         R0_elaru=max(R_(183)-deltat,0.0)
      endif
      L_(413)=R0_elaru.gt.0.0
      L0_olaru=L_(411)
C SRG_logic.fgi( 387, 298):������������  �� T
      L0_uparu=(L_(415).or.L0_uparu).and..not.(L_(413))
      L_(414)=.not.L0_uparu
C SRG_logic.fgi( 375, 310):RS �������
      if(L0_uparu.and..not.L0_oparu) then
         R0_aparu=R0_eparu
      else
         R0_aparu=max(R_(184)-deltat,0.0)
      endif
      L_iparu=R0_aparu.gt.0.0
      L0_oparu=L0_uparu
C SRG_logic.fgi( 388, 312):������������  �� T
      L_umaru=L_iparu
C SRG_logic.fgi( 412, 312):������,20SRG10AA281YA22
      !{
      Call KLAPAN_MAN(deltat,REAL(R_isose,4),R8_epose,I_ovose
     &,C8_arose,I_axose,R_urose,
     & R_orose,R_elose,REAL(R_olose,4),R_imose,
     & REAL(R_umose,4),R_ulose,REAL(R_emose,4),I_evose,
     & I_exose,I_uvose,I_avose,L_apose,L_oxose,L_obuse,L_omose
     &,
     & L_amose,REAL(R_erose,4),L_opose,L_ipose,L_abuse,
     & L_ilose,L_irose,L_eduse,L_asose,L_esose,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(174),L_umaru,L_(175),L_imaru
     &,L_uxose,I_ixose,L_ubuse,
     & R_utose,REAL(R_upose,4),L_aduse,L_emaru,L_iduse,L_ekaru
     &,L_osose,L_usose,
     & L_atose,L_itose,L_otose,L_etose)
      !}

      if(L_otose.or.L_itose.or.L_etose.or.L_atose.or.L_usose.or.L_osose
     &) then      
                  I_ivose = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ivose = z'40000000'
      endif
C SRG_vlv.fgi( 464, 354):���� ���������� ��������,20SRG10AA281
      L_(406) = L_ekaru.AND.L_etaru.AND.L_akaru
C SRG_logic.fgi( 360, 266):�
      L_(405) = L_(406).AND.L0_ufaru
C SRG_logic.fgi( 379, 265):�
      if(L_(405).and..not.L0_ofaru) then
         R0_efaru=R0_ifaru
      else
         R0_efaru=max(R_(181)-deltat,0.0)
      endif
      L_(407)=R0_efaru.gt.0.0
      L0_ofaru=L_(405)
C SRG_logic.fgi( 387, 265):������������  �� T
      L0_alaru=(L_(409).or.L0_alaru).and..not.(L_(407))
      L_(408)=.not.L0_alaru
C SRG_logic.fgi( 375, 277):RS �������
      if(L0_alaru.and..not.L0_ukaru) then
         R0_ikaru=R0_okaru
      else
         R0_ikaru=max(R_(182)-deltat,0.0)
      endif
      L_omaru=R0_ikaru.gt.0.0
      L0_ukaru=L0_alaru
C SRG_logic.fgi( 388, 279):������������  �� T
C sav1=L_imaru
C SRG_logic.fgi( 412, 279):recalc:������,20SRG10AA281YA21
C if(sav1.ne.L_imaru .and. try3198.gt.0) goto 3198
      L_obaru=L_ubaru
C SRG_logic.fgi( 542, 279):������,20SRG10AA315YA21
      !{
      Call KLAPAN_MAN(deltat,REAL(R_otire,4),R8_irire,I_uxire
     &,C8_esire,I_ebore,R_atire,
     & R_usire,R_imire,REAL(R_umire,4),R_opire,
     & REAL(R_arire,4),R_apire,REAL(R_ipire,4),I_ixire,
     & I_ibore,I_abore,I_exire,L_erire,L_ubore,L_udore,L_upire
     &,
     & L_epire,REAL(R_isire,4),L_urire,L_orire,L_edore,
     & L_omire,L_osire,L_ifore,L_etire,L_itire,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(164),L_ubaru,L_(165),L_odaru
     &,L_adore,I_obore,L_afore,
     & R_axire,REAL(R_asire,4),L_efore,L_evupu,L_ofore,L_ebaru
     &,L_utire,L_avire,
     & L_evire,L_ovire,L_uvire,L_ivire)
      !}

      if(L_uvire.or.L_ovire.or.L_ivire.or.L_evire.or.L_avire.or.L_utire
     &) then      
                  I_oxire = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_oxire = z'40000000'
      endif
C SRG_vlv.fgi( 732, 605):���� ���������� ��������,20SRG10AA316
      L_(400) = L_ibaru.AND.L_uxaru.AND.L_ebaru
C SRG_logic.fgi( 489, 299):�
      L_(399) = L_(400).AND.L0_abaru
C SRG_logic.fgi( 508, 298):�
      if(L_(399).and..not.L0_uxupu) then
         R0_ixupu=R0_oxupu
      else
         R0_ixupu=max(R_(179)-deltat,0.0)
      endif
      L_(401)=R0_ixupu.gt.0.0
      L0_uxupu=L_(399)
C SRG_logic.fgi( 516, 298):������������  �� T
      L0_afaru=(L_(403).or.L0_afaru).and..not.(L_(401))
      L_(402)=.not.L0_afaru
C SRG_logic.fgi( 504, 310):RS �������
      if(L0_afaru.and..not.L0_udaru) then
         R0_edaru=R0_idaru
      else
         R0_edaru=max(R_(180)-deltat,0.0)
      endif
      L_odaru=R0_edaru.gt.0.0
      L0_udaru=L0_afaru
C SRG_logic.fgi( 517, 312):������������  �� T
      L_adaru=L_odaru
C SRG_logic.fgi( 542, 312):������,20SRG10AA315YA22
      !{
      Call KLAPAN_MAN(deltat,REAL(R_arodi,4),R8_ulodi,I_etodi
     &,C8_omodi,I_otodi,R_ipodi,
     & R_epodi,R_ufodi,REAL(R_ekodi,4),R_alodi,
     & REAL(R_ilodi,4),R_ikodi,REAL(R_ukodi,4),I_usodi,
     & I_utodi,I_itodi,I_osodi,L_olodi,L_evodi,L_exodi,L_elodi
     &,
     & L_okodi,REAL(R_umodi,4),L_emodi,L_amodi,L_ovodi,
     & L_akodi,L_apodi,L_uxodi,L_opodi,L_upodi,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(234),L_adaru,L_(235),L_obaru
     &,L_ivodi,I_avodi,L_ixodi,
     & R_isodi,REAL(R_imodi,4),L_oxodi,L_ibaru,L_abudi,L_ivupu
     &,L_erodi,L_irodi,
     & L_orodi,L_asodi,L_esodi,L_urodi)
      !}

      if(L_esodi.or.L_asodi.or.L_urodi.or.L_orodi.or.L_irodi.or.L_erodi
     &) then      
                  I_atodi = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_atodi = z'40000000'
      endif
C SRG_vlv.fgi( 690, 629):���� ���������� ��������,20SRG10AA315
      L_(394) = L_ivupu.AND.L_etaru.AND.L_evupu
C SRG_logic.fgi( 489, 266):�
      L_(393) = L_(394).AND.L0_avupu
C SRG_logic.fgi( 508, 265):�
      if(L_(393).and..not.L0_utupu) then
         R0_itupu=R0_otupu
      else
         R0_itupu=max(R_(177)-deltat,0.0)
      endif
      L_(395)=R0_itupu.gt.0.0
      L0_utupu=L_(393)
C SRG_logic.fgi( 516, 265):������������  �� T
      L0_exupu=(L_(397).or.L0_exupu).and..not.(L_(395))
      L_(396)=.not.L0_exupu
C SRG_logic.fgi( 504, 277):RS �������
      if(L0_exupu.and..not.L0_axupu) then
         R0_ovupu=R0_uvupu
      else
         R0_ovupu=max(R_(178)-deltat,0.0)
      endif
      L_ubaru=R0_ovupu.gt.0.0
      L0_axupu=L0_exupu
C SRG_logic.fgi( 517, 279):������������  �� T
C sav1=L_obaru
C SRG_logic.fgi( 542, 279):recalc:������,20SRG10AA315YA21
C if(sav1.ne.L_obaru .and. try3246.gt.0) goto 3246
      L_atupu = L_iberu.OR.L_omaru.OR.L_ubaru
C SRG_logic.fgi( 226, 211):���
      !{
      Call KLAPAN_MAN(deltat,REAL(R_avibo,4),R8_uribo,I_ebobo
     &,C8_osibo,I_obobo,R_itibo,
     & R_etibo,R_umibo,REAL(R_epibo,4),R_aribo,
     & REAL(R_iribo,4),R_ipibo,REAL(R_upibo,4),I_uxibo,
     & I_ubobo,I_ibobo,I_oxibo,L_oribo,L_edobo,L_efobo,L_eribo
     &,
     & L_opibo,REAL(R_usibo,4),L_esibo,L_asibo,L_odobo,
     & L_apibo,L_atibo,L_ufobo,L_otibo,L_utibo,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(290),L_atupu,L_(291),L_etupu
     &,L_idobo,I_adobo,L_ifobo,
     & R_ixibo,REAL(R_isibo,4),L_ofobo,L_etaru,L_akobo,L_uxaru
     &,L_evibo,L_ivibo,
     & L_ovibo,L_axibo,L_exibo,L_uvibo)
      !}

      if(L_exibo.or.L_axibo.or.L_uvibo.or.L_ovibo.or.L_ivibo.or.L_evibo
     &) then      
                  I_abobo = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_abobo = z'40000000'
      endif
C SRG_vlv.fgi( 519, 605):���� ���������� ��������,20SRG10AA248
C sav1=L_(412)
      L_(412) = L_emaru.AND.L_uxaru.AND.L_amaru
C SRG_logic.fgi( 360, 299):recalc:�
C if(sav1.ne.L_(412) .and. try3204.gt.0) goto 3204
C sav1=L_(400)
      L_(400) = L_ibaru.AND.L_uxaru.AND.L_ebaru
C SRG_logic.fgi( 489, 299):recalc:�
C if(sav1.ne.L_(400) .and. try3252.gt.0) goto 3252
C sav1=L_(406)
      L_(406) = L_ekaru.AND.L_etaru.AND.L_akaru
C SRG_logic.fgi( 360, 266):recalc:�
C if(sav1.ne.L_(406) .and. try3228.gt.0) goto 3228
C sav1=L_(394)
      L_(394) = L_ivupu.AND.L_etaru.AND.L_evupu
C SRG_logic.fgi( 489, 266):recalc:�
C if(sav1.ne.L_(394) .and. try3276.gt.0) goto 3276
      !{
      Call KLAPAN_MAN(deltat,REAL(R_irobo,4),R8_emobo,I_otobo
     &,C8_apobo,I_avobo,R_upobo,
     & R_opobo,R_ekobo,REAL(R_okobo,4),R_ilobo,
     & REAL(R_ulobo,4),R_ukobo,REAL(R_elobo,4),I_etobo,
     & I_evobo,I_utobo,I_atobo,L_amobo,L_ovobo,L_oxobo,L_olobo
     &,
     & L_alobo,REAL(R_epobo,4),L_omobo,L_imobo,L_axobo,
     & L_ikobo,L_ipobo,L_ebubo,L_arobo,L_erobo,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(292),L_iberu,L_(293),L_oderu
     &,L_uvobo,I_ivobo,L_uxobo,
     & R_usobo,REAL(R_umobo,4),L_abubo,L_ataru,L_ibubo,L_oxaru
     &,L_orobo,L_urobo,
     & L_asobo,L_isobo,L_osobo,L_esobo)
      !}

      if(L_osobo.or.L_isobo.or.L_esobo.or.L_asobo.or.L_urobo.or.L_orobo
     &) then      
                  I_itobo = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_itobo = z'40000000'
      endif
C SRG_vlv.fgi( 505, 605):���� ���������� ��������,20SRG10AA247
      L_(418) = L_itaru.AND.L_etaru.AND.L_ataru
C SRG_logic.fgi( 232, 238):�
      L0_usaru=(L0_ivaru.or.L0_usaru).and..not.(L_evaru)
      L_(416)=.not.L0_usaru
C SRG_logic.fgi( 243, 234):RS �������
      L_(417) = L_(418).AND.L0_usaru
C SRG_logic.fgi( 251, 237):�
      if(L_(417).and..not.L0_osaru) then
         R0_esaru=R0_isaru
      else
         R0_esaru=max(R_(185)-deltat,0.0)
      endif
      L_evaru=R0_esaru.gt.0.0
      L0_osaru=L_(417)
C SRG_logic.fgi( 259, 237):������������  �� T
      L0_ivaru=(L_(420).or.L0_ivaru).and..not.(L_evaru)
      L_(419)=.not.L0_ivaru
C SRG_logic.fgi( 247, 249):RS �������
      if(L0_ivaru.and..not.L0_avaru) then
         R0_otaru=R0_utaru
      else
         R0_otaru=max(R_(186)-deltat,0.0)
      endif
      L_iberu=R0_otaru.gt.0.0
      L0_avaru=L0_ivaru
C SRG_logic.fgi( 260, 251):������������  �� T
C sav1=L_atupu
      L_atupu = L_iberu.OR.L_omaru.OR.L_ubaru
C SRG_logic.fgi( 226, 211):recalc:���
C if(sav1.ne.L_atupu .and. try3294.gt.0) goto 3294
      L_eberu=L_iberu
C SRG_logic.fgi( 284, 251):������,20SRG10AA246YA21
      L_oberu=L_oderu
C SRG_logic.fgi( 284, 312):������,20SRG10AA246YA22
      !{
      Call KLAPAN_MAN(deltat,REAL(R_ulubo,4),R8_ofubo,I_arubo
     &,C8_ikubo,I_irubo,R_elubo,
     & R_alubo,R_obubo,REAL(R_adubo,4),R_udubo,
     & REAL(R_efubo,4),R_edubo,REAL(R_odubo,4),I_opubo,
     & I_orubo,I_erubo,I_ipubo,L_ifubo,L_asubo,L_atubo,L_afubo
     &,
     & L_idubo,REAL(R_okubo,4),L_akubo,L_ufubo,L_isubo,
     & L_ububo,L_ukubo,L_otubo,L_ilubo,L_olubo,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(294),L_oberu,L_(295),L_eberu
     &,L_esubo,I_urubo,L_etubo,
     & R_epubo,REAL(R_ekubo,4),L_itubo,L_aberu,L_utubo,L_itaru
     &,L_amubo,L_emubo,
     & L_imubo,L_umubo,L_apubo,L_omubo)
      !}

      if(L_apubo.or.L_umubo.or.L_omubo.or.L_imubo.or.L_emubo.or.L_amubo
     &) then      
                  I_upubo = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_upubo = z'40000000'
      endif
C SRG_vlv.fgi( 519, 629):���� ���������� ��������,20SRG10AA246
C sav1=L_(418)
      L_(418) = L_itaru.AND.L_etaru.AND.L_ataru
C SRG_logic.fgi( 232, 238):recalc:�
C if(sav1.ne.L_(418) .and. try3315.gt.0) goto 3315
      L_(423) = L_aberu.AND.L_uxaru.AND.L_oxaru
C SRG_logic.fgi( 232, 283):�
      L0_ixaru=(L0_eferu.or.L0_ixaru).and..not.(L_aferu)
      L_(421)=.not.L0_ixaru
C SRG_logic.fgi( 243, 279):RS �������
      L_(422) = L_(423).AND.L0_ixaru
C SRG_logic.fgi( 251, 282):�
      if(L_(422).and..not.L0_exaru) then
         R0_uvaru=R0_axaru
      else
         R0_uvaru=max(R_(187)-deltat,0.0)
      endif
      L_aferu=R0_uvaru.gt.0.0
      L0_exaru=L_(422)
C SRG_logic.fgi( 259, 282):������������  �� T
      L0_eferu=(L_(425).or.L0_eferu).and..not.(L_aferu)
      L_(424)=.not.L0_eferu
C SRG_logic.fgi( 247, 310):RS �������
      if(L0_eferu.and..not.L0_uderu) then
         R0_ederu=R0_ideru
      else
         R0_ederu=max(R_(188)-deltat,0.0)
      endif
      L_oderu=R0_ederu.gt.0.0
      L0_uderu=L0_eferu
C SRG_logic.fgi( 260, 312):������������  �� T
C sav1=L_etupu
      L_etupu = L_oderu.OR.L_iparu.OR.L_odaru
C SRG_logic.fgi( 226, 220):recalc:���
C if(sav1.ne.L_etupu .and. try3195.gt.0) goto 3195
C sav1=L_oberu
C SRG_logic.fgi( 284, 312):recalc:������,20SRG10AA246YA22
C if(sav1.ne.L_oberu .and. try3338.gt.0) goto 3338
      if(L_oderu) then
         I_asaru=I_(41)
      else
         I_asaru=I_(42)
      endif
C SRG_logic.fgi( 291, 280):���� RE IN LO CH7
      if(L0_eferu) then
         I_osapu=I0_usapu
      else
         I_osapu=I0_atapu
      endif
C SRG_logic.fgi( 291, 290):���� RE IN LO CH7
      if(L_iberu) then
         I_uraru=I_(39)
      else
         I_uraru=I_(40)
      endif
C SRG_logic.fgi( 291, 234):���� RE IN LO CH7
      if(L_ubaru) then
         I_eraru=I_(31)
      else
         I_eraru=I_(32)
      endif
C SRG_logic.fgi( 548, 262):���� RE IN LO CH7
      L0_avupu=(L0_exupu.or.L0_avupu).and..not.(L_(395))
      L_(392)=.not.L0_avupu
C SRG_logic.fgi( 500, 262):RS �������
C sav1=L_(393)
      L_(393) = L_(394).AND.L0_avupu
C SRG_logic.fgi( 508, 265):recalc:�
C if(sav1.ne.L_(393) .and. try3278.gt.0) goto 3278
      if(L_odaru) then
         I_araru=I_(33)
      else
         I_araru=I_(34)
      endif
C SRG_logic.fgi( 548, 296):���� RE IN LO CH7
      L0_abaru=(L0_afaru.or.L0_abaru).and..not.(L_(401))
      L_(398)=.not.L0_abaru
C SRG_logic.fgi( 500, 295):RS �������
C sav1=L_(399)
      L_(399) = L_(400).AND.L0_abaru
C SRG_logic.fgi( 508, 298):recalc:�
C if(sav1.ne.L_(399) .and. try3254.gt.0) goto 3254
      if(L_omaru) then
         I_oraru=I_(35)
      else
         I_oraru=I_(36)
      endif
C SRG_logic.fgi( 419, 262):���� RE IN LO CH7
      L0_ufaru=(L0_alaru.or.L0_ufaru).and..not.(L_(407))
      L_(404)=.not.L0_ufaru
C SRG_logic.fgi( 371, 262):RS �������
C sav1=L_(405)
      L_(405) = L_(406).AND.L0_ufaru
C SRG_logic.fgi( 379, 265):recalc:�
C if(sav1.ne.L_(405) .and. try3230.gt.0) goto 3230
      if(L_iparu) then
         I_iraru=I_(37)
      else
         I_iraru=I_(38)
      endif
C SRG_logic.fgi( 419, 296):���� RE IN LO CH7
      L0_ularu=(L0_uparu.or.L0_ularu).and..not.(L_(413))
      L_(410)=.not.L0_ularu
C SRG_logic.fgi( 371, 295):RS �������
C sav1=L_(411)
      L_(411) = L_(412).AND.L0_ularu
C SRG_logic.fgi( 379, 298):recalc:�
C if(sav1.ne.L_(411) .and. try3206.gt.0) goto 3206
      !{
      Call KLAPAN_MAN(deltat,REAL(R_ifeli,4),R8_ebeli,I_oleli
     &,C8_adeli,I_ameli,R_udeli,
     & R_odeli,R_evali,REAL(R_ovali,4),R_ixali,
     & REAL(R_uxali,4),R_uvali,REAL(R_exali,4),I_eleli,
     & I_emeli,I_uleli,I_aleli,L_abeli,L_omeli,L_opeli,L_oxali
     &,
     & L_axali,REAL(R_edeli,4),L_obeli,L_ibeli,L_apeli,
     & L_ivali,L_ideli,L_ereli,L_afeli,L_efeli,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(264),L_iteru,L_(265),L_okeru
     &,L_umeli,I_imeli,L_upeli,
     & R_ukeli,REAL(R_ubeli,4),L_areli,L_emeru,L_ireli,L_ikeru
     &,L_ofeli,L_ufeli,
     & L_akeli,L_ikeli,L_okeli,L_ekeli)
      !}

      if(L_okeli.or.L_ikeli.or.L_ekeli.or.L_akeli.or.L_ufeli.or.L_ofeli
     &) then      
                  I_ileli = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ileli = z'40000000'
      endif
C SRG_vlv.fgi( 620, 653):���� ���������� ��������,20SRG10AA310
C label 3396  try3396=try3396-1
      L_(426) = L_ikeru.AND.L_ekeru
C SRG_logic.fgi( 472, 340):�
      if(L_(426).and..not.L0_akeru) then
         R0_oferu=R0_uferu
      else
         R0_oferu=max(R_(189)-deltat,0.0)
      endif
      L_ubiru=R0_oferu.gt.0.0
      L0_akeru=L_(426)
C SRG_logic.fgi( 487, 340):������������  �� T
      L0_adiru=(L_(435).or.L0_adiru).and..not.(L_ubiru)
      L_(434)=.not.L0_adiru
C SRG_logic.fgi( 487, 465):RS �������
      if(L0_adiru.and..not.L0_obiru) then
         R0_abiru=R0_ebiru
      else
         R0_abiru=max(R_(194)-deltat,0.0)
      endif
      L_ibiru=R0_abiru.gt.0.0
      L0_obiru=L0_adiru
C SRG_logic.fgi( 500, 467):������������  �� T
      L_uxeru=L_ibiru
C SRG_logic.fgi( 520, 467):������,20SRG10AA317YA21
      !{
      Call KLAPAN_MAN(deltat,REAL(R_ixedi,4),R8_etedi,I_odidi
     &,C8_avedi,I_afidi,R_uvedi,
     & R_ovedi,R_eredi,REAL(R_oredi,4),R_isedi,
     & REAL(R_usedi,4),R_uredi,REAL(R_esedi,4),I_edidi,
     & I_efidi,I_udidi,I_adidi,L_atedi,L_ofidi,L_okidi,L_osedi
     &,
     & L_asedi,REAL(R_evedi,4),L_otedi,L_itedi,L_akidi,
     & L_iredi,L_ivedi,L_elidi,L_axedi,L_exedi,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(230),L_upedi,L_(231),L_uxeru
     &,L_ufidi,I_ifidi,L_ukidi,
     & R_ubidi,REAL(R_utedi,4),L_alidi,L_aredi,L_ilidi,L_ateru
     &,L_oxedi,L_uxedi,
     & L_abidi,L_ibidi,L_obidi,L_ebidi)
      !}

      if(L_obidi.or.L_ibidi.or.L_ebidi.or.L_abidi.or.L_uxedi.or.L_oxedi
     &) then      
                  I_ididi = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ididi = z'40000000'
      endif
C SRG_vlv.fgi( 690, 605):���� ���������� ��������,20SRG10AA317
      L_oxeru=L_ibiru
C SRG_logic.fgi( 520, 463):������,20SRG10AA318YA21
      !{
      Call KLAPAN_MAN(deltat,REAL(R_ebixe,4),R8_avexe,I_ifixe
     &,C8_uvexe,I_ufixe,R_oxexe,
     & R_ixexe,R_asexe,REAL(R_isexe,4),R_etexe,
     & REAL(R_otexe,4),R_osexe,REAL(R_atexe,4),I_afixe,
     & I_akixe,I_ofixe,I_udixe,L_utexe,L_ikixe,L_ilixe,L_itexe
     &,
     & L_usexe,REAL(R_axexe,4),L_ivexe,L_evexe,L_ukixe,
     & L_esexe,L_exexe,L_amixe,L_uxexe,L_abixe,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(206),L_orexe,L_(207),L_oxeru
     &,L_okixe,I_ekixe,L_olixe,
     & R_odixe,REAL(R_ovexe,4),L_ulixe,L_urexe,L_emixe,L_useru
     &,L_ibixe,L_obixe,
     & L_ubixe,L_edixe,L_idixe,L_adixe)
      !}

      if(L_idixe.or.L_edixe.or.L_adixe.or.L_ubixe.or.L_obixe.or.L_ibixe
     &) then      
                  I_efixe = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_efixe = z'40000000'
      endif
C SRG_vlv.fgi( 690, 581):���� ���������� ��������,20SRG10AA318
      L_ixeru=L_ibiru
C SRG_logic.fgi( 520, 459):������,20SRG10AA319YA21
      !{
      Call KLAPAN_MAN(deltat,REAL(R_odedi,4),R8_ixadi,I_ukedi
     &,C8_ebedi,I_eledi,R_adedi,
     & R_ubedi,R_itadi,REAL(R_utadi,4),R_ovadi,
     & REAL(R_axadi,4),R_avadi,REAL(R_ivadi,4),I_ikedi,
     & I_iledi,I_aledi,I_ekedi,L_exadi,L_uledi,L_umedi,L_uvadi
     &,
     & L_evadi,REAL(R_ibedi,4),L_uxadi,L_oxadi,L_emedi,
     & L_otadi,L_obedi,L_ipedi,L_ededi,L_idedi,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(228),L_atadi,L_(229),L_ixeru
     &,L_amedi,I_oledi,L_apedi,
     & R_akedi,REAL(R_abedi,4),L_epedi,L_etadi,L_opedi,L_oseru
     &,L_udedi,L_afedi,
     & L_efedi,L_ofedi,L_ufedi,L_ifedi)
      !}

      if(L_ufedi.or.L_ofedi.or.L_ifedi.or.L_efedi.or.L_afedi.or.L_udedi
     &) then      
                  I_okedi = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_okedi = z'40000000'
      endif
C SRG_vlv.fgi( 704, 653):���� ���������� ��������,20SRG10AA319
      L_exeru=L_ibiru
C SRG_logic.fgi( 520, 455):������,20SRG10AA320YA21
      !{
      Call KLAPAN_MAN(deltat,REAL(R_apubi,4),R8_ukubi,I_esubi
     &,C8_olubi,I_osubi,R_imubi,
     & R_emubi,R_udubi,REAL(R_efubi,4),R_akubi,
     & REAL(R_ikubi,4),R_ifubi,REAL(R_ufubi,4),I_urubi,
     & I_usubi,I_isubi,I_orubi,L_okubi,L_etubi,L_evubi,L_ekubi
     &,
     & L_ofubi,REAL(R_ulubi,4),L_elubi,L_alubi,L_otubi,
     & L_afubi,L_amubi,L_uvubi,L_omubi,L_umubi,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(224),L_idubi,L_(225),L_exeru
     &,L_itubi,I_atubi,L_ivubi,
     & R_irubi,REAL(R_ilubi,4),L_ovubi,L_odubi,L_axubi,L_iseru
     &,L_epubi,L_ipubi,
     & L_opubi,L_arubi,L_erubi,L_upubi)
      !}

      if(L_erubi.or.L_arubi.or.L_upubi.or.L_opubi.or.L_ipubi.or.L_epubi
     &) then      
                  I_asubi = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_asubi = z'40000000'
      endif
C SRG_vlv.fgi( 704, 629):���� ���������� ��������,20SRG10AA320
      L_axeru=L_ibiru
C SRG_logic.fgi( 520, 451):������,20SRG10AA321YA21
      !{
      Call KLAPAN_MAN(deltat,REAL(R_ivibi,4),R8_esibi,I_obobi
     &,C8_atibi,I_adobi,R_utibi,
     & R_otibi,R_epibi,REAL(R_opibi,4),R_iribi,
     & REAL(R_uribi,4),R_upibi,REAL(R_eribi,4),I_ebobi,
     & I_edobi,I_ubobi,I_abobi,L_asibi,L_odobi,L_ofobi,L_oribi
     &,
     & L_aribi,REAL(R_etibi,4),L_osibi,L_isibi,L_afobi,
     & L_ipibi,L_itibi,L_ekobi,L_avibi,L_evibi,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(220),L_umibi,L_(221),L_axeru
     &,L_udobi,I_idobi,L_ufobi,
     & R_uxibi,REAL(R_usibi,4),L_akobi,L_apibi,L_ikobi,L_eseru
     &,L_ovibi,L_uvibi,
     & L_axibi,L_ixibi,L_oxibi,L_exibi)
      !}

      if(L_oxibi.or.L_ixibi.or.L_exibi.or.L_axibi.or.L_uvibi.or.L_ovibi
     &) then      
                  I_ibobi = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ibobi = z'40000000'
      endif
C SRG_vlv.fgi( 704, 605):���� ���������� ��������,20SRG10AA321
      L_uveru=L_ibiru
C SRG_logic.fgi( 520, 447):������,20SRG10AA322YA21
      !{
      Call KLAPAN_MAN(deltat,REAL(R_ifexe,4),R8_ebexe,I_olexe
     &,C8_adexe,I_amexe,R_udexe,
     & R_odexe,R_evaxe,REAL(R_ovaxe,4),R_ixaxe,
     & REAL(R_uxaxe,4),R_uvaxe,REAL(R_exaxe,4),I_elexe,
     & I_emexe,I_ulexe,I_alexe,L_abexe,L_omexe,L_opexe,L_oxaxe
     &,
     & L_axaxe,REAL(R_edexe,4),L_obexe,L_ibexe,L_apexe,
     & L_ivaxe,L_idexe,L_erexe,L_afexe,L_efexe,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(204),L_utaxe,L_(205),L_uveru
     &,L_umexe,I_imexe,L_upexe,
     & R_ukexe,REAL(R_ubexe,4),L_arexe,L_avaxe,L_irexe,L_aseru
     &,L_ofexe,L_ufexe,
     & L_akexe,L_ikexe,L_okexe,L_ekexe)
      !}

      if(L_okexe.or.L_ikexe.or.L_ekexe.or.L_akexe.or.L_ufexe.or.L_ofexe
     &) then      
                  I_ilexe = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ilexe = z'40000000'
      endif
C SRG_vlv.fgi( 704, 581):���� ���������� ��������,20SRG10AA322
      L_overu=L_ibiru
C SRG_logic.fgi( 520, 443):������,20SRG10AA323YA21
      !{
      Call KLAPAN_MAN(deltat,REAL(R_ukadi,4),R8_odadi,I_apadi
     &,C8_ifadi,I_ipadi,R_ekadi,
     & R_akadi,R_oxubi,REAL(R_abadi,4),R_ubadi,
     & REAL(R_edadi,4),R_ebadi,REAL(R_obadi,4),I_omadi,
     & I_opadi,I_epadi,I_imadi,L_idadi,L_aradi,L_asadi,L_adadi
     &,
     & L_ibadi,REAL(R_ofadi,4),L_afadi,L_udadi,L_iradi,
     & L_uxubi,L_ufadi,L_osadi,L_ikadi,L_okadi,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(226),L_exubi,L_(227),L_overu
     &,L_eradi,I_upadi,L_esadi,
     & R_emadi,REAL(R_efadi,4),L_isadi,L_ixubi,L_usadi,L_ureru
     &,L_aladi,L_eladi,
     & L_iladi,L_uladi,L_amadi,L_oladi)
      !}

      if(L_amadi.or.L_uladi.or.L_oladi.or.L_iladi.or.L_eladi.or.L_aladi
     &) then      
                  I_umadi = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_umadi = z'40000000'
      endif
C SRG_vlv.fgi( 718, 653):���� ���������� ��������,20SRG10AA323
      L_iveru=L_ibiru
C SRG_logic.fgi( 520, 439):������,20SRG10AA324YA21
      !{
      Call KLAPAN_MAN(deltat,REAL(R_esobi,4),R8_apobi,I_ivobi
     &,C8_upobi,I_uvobi,R_orobi,
     & R_irobi,R_alobi,REAL(R_ilobi,4),R_emobi,
     & REAL(R_omobi,4),R_olobi,REAL(R_amobi,4),I_avobi,
     & I_axobi,I_ovobi,I_utobi,L_umobi,L_ixobi,L_ibubi,L_imobi
     &,
     & L_ulobi,REAL(R_arobi,4),L_ipobi,L_epobi,L_uxobi,
     & L_elobi,L_erobi,L_adubi,L_urobi,L_asobi,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(222),L_okobi,L_(223),L_iveru
     &,L_oxobi,I_exobi,L_obubi,
     & R_otobi,REAL(R_opobi,4),L_ububi,L_ukobi,L_edubi,L_oreru
     &,L_isobi,L_osobi,
     & L_usobi,L_etobi,L_itobi,L_atobi)
      !}

      if(L_itobi.or.L_etobi.or.L_atobi.or.L_usobi.or.L_osobi.or.L_isobi
     &) then      
                  I_evobi = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_evobi = z'40000000'
      endif
C SRG_vlv.fgi( 718, 629):���� ���������� ��������,20SRG10AA324
      L_everu=L_ibiru
C SRG_logic.fgi( 520, 435):������,20SRG10AA325YA21
      !{
      Call KLAPAN_MAN(deltat,REAL(R_obibi,4),R8_ivebi,I_ufibi
     &,C8_exebi,I_ekibi,R_abibi,
     & R_uxebi,R_isebi,REAL(R_usebi,4),R_otebi,
     & REAL(R_avebi,4),R_atebi,REAL(R_itebi,4),I_ifibi,
     & I_ikibi,I_akibi,I_efibi,L_evebi,L_ukibi,L_ulibi,L_utebi
     &,
     & L_etebi,REAL(R_ixebi,4),L_uvebi,L_ovebi,L_elibi,
     & L_osebi,L_oxebi,L_imibi,L_ebibi,L_ibibi,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(218),L_asebi,L_(219),L_everu
     &,L_alibi,I_okibi,L_amibi,
     & R_afibi,REAL(R_axebi,4),L_emibi,L_esebi,L_omibi,L_ireru
     &,L_ubibi,L_adibi,
     & L_edibi,L_odibi,L_udibi,L_idibi)
      !}

      if(L_udibi.or.L_odibi.or.L_idibi.or.L_edibi.or.L_adibi.or.L_ubibi
     &) then      
                  I_ofibi = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ofibi = z'40000000'
      endif
C SRG_vlv.fgi( 718, 605):���� ���������� ��������,20SRG10AA325
      L_averu=L_ibiru
C SRG_logic.fgi( 520, 431):������,20SRG10AA326YA21
      !{
      Call KLAPAN_MAN(deltat,REAL(R_atove,4),R8_upove,I_exove
     &,C8_orove,I_oxove,R_isove,
     & R_esove,R_ulove,REAL(R_emove,4),R_apove,
     & REAL(R_ipove,4),R_imove,REAL(R_umove,4),I_uvove,
     & I_uxove,I_ixove,I_ovove,L_opove,L_ebuve,L_eduve,L_epove
     &,
     & L_omove,REAL(R_urove,4),L_erove,L_arove,L_obuve,
     & L_amove,L_asove,L_uduve,L_osove,L_usove,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(198),L_ilove,L_(199),L_averu
     &,L_ibuve,I_abuve,L_iduve,
     & R_ivove,REAL(R_irove,4),L_oduve,L_olove,L_afuve,L_ereru
     &,L_etove,L_itove,
     & L_otove,L_avove,L_evove,L_utove)
      !}

      if(L_evove.or.L_avove.or.L_utove.or.L_otove.or.L_itove.or.L_etove
     &) then      
                  I_axove = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_axove = z'40000000'
      endif
C SRG_vlv.fgi( 718, 581):���� ���������� ��������,20SRG10AA326
      L_uteru=L_ibiru
C SRG_logic.fgi( 520, 427):������,20SRG10AA327YA21
      !{
      Call KLAPAN_MAN(deltat,REAL(R_olaxe,4),R8_ifaxe,I_upaxe
     &,C8_ekaxe,I_eraxe,R_alaxe,
     & R_ukaxe,R_ibaxe,REAL(R_ubaxe,4),R_odaxe,
     & REAL(R_afaxe,4),R_adaxe,REAL(R_idaxe,4),I_ipaxe,
     & I_iraxe,I_araxe,I_epaxe,L_efaxe,L_uraxe,L_usaxe,L_udaxe
     &,
     & L_edaxe,REAL(R_ikaxe,4),L_ufaxe,L_ofaxe,L_esaxe,
     & L_obaxe,L_okaxe,L_itaxe,L_elaxe,L_ilaxe,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(202),L_abaxe,L_(203),L_uteru
     &,L_asaxe,I_oraxe,L_ataxe,
     & R_apaxe,REAL(R_akaxe,4),L_etaxe,L_ebaxe,L_otaxe,L_areru
     &,L_ulaxe,L_amaxe,
     & L_emaxe,L_omaxe,L_umaxe,L_imaxe)
      !}

      if(L_umaxe.or.L_omaxe.or.L_imaxe.or.L_emaxe.or.L_amaxe.or.L_ulaxe
     &) then      
                  I_opaxe = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_opaxe = z'40000000'
      endif
C SRG_vlv.fgi( 732, 653):���� ���������� ��������,20SRG10AA327
      !{
      Call KLAPAN_MAN(deltat,REAL(R_upuve,4),R8_oluve,I_atuve
     &,C8_imuve,I_ituve,R_epuve,
     & R_apuve,R_ofuve,REAL(R_akuve,4),R_ukuve,
     & REAL(R_eluve,4),R_ekuve,REAL(R_okuve,4),I_osuve,
     & I_otuve,I_etuve,I_isuve,L_iluve,L_avuve,L_axuve,L_aluve
     &,
     & L_ikuve,REAL(R_omuve,4),L_amuve,L_uluve,L_ivuve,
     & L_ufuve,L_umuve,L_oxuve,L_ipuve,L_opuve,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(200),L_efuve,L_(201),L_ibiru
     &,L_evuve,I_utuve,L_exuve,
     & R_esuve,REAL(R_emuve,4),L_ixuve,L_ifuve,L_uxuve,L_uperu
     &,L_aruve,L_eruve,
     & L_iruve,L_uruve,L_asuve,L_oruve)
      !}

      if(L_asuve.or.L_uruve.or.L_oruve.or.L_iruve.or.L_eruve.or.L_aruve
     &) then      
                  I_usuve = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_usuve = z'40000000'
      endif
C SRG_vlv.fgi( 732, 629):���� ���������� ��������,20SRG10AA328
      L_(433) = L_ateru.AND.L_useru.AND.L_oseru.AND.L_iseru.AND.L_eseru.
     &AND.L_aseru.AND.L_ureru.AND.L_oreru.AND.L_ireru.AND.L_ereru.AND.L_
     &areru.AND.L_uperu
C SRG_logic.fgi( 481, 411):�
      L0_operu=(L0_adiru.or.L0_operu).and..not.(L_ubiru)
      L_(431)=.not.L0_operu
C SRG_logic.fgi( 492, 407):RS �������
      L_(432) = L_(433).AND.L0_operu
C SRG_logic.fgi( 500, 410):�
      if(L_(432).and..not.L0_iperu) then
         R0_aperu=R0_eperu
      else
         R0_aperu=max(R_(193)-deltat,0.0)
      endif
      L_oteru=R0_aperu.gt.0.0
      L0_iperu=L_(432)
C SRG_logic.fgi( 508, 410):������������  �� T
      L_eteru=L_oteru
C SRG_logic.fgi( 528, 406):������,20SRG10AA312YA21
      !{
      Call KLAPAN_MAN(deltat,REAL(R_iviki,4),R8_esiki,I_oboki
     &,C8_atiki,I_adoki,R_utiki,
     & R_otiki,R_epiki,REAL(R_opiki,4),R_iriki,
     & REAL(R_uriki,4),R_upiki,REAL(R_eriki,4),I_eboki,
     & I_edoki,I_uboki,I_aboki,L_asiki,L_odoki,L_ofoki,L_oriki
     &,
     & L_ariki,REAL(R_etiki,4),L_osiki,L_isiki,L_afoki,
     & L_ipiki,L_itiki,L_ekoki,L_aviki,L_eviki,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(256),L_eleru,L_(257),L_eteru
     &,L_udoki,I_idoki,L_ufoki,
     & R_uxiki,REAL(R_usiki,4),L_akoki,L_ekeru,L_ikoki,L_ameru
     &,L_oviki,L_uviki,
     & L_axiki,L_ixiki,L_oxiki,L_exiki)
      !}

      if(L_oxiki.or.L_ixiki.or.L_exiki.or.L_axiki.or.L_uviki.or.L_oviki
     &) then      
                  I_iboki = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_iboki = z'40000000'
      endif
C SRG_vlv.fgi( 620, 605):���� ���������� ��������,20SRG10AA312
C sav1=L_(426)
      L_(426) = L_ikeru.AND.L_ekeru
C SRG_logic.fgi( 472, 340):recalc:�
C if(sav1.ne.L_(426) .and. try3401.gt.0) goto 3401
      !{
      Call KLAPAN_MAN(deltat,REAL(R_ufebi,4),R8_obebi,I_amebi
     &,C8_idebi,I_imebi,R_efebi,
     & R_afebi,R_ovabi,REAL(R_axabi,4),R_uxabi,
     & REAL(R_ebebi,4),R_exabi,REAL(R_oxabi,4),I_olebi,
     & I_omebi,I_emebi,I_ilebi,L_ibebi,L_apebi,L_arebi,L_abebi
     &,
     & L_ixabi,REAL(R_odebi,4),L_adebi,L_ubebi,L_ipebi,
     & L_uvabi,L_udebi,L_orebi,L_ifebi,L_ofebi,REAL(R8_elomu
     &,8),
     & REAL(1.0,4),R8_aromu,L_(216),L_evabi,L_(217),L_oteru
     &,L_epebi,I_umebi,L_erebi,
     & R_elebi,REAL(R_edebi,4),L_irebi,L_ivabi,L_urebi,L_uleru
     &,L_akebi,L_ekebi,
     & L_ikebi,L_ukebi,L_alebi,L_okebi)
      !}

      if(L_alebi.or.L_ukebi.or.L_okebi.or.L_ikebi.or.L_ekebi.or.L_akebi
     &) then      
                  I_ulebi = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ulebi = z'40000000'
      endif
C SRG_vlv.fgi( 620, 581):���� ���������� ��������,20SRG10AA313
      L_(429) = L_emeru.AND.L_ameru.AND.L_uleru
C SRG_logic.fgi( 472, 366):�
      L0_oleru=(L0_adiru.or.L0_oleru).and..not.(L_ubiru)
      L_(427)=.not.L0_oleru
C SRG_logic.fgi( 495, 362):RS �������
      L_(428) = L_(429).AND.L0_oleru.AND.L_(430)
C SRG_logic.fgi( 503, 364):�
      if(L_(428).and..not.L0_ileru) then
         R0_ukeru=R0_aleru
      else
         R0_ukeru=max(R_(190)-deltat,0.0)
      endif
      L_eleru=R0_ukeru.gt.0.0
      L0_ileru=L_(428)
C SRG_logic.fgi( 511, 364):������������  �� T
      L_okeru=L_eleru
C SRG_logic.fgi( 530, 364):������,20SRG10AA310YA21
      L_iteru=L_oteru
C SRG_logic.fgi( 528, 410):������,20SRG10AA310YA22
      L0_isepu=L_ubiru.or.(L0_isepu.and..not.(L_(342)))
      L_(343)=.not.L0_isepu
C SRG_logic.fgi( 373, 154):RS �������
      if(L0_isepu) then
         R8_osepu=R_(135)
      else
         R8_osepu=R_(136)
      endif
C SRG_logic.fgi( 443, 175):���� RE IN LO CH7
      L0_akepu=L_ubiru.or.(L0_akepu.and..not.(L_(339)))
      L_(340)=.not.L0_akepu
C SRG_logic.fgi( 373,  95):RS �������
      if(L0_akepu) then
         R8_ekepu=R_(125)
      else
         R8_ekepu=R_(126)
      endif
C SRG_logic.fgi( 443, 116):���� RE IN LO CH7
      Call PUMP_HANDLER(deltat,C30_ipaf,I_asaf,L_esotu,L_exotu
     &,L_upotu,
     & I_esaf,I_uraf,R_emaf,REAL(R_omaf,4),
     & R_olaf,REAL(R_amaf,4),I_isaf,L_ovaf,L_avaf,L_ibef,
     & L_obef,L_eraf,L_etaf,L_otaf,L_itaf,L_utaf,L_asapu,L_ilaf
     &,
     & L_imaf,L_elaf,L_ulaf,L_uxaf,L_(27),
     & L_abef,L_(26),L_evaru,L_aferu,L_iraf,I_osaf,R_axaf
     &,R_exaf,
     & L_edef,L_axomu,L_ebef,REAL(R8_elomu,8),L_ivaf,
     & REAL(R8_evaf,8),R_ubef,REAL(R_usaf,4),R_ataf,REAL(R8_araf
     &,8),R_upaf,
     & R8_aromu,R_adef,R8_evaf,REAL(R_umaf,4),REAL(R_apaf
     &,4))
C SRG_vlv.fgi( 822, 289):���������� ���������� �������,20SRG10AN021
C label 3522  try3522=try3522-1
C sav1=R_adef
C sav2=R_ubef
C sav3=L_ovaf
C sav4=L_asapu
C sav5=L_avaf
C sav6=R8_evaf
C sav7=R_ataf
C sav8=I_osaf
C sav9=I_isaf
C sav10=I_esaf
C sav11=I_asaf
C sav12=I_uraf
C sav13=I_oraf
C sav14=L_iraf
C sav15=L_eraf
C sav16=R_upaf
C sav17=C30_ipaf
C sav18=L_imaf
C sav19=L_ulaf
      Call PUMP_HANDLER(deltat,C30_ipaf,I_asaf,L_esotu,L_exotu
     &,L_upotu,
     & I_esaf,I_uraf,R_emaf,REAL(R_omaf,4),
     & R_olaf,REAL(R_amaf,4),I_isaf,L_ovaf,L_avaf,L_ibef,
     & L_obef,L_eraf,L_etaf,L_otaf,L_itaf,L_utaf,L_asapu,L_ilaf
     &,
     & L_imaf,L_elaf,L_ulaf,L_uxaf,L_(27),
     & L_abef,L_(26),L_evaru,L_aferu,L_iraf,I_osaf,R_axaf
     &,R_exaf,
     & L_edef,L_axomu,L_ebef,REAL(R8_elomu,8),L_ivaf,
     & REAL(R8_evaf,8),R_ubef,REAL(R_usaf,4),R_ataf,REAL(R8_araf
     &,8),R_upaf,
     & R8_aromu,R_adef,R8_evaf,REAL(R_umaf,4),REAL(R_apaf
     &,4))
C SRG_vlv.fgi( 822, 289):recalc:���������� ���������� �������,20SRG10AN021
C if(sav1.ne.R_adef .and. try3522.gt.0) goto 3522
C if(sav2.ne.R_ubef .and. try3522.gt.0) goto 3522
C if(sav3.ne.L_ovaf .and. try3522.gt.0) goto 3522
C if(sav4.ne.L_asapu .and. try3522.gt.0) goto 3522
C if(sav5.ne.L_avaf .and. try3522.gt.0) goto 3522
C if(sav6.ne.R8_evaf .and. try3522.gt.0) goto 3522
C if(sav7.ne.R_ataf .and. try3522.gt.0) goto 3522
C if(sav8.ne.I_osaf .and. try3522.gt.0) goto 3522
C if(sav9.ne.I_isaf .and. try3522.gt.0) goto 3522
C if(sav10.ne.I_esaf .and. try3522.gt.0) goto 3522
C if(sav11.ne.I_asaf .and. try3522.gt.0) goto 3522
C if(sav12.ne.I_uraf .and. try3522.gt.0) goto 3522
C if(sav13.ne.I_oraf .and. try3522.gt.0) goto 3522
C if(sav14.ne.L_iraf .and. try3522.gt.0) goto 3522
C if(sav15.ne.L_eraf .and. try3522.gt.0) goto 3522
C if(sav16.ne.R_upaf .and. try3522.gt.0) goto 3522
C if(sav17.ne.C30_ipaf .and. try3522.gt.0) goto 3522
C if(sav18.ne.L_imaf .and. try3522.gt.0) goto 3522
C if(sav19.ne.L_ulaf .and. try3522.gt.0) goto 3522
      if(L_asapu) then
         I_irapu=I0_orapu
      else
         I_irapu=I0_urapu
      endif
C SRG_logic.fgi( 291, 257):���� RE IN LO CH7
      if(L_asapu) then
         I_etapu=I0_isapu
      else
         I_etapu=I0_esapu
      endif
C SRG_logic.fgi( 291, 267):���� RE IN LO CH7
      End
