      Subroutine DAA_DAB(ext_deltat)
      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      Include 'DAA_DAB.fh'

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      !{
      Call DAT_SAS_4_HANDLER(deltat,L_i,I_o,L_e,I_u)
      !}
C DAA_DAB_lamp.fgi( 131, 196):���������� ������� ��� 4,20DAA07GU001-B02
      !{
      Call DAT_SAS_4_HANDLER(deltat,L_ed,I_id,L_ad,I_od)
      !}
C DAA_DAB_lamp.fgi( 131, 208):���������� ������� ��� 4,20DAA07GU001-B01
      !{
      Call DAT_SAS_4_HANDLER(deltat,L_af,I_ef,L_ud,I_if)
      !}
C DAA_DAB_lamp.fgi( 131, 223):���������� ������� ��� 4,20DAA14GU001-B02
      !{
      Call DAT_SAS_4_HANDLER(deltat,L_uf,I_ak,L_of,I_ek)
      !}
C DAA_DAB_lamp.fgi( 131, 235):���������� ������� ��� 4,20DAA14GU001-B01
      !{
      Call DAT_SAS_4_HANDLER(deltat,L_ok,I_uk,L_ik,I_al)
      !}
C DAA_DAB_lamp.fgi( 131, 250):���������� ������� ��� 4,20DAA13GU001-B02
      !{
      Call DAT_SAS_4_HANDLER(deltat,L_il,I_ol,L_el,I_ul)
      !}
C DAA_DAB_lamp.fgi( 131, 262):���������� ������� ��� 4,20DAA13GU001-B01
      !{
      Call DAT_SAS_4_HANDLER(deltat,L_em,I_im,L_am,I_om)
      !}
C DAA_DAB_lamp.fgi( 113, 282):���������� ������� ��� 4,20DAB01GK206
      !{
      Call DAT_SAS_4_HANDLER(deltat,L_ap,I_ep,L_um,I_ip)
      !}
C DAA_DAB_lamp.fgi( 108, 196):���������� ������� ��� 4,20DAA12GU001-B02
      !{
      Call DAT_SAS_4_HANDLER(deltat,L_up,I_ar,L_op,I_er)
      !}
C DAA_DAB_lamp.fgi( 108, 208):���������� ������� ��� 4,20DAA12GU001-B01
      !{
      Call DAT_SAS_4_HANDLER(deltat,L_or,I_ur,L_ir,I_as)
      !}
C DAA_DAB_lamp.fgi( 108, 223):���������� ������� ��� 4,20DAA06GU001-B02
      !{
      Call DAT_SAS_4_HANDLER(deltat,L_is,I_os,L_es,I_us)
      !}
C DAA_DAB_lamp.fgi( 108, 235):���������� ������� ��� 4,20DAA06GU001-B01
      !{
      Call DAT_SAS_4_HANDLER(deltat,L_et,I_it,L_at,I_ot)
      !}
C DAA_DAB_lamp.fgi( 108, 250):���������� ������� ��� 4,20DAA05GU001-B02
      !{
      Call DAT_SAS_4_HANDLER(deltat,L_av,I_ev,L_ut,I_iv)
      !}
C DAA_DAB_lamp.fgi( 108, 262):���������� ������� ��� 4,20DAA05GU001-B01
      !{
      Call DAT_SAS_4_HANDLER(deltat,L_uv,I_ax,L_ov,I_ex)
      !}
C DAA_DAB_lamp.fgi(  94, 282):���������� ������� ��� 4,20DAB01GK205
      !{
      Call DAT_SAS_4_HANDLER(deltat,L_ox,I_ux,L_ix,I_abe)
      !}
C DAA_DAB_lamp.fgi(  86, 223):���������� ������� ��� 4,20DAA11GU001-B02
      !{
      Call DAT_SAS_4_HANDLER(deltat,L_ibe,I_obe,L_ebe,I_ube
     &)
      !}
C DAA_DAB_lamp.fgi(  86, 235):���������� ������� ��� 4,20DAA11GU001-B01
      !{
      Call DAT_SAS_4_HANDLER(deltat,L_ede,I_ide,L_ade,I_ode
     &)
      !}
C DAA_DAB_lamp.fgi(  86, 250):���������� ������� ��� 4,20DAA10GU001-B02
      !{
      Call DAT_SAS_4_HANDLER(deltat,L_afe,I_efe,L_ude,I_ife
     &)
      !}
C DAA_DAB_lamp.fgi(  86, 262):���������� ������� ��� 4,20DAA10GU001-B01
      !{
      Call DAT_SAS_4_HANDLER(deltat,L_ufe,I_ake,L_ofe,I_eke
     &)
      !}
C DAA_DAB_lamp.fgi(  75, 282):���������� ������� ��� 4,20DAB01GK204
      !{
      Call DAT_SAS_4_HANDLER(deltat,L_oke,I_uke,L_ike,I_ale
     &)
      !}
C DAA_DAB_lamp.fgi(  63, 223):���������� ������� ��� 4,20DAA09GU001-B02
      !{
      Call DAT_SAS_4_HANDLER(deltat,L_ile,I_ole,L_ele,I_ule
     &)
      !}
C DAA_DAB_lamp.fgi(  63, 235):���������� ������� ��� 4,20DAA09GU001-B01
      !{
      Call DAT_SAS_4_HANDLER(deltat,L_eme,I_ime,L_ame,I_ome
     &)
      !}
C DAA_DAB_lamp.fgi(  63, 250):���������� ������� ��� 4,20DAA08GU001-B02
      !{
      Call DAT_SAS_4_HANDLER(deltat,L_ape,I_epe,L_ume,I_ipe
     &)
      !}
C DAA_DAB_lamp.fgi(  63, 262):���������� ������� ��� 4,20DAA08GU001-B01
      !{
      Call DAT_SAS_4_HANDLER(deltat,L_upe,I_are,L_ope,I_ere
     &)
      !}
C DAA_DAB_lamp.fgi(  56, 282):���������� ������� ��� 4,20DAB01GK203
      !{
      Call DAT_SAS_4_HANDLER(deltat,L_ore,I_ure,L_ire,I_ase
     &)
      !}
C DAA_DAB_lamp.fgi(  40, 223):���������� ������� ��� 4,20DAA04GU001-B02
      !{
      Call DAT_SAS_4_HANDLER(deltat,L_ise,I_ose,L_ese,I_use
     &)
      !}
C DAA_DAB_lamp.fgi(  40, 235):���������� ������� ��� 4,20DAA04GU001-B01
      !{
      Call DAT_SAS_4_HANDLER(deltat,L_ete,I_ite,L_ate,I_ote
     &)
      !}
C DAA_DAB_lamp.fgi(  40, 250):���������� ������� ��� 4,20DAA03GU001-B02
      !{
      Call DAT_SAS_4_HANDLER(deltat,L_ave,I_eve,L_ute,I_ive
     &)
      !}
C DAA_DAB_lamp.fgi(  40, 262):���������� ������� ��� 4,20DAA03GU001-B01
      !{
      Call DAT_SAS_4_HANDLER(deltat,L_uve,I_axe,L_ove,I_exe
     &)
      !}
C DAA_DAB_lamp.fgi(  37, 282):���������� ������� ��� 4,20DAB01GK202
      !{
      Call DAT_SAS_4_HANDLER(deltat,L_oxe,I_uxe,L_ixe,I_abi
     &)
      !}
C DAA_DAB_lamp.fgi(  18, 223):���������� ������� ��� 4,20DAA02GU001-B02
      !{
      Call DAT_SAS_4_HANDLER(deltat,L_ibi,I_obi,L_ebi,I_ubi
     &)
      !}
C DAA_DAB_lamp.fgi(  18, 235):���������� ������� ��� 4,20DAA02GU001-B01
      !{
      Call DAT_SAS_4_HANDLER(deltat,L_edi,I_idi,L_adi,I_odi
     &)
      !}
C DAA_DAB_lamp.fgi(  18, 250):���������� ������� ��� 4,20DAA01GU001-B02
      !{
      Call DAT_SAS_4_HANDLER(deltat,L_afi,I_efi,L_udi,I_ifi
     &)
      !}
C DAA_DAB_lamp.fgi(  18, 262):���������� ������� ��� 4,20DAA01GU001-B01
      !{
      Call DAT_SAS_4_HANDLER(deltat,L_ufi,I_aki,L_ofi,I_eki
     &)
      !}
C DAA_DAB_lamp.fgi(  18, 282):���������� ������� ��� 4,20DAB01GK201
      End
