      Subroutine FDA_70boats(ext_deltat)
      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      Include 'FDA_70boats.fh'

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      I_ixir=0
      I_oxir=0
      I_exir=0
      I_axir=0
      I_uvir=0
      I_otir=0
      I_itir=0
      I_etir=0
      I_atir=0
      I_usir=0
      I_eker=0
      I_aker=0
      I_ufer=0
      I_ofer=0
      I_ifer=0
      I_ekir=0
      I_akir=0
      I_ufir=0
      I_ofir=0
      I_ifir=0
      I_eder=0
      I_ader=0
      I_uber=0
      I_ober=0
      I_iber=0
      I_edir=0
      I_adir=0
      I_ubir=0
      I_obir=0
      I_ibir=0
      I_exar=0
      I_axar=0
      I_uvar=0
      I_ovar=0
      I_ivar=0
      I_exer=0
      I_axer=0
      I_uver=0
      I_over=0
      I_iver=0
      I_etar=0
      I_atar=0
      I_usar=0
      I_osar=0
      I_isar=0
      I_eter=0
      I_ater=0
      I_user=0
      I_oser=0
      I_iser=0
      I_erar=0
      I_arar=0
      I_upar=0
      I_opar=0
      I_ipar=0
      I_erer=0
      I_arer=0
      I_uper=0
      I_oper=0
      I_iper=0
      I_emar=0
      I_amar=0
      I_ular=0
      I_olar=0
      I_ilar=0
      I_emer=0
      I_amer=0
      I_uler=0
      I_oler=0
      I_iler=0
      R_ibor=0
      R_obor=0
      R_ebor=0
      R_abor=0
      R_uxir=0
      R_ovir=0
      R_ivir=0
      R_evir=0
      R_avir=0
      R_utir=0
      R_eler=0
      R_aler=0
      R_uker=0
      R_oker=0
      R_iker=0
      R_elir=0
      R_alir=0
      R_ukir=0
      R_okir=0
      R_ikir=0
      R_efer=0
      R_afer=0
      R_uder=0
      R_oder=0
      R_ider=0
      R_efir=0
      R_afir=0
      R_udir=0
      R_odir=0
      R_idir=0
      R_eber=0
      R_aber=0
      R_uxar=0
      R_oxar=0
      R_ixar=0
      R_ebir=0
      R_abir=0
      R_uxer=0
      R_oxer=0
      R_ixer=0
      R_evar=0
      R_avar=0
      R_utar=0
      R_otar=0
      R_itar=0
      R_ever=0
      R_aver=0
      R_uter=0
      R_oter=0
      R_iter=0
      R_esar=0
      R_asar=0
      R_urar=0
      R_orar=0
      R_irar=0
      R_eser=0
      R_aser=0
      R_urer=0
      R_orer=0
      R_irer=0
      R_epar=0
      R_apar=0
      R_umar=0
      R_omar=0
      R_imar=0
      R_eper=0
      R_aper=0
      R_umer=0
      R_omer=0
      R_imer=0
      I_afor=0
      R_efor=0
      I_ufor=0
      R_akor=0
      I_odor=0
      R_udor=0
      I_ifor=0
      R_ofor=0
      I_edor=0
      R_idor=0
      I_esir=0
      R_osir=0
      I_asir=0
      R_isir=0
      I_ubor=0
      R_ador=0
      I_ipir=0
      R_urir=0
      I_epir=0
      R_orir=0
      I_apir=0
      R_irir=0
      I_umir=0
      R_erir=0
      I_omir=0
      R_arir=0
      I_imir=0
      R_upir=0
      I_emir=0
      R_opir=0
      I_ulir=0
      R_amir=0
      I_ilir=0
      R_olir=0
      I_ekor=0
      R_ikor=0
C FDA_70boats.fgi( 126, 250):pre: ���������� ���������� ������� �� ������� FDA60,BOAT70
      I_(10)=I_e !CopyBack
C FDA_70boats.fgi( 125, 264):pre: ������-�������: ���������� ��� �������������� ������,cpy BOAT70:CR
      I_ixir=0
      I_oxir=0
      I_exir=0
      I_axir=0
      I_uvir=0
      I_otir=0
      I_itir=0
      I_etir=0
      I_atir=0
      I_usir=0
      I_eker=0
      I_aker=0
      I_ufer=0
      I_ofer=0
      I_ifer=0
      I_ekir=0
      I_akir=0
      I_ufir=0
      I_ofir=0
      I_ifir=0
      I_eder=0
      I_ader=0
      I_uber=0
      I_ober=0
      I_iber=0
      I_edir=0
      I_adir=0
      I_ubir=0
      I_obir=0
      I_ibir=0
      I_exar=0
      I_axar=0
      I_uvar=0
      I_ovar=0
      I_ivar=0
      I_exer=0
      I_axer=0
      I_uver=0
      I_over=0
      I_iver=0
      I_etar=0
      I_atar=0
      I_usar=0
      I_osar=0
      I_isar=0
      I_eter=0
      I_ater=0
      I_user=0
      I_oser=0
      I_iser=0
      I_erar=0
      I_arar=0
      I_upar=0
      I_opar=0
      I_ipar=0
      I_erer=0
      I_arer=0
      I_uper=0
      I_oper=0
      I_iper=0
      I_emar=0
      I_amar=0
      I_ular=0
      I_olar=0
      I_ilar=0
      I_emer=0
      I_amer=0
      I_uler=0
      I_oler=0
      I_iler=0
      R_ibor=0
      R_obor=0
      R_ebor=0
      R_abor=0
      R_uxir=0
      R_ovir=0
      R_ivir=0
      R_evir=0
      R_avir=0
      R_utir=0
      R_eler=0
      R_aler=0
      R_uker=0
      R_oker=0
      R_iker=0
      R_elir=0
      R_alir=0
      R_ukir=0
      R_okir=0
      R_ikir=0
      R_efer=0
      R_afer=0
      R_uder=0
      R_oder=0
      R_ider=0
      R_efir=0
      R_afir=0
      R_udir=0
      R_odir=0
      R_idir=0
      R_eber=0
      R_aber=0
      R_uxar=0
      R_oxar=0
      R_ixar=0
      R_ebir=0
      R_abir=0
      R_uxer=0
      R_oxer=0
      R_ixer=0
      R_evar=0
      R_avar=0
      R_utar=0
      R_otar=0
      R_itar=0
      R_ever=0
      R_aver=0
      R_uter=0
      R_oter=0
      R_iter=0
      R_esar=0
      R_asar=0
      R_urar=0
      R_orar=0
      R_irar=0
      R_eser=0
      R_aser=0
      R_urer=0
      R_orer=0
      R_irer=0
      R_epar=0
      R_apar=0
      R_umar=0
      R_omar=0
      R_imar=0
      R_eper=0
      R_aper=0
      R_umer=0
      R_omer=0
      R_imer=0
      I_afor=0
      R_efor=0
      I_ufor=0
      R_akor=0
      I_odor=0
      R_udor=0
      I_ifor=0
      R_ofor=0
      I_edor=0
      R_idor=0
      I_esir=0
      R_osir=0
      I_asir=0
      R_isir=0
      I_ubor=0
      R_ador=0
      I_ipir=0
      R_urir=0
      I_epir=0
      R_orir=0
      I_apir=0
      R_irir=0
      I_umir=0
      R_erir=0
      I_omir=0
      R_arir=0
      I_imir=0
      R_upir=0
      I_emir=0
      R_opir=0
      I_ulir=0
      R_amir=0
      I_ilir=0
      R_olir=0
      I_ekor=0
      R_ikor=0
C FDA_70boats.fgi( 146, 250):pre: ���������� ���������� ������� �� ������� FDA60,BOAT69
      I_(9)=I_i !CopyBack
C FDA_70boats.fgi( 145, 264):pre: ������-�������: ���������� ��� �������������� ������,cpy BOAT69:CR
      I_ixir=0
      I_oxir=0
      I_exir=0
      I_axir=0
      I_uvir=0
      I_otir=0
      I_itir=0
      I_etir=0
      I_atir=0
      I_usir=0
      I_eker=0
      I_aker=0
      I_ufer=0
      I_ofer=0
      I_ifer=0
      I_ekir=0
      I_akir=0
      I_ufir=0
      I_ofir=0
      I_ifir=0
      I_eder=0
      I_ader=0
      I_uber=0
      I_ober=0
      I_iber=0
      I_edir=0
      I_adir=0
      I_ubir=0
      I_obir=0
      I_ibir=0
      I_exar=0
      I_axar=0
      I_uvar=0
      I_ovar=0
      I_ivar=0
      I_exer=0
      I_axer=0
      I_uver=0
      I_over=0
      I_iver=0
      I_etar=0
      I_atar=0
      I_usar=0
      I_osar=0
      I_isar=0
      I_eter=0
      I_ater=0
      I_user=0
      I_oser=0
      I_iser=0
      I_erar=0
      I_arar=0
      I_upar=0
      I_opar=0
      I_ipar=0
      I_erer=0
      I_arer=0
      I_uper=0
      I_oper=0
      I_iper=0
      I_emar=0
      I_amar=0
      I_ular=0
      I_olar=0
      I_ilar=0
      I_emer=0
      I_amer=0
      I_uler=0
      I_oler=0
      I_iler=0
      R_ibor=0
      R_obor=0
      R_ebor=0
      R_abor=0
      R_uxir=0
      R_ovir=0
      R_ivir=0
      R_evir=0
      R_avir=0
      R_utir=0
      R_eler=0
      R_aler=0
      R_uker=0
      R_oker=0
      R_iker=0
      R_elir=0
      R_alir=0
      R_ukir=0
      R_okir=0
      R_ikir=0
      R_efer=0
      R_afer=0
      R_uder=0
      R_oder=0
      R_ider=0
      R_efir=0
      R_afir=0
      R_udir=0
      R_odir=0
      R_idir=0
      R_eber=0
      R_aber=0
      R_uxar=0
      R_oxar=0
      R_ixar=0
      R_ebir=0
      R_abir=0
      R_uxer=0
      R_oxer=0
      R_ixer=0
      R_evar=0
      R_avar=0
      R_utar=0
      R_otar=0
      R_itar=0
      R_ever=0
      R_aver=0
      R_uter=0
      R_oter=0
      R_iter=0
      R_esar=0
      R_asar=0
      R_urar=0
      R_orar=0
      R_irar=0
      R_eser=0
      R_aser=0
      R_urer=0
      R_orer=0
      R_irer=0
      R_epar=0
      R_apar=0
      R_umar=0
      R_omar=0
      R_imar=0
      R_eper=0
      R_aper=0
      R_umer=0
      R_omer=0
      R_imer=0
      I_afor=0
      R_efor=0
      I_ufor=0
      R_akor=0
      I_odor=0
      R_udor=0
      I_ifor=0
      R_ofor=0
      I_edor=0
      R_idor=0
      I_esir=0
      R_osir=0
      I_asir=0
      R_isir=0
      I_ubor=0
      R_ador=0
      I_ipir=0
      R_urir=0
      I_epir=0
      R_orir=0
      I_apir=0
      R_irir=0
      I_umir=0
      R_erir=0
      I_omir=0
      R_arir=0
      I_imir=0
      R_upir=0
      I_emir=0
      R_opir=0
      I_ulir=0
      R_amir=0
      I_ilir=0
      R_olir=0
      I_ekor=0
      R_ikor=0
C FDA_70boats.fgi( 166, 250):pre: ���������� ���������� ������� �� ������� FDA60,BOAT68
      I_(8)=I_o !CopyBack
C FDA_70boats.fgi( 165, 264):pre: ������-�������: ���������� ��� �������������� ������,cpy BOAT68:CR
      I_ixir=0
      I_oxir=0
      I_exir=0
      I_axir=0
      I_uvir=0
      I_otir=0
      I_itir=0
      I_etir=0
      I_atir=0
      I_usir=0
      I_eker=0
      I_aker=0
      I_ufer=0
      I_ofer=0
      I_ifer=0
      I_ekir=0
      I_akir=0
      I_ufir=0
      I_ofir=0
      I_ifir=0
      I_eder=0
      I_ader=0
      I_uber=0
      I_ober=0
      I_iber=0
      I_edir=0
      I_adir=0
      I_ubir=0
      I_obir=0
      I_ibir=0
      I_exar=0
      I_axar=0
      I_uvar=0
      I_ovar=0
      I_ivar=0
      I_exer=0
      I_axer=0
      I_uver=0
      I_over=0
      I_iver=0
      I_etar=0
      I_atar=0
      I_usar=0
      I_osar=0
      I_isar=0
      I_eter=0
      I_ater=0
      I_user=0
      I_oser=0
      I_iser=0
      I_erar=0
      I_arar=0
      I_upar=0
      I_opar=0
      I_ipar=0
      I_erer=0
      I_arer=0
      I_uper=0
      I_oper=0
      I_iper=0
      I_emar=0
      I_amar=0
      I_ular=0
      I_olar=0
      I_ilar=0
      I_emer=0
      I_amer=0
      I_uler=0
      I_oler=0
      I_iler=0
      R_ibor=0
      R_obor=0
      R_ebor=0
      R_abor=0
      R_uxir=0
      R_ovir=0
      R_ivir=0
      R_evir=0
      R_avir=0
      R_utir=0
      R_eler=0
      R_aler=0
      R_uker=0
      R_oker=0
      R_iker=0
      R_elir=0
      R_alir=0
      R_ukir=0
      R_okir=0
      R_ikir=0
      R_efer=0
      R_afer=0
      R_uder=0
      R_oder=0
      R_ider=0
      R_efir=0
      R_afir=0
      R_udir=0
      R_odir=0
      R_idir=0
      R_eber=0
      R_aber=0
      R_uxar=0
      R_oxar=0
      R_ixar=0
      R_ebir=0
      R_abir=0
      R_uxer=0
      R_oxer=0
      R_ixer=0
      R_evar=0
      R_avar=0
      R_utar=0
      R_otar=0
      R_itar=0
      R_ever=0
      R_aver=0
      R_uter=0
      R_oter=0
      R_iter=0
      R_esar=0
      R_asar=0
      R_urar=0
      R_orar=0
      R_irar=0
      R_eser=0
      R_aser=0
      R_urer=0
      R_orer=0
      R_irer=0
      R_epar=0
      R_apar=0
      R_umar=0
      R_omar=0
      R_imar=0
      R_eper=0
      R_aper=0
      R_umer=0
      R_omer=0
      R_imer=0
      I_afor=0
      R_efor=0
      I_ufor=0
      R_akor=0
      I_odor=0
      R_udor=0
      I_ifor=0
      R_ofor=0
      I_edor=0
      R_idor=0
      I_esir=0
      R_osir=0
      I_asir=0
      R_isir=0
      I_ubor=0
      R_ador=0
      I_ipir=0
      R_urir=0
      I_epir=0
      R_orir=0
      I_apir=0
      R_irir=0
      I_umir=0
      R_erir=0
      I_omir=0
      R_arir=0
      I_imir=0
      R_upir=0
      I_emir=0
      R_opir=0
      I_ulir=0
      R_amir=0
      I_ilir=0
      R_olir=0
      I_ekor=0
      R_ikor=0
C FDA_70boats.fgi( 186, 250):pre: ���������� ���������� ������� �� ������� FDA60,BOAT67
      I_(7)=I_u !CopyBack
C FDA_70boats.fgi( 185, 264):pre: ������-�������: ���������� ��� �������������� ������,cpy BOAT67:CR
      I_ixir=0
      I_oxir=0
      I_exir=0
      I_axir=0
      I_uvir=0
      I_otir=0
      I_itir=0
      I_etir=0
      I_atir=0
      I_usir=0
      I_eker=0
      I_aker=0
      I_ufer=0
      I_ofer=0
      I_ifer=0
      I_ekir=0
      I_akir=0
      I_ufir=0
      I_ofir=0
      I_ifir=0
      I_eder=0
      I_ader=0
      I_uber=0
      I_ober=0
      I_iber=0
      I_edir=0
      I_adir=0
      I_ubir=0
      I_obir=0
      I_ibir=0
      I_exar=0
      I_axar=0
      I_uvar=0
      I_ovar=0
      I_ivar=0
      I_exer=0
      I_axer=0
      I_uver=0
      I_over=0
      I_iver=0
      I_etar=0
      I_atar=0
      I_usar=0
      I_osar=0
      I_isar=0
      I_eter=0
      I_ater=0
      I_user=0
      I_oser=0
      I_iser=0
      I_erar=0
      I_arar=0
      I_upar=0
      I_opar=0
      I_ipar=0
      I_erer=0
      I_arer=0
      I_uper=0
      I_oper=0
      I_iper=0
      I_emar=0
      I_amar=0
      I_ular=0
      I_olar=0
      I_ilar=0
      I_emer=0
      I_amer=0
      I_uler=0
      I_oler=0
      I_iler=0
      R_ibor=0
      R_obor=0
      R_ebor=0
      R_abor=0
      R_uxir=0
      R_ovir=0
      R_ivir=0
      R_evir=0
      R_avir=0
      R_utir=0
      R_eler=0
      R_aler=0
      R_uker=0
      R_oker=0
      R_iker=0
      R_elir=0
      R_alir=0
      R_ukir=0
      R_okir=0
      R_ikir=0
      R_efer=0
      R_afer=0
      R_uder=0
      R_oder=0
      R_ider=0
      R_efir=0
      R_afir=0
      R_udir=0
      R_odir=0
      R_idir=0
      R_eber=0
      R_aber=0
      R_uxar=0
      R_oxar=0
      R_ixar=0
      R_ebir=0
      R_abir=0
      R_uxer=0
      R_oxer=0
      R_ixer=0
      R_evar=0
      R_avar=0
      R_utar=0
      R_otar=0
      R_itar=0
      R_ever=0
      R_aver=0
      R_uter=0
      R_oter=0
      R_iter=0
      R_esar=0
      R_asar=0
      R_urar=0
      R_orar=0
      R_irar=0
      R_eser=0
      R_aser=0
      R_urer=0
      R_orer=0
      R_irer=0
      R_epar=0
      R_apar=0
      R_umar=0
      R_omar=0
      R_imar=0
      R_eper=0
      R_aper=0
      R_umer=0
      R_omer=0
      R_imer=0
      I_afor=0
      R_efor=0
      I_ufor=0
      R_akor=0
      I_odor=0
      R_udor=0
      I_ifor=0
      R_ofor=0
      I_edor=0
      R_idor=0
      I_esir=0
      R_osir=0
      I_asir=0
      R_isir=0
      I_ubor=0
      R_ador=0
      I_ipir=0
      R_urir=0
      I_epir=0
      R_orir=0
      I_apir=0
      R_irir=0
      I_umir=0
      R_erir=0
      I_omir=0
      R_arir=0
      I_imir=0
      R_upir=0
      I_emir=0
      R_opir=0
      I_ulir=0
      R_amir=0
      I_ilir=0
      R_olir=0
      I_ekor=0
      R_ikor=0
C FDA_70boats.fgi( 206, 250):pre: ���������� ���������� ������� �� ������� FDA60,BOAT66
      I_(6)=I_ad !CopyBack
C FDA_70boats.fgi( 205, 264):pre: ������-�������: ���������� ��� �������������� ������,cpy BOAT66:CR
      I_ixir=0
      I_oxir=0
      I_exir=0
      I_axir=0
      I_uvir=0
      I_otir=0
      I_itir=0
      I_etir=0
      I_atir=0
      I_usir=0
      I_eker=0
      I_aker=0
      I_ufer=0
      I_ofer=0
      I_ifer=0
      I_ekir=0
      I_akir=0
      I_ufir=0
      I_ofir=0
      I_ifir=0
      I_eder=0
      I_ader=0
      I_uber=0
      I_ober=0
      I_iber=0
      I_edir=0
      I_adir=0
      I_ubir=0
      I_obir=0
      I_ibir=0
      I_exar=0
      I_axar=0
      I_uvar=0
      I_ovar=0
      I_ivar=0
      I_exer=0
      I_axer=0
      I_uver=0
      I_over=0
      I_iver=0
      I_etar=0
      I_atar=0
      I_usar=0
      I_osar=0
      I_isar=0
      I_eter=0
      I_ater=0
      I_user=0
      I_oser=0
      I_iser=0
      I_erar=0
      I_arar=0
      I_upar=0
      I_opar=0
      I_ipar=0
      I_erer=0
      I_arer=0
      I_uper=0
      I_oper=0
      I_iper=0
      I_emar=0
      I_amar=0
      I_ular=0
      I_olar=0
      I_ilar=0
      I_emer=0
      I_amer=0
      I_uler=0
      I_oler=0
      I_iler=0
      R_ibor=0
      R_obor=0
      R_ebor=0
      R_abor=0
      R_uxir=0
      R_ovir=0
      R_ivir=0
      R_evir=0
      R_avir=0
      R_utir=0
      R_eler=0
      R_aler=0
      R_uker=0
      R_oker=0
      R_iker=0
      R_elir=0
      R_alir=0
      R_ukir=0
      R_okir=0
      R_ikir=0
      R_efer=0
      R_afer=0
      R_uder=0
      R_oder=0
      R_ider=0
      R_efir=0
      R_afir=0
      R_udir=0
      R_odir=0
      R_idir=0
      R_eber=0
      R_aber=0
      R_uxar=0
      R_oxar=0
      R_ixar=0
      R_ebir=0
      R_abir=0
      R_uxer=0
      R_oxer=0
      R_ixer=0
      R_evar=0
      R_avar=0
      R_utar=0
      R_otar=0
      R_itar=0
      R_ever=0
      R_aver=0
      R_uter=0
      R_oter=0
      R_iter=0
      R_esar=0
      R_asar=0
      R_urar=0
      R_orar=0
      R_irar=0
      R_eser=0
      R_aser=0
      R_urer=0
      R_orer=0
      R_irer=0
      R_epar=0
      R_apar=0
      R_umar=0
      R_omar=0
      R_imar=0
      R_eper=0
      R_aper=0
      R_umer=0
      R_omer=0
      R_imer=0
      I_afor=0
      R_efor=0
      I_ufor=0
      R_akor=0
      I_odor=0
      R_udor=0
      I_ifor=0
      R_ofor=0
      I_edor=0
      R_idor=0
      I_esir=0
      R_osir=0
      I_asir=0
      R_isir=0
      I_ubor=0
      R_ador=0
      I_ipir=0
      R_urir=0
      I_epir=0
      R_orir=0
      I_apir=0
      R_irir=0
      I_umir=0
      R_erir=0
      I_omir=0
      R_arir=0
      I_imir=0
      R_upir=0
      I_emir=0
      R_opir=0
      I_ulir=0
      R_amir=0
      I_ilir=0
      R_olir=0
      I_ekor=0
      R_ikor=0
C FDA_70boats.fgi( 226, 250):pre: ���������� ���������� ������� �� ������� FDA60,BOAT65
      I_(5)=I_ed !CopyBack
C FDA_70boats.fgi( 225, 264):pre: ������-�������: ���������� ��� �������������� ������,cpy BOAT65:CR
      I_ixir=0
      I_oxir=0
      I_exir=0
      I_axir=0
      I_uvir=0
      I_otir=0
      I_itir=0
      I_etir=0
      I_atir=0
      I_usir=0
      I_eker=0
      I_aker=0
      I_ufer=0
      I_ofer=0
      I_ifer=0
      I_ekir=0
      I_akir=0
      I_ufir=0
      I_ofir=0
      I_ifir=0
      I_eder=0
      I_ader=0
      I_uber=0
      I_ober=0
      I_iber=0
      I_edir=0
      I_adir=0
      I_ubir=0
      I_obir=0
      I_ibir=0
      I_exar=0
      I_axar=0
      I_uvar=0
      I_ovar=0
      I_ivar=0
      I_exer=0
      I_axer=0
      I_uver=0
      I_over=0
      I_iver=0
      I_etar=0
      I_atar=0
      I_usar=0
      I_osar=0
      I_isar=0
      I_eter=0
      I_ater=0
      I_user=0
      I_oser=0
      I_iser=0
      I_erar=0
      I_arar=0
      I_upar=0
      I_opar=0
      I_ipar=0
      I_erer=0
      I_arer=0
      I_uper=0
      I_oper=0
      I_iper=0
      I_emar=0
      I_amar=0
      I_ular=0
      I_olar=0
      I_ilar=0
      I_emer=0
      I_amer=0
      I_uler=0
      I_oler=0
      I_iler=0
      R_ibor=0
      R_obor=0
      R_ebor=0
      R_abor=0
      R_uxir=0
      R_ovir=0
      R_ivir=0
      R_evir=0
      R_avir=0
      R_utir=0
      R_eler=0
      R_aler=0
      R_uker=0
      R_oker=0
      R_iker=0
      R_elir=0
      R_alir=0
      R_ukir=0
      R_okir=0
      R_ikir=0
      R_efer=0
      R_afer=0
      R_uder=0
      R_oder=0
      R_ider=0
      R_efir=0
      R_afir=0
      R_udir=0
      R_odir=0
      R_idir=0
      R_eber=0
      R_aber=0
      R_uxar=0
      R_oxar=0
      R_ixar=0
      R_ebir=0
      R_abir=0
      R_uxer=0
      R_oxer=0
      R_ixer=0
      R_evar=0
      R_avar=0
      R_utar=0
      R_otar=0
      R_itar=0
      R_ever=0
      R_aver=0
      R_uter=0
      R_oter=0
      R_iter=0
      R_esar=0
      R_asar=0
      R_urar=0
      R_orar=0
      R_irar=0
      R_eser=0
      R_aser=0
      R_urer=0
      R_orer=0
      R_irer=0
      R_epar=0
      R_apar=0
      R_umar=0
      R_omar=0
      R_imar=0
      R_eper=0
      R_aper=0
      R_umer=0
      R_omer=0
      R_imer=0
      I_afor=0
      R_efor=0
      I_ufor=0
      R_akor=0
      I_odor=0
      R_udor=0
      I_ifor=0
      R_ofor=0
      I_edor=0
      R_idor=0
      I_esir=0
      R_osir=0
      I_asir=0
      R_isir=0
      I_ubor=0
      R_ador=0
      I_ipir=0
      R_urir=0
      I_epir=0
      R_orir=0
      I_apir=0
      R_irir=0
      I_umir=0
      R_erir=0
      I_omir=0
      R_arir=0
      I_imir=0
      R_upir=0
      I_emir=0
      R_opir=0
      I_ulir=0
      R_amir=0
      I_ilir=0
      R_olir=0
      I_ekor=0
      R_ikor=0
C FDA_70boats.fgi( 246, 250):pre: ���������� ���������� ������� �� ������� FDA60,BOAT64
      I_(4)=I_id !CopyBack
C FDA_70boats.fgi( 245, 264):pre: ������-�������: ���������� ��� �������������� ������,cpy BOAT64:CR
      I_ixir=0
      I_oxir=0
      I_exir=0
      I_axir=0
      I_uvir=0
      I_otir=0
      I_itir=0
      I_etir=0
      I_atir=0
      I_usir=0
      I_eker=0
      I_aker=0
      I_ufer=0
      I_ofer=0
      I_ifer=0
      I_ekir=0
      I_akir=0
      I_ufir=0
      I_ofir=0
      I_ifir=0
      I_eder=0
      I_ader=0
      I_uber=0
      I_ober=0
      I_iber=0
      I_edir=0
      I_adir=0
      I_ubir=0
      I_obir=0
      I_ibir=0
      I_exar=0
      I_axar=0
      I_uvar=0
      I_ovar=0
      I_ivar=0
      I_exer=0
      I_axer=0
      I_uver=0
      I_over=0
      I_iver=0
      I_etar=0
      I_atar=0
      I_usar=0
      I_osar=0
      I_isar=0
      I_eter=0
      I_ater=0
      I_user=0
      I_oser=0
      I_iser=0
      I_erar=0
      I_arar=0
      I_upar=0
      I_opar=0
      I_ipar=0
      I_erer=0
      I_arer=0
      I_uper=0
      I_oper=0
      I_iper=0
      I_emar=0
      I_amar=0
      I_ular=0
      I_olar=0
      I_ilar=0
      I_emer=0
      I_amer=0
      I_uler=0
      I_oler=0
      I_iler=0
      R_ibor=0
      R_obor=0
      R_ebor=0
      R_abor=0
      R_uxir=0
      R_ovir=0
      R_ivir=0
      R_evir=0
      R_avir=0
      R_utir=0
      R_eler=0
      R_aler=0
      R_uker=0
      R_oker=0
      R_iker=0
      R_elir=0
      R_alir=0
      R_ukir=0
      R_okir=0
      R_ikir=0
      R_efer=0
      R_afer=0
      R_uder=0
      R_oder=0
      R_ider=0
      R_efir=0
      R_afir=0
      R_udir=0
      R_odir=0
      R_idir=0
      R_eber=0
      R_aber=0
      R_uxar=0
      R_oxar=0
      R_ixar=0
      R_ebir=0
      R_abir=0
      R_uxer=0
      R_oxer=0
      R_ixer=0
      R_evar=0
      R_avar=0
      R_utar=0
      R_otar=0
      R_itar=0
      R_ever=0
      R_aver=0
      R_uter=0
      R_oter=0
      R_iter=0
      R_esar=0
      R_asar=0
      R_urar=0
      R_orar=0
      R_irar=0
      R_eser=0
      R_aser=0
      R_urer=0
      R_orer=0
      R_irer=0
      R_epar=0
      R_apar=0
      R_umar=0
      R_omar=0
      R_imar=0
      R_eper=0
      R_aper=0
      R_umer=0
      R_omer=0
      R_imer=0
      I_afor=0
      R_efor=0
      I_ufor=0
      R_akor=0
      I_odor=0
      R_udor=0
      I_ifor=0
      R_ofor=0
      I_edor=0
      R_idor=0
      I_esir=0
      R_osir=0
      I_asir=0
      R_isir=0
      I_ubor=0
      R_ador=0
      I_ipir=0
      R_urir=0
      I_epir=0
      R_orir=0
      I_apir=0
      R_irir=0
      I_umir=0
      R_erir=0
      I_omir=0
      R_arir=0
      I_imir=0
      R_upir=0
      I_emir=0
      R_opir=0
      I_ulir=0
      R_amir=0
      I_ilir=0
      R_olir=0
      I_ekor=0
      R_ikor=0
C FDA_70boats.fgi( 266, 250):pre: ���������� ���������� ������� �� ������� FDA60,BOAT63
      I_(3)=I_od !CopyBack
C FDA_70boats.fgi( 265, 264):pre: ������-�������: ���������� ��� �������������� ������,cpy BOAT63:CR
      I_ixir=0
      I_oxir=0
      I_exir=0
      I_axir=0
      I_uvir=0
      I_otir=0
      I_itir=0
      I_etir=0
      I_atir=0
      I_usir=0
      I_eker=0
      I_aker=0
      I_ufer=0
      I_ofer=0
      I_ifer=0
      I_ekir=0
      I_akir=0
      I_ufir=0
      I_ofir=0
      I_ifir=0
      I_eder=0
      I_ader=0
      I_uber=0
      I_ober=0
      I_iber=0
      I_edir=0
      I_adir=0
      I_ubir=0
      I_obir=0
      I_ibir=0
      I_exar=0
      I_axar=0
      I_uvar=0
      I_ovar=0
      I_ivar=0
      I_exer=0
      I_axer=0
      I_uver=0
      I_over=0
      I_iver=0
      I_etar=0
      I_atar=0
      I_usar=0
      I_osar=0
      I_isar=0
      I_eter=0
      I_ater=0
      I_user=0
      I_oser=0
      I_iser=0
      I_erar=0
      I_arar=0
      I_upar=0
      I_opar=0
      I_ipar=0
      I_erer=0
      I_arer=0
      I_uper=0
      I_oper=0
      I_iper=0
      I_emar=0
      I_amar=0
      I_ular=0
      I_olar=0
      I_ilar=0
      I_emer=0
      I_amer=0
      I_uler=0
      I_oler=0
      I_iler=0
      R_ibor=0
      R_obor=0
      R_ebor=0
      R_abor=0
      R_uxir=0
      R_ovir=0
      R_ivir=0
      R_evir=0
      R_avir=0
      R_utir=0
      R_eler=0
      R_aler=0
      R_uker=0
      R_oker=0
      R_iker=0
      R_elir=0
      R_alir=0
      R_ukir=0
      R_okir=0
      R_ikir=0
      R_efer=0
      R_afer=0
      R_uder=0
      R_oder=0
      R_ider=0
      R_efir=0
      R_afir=0
      R_udir=0
      R_odir=0
      R_idir=0
      R_eber=0
      R_aber=0
      R_uxar=0
      R_oxar=0
      R_ixar=0
      R_ebir=0
      R_abir=0
      R_uxer=0
      R_oxer=0
      R_ixer=0
      R_evar=0
      R_avar=0
      R_utar=0
      R_otar=0
      R_itar=0
      R_ever=0
      R_aver=0
      R_uter=0
      R_oter=0
      R_iter=0
      R_esar=0
      R_asar=0
      R_urar=0
      R_orar=0
      R_irar=0
      R_eser=0
      R_aser=0
      R_urer=0
      R_orer=0
      R_irer=0
      R_epar=0
      R_apar=0
      R_umar=0
      R_omar=0
      R_imar=0
      R_eper=0
      R_aper=0
      R_umer=0
      R_omer=0
      R_imer=0
      I_afor=0
      R_efor=0
      I_ufor=0
      R_akor=0
      I_odor=0
      R_udor=0
      I_ifor=0
      R_ofor=0
      I_edor=0
      R_idor=0
      I_esir=0
      R_osir=0
      I_asir=0
      R_isir=0
      I_ubor=0
      R_ador=0
      I_ipir=0
      R_urir=0
      I_epir=0
      R_orir=0
      I_apir=0
      R_irir=0
      I_umir=0
      R_erir=0
      I_omir=0
      R_arir=0
      I_imir=0
      R_upir=0
      I_emir=0
      R_opir=0
      I_ulir=0
      R_amir=0
      I_ilir=0
      R_olir=0
      I_ekor=0
      R_ikor=0
C FDA_70boats.fgi( 286, 250):pre: ���������� ���������� ������� �� ������� FDA60,BOAT62
      I_(2)=I_ud !CopyBack
C FDA_70boats.fgi( 285, 264):pre: ������-�������: ���������� ��� �������������� ������,cpy BOAT62:CR
      I_ixir=0
      I_oxir=0
      I_exir=0
      I_axir=0
      I_uvir=0
      I_otir=0
      I_itir=0
      I_etir=0
      I_atir=0
      I_usir=0
      I_eker=0
      I_aker=0
      I_ufer=0
      I_ofer=0
      I_ifer=0
      I_ekir=0
      I_akir=0
      I_ufir=0
      I_ofir=0
      I_ifir=0
      I_eder=0
      I_ader=0
      I_uber=0
      I_ober=0
      I_iber=0
      I_edir=0
      I_adir=0
      I_ubir=0
      I_obir=0
      I_ibir=0
      I_exar=0
      I_axar=0
      I_uvar=0
      I_ovar=0
      I_ivar=0
      I_exer=0
      I_axer=0
      I_uver=0
      I_over=0
      I_iver=0
      I_etar=0
      I_atar=0
      I_usar=0
      I_osar=0
      I_isar=0
      I_eter=0
      I_ater=0
      I_user=0
      I_oser=0
      I_iser=0
      I_erar=0
      I_arar=0
      I_upar=0
      I_opar=0
      I_ipar=0
      I_erer=0
      I_arer=0
      I_uper=0
      I_oper=0
      I_iper=0
      I_emar=0
      I_amar=0
      I_ular=0
      I_olar=0
      I_ilar=0
      I_emer=0
      I_amer=0
      I_uler=0
      I_oler=0
      I_iler=0
      R_ibor=0
      R_obor=0
      R_ebor=0
      R_abor=0
      R_uxir=0
      R_ovir=0
      R_ivir=0
      R_evir=0
      R_avir=0
      R_utir=0
      R_eler=0
      R_aler=0
      R_uker=0
      R_oker=0
      R_iker=0
      R_elir=0
      R_alir=0
      R_ukir=0
      R_okir=0
      R_ikir=0
      R_efer=0
      R_afer=0
      R_uder=0
      R_oder=0
      R_ider=0
      R_efir=0
      R_afir=0
      R_udir=0
      R_odir=0
      R_idir=0
      R_eber=0
      R_aber=0
      R_uxar=0
      R_oxar=0
      R_ixar=0
      R_ebir=0
      R_abir=0
      R_uxer=0
      R_oxer=0
      R_ixer=0
      R_evar=0
      R_avar=0
      R_utar=0
      R_otar=0
      R_itar=0
      R_ever=0
      R_aver=0
      R_uter=0
      R_oter=0
      R_iter=0
      R_esar=0
      R_asar=0
      R_urar=0
      R_orar=0
      R_irar=0
      R_eser=0
      R_aser=0
      R_urer=0
      R_orer=0
      R_irer=0
      R_epar=0
      R_apar=0
      R_umar=0
      R_omar=0
      R_imar=0
      R_eper=0
      R_aper=0
      R_umer=0
      R_omer=0
      R_imer=0
      I_afor=0
      R_efor=0
      I_ufor=0
      R_akor=0
      I_odor=0
      R_udor=0
      I_ifor=0
      R_ofor=0
      I_edor=0
      R_idor=0
      I_esir=0
      R_osir=0
      I_asir=0
      R_isir=0
      I_ubor=0
      R_ador=0
      I_ipir=0
      R_urir=0
      I_epir=0
      R_orir=0
      I_apir=0
      R_irir=0
      I_umir=0
      R_erir=0
      I_omir=0
      R_arir=0
      I_imir=0
      R_upir=0
      I_emir=0
      R_opir=0
      I_ulir=0
      R_amir=0
      I_ilir=0
      R_olir=0
      I_ekor=0
      R_ikor=0
C FDA_70boats.fgi( 306, 250):pre: ���������� ���������� ������� �� ������� FDA60,BOAT61
      I_(1)=I_af !CopyBack
C FDA_70boats.fgi( 305, 264):pre: ������-�������: ���������� ��� �������������� ������,cpy BOAT61:CR
      I_amuk=z'1000021'
      I_ufuk=z'1000021'
      I_obuk=z'1000021'
      I_ivok=z'1000021'
      I_esok=z'1000021'
      I_apok=z'1000021'
      I_ukok=z'1000021'
      I_odok=z'1000021'
      I_edok=z'1000021'
      I_uluk=z'1000021'
      I_oluk=z'1000021'
      I_iluk=z'1000021'
      I_eluk=z'1000021'
      I_aluk=z'1000021'
      I_ukuk=z'1000021'
      I_okuk=z'1000021'
      I_ikuk=z'1000021'
      I_ekuk=z'1000021'
      I_akuk=z'1000021'
      I_ofuk=z'1000021'
      I_ifuk=z'1000021'
      I_efuk=z'1000021'
      I_afuk=z'1000021'
      I_uduk=z'1000021'
      I_oduk=z'1000021'
      I_iduk=z'1000021'
      I_eduk=z'1000021'
      I_aduk=z'1000021'
      I_ubuk=z'1000021'
      I_ibuk=z'1000021'
      I_ebuk=z'1000021'
      I_abuk=z'1000021'
      I_uxok=z'1000021'
      I_oxok=z'1000021'
      I_ixok=z'1000021'
      I_exok=z'1000021'
      I_axok=z'1000021'
      I_uvok=z'1000021'
      I_ovok=z'1000021'
      I_evok=z'1000021'
      I_avok=z'1000021'
      I_utok=z'1000021'
      I_otok=z'1000021'
      I_itok=z'1000021'
      I_etok=z'1000021'
      I_atok=z'1000021'
      I_usok=z'1000021'
      I_osok=z'1000021'
      I_isok=z'1000021'
      I_asok=z'1000021'
      I_urok=z'1000021'
      I_orok=z'1000021'
      I_irok=z'1000021'
      I_erok=z'1000021'
      I_arok=z'1000021'
      I_upok=z'1000021'
      I_opok=z'1000021'
      I_ipok=z'1000021'
      I_epok=z'1000021'
      I_umok=z'1000021'
      I_omok=z'1000021'
      I_imok=z'1000021'
      I_emok=z'1000021'
      I_amok=z'1000021'
      I_ulok=z'1000021'
      I_olok=z'1000021'
      I_ilok=z'1000021'
      I_elok=z'1000021'
      I_alok=z'1000021'
      I_okok=z'1000021'
      I_ikok=z'1000021'
      I_ekok=z'1000021'
      I_akok=z'1000021'
      I_ufok=z'1000021'
      I_ofok=z'1000021'
      I_ifok=z'1000021'
      I_efok=z'1000021'
      I_afok=z'1000021'
      I_udok=z'1000021'
      I_idok=z'1000021'
C FDA_70boats.fgi( 126, 222):pre: ���������� ���������� ������� �� ������� FDA50,BOAT70
      I_amuk=z'1000021'
      I_ufuk=z'1000021'
      I_obuk=z'1000021'
      I_ivok=z'1000021'
      I_esok=z'1000021'
      I_apok=z'1000021'
      I_ukok=z'1000021'
      I_odok=z'1000021'
      I_edok=z'1000021'
      I_uluk=z'1000021'
      I_oluk=z'1000021'
      I_iluk=z'1000021'
      I_eluk=z'1000021'
      I_aluk=z'1000021'
      I_ukuk=z'1000021'
      I_okuk=z'1000021'
      I_ikuk=z'1000021'
      I_ekuk=z'1000021'
      I_akuk=z'1000021'
      I_ofuk=z'1000021'
      I_ifuk=z'1000021'
      I_efuk=z'1000021'
      I_afuk=z'1000021'
      I_uduk=z'1000021'
      I_oduk=z'1000021'
      I_iduk=z'1000021'
      I_eduk=z'1000021'
      I_aduk=z'1000021'
      I_ubuk=z'1000021'
      I_ibuk=z'1000021'
      I_ebuk=z'1000021'
      I_abuk=z'1000021'
      I_uxok=z'1000021'
      I_oxok=z'1000021'
      I_ixok=z'1000021'
      I_exok=z'1000021'
      I_axok=z'1000021'
      I_uvok=z'1000021'
      I_ovok=z'1000021'
      I_evok=z'1000021'
      I_avok=z'1000021'
      I_utok=z'1000021'
      I_otok=z'1000021'
      I_itok=z'1000021'
      I_etok=z'1000021'
      I_atok=z'1000021'
      I_usok=z'1000021'
      I_osok=z'1000021'
      I_isok=z'1000021'
      I_asok=z'1000021'
      I_urok=z'1000021'
      I_orok=z'1000021'
      I_irok=z'1000021'
      I_erok=z'1000021'
      I_arok=z'1000021'
      I_upok=z'1000021'
      I_opok=z'1000021'
      I_ipok=z'1000021'
      I_epok=z'1000021'
      I_umok=z'1000021'
      I_omok=z'1000021'
      I_imok=z'1000021'
      I_emok=z'1000021'
      I_amok=z'1000021'
      I_ulok=z'1000021'
      I_olok=z'1000021'
      I_ilok=z'1000021'
      I_elok=z'1000021'
      I_alok=z'1000021'
      I_okok=z'1000021'
      I_ikok=z'1000021'
      I_ekok=z'1000021'
      I_akok=z'1000021'
      I_ufok=z'1000021'
      I_ofok=z'1000021'
      I_ifok=z'1000021'
      I_efok=z'1000021'
      I_afok=z'1000021'
      I_udok=z'1000021'
      I_idok=z'1000021'
C FDA_70boats.fgi( 146, 222):pre: ���������� ���������� ������� �� ������� FDA50,BOAT69
      I_amuk=z'1000021'
      I_ufuk=z'1000021'
      I_obuk=z'1000021'
      I_ivok=z'1000021'
      I_esok=z'1000021'
      I_apok=z'1000021'
      I_ukok=z'1000021'
      I_odok=z'1000021'
      I_edok=z'1000021'
      I_uluk=z'1000021'
      I_oluk=z'1000021'
      I_iluk=z'1000021'
      I_eluk=z'1000021'
      I_aluk=z'1000021'
      I_ukuk=z'1000021'
      I_okuk=z'1000021'
      I_ikuk=z'1000021'
      I_ekuk=z'1000021'
      I_akuk=z'1000021'
      I_ofuk=z'1000021'
      I_ifuk=z'1000021'
      I_efuk=z'1000021'
      I_afuk=z'1000021'
      I_uduk=z'1000021'
      I_oduk=z'1000021'
      I_iduk=z'1000021'
      I_eduk=z'1000021'
      I_aduk=z'1000021'
      I_ubuk=z'1000021'
      I_ibuk=z'1000021'
      I_ebuk=z'1000021'
      I_abuk=z'1000021'
      I_uxok=z'1000021'
      I_oxok=z'1000021'
      I_ixok=z'1000021'
      I_exok=z'1000021'
      I_axok=z'1000021'
      I_uvok=z'1000021'
      I_ovok=z'1000021'
      I_evok=z'1000021'
      I_avok=z'1000021'
      I_utok=z'1000021'
      I_otok=z'1000021'
      I_itok=z'1000021'
      I_etok=z'1000021'
      I_atok=z'1000021'
      I_usok=z'1000021'
      I_osok=z'1000021'
      I_isok=z'1000021'
      I_asok=z'1000021'
      I_urok=z'1000021'
      I_orok=z'1000021'
      I_irok=z'1000021'
      I_erok=z'1000021'
      I_arok=z'1000021'
      I_upok=z'1000021'
      I_opok=z'1000021'
      I_ipok=z'1000021'
      I_epok=z'1000021'
      I_umok=z'1000021'
      I_omok=z'1000021'
      I_imok=z'1000021'
      I_emok=z'1000021'
      I_amok=z'1000021'
      I_ulok=z'1000021'
      I_olok=z'1000021'
      I_ilok=z'1000021'
      I_elok=z'1000021'
      I_alok=z'1000021'
      I_okok=z'1000021'
      I_ikok=z'1000021'
      I_ekok=z'1000021'
      I_akok=z'1000021'
      I_ufok=z'1000021'
      I_ofok=z'1000021'
      I_ifok=z'1000021'
      I_efok=z'1000021'
      I_afok=z'1000021'
      I_udok=z'1000021'
      I_idok=z'1000021'
C FDA_70boats.fgi( 166, 222):pre: ���������� ���������� ������� �� ������� FDA50,BOAT68
      I_amuk=z'1000021'
      I_ufuk=z'1000021'
      I_obuk=z'1000021'
      I_ivok=z'1000021'
      I_esok=z'1000021'
      I_apok=z'1000021'
      I_ukok=z'1000021'
      I_odok=z'1000021'
      I_edok=z'1000021'
      I_uluk=z'1000021'
      I_oluk=z'1000021'
      I_iluk=z'1000021'
      I_eluk=z'1000021'
      I_aluk=z'1000021'
      I_ukuk=z'1000021'
      I_okuk=z'1000021'
      I_ikuk=z'1000021'
      I_ekuk=z'1000021'
      I_akuk=z'1000021'
      I_ofuk=z'1000021'
      I_ifuk=z'1000021'
      I_efuk=z'1000021'
      I_afuk=z'1000021'
      I_uduk=z'1000021'
      I_oduk=z'1000021'
      I_iduk=z'1000021'
      I_eduk=z'1000021'
      I_aduk=z'1000021'
      I_ubuk=z'1000021'
      I_ibuk=z'1000021'
      I_ebuk=z'1000021'
      I_abuk=z'1000021'
      I_uxok=z'1000021'
      I_oxok=z'1000021'
      I_ixok=z'1000021'
      I_exok=z'1000021'
      I_axok=z'1000021'
      I_uvok=z'1000021'
      I_ovok=z'1000021'
      I_evok=z'1000021'
      I_avok=z'1000021'
      I_utok=z'1000021'
      I_otok=z'1000021'
      I_itok=z'1000021'
      I_etok=z'1000021'
      I_atok=z'1000021'
      I_usok=z'1000021'
      I_osok=z'1000021'
      I_isok=z'1000021'
      I_asok=z'1000021'
      I_urok=z'1000021'
      I_orok=z'1000021'
      I_irok=z'1000021'
      I_erok=z'1000021'
      I_arok=z'1000021'
      I_upok=z'1000021'
      I_opok=z'1000021'
      I_ipok=z'1000021'
      I_epok=z'1000021'
      I_umok=z'1000021'
      I_omok=z'1000021'
      I_imok=z'1000021'
      I_emok=z'1000021'
      I_amok=z'1000021'
      I_ulok=z'1000021'
      I_olok=z'1000021'
      I_ilok=z'1000021'
      I_elok=z'1000021'
      I_alok=z'1000021'
      I_okok=z'1000021'
      I_ikok=z'1000021'
      I_ekok=z'1000021'
      I_akok=z'1000021'
      I_ufok=z'1000021'
      I_ofok=z'1000021'
      I_ifok=z'1000021'
      I_efok=z'1000021'
      I_afok=z'1000021'
      I_udok=z'1000021'
      I_idok=z'1000021'
C FDA_70boats.fgi( 186, 222):pre: ���������� ���������� ������� �� ������� FDA50,BOAT67
      I_amuk=z'1000021'
      I_ufuk=z'1000021'
      I_obuk=z'1000021'
      I_ivok=z'1000021'
      I_esok=z'1000021'
      I_apok=z'1000021'
      I_ukok=z'1000021'
      I_odok=z'1000021'
      I_edok=z'1000021'
      I_uluk=z'1000021'
      I_oluk=z'1000021'
      I_iluk=z'1000021'
      I_eluk=z'1000021'
      I_aluk=z'1000021'
      I_ukuk=z'1000021'
      I_okuk=z'1000021'
      I_ikuk=z'1000021'
      I_ekuk=z'1000021'
      I_akuk=z'1000021'
      I_ofuk=z'1000021'
      I_ifuk=z'1000021'
      I_efuk=z'1000021'
      I_afuk=z'1000021'
      I_uduk=z'1000021'
      I_oduk=z'1000021'
      I_iduk=z'1000021'
      I_eduk=z'1000021'
      I_aduk=z'1000021'
      I_ubuk=z'1000021'
      I_ibuk=z'1000021'
      I_ebuk=z'1000021'
      I_abuk=z'1000021'
      I_uxok=z'1000021'
      I_oxok=z'1000021'
      I_ixok=z'1000021'
      I_exok=z'1000021'
      I_axok=z'1000021'
      I_uvok=z'1000021'
      I_ovok=z'1000021'
      I_evok=z'1000021'
      I_avok=z'1000021'
      I_utok=z'1000021'
      I_otok=z'1000021'
      I_itok=z'1000021'
      I_etok=z'1000021'
      I_atok=z'1000021'
      I_usok=z'1000021'
      I_osok=z'1000021'
      I_isok=z'1000021'
      I_asok=z'1000021'
      I_urok=z'1000021'
      I_orok=z'1000021'
      I_irok=z'1000021'
      I_erok=z'1000021'
      I_arok=z'1000021'
      I_upok=z'1000021'
      I_opok=z'1000021'
      I_ipok=z'1000021'
      I_epok=z'1000021'
      I_umok=z'1000021'
      I_omok=z'1000021'
      I_imok=z'1000021'
      I_emok=z'1000021'
      I_amok=z'1000021'
      I_ulok=z'1000021'
      I_olok=z'1000021'
      I_ilok=z'1000021'
      I_elok=z'1000021'
      I_alok=z'1000021'
      I_okok=z'1000021'
      I_ikok=z'1000021'
      I_ekok=z'1000021'
      I_akok=z'1000021'
      I_ufok=z'1000021'
      I_ofok=z'1000021'
      I_ifok=z'1000021'
      I_efok=z'1000021'
      I_afok=z'1000021'
      I_udok=z'1000021'
      I_idok=z'1000021'
C FDA_70boats.fgi( 206, 222):pre: ���������� ���������� ������� �� ������� FDA50,BOAT66
      I_amuk=z'1000021'
      I_ufuk=z'1000021'
      I_obuk=z'1000021'
      I_ivok=z'1000021'
      I_esok=z'1000021'
      I_apok=z'1000021'
      I_ukok=z'1000021'
      I_odok=z'1000021'
      I_edok=z'1000021'
      I_uluk=z'1000021'
      I_oluk=z'1000021'
      I_iluk=z'1000021'
      I_eluk=z'1000021'
      I_aluk=z'1000021'
      I_ukuk=z'1000021'
      I_okuk=z'1000021'
      I_ikuk=z'1000021'
      I_ekuk=z'1000021'
      I_akuk=z'1000021'
      I_ofuk=z'1000021'
      I_ifuk=z'1000021'
      I_efuk=z'1000021'
      I_afuk=z'1000021'
      I_uduk=z'1000021'
      I_oduk=z'1000021'
      I_iduk=z'1000021'
      I_eduk=z'1000021'
      I_aduk=z'1000021'
      I_ubuk=z'1000021'
      I_ibuk=z'1000021'
      I_ebuk=z'1000021'
      I_abuk=z'1000021'
      I_uxok=z'1000021'
      I_oxok=z'1000021'
      I_ixok=z'1000021'
      I_exok=z'1000021'
      I_axok=z'1000021'
      I_uvok=z'1000021'
      I_ovok=z'1000021'
      I_evok=z'1000021'
      I_avok=z'1000021'
      I_utok=z'1000021'
      I_otok=z'1000021'
      I_itok=z'1000021'
      I_etok=z'1000021'
      I_atok=z'1000021'
      I_usok=z'1000021'
      I_osok=z'1000021'
      I_isok=z'1000021'
      I_asok=z'1000021'
      I_urok=z'1000021'
      I_orok=z'1000021'
      I_irok=z'1000021'
      I_erok=z'1000021'
      I_arok=z'1000021'
      I_upok=z'1000021'
      I_opok=z'1000021'
      I_ipok=z'1000021'
      I_epok=z'1000021'
      I_umok=z'1000021'
      I_omok=z'1000021'
      I_imok=z'1000021'
      I_emok=z'1000021'
      I_amok=z'1000021'
      I_ulok=z'1000021'
      I_olok=z'1000021'
      I_ilok=z'1000021'
      I_elok=z'1000021'
      I_alok=z'1000021'
      I_okok=z'1000021'
      I_ikok=z'1000021'
      I_ekok=z'1000021'
      I_akok=z'1000021'
      I_ufok=z'1000021'
      I_ofok=z'1000021'
      I_ifok=z'1000021'
      I_efok=z'1000021'
      I_afok=z'1000021'
      I_udok=z'1000021'
      I_idok=z'1000021'
C FDA_70boats.fgi( 226, 222):pre: ���������� ���������� ������� �� ������� FDA50,BOAT65
      I_amuk=z'1000021'
      I_ufuk=z'1000021'
      I_obuk=z'1000021'
      I_ivok=z'1000021'
      I_esok=z'1000021'
      I_apok=z'1000021'
      I_ukok=z'1000021'
      I_odok=z'1000021'
      I_edok=z'1000021'
      I_uluk=z'1000021'
      I_oluk=z'1000021'
      I_iluk=z'1000021'
      I_eluk=z'1000021'
      I_aluk=z'1000021'
      I_ukuk=z'1000021'
      I_okuk=z'1000021'
      I_ikuk=z'1000021'
      I_ekuk=z'1000021'
      I_akuk=z'1000021'
      I_ofuk=z'1000021'
      I_ifuk=z'1000021'
      I_efuk=z'1000021'
      I_afuk=z'1000021'
      I_uduk=z'1000021'
      I_oduk=z'1000021'
      I_iduk=z'1000021'
      I_eduk=z'1000021'
      I_aduk=z'1000021'
      I_ubuk=z'1000021'
      I_ibuk=z'1000021'
      I_ebuk=z'1000021'
      I_abuk=z'1000021'
      I_uxok=z'1000021'
      I_oxok=z'1000021'
      I_ixok=z'1000021'
      I_exok=z'1000021'
      I_axok=z'1000021'
      I_uvok=z'1000021'
      I_ovok=z'1000021'
      I_evok=z'1000021'
      I_avok=z'1000021'
      I_utok=z'1000021'
      I_otok=z'1000021'
      I_itok=z'1000021'
      I_etok=z'1000021'
      I_atok=z'1000021'
      I_usok=z'1000021'
      I_osok=z'1000021'
      I_isok=z'1000021'
      I_asok=z'1000021'
      I_urok=z'1000021'
      I_orok=z'1000021'
      I_irok=z'1000021'
      I_erok=z'1000021'
      I_arok=z'1000021'
      I_upok=z'1000021'
      I_opok=z'1000021'
      I_ipok=z'1000021'
      I_epok=z'1000021'
      I_umok=z'1000021'
      I_omok=z'1000021'
      I_imok=z'1000021'
      I_emok=z'1000021'
      I_amok=z'1000021'
      I_ulok=z'1000021'
      I_olok=z'1000021'
      I_ilok=z'1000021'
      I_elok=z'1000021'
      I_alok=z'1000021'
      I_okok=z'1000021'
      I_ikok=z'1000021'
      I_ekok=z'1000021'
      I_akok=z'1000021'
      I_ufok=z'1000021'
      I_ofok=z'1000021'
      I_ifok=z'1000021'
      I_efok=z'1000021'
      I_afok=z'1000021'
      I_udok=z'1000021'
      I_idok=z'1000021'
C FDA_70boats.fgi( 246, 222):pre: ���������� ���������� ������� �� ������� FDA50,BOAT64
      I_amuk=z'1000021'
      I_ufuk=z'1000021'
      I_obuk=z'1000021'
      I_ivok=z'1000021'
      I_esok=z'1000021'
      I_apok=z'1000021'
      I_ukok=z'1000021'
      I_odok=z'1000021'
      I_edok=z'1000021'
      I_uluk=z'1000021'
      I_oluk=z'1000021'
      I_iluk=z'1000021'
      I_eluk=z'1000021'
      I_aluk=z'1000021'
      I_ukuk=z'1000021'
      I_okuk=z'1000021'
      I_ikuk=z'1000021'
      I_ekuk=z'1000021'
      I_akuk=z'1000021'
      I_ofuk=z'1000021'
      I_ifuk=z'1000021'
      I_efuk=z'1000021'
      I_afuk=z'1000021'
      I_uduk=z'1000021'
      I_oduk=z'1000021'
      I_iduk=z'1000021'
      I_eduk=z'1000021'
      I_aduk=z'1000021'
      I_ubuk=z'1000021'
      I_ibuk=z'1000021'
      I_ebuk=z'1000021'
      I_abuk=z'1000021'
      I_uxok=z'1000021'
      I_oxok=z'1000021'
      I_ixok=z'1000021'
      I_exok=z'1000021'
      I_axok=z'1000021'
      I_uvok=z'1000021'
      I_ovok=z'1000021'
      I_evok=z'1000021'
      I_avok=z'1000021'
      I_utok=z'1000021'
      I_otok=z'1000021'
      I_itok=z'1000021'
      I_etok=z'1000021'
      I_atok=z'1000021'
      I_usok=z'1000021'
      I_osok=z'1000021'
      I_isok=z'1000021'
      I_asok=z'1000021'
      I_urok=z'1000021'
      I_orok=z'1000021'
      I_irok=z'1000021'
      I_erok=z'1000021'
      I_arok=z'1000021'
      I_upok=z'1000021'
      I_opok=z'1000021'
      I_ipok=z'1000021'
      I_epok=z'1000021'
      I_umok=z'1000021'
      I_omok=z'1000021'
      I_imok=z'1000021'
      I_emok=z'1000021'
      I_amok=z'1000021'
      I_ulok=z'1000021'
      I_olok=z'1000021'
      I_ilok=z'1000021'
      I_elok=z'1000021'
      I_alok=z'1000021'
      I_okok=z'1000021'
      I_ikok=z'1000021'
      I_ekok=z'1000021'
      I_akok=z'1000021'
      I_ufok=z'1000021'
      I_ofok=z'1000021'
      I_ifok=z'1000021'
      I_efok=z'1000021'
      I_afok=z'1000021'
      I_udok=z'1000021'
      I_idok=z'1000021'
C FDA_70boats.fgi( 266, 222):pre: ���������� ���������� ������� �� ������� FDA50,BOAT63
      I_amuk=z'1000021'
      I_ufuk=z'1000021'
      I_obuk=z'1000021'
      I_ivok=z'1000021'
      I_esok=z'1000021'
      I_apok=z'1000021'
      I_ukok=z'1000021'
      I_odok=z'1000021'
      I_edok=z'1000021'
      I_uluk=z'1000021'
      I_oluk=z'1000021'
      I_iluk=z'1000021'
      I_eluk=z'1000021'
      I_aluk=z'1000021'
      I_ukuk=z'1000021'
      I_okuk=z'1000021'
      I_ikuk=z'1000021'
      I_ekuk=z'1000021'
      I_akuk=z'1000021'
      I_ofuk=z'1000021'
      I_ifuk=z'1000021'
      I_efuk=z'1000021'
      I_afuk=z'1000021'
      I_uduk=z'1000021'
      I_oduk=z'1000021'
      I_iduk=z'1000021'
      I_eduk=z'1000021'
      I_aduk=z'1000021'
      I_ubuk=z'1000021'
      I_ibuk=z'1000021'
      I_ebuk=z'1000021'
      I_abuk=z'1000021'
      I_uxok=z'1000021'
      I_oxok=z'1000021'
      I_ixok=z'1000021'
      I_exok=z'1000021'
      I_axok=z'1000021'
      I_uvok=z'1000021'
      I_ovok=z'1000021'
      I_evok=z'1000021'
      I_avok=z'1000021'
      I_utok=z'1000021'
      I_otok=z'1000021'
      I_itok=z'1000021'
      I_etok=z'1000021'
      I_atok=z'1000021'
      I_usok=z'1000021'
      I_osok=z'1000021'
      I_isok=z'1000021'
      I_asok=z'1000021'
      I_urok=z'1000021'
      I_orok=z'1000021'
      I_irok=z'1000021'
      I_erok=z'1000021'
      I_arok=z'1000021'
      I_upok=z'1000021'
      I_opok=z'1000021'
      I_ipok=z'1000021'
      I_epok=z'1000021'
      I_umok=z'1000021'
      I_omok=z'1000021'
      I_imok=z'1000021'
      I_emok=z'1000021'
      I_amok=z'1000021'
      I_ulok=z'1000021'
      I_olok=z'1000021'
      I_ilok=z'1000021'
      I_elok=z'1000021'
      I_alok=z'1000021'
      I_okok=z'1000021'
      I_ikok=z'1000021'
      I_ekok=z'1000021'
      I_akok=z'1000021'
      I_ufok=z'1000021'
      I_ofok=z'1000021'
      I_ifok=z'1000021'
      I_efok=z'1000021'
      I_afok=z'1000021'
      I_udok=z'1000021'
      I_idok=z'1000021'
C FDA_70boats.fgi( 286, 222):pre: ���������� ���������� ������� �� ������� FDA50,BOAT62
      I_amuk=z'1000021'
      I_ufuk=z'1000021'
      I_obuk=z'1000021'
      I_ivok=z'1000021'
      I_esok=z'1000021'
      I_apok=z'1000021'
      I_ukok=z'1000021'
      I_odok=z'1000021'
      I_edok=z'1000021'
      I_uluk=z'1000021'
      I_oluk=z'1000021'
      I_iluk=z'1000021'
      I_eluk=z'1000021'
      I_aluk=z'1000021'
      I_ukuk=z'1000021'
      I_okuk=z'1000021'
      I_ikuk=z'1000021'
      I_ekuk=z'1000021'
      I_akuk=z'1000021'
      I_ofuk=z'1000021'
      I_ifuk=z'1000021'
      I_efuk=z'1000021'
      I_afuk=z'1000021'
      I_uduk=z'1000021'
      I_oduk=z'1000021'
      I_iduk=z'1000021'
      I_eduk=z'1000021'
      I_aduk=z'1000021'
      I_ubuk=z'1000021'
      I_ibuk=z'1000021'
      I_ebuk=z'1000021'
      I_abuk=z'1000021'
      I_uxok=z'1000021'
      I_oxok=z'1000021'
      I_ixok=z'1000021'
      I_exok=z'1000021'
      I_axok=z'1000021'
      I_uvok=z'1000021'
      I_ovok=z'1000021'
      I_evok=z'1000021'
      I_avok=z'1000021'
      I_utok=z'1000021'
      I_otok=z'1000021'
      I_itok=z'1000021'
      I_etok=z'1000021'
      I_atok=z'1000021'
      I_usok=z'1000021'
      I_osok=z'1000021'
      I_isok=z'1000021'
      I_asok=z'1000021'
      I_urok=z'1000021'
      I_orok=z'1000021'
      I_irok=z'1000021'
      I_erok=z'1000021'
      I_arok=z'1000021'
      I_upok=z'1000021'
      I_opok=z'1000021'
      I_ipok=z'1000021'
      I_epok=z'1000021'
      I_umok=z'1000021'
      I_omok=z'1000021'
      I_imok=z'1000021'
      I_emok=z'1000021'
      I_amok=z'1000021'
      I_ulok=z'1000021'
      I_olok=z'1000021'
      I_ilok=z'1000021'
      I_elok=z'1000021'
      I_alok=z'1000021'
      I_okok=z'1000021'
      I_ikok=z'1000021'
      I_ekok=z'1000021'
      I_akok=z'1000021'
      I_ufok=z'1000021'
      I_ofok=z'1000021'
      I_ifok=z'1000021'
      I_efok=z'1000021'
      I_afok=z'1000021'
      I_udok=z'1000021'
      I_idok=z'1000021'
C FDA_70boats.fgi( 306, 222):pre: ���������� ���������� ������� �� ������� FDA50,BOAT61
      Call LODOCHKA_HANDLER_KTS(deltat,L_et,L_it,
     & L_ot,L_ut,L_av,
     & L_ev,L_iv,L_ov,L_uv,
     & L_ax,L_epe,L_ipe,
     & L_ope,L_upe,L_are,
     & L_ere,L_ire,L_ore,L_ure,
     & L_ase,L_eki,L_iki,
     & L_oki,L_uki,L_ali,
     & L_eli,L_ili,L_oli,L_uli,
     & L_ami,L_ebo,L_ibo,
     & L_obo,L_ubo,L_ado,
     & L_edo,L_ido,L_odo,L_udo,
     & L_afo,L_eto,L_ito,
     & L_oto,L_uto,L_avo,
     & L_evo,L_ivo,L_ovo,L_uvo,
     & L_axo,L_ep,L_ip,
     & L_op,L_up,L_ar,
     & L_er,L_ir,L_or,
     & L_ur,L_as,L_es,
     & L_is,L_os,L_us,
     & L_at,L_eke,L_ike,
     & L_oke,L_uke,L_ale,
     & L_ele,L_ile,L_ole,
     & L_ule,L_ame,L_eme,
     & L_ime,L_ome,L_ume,
     & L_ape,L_ebi,L_ibi,
     & L_obi,L_ubi,L_adi,
     & L_edi,L_idi,L_odi,
     & L_udi,L_afi,L_efi,
     & L_ifi,L_ofi,L_ufi,
     & L_aki,L_epo,L_ipo,
     & L_opo,L_upo,L_aro,
     & L_ero,L_iro,L_oro,
     & L_uro,L_aso,L_eso,
     & L_iso,L_oso,L_uso,
     & L_ato,L_eti,L_iti,
     & L_oti,L_uti,L_avi,
     & L_evi,L_ivi,L_ovi,
     & L_uvi,L_axi,L_exi,
     & L_ixi,L_oxi,L_uxi,
     & L_abo,L_epu,L_ipu,
     & L_opu,L_upu,L_aru,
     & L_eru,L_iru,L_oru,L_uru,
     & L_asu,L_eku,L_iku,
     & L_oku,L_uku,L_alu,
     & L_elu,L_ilu,L_olu,
     & L_ulu,L_amu,L_emu,
     & L_imu,L_omu,L_umu,
     & L_apu,L_ek,L_ik,
     & L_ok,L_al,L_el,L_em,
     & L_im,L_om,L_um,L_ap,
     & L_ef,L_if,L_of,L_uf,
     & L_ak,L_uk,L_il,L_ol,
     & L_ul,L_am,L_ebe,L_ibe,
     & L_obe,L_ade,L_ede,L_efe,
     & L_ife,L_ofe,L_ufe,L_ake,
     & L_ex,L_ix,L_ox,L_ux,
     & L_abe,L_ube,L_ide,L_ode,
     & L_ude,L_afe,L_ete,L_ite,
     & L_ote,L_ave,L_eve,L_exe,
     & L_ixe,L_oxe,L_uxe,L_abi,
     & L_ese,L_ise,L_ose,L_use,
     & L_ate,L_ute,L_ive,L_ove,
     & L_uve,L_axe,L_epi,L_ipi,
     & L_opi,L_ari,L_eri,L_esi,
     & L_isi,L_osi,L_usi,L_ati,
     & L_emi,L_imi,L_omi,L_umi,
     & L_api,L_upi,L_iri,L_ori,
     & L_uri,L_asi,L_eko,L_iko,
     & L_oko,L_alo,L_elo,L_emo,
     & L_imo,L_omo,L_umo,L_apo,
     & L_efo,L_ifo,L_ofo,L_ufo,
     & L_ako,L_uko,L_ilo,L_olo,
     & L_ulo,L_amo,L_ebu,L_ibu,
     & L_obu,L_adu,L_edu,L_efu,
     & L_ifu,L_ofu,L_ufu,L_aku,
     & L_exo,L_ixo,L_oxo,L_uxo,
     & L_abu,L_ubu,L_idu,L_odu,
     & L_udu,L_afu,I_(1))
C FDA_70boats.fgi( 306, 208):���������� ������� � ���� ���,BOAT61
      Call LODOCHKA_HANDLER_KTS(deltat,L_et,L_it,
     & L_ot,L_ut,L_av,
     & L_ev,L_iv,L_ov,L_uv,
     & L_ax,L_epe,L_ipe,
     & L_ope,L_upe,L_are,
     & L_ere,L_ire,L_ore,L_ure,
     & L_ase,L_eki,L_iki,
     & L_oki,L_uki,L_ali,
     & L_eli,L_ili,L_oli,L_uli,
     & L_ami,L_ebo,L_ibo,
     & L_obo,L_ubo,L_ado,
     & L_edo,L_ido,L_odo,L_udo,
     & L_afo,L_eto,L_ito,
     & L_oto,L_uto,L_avo,
     & L_evo,L_ivo,L_ovo,L_uvo,
     & L_axo,L_ep,L_ip,
     & L_op,L_up,L_ar,
     & L_er,L_ir,L_or,
     & L_ur,L_as,L_es,
     & L_is,L_os,L_us,
     & L_at,L_eke,L_ike,
     & L_oke,L_uke,L_ale,
     & L_ele,L_ile,L_ole,
     & L_ule,L_ame,L_eme,
     & L_ime,L_ome,L_ume,
     & L_ape,L_ebi,L_ibi,
     & L_obi,L_ubi,L_adi,
     & L_edi,L_idi,L_odi,
     & L_udi,L_afi,L_efi,
     & L_ifi,L_ofi,L_ufi,
     & L_aki,L_epo,L_ipo,
     & L_opo,L_upo,L_aro,
     & L_ero,L_iro,L_oro,
     & L_uro,L_aso,L_eso,
     & L_iso,L_oso,L_uso,
     & L_ato,L_eti,L_iti,
     & L_oti,L_uti,L_avi,
     & L_evi,L_ivi,L_ovi,
     & L_uvi,L_axi,L_exi,
     & L_ixi,L_oxi,L_uxi,
     & L_abo,L_epu,L_ipu,
     & L_opu,L_upu,L_aru,
     & L_eru,L_iru,L_oru,L_uru,
     & L_asu,L_eku,L_iku,
     & L_oku,L_uku,L_alu,
     & L_elu,L_ilu,L_olu,
     & L_ulu,L_amu,L_emu,
     & L_imu,L_omu,L_umu,
     & L_apu,L_ek,L_ik,
     & L_ok,L_al,L_el,L_em,
     & L_im,L_om,L_um,L_ap,
     & L_ef,L_if,L_of,L_uf,
     & L_ak,L_uk,L_il,L_ol,
     & L_ul,L_am,L_ebe,L_ibe,
     & L_obe,L_ade,L_ede,L_efe,
     & L_ife,L_ofe,L_ufe,L_ake,
     & L_ex,L_ix,L_ox,L_ux,
     & L_abe,L_ube,L_ide,L_ode,
     & L_ude,L_afe,L_ete,L_ite,
     & L_ote,L_ave,L_eve,L_exe,
     & L_ixe,L_oxe,L_uxe,L_abi,
     & L_ese,L_ise,L_ose,L_use,
     & L_ate,L_ute,L_ive,L_ove,
     & L_uve,L_axe,L_epi,L_ipi,
     & L_opi,L_ari,L_eri,L_esi,
     & L_isi,L_osi,L_usi,L_ati,
     & L_emi,L_imi,L_omi,L_umi,
     & L_api,L_upi,L_iri,L_ori,
     & L_uri,L_asi,L_eko,L_iko,
     & L_oko,L_alo,L_elo,L_emo,
     & L_imo,L_omo,L_umo,L_apo,
     & L_efo,L_ifo,L_ofo,L_ufo,
     & L_ako,L_uko,L_ilo,L_olo,
     & L_ulo,L_amo,L_ebu,L_ibu,
     & L_obu,L_adu,L_edu,L_efu,
     & L_ifu,L_ofu,L_ufu,L_aku,
     & L_exo,L_ixo,L_oxo,L_uxo,
     & L_abu,L_ubu,L_idu,L_odu,
     & L_udu,L_afu,I_(2))
C FDA_70boats.fgi( 286, 208):���������� ������� � ���� ���,BOAT62
      Call LODOCHKA_HANDLER_KTS(deltat,L_et,L_it,
     & L_ot,L_ut,L_av,
     & L_ev,L_iv,L_ov,L_uv,
     & L_ax,L_epe,L_ipe,
     & L_ope,L_upe,L_are,
     & L_ere,L_ire,L_ore,L_ure,
     & L_ase,L_eki,L_iki,
     & L_oki,L_uki,L_ali,
     & L_eli,L_ili,L_oli,L_uli,
     & L_ami,L_ebo,L_ibo,
     & L_obo,L_ubo,L_ado,
     & L_edo,L_ido,L_odo,L_udo,
     & L_afo,L_eto,L_ito,
     & L_oto,L_uto,L_avo,
     & L_evo,L_ivo,L_ovo,L_uvo,
     & L_axo,L_ep,L_ip,
     & L_op,L_up,L_ar,
     & L_er,L_ir,L_or,
     & L_ur,L_as,L_es,
     & L_is,L_os,L_us,
     & L_at,L_eke,L_ike,
     & L_oke,L_uke,L_ale,
     & L_ele,L_ile,L_ole,
     & L_ule,L_ame,L_eme,
     & L_ime,L_ome,L_ume,
     & L_ape,L_ebi,L_ibi,
     & L_obi,L_ubi,L_adi,
     & L_edi,L_idi,L_odi,
     & L_udi,L_afi,L_efi,
     & L_ifi,L_ofi,L_ufi,
     & L_aki,L_epo,L_ipo,
     & L_opo,L_upo,L_aro,
     & L_ero,L_iro,L_oro,
     & L_uro,L_aso,L_eso,
     & L_iso,L_oso,L_uso,
     & L_ato,L_eti,L_iti,
     & L_oti,L_uti,L_avi,
     & L_evi,L_ivi,L_ovi,
     & L_uvi,L_axi,L_exi,
     & L_ixi,L_oxi,L_uxi,
     & L_abo,L_epu,L_ipu,
     & L_opu,L_upu,L_aru,
     & L_eru,L_iru,L_oru,L_uru,
     & L_asu,L_eku,L_iku,
     & L_oku,L_uku,L_alu,
     & L_elu,L_ilu,L_olu,
     & L_ulu,L_amu,L_emu,
     & L_imu,L_omu,L_umu,
     & L_apu,L_ek,L_ik,
     & L_ok,L_al,L_el,L_em,
     & L_im,L_om,L_um,L_ap,
     & L_ef,L_if,L_of,L_uf,
     & L_ak,L_uk,L_il,L_ol,
     & L_ul,L_am,L_ebe,L_ibe,
     & L_obe,L_ade,L_ede,L_efe,
     & L_ife,L_ofe,L_ufe,L_ake,
     & L_ex,L_ix,L_ox,L_ux,
     & L_abe,L_ube,L_ide,L_ode,
     & L_ude,L_afe,L_ete,L_ite,
     & L_ote,L_ave,L_eve,L_exe,
     & L_ixe,L_oxe,L_uxe,L_abi,
     & L_ese,L_ise,L_ose,L_use,
     & L_ate,L_ute,L_ive,L_ove,
     & L_uve,L_axe,L_epi,L_ipi,
     & L_opi,L_ari,L_eri,L_esi,
     & L_isi,L_osi,L_usi,L_ati,
     & L_emi,L_imi,L_omi,L_umi,
     & L_api,L_upi,L_iri,L_ori,
     & L_uri,L_asi,L_eko,L_iko,
     & L_oko,L_alo,L_elo,L_emo,
     & L_imo,L_omo,L_umo,L_apo,
     & L_efo,L_ifo,L_ofo,L_ufo,
     & L_ako,L_uko,L_ilo,L_olo,
     & L_ulo,L_amo,L_ebu,L_ibu,
     & L_obu,L_adu,L_edu,L_efu,
     & L_ifu,L_ofu,L_ufu,L_aku,
     & L_exo,L_ixo,L_oxo,L_uxo,
     & L_abu,L_ubu,L_idu,L_odu,
     & L_udu,L_afu,I_(3))
C FDA_70boats.fgi( 266, 208):���������� ������� � ���� ���,BOAT63
      Call LODOCHKA_HANDLER_KTS(deltat,L_et,L_it,
     & L_ot,L_ut,L_av,
     & L_ev,L_iv,L_ov,L_uv,
     & L_ax,L_epe,L_ipe,
     & L_ope,L_upe,L_are,
     & L_ere,L_ire,L_ore,L_ure,
     & L_ase,L_eki,L_iki,
     & L_oki,L_uki,L_ali,
     & L_eli,L_ili,L_oli,L_uli,
     & L_ami,L_ebo,L_ibo,
     & L_obo,L_ubo,L_ado,
     & L_edo,L_ido,L_odo,L_udo,
     & L_afo,L_eto,L_ito,
     & L_oto,L_uto,L_avo,
     & L_evo,L_ivo,L_ovo,L_uvo,
     & L_axo,L_ep,L_ip,
     & L_op,L_up,L_ar,
     & L_er,L_ir,L_or,
     & L_ur,L_as,L_es,
     & L_is,L_os,L_us,
     & L_at,L_eke,L_ike,
     & L_oke,L_uke,L_ale,
     & L_ele,L_ile,L_ole,
     & L_ule,L_ame,L_eme,
     & L_ime,L_ome,L_ume,
     & L_ape,L_ebi,L_ibi,
     & L_obi,L_ubi,L_adi,
     & L_edi,L_idi,L_odi,
     & L_udi,L_afi,L_efi,
     & L_ifi,L_ofi,L_ufi,
     & L_aki,L_epo,L_ipo,
     & L_opo,L_upo,L_aro,
     & L_ero,L_iro,L_oro,
     & L_uro,L_aso,L_eso,
     & L_iso,L_oso,L_uso,
     & L_ato,L_eti,L_iti,
     & L_oti,L_uti,L_avi,
     & L_evi,L_ivi,L_ovi,
     & L_uvi,L_axi,L_exi,
     & L_ixi,L_oxi,L_uxi,
     & L_abo,L_epu,L_ipu,
     & L_opu,L_upu,L_aru,
     & L_eru,L_iru,L_oru,L_uru,
     & L_asu,L_eku,L_iku,
     & L_oku,L_uku,L_alu,
     & L_elu,L_ilu,L_olu,
     & L_ulu,L_amu,L_emu,
     & L_imu,L_omu,L_umu,
     & L_apu,L_ek,L_ik,
     & L_ok,L_al,L_el,L_em,
     & L_im,L_om,L_um,L_ap,
     & L_ef,L_if,L_of,L_uf,
     & L_ak,L_uk,L_il,L_ol,
     & L_ul,L_am,L_ebe,L_ibe,
     & L_obe,L_ade,L_ede,L_efe,
     & L_ife,L_ofe,L_ufe,L_ake,
     & L_ex,L_ix,L_ox,L_ux,
     & L_abe,L_ube,L_ide,L_ode,
     & L_ude,L_afe,L_ete,L_ite,
     & L_ote,L_ave,L_eve,L_exe,
     & L_ixe,L_oxe,L_uxe,L_abi,
     & L_ese,L_ise,L_ose,L_use,
     & L_ate,L_ute,L_ive,L_ove,
     & L_uve,L_axe,L_epi,L_ipi,
     & L_opi,L_ari,L_eri,L_esi,
     & L_isi,L_osi,L_usi,L_ati,
     & L_emi,L_imi,L_omi,L_umi,
     & L_api,L_upi,L_iri,L_ori,
     & L_uri,L_asi,L_eko,L_iko,
     & L_oko,L_alo,L_elo,L_emo,
     & L_imo,L_omo,L_umo,L_apo,
     & L_efo,L_ifo,L_ofo,L_ufo,
     & L_ako,L_uko,L_ilo,L_olo,
     & L_ulo,L_amo,L_ebu,L_ibu,
     & L_obu,L_adu,L_edu,L_efu,
     & L_ifu,L_ofu,L_ufu,L_aku,
     & L_exo,L_ixo,L_oxo,L_uxo,
     & L_abu,L_ubu,L_idu,L_odu,
     & L_udu,L_afu,I_(4))
C FDA_70boats.fgi( 246, 208):���������� ������� � ���� ���,BOAT64
      Call LODOCHKA_HANDLER_KTS(deltat,L_et,L_it,
     & L_ot,L_ut,L_av,
     & L_ev,L_iv,L_ov,L_uv,
     & L_ax,L_epe,L_ipe,
     & L_ope,L_upe,L_are,
     & L_ere,L_ire,L_ore,L_ure,
     & L_ase,L_eki,L_iki,
     & L_oki,L_uki,L_ali,
     & L_eli,L_ili,L_oli,L_uli,
     & L_ami,L_ebo,L_ibo,
     & L_obo,L_ubo,L_ado,
     & L_edo,L_ido,L_odo,L_udo,
     & L_afo,L_eto,L_ito,
     & L_oto,L_uto,L_avo,
     & L_evo,L_ivo,L_ovo,L_uvo,
     & L_axo,L_ep,L_ip,
     & L_op,L_up,L_ar,
     & L_er,L_ir,L_or,
     & L_ur,L_as,L_es,
     & L_is,L_os,L_us,
     & L_at,L_eke,L_ike,
     & L_oke,L_uke,L_ale,
     & L_ele,L_ile,L_ole,
     & L_ule,L_ame,L_eme,
     & L_ime,L_ome,L_ume,
     & L_ape,L_ebi,L_ibi,
     & L_obi,L_ubi,L_adi,
     & L_edi,L_idi,L_odi,
     & L_udi,L_afi,L_efi,
     & L_ifi,L_ofi,L_ufi,
     & L_aki,L_epo,L_ipo,
     & L_opo,L_upo,L_aro,
     & L_ero,L_iro,L_oro,
     & L_uro,L_aso,L_eso,
     & L_iso,L_oso,L_uso,
     & L_ato,L_eti,L_iti,
     & L_oti,L_uti,L_avi,
     & L_evi,L_ivi,L_ovi,
     & L_uvi,L_axi,L_exi,
     & L_ixi,L_oxi,L_uxi,
     & L_abo,L_epu,L_ipu,
     & L_opu,L_upu,L_aru,
     & L_eru,L_iru,L_oru,L_uru,
     & L_asu,L_eku,L_iku,
     & L_oku,L_uku,L_alu,
     & L_elu,L_ilu,L_olu,
     & L_ulu,L_amu,L_emu,
     & L_imu,L_omu,L_umu,
     & L_apu,L_ek,L_ik,
     & L_ok,L_al,L_el,L_em,
     & L_im,L_om,L_um,L_ap,
     & L_ef,L_if,L_of,L_uf,
     & L_ak,L_uk,L_il,L_ol,
     & L_ul,L_am,L_ebe,L_ibe,
     & L_obe,L_ade,L_ede,L_efe,
     & L_ife,L_ofe,L_ufe,L_ake,
     & L_ex,L_ix,L_ox,L_ux,
     & L_abe,L_ube,L_ide,L_ode,
     & L_ude,L_afe,L_ete,L_ite,
     & L_ote,L_ave,L_eve,L_exe,
     & L_ixe,L_oxe,L_uxe,L_abi,
     & L_ese,L_ise,L_ose,L_use,
     & L_ate,L_ute,L_ive,L_ove,
     & L_uve,L_axe,L_epi,L_ipi,
     & L_opi,L_ari,L_eri,L_esi,
     & L_isi,L_osi,L_usi,L_ati,
     & L_emi,L_imi,L_omi,L_umi,
     & L_api,L_upi,L_iri,L_ori,
     & L_uri,L_asi,L_eko,L_iko,
     & L_oko,L_alo,L_elo,L_emo,
     & L_imo,L_omo,L_umo,L_apo,
     & L_efo,L_ifo,L_ofo,L_ufo,
     & L_ako,L_uko,L_ilo,L_olo,
     & L_ulo,L_amo,L_ebu,L_ibu,
     & L_obu,L_adu,L_edu,L_efu,
     & L_ifu,L_ofu,L_ufu,L_aku,
     & L_exo,L_ixo,L_oxo,L_uxo,
     & L_abu,L_ubu,L_idu,L_odu,
     & L_udu,L_afu,I_(5))
C FDA_70boats.fgi( 226, 208):���������� ������� � ���� ���,BOAT65
      Call LODOCHKA_HANDLER_KTS(deltat,L_et,L_it,
     & L_ot,L_ut,L_av,
     & L_ev,L_iv,L_ov,L_uv,
     & L_ax,L_epe,L_ipe,
     & L_ope,L_upe,L_are,
     & L_ere,L_ire,L_ore,L_ure,
     & L_ase,L_eki,L_iki,
     & L_oki,L_uki,L_ali,
     & L_eli,L_ili,L_oli,L_uli,
     & L_ami,L_ebo,L_ibo,
     & L_obo,L_ubo,L_ado,
     & L_edo,L_ido,L_odo,L_udo,
     & L_afo,L_eto,L_ito,
     & L_oto,L_uto,L_avo,
     & L_evo,L_ivo,L_ovo,L_uvo,
     & L_axo,L_ep,L_ip,
     & L_op,L_up,L_ar,
     & L_er,L_ir,L_or,
     & L_ur,L_as,L_es,
     & L_is,L_os,L_us,
     & L_at,L_eke,L_ike,
     & L_oke,L_uke,L_ale,
     & L_ele,L_ile,L_ole,
     & L_ule,L_ame,L_eme,
     & L_ime,L_ome,L_ume,
     & L_ape,L_ebi,L_ibi,
     & L_obi,L_ubi,L_adi,
     & L_edi,L_idi,L_odi,
     & L_udi,L_afi,L_efi,
     & L_ifi,L_ofi,L_ufi,
     & L_aki,L_epo,L_ipo,
     & L_opo,L_upo,L_aro,
     & L_ero,L_iro,L_oro,
     & L_uro,L_aso,L_eso,
     & L_iso,L_oso,L_uso,
     & L_ato,L_eti,L_iti,
     & L_oti,L_uti,L_avi,
     & L_evi,L_ivi,L_ovi,
     & L_uvi,L_axi,L_exi,
     & L_ixi,L_oxi,L_uxi,
     & L_abo,L_epu,L_ipu,
     & L_opu,L_upu,L_aru,
     & L_eru,L_iru,L_oru,L_uru,
     & L_asu,L_eku,L_iku,
     & L_oku,L_uku,L_alu,
     & L_elu,L_ilu,L_olu,
     & L_ulu,L_amu,L_emu,
     & L_imu,L_omu,L_umu,
     & L_apu,L_ek,L_ik,
     & L_ok,L_al,L_el,L_em,
     & L_im,L_om,L_um,L_ap,
     & L_ef,L_if,L_of,L_uf,
     & L_ak,L_uk,L_il,L_ol,
     & L_ul,L_am,L_ebe,L_ibe,
     & L_obe,L_ade,L_ede,L_efe,
     & L_ife,L_ofe,L_ufe,L_ake,
     & L_ex,L_ix,L_ox,L_ux,
     & L_abe,L_ube,L_ide,L_ode,
     & L_ude,L_afe,L_ete,L_ite,
     & L_ote,L_ave,L_eve,L_exe,
     & L_ixe,L_oxe,L_uxe,L_abi,
     & L_ese,L_ise,L_ose,L_use,
     & L_ate,L_ute,L_ive,L_ove,
     & L_uve,L_axe,L_epi,L_ipi,
     & L_opi,L_ari,L_eri,L_esi,
     & L_isi,L_osi,L_usi,L_ati,
     & L_emi,L_imi,L_omi,L_umi,
     & L_api,L_upi,L_iri,L_ori,
     & L_uri,L_asi,L_eko,L_iko,
     & L_oko,L_alo,L_elo,L_emo,
     & L_imo,L_omo,L_umo,L_apo,
     & L_efo,L_ifo,L_ofo,L_ufo,
     & L_ako,L_uko,L_ilo,L_olo,
     & L_ulo,L_amo,L_ebu,L_ibu,
     & L_obu,L_adu,L_edu,L_efu,
     & L_ifu,L_ofu,L_ufu,L_aku,
     & L_exo,L_ixo,L_oxo,L_uxo,
     & L_abu,L_ubu,L_idu,L_odu,
     & L_udu,L_afu,I_(6))
C FDA_70boats.fgi( 206, 208):���������� ������� � ���� ���,BOAT66
      Call LODOCHKA_HANDLER_KTS(deltat,L_et,L_it,
     & L_ot,L_ut,L_av,
     & L_ev,L_iv,L_ov,L_uv,
     & L_ax,L_epe,L_ipe,
     & L_ope,L_upe,L_are,
     & L_ere,L_ire,L_ore,L_ure,
     & L_ase,L_eki,L_iki,
     & L_oki,L_uki,L_ali,
     & L_eli,L_ili,L_oli,L_uli,
     & L_ami,L_ebo,L_ibo,
     & L_obo,L_ubo,L_ado,
     & L_edo,L_ido,L_odo,L_udo,
     & L_afo,L_eto,L_ito,
     & L_oto,L_uto,L_avo,
     & L_evo,L_ivo,L_ovo,L_uvo,
     & L_axo,L_ep,L_ip,
     & L_op,L_up,L_ar,
     & L_er,L_ir,L_or,
     & L_ur,L_as,L_es,
     & L_is,L_os,L_us,
     & L_at,L_eke,L_ike,
     & L_oke,L_uke,L_ale,
     & L_ele,L_ile,L_ole,
     & L_ule,L_ame,L_eme,
     & L_ime,L_ome,L_ume,
     & L_ape,L_ebi,L_ibi,
     & L_obi,L_ubi,L_adi,
     & L_edi,L_idi,L_odi,
     & L_udi,L_afi,L_efi,
     & L_ifi,L_ofi,L_ufi,
     & L_aki,L_epo,L_ipo,
     & L_opo,L_upo,L_aro,
     & L_ero,L_iro,L_oro,
     & L_uro,L_aso,L_eso,
     & L_iso,L_oso,L_uso,
     & L_ato,L_eti,L_iti,
     & L_oti,L_uti,L_avi,
     & L_evi,L_ivi,L_ovi,
     & L_uvi,L_axi,L_exi,
     & L_ixi,L_oxi,L_uxi,
     & L_abo,L_epu,L_ipu,
     & L_opu,L_upu,L_aru,
     & L_eru,L_iru,L_oru,L_uru,
     & L_asu,L_eku,L_iku,
     & L_oku,L_uku,L_alu,
     & L_elu,L_ilu,L_olu,
     & L_ulu,L_amu,L_emu,
     & L_imu,L_omu,L_umu,
     & L_apu,L_ek,L_ik,
     & L_ok,L_al,L_el,L_em,
     & L_im,L_om,L_um,L_ap,
     & L_ef,L_if,L_of,L_uf,
     & L_ak,L_uk,L_il,L_ol,
     & L_ul,L_am,L_ebe,L_ibe,
     & L_obe,L_ade,L_ede,L_efe,
     & L_ife,L_ofe,L_ufe,L_ake,
     & L_ex,L_ix,L_ox,L_ux,
     & L_abe,L_ube,L_ide,L_ode,
     & L_ude,L_afe,L_ete,L_ite,
     & L_ote,L_ave,L_eve,L_exe,
     & L_ixe,L_oxe,L_uxe,L_abi,
     & L_ese,L_ise,L_ose,L_use,
     & L_ate,L_ute,L_ive,L_ove,
     & L_uve,L_axe,L_epi,L_ipi,
     & L_opi,L_ari,L_eri,L_esi,
     & L_isi,L_osi,L_usi,L_ati,
     & L_emi,L_imi,L_omi,L_umi,
     & L_api,L_upi,L_iri,L_ori,
     & L_uri,L_asi,L_eko,L_iko,
     & L_oko,L_alo,L_elo,L_emo,
     & L_imo,L_omo,L_umo,L_apo,
     & L_efo,L_ifo,L_ofo,L_ufo,
     & L_ako,L_uko,L_ilo,L_olo,
     & L_ulo,L_amo,L_ebu,L_ibu,
     & L_obu,L_adu,L_edu,L_efu,
     & L_ifu,L_ofu,L_ufu,L_aku,
     & L_exo,L_ixo,L_oxo,L_uxo,
     & L_abu,L_ubu,L_idu,L_odu,
     & L_udu,L_afu,I_(7))
C FDA_70boats.fgi( 186, 208):���������� ������� � ���� ���,BOAT67
      Call LODOCHKA_HANDLER_KTS(deltat,L_et,L_it,
     & L_ot,L_ut,L_av,
     & L_ev,L_iv,L_ov,L_uv,
     & L_ax,L_epe,L_ipe,
     & L_ope,L_upe,L_are,
     & L_ere,L_ire,L_ore,L_ure,
     & L_ase,L_eki,L_iki,
     & L_oki,L_uki,L_ali,
     & L_eli,L_ili,L_oli,L_uli,
     & L_ami,L_ebo,L_ibo,
     & L_obo,L_ubo,L_ado,
     & L_edo,L_ido,L_odo,L_udo,
     & L_afo,L_eto,L_ito,
     & L_oto,L_uto,L_avo,
     & L_evo,L_ivo,L_ovo,L_uvo,
     & L_axo,L_ep,L_ip,
     & L_op,L_up,L_ar,
     & L_er,L_ir,L_or,
     & L_ur,L_as,L_es,
     & L_is,L_os,L_us,
     & L_at,L_eke,L_ike,
     & L_oke,L_uke,L_ale,
     & L_ele,L_ile,L_ole,
     & L_ule,L_ame,L_eme,
     & L_ime,L_ome,L_ume,
     & L_ape,L_ebi,L_ibi,
     & L_obi,L_ubi,L_adi,
     & L_edi,L_idi,L_odi,
     & L_udi,L_afi,L_efi,
     & L_ifi,L_ofi,L_ufi,
     & L_aki,L_epo,L_ipo,
     & L_opo,L_upo,L_aro,
     & L_ero,L_iro,L_oro,
     & L_uro,L_aso,L_eso,
     & L_iso,L_oso,L_uso,
     & L_ato,L_eti,L_iti,
     & L_oti,L_uti,L_avi,
     & L_evi,L_ivi,L_ovi,
     & L_uvi,L_axi,L_exi,
     & L_ixi,L_oxi,L_uxi,
     & L_abo,L_epu,L_ipu,
     & L_opu,L_upu,L_aru,
     & L_eru,L_iru,L_oru,L_uru,
     & L_asu,L_eku,L_iku,
     & L_oku,L_uku,L_alu,
     & L_elu,L_ilu,L_olu,
     & L_ulu,L_amu,L_emu,
     & L_imu,L_omu,L_umu,
     & L_apu,L_ek,L_ik,
     & L_ok,L_al,L_el,L_em,
     & L_im,L_om,L_um,L_ap,
     & L_ef,L_if,L_of,L_uf,
     & L_ak,L_uk,L_il,L_ol,
     & L_ul,L_am,L_ebe,L_ibe,
     & L_obe,L_ade,L_ede,L_efe,
     & L_ife,L_ofe,L_ufe,L_ake,
     & L_ex,L_ix,L_ox,L_ux,
     & L_abe,L_ube,L_ide,L_ode,
     & L_ude,L_afe,L_ete,L_ite,
     & L_ote,L_ave,L_eve,L_exe,
     & L_ixe,L_oxe,L_uxe,L_abi,
     & L_ese,L_ise,L_ose,L_use,
     & L_ate,L_ute,L_ive,L_ove,
     & L_uve,L_axe,L_epi,L_ipi,
     & L_opi,L_ari,L_eri,L_esi,
     & L_isi,L_osi,L_usi,L_ati,
     & L_emi,L_imi,L_omi,L_umi,
     & L_api,L_upi,L_iri,L_ori,
     & L_uri,L_asi,L_eko,L_iko,
     & L_oko,L_alo,L_elo,L_emo,
     & L_imo,L_omo,L_umo,L_apo,
     & L_efo,L_ifo,L_ofo,L_ufo,
     & L_ako,L_uko,L_ilo,L_olo,
     & L_ulo,L_amo,L_ebu,L_ibu,
     & L_obu,L_adu,L_edu,L_efu,
     & L_ifu,L_ofu,L_ufu,L_aku,
     & L_exo,L_ixo,L_oxo,L_uxo,
     & L_abu,L_ubu,L_idu,L_odu,
     & L_udu,L_afu,I_(8))
C FDA_70boats.fgi( 166, 208):���������� ������� � ���� ���,BOAT68
      Call LODOCHKA_HANDLER_KTS(deltat,L_et,L_it,
     & L_ot,L_ut,L_av,
     & L_ev,L_iv,L_ov,L_uv,
     & L_ax,L_epe,L_ipe,
     & L_ope,L_upe,L_are,
     & L_ere,L_ire,L_ore,L_ure,
     & L_ase,L_eki,L_iki,
     & L_oki,L_uki,L_ali,
     & L_eli,L_ili,L_oli,L_uli,
     & L_ami,L_ebo,L_ibo,
     & L_obo,L_ubo,L_ado,
     & L_edo,L_ido,L_odo,L_udo,
     & L_afo,L_eto,L_ito,
     & L_oto,L_uto,L_avo,
     & L_evo,L_ivo,L_ovo,L_uvo,
     & L_axo,L_ep,L_ip,
     & L_op,L_up,L_ar,
     & L_er,L_ir,L_or,
     & L_ur,L_as,L_es,
     & L_is,L_os,L_us,
     & L_at,L_eke,L_ike,
     & L_oke,L_uke,L_ale,
     & L_ele,L_ile,L_ole,
     & L_ule,L_ame,L_eme,
     & L_ime,L_ome,L_ume,
     & L_ape,L_ebi,L_ibi,
     & L_obi,L_ubi,L_adi,
     & L_edi,L_idi,L_odi,
     & L_udi,L_afi,L_efi,
     & L_ifi,L_ofi,L_ufi,
     & L_aki,L_epo,L_ipo,
     & L_opo,L_upo,L_aro,
     & L_ero,L_iro,L_oro,
     & L_uro,L_aso,L_eso,
     & L_iso,L_oso,L_uso,
     & L_ato,L_eti,L_iti,
     & L_oti,L_uti,L_avi,
     & L_evi,L_ivi,L_ovi,
     & L_uvi,L_axi,L_exi,
     & L_ixi,L_oxi,L_uxi,
     & L_abo,L_epu,L_ipu,
     & L_opu,L_upu,L_aru,
     & L_eru,L_iru,L_oru,L_uru,
     & L_asu,L_eku,L_iku,
     & L_oku,L_uku,L_alu,
     & L_elu,L_ilu,L_olu,
     & L_ulu,L_amu,L_emu,
     & L_imu,L_omu,L_umu,
     & L_apu,L_ek,L_ik,
     & L_ok,L_al,L_el,L_em,
     & L_im,L_om,L_um,L_ap,
     & L_ef,L_if,L_of,L_uf,
     & L_ak,L_uk,L_il,L_ol,
     & L_ul,L_am,L_ebe,L_ibe,
     & L_obe,L_ade,L_ede,L_efe,
     & L_ife,L_ofe,L_ufe,L_ake,
     & L_ex,L_ix,L_ox,L_ux,
     & L_abe,L_ube,L_ide,L_ode,
     & L_ude,L_afe,L_ete,L_ite,
     & L_ote,L_ave,L_eve,L_exe,
     & L_ixe,L_oxe,L_uxe,L_abi,
     & L_ese,L_ise,L_ose,L_use,
     & L_ate,L_ute,L_ive,L_ove,
     & L_uve,L_axe,L_epi,L_ipi,
     & L_opi,L_ari,L_eri,L_esi,
     & L_isi,L_osi,L_usi,L_ati,
     & L_emi,L_imi,L_omi,L_umi,
     & L_api,L_upi,L_iri,L_ori,
     & L_uri,L_asi,L_eko,L_iko,
     & L_oko,L_alo,L_elo,L_emo,
     & L_imo,L_omo,L_umo,L_apo,
     & L_efo,L_ifo,L_ofo,L_ufo,
     & L_ako,L_uko,L_ilo,L_olo,
     & L_ulo,L_amo,L_ebu,L_ibu,
     & L_obu,L_adu,L_edu,L_efu,
     & L_ifu,L_ofu,L_ufu,L_aku,
     & L_exo,L_ixo,L_oxo,L_uxo,
     & L_abu,L_ubu,L_idu,L_odu,
     & L_udu,L_afu,I_(9))
C FDA_70boats.fgi( 146, 208):���������� ������� � ���� ���,BOAT69
      Call LODOCHKA_HANDLER_KTS(deltat,L_et,L_it,
     & L_ot,L_ut,L_av,
     & L_ev,L_iv,L_ov,L_uv,
     & L_ax,L_epe,L_ipe,
     & L_ope,L_upe,L_are,
     & L_ere,L_ire,L_ore,L_ure,
     & L_ase,L_eki,L_iki,
     & L_oki,L_uki,L_ali,
     & L_eli,L_ili,L_oli,L_uli,
     & L_ami,L_ebo,L_ibo,
     & L_obo,L_ubo,L_ado,
     & L_edo,L_ido,L_odo,L_udo,
     & L_afo,L_eto,L_ito,
     & L_oto,L_uto,L_avo,
     & L_evo,L_ivo,L_ovo,L_uvo,
     & L_axo,L_ep,L_ip,
     & L_op,L_up,L_ar,
     & L_er,L_ir,L_or,
     & L_ur,L_as,L_es,
     & L_is,L_os,L_us,
     & L_at,L_eke,L_ike,
     & L_oke,L_uke,L_ale,
     & L_ele,L_ile,L_ole,
     & L_ule,L_ame,L_eme,
     & L_ime,L_ome,L_ume,
     & L_ape,L_ebi,L_ibi,
     & L_obi,L_ubi,L_adi,
     & L_edi,L_idi,L_odi,
     & L_udi,L_afi,L_efi,
     & L_ifi,L_ofi,L_ufi,
     & L_aki,L_epo,L_ipo,
     & L_opo,L_upo,L_aro,
     & L_ero,L_iro,L_oro,
     & L_uro,L_aso,L_eso,
     & L_iso,L_oso,L_uso,
     & L_ato,L_eti,L_iti,
     & L_oti,L_uti,L_avi,
     & L_evi,L_ivi,L_ovi,
     & L_uvi,L_axi,L_exi,
     & L_ixi,L_oxi,L_uxi,
     & L_abo,L_epu,L_ipu,
     & L_opu,L_upu,L_aru,
     & L_eru,L_iru,L_oru,L_uru,
     & L_asu,L_eku,L_iku,
     & L_oku,L_uku,L_alu,
     & L_elu,L_ilu,L_olu,
     & L_ulu,L_amu,L_emu,
     & L_imu,L_omu,L_umu,
     & L_apu,L_ek,L_ik,
     & L_ok,L_al,L_el,L_em,
     & L_im,L_om,L_um,L_ap,
     & L_ef,L_if,L_of,L_uf,
     & L_ak,L_uk,L_il,L_ol,
     & L_ul,L_am,L_ebe,L_ibe,
     & L_obe,L_ade,L_ede,L_efe,
     & L_ife,L_ofe,L_ufe,L_ake,
     & L_ex,L_ix,L_ox,L_ux,
     & L_abe,L_ube,L_ide,L_ode,
     & L_ude,L_afe,L_ete,L_ite,
     & L_ote,L_ave,L_eve,L_exe,
     & L_ixe,L_oxe,L_uxe,L_abi,
     & L_ese,L_ise,L_ose,L_use,
     & L_ate,L_ute,L_ive,L_ove,
     & L_uve,L_axe,L_epi,L_ipi,
     & L_opi,L_ari,L_eri,L_esi,
     & L_isi,L_osi,L_usi,L_ati,
     & L_emi,L_imi,L_omi,L_umi,
     & L_api,L_upi,L_iri,L_ori,
     & L_uri,L_asi,L_eko,L_iko,
     & L_oko,L_alo,L_elo,L_emo,
     & L_imo,L_omo,L_umo,L_apo,
     & L_efo,L_ifo,L_ofo,L_ufo,
     & L_ako,L_uko,L_ilo,L_olo,
     & L_ulo,L_amo,L_ebu,L_ibu,
     & L_obu,L_adu,L_edu,L_efu,
     & L_ifu,L_ofu,L_ufu,L_aku,
     & L_exo,L_ixo,L_oxo,L_uxo,
     & L_abu,L_ubu,L_idu,L_odu,
     & L_udu,L_afu,I_(10))
C FDA_70boats.fgi( 126, 208):���������� ������� � ���� ���,BOAT70
      Call LODOCHKA_HANDLER_FDA50_COORD(deltat,I_idok,I_isu
     &,
     & I_udok,I_osu,I_afok,I_usu,I_efok,
     & I_atu,I_ifok,I_etu,I_ofok,I_itu,
     & I_ufok,I_otu,I_akok,I_utu,I_avu,
     & I_evu,I_isok,I_ivu,I_osok,I_ovu,
     & I_usok,I_uvu,I_atok,I_axu,I_etok,
     & I_exu,I_itok,I_ixu,I_otok,I_oxu,
     & I_utok,I_uxu,I_avok,I_abad,I_evok,
     & I_ebad,I_ovok,I_ibad,I_uvok,I_obad,
     & I_axok,I_ubad,I_exok,I_adad,I_ixok,
     & I_edad,I_oxok,I_idad,I_uxok,I_odad,
     & I_abuk,I_udad,I_ebuk,I_afad,I_ibuk,
     & I_efad,I_ubuk,I_aduk,I_eduk,I_ifad,
     & I_iduk,I_ofad,I_irok,I_ufad,I_orok,
     & I_akad,I_urok,I_ekad,I_asok,I_ikad,
     & I_ekok,I_okad,I_ikok,I_ukad,I_okok,
     & I_alad,I_alok,I_elad,I_elok,I_ilad,
     & I_ilok,I_olad,I_olok,I_ulad,I_ulok,
     & I_amad,I_amok,I_emad,I_emok,I_imad,
     & I_imok,I_omad,I_omok,I_umad,I_umok,
     & I_apad,I_epok,I_epad,I_ipok,I_ipad,
     & I_opok,I_opad,I_upok,I_upad,I_arok,
     & I_arad,I_erok,I_erad,I_oduk,I_irad,
     & I_uduk,I_orad,I_afuk,I_urad,I_efuk,
     & I_asad,I_ifuk,I_esad,I_ofuk,I_isad,
     & I_akuk,I_osad,I_ekuk,I_usad,I_ikuk,
     & I_atad,I_okuk,I_etad,I_ukuk,I_itad,
     & I_aluk,I_otad,I_eluk,I_utad,I_iluk,
     & I_avad,I_oluk,I_evad,I_uluk,I_ivad,
     & I_edok,I_ovad,I_odok,I_uvad,I_ukok,
     & I_axad,I_apok,I_exad,I_esok,I_ixad,
     & I_ivok,I_oxad,I_obuk,I_uxad,I_ufuk,
     & I_abed,I_ebed,I_ibed,I_obed,I_amuk,
     & INT(I_esu,4),I_ubed,I_aded,I_eded,I_ided,
     & L_ubok,L_ovik,L_ebok,
     & L_adok,L_uvik,L_axik,
     & L_ivik,L_atik,L_etik,
     & L_itik,L_otik,L_utik,
     & L_avik,L_evik,L_exik,
     & L_ixik,L_oxik,L_uxik,
     & L_abok,L_ibok,L_abel,
     & L_obok,I_(1))
C FDA_70boats.fgi( 306, 222):���������� ���������� ������� �� ������� FDA50,BOAT61
      Call LODOCHKA_HANDLER_FDA50_COORD(deltat,I_idok,I_uded
     &,
     & I_udok,I_afed,I_afok,I_efed,I_efok,
     & I_ifed,I_ifok,I_ofed,I_ofok,I_ufed,
     & I_ufok,I_aked,I_akok,I_eked,I_iked,
     & I_oked,I_isok,I_uked,I_osok,I_aled,
     & I_usok,I_eled,I_atok,I_iled,I_etok,
     & I_oled,I_itok,I_uled,I_otok,I_amed,
     & I_utok,I_emed,I_avok,I_imed,I_evok,
     & I_omed,I_ovok,I_umed,I_uvok,I_aped,
     & I_axok,I_eped,I_exok,I_iped,I_ixok,
     & I_oped,I_oxok,I_uped,I_uxok,I_ared,
     & I_abuk,I_ered,I_ebuk,I_ired,I_ibuk,
     & I_ored,I_ubuk,I_aduk,I_eduk,I_ured,
     & I_iduk,I_ased,I_irok,I_esed,I_orok,
     & I_ised,I_urok,I_osed,I_asok,I_used,
     & I_ekok,I_ated,I_ikok,I_eted,I_okok,
     & I_ited,I_alok,I_oted,I_elok,I_uted,
     & I_ilok,I_aved,I_olok,I_eved,I_ulok,
     & I_ived,I_amok,I_oved,I_emok,I_uved,
     & I_imok,I_axed,I_omok,I_exed,I_umok,
     & I_ixed,I_epok,I_oxed,I_ipok,I_uxed,
     & I_opok,I_abid,I_upok,I_ebid,I_arok,
     & I_ibid,I_erok,I_obid,I_oduk,I_ubid,
     & I_uduk,I_adid,I_afuk,I_edid,I_efuk,
     & I_idid,I_ifuk,I_odid,I_ofuk,I_udid,
     & I_akuk,I_afid,I_ekuk,I_efid,I_ikuk,
     & I_ifid,I_okuk,I_ofid,I_ukuk,I_ufid,
     & I_aluk,I_akid,I_eluk,I_ekid,I_iluk,
     & I_ikid,I_oluk,I_okid,I_uluk,I_ukid,
     & I_edok,I_alid,I_odok,I_elid,I_ukok,
     & I_ilid,I_apok,I_olid,I_esok,I_ulid,
     & I_ivok,I_amid,I_obuk,I_emid,I_ufuk,
     & I_imid,I_omid,I_umid,I_apid,I_amuk,
     & INT(I_oded,4),I_epid,I_ipid,I_opid,I_upid,
     & L_ubok,L_ovik,L_ebok,
     & L_adok,L_uvik,L_axik,
     & L_ivik,L_atik,L_etik,
     & L_itik,L_otik,L_utik,
     & L_avik,L_evik,L_exik,
     & L_ixik,L_oxik,L_uxik,
     & L_abok,L_ibok,L_abel,
     & L_obok,I_(2))
C FDA_70boats.fgi( 286, 222):���������� ���������� ������� �� ������� FDA50,BOAT62
      Call LODOCHKA_HANDLER_FDA50_COORD(deltat,I_idok,I_erid
     &,
     & I_udok,I_irid,I_afok,I_orid,I_efok,
     & I_urid,I_ifok,I_asid,I_ofok,I_esid,
     & I_ufok,I_isid,I_akok,I_osid,I_usid,
     & I_atid,I_isok,I_etid,I_osok,I_itid,
     & I_usok,I_otid,I_atok,I_utid,I_etok,
     & I_avid,I_itok,I_evid,I_otok,I_ivid,
     & I_utok,I_ovid,I_avok,I_uvid,I_evok,
     & I_axid,I_ovok,I_exid,I_uvok,I_ixid,
     & I_axok,I_oxid,I_exok,I_uxid,I_ixok,
     & I_abod,I_oxok,I_ebod,I_uxok,I_ibod,
     & I_abuk,I_obod,I_ebuk,I_ubod,I_ibuk,
     & I_adod,I_ubuk,I_aduk,I_eduk,I_edod,
     & I_iduk,I_idod,I_irok,I_odod,I_orok,
     & I_udod,I_urok,I_afod,I_asok,I_efod,
     & I_ekok,I_ifod,I_ikok,I_ofod,I_okok,
     & I_ufod,I_alok,I_akod,I_elok,I_ekod,
     & I_ilok,I_ikod,I_olok,I_okod,I_ulok,
     & I_ukod,I_amok,I_alod,I_emok,I_elod,
     & I_imok,I_ilod,I_omok,I_olod,I_umok,
     & I_ulod,I_epok,I_amod,I_ipok,I_emod,
     & I_opok,I_imod,I_upok,I_omod,I_arok,
     & I_umod,I_erok,I_apod,I_oduk,I_epod,
     & I_uduk,I_ipod,I_afuk,I_opod,I_efuk,
     & I_upod,I_ifuk,I_arod,I_ofuk,I_erod,
     & I_akuk,I_irod,I_ekuk,I_orod,I_ikuk,
     & I_urod,I_okuk,I_asod,I_ukuk,I_esod,
     & I_aluk,I_isod,I_eluk,I_osod,I_iluk,
     & I_usod,I_oluk,I_atod,I_uluk,I_etod,
     & I_edok,I_itod,I_odok,I_otod,I_ukok,
     & I_utod,I_apok,I_avod,I_esok,I_evod,
     & I_ivok,I_ivod,I_obuk,I_ovod,I_ufuk,
     & I_uvod,I_axod,I_exod,I_ixod,I_amuk,
     & INT(I_arid,4),I_oxod,I_uxod,I_abud,I_ebud,
     & L_ubok,L_ovik,L_ebok,
     & L_adok,L_uvik,L_axik,
     & L_ivik,L_atik,L_etik,
     & L_itik,L_otik,L_utik,
     & L_avik,L_evik,L_exik,
     & L_ixik,L_oxik,L_uxik,
     & L_abok,L_ibok,L_abel,
     & L_obok,I_(3))
C FDA_70boats.fgi( 266, 222):���������� ���������� ������� �� ������� FDA50,BOAT63
      Call LODOCHKA_HANDLER_FDA50_COORD(deltat,I_idok,I_obud
     &,
     & I_udok,I_ubud,I_afok,I_adud,I_efok,
     & I_edud,I_ifok,I_idud,I_ofok,I_odud,
     & I_ufok,I_udud,I_akok,I_afud,I_efud,
     & I_ifud,I_isok,I_ofud,I_osok,I_ufud,
     & I_usok,I_akud,I_atok,I_ekud,I_etok,
     & I_ikud,I_itok,I_okud,I_otok,I_ukud,
     & I_utok,I_alud,I_avok,I_elud,I_evok,
     & I_ilud,I_ovok,I_olud,I_uvok,I_ulud,
     & I_axok,I_amud,I_exok,I_emud,I_ixok,
     & I_imud,I_oxok,I_omud,I_uxok,I_umud,
     & I_abuk,I_apud,I_ebuk,I_epud,I_ibuk,
     & I_ipud,I_ubuk,I_aduk,I_eduk,I_opud,
     & I_iduk,I_upud,I_irok,I_arud,I_orok,
     & I_erud,I_urok,I_irud,I_asok,I_orud,
     & I_ekok,I_urud,I_ikok,I_asud,I_okok,
     & I_esud,I_alok,I_isud,I_elok,I_osud,
     & I_ilok,I_usud,I_olok,I_atud,I_ulok,
     & I_etud,I_amok,I_itud,I_emok,I_otud,
     & I_imok,I_utud,I_omok,I_avud,I_umok,
     & I_evud,I_epok,I_ivud,I_ipok,I_ovud,
     & I_opok,I_uvud,I_upok,I_axud,I_arok,
     & I_exud,I_erok,I_ixud,I_oduk,I_oxud,
     & I_uduk,I_uxud,I_afuk,I_abaf,I_efuk,
     & I_ebaf,I_ifuk,I_ibaf,I_ofuk,I_obaf,
     & I_akuk,I_ubaf,I_ekuk,I_adaf,I_ikuk,
     & I_edaf,I_okuk,I_idaf,I_ukuk,I_odaf,
     & I_aluk,I_udaf,I_eluk,I_afaf,I_iluk,
     & I_efaf,I_oluk,I_ifaf,I_uluk,I_ofaf,
     & I_edok,I_ufaf,I_odok,I_akaf,I_ukok,
     & I_ekaf,I_apok,I_ikaf,I_esok,I_okaf,
     & I_ivok,I_ukaf,I_obuk,I_alaf,I_ufuk,
     & I_elaf,I_ilaf,I_olaf,I_ulaf,I_amuk,
     & INT(I_ibud,4),I_amaf,I_emaf,I_imaf,I_omaf,
     & L_ubok,L_ovik,L_ebok,
     & L_adok,L_uvik,L_axik,
     & L_ivik,L_atik,L_etik,
     & L_itik,L_otik,L_utik,
     & L_avik,L_evik,L_exik,
     & L_ixik,L_oxik,L_uxik,
     & L_abok,L_ibok,L_abel,
     & L_obok,I_(4))
C FDA_70boats.fgi( 246, 222):���������� ���������� ������� �� ������� FDA50,BOAT64
      Call LODOCHKA_HANDLER_FDA50_COORD(deltat,I_idok,I_apaf
     &,
     & I_udok,I_epaf,I_afok,I_ipaf,I_efok,
     & I_opaf,I_ifok,I_upaf,I_ofok,I_araf,
     & I_ufok,I_eraf,I_akok,I_iraf,I_oraf,
     & I_uraf,I_isok,I_asaf,I_osok,I_esaf,
     & I_usok,I_isaf,I_atok,I_osaf,I_etok,
     & I_usaf,I_itok,I_ataf,I_otok,I_etaf,
     & I_utok,I_itaf,I_avok,I_otaf,I_evok,
     & I_utaf,I_ovok,I_avaf,I_uvok,I_evaf,
     & I_axok,I_ivaf,I_exok,I_ovaf,I_ixok,
     & I_uvaf,I_oxok,I_axaf,I_uxok,I_exaf,
     & I_abuk,I_ixaf,I_ebuk,I_oxaf,I_ibuk,
     & I_uxaf,I_ubuk,I_aduk,I_eduk,I_abef,
     & I_iduk,I_ebef,I_irok,I_ibef,I_orok,
     & I_obef,I_urok,I_ubef,I_asok,I_adef,
     & I_ekok,I_edef,I_ikok,I_idef,I_okok,
     & I_odef,I_alok,I_udef,I_elok,I_afef,
     & I_ilok,I_efef,I_olok,I_ifef,I_ulok,
     & I_ofef,I_amok,I_ufef,I_emok,I_akef,
     & I_imok,I_ekef,I_omok,I_ikef,I_umok,
     & I_okef,I_epok,I_ukef,I_ipok,I_alef,
     & I_opok,I_elef,I_upok,I_ilef,I_arok,
     & I_olef,I_erok,I_ulef,I_oduk,I_amef,
     & I_uduk,I_emef,I_afuk,I_imef,I_efuk,
     & I_omef,I_ifuk,I_umef,I_ofuk,I_apef,
     & I_akuk,I_epef,I_ekuk,I_ipef,I_ikuk,
     & I_opef,I_okuk,I_upef,I_ukuk,I_aref,
     & I_aluk,I_eref,I_eluk,I_iref,I_iluk,
     & I_oref,I_oluk,I_uref,I_uluk,I_asef,
     & I_edok,I_esef,I_odok,I_isef,I_ukok,
     & I_osef,I_apok,I_usef,I_esok,I_atef,
     & I_ivok,I_etef,I_obuk,I_itef,I_ufuk,
     & I_otef,I_utef,I_avef,I_evef,I_amuk,
     & INT(I_umaf,4),I_ivef,I_ovef,I_uvef,I_axef,
     & L_ubok,L_ovik,L_ebok,
     & L_adok,L_uvik,L_axik,
     & L_ivik,L_atik,L_etik,
     & L_itik,L_otik,L_utik,
     & L_avik,L_evik,L_exik,
     & L_ixik,L_oxik,L_uxik,
     & L_abok,L_ibok,L_abel,
     & L_obok,I_(5))
C FDA_70boats.fgi( 226, 222):���������� ���������� ������� �� ������� FDA50,BOAT65
      Call LODOCHKA_HANDLER_FDA50_COORD(deltat,I_idok,I_ixef
     &,
     & I_udok,I_oxef,I_afok,I_uxef,I_efok,
     & I_abif,I_ifok,I_ebif,I_ofok,I_ibif,
     & I_ufok,I_obif,I_akok,I_ubif,I_adif,
     & I_edif,I_isok,I_idif,I_osok,I_odif,
     & I_usok,I_udif,I_atok,I_afif,I_etok,
     & I_efif,I_itok,I_ifif,I_otok,I_ofif,
     & I_utok,I_ufif,I_avok,I_akif,I_evok,
     & I_ekif,I_ovok,I_ikif,I_uvok,I_okif,
     & I_axok,I_ukif,I_exok,I_alif,I_ixok,
     & I_elif,I_oxok,I_ilif,I_uxok,I_olif,
     & I_abuk,I_ulif,I_ebuk,I_amif,I_ibuk,
     & I_emif,I_ubuk,I_aduk,I_eduk,I_imif,
     & I_iduk,I_omif,I_irok,I_umif,I_orok,
     & I_apif,I_urok,I_epif,I_asok,I_ipif,
     & I_ekok,I_opif,I_ikok,I_upif,I_okok,
     & I_arif,I_alok,I_erif,I_elok,I_irif,
     & I_ilok,I_orif,I_olok,I_urif,I_ulok,
     & I_asif,I_amok,I_esif,I_emok,I_isif,
     & I_imok,I_osif,I_omok,I_usif,I_umok,
     & I_atif,I_epok,I_etif,I_ipok,I_itif,
     & I_opok,I_otif,I_upok,I_utif,I_arok,
     & I_avif,I_erok,I_evif,I_oduk,I_ivif,
     & I_uduk,I_ovif,I_afuk,I_uvif,I_efuk,
     & I_axif,I_ifuk,I_exif,I_ofuk,I_ixif,
     & I_akuk,I_oxif,I_ekuk,I_uxif,I_ikuk,
     & I_abof,I_okuk,I_ebof,I_ukuk,I_ibof,
     & I_aluk,I_obof,I_eluk,I_ubof,I_iluk,
     & I_adof,I_oluk,I_edof,I_uluk,I_idof,
     & I_edok,I_odof,I_odok,I_udof,I_ukok,
     & I_afof,I_apok,I_efof,I_esok,I_ifof,
     & I_ivok,I_ofof,I_obuk,I_ufof,I_ufuk,
     & I_akof,I_ekof,I_ikof,I_okof,I_amuk,
     & INT(I_exef,4),I_ukof,I_alof,I_elof,I_ilof,
     & L_ubok,L_ovik,L_ebok,
     & L_adok,L_uvik,L_axik,
     & L_ivik,L_atik,L_etik,
     & L_itik,L_otik,L_utik,
     & L_avik,L_evik,L_exik,
     & L_ixik,L_oxik,L_uxik,
     & L_abok,L_ibok,L_abel,
     & L_obok,I_(6))
C FDA_70boats.fgi( 206, 222):���������� ���������� ������� �� ������� FDA50,BOAT66
      Call LODOCHKA_HANDLER_FDA50_COORD(deltat,I_idok,I_ulof
     &,
     & I_udok,I_amof,I_afok,I_emof,I_efok,
     & I_imof,I_ifok,I_omof,I_ofok,I_umof,
     & I_ufok,I_apof,I_akok,I_epof,I_ipof,
     & I_opof,I_isok,I_upof,I_osok,I_arof,
     & I_usok,I_erof,I_atok,I_irof,I_etok,
     & I_orof,I_itok,I_urof,I_otok,I_asof,
     & I_utok,I_esof,I_avok,I_isof,I_evok,
     & I_osof,I_ovok,I_usof,I_uvok,I_atof,
     & I_axok,I_etof,I_exok,I_itof,I_ixok,
     & I_otof,I_oxok,I_utof,I_uxok,I_avof,
     & I_abuk,I_evof,I_ebuk,I_ivof,I_ibuk,
     & I_ovof,I_ubuk,I_aduk,I_eduk,I_uvof,
     & I_iduk,I_axof,I_irok,I_exof,I_orok,
     & I_ixof,I_urok,I_oxof,I_asok,I_uxof,
     & I_ekok,I_abuf,I_ikok,I_ebuf,I_okok,
     & I_ibuf,I_alok,I_obuf,I_elok,I_ubuf,
     & I_ilok,I_aduf,I_olok,I_eduf,I_ulok,
     & I_iduf,I_amok,I_oduf,I_emok,I_uduf,
     & I_imok,I_afuf,I_omok,I_efuf,I_umok,
     & I_ifuf,I_epok,I_ofuf,I_ipok,I_ufuf,
     & I_opok,I_akuf,I_upok,I_ekuf,I_arok,
     & I_ikuf,I_erok,I_okuf,I_oduk,I_ukuf,
     & I_uduk,I_aluf,I_afuk,I_eluf,I_efuk,
     & I_iluf,I_ifuk,I_oluf,I_ofuk,I_uluf,
     & I_akuk,I_amuf,I_ekuk,I_emuf,I_ikuk,
     & I_imuf,I_okuk,I_omuf,I_ukuk,I_umuf,
     & I_aluk,I_apuf,I_eluk,I_epuf,I_iluk,
     & I_ipuf,I_oluk,I_opuf,I_uluk,I_upuf,
     & I_edok,I_aruf,I_odok,I_eruf,I_ukok,
     & I_iruf,I_apok,I_oruf,I_esok,I_uruf,
     & I_ivok,I_asuf,I_obuk,I_esuf,I_ufuk,
     & I_isuf,I_osuf,I_usuf,I_atuf,I_amuk,
     & INT(I_olof,4),I_etuf,I_ituf,I_otuf,I_utuf,
     & L_ubok,L_ovik,L_ebok,
     & L_adok,L_uvik,L_axik,
     & L_ivik,L_atik,L_etik,
     & L_itik,L_otik,L_utik,
     & L_avik,L_evik,L_exik,
     & L_ixik,L_oxik,L_uxik,
     & L_abok,L_ibok,L_abel,
     & L_obok,I_(7))
C FDA_70boats.fgi( 186, 222):���������� ���������� ������� �� ������� FDA50,BOAT67
      Call LODOCHKA_HANDLER_FDA50_COORD(deltat,I_idok,I_evuf
     &,
     & I_udok,I_ivuf,I_afok,I_ovuf,I_efok,
     & I_uvuf,I_ifok,I_axuf,I_ofok,I_exuf,
     & I_ufok,I_ixuf,I_akok,I_oxuf,I_uxuf,
     & I_abak,I_isok,I_ebak,I_osok,I_ibak,
     & I_usok,I_obak,I_atok,I_ubak,I_etok,
     & I_adak,I_itok,I_edak,I_otok,I_idak,
     & I_utok,I_odak,I_avok,I_udak,I_evok,
     & I_afak,I_ovok,I_efak,I_uvok,I_ifak,
     & I_axok,I_ofak,I_exok,I_ufak,I_ixok,
     & I_akak,I_oxok,I_ekak,I_uxok,I_ikak,
     & I_abuk,I_okak,I_ebuk,I_ukak,I_ibuk,
     & I_alak,I_ubuk,I_aduk,I_eduk,I_elak,
     & I_iduk,I_ilak,I_irok,I_olak,I_orok,
     & I_ulak,I_urok,I_amak,I_asok,I_emak,
     & I_ekok,I_imak,I_ikok,I_omak,I_okok,
     & I_umak,I_alok,I_apak,I_elok,I_epak,
     & I_ilok,I_ipak,I_olok,I_opak,I_ulok,
     & I_upak,I_amok,I_arak,I_emok,I_erak,
     & I_imok,I_irak,I_omok,I_orak,I_umok,
     & I_urak,I_epok,I_asak,I_ipok,I_esak,
     & I_opok,I_isak,I_upok,I_osak,I_arok,
     & I_usak,I_erok,I_atak,I_oduk,I_etak,
     & I_uduk,I_itak,I_afuk,I_otak,I_efuk,
     & I_utak,I_ifuk,I_avak,I_ofuk,I_evak,
     & I_akuk,I_ivak,I_ekuk,I_ovak,I_ikuk,
     & I_uvak,I_okuk,I_axak,I_ukuk,I_exak,
     & I_aluk,I_ixak,I_eluk,I_oxak,I_iluk,
     & I_uxak,I_oluk,I_abek,I_uluk,I_ebek,
     & I_edok,I_ibek,I_odok,I_obek,I_ukok,
     & I_ubek,I_apok,I_adek,I_esok,I_edek,
     & I_ivok,I_idek,I_obuk,I_odek,I_ufuk,
     & I_udek,I_afek,I_efek,I_ifek,I_amuk,
     & INT(I_avuf,4),I_ofek,I_ufek,I_akek,I_ekek,
     & L_ubok,L_ovik,L_ebok,
     & L_adok,L_uvik,L_axik,
     & L_ivik,L_atik,L_etik,
     & L_itik,L_otik,L_utik,
     & L_avik,L_evik,L_exik,
     & L_ixik,L_oxik,L_uxik,
     & L_abok,L_ibok,L_abel,
     & L_obok,I_(8))
C FDA_70boats.fgi( 166, 222):���������� ���������� ������� �� ������� FDA50,BOAT68
      Call LODOCHKA_HANDLER_FDA50_COORD(deltat,I_idok,I_okek
     &,
     & I_udok,I_ukek,I_afok,I_alek,I_efok,
     & I_elek,I_ifok,I_ilek,I_ofok,I_olek,
     & I_ufok,I_ulek,I_akok,I_amek,I_emek,
     & I_imek,I_isok,I_omek,I_osok,I_umek,
     & I_usok,I_apek,I_atok,I_epek,I_etok,
     & I_ipek,I_itok,I_opek,I_otok,I_upek,
     & I_utok,I_arek,I_avok,I_erek,I_evok,
     & I_irek,I_ovok,I_orek,I_uvok,I_urek,
     & I_axok,I_asek,I_exok,I_esek,I_ixok,
     & I_isek,I_oxok,I_osek,I_uxok,I_usek,
     & I_abuk,I_atek,I_ebuk,I_etek,I_ibuk,
     & I_itek,I_ubuk,I_aduk,I_eduk,I_otek,
     & I_iduk,I_utek,I_irok,I_avek,I_orok,
     & I_evek,I_urok,I_ivek,I_asok,I_ovek,
     & I_ekok,I_uvek,I_ikok,I_axek,I_okok,
     & I_exek,I_alok,I_ixek,I_elok,I_oxek,
     & I_ilok,I_uxek,I_olok,I_abik,I_ulok,
     & I_ebik,I_amok,I_ibik,I_emok,I_obik,
     & I_imok,I_ubik,I_omok,I_adik,I_umok,
     & I_edik,I_epok,I_idik,I_ipok,I_odik,
     & I_opok,I_udik,I_upok,I_afik,I_arok,
     & I_efik,I_erok,I_ifik,I_oduk,I_ofik,
     & I_uduk,I_ufik,I_afuk,I_akik,I_efuk,
     & I_ekik,I_ifuk,I_ikik,I_ofuk,I_okik,
     & I_akuk,I_ukik,I_ekuk,I_alik,I_ikuk,
     & I_elik,I_okuk,I_ilik,I_ukuk,I_olik,
     & I_aluk,I_ulik,I_eluk,I_amik,I_iluk,
     & I_emik,I_oluk,I_imik,I_uluk,I_omik,
     & I_edok,I_umik,I_odok,I_apik,I_ukok,
     & I_epik,I_apok,I_ipik,I_esok,I_opik,
     & I_ivok,I_upik,I_obuk,I_arik,I_ufuk,
     & I_erik,I_irik,I_orik,I_urik,I_amuk,
     & INT(I_ikek,4),I_asik,I_esik,I_isik,I_osik,
     & L_ubok,L_ovik,L_ebok,
     & L_adok,L_uvik,L_axik,
     & L_ivik,L_atik,L_etik,
     & L_itik,L_otik,L_utik,
     & L_avik,L_evik,L_exik,
     & L_ixik,L_oxik,L_uxik,
     & L_abok,L_ibok,L_abel,
     & L_obok,I_(9))
C FDA_70boats.fgi( 146, 222):���������� ���������� ������� �� ������� FDA50,BOAT69
      Call LODOCHKA_HANDLER_FDA50_COORD(deltat,I_idok,I_emuk
     &,
     & I_udok,I_imuk,I_afok,I_omuk,I_efok,
     & I_umuk,I_ifok,I_apuk,I_ofok,I_epuk,
     & I_ufok,I_ipuk,I_akok,I_opuk,I_upuk,
     & I_aruk,I_isok,I_eruk,I_osok,I_iruk,
     & I_usok,I_oruk,I_atok,I_uruk,I_etok,
     & I_asuk,I_itok,I_esuk,I_otok,I_isuk,
     & I_utok,I_osuk,I_avok,I_usuk,I_evok,
     & I_atuk,I_ovok,I_etuk,I_uvok,I_ituk,
     & I_axok,I_otuk,I_exok,I_utuk,I_ixok,
     & I_avuk,I_oxok,I_evuk,I_uxok,I_ivuk,
     & I_abuk,I_ovuk,I_ebuk,I_uvuk,I_ibuk,
     & I_axuk,I_ubuk,I_aduk,I_eduk,I_exuk,
     & I_iduk,I_ixuk,I_irok,I_oxuk,I_orok,
     & I_uxuk,I_urok,I_abal,I_asok,I_ebal,
     & I_ekok,I_ibal,I_ikok,I_obal,I_okok,
     & I_ubal,I_alok,I_adal,I_elok,I_edal,
     & I_ilok,I_idal,I_olok,I_odal,I_ulok,
     & I_udal,I_amok,I_afal,I_emok,I_efal,
     & I_imok,I_ifal,I_omok,I_ofal,I_umok,
     & I_ufal,I_epok,I_akal,I_ipok,I_ekal,
     & I_opok,I_ikal,I_upok,I_okal,I_arok,
     & I_ukal,I_erok,I_alal,I_oduk,I_elal,
     & I_uduk,I_ilal,I_afuk,I_olal,I_efuk,
     & I_ulal,I_ifuk,I_amal,I_ofuk,I_emal,
     & I_akuk,I_imal,I_ekuk,I_omal,I_ikuk,
     & I_umal,I_okuk,I_apal,I_ukuk,I_epal,
     & I_aluk,I_ipal,I_eluk,I_opal,I_iluk,
     & I_upal,I_oluk,I_aral,I_uluk,I_eral,
     & I_edok,I_iral,I_odok,I_oral,I_ukok,
     & I_ural,I_apok,I_asal,I_esok,I_esal,
     & I_ivok,I_isal,I_obuk,I_osal,I_ufuk,
     & I_usal,I_atal,I_etal,I_ital,I_amuk,
     & INT(I_usik,4),I_otal,I_utal,I_aval,I_eval,
     & L_ubok,L_ovik,L_ebok,
     & L_adok,L_uvik,L_axik,
     & L_ivik,L_atik,L_etik,
     & L_itik,L_otik,L_utik,
     & L_avik,L_evik,L_exik,
     & L_ixik,L_oxik,L_uxik,
     & L_abok,L_ibok,L_abel,
     & L_obok,I_(10))
C FDA_70boats.fgi( 126, 222):���������� ���������� ������� �� ������� FDA50,BOAT70
      Call LODOCHKA_HANDLER_FDA90_COORD(deltat,L_ival,
     & L_axal,L_ixal,L_uxal,
     & L_abel,L_exal,L_ebel,
     & L_oxal,L_oval,L_opabe,L_uval,
     & L_osabe,L_otabe,L_orabe,I_(1))
C FDA_70boats.fgi( 306, 236):���������� ���������� ������� �� ������� FDA90,BOAT61
      Call LODOCHKA_HANDLER_FDA90_COORD(deltat,L_ival,
     & L_axal,L_ixal,L_uxal,
     & L_abel,L_exal,L_ebel,
     & L_oxal,L_oval,L_opabe,L_uval,
     & L_osabe,L_otabe,L_orabe,I_(2))
C FDA_70boats.fgi( 286, 236):���������� ���������� ������� �� ������� FDA90,BOAT62
      Call LODOCHKA_HANDLER_FDA90_COORD(deltat,L_ival,
     & L_axal,L_ixal,L_uxal,
     & L_abel,L_exal,L_ebel,
     & L_oxal,L_oval,L_opabe,L_uval,
     & L_osabe,L_otabe,L_orabe,I_(3))
C FDA_70boats.fgi( 266, 236):���������� ���������� ������� �� ������� FDA90,BOAT63
      Call LODOCHKA_HANDLER_FDA90_COORD(deltat,L_ival,
     & L_axal,L_ixal,L_uxal,
     & L_abel,L_exal,L_ebel,
     & L_oxal,L_oval,L_opabe,L_uval,
     & L_osabe,L_otabe,L_orabe,I_(4))
C FDA_70boats.fgi( 246, 236):���������� ���������� ������� �� ������� FDA90,BOAT64
      Call LODOCHKA_HANDLER_FDA90_COORD(deltat,L_ival,
     & L_axal,L_ixal,L_uxal,
     & L_abel,L_exal,L_ebel,
     & L_oxal,L_oval,L_opabe,L_uval,
     & L_osabe,L_otabe,L_orabe,I_(5))
C FDA_70boats.fgi( 226, 236):���������� ���������� ������� �� ������� FDA90,BOAT65
      Call LODOCHKA_HANDLER_FDA90_COORD(deltat,L_ival,
     & L_axal,L_ixal,L_uxal,
     & L_abel,L_exal,L_ebel,
     & L_oxal,L_oval,L_opabe,L_uval,
     & L_osabe,L_otabe,L_orabe,I_(6))
C FDA_70boats.fgi( 206, 236):���������� ���������� ������� �� ������� FDA90,BOAT66
      Call LODOCHKA_HANDLER_FDA90_COORD(deltat,L_ival,
     & L_axal,L_ixal,L_uxal,
     & L_abel,L_exal,L_ebel,
     & L_oxal,L_oval,L_opabe,L_uval,
     & L_osabe,L_otabe,L_orabe,I_(7))
C FDA_70boats.fgi( 186, 236):���������� ���������� ������� �� ������� FDA90,BOAT67
      Call LODOCHKA_HANDLER_FDA90_COORD(deltat,L_ival,
     & L_axal,L_ixal,L_uxal,
     & L_abel,L_exal,L_ebel,
     & L_oxal,L_oval,L_opabe,L_uval,
     & L_osabe,L_otabe,L_orabe,I_(8))
C FDA_70boats.fgi( 166, 236):���������� ���������� ������� �� ������� FDA90,BOAT68
      Call LODOCHKA_HANDLER_FDA90_COORD(deltat,L_ival,
     & L_axal,L_ixal,L_uxal,
     & L_abel,L_exal,L_ebel,
     & L_oxal,L_oval,L_opabe,L_uval,
     & L_osabe,L_otabe,L_orabe,I_(9))
C FDA_70boats.fgi( 146, 236):���������� ���������� ������� �� ������� FDA90,BOAT69
      Call LODOCHKA_HANDLER_FDA90_COORD(deltat,L_ival,
     & L_axal,L_ixal,L_uxal,
     & L_abel,L_exal,L_ebel,
     & L_oxal,L_oval,L_opabe,L_uval,
     & L_osabe,L_otabe,L_orabe,I_(10))
C FDA_70boats.fgi( 126, 236):���������� ���������� ������� �� ������� FDA90,BOAT70
      Call LODOCHKA_HANDLER(deltat,REAL(R_umebe,4),
     & L_olebe,L_alebe,
     & L_elebe,L_ulebe,L_ukebe,
     & L_ilebe,R_utas,R_isas,L_udes,L_emebe,
     & L_omabe,L_imabe,R_avas,R_osas,
     & L_afes,L_emabe,R_evas,R_usas,
     & L_efes,I_ifes,I_ofes,I_ufes,I_akes,I_ekes,
     & INT(I_akebe,4),L_ekebe,L_emas,
     & L_imas,L_omas,L_umas,
     & L_apas,L_epas,L_ipas,
     & L_opas,L_upas,L_aras,I_(1),I_ufas,
     & R_ikes,REAL(R_afebe,4),R_okes,
     & REAL(R_udebe,4),REAL(R_odebe,4),R_ukes,R_ales,
     & R_eles,REAL(R_ikebe,4),R_iles,
     & REAL(R_okebe,4),R_etas,R_uras,L_edes,L_atas,
     & L_imebe,REAL(R_omebe,4),R_ofas,R_ules,
     & R_ames,R_emes,REAL(R_uvabe,4),R_imes,
     & REAL(R_ivabe,4),REAL(R_utabe,4),R_omes,
     & REAL(R_avabe,4),R_umes,R_itas,R_asas,
     & L_ides,R_apes,R_epes,R_ipes,L_elas,
     & R_opes,R_upes,REAL(R_erabe,4),R_ares,R_otas,
     & R_esas,L_odes,R_eres,R_ires,R_ores,R_ures,
     & R_ases,REAL(R_esabe,4),R_eses,L_ilas,
     & L_ivas,L_asabe,REAL(R_urabe,4),L_osabe,
     & R_ises,R_oses,R_uses,REAL(R_etabe,4),R_ates,
     & L_olas,L_ovas,L_atabe,
     & REAL(R_usabe,4),L_otabe,R_etes,R_ites,
     & R_otes,R_utes,REAL(R_epabe,4),R_aves,
     & L_alas,L_arabe,L_orabe,L_uvas,
     & L_axas,L_apabe,R_usur,R_uxas,
     & REAL(R_upabe,4),REAL(R_umabe,4),L_opabe,
     & R_abes,I_ekas,R_ebas,R_ebes,R_eves,
     & REAL(R_efebe,4),REAL(R_ofebe,4),R_oles,R_ixur,C20_obas
     &)
C FDA_70boats.fgi( 305, 264):���������� �������,BOAT61
      I_af=I_(1)
C FDA_70boats.fgi( 305, 264):������-�������: ���������� ��� �������������� ������,cpy BOAT61:CR
      Call LODOCHKA_HANDLER_FDA60_COORD(deltat,I_opil,R_elar
     &,
     & I_alar,R_ikor,I_upil,I_ekor,I_aril,
     & I_emir,R_opir,I_imir,
     & R_upir,I_omir,R_arir,
     & I_umir,R_erir,I_apir,
     & R_irir,I_epir,R_orir,
     & I_ipir,R_urir,I_ufel,
     & R_olir,I_ilir,I_akel,
     & R_amir,I_ulir,R_ador,
     & I_ubor,I_ekel,R_isir,
     & I_asir,I_ikel,R_osir,
     & I_esir,I_okel,R_idor,I_edor,
     & I_ukel,R_ofor,I_ifor,I_alel,
     & R_udor,I_odor,I_elel,
     & R_akor,I_ufor,I_ilel,
     & R_efor,I_afor,I_olel,I_ulel,
     & I_amel,I_emel,I_imel,I_omel,I_umel,I_apel,
     & I_epel,I_ipel,I_opel,I_upel,I_arel,I_erel,
     & I_irel,I_orel,I_urel,I_asel,I_esel,I_isel,
     & I_osel,I_usel,I_atel,I_etel,I_itel,I_otel,
     & I_utel,I_avel,I_evel,I_ivel,I_ovel,I_uvel,
     & I_axel,I_exel,I_ixel,I_oxel,I_uxel,I_abil,
     & I_ebil,I_ibil,I_obil,I_ubil,I_adil,I_edil,
     & I_idil,I_odil,I_udil,I_afil,I_efil,I_ifil,
     & I_ofil,I_ufil,I_akil,I_ekil,I_ikil,I_okil,
     & I_ukil,I_alil,I_elil,I_ilil,I_olil,I_ulil,
     & I_amil,I_emil,I_imil,I_omil,I_umil,I_apil,
     & I_epil,I_ipil,INT(I_af,4),R_epar,R_apar,
     & R_umar,R_omar,R_imar,R_eper,
     & R_aper,R_umer,R_omer,R_imer,R_irer,
     & R_orer,R_urer,R_aser,R_eser,
     & R_irar,R_orar,R_urar,R_asar,
     & R_esar,R_iter,R_oter,R_uter,
     & R_aver,R_ever,R_itar,R_otar,
     & R_utar,R_avar,R_evar,R_obor,
     & R_ibor,R_ebor,R_abor,
     & R_uxir,R_ovir,R_ivir,
     & R_evir,R_avir,R_utir,
     & R_ixer,R_oxer,R_uxer,R_abir,R_ebir,
     & R_ixar,R_oxar,R_uxar,R_aber,
     & R_eber,R_efer,R_afer,R_uder,
     & R_oder,R_ider,R_efir,R_afir,
     & R_udir,R_odir,R_idir,R_ikir,R_okir,
     & R_ukir,R_alir,R_elir,R_iker,
     & R_oker,R_uker,R_aler,REAL(R_ofas,4),
     & R_eler,L_afel,L_udel,L_odel,L_efel,L_ifel,L_ofel,
     & L_ubel,L_obel,L_ibel,L_adel,L_edel,L_idel,I_emar,
     & I_amar,I_iler,I_oler,I_uler,I_amer,I_emer,
     & I_ilar,I_olar,I_ular,I_erar,I_arar,
     & I_iper,I_oper,I_uper,I_arer,I_erer,I_ipar,
     & I_opar,I_upar,I_etar,I_atar,I_iser,
     & I_oser,I_user,I_ater,I_eter,I_isar,I_osar,
     & I_usar,I_exar,I_axar,I_iver,I_over,
     & I_uver,I_axer,I_exer,I_ivar,I_ovar,
     & I_uvar,I_eder,I_ader,I_ibir,I_obir,
     & I_ubir,I_adir,I_edir,I_iber,I_ober,
     & I_uber,I_eker,I_aker,I_ifir,I_ofir,
     & I_ufir,I_akir,I_ekir,I_ifer,I_ofer,
     & I_ufer,I_usir,I_atir,I_etir,
     & I_itir,I_otir,61,I_uvir,
     & I_axir,I_exir,I_ixir,
     & I_oxir)
C FDA_70boats.fgi( 306, 250):���������� ���������� ������� �� ������� FDA60,BOAT61
      Call LODOCHKA_HANDLER(deltat,REAL(R_umebe,4),
     & L_olebe,L_alebe,
     & L_elebe,L_ulebe,L_ukebe,
     & L_ilebe,R_obos,R_exis,L_olos,L_emebe,
     & L_omabe,L_imabe,R_ubos,R_ixis,
     & L_ulos,L_emabe,R_ados,R_oxis,
     & L_amos,I_emos,I_imos,I_omos,I_umos,I_apos,
     & INT(I_akebe,4),L_ekebe,L_asis,
     & L_esis,L_isis,L_osis,
     & L_usis,L_atis,L_etis,
     & L_itis,L_otis,L_utis,I_(2),I_omis,
     & R_epos,REAL(R_afebe,4),R_ipos,
     & REAL(R_udebe,4),REAL(R_odebe,4),R_opos,R_upos,
     & R_aros,REAL(R_ikebe,4),R_eros,
     & REAL(R_okebe,4),R_abos,R_ovis,L_alos,L_uxis,
     & L_imebe,REAL(R_omebe,4),R_imis,R_oros,
     & R_uros,R_asos,REAL(R_uvabe,4),R_esos,
     & REAL(R_ivabe,4),REAL(R_utabe,4),R_isos,
     & REAL(R_avabe,4),R_osos,R_ebos,R_uvis,
     & L_elos,R_usos,R_atos,R_etos,L_aris,
     & R_itos,R_otos,REAL(R_erabe,4),R_utos,R_ibos,
     & R_axis,L_ilos,R_avos,R_evos,R_ivos,R_ovos,
     & R_uvos,REAL(R_esabe,4),R_axos,L_eris,
     & L_edos,L_asabe,REAL(R_urabe,4),L_osabe,
     & R_exos,R_ixos,R_oxos,REAL(R_etabe,4),R_uxos,
     & L_iris,L_idos,L_atabe,
     & REAL(R_usabe,4),L_otabe,R_abus,R_ebus,
     & R_ibus,R_obus,REAL(R_epabe,4),R_ubus,
     & L_upis,L_arabe,L_orabe,L_odos,
     & L_udos,L_apabe,R_oxes,R_ofos,
     & REAL(R_upabe,4),REAL(R_umabe,4),L_opabe,
     & R_ufos,I_apis,R_akis,R_akos,R_adus,
     & REAL(R_efebe,4),REAL(R_ofebe,4),R_iros,R_efis,C20_ikis
     &)
C FDA_70boats.fgi( 285, 264):���������� �������,BOAT62
      I_ud=I_(2)
C FDA_70boats.fgi( 285, 264):������-�������: ���������� ��� �������������� ������,cpy BOAT62:CR
      Call LODOCHKA_HANDLER_FDA60_COORD(deltat,I_idul,R_elar
     &,
     & I_alar,R_ikor,I_odul,I_ekor,I_udul,
     & I_emir,R_opir,I_imir,
     & R_upir,I_omir,R_arir,
     & I_umir,R_erir,I_apir,
     & R_irir,I_epir,R_orir,
     & I_ipir,R_urir,I_otil,
     & R_olir,I_ilir,I_util,
     & R_amir,I_ulir,R_ador,
     & I_ubor,I_avil,R_isir,
     & I_asir,I_evil,R_osir,
     & I_esir,I_ivil,R_idor,I_edor,
     & I_ovil,R_ofor,I_ifor,I_uvil,
     & R_udor,I_odor,I_axil,
     & R_akor,I_ufor,I_exil,
     & R_efor,I_afor,I_ixil,I_oxil,
     & I_uxil,I_abol,I_ebol,I_ibol,I_obol,I_ubol,
     & I_adol,I_edol,I_idol,I_odol,I_udol,I_afol,
     & I_efol,I_ifol,I_ofol,I_ufol,I_akol,I_ekol,
     & I_ikol,I_okol,I_ukol,I_alol,I_elol,I_ilol,
     & I_olol,I_ulol,I_amol,I_emol,I_imol,I_omol,
     & I_umol,I_apol,I_epol,I_ipol,I_opol,I_upol,
     & I_arol,I_erol,I_irol,I_orol,I_urol,I_asol,
     & I_esol,I_isol,I_osol,I_usol,I_atol,I_etol,
     & I_itol,I_otol,I_utol,I_avol,I_evol,I_ivol,
     & I_ovol,I_uvol,I_axol,I_exol,I_ixol,I_oxol,
     & I_uxol,I_abul,I_ebul,I_ibul,I_obul,I_ubul,
     & I_adul,I_edul,INT(I_ud,4),R_epar,R_apar,
     & R_umar,R_omar,R_imar,R_eper,
     & R_aper,R_umer,R_omer,R_imer,R_irer,
     & R_orer,R_urer,R_aser,R_eser,
     & R_irar,R_orar,R_urar,R_asar,
     & R_esar,R_iter,R_oter,R_uter,
     & R_aver,R_ever,R_itar,R_otar,
     & R_utar,R_avar,R_evar,R_obor,
     & R_ibor,R_ebor,R_abor,
     & R_uxir,R_ovir,R_ivir,
     & R_evir,R_avir,R_utir,
     & R_ixer,R_oxer,R_uxer,R_abir,R_ebir,
     & R_ixar,R_oxar,R_uxar,R_aber,
     & R_eber,R_efer,R_afer,R_uder,
     & R_oder,R_ider,R_efir,R_afir,
     & R_udir,R_odir,R_idir,R_ikir,R_okir,
     & R_ukir,R_alir,R_elir,R_iker,
     & R_oker,R_uker,R_aler,REAL(R_imis,4),
     & R_eler,L_usil,L_osil,L_isil,L_atil,L_etil,L_itil,
     & L_oril,L_iril,L_eril,L_uril,L_asil,L_esil,I_emar,
     & I_amar,I_iler,I_oler,I_uler,I_amer,I_emer,
     & I_ilar,I_olar,I_ular,I_erar,I_arar,
     & I_iper,I_oper,I_uper,I_arer,I_erer,I_ipar,
     & I_opar,I_upar,I_etar,I_atar,I_iser,
     & I_oser,I_user,I_ater,I_eter,I_isar,I_osar,
     & I_usar,I_exar,I_axar,I_iver,I_over,
     & I_uver,I_axer,I_exer,I_ivar,I_ovar,
     & I_uvar,I_eder,I_ader,I_ibir,I_obir,
     & I_ubir,I_adir,I_edir,I_iber,I_ober,
     & I_uber,I_eker,I_aker,I_ifir,I_ofir,
     & I_ufir,I_akir,I_ekir,I_ifer,I_ofer,
     & I_ufer,I_usir,I_atir,I_etir,
     & I_itir,I_otir,62,I_uvir,
     & I_axir,I_exir,I_ixir,
     & I_oxir)
C FDA_70boats.fgi( 286, 250):���������� ���������� ������� �� ������� FDA60,BOAT62
      Call LODOCHKA_HANDLER(deltat,REAL(R_umebe,4),
     & L_olebe,L_alebe,
     & L_elebe,L_ulebe,L_ukebe,
     & L_ilebe,R_ikat,R_afat,L_irat,L_emebe,
     & L_omabe,L_imabe,R_okat,R_efat,
     & L_orat,L_emabe,R_ukat,R_ifat,
     & L_urat,I_asat,I_esat,I_isat,I_osat,I_usat,
     & INT(I_akebe,4),L_ekebe,L_uvus,
     & L_axus,L_exus,L_ixus,
     & L_oxus,L_uxus,L_abat,
     & L_ebat,L_ibat,L_obat,I_(3),I_isus,
     & R_atat,REAL(R_afebe,4),R_etat,
     & REAL(R_udebe,4),REAL(R_odebe,4),R_itat,R_otat,
     & R_utat,REAL(R_ikebe,4),R_avat,
     & REAL(R_okebe,4),R_ufat,R_idat,L_upat,L_ofat,
     & L_imebe,REAL(R_omebe,4),R_esus,R_ivat,
     & R_ovat,R_uvat,REAL(R_uvabe,4),R_axat,
     & REAL(R_ivabe,4),REAL(R_utabe,4),R_exat,
     & REAL(R_avabe,4),R_ixat,R_akat,R_odat,
     & L_arat,R_oxat,R_uxat,R_abet,L_utus,
     & R_ebet,R_ibet,REAL(R_erabe,4),R_obet,R_ekat,
     & R_udat,L_erat,R_ubet,R_adet,R_edet,R_idet,
     & R_odet,REAL(R_esabe,4),R_udet,L_avus,
     & L_alat,L_asabe,REAL(R_urabe,4),L_osabe,
     & R_afet,R_efet,R_ifet,REAL(R_etabe,4),R_ofet,
     & L_evus,L_elat,L_atabe,
     & REAL(R_usabe,4),L_otabe,R_ufet,R_aket,
     & R_eket,R_iket,REAL(R_epabe,4),R_oket,
     & L_otus,L_arabe,L_orabe,L_ilat,
     & L_olat,L_apabe,R_ifus,R_imat,
     & REAL(R_upabe,4),REAL(R_umabe,4),L_opabe,
     & R_omat,I_usus,R_umus,R_umat,R_uket,
     & REAL(R_efebe,4),REAL(R_ofebe,4),R_evat,R_amus,C20_epus
     &)
C FDA_70boats.fgi( 265, 264):���������� �������,BOAT63
      I_od=I_(3)
C FDA_70boats.fgi( 265, 264):������-�������: ���������� ��� �������������� ������,cpy BOAT63:CR
      Call LODOCHKA_HANDLER_FDA60_COORD(deltat,I_esam,R_elar
     &,
     & I_alar,R_ikor,I_isam,I_ekor,I_osam,
     & I_emir,R_opir,I_imir,
     & R_upir,I_omir,R_arir,
     & I_umir,R_erir,I_apir,
     & R_irir,I_epir,R_orir,
     & I_ipir,R_urir,I_ilul,
     & R_olir,I_ilir,I_olul,
     & R_amir,I_ulir,R_ador,
     & I_ubor,I_ulul,R_isir,
     & I_asir,I_amul,R_osir,
     & I_esir,I_emul,R_idor,I_edor,
     & I_imul,R_ofor,I_ifor,I_omul,
     & R_udor,I_odor,I_umul,
     & R_akor,I_ufor,I_apul,
     & R_efor,I_afor,I_epul,I_ipul,
     & I_opul,I_upul,I_arul,I_erul,I_irul,I_orul,
     & I_urul,I_asul,I_esul,I_isul,I_osul,I_usul,
     & I_atul,I_etul,I_itul,I_otul,I_utul,I_avul,
     & I_evul,I_ivul,I_ovul,I_uvul,I_axul,I_exul,
     & I_ixul,I_oxul,I_uxul,I_abam,I_ebam,I_ibam,
     & I_obam,I_ubam,I_adam,I_edam,I_idam,I_odam,
     & I_udam,I_afam,I_efam,I_ifam,I_ofam,I_ufam,
     & I_akam,I_ekam,I_ikam,I_okam,I_ukam,I_alam,
     & I_elam,I_ilam,I_olam,I_ulam,I_amam,I_emam,
     & I_imam,I_omam,I_umam,I_apam,I_epam,I_ipam,
     & I_opam,I_upam,I_aram,I_eram,I_iram,I_oram,
     & I_uram,I_asam,INT(I_od,4),R_epar,R_apar,
     & R_umar,R_omar,R_imar,R_eper,
     & R_aper,R_umer,R_omer,R_imer,R_irer,
     & R_orer,R_urer,R_aser,R_eser,
     & R_irar,R_orar,R_urar,R_asar,
     & R_esar,R_iter,R_oter,R_uter,
     & R_aver,R_ever,R_itar,R_otar,
     & R_utar,R_avar,R_evar,R_obor,
     & R_ibor,R_ebor,R_abor,
     & R_uxir,R_ovir,R_ivir,
     & R_evir,R_avir,R_utir,
     & R_ixer,R_oxer,R_uxer,R_abir,R_ebir,
     & R_ixar,R_oxar,R_uxar,R_aber,
     & R_eber,R_efer,R_afer,R_uder,
     & R_oder,R_ider,R_efir,R_afir,
     & R_udir,R_odir,R_idir,R_ikir,R_okir,
     & R_ukir,R_alir,R_elir,R_iker,
     & R_oker,R_uker,R_aler,REAL(R_esus,4),
     & R_eler,L_okul,L_ikul,L_ekul,L_ukul,L_alul,L_elul,
     & L_iful,L_eful,L_aful,L_oful,L_uful,L_akul,I_emar,
     & I_amar,I_iler,I_oler,I_uler,I_amer,I_emer,
     & I_ilar,I_olar,I_ular,I_erar,I_arar,
     & I_iper,I_oper,I_uper,I_arer,I_erer,I_ipar,
     & I_opar,I_upar,I_etar,I_atar,I_iser,
     & I_oser,I_user,I_ater,I_eter,I_isar,I_osar,
     & I_usar,I_exar,I_axar,I_iver,I_over,
     & I_uver,I_axer,I_exer,I_ivar,I_ovar,
     & I_uvar,I_eder,I_ader,I_ibir,I_obir,
     & I_ubir,I_adir,I_edir,I_iber,I_ober,
     & I_uber,I_eker,I_aker,I_ifir,I_ofir,
     & I_ufir,I_akir,I_ekir,I_ifer,I_ofer,
     & I_ufer,I_usir,I_atir,I_etir,
     & I_itir,I_otir,63,I_uvir,
     & I_axir,I_exir,I_ixir,
     & I_oxir)
C FDA_70boats.fgi( 266, 250):���������� ���������� ������� �� ������� FDA60,BOAT63
      Call LODOCHKA_HANDLER(deltat,REAL(R_umebe,4),
     & L_olebe,L_alebe,
     & L_elebe,L_ulebe,L_ukebe,
     & L_ilebe,R_epit,R_ulit,L_evit,L_emebe,
     & L_omabe,L_imabe,R_ipit,R_amit,
     & L_ivit,L_emabe,R_opit,R_emit,
     & L_ovit,I_uvit,I_axit,I_exit,I_ixit,I_oxit,
     & INT(I_akebe,4),L_ekebe,L_odit,
     & L_udit,L_afit,L_efit,
     & L_ifit,L_ofit,L_ufit,
     & L_akit,L_ekit,L_ikit,I_(4),I_exet,
     & R_uxit,REAL(R_afebe,4),R_abot,
     & REAL(R_udebe,4),REAL(R_odebe,4),R_ebot,R_ibot,
     & R_obot,REAL(R_ikebe,4),R_ubot,
     & REAL(R_okebe,4),R_omit,R_elit,L_otit,L_imit,
     & L_imebe,REAL(R_omebe,4),R_axet,R_edot,
     & R_idot,R_odot,REAL(R_uvabe,4),R_udot,
     & REAL(R_ivabe,4),REAL(R_utabe,4),R_afot,
     & REAL(R_avabe,4),R_efot,R_umit,R_ilit,
     & L_utit,R_ifot,R_ofot,R_ufot,L_obit,
     & R_akot,R_ekot,REAL(R_erabe,4),R_ikot,R_apit,
     & R_olit,L_avit,R_okot,R_ukot,R_alot,R_elot,
     & R_ilot,REAL(R_esabe,4),R_olot,L_ubit,
     & L_upit,L_asabe,REAL(R_urabe,4),L_osabe,
     & R_ulot,R_amot,R_emot,REAL(R_etabe,4),R_imot,
     & L_adit,L_arit,L_atabe,
     & REAL(R_usabe,4),L_otabe,R_omot,R_umot,
     & R_apot,R_epot,REAL(R_epabe,4),R_ipot,
     & L_ibit,L_arabe,L_orabe,L_erit,
     & L_irit,L_apabe,R_emet,R_esit,
     & REAL(R_upabe,4),REAL(R_umabe,4),L_opabe,
     & R_isit,I_oxet,R_oset,R_osit,R_opot,
     & REAL(R_efebe,4),REAL(R_ofebe,4),R_adot,R_uret,C20_atet
     &)
C FDA_70boats.fgi( 245, 264):���������� �������,BOAT64
      I_id=I_(4)
C FDA_70boats.fgi( 245, 264):������-�������: ���������� ��� �������������� ������,cpy BOAT64:CR
      Call LODOCHKA_HANDLER_FDA60_COORD(deltat,I_akim,R_elar
     &,
     & I_alar,R_ikor,I_ekim,I_ekor,I_ikim,
     & I_emir,R_opir,I_imir,
     & R_upir,I_omir,R_arir,
     & I_umir,R_erir,I_apir,
     & R_irir,I_epir,R_orir,
     & I_ipir,R_urir,I_exam,
     & R_olir,I_ilir,I_ixam,
     & R_amir,I_ulir,R_ador,
     & I_ubor,I_oxam,R_isir,
     & I_asir,I_uxam,R_osir,
     & I_esir,I_abem,R_idor,I_edor,
     & I_ebem,R_ofor,I_ifor,I_ibem,
     & R_udor,I_odor,I_obem,
     & R_akor,I_ufor,I_ubem,
     & R_efor,I_afor,I_adem,I_edem,
     & I_idem,I_odem,I_udem,I_afem,I_efem,I_ifem,
     & I_ofem,I_ufem,I_akem,I_ekem,I_ikem,I_okem,
     & I_ukem,I_alem,I_elem,I_ilem,I_olem,I_ulem,
     & I_amem,I_emem,I_imem,I_omem,I_umem,I_apem,
     & I_epem,I_ipem,I_opem,I_upem,I_arem,I_erem,
     & I_irem,I_orem,I_urem,I_asem,I_esem,I_isem,
     & I_osem,I_usem,I_atem,I_etem,I_item,I_otem,
     & I_utem,I_avem,I_evem,I_ivem,I_ovem,I_uvem,
     & I_axem,I_exem,I_ixem,I_oxem,I_uxem,I_abim,
     & I_ebim,I_ibim,I_obim,I_ubim,I_adim,I_edim,
     & I_idim,I_odim,I_udim,I_afim,I_efim,I_ifim,
     & I_ofim,I_ufim,INT(I_id,4),R_epar,R_apar,
     & R_umar,R_omar,R_imar,R_eper,
     & R_aper,R_umer,R_omer,R_imer,R_irer,
     & R_orer,R_urer,R_aser,R_eser,
     & R_irar,R_orar,R_urar,R_asar,
     & R_esar,R_iter,R_oter,R_uter,
     & R_aver,R_ever,R_itar,R_otar,
     & R_utar,R_avar,R_evar,R_obor,
     & R_ibor,R_ebor,R_abor,
     & R_uxir,R_ovir,R_ivir,
     & R_evir,R_avir,R_utir,
     & R_ixer,R_oxer,R_uxer,R_abir,R_ebir,
     & R_ixar,R_oxar,R_uxar,R_aber,
     & R_eber,R_efer,R_afer,R_uder,
     & R_oder,R_ider,R_efir,R_afir,
     & R_udir,R_odir,R_idir,R_ikir,R_okir,
     & R_ukir,R_alir,R_elir,R_iker,
     & R_oker,R_uker,R_aler,REAL(R_axet,4),
     & R_eler,L_ivam,L_evam,L_avam,L_ovam,L_uvam,L_axam,
     & L_etam,L_atam,L_usam,L_itam,L_otam,L_utam,I_emar,
     & I_amar,I_iler,I_oler,I_uler,I_amer,I_emer,
     & I_ilar,I_olar,I_ular,I_erar,I_arar,
     & I_iper,I_oper,I_uper,I_arer,I_erer,I_ipar,
     & I_opar,I_upar,I_etar,I_atar,I_iser,
     & I_oser,I_user,I_ater,I_eter,I_isar,I_osar,
     & I_usar,I_exar,I_axar,I_iver,I_over,
     & I_uver,I_axer,I_exer,I_ivar,I_ovar,
     & I_uvar,I_eder,I_ader,I_ibir,I_obir,
     & I_ubir,I_adir,I_edir,I_iber,I_ober,
     & I_uber,I_eker,I_aker,I_ifir,I_ofir,
     & I_ufir,I_akir,I_ekir,I_ifer,I_ofer,
     & I_ufer,I_usir,I_atir,I_etir,
     & I_itir,I_otir,64,I_uvir,
     & I_axir,I_exir,I_ixir,
     & I_oxir)
C FDA_70boats.fgi( 246, 250):���������� ���������� ������� �� ������� FDA60,BOAT64
      Call LODOCHKA_HANDLER(deltat,REAL(R_umebe,4),
     & L_olebe,L_alebe,
     & L_elebe,L_ulebe,L_ukebe,
     & L_ilebe,R_atut,R_orut,L_adav,L_emebe,
     & L_omabe,L_imabe,R_etut,R_urut,
     & L_edav,L_emabe,R_itut,R_asut,
     & L_idav,I_odav,I_udav,I_afav,I_efav,I_ifav,
     & INT(I_akebe,4),L_ekebe,L_ilut,
     & L_olut,L_ulut,L_amut,
     & L_emut,L_imut,L_omut,
     & L_umut,L_aput,L_eput,I_(5),I_afut,
     & R_ofav,REAL(R_afebe,4),R_ufav,
     & REAL(R_udebe,4),REAL(R_odebe,4),R_akav,R_ekav,
     & R_ikav,REAL(R_ikebe,4),R_okav,
     & REAL(R_okebe,4),R_isut,R_arut,L_ibav,L_esut,
     & L_imebe,REAL(R_omebe,4),R_udut,R_alav,
     & R_elav,R_ilav,REAL(R_uvabe,4),R_olav,
     & REAL(R_ivabe,4),REAL(R_utabe,4),R_ulav,
     & REAL(R_avabe,4),R_amav,R_osut,R_erut,
     & L_obav,R_emav,R_imav,R_omav,L_ikut,
     & R_umav,R_apav,REAL(R_erabe,4),R_epav,R_usut,
     & R_irut,L_ubav,R_ipav,R_opav,R_upav,R_arav,
     & R_erav,REAL(R_esabe,4),R_irav,L_okut,
     & L_otut,L_asabe,REAL(R_urabe,4),L_osabe,
     & R_orav,R_urav,R_asav,REAL(R_etabe,4),R_esav,
     & L_ukut,L_utut,L_atabe,
     & REAL(R_usabe,4),L_otabe,R_isav,R_osav,
     & R_usav,R_atav,REAL(R_epabe,4),R_etav,
     & L_ekut,L_arabe,L_orabe,L_avut,
     & L_evut,L_apabe,R_asot,R_axut,
     & REAL(R_upabe,4),REAL(R_umabe,4),L_opabe,
     & R_exut,I_ifut,R_ixot,R_ixut,R_itav,
     & REAL(R_efebe,4),REAL(R_ofebe,4),R_ukav,R_ovot,C20_uxot
     &)
C FDA_70boats.fgi( 225, 264):���������� �������,BOAT65
      I_ed=I_(5)
C FDA_70boats.fgi( 225, 264):������-�������: ���������� ��� �������������� ������,cpy BOAT65:CR
      Call LODOCHKA_HANDLER_FDA60_COORD(deltat,I_utom,R_elar
     &,
     & I_alar,R_ikor,I_avom,I_ekor,I_evom,
     & I_emir,R_opir,I_imir,
     & R_upir,I_omir,R_arir,
     & I_umir,R_erir,I_apir,
     & R_irir,I_epir,R_orir,
     & I_ipir,R_urir,I_apim,
     & R_olir,I_ilir,I_epim,
     & R_amir,I_ulir,R_ador,
     & I_ubor,I_ipim,R_isir,
     & I_asir,I_opim,R_osir,
     & I_esir,I_upim,R_idor,I_edor,
     & I_arim,R_ofor,I_ifor,I_erim,
     & R_udor,I_odor,I_irim,
     & R_akor,I_ufor,I_orim,
     & R_efor,I_afor,I_urim,I_asim,
     & I_esim,I_isim,I_osim,I_usim,I_atim,I_etim,
     & I_itim,I_otim,I_utim,I_avim,I_evim,I_ivim,
     & I_ovim,I_uvim,I_axim,I_exim,I_ixim,I_oxim,
     & I_uxim,I_abom,I_ebom,I_ibom,I_obom,I_ubom,
     & I_adom,I_edom,I_idom,I_odom,I_udom,I_afom,
     & I_efom,I_ifom,I_ofom,I_ufom,I_akom,I_ekom,
     & I_ikom,I_okom,I_ukom,I_alom,I_elom,I_ilom,
     & I_olom,I_ulom,I_amom,I_emom,I_imom,I_omom,
     & I_umom,I_apom,I_epom,I_ipom,I_opom,I_upom,
     & I_arom,I_erom,I_irom,I_orom,I_urom,I_asom,
     & I_esom,I_isom,I_osom,I_usom,I_atom,I_etom,
     & I_itom,I_otom,INT(I_ed,4),R_epar,R_apar,
     & R_umar,R_omar,R_imar,R_eper,
     & R_aper,R_umer,R_omer,R_imer,R_irer,
     & R_orer,R_urer,R_aser,R_eser,
     & R_irar,R_orar,R_urar,R_asar,
     & R_esar,R_iter,R_oter,R_uter,
     & R_aver,R_ever,R_itar,R_otar,
     & R_utar,R_avar,R_evar,R_obor,
     & R_ibor,R_ebor,R_abor,
     & R_uxir,R_ovir,R_ivir,
     & R_evir,R_avir,R_utir,
     & R_ixer,R_oxer,R_uxer,R_abir,R_ebir,
     & R_ixar,R_oxar,R_uxar,R_aber,
     & R_eber,R_efer,R_afer,R_uder,
     & R_oder,R_ider,R_efir,R_afir,
     & R_udir,R_odir,R_idir,R_ikir,R_okir,
     & R_ukir,R_alir,R_elir,R_iker,
     & R_oker,R_uker,R_aler,REAL(R_udut,4),
     & R_eler,L_emim,L_amim,L_ulim,L_imim,L_omim,L_umim,
     & L_alim,L_ukim,L_okim,L_elim,L_ilim,L_olim,I_emar,
     & I_amar,I_iler,I_oler,I_uler,I_amer,I_emer,
     & I_ilar,I_olar,I_ular,I_erar,I_arar,
     & I_iper,I_oper,I_uper,I_arer,I_erer,I_ipar,
     & I_opar,I_upar,I_etar,I_atar,I_iser,
     & I_oser,I_user,I_ater,I_eter,I_isar,I_osar,
     & I_usar,I_exar,I_axar,I_iver,I_over,
     & I_uver,I_axer,I_exer,I_ivar,I_ovar,
     & I_uvar,I_eder,I_ader,I_ibir,I_obir,
     & I_ubir,I_adir,I_edir,I_iber,I_ober,
     & I_uber,I_eker,I_aker,I_ifir,I_ofir,
     & I_ufir,I_akir,I_ekir,I_ifer,I_ofer,
     & I_ufer,I_usir,I_atir,I_etir,
     & I_itir,I_otir,65,I_uvir,
     & I_axir,I_exir,I_ixir,
     & I_oxir)
C FDA_70boats.fgi( 226, 250):���������� ���������� ������� �� ������� FDA60,BOAT65
      Call LODOCHKA_HANDLER(deltat,REAL(R_umebe,4),
     & L_olebe,L_alebe,
     & L_elebe,L_ulebe,L_ukebe,
     & L_ilebe,R_uxev,R_ivev,L_ukiv,L_emebe,
     & L_omabe,L_imabe,R_abiv,R_ovev,
     & L_aliv,L_emabe,R_ebiv,R_uvev,
     & L_eliv,I_iliv,I_oliv,I_uliv,I_amiv,I_emiv,
     & INT(I_akebe,4),L_ekebe,L_erev,
     & L_irev,L_orev,L_urev,
     & L_asev,L_esev,L_isev,
     & L_osev,L_usev,L_atev,I_(6),I_ulev,
     & R_imiv,REAL(R_afebe,4),R_omiv,
     & REAL(R_udebe,4),REAL(R_odebe,4),R_umiv,R_apiv,
     & R_epiv,REAL(R_ikebe,4),R_ipiv,
     & REAL(R_okebe,4),R_exev,R_utev,L_ekiv,L_axev,
     & L_imebe,REAL(R_omebe,4),R_olev,R_upiv,
     & R_ariv,R_eriv,REAL(R_uvabe,4),R_iriv,
     & REAL(R_ivabe,4),REAL(R_utabe,4),R_oriv,
     & REAL(R_avabe,4),R_uriv,R_ixev,R_avev,
     & L_ikiv,R_asiv,R_esiv,R_isiv,L_epev,
     & R_osiv,R_usiv,REAL(R_erabe,4),R_ativ,R_oxev,
     & R_evev,L_okiv,R_etiv,R_itiv,R_otiv,R_utiv,
     & R_aviv,REAL(R_esabe,4),R_eviv,L_ipev,
     & L_ibiv,L_asabe,REAL(R_urabe,4),L_osabe,
     & R_iviv,R_oviv,R_uviv,REAL(R_etabe,4),R_axiv,
     & L_opev,L_obiv,L_atabe,
     & REAL(R_usabe,4),L_otabe,R_exiv,R_ixiv,
     & R_oxiv,R_uxiv,REAL(R_epabe,4),R_abov,
     & L_apev,L_arabe,L_orabe,L_ubiv,
     & L_adiv,L_apabe,R_uvav,R_udiv,
     & REAL(R_upabe,4),REAL(R_umabe,4),L_opabe,
     & R_afiv,I_emev,R_efev,R_efiv,R_ebov,
     & REAL(R_efebe,4),REAL(R_ofebe,4),R_opiv,R_idev,C20_ofev
     &)
C FDA_70boats.fgi( 205, 264):���������� �������,BOAT66
      I_ad=I_(6)
C FDA_70boats.fgi( 205, 264):������-�������: ���������� ��� �������������� ������,cpy BOAT66:CR
      Call LODOCHKA_HANDLER_FDA60_COORD(deltat,I_olap,R_elar
     &,
     & I_alar,R_ikor,I_ulap,I_ekor,I_amap,
     & I_emir,R_opir,I_imir,
     & R_upir,I_omir,R_arir,
     & I_umir,R_erir,I_apir,
     & R_irir,I_epir,R_orir,
     & I_ipir,R_urir,I_ubum,
     & R_olir,I_ilir,I_adum,
     & R_amir,I_ulir,R_ador,
     & I_ubor,I_edum,R_isir,
     & I_asir,I_idum,R_osir,
     & I_esir,I_odum,R_idor,I_edor,
     & I_udum,R_ofor,I_ifor,I_afum,
     & R_udor,I_odor,I_efum,
     & R_akor,I_ufor,I_ifum,
     & R_efor,I_afor,I_ofum,I_ufum,
     & I_akum,I_ekum,I_ikum,I_okum,I_ukum,I_alum,
     & I_elum,I_ilum,I_olum,I_ulum,I_amum,I_emum,
     & I_imum,I_omum,I_umum,I_apum,I_epum,I_ipum,
     & I_opum,I_upum,I_arum,I_erum,I_irum,I_orum,
     & I_urum,I_asum,I_esum,I_isum,I_osum,I_usum,
     & I_atum,I_etum,I_itum,I_otum,I_utum,I_avum,
     & I_evum,I_ivum,I_ovum,I_uvum,I_axum,I_exum,
     & I_ixum,I_oxum,I_uxum,I_abap,I_ebap,I_ibap,
     & I_obap,I_ubap,I_adap,I_edap,I_idap,I_odap,
     & I_udap,I_afap,I_efap,I_ifap,I_ofap,I_ufap,
     & I_akap,I_ekap,I_ikap,I_okap,I_ukap,I_alap,
     & I_elap,I_ilap,INT(I_ad,4),R_epar,R_apar,
     & R_umar,R_omar,R_imar,R_eper,
     & R_aper,R_umer,R_omer,R_imer,R_irer,
     & R_orer,R_urer,R_aser,R_eser,
     & R_irar,R_orar,R_urar,R_asar,
     & R_esar,R_iter,R_oter,R_uter,
     & R_aver,R_ever,R_itar,R_otar,
     & R_utar,R_avar,R_evar,R_obor,
     & R_ibor,R_ebor,R_abor,
     & R_uxir,R_ovir,R_ivir,
     & R_evir,R_avir,R_utir,
     & R_ixer,R_oxer,R_uxer,R_abir,R_ebir,
     & R_ixar,R_oxar,R_uxar,R_aber,
     & R_eber,R_efer,R_afer,R_uder,
     & R_oder,R_ider,R_efir,R_afir,
     & R_udir,R_odir,R_idir,R_ikir,R_okir,
     & R_ukir,R_alir,R_elir,R_iker,
     & R_oker,R_uker,R_aler,REAL(R_olev,4),
     & R_eler,L_abum,L_uxom,L_oxom,L_ebum,L_ibum,L_obum,
     & L_uvom,L_ovom,L_ivom,L_axom,L_exom,L_ixom,I_emar,
     & I_amar,I_iler,I_oler,I_uler,I_amer,I_emer,
     & I_ilar,I_olar,I_ular,I_erar,I_arar,
     & I_iper,I_oper,I_uper,I_arer,I_erer,I_ipar,
     & I_opar,I_upar,I_etar,I_atar,I_iser,
     & I_oser,I_user,I_ater,I_eter,I_isar,I_osar,
     & I_usar,I_exar,I_axar,I_iver,I_over,
     & I_uver,I_axer,I_exer,I_ivar,I_ovar,
     & I_uvar,I_eder,I_ader,I_ibir,I_obir,
     & I_ubir,I_adir,I_edir,I_iber,I_ober,
     & I_uber,I_eker,I_aker,I_ifir,I_ofir,
     & I_ufir,I_akir,I_ekir,I_ifer,I_ofer,
     & I_ufer,I_usir,I_atir,I_etir,
     & I_itir,I_otir,66,I_uvir,
     & I_axir,I_exir,I_ixir,
     & I_oxir)
C FDA_70boats.fgi( 206, 250):���������� ���������� ������� �� ������� FDA60,BOAT66
      Call LODOCHKA_HANDLER(deltat,REAL(R_umebe,4),
     & L_olebe,L_alebe,
     & L_elebe,L_ulebe,L_ukebe,
     & L_ilebe,R_ofuv,R_eduv,L_opuv,L_emebe,
     & L_omabe,L_imabe,R_ufuv,R_iduv,
     & L_upuv,L_emabe,R_akuv,R_oduv,
     & L_aruv,I_eruv,I_iruv,I_oruv,I_uruv,I_asuv,
     & INT(I_akebe,4),L_ekebe,L_avov,
     & L_evov,L_ivov,L_ovov,
     & L_uvov,L_axov,L_exov,
     & L_ixov,L_oxov,L_uxov,I_(7),I_orov,
     & R_esuv,REAL(R_afebe,4),R_isuv,
     & REAL(R_udebe,4),REAL(R_odebe,4),R_osuv,R_usuv,
     & R_atuv,REAL(R_ikebe,4),R_etuv,
     & REAL(R_okebe,4),R_afuv,R_obuv,L_apuv,L_uduv,
     & L_imebe,REAL(R_omebe,4),R_irov,R_otuv,
     & R_utuv,R_avuv,REAL(R_uvabe,4),R_evuv,
     & REAL(R_ivabe,4),REAL(R_utabe,4),R_ivuv,
     & REAL(R_avabe,4),R_ovuv,R_efuv,R_ubuv,
     & L_epuv,R_uvuv,R_axuv,R_exuv,L_atov,
     & R_ixuv,R_oxuv,REAL(R_erabe,4),R_uxuv,R_ifuv,
     & R_aduv,L_ipuv,R_abax,R_ebax,R_ibax,R_obax,
     & R_ubax,REAL(R_esabe,4),R_adax,L_etov,
     & L_ekuv,L_asabe,REAL(R_urabe,4),L_osabe,
     & R_edax,R_idax,R_odax,REAL(R_etabe,4),R_udax,
     & L_itov,L_ikuv,L_atabe,
     & REAL(R_usabe,4),L_otabe,R_afax,R_efax,
     & R_ifax,R_ofax,REAL(R_epabe,4),R_ufax,
     & L_usov,L_arabe,L_orabe,L_okuv,
     & L_ukuv,L_apabe,R_odov,R_oluv,
     & REAL(R_upabe,4),REAL(R_umabe,4),L_opabe,
     & R_uluv,I_asov,R_amov,R_amuv,R_akax,
     & REAL(R_efebe,4),REAL(R_ofebe,4),R_ituv,R_elov,C20_imov
     &)
C FDA_70boats.fgi( 185, 264):���������� �������,BOAT67
      I_u=I_(7)
C FDA_70boats.fgi( 185, 264):������-�������: ���������� ��� �������������� ������,cpy BOAT67:CR
      Call LODOCHKA_HANDLER_FDA60_COORD(deltat,I_ixep,R_elar
     &,
     & I_alar,R_ikor,I_oxep,I_ekor,I_uxep,
     & I_emir,R_opir,I_imir,
     & R_upir,I_omir,R_arir,
     & I_umir,R_erir,I_apir,
     & R_irir,I_epir,R_orir,
     & I_ipir,R_urir,I_orap,
     & R_olir,I_ilir,I_urap,
     & R_amir,I_ulir,R_ador,
     & I_ubor,I_asap,R_isir,
     & I_asir,I_esap,R_osir,
     & I_esir,I_isap,R_idor,I_edor,
     & I_osap,R_ofor,I_ifor,I_usap,
     & R_udor,I_odor,I_atap,
     & R_akor,I_ufor,I_etap,
     & R_efor,I_afor,I_itap,I_otap,
     & I_utap,I_avap,I_evap,I_ivap,I_ovap,I_uvap,
     & I_axap,I_exap,I_ixap,I_oxap,I_uxap,I_abep,
     & I_ebep,I_ibep,I_obep,I_ubep,I_adep,I_edep,
     & I_idep,I_odep,I_udep,I_afep,I_efep,I_ifep,
     & I_ofep,I_ufep,I_akep,I_ekep,I_ikep,I_okep,
     & I_ukep,I_alep,I_elep,I_ilep,I_olep,I_ulep,
     & I_amep,I_emep,I_imep,I_omep,I_umep,I_apep,
     & I_epep,I_ipep,I_opep,I_upep,I_arep,I_erep,
     & I_irep,I_orep,I_urep,I_asep,I_esep,I_isep,
     & I_osep,I_usep,I_atep,I_etep,I_itep,I_otep,
     & I_utep,I_avep,I_evep,I_ivep,I_ovep,I_uvep,
     & I_axep,I_exep,INT(I_u,4),R_epar,R_apar,
     & R_umar,R_omar,R_imar,R_eper,
     & R_aper,R_umer,R_omer,R_imer,R_irer,
     & R_orer,R_urer,R_aser,R_eser,
     & R_irar,R_orar,R_urar,R_asar,
     & R_esar,R_iter,R_oter,R_uter,
     & R_aver,R_ever,R_itar,R_otar,
     & R_utar,R_avar,R_evar,R_obor,
     & R_ibor,R_ebor,R_abor,
     & R_uxir,R_ovir,R_ivir,
     & R_evir,R_avir,R_utir,
     & R_ixer,R_oxer,R_uxer,R_abir,R_ebir,
     & R_ixar,R_oxar,R_uxar,R_aber,
     & R_eber,R_efer,R_afer,R_uder,
     & R_oder,R_ider,R_efir,R_afir,
     & R_udir,R_odir,R_idir,R_ikir,R_okir,
     & R_ukir,R_alir,R_elir,R_iker,
     & R_oker,R_uker,R_aler,REAL(R_irov,4),
     & R_eler,L_upap,L_opap,L_ipap,L_arap,L_erap,L_irap,
     & L_omap,L_imap,L_emap,L_umap,L_apap,L_epap,I_emar,
     & I_amar,I_iler,I_oler,I_uler,I_amer,I_emer,
     & I_ilar,I_olar,I_ular,I_erar,I_arar,
     & I_iper,I_oper,I_uper,I_arer,I_erer,I_ipar,
     & I_opar,I_upar,I_etar,I_atar,I_iser,
     & I_oser,I_user,I_ater,I_eter,I_isar,I_osar,
     & I_usar,I_exar,I_axar,I_iver,I_over,
     & I_uver,I_axer,I_exer,I_ivar,I_ovar,
     & I_uvar,I_eder,I_ader,I_ibir,I_obir,
     & I_ubir,I_adir,I_edir,I_iber,I_ober,
     & I_uber,I_eker,I_aker,I_ifir,I_ofir,
     & I_ufir,I_akir,I_ekir,I_ifer,I_ofer,
     & I_ufer,I_usir,I_atir,I_etir,
     & I_itir,I_otir,67,I_uvir,
     & I_axir,I_exir,I_ixir,
     & I_oxir)
C FDA_70boats.fgi( 186, 250):���������� ���������� ������� �� ������� FDA60,BOAT67
      Call LODOCHKA_HANDLER(deltat,REAL(R_umebe,4),
     & L_olebe,L_alebe,
     & L_elebe,L_ulebe,L_ukebe,
     & L_ilebe,R_imex,R_alex,L_itex,L_emebe,
     & L_omabe,L_imabe,R_omex,R_elex,
     & L_otex,L_emabe,R_umex,R_ilex,
     & L_utex,I_avex,I_evex,I_ivex,I_ovex,I_uvex,
     & INT(I_akebe,4),L_ekebe,L_ubex,
     & L_adex,L_edex,L_idex,
     & L_odex,L_udex,L_afex,
     & L_efex,L_ifex,L_ofex,I_(8),I_ivax,
     & R_axex,REAL(R_afebe,4),R_exex,
     & REAL(R_udebe,4),REAL(R_odebe,4),R_ixex,R_oxex,
     & R_uxex,REAL(R_ikebe,4),R_abix,
     & REAL(R_okebe,4),R_ulex,R_ikex,L_usex,L_olex,
     & L_imebe,REAL(R_omebe,4),R_evax,R_ibix,
     & R_obix,R_ubix,REAL(R_uvabe,4),R_adix,
     & REAL(R_ivabe,4),REAL(R_utabe,4),R_edix,
     & REAL(R_avabe,4),R_idix,R_amex,R_okex,
     & L_atex,R_odix,R_udix,R_afix,L_uxax,
     & R_efix,R_ifix,REAL(R_erabe,4),R_ofix,R_emex,
     & R_ukex,L_etex,R_ufix,R_akix,R_ekix,R_ikix,
     & R_okix,REAL(R_esabe,4),R_ukix,L_abex,
     & L_apex,L_asabe,REAL(R_urabe,4),L_osabe,
     & R_alix,R_elix,R_ilix,REAL(R_etabe,4),R_olix,
     & L_ebex,L_epex,L_atabe,
     & REAL(R_usabe,4),L_otabe,R_ulix,R_amix,
     & R_emix,R_imix,REAL(R_epabe,4),R_omix,
     & L_oxax,L_arabe,L_orabe,L_ipex,
     & L_opex,L_apabe,R_ilax,R_irex,
     & REAL(R_upabe,4),REAL(R_umabe,4),L_opabe,
     & R_orex,I_uvax,R_urax,R_urex,R_umix,
     & REAL(R_efebe,4),REAL(R_ofebe,4),R_ebix,R_arax,C20_esax
     &)
C FDA_70boats.fgi( 165, 264):���������� �������,BOAT68
      I_o=I_(8)
C FDA_70boats.fgi( 165, 264):������-�������: ���������� ��� �������������� ������,cpy BOAT68:CR
      Call LODOCHKA_HANDLER_FDA60_COORD(deltat,I_epop,R_elar
     &,
     & I_alar,R_ikor,I_ipop,I_ekor,I_opop,
     & I_emir,R_opir,I_imir,
     & R_upir,I_omir,R_arir,
     & I_umir,R_erir,I_apir,
     & R_irir,I_epir,R_orir,
     & I_ipir,R_urir,I_ifip,
     & R_olir,I_ilir,I_ofip,
     & R_amir,I_ulir,R_ador,
     & I_ubor,I_ufip,R_isir,
     & I_asir,I_akip,R_osir,
     & I_esir,I_ekip,R_idor,I_edor,
     & I_ikip,R_ofor,I_ifor,I_okip,
     & R_udor,I_odor,I_ukip,
     & R_akor,I_ufor,I_alip,
     & R_efor,I_afor,I_elip,I_ilip,
     & I_olip,I_ulip,I_amip,I_emip,I_imip,I_omip,
     & I_umip,I_apip,I_epip,I_ipip,I_opip,I_upip,
     & I_arip,I_erip,I_irip,I_orip,I_urip,I_asip,
     & I_esip,I_isip,I_osip,I_usip,I_atip,I_etip,
     & I_itip,I_otip,I_utip,I_avip,I_evip,I_ivip,
     & I_ovip,I_uvip,I_axip,I_exip,I_ixip,I_oxip,
     & I_uxip,I_abop,I_ebop,I_ibop,I_obop,I_ubop,
     & I_adop,I_edop,I_idop,I_odop,I_udop,I_afop,
     & I_efop,I_ifop,I_ofop,I_ufop,I_akop,I_ekop,
     & I_ikop,I_okop,I_ukop,I_alop,I_elop,I_ilop,
     & I_olop,I_ulop,I_amop,I_emop,I_imop,I_omop,
     & I_umop,I_apop,INT(I_o,4),R_epar,R_apar,
     & R_umar,R_omar,R_imar,R_eper,
     & R_aper,R_umer,R_omer,R_imer,R_irer,
     & R_orer,R_urer,R_aser,R_eser,
     & R_irar,R_orar,R_urar,R_asar,
     & R_esar,R_iter,R_oter,R_uter,
     & R_aver,R_ever,R_itar,R_otar,
     & R_utar,R_avar,R_evar,R_obor,
     & R_ibor,R_ebor,R_abor,
     & R_uxir,R_ovir,R_ivir,
     & R_evir,R_avir,R_utir,
     & R_ixer,R_oxer,R_uxer,R_abir,R_ebir,
     & R_ixar,R_oxar,R_uxar,R_aber,
     & R_eber,R_efer,R_afer,R_uder,
     & R_oder,R_ider,R_efir,R_afir,
     & R_udir,R_odir,R_idir,R_ikir,R_okir,
     & R_ukir,R_alir,R_elir,R_iker,
     & R_oker,R_uker,R_aler,REAL(R_evax,4),
     & R_eler,L_odip,L_idip,L_edip,L_udip,L_afip,L_efip,
     & L_ibip,L_ebip,L_abip,L_obip,L_ubip,L_adip,I_emar,
     & I_amar,I_iler,I_oler,I_uler,I_amer,I_emer,
     & I_ilar,I_olar,I_ular,I_erar,I_arar,
     & I_iper,I_oper,I_uper,I_arer,I_erer,I_ipar,
     & I_opar,I_upar,I_etar,I_atar,I_iser,
     & I_oser,I_user,I_ater,I_eter,I_isar,I_osar,
     & I_usar,I_exar,I_axar,I_iver,I_over,
     & I_uver,I_axer,I_exer,I_ivar,I_ovar,
     & I_uvar,I_eder,I_ader,I_ibir,I_obir,
     & I_ubir,I_adir,I_edir,I_iber,I_ober,
     & I_uber,I_eker,I_aker,I_ifir,I_ofir,
     & I_ufir,I_akir,I_ekir,I_ifer,I_ofer,
     & I_ufer,I_usir,I_atir,I_etir,
     & I_itir,I_otir,68,I_uvir,
     & I_axir,I_exir,I_ixir,
     & I_oxir)
C FDA_70boats.fgi( 166, 250):���������� ���������� ������� �� ������� FDA60,BOAT68
      Call LODOCHKA_HANDLER(deltat,REAL(R_umebe,4),
     & L_olebe,L_alebe,
     & L_elebe,L_ulebe,L_ukebe,
     & L_ilebe,R_esox,R_upox,L_ebux,L_emebe,
     & L_omabe,L_imabe,R_isox,R_arox,
     & L_ibux,L_emabe,R_osox,R_erox,
     & L_obux,I_ubux,I_adux,I_edux,I_idux,I_odux,
     & INT(I_akebe,4),L_ekebe,L_okox,
     & L_ukox,L_alox,L_elox,
     & L_ilox,L_olox,L_ulox,
     & L_amox,L_emox,L_imox,I_(9),I_edox,
     & R_udux,REAL(R_afebe,4),R_afux,
     & REAL(R_udebe,4),REAL(R_odebe,4),R_efux,R_ifux,
     & R_ofux,REAL(R_ikebe,4),R_ufux,
     & REAL(R_okebe,4),R_orox,R_epox,L_oxox,L_irox,
     & L_imebe,REAL(R_omebe,4),R_adox,R_ekux,
     & R_ikux,R_okux,REAL(R_uvabe,4),R_ukux,
     & REAL(R_ivabe,4),REAL(R_utabe,4),R_alux,
     & REAL(R_avabe,4),R_elux,R_urox,R_ipox,
     & L_uxox,R_ilux,R_olux,R_ulux,L_ofox,
     & R_amux,R_emux,REAL(R_erabe,4),R_imux,R_asox,
     & R_opox,L_abux,R_omux,R_umux,R_apux,R_epux,
     & R_ipux,REAL(R_esabe,4),R_opux,L_ufox,
     & L_usox,L_asabe,REAL(R_urabe,4),L_osabe,
     & R_upux,R_arux,R_erux,REAL(R_etabe,4),R_irux,
     & L_akox,L_atox,L_atabe,
     & REAL(R_usabe,4),L_otabe,R_orux,R_urux,
     & R_asux,R_esux,REAL(R_epabe,4),R_isux,
     & L_ifox,L_arabe,L_orabe,L_etox,
     & L_itox,L_apabe,R_erix,R_evox,
     & REAL(R_upabe,4),REAL(R_umabe,4),L_opabe,
     & R_ivox,I_odox,R_ovix,R_ovox,R_osux,
     & REAL(R_efebe,4),REAL(R_ofebe,4),R_akux,R_utix,C20_axix
     &)
C FDA_70boats.fgi( 145, 264):���������� �������,BOAT69
      I_i=I_(9)
C FDA_70boats.fgi( 145, 264):������-�������: ���������� ��� �������������� ������,cpy BOAT69:CR
      Call LODOCHKA_HANDLER_FDA60_COORD(deltat,I_adar,R_elar
     &,
     & I_alar,R_ikor,I_edar,I_ekor,I_idar,
     & I_emir,R_opir,I_imir,
     & R_upir,I_omir,R_arir,
     & I_umir,R_erir,I_apir,
     & R_irir,I_epir,R_orir,
     & I_ipir,R_urir,I_etop,
     & R_olir,I_ilir,I_itop,
     & R_amir,I_ulir,R_ador,
     & I_ubor,I_otop,R_isir,
     & I_asir,I_utop,R_osir,
     & I_esir,I_avop,R_idor,I_edor,
     & I_evop,R_ofor,I_ifor,I_ivop,
     & R_udor,I_odor,I_ovop,
     & R_akor,I_ufor,I_uvop,
     & R_efor,I_afor,I_axop,I_exop,
     & I_ixop,I_oxop,I_uxop,I_abup,I_ebup,I_ibup,
     & I_obup,I_ubup,I_adup,I_edup,I_idup,I_odup,
     & I_udup,I_afup,I_efup,I_ifup,I_ofup,I_ufup,
     & I_akup,I_ekup,I_ikup,I_okup,I_ukup,I_alup,
     & I_elup,I_ilup,I_olup,I_ulup,I_amup,I_emup,
     & I_imup,I_omup,I_umup,I_apup,I_epup,I_ipup,
     & I_opup,I_upup,I_arup,I_erup,I_irup,I_orup,
     & I_urup,I_asup,I_esup,I_isup,I_osup,I_usup,
     & I_atup,I_etup,I_itup,I_otup,I_utup,I_avup,
     & I_evup,I_ivup,I_ovup,I_uvup,I_axup,I_exup,
     & I_ixup,I_oxup,I_uxup,I_abar,I_ebar,I_ibar,
     & I_obar,I_ubar,INT(I_i,4),R_epar,R_apar,
     & R_umar,R_omar,R_imar,R_eper,
     & R_aper,R_umer,R_omer,R_imer,R_irer,
     & R_orer,R_urer,R_aser,R_eser,
     & R_irar,R_orar,R_urar,R_asar,
     & R_esar,R_iter,R_oter,R_uter,
     & R_aver,R_ever,R_itar,R_otar,
     & R_utar,R_avar,R_evar,R_obor,
     & R_ibor,R_ebor,R_abor,
     & R_uxir,R_ovir,R_ivir,
     & R_evir,R_avir,R_utir,
     & R_ixer,R_oxer,R_uxer,R_abir,R_ebir,
     & R_ixar,R_oxar,R_uxar,R_aber,
     & R_eber,R_efer,R_afer,R_uder,
     & R_oder,R_ider,R_efir,R_afir,
     & R_udir,R_odir,R_idir,R_ikir,R_okir,
     & R_ukir,R_alir,R_elir,R_iker,
     & R_oker,R_uker,R_aler,REAL(R_adox,4),
     & R_eler,L_isop,L_esop,L_asop,L_osop,L_usop,L_atop,
     & L_erop,L_arop,L_upop,L_irop,L_orop,L_urop,I_emar,
     & I_amar,I_iler,I_oler,I_uler,I_amer,I_emer,
     & I_ilar,I_olar,I_ular,I_erar,I_arar,
     & I_iper,I_oper,I_uper,I_arer,I_erer,I_ipar,
     & I_opar,I_upar,I_etar,I_atar,I_iser,
     & I_oser,I_user,I_ater,I_eter,I_isar,I_osar,
     & I_usar,I_exar,I_axar,I_iver,I_over,
     & I_uver,I_axer,I_exer,I_ivar,I_ovar,
     & I_uvar,I_eder,I_ader,I_ibir,I_obir,
     & I_ubir,I_adir,I_edir,I_iber,I_ober,
     & I_uber,I_eker,I_aker,I_ifir,I_ofir,
     & I_ufir,I_akir,I_ekir,I_ifer,I_ofer,
     & I_ufer,I_usir,I_atir,I_etir,
     & I_itir,I_otir,69,I_uvir,
     & I_axir,I_exir,I_ixir,
     & I_oxir)
C FDA_70boats.fgi( 146, 250):���������� ���������� ������� �� ������� FDA60,BOAT69
      Call LODOCHKA_HANDLER(deltat,REAL(R_umebe,4),
     & L_olebe,L_alebe,
     & L_elebe,L_ulebe,L_ukebe,
     & L_ilebe,R_osebe,R_erebe,L_obibe,L_emebe,
     & L_omabe,L_imabe,R_usebe,R_irebe,
     & L_ubibe,L_emabe,R_atebe,R_orebe,
     & L_adibe,I_edibe,I_idibe,I_odibe,I_udibe,I_afibe,
     & INT(I_akebe,4),L_ekebe,L_ixabe,
     & L_oxabe,L_uxabe,L_abebe,
     & L_ebebe,L_ibebe,L_obebe,
     & L_ubebe,L_adebe,L_edebe,I_(10),I_alabe,
     & R_efibe,REAL(R_afebe,4),R_ifibe,
     & REAL(R_udebe,4),REAL(R_odebe,4),R_ofibe,R_ufibe,
     & R_akibe,REAL(R_ikebe,4),R_ekibe,
     & REAL(R_okebe,4),R_asebe,R_opebe,L_abibe,L_urebe,
     & L_imebe,REAL(R_omebe,4),R_ukabe,R_okibe,
     & R_ukibe,R_alibe,REAL(R_uvabe,4),R_elibe,
     & REAL(R_ivabe,4),REAL(R_utabe,4),R_ilibe,
     & REAL(R_avabe,4),R_olibe,R_esebe,R_upebe,
     & L_ebibe,R_ulibe,R_amibe,R_emibe,L_irabe,
     & R_imibe,R_omibe,REAL(R_erabe,4),R_umibe,R_isebe,
     & R_arebe,L_ibibe,R_apibe,R_epibe,R_ipibe,R_opibe,
     & R_upibe,REAL(R_esabe,4),R_aribe,L_isabe,
     & L_etebe,L_asabe,REAL(R_urabe,4),L_osabe,
     & R_eribe,R_iribe,R_oribe,REAL(R_etabe,4),R_uribe,
     & L_itabe,L_itebe,L_atabe,
     & REAL(R_usabe,4),L_otabe,R_asibe,R_esibe,
     & R_isibe,R_osibe,REAL(R_epabe,4),R_usibe,
     & L_ipabe,L_arabe,L_orabe,L_otebe,
     & L_utebe,L_apabe,R_avux,R_ovebe,
     & REAL(R_upabe,4),REAL(R_umabe,4),L_opabe,
     & R_uvebe,I_ilabe,R_idabe,R_axebe,R_atibe,
     & REAL(R_efebe,4),REAL(R_ofebe,4),R_ikibe,R_obabe,C20_udabe
     &)
C FDA_70boats.fgi( 125, 264):���������� �������,BOAT70
      I_e=I_(10)
C FDA_70boats.fgi( 125, 264):������-�������: ���������� ��� �������������� ������,cpy BOAT70:CR
      Call LODOCHKA_HANDLER_FDA60_COORD(deltat,I_irur,R_elar
     &,
     & I_alar,R_ikor,I_orur,I_ekor,I_urur,
     & I_emir,R_opir,I_imir,
     & R_upir,I_omir,R_arir,
     & I_umir,R_erir,I_apir,
     & R_irir,I_epir,R_orir,
     & I_ipir,R_urir,I_okor,
     & R_olir,I_ilir,I_ukor,
     & R_amir,I_ulir,R_ador,
     & I_ubor,I_alor,R_isir,
     & I_asir,I_elor,R_osir,
     & I_esir,I_ilor,R_idor,I_edor,
     & I_olor,R_ofor,I_ifor,I_ulor,
     & R_udor,I_odor,I_amor,
     & R_akor,I_ufor,I_emor,
     & R_efor,I_afor,I_imor,I_omor,
     & I_umor,I_apor,I_epor,I_ipor,I_opor,I_upor,
     & I_aror,I_eror,I_iror,I_oror,I_uror,I_asor,
     & I_esor,I_isor,I_osor,I_usor,I_ator,I_etor,
     & I_itor,I_otor,I_utor,I_avor,I_evor,I_ivor,
     & I_ovor,I_uvor,I_axor,I_exor,I_ixor,I_oxor,
     & I_uxor,I_abur,I_ebur,I_ibur,I_obur,I_ubur,
     & I_adur,I_edur,I_idur,I_odur,I_udur,I_afur,
     & I_efur,I_ifur,I_ofur,I_ufur,I_akur,I_ekur,
     & I_ikur,I_okur,I_ukur,I_alur,I_elur,I_ilur,
     & I_olur,I_ulur,I_amur,I_emur,I_imur,I_omur,
     & I_umur,I_apur,I_epur,I_ipur,I_opur,I_upur,
     & I_arur,I_erur,INT(I_e,4),R_epar,R_apar,
     & R_umar,R_omar,R_imar,R_eper,
     & R_aper,R_umer,R_omer,R_imer,R_irer,
     & R_orer,R_urer,R_aser,R_eser,
     & R_irar,R_orar,R_urar,R_asar,
     & R_esar,R_iter,R_oter,R_uter,
     & R_aver,R_ever,R_itar,R_otar,
     & R_utar,R_avar,R_evar,R_obor,
     & R_ibor,R_ebor,R_abor,
     & R_uxir,R_ovir,R_ivir,
     & R_evir,R_avir,R_utir,
     & R_ixer,R_oxer,R_uxer,R_abir,R_ebir,
     & R_ixar,R_oxar,R_uxar,R_aber,
     & R_eber,R_efer,R_afer,R_uder,
     & R_oder,R_ider,R_efir,R_afir,
     & R_udir,R_odir,R_idir,R_ikir,R_okir,
     & R_ukir,R_alir,R_elir,R_iker,
     & R_oker,R_uker,R_aler,REAL(R_ukabe,4),
     & R_eler,L_ekar,L_akar,L_ufar,L_ikar,L_okar,L_ukar,
     & L_afar,L_udar,L_odar,L_efar,L_ifar,L_ofar,I_emar,
     & I_amar,I_iler,I_oler,I_uler,I_amer,I_emer,
     & I_ilar,I_olar,I_ular,I_erar,I_arar,
     & I_iper,I_oper,I_uper,I_arer,I_erer,I_ipar,
     & I_opar,I_upar,I_etar,I_atar,I_iser,
     & I_oser,I_user,I_ater,I_eter,I_isar,I_osar,
     & I_usar,I_exar,I_axar,I_iver,I_over,
     & I_uver,I_axer,I_exer,I_ivar,I_ovar,
     & I_uvar,I_eder,I_ader,I_ibir,I_obir,
     & I_ubir,I_adir,I_edir,I_iber,I_ober,
     & I_uber,I_eker,I_aker,I_ifir,I_ofir,
     & I_ufir,I_akir,I_ekir,I_ifer,I_ofer,
     & I_ufer,I_usir,I_atir,I_etir,
     & I_itir,I_otir,70,I_uvir,
     & I_axir,I_exir,I_ixir,
     & I_oxir)
C FDA_70boats.fgi( 126, 250):���������� ���������� ������� �� ������� FDA60,BOAT70
      End
