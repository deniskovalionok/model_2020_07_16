      Subroutine CONTROL_FDD(ext_deltat)
      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      Include 'CONTROL_FDD.fh'

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      R_(34)=R0_exel
C control_tg.fgi(  41, 113):pre: ���������������� ����� 
      R_(60)=R0_uxom
C control_th.fgi(  85, 215):pre: ���������������� ����� 
      R_aref=0
      R_apef=0
      R_amef=0
      R_alef=0
      R_efef=0
      R_edef=0
      R_ebef=0
      R_exaf=0
      R_evaf=0
      R_otaf=0
      R_ataf=0
      R_isaf=0
      R_uraf=0
      R_eraf=0
      R_opaf=0
C FDD_logic.fgi(  49, 277):pre: ���������� ���,TVS1
      R_aref=0
      R_apef=0
      R_amef=0
      R_alef=0
      R_efef=0
      R_edef=0
      R_ebef=0
      R_exaf=0
      R_evaf=0
      R_otaf=0
      R_ataf=0
      R_isaf=0
      R_uraf=0
      R_eraf=0
      R_opaf=0
C FDD_logic.fgi(  55, 277):pre: ���������� ���,TVS2
      R_aref=0
      R_apef=0
      R_amef=0
      R_alef=0
      R_efef=0
      R_edef=0
      R_ebef=0
      R_exaf=0
      R_evaf=0
      R_otaf=0
      R_ataf=0
      R_isaf=0
      R_uraf=0
      R_eraf=0
      R_opaf=0
C FDD_logic.fgi(  61, 277):pre: ���������� ���,TVS3
      R_aref=0
      R_apef=0
      R_amef=0
      R_alef=0
      R_efef=0
      R_edef=0
      R_ebef=0
      R_exaf=0
      R_evaf=0
      R_otaf=0
      R_ataf=0
      R_isaf=0
      R_uraf=0
      R_eraf=0
      R_opaf=0
C FDD_logic.fgi(  67, 277):pre: ���������� ���,TVS4
      I_(1) = 1
C FDD_logic.fgi( 116, 199):��������� ������������� IN (�������)
      I_(2) = 0
C FDD_logic.fgi( 116, 201):��������� ������������� IN (�������)
      I_(3) = 1
C FDD_logic.fgi( 116, 216):��������� ������������� IN (�������)
      I_(4) = 0
C FDD_logic.fgi( 116, 218):��������� ������������� IN (�������)
      I_(5) = 1
C FDD_logic.fgi( 116, 233):��������� ������������� IN (�������)
      I_(6) = 0
C FDD_logic.fgi( 116, 235):��������� ������������� IN (�������)
      I_(7) = 1
C FDD_logic.fgi( 115, 248):��������� ������������� IN (�������)
      I_(8) = 0
C FDD_logic.fgi( 115, 250):��������� ������������� IN (�������)
      I_(14) = 1
C FDD_logic.fgi( 115, 265):��������� ������������� IN (�������)
      I_(15) = 0
C FDD_logic.fgi( 115, 267):��������� ������������� IN (�������)
      I_(16) = 0
C FDD_logic.fgi(  30, 188):��������� ������������� IN (�������)
      L_(1)=I_i.ne.I_(16)
C FDD_logic.fgi(  35, 189):���������� �������������
      if(L_(1)) then
         I_u=I0_o
      else
         I_u=I0_ad
      endif
C FDD_logic.fgi(  42, 196):���� RE IN LO CH7
      if(L_(1)) then
         I_(9)=I_(1)
      else
         I_(9)=I_(2)
      endif
C FDD_logic.fgi( 124, 199):���� RE IN LO CH7
      !�����. �������� I0_o = FDD_logicC?? /z'0100000A'/
C FDD_logic.fgi(  34, 196):��������� �����
      !�����. �������� I0_ad = FDD_logicC?? /z'01000003'/
C FDD_logic.fgi(  34, 198):��������� �����
      I_(17) = 0
C FDD_logic.fgi(  30, 206):��������� ������������� IN (�������)
      L_(2)=I_ed.ne.I_(17)
C FDD_logic.fgi(  35, 207):���������� �������������
      if(L_(2)) then
         I_od=I0_id
      else
         I_od=I0_ud
      endif
C FDD_logic.fgi(  42, 214):���� RE IN LO CH7
      if(L_(2)) then
         I_(10)=I_(3)
      else
         I_(10)=I_(4)
      endif
C FDD_logic.fgi( 124, 216):���� RE IN LO CH7
      !�����. �������� I0_id = FDD_logicC?? /z'0100000A'/
C FDD_logic.fgi(  34, 214):��������� �����
      !�����. �������� I0_ud = FDD_logicC?? /z'01000003'/
C FDD_logic.fgi(  34, 216):��������� �����
      I_(18) = 0
C FDD_logic.fgi(  30, 224):��������� ������������� IN (�������)
      L_(3)=I_af.ne.I_(18)
C FDD_logic.fgi(  35, 225):���������� �������������
      if(L_(3)) then
         I_if=I0_ef
      else
         I_if=I0_of
      endif
C FDD_logic.fgi(  42, 232):���� RE IN LO CH7
      if(L_(3)) then
         I_(11)=I_(5)
      else
         I_(11)=I_(6)
      endif
C FDD_logic.fgi( 124, 233):���� RE IN LO CH7
      !�����. �������� I0_ef = FDD_logicC?? /z'0100000A'/
C FDD_logic.fgi(  34, 232):��������� �����
      !�����. �������� I0_of = FDD_logicC?? /z'01000003'/
C FDD_logic.fgi(  34, 234):��������� �����
      I_(19) = 0
C FDD_logic.fgi(  30, 241):��������� ������������� IN (�������)
      L_(4)=I_uf.ne.I_(19)
C FDD_logic.fgi(  35, 242):���������� �������������
      if(L_(4)) then
         I_ek=I0_ak
      else
         I_ek=I0_ik
      endif
C FDD_logic.fgi(  42, 249):���� RE IN LO CH7
      if(L_(4)) then
         I_(12)=I_(7)
      else
         I_(12)=I_(8)
      endif
C FDD_logic.fgi( 123, 248):���� RE IN LO CH7
      !�����. �������� I0_ak = FDD_logicC?? /z'0100000A'/
C FDD_logic.fgi(  34, 249):��������� �����
      !�����. �������� I0_ik = FDD_logicC?? /z'01000003'/
C FDD_logic.fgi(  34, 251):��������� �����
      I_(20) = 0
C FDD_logic.fgi(  30, 256):��������� ������������� IN (�������)
      L_(5)=I_ok.ne.I_(20)
C FDD_logic.fgi(  35, 257):���������� �������������
      if(L_(5)) then
         I_al=I0_uk
      else
         I_al=I0_el
      endif
C FDD_logic.fgi(  42, 264):���� RE IN LO CH7
      if(L_(5)) then
         I_(13)=I_(14)
      else
         I_(13)=I_(15)
      endif
C FDD_logic.fgi( 123, 265):���� RE IN LO CH7
      I_il = I_(13) + I_(12) + I_(11) + I_(10) + I_(9)
C FDD_logic.fgi( 133, 234):��������
      !�����. �������� I0_uk = FDD_logicC?? /z'0100000A'/
C FDD_logic.fgi(  34, 264):��������� �����
      !�����. �������� I0_el = FDD_logicC?? /z'01000003'/
C FDD_logic.fgi(  34, 266):��������� �����
      Call TVS_HANDLER(deltat,I_ul,I_ol,R_ope,
     & R_upe,R_are,R_ere,R_ire,R_ore,R_ure,
     & R_ase,R_ese,R_ise,R_ose,R_use,R_ate,
     & R_ete,I_eti,R_exe,R_ixe,I_iti,R_oxe,
     & R_uxe,R_abi,R_ebi,I_oti,I_uti,I_avi,I_evi,
     & I_ivi,I_ovi,I_uvi,R_ibi,R_obi,I_axaf,
     & R_ubi,R_adi,R_edi,I_uvaf,R_idi,
     & I_abef,R_odi,R_udi,R_afi,I_uxaf,
     & R_efi,I_adef,R_ifi,R_ofi,R_ufi,
     & I_ubef,R_aki,I_afef,R_eki,R_iki,
     & R_oki,I_udef,R_uki,R_ali,I_olef,
     & R_eli,R_ili,R_oli,R_uli,I_omef,
     & R_ami,R_emi,R_imi,R_omi,I_opef,
     & R_umi,R_exi,R_ixi,R_oxi,R_uxi,R_abo,R_ebo,
     & R_ibo,R_obo,R_ubo,R_ado,R_edo,R_ido,R_odo,R_udo,
     & R_afo,R_efo,R_ifo,R_ofo,I_ufo,I_ako,I_eko,I_iko,
     & I_oko,I_uko,I_alo,I_elo,I_ilo,I_olo,I_ulo,I_amo,
     & I_emo,I_imo,R_umo,R_apo,R_epo,R_ipo,R_opo,R_upo,
     & R_aro,R_ero,R_iro,R_oro,R_uro,R_aso,L_epef,
     & I_iso,L_apaf,R_opi,R_ir,R_iv,
     & R_ide,R_ile,R_ipaf,R_opaf,I_oso,
     & L_epaf,R_upi,R_or,R_ov,R_ode,
     & R_ole,R_araf,R_eraf,I_uso,L_upaf,
     & R_ari,R_ur,R_uv,R_ude,R_ule,
     & R_oraf,R_uraf,I_eri,L_iraf,R_ame,
     & R_am,R_as,R_ax,R_afe,R_esaf,
     & R_isaf,I_iri,L_asaf,R_eme,R_em,R_es,
     & R_ex,R_efe,R_usaf,R_ataf,I_ori,
     & L_osaf,R_ime,R_im,R_is,R_ix,R_ife,
     & R_itaf,R_otaf,I_uri,L_etaf,R_ome,
     & R_om,R_os,R_ox,R_ofe,R_avaf,
     & R_evaf,I_asi,L_utaf,R_ume,R_um,R_us,
     & R_ux,R_ufe,R_ovaf,R_exaf,I_isi,
     & L_ivaf,R_ape,R_ap,R_at,R_abe,R_ake,
     & R_oxaf,R_ebef,I_osi,L_ixaf,R_epe,
     & R_ep,R_et,R_ebe,R_eke,R_obef,
     & R_edef,I_usi,L_ibef,R_ipe,R_ip,R_it,
     & R_ibe,R_ike,R_odef,R_efef,I_axi,
     & L_idef,R_api,R_op,R_ot,R_obe,R_oke,
     & R_ofef,R_alef,I_omo,L_ifef,R_epi,
     & R_up,R_ut,R_ube,R_uke,R_ilef,
     & R_amef,I_eso,L_elef,R_ipi,R_ar,R_av,
     & R_ade,R_ale,R_imef,R_apef,I_ati,
     & L_emef,R_axe,R_er,R_ev,R_ede,R_ele,
     & R_ipef,I_esi,I_umaf,R_aref)
C FDD_logic.fgi(  67, 277):���������� ���,TVS4
      Call TVS_HANDLER(deltat,I_eto,I_ato,R_axu,
     & R_exu,R_ixu,R_oxu,R_uxu,R_abad,R_ebad,
     & R_ibad,R_obad,R_ubad,R_adad,R_edad,R_idad,
     & R_odad,I_oded,R_okad,R_ukad,I_uded,R_alad,
     & R_elad,R_ilad,R_olad,I_afed,I_efed,I_ifed,I_ofed,
     & I_ufed,I_aked,I_eked,R_ulad,R_amad,I_axaf,
     & R_emad,R_imad,R_omad,I_uvaf,R_umad,
     & I_abef,R_apad,R_epad,R_ipad,I_uxaf,
     & R_opad,I_adef,R_upad,R_arad,R_erad,
     & I_ubef,R_irad,I_afef,R_orad,R_urad,
     & R_asad,I_udef,R_esad,R_isad,I_olef,
     & R_osad,R_usad,R_atad,R_etad,I_omef,
     & R_itad,R_otad,R_utad,R_avad,I_opef,
     & R_evad,R_oked,R_uked,R_aled,R_eled,R_iled,R_oled,
     & R_uled,R_amed,R_emed,R_imed,R_omed,R_umed,R_aped,R_eped
     &,
     & R_iped,R_oped,R_uped,R_ared,I_ered,I_ired,I_ored,I_ured
     &,
     & I_ased,I_esed,I_ised,I_osed,I_used,I_ated,I_eted,I_ited
     &,
     & I_oted,I_uted,R_eved,R_ived,R_oved,R_uved,R_axed,R_exed
     &,
     & R_ixed,R_oxed,R_uxed,R_abid,R_ebid,R_ibid,L_epef,
     & I_ubid,L_apaf,R_axad,R_uxo,R_ufu,
     & R_umu,R_usu,R_ipaf,R_opaf,I_adid,
     & L_epaf,R_exad,R_abu,R_aku,R_apu,
     & R_atu,R_araf,R_eraf,I_edid,L_upaf,
     & R_ixad,R_ebu,R_eku,R_epu,R_etu,
     & R_oraf,R_uraf,I_oxad,L_iraf,R_itu,
     & R_ito,R_ibu,R_iku,R_ipu,R_esaf,
     & R_isaf,I_uxad,L_asaf,R_otu,R_oto,R_obu,
     & R_oku,R_opu,R_usaf,R_ataf,I_abed,
     & L_osaf,R_utu,R_uto,R_ubu,R_uku,R_upu,
     & R_itaf,R_otaf,I_ebed,L_etaf,R_avu,
     & R_avo,R_adu,R_alu,R_aru,R_avaf,
     & R_evaf,I_ibed,L_utaf,R_evu,R_evo,R_edu,
     & R_elu,R_eru,R_ovaf,R_exaf,I_ubed,
     & L_ivaf,R_ivu,R_ivo,R_idu,R_ilu,R_iru,
     & R_oxaf,R_ebef,I_aded,L_ixaf,R_ovu,
     & R_ovo,R_odu,R_olu,R_oru,R_obef,
     & R_edef,I_eded,L_ibef,R_uvu,R_uvo,R_udu,
     & R_ulu,R_uru,R_odef,R_efef,I_iked,
     & L_idef,R_ivad,R_axo,R_afu,R_amu,R_asu,
     & R_ofef,R_alef,I_aved,L_ifef,R_ovad,
     & R_exo,R_efu,R_emu,R_esu,R_ilef,
     & R_amef,I_obid,L_elef,R_uvad,R_ixo,R_ifu,
     & R_imu,R_isu,R_imef,R_apef,I_ided,
     & L_emef,R_ikad,R_oxo,R_ofu,R_omu,R_osu,
     & R_ipef,I_obed,I_umaf,R_aref)
C FDD_logic.fgi(  61, 277):���������� ���,TVS3
      Call TVS_HANDLER(deltat,I_odid,I_idid,R_ikod,
     & R_okod,R_ukod,R_alod,R_elod,R_ilod,R_olod,
     & R_ulod,R_amod,R_emod,R_imod,R_omod,R_umod,
     & R_apod,I_apud,R_asod,R_esod,I_epud,R_isod,
     & R_osod,R_usod,R_atod,I_ipud,I_opud,I_upud,I_arud,
     & I_erud,I_irud,I_orud,R_etod,R_itod,I_axaf,
     & R_otod,R_utod,R_avod,I_uvaf,R_evod,
     & I_abef,R_ivod,R_ovod,R_uvod,I_uxaf,
     & R_axod,I_adef,R_exod,R_ixod,R_oxod,
     & I_ubef,R_uxod,I_afef,R_abud,R_ebud,
     & R_ibud,I_udef,R_obud,R_ubud,I_olef,
     & R_adud,R_edud,R_idud,R_odud,I_omef,
     & R_udud,R_afud,R_efud,R_ifud,I_opef,
     & R_ofud,R_asud,R_esud,R_isud,R_osud,R_usud,R_atud,
     & R_etud,R_itud,R_otud,R_utud,R_avud,R_evud,R_ivud,R_ovud
     &,
     & R_uvud,R_axud,R_exud,R_ixud,I_oxud,I_uxud,I_abaf,I_ebaf
     &,
     & I_ibaf,I_obaf,I_ubaf,I_adaf,I_edaf,I_idaf,I_odaf,I_udaf
     &,
     & I_afaf,I_efaf,R_ofaf,R_ufaf,R_akaf,R_ekaf,R_ikaf,R_okaf
     &,
     & R_ukaf,R_alaf,R_elaf,R_ilaf,R_olaf,R_ulaf,L_epef,
     & I_emaf,L_apaf,R_ikud,R_elid,R_erid,
     & R_evid,R_edod,R_ipaf,R_opaf,I_imaf,
     & L_epaf,R_okud,R_ilid,R_irid,R_ivid,
     & R_idod,R_araf,R_eraf,I_omaf,L_upaf,
     & R_ukud,R_olid,R_orid,R_ovid,R_odod,
     & R_oraf,R_uraf,I_alud,L_iraf,R_udod,
     & R_udid,R_ulid,R_urid,R_uvid,R_esaf,
     & R_isaf,I_elud,L_asaf,R_afod,R_afid,R_amid,
     & R_asid,R_axid,R_usaf,R_ataf,I_ilud,
     & L_osaf,R_efod,R_efid,R_emid,R_esid,R_exid,
     & R_itaf,R_otaf,I_olud,L_etaf,R_ifod,
     & R_ifid,R_imid,R_isid,R_ixid,R_avaf,
     & R_evaf,I_ulud,L_utaf,R_ofod,R_ofid,R_omid,
     & R_osid,R_oxid,R_ovaf,R_exaf,I_emud,
     & L_ivaf,R_ufod,R_ufid,R_umid,R_usid,R_uxid,
     & R_oxaf,R_ebef,I_imud,L_ixaf,R_akod,
     & R_akid,R_apid,R_atid,R_abod,R_obef,
     & R_edef,I_omud,L_ibef,R_ekod,R_ekid,R_epid,
     & R_etid,R_ebod,R_odef,R_efef,I_urud,
     & L_idef,R_ufud,R_ikid,R_ipid,R_itid,R_ibod,
     & R_ofef,R_alef,I_ifaf,L_ifef,R_akud,
     & R_okid,R_opid,R_otid,R_obod,R_ilef,
     & R_amef,I_amaf,L_elef,R_ekud,R_ukid,R_upid,
     & R_utid,R_ubod,R_imef,R_apef,I_umud,
     & L_emef,R_urod,R_alid,R_arid,R_avid,R_adod,
     & R_ipef,I_amud,I_umaf,R_aref)
C FDD_logic.fgi(  55, 277):���������� ���,TVS2
      Call TVS_HANDLER(deltat,I_akef,I_ufef,R_usif,
     & R_atif,R_etif,R_itif,R_otif,R_utif,R_avif,
     & R_evif,R_ivif,R_ovif,R_uvif,R_axif,R_exif,
     & R_ixif,I_ixof,R_idof,R_odof,I_oxof,R_udof,
     & R_afof,R_efof,R_ifof,I_uxof,I_abuf,I_ebuf,I_ibuf,
     & I_obuf,I_ubuf,I_aduf,R_ofof,R_ufof,I_axaf,
     & R_akof,R_ekof,R_ikof,I_uvaf,R_okof,
     & I_abef,R_ukof,R_alof,R_elof,I_uxaf,
     & R_ilof,I_adef,R_olof,R_ulof,R_amof,
     & I_ubef,R_emof,I_afef,R_imof,R_omof,
     & R_umof,I_udef,R_apof,R_epof,I_olef,
     & R_ipof,R_opof,R_upof,R_arof,I_omef,
     & R_erof,R_irof,R_orof,R_urof,I_opef,
     & R_asof,R_iduf,R_oduf,R_uduf,R_afuf,R_efuf,R_ifuf,
     & R_ofuf,R_ufuf,R_akuf,R_ekuf,R_ikuf,R_okuf,R_ukuf,R_aluf
     &,
     & R_eluf,R_iluf,R_oluf,R_uluf,I_amuf,I_emuf,I_imuf,I_omuf
     &,
     & I_umuf,I_apuf,I_epuf,I_ipuf,I_opuf,I_upuf,I_aruf,I_eruf
     &,
     & I_iruf,I_oruf,R_asuf,R_esuf,R_isuf,R_osuf,R_usuf,R_atuf
     &,
     & R_etuf,R_ituf,R_otuf,R_utuf,R_avuf,R_evuf,L_epef,
     & I_ovuf,L_apaf,R_usof,R_otef,R_obif,
     & R_okif,R_opif,R_ipaf,R_opaf,I_uvuf,
     & L_epaf,R_atof,R_utef,R_ubif,R_ukif,
     & R_upif,R_araf,R_eraf,I_axuf,L_upaf,
     & R_etof,R_avef,R_adif,R_alif,R_arif,
     & R_oraf,R_uraf,I_itof,L_iraf,R_erif,
     & R_eref,R_evef,R_edif,R_elif,R_esaf,
     & R_isaf,I_otof,L_asaf,R_irif,R_iref,R_ivef,
     & R_idif,R_ilif,R_usaf,R_ataf,I_utof,
     & L_osaf,R_orif,R_oref,R_ovef,R_odif,R_olif,
     & R_itaf,R_otaf,I_avof,L_etaf,R_urif,
     & R_uref,R_uvef,R_udif,R_ulif,R_avaf,
     & R_evaf,I_evof,L_utaf,R_asif,R_asef,R_axef,
     & R_afif,R_amif,R_ovaf,R_exaf,I_ovof,
     & L_ivaf,R_esif,R_esef,R_exef,R_efif,R_emif,
     & R_oxaf,R_ebef,I_uvof,L_ixaf,R_isif,
     & R_isef,R_ixef,R_ifif,R_imif,R_obef,
     & R_edef,I_axof,L_ibef,R_osif,R_osef,R_oxef,
     & R_ofif,R_omif,R_odef,R_efef,I_eduf,
     & L_idef,R_esof,R_usef,R_uxef,R_ufif,R_umif,
     & R_ofef,R_alef,I_uruf,L_ifef,R_isof,
     & R_atef,R_abif,R_akif,R_apif,R_ilef,
     & R_amef,I_ivuf,L_elef,R_osof,R_etef,R_ebif,
     & R_ekif,R_epif,R_imef,R_apef,I_exof,
     & L_emef,R_edof,R_itef,R_ibif,R_ikif,R_ipif,
     & R_ipef,I_ivof,I_umaf,R_aref)
C FDD_logic.fgi(  49, 277):���������� ���,TVS1
      !��������� I_(38) = control_tgC?? /65486/
      I_(38)=I0_ukak
C control_tg.fgi( 117,  31):��������� ������������� IN
      !��������� I_(39) = control_tgC?? /65519/
      I_(39)=I0_alak
C control_tg.fgi( 117,  20):��������� ������������� IN
      !��������� I_(40) = control_tgC?? /65503/
      I_(40)=I0_elak
C control_tg.fgi( 117,  22):��������� ������������� IN
      !��������� I_(41) = control_tgC?? /65534/
      I_(41)=I0_ilak
C control_tg.fgi( 117,  29):��������� ������������� IN
      I_(42) = 2
C control_tg.fgi( 120,  38):��������� ������������� IN (�������)
      I_(43) = 3
C control_tg.fgi( 120,  40):��������� ������������� IN (�������)
      L_(34)=.false.
C control_tg.fgi( 196,  53):��������� ���������� (�������)
      if(L_(34)) then
         I0_opak=0
      endif
      I_(48)=I0_opak
      L_(33)=I0_opak.ne.0
C control_tg.fgi( 204,  55):���������� ��������� 
      L_uruk=.false.
C control_tg.fgi( 142, 117):��������� ���������� (�������)
      L_(40)=.true.
C control_tg.fgi( 357, 290):��������� ���������� (�������)
      L_(42)=.false.
C control_tg.fgi(   7, 156):��������� ���������� (�������)
      if(L_(42)) then
         I0_epal=0
      endif
      I_(50)=I0_epal
      L_(41)=I0_epal.ne.0
C control_tg.fgi(  15, 158):���������� ��������� 
      !�����. �������� I0_oral = control_tgC?? /z'01000007'
C /
C control_tg.fgi( 253,  91):��������� �����
      !�����. �������� I0_asal = control_tgC?? /z'0100000A'
C /
C control_tg.fgi( 232,  92):��������� �����
      !�����. �������� I0_esal = control_tgC?? /z'01000003'
C /
C control_tg.fgi( 232,  94):��������� �����
      !�����. �������� I0_usal = control_tgC?? /z'01000007'
C /
C control_tg.fgi( 253, 105):��������� �����
      !�����. �������� I0_etal = control_tgC?? /z'0100000A'
C /
C control_tg.fgi( 232, 106):��������� �����
      !�����. �������� I0_ital = control_tgC?? /z'01000003'
C /
C control_tg.fgi( 232, 108):��������� �����
      !��������� R_(7) = control_tgC?? /1/
      R_(7)=R0_utal
C control_tg.fgi(  29,  74):���������
      R_(9) = 0
C control_tg.fgi(  46,  81):��������� (RE4) (�������)
      L_(43)=.false.
C control_tg.fgi(  38,  69):��������� ���������� (�������)
      R_(8) = 0.1
C control_tg.fgi(  40,  81):��������� (RE4) (�������)
      R_(19) = 750
C control_tg.fgi( 111, 180):��������� (RE4) (�������)
      L_ekel=L_ibel
C control_tg.fgi( 155, 189):������,m3m4_fidpos
      L_(45)=.false.
C control_tg.fgi(  98, 164):��������� ���������� (�������)
      !��������� R_(12) = control_tgC?? /3.3/
      R_(12)=R0_axal
C control_tg.fgi(  95, 172):���������
      !��������� R_(15) = control_tgC?? /5.357/
      R_(15)=R0_exal
C control_tg.fgi( 118, 159):���������
      !��������� R_(13) = control_tgCvslow /1/
      R_(13)=R0_abel
C control_tg.fgi(  35, 180):���������,vslow
      !��������� R_(14) = control_tgCvnorm /0.1/
      R_(14)=R0_ebel
C control_tg.fgi(  35, 183):���������,vnorm
      if(L_ibel) then
         R_(20)=R_(13)
      else
         R_(20)=R_(14)
      endif
C control_tg.fgi(  44, 181):���� RE IN LO CH7
      R_(21) = 0
C control_tg.fgi( 109, 177):��������� (RE4) (�������)
      !��������� R_(23) = control_tgC497 /750/
      R_(23)=R0_obel
C control_tg.fgi( 106, 177):���������
      L_(57) = L_akel.OR.L_ufel
C control_tg.fgi( 220, 134):���
      L_(64) = L_akel.OR.L_ufel
C control_tg.fgi( 220, 225):���
      !��������� R_(33) = control_tgC?? /20/
      R_(33)=R0_umel
C control_tg.fgi(  77, 284):���������
      !��������� R_(28) = control_tgC?? /-750/
      R_(28)=R0_asel
C control_tg.fgi(  91, 217):���������
      !��������� R_(27) = control_tgC?? /750/
      R_(27)=R0_esel
C control_tg.fgi( 106, 217):���������
      !��������� R_(30) = control_tgC?? /750/
      R_(30)=R0_isel
C control_tg.fgi( 104, 284):���������
      R_(26) = 0.0
C control_tg.fgi(  77, 216):��������� (RE4) (�������)
      !��������� R_(31) = control_tgC?? /730/
      R_(31)=R0_osel
C control_tg.fgi(  90, 284):���������
      L0_abil=R0_ibil.ne.R0_ebil
      R0_ebil=R0_ibil
C control_tg.fgi( 176,  13):���������� ������������� ������
      L0_ofil=R0_afil.ne.R0_udil
      R0_udil=R0_afil
C control_th.fgi( 178,  35):���������� ������������� ������
      !�����. �������� I0_oril = control_thC?? /z'01000007'
C /
C control_th.fgi( 133,  33):��������� �����
      !�����. �������� I0_uril = control_thC?? /z'0100000A'
C /
C control_th.fgi( 112,  34):��������� �����
      !�����. �������� I0_asil = control_thC?? /z'01000003'
C /
C control_th.fgi( 112,  36):��������� �����
      !�����. �������� I0_osil = control_thC?? /z'01000007'
C /
C control_th.fgi( 133,  49):��������� �����
      !�����. �������� I0_usil = control_thC?? /z'0100000A'
C /
C control_th.fgi( 112,  50):��������� �����
      !�����. �������� I0_atil = control_thC?? /z'01000003'
C /
C control_th.fgi( 112,  52):��������� �����
      !�����. �������� I0_exil = control_thC?? /z'01000085'
C /
C control_th.fgi( 116, 122):��������� �����
      !�����. �������� I0_ixil = control_thC?? /z'01000007'
C /
C control_th.fgi( 116, 124):��������� �����
      R_(44) = 26000
C control_th.fgi( 115, 186):��������� (RE4) (�������)
      L_(119)=.false.
C control_th.fgi( 102, 173):��������� ���������� (�������)
      !��������� R_(38) = control_thC?? /33/
      R_(38)=R0_obol
C control_th.fgi(  99, 181):���������
      !��������� R_(41) = control_thC?? /28.26/
      R_(41)=R0_ubol
C control_th.fgi( 130, 187):���������
      !��������� R_(39) = control_thCvslow /1/
      R_(39)=R0_udol
C control_th.fgi(  82, 193):���������,vslow
      !��������� R_(40) = control_thCvnorm /0.1/
      R_(40)=R0_afol
C control_th.fgi(  82, 196):���������,vnorm
      R_(46) = 0.0
C control_th.fgi( 113, 186):��������� (RE4) (�������)
      !��������� R_(48) = control_thC497 /0.0/
      R_(48)=R0_efol
C control_th.fgi( 110, 186):���������
      L0_okul=R0_alul.ne.R0_ukul
      R0_ukul=R0_alul
C control_th.fgi( 203,  86):���������� ������������� ������
      L_(137)=.false.
C control_th.fgi( 241,  84):��������� ���������� (�������)
      L_(138)=.false.
C control_th.fgi( 241,  88):��������� ���������� (�������)
      L0_ulul=R0_emul.ne.R0_amul
      R0_amul=R0_emul
C control_th.fgi( 203,  99):���������� ������������� ������
      L_(139)=.false.
C control_th.fgi( 241,  97):��������� ���������� (�������)
      L_(140)=.false.
C control_th.fgi( 241, 101):��������� ���������� (�������)
      L_(141)=.true.
C control_th.fgi( 362, 139):��������� ���������� (�������)
      L0_epem=.false.
      I0_apem=0

      if(L0_ekap)then
        I0_apom=1
         L0_omem=.false.
         L0_idem=.false.
         L0_atam=.false.
         L0_esam=.false.
         L0_uram=.false.
         L0_iram=.false.
         L0_aram=.false.
         L0_opam=.false.
         L0_epam=.false.
         L0_emem=.false.
         L0_ulem=.false.
         L0_ilem=.false.
         L0_alem=.false.
         L0_okem=.false.
         L0_ekem=.false.
         L0_ufem=.false.
         L0_ifem=.false.
         L0_afem=.false.
         L0_odem=.false.
         L0_adem=.false.
         L0_obem=.false.
         L0_ebem=.false.
         L0_uxam=.false.
         L0_ixam=.false.
         L0_axam=.false.
         L0_ovam=.false.
         L0_evam=.false.
         L0_utam=.false.
         L0_itam=.false.
         L0_osam=.false.
      endif

      if(L0_omem)I0_apem=I0_apem+1
      if(L0_idem)I0_apem=I0_apem+1
      if(L0_atam)I0_apem=I0_apem+1
      if(L0_esam)I0_apem=I0_apem+1
      if(L0_uram)I0_apem=I0_apem+1
      if(L0_iram)I0_apem=I0_apem+1
      if(L0_aram)I0_apem=I0_apem+1
      if(L0_opam)I0_apem=I0_apem+1
      if(L0_epam)I0_apem=I0_apem+1
      if(L0_emem)I0_apem=I0_apem+1
      if(L0_ulem)I0_apem=I0_apem+1
      if(L0_ilem)I0_apem=I0_apem+1
      if(L0_alem)I0_apem=I0_apem+1
      if(L0_okem)I0_apem=I0_apem+1
      if(L0_ekem)I0_apem=I0_apem+1
      if(L0_ufem)I0_apem=I0_apem+1
      if(L0_ifem)I0_apem=I0_apem+1
      if(L0_afem)I0_apem=I0_apem+1
      if(L0_odem)I0_apem=I0_apem+1
      if(L0_adem)I0_apem=I0_apem+1
      if(L0_obem)I0_apem=I0_apem+1
      if(L0_ebem)I0_apem=I0_apem+1
      if(L0_uxam)I0_apem=I0_apem+1
      if(L0_ixam)I0_apem=I0_apem+1
      if(L0_axam)I0_apem=I0_apem+1
      if(L0_ovam)I0_apem=I0_apem+1
      if(L0_evam)I0_apem=I0_apem+1
      if(L0_utam)I0_apem=I0_apem+1
      if(L0_itam)I0_apem=I0_apem+1
      if(L0_osam)I0_apem=I0_apem+1


      if(.not.L0_epem.and.L0_umem.and..not.L0_omem)then
        I0_apom=1
        L0_epem=.true.
      endif

      if(.not.L0_epem.and.L_(141).and..not.L0_idem)then
        I0_apom=2
        L0_epem=.true.
      endif

      if(.not.L0_epem.and.L0_etam.and..not.L0_atam)then
        I0_apom=3
        L0_epem=.true.
      endif

      if(.not.L0_epem.and.L0_isam.and..not.L0_esam)then
        I0_apom=4
        L0_epem=.true.
      endif

      if(.not.L0_epem.and.L0_asam.and..not.L0_uram)then
        I0_apom=5
        L0_epem=.true.
      endif

      if(.not.L0_epem.and.L0_oram.and..not.L0_iram)then
        I0_apom=6
        L0_epem=.true.
      endif

      if(.not.L0_epem.and.L0_eram.and..not.L0_aram)then
        I0_apom=7
        L0_epem=.true.
      endif

      if(.not.L0_epem.and.L0_upam.and..not.L0_opam)then
        I0_apom=8
        L0_epem=.true.
      endif

      if(.not.L0_epem.and.L0_ipam.and..not.L0_epam)then
        I0_apom=9
        L0_epem=.true.
      endif

      if(.not.L0_epem.and.L0_imem.and..not.L0_emem)then
        I0_apom=10
        L0_epem=.true.
      endif

      if(.not.L0_epem.and.L0_amem.and..not.L0_ulem)then
        I0_apom=11
        L0_epem=.true.
      endif

      if(.not.L0_epem.and.L0_olem.and..not.L0_ilem)then
        I0_apom=12
        L0_epem=.true.
      endif

      if(.not.L0_epem.and.L0_elem.and..not.L0_alem)then
        I0_apom=13
        L0_epem=.true.
      endif

      if(.not.L0_epem.and.L0_ukem.and..not.L0_okem)then
        I0_apom=14
        L0_epem=.true.
      endif

      if(.not.L0_epem.and.L0_ikem.and..not.L0_ekem)then
        I0_apom=15
        L0_epem=.true.
      endif

      if(.not.L0_epem.and.L0_akem.and..not.L0_ufem)then
        I0_apom=16
        L0_epem=.true.
      endif

      if(.not.L0_epem.and.L0_ofem.and..not.L0_ifem)then
        I0_apom=17
        L0_epem=.true.
      endif

      if(.not.L0_epem.and.L0_efem.and..not.L0_afem)then
        I0_apom=18
        L0_epem=.true.
      endif

      if(.not.L0_epem.and.L0_udem.and..not.L0_odem)then
        I0_apom=19
        L0_epem=.true.
      endif

      if(.not.L0_epem.and.L0_edem.and..not.L0_adem)then
        I0_apom=20
        L0_epem=.true.
      endif

      if(.not.L0_epem.and.L0_ubem.and..not.L0_obem)then
        I0_apom=21
        L0_epem=.true.
      endif

      if(.not.L0_epem.and.L0_ibem.and..not.L0_ebem)then
        I0_apom=22
        L0_epem=.true.
      endif

      if(.not.L0_epem.and.L0_abem.and..not.L0_uxam)then
        I0_apom=23
        L0_epem=.true.
      endif

      if(.not.L0_epem.and.L0_oxam.and..not.L0_ixam)then
        I0_apom=24
        L0_epem=.true.
      endif

      if(.not.L0_epem.and.L0_exam.and..not.L0_axam)then
        I0_apom=25
        L0_epem=.true.
      endif

      if(.not.L0_epem.and.L0_uvam.and..not.L0_ovam)then
        I0_apom=26
        L0_epem=.true.
      endif

      if(.not.L0_epem.and.L0_ivam.and..not.L0_evam)then
        I0_apom=27
        L0_epem=.true.
      endif

      if(.not.L0_epem.and.L0_avam.and..not.L0_utam)then
        I0_apom=28
        L0_epem=.true.
      endif

      if(.not.L0_epem.and.L0_otam.and..not.L0_itam)then
        I0_apom=29
        L0_epem=.true.
      endif

      if(.not.L0_epem.and.L0_usam.and..not.L0_osam)then
        I0_apom=30
        L0_epem=.true.
      endif

      if(I0_apem.eq.1)then
         L0_omem=.false.
         L0_idem=.false.
         L0_atam=.false.
         L0_esam=.false.
         L0_uram=.false.
         L0_iram=.false.
         L0_aram=.false.
         L0_opam=.false.
         L0_epam=.false.
         L0_emem=.false.
         L0_ulem=.false.
         L0_ilem=.false.
         L0_alem=.false.
         L0_okem=.false.
         L0_ekem=.false.
         L0_ufem=.false.
         L0_ifem=.false.
         L0_afem=.false.
         L0_odem=.false.
         L0_adem=.false.
         L0_obem=.false.
         L0_ebem=.false.
         L0_uxam=.false.
         L0_ixam=.false.
         L0_axam=.false.
         L0_ovam=.false.
         L0_evam=.false.
         L0_utam=.false.
         L0_itam=.false.
         L0_osam=.false.
      else
         L0_omem=L0_umem
         L0_idem=L_(141)
         L0_atam=L0_etam
         L0_esam=L0_isam
         L0_uram=L0_asam
         L0_iram=L0_oram
         L0_aram=L0_eram
         L0_opam=L0_upam
         L0_epam=L0_ipam
         L0_emem=L0_imem
         L0_ulem=L0_amem
         L0_ilem=L0_olem
         L0_alem=L0_elem
         L0_okem=L0_ukem
         L0_ekem=L0_ikem
         L0_ufem=L0_akem
         L0_ifem=L0_ofem
         L0_afem=L0_efem
         L0_odem=L0_udem
         L0_adem=L0_edem
         L0_obem=L0_ubem
         L0_ebem=L0_ibem
         L0_uxam=L0_abem
         L0_ixam=L0_oxam
         L0_axam=L0_exam
         L0_ovam=L0_uvam
         L0_evam=L0_ivam
         L0_utam=L0_avam
         L0_itam=L0_otam
         L0_osam=L0_usam
      endif
C control_th.fgi( 371, 112):��������� �������
      select case (I0_apom)
      case (1)
        C255_umom="����-� ��������� �� ����������"
      case (2)
        C255_umom="��������� ������� ������ � ��������"
      case (3)
        C255_umom="��������� �������, ������"
      case (4)
        C255_umom="��������� ������� ������ � ��������"
      case (5)
        C255_umom="��������� �������, ������"
      case (6)
        C255_umom="������ � ����.�������� ���������"
      case (7)
        C255_umom="������ �������"
      case (8)
        C255_umom="������ �����������"
      case (9)
        C255_umom="������ �����������"
      case (10)
        C255_umom="������ �������"
      case (11)
        C255_umom="������ � ����.�������� ���������"
      case (12)
        C255_umom="???"
      case (13)
        C255_umom="???"
      case (14)
        C255_umom="???"
      case (15)
        C255_umom="???"
      case (16)
        C255_umom="???"
      case (17)
        C255_umom="???"
      case (18)
        C255_umom="???"
      case (19)
        C255_umom="???"
      case (20)
        C255_umom="???"
      case (21)
        C255_umom="???"
      case (22)
        C255_umom="???"
      case (23)
        C255_umom="???"
      case (24)
        C255_umom="???"
      case (25)
        C255_umom="???"
      case (26)
        C255_umom="???"
      case (27)
        C255_umom="???"
      case (28)
        C255_umom="???"
      case (29)
        C255_umom="???"
      case (30)
        C255_umom="???"
      end select
C control_th.fgi( 386, 141):MarkVI text out 30 variants,mo_text4
      !��������� R_(59) = control_thC?? /60/
      R_(59)=R0_omom
C control_th.fgi(  81, 284):���������
      !��������� R_(53) = control_thC?? /26000/
      R_(53)=R0_asom
C control_th.fgi(  95, 247):���������
      !��������� R_(52) = control_thC?? /-26000/
      R_(52)=R0_esom
C control_th.fgi( 110, 247):���������
      !��������� R_(55) = control_thC?? /25150/
      R_(55)=R0_isom
C control_th.fgi( 121, 284):���������
      !��������� R_(56) = control_thC?? /16956/
      R_(56)=R0_osom
C control_th.fgi( 108, 284):���������
      R_(51) = 0.0
C control_th.fgi(  81, 246):��������� (RE4) (�������)
      !��������� R_(57) = control_thC?? /8760/
      R_(57)=R0_usom
C control_th.fgi(  94, 284):���������
      I_(105) = 1
C common.fgi(  50,  18):��������� ������������� IN (�������)
      I_(106) = 0
C common.fgi(  50,  20):��������� ������������� IN (�������)
      L_(155)=I_elap.eq.I0_udum
C common.fgi(  86, 103):���������� �������������
      L_(156)=I_elap.eq.I0_afum
C common.fgi(  86, 109):���������� �������������
      L_efum = L_(156).OR.L_(155)
C common.fgi(  97, 108):���
      if(L_efum) then
         I_odum=I_(105)
      else
         I_odum=I_(106)
      endif
C common.fgi(  54,  18):���� RE IN LO CH7
      if(L_efum) then
         I0_ekak=I_(50)
      endif
      I_(49)=I0_ekak
C control_tg.fgi(  56, 157):�������-��������
      L_(39)=I_(49).eq.I0_orak
C control_tg.fgi(  75, 158):���������� �������������
      if(L_efum) then
         I0_akak=I_(48)
      endif
      I_(37)=I0_akak
C control_tg.fgi( 219,  54):�������-��������
      L_(32)=I_(37).eq.I0_ufak
C control_tg.fgi( 228,  55):���������� �������������
      if(L_efum) then
         L0_opil=L0_okul
      endif
      L_(135)=L0_opil
C control_th.fgi( 217,  85):�������-��������
      if(L_(138).or.L_(137)) then
         L_elul=(L_(138).or.L_elul).and..not.(L_(137))
      else
         if(L_(135).and..not.L0_uful) L_elul=.not.L_elul
      endif
      L0_uful=L_(135)
      L_akul=.not.L_elul
C control_th.fgi( 247,  86):T �������
      L_ikul=L_elul
C control_th.fgi( 272,  92):������,mainops_chkbox02_sw
      if(L_elul) then
         I_ilul=1
      else
         I_ilul=0
      endif
C control_th.fgi( 258,  88):��������� LO->1
      if(L_efum) then
         L0_ipil=L0_ulul
      endif
      L_(136)=L0_ipil
C control_th.fgi( 217,  98):�������-��������
      if(L_(140).or.L_(139)) then
         L_imul=(L_(140).or.L_imul).and..not.(L_(139))
      else
         if(L_(136).and..not.L0_ekul) L_imul=.not.L_imul
      endif
      L0_ekul=L_(136)
      L_esul=.not.L_imul
C control_th.fgi( 247,  99):T �������
      L_olul=L_imul
C control_th.fgi( 272, 105):������,mainops_chkbox01_sw
      L_(115) =.NOT.(L_olul.OR.L_ikul)
C control_th.fgi(  50, 165):���
      if(L_imul) then
         I_omul=1
      else
         I_omul=0
      endif
C control_th.fgi( 258, 101):��������� LO->1
      I_(107) = 1
C common.fgi(  50,  30):��������� ������������� IN (�������)
      if(L_efum) then
         I_(108)=I_olap
      else
         I_(108)=I_(107)
      endif
C common.fgi(  54,  28):���� RE IN LO CH7
      select case (I_(108))
      case (1)
        C255_ilap="����� ������������"
      case (2)
        C255_ilap="�1.�����.����������� ������.������� 1"
      case (3)
        C255_ilap="�2.�����.����������� ������.������� 2"
      case (4)
        C255_ilap="�3.����.����������� ����.�������"
      case (5)
        C255_ilap="�4.����.����������� ����.�������(�)"
      case (6)
        C255_ilap="�5.���������� ����� ������.������� 1"
      case (7)
        C255_ilap="�6.���������� ����� ������.������� 1"
      case (8)
        C255_ilap="�7.����������� ������� ������"
      case (9)
        C255_ilap="�8.����������� ������� ������"
      case (10)
        C255_ilap="�9.������ ����� ������(�����)"
      case (11)
        C255_ilap="�10.������ ����� �������(������)"
      case (12)
        C255_ilap="�11.������ ������ �������(�����)"
      case (13)
        C255_ilap="�12.������ ������ �������(������)"
      case (14)
        C255_ilap="???"
      case (15)
        C255_ilap="???"
      case (16)
        C255_ilap="???"
      case (17)
        C255_ilap="???"
      case (18)
        C255_ilap="???"
      case (19)
        C255_ilap="???"
      case (20)
        C255_ilap="???"
      case (21)
        C255_ilap="???"
      case (22)
        C255_ilap="???"
      case (23)
        C255_ilap="???"
      case (24)
        C255_ilap="???"
      case (25)
        C255_ilap="???"
      case (26)
        C255_ilap="???"
      case (27)
        C255_ilap="???"
      case (28)
        C255_ilap="???"
      case (29)
        C255_ilap="???"
      case (30)
        C255_ilap="???"
      end select
C common.fgi( 153,  29):MarkVI text out 30 variants,sel_dev_menu
      L_(158)=I_(108).gt.I0_ifum
C common.fgi(  86,  47):���������� �������������
      L_(159)=I_(108).eq.I0_ofum
C common.fgi(  86,  53):���������� �������������
      L_(162)=I_elap.eq.I0_ufum
C common.fgi(  86,  59):���������� �������������
      L_(157)=I_elap.eq.I0_akum
C common.fgi(  86,  65):���������� �������������
      L_(160)=.true.
C common.fgi( 127,  42):��������� ���������� (�������)
      select case (I_elap)
      case (1)
        C255_alap="����� ������"
      case (2)
        C255_alap="��������������"
      case (3)
        C255_alap="������"
      case (4)
        C255_alap="��������������� ������������"
      case (5)
        C255_alap="???"
      case (6)
        C255_alap="???"
      case (7)
        C255_alap="???"
      case (8)
        C255_alap="???"
      case (9)
        C255_alap="???"
      case (10)
        C255_alap="???"
      case (11)
        C255_alap="???"
      case (12)
        C255_alap="???"
      case (13)
        C255_alap="???"
      case (14)
        C255_alap="???"
      case (15)
        C255_alap="???"
      case (16)
        C255_alap="???"
      case (17)
        C255_alap="???"
      case (18)
        C255_alap="???"
      case (19)
        C255_alap="???"
      case (20)
        C255_alap="???"
      case (21)
        C255_alap="???"
      case (22)
        C255_alap="???"
      case (23)
        C255_alap="???"
      case (24)
        C255_alap="???"
      case (25)
        C255_alap="???"
      case (26)
        C255_alap="???"
      case (27)
        C255_alap="???"
      case (28)
        C255_alap="???"
      case (29)
        C255_alap="???"
      case (30)
        C255_alap="???"
      end select
C common.fgi( 152, 116):MarkVI text out 30 variants,sel_reg_menu
      if(C255_emap.ne.C255_ulap) call spw_ShowFormat(C255_emap
     &,C255_amap,-1)
      C255_ulap=C255_emap
C common.fgi( 154,   9):call_fmt
      L_(154) = (.NOT.L_ifom)
C control_th.fgi(  25, 265):���
C label 306  try306=try306-1
      if(L_(154)) then
         I0_ubum=0
      endif
      I_(104)=I0_ubum
      L_(153)=I0_ubum.ne.0
C control_th.fgi(  35, 267):���������� ��������� 
      if(L_efum) then
         I0_aril=I_(104)
      endif
      I_axom=I0_aril
C control_th.fgi(  59, 266):�������-��������
      L_uvom=I_axom.eq.1
      L_ovom=I_axom.eq.2
      L_ivom=I_axom.eq.3
      L_evom=I_axom.eq.4
C control_th.fgi(  80, 267):���������� �� 4
      if(L_uvom) then
         R_(58)=R_(59)
      endif
C control_th.fgi(  84, 283):���� � ������������� �������
      if(L_ovom) then
         R_(58)=R_(57)
      endif
C control_th.fgi(  97, 283):���� � ������������� �������
      if(L_ivom) then
         R_(58)=R_(56)
      endif
C control_th.fgi( 111, 283):���� � ������������� �������
      if(L_evom) then
         R_(58)=R_(55)
      endif
C control_th.fgi( 124, 283):���� � ������������� �������
      R_(1)=R_(58)
C control_th.fgi(  84, 283):������-�������: ���������� ��� �������������� ������
      if(L_(154)) then
         I0_obum=0
      endif
      I_(103)=I0_obum
      L_(152)=I0_obum.ne.0
C control_th.fgi(  35, 236):���������� ��������� 
      if(L_efum) then
         I0_upil=I_(103)
      endif
      I_(102)=I0_upil
C control_th.fgi(  59, 235):�������-��������
      L_(151)=I_(102).eq.1
      L_utom=I_(102).eq.2
      L_otom=I_(102).eq.3
      L_itom=I_(102).eq.4
C control_th.fgi(  77, 236):���������� �� 4
      if(L_(151)) then
         R_(54)=R_(51)
      endif
C control_th.fgi(  84, 246):���� � ������������� �������
      if(L_utom) then
         R_(54)=R_(53)
      endif
C control_th.fgi(  98, 246):���� � ������������� �������
      if(L_otom) then
         R_(54)=R_(52)
      endif
C control_th.fgi( 113, 246):���� � ������������� �������
      R_(50) = (-R_(1)) + R_ibum
C control_th.fgi( 134, 283):��������
      if(L_itom) then
         R_(54)=R_(50)
      endif
C control_th.fgi( 123, 246):���� � ������������� �������
      R_(2)=R_(54)
C control_th.fgi(  84, 246):������-�������: ���������� ��� �������������� ������
      R_avom = R_(1) + R_(2)
C control_th.fgi( 134, 290):��������
      R_(43) = R_avom + (-R_ifol)
C control_th.fgi(  58, 178):��������
      L_(118)=R_(43).gt.R0_idol
C control_th.fgi(  67, 182):���������� >
      I_(96)=0
      if(L_(118))I_(96)=1
C control_th.fgi(  77, 182):lo2digit
      L_(117)=R_(43).lt.R0_edol
C control_th.fgi(  67, 176):���������� <
      I_(95)=0
      if(L_(117))I_(95)=-1
C control_th.fgi(  77, 176):lo2digit
      I_(97) = I_(96) + I_(95)
C control_th.fgi(  93, 179):��������
      R_(47) = R_(38) * I_(97)
C control_th.fgi( 102, 180):����������
      L_(95)=R0_omil.lt.R0_ukil
C control_th.fgi( 271,  57):���������� <
      L_(96) = L_elil.OR.L_(95)
C control_th.fgi( 283,  62):���
      L_(93)=L_(96).and..not.L0_akil
      L0_akil=L_(96)
C control_th.fgi( 290,  62):������������  �� 1 ���
      L_(90) = L_(93).OR.L0_ofil.OR.L_ufol
C control_th.fgi( 199,  56):���
      if(L_(90)) then
         I0_ifil=0
      endif
      I_(86)=I0_ifil
      L_(80)=I0_ifil.ne.0
C control_th.fgi( 208,  58):���������� ��������� 
      if(L_efum) then
         I0_epil=I_(86)
      endif
      I_(88)=I0_epil
C control_th.fgi( 217,  57):�������-��������
      L_(104)=I_(88).eq.1
      L_(103)=I_(88).eq.2
      L_(102)=I_(88).eq.3
      L_(101)=I_(88).eq.4
C control_th.fgi( 223,  58):���������� �� 4
      I_(84)=0
      if(L_(104))I_(84)=1
C control_th.fgi( 233,  61):lo2digit
      I_(83)=0
      if(L_(103))I_(83)=-1
C control_th.fgi( 233,  59):lo2digit
      I_(80) = I_(84) + I_(83)
C control_th.fgi( 238,  60):��������
      R_(36)=I_(80)
C control_th.fgi( 245,  60):��������� IN->RE
      R0_omil=R0_omil+deltat/R0_imil*R_(36)
      if(R0_omil.gt.R0_emil) then
         R0_omil=R0_emil
      elseif(R0_omil.lt.R0_umil) then
         R0_omil=R0_umil
      endif
C control_th.fgi( 257,  58):����������
      L_elil=R0_omil.gt.R0_alil
C control_th.fgi( 271,  63):���������� >
C sav1=L_(96)
      L_(96) = L_elil.OR.L_(95)
C control_th.fgi( 283,  62):recalc:���
C if(sav1.ne.L_(96) .and. try424.gt.0) goto 424
      L_(94)=R0_ulil.lt.R0_ekil
C control_th.fgi( 271,  36):���������� <
      L_(92) = L_ikil.OR.L_(94)
C control_th.fgi( 283,  41):���
      L_(91)=L_(92).and..not.L0_ufil
      L0_ufil=L_(92)
C control_th.fgi( 290,  41):������������  �� 1 ���
      L_(89) = L_(91).OR.L0_ofil.OR.L_ufol
C control_th.fgi( 199,  35):���
      if(L_(89)) then
         I0_efil=0
      endif
      I_(85)=I0_efil
      L_(79)=I0_efil.ne.0
C control_th.fgi( 208,  37):���������� ��������� 
      if(L_efum) then
         I0_apil=I_(85)
      endif
      I_(87)=I0_apil
C control_th.fgi( 217,  36):�������-��������
      L_(100)=I_(87).eq.1
      L_(99)=I_(87).eq.2
      L_(98)=I_(87).eq.3
      L_(97)=I_(87).eq.4
C control_th.fgi( 223,  37):���������� �� 4
      I_(82)=0
      if(L_(100))I_(82)=1
C control_th.fgi( 233,  40):lo2digit
      I_(81)=0
      if(L_(99))I_(81)=-1
C control_th.fgi( 233,  38):lo2digit
      I_(79) = I_(82) + I_(81)
C control_th.fgi( 238,  39):��������
      R_(35)=I_(79)
C control_th.fgi( 245,  39):��������� IN->RE
      R0_ulil=R0_ulil+deltat/R0_olil*R_(35)
      if(R0_ulil.gt.R0_ilil) then
         R0_ulil=R0_ilil
      elseif(R0_ulil.lt.R0_amil) then
         R0_ulil=R0_amil
      endif
C control_th.fgi( 257,  37):����������
      L_ikil=R0_ulil.gt.R0_okil
C control_th.fgi( 271,  42):���������� >
C sav1=L_(92)
      L_(92) = L_ikil.OR.L_(94)
C control_th.fgi( 283,  41):recalc:���
C if(sav1.ne.L_(92) .and. try462.gt.0) goto 462
      L_(78) =.NOT.(L_elil.OR.L_ikil)
C control_th.fgi(  50, 157):���
      L_(120) = L_(115).OR.L_(78)
C control_th.fgi(  58, 164):���
      if(R_(41).ge.0.0) then
         R_(42)=R_ifol/max(R_(41),1.0e-10)
      else
         R_(42)=R_ifol/min(R_(41),-1.0e-10)
      endif
C control_th.fgi( 135, 189):�������� ����������
      I_adol=R_(42)
C control_th.fgi( 144, 189):��������� RE->IN
      R_(37)=I_adol
C control_th.fgi(  70, 119):��������� IN->RE
      L_(108)=R_(37).gt.R0_otil
C control_th.fgi(  83, 119):���������� >
      L_(107)=R_(37).lt.R0_itil
C control_th.fgi(  83, 113):���������� <
      L_alom = L_(108).AND.L_(107)
C control_th.fgi(  99, 118):�
      L_(114)=R_(37).gt.R0_uvil
C control_th.fgi(  83, 107):���������� >
      L_(113)=R_(37).lt.R0_ovil
C control_th.fgi(  83, 101):���������� <
      L_ulom = L_(114).AND.L_(113)
C control_th.fgi(  99, 106):�
      L_(112)=R_(37).gt.R0_ivil
C control_th.fgi(  83,  95):���������� >
      L_(111)=R_(37).lt.R0_evil
C control_th.fgi(  83,  89):���������� <
      L_opim = L_(112).AND.L_(111)
C control_th.fgi(  99,  94):�
      L_(110)=R_(37).gt.R0_avil
C control_th.fgi(  83,  83):���������� >
      L_(109)=R_(37).lt.R0_util
C control_th.fgi(  83,  77):���������� <
      L_irim = L_(110).AND.L_(109)
C control_th.fgi(  99,  82):�
      L_odol = L_alom.OR.L_ulom.OR.L_opim.OR.L_irim
C control_th.fgi( 117,  72):���
      I_(94)=0
      if(L_alom)I_(94)=1
C control_th.fgi( 126,  66):lo2digit
      I_(93)=0
      if(L_ulom)I_(93)=2
C control_th.fgi( 126,  64):lo2digit
      I_(92)=0
      if(L_opim)I_(92)=3
C control_th.fgi( 126,  62):lo2digit
      I_(91)=0
      if(L_irim)I_(91)=4
C control_th.fgi( 126,  60):lo2digit
      I_ibol = I_(94) + I_(93) + I_(92) + I_(91)
C control_th.fgi( 132,  63):��������
      L_(116)=I_axom.eq.I_ibol
C control_th.fgi(  41, 186):���������� �������������
      L_ofol = L_odol.AND.L_(116)
C control_th.fgi(  50, 190):�
      if(L_ofol) then
         R_(45)=R_(39)
      else
         R_(45)=R_(40)
      endif
C control_th.fgi(  91, 194):���� RE IN LO CH7
      if(L_(119)) then
         R_ifol=R_(48)
      elseif(L_(120)) then
         R_ifol=R_ifol
      else
         R_ifol=R_ifol+deltat/R_(45)*R_(47)
      endif
      if(R_ifol.gt.R_(44)) then
         R_ifol=R_(44)
      elseif(R_ifol.lt.R_(46)) then
         R_ifol=R_(46)
      endif
C control_th.fgi( 113, 180):����������
C sav1=R_(43)
      R_(43) = R_avom + (-R_ifol)
C control_th.fgi(  58, 178):recalc:��������
C if(sav1.ne.R_(43) .and. try406.gt.0) goto 406
      R_ibum=R_ifol
C control_th.fgi( 159, 180):������,truck_x_coord_mm
C sav1=R_(50)
      R_(50) = (-R_(1)) + R_ibum
C control_th.fgi( 134, 283):recalc:��������
C if(sav1.ne.R_(50) .and. try393.gt.0) goto 393
      R0_abum=(R_ibum-R_(60)+R0_oxom*R0_abum)/(R0_oxom+deltat
     &)
      R0_uxom=R_ibum
C control_th.fgi(  85, 215):���������������� ����� 
      if(R0_abum.gt.R0_atom) then
         R_(49)=R0_abum-R0_atom
      elseif(R0_abum.le.-R0_atom) then
         R_(49)=R0_abum+R0_atom
      else
         R_(49)=0.0
      endif
C control_th.fgi(  99, 215):���� ������������������
      R_ixom=abs(R_(49))
C control_th.fgi( 112, 215):���������� ��������
      L_ufol=R_ixom.gt.R0_eril
C control_th.fgi( 126, 207):���������� >
C sav1=L_(90)
      L_(90) = L_(93).OR.L0_ofil.OR.L_ufol
C control_th.fgi( 199,  56):recalc:���
C if(sav1.ne.L_(90) .and. try428.gt.0) goto 428
C sav1=L_(89)
      L_(89) = L_(91).OR.L0_ofil.OR.L_ufol
C control_th.fgi( 199,  35):recalc:���
C if(sav1.ne.L_(89) .and. try466.gt.0) goto 466
      if(L_ufol) then
         I0_edil=0
      endif
      I_(68)=I0_edil
      L_(77)=I0_edil.ne.0
C control_tg.fgi(  38, 267):���������� ��������� 
      if(L_efum) then
         I0_ikak=I_(68)
      endif
      I_ovel=I0_ikak
C control_tg.fgi(  56, 266):�������-��������
      L_ivel=I_ovel.eq.1
      L_(71)=I_ovel.eq.2
      L_(70)=I_ovel.eq.3
      L_ipum=I_ovel.eq.4
C control_tg.fgi(  73, 267):���������� �� 4
      if(L_ivel) then
         R_(32)=R_(33)
      endif
C control_tg.fgi(  80, 283):���� � ������������� �������
      L_imum=I_ovel.eq.I0_apal
C control_tg.fgi(  74, 249):���������� �������������
      L_aruk=R_uxel.gt.R0_omak
C control_tg.fgi( 118, 105):���������� >
      L_(29)=L_imum.and..not.L0_apak
      L0_apak=L_imum
C control_tg.fgi(  85, 244):������������  �� 1 ���
      L0_epak=(L_(29).or.L0_epak).and..not.(L_aruk)
      L_(30)=.not.L0_epak
C control_tg.fgi(  94, 242):RS �������
      L_(31) = L_imum.AND.L_(30)
C control_tg.fgi(  84, 259):�
      L_evel = L_(71).OR.L_ipum.OR.L_(31)
C control_tg.fgi(  90, 264):���
      if(L_evel) then
         R_(32)=R_(31)
      endif
C control_tg.fgi(  93, 283):���� � ������������� �������
      L_avel = L_(70).OR.L0_epak
C control_tg.fgi( 102, 265):���
      if(L_avel) then
         R_(32)=R_(30)
      endif
C control_tg.fgi( 107, 283):���� � ������������� �������
      R_(3)=R_(32)
C control_tg.fgi(  80, 283):������-�������: ���������� ��� �������������� ������
      if(L_ufol) then
         I0_adil=0
      endif
      I_(67)=I0_adil
      L_(76)=I0_adil.ne.0
C control_tg.fgi(  38, 206):���������� ��������� 
      if(L_efum) then
         I0_okak=I_(67)
      endif
      I_(64)=I0_okak
C control_tg.fgi(  56, 205):�������-��������
      L_(69)=I_(64).eq.1
      L_otel=I_(64).eq.2
      L_itel=I_(64).eq.3
      L_etel=I_(64).eq.4
C control_tg.fgi(  73, 206):���������� �� 4
      if(L_(69)) then
         R_(29)=R_(26)
      endif
C control_tg.fgi(  80, 216):���� � ������������� �������
      if(L_otel) then
         R_(29)=R_(28)
      endif
C control_tg.fgi(  94, 216):���� � ������������� �������
      if(L_itel) then
         R_(29)=R_(27)
      endif
C control_tg.fgi( 109, 216):���� � ������������� �������
      R_(25) = (-R_(3)) + R_uxel
C control_tg.fgi( 130, 283):��������
      if(L_etel) then
         R_(29)=R_(25)
      endif
C control_tg.fgi( 119, 216):���� � ������������� �������
      R_(4)=R_(29)
C control_tg.fgi(  80, 216):������-�������: ���������� ��� �������������� ������
      R_utel = R_(3) + R_(4)
C control_tg.fgi( 130, 287):��������
      R_(18) = R_utel + (-R_ubel)
C control_tg.fgi(  33, 169):��������
      L_umum=R_(18).gt.R0_uxal
C control_tg.fgi(  44, 173):���������� >
      I_(56)=0
      if(L_umum)I_(56)=1
C control_tg.fgi(  73, 173):lo2digit
      L_(44)=R_(18).lt.R0_oxal
C control_tg.fgi(  44, 167):���������� <
      I_(55)=0
      if(L_(44))I_(55)=-1
C control_tg.fgi(  73, 167):lo2digit
      I_(57) = I_(56) + I_(55)
C control_tg.fgi(  89, 170):��������
      R_(22) = R_(12) * I_(57)
C control_tg.fgi(  98, 171):����������
      L_(19)=R0_afak.lt.R0_ebak
C control_tg.fgi( 275,  35):���������� <
      L_(20) = L_obak.OR.L_(19)
C control_tg.fgi( 287,  40):���
      L_(17)=L_(20).and..not.L0_ixuf
      L0_ixuf=L_(20)
C control_tg.fgi( 294,  40):������������  �� 1 ���
      R0_ixel=(R_uxel-R_(34)+R0_axel*R0_ixel)/(R0_axel+deltat
     &)
      R0_exel=R_uxel
C control_tg.fgi(  41, 113):���������������� ����� 
      if(R0_ixel.gt.R0_usel) then
         R_(24)=R0_ixel-R0_usel
      elseif(R0_ixel.le.-R0_usel) then
         R_(24)=R0_ixel+R0_usel
      else
         R_(24)=0.0
      endif
C control_tg.fgi(  55, 113):���� ������������������
      R_uvel=abs(R_(24))
C control_tg.fgi(  68, 113):���������� ��������
      L_ikel=R_uvel.gt.R0_umak
C control_tg.fgi( 118,  99):���������� >
      L_(75) = L_(17).OR.L0_abil.OR.L_ikel
C control_tg.fgi( 193,  34):���
      if(L_(75)) then
         I0_ubil=0
      endif
      I_(66)=I0_ubil
      L_(74)=I0_ubil.ne.0
C control_tg.fgi( 204,  36):���������� ��������� 
      if(L_efum) then
         I0_ofak=I_(66)
      endif
      I_(59)=I0_ofak
C control_tg.fgi( 219,  35):�������-��������
      L_(54)=I_(59).eq.1
      L_(53)=I_(59).eq.2
      L_(52)=I_(59).eq.3
      L_(51)=I_(59).eq.4
C control_tg.fgi( 227,  36):���������� �� 4
      I_(36)=0
      if(L_(54))I_(36)=1
C control_tg.fgi( 237,  39):lo2digit
      I_(35)=0
      if(L_(53))I_(35)=-1
C control_tg.fgi( 237,  37):lo2digit
      I_(32) = I_(36) + I_(35)
C control_tg.fgi( 242,  38):��������
      R_(6)=I_(32)
C control_tg.fgi( 249,  38):��������� IN->RE
      R0_afak=R0_afak+deltat/R0_udak*R_(6)
      if(R0_afak.gt.R0_odak) then
         R0_afak=R0_odak
      elseif(R0_afak.lt.R0_efak) then
         R0_afak=R0_efak
      endif
C control_tg.fgi( 261,  36):����������
      L_obak=R0_afak.gt.R0_ibak
C control_tg.fgi( 275,  41):���������� >
C sav1=L_(20)
      L_(20) = L_obak.OR.L_(19)
C control_tg.fgi( 287,  40):recalc:���
C if(sav1.ne.L_(20) .and. try783.gt.0) goto 783
      L_(18)=R0_edak.lt.R0_oxuf
C control_tg.fgi( 275,  14):���������� <
      L_(16) = L_uxuf.OR.L_(18)
C control_tg.fgi( 287,  19):���
      L_(15)=L_(16).and..not.L0_exuf
      L0_exuf=L_(16)
C control_tg.fgi( 294,  19):������������  �� 1 ���
      L_(73) = L_(15).OR.L0_abil.OR.L_ikel
C control_tg.fgi( 193,  13):���
      if(L_(73)) then
         I0_obil=0
      endif
      I_(65)=I0_obil
      L_(72)=I0_obil.ne.0
C control_tg.fgi( 204,  15):���������� ��������� 
      if(L_efum) then
         I0_ifak=I_(65)
      endif
      I_(58)=I0_ifak
C control_tg.fgi( 219,  14):�������-��������
      L_(50)=I_(58).eq.1
      L_(49)=I_(58).eq.2
      L_(48)=I_(58).eq.3
      L_(47)=I_(58).eq.4
C control_tg.fgi( 227,  15):���������� �� 4
      I_(34)=0
      if(L_(50))I_(34)=1
C control_tg.fgi( 237,  18):lo2digit
      I_(33)=0
      if(L_(49))I_(33)=-1
C control_tg.fgi( 237,  16):lo2digit
      I_(31) = I_(34) + I_(33)
C control_tg.fgi( 242,  17):��������
      R_(5)=I_(31)
C control_tg.fgi( 249,  17):��������� IN->RE
      R0_edak=R0_edak+deltat/R0_adak*R_(5)
      if(R0_edak.gt.R0_ubak) then
         R0_edak=R0_ubak
      elseif(R0_edak.lt.R0_idak) then
         R0_edak=R0_idak
      endif
C control_tg.fgi( 261,  15):����������
      L_uxuf=R0_edak.gt.R0_abak
C control_tg.fgi( 275,  20):���������� >
C sav1=L_(16)
      L_(16) = L_uxuf.OR.L_(18)
C control_tg.fgi( 287,  19):recalc:���
C if(sav1.ne.L_(16) .and. try850.gt.0) goto 850
      L_(6) =.NOT.(L_obak.OR.L_uxuf)
C control_tg.fgi(  73, 145):���
      L_(46) = L_(39).OR.L_(6)
C control_tg.fgi(  87, 157):���
      if(L_(45)) then
         R_ubel=R_(23)
      elseif(L_(46)) then
         R_ubel=R_ubel
      else
         R_ubel=R_ubel+deltat/R_(20)*R_(22)
      endif
      if(R_ubel.gt.R_(19)) then
         R_ubel=R_(19)
      elseif(R_ubel.lt.R_(21)) then
         R_ubel=R_(21)
      endif
C control_tg.fgi( 109, 171):����������
C sav1=R_(18)
      R_(18) = R_utel + (-R_ubel)
C control_tg.fgi(  33, 169):recalc:��������
C if(sav1.ne.R_(18) .and. try761.gt.0) goto 761
      R_uxel=R_ubel
C control_tg.fgi( 155, 171):������,travers_y_coord_mm
C sav1=R_(25)
      R_(25) = (-R_(3)) + R_uxel
C control_tg.fgi( 130, 283):recalc:��������
C if(sav1.ne.R_(25) .and. try748.gt.0) goto 748
C sav1=R0_ixel
      R0_ixel=(R_uxel-R_(34)+R0_axel*R0_ixel)/(R0_axel+deltat
     &)
      R0_exel=R_uxel
C control_tg.fgi(  41, 113):recalc:���������������� ����� 
C if(sav1.ne.R0_ixel .and. try787.gt.0) goto 787
      L_(36)=R_uxel.gt.R0_arak
C control_tg.fgi( 118, 129):���������� >
      L_(35)=R_uxel.lt.R0_upak
C control_tg.fgi( 118, 123):���������� <
      L_ifom = L_(36).AND.L_(35)
C control_tg.fgi( 134, 128):�
C sav1=L_(154)
      L_(154) = (.NOT.L_ifom)
C control_th.fgi(  25, 265):recalc:���
C if(sav1.ne.L_(154) .and. try306.gt.0) goto 306
      L_(150) = (.NOT.L_ifom)
C control_th.fgi( 362, 258):���
      L_edal = (.NOT.L_(36))
C control_tg.fgi( 134, 134):���
      if(R_uxel.gt.R0_atel) then
         R_oxel=R_uxel-R0_atel
      elseif(R_uxel.le.-R0_atel) then
         R_oxel=R_uxel+R0_atel
      else
         R_oxel=0.0
      endif
C control_tg.fgi(  55, 122):���� ������������������
      L_(38)=R_uxel.gt.R0_irak
C control_tg.fgi( 118, 117):���������� >
      L_esuk = (.NOT.L_(35)).AND.(.NOT.L_(38))
C control_tg.fgi( 134, 122):�
      L_(37)=R_uxel.lt.R0_erak
C control_tg.fgi( 118, 111):���������� <
      L_iruk = L_(38).AND.L_(37)
C control_tg.fgi( 134, 112):�
      L0_umal=.false.
      I0_omal=0

      if(L0_ekap)then
        I0_odil=1
         L0_imal=.false.
         L0_adal=.false.
         L0_usuk=.false.
         L0_asuk=.false.
         L0_oruk=.false.
         L0_eruk=.false.
         L0_upuk=.false.
         L0_ipuk=.false.
         L0_apuk=.false.
         L0_amal=.false.
         L0_olal=.false.
         L0_elal=.false.
         L0_ukal=.false.
         L0_ikal=.false.
         L0_akal=.false.
         L0_ofal=.false.
         L0_efal=.false.
         L0_udal=.false.
         L0_idal=.false.
         L0_obal=.false.
         L0_ebal=.false.
         L0_uxuk=.false.
         L0_ixuk=.false.
         L0_axuk=.false.
         L0_ovuk=.false.
         L0_evuk=.false.
         L0_utuk=.false.
         L0_ituk=.false.
         L0_atuk=.false.
         L0_isuk=.false.
      endif

      if(L0_imal)I0_omal=I0_omal+1
      if(L0_adal)I0_omal=I0_omal+1
      if(L0_usuk)I0_omal=I0_omal+1
      if(L0_asuk)I0_omal=I0_omal+1
      if(L0_oruk)I0_omal=I0_omal+1
      if(L0_eruk)I0_omal=I0_omal+1
      if(L0_upuk)I0_omal=I0_omal+1
      if(L0_ipuk)I0_omal=I0_omal+1
      if(L0_apuk)I0_omal=I0_omal+1
      if(L0_amal)I0_omal=I0_omal+1
      if(L0_olal)I0_omal=I0_omal+1
      if(L0_elal)I0_omal=I0_omal+1
      if(L0_ukal)I0_omal=I0_omal+1
      if(L0_ikal)I0_omal=I0_omal+1
      if(L0_akal)I0_omal=I0_omal+1
      if(L0_ofal)I0_omal=I0_omal+1
      if(L0_efal)I0_omal=I0_omal+1
      if(L0_udal)I0_omal=I0_omal+1
      if(L0_idal)I0_omal=I0_omal+1
      if(L0_obal)I0_omal=I0_omal+1
      if(L0_ebal)I0_omal=I0_omal+1
      if(L0_uxuk)I0_omal=I0_omal+1
      if(L0_ixuk)I0_omal=I0_omal+1
      if(L0_axuk)I0_omal=I0_omal+1
      if(L0_ovuk)I0_omal=I0_omal+1
      if(L0_evuk)I0_omal=I0_omal+1
      if(L0_utuk)I0_omal=I0_omal+1
      if(L0_ituk)I0_omal=I0_omal+1
      if(L0_atuk)I0_omal=I0_omal+1
      if(L0_isuk)I0_omal=I0_omal+1


      if(.not.L0_umal.and.L_(40).and..not.L0_imal)then
        I0_odil=1
        L0_umal=.true.
      endif

      if(.not.L0_umal.and.L_edal.and..not.L0_adal)then
        I0_odil=2
        L0_umal=.true.
      endif

      if(.not.L0_umal.and.L_ifom.and..not.L0_usuk)then
        I0_odil=3
        L0_umal=.true.
      endif

      if(.not.L0_umal.and.L_esuk.and..not.L0_asuk)then
        I0_odil=4
        L0_umal=.true.
      endif

      if(.not.L0_umal.and.L_uruk.and..not.L0_oruk)then
        I0_odil=5
        L0_umal=.true.
      endif

      if(.not.L0_umal.and.L_iruk.and..not.L0_eruk)then
        I0_odil=6
        L0_umal=.true.
      endif

      if(.not.L0_umal.and.L_aruk.and..not.L0_upuk)then
        I0_odil=7
        L0_umal=.true.
      endif

      if(.not.L0_umal.and.L0_opuk.and..not.L0_ipuk)then
        I0_odil=8
        L0_umal=.true.
      endif

      if(.not.L0_umal.and.L0_epuk.and..not.L0_apuk)then
        I0_odil=9
        L0_umal=.true.
      endif

      if(.not.L0_umal.and.L0_emal.and..not.L0_amal)then
        I0_odil=10
        L0_umal=.true.
      endif

      if(.not.L0_umal.and.L0_ulal.and..not.L0_olal)then
        I0_odil=11
        L0_umal=.true.
      endif

      if(.not.L0_umal.and.L0_ilal.and..not.L0_elal)then
        I0_odil=12
        L0_umal=.true.
      endif

      if(.not.L0_umal.and.L0_alal.and..not.L0_ukal)then
        I0_odil=13
        L0_umal=.true.
      endif

      if(.not.L0_umal.and.L0_okal.and..not.L0_ikal)then
        I0_odil=14
        L0_umal=.true.
      endif

      if(.not.L0_umal.and.L0_ekal.and..not.L0_akal)then
        I0_odil=15
        L0_umal=.true.
      endif

      if(.not.L0_umal.and.L0_ufal.and..not.L0_ofal)then
        I0_odil=16
        L0_umal=.true.
      endif

      if(.not.L0_umal.and.L0_ifal.and..not.L0_efal)then
        I0_odil=17
        L0_umal=.true.
      endif

      if(.not.L0_umal.and.L0_afal.and..not.L0_udal)then
        I0_odil=18
        L0_umal=.true.
      endif

      if(.not.L0_umal.and.L0_odal.and..not.L0_idal)then
        I0_odil=19
        L0_umal=.true.
      endif

      if(.not.L0_umal.and.L0_ubal.and..not.L0_obal)then
        I0_odil=20
        L0_umal=.true.
      endif

      if(.not.L0_umal.and.L0_ibal.and..not.L0_ebal)then
        I0_odil=21
        L0_umal=.true.
      endif

      if(.not.L0_umal.and.L0_abal.and..not.L0_uxuk)then
        I0_odil=22
        L0_umal=.true.
      endif

      if(.not.L0_umal.and.L0_oxuk.and..not.L0_ixuk)then
        I0_odil=23
        L0_umal=.true.
      endif

      if(.not.L0_umal.and.L0_exuk.and..not.L0_axuk)then
        I0_odil=24
        L0_umal=.true.
      endif

      if(.not.L0_umal.and.L0_uvuk.and..not.L0_ovuk)then
        I0_odil=25
        L0_umal=.true.
      endif

      if(.not.L0_umal.and.L0_ivuk.and..not.L0_evuk)then
        I0_odil=26
        L0_umal=.true.
      endif

      if(.not.L0_umal.and.L0_avuk.and..not.L0_utuk)then
        I0_odil=27
        L0_umal=.true.
      endif

      if(.not.L0_umal.and.L0_otuk.and..not.L0_ituk)then
        I0_odil=28
        L0_umal=.true.
      endif

      if(.not.L0_umal.and.L0_etuk.and..not.L0_atuk)then
        I0_odil=29
        L0_umal=.true.
      endif

      if(.not.L0_umal.and.L0_osuk.and..not.L0_isuk)then
        I0_odil=30
        L0_umal=.true.
      endif

      if(I0_omal.eq.1)then
         L0_imal=.false.
         L0_adal=.false.
         L0_usuk=.false.
         L0_asuk=.false.
         L0_oruk=.false.
         L0_eruk=.false.
         L0_upuk=.false.
         L0_ipuk=.false.
         L0_apuk=.false.
         L0_amal=.false.
         L0_olal=.false.
         L0_elal=.false.
         L0_ukal=.false.
         L0_ikal=.false.
         L0_akal=.false.
         L0_ofal=.false.
         L0_efal=.false.
         L0_udal=.false.
         L0_idal=.false.
         L0_obal=.false.
         L0_ebal=.false.
         L0_uxuk=.false.
         L0_ixuk=.false.
         L0_axuk=.false.
         L0_ovuk=.false.
         L0_evuk=.false.
         L0_utuk=.false.
         L0_ituk=.false.
         L0_atuk=.false.
         L0_isuk=.false.
      else
         L0_imal=L_(40)
         L0_adal=L_edal
         L0_usuk=L_ifom
         L0_asuk=L_esuk
         L0_oruk=L_uruk
         L0_eruk=L_iruk
         L0_upuk=L_aruk
         L0_ipuk=L0_opuk
         L0_apuk=L0_epuk
         L0_amal=L0_emal
         L0_olal=L0_ulal
         L0_elal=L0_ilal
         L0_ukal=L0_alal
         L0_ikal=L0_okal
         L0_akal=L0_ekal
         L0_ofal=L0_ufal
         L0_efal=L0_ifal
         L0_udal=L0_afal
         L0_idal=L0_odal
         L0_obal=L0_ubal
         L0_ebal=L0_ibal
         L0_uxuk=L0_abal
         L0_ixuk=L0_oxuk
         L0_axuk=L0_exuk
         L0_ovuk=L0_uvuk
         L0_evuk=L0_ivuk
         L0_utuk=L0_avuk
         L0_ituk=L0_otuk
         L0_atuk=L0_etuk
         L0_isuk=L0_osuk
      endif
C control_tg.fgi( 371, 261):��������� �������
      select case (I0_odil)
      case (1)
        C255_urel="��������� ������������"
      case (2)
        C255_urel="������� ������������"
      case (3)
        C255_urel="������� ���������"
      case (4)
        C255_urel="� ������� ���������"
      case (5)
        C255_urel="������ ��������� �������"
      case (6)
        C255_urel="��������� �������"
      case (7)
        C255_urel="������ ������������"
      case (8)
        C255_urel="???"
      case (9)
        C255_urel="???"
      case (10)
        C255_urel="???"
      case (11)
        C255_urel="???"
      case (12)
        C255_urel="???"
      case (13)
        C255_urel="???"
      case (14)
        C255_urel="???"
      case (15)
        C255_urel="???"
      case (16)
        C255_urel="???"
      case (17)
        C255_urel="???"
      case (18)
        C255_urel="???"
      case (19)
        C255_urel="???"
      case (20)
        C255_urel="???"
      case (21)
        C255_urel="???"
      case (22)
        C255_urel="???"
      case (23)
        C255_urel="???"
      case (24)
        C255_urel="???"
      case (25)
        C255_urel="???"
      case (26)
        C255_urel="???"
      case (27)
        C255_urel="???"
      case (28)
        C255_urel="???"
      case (29)
        C255_urel="???"
      case (30)
        C255_urel="???"
      end select
C control_tg.fgi( 386, 290):MarkVI text out 30 variants,tg_text1
      L_(27) = L_ipum.AND.L_iruk
C control_tg.fgi( 109, 235):�
      R_(16) = R_(19) + (-R_ubel)
C control_tg.fgi( 124, 163):��������
      if(R_(15).ge.0.0) then
         R_(17)=R_(16)/max(R_(15),1.0e-10)
      else
         R_(17)=R_(16)/min(R_(15),-1.0e-10)
      endif
C control_tg.fgi( 131, 161):�������� ����������
      I_ixal=R_(17)
C control_tg.fgi( 140, 161):��������� RE->IN
      L_(13) =.NOT.(L_uxuf)
C control_tg.fgi( 335,  12):���
      I_(28)=0
      if(L_(13))I_(28)=1
C control_tg.fgi( 341,  12):lo2digit
      I_(27)=0
      if(L_uxuf)I_(27)=2
C control_tg.fgi( 341,  10):lo2digit
      I_(62) = I_(28) + I_(27)
C control_tg.fgi( 347,  11):��������
      select case (I_(62))
      case (1)
        C255_upel="�����"
      case (2)
        C255_upel="� ������"
      case (3)
        C255_upel="� ������"
      case (4)
        C255_upel="��������"
      case (5)
        C255_upel="�� ����������"
      case (6)
        C255_upel="����� ������ � ����������"
      case (7)
        C255_upel="����� ������ ����������"
      case (8)
        C255_upel="???"
      case (9)
        C255_upel="???"
      case (10)
        C255_upel="???"
      case (11)
        C255_upel="???"
      case (12)
        C255_upel="???"
      case (13)
        C255_upel="???"
      case (14)
        C255_upel="???"
      case (15)
        C255_upel="???"
      case (16)
        C255_upel="???"
      case (17)
        C255_upel="???"
      case (18)
        C255_upel="???"
      case (19)
        C255_upel="???"
      case (20)
        C255_upel="???"
      case (21)
        C255_upel="???"
      case (22)
        C255_upel="???"
      case (23)
        C255_upel="???"
      case (24)
        C255_upel="???"
      case (25)
        C255_upel="???"
      case (26)
        C255_upel="???"
      case (27)
        C255_upel="???"
      case (28)
        C255_upel="???"
      case (29)
        C255_upel="???"
      case (30)
        C255_upel="???"
      end select
C control_tg.fgi( 361,  11):MarkVI text out 30 variants,tg_text7
      L_(9) = L_(50).AND.(.NOT.L_uxuf)
C control_tg.fgi( 304,  23):�
      I_(23)=0
      if(L_(9))I_(23)=8
C control_tg.fgi( 315,  24):lo2digit
      L_(8) = (.NOT.L_(18)).AND.L_(49)
C control_tg.fgi( 304,  11):�
      I_(22)=0
      if(L_(8))I_(22)=10
C control_tg.fgi( 315,  11):lo2digit
      L_(7) = (.NOT.L_(9)).AND.(.NOT.L_(8))
C control_tg.fgi( 310,  20):�
      I_(21)=0
      if(L_(7))I_(21)=6
C control_tg.fgi( 315,  20):lo2digit
      I_(60) = I_(23) + I_(22) + I_(21)
C control_tg.fgi( 323,  22):��������
      select case (I_(60))
      case (1)
        C255_ipel="��� �����"
      case (2)
        C255_ipel="���������� � UZ"
      case (3)
        C255_ipel="������������"
      case (4)
        C255_ipel="��� 380�"
      case (5)
        C255_ipel="�������������"
      case (6)
        C255_ipel="����� � ������"
      case (7)
        C255_ipel="����� ����"
      case (8)
        C255_ipel="������ ������"
      case (9)
        C255_ipel="������ �� ����"
      case (10)
        C255_ipel="������ �����"
      case (11)
        C255_ipel="����� �������"
      case (12)
        C255_ipel="����������������"
      case (13)
        C255_ipel="����� ����"
      case (14)
        C255_ipel="����� ������� �����"
      case (15)
        C255_ipel="������ �������"
      case (16)
        C255_ipel="������ ����������"
      case (17)
        C255_ipel="�� ����������"
      case (18)
        C255_ipel="???"
      case (19)
        C255_ipel="???"
      case (20)
        C255_ipel="???"
      case (21)
        C255_ipel="???"
      case (22)
        C255_ipel="???"
      case (23)
        C255_ipel="???"
      case (24)
        C255_ipel="???"
      case (25)
        C255_ipel="???"
      case (26)
        C255_ipel="???"
      case (27)
        C255_ipel="???"
      case (28)
        C255_ipel="???"
      case (29)
        C255_ipel="???"
      case (30)
        C255_ipel="???"
      end select
C control_tg.fgi( 361,  22):MarkVI text out 30 variants,tg_text9
      L_(14) =.NOT.(L_obak)
C control_tg.fgi( 335,  39):���
      I_(30)=0
      if(L_(14))I_(30)=1
C control_tg.fgi( 341,  39):lo2digit
      I_(29)=0
      if(L_obak)I_(29)=2
C control_tg.fgi( 341,  37):lo2digit
      I_(63) = I_(30) + I_(29)
C control_tg.fgi( 347,  38):��������
      select case (I_(63))
      case (1)
        C255_arel="�����"
      case (2)
        C255_arel="� ������"
      case (3)
        C255_arel="� ������"
      case (4)
        C255_arel="��������"
      case (5)
        C255_arel="�� ����������"
      case (6)
        C255_arel="����� ������ � ����������"
      case (7)
        C255_arel="����� ������ ����������"
      case (8)
        C255_arel="???"
      case (9)
        C255_arel="???"
      case (10)
        C255_arel="???"
      case (11)
        C255_arel="???"
      case (12)
        C255_arel="???"
      case (13)
        C255_arel="???"
      case (14)
        C255_arel="???"
      case (15)
        C255_arel="???"
      case (16)
        C255_arel="???"
      case (17)
        C255_arel="???"
      case (18)
        C255_arel="???"
      case (19)
        C255_arel="???"
      case (20)
        C255_arel="???"
      case (21)
        C255_arel="???"
      case (22)
        C255_arel="???"
      case (23)
        C255_arel="???"
      case (24)
        C255_arel="???"
      case (25)
        C255_arel="???"
      case (26)
        C255_arel="???"
      case (27)
        C255_arel="???"
      case (28)
        C255_arel="???"
      case (29)
        C255_arel="???"
      case (30)
        C255_arel="???"
      end select
C control_tg.fgi( 361,  38):MarkVI text out 30 variants,tg_text6
      L_(12) = L_(54).AND.(.NOT.L_obak)
C control_tg.fgi( 304,  44):�
      I_(26)=0
      if(L_(12))I_(26)=8
C control_tg.fgi( 315,  45):lo2digit
      L_(11) = (.NOT.L_(19)).AND.L_(53)
C control_tg.fgi( 304,  32):�
      I_(25)=0
      if(L_(11))I_(25)=10
C control_tg.fgi( 315,  32):lo2digit
      L_(10) = (.NOT.L_(12)).AND.(.NOT.L_(11))
C control_tg.fgi( 310,  41):�
      I_(24)=0
      if(L_(10))I_(24)=6
C control_tg.fgi( 315,  41):lo2digit
      I_(61) = I_(26) + I_(25) + I_(24)
C control_tg.fgi( 323,  43):��������
      select case (I_(61))
      case (1)
        C255_opel="��� �����"
      case (2)
        C255_opel="���������� � UZ"
      case (3)
        C255_opel="������������"
      case (4)
        C255_opel="��� 380�"
      case (5)
        C255_opel="�������������"
      case (6)
        C255_opel="����� � ������"
      case (7)
        C255_opel="����� ����"
      case (8)
        C255_opel="������ ������"
      case (9)
        C255_opel="������ �� ����"
      case (10)
        C255_opel="������ �����"
      case (11)
        C255_opel="����� �������"
      case (12)
        C255_opel="����������������"
      case (13)
        C255_opel="����� ����"
      case (14)
        C255_opel="����� ������� �����"
      case (15)
        C255_opel="������ �������"
      case (16)
        C255_opel="������ ����������"
      case (17)
        C255_opel="�� ����������"
      case (18)
        C255_opel="???"
      case (19)
        C255_opel="???"
      case (20)
        C255_opel="???"
      case (21)
        C255_opel="???"
      case (22)
        C255_opel="???"
      case (23)
        C255_opel="???"
      case (24)
        C255_opel="???"
      case (25)
        C255_opel="???"
      case (26)
        C255_opel="???"
      case (27)
        C255_opel="???"
      case (28)
        C255_opel="???"
      case (29)
        C255_opel="???"
      case (30)
        C255_opel="???"
      end select
C control_tg.fgi( 361,  49):MarkVI text out 30 variants,tg_text8
      L_(66) = L_ikel.AND.L_ekel.AND.(.NOT.L_itel).AND.(.NOT.L_otel
     &)
C control_tg.fgi( 237, 242):�
      L_okel = (.NOT.L_amel).AND.L_(66)
C control_tg.fgi( 244, 243):�
      L_(65) = L_itel.AND.L_ikel.AND.(.NOT.L_(64))
C control_tg.fgi( 237, 233):�
      L_alel = (.NOT.L_amel).AND.L_(65)
C control_tg.fgi( 244, 234):�
      L_(63) = L_ikel.AND.L_otel.AND.(.NOT.L_(64))
C control_tg.fgi( 237, 227):�
      L_ilel = (.NOT.L_amel).AND.L_(63)
C control_tg.fgi( 244, 228):�
      L_(62) = L_(64).AND.L_ikel
C control_tg.fgi( 237, 222):�
      L_elel = (.NOT.L_amel).AND.L_(62)
C control_tg.fgi( 244, 223):�
      L_(59) = L_ikel.AND.L_ekel.AND.(.NOT.L_itel).AND.(.NOT.L_otel
     &)
C control_tg.fgi( 237, 151):�
      L_adel = (.NOT.L_ifel).AND.L_(59)
C control_tg.fgi( 244, 152):�
      L_(58) = L_itel.AND.L_ikel.AND.(.NOT.L_(57))
C control_tg.fgi( 237, 142):�
      L_idel = (.NOT.L_ifel).AND.L_(58)
C control_tg.fgi( 244, 143):�
      L_(56) = L_ikel.AND.L_otel.AND.(.NOT.L_(57))
C control_tg.fgi( 237, 136):�
      L_udel = (.NOT.L_ifel).AND.L_(56)
C control_tg.fgi( 244, 137):�
      L_(55) = L_(57).AND.L_ikel
C control_tg.fgi( 237, 131):�
      L_odel = (.NOT.L_ifel).AND.L_(55)
C control_tg.fgi( 244, 132):�
      L_atal=L_ikel
C control_tg.fgi( 284,  65):������,M4run
      if(L_atal) then
         I_(52)=I0_etal
      else
         I_(52)=I0_ital
      endif
C control_tg.fgi( 239, 106):���� RE IN LO CH7
      if(L_osal) then
         I_otal=I0_usal
      else
         I_otal=I_(52)
      endif
C control_tg.fgi( 257, 105):���� RE IN LO CH7
      L_ural = L_ikel.AND.L_(32)
C control_tg.fgi( 237,  56):�
      L_afik = L_atal.AND.L_ural
C control_tg.fgi( 225,  73):�
      L_otek = (.NOT.L_atal).AND.(.NOT.L_ural)
C control_tg.fgi( 225,  77):�
      L_usek = L_atal.AND.(.NOT.L_ural)
C control_tg.fgi( 225,  81):�
      L_ipik =.NOT.(L_afik.OR.L_otek.OR.L_usek)
C control_tg.fgi( 268,  85):���
      L0_upik=.false.
      I0_opik=0

      if(L0_ekap)then
        I0_epel=1
         L0_epik=.false.
         L0_udik=.false.
         L0_itek=.false.
         L0_osek=.false.
         L0_esek=.false.
         L0_urek=.false.
         L0_irek=.false.
         L0_arek=.false.
         L0_opek=.false.
         L0_umik=.false.
         L0_imik=.false.
         L0_amik=.false.
         L0_olik=.false.
         L0_elik=.false.
         L0_ukik=.false.
         L0_ikik=.false.
         L0_akik=.false.
         L0_ofik=.false.
         L0_efik=.false.
         L0_idik=.false.
         L0_adik=.false.
         L0_obik=.false.
         L0_ebik=.false.
         L0_uxek=.false.
         L0_ixek=.false.
         L0_axek=.false.
         L0_ovek=.false.
         L0_evek=.false.
         L0_utek=.false.
         L0_atek=.false.
      endif

      if(L0_epik)I0_opik=I0_opik+1
      if(L0_udik)I0_opik=I0_opik+1
      if(L0_itek)I0_opik=I0_opik+1
      if(L0_osek)I0_opik=I0_opik+1
      if(L0_esek)I0_opik=I0_opik+1
      if(L0_urek)I0_opik=I0_opik+1
      if(L0_irek)I0_opik=I0_opik+1
      if(L0_arek)I0_opik=I0_opik+1
      if(L0_opek)I0_opik=I0_opik+1
      if(L0_umik)I0_opik=I0_opik+1
      if(L0_imik)I0_opik=I0_opik+1
      if(L0_amik)I0_opik=I0_opik+1
      if(L0_olik)I0_opik=I0_opik+1
      if(L0_elik)I0_opik=I0_opik+1
      if(L0_ukik)I0_opik=I0_opik+1
      if(L0_ikik)I0_opik=I0_opik+1
      if(L0_akik)I0_opik=I0_opik+1
      if(L0_ofik)I0_opik=I0_opik+1
      if(L0_efik)I0_opik=I0_opik+1
      if(L0_idik)I0_opik=I0_opik+1
      if(L0_adik)I0_opik=I0_opik+1
      if(L0_obik)I0_opik=I0_opik+1
      if(L0_ebik)I0_opik=I0_opik+1
      if(L0_uxek)I0_opik=I0_opik+1
      if(L0_ixek)I0_opik=I0_opik+1
      if(L0_axek)I0_opik=I0_opik+1
      if(L0_ovek)I0_opik=I0_opik+1
      if(L0_evek)I0_opik=I0_opik+1
      if(L0_utek)I0_opik=I0_opik+1
      if(L0_atek)I0_opik=I0_opik+1


      if(.not.L0_upik.and.L_ipik.and..not.L0_epik)then
        I0_epel=1
        L0_upik=.true.
      endif

      if(.not.L0_upik.and.L_afik.and..not.L0_udik)then
        I0_epel=2
        L0_upik=.true.
      endif

      if(.not.L0_upik.and.L_otek.and..not.L0_itek)then
        I0_epel=3
        L0_upik=.true.
      endif

      if(.not.L0_upik.and.L_usek.and..not.L0_osek)then
        I0_epel=4
        L0_upik=.true.
      endif

      if(.not.L0_upik.and.L0_isek.and..not.L0_esek)then
        I0_epel=5
        L0_upik=.true.
      endif

      if(.not.L0_upik.and.L0_asek.and..not.L0_urek)then
        I0_epel=6
        L0_upik=.true.
      endif

      if(.not.L0_upik.and.L0_orek.and..not.L0_irek)then
        I0_epel=7
        L0_upik=.true.
      endif

      if(.not.L0_upik.and.L0_erek.and..not.L0_arek)then
        I0_epel=8
        L0_upik=.true.
      endif

      if(.not.L0_upik.and.L0_upek.and..not.L0_opek)then
        I0_epel=9
        L0_upik=.true.
      endif

      if(.not.L0_upik.and.L0_apik.and..not.L0_umik)then
        I0_epel=10
        L0_upik=.true.
      endif

      if(.not.L0_upik.and.L0_omik.and..not.L0_imik)then
        I0_epel=11
        L0_upik=.true.
      endif

      if(.not.L0_upik.and.L0_emik.and..not.L0_amik)then
        I0_epel=12
        L0_upik=.true.
      endif

      if(.not.L0_upik.and.L0_ulik.and..not.L0_olik)then
        I0_epel=13
        L0_upik=.true.
      endif

      if(.not.L0_upik.and.L0_ilik.and..not.L0_elik)then
        I0_epel=14
        L0_upik=.true.
      endif

      if(.not.L0_upik.and.L0_alik.and..not.L0_ukik)then
        I0_epel=15
        L0_upik=.true.
      endif

      if(.not.L0_upik.and.L0_okik.and..not.L0_ikik)then
        I0_epel=16
        L0_upik=.true.
      endif

      if(.not.L0_upik.and.L0_ekik.and..not.L0_akik)then
        I0_epel=17
        L0_upik=.true.
      endif

      if(.not.L0_upik.and.L0_ufik.and..not.L0_ofik)then
        I0_epel=18
        L0_upik=.true.
      endif

      if(.not.L0_upik.and.L0_ifik.and..not.L0_efik)then
        I0_epel=19
        L0_upik=.true.
      endif

      if(.not.L0_upik.and.L0_odik.and..not.L0_idik)then
        I0_epel=20
        L0_upik=.true.
      endif

      if(.not.L0_upik.and.L0_edik.and..not.L0_adik)then
        I0_epel=21
        L0_upik=.true.
      endif

      if(.not.L0_upik.and.L0_ubik.and..not.L0_obik)then
        I0_epel=22
        L0_upik=.true.
      endif

      if(.not.L0_upik.and.L0_ibik.and..not.L0_ebik)then
        I0_epel=23
        L0_upik=.true.
      endif

      if(.not.L0_upik.and.L0_abik.and..not.L0_uxek)then
        I0_epel=24
        L0_upik=.true.
      endif

      if(.not.L0_upik.and.L0_oxek.and..not.L0_ixek)then
        I0_epel=25
        L0_upik=.true.
      endif

      if(.not.L0_upik.and.L0_exek.and..not.L0_axek)then
        I0_epel=26
        L0_upik=.true.
      endif

      if(.not.L0_upik.and.L0_uvek.and..not.L0_ovek)then
        I0_epel=27
        L0_upik=.true.
      endif

      if(.not.L0_upik.and.L0_ivek.and..not.L0_evek)then
        I0_epel=28
        L0_upik=.true.
      endif

      if(.not.L0_upik.and.L0_avek.and..not.L0_utek)then
        I0_epel=29
        L0_upik=.true.
      endif

      if(.not.L0_upik.and.L0_etek.and..not.L0_atek)then
        I0_epel=30
        L0_upik=.true.
      endif

      if(I0_opik.eq.1)then
         L0_epik=.false.
         L0_udik=.false.
         L0_itek=.false.
         L0_osek=.false.
         L0_esek=.false.
         L0_urek=.false.
         L0_irek=.false.
         L0_arek=.false.
         L0_opek=.false.
         L0_umik=.false.
         L0_imik=.false.
         L0_amik=.false.
         L0_olik=.false.
         L0_elik=.false.
         L0_ukik=.false.
         L0_ikik=.false.
         L0_akik=.false.
         L0_ofik=.false.
         L0_efik=.false.
         L0_idik=.false.
         L0_adik=.false.
         L0_obik=.false.
         L0_ebik=.false.
         L0_uxek=.false.
         L0_ixek=.false.
         L0_axek=.false.
         L0_ovek=.false.
         L0_evek=.false.
         L0_utek=.false.
         L0_atek=.false.
      else
         L0_epik=L_ipik
         L0_udik=L_afik
         L0_itek=L_otek
         L0_osek=L_usek
         L0_esek=L0_isek
         L0_urek=L0_asek
         L0_irek=L0_orek
         L0_arek=L0_erek
         L0_opek=L0_upek
         L0_umik=L0_apik
         L0_imik=L0_omik
         L0_amik=L0_emik
         L0_olik=L0_ulik
         L0_elik=L0_ilik
         L0_ukik=L0_alik
         L0_ikik=L0_okik
         L0_akik=L0_ekik
         L0_ofik=L0_ufik
         L0_efik=L0_ifik
         L0_idik=L0_odik
         L0_adik=L0_edik
         L0_obik=L0_ubik
         L0_ebik=L0_ibik
         L0_uxek=L0_abik
         L0_ixek=L0_oxek
         L0_axek=L0_exek
         L0_ovek=L0_uvek
         L0_evek=L0_ivek
         L0_utek=L0_avek
         L0_atek=L0_etek
      endif
C control_tg.fgi( 371, 130):��������� �������
      select case (I0_epel)
      case (1)
        C255_apel="��������� ����� �� ����������"
      case (2)
        C255_apel="���� �������"
      case (3)
        C255_apel="���� � �����"
      case (4)
        C255_apel="���� ��������"
      case (5)
        C255_apel="???"
      case (6)
        C255_apel="???"
      case (7)
        C255_apel="???"
      case (8)
        C255_apel="???"
      case (9)
        C255_apel="???"
      case (10)
        C255_apel="???"
      case (11)
        C255_apel="???"
      case (12)
        C255_apel="???"
      case (13)
        C255_apel="???"
      case (14)
        C255_apel="???"
      case (15)
        C255_apel="???"
      case (16)
        C255_apel="???"
      case (17)
        C255_apel="???"
      case (18)
        C255_apel="???"
      case (19)
        C255_apel="???"
      case (20)
        C255_apel="???"
      case (21)
        C255_apel="???"
      case (22)
        C255_apel="???"
      case (23)
        C255_apel="???"
      case (24)
        C255_apel="???"
      case (25)
        C255_apel="???"
      case (26)
        C255_apel="???"
      case (27)
        C255_apel="???"
      case (28)
        C255_apel="???"
      case (29)
        C255_apel="???"
      case (30)
        C255_apel="???"
      end select
C control_tg.fgi( 386, 159):MarkVI text out 30 variants,tg_text3
      if(L_ural) then
         I_(51)=I0_asal
      else
         I_(51)=I0_esal
      endif
C control_tg.fgi( 239,  92):���� RE IN LO CH7
      if(L_ofel) then
         I_isal=I0_oral
      else
         I_isal=I_(51)
      endif
C control_tg.fgi( 257,  91):���� RE IN LO CH7
      L_(28) = L_imum.AND.L_aruk
C control_tg.fgi( 109, 231):�
      L_ipak=(L_(27).or.L_ipak).and..not.(L_(28))
      L_utik=.not.L_ipak
C control_tg.fgi( 117, 233):RS �������
      L_efok=L_ipak
C control_tg.fgi( 155, 235):������,capt_on
      L_(22)=L_utik.and..not.L0_ulak
      L0_ulak=L_utik
C control_tg.fgi(  86,  41):������������  �� 1 ���
      L_omel = (.NOT.L_(77))
C control_tg.fgi(  74, 225):���
      L_(61) = (.NOT.L_omel)
C control_tg.fgi( 220, 160):���
      L_(60) = L_ikel.AND.L_(61).AND.(.NOT.L_ekel).AND.(.NOT.L_otel
     &).AND.(.NOT.L_itel)
C control_tg.fgi( 237, 160):�
      L_edel = (.NOT.L_ifel).AND.L_(60)
C control_tg.fgi( 244, 161):�
      L_afel = (.NOT.L_edel).AND.(.NOT.L_adel).AND.(.NOT.L_idel
     &).AND.(.NOT.L_udel).AND.(.NOT.L_odel).AND.(.NOT.L_ifel
     &).AND.(.NOT.L_ofel)
C control_tg.fgi( 259, 119):�
      L0_ipek=.false.
      I0_epek=0

      if(L0_ekap)then
        I0_irel=1
         L0_umek=.false.
         L0_afek=.false.
         L0_otak=.false.
         L0_atak=.false.
         L0_usak=.false.
         L0_osak=.false.
         L0_esak=.false.
         L0_asak=.false.
         L0_urak=.false.
         L0_omek=.false.
         L0_imek=.false.
         L0_emek=.false.
         L0_ulek=.false.
         L0_ilek=.false.
         L0_alek=.false.
         L0_okek=.false.
         L0_ekek=.false.
         L0_ufek=.false.
         L0_ifek=.false.
         L0_odek=.false.
         L0_edek=.false.
         L0_ubek=.false.
         L0_ibek=.false.
         L0_abek=.false.
         L0_oxak=.false.
         L0_exak=.false.
         L0_uvak=.false.
         L0_ivak=.false.
         L0_avak=.false.
         L0_etak=.false.
      endif

      if(L0_umek)I0_epek=I0_epek+1
      if(L0_afek)I0_epek=I0_epek+1
      if(L0_otak)I0_epek=I0_epek+1
      if(L0_atak)I0_epek=I0_epek+1
      if(L0_usak)I0_epek=I0_epek+1
      if(L0_osak)I0_epek=I0_epek+1
      if(L0_esak)I0_epek=I0_epek+1
      if(L0_asak)I0_epek=I0_epek+1
      if(L0_urak)I0_epek=I0_epek+1
      if(L0_omek)I0_epek=I0_epek+1
      if(L0_imek)I0_epek=I0_epek+1
      if(L0_emek)I0_epek=I0_epek+1
      if(L0_ulek)I0_epek=I0_epek+1
      if(L0_ilek)I0_epek=I0_epek+1
      if(L0_alek)I0_epek=I0_epek+1
      if(L0_okek)I0_epek=I0_epek+1
      if(L0_ekek)I0_epek=I0_epek+1
      if(L0_ufek)I0_epek=I0_epek+1
      if(L0_ifek)I0_epek=I0_epek+1
      if(L0_odek)I0_epek=I0_epek+1
      if(L0_edek)I0_epek=I0_epek+1
      if(L0_ubek)I0_epek=I0_epek+1
      if(L0_ibek)I0_epek=I0_epek+1
      if(L0_abek)I0_epek=I0_epek+1
      if(L0_oxak)I0_epek=I0_epek+1
      if(L0_exak)I0_epek=I0_epek+1
      if(L0_uvak)I0_epek=I0_epek+1
      if(L0_ivak)I0_epek=I0_epek+1
      if(L0_avak)I0_epek=I0_epek+1
      if(L0_etak)I0_epek=I0_epek+1


      if(.not.L0_ipek.and.L0_apek.and..not.L0_umek)then
        I0_irel=1
        L0_ipek=.true.
      endif

      if(.not.L0_ipek.and.L0_efek.and..not.L0_afek)then
        I0_irel=2
        L0_ipek=.true.
      endif

      if(.not.L0_ipek.and.L0_utak.and..not.L0_otak)then
        I0_irel=3
        L0_ipek=.true.
      endif

      if(.not.L0_ipek.and.L_ifel.and..not.L0_atak)then
        I0_irel=4
        L0_ipek=.true.
      endif

      if(.not.L0_ipek.and.L_efel.and..not.L0_usak)then
        I0_irel=5
        L0_ipek=.true.
      endif

      if(.not.L0_ipek.and.L_afel.and..not.L0_osak)then
        I0_irel=6
        L0_ipek=.true.
      endif

      if(.not.L0_ipek.and.L0_isak.and..not.L0_esak)then
        I0_irel=7
        L0_ipek=.true.
      endif

      if(.not.L0_ipek.and.L_udel.and..not.L0_asak)then
        I0_irel=8
        L0_ipek=.true.
      endif

      if(.not.L0_ipek.and.L_odel.and..not.L0_urak)then
        I0_irel=9
        L0_ipek=.true.
      endif

      if(.not.L0_ipek.and.L_idel.and..not.L0_omek)then
        I0_irel=10
        L0_ipek=.true.
      endif

      if(.not.L0_ipek.and.L_edel.and..not.L0_imek)then
        I0_irel=11
        L0_ipek=.true.
      endif

      if(.not.L0_ipek.and.L_adel.and..not.L0_emek)then
        I0_irel=12
        L0_ipek=.true.
      endif

      if(.not.L0_ipek.and.L0_amek.and..not.L0_ulek)then
        I0_irel=13
        L0_ipek=.true.
      endif

      if(.not.L0_ipek.and.L0_olek.and..not.L0_ilek)then
        I0_irel=14
        L0_ipek=.true.
      endif

      if(.not.L0_ipek.and.L0_elek.and..not.L0_alek)then
        I0_irel=15
        L0_ipek=.true.
      endif

      if(.not.L0_ipek.and.L0_ukek.and..not.L0_okek)then
        I0_irel=16
        L0_ipek=.true.
      endif

      if(.not.L0_ipek.and.L0_ikek.and..not.L0_ekek)then
        I0_irel=17
        L0_ipek=.true.
      endif

      if(.not.L0_ipek.and.L0_akek.and..not.L0_ufek)then
        I0_irel=18
        L0_ipek=.true.
      endif

      if(.not.L0_ipek.and.L0_ofek.and..not.L0_ifek)then
        I0_irel=19
        L0_ipek=.true.
      endif

      if(.not.L0_ipek.and.L0_udek.and..not.L0_odek)then
        I0_irel=20
        L0_ipek=.true.
      endif

      if(.not.L0_ipek.and.L0_idek.and..not.L0_edek)then
        I0_irel=21
        L0_ipek=.true.
      endif

      if(.not.L0_ipek.and.L0_adek.and..not.L0_ubek)then
        I0_irel=22
        L0_ipek=.true.
      endif

      if(.not.L0_ipek.and.L0_obek.and..not.L0_ibek)then
        I0_irel=23
        L0_ipek=.true.
      endif

      if(.not.L0_ipek.and.L0_ebek.and..not.L0_abek)then
        I0_irel=24
        L0_ipek=.true.
      endif

      if(.not.L0_ipek.and.L0_uxak.and..not.L0_oxak)then
        I0_irel=25
        L0_ipek=.true.
      endif

      if(.not.L0_ipek.and.L0_ixak.and..not.L0_exak)then
        I0_irel=26
        L0_ipek=.true.
      endif

      if(.not.L0_ipek.and.L0_axak.and..not.L0_uvak)then
        I0_irel=27
        L0_ipek=.true.
      endif

      if(.not.L0_ipek.and.L0_ovak.and..not.L0_ivak)then
        I0_irel=28
        L0_ipek=.true.
      endif

      if(.not.L0_ipek.and.L0_evak.and..not.L0_avak)then
        I0_irel=29
        L0_ipek=.true.
      endif

      if(.not.L0_ipek.and.L0_itak.and..not.L0_etak)then
        I0_irel=30
        L0_ipek=.true.
      endif

      if(I0_epek.eq.1)then
         L0_umek=.false.
         L0_afek=.false.
         L0_otak=.false.
         L0_atak=.false.
         L0_usak=.false.
         L0_osak=.false.
         L0_esak=.false.
         L0_asak=.false.
         L0_urak=.false.
         L0_omek=.false.
         L0_imek=.false.
         L0_emek=.false.
         L0_ulek=.false.
         L0_ilek=.false.
         L0_alek=.false.
         L0_okek=.false.
         L0_ekek=.false.
         L0_ufek=.false.
         L0_ifek=.false.
         L0_odek=.false.
         L0_edek=.false.
         L0_ubek=.false.
         L0_ibek=.false.
         L0_abek=.false.
         L0_oxak=.false.
         L0_exak=.false.
         L0_uvak=.false.
         L0_ivak=.false.
         L0_avak=.false.
         L0_etak=.false.
      else
         L0_umek=L0_apek
         L0_afek=L0_efek
         L0_otak=L0_utak
         L0_atak=L_ifel
         L0_usak=L_efel
         L0_osak=L_afel
         L0_esak=L0_isak
         L0_asak=L_udel
         L0_urak=L_odel
         L0_omek=L_idel
         L0_imek=L_edel
         L0_emek=L_adel
         L0_ulek=L0_amek
         L0_ilek=L0_olek
         L0_alek=L0_elek
         L0_okek=L0_ukek
         L0_ekek=L0_ikek
         L0_ufek=L0_akek
         L0_ifek=L0_ofek
         L0_odek=L0_udek
         L0_edek=L0_idek
         L0_ubek=L0_adek
         L0_ibek=L0_obek
         L0_abek=L0_ebek
         L0_oxak=L0_uxak
         L0_exak=L0_ixak
         L0_uvak=L0_axak
         L0_ivak=L0_ovak
         L0_avak=L0_evak
         L0_etak=L0_itak
      endif
C control_tg.fgi( 289, 170):��������� �������
      select case (I0_irel)
      case (1)
        C255_erel="��� �����"
      case (2)
        C255_erel="���������� � UZ"
      case (3)
        C255_erel="������������"
      case (4)
        C255_erel="��� 380�"
      case (5)
        C255_erel="�������������"
      case (6)
        C255_erel="����� � ������"
      case (7)
        C255_erel="����� ����"
      case (8)
        C255_erel="������ ������"
      case (9)
        C255_erel="������ �� ����"
      case (10)
        C255_erel="������ �����"
      case (11)
        C255_erel="����� �������"
      case (12)
        C255_erel="����������������"
      case (13)
        C255_erel="����� ����"
      case (14)
        C255_erel="����� ������� �����"
      case (15)
        C255_erel="������ �������"
      case (16)
        C255_erel="������ ����������"
      case (17)
        C255_erel="�� ����������"
      case (18)
        C255_erel="???"
      case (19)
        C255_erel="???"
      case (20)
        C255_erel="???"
      case (21)
        C255_erel="???"
      case (22)
        C255_erel="???"
      case (23)
        C255_erel="???"
      case (24)
        C255_erel="???"
      case (25)
        C255_erel="???"
      case (26)
        C255_erel="???"
      case (27)
        C255_erel="???"
      case (28)
        C255_erel="???"
      case (29)
        C255_erel="???"
      case (30)
        C255_erel="???"
      end select
C control_tg.fgi( 306, 199):MarkVI text out 30 variants,tg_text5
      L_(68) = (.NOT.L_omel)
C control_tg.fgi( 220, 251):���
      L_(67) = L_ikel.AND.L_(68).AND.(.NOT.L_ekel).AND.(.NOT.L_otel
     &).AND.(.NOT.L_itel)
C control_tg.fgi( 237, 251):�
      L_ukel = (.NOT.L_amel).AND.L_(67)
C control_tg.fgi( 244, 252):�
      L_olel = (.NOT.L_ukel).AND.(.NOT.L_okel).AND.(.NOT.L_alel
     &).AND.(.NOT.L_ilel).AND.(.NOT.L_elel).AND.(.NOT.L_amel
     &).AND.(.NOT.L_ofel)
C control_tg.fgi( 259, 210):�
      L0_umuk=.false.
      I0_omuk=0

      if(L0_ekap)then
        I0_imel=1
         L0_emuk=.false.
         L0_iduk=.false.
         L0_atok=.false.
         L0_isok=.false.
         L0_esok=.false.
         L0_asok=.false.
         L0_orok=.false.
         L0_irok=.false.
         L0_erok=.false.
         L0_amuk=.false.
         L0_uluk=.false.
         L0_oluk=.false.
         L0_eluk=.false.
         L0_ukuk=.false.
         L0_ikuk=.false.
         L0_akuk=.false.
         L0_ofuk=.false.
         L0_efuk=.false.
         L0_uduk=.false.
         L0_aduk=.false.
         L0_obuk=.false.
         L0_ebuk=.false.
         L0_uxok=.false.
         L0_ixok=.false.
         L0_axok=.false.
         L0_ovok=.false.
         L0_evok=.false.
         L0_utok=.false.
         L0_itok=.false.
         L0_osok=.false.
      endif

      if(L0_emuk)I0_omuk=I0_omuk+1
      if(L0_iduk)I0_omuk=I0_omuk+1
      if(L0_atok)I0_omuk=I0_omuk+1
      if(L0_isok)I0_omuk=I0_omuk+1
      if(L0_esok)I0_omuk=I0_omuk+1
      if(L0_asok)I0_omuk=I0_omuk+1
      if(L0_orok)I0_omuk=I0_omuk+1
      if(L0_irok)I0_omuk=I0_omuk+1
      if(L0_erok)I0_omuk=I0_omuk+1
      if(L0_amuk)I0_omuk=I0_omuk+1
      if(L0_uluk)I0_omuk=I0_omuk+1
      if(L0_oluk)I0_omuk=I0_omuk+1
      if(L0_eluk)I0_omuk=I0_omuk+1
      if(L0_ukuk)I0_omuk=I0_omuk+1
      if(L0_ikuk)I0_omuk=I0_omuk+1
      if(L0_akuk)I0_omuk=I0_omuk+1
      if(L0_ofuk)I0_omuk=I0_omuk+1
      if(L0_efuk)I0_omuk=I0_omuk+1
      if(L0_uduk)I0_omuk=I0_omuk+1
      if(L0_aduk)I0_omuk=I0_omuk+1
      if(L0_obuk)I0_omuk=I0_omuk+1
      if(L0_ebuk)I0_omuk=I0_omuk+1
      if(L0_uxok)I0_omuk=I0_omuk+1
      if(L0_ixok)I0_omuk=I0_omuk+1
      if(L0_axok)I0_omuk=I0_omuk+1
      if(L0_ovok)I0_omuk=I0_omuk+1
      if(L0_evok)I0_omuk=I0_omuk+1
      if(L0_utok)I0_omuk=I0_omuk+1
      if(L0_itok)I0_omuk=I0_omuk+1
      if(L0_osok)I0_omuk=I0_omuk+1


      if(.not.L0_umuk.and.L0_imuk.and..not.L0_emuk)then
        I0_imel=1
        L0_umuk=.true.
      endif

      if(.not.L0_umuk.and.L0_oduk.and..not.L0_iduk)then
        I0_imel=2
        L0_umuk=.true.
      endif

      if(.not.L0_umuk.and.L0_etok.and..not.L0_atok)then
        I0_imel=3
        L0_umuk=.true.
      endif

      if(.not.L0_umuk.and.L_amel.and..not.L0_isok)then
        I0_imel=4
        L0_umuk=.true.
      endif

      if(.not.L0_umuk.and.L_ulel.and..not.L0_esok)then
        I0_imel=5
        L0_umuk=.true.
      endif

      if(.not.L0_umuk.and.L_olel.and..not.L0_asok)then
        I0_imel=6
        L0_umuk=.true.
      endif

      if(.not.L0_umuk.and.L0_urok.and..not.L0_orok)then
        I0_imel=7
        L0_umuk=.true.
      endif

      if(.not.L0_umuk.and.L_ilel.and..not.L0_irok)then
        I0_imel=8
        L0_umuk=.true.
      endif

      if(.not.L0_umuk.and.L_elel.and..not.L0_erok)then
        I0_imel=9
        L0_umuk=.true.
      endif

      if(.not.L0_umuk.and.L_alel.and..not.L0_amuk)then
        I0_imel=10
        L0_umuk=.true.
      endif

      if(.not.L0_umuk.and.L_ukel.and..not.L0_uluk)then
        I0_imel=11
        L0_umuk=.true.
      endif

      if(.not.L0_umuk.and.L_okel.and..not.L0_oluk)then
        I0_imel=12
        L0_umuk=.true.
      endif

      if(.not.L0_umuk.and.L0_iluk.and..not.L0_eluk)then
        I0_imel=13
        L0_umuk=.true.
      endif

      if(.not.L0_umuk.and.L0_aluk.and..not.L0_ukuk)then
        I0_imel=14
        L0_umuk=.true.
      endif

      if(.not.L0_umuk.and.L0_okuk.and..not.L0_ikuk)then
        I0_imel=15
        L0_umuk=.true.
      endif

      if(.not.L0_umuk.and.L0_ekuk.and..not.L0_akuk)then
        I0_imel=16
        L0_umuk=.true.
      endif

      if(.not.L0_umuk.and.L0_ufuk.and..not.L0_ofuk)then
        I0_imel=17
        L0_umuk=.true.
      endif

      if(.not.L0_umuk.and.L0_ifuk.and..not.L0_efuk)then
        I0_imel=18
        L0_umuk=.true.
      endif

      if(.not.L0_umuk.and.L0_afuk.and..not.L0_uduk)then
        I0_imel=19
        L0_umuk=.true.
      endif

      if(.not.L0_umuk.and.L0_eduk.and..not.L0_aduk)then
        I0_imel=20
        L0_umuk=.true.
      endif

      if(.not.L0_umuk.and.L0_ubuk.and..not.L0_obuk)then
        I0_imel=21
        L0_umuk=.true.
      endif

      if(.not.L0_umuk.and.L0_ibuk.and..not.L0_ebuk)then
        I0_imel=22
        L0_umuk=.true.
      endif

      if(.not.L0_umuk.and.L0_abuk.and..not.L0_uxok)then
        I0_imel=23
        L0_umuk=.true.
      endif

      if(.not.L0_umuk.and.L0_oxok.and..not.L0_ixok)then
        I0_imel=24
        L0_umuk=.true.
      endif

      if(.not.L0_umuk.and.L0_exok.and..not.L0_axok)then
        I0_imel=25
        L0_umuk=.true.
      endif

      if(.not.L0_umuk.and.L0_uvok.and..not.L0_ovok)then
        I0_imel=26
        L0_umuk=.true.
      endif

      if(.not.L0_umuk.and.L0_ivok.and..not.L0_evok)then
        I0_imel=27
        L0_umuk=.true.
      endif

      if(.not.L0_umuk.and.L0_avok.and..not.L0_utok)then
        I0_imel=28
        L0_umuk=.true.
      endif

      if(.not.L0_umuk.and.L0_otok.and..not.L0_itok)then
        I0_imel=29
        L0_umuk=.true.
      endif

      if(.not.L0_umuk.and.L0_usok.and..not.L0_osok)then
        I0_imel=30
        L0_umuk=.true.
      endif

      if(I0_omuk.eq.1)then
         L0_emuk=.false.
         L0_iduk=.false.
         L0_atok=.false.
         L0_isok=.false.
         L0_esok=.false.
         L0_asok=.false.
         L0_orok=.false.
         L0_irok=.false.
         L0_erok=.false.
         L0_amuk=.false.
         L0_uluk=.false.
         L0_oluk=.false.
         L0_eluk=.false.
         L0_ukuk=.false.
         L0_ikuk=.false.
         L0_akuk=.false.
         L0_ofuk=.false.
         L0_efuk=.false.
         L0_uduk=.false.
         L0_aduk=.false.
         L0_obuk=.false.
         L0_ebuk=.false.
         L0_uxok=.false.
         L0_ixok=.false.
         L0_axok=.false.
         L0_ovok=.false.
         L0_evok=.false.
         L0_utok=.false.
         L0_itok=.false.
         L0_osok=.false.
      else
         L0_emuk=L0_imuk
         L0_iduk=L0_oduk
         L0_atok=L0_etok
         L0_isok=L_amel
         L0_esok=L_ulel
         L0_asok=L_olel
         L0_orok=L0_urok
         L0_irok=L_ilel
         L0_erok=L_elel
         L0_amuk=L_alel
         L0_uluk=L_ukel
         L0_oluk=L_okel
         L0_eluk=L0_iluk
         L0_ukuk=L0_aluk
         L0_ikuk=L0_okuk
         L0_akuk=L0_ekuk
         L0_ofuk=L0_ufuk
         L0_efuk=L0_ifuk
         L0_uduk=L0_afuk
         L0_aduk=L0_eduk
         L0_obuk=L0_ubuk
         L0_ebuk=L0_ibuk
         L0_uxok=L0_abuk
         L0_ixok=L0_oxok
         L0_axok=L0_exok
         L0_ovok=L0_uvok
         L0_evok=L0_ivok
         L0_utok=L0_avok
         L0_itok=L0_otok
         L0_osok=L0_usok
      endif
C control_tg.fgi( 289, 261):��������� �������
      select case (I0_imel)
      case (1)
        C255_emel="��� �����"
      case (2)
        C255_emel="���������� � UZ"
      case (3)
        C255_emel="������������"
      case (4)
        C255_emel="��� 380�"
      case (5)
        C255_emel="�������������"
      case (6)
        C255_emel="����� � ������"
      case (7)
        C255_emel="����� ����"
      case (8)
        C255_emel="������ ������"
      case (9)
        C255_emel="������ �� ����"
      case (10)
        C255_emel="������ �����"
      case (11)
        C255_emel="����� �������"
      case (12)
        C255_emel="����������������"
      case (13)
        C255_emel="����� ����"
      case (14)
        C255_emel="����� ������� �����"
      case (15)
        C255_emel="������ �������"
      case (16)
        C255_emel="������ ����������"
      case (17)
        C255_emel="�� ����������"
      case (18)
        C255_emel="???"
      case (19)
        C255_emel="???"
      case (20)
        C255_emel="???"
      case (21)
        C255_emel="???"
      case (22)
        C255_emel="???"
      case (23)
        C255_emel="???"
      case (24)
        C255_emel="???"
      case (25)
        C255_emel="???"
      case (26)
        C255_emel="???"
      case (27)
        C255_emel="???"
      case (28)
        C255_emel="???"
      case (29)
        C255_emel="???"
      case (30)
        C255_emel="???"
      end select
C control_tg.fgi( 306, 290):MarkVI text out 30 variants,tg_text4
      L_(132) = L_ufol.AND.L_ofol.AND.(.NOT.L_otom).AND.(.NOT.L_utom
     &)
C control_th.fgi( 237, 242):�
      L_okum = (.NOT.L_esul).AND.L_(132)
C control_th.fgi( 244, 243):�
      L_(125) = L_ufol.AND.L_ofol.AND.(.NOT.L_otom).AND.(.NOT.L_utom
     &)
C control_th.fgi( 237, 149):�
      L_ekum = (.NOT.L_akul).AND.L_(125)
C control_th.fgi( 244, 150):�
      L_(105) = L_ufol.AND.L_ikul
C control_th.fgi(  88,  30):�
      if(L_(105)) then
         I_(89)=I0_uril
      else
         I_(89)=I0_asil
      endif
C control_th.fgi( 119,  34):���� RE IN LO CH7
      if(L_iril) then
         I_esil=I0_oril
      else
         I_esil=I_(89)
      endif
C control_th.fgi( 137,  33):���� RE IN LO CH7
      L_(106) = L_ufol.AND.L_olul
C control_th.fgi(  88,  44):�
      if(L_(106)) then
         I_(90)=I0_usil
      else
         I_(90)=I0_atil
      endif
C control_th.fgi( 119,  50):���� RE IN LO CH7
      if(L_isil) then
         I_etil=I0_osil
      else
         I_etil=I_(90)
      endif
C control_th.fgi( 137,  49):���� RE IN LO CH7
      R_exom=R_ixom
C control_th.fgi( 159, 215):������,mo_digit1
      if(R_ibum.gt.R0_etom) then
         R_ebum=R_ibum-R0_etom
      elseif(R_ibum.le.-R0_etom) then
         R_ebum=R_ibum+R0_etom
      else
         R_ebum=0.0
      endif
C control_th.fgi(  99, 224):���� ������������������
      if(L_odol) then
         I_axil=I0_exil
      else
         I_axil=I0_ixil
      endif
C control_th.fgi( 128,  76):���� RE IN LO CH7
      if(L_irim) then
         I_oxil=I0_exil
      else
         I_oxil=I0_ixil
      endif
C control_th.fgi( 128,  86):���� RE IN LO CH7
      L_urim = (.NOT.L_(109))
C control_th.fgi( 132, 129):���
      L_arim = (.NOT.L_(111)).AND.(.NOT.L_(110))
C control_th.fgi( 132, 137):�
      if(L_opim) then
         I_uxil=I0_exil
      else
         I_uxil=I0_ixil
      endif
C control_th.fgi( 128,  98):���� RE IN LO CH7
      L_epim = (.NOT.L_(113)).AND.(.NOT.L_(112))
C control_th.fgi( 132, 145):�
      if(L_ulom) then
         I_abol=I0_exil
      else
         I_abol=I0_ixil
      endif
C control_th.fgi( 128, 110):���� RE IN LO CH7
      L_ilom = (.NOT.L_(107)).AND.(.NOT.L_(114))
C control_th.fgi( 132, 153):�
      if(L_alom) then
         I_ebol=I0_exil
      else
         I_ebol=I0_ixil
      endif
C control_th.fgi( 128, 122):���� RE IN LO CH7
      L_udom = (.NOT.L_(108))
C control_th.fgi( 132, 161):���
      L0_imom=.false.
      I0_emom=0

      if(L0_ekap)then
        I0_idum=1
         L0_amom=.false.
         L0_adom=.false.
         L0_usim=.false.
         L0_asim=.false.
         L0_orim=.false.
         L0_erim=.false.
         L0_upim=.false.
         L0_ipim=.false.
         L0_apim=.false.
         L0_olom=.false.
         L0_elom=.false.
         L0_ukom=.false.
         L0_ikom=.false.
         L0_akom=.false.
         L0_ofom=.false.
         L0_efom=.false.
         L0_afom=.false.
         L0_odom=.false.
         L0_edom=.false.
         L0_obom=.false.
         L0_ebom=.false.
         L0_uxim=.false.
         L0_ixim=.false.
         L0_axim=.false.
         L0_ovim=.false.
         L0_evim=.false.
         L0_utim=.false.
         L0_itim=.false.
         L0_atim=.false.
         L0_isim=.false.
      endif

      if(L0_amom)I0_emom=I0_emom+1
      if(L0_adom)I0_emom=I0_emom+1
      if(L0_usim)I0_emom=I0_emom+1
      if(L0_asim)I0_emom=I0_emom+1
      if(L0_orim)I0_emom=I0_emom+1
      if(L0_erim)I0_emom=I0_emom+1
      if(L0_upim)I0_emom=I0_emom+1
      if(L0_ipim)I0_emom=I0_emom+1
      if(L0_apim)I0_emom=I0_emom+1
      if(L0_olom)I0_emom=I0_emom+1
      if(L0_elom)I0_emom=I0_emom+1
      if(L0_ukom)I0_emom=I0_emom+1
      if(L0_ikom)I0_emom=I0_emom+1
      if(L0_akom)I0_emom=I0_emom+1
      if(L0_ofom)I0_emom=I0_emom+1
      if(L0_efom)I0_emom=I0_emom+1
      if(L0_afom)I0_emom=I0_emom+1
      if(L0_odom)I0_emom=I0_emom+1
      if(L0_edom)I0_emom=I0_emom+1
      if(L0_obom)I0_emom=I0_emom+1
      if(L0_ebom)I0_emom=I0_emom+1
      if(L0_uxim)I0_emom=I0_emom+1
      if(L0_ixim)I0_emom=I0_emom+1
      if(L0_axim)I0_emom=I0_emom+1
      if(L0_ovim)I0_emom=I0_emom+1
      if(L0_evim)I0_emom=I0_emom+1
      if(L0_utim)I0_emom=I0_emom+1
      if(L0_itim)I0_emom=I0_emom+1
      if(L0_atim)I0_emom=I0_emom+1
      if(L0_isim)I0_emom=I0_emom+1


      if(.not.L0_imom.and.L_itom.and..not.L0_amom)then
        I0_idum=1
        L0_imom=.true.
      endif

      if(.not.L0_imom.and.L_otom.and..not.L0_adom)then
        I0_idum=2
        L0_imom=.true.
      endif

      if(.not.L0_imom.and.L_utom.and..not.L0_usim)then
        I0_idum=3
        L0_imom=.true.
      endif

      if(.not.L0_imom.and.L0_esim.and..not.L0_asim)then
        I0_idum=4
        L0_imom=.true.
      endif

      if(.not.L0_imom.and.L_urim.and..not.L0_orim)then
        I0_idum=5
        L0_imom=.true.
      endif

      if(.not.L0_imom.and.L_irim.and..not.L0_erim)then
        I0_idum=6
        L0_imom=.true.
      endif

      if(.not.L0_imom.and.L_arim.and..not.L0_upim)then
        I0_idum=7
        L0_imom=.true.
      endif

      if(.not.L0_imom.and.L_opim.and..not.L0_ipim)then
        I0_idum=8
        L0_imom=.true.
      endif

      if(.not.L0_imom.and.L_epim.and..not.L0_apim)then
        I0_idum=9
        L0_imom=.true.
      endif

      if(.not.L0_imom.and.L_ulom.and..not.L0_olom)then
        I0_idum=10
        L0_imom=.true.
      endif

      if(.not.L0_imom.and.L_ilom.and..not.L0_elom)then
        I0_idum=11
        L0_imom=.true.
      endif

      if(.not.L0_imom.and.L_alom.and..not.L0_ukom)then
        I0_idum=12
        L0_imom=.true.
      endif

      if(.not.L0_imom.and.L0_okom.and..not.L0_ikom)then
        I0_idum=13
        L0_imom=.true.
      endif

      if(.not.L0_imom.and.L0_ekom.and..not.L0_akom)then
        I0_idum=14
        L0_imom=.true.
      endif

      if(.not.L0_imom.and.L0_ufom.and..not.L0_ofom)then
        I0_idum=15
        L0_imom=.true.
      endif

      if(.not.L0_imom.and.L_ifom.and..not.L0_efom)then
        I0_idum=16
        L0_imom=.true.
      endif

      if(.not.L0_imom.and.L_(150).and..not.L0_afom)then
        I0_idum=17
        L0_imom=.true.
      endif

      if(.not.L0_imom.and.L_udom.and..not.L0_odom)then
        I0_idum=18
        L0_imom=.true.
      endif

      if(.not.L0_imom.and.L0_idom.and..not.L0_edom)then
        I0_idum=19
        L0_imom=.true.
      endif

      if(.not.L0_imom.and.L0_ubom.and..not.L0_obom)then
        I0_idum=20
        L0_imom=.true.
      endif

      if(.not.L0_imom.and.L0_ibom.and..not.L0_ebom)then
        I0_idum=21
        L0_imom=.true.
      endif

      if(.not.L0_imom.and.L0_abom.and..not.L0_uxim)then
        I0_idum=22
        L0_imom=.true.
      endif

      if(.not.L0_imom.and.L0_oxim.and..not.L0_ixim)then
        I0_idum=23
        L0_imom=.true.
      endif

      if(.not.L0_imom.and.L0_exim.and..not.L0_axim)then
        I0_idum=24
        L0_imom=.true.
      endif

      if(.not.L0_imom.and.L0_uvim.and..not.L0_ovim)then
        I0_idum=25
        L0_imom=.true.
      endif

      if(.not.L0_imom.and.L0_ivim.and..not.L0_evim)then
        I0_idum=26
        L0_imom=.true.
      endif

      if(.not.L0_imom.and.L0_avim.and..not.L0_utim)then
        I0_idum=27
        L0_imom=.true.
      endif

      if(.not.L0_imom.and.L0_otim.and..not.L0_itim)then
        I0_idum=28
        L0_imom=.true.
      endif

      if(.not.L0_imom.and.L0_etim.and..not.L0_atim)then
        I0_idum=29
        L0_imom=.true.
      endif

      if(.not.L0_imom.and.L0_osim.and..not.L0_isim)then
        I0_idum=30
        L0_imom=.true.
      endif

      if(I0_emom.eq.1)then
         L0_amom=.false.
         L0_adom=.false.
         L0_usim=.false.
         L0_asim=.false.
         L0_orim=.false.
         L0_erim=.false.
         L0_upim=.false.
         L0_ipim=.false.
         L0_apim=.false.
         L0_olom=.false.
         L0_elom=.false.
         L0_ukom=.false.
         L0_ikom=.false.
         L0_akom=.false.
         L0_ofom=.false.
         L0_efom=.false.
         L0_afom=.false.
         L0_odom=.false.
         L0_edom=.false.
         L0_obom=.false.
         L0_ebom=.false.
         L0_uxim=.false.
         L0_ixim=.false.
         L0_axim=.false.
         L0_ovim=.false.
         L0_evim=.false.
         L0_utim=.false.
         L0_itim=.false.
         L0_atim=.false.
         L0_isim=.false.
      else
         L0_amom=L_itom
         L0_adom=L_otom
         L0_usim=L_utom
         L0_asim=L0_esim
         L0_orim=L_urim
         L0_erim=L_irim
         L0_upim=L_arim
         L0_ipim=L_opim
         L0_apim=L_epim
         L0_olom=L_ulom
         L0_elom=L_ilom
         L0_ukom=L_alom
         L0_ikom=L0_okom
         L0_akom=L0_ekom
         L0_ofom=L0_ufom
         L0_efom=L_ifom
         L0_afom=L_(150)
         L0_odom=L_udom
         L0_edom=L0_idom
         L0_obom=L0_ubom
         L0_ebom=L0_ibom
         L0_uxim=L0_abom
         L0_ixim=L0_oxim
         L0_axim=L0_exim
         L0_ovim=L0_uvim
         L0_evim=L0_ivim
         L0_utim=L0_avim
         L0_itim=L0_otim
         L0_atim=L0_etim
         L0_isim=L0_osim
      endif
C control_th.fgi( 371, 261):��������� �������
      select case (I0_idum)
      case (1)
        C255_urom="�����������"
      case (2)
        C255_urom="����������� �����"
      case (3)
        C255_urom="����������� ������"
      case (4)
        C255_urom="�����-� � ����-� �� ����������"
      case (5)
        C255_urom="������ ������������"
      case (6)
        C255_urom="������� ������"
      case (7)
        C255_urom="����� ���.������ � ��������"
      case (8)
        C255_urom="������� ��������"
      case (9)
        C255_urom="����� ���.�������� � ������������"
      case (10)
        C255_urom="������� �����������"
      case (11)
        C255_urom="����� ���.����������� � ���������"
      case (12)
        C255_urom="������� ���������"
      case (13)
        C255_urom="???"
      case (14)
        C255_urom="�� ����������"
      case (15)
        C255_urom="����-� ������� �������� �� ���."
      case (16)
        C255_urom="������� �������� ������ � �����������"
      case (17)
        C255_urom="������� �������� �� ���.� �����������"
      case (18)
        C255_urom="����� ������������"
      case (19)
        C255_urom="???"
      case (20)
        C255_urom="???"
      case (21)
        C255_urom="???"
      case (22)
        C255_urom="???"
      case (23)
        C255_urom="???"
      case (24)
        C255_urom="???"
      case (25)
        C255_urom="???"
      case (26)
        C255_urom="???"
      case (27)
        C255_urom="???"
      case (28)
        C255_urom="???"
      case (29)
        C255_urom="???"
      case (30)
        C255_urom="???"
      end select
C control_th.fgi( 386, 290):MarkVI text out 30 variants,mo_text1
      L_(130) = L_udom.OR.L_urim
C control_th.fgi( 220, 225):���
      L_(131) = L_otom.AND.L_ufol.AND.(.NOT.L_(130))
C control_th.fgi( 237, 233):�
      L_amam = (.NOT.L_esul).AND.L_(131)
C control_th.fgi( 244, 234):�
      L_(129) = L_ufol.AND.L_utom.AND.(.NOT.L_(130))
C control_th.fgi( 237, 227):�
      L_opul = (.NOT.L_esul).AND.L_(129)
C control_th.fgi( 244, 228):�
      L_(128) = L_(130).AND.L_ufol
C control_th.fgi( 237, 222):�
      L_epul = (.NOT.L_esul).AND.L_(128)
C control_th.fgi( 244, 223):�
      L_(123) = L_udom.OR.L_urim
C control_th.fgi( 220, 132):���
      L_(124) = L_otom.AND.L_ufol.AND.(.NOT.L_(123))
C control_th.fgi( 237, 140):�
      L_udul = (.NOT.L_akul).AND.L_(124)
C control_th.fgi( 244, 141):�
      L_(121) = L_(123).AND.L_ufol
C control_th.fgi( 237, 129):�
      L_ekol = (.NOT.L_akul).AND.L_(121)
C control_th.fgi( 244, 130):�
      L_(122) = L_ufol.AND.L_utom.AND.(.NOT.L_(123))
C control_th.fgi( 237, 134):�
      L_okol = (.NOT.L_akul).AND.L_(122)
C control_th.fgi( 244, 135):�
      L_(87) =.NOT.(L_ikil)
C control_th.fgi( 331,  34):���
      I_(76)=0
      if(L_(87))I_(76)=7
C control_th.fgi( 337,  34):lo2digit
      I_(75)=0
      if(L_ikil)I_(75)=6
C control_th.fgi( 337,  32):lo2digit
      I_(100) = I_(76) + I_(75)
C control_th.fgi( 343,  33):��������
      select case (I_(100))
      case (1)
        C255_opom="�����"
      case (2)
        C255_opom="� ������"
      case (3)
        C255_opom="� ������"
      case (4)
        C255_opom="��������"
      case (5)
        C255_opom="�� ����������"
      case (6)
        C255_opom="����� ������ � ����������"
      case (7)
        C255_opom="����� ������ ����������"
      case (8)
        C255_opom="???"
      case (9)
        C255_opom="???"
      case (10)
        C255_opom="???"
      case (11)
        C255_opom="???"
      case (12)
        C255_opom="???"
      case (13)
        C255_opom="???"
      case (14)
        C255_opom="???"
      case (15)
        C255_opom="???"
      case (16)
        C255_opom="???"
      case (17)
        C255_opom="???"
      case (18)
        C255_opom="???"
      case (19)
        C255_opom="???"
      case (20)
        C255_opom="???"
      case (21)
        C255_opom="???"
      case (22)
        C255_opom="???"
      case (23)
        C255_opom="???"
      case (24)
        C255_opom="???"
      case (25)
        C255_opom="???"
      case (26)
        C255_opom="???"
      case (27)
        C255_opom="???"
      case (28)
        C255_opom="???"
      case (29)
        C255_opom="???"
      case (30)
        C255_opom="???"
      end select
C control_th.fgi( 357,  33):MarkVI text out 30 variants,mo_text8
      L_(83) = L_(100).AND.(.NOT.L_ikil)
C control_th.fgi( 300,  45):�
      I_(71)=0
      if(L_(83))I_(71)=8
C control_th.fgi( 311,  46):lo2digit
      L_(82) = (.NOT.L_(94)).AND.L_(99)
C control_th.fgi( 300,  33):�
      I_(70)=0
      if(L_(82))I_(70)=10
C control_th.fgi( 311,  33):lo2digit
      L_(81) = (.NOT.L_(83)).AND.(.NOT.L_(82))
C control_th.fgi( 306,  42):�
      I_(69)=0
      if(L_(81))I_(69)=6
C control_th.fgi( 311,  42):lo2digit
      I_(98) = I_(71) + I_(70) + I_(69)
C control_th.fgi( 319,  44):��������
      select case (I_(98))
      case (1)
        C255_epom="��� �����"
      case (2)
        C255_epom="���������� � UZ"
      case (3)
        C255_epom="������������"
      case (4)
        C255_epom="��� 380�"
      case (5)
        C255_epom="�������������"
      case (6)
        C255_epom="����� � ������"
      case (7)
        C255_epom="����� ����"
      case (8)
        C255_epom="������ ������"
      case (9)
        C255_epom="������ �� ����"
      case (10)
        C255_epom="������ �����"
      case (11)
        C255_epom="����� �������"
      case (12)
        C255_epom="����������������"
      case (13)
        C255_epom="����� ����"
      case (14)
        C255_epom="����� ������� �����"
      case (15)
        C255_epom="������ �������"
      case (16)
        C255_epom="������ ����������"
      case (17)
        C255_epom="�� ����������"
      case (18)
        C255_epom="???"
      case (19)
        C255_epom="???"
      case (20)
        C255_epom="???"
      case (21)
        C255_epom="???"
      case (22)
        C255_epom="???"
      case (23)
        C255_epom="???"
      case (24)
        C255_epom="???"
      case (25)
        C255_epom="???"
      case (26)
        C255_epom="???"
      case (27)
        C255_epom="???"
      case (28)
        C255_epom="???"
      case (29)
        C255_epom="???"
      case (30)
        C255_epom="???"
      end select
C control_th.fgi( 357,  44):MarkVI text out 30 variants,mo_text10
      L_(88) =.NOT.(L_elil)
C control_th.fgi( 331,  61):���
      I_(78)=0
      if(L_(88))I_(78)=7
C control_th.fgi( 337,  61):lo2digit
      I_(77)=0
      if(L_elil)I_(77)=6
C control_th.fgi( 337,  59):lo2digit
      I_(101) = I_(78) + I_(77)
C control_th.fgi( 343,  60):��������
      select case (I_(101))
      case (1)
        C255_upom="�����"
      case (2)
        C255_upom="� ������"
      case (3)
        C255_upom="� ������"
      case (4)
        C255_upom="��������"
      case (5)
        C255_upom="�� ����������"
      case (6)
        C255_upom="����� ������ � ����������"
      case (7)
        C255_upom="����� ������ ����������"
      case (8)
        C255_upom="???"
      case (9)
        C255_upom="???"
      case (10)
        C255_upom="???"
      case (11)
        C255_upom="???"
      case (12)
        C255_upom="???"
      case (13)
        C255_upom="???"
      case (14)
        C255_upom="???"
      case (15)
        C255_upom="???"
      case (16)
        C255_upom="???"
      case (17)
        C255_upom="???"
      case (18)
        C255_upom="???"
      case (19)
        C255_upom="???"
      case (20)
        C255_upom="???"
      case (21)
        C255_upom="???"
      case (22)
        C255_upom="???"
      case (23)
        C255_upom="???"
      case (24)
        C255_upom="???"
      case (25)
        C255_upom="???"
      case (26)
        C255_upom="???"
      case (27)
        C255_upom="???"
      case (28)
        C255_upom="???"
      case (29)
        C255_upom="???"
      case (30)
        C255_upom="???"
      end select
C control_th.fgi( 357,  60):MarkVI text out 30 variants,mo_text7
      L_(86) = L_(104).AND.(.NOT.L_elil)
C control_th.fgi( 300,  66):�
      I_(74)=0
      if(L_(86))I_(74)=8
C control_th.fgi( 311,  67):lo2digit
      L_(85) = (.NOT.L_(95)).AND.L_(103)
C control_th.fgi( 300,  54):�
      I_(73)=0
      if(L_(85))I_(73)=10
C control_th.fgi( 311,  54):lo2digit
      L_(84) = (.NOT.L_(86)).AND.(.NOT.L_(85))
C control_th.fgi( 306,  63):�
      I_(72)=0
      if(L_(84))I_(72)=6
C control_th.fgi( 311,  63):lo2digit
      I_(99) = I_(74) + I_(73) + I_(72)
C control_th.fgi( 319,  65):��������
      select case (I_(99))
      case (1)
        C255_ipom="��� �����"
      case (2)
        C255_ipom="���������� � UZ"
      case (3)
        C255_ipom="������������"
      case (4)
        C255_ipom="��� 380�"
      case (5)
        C255_ipom="�������������"
      case (6)
        C255_ipom="����� � ������"
      case (7)
        C255_ipom="����� ����"
      case (8)
        C255_ipom="������ ������"
      case (9)
        C255_ipom="������ �� ����"
      case (10)
        C255_ipom="������ �����"
      case (11)
        C255_ipom="����� �������"
      case (12)
        C255_ipom="����������������"
      case (13)
        C255_ipom="����� ����"
      case (14)
        C255_ipom="����� ������� �����"
      case (15)
        C255_ipom="������ �������"
      case (16)
        C255_ipom="������ ����������"
      case (17)
        C255_ipom="�� ����������"
      case (18)
        C255_ipom="???"
      case (19)
        C255_ipom="???"
      case (20)
        C255_ipom="???"
      case (21)
        C255_ipom="???"
      case (22)
        C255_ipom="???"
      case (23)
        C255_ipom="???"
      case (24)
        C255_ipom="???"
      case (25)
        C255_ipom="???"
      case (26)
        C255_ipom="???"
      case (27)
        C255_ipom="???"
      case (28)
        C255_ipom="???"
      case (29)
        C255_ipom="???"
      case (30)
        C255_ipom="???"
      end select
C control_th.fgi( 357,  71):MarkVI text out 30 variants,mo_text9
      L_(149) = L_evom.AND.L_ipem
C control_th.fgi( 356, 223):�
      L_(145) = (.NOT.L_ipem).AND.L_evom
C control_th.fgi( 356, 219):�
      L_(148) = L_ivom.AND.L_opem
C control_th.fgi( 356, 215):�
      L_(144) = (.NOT.L_opem).AND.L_ivom
C control_th.fgi( 356, 211):�
      L_(147) = L_ovom.AND.L_upem
C control_th.fgi( 356, 207):�
      L_(143) = (.NOT.L_upem).AND.L_ovom
C control_th.fgi( 356, 203):�
      L_(146) = L_uvom.AND.L_arem
C control_th.fgi( 356, 199):�
      L_(142) = (.NOT.L_arem).AND.L_uvom
C control_th.fgi( 356, 195):�
      L_imim = (.NOT.L_(153))
C control_th.fgi(  81, 256):���
      L0_umim=.false.
      I0_omim=0

      if(L0_ekap)then
        I0_edum=1
         L0_emim=.false.
         L0_adim=.false.
         L0_usem=.false.
         L0_esem=.false.
         L0_asem=.false.
         L0_urem=.false.
         L0_orem=.false.
         L0_irem=.false.
         L0_erem=.false.
         L0_ulim=.false.
         L0_ilim=.false.
         L0_alim=.false.
         L0_okim=.false.
         L0_ekim=.false.
         L0_ufim=.false.
         L0_ifim=.false.
         L0_afim=.false.
         L0_odim=.false.
         L0_edim=.false.
         L0_obim=.false.
         L0_ebim=.false.
         L0_uxem=.false.
         L0_ixem=.false.
         L0_axem=.false.
         L0_ovem=.false.
         L0_evem=.false.
         L0_utem=.false.
         L0_item=.false.
         L0_atem=.false.
         L0_isem=.false.
      endif

      if(L0_emim)I0_omim=I0_omim+1
      if(L0_adim)I0_omim=I0_omim+1
      if(L0_usem)I0_omim=I0_omim+1
      if(L0_esem)I0_omim=I0_omim+1
      if(L0_asem)I0_omim=I0_omim+1
      if(L0_urem)I0_omim=I0_omim+1
      if(L0_orem)I0_omim=I0_omim+1
      if(L0_irem)I0_omim=I0_omim+1
      if(L0_erem)I0_omim=I0_omim+1
      if(L0_ulim)I0_omim=I0_omim+1
      if(L0_ilim)I0_omim=I0_omim+1
      if(L0_alim)I0_omim=I0_omim+1
      if(L0_okim)I0_omim=I0_omim+1
      if(L0_ekim)I0_omim=I0_omim+1
      if(L0_ufim)I0_omim=I0_omim+1
      if(L0_ifim)I0_omim=I0_omim+1
      if(L0_afim)I0_omim=I0_omim+1
      if(L0_odim)I0_omim=I0_omim+1
      if(L0_edim)I0_omim=I0_omim+1
      if(L0_obim)I0_omim=I0_omim+1
      if(L0_ebim)I0_omim=I0_omim+1
      if(L0_uxem)I0_omim=I0_omim+1
      if(L0_ixem)I0_omim=I0_omim+1
      if(L0_axem)I0_omim=I0_omim+1
      if(L0_ovem)I0_omim=I0_omim+1
      if(L0_evem)I0_omim=I0_omim+1
      if(L0_utem)I0_omim=I0_omim+1
      if(L0_item)I0_omim=I0_omim+1
      if(L0_atem)I0_omim=I0_omim+1
      if(L0_isem)I0_omim=I0_omim+1


      if(.not.L0_umim.and.L_imim.and..not.L0_emim)then
        I0_edum=1
        L0_umim=.true.
      endif

      if(.not.L0_umim.and.L_(149).and..not.L0_adim)then
        I0_edum=2
        L0_umim=.true.
      endif

      if(.not.L0_umim.and.L_(148).and..not.L0_usem)then
        I0_edum=3
        L0_umim=.true.
      endif

      if(.not.L0_umim.and.L_(147).and..not.L0_esem)then
        I0_edum=4
        L0_umim=.true.
      endif

      if(.not.L0_umim.and.L_(146).and..not.L0_asem)then
        I0_edum=5
        L0_umim=.true.
      endif

      if(.not.L0_umim.and.L_(145).and..not.L0_urem)then
        I0_edum=6
        L0_umim=.true.
      endif

      if(.not.L0_umim.and.L_(144).and..not.L0_orem)then
        I0_edum=7
        L0_umim=.true.
      endif

      if(.not.L0_umim.and.L_(143).and..not.L0_irem)then
        I0_edum=8
        L0_umim=.true.
      endif

      if(.not.L0_umim.and.L_(142).and..not.L0_erem)then
        I0_edum=9
        L0_umim=.true.
      endif

      if(.not.L0_umim.and.L0_amim.and..not.L0_ulim)then
        I0_edum=10
        L0_umim=.true.
      endif

      if(.not.L0_umim.and.L0_olim.and..not.L0_ilim)then
        I0_edum=11
        L0_umim=.true.
      endif

      if(.not.L0_umim.and.L0_elim.and..not.L0_alim)then
        I0_edum=12
        L0_umim=.true.
      endif

      if(.not.L0_umim.and.L0_ukim.and..not.L0_okim)then
        I0_edum=13
        L0_umim=.true.
      endif

      if(.not.L0_umim.and.L0_ikim.and..not.L0_ekim)then
        I0_edum=14
        L0_umim=.true.
      endif

      if(.not.L0_umim.and.L0_akim.and..not.L0_ufim)then
        I0_edum=15
        L0_umim=.true.
      endif

      if(.not.L0_umim.and.L0_ofim.and..not.L0_ifim)then
        I0_edum=16
        L0_umim=.true.
      endif

      if(.not.L0_umim.and.L0_efim.and..not.L0_afim)then
        I0_edum=17
        L0_umim=.true.
      endif

      if(.not.L0_umim.and.L0_udim.and..not.L0_odim)then
        I0_edum=18
        L0_umim=.true.
      endif

      if(.not.L0_umim.and.L0_idim.and..not.L0_edim)then
        I0_edum=19
        L0_umim=.true.
      endif

      if(.not.L0_umim.and.L0_ubim.and..not.L0_obim)then
        I0_edum=20
        L0_umim=.true.
      endif

      if(.not.L0_umim.and.L0_ibim.and..not.L0_ebim)then
        I0_edum=21
        L0_umim=.true.
      endif

      if(.not.L0_umim.and.L0_abim.and..not.L0_uxem)then
        I0_edum=22
        L0_umim=.true.
      endif

      if(.not.L0_umim.and.L0_oxem.and..not.L0_ixem)then
        I0_edum=23
        L0_umim=.true.
      endif

      if(.not.L0_umim.and.L0_exem.and..not.L0_axem)then
        I0_edum=24
        L0_umim=.true.
      endif

      if(.not.L0_umim.and.L0_uvem.and..not.L0_ovem)then
        I0_edum=25
        L0_umim=.true.
      endif

      if(.not.L0_umim.and.L0_ivem.and..not.L0_evem)then
        I0_edum=26
        L0_umim=.true.
      endif

      if(.not.L0_umim.and.L0_avem.and..not.L0_utem)then
        I0_edum=27
        L0_umim=.true.
      endif

      if(.not.L0_umim.and.L0_otem.and..not.L0_item)then
        I0_edum=28
        L0_umim=.true.
      endif

      if(.not.L0_umim.and.L0_etem.and..not.L0_atem)then
        I0_edum=29
        L0_umim=.true.
      endif

      if(.not.L0_umim.and.L0_osem.and..not.L0_isem)then
        I0_edum=30
        L0_umim=.true.
      endif

      if(I0_omim.eq.1)then
         L0_emim=.false.
         L0_adim=.false.
         L0_usem=.false.
         L0_esem=.false.
         L0_asem=.false.
         L0_urem=.false.
         L0_orem=.false.
         L0_irem=.false.
         L0_erem=.false.
         L0_ulim=.false.
         L0_ilim=.false.
         L0_alim=.false.
         L0_okim=.false.
         L0_ekim=.false.
         L0_ufim=.false.
         L0_ifim=.false.
         L0_afim=.false.
         L0_odim=.false.
         L0_edim=.false.
         L0_obim=.false.
         L0_ebim=.false.
         L0_uxem=.false.
         L0_ixem=.false.
         L0_axem=.false.
         L0_ovem=.false.
         L0_evem=.false.
         L0_utem=.false.
         L0_item=.false.
         L0_atem=.false.
         L0_isem=.false.
      else
         L0_emim=L_imim
         L0_adim=L_(149)
         L0_usem=L_(148)
         L0_esem=L_(147)
         L0_asem=L_(146)
         L0_urem=L_(145)
         L0_orem=L_(144)
         L0_irem=L_(143)
         L0_erem=L_(142)
         L0_ulim=L0_amim
         L0_ilim=L0_olim
         L0_alim=L0_elim
         L0_okim=L0_ukim
         L0_ekim=L0_ikim
         L0_ufim=L0_akim
         L0_ifim=L0_ofim
         L0_afim=L0_efim
         L0_odim=L0_udim
         L0_edim=L0_idim
         L0_obim=L0_ubim
         L0_ebim=L0_ibim
         L0_uxem=L0_abim
         L0_ixem=L0_oxem
         L0_axem=L0_exem
         L0_ovem=L0_uvem
         L0_evem=L0_ivem
         L0_utem=L0_avem
         L0_item=L0_otem
         L0_atem=L0_etem
         L0_isem=L0_osem
      endif
C control_th.fgi( 371, 196):��������� �������
      select case (I0_edum)
      case (1)
        C255_orom="���.�� �������"
      case (2)
        C255_orom="���.������ ������ ������� �������"
      case (3)
        C255_orom="���.�������� ������ ������� �������"
      case (4)
        C255_orom="���.����������� ������ ������� �������"
      case (5)
        C255_orom="���.��������� ������ ������� �������"
      case (6)
        C255_orom="���.������ �� ������ ������� �������"
      case (7)
        C255_orom="���.�������� �� ������ ������� �������"
      case (8)
        C255_orom="���.����������� �� ������ ������� �������"
      case (9)
        C255_orom="���.��������� �� ������ ������� �������"
      case (10)
        C255_orom="???"
      case (11)
        C255_orom="???"
      case (12)
        C255_orom="???"
      case (13)
        C255_orom="???"
      case (14)
        C255_orom="???"
      case (15)
        C255_orom="???"
      case (16)
        C255_orom="???"
      case (17)
        C255_orom="???"
      case (18)
        C255_orom="???"
      case (19)
        C255_orom="???"
      case (20)
        C255_orom="???"
      case (21)
        C255_orom="???"
      case (22)
        C255_orom="???"
      case (23)
        C255_orom="???"
      case (24)
        C255_orom="???"
      case (25)
        C255_orom="???"
      case (26)
        C255_orom="???"
      case (27)
        C255_orom="???"
      case (28)
        C255_orom="???"
      case (29)
        C255_orom="???"
      case (30)
        C255_orom="???"
      end select
C control_th.fgi( 386, 225):MarkVI text out 30 variants,mo_text2
      L_(134) = (.NOT.L_imim)
C control_th.fgi( 220, 251):���
      L_(133) = L_ufol.AND.L_(134).AND.(.NOT.L_ofol).AND.
     &(.NOT.L_utom).AND.(.NOT.L_otom)
C control_th.fgi( 237, 251):�
      L_ukum = (.NOT.L_esul).AND.L_(133)
C control_th.fgi( 244, 252):�
      L_irul = (.NOT.L_ukum).AND.(.NOT.L_okum).AND.(.NOT.L_amam
     &).AND.(.NOT.L_opul).AND.(.NOT.L_epul).AND.(.NOT.L_esul
     &)
C control_th.fgi( 259, 211):�
      L0_apam=.false.
      I0_omam=0

      if(L0_ekap)then
        I0_umam=1
         L0_emam=.false.
         L0_edam=.false.
         L0_usul=.false.
         L0_asul=.false.
         L0_orul=.false.
         L0_erul=.false.
         L0_upul=.false.
         L0_ipul=.false.
         L0_apul=.false.
         L0_ulam=.false.
         L0_olam=.false.
         L0_ilam=.false.
         L0_alam=.false.
         L0_okam=.false.
         L0_ekam=.false.
         L0_ufam=.false.
         L0_ifam=.false.
         L0_afam=.false.
         L0_odam=.false.
         L0_ubam=.false.
         L0_ibam=.false.
         L0_abam=.false.
         L0_oxul=.false.
         L0_exul=.false.
         L0_uvul=.false.
         L0_ivul=.false.
         L0_avul=.false.
         L0_otul=.false.
         L0_etul=.false.
         L0_isul=.false.
      endif

      if(L0_emam)I0_omam=I0_omam+1
      if(L0_edam)I0_omam=I0_omam+1
      if(L0_usul)I0_omam=I0_omam+1
      if(L0_asul)I0_omam=I0_omam+1
      if(L0_orul)I0_omam=I0_omam+1
      if(L0_erul)I0_omam=I0_omam+1
      if(L0_upul)I0_omam=I0_omam+1
      if(L0_ipul)I0_omam=I0_omam+1
      if(L0_apul)I0_omam=I0_omam+1
      if(L0_ulam)I0_omam=I0_omam+1
      if(L0_olam)I0_omam=I0_omam+1
      if(L0_ilam)I0_omam=I0_omam+1
      if(L0_alam)I0_omam=I0_omam+1
      if(L0_okam)I0_omam=I0_omam+1
      if(L0_ekam)I0_omam=I0_omam+1
      if(L0_ufam)I0_omam=I0_omam+1
      if(L0_ifam)I0_omam=I0_omam+1
      if(L0_afam)I0_omam=I0_omam+1
      if(L0_odam)I0_omam=I0_omam+1
      if(L0_ubam)I0_omam=I0_omam+1
      if(L0_ibam)I0_omam=I0_omam+1
      if(L0_abam)I0_omam=I0_omam+1
      if(L0_oxul)I0_omam=I0_omam+1
      if(L0_exul)I0_omam=I0_omam+1
      if(L0_uvul)I0_omam=I0_omam+1
      if(L0_ivul)I0_omam=I0_omam+1
      if(L0_avul)I0_omam=I0_omam+1
      if(L0_otul)I0_omam=I0_omam+1
      if(L0_etul)I0_omam=I0_omam+1
      if(L0_isul)I0_omam=I0_omam+1


      if(.not.L0_apam.and.L0_imam.and..not.L0_emam)then
        I0_umam=1
        L0_apam=.true.
      endif

      if(.not.L0_apam.and.L0_idam.and..not.L0_edam)then
        I0_umam=2
        L0_apam=.true.
      endif

      if(.not.L0_apam.and.L0_atul.and..not.L0_usul)then
        I0_umam=3
        L0_apam=.true.
      endif

      if(.not.L0_apam.and.L_esul.and..not.L0_asul)then
        I0_umam=4
        L0_apam=.true.
      endif

      if(.not.L0_apam.and.L_urul.and..not.L0_orul)then
        I0_umam=5
        L0_apam=.true.
      endif

      if(.not.L0_apam.and.L_irul.and..not.L0_erul)then
        I0_umam=6
        L0_apam=.true.
      endif

      if(.not.L0_apam.and.L0_arul.and..not.L0_upul)then
        I0_umam=7
        L0_apam=.true.
      endif

      if(.not.L0_apam.and.L_opul.and..not.L0_ipul)then
        I0_umam=8
        L0_apam=.true.
      endif

      if(.not.L0_apam.and.L_epul.and..not.L0_apul)then
        I0_umam=9
        L0_apam=.true.
      endif

      if(.not.L0_apam.and.L_amam.and..not.L0_ulam)then
        I0_umam=10
        L0_apam=.true.
      endif

      if(.not.L0_apam.and.L_ukum.and..not.L0_olam)then
        I0_umam=11
        L0_apam=.true.
      endif

      if(.not.L0_apam.and.L_okum.and..not.L0_ilam)then
        I0_umam=12
        L0_apam=.true.
      endif

      if(.not.L0_apam.and.L0_elam.and..not.L0_alam)then
        I0_umam=13
        L0_apam=.true.
      endif

      if(.not.L0_apam.and.L0_ukam.and..not.L0_okam)then
        I0_umam=14
        L0_apam=.true.
      endif

      if(.not.L0_apam.and.L0_ikam.and..not.L0_ekam)then
        I0_umam=15
        L0_apam=.true.
      endif

      if(.not.L0_apam.and.L0_akam.and..not.L0_ufam)then
        I0_umam=16
        L0_apam=.true.
      endif

      if(.not.L0_apam.and.L0_ofam.and..not.L0_ifam)then
        I0_umam=17
        L0_apam=.true.
      endif

      if(.not.L0_apam.and.L0_efam.and..not.L0_afam)then
        I0_umam=18
        L0_apam=.true.
      endif

      if(.not.L0_apam.and.L0_udam.and..not.L0_odam)then
        I0_umam=19
        L0_apam=.true.
      endif

      if(.not.L0_apam.and.L0_adam.and..not.L0_ubam)then
        I0_umam=20
        L0_apam=.true.
      endif

      if(.not.L0_apam.and.L0_obam.and..not.L0_ibam)then
        I0_umam=21
        L0_apam=.true.
      endif

      if(.not.L0_apam.and.L0_ebam.and..not.L0_abam)then
        I0_umam=22
        L0_apam=.true.
      endif

      if(.not.L0_apam.and.L0_uxul.and..not.L0_oxul)then
        I0_umam=23
        L0_apam=.true.
      endif

      if(.not.L0_apam.and.L0_ixul.and..not.L0_exul)then
        I0_umam=24
        L0_apam=.true.
      endif

      if(.not.L0_apam.and.L0_axul.and..not.L0_uvul)then
        I0_umam=25
        L0_apam=.true.
      endif

      if(.not.L0_apam.and.L0_ovul.and..not.L0_ivul)then
        I0_umam=26
        L0_apam=.true.
      endif

      if(.not.L0_apam.and.L0_evul.and..not.L0_avul)then
        I0_umam=27
        L0_apam=.true.
      endif

      if(.not.L0_apam.and.L0_utul.and..not.L0_otul)then
        I0_umam=28
        L0_apam=.true.
      endif

      if(.not.L0_apam.and.L0_itul.and..not.L0_etul)then
        I0_umam=29
        L0_apam=.true.
      endif

      if(.not.L0_apam.and.L0_osul.and..not.L0_isul)then
        I0_umam=30
        L0_apam=.true.
      endif

      if(I0_omam.eq.1)then
         L0_emam=.false.
         L0_edam=.false.
         L0_usul=.false.
         L0_asul=.false.
         L0_orul=.false.
         L0_erul=.false.
         L0_upul=.false.
         L0_ipul=.false.
         L0_apul=.false.
         L0_ulam=.false.
         L0_olam=.false.
         L0_ilam=.false.
         L0_alam=.false.
         L0_okam=.false.
         L0_ekam=.false.
         L0_ufam=.false.
         L0_ifam=.false.
         L0_afam=.false.
         L0_odam=.false.
         L0_ubam=.false.
         L0_ibam=.false.
         L0_abam=.false.
         L0_oxul=.false.
         L0_exul=.false.
         L0_uvul=.false.
         L0_ivul=.false.
         L0_avul=.false.
         L0_otul=.false.
         L0_etul=.false.
         L0_isul=.false.
      else
         L0_emam=L0_imam
         L0_edam=L0_idam
         L0_usul=L0_atul
         L0_asul=L_esul
         L0_orul=L_urul
         L0_erul=L_irul
         L0_upul=L0_arul
         L0_ipul=L_opul
         L0_apul=L_epul
         L0_ulam=L_amam
         L0_olam=L_ukum
         L0_ilam=L_okum
         L0_alam=L0_elam
         L0_okam=L0_ukam
         L0_ekam=L0_ikam
         L0_ufam=L0_akam
         L0_ifam=L0_ofam
         L0_afam=L0_efam
         L0_odam=L0_udam
         L0_ubam=L0_adam
         L0_ibam=L0_obam
         L0_abam=L0_ebam
         L0_oxul=L0_uxul
         L0_exul=L0_ixul
         L0_uvul=L0_axul
         L0_ivul=L0_ovul
         L0_avul=L0_evul
         L0_otul=L0_utul
         L0_etul=L0_itul
         L0_isul=L0_osul
      endif
C control_th.fgi( 289, 261):��������� �������
      select case (I0_umam)
      case (1)
        C255_umul="��� �����"
      case (2)
        C255_umul="���������� � UZ"
      case (3)
        C255_umul="������������"
      case (4)
        C255_umul="��� 380�"
      case (5)
        C255_umul="�������������"
      case (6)
        C255_umul="����� � ������"
      case (7)
        C255_umul="����� ����"
      case (8)
        C255_umul="������ ������"
      case (9)
        C255_umul="������ �� ����"
      case (10)
        C255_umul="������ �����"
      case (11)
        C255_umul="����� �������"
      case (12)
        C255_umul="����������������"
      case (13)
        C255_umul="����� ����"
      case (14)
        C255_umul="����� ������� �����"
      case (15)
        C255_umul="������ �������"
      case (16)
        C255_umul="������ ����������"
      case (17)
        C255_umul="�� ����������"
      case (18)
        C255_umul="???"
      case (19)
        C255_umul="???"
      case (20)
        C255_umul="???"
      case (21)
        C255_umul="???"
      case (22)
        C255_umul="???"
      case (23)
        C255_umul="???"
      case (24)
        C255_umul="???"
      case (25)
        C255_umul="???"
      case (26)
        C255_umul="???"
      case (27)
        C255_umul="???"
      case (28)
        C255_umul="???"
      case (29)
        C255_umul="???"
      case (30)
        C255_umul="???"
      end select
C control_th.fgi( 306, 290):MarkVI text out 30 variants,mo_text5
      L_(127) = (.NOT.L_imim)
C control_th.fgi( 220, 158):���
      L_(126) = L_ufol.AND.L_(127).AND.(.NOT.L_ofol).AND.
     &(.NOT.L_utom).AND.(.NOT.L_otom)
C control_th.fgi( 237, 158):�
      L_ikum = (.NOT.L_akul).AND.L_(126)
C control_th.fgi( 244, 159):�
      L_(161) = L_ukum.OR.L_okum.OR.L_ikum.OR.L_ekum
C common.fgi(  97,  93):���
      Call CONTROL_FDD_1(ext_deltat)
      End
      Subroutine CONTROL_FDD_1(ext_deltat)
      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      Include 'CONTROL_FDD.fh'

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      L0_ikap=.false.
      I0_akap=0

      if(L0_ekap)then
        I0_ukap=1
         L0_ofap=.false.
         L0_ovum=.false.
         L0_epum=.false.
         L0_omum=.false.
         L0_emum=.false.
         L0_ulum=.false.
         L0_olum=.false.
         L0_ilum=.false.
         L0_elum=.false.
         L0_ifap=.false.
         L0_afap=.false.
         L0_odap=.false.
         L0_edap=.false.
         L0_ubap=.false.
         L0_ibap=.false.
         L0_abap=.false.
         L0_oxum=.false.
         L0_exum=.false.
         L0_uvum=.false.
         L0_evum=.false.
         L0_utum=.false.
         L0_itum=.false.
         L0_atum=.false.
         L0_osum=.false.
         L0_esum=.false.
         L0_urum=.false.
         L0_irum=.false.
         L0_arum=.false.
         L0_opum=.false.
         L0_apum=.false.
      endif

      if(L0_ofap)I0_akap=I0_akap+1
      if(L0_ovum)I0_akap=I0_akap+1
      if(L0_epum)I0_akap=I0_akap+1
      if(L0_omum)I0_akap=I0_akap+1
      if(L0_emum)I0_akap=I0_akap+1
      if(L0_ulum)I0_akap=I0_akap+1
      if(L0_olum)I0_akap=I0_akap+1
      if(L0_ilum)I0_akap=I0_akap+1
      if(L0_elum)I0_akap=I0_akap+1
      if(L0_ifap)I0_akap=I0_akap+1
      if(L0_afap)I0_akap=I0_akap+1
      if(L0_odap)I0_akap=I0_akap+1
      if(L0_edap)I0_akap=I0_akap+1
      if(L0_ubap)I0_akap=I0_akap+1
      if(L0_ibap)I0_akap=I0_akap+1
      if(L0_abap)I0_akap=I0_akap+1
      if(L0_oxum)I0_akap=I0_akap+1
      if(L0_exum)I0_akap=I0_akap+1
      if(L0_uvum)I0_akap=I0_akap+1
      if(L0_evum)I0_akap=I0_akap+1
      if(L0_utum)I0_akap=I0_akap+1
      if(L0_itum)I0_akap=I0_akap+1
      if(L0_atum)I0_akap=I0_akap+1
      if(L0_osum)I0_akap=I0_akap+1
      if(L0_esum)I0_akap=I0_akap+1
      if(L0_urum)I0_akap=I0_akap+1
      if(L0_irum)I0_akap=I0_akap+1
      if(L0_arum)I0_akap=I0_akap+1
      if(L0_opum)I0_akap=I0_akap+1
      if(L0_apum)I0_akap=I0_akap+1


      if(.not.L0_ikap.and.L0_ufap.and..not.L0_ofap)then
        I0_ukap=1
        L0_ikap=.true.
      endif

      if(.not.L0_ikap.and.L_(161).and..not.L0_ovum)then
        I0_ukap=2
        L0_ikap=.true.
      endif

      if(.not.L0_ikap.and.L_ipum.and..not.L0_epum)then
        I0_ukap=3
        L0_ikap=.true.
      endif

      if(.not.L0_ikap.and.L_umum.and..not.L0_omum)then
        I0_ukap=4
        L0_ikap=.true.
      endif

      if(.not.L0_ikap.and.L_imum.and..not.L0_emum)then
        I0_ukap=5
        L0_ikap=.true.
      endif

      if(.not.L0_ikap.and.L0_amum.and..not.L0_ulum)then
        I0_ukap=6
        L0_ikap=.true.
      endif

      if(.not.L0_ikap.and.L_(159).and..not.L0_olum)then
        I0_ukap=7
        L0_ikap=.true.
      endif

      if(.not.L0_ikap.and.L_(158).and..not.L0_ilum)then
        I0_ukap=8
        L0_ikap=.true.
      endif

      if(.not.L0_ikap.and.L_(157).and..not.L0_elum)then
        I0_ukap=9
        L0_ikap=.true.
      endif

      if(.not.L0_ikap.and.L_(162).and..not.L0_ifap)then
        I0_ukap=10
        L0_ikap=.true.
      endif

      if(.not.L0_ikap.and.L0_efap.and..not.L0_afap)then
        I0_ukap=11
        L0_ikap=.true.
      endif

      if(.not.L0_ikap.and.L0_udap.and..not.L0_odap)then
        I0_ukap=12
        L0_ikap=.true.
      endif

      if(.not.L0_ikap.and.L0_idap.and..not.L0_edap)then
        I0_ukap=13
        L0_ikap=.true.
      endif

      if(.not.L0_ikap.and.L0_adap.and..not.L0_ubap)then
        I0_ukap=14
        L0_ikap=.true.
      endif

      if(.not.L0_ikap.and.L0_obap.and..not.L0_ibap)then
        I0_ukap=15
        L0_ikap=.true.
      endif

      if(.not.L0_ikap.and.L0_ebap.and..not.L0_abap)then
        I0_ukap=16
        L0_ikap=.true.
      endif

      if(.not.L0_ikap.and.L0_uxum.and..not.L0_oxum)then
        I0_ukap=17
        L0_ikap=.true.
      endif

      if(.not.L0_ikap.and.L0_ixum.and..not.L0_exum)then
        I0_ukap=18
        L0_ikap=.true.
      endif

      if(.not.L0_ikap.and.L0_axum.and..not.L0_uvum)then
        I0_ukap=19
        L0_ikap=.true.
      endif

      if(.not.L0_ikap.and.L0_ivum.and..not.L0_evum)then
        I0_ukap=20
        L0_ikap=.true.
      endif

      if(.not.L0_ikap.and.L0_avum.and..not.L0_utum)then
        I0_ukap=21
        L0_ikap=.true.
      endif

      if(.not.L0_ikap.and.L0_otum.and..not.L0_itum)then
        I0_ukap=22
        L0_ikap=.true.
      endif

      if(.not.L0_ikap.and.L0_etum.and..not.L0_atum)then
        I0_ukap=23
        L0_ikap=.true.
      endif

      if(.not.L0_ikap.and.L0_usum.and..not.L0_osum)then
        I0_ukap=24
        L0_ikap=.true.
      endif

      if(.not.L0_ikap.and.L0_isum.and..not.L0_esum)then
        I0_ukap=25
        L0_ikap=.true.
      endif

      if(.not.L0_ikap.and.L0_asum.and..not.L0_urum)then
        I0_ukap=26
        L0_ikap=.true.
      endif

      if(.not.L0_ikap.and.L0_orum.and..not.L0_irum)then
        I0_ukap=27
        L0_ikap=.true.
      endif

      if(.not.L0_ikap.and.L0_erum.and..not.L0_arum)then
        I0_ukap=28
        L0_ikap=.true.
      endif

      if(.not.L0_ikap.and.L0_upum.and..not.L0_opum)then
        I0_ukap=29
        L0_ikap=.true.
      endif

      if(.not.L0_ikap.and.L_(160).and..not.L0_apum)then
        I0_ukap=30
        L0_ikap=.true.
      endif

      if(I0_akap.eq.1)then
         L0_ofap=.false.
         L0_ovum=.false.
         L0_epum=.false.
         L0_omum=.false.
         L0_emum=.false.
         L0_ulum=.false.
         L0_olum=.false.
         L0_ilum=.false.
         L0_elum=.false.
         L0_ifap=.false.
         L0_afap=.false.
         L0_odap=.false.
         L0_edap=.false.
         L0_ubap=.false.
         L0_ibap=.false.
         L0_abap=.false.
         L0_oxum=.false.
         L0_exum=.false.
         L0_uvum=.false.
         L0_evum=.false.
         L0_utum=.false.
         L0_itum=.false.
         L0_atum=.false.
         L0_osum=.false.
         L0_esum=.false.
         L0_urum=.false.
         L0_irum=.false.
         L0_arum=.false.
         L0_opum=.false.
         L0_apum=.false.
      else
         L0_ofap=L0_ufap
         L0_ovum=L_(161)
         L0_epum=L_ipum
         L0_omum=L_umum
         L0_emum=L_imum
         L0_ulum=L0_amum
         L0_olum=L_(159)
         L0_ilum=L_(158)
         L0_elum=L_(157)
         L0_ifap=L_(162)
         L0_afap=L0_efap
         L0_odap=L0_udap
         L0_edap=L0_idap
         L0_ubap=L0_adap
         L0_ibap=L0_obap
         L0_abap=L0_ebap
         L0_oxum=L0_uxum
         L0_exum=L0_ixum
         L0_uvum=L0_axum
         L0_evum=L0_ivum
         L0_utum=L0_avum
         L0_itum=L0_otum
         L0_atum=L0_etum
         L0_osum=L0_usum
         L0_esum=L0_isum
         L0_urum=L0_asum
         L0_irum=L0_orum
         L0_arum=L0_erum
         L0_opum=L0_upum
         L0_apum=L_(160)
      endif
C common.fgi( 134,  71):��������� �������
      select case (I0_ukap)
      case (1)
        C255_okap="====================="
      case (2)
        C255_okap="����������� � �������"
      case (3)
        C255_okap="������ �� ������� �������"
      case (4)
        C255_okap="�����"
      case (5)
        C255_okap="�����������"
      case (6)
        C255_okap="���������� ������ � ��������"
      case (7)
        C255_okap="�������� ������ ������������"
      case (8)
        C255_okap="���������� �������������"
      case (9)
        C255_okap="�������� ������ �� ��� ��"
      case (10)
        C255_okap="�������� ������ �� ���������"
      case (11)
        C255_okap="�������� ������������� ����������"
      case (12)
        C255_okap="��������� �������"
      case (13)
        C255_okap="???"
      case (14)
        C255_okap="???"
      case (15)
        C255_okap="???"
      case (16)
        C255_okap="???"
      case (17)
        C255_okap="???"
      case (18)
        C255_okap="???"
      case (19)
        C255_okap="???"
      case (20)
        C255_okap="???"
      case (21)
        C255_okap="???"
      case (22)
        C255_okap="???"
      case (23)
        C255_okap="???"
      case (24)
        C255_okap="???"
      case (25)
        C255_okap="???"
      case (26)
        C255_okap="???"
      case (27)
        C255_okap="???"
      case (28)
        C255_okap="???"
      case (29)
        C255_okap="???"
      case (30)
        C255_okap="====================="
      end select
C common.fgi( 152, 100):MarkVI text out 30 variants,regime_state
      L_ilol = (.NOT.L_ikum).AND.(.NOT.L_ekum).AND.(.NOT.L_udul
     &).AND.(.NOT.L_okol).AND.(.NOT.L_ekol).AND.(.NOT.L_akul
     &)
C control_th.fgi( 259, 118):�
      L0_oful=.false.
      I0_iful=0

      if(L0_ekap)then
        I0_erom=1
         L0_aful=.false.
         L0_avol=.false.
         L0_omol=.false.
         L0_amol=.false.
         L0_olol=.false.
         L0_elol=.false.
         L0_ukol=.false.
         L0_ikol=.false.
         L0_akol=.false.
         L0_odul=.false.
         L0_idul=.false.
         L0_edul=.false.
         L0_ubul=.false.
         L0_ibul=.false.
         L0_abul=.false.
         L0_oxol=.false.
         L0_exol=.false.
         L0_uvol=.false.
         L0_ivol=.false.
         L0_otol=.false.
         L0_etol=.false.
         L0_usol=.false.
         L0_isol=.false.
         L0_asol=.false.
         L0_orol=.false.
         L0_erol=.false.
         L0_upol=.false.
         L0_ipol=.false.
         L0_apol=.false.
         L0_emol=.false.
      endif

      if(L0_aful)I0_iful=I0_iful+1
      if(L0_avol)I0_iful=I0_iful+1
      if(L0_omol)I0_iful=I0_iful+1
      if(L0_amol)I0_iful=I0_iful+1
      if(L0_olol)I0_iful=I0_iful+1
      if(L0_elol)I0_iful=I0_iful+1
      if(L0_ukol)I0_iful=I0_iful+1
      if(L0_ikol)I0_iful=I0_iful+1
      if(L0_akol)I0_iful=I0_iful+1
      if(L0_odul)I0_iful=I0_iful+1
      if(L0_idul)I0_iful=I0_iful+1
      if(L0_edul)I0_iful=I0_iful+1
      if(L0_ubul)I0_iful=I0_iful+1
      if(L0_ibul)I0_iful=I0_iful+1
      if(L0_abul)I0_iful=I0_iful+1
      if(L0_oxol)I0_iful=I0_iful+1
      if(L0_exol)I0_iful=I0_iful+1
      if(L0_uvol)I0_iful=I0_iful+1
      if(L0_ivol)I0_iful=I0_iful+1
      if(L0_otol)I0_iful=I0_iful+1
      if(L0_etol)I0_iful=I0_iful+1
      if(L0_usol)I0_iful=I0_iful+1
      if(L0_isol)I0_iful=I0_iful+1
      if(L0_asol)I0_iful=I0_iful+1
      if(L0_orol)I0_iful=I0_iful+1
      if(L0_erol)I0_iful=I0_iful+1
      if(L0_upol)I0_iful=I0_iful+1
      if(L0_ipol)I0_iful=I0_iful+1
      if(L0_apol)I0_iful=I0_iful+1
      if(L0_emol)I0_iful=I0_iful+1


      if(.not.L0_oful.and.L0_eful.and..not.L0_aful)then
        I0_erom=1
        L0_oful=.true.
      endif

      if(.not.L0_oful.and.L0_evol.and..not.L0_avol)then
        I0_erom=2
        L0_oful=.true.
      endif

      if(.not.L0_oful.and.L0_umol.and..not.L0_omol)then
        I0_erom=3
        L0_oful=.true.
      endif

      if(.not.L0_oful.and.L_akul.and..not.L0_amol)then
        I0_erom=4
        L0_oful=.true.
      endif

      if(.not.L0_oful.and.L_ulol.and..not.L0_olol)then
        I0_erom=5
        L0_oful=.true.
      endif

      if(.not.L0_oful.and.L_ilol.and..not.L0_elol)then
        I0_erom=6
        L0_oful=.true.
      endif

      if(.not.L0_oful.and.L0_alol.and..not.L0_ukol)then
        I0_erom=7
        L0_oful=.true.
      endif

      if(.not.L0_oful.and.L_okol.and..not.L0_ikol)then
        I0_erom=8
        L0_oful=.true.
      endif

      if(.not.L0_oful.and.L_ekol.and..not.L0_akol)then
        I0_erom=9
        L0_oful=.true.
      endif

      if(.not.L0_oful.and.L_udul.and..not.L0_odul)then
        I0_erom=10
        L0_oful=.true.
      endif

      if(.not.L0_oful.and.L_ikum.and..not.L0_idul)then
        I0_erom=11
        L0_oful=.true.
      endif

      if(.not.L0_oful.and.L_ekum.and..not.L0_edul)then
        I0_erom=12
        L0_oful=.true.
      endif

      if(.not.L0_oful.and.L0_adul.and..not.L0_ubul)then
        I0_erom=13
        L0_oful=.true.
      endif

      if(.not.L0_oful.and.L0_obul.and..not.L0_ibul)then
        I0_erom=14
        L0_oful=.true.
      endif

      if(.not.L0_oful.and.L0_ebul.and..not.L0_abul)then
        I0_erom=15
        L0_oful=.true.
      endif

      if(.not.L0_oful.and.L0_uxol.and..not.L0_oxol)then
        I0_erom=16
        L0_oful=.true.
      endif

      if(.not.L0_oful.and.L0_ixol.and..not.L0_exol)then
        I0_erom=17
        L0_oful=.true.
      endif

      if(.not.L0_oful.and.L0_axol.and..not.L0_uvol)then
        I0_erom=18
        L0_oful=.true.
      endif

      if(.not.L0_oful.and.L0_ovol.and..not.L0_ivol)then
        I0_erom=19
        L0_oful=.true.
      endif

      if(.not.L0_oful.and.L0_utol.and..not.L0_otol)then
        I0_erom=20
        L0_oful=.true.
      endif

      if(.not.L0_oful.and.L0_itol.and..not.L0_etol)then
        I0_erom=21
        L0_oful=.true.
      endif

      if(.not.L0_oful.and.L0_atol.and..not.L0_usol)then
        I0_erom=22
        L0_oful=.true.
      endif

      if(.not.L0_oful.and.L0_osol.and..not.L0_isol)then
        I0_erom=23
        L0_oful=.true.
      endif

      if(.not.L0_oful.and.L0_esol.and..not.L0_asol)then
        I0_erom=24
        L0_oful=.true.
      endif

      if(.not.L0_oful.and.L0_urol.and..not.L0_orol)then
        I0_erom=25
        L0_oful=.true.
      endif

      if(.not.L0_oful.and.L0_irol.and..not.L0_erol)then
        I0_erom=26
        L0_oful=.true.
      endif

      if(.not.L0_oful.and.L0_arol.and..not.L0_upol)then
        I0_erom=27
        L0_oful=.true.
      endif

      if(.not.L0_oful.and.L0_opol.and..not.L0_ipol)then
        I0_erom=28
        L0_oful=.true.
      endif

      if(.not.L0_oful.and.L0_epol.and..not.L0_apol)then
        I0_erom=29
        L0_oful=.true.
      endif

      if(.not.L0_oful.and.L0_imol.and..not.L0_emol)then
        I0_erom=30
        L0_oful=.true.
      endif

      if(I0_iful.eq.1)then
         L0_aful=.false.
         L0_avol=.false.
         L0_omol=.false.
         L0_amol=.false.
         L0_olol=.false.
         L0_elol=.false.
         L0_ukol=.false.
         L0_ikol=.false.
         L0_akol=.false.
         L0_odul=.false.
         L0_idul=.false.
         L0_edul=.false.
         L0_ubul=.false.
         L0_ibul=.false.
         L0_abul=.false.
         L0_oxol=.false.
         L0_exol=.false.
         L0_uvol=.false.
         L0_ivol=.false.
         L0_otol=.false.
         L0_etol=.false.
         L0_usol=.false.
         L0_isol=.false.
         L0_asol=.false.
         L0_orol=.false.
         L0_erol=.false.
         L0_upol=.false.
         L0_ipol=.false.
         L0_apol=.false.
         L0_emol=.false.
      else
         L0_aful=L0_eful
         L0_avol=L0_evol
         L0_omol=L0_umol
         L0_amol=L_akul
         L0_olol=L_ulol
         L0_elol=L_ilol
         L0_ukol=L0_alol
         L0_ikol=L_okol
         L0_akol=L_ekol
         L0_odul=L_udul
         L0_idul=L_ikum
         L0_edul=L_ekum
         L0_ubul=L0_adul
         L0_ibul=L0_obul
         L0_abul=L0_ebul
         L0_oxol=L0_uxol
         L0_exol=L0_ixol
         L0_uvol=L0_axol
         L0_ivol=L0_ovol
         L0_otol=L0_utol
         L0_etol=L0_itol
         L0_usol=L0_atol
         L0_isol=L0_osol
         L0_asol=L0_esol
         L0_orol=L0_urol
         L0_erol=L0_irol
         L0_upol=L0_arol
         L0_ipol=L0_opol
         L0_apol=L0_epol
         L0_emol=L0_imol
      endif
C control_th.fgi( 289, 168):��������� �������
      select case (I0_erom)
      case (1)
        C255_arom="��� �����"
      case (2)
        C255_arom="���������� � UZ"
      case (3)
        C255_arom="������������"
      case (4)
        C255_arom="��� 380�"
      case (5)
        C255_arom="�������������"
      case (6)
        C255_arom="����� � ������"
      case (7)
        C255_arom="����� ����"
      case (8)
        C255_arom="������ ������"
      case (9)
        C255_arom="������ �� ����"
      case (10)
        C255_arom="������ �����"
      case (11)
        C255_arom="����� �������"
      case (12)
        C255_arom="����������������"
      case (13)
        C255_arom="����� ����"
      case (14)
        C255_arom="����� ������� �����"
      case (15)
        C255_arom="������ �������"
      case (16)
        C255_arom="������ ����������"
      case (17)
        C255_arom="�� ����������"
      case (18)
        C255_arom="???"
      case (19)
        C255_arom="???"
      case (20)
        C255_arom="???"
      case (21)
        C255_arom="???"
      case (22)
        C255_arom="???"
      case (23)
        C255_arom="???"
      case (24)
        C255_arom="???"
      case (25)
        C255_arom="???"
      case (26)
        C255_arom="???"
      case (27)
        C255_arom="???"
      case (28)
        C255_arom="???"
      case (29)
        C255_arom="???"
      case (30)
        C255_arom="???"
      end select
C control_th.fgi( 306, 197):MarkVI text out 30 variants,mo_text6
      I_(47) = I_uval + (-I_ixal)
C control_tg.fgi(  32,  47):��������
C label 1467  try1467=try1467-1
      I_(46)=abs(I_(47))
C control_tg.fgi(  39,  47):���������� ��������
      L_(25)=I_(46).lt.I0_imak
C control_tg.fgi(  49,  47):���������� �������������
      if(L_ipal) then
         I0_opal=I_adol
      endif
      I_aral=I0_opal
C control_tg.fgi( 126,  57):�������-��������
      I_(45) = I_adol + (-I_aral)
C control_tg.fgi(  32,  39):��������
      I_(44)=abs(I_(45))
C control_tg.fgi(  39,  39):���������� ��������
      L_(24)=I_(44).lt.I0_emak
C control_tg.fgi(  49,  39):���������� �������������
      L_(26) = L_(25).AND.L_(24).AND.L_efok
C control_tg.fgi(  66,  45):�
      L_(21)=L_(26).and..not.L0_amak
      L0_amak=L_(26)
C control_tg.fgi(  86,  45):������������  �� 1 ���
      L_ipal=(L_(21).or.L_ipal).and..not.(L_(22))
      L_(23)=.not.L_ipal
C control_tg.fgi(  95,  43):RS �������
      L_ival=L_ipal
C control_tg.fgi( 155,  45):������,capt_take
      if(L_ival) then
         I0_eval=I_ixal
      endif
      I_(53)=I0_eval
C control_tg.fgi(  68,  81):�������-��������
      R_(11)=I_(53)
C control_tg.fgi(  73,  82):��������� IN->RE
      R_(10) = (-R_(11)) + R_(7)
C control_tg.fgi(  36,  75):��������
      if(L_ival) then
         R0_aval=R_(11)
      elseif(L_(43)) then
         R0_aval=R0_aval
      else
         R0_aval=R0_aval+deltat/R_(8)*R_(10)
      endif
      if(R0_aval.gt.R_(11)) then
         R0_aval=R_(11)
      elseif(R0_aval.lt.R_(9)) then
         R0_aval=R_(9)
      endif
C control_tg.fgi(  46,  75):����������
      I_(54)=R0_aval
C control_tg.fgi(  56,  75):��������� RE->IN
      if(L_ival) then
         I_uval=I_ixal
      else
         I_uval=I_(54)
      endif
C control_tg.fgi(  67,  73):���� RE IN LO CH7
C sav1=I_(47)
      I_(47) = I_uval + (-I_ixal)
C control_tg.fgi(  32,  47):recalc:��������
C if(sav1.ne.I_(47) .and. try1467.gt.0) goto 1467
      I_oval=I_uval
C control_tg.fgi( 155,  80):������,bun_y_coord
      L0_arok=.false.
      I0_upok=0

      if(L0_ekap)then
        I0_idil=1
         L0_ipok=.false.
         L0_afok=.false.
         L0_otik=.false.
         L0_atik=.false.
         L0_osik=.false.
         L0_esik=.false.
         L0_urik=.false.
         L0_irik=.false.
         L0_arik=.false.
         L0_apok=.false.
         L0_omok=.false.
         L0_emok=.false.
         L0_ulok=.false.
         L0_ilok=.false.
         L0_alok=.false.
         L0_okok=.false.
         L0_ekok=.false.
         L0_ufok=.false.
         L0_ifok=.false.
         L0_odok=.false.
         L0_edok=.false.
         L0_ubok=.false.
         L0_ibok=.false.
         L0_abok=.false.
         L0_oxik=.false.
         L0_exik=.false.
         L0_uvik=.false.
         L0_ivik=.false.
         L0_avik=.false.
         L0_etik=.false.
      endif

      if(L0_ipok)I0_upok=I0_upok+1
      if(L0_afok)I0_upok=I0_upok+1
      if(L0_otik)I0_upok=I0_upok+1
      if(L0_atik)I0_upok=I0_upok+1
      if(L0_osik)I0_upok=I0_upok+1
      if(L0_esik)I0_upok=I0_upok+1
      if(L0_urik)I0_upok=I0_upok+1
      if(L0_irik)I0_upok=I0_upok+1
      if(L0_arik)I0_upok=I0_upok+1
      if(L0_apok)I0_upok=I0_upok+1
      if(L0_omok)I0_upok=I0_upok+1
      if(L0_emok)I0_upok=I0_upok+1
      if(L0_ulok)I0_upok=I0_upok+1
      if(L0_ilok)I0_upok=I0_upok+1
      if(L0_alok)I0_upok=I0_upok+1
      if(L0_okok)I0_upok=I0_upok+1
      if(L0_ekok)I0_upok=I0_upok+1
      if(L0_ufok)I0_upok=I0_upok+1
      if(L0_ifok)I0_upok=I0_upok+1
      if(L0_odok)I0_upok=I0_upok+1
      if(L0_edok)I0_upok=I0_upok+1
      if(L0_ubok)I0_upok=I0_upok+1
      if(L0_ibok)I0_upok=I0_upok+1
      if(L0_abok)I0_upok=I0_upok+1
      if(L0_oxik)I0_upok=I0_upok+1
      if(L0_exik)I0_upok=I0_upok+1
      if(L0_uvik)I0_upok=I0_upok+1
      if(L0_ivik)I0_upok=I0_upok+1
      if(L0_avik)I0_upok=I0_upok+1
      if(L0_etik)I0_upok=I0_upok+1


      if(.not.L0_arok.and.L0_opok.and..not.L0_ipok)then
        I0_idil=1
        L0_arok=.true.
      endif

      if(.not.L0_arok.and.L_efok.and..not.L0_afok)then
        I0_idil=2
        L0_arok=.true.
      endif

      if(.not.L0_arok.and.L_utik.and..not.L0_otik)then
        I0_idil=3
        L0_arok=.true.
      endif

      if(.not.L0_arok.and.L_ival.and..not.L0_atik)then
        I0_idil=4
        L0_arok=.true.
      endif

      if(.not.L0_arok.and.L0_usik.and..not.L0_osik)then
        I0_idil=5
        L0_arok=.true.
      endif

      if(.not.L0_arok.and.L0_isik.and..not.L0_esik)then
        I0_idil=6
        L0_arok=.true.
      endif

      if(.not.L0_arok.and.L0_asik.and..not.L0_urik)then
        I0_idil=7
        L0_arok=.true.
      endif

      if(.not.L0_arok.and.L0_orik.and..not.L0_irik)then
        I0_idil=8
        L0_arok=.true.
      endif

      if(.not.L0_arok.and.L0_erik.and..not.L0_arik)then
        I0_idil=9
        L0_arok=.true.
      endif

      if(.not.L0_arok.and.L0_epok.and..not.L0_apok)then
        I0_idil=10
        L0_arok=.true.
      endif

      if(.not.L0_arok.and.L0_umok.and..not.L0_omok)then
        I0_idil=11
        L0_arok=.true.
      endif

      if(.not.L0_arok.and.L0_imok.and..not.L0_emok)then
        I0_idil=12
        L0_arok=.true.
      endif

      if(.not.L0_arok.and.L0_amok.and..not.L0_ulok)then
        I0_idil=13
        L0_arok=.true.
      endif

      if(.not.L0_arok.and.L0_olok.and..not.L0_ilok)then
        I0_idil=14
        L0_arok=.true.
      endif

      if(.not.L0_arok.and.L0_elok.and..not.L0_alok)then
        I0_idil=15
        L0_arok=.true.
      endif

      if(.not.L0_arok.and.L0_ukok.and..not.L0_okok)then
        I0_idil=16
        L0_arok=.true.
      endif

      if(.not.L0_arok.and.L0_ikok.and..not.L0_ekok)then
        I0_idil=17
        L0_arok=.true.
      endif

      if(.not.L0_arok.and.L0_akok.and..not.L0_ufok)then
        I0_idil=18
        L0_arok=.true.
      endif

      if(.not.L0_arok.and.L0_ofok.and..not.L0_ifok)then
        I0_idil=19
        L0_arok=.true.
      endif

      if(.not.L0_arok.and.L0_udok.and..not.L0_odok)then
        I0_idil=20
        L0_arok=.true.
      endif

      if(.not.L0_arok.and.L0_idok.and..not.L0_edok)then
        I0_idil=21
        L0_arok=.true.
      endif

      if(.not.L0_arok.and.L0_adok.and..not.L0_ubok)then
        I0_idil=22
        L0_arok=.true.
      endif

      if(.not.L0_arok.and.L0_obok.and..not.L0_ibok)then
        I0_idil=23
        L0_arok=.true.
      endif

      if(.not.L0_arok.and.L0_ebok.and..not.L0_abok)then
        I0_idil=24
        L0_arok=.true.
      endif

      if(.not.L0_arok.and.L0_uxik.and..not.L0_oxik)then
        I0_idil=25
        L0_arok=.true.
      endif

      if(.not.L0_arok.and.L0_ixik.and..not.L0_exik)then
        I0_idil=26
        L0_arok=.true.
      endif

      if(.not.L0_arok.and.L0_axik.and..not.L0_uvik)then
        I0_idil=27
        L0_arok=.true.
      endif

      if(.not.L0_arok.and.L0_ovik.and..not.L0_ivik)then
        I0_idil=28
        L0_arok=.true.
      endif

      if(.not.L0_arok.and.L0_evik.and..not.L0_avik)then
        I0_idil=29
        L0_arok=.true.
      endif

      if(.not.L0_arok.and.L0_itik.and..not.L0_etik)then
        I0_idil=30
        L0_arok=.true.
      endif

      if(I0_upok.eq.1)then
         L0_ipok=.false.
         L0_afok=.false.
         L0_otik=.false.
         L0_atik=.false.
         L0_osik=.false.
         L0_esik=.false.
         L0_urik=.false.
         L0_irik=.false.
         L0_arik=.false.
         L0_apok=.false.
         L0_omok=.false.
         L0_emok=.false.
         L0_ulok=.false.
         L0_ilok=.false.
         L0_alok=.false.
         L0_okok=.false.
         L0_ekok=.false.
         L0_ufok=.false.
         L0_ifok=.false.
         L0_odok=.false.
         L0_edok=.false.
         L0_ubok=.false.
         L0_ibok=.false.
         L0_abok=.false.
         L0_oxik=.false.
         L0_exik=.false.
         L0_uvik=.false.
         L0_ivik=.false.
         L0_avik=.false.
         L0_etik=.false.
      else
         L0_ipok=L0_opok
         L0_afok=L_efok
         L0_otik=L_utik
         L0_atik=L_ival
         L0_osik=L0_usik
         L0_esik=L0_isik
         L0_urik=L0_asik
         L0_irik=L0_orik
         L0_arik=L0_erik
         L0_apok=L0_epok
         L0_omok=L0_umok
         L0_emok=L0_imok
         L0_ulok=L0_amok
         L0_ilok=L0_olok
         L0_alok=L0_elok
         L0_okok=L0_ukok
         L0_ekok=L0_ikok
         L0_ufok=L0_akok
         L0_ifok=L0_ofok
         L0_odok=L0_udok
         L0_edok=L0_idok
         L0_ubok=L0_adok
         L0_ibok=L0_obok
         L0_abok=L0_ebok
         L0_oxik=L0_uxik
         L0_exik=L0_ixik
         L0_uvik=L0_axik
         L0_ivik=L0_ovik
         L0_avik=L0_evik
         L0_etik=L0_itik
      endif
C control_tg.fgi( 371, 196):��������� �������
      select case (I0_idil)
      case (1)
        C255_orel="��������� �������� ������������"
      case (2)
        C255_orel="������� ������"
      case (3)
        C255_orel="������� �������"
      case (4)
        C255_orel="������� �����"
      case (5)
        C255_orel="???"
      case (6)
        C255_orel="???"
      case (7)
        C255_orel="???"
      case (8)
        C255_orel="???"
      case (9)
        C255_orel="???"
      case (10)
        C255_orel="???"
      case (11)
        C255_orel="???"
      case (12)
        C255_orel="???"
      case (13)
        C255_orel="???"
      case (14)
        C255_orel="???"
      case (15)
        C255_orel="???"
      case (16)
        C255_orel="???"
      case (17)
        C255_orel="???"
      case (18)
        C255_orel="???"
      case (19)
        C255_orel="???"
      case (20)
        C255_orel="???"
      case (21)
        C255_orel="???"
      case (22)
        C255_orel="???"
      case (23)
        C255_orel="???"
      case (24)
        C255_orel="???"
      case (25)
        C255_orel="???"
      case (26)
        C255_orel="???"
      case (27)
        C255_orel="???"
      case (28)
        C255_orel="???"
      case (29)
        C255_orel="???"
      case (30)
        C255_orel="???"
      end select
C control_tg.fgi( 386, 225):MarkVI text out 30 variants,tg_text2
      if(L_ival) then
         I_iral=1
      else
         I_iral=0
      endif
C control_tg.fgi(  73,  66):��������� LO->1
      I_eral=I_iral
C control_tg.fgi( 155,  70):������,capt1
      if(L_ipal) then
         I_adum=I_(42)
      else
         I_adum=I_(43)
      endif
C control_tg.fgi( 125,  38):���� RE IN LO CH7
      select case (I_adum)
      case (1)
        C255_irom="������� ��������� �� ���."
      case (2)
        C255_irom="�������� ����"
      case (3)
        C255_irom="��������� ���"
      case (4)
        C255_irom="???"
      case (5)
        C255_irom="???"
      case (6)
        C255_irom="???"
      case (7)
        C255_irom="???"
      case (8)
        C255_irom="???"
      case (9)
        C255_irom="???"
      case (10)
        C255_irom="???"
      case (11)
        C255_irom="???"
      case (12)
        C255_irom="???"
      case (13)
        C255_irom="???"
      case (14)
        C255_irom="???"
      case (15)
        C255_irom="???"
      case (16)
        C255_irom="???"
      case (17)
        C255_irom="???"
      case (18)
        C255_irom="???"
      case (19)
        C255_irom="???"
      case (20)
        C255_irom="???"
      case (21)
        C255_irom="???"
      case (22)
        C255_irom="???"
      case (23)
        C255_irom="???"
      case (24)
        C255_irom="???"
      case (25)
        C255_irom="???"
      case (26)
        C255_irom="???"
      case (27)
        C255_irom="???"
      case (28)
        C255_irom="???"
      case (29)
        C255_irom="???"
      case (30)
        C255_irom="???"
      end select
C control_th.fgi( 386, 155):MarkVI text out 30 variants,mo_text3
      if(L_(24)) then
         I_olak=I_(41)
      else
         I_olak=I_(38)
      endif
C control_tg.fgi( 125,  29):���� RE IN LO CH7
      I_upal=I_aral
C control_tg.fgi( 155,  62):������,bun_x_coord
      End
