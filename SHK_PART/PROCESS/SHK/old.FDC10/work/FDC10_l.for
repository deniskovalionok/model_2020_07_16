      Subroutine FDC10_l(ext_deltat)
      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      Include 'FDC10_l.fh'

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      R_(2) = 1.0
C FDC_PSU_L.fgi( 196, 184):��������� (RE4) (�������)
      !��������� R_(4) = FDC_PSU_LC?? /0.6/
      R_(4)=R0_i
C FDC_PSU_L.fgi( 193, 192):���������
      R_(5) = R_(4) * R8_if
C FDC_PSU_L.fgi( 210, 191):����������
      R_(3) = (-R_(4)) + R_(2)
C FDC_PSU_L.fgi( 199, 186):��������
      R_(1) = R_(3) * R8_ef
C FDC_PSU_L.fgi( 210, 185):����������
      R8_e = R_(5) + R_(1)
C FDC_PSU_L.fgi( 222, 190):��������
      R_(7) = 1.0
C FDC_PSU_L.fgi( 196, 196):��������� (RE4) (�������)
      !��������� R_(9) = FDC_PSU_LC?? /0.7/
      R_(9)=R0_u
C FDC_PSU_L.fgi( 193, 204):���������
      R_(10) = R_(9) * R8_if
C FDC_PSU_L.fgi( 210, 203):����������
      R_(8) = (-R_(9)) + R_(7)
C FDC_PSU_L.fgi( 199, 198):��������
      R_(6) = R_(8) * R8_ef
C FDC_PSU_L.fgi( 210, 197):����������
      R8_o = R_(10) + R_(6)
C FDC_PSU_L.fgi( 222, 202):��������
      R_(12) = 1.0
C FDC_PSU_L.fgi( 196, 208):��������� (RE4) (�������)
      !��������� R_(14) = FDC_PSU_LC?? /0.8/
      R_(14)=R0_ed
C FDC_PSU_L.fgi( 193, 216):���������
      R_(15) = R_(14) * R8_if
C FDC_PSU_L.fgi( 210, 215):����������
      R_(13) = (-R_(14)) + R_(12)
C FDC_PSU_L.fgi( 199, 210):��������
      R_(11) = R_(13) * R8_ef
C FDC_PSU_L.fgi( 210, 209):����������
      R8_ad = R_(15) + R_(11)
C FDC_PSU_L.fgi( 222, 214):��������
      R_(17) = 1.0
C FDC_PSU_L.fgi( 196, 220):��������� (RE4) (�������)
      !��������� R_(19) = FDC_PSU_LC?? /1.0/
      R_(19)=R0_od
C FDC_PSU_L.fgi( 193, 228):���������
      R_(20) = R_(19) * R8_if
C FDC_PSU_L.fgi( 210, 227):����������
      R_(18) = (-R_(19)) + R_(17)
C FDC_PSU_L.fgi( 199, 222):��������
      R_(16) = R_(18) * R8_ef
C FDC_PSU_L.fgi( 210, 221):����������
      R8_id = R_(20) + R_(16)
C FDC_PSU_L.fgi( 222, 226):��������
      R_(22) = 1.0
C FDC_PSU_L.fgi( 196, 232):��������� (RE4) (�������)
      !��������� R_(24) = FDC_PSU_LC?? /0.5/
      R_(24)=R0_af
C FDC_PSU_L.fgi( 193, 240):���������
      R_(25) = R_(24) * R8_if
C FDC_PSU_L.fgi( 210, 239):����������
      R_(23) = (-R_(24)) + R_(22)
C FDC_PSU_L.fgi( 199, 234):��������
      R_(21) = R_(23) * R8_ef
C FDC_PSU_L.fgi( 210, 233):����������
      R8_ud = R_(25) + R_(21)
C FDC_PSU_L.fgi( 222, 238):��������
      call lin_ext(REAL(R8_ek,4),I2_ak,R_(26),R_uf,R_of)
      R8_ok=R_(26)
C FDC_PSU_L.fgi(  65, 131):�������-�������� ������� (20)
      R8_ik=R8_ok
C FDC_PSU_L.fgi(  84, 137):������,Pvssh_P
      call lin_ext(REAL(R8_ep,4),I2_el,R_(27),R_al,R_uk)
      R0_am=R_(27)
C FDC_PSU_L.fgi(  67,  86):�������-�������� ������� (20)
      call lin_ext(REAL(R0_am,4),I2_ul,R_(28),R_ol,R_il)
      R0_im=R_(28)
C FDC_PSU_L.fgi(  87,  86):�������-�������� ������� (20)
      R_(29) = R0_im * R8_em
C FDC_PSU_L.fgi( 114,  91):����������
      !��������� R_(30) = FDC_PSU_LC?? /0.0/
      R_(30)=R0_om
C FDC_PSU_L.fgi( 129,  93):���������
      if(L_um) then
         R8_ap=R_(29)
      else
         R8_ap=R_(30)
      endif
C FDC_PSU_L.fgi( 132,  91):���� RE IN LO CH7
      call lin_ext(REAL(R8_ar,4),I2_up,R_(31),R_op,R_ip)
      R8_ir=R_(31)
C FDC_PSU_L.fgi(  54, 235):�������-�������� ������� (20)
      R8_er=R8_ir
C FDC_PSU_L.fgi(  74, 241):������,PT1_P
      call lin_ext(REAL(R8_ov,4),I2_as,R_(32),R_ur,R_or)
      R0_us=R_(32)
C FDC_PSU_L.fgi(  53, 200):�������-�������� ������� (20)
      call lin_ext(REAL(R0_us,4),I2_os,R_(33),R_is,R_es)
      R0_et=R_(33)
C FDC_PSU_L.fgi(  73, 200):�������-�������� ������� (20)
      R_(35) = R0_et * R8_at
C FDC_PSU_L.fgi( 100, 205):����������
      !��������� R_(34) = FDC_PSU_LC?? /0.0/
      R_(34)=R0_it
C FDC_PSU_L.fgi( 115, 194):���������
      if(L_ot) then
         R8_ut=R_(35)
      else
         R8_ut=R_(34)
      endif
C FDC_PSU_L.fgi( 118, 192):���� RE IN LO CH7
      !��������� R_(36) = FDC_PSU_LC?? /0.0/
      R_(36)=R0_av
C FDC_PSU_L.fgi( 115, 207):���������
      if(L_ev) then
         R8_iv=R_(35)
      else
         R8_iv=R_(36)
      endif
C FDC_PSU_L.fgi( 118, 205):���� RE IN LO CH7
      End
