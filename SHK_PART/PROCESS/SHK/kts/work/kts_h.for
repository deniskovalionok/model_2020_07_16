      Subroutine kts_hLinkData
      Call kts_hData
      End

****** Begin file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET41.fs'
c   ������ ��������, �������� � ��������� � ����                     
c   �� ���������, ������������� �.�.��������� 19.01.99

      subroutine kts_h(deltat)
c
      IMPLICIT NONE
  
      include 'kts_h.fh'
      REAL*8 FlwTnSqFrom(894)
      COMMON/kts_h_nowrt/ FlwTnSqFrom
      REAL*8 FlwHTnFromUp(894)
      COMMON/kts_h_nowrt/ FlwHTnFromUp
      REAL*8 FlwTnSqTo(894)
      COMMON/kts_h_nowrt/ FlwTnSqTo
      REAL*8 FlwHTnFromDo(894)
      COMMON/kts_h_nowrt/ FlwHTnFromDo
      REAL*8 FlwHTnToUp(894)
      COMMON/kts_h_nowrt/ FlwHTnToUp
      REAL*8 FlwHTnToDo(894)
      COMMON/kts_h_nowrt/ FlwHTnToDo
      INTEGER*4 Fan2NTN2N
      PARAMETER (Fan2NTN2N=0)
      INTEGER*4 Fan2NTN0N
      PARAMETER (Fan2NTN0N=0)
      INTEGER*4 Fan2NTauNN
      PARAMETER (Fan2NTauNN=0)
      INTEGER*4 Fan2NdPG5N
      PARAMETER (Fan2NdPG5N=0)
      INTEGER*4 Fan2NdPG4N
      PARAMETER (Fan2NdPG4N=0)
      INTEGER*4 Fan2NdPG3N
      PARAMETER (Fan2NdPG3N=0)
      INTEGER*4 Fan2NdPG2N
      PARAMETER (Fan2NdPG2N=0)
      INTEGER*4 Fan2NdPG0N
      PARAMETER (Fan2NdPG0N=0)
      INTEGER*4 Fan2TauNN
      PARAMETER (Fan2TauNN=0)
      INTEGER*4 FanDim_TauNmax
      PARAMETER (FanDim_TauNmax=0)
      INTEGER*4 FanDim_Taumax
      PARAMETER (FanDim_Taumax=0)
      INTEGER*4 FanDim_KMax
      PARAMETER (FanDim_KMax=0)
      INTEGER*4 FanDim_PMax
      PARAMETER (FanDim_PMax=0)
      INTEGER*4 Tn8Ne
      PARAMETER (Tn8Ne=2)
      INTEGER*4 Tn3Np
      PARAMETER (Tn3Np=2)
      INTEGER*4 Tn3Ne
      PARAMETER (Tn3Ne=4)
      INTEGER*4 INVPERe(551)
      COMMON/kts_h_nowrt/ INVPERe
      INTEGER*4 NZSUBe(4335)
      COMMON/kts_h_nowrt/ NZSUBe
      INTEGER*4 INZSUBe(552)
      COMMON/kts_h_nowrt/ INZSUBe
      INTEGER*4 ILNZe(552)
      COMMON/kts_h_nowrt/ ILNZe
      INTEGER*4 INVPERp(551)
      COMMON/kts_h_nowrt/ INVPERp
      INTEGER*4 NZSUBp(4335)
      COMMON/kts_h_nowrt/ NZSUBp
      INTEGER*4 INZSUBp(552)
      COMMON/kts_h_nowrt/ INZSUBp
      INTEGER*4 ILNZp(552)
      COMMON/kts_h_nowrt/ ILNZp
      INTEGER*4 Nthermo
      PARAMETER (Nthermo=894)
      INTEGER*4 Nhydro
      PARAMETER (Nhydro=894)
      INTEGER*4 Nent
      PARAMETER (Nent=551)
      INTEGER*4 Npc
      PARAMETER (Npc=1102)
      INTEGER*4 Npres
      PARAMETER (Npres=551)
      INTEGER*4 Nf
      PARAMETER (Nf=6)
      INTEGER*4 Tn7Ne
      PARAMETER (Tn7Ne=1)
      INTEGER*4 Tn7Np
      PARAMETER (Tn7Np=1)
      INTEGER*4 Tn9Ne
      PARAMETER (Tn9Ne=6)
      INTEGER*4 Tn9Np
      PARAMETER (Tn9Np=2)
      INTEGER*4 Fan2NTN3N
      PARAMETER (Fan2NTN3N=0)
      INTEGER*4 Fan2NTN4N
      PARAMETER (Fan2NTN4N=0)
      INTEGER*4 Fan2NTN5N
      PARAMETER (Fan2NTN5N=0)
      INTEGER*4 Tr1Dim_Nu
      PARAMETER (Tr1Dim_Nu=-111)
      INTEGER*4 Tr2Dim_Ksi
      PARAMETER (Tr2Dim_Ksi=-111)
      INTEGER*4 Tn8Np
      PARAMETER (Tn8Np=1)
      LOGICAL*1 PIntIter(551)
      COMMON/kts_h_nowrt/ PIntIter
      LOGICAL*1 EIntIter(551)
      COMMON/kts_h_nowrt/ EIntIter
      LOGICAL*1 EextIter(551)
      COMMON/kts_h_nowrt/ EextIter
      LOGICAL*1 PExtIter(551)
      COMMON/kts_h_nowrt/ PExtIter
      !DEC$PSECT/kts_h_NOWRT/ NoWRT
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 13 

****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
      integer*4 Ncomp,CompLin(1),CompOin(1),CompGenTyp(1)
      parameter(Ncomp=0)
      real*8 CompQq(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 22 
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 27 


****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'

c      real*8 FlwCondRegul(nv)
c      common/kts_hFlwL/ FlwCondRegul
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 38 

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 73 
      integer*4 nfwv
      parameter(nfwv=0)
      integer*4 FwvFloWw(1),FwvFloWv(1)
      real*8 FwvRoFrom1(1),FwvRoFrom2(1),FwvRoTo1(1),FwvRoTo2(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 79 
      integer*4 nfwv1
      parameter(nfwv1=0)
      integer*4 Fwv1Flow(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 84 
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
      integer NHe2
      parameter(NHe2=0)
* ���������� ��� ��������������� ��������� ����������
      real*8 C0He2(1) !����. ��� ������� ����������� ���� ��
      real*8 C1He2(1) !����. ��� ������� ����������� ���� ��
      real*8 C2He2(1) !����. ��� ������� ����������� ���� ��
      real*8 DTHe2
      real*8 C0He2b1(1),C1He2b1(1),C2He2b1(1)
      real*8 C0He2b2(1),C1He2b2(1),C2He2b2(1)
      real*8 He2T1t(2,1),He2T2t(2,1),He2decQt(1),He2_gar2(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 100 
C     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 114 
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'

      real*8 Mtl_QqOut(NMtl)  !��������� �������� ����� � ������� ��������
      real*8 MtlTt_t(NMtl)
      real*8 MtlAlCulc(nMtl)
      common/kts_hMetal/ Mtl_QqOut,MtlTt_t,MtlAlCulc
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 125 

C     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 132 

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 140 

      integer*4 NMtlO,MtlOIndRoom(1)
      parameter ( NMtlO=0 )
      real*8 MtlOTt(1),MtlOMass(1),MtlOCp(1),MtlOAlCulcWl1(1)
      real*8 MtlO_QqOut(1)  !��������� �������� ����� � ������� ��������
      real*8 MtlOLamdC(1),MtlOLamd(1),MtlOQqEnv(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 148 
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'

      integer*4 NUCH
      parameter (NUCH=0)
      integer*4 CoalHConNodInd(1)
      real*8 CoalHNodMoistG(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 157 

      integer*4 NFlwWasteSet
      parameter(NFlwWasteSet=0)
      integer*4 FlwWasteSetConnInd(1),FlwWasteSetNum(1)
      real*8 FlwWasteSetWaste(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 164 

      integer*4 FlwConnFlwInd(1),NFlwConn
      parameter(NFlwConn=0)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 169 

C     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 174 

      integer*4 NHCO
      parameter(NHCO=0)
      integer*4  HCOFlMod(1),HCOIndFlw(1)
      real*8 HCOCfMod(1),HCOOFg(1),HCOONodVol(1),HCOONoddRodP(1)
      common/kts_hConFlOut/ HCOFlMod,HCOCfMod,HCOOFg,HCOONodVol,
     >HCOONoddRodP,HCOIndFlw
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 182 

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 189 

      integer*4 nCrv
      parameter(nCrv=0)
      real*8 CrvRes(1)
      integer*4 CrvFlw(nCrv)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 196 

      integer*4 nWst
      parameter(nWst=0)
      integer*4 WstNode(1)
      real*8    WSTConsN2(1),WSTConsO2(1),WSTConsH2O(1),
     *          WSTConsCO2(1),WSTConsSoot(1),WSTConsAsh(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 204 

      integer*4 NXS
      parameter(NXS=0)
      integer*4 XSFlagSetCc(1),XSFlSet(30,1),XSIndSetCc(1),
     >XSIndSetCc0(1)
      real*8    XSCcstr(30,1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 211 

      integer*4 ntFrB
      parameter(ntFrB=0)
      integer*4 tFrBNdInd(1),tFrBi(1)
      real*8    tFrBCcOld(100,1),tFrBTau(1)
      real*8    tFrBGIn(1),tFrBAa(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 219 

      integer*4 np4
      parameter(np4=0)
      integer*4 Pm4Nb(1)
      real*8 Pm4Res(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 226 

c ��������� �����. ��� ������� � ���������������, ���� �� (�������) ���
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 234 
      integer*4 np2
      parameter(np2=0)
      integer*4 Pm2Nb(1)
      real*8 Pm2Res(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 240 
      integer*4 nFan
      parameter(nFan=0)
      integer*4 FanFlw(1)
      real*8 FanRes(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 246 
      integer*4 nFan2
      parameter(nFan2=0)
      integer*4 Fan2Flw(1)
      real*8 Fan2Res(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 252 
      integer*4 np3
      parameter(np3=0)
      real*8 Pm3QqNod(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 257 
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 267 

      real*8 time_df1  !������� ��� ������� ������� (� �����)
      real*8 time_tmp

      real   deltat      !��� ����������
      real   deltat1      !��� ����������
      REAL*8 Atau        !1/dt
c      save Atau
      real*4 dtInt       !dt*Accel
      real*8 TimeSrv     !dt/Niter
      character*100 fn1  ! ��� ������������� .cbf ������
c      common/kts_hLocal1/ time_tmp,deltat,Atau,dtInt,TimeSrv,fn1
      common/kts_hLocal1/ Atau,time_tmp,deltat1,dtInt,TimeSrv,fn1
      real*8 VOL1(nu) !V/dt
      real*8 NodCfdE(nu) !����. � ��. ����. ��� ����� ���������
      real*8 NodTtk(nu)  !����������� ��������. � ������� ����� �� k-�� ��������
      real*8 RotVol(nu) !��������� ������
      real*8 RokVol(nu) !��������� ������
      real*8 NodVolk(nu)  !������������ ����� �� k-�� ��������
      real*8 NoddVdPk(nu) !����������� ������ � ���� �� �������� �� k-�� ����.

c ---  ������ �. (29.08.02) - ������
      real*8 NodSum1(nu)
      real*8 NodSum2(nu)
c ---  ������ �. (29.08.02) - �����

      real*8 NodQqPmAd(nu)

      common/kts_hNODE1/ VOL1,NodCfdE,NodTtk,RotVol,RokVol,
     * NodVolk,NoddVdPk,NodSum1,NodSum2,NodQqPmAd
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 299 

      real*8 C7(nv)   !��������� ������������
      real*8 Gk(nv)   !������� �� k-�� ��������
      real*8 dHLev(nv)!������ �� ����� �� ���� ������� � �������
      real*8 Rovk(nv)    !��. ��������� �� k-�� ��������
      real*8 AconRovk(nv)!��������� ��� ��������
c###### ��������� ####################################
      real*8 FlwGaVeSq(nv)  ! ��������� ������ ��� �������� ������������� �����
      real*8 flwbcon(nv)    ! �������� ��������� �������� �������� �� ����� 
      real*8 FlwC7work1(nv)   ! ��������� ������ ��� �������� ������������� �����
      real*8 FlwC7work2(nv)   ! ��������� ������ ��� �������� ������������� �����
      real*8 FlwC7work3(nv)   ! ��������� ������ ��� �������� ������������� �����
      real*8 FlwC7work4(nv)   ! ��������� ������ ��� �������� ������������� �����
      real*8 FlwC7work5(nv)   ! ��������� ������ ��� �������� ������������� �����
      real*8 FlwC7work7(nv)
      real*8 FlwVsk    (nv)   ! �������� �� ����� �� ���������
      real*8 ComEeFrom  (nv)   ! ��������� ��������� ��������� � ��������
      real*8 ComEeTo    (nv)   ! ��������� ��������� ��������� � ��������  
      real*8 FlwSlGaFrom(nv)   ! �������� ��� ��������������� ���� �� �����
      real*8 FlwSlGaFrom0(nv)  ! �������� ��� ��������������� ���� �� �����
      real*8 FlwSlGaTo   (nv)  ! �������� ��� ��������������� ���� �� �����
      real*8 FlwSlGaTo0  (nv)  ! �������� ��� ��������������� ���� �� �����
      real*8 FlwCcFrom(nv) ! ��������� ������ ��� ������������ ���� �� ������ �����
      real*8 FlwCcTo(nv)   ! ��������� ������ ��� ������������ ���� �� ������ �����
      real*8 flwdhz0(nv)
      real*8 ComRoFrom(3,nv)
      real*8 ComRoTo  (3,nv)
c###### ��������� ####################################
      real*8 FlwKgp(nf,2,nv)    ! 
      real*8 FlwKge(nf,nf,nv)
      real*8 FlwKgg(3,nv)
      real*8 FlwKgpt(2,nv)
      real*8 FlwdPLevFrom(nv),FlwdPLevTo(nv)
      real*8 FlwCcGgWtJa(nv)
      real*8 FlwRes_Iter(nv)
      real*8 FlwGG_TMP(nv)
      real*8 FlwRes_Tmp
      real*8 FlwdCgDCcFrom(nv),FlwdCgDCcTo(nv)
c      real*8 frco0(nv)
      common/kts_hFLOW/ C7,Gk,dHLev,Rovk,AconRovk,FlwGaVeSq,
     * flwbcon,FlwC7work1,
     * FlwC7work2,FlwC7work3,FlwC7work4,FlwC7work5,FlwC7work7,
     * FlwVsk,ComEeFrom,ComEeTo,FlwSlGaFrom,FlwSlGaFrom0,
     * FlwSlGaTo,FlwSlGaTo0,FlwCcFrom,FlwCcTo,flwdhz0,ComRoFrom,
     * ComRoTo,FlwKgp,FlwKge,FlwKgg,FlwKgpt,
     * FlwdPLevFrom,FlwdPLevTo,FlwCcGgWtJa,
     * FlwRes_Iter,FlwRes_Tmp,FlwGG_TMP,FlwdCgDCcFrom,FlwdCgDCcTo
c    * ,frco0
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 349 

C     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 354 

C     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 359 


c      !������� ����. ����� ����� � ���. ��������� ��� ��������
       real*8 Dmp(Npres,Npres)
C       real*8 Dmpc(Npc,Npc) ! �������� 
c      !������� ����. ����� ����� � ������� ��������� ��� ���������
       real*8 Dmh(Nent,Nent)
       real*8 DmhG(Nent,Nent)
c      !������ ����� ��� ���������� ������� �� ����.
      Real*8 Yp(Npres)
c      Real*8 Ypc(Npc)
c      !������ ����� ��� ���������� ������� �� ���.
      real*8 Ye(Nent)
      real*8 YeG(Nent)
c      !������� �� ���������
      real*8 dP(Npres)
c      real*8 dPC(Npc)
c      !������� �� ����������
      real*8 dE(Nent)
      real*8 dEG(Nent)
c      !������ ������ �������� �� k-�� ��������
      real*8 Pk(Npres)
c      !������ ������ �������� �� (k+1)-�� ��������
      real*8 Pk1(Npres)
c      !������ ������ ��������� �� k-�� ��������
      real*8 Ek(Nent)
      real*8 EGk(Nent)
c      !������ ������ ��������� �� (k+1)-�� ��������
      real*8 Ek1(Nent)
      real*8 EGk1(Nent)
      real*8 Rok(Nu) !�����. �\� ����� � ������� ����� �� k-�� ����.
      !������ ������ ��������� �����. ����
      real*8 EeWtSatk(Nent)
      !������ ������ ��������� �����. ����
      real*8 EeVpSatk(Nent)
      real*8 MaxDevP     !����. �����. ����. �������� �� ��������
      real*8 MaxDevH     !����. �����. ����. ��������� �� ��������
      INTEGER*4 I,j     !���������� �����
      integer*4 IC        !������� ��������
      integer*4 ICInt !������� ���������� ��������
      common/kts_hLocal2/ Dmp,Dmh,Yp,Ye,dP,dE,Pk,Pk1,Ek,Ek1,Rok,
     *  EeWtSatk,EeVpSatk,MaxDevP,MaxDevH,IC,ICInt,DmhG,YeG,dEG,
     *  EGk,EGk1       !,dPC,Ypc,Dmpc <=������ ��������!!!!

C     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 407 

      INTEGER*4 nvlgpos !����� ����� � �������������� ���������� � ����������
      common/kts_hVlvGeo/ nvlgpos
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 412 

C     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 417 

C     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 422 

C     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 427 

C     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 432 

      integer*4 nsep1,Sep1FlwS(1),Sep1Node(1)
        parameter(nsep1=0)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 437 

      integer*4 nInWst,InWstNum(1),InWstNode(1),InWstNdTp(1)
      real*8    InWstdt(1)
      integer*4 InWstTyp(1)
      real*8    InWstCc(1)
      real*8 InWstExtCon(1),InWstExtWstdt(1)
      parameter(nInWst=0)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 446 

C     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 451 


      LOGICAL*1 go,goint  !������� ����������� ��������
      DATA go/.true./     !����������� �������� �������. ��������
      common/kts_hLocal4/ go,goint
      real*8  RDTSC
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 490 
      real*8 NoddRodPk(nu) !dRo/dP ��� ������� ����� �� ����. ����.
      real*8 NoddRodEk(nu) !dRo/d� ��� ������� ����� �� ����. ����.
      real*8 NoddRodPk1(nu) !dRo/dP ��� ������� ����� �� ���. ����.
      real*8 NoddRodEk1(nu) !dRo/d� ��� ������� ����� �� ���. ����.
      real*8 nodrorelax(nu)
      real*8 NodRoGa0(nu)
      real*8 NodCcGa_Aeq0(nu) 
      real*8 NodCcGa_Deq0(nu) 
      real*8 NodCcGa_Beq (nu)
      real*8 NodCcGa_Beq0(nu)
      real*8 NodCcGa_Aeq (nu)
      real*8 NodCcGa_Deq (nu)
      real*8 NodXxk1(nu)    !���. �������. � ������� ����� �� ���. ����.
      real*8 NodXxBalk1(nu) !���. �������. � ������� ����� �� ���. ����.
      real*8 NodQqAd(nu)
      real*8 NodCfEeAsh(nu)
      real*8 NodEeWvt(nu)
      real*8 NodAlSqGaWv(nu),NodYe(nu),NodDmh(nu)
      real*8 NodPsw(nu),NodRoWvTmp(nu),NoddRdHGas(nu)
     *,NoddRdPWvTmp(nu),NoddRdHWvTmp(nu)
     *,NodTtWvTmp(nu),NodCpWvTmp(nu),NodXxTmp(nu)
      integer*4 Nod_ghm_flag(nu),NodPropRelaxFlag(nu)
      logical*1 EExtIter_(Nent)
      logical*1 PExtIter_(Npres)
      common /kts_hNODE/ NoddRodPk,NoddRodEk,NoddRodPk1,NoddRodEk1,
     *  nodrorelax,NodRoGa0,NodCcGa_Aeq0,NodCcGa_Deq0,NodCcGa_Beq,
     *  NodCcGa_Beq0,NodCcGa_Aeq,NodCcGa_Deq,NodXxk1,NodXxBalk1,
     *  NodQqAd,NodCfEeAsh,NodEeWvt,NodAlSqGaWv,NodYe,NodDmh,
     *  NodPsw,NodRoWvTmp,NoddRdHGas,NodTtWvTmp,NodCpWvTmp,NodXxTmp
      common /kts_hNODE_I/ Nod_ghm_flag,NodPropRelaxFlag
      common /kts_hNODE_L/ EExtIter_,PExtIter_
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 523 
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'

      integer NTc0
      parameter(NTc0=0)
      integer*4 Tc0IbnFro(1),Tc0NodFro(1),Tc0IbnTo(1),Tc0NodTo(1)
      real*8 Tc0zt(1),Tc0AlG23(1),Tc0AlG43(1)
     *,Tc0G43(1),Tc0G23(1),Tc0G34(1),Tc0CpWl(1)
     *,Tc0TtEnv(1),Tc0TtWlk(1),Tc0TtWl(1),Tc0AlSqEnv(1),Tc0AlEnv(1)
     *,Tc0AlSqPhWl(1,nf),Tc0MasCpWl(1),Tc0CfAlEnv(1)
     *,Tc0CfAlGaWl(1),Tc0CfAlWpUpWl(1)
     *,Tc0CfAlWtWl(1),Tc0CfAlWpDoWl(1)
     *,Tc0EvUpWl(1),Tc0EvDoWl(1)
     *,Tc0dG23dp(1),Tc0dG34dp(1),Tc0dG43dp(1)
     *,Tc0CfGgWpUpWt(1),Tc0CcGgWtWpDo(1),Tc0CfGgWpDoWt(1)
     *,Tc0G32(1),Tc0dG32dp(1),Tc0CfGgWt(1),Tc0SqWl(1),Tc0TettaQ(1,nf)
     *,Tc0QqIsp(1),Tc0AlIsp(1)
     *,Tc0dQIspdP(1),Tc0TtTnIsp(1),Tc0AlSqIsp(1),TC0ResWl(1)
     *,Tc0dQG23dp(1),Tc0TettaQ2(1,nf),Tc0DeltaTs(1)
     *,Tc0AlG23From(1),Tc0AlG43From(1)
     *,Tc0G23From(1),Tc0G34From(1),Tc0G32From(1),Tc0G43From(1)
     *,Tc0dG23dpFrom(1),Tc0dG34dpFrom(1),Tc0dG43dpFrom(1),
     >Tc0dG32dpFrom(1)
     *,Tc0dQIspdPFrom(1),Tc0TtTnIspFrom(1),Tc0AlSqIspFrom(1)
     *,Tc0CcGgWtWpDoFrom(1),Tc0CfGgWpUpWtFrom(1),Tc0CfGgWpDoWtFrom(1)
     *,Tc0QqIspFrom(1),Tc0AlIspFrom(1),Tc0CfGgWtFrom(1),Tc0TettaQFrom(1,
     >nf)
     *,Tc0AlSqPhWlFrom(1,nf),Tc0CfRelAlIsp(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 553 
C     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 582 

****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'

      integer NTc2
      parameter(NTc2=0)
      integer*4 Tc2Ibn(1),Tc2Node(1)
      real*8 Tc2zt(1),Tc2QqEnv(1),Tc2AlG23(1),Tc2G23(1),Tc2G34(1)
     *,Tc2AlG43(1),Tc2G43(1),Tc2CpWl(1)
     *,Tc2TtEnv(1),Tc2TtWlk(1),Tc2TtWl(1),Tc2AlSqEnv(1),Tc2AlEnv(1)
     *,Tc2AlSqPhWl(1,nf),Tc2MasCpWl(1),Tc2CfAlEnv(1)
     *,Tc2CfAlGaWl(1),Tc2CfAlWpUpWl(1),Tc2CfAlWtWl(1),Tc2CfAlWpDoWl(1)
     *,Tc2EvUpWl(1),Tc2EvDoWl(1)
     *,Tc2dG23dp(1),Tc2dG34dp(1),Tc2dG43dp(1)
     *,Tc2CfGgWpUpWt(1),Tc2CcGgWtWpDo(1),Tc2CfGgWpDoWt(1)
     *,Tc2G32(1),Tc2dG32dp(1),Tc2CfGgWt(1),Tc2SqWl(1),Tc2TettaQ(1,nf)
     *,Tc2QqIsp(1),Tc2AlIsp(1),Tc2DeltaTs(1)
     *,Tc2dQIspdP(1),Tc2TtTnIsp(1),Tc2AlSqIsp(1),TC2ResWl(1),
     * Tc2LlWl(1),Tc2LaWl(1),Tc2ResWl0(1),Tc2CfRelAlIsp(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 605 
C     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 619 

****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'

C     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 629 

      integer*4 NTCR,TcRNode(1)
      parameter ( NTCR=0 )
      real*8 TcR_XQ(1),TcR_BQ(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 635 

      integer*4 Nwall2O
      parameter ( Nwall2O=0 )
      real*8  Wl2OTt(6,1),Wl2ORAw(1),Wl2OSqEnv(1),Wl2OQqBout(1),
     &        Wl2ODout(1)

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 643 

****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 649 

****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'

      integer NTen
      parameter(NTen=0)
      integer*4 TenIbn(1),TenNode(1)
      real*8 Tenzt(1),TenQqIn(1),TenAlG23(1),TenG23(1),TenG34(1)
     *,TenAlG43(1),TenG43(1),TenCpWl(1),TenTtWlk(1),TenTtWl(1)
     *,TenAlSqPhWl(1,nf),TenMasCpWl(1),TenCfAlGaWl(1),TenCfAlWpUpWl(1)
     *,TenCfAlWtWl(1),TenCfAlWpDoWl(1),TenEvUpWl(1),TenEvDoWl(1)
     *,TendG23dp(1),TendG34dp(1),TendG43dp(1)
     *,TenCfGgWpUpWt(1),TenCcGgWtWpDo(1),TenCfGgWpDoWt(1)
     *,TenG32(1),TendG32dp(1),TenCfGgWt(1),TenSqWl(1),TenTettaQ(1,nf)
     *,TenQqIsp(1),TenAlIsp(1),TenDeltaTs(1)
     *,TendQIspdP(1),TenTtTnIsp(1),TenAlSqIsp(1),TenResWl(1),
     >TenTcwAlCulc(1),TenTcwQq(1)
     *,TenLawL(1),TenQqInt(1),TenQqExt(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 672 
C     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 686 
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'

      integer n03
      parameter(n03=0)
      integer*4 Tn3RelP(2,1),Tn3RelE(4,1)
      real*8 Tn3RoWt(1),Tn3Cp(1,1),Tn3Lev(1),
     *       Tn3Ek(1,1),Tn3Rok(1,Tn3Ne),Tn3PpWt(1),Tn3Pk(1,1),Tn3Rot(1,
     >Tn3Ne),
     *       Tn3Xxk(1,Tn3Ne),Tn3MasBor(1),Tn3ccBor(1),Tn3VolWt(1),
     *       Tn3dp(1,Tn3Ne),Tn3MasWt(1),Tn3GIspSum(1),Tn3Pp(1),
     *       Tn3GCondSum(1),Tn3G32sum(1),Tn3G34sum(1),Tn3G23sum(1),
     *       Tn3G43sum(1),Tn3TettaQ(1,Tn3Ne),Tn3Ev_t(1,1),Tn3Dim_t(1),
     *       Tn3Dim_tmax/1/,Tn3EvDo(1),Tn3EvDo0(1),Tn3FlwDdMin(1),
     >Tn3MasWpUp(1),
     *       Tn3MasWpDo(1),Tn3MasGa(1),Tn3LevMas(1),Tn3LevMask(1),
     >Tn3PpGa(1),
     *       Tn3PpWpUp(1),Tn3EeGa(1),Tn3EeWpUp(1),Tn3EeWt(1),
     *       Tn3EeWpDo(1),Tn3LevRelax(1),Tn3VsGa(1),Tn3VsVpUp(1),
     *       Tn3VsWt(1),Tn3VsVpDo(1),Tn3EeWtSatWt(1),Tn3EeWpSatWt(1),
     *       Tn3RoGa(1),Tn3ROWpUp(1),Tn3RoWpDo(1),Tn3LevRel(1),Tn3Hi(1),
     *       Tn3Volk(1,Tn3Ne),Tn3Mask(1,1),Tn3CcGas(1,1),
     *       Tn3CfMasGasMin(1)/1.0d-7/,
     *       Tn3CcUp(1,1),Tn3FlwHUp(1,1),Tn3FlwHDo(1,1),Tn3Ee(1,1),
     *       Tn3TtGa(1),Tn3TtWpUp(1),Tn3TtWt(1),Tn3TtWpDo(1),
     >Tn3MasGaRealk(1),
     *       Tn3GCorr(1,1),Tn3drdp(1,1),Tn3drdh(1,1),Tn3vk(1,1),
     *       Tn3Waste_T(20,1),Tn3Dmint(1),Tn3RoDo(1),Tn3dEInt(1,1),
     *       Tn3WlUpQqOut(1),Tn3WlDoQqOut(1),Tn3TcwAlCulc(1,1),
     *       Tn3TtWlUp(1),Tn3TtWlDo(1),Tn3LaWl(1),
     *       Tn3QqWlEnvUp(1),Tn3QqWlEnvDo(1),Tn3RoomUpInd(1),
     >Tn3RoomDoInd(1),Tn3Tt(1,1),
     *       Tn3CcWpDo(1),Tn3CfGgWpDo_tmp(1),Tn3pmax(1),Tn3TtUp(1),
     >Tn3AlSqPhWl(4,1),Tn3AlWlUpDo(1),
     *       Tn3Levadd(1),Tn3cfdebGg(1),
     *       Tn3PressCorr(1),TN3VOL(1),TN3VOLGA(1),TN3VOLCORRUP(1),
     >TN3VOLCORRDO(1),Tn3FlwIntCoef(1),
     *       Tn3VolVpUpLim(1),Tn3dmintsum(1),Tn3CfGCorr(1),
     >Tn3LevTunG(1),Tn3LevTunPpRegG(6,1)
      integer*4 Tn3FlwNum(1),Tn3CcSetInd(1)
      logical*1 Tn3CcBorLb(1),Tn3VpInFlag(1),Tn3BorFl(1),
     >Tn3EntOut_Key(1),Tn3FlagGCorr(1),
     * Tn3FlagGCorrDn(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 724 

C     common/kts_hC     common/kts_hC     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 866 


****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'

      integer*4 n07,Tn7RelP(1,1),Tn7RelE(2,1),Tn7Homo(1),Tn7CcSetInd(1)
      real*8    Tn7RoGa(1),Tn7RoWt(1),Tn7Rok(1,2),Tn7XxWt(1),Tn7Xk(1,2),
     *          Tn7Lev(1),Tn7EeWt(1),Tn7VsGa(1),Tn7VsWt(1),Tn7PpGa(1),
     *          Tn7EeGa(1),Tn7EeWtSat(1),Tn7EeVpSat(1),Tn7LevRel(1),
     *          Tn7Hi(1),Tn7PpWt(1),Tn7TtWt(1),Tn7Ee(2,1),
     *          Tn7Ek(2,1),Tn7CcVolk(1),Tn7CcMask(1)
     *         ,Tn7XxBalWt(1),Tn7Levk(1),Tn7Lev_Rlx(1),Tn7Acon(1),
     *          Tn7Sq(1),Tn7CcBor(1),Tn7MasWt(1),Tn7MasGa(1),
     *          Tn7MasBor(1),Tn7GCorrWt(1),
     *          Tn7dRdPk(2,1),Tn7dRdP(2,1),Tn7dRdHk(2,1),Tn7dRdH(2,1),
     *          dP_Tn7(1),Tn7Dcon(1),Tn7LsMin(1),Tn7dMLoc(2,1),
     *          Tn7CfRoGaCorr(1),Tn7CfRoWtCorr(1),
     *          Tn7Waste_T(20,1),Tn7Rowttrue(1),Tn7RoWtSat(1),
     *          Tn7RoVpSat(1),Tn7Dmint(1),Tn7Gcorr(1),
     *          Tn7WlUpQqOut(1),Tn7WlDoQqOut(1),Tn7TcwAlCulc(1,1),
     *          Tn7TtWlUp(1),Tn7TtWlDo(1),Tn7LaWl(1),Tn7QqWlEnv(1),
     >Tn7RoomInd(1)
     *          ,Tn7EvDo(1),Tn7EvDo0(1),Tn7TtGa(1),Tn7AlSqPhWl(2,1),
     >Tn7AlWlUpDo(1)
     *          ,Tn7dMIntGa(1),Tn7dMIntWt(1),Tn7GCorrGa(1),
     >Tn7LevTunG(1),Tn7LevTunPpRegG(1)
      logical*1 Tn7_Flag_Mas_Deb(1),Tn7CfCorr_On(1)


      logical*1 Tn7CcBorLb(1)

      parameter(n07=0)
      real*8 Tn7Pk(2,1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 900 

C     common/kts_hC     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 957 

****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
      integer n08
      parameter(n08=0)
      integer*4 Tn8RelP(1,0),Tn8RelE(2,0),Tn8CcSetInd(1)
      real*8 Tn8RoWt(1),Tn8RoGa(1),Tn8GSumWt1(1),Tn8GSumWt2(1),
     *       Tn8Lev(1),Tn8RoWtk(1),Tn8PpWt(1),Tn8MasBor(1),Tn8ccBor(1),
     *       Tn8VolWt(1),Tn8XxWt(1),Tn8XxBalWt(1),
     *       Tn8MasWt(1),Tn8MasWtk(1),
     *       Tn8RoGak(1),Tn8Gsliv(1),Tn8EeGa(1),
     *       Tn8EeWt(1),Tn8PpGa(1),Tn8VsWt(1),Tn8VsGa(1),Tn8VsVp(1),
     *       Tn8EeWtSat(1),Tn8EeVpSat(1),Tn8LevRel(1),Tn8Hi(1),
     *       Tn8TtGa(1),Tn8TtWt(1),Tn8GgXx(1),
     *       Tn8dRdP1(1),Tn8dRdH1(1),Tn8dRdP2(1),Tn8dRdH2(1),
     *       Tn8Waste_T(20,1),Tn8RowtSat(1),Tn8RovpSat(1),
     *       Tn8QqEnv(1),Tn8RoomInd(1),Tn8TtMeDo(1)
     *       ,Tn8EvDo(1),Tn8EvDo0(1),Tn8GInOut(1),Tn8LevTunG(1)

      logical*1 Tn8CcBorLb(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 981 

C     common/kts_hC     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 1018 

****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'

      integer n09
      parameter(n09=0)
      integer*4 Tn9RelP(2,1),Tn9RelE(Tn9Ne,1),Tn9CcSetInd(1)
      real*8 Tn9RoWt(1),Tn9Cp(1,1),Tn9Lev(1),
     *       Tn9Ek(1,1),Tn9Rok(1,Tn9Ne),Tn9PpWt(1),Tn9Pk(1,1),Tn9Rot(1,
     >Tn9Ne),
     *       Tn9Xxk(1,Tn9Ne),Tn9MasBor(1),Tn9ccBor(1),Tn9VolWt(1),
     *       Tn9dp(1,Tn9Ne),Tn9MasWt(1),Tn9GIspSum(1),Tn9Pp(1),
     *       Tn9GCondSum(1),Tn9G32sum(1),Tn9G34sum(1),Tn9G23sum(1),
     *       Tn9G43sum(1),Tn9TettaQ(1,Tn9Ne),Tn9Ev_t(1,1),Tn9Dim_t(1),
     *       Tn9Dim_tmax/1/,Tn9EvDo(1),Tn9EvDo0(1),Tn9FlwDdMin(1),
     >Tn9MasVpUp(1),
     *       Tn9MasVpDo(1),Tn9MasGa(1),Tn9LevMas(1),Tn9LevMask(1),
     >Tn9PpGa(1),
     *       Tn9PpVpUp(1),Tn9EeGa(1),Tn9EeVpUp(1),Tn9EeWt(1),
     *       Tn9EeVpDo(1),Tn9LevRelax(1),Tn9VsGa(1),Tn9VsVpUp(1),
     *       Tn9VsWt(1),Tn9VsVpDo(1),Tn9EeWtSatWt(1),Tn9EeVpSatWt(1),
     *       Tn9RoGa(1),Tn9ROVpUp(1),Tn9RoVpDo(1),Tn9LevRel(1),Tn9Hi(1),
     *       Tn9Volk(1,Tn9Ne),  ! ������ ��� �� ��������
     *       Tn9VsWtUp(1),Tn9VsWtJa(1),Tn9Ee(1,1),Tn9TtGa(1),
     *       Tn9TtVpUp(1),Tn9TtWt(1),Tn9TtVpDo(1),
     *       Tn9Mask(1,1),Tn9CcGas(1,1),
     *       Tn9CfMasGasMin(1)/7.5d-7/,
     *       Tn9CcUp(1,1),Tn9FlwHUp(1,1),Tn9FlwHDo(1,1),Tn9GSepSum(1),
     *       Tn9GFlwWtSum(1),Tn9MasGaRealk(1),Tn9dEInt(1,1),
     *       Tn9GCorr(1,1),Tn9drdp(1,1),Tn9drdh(1,1),Tn9vk(1,1),
     *       Tn9Waste_T(20,1),Tn9Dmint(1),Tn9MasWtUp(1),Tn9MasWtJa(1),
     >Tn9RoDo(1),
     *       Tn9WlUpQqOut(1),Tn9WlDoQqOut(1),Tn9TcwAlCulc(1,1),
     *       Tn9TtWlUp(1),Tn9TtWlDo(1),Tn9LaWl(1),
     *       Tn9QqWlEnvUp(1),Tn9QqWlEnvDo(1),Tn9RoomUpInd(1),
     >Tn9RoomDoInd(1),
     *       Tn9CcVpDo(1),Tn9pmax(1),Tn9TtUp(1),Tn9AlSqPhWl(6,1),
     >Tn9AlWlUpDo(1),
     *       Tn9TtSatWt_(1),Tn9Levadd(1),Tn9vol(1),Tn9VolCorrUp(1),
     >Tn9VolCorrDo(1),
     *       Tn9PressCorr(1),Tn9VolGa(1),Tn9FlwIntCoef(1),
     >Tn9VolVpUpLim(1),
     *       Tn9LevTunG(1),Tn9LevTunPpRegG(6,1)

      integer*4 Tn9FlwNum(1)
      logical*1 Tn9CcBorLb(1),Tn9VpInFlag(1),Tn9BorFl(1),
     >Tn9EntOut_Key(1),
     *          Tn9FlagGCorr(1),Tn9FlagGCorrDn(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 1061 

C     common/kts_hC     common/kts_hC     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 1205 


****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
      integer*4 Ntur1,Tr1Lin(1),tr2RLinTyp(1)
      parameter(ntur1=0)
      real*8    tr2FlwKsi1(1)
      real*8 Tr1Oin(1),Tr1Qsd(1),Tr1NdP(1),Tr1genTyp(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 1217 
      integer*4 Ntur3
      parameter(Ntur3=0)
         integer*4 Tr3beaTyp(1),Tr3beaInd(1)
      real*8    Tr3Omdl(1),Tr3Qsd(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 1223 
      integer*4 Ntur4
      parameter(Ntur4=0)
         integer*4 Tr4beaTyp(1),Tr4beaInd(1)
      real*8    Tr4Omdl(1),Tr4Qsd(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 1229 



****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'

      real*8 C1serv(nwall) !����. ��� ������� ������. ������� ���
      real*8 C2serv(nwall) !����������� ���� � ���. ��. ����� ������
      real*8 wl1_QqOut(nwall)  !��������� �������� ����� � ������� ��������
      real*8 wl1AlCulcWl(nwall)   !��������� �������� ����� � ������� ��������
      real*8 wl1_Tt_t(nwall)
      real*8 wl1_XQ(nwall),wl1_BQ(nwall)  ! �-�� � ������� ���������
      common/kts_hWALL1/ C1serv,C2serv,wl1_QqOut,wl1AlCulcWl,
     *  wl1_Tt_t,wl1_XQ,wl1_BQ
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 1249 

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 1263 

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 1269 

****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'

C     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 1289 

      integer*4 Nwall2
      parameter ( Nwall2=0 )
      real*8 wl2_QqOut(1)  !��������� �������� ����� � ������� ��������
      real*8 Wl2Tt_t(1)
      real*8 WL2mass(1),WL2Cp(1),WL2Tt(1)
      real*8 Wl2LamdC(1)
      integer*4 Wl2IndRoom(1),Wl2Ndet(1),Wl2Node(1)
      real*8 Wl2QqEnv(1),Wl2AlCulcWl2(1)
      real*8 Wl2Leff(1)
      real*8 Wl2Rb(1)
      real*8 Wl2Raw(1), Wl2SqEnv(1), Wl2QqBout(1), Wl2DOut(1)
      real*8 Wl2_XQ(1),Wl2_BQ(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 1304 

****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
      integer*4 Nwlcon2
      parameter ( Nwlcon2=0 )
      integer*4 WLC2Iwl(1)
      real*8 WLC2_Qq(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 1313 




****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

!!      external kts_h_it,kts_h_end
!!      call spw_IterReg(kts_h_it,kts_h_end)
c
      ErrorNum(1)=-1
c
      deltat1=deltat

      if(StpFlag(1).eq.0) then
       StpFlag(1)=1
      else
       StpFlag(1)=0
      end if

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 1337 

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 1343 
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 1348 

c --- �������� ������� ���������� ��� ����� � ��������
      time_tmp = time_df1()
      call kts_hconnIn
      tick(2,1) = time_df1()-time_tmp
      ErrorNum(1)=2
c
      do i=1,nu
        if( VOLUKZ(i).gt.VolNodMax(1)) VOLUKZ(i)=VolNodMax(1)
      enddo
      do i=1,nv
        if( FlwLnSq(i).gt.FlwLnSqMax(1)) FlwLnSq(i)=FlwLnSqMax(1)
      enddo
c
c uvk ��������� � net42
      call SetCfPp(Nodcfppup_,Tn3CfPp,Tn7CfPp,Tn8CfPp,Tn9CfPp,NodCfPp
     &            ,NodcfppLoc)
c     ��������� ��������� ������ ���������� ���������
      time_tmp = time_df1()
      call start_switch(flag_first(1),flag_start(1),accel(1),dtint,atau,
     &                 accelnew(1),maxiter(1),maxiternew(1),deltat,
     &                 nodcfppup(1), nodcfppup_(1))

      tick(3,1) = time_df1()-time_tmp
      ErrorNum(1)=3
C     ��������� ��������� ������ ���������� ���������

      TimeSrv=dtInt/MaxIternew(1)
      !���� ��������� ����� ��� �������� ������ ��� ���������
      if((Event(1).eq.3).or.(Event(1).eq.5)) then

c   ������ ���������� �������
        call PhConst             !����������� ���������� ����������
c        call spw_getfile(' ','input','airph_cbf',fn1)
c        call airfph(fn1)       !�������� ������ ��� ���������
                               !������� �������
c        call spw_getfile(' ','input','helph_cbf',fn1)
c        call helfph(fn1)       !�������� ������ ��� ���������
                               !������� �����
c        call spw_getfile(' ','input','nitrph_cbf',fn1)
c        call nitrfph(fn1)       !�������� ������ ��� ���������
                               !������� �����
c        call spw_getfile(' ','input','oxiph_cbf',fn1)
c        call oxifph(fn1)       !�������� ������ ��� ���������
                               !������� ���������     
      endif

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 1418 
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 1452 
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 1486 
      time_tmp = time_df1()
      call clr_Y(Nent,Ye)  !��������� ������ ������ ��� ���������
      tick(5,1) = time_df1()-time_tmp
      ErrorNum(1)=5
      time_tmp = time_df1()
      call Room(nRm1,Rm1Tt,Rm1TtOut,Rm1Pp,Rm1PpOut,
     *          Rm1IndRoom0,Rm1IndRoom,Rm1TenFlag,Rm1TenTt)
      tick(1,1) = time_df1()-Time_tmp
      ErrorNum(1)=1
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 1497 
      call RQcf(nRQ,RQCfenvIn,RQCfenv,RQCfalfIn,RQCfalf,RQCfMasMe,
     >RQCfMasMeIn)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 1500 
c   ��������������� ������� ��� ������� �����
      time_tmp = time_df1()
       call Node_pre(nu,VOLUKZ0,Atau,Vol1,
     &      NodGaType,SochiGasIn1,SochiGasIn2,SochiCfCpGas,
     &                    SochiGasIn10,
     &                    SochiGasIn20,SochiCfCpGas0,
     &                    PropsKey )

      tick(6,1) = time_df1()-time_tmp
      ErrorNum(1)=6
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 1512 

c --- ���������� ��������� ������ � ������ �������� ---

      call RegPout(nVPO,nu,nVlg,dtInt,VPOVlg,VPONod,VPOTyp,
     *                 VPOPout,VPOdP,VPOTime,VlgPos,NodPp)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 1519 

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 1554 
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 1588 
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 1597 
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 1603 
c
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 1674 
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 1744 

****** Begin section 'ForwEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'ForwEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'ForwEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 1752 


****** End   section 'ForwEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'ForwEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'

      time_tmp = time_df1()
c      call Nod_Typ_Tnk(nv,nu,n03,n07,n08,n09,
c     *                 LinIBNfro,LinIBNto,IFROKZ,ITONKz,
c     *                 HFrom,HTo,
c     *                 Tn3Lev,Tn7Lev,Tn8Lev,Tn9Lev,
c     *                 NodTypTnk)
      call Flw_ForwEx(nv,Gk,GZ,Rovk,Rovz,FlwVsk,FlwVs,FlwRo,FlwRoFrc,
     >FlwRoFrom,FlwRoTo
     *,nfwv,FWVFLOWW,FWVFLOWV,FwvRoFrom1,FwvRoFrom2,FwvRoTo1,FwvRoTo2)
      tick(153,1) = time_df1()-time_tmp
      ErrorNum(1)=153
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 1769 


****** End   section 'ForwEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'ForwEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'ForwEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'ForwEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'ForwEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'ForwEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'ForwEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'ForwEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'

        call Mtl_ForwEx(NMtl,Mtl_QqOut,MtlTt,MtlTt_t
     &                     ,MtlLamdC,MtlCpC
     &                     ,MtlMatType,MtlMatDepType,MtlLamd,MtlCp )
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 1785 

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 1793 


****** End   section 'ForwEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'ForwEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'ForwEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'ForwEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'ForwEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'ForwEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'



      time_tmp = time_df1()

      call Node_ForwEx(nu,Npres,Nent,Pk,NodPp,Ek,NodEe,Rok,
     *                   Rozt,NodRo,EeWtSatk,NodEeWtSat,EeVpSatk,
     *                   NodEeVpSat,NodTtk,NodTt,NodCp,NodCp,
     *                   NoddRodPk,NoddRodPk1,NoddRodP,NoddRodEk,
     *                   NoddRodEk1,NoddRodE,nodEeGa,nodEeGat,
     *                   nodccgat,nodvvgat,nodccga,nodvvga,
     *                   VOLUKZ0,VOLUKZt,NodXxk1,
     *                   NodXx,NodXxBalk1,NodXxBal,
     *                   nodrotrue,nodrotrue0,NodFreez,
     *                   PExtIter,PExtIter_,invperP,EExtIter,EExtIter_,
     *                   invperE,NodTyp,
     *                   NodRoGa,NodRoGa0,
     *                   NodCgEe0,NodCgEe,NodRoomFl,NodTtenv,NodPpenv
     &                      ,LimTmax,LimTmin,dPMaxFlag
     *                      ,NodRoTot,NodRoTot0,
     *                       LimdMWmax,LimdMGmax,Masdt_Wv,Masdt_Ga,
     *                       NWall,Wl1Tt,LimTWmax,LimTWmin,
     *                       NodMasWvMax,NodMasGaMax,
     *                       LimddMWmax,LimddMGmax
     *           ,LimPmax,NODFREEZPH
     *           ,NodCfdPdt_main,NodCfdPdt_loc,NodCfdPdt_key,NodCfdPdt
     *           ,NodCfdPdt_loc1,NodEeWv,NodEeWvt,EGk,NodNumEe(1)
     *           ,NodEeWv_tmp,NodTe1Key,NumFlStop(1),NumFlagStop(1),
     *       OutLimPNodNum(1),OutLimTupNodNum(1),OutLimTdoNodNum(1),
     >volukz)

c      call Node_Vs(nu,NodPp,NodTt,NodVsVp,NodVsWt,
c     *             NodVsFr,NodXx,NodBodyTyp)
      tick(162,1) = time_df1()-time_tmp
      ErrorNum(1)=162


C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 1839 

****** End   section 'ForwEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'ForwEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'ForwEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'ForwEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 1861 

****** End   section 'ForwEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'ForwEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'ForwEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'ForwEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 1878 

****** End   section 'ForwEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'ForwEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'ForwEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'ForwEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'ForwEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'ForwEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'ForwEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'ForwEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'ForwEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'ForwEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'ForwEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'ForwEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 1899 

****** End   section 'ForwEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'ForwEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'ForwEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'ForwEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'

        if (TnkNormFlag(1)) then
          TnkNormFlag=.false.
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 1912 
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 1917 
        endif

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 1942 



****** End   section 'ForwEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'ForwEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'ForwEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'ForwEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 1994 

****** End   section 'ForwEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'ForwEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'ForwEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'ForwEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 2017 


****** End   section 'ForwEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'ForwEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'ForwEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'ForwEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 2048 


****** End   section 'ForwEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'ForwEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'ForwEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'ForwEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'ForwEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'ForwEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 2060 




****** End   section 'ForwEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'ForwEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'ForwEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'ForwEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'

        call Wl1_ForwEx(nwall,wl1_QqOut,wl1_Tt_t,Wl1Tt
     &            ,WL1MatTyp,Wl1MatDepTyp
     &            ,Wl1Lamd,Wl1Cp,Wl1LamdC,Wl1CpC)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 2074 


****** End   section 'ForwEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'ForwEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'ForwEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'ForwEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 2088 


****** End   section 'ForwEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'ForwEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'ForwEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'ForwEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 2100 





****** End   section 'ForwEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

      FlwCfSum=FlwCfResL !�-� �������� �/�
      time_tmp = time_df1()
      call clr_FlwKge(FlwKge,nf,nv,LinIbnFro,LinIbnTo)
      call Flw_Tn_K(nv,LinIBNfro,LinIBNto,Gzz,Gk,
     *               FlwCfSum,FlwCfTnk,CfTnkFlw(1))  !!!!!!!!!!!!!!!!!!!!!!
      tick(7,1) = time_df1()-time_tmp
      ErrorNum(1)=7
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 2116 

****** Begin section 'Tn9_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'Tn9_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'Tn9_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'Tn9_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'Tn9_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'Tn9_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'Tn9_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'Tn9_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'Tn9_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'Tn9_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'Tn9_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'Tn9_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'Tn9_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'Tn9_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'Tn9_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'Tn9_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'Tn9_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'Tn9_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'Tn9_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'Tn9_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'Tn9_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'Tn9_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'Tn9_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'Tn9_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'Tn9_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'Tn9_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'Tn9_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'Tn9_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'Tn9_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'Tn9_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'Tn9_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'Tn9_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'Tn9_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'Tn9_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'Tn9_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'Tn9_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'Tn9_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'Tn9_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'Tn9_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'Tn9_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'Tn9_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'Tn9_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'Tn9_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** End   section 'Tn9_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'Tn9_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'Tn9_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'Tn9_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'Tn9_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'Tn9_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'Tn9_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'Tn9_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'Tn9_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'Tn9_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'Tn9_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'Tn9_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 2327 


****** End   section 'Tn9_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'Tn9_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'Tn9_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'Tn9_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'Tn9_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'Tn9_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'Tn9_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'Tn9_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'Tn9_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'Tn9_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'Tn9_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'Tn9_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'Tn9_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'Tn9_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'Tn9_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'Tn9_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'Tn9_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'Tn9_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'Tn9_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

****** Begin section 'Tn3_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'Tn3_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'Tn3_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'Tn3_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'Tn3_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'Tn3_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'Tn3_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'Tn3_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'Tn3_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'Tn3_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'Tn3_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'Tn3_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'Tn3_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'Tn3_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'Tn3_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'Tn3_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'Tn3_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'Tn3_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'Tn3_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'Tn3_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'Tn3_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'Tn3_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'Tn3_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'Tn3_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'Tn3_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'Tn3_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'Tn3_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'Tn3_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'Tn3_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'Tn3_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'Tn3_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'Tn3_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'Tn3_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'Tn3_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'Tn3_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'Tn3_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'Tn3_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'Tn3_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'Tn3_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'Tn3_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'Tn3_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'Tn3_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'Tn3_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 2561 


****** End   section 'Tn3_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'Tn3_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'Tn3_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'Tn3_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'Tn3_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'Tn3_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'Tn3_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'Tn3_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'Tn3_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'Tn3_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'Tn3_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'Tn3_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** End   section 'Tn3_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'Tn3_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'Tn3_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'Tn3_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'Tn3_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'Tn3_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'Tn3_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'Tn3_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'Tn3_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'Tn3_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'Tn3_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'Tn3_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'Tn3_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'Tn3_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'Tn3_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'Tn3_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'Tn3_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'Tn3_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'Tn3_PreIt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 2641 

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 2649 
c   ������ ������� ��������� � ����. ������ �������� �� ������
      time_tmp = time_df1()
        call Flw_Xx_v1(nv,n03,Nf,IFROKZ,ITONKZ,LinIBNfro,
     *                 LinIBNto,NodXxBal,Tn8XxBalWt,
     *                 Tn3Xxk,
     *                 FlwXxBalFrom,FlwXxBalTo,FlwXxFrom,FlwXxTo,
     *                 Tn7XxWt,n09,Tn9Xxk,ComGamFrom,
     *                 ComGamTo,nu,n07,n08,
     *                 NodXb_prev,NodXb_next,Nodxb_filter,Gz)
      tick(11,1) = time_df1()-time_tmp
      ErrorNum(1)=11
      time_tmp = time_df1()
        call Density_new(nv,nu,
     *               n07,n03,n08,n09,
     *               LinIBNfro,LinIBNto,FlwRoFrom,
     *               IFROKZ,ITONKZ,
     *               FlwRoTo,
     *               ROVz,Rovz,
     *               FlwLl,FlwSq,GZ,FlwRo,
     *               dtInt,FlwRoFrc,Hfrom,Hto,
     *               AconRovk,Tn7lev,Tn8lev,Tn3Lev,Tn9Lev,NodTypTnk,
     >FlwResTyp,
     *               FlwHTnToDo,FlwHTnToUp,
     *               FlwHTnFromDo,FlwHTnFromUp,
     *               FlwVsFrom,FlwVsTo,FlwVs,FlwVs,
     *               FlwRoFromSt,
     *               FlwRoToSt,NodLaFr,FlwLaFrc,FlwRoFlag,FlwCfdh,
     *               NodGgSumZero,NodGgSumZeroin,NodLaFr_cor(1),
     *                 FlwDensCfL,FlwRoC)
     
      tick(12,1) = time_df1()-time_tmp
      ErrorNum(1)=12
      time_tmp = time_df1()
        call Volume(nv,LinIBNFro,LinIBNTo,IFROKZ,ITONKZ,
     *              HFrom,HTo,
     *              Tn8Lev,Tn3LevRel,Tn9LevRel,
     *              Tn8RoWt,Tn3RoWt,Tn9RoWt,dHLev,
     *              FlwdPStat,
     *              FlwRoFrc,
     *              InternalIter,Tn3EvDo,
     *              Tn7Levk,Tn7RoWt,Tn9EvDo,
     *              Tn8RoGa,Tn3RoGa,Tn9RoGa,
     *              Tn7RoGa,Gz,aconRovk,
     *              FlwdPLevFrom,FlwdPLevTo,
     *              Flwdd,FlwAngFrom,FlwAngTo,Tn3RoDo,Tn9RoDo
     *                 ,FlwHTnFromUp,FlwHTnFromDo,FlwHTnToUp,FlwHTnToDo
     *                 ,FlwRoFrom,FlwRoTo,n08,n03,n07,n09,FlwKkdh,
     >FlwFlagm2,
     *              NodCcGa,NodRoWtSat,NodRoGa,Tn3RoWpUp,Tn9RoVpUp)
      tick(13,1) = time_df1()-time_tmp
      ErrorNum(1)=13
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 2700 
c---------------------------------------------------------------------
c   ������ ��������� �������� � ����
c   ���� ������ �������
      time_tmp = time_df1()
      call Clr_Y(nv,FlwdPdGPm)
c      call Clr_Y(nv,FlwdPPm)
      FlwdPPm=FlwdPdop
      tick(14,1) = time_df1()-time_tmp
      ErrorNum(1)=14
      time_tmp = time_df1()
      call Clr_Y(nu,NodQqPm)
      tick(15,1) = time_df1()-time_tmp
      ErrorNum(1)=15
      time_tmp = time_df1()
      call Pump1(Np,FlwRoFrom,VNKZ,DPN,DPN0,OMPN,rovk,PumKRo
     &          ,Gz,PumG0,PumPow0,PumdPow,PumPow,PumdPowdom,nv
     &          ,Nodromas,IFROKZ,nu,Pum_flag,PumNRo,NodPp,
     &      PumRes,PumRes0,PumOm0,PumResKk2,PumPowPol,PumKkOm,
     &      PumOmLoc,PumUnlin,PumTUnlin,0,dtint)
      tick(16,1) = time_df1()-time_tmp
      ErrorNum(1)=16
      time_tmp = time_df1()
      do i=1,np
        FlwdPPm(VNKZ(i))=FlwdPPm(VNKZ(i))+DPN(i)
      enddo
      tick(17,1) = time_df1()-time_tmp
      ErrorNum(1)=17
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 2908 

****** Begin section 'TurPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'TurPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'TurPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'TurPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'TurPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'TurPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'TurPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'TurPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'TurPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'TurPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'TurPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'TurPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'TurPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'TurPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'TurPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'TurPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'TurPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'TurPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'TurPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'TurPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'TurPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'TurPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'TurPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'TurPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'TurPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'TurPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'TurPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'TurPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'TurPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'TurPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'TurPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'TurPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'TurPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'TurPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'TurPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'TurPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'TurPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'TurPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'TurPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'TurPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'TurPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'TurPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'TurPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** End   section 'TurPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'TurPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'TurPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'TurPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'TurPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'TurPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'TurPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'TurPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'TurPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'TurPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'TurPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'TurPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** End   section 'TurPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'TurPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'TurPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'TurPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'TurPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'TurPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 2974 

****** End   section 'TurPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'TurPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'TurPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'TurPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'TurPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'TurPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'TurPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'TurPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'TurPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'TurPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'TurPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'TurPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'TurPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'




c---------------------------------------------------------------
c     ���������� ������������� ������
*              MIKE2
      FlwSqMin=FlwSq
      call Clr_Y(nv,FlwRes)
      call Clr_Y(nv,FlwSumRes)
* ������ �/� ������
c      call FlwSub_Res(nv,Gz,FlwRoFrc,FlwLl,
      time_tmp = time_df1()
      call FlwSub_Res(nv,Gz,Rovk,FlwLl,
     &             FlwRg,FlwSq,FlwDd,FlwCon,
     &             FlwRes,FlwRes0,FlwResTyp,FlwResDel,
     &             FlwVs,FlwRoFrom,FlwRoTo,
     &             np2,Pm2Nb,Pm2Res,     
     &             np4,Pm4Nb,Pm4Res,     
     &             FlwSumRes,FlwResCf,FlwKeyVp,
     &             FlwXxBalFrom,FlwXxBalTo,
     &             nf,FlwRedGa,FlwADd,FlwASq2,
     &             Rov0,LbResRo,FlwType
     &           ,FlwSqMin
     *           ,ComGamFrom,ComGamTo
     &           )
      tick(24,1) = time_df1()-time_tmp
      ErrorNum(1)=24
c
* �������� Kv
      time_tmp = time_df1()
      call Flwsub_Kv(nv,Kv1,Kv,FlwResCf)
      tick(26,1) = time_df1()-time_tmp
      ErrorNum(1)=26
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 3035 

* ��������� ��������� �������������
      time_tmp = time_df1()
      call Rs1Sub_Res(nv,nrs1,Rs1Flw,Rs1Type,
     &            Rovk,FlwSq,Rs1Res,Rs1Con,
     &            Flwres,Flwcon,
     &            FlwSumRes
     &           ,Rs1dpRes
     &           ,Gz
     &           ,Rs1Res0,Rs1ResRev0
     &           ,FlwType
     &           ,Rs1TypeCor,FlwVs,Rs1Vs0,Rs1PowCor
     &           ,FlwResTyp,Rs1IniRo,Rs1RoIni,Rs1ksi)
      tick(27,1) = time_df1()-time_tmp
      ErrorNum(1)=27
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 3052 

* ��������� ���������
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 3070 

      nvlgpos=maxval(VlgNT)
      call VlgSub_Res(nv,nvlg,VlgFlw,Gz,VlgGTCh,
     &            Rovk,FlwSq,Flwres,Flwcon,FlwSumRes,
     &            VlgPos,VlgRes,VlgResMax,VlgCon,VlgSm,
     &            VlgKk,VlgType,nvlgpos,VlgNT,VlgSm_T,VlgSs_T,
     &            VlgFlag,VlgSmt,VlgKSmin,VlgNSmin,VlgVid,
     &            VlgSq0Cor,VlgReSmSs,
     &            VlgTypeCor,FlwVs,VlgVs0,VlgPowCor,VlgKvmax,
     &            VlgSmOld,FlwRe,FlwRg,FlwDd,VlgSSm,FlwSqMin,
     &          VlgMf5Lb,VlgMf5Cf,VlgCfRes,VlgSSNul,VlgMfkSs)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 3083 
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 3091 

* ��������� �������
      time_tmp = time_df1()
      call Fr1sub_res(nv,nfr1,Fr1Flw,Fr1Res,Fr1Con,
     &             Flwcon,Flwres,FlwSumRes,
     &             Fr1Mf1Lb,Fr1Mf1Lb1,Fr1Mf1Cf,Fr1Res0
     &            ,Fr1Type
     &            ,Fr1dPRes
     &            ,FlwSq,Gz,Rovz
     &            ,Fr1ResCf,Fr1ksi
     &                  ,FlwResTyp,Fr1IniRo,Fr1RoIni,Fr1ResK
     &                  ,Fr1TypeCor,FlwVs,Fr1Vs0,Fr1PowCor,Fr1dP)
      tick(31,1) = time_df1()-time_tmp
      ErrorNum(1)=31
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 3107 

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 3113 

c   ����������� ������������� �������� ��������
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 3156 
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 3162 

c   ����������� �/� ����������������� �������� �2
      call vlr2_Res(nVls2,nVls,nVlg,Vls2Vlg,Vls2Vls,
     &              VlgRes,VlsRes)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 3168 
c   ����������� ��������� ����������������� ��������
      time_tmp = time_df1()
      call VlsSub_Pos(nv,nu,n08,n03,nvls,
     &                VlsFlw,VlsKeyGg,VlsTime,
     &                NodPp,Tn8PpWt,Tn3PpWt,
     &                Ifrokz,Itonkz,VlsPpMin,VlsPpWrk,
     &                VlsPos,VlsPpDel,FlwDpPm,
     &                VlsCharFl,VlsGis,FlwCon,
     &                dtInt,
     &                ROVk,HFrom,HTo,dHLev,
     &                VlsGgCf0,FlwGrdPp,
     &                VlsMf1Lb,VlsMf1Lb1,VlsMf1Cf,
     &                VlsMf2Lb,VlsMf2Lb1,VlsMf2Cf,VlsRes,
     &                vlsmf3lb,vlsmf4lb,vlsmf4cf,vlsposO)
      tick(36,1) = time_df1()-time_tmp
      ErrorNum(1)=36

c   ����������� ������������� ����������������� ��������
      time_tmp = time_df1()
      call VlsSub_Res(nv,nvls,VlsFlw,VlsMax,Vlsmin,
     &             VlsCf,VlsPos,VlsRes,VlsCon,
     &             Flwcon,Flwres,
     &             FlwSumRes,FlwResCf,VlsMin1
     &            ,FlwSqMin,FlwSq,
     &             FlwResTyp,VlsIniRo,FlwRo0,Rovz)
      tick(37,1) = time_df1()-time_tmp
      ErrorNum(1)=37
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 3197 
c   ����������� ��������� ����������������� �������� �2
      call vls2pos(nVls2,nVls,nVlg,Vls2Vlg,Vls2Vls,
     &             VlgPos,VlsPosO,
     *                 VlgMfkSs,Vls2MfkSs)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 3203 

c   ����������� ������������� ��������
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 3230 

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 3235 

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 3241 

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 3246 

      call Drssub_Res(nv,nDrs,DrsFlw,DrsType,
     *           DrsD,DrsL,DrsRes,
     *           FlwSqMin,FlwDd,FlwRe,FlwRg,FlwCfSum,DrsN,
     *           DrsKk,Rovz,
     *           FlwVs,DrsTypeCor,DrsVs0,DrsPowCor,FlwSq,
     *           DrsDd,DrsFlDd,DrsInOff,DrsDIn)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 3255 

*!!! ��� ����������� ����� ��������

      time_tmp = time_df1()
      call Clr_Y(nv,FlwWt)
      tick(38,1) = time_df1()-time_tmp
      ErrorNum(1)=38
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 3264 

c   ����������� ����� ��� ��������
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 3276 

c   ����������� ����� ��� �������� ��������
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 3287 
c   ����������� ����� ��� ����������������� ��������
      time_tmp = time_df1()
      call VlsSub_Wt(nv,nvls,Vlsmin,VlsWt,
     &               VlsFlw,FlwSumRes,FlwWt
     &              ,FlwType)
      tick(41,1) = time_df1()-time_tmp
      ErrorNum(1)=41


C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 3298 



      time_tmp = time_df1()
      call clr_Y(nu,NodGgSum)
      tick(45,1) = time_df1()-time_tmp
      ErrorNum(1)=45
c      call clr_Y(nu,NodGgSumIn)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 3308 

c ������������� ������� ���������� ����� ������� ��������
      Pk1=Pk
      Ek1=Ek
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

c ������ ������� ������������� ������������� ����� �����������
****** Begin section 'CALL_ALFA' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'CALL_ALFA' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'CALL_ALFA' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'CALL_ALFA' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'CALL_ALFA' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'CALL_ALFA' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'CALL_ALFA' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'CALL_ALFA' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'CALL_ALFA' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 3360 

****** End   section 'CALL_ALFA' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'CALL_ALFA' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'CALL_ALFA' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'CALL_ALFA' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'CALL_ALFA' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'CALL_ALFA' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'CALL_ALFA' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'CALL_ALFA' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'CALL_ALFA' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'CALL_ALFA' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'CALL_ALFA' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'CALL_ALFA' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'CALL_ALFA' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'CALL_ALFA' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'CALL_ALFA' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'CALL_ALFA' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'CALL_ALFA' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'CALL_ALFA' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'CALL_ALFA' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'CALL_ALFA' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'CALL_ALFA' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'CALL_ALFA' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'CALL_ALFA' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'CALL_ALFA' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'CALL_ALFA' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'CALL_ALFA' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'CALL_ALFA' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'CALL_ALFA' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'CALL_ALFA' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'CALL_ALFA' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'CALL_ALFA' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'CALL_ALFA' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'CALL_ALFA' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'CALL_ALFA' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** End   section 'CALL_ALFA' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'CALL_ALFA' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'CALL_ALFA' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'CALL_ALFA' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'CALL_ALFA' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'CALL_ALFA' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'CALL_ALFA' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'CALL_ALFA' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'CALL_ALFA' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'CALL_ALFA' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'CALL_ALFA' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'CALL_ALFA' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** End   section 'CALL_ALFA' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'CALL_ALFA' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'CALL_ALFA' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'CALL_ALFA' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'CALL_ALFA' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'CALL_ALFA' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'CALL_ALFA' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'CALL_ALFA' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'CALL_ALFA' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'CALL_ALFA' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'CALL_ALFA' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'CALL_ALFA' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'CALL_ALFA' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'CALL_ALFA' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'CALL_ALFA' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'CALL_ALFA' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'CALL_ALFA' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'CALL_ALFA' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'CALL_ALFA' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
      IC=0
      go=.true.
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 3432 
      end
****** End   file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET41.fs'
****** Begin file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Look.fs'
c --------------------------------------------------------------------
c ��������� ������������ ����'� ��
c 
c --------------------------------------------------------------------
c
c � ���� ������������ ������������ ��� �������, ������������� 
c     � GIW'� �� ��������� ����.
c
c ��������: �������� �. �.
c
c ����� ���������: ������ �.�.
c
c �����������: ����� ���
c
c ---------------------------------------------------------------------
c
c
c     $LOOK:Look
      subroutine kts_hLook(arg1,arg2)
      
      implicit none

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 3461 
      integer FirstStepErrFlag
      common/kts_hErrorFlags/ FirstStepErrFlag
      integer is_error

      include 'kts_h.fh'
      REAL*8 FlwTnSqFrom(894)
      COMMON/kts_h_nowrt/ FlwTnSqFrom
      REAL*8 FlwHTnFromUp(894)
      COMMON/kts_h_nowrt/ FlwHTnFromUp
      REAL*8 FlwTnSqTo(894)
      COMMON/kts_h_nowrt/ FlwTnSqTo
      REAL*8 FlwHTnFromDo(894)
      COMMON/kts_h_nowrt/ FlwHTnFromDo
      REAL*8 FlwHTnToUp(894)
      COMMON/kts_h_nowrt/ FlwHTnToUp
      REAL*8 FlwHTnToDo(894)
      COMMON/kts_h_nowrt/ FlwHTnToDo
      INTEGER*4 Fan2NTN2N
      PARAMETER (Fan2NTN2N=0)
      INTEGER*4 Fan2NTN0N
      PARAMETER (Fan2NTN0N=0)
      INTEGER*4 Fan2NTauNN
      PARAMETER (Fan2NTauNN=0)
      INTEGER*4 Fan2NdPG5N
      PARAMETER (Fan2NdPG5N=0)
      INTEGER*4 Fan2NdPG4N
      PARAMETER (Fan2NdPG4N=0)
      INTEGER*4 Fan2NdPG3N
      PARAMETER (Fan2NdPG3N=0)
      INTEGER*4 Fan2NdPG2N
      PARAMETER (Fan2NdPG2N=0)
      INTEGER*4 Fan2NdPG0N
      PARAMETER (Fan2NdPG0N=0)
      INTEGER*4 Fan2TauNN
      PARAMETER (Fan2TauNN=0)
      INTEGER*4 FanDim_TauNmax
      PARAMETER (FanDim_TauNmax=0)
      INTEGER*4 FanDim_Taumax
      PARAMETER (FanDim_Taumax=0)
      INTEGER*4 FanDim_KMax
      PARAMETER (FanDim_KMax=0)
      INTEGER*4 FanDim_PMax
      PARAMETER (FanDim_PMax=0)
      INTEGER*4 Tn8Ne
      PARAMETER (Tn8Ne=2)
      INTEGER*4 Tn3Np
      PARAMETER (Tn3Np=2)
      INTEGER*4 Tn3Ne
      PARAMETER (Tn3Ne=4)
      INTEGER*4 INVPERe(551)
      COMMON/kts_h_nowrt/ INVPERe
      INTEGER*4 NZSUBe(4335)
      COMMON/kts_h_nowrt/ NZSUBe
      INTEGER*4 INZSUBe(552)
      COMMON/kts_h_nowrt/ INZSUBe
      INTEGER*4 ILNZe(552)
      COMMON/kts_h_nowrt/ ILNZe
      INTEGER*4 INVPERp(551)
      COMMON/kts_h_nowrt/ INVPERp
      INTEGER*4 NZSUBp(4335)
      COMMON/kts_h_nowrt/ NZSUBp
      INTEGER*4 INZSUBp(552)
      COMMON/kts_h_nowrt/ INZSUBp
      INTEGER*4 ILNZp(552)
      COMMON/kts_h_nowrt/ ILNZp
      INTEGER*4 Nthermo
      PARAMETER (Nthermo=894)
      INTEGER*4 Nhydro
      PARAMETER (Nhydro=894)
      INTEGER*4 Nent
      PARAMETER (Nent=551)
      INTEGER*4 Npc
      PARAMETER (Npc=1102)
      INTEGER*4 Npres
      PARAMETER (Npres=551)
      INTEGER*4 Nf
      PARAMETER (Nf=6)
      INTEGER*4 Tn7Ne
      PARAMETER (Tn7Ne=1)
      INTEGER*4 Tn7Np
      PARAMETER (Tn7Np=1)
      INTEGER*4 Tn9Ne
      PARAMETER (Tn9Ne=6)
      INTEGER*4 Tn9Np
      PARAMETER (Tn9Np=2)
      INTEGER*4 Fan2NTN3N
      PARAMETER (Fan2NTN3N=0)
      INTEGER*4 Fan2NTN4N
      PARAMETER (Fan2NTN4N=0)
      INTEGER*4 Fan2NTN5N
      PARAMETER (Fan2NTN5N=0)
      INTEGER*4 Tr1Dim_Nu
      PARAMETER (Tr1Dim_Nu=-111)
      INTEGER*4 Tr2Dim_Ksi
      PARAMETER (Tr2Dim_Ksi=-111)
      INTEGER*4 Tn8Np
      PARAMETER (Tn8Np=1)
      LOGICAL*1 PIntIter(551)
      COMMON/kts_h_nowrt/ PIntIter
      LOGICAL*1 EIntIter(551)
      COMMON/kts_h_nowrt/ EIntIter
      LOGICAL*1 EextIter(551)
      COMMON/kts_h_nowrt/ EextIter
      LOGICAL*1 PExtIter(551)
      COMMON/kts_h_nowrt/ PExtIter
      !DEC$PSECT/kts_h_NOWRT/ NoWRT

      integer*4 arg1,arg2
      character*256 :: spjnam='kts_h'

c      character*80 msg
      
c      write(msg,*) 'kts_hLook: a1=',arg1,'    a2=',arg2
c      call wrcons(msg)

      select case(arg1)
      case(0)                 !�������� ������
         Event(1)=0
c         do i=1,nu
c           if((IBN(i)).and.(NodBodyType(i).eq.0) then   !���� 
c             if(NodEe.eq.0.d0) then
c               call gas_pt()
c             else
c               call gas_ph()
c             endif
c           endif
c         enddo
c****************************
c �������� ���������������� ������ ��� ������� ����� ����, ���� � ���� /�����/
c****************************
c        call partpre_initab
c****************************
      case(2)                 !����� ����� ��������� ������
c****************************
c ���������� ������, ������� ����������������� ��������� ������� /�����/
c****************************
c        call partpre_killtab
c****************************
      case(3)                 !����� ������
         Event(1)=3
c        is_error=FirstStepErrFlag
c         if(is_error.ne.0) then
c          write(*,*) 'ERROR! ������ � �������� ������. ���������'
c     * //' ������ ��������� ���� � ����������� ������!'
c          stop
c         end if
      case(4)                 !������� ������

      case(5)                 !�������� ���������
         Event(1)=5
         PropsKey(1) = 0
         FirstStepErrFlag=0
         TnkNormFlag=.true.
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 3588 
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 3660 
         call kts_hconnIn
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 3665 
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 3686 
      end select


* ���������� �������������� ���������� ������

      if(arg1.eq.5) then
         if(not_dic_state.eq.0) then ! 
            call kts_h_OnLoadDIC
         else if(not_cvt_state.eq.0) then
            call kts_h_OnLoadCvt
         else
            call kts_h_OnLoadNoncvt
         endif
c         call kts_h_OnLoadAny
         not_cvt_state=1
         not_dic_state=1
      endif

      end

*================================================
*-----------------------------------
      subroutine kts_h_OnLoadDIC
      implicit none
      include 'kts_h.fh'
      REAL*8 FlwTnSqFrom(894)
      COMMON/kts_h_nowrt/ FlwTnSqFrom
      REAL*8 FlwHTnFromUp(894)
      COMMON/kts_h_nowrt/ FlwHTnFromUp
      REAL*8 FlwTnSqTo(894)
      COMMON/kts_h_nowrt/ FlwTnSqTo
      REAL*8 FlwHTnFromDo(894)
      COMMON/kts_h_nowrt/ FlwHTnFromDo
      REAL*8 FlwHTnToUp(894)
      COMMON/kts_h_nowrt/ FlwHTnToUp
      REAL*8 FlwHTnToDo(894)
      COMMON/kts_h_nowrt/ FlwHTnToDo
      INTEGER*4 Fan2NTN2N
      PARAMETER (Fan2NTN2N=0)
      INTEGER*4 Fan2NTN0N
      PARAMETER (Fan2NTN0N=0)
      INTEGER*4 Fan2NTauNN
      PARAMETER (Fan2NTauNN=0)
      INTEGER*4 Fan2NdPG5N
      PARAMETER (Fan2NdPG5N=0)
      INTEGER*4 Fan2NdPG4N
      PARAMETER (Fan2NdPG4N=0)
      INTEGER*4 Fan2NdPG3N
      PARAMETER (Fan2NdPG3N=0)
      INTEGER*4 Fan2NdPG2N
      PARAMETER (Fan2NdPG2N=0)
      INTEGER*4 Fan2NdPG0N
      PARAMETER (Fan2NdPG0N=0)
      INTEGER*4 Fan2TauNN
      PARAMETER (Fan2TauNN=0)
      INTEGER*4 FanDim_TauNmax
      PARAMETER (FanDim_TauNmax=0)
      INTEGER*4 FanDim_Taumax
      PARAMETER (FanDim_Taumax=0)
      INTEGER*4 FanDim_KMax
      PARAMETER (FanDim_KMax=0)
      INTEGER*4 FanDim_PMax
      PARAMETER (FanDim_PMax=0)
      INTEGER*4 Tn8Ne
      PARAMETER (Tn8Ne=2)
      INTEGER*4 Tn3Np
      PARAMETER (Tn3Np=2)
      INTEGER*4 Tn3Ne
      PARAMETER (Tn3Ne=4)
      INTEGER*4 INVPERe(551)
      COMMON/kts_h_nowrt/ INVPERe
      INTEGER*4 NZSUBe(4335)
      COMMON/kts_h_nowrt/ NZSUBe
      INTEGER*4 INZSUBe(552)
      COMMON/kts_h_nowrt/ INZSUBe
      INTEGER*4 ILNZe(552)
      COMMON/kts_h_nowrt/ ILNZe
      INTEGER*4 INVPERp(551)
      COMMON/kts_h_nowrt/ INVPERp
      INTEGER*4 NZSUBp(4335)
      COMMON/kts_h_nowrt/ NZSUBp
      INTEGER*4 INZSUBp(552)
      COMMON/kts_h_nowrt/ INZSUBp
      INTEGER*4 ILNZp(552)
      COMMON/kts_h_nowrt/ ILNZp
      INTEGER*4 Nthermo
      PARAMETER (Nthermo=894)
      INTEGER*4 Nhydro
      PARAMETER (Nhydro=894)
      INTEGER*4 Nent
      PARAMETER (Nent=551)
      INTEGER*4 Npc
      PARAMETER (Npc=1102)
      INTEGER*4 Npres
      PARAMETER (Npres=551)
      INTEGER*4 Nf
      PARAMETER (Nf=6)
      INTEGER*4 Tn7Ne
      PARAMETER (Tn7Ne=1)
      INTEGER*4 Tn7Np
      PARAMETER (Tn7Np=1)
      INTEGER*4 Tn9Ne
      PARAMETER (Tn9Ne=6)
      INTEGER*4 Tn9Np
      PARAMETER (Tn9Np=2)
      INTEGER*4 Fan2NTN3N
      PARAMETER (Fan2NTN3N=0)
      INTEGER*4 Fan2NTN4N
      PARAMETER (Fan2NTN4N=0)
      INTEGER*4 Fan2NTN5N
      PARAMETER (Fan2NTN5N=0)
      INTEGER*4 Tr1Dim_Nu
      PARAMETER (Tr1Dim_Nu=-111)
      INTEGER*4 Tr2Dim_Ksi
      PARAMETER (Tr2Dim_Ksi=-111)
      INTEGER*4 Tn8Np
      PARAMETER (Tn8Np=1)
      LOGICAL*1 PIntIter(551)
      COMMON/kts_h_nowrt/ PIntIter
      LOGICAL*1 EIntIter(551)
      COMMON/kts_h_nowrt/ EIntIter
      LOGICAL*1 EextIter(551)
      COMMON/kts_h_nowrt/ EextIter
      LOGICAL*1 PExtIter(551)
      COMMON/kts_h_nowrt/ PExtIter
      !DEC$PSECT/kts_h_NOWRT/ NoWRT

        flag_start(1) = 2

      return
      end
*-----------------------------------
      subroutine kts_h_OnLoadCvt
      implicit none
      include 'kts_h.fh'
      REAL*8 FlwTnSqFrom(894)
      COMMON/kts_h_nowrt/ FlwTnSqFrom
      REAL*8 FlwHTnFromUp(894)
      COMMON/kts_h_nowrt/ FlwHTnFromUp
      REAL*8 FlwTnSqTo(894)
      COMMON/kts_h_nowrt/ FlwTnSqTo
      REAL*8 FlwHTnFromDo(894)
      COMMON/kts_h_nowrt/ FlwHTnFromDo
      REAL*8 FlwHTnToUp(894)
      COMMON/kts_h_nowrt/ FlwHTnToUp
      REAL*8 FlwHTnToDo(894)
      COMMON/kts_h_nowrt/ FlwHTnToDo
      INTEGER*4 Fan2NTN2N
      PARAMETER (Fan2NTN2N=0)
      INTEGER*4 Fan2NTN0N
      PARAMETER (Fan2NTN0N=0)
      INTEGER*4 Fan2NTauNN
      PARAMETER (Fan2NTauNN=0)
      INTEGER*4 Fan2NdPG5N
      PARAMETER (Fan2NdPG5N=0)
      INTEGER*4 Fan2NdPG4N
      PARAMETER (Fan2NdPG4N=0)
      INTEGER*4 Fan2NdPG3N
      PARAMETER (Fan2NdPG3N=0)
      INTEGER*4 Fan2NdPG2N
      PARAMETER (Fan2NdPG2N=0)
      INTEGER*4 Fan2NdPG0N
      PARAMETER (Fan2NdPG0N=0)
      INTEGER*4 Fan2TauNN
      PARAMETER (Fan2TauNN=0)
      INTEGER*4 FanDim_TauNmax
      PARAMETER (FanDim_TauNmax=0)
      INTEGER*4 FanDim_Taumax
      PARAMETER (FanDim_Taumax=0)
      INTEGER*4 FanDim_KMax
      PARAMETER (FanDim_KMax=0)
      INTEGER*4 FanDim_PMax
      PARAMETER (FanDim_PMax=0)
      INTEGER*4 Tn8Ne
      PARAMETER (Tn8Ne=2)
      INTEGER*4 Tn3Np
      PARAMETER (Tn3Np=2)
      INTEGER*4 Tn3Ne
      PARAMETER (Tn3Ne=4)
      INTEGER*4 INVPERe(551)
      COMMON/kts_h_nowrt/ INVPERe
      INTEGER*4 NZSUBe(4335)
      COMMON/kts_h_nowrt/ NZSUBe
      INTEGER*4 INZSUBe(552)
      COMMON/kts_h_nowrt/ INZSUBe
      INTEGER*4 ILNZe(552)
      COMMON/kts_h_nowrt/ ILNZe
      INTEGER*4 INVPERp(551)
      COMMON/kts_h_nowrt/ INVPERp
      INTEGER*4 NZSUBp(4335)
      COMMON/kts_h_nowrt/ NZSUBp
      INTEGER*4 INZSUBp(552)
      COMMON/kts_h_nowrt/ INZSUBp
      INTEGER*4 ILNZp(552)
      COMMON/kts_h_nowrt/ ILNZp
      INTEGER*4 Nthermo
      PARAMETER (Nthermo=894)
      INTEGER*4 Nhydro
      PARAMETER (Nhydro=894)
      INTEGER*4 Nent
      PARAMETER (Nent=551)
      INTEGER*4 Npc
      PARAMETER (Npc=1102)
      INTEGER*4 Npres
      PARAMETER (Npres=551)
      INTEGER*4 Nf
      PARAMETER (Nf=6)
      INTEGER*4 Tn7Ne
      PARAMETER (Tn7Ne=1)
      INTEGER*4 Tn7Np
      PARAMETER (Tn7Np=1)
      INTEGER*4 Tn9Ne
      PARAMETER (Tn9Ne=6)
      INTEGER*4 Tn9Np
      PARAMETER (Tn9Np=2)
      INTEGER*4 Fan2NTN3N
      PARAMETER (Fan2NTN3N=0)
      INTEGER*4 Fan2NTN4N
      PARAMETER (Fan2NTN4N=0)
      INTEGER*4 Fan2NTN5N
      PARAMETER (Fan2NTN5N=0)
      INTEGER*4 Tr1Dim_Nu
      PARAMETER (Tr1Dim_Nu=-111)
      INTEGER*4 Tr2Dim_Ksi
      PARAMETER (Tr2Dim_Ksi=-111)
      INTEGER*4 Tn8Np
      PARAMETER (Tn8Np=1)
      LOGICAL*1 PIntIter(551)
      COMMON/kts_h_nowrt/ PIntIter
      LOGICAL*1 EIntIter(551)
      COMMON/kts_h_nowrt/ EIntIter
      LOGICAL*1 EextIter(551)
      COMMON/kts_h_nowrt/ EextIter
      LOGICAL*1 PExtIter(551)
      COMMON/kts_h_nowrt/ PExtIter
      !DEC$PSECT/kts_h_NOWRT/ NoWRT

c        flag_start(1) = 1

      return
      end
*-----------------------------------
      subroutine kts_h_OnLoadNoncvt
      implicit none
      include 'kts_h.fh'
      REAL*8 FlwTnSqFrom(894)
      COMMON/kts_h_nowrt/ FlwTnSqFrom
      REAL*8 FlwHTnFromUp(894)
      COMMON/kts_h_nowrt/ FlwHTnFromUp
      REAL*8 FlwTnSqTo(894)
      COMMON/kts_h_nowrt/ FlwTnSqTo
      REAL*8 FlwHTnFromDo(894)
      COMMON/kts_h_nowrt/ FlwHTnFromDo
      REAL*8 FlwHTnToUp(894)
      COMMON/kts_h_nowrt/ FlwHTnToUp
      REAL*8 FlwHTnToDo(894)
      COMMON/kts_h_nowrt/ FlwHTnToDo
      INTEGER*4 Fan2NTN2N
      PARAMETER (Fan2NTN2N=0)
      INTEGER*4 Fan2NTN0N
      PARAMETER (Fan2NTN0N=0)
      INTEGER*4 Fan2NTauNN
      PARAMETER (Fan2NTauNN=0)
      INTEGER*4 Fan2NdPG5N
      PARAMETER (Fan2NdPG5N=0)
      INTEGER*4 Fan2NdPG4N
      PARAMETER (Fan2NdPG4N=0)
      INTEGER*4 Fan2NdPG3N
      PARAMETER (Fan2NdPG3N=0)
      INTEGER*4 Fan2NdPG2N
      PARAMETER (Fan2NdPG2N=0)
      INTEGER*4 Fan2NdPG0N
      PARAMETER (Fan2NdPG0N=0)
      INTEGER*4 Fan2TauNN
      PARAMETER (Fan2TauNN=0)
      INTEGER*4 FanDim_TauNmax
      PARAMETER (FanDim_TauNmax=0)
      INTEGER*4 FanDim_Taumax
      PARAMETER (FanDim_Taumax=0)
      INTEGER*4 FanDim_KMax
      PARAMETER (FanDim_KMax=0)
      INTEGER*4 FanDim_PMax
      PARAMETER (FanDim_PMax=0)
      INTEGER*4 Tn8Ne
      PARAMETER (Tn8Ne=2)
      INTEGER*4 Tn3Np
      PARAMETER (Tn3Np=2)
      INTEGER*4 Tn3Ne
      PARAMETER (Tn3Ne=4)
      INTEGER*4 INVPERe(551)
      COMMON/kts_h_nowrt/ INVPERe
      INTEGER*4 NZSUBe(4335)
      COMMON/kts_h_nowrt/ NZSUBe
      INTEGER*4 INZSUBe(552)
      COMMON/kts_h_nowrt/ INZSUBe
      INTEGER*4 ILNZe(552)
      COMMON/kts_h_nowrt/ ILNZe
      INTEGER*4 INVPERp(551)
      COMMON/kts_h_nowrt/ INVPERp
      INTEGER*4 NZSUBp(4335)
      COMMON/kts_h_nowrt/ NZSUBp
      INTEGER*4 INZSUBp(552)
      COMMON/kts_h_nowrt/ INZSUBp
      INTEGER*4 ILNZp(552)
      COMMON/kts_h_nowrt/ ILNZp
      INTEGER*4 Nthermo
      PARAMETER (Nthermo=894)
      INTEGER*4 Nhydro
      PARAMETER (Nhydro=894)
      INTEGER*4 Nent
      PARAMETER (Nent=551)
      INTEGER*4 Npc
      PARAMETER (Npc=1102)
      INTEGER*4 Npres
      PARAMETER (Npres=551)
      INTEGER*4 Nf
      PARAMETER (Nf=6)
      INTEGER*4 Tn7Ne
      PARAMETER (Tn7Ne=1)
      INTEGER*4 Tn7Np
      PARAMETER (Tn7Np=1)
      INTEGER*4 Tn9Ne
      PARAMETER (Tn9Ne=6)
      INTEGER*4 Tn9Np
      PARAMETER (Tn9Np=2)
      INTEGER*4 Fan2NTN3N
      PARAMETER (Fan2NTN3N=0)
      INTEGER*4 Fan2NTN4N
      PARAMETER (Fan2NTN4N=0)
      INTEGER*4 Fan2NTN5N
      PARAMETER (Fan2NTN5N=0)
      INTEGER*4 Tr1Dim_Nu
      PARAMETER (Tr1Dim_Nu=-111)
      INTEGER*4 Tr2Dim_Ksi
      PARAMETER (Tr2Dim_Ksi=-111)
      INTEGER*4 Tn8Np
      PARAMETER (Tn8Np=1)
      LOGICAL*1 PIntIter(551)
      COMMON/kts_h_nowrt/ PIntIter
      LOGICAL*1 EIntIter(551)
      COMMON/kts_h_nowrt/ EIntIter
      LOGICAL*1 EextIter(551)
      COMMON/kts_h_nowrt/ EextIter
      LOGICAL*1 PExtIter(551)
      COMMON/kts_h_nowrt/ PExtIter
      !DEC$PSECT/kts_h_NOWRT/ NoWRT

c        flag_start(1) = 0

      return
      end
****** End   file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Look.fs'
****** Begin file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mqnew.fs'
      subroutine kts_hMQNEW(dtInt1)
      IMPLICIT NONE
      include 'kts_h.fh'
      REAL*8 FlwTnSqFrom(894)
      COMMON/kts_h_nowrt/ FlwTnSqFrom
      REAL*8 FlwHTnFromUp(894)
      COMMON/kts_h_nowrt/ FlwHTnFromUp
      REAL*8 FlwTnSqTo(894)
      COMMON/kts_h_nowrt/ FlwTnSqTo
      REAL*8 FlwHTnFromDo(894)
      COMMON/kts_h_nowrt/ FlwHTnFromDo
      REAL*8 FlwHTnToUp(894)
      COMMON/kts_h_nowrt/ FlwHTnToUp
      REAL*8 FlwHTnToDo(894)
      COMMON/kts_h_nowrt/ FlwHTnToDo
      INTEGER*4 Fan2NTN2N
      PARAMETER (Fan2NTN2N=0)
      INTEGER*4 Fan2NTN0N
      PARAMETER (Fan2NTN0N=0)
      INTEGER*4 Fan2NTauNN
      PARAMETER (Fan2NTauNN=0)
      INTEGER*4 Fan2NdPG5N
      PARAMETER (Fan2NdPG5N=0)
      INTEGER*4 Fan2NdPG4N
      PARAMETER (Fan2NdPG4N=0)
      INTEGER*4 Fan2NdPG3N
      PARAMETER (Fan2NdPG3N=0)
      INTEGER*4 Fan2NdPG2N
      PARAMETER (Fan2NdPG2N=0)
      INTEGER*4 Fan2NdPG0N
      PARAMETER (Fan2NdPG0N=0)
      INTEGER*4 Fan2TauNN
      PARAMETER (Fan2TauNN=0)
      INTEGER*4 FanDim_TauNmax
      PARAMETER (FanDim_TauNmax=0)
      INTEGER*4 FanDim_Taumax
      PARAMETER (FanDim_Taumax=0)
      INTEGER*4 FanDim_KMax
      PARAMETER (FanDim_KMax=0)
      INTEGER*4 FanDim_PMax
      PARAMETER (FanDim_PMax=0)
      INTEGER*4 Tn8Ne
      PARAMETER (Tn8Ne=2)
      INTEGER*4 Tn3Np
      PARAMETER (Tn3Np=2)
      INTEGER*4 Tn3Ne
      PARAMETER (Tn3Ne=4)
      INTEGER*4 INVPERe(551)
      COMMON/kts_h_nowrt/ INVPERe
      INTEGER*4 NZSUBe(4335)
      COMMON/kts_h_nowrt/ NZSUBe
      INTEGER*4 INZSUBe(552)
      COMMON/kts_h_nowrt/ INZSUBe
      INTEGER*4 ILNZe(552)
      COMMON/kts_h_nowrt/ ILNZe
      INTEGER*4 INVPERp(551)
      COMMON/kts_h_nowrt/ INVPERp
      INTEGER*4 NZSUBp(4335)
      COMMON/kts_h_nowrt/ NZSUBp
      INTEGER*4 INZSUBp(552)
      COMMON/kts_h_nowrt/ INZSUBp
      INTEGER*4 ILNZp(552)
      COMMON/kts_h_nowrt/ ILNZp
      INTEGER*4 Nthermo
      PARAMETER (Nthermo=894)
      INTEGER*4 Nhydro
      PARAMETER (Nhydro=894)
      INTEGER*4 Nent
      PARAMETER (Nent=551)
      INTEGER*4 Npc
      PARAMETER (Npc=1102)
      INTEGER*4 Npres
      PARAMETER (Npres=551)
      INTEGER*4 Nf
      PARAMETER (Nf=6)
      INTEGER*4 Tn7Ne
      PARAMETER (Tn7Ne=1)
      INTEGER*4 Tn7Np
      PARAMETER (Tn7Np=1)
      INTEGER*4 Tn9Ne
      PARAMETER (Tn9Ne=6)
      INTEGER*4 Tn9Np
      PARAMETER (Tn9Np=2)
      INTEGER*4 Fan2NTN3N
      PARAMETER (Fan2NTN3N=0)
      INTEGER*4 Fan2NTN4N
      PARAMETER (Fan2NTN4N=0)
      INTEGER*4 Fan2NTN5N
      PARAMETER (Fan2NTN5N=0)
      INTEGER*4 Tr1Dim_Nu
      PARAMETER (Tr1Dim_Nu=-111)
      INTEGER*4 Tr2Dim_Ksi
      PARAMETER (Tr2Dim_Ksi=-111)
      INTEGER*4 Tn8Np
      PARAMETER (Tn8Np=1)
      LOGICAL*1 PIntIter(551)
      COMMON/kts_h_nowrt/ PIntIter
      LOGICAL*1 EIntIter(551)
      COMMON/kts_h_nowrt/ EIntIter
      LOGICAL*1 EextIter(551)
      COMMON/kts_h_nowrt/ EextIter
      LOGICAL*1 PExtIter(551)
      COMMON/kts_h_nowrt/ PExtIter
      !DEC$PSECT/kts_h_NOWRT/ NoWRT

****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
      integer*4 Ncomp,CompLin(1),CompOin(1),CompGenTyp(1)
      parameter(Ncomp=0)
      real*8 CompQq(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 3748 
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 3753 


****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'

c      real*8 FlwCondRegul(nv)
c      common/kts_hFlwL/ FlwCondRegul
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 3764 

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 3799 
      integer*4 nfwv
      parameter(nfwv=0)
      integer*4 FwvFloWw(1),FwvFloWv(1)
      real*8 FwvRoFrom1(1),FwvRoFrom2(1),FwvRoTo1(1),FwvRoTo2(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 3805 
      integer*4 nfwv1
      parameter(nfwv1=0)
      integer*4 Fwv1Flow(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 3810 
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
      integer NHe2
      parameter(NHe2=0)
* ���������� ��� ��������������� ��������� ����������
      real*8 C0He2(1) !����. ��� ������� ����������� ���� ��
      real*8 C1He2(1) !����. ��� ������� ����������� ���� ��
      real*8 C2He2(1) !����. ��� ������� ����������� ���� ��
      real*8 DTHe2
      real*8 C0He2b1(1),C1He2b1(1),C2He2b1(1)
      real*8 C0He2b2(1),C1He2b2(1),C2He2b2(1)
      real*8 He2T1t(2,1),He2T2t(2,1),He2decQt(1),He2_gar2(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 3826 
C     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 3840 
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'

      real*8 Mtl_QqOut(NMtl)  !��������� �������� ����� � ������� ��������
      real*8 MtlTt_t(NMtl)
      real*8 MtlAlCulc(nMtl)
      common/kts_hMetal/ Mtl_QqOut,MtlTt_t,MtlAlCulc
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 3851 

C     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 3858 

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 3866 

      integer*4 NMtlO,MtlOIndRoom(1)
      parameter ( NMtlO=0 )
      real*8 MtlOTt(1),MtlOMass(1),MtlOCp(1),MtlOAlCulcWl1(1)
      real*8 MtlO_QqOut(1)  !��������� �������� ����� � ������� ��������
      real*8 MtlOLamdC(1),MtlOLamd(1),MtlOQqEnv(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 3874 
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'

      integer*4 NUCH
      parameter (NUCH=0)
      integer*4 CoalHConNodInd(1)
      real*8 CoalHNodMoistG(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 3883 

      integer*4 NFlwWasteSet
      parameter(NFlwWasteSet=0)
      integer*4 FlwWasteSetConnInd(1),FlwWasteSetNum(1)
      real*8 FlwWasteSetWaste(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 3890 

      integer*4 FlwConnFlwInd(1),NFlwConn
      parameter(NFlwConn=0)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 3895 

C     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 3900 

      integer*4 NHCO
      parameter(NHCO=0)
      integer*4  HCOFlMod(1),HCOIndFlw(1)
      real*8 HCOCfMod(1),HCOOFg(1),HCOONodVol(1),HCOONoddRodP(1)
      common/kts_hConFlOut/ HCOFlMod,HCOCfMod,HCOOFg,HCOONodVol,
     >HCOONoddRodP,HCOIndFlw
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 3908 

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 3915 

      integer*4 nCrv
      parameter(nCrv=0)
      real*8 CrvRes(1)
      integer*4 CrvFlw(nCrv)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 3922 

      integer*4 nWst
      parameter(nWst=0)
      integer*4 WstNode(1)
      real*8    WSTConsN2(1),WSTConsO2(1),WSTConsH2O(1),
     *          WSTConsCO2(1),WSTConsSoot(1),WSTConsAsh(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 3930 

      integer*4 NXS
      parameter(NXS=0)
      integer*4 XSFlagSetCc(1),XSFlSet(30,1),XSIndSetCc(1),
     >XSIndSetCc0(1)
      real*8    XSCcstr(30,1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 3937 

      integer*4 ntFrB
      parameter(ntFrB=0)
      integer*4 tFrBNdInd(1),tFrBi(1)
      real*8    tFrBCcOld(100,1),tFrBTau(1)
      real*8    tFrBGIn(1),tFrBAa(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 3945 

      integer*4 np4
      parameter(np4=0)
      integer*4 Pm4Nb(1)
      real*8 Pm4Res(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 3952 

c ��������� �����. ��� ������� � ���������������, ���� �� (�������) ���
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 3960 
      integer*4 np2
      parameter(np2=0)
      integer*4 Pm2Nb(1)
      real*8 Pm2Res(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 3966 
      integer*4 nFan
      parameter(nFan=0)
      integer*4 FanFlw(1)
      real*8 FanRes(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 3972 
      integer*4 nFan2
      parameter(nFan2=0)
      integer*4 Fan2Flw(1)
      real*8 Fan2Res(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 3978 
      integer*4 np3
      parameter(np3=0)
      real*8 Pm3QqNod(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 3983 
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 3993 

      real*8 time_df1  !������� ��� ������� ������� (� �����)
      real*8 time_tmp

      real   deltat      !��� ����������
      real   deltat1      !��� ����������
      REAL*8 Atau        !1/dt
c      save Atau
      real*4 dtInt       !dt*Accel
      real*8 TimeSrv     !dt/Niter
      character*100 fn1  ! ��� ������������� .cbf ������
c      common/kts_hLocal1/ time_tmp,deltat,Atau,dtInt,TimeSrv,fn1
      common/kts_hLocal1/ Atau,time_tmp,deltat1,dtInt,TimeSrv,fn1
      real*8 VOL1(nu) !V/dt
      real*8 NodCfdE(nu) !����. � ��. ����. ��� ����� ���������
      real*8 NodTtk(nu)  !����������� ��������. � ������� ����� �� k-�� ��������
      real*8 RotVol(nu) !��������� ������
      real*8 RokVol(nu) !��������� ������
      real*8 NodVolk(nu)  !������������ ����� �� k-�� ��������
      real*8 NoddVdPk(nu) !����������� ������ � ���� �� �������� �� k-�� ����.

c ---  ������ �. (29.08.02) - ������
      real*8 NodSum1(nu)
      real*8 NodSum2(nu)
c ---  ������ �. (29.08.02) - �����

      real*8 NodQqPmAd(nu)

      common/kts_hNODE1/ VOL1,NodCfdE,NodTtk,RotVol,RokVol,
     * NodVolk,NoddVdPk,NodSum1,NodSum2,NodQqPmAd
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 4025 

      real*8 C7(nv)   !��������� ������������
      real*8 Gk(nv)   !������� �� k-�� ��������
      real*8 dHLev(nv)!������ �� ����� �� ���� ������� � �������
      real*8 Rovk(nv)    !��. ��������� �� k-�� ��������
      real*8 AconRovk(nv)!��������� ��� ��������
c###### ��������� ####################################
      real*8 FlwGaVeSq(nv)  ! ��������� ������ ��� �������� ������������� �����
      real*8 flwbcon(nv)    ! �������� ��������� �������� �������� �� ����� 
      real*8 FlwC7work1(nv)   ! ��������� ������ ��� �������� ������������� �����
      real*8 FlwC7work2(nv)   ! ��������� ������ ��� �������� ������������� �����
      real*8 FlwC7work3(nv)   ! ��������� ������ ��� �������� ������������� �����
      real*8 FlwC7work4(nv)   ! ��������� ������ ��� �������� ������������� �����
      real*8 FlwC7work5(nv)   ! ��������� ������ ��� �������� ������������� �����
      real*8 FlwC7work7(nv)
      real*8 FlwVsk    (nv)   ! �������� �� ����� �� ���������
      real*8 ComEeFrom  (nv)   ! ��������� ��������� ��������� � ��������
      real*8 ComEeTo    (nv)   ! ��������� ��������� ��������� � ��������  
      real*8 FlwSlGaFrom(nv)   ! �������� ��� ��������������� ���� �� �����
      real*8 FlwSlGaFrom0(nv)  ! �������� ��� ��������������� ���� �� �����
      real*8 FlwSlGaTo   (nv)  ! �������� ��� ��������������� ���� �� �����
      real*8 FlwSlGaTo0  (nv)  ! �������� ��� ��������������� ���� �� �����
      real*8 FlwCcFrom(nv) ! ��������� ������ ��� ������������ ���� �� ������ �����
      real*8 FlwCcTo(nv)   ! ��������� ������ ��� ������������ ���� �� ������ �����
      real*8 flwdhz0(nv)
      real*8 ComRoFrom(3,nv)
      real*8 ComRoTo  (3,nv)
c###### ��������� ####################################
      real*8 FlwKgp(nf,2,nv)    ! 
      real*8 FlwKge(nf,nf,nv)
      real*8 FlwKgg(3,nv)
      real*8 FlwKgpt(2,nv)
      real*8 FlwdPLevFrom(nv),FlwdPLevTo(nv)
      real*8 FlwCcGgWtJa(nv)
      real*8 FlwRes_Iter(nv)
      real*8 FlwGG_TMP(nv)
      real*8 FlwRes_Tmp
      real*8 FlwdCgDCcFrom(nv),FlwdCgDCcTo(nv)
c      real*8 frco0(nv)
      common/kts_hFLOW/ C7,Gk,dHLev,Rovk,AconRovk,FlwGaVeSq,
     * flwbcon,FlwC7work1,
     * FlwC7work2,FlwC7work3,FlwC7work4,FlwC7work5,FlwC7work7,
     * FlwVsk,ComEeFrom,ComEeTo,FlwSlGaFrom,FlwSlGaFrom0,
     * FlwSlGaTo,FlwSlGaTo0,FlwCcFrom,FlwCcTo,flwdhz0,ComRoFrom,
     * ComRoTo,FlwKgp,FlwKge,FlwKgg,FlwKgpt,
     * FlwdPLevFrom,FlwdPLevTo,FlwCcGgWtJa,
     * FlwRes_Iter,FlwRes_Tmp,FlwGG_TMP,FlwdCgDCcFrom,FlwdCgDCcTo
c    * ,frco0
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 4075 

C     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 4080 

C     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 4085 


c      !������� ����. ����� ����� � ���. ��������� ��� ��������
       real*8 Dmp(Npres,Npres)
C       real*8 Dmpc(Npc,Npc) ! �������� 
c      !������� ����. ����� ����� � ������� ��������� ��� ���������
       real*8 Dmh(Nent,Nent)
       real*8 DmhG(Nent,Nent)
c      !������ ����� ��� ���������� ������� �� ����.
      Real*8 Yp(Npres)
c      Real*8 Ypc(Npc)
c      !������ ����� ��� ���������� ������� �� ���.
      real*8 Ye(Nent)
      real*8 YeG(Nent)
c      !������� �� ���������
      real*8 dP(Npres)
c      real*8 dPC(Npc)
c      !������� �� ����������
      real*8 dE(Nent)
      real*8 dEG(Nent)
c      !������ ������ �������� �� k-�� ��������
      real*8 Pk(Npres)
c      !������ ������ �������� �� (k+1)-�� ��������
      real*8 Pk1(Npres)
c      !������ ������ ��������� �� k-�� ��������
      real*8 Ek(Nent)
      real*8 EGk(Nent)
c      !������ ������ ��������� �� (k+1)-�� ��������
      real*8 Ek1(Nent)
      real*8 EGk1(Nent)
      real*8 Rok(Nu) !�����. �\� ����� � ������� ����� �� k-�� ����.
      !������ ������ ��������� �����. ����
      real*8 EeWtSatk(Nent)
      !������ ������ ��������� �����. ����
      real*8 EeVpSatk(Nent)
      real*8 MaxDevP     !����. �����. ����. �������� �� ��������
      real*8 MaxDevH     !����. �����. ����. ��������� �� ��������
      INTEGER*4 I,j     !���������� �����
      integer*4 IC        !������� ��������
      integer*4 ICInt !������� ���������� ��������
      common/kts_hLocal2/ Dmp,Dmh,Yp,Ye,dP,dE,Pk,Pk1,Ek,Ek1,Rok,
     *  EeWtSatk,EeVpSatk,MaxDevP,MaxDevH,IC,ICInt,DmhG,YeG,dEG,
     *  EGk,EGk1       !,dPC,Ypc,Dmpc <=������ ��������!!!!

C     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 4133 

      INTEGER*4 nvlgpos !����� ����� � �������������� ���������� � ����������
      common/kts_hVlvGeo/ nvlgpos
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 4138 

C     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 4143 

C     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 4148 

C     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 4153 

C     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 4158 

      integer*4 nsep1,Sep1FlwS(1),Sep1Node(1)
        parameter(nsep1=0)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 4163 

      integer*4 nInWst,InWstNum(1),InWstNode(1),InWstNdTp(1)
      real*8    InWstdt(1)
      integer*4 InWstTyp(1)
      real*8    InWstCc(1)
      real*8 InWstExtCon(1),InWstExtWstdt(1)
      parameter(nInWst=0)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 4172 

C     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 4177 


      LOGICAL*1 go,goint  !������� ����������� ��������
      DATA go/.true./     !����������� �������� �������. ��������
      common/kts_hLocal4/ go,goint
      real*8  RDTSC
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 4216 
      real*8 NoddRodPk(nu) !dRo/dP ��� ������� ����� �� ����. ����.
      real*8 NoddRodEk(nu) !dRo/d� ��� ������� ����� �� ����. ����.
      real*8 NoddRodPk1(nu) !dRo/dP ��� ������� ����� �� ���. ����.
      real*8 NoddRodEk1(nu) !dRo/d� ��� ������� ����� �� ���. ����.
      real*8 nodrorelax(nu)
      real*8 NodRoGa0(nu)
      real*8 NodCcGa_Aeq0(nu) 
      real*8 NodCcGa_Deq0(nu) 
      real*8 NodCcGa_Beq (nu)
      real*8 NodCcGa_Beq0(nu)
      real*8 NodCcGa_Aeq (nu)
      real*8 NodCcGa_Deq (nu)
      real*8 NodXxk1(nu)    !���. �������. � ������� ����� �� ���. ����.
      real*8 NodXxBalk1(nu) !���. �������. � ������� ����� �� ���. ����.
      real*8 NodQqAd(nu)
      real*8 NodCfEeAsh(nu)
      real*8 NodEeWvt(nu)
      real*8 NodAlSqGaWv(nu),NodYe(nu),NodDmh(nu)
      real*8 NodPsw(nu),NodRoWvTmp(nu),NoddRdHGas(nu)
     *,NoddRdPWvTmp(nu),NoddRdHWvTmp(nu)
     *,NodTtWvTmp(nu),NodCpWvTmp(nu),NodXxTmp(nu)
      integer*4 Nod_ghm_flag(nu),NodPropRelaxFlag(nu)
      logical*1 EExtIter_(Nent)
      logical*1 PExtIter_(Npres)
      common /kts_hNODE/ NoddRodPk,NoddRodEk,NoddRodPk1,NoddRodEk1,
     *  nodrorelax,NodRoGa0,NodCcGa_Aeq0,NodCcGa_Deq0,NodCcGa_Beq,
     *  NodCcGa_Beq0,NodCcGa_Aeq,NodCcGa_Deq,NodXxk1,NodXxBalk1,
     *  NodQqAd,NodCfEeAsh,NodEeWvt,NodAlSqGaWv,NodYe,NodDmh,
     *  NodPsw,NodRoWvTmp,NoddRdHGas,NodTtWvTmp,NodCpWvTmp,NodXxTmp
      common /kts_hNODE_I/ Nod_ghm_flag,NodPropRelaxFlag
      common /kts_hNODE_L/ EExtIter_,PExtIter_
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 4249 
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'

      integer NTc0
      parameter(NTc0=0)
      integer*4 Tc0IbnFro(1),Tc0NodFro(1),Tc0IbnTo(1),Tc0NodTo(1)
      real*8 Tc0zt(1),Tc0AlG23(1),Tc0AlG43(1)
     *,Tc0G43(1),Tc0G23(1),Tc0G34(1),Tc0CpWl(1)
     *,Tc0TtEnv(1),Tc0TtWlk(1),Tc0TtWl(1),Tc0AlSqEnv(1),Tc0AlEnv(1)
     *,Tc0AlSqPhWl(1,nf),Tc0MasCpWl(1),Tc0CfAlEnv(1)
     *,Tc0CfAlGaWl(1),Tc0CfAlWpUpWl(1)
     *,Tc0CfAlWtWl(1),Tc0CfAlWpDoWl(1)
     *,Tc0EvUpWl(1),Tc0EvDoWl(1)
     *,Tc0dG23dp(1),Tc0dG34dp(1),Tc0dG43dp(1)
     *,Tc0CfGgWpUpWt(1),Tc0CcGgWtWpDo(1),Tc0CfGgWpDoWt(1)
     *,Tc0G32(1),Tc0dG32dp(1),Tc0CfGgWt(1),Tc0SqWl(1),Tc0TettaQ(1,nf)
     *,Tc0QqIsp(1),Tc0AlIsp(1)
     *,Tc0dQIspdP(1),Tc0TtTnIsp(1),Tc0AlSqIsp(1),TC0ResWl(1)
     *,Tc0dQG23dp(1),Tc0TettaQ2(1,nf),Tc0DeltaTs(1)
     *,Tc0AlG23From(1),Tc0AlG43From(1)
     *,Tc0G23From(1),Tc0G34From(1),Tc0G32From(1),Tc0G43From(1)
     *,Tc0dG23dpFrom(1),Tc0dG34dpFrom(1),Tc0dG43dpFrom(1),
     >Tc0dG32dpFrom(1)
     *,Tc0dQIspdPFrom(1),Tc0TtTnIspFrom(1),Tc0AlSqIspFrom(1)
     *,Tc0CcGgWtWpDoFrom(1),Tc0CfGgWpUpWtFrom(1),Tc0CfGgWpDoWtFrom(1)
     *,Tc0QqIspFrom(1),Tc0AlIspFrom(1),Tc0CfGgWtFrom(1),Tc0TettaQFrom(1,
     >nf)
     *,Tc0AlSqPhWlFrom(1,nf),Tc0CfRelAlIsp(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 4279 
C     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 4308 

****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'

      integer NTc2
      parameter(NTc2=0)
      integer*4 Tc2Ibn(1),Tc2Node(1)
      real*8 Tc2zt(1),Tc2QqEnv(1),Tc2AlG23(1),Tc2G23(1),Tc2G34(1)
     *,Tc2AlG43(1),Tc2G43(1),Tc2CpWl(1)
     *,Tc2TtEnv(1),Tc2TtWlk(1),Tc2TtWl(1),Tc2AlSqEnv(1),Tc2AlEnv(1)
     *,Tc2AlSqPhWl(1,nf),Tc2MasCpWl(1),Tc2CfAlEnv(1)
     *,Tc2CfAlGaWl(1),Tc2CfAlWpUpWl(1),Tc2CfAlWtWl(1),Tc2CfAlWpDoWl(1)
     *,Tc2EvUpWl(1),Tc2EvDoWl(1)
     *,Tc2dG23dp(1),Tc2dG34dp(1),Tc2dG43dp(1)
     *,Tc2CfGgWpUpWt(1),Tc2CcGgWtWpDo(1),Tc2CfGgWpDoWt(1)
     *,Tc2G32(1),Tc2dG32dp(1),Tc2CfGgWt(1),Tc2SqWl(1),Tc2TettaQ(1,nf)
     *,Tc2QqIsp(1),Tc2AlIsp(1),Tc2DeltaTs(1)
     *,Tc2dQIspdP(1),Tc2TtTnIsp(1),Tc2AlSqIsp(1),TC2ResWl(1),
     * Tc2LlWl(1),Tc2LaWl(1),Tc2ResWl0(1),Tc2CfRelAlIsp(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 4331 
C     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 4345 

****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'

C     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 4355 

      integer*4 NTCR,TcRNode(1)
      parameter ( NTCR=0 )
      real*8 TcR_XQ(1),TcR_BQ(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 4361 

      integer*4 Nwall2O
      parameter ( Nwall2O=0 )
      real*8  Wl2OTt(6,1),Wl2ORAw(1),Wl2OSqEnv(1),Wl2OQqBout(1),
     &        Wl2ODout(1)

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 4369 

****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 4375 

****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'

      integer NTen
      parameter(NTen=0)
      integer*4 TenIbn(1),TenNode(1)
      real*8 Tenzt(1),TenQqIn(1),TenAlG23(1),TenG23(1),TenG34(1)
     *,TenAlG43(1),TenG43(1),TenCpWl(1),TenTtWlk(1),TenTtWl(1)
     *,TenAlSqPhWl(1,nf),TenMasCpWl(1),TenCfAlGaWl(1),TenCfAlWpUpWl(1)
     *,TenCfAlWtWl(1),TenCfAlWpDoWl(1),TenEvUpWl(1),TenEvDoWl(1)
     *,TendG23dp(1),TendG34dp(1),TendG43dp(1)
     *,TenCfGgWpUpWt(1),TenCcGgWtWpDo(1),TenCfGgWpDoWt(1)
     *,TenG32(1),TendG32dp(1),TenCfGgWt(1),TenSqWl(1),TenTettaQ(1,nf)
     *,TenQqIsp(1),TenAlIsp(1),TenDeltaTs(1)
     *,TendQIspdP(1),TenTtTnIsp(1),TenAlSqIsp(1),TenResWl(1),
     >TenTcwAlCulc(1),TenTcwQq(1)
     *,TenLawL(1),TenQqInt(1),TenQqExt(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 4398 
C     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 4412 
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'

      integer n03
      parameter(n03=0)
      integer*4 Tn3RelP(2,1),Tn3RelE(4,1)
      real*8 Tn3RoWt(1),Tn3Cp(1,1),Tn3Lev(1),
     *       Tn3Ek(1,1),Tn3Rok(1,Tn3Ne),Tn3PpWt(1),Tn3Pk(1,1),Tn3Rot(1,
     >Tn3Ne),
     *       Tn3Xxk(1,Tn3Ne),Tn3MasBor(1),Tn3ccBor(1),Tn3VolWt(1),
     *       Tn3dp(1,Tn3Ne),Tn3MasWt(1),Tn3GIspSum(1),Tn3Pp(1),
     *       Tn3GCondSum(1),Tn3G32sum(1),Tn3G34sum(1),Tn3G23sum(1),
     *       Tn3G43sum(1),Tn3TettaQ(1,Tn3Ne),Tn3Ev_t(1,1),Tn3Dim_t(1),
     *       Tn3Dim_tmax/1/,Tn3EvDo(1),Tn3EvDo0(1),Tn3FlwDdMin(1),
     >Tn3MasWpUp(1),
     *       Tn3MasWpDo(1),Tn3MasGa(1),Tn3LevMas(1),Tn3LevMask(1),
     >Tn3PpGa(1),
     *       Tn3PpWpUp(1),Tn3EeGa(1),Tn3EeWpUp(1),Tn3EeWt(1),
     *       Tn3EeWpDo(1),Tn3LevRelax(1),Tn3VsGa(1),Tn3VsVpUp(1),
     *       Tn3VsWt(1),Tn3VsVpDo(1),Tn3EeWtSatWt(1),Tn3EeWpSatWt(1),
     *       Tn3RoGa(1),Tn3ROWpUp(1),Tn3RoWpDo(1),Tn3LevRel(1),Tn3Hi(1),
     *       Tn3Volk(1,Tn3Ne),Tn3Mask(1,1),Tn3CcGas(1,1),
     *       Tn3CfMasGasMin(1)/1.0d-7/,
     *       Tn3CcUp(1,1),Tn3FlwHUp(1,1),Tn3FlwHDo(1,1),Tn3Ee(1,1),
     *       Tn3TtGa(1),Tn3TtWpUp(1),Tn3TtWt(1),Tn3TtWpDo(1),
     >Tn3MasGaRealk(1),
     *       Tn3GCorr(1,1),Tn3drdp(1,1),Tn3drdh(1,1),Tn3vk(1,1),
     *       Tn3Waste_T(20,1),Tn3Dmint(1),Tn3RoDo(1),Tn3dEInt(1,1),
     *       Tn3WlUpQqOut(1),Tn3WlDoQqOut(1),Tn3TcwAlCulc(1,1),
     *       Tn3TtWlUp(1),Tn3TtWlDo(1),Tn3LaWl(1),
     *       Tn3QqWlEnvUp(1),Tn3QqWlEnvDo(1),Tn3RoomUpInd(1),
     >Tn3RoomDoInd(1),Tn3Tt(1,1),
     *       Tn3CcWpDo(1),Tn3CfGgWpDo_tmp(1),Tn3pmax(1),Tn3TtUp(1),
     >Tn3AlSqPhWl(4,1),Tn3AlWlUpDo(1),
     *       Tn3Levadd(1),Tn3cfdebGg(1),
     *       Tn3PressCorr(1),TN3VOL(1),TN3VOLGA(1),TN3VOLCORRUP(1),
     >TN3VOLCORRDO(1),Tn3FlwIntCoef(1),
     *       Tn3VolVpUpLim(1),Tn3dmintsum(1),Tn3CfGCorr(1),
     >Tn3LevTunG(1),Tn3LevTunPpRegG(6,1)
      integer*4 Tn3FlwNum(1),Tn3CcSetInd(1)
      logical*1 Tn3CcBorLb(1),Tn3VpInFlag(1),Tn3BorFl(1),
     >Tn3EntOut_Key(1),Tn3FlagGCorr(1),
     * Tn3FlagGCorrDn(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 4450 

C     common/kts_hC     common/kts_hC     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 4592 


****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'

      integer*4 n07,Tn7RelP(1,1),Tn7RelE(2,1),Tn7Homo(1),Tn7CcSetInd(1)
      real*8    Tn7RoGa(1),Tn7RoWt(1),Tn7Rok(1,2),Tn7XxWt(1),Tn7Xk(1,2),
     *          Tn7Lev(1),Tn7EeWt(1),Tn7VsGa(1),Tn7VsWt(1),Tn7PpGa(1),
     *          Tn7EeGa(1),Tn7EeWtSat(1),Tn7EeVpSat(1),Tn7LevRel(1),
     *          Tn7Hi(1),Tn7PpWt(1),Tn7TtWt(1),Tn7Ee(2,1),
     *          Tn7Ek(2,1),Tn7CcVolk(1),Tn7CcMask(1)
     *         ,Tn7XxBalWt(1),Tn7Levk(1),Tn7Lev_Rlx(1),Tn7Acon(1),
     *          Tn7Sq(1),Tn7CcBor(1),Tn7MasWt(1),Tn7MasGa(1),
     *          Tn7MasBor(1),Tn7GCorrWt(1),
     *          Tn7dRdPk(2,1),Tn7dRdP(2,1),Tn7dRdHk(2,1),Tn7dRdH(2,1),
     *          dP_Tn7(1),Tn7Dcon(1),Tn7LsMin(1),Tn7dMLoc(2,1),
     *          Tn7CfRoGaCorr(1),Tn7CfRoWtCorr(1),
     *          Tn7Waste_T(20,1),Tn7Rowttrue(1),Tn7RoWtSat(1),
     *          Tn7RoVpSat(1),Tn7Dmint(1),Tn7Gcorr(1),
     *          Tn7WlUpQqOut(1),Tn7WlDoQqOut(1),Tn7TcwAlCulc(1,1),
     *          Tn7TtWlUp(1),Tn7TtWlDo(1),Tn7LaWl(1),Tn7QqWlEnv(1),
     >Tn7RoomInd(1)
     *          ,Tn7EvDo(1),Tn7EvDo0(1),Tn7TtGa(1),Tn7AlSqPhWl(2,1),
     >Tn7AlWlUpDo(1)
     *          ,Tn7dMIntGa(1),Tn7dMIntWt(1),Tn7GCorrGa(1),
     >Tn7LevTunG(1),Tn7LevTunPpRegG(1)
      logical*1 Tn7_Flag_Mas_Deb(1),Tn7CfCorr_On(1)


      logical*1 Tn7CcBorLb(1)

      parameter(n07=0)
      real*8 Tn7Pk(2,1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 4626 

C     common/kts_hC     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 4683 

****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
      integer n08
      parameter(n08=0)
      integer*4 Tn8RelP(1,0),Tn8RelE(2,0),Tn8CcSetInd(1)
      real*8 Tn8RoWt(1),Tn8RoGa(1),Tn8GSumWt1(1),Tn8GSumWt2(1),
     *       Tn8Lev(1),Tn8RoWtk(1),Tn8PpWt(1),Tn8MasBor(1),Tn8ccBor(1),
     *       Tn8VolWt(1),Tn8XxWt(1),Tn8XxBalWt(1),
     *       Tn8MasWt(1),Tn8MasWtk(1),
     *       Tn8RoGak(1),Tn8Gsliv(1),Tn8EeGa(1),
     *       Tn8EeWt(1),Tn8PpGa(1),Tn8VsWt(1),Tn8VsGa(1),Tn8VsVp(1),
     *       Tn8EeWtSat(1),Tn8EeVpSat(1),Tn8LevRel(1),Tn8Hi(1),
     *       Tn8TtGa(1),Tn8TtWt(1),Tn8GgXx(1),
     *       Tn8dRdP1(1),Tn8dRdH1(1),Tn8dRdP2(1),Tn8dRdH2(1),
     *       Tn8Waste_T(20,1),Tn8RowtSat(1),Tn8RovpSat(1),
     *       Tn8QqEnv(1),Tn8RoomInd(1),Tn8TtMeDo(1)
     *       ,Tn8EvDo(1),Tn8EvDo0(1),Tn8GInOut(1),Tn8LevTunG(1)

      logical*1 Tn8CcBorLb(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 4707 

C     common/kts_hC     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 4744 

****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'

      integer n09
      parameter(n09=0)
      integer*4 Tn9RelP(2,1),Tn9RelE(Tn9Ne,1),Tn9CcSetInd(1)
      real*8 Tn9RoWt(1),Tn9Cp(1,1),Tn9Lev(1),
     *       Tn9Ek(1,1),Tn9Rok(1,Tn9Ne),Tn9PpWt(1),Tn9Pk(1,1),Tn9Rot(1,
     >Tn9Ne),
     *       Tn9Xxk(1,Tn9Ne),Tn9MasBor(1),Tn9ccBor(1),Tn9VolWt(1),
     *       Tn9dp(1,Tn9Ne),Tn9MasWt(1),Tn9GIspSum(1),Tn9Pp(1),
     *       Tn9GCondSum(1),Tn9G32sum(1),Tn9G34sum(1),Tn9G23sum(1),
     *       Tn9G43sum(1),Tn9TettaQ(1,Tn9Ne),Tn9Ev_t(1,1),Tn9Dim_t(1),
     *       Tn9Dim_tmax/1/,Tn9EvDo(1),Tn9EvDo0(1),Tn9FlwDdMin(1),
     >Tn9MasVpUp(1),
     *       Tn9MasVpDo(1),Tn9MasGa(1),Tn9LevMas(1),Tn9LevMask(1),
     >Tn9PpGa(1),
     *       Tn9PpVpUp(1),Tn9EeGa(1),Tn9EeVpUp(1),Tn9EeWt(1),
     *       Tn9EeVpDo(1),Tn9LevRelax(1),Tn9VsGa(1),Tn9VsVpUp(1),
     *       Tn9VsWt(1),Tn9VsVpDo(1),Tn9EeWtSatWt(1),Tn9EeVpSatWt(1),
     *       Tn9RoGa(1),Tn9ROVpUp(1),Tn9RoVpDo(1),Tn9LevRel(1),Tn9Hi(1),
     *       Tn9Volk(1,Tn9Ne),  ! ������ ��� �� ��������
     *       Tn9VsWtUp(1),Tn9VsWtJa(1),Tn9Ee(1,1),Tn9TtGa(1),
     *       Tn9TtVpUp(1),Tn9TtWt(1),Tn9TtVpDo(1),
     *       Tn9Mask(1,1),Tn9CcGas(1,1),
     *       Tn9CfMasGasMin(1)/7.5d-7/,
     *       Tn9CcUp(1,1),Tn9FlwHUp(1,1),Tn9FlwHDo(1,1),Tn9GSepSum(1),
     *       Tn9GFlwWtSum(1),Tn9MasGaRealk(1),Tn9dEInt(1,1),
     *       Tn9GCorr(1,1),Tn9drdp(1,1),Tn9drdh(1,1),Tn9vk(1,1),
     *       Tn9Waste_T(20,1),Tn9Dmint(1),Tn9MasWtUp(1),Tn9MasWtJa(1),
     >Tn9RoDo(1),
     *       Tn9WlUpQqOut(1),Tn9WlDoQqOut(1),Tn9TcwAlCulc(1,1),
     *       Tn9TtWlUp(1),Tn9TtWlDo(1),Tn9LaWl(1),
     *       Tn9QqWlEnvUp(1),Tn9QqWlEnvDo(1),Tn9RoomUpInd(1),
     >Tn9RoomDoInd(1),
     *       Tn9CcVpDo(1),Tn9pmax(1),Tn9TtUp(1),Tn9AlSqPhWl(6,1),
     >Tn9AlWlUpDo(1),
     *       Tn9TtSatWt_(1),Tn9Levadd(1),Tn9vol(1),Tn9VolCorrUp(1),
     >Tn9VolCorrDo(1),
     *       Tn9PressCorr(1),Tn9VolGa(1),Tn9FlwIntCoef(1),
     >Tn9VolVpUpLim(1),
     *       Tn9LevTunG(1),Tn9LevTunPpRegG(6,1)

      integer*4 Tn9FlwNum(1)
      logical*1 Tn9CcBorLb(1),Tn9VpInFlag(1),Tn9BorFl(1),
     >Tn9EntOut_Key(1),
     *          Tn9FlagGCorr(1),Tn9FlagGCorrDn(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 4787 

C     common/kts_hC     common/kts_hC     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 4931 


****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
      integer*4 Ntur1,Tr1Lin(1),tr2RLinTyp(1)
      parameter(ntur1=0)
      real*8    tr2FlwKsi1(1)
      real*8 Tr1Oin(1),Tr1Qsd(1),Tr1NdP(1),Tr1genTyp(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 4943 
      integer*4 Ntur3
      parameter(Ntur3=0)
         integer*4 Tr3beaTyp(1),Tr3beaInd(1)
      real*8    Tr3Omdl(1),Tr3Qsd(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 4949 
      integer*4 Ntur4
      parameter(Ntur4=0)
         integer*4 Tr4beaTyp(1),Tr4beaInd(1)
      real*8    Tr4Omdl(1),Tr4Qsd(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 4955 



****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'

      real*8 C1serv(nwall) !����. ��� ������� ������. ������� ���
      real*8 C2serv(nwall) !����������� ���� � ���. ��. ����� ������
      real*8 wl1_QqOut(nwall)  !��������� �������� ����� � ������� ��������
      real*8 wl1AlCulcWl(nwall)   !��������� �������� ����� � ������� ��������
      real*8 wl1_Tt_t(nwall)
      real*8 wl1_XQ(nwall),wl1_BQ(nwall)  ! �-�� � ������� ���������
      common/kts_hWALL1/ C1serv,C2serv,wl1_QqOut,wl1AlCulcWl,
     *  wl1_Tt_t,wl1_XQ,wl1_BQ
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 4975 

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 4989 

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 4995 

****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'

C     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 5015 

      integer*4 Nwall2
      parameter ( Nwall2=0 )
      real*8 wl2_QqOut(1)  !��������� �������� ����� � ������� ��������
      real*8 Wl2Tt_t(1)
      real*8 WL2mass(1),WL2Cp(1),WL2Tt(1)
      real*8 Wl2LamdC(1)
      integer*4 Wl2IndRoom(1),Wl2Ndet(1),Wl2Node(1)
      real*8 Wl2QqEnv(1),Wl2AlCulcWl2(1)
      real*8 Wl2Leff(1)
      real*8 Wl2Rb(1)
      real*8 Wl2Raw(1), Wl2SqEnv(1), Wl2QqBout(1), Wl2DOut(1)
      real*8 Wl2_XQ(1),Wl2_BQ(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 5030 

****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
      integer*4 Nwlcon2
      parameter ( Nwlcon2=0 )
      integer*4 WLC2Iwl(1)
      real*8 WLC2_Qq(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 5039 




****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
      real*4 dtInt1
      call SYBMQNEW(dtInt1,nv,nu,nf,n03,n07,n08,n09,
     *           InpMass,InpMass0,
     *           IFROkz,ITONkz,LinIBNFro,LinIBNto,
     *           FlwGgWv,FlwGgGa,GZ,
     *           NodPp,NodTyp,NodFreez,NodGgSum,NodGgSum_Wv,
     *           NodGgSum_Ga,NodGgSumZeroIn,NodGgSumZero,NodGgComp,
     *           NodGgCompGas,NodGgCompCorGa,NodGgCompWvp,
     *           NodGgCompCorWv,Volukz0,NodRo,NodRotrue,Nodrotrue0,
     *           NodRoGa,nodvvga,nodccga,nodvvgat,
     *           Masdt,Masdt_Ga,Masdt_Wv,
     *           Volukzt,Rozt,nodccgat,NodMasWvMaxTmp,NodMasWvMax,
     *           NodMasGaMaxTmp,NodMasGaMax,Nodtyptnk,NodCfTime,
     *           Volukz,NodGgSumIn,NodCfGgComp,Nod_Flag_Mas_Deb,
     *           MasdtIntegr_Wv,MasdtIntegr_Ga,MasdtIntegr,
     *           Tn3MasGa,Tn3MasWt,Tn3MasWpUp,Tn3MasWpDo,Tn3dMInt,
     *           Tn3GCorr,
     *           Tn7MasWt,Tn7MasGa,Tn7dMIntGa,Tn7dMIntWt,Tn7GCorrGa,
     *           Tn7GCorrWt,
     *           Tn8Gsliv,Tn8GgXx,Tn8MasWt,
     *           Tn9MasVpDo,Tn9MasWt,Tn9MasVpUp,Tn9MasWtUp,Tn9MasWtJa,
     *           Tn9dMInt,Tn9GCorr,
     *           CoolerMass,GazMass,db_mass_max,
     *           NodCountMax,flag_Mas_deb_,alph1,nodalph1,
     *          flag_Mas_deb,flag_start,Maxiterint,ItIntStart,T_fist_in,
     *          flag_first,accelnew,accel,Maxiternew,Maxiter,Tn9MasGa,
     *          FirstStepLabel,CoolerMass0,CoolerMass00,CoolerMass11,
     *          GazMass0,KeyDel,Tn3dEInt,Tn9dEInt,NodTypCon,
     *          Maxiterint0(1),ItIntStart0(1),Nod_Flag_dM,NodRoMas,
     *          NodPropsKey,NodPropsKey1,NodePropsKey(1),NODFREEZPH,
     *          NUCH,CoalHConNodInd,CoalHNodMoistG,NodDbMasZero,
     *          NodalfaH,dbIntMassga,dbIntMassWv,dbIntMassdif,
     *          dCm2_sum,dCm_hist,dCmdt_Mean,step_num,DebMaxG,DebMaxWv,
     *          DebMaxFull,iDebMaxG,iDebMaxWv,tiDebMaxG,tiDebMaxWv,
     *          NodXxBal,GCorCf,iDebMaxSum,CoolerMassNod,pzt,
     *          ComGamFrom,ComGamTo,Tn3EvDo,Tn3LevRel,Tn3FlwDdMin,
     *          Tn9EvDo,Tn9LevRel,Tn9FlwDdMin,HFrom,HTo,Tn3FlagGCorr,
     *          Tn9FlagGCorr,Tn3FlagGCorrDn,Tn9FlagGCorrDn,
     *          Fast_Option_Deb,FlwDpPm,FlwDpPm0,NodtypDeb,
     *          Nod_flag_Mas_deb_tmp,nodcfppup_,nodcfppup_tmp,
     *          Tn3Hi,Tn9Hi,NodCfdPdt_loc,NodCfdPdt_loc1,NodTt,
     >CoolerType,
     *          Tn3LevTunG,Tn9LevTunG,Tn7LevTunG,Tn8LevTunG,
     *          Tn3LevTunPpRegG,Tn9LevTunPpRegG,Tn7LevTunPpRegG,
     *          NodPpMinWtDebCor)

                                                         


      return
      end
****** End   file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mqnew.fs'
****** Begin file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET42.fs'
      logical function kts_h_it(deltat)
      implicit none
      include 'kts_h.fh'
      REAL*8 FlwTnSqFrom(894)
      COMMON/kts_h_nowrt/ FlwTnSqFrom
      REAL*8 FlwHTnFromUp(894)
      COMMON/kts_h_nowrt/ FlwHTnFromUp
      REAL*8 FlwTnSqTo(894)
      COMMON/kts_h_nowrt/ FlwTnSqTo
      REAL*8 FlwHTnFromDo(894)
      COMMON/kts_h_nowrt/ FlwHTnFromDo
      REAL*8 FlwHTnToUp(894)
      COMMON/kts_h_nowrt/ FlwHTnToUp
      REAL*8 FlwHTnToDo(894)
      COMMON/kts_h_nowrt/ FlwHTnToDo
      INTEGER*4 Fan2NTN2N
      PARAMETER (Fan2NTN2N=0)
      INTEGER*4 Fan2NTN0N
      PARAMETER (Fan2NTN0N=0)
      INTEGER*4 Fan2NTauNN
      PARAMETER (Fan2NTauNN=0)
      INTEGER*4 Fan2NdPG5N
      PARAMETER (Fan2NdPG5N=0)
      INTEGER*4 Fan2NdPG4N
      PARAMETER (Fan2NdPG4N=0)
      INTEGER*4 Fan2NdPG3N
      PARAMETER (Fan2NdPG3N=0)
      INTEGER*4 Fan2NdPG2N
      PARAMETER (Fan2NdPG2N=0)
      INTEGER*4 Fan2NdPG0N
      PARAMETER (Fan2NdPG0N=0)
      INTEGER*4 Fan2TauNN
      PARAMETER (Fan2TauNN=0)
      INTEGER*4 FanDim_TauNmax
      PARAMETER (FanDim_TauNmax=0)
      INTEGER*4 FanDim_Taumax
      PARAMETER (FanDim_Taumax=0)
      INTEGER*4 FanDim_KMax
      PARAMETER (FanDim_KMax=0)
      INTEGER*4 FanDim_PMax
      PARAMETER (FanDim_PMax=0)
      INTEGER*4 Tn8Ne
      PARAMETER (Tn8Ne=2)
      INTEGER*4 Tn3Np
      PARAMETER (Tn3Np=2)
      INTEGER*4 Tn3Ne
      PARAMETER (Tn3Ne=4)
      INTEGER*4 INVPERe(551)
      COMMON/kts_h_nowrt/ INVPERe
      INTEGER*4 NZSUBe(4335)
      COMMON/kts_h_nowrt/ NZSUBe
      INTEGER*4 INZSUBe(552)
      COMMON/kts_h_nowrt/ INZSUBe
      INTEGER*4 ILNZe(552)
      COMMON/kts_h_nowrt/ ILNZe
      INTEGER*4 INVPERp(551)
      COMMON/kts_h_nowrt/ INVPERp
      INTEGER*4 NZSUBp(4335)
      COMMON/kts_h_nowrt/ NZSUBp
      INTEGER*4 INZSUBp(552)
      COMMON/kts_h_nowrt/ INZSUBp
      INTEGER*4 ILNZp(552)
      COMMON/kts_h_nowrt/ ILNZp
      INTEGER*4 Nthermo
      PARAMETER (Nthermo=894)
      INTEGER*4 Nhydro
      PARAMETER (Nhydro=894)
      INTEGER*4 Nent
      PARAMETER (Nent=551)
      INTEGER*4 Npc
      PARAMETER (Npc=1102)
      INTEGER*4 Npres
      PARAMETER (Npres=551)
      INTEGER*4 Nf
      PARAMETER (Nf=6)
      INTEGER*4 Tn7Ne
      PARAMETER (Tn7Ne=1)
      INTEGER*4 Tn7Np
      PARAMETER (Tn7Np=1)
      INTEGER*4 Tn9Ne
      PARAMETER (Tn9Ne=6)
      INTEGER*4 Tn9Np
      PARAMETER (Tn9Np=2)
      INTEGER*4 Fan2NTN3N
      PARAMETER (Fan2NTN3N=0)
      INTEGER*4 Fan2NTN4N
      PARAMETER (Fan2NTN4N=0)
      INTEGER*4 Fan2NTN5N
      PARAMETER (Fan2NTN5N=0)
      INTEGER*4 Tr1Dim_Nu
      PARAMETER (Tr1Dim_Nu=-111)
      INTEGER*4 Tr2Dim_Ksi
      PARAMETER (Tr2Dim_Ksi=-111)
      INTEGER*4 Tn8Np
      PARAMETER (Tn8Np=1)
      LOGICAL*1 PIntIter(551)
      COMMON/kts_h_nowrt/ PIntIter
      LOGICAL*1 EIntIter(551)
      COMMON/kts_h_nowrt/ EIntIter
      LOGICAL*1 EextIter(551)
      COMMON/kts_h_nowrt/ EextIter
      LOGICAL*1 PExtIter(551)
      COMMON/kts_h_nowrt/ PExtIter
      !DEC$PSECT/kts_h_NOWRT/ NoWRT
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 5102 

****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
      integer*4 Ncomp,CompLin(1),CompOin(1),CompGenTyp(1)
      parameter(Ncomp=0)
      real*8 CompQq(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 5111 
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 5116 


****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'

c      real*8 FlwCondRegul(nv)
c      common/kts_hFlwL/ FlwCondRegul
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 5127 

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 5162 
      integer*4 nfwv
      parameter(nfwv=0)
      integer*4 FwvFloWw(1),FwvFloWv(1)
      real*8 FwvRoFrom1(1),FwvRoFrom2(1),FwvRoTo1(1),FwvRoTo2(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 5168 
      integer*4 nfwv1
      parameter(nfwv1=0)
      integer*4 Fwv1Flow(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 5173 
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
      integer NHe2
      parameter(NHe2=0)
* ���������� ��� ��������������� ��������� ����������
      real*8 C0He2(1) !����. ��� ������� ����������� ���� ��
      real*8 C1He2(1) !����. ��� ������� ����������� ���� ��
      real*8 C2He2(1) !����. ��� ������� ����������� ���� ��
      real*8 DTHe2
      real*8 C0He2b1(1),C1He2b1(1),C2He2b1(1)
      real*8 C0He2b2(1),C1He2b2(1),C2He2b2(1)
      real*8 He2T1t(2,1),He2T2t(2,1),He2decQt(1),He2_gar2(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 5189 
C     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 5203 
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'

      real*8 Mtl_QqOut(NMtl)  !��������� �������� ����� � ������� ��������
      real*8 MtlTt_t(NMtl)
      real*8 MtlAlCulc(nMtl)
      common/kts_hMetal/ Mtl_QqOut,MtlTt_t,MtlAlCulc
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 5214 

C     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 5221 

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 5229 

      integer*4 NMtlO,MtlOIndRoom(1)
      parameter ( NMtlO=0 )
      real*8 MtlOTt(1),MtlOMass(1),MtlOCp(1),MtlOAlCulcWl1(1)
      real*8 MtlO_QqOut(1)  !��������� �������� ����� � ������� ��������
      real*8 MtlOLamdC(1),MtlOLamd(1),MtlOQqEnv(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 5237 
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'

      integer*4 NUCH
      parameter (NUCH=0)
      integer*4 CoalHConNodInd(1)
      real*8 CoalHNodMoistG(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 5246 

      integer*4 NFlwWasteSet
      parameter(NFlwWasteSet=0)
      integer*4 FlwWasteSetConnInd(1),FlwWasteSetNum(1)
      real*8 FlwWasteSetWaste(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 5253 

      integer*4 FlwConnFlwInd(1),NFlwConn
      parameter(NFlwConn=0)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 5258 

C     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 5263 

      integer*4 NHCO
      parameter(NHCO=0)
      integer*4  HCOFlMod(1),HCOIndFlw(1)
      real*8 HCOCfMod(1),HCOOFg(1),HCOONodVol(1),HCOONoddRodP(1)
      common/kts_hConFlOut/ HCOFlMod,HCOCfMod,HCOOFg,HCOONodVol,
     >HCOONoddRodP,HCOIndFlw
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 5271 

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 5278 

      integer*4 nCrv
      parameter(nCrv=0)
      real*8 CrvRes(1)
      integer*4 CrvFlw(nCrv)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 5285 

      integer*4 nWst
      parameter(nWst=0)
      integer*4 WstNode(1)
      real*8    WSTConsN2(1),WSTConsO2(1),WSTConsH2O(1),
     *          WSTConsCO2(1),WSTConsSoot(1),WSTConsAsh(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 5293 

      integer*4 NXS
      parameter(NXS=0)
      integer*4 XSFlagSetCc(1),XSFlSet(30,1),XSIndSetCc(1),
     >XSIndSetCc0(1)
      real*8    XSCcstr(30,1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 5300 

      integer*4 ntFrB
      parameter(ntFrB=0)
      integer*4 tFrBNdInd(1),tFrBi(1)
      real*8    tFrBCcOld(100,1),tFrBTau(1)
      real*8    tFrBGIn(1),tFrBAa(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 5308 

      integer*4 np4
      parameter(np4=0)
      integer*4 Pm4Nb(1)
      real*8 Pm4Res(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 5315 

c ��������� �����. ��� ������� � ���������������, ���� �� (�������) ���
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 5323 
      integer*4 np2
      parameter(np2=0)
      integer*4 Pm2Nb(1)
      real*8 Pm2Res(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 5329 
      integer*4 nFan
      parameter(nFan=0)
      integer*4 FanFlw(1)
      real*8 FanRes(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 5335 
      integer*4 nFan2
      parameter(nFan2=0)
      integer*4 Fan2Flw(1)
      real*8 Fan2Res(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 5341 
      integer*4 np3
      parameter(np3=0)
      real*8 Pm3QqNod(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 5346 
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 5356 

      real*8 time_df1  !������� ��� ������� ������� (� �����)
      real*8 time_tmp

      real   deltat      !��� ����������
      real   deltat1      !��� ����������
      REAL*8 Atau        !1/dt
c      save Atau
      real*4 dtInt       !dt*Accel
      real*8 TimeSrv     !dt/Niter
      character*100 fn1  ! ��� ������������� .cbf ������
c      common/kts_hLocal1/ time_tmp,deltat,Atau,dtInt,TimeSrv,fn1
      common/kts_hLocal1/ Atau,time_tmp,deltat1,dtInt,TimeSrv,fn1
      real*8 VOL1(nu) !V/dt
      real*8 NodCfdE(nu) !����. � ��. ����. ��� ����� ���������
      real*8 NodTtk(nu)  !����������� ��������. � ������� ����� �� k-�� ��������
      real*8 RotVol(nu) !��������� ������
      real*8 RokVol(nu) !��������� ������
      real*8 NodVolk(nu)  !������������ ����� �� k-�� ��������
      real*8 NoddVdPk(nu) !����������� ������ � ���� �� �������� �� k-�� ����.

c ---  ������ �. (29.08.02) - ������
      real*8 NodSum1(nu)
      real*8 NodSum2(nu)
c ---  ������ �. (29.08.02) - �����

      real*8 NodQqPmAd(nu)

      common/kts_hNODE1/ VOL1,NodCfdE,NodTtk,RotVol,RokVol,
     * NodVolk,NoddVdPk,NodSum1,NodSum2,NodQqPmAd
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 5388 

      real*8 C7(nv)   !��������� ������������
      real*8 Gk(nv)   !������� �� k-�� ��������
      real*8 dHLev(nv)!������ �� ����� �� ���� ������� � �������
      real*8 Rovk(nv)    !��. ��������� �� k-�� ��������
      real*8 AconRovk(nv)!��������� ��� ��������
c###### ��������� ####################################
      real*8 FlwGaVeSq(nv)  ! ��������� ������ ��� �������� ������������� �����
      real*8 flwbcon(nv)    ! �������� ��������� �������� �������� �� ����� 
      real*8 FlwC7work1(nv)   ! ��������� ������ ��� �������� ������������� �����
      real*8 FlwC7work2(nv)   ! ��������� ������ ��� �������� ������������� �����
      real*8 FlwC7work3(nv)   ! ��������� ������ ��� �������� ������������� �����
      real*8 FlwC7work4(nv)   ! ��������� ������ ��� �������� ������������� �����
      real*8 FlwC7work5(nv)   ! ��������� ������ ��� �������� ������������� �����
      real*8 FlwC7work7(nv)
      real*8 FlwVsk    (nv)   ! �������� �� ����� �� ���������
      real*8 ComEeFrom  (nv)   ! ��������� ��������� ��������� � ��������
      real*8 ComEeTo    (nv)   ! ��������� ��������� ��������� � ��������  
      real*8 FlwSlGaFrom(nv)   ! �������� ��� ��������������� ���� �� �����
      real*8 FlwSlGaFrom0(nv)  ! �������� ��� ��������������� ���� �� �����
      real*8 FlwSlGaTo   (nv)  ! �������� ��� ��������������� ���� �� �����
      real*8 FlwSlGaTo0  (nv)  ! �������� ��� ��������������� ���� �� �����
      real*8 FlwCcFrom(nv) ! ��������� ������ ��� ������������ ���� �� ������ �����
      real*8 FlwCcTo(nv)   ! ��������� ������ ��� ������������ ���� �� ������ �����
      real*8 flwdhz0(nv)
      real*8 ComRoFrom(3,nv)
      real*8 ComRoTo  (3,nv)
c###### ��������� ####################################
      real*8 FlwKgp(nf,2,nv)    ! 
      real*8 FlwKge(nf,nf,nv)
      real*8 FlwKgg(3,nv)
      real*8 FlwKgpt(2,nv)
      real*8 FlwdPLevFrom(nv),FlwdPLevTo(nv)
      real*8 FlwCcGgWtJa(nv)
      real*8 FlwRes_Iter(nv)
      real*8 FlwGG_TMP(nv)
      real*8 FlwRes_Tmp
      real*8 FlwdCgDCcFrom(nv),FlwdCgDCcTo(nv)
c      real*8 frco0(nv)
      common/kts_hFLOW/ C7,Gk,dHLev,Rovk,AconRovk,FlwGaVeSq,
     * flwbcon,FlwC7work1,
     * FlwC7work2,FlwC7work3,FlwC7work4,FlwC7work5,FlwC7work7,
     * FlwVsk,ComEeFrom,ComEeTo,FlwSlGaFrom,FlwSlGaFrom0,
     * FlwSlGaTo,FlwSlGaTo0,FlwCcFrom,FlwCcTo,flwdhz0,ComRoFrom,
     * ComRoTo,FlwKgp,FlwKge,FlwKgg,FlwKgpt,
     * FlwdPLevFrom,FlwdPLevTo,FlwCcGgWtJa,
     * FlwRes_Iter,FlwRes_Tmp,FlwGG_TMP,FlwdCgDCcFrom,FlwdCgDCcTo
c    * ,frco0
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 5438 

C     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 5443 

C     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 5448 


c      !������� ����. ����� ����� � ���. ��������� ��� ��������
       real*8 Dmp(Npres,Npres)
C       real*8 Dmpc(Npc,Npc) ! �������� 
c      !������� ����. ����� ����� � ������� ��������� ��� ���������
       real*8 Dmh(Nent,Nent)
       real*8 DmhG(Nent,Nent)
c      !������ ����� ��� ���������� ������� �� ����.
      Real*8 Yp(Npres)
c      Real*8 Ypc(Npc)
c      !������ ����� ��� ���������� ������� �� ���.
      real*8 Ye(Nent)
      real*8 YeG(Nent)
c      !������� �� ���������
      real*8 dP(Npres)
c      real*8 dPC(Npc)
c      !������� �� ����������
      real*8 dE(Nent)
      real*8 dEG(Nent)
c      !������ ������ �������� �� k-�� ��������
      real*8 Pk(Npres)
c      !������ ������ �������� �� (k+1)-�� ��������
      real*8 Pk1(Npres)
c      !������ ������ ��������� �� k-�� ��������
      real*8 Ek(Nent)
      real*8 EGk(Nent)
c      !������ ������ ��������� �� (k+1)-�� ��������
      real*8 Ek1(Nent)
      real*8 EGk1(Nent)
      real*8 Rok(Nu) !�����. �\� ����� � ������� ����� �� k-�� ����.
      !������ ������ ��������� �����. ����
      real*8 EeWtSatk(Nent)
      !������ ������ ��������� �����. ����
      real*8 EeVpSatk(Nent)
      real*8 MaxDevP     !����. �����. ����. �������� �� ��������
      real*8 MaxDevH     !����. �����. ����. ��������� �� ��������
      INTEGER*4 I,j     !���������� �����
      integer*4 IC        !������� ��������
      integer*4 ICInt !������� ���������� ��������
      common/kts_hLocal2/ Dmp,Dmh,Yp,Ye,dP,dE,Pk,Pk1,Ek,Ek1,Rok,
     *  EeWtSatk,EeVpSatk,MaxDevP,MaxDevH,IC,ICInt,DmhG,YeG,dEG,
     *  EGk,EGk1       !,dPC,Ypc,Dmpc <=������ ��������!!!!

C     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 5496 

      INTEGER*4 nvlgpos !����� ����� � �������������� ���������� � ����������
      common/kts_hVlvGeo/ nvlgpos
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 5501 

C     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 5506 

C     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 5511 

C     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 5516 

C     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 5521 

      integer*4 nsep1,Sep1FlwS(1),Sep1Node(1)
        parameter(nsep1=0)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 5526 

      integer*4 nInWst,InWstNum(1),InWstNode(1),InWstNdTp(1)
      real*8    InWstdt(1)
      integer*4 InWstTyp(1)
      real*8    InWstCc(1)
      real*8 InWstExtCon(1),InWstExtWstdt(1)
      parameter(nInWst=0)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 5535 

C     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 5540 


      LOGICAL*1 go,goint  !������� ����������� ��������
      DATA go/.true./     !����������� �������� �������. ��������
      common/kts_hLocal4/ go,goint
      real*8  RDTSC
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 5579 
      real*8 NoddRodPk(nu) !dRo/dP ��� ������� ����� �� ����. ����.
      real*8 NoddRodEk(nu) !dRo/d� ��� ������� ����� �� ����. ����.
      real*8 NoddRodPk1(nu) !dRo/dP ��� ������� ����� �� ���. ����.
      real*8 NoddRodEk1(nu) !dRo/d� ��� ������� ����� �� ���. ����.
      real*8 nodrorelax(nu)
      real*8 NodRoGa0(nu)
      real*8 NodCcGa_Aeq0(nu) 
      real*8 NodCcGa_Deq0(nu) 
      real*8 NodCcGa_Beq (nu)
      real*8 NodCcGa_Beq0(nu)
      real*8 NodCcGa_Aeq (nu)
      real*8 NodCcGa_Deq (nu)
      real*8 NodXxk1(nu)    !���. �������. � ������� ����� �� ���. ����.
      real*8 NodXxBalk1(nu) !���. �������. � ������� ����� �� ���. ����.
      real*8 NodQqAd(nu)
      real*8 NodCfEeAsh(nu)
      real*8 NodEeWvt(nu)
      real*8 NodAlSqGaWv(nu),NodYe(nu),NodDmh(nu)
      real*8 NodPsw(nu),NodRoWvTmp(nu),NoddRdHGas(nu)
     *,NoddRdPWvTmp(nu),NoddRdHWvTmp(nu)
     *,NodTtWvTmp(nu),NodCpWvTmp(nu),NodXxTmp(nu)
      integer*4 Nod_ghm_flag(nu),NodPropRelaxFlag(nu)
      logical*1 EExtIter_(Nent)
      logical*1 PExtIter_(Npres)
      common /kts_hNODE/ NoddRodPk,NoddRodEk,NoddRodPk1,NoddRodEk1,
     *  nodrorelax,NodRoGa0,NodCcGa_Aeq0,NodCcGa_Deq0,NodCcGa_Beq,
     *  NodCcGa_Beq0,NodCcGa_Aeq,NodCcGa_Deq,NodXxk1,NodXxBalk1,
     *  NodQqAd,NodCfEeAsh,NodEeWvt,NodAlSqGaWv,NodYe,NodDmh,
     *  NodPsw,NodRoWvTmp,NoddRdHGas,NodTtWvTmp,NodCpWvTmp,NodXxTmp
      common /kts_hNODE_I/ Nod_ghm_flag,NodPropRelaxFlag
      common /kts_hNODE_L/ EExtIter_,PExtIter_
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 5612 
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'

      integer NTc0
      parameter(NTc0=0)
      integer*4 Tc0IbnFro(1),Tc0NodFro(1),Tc0IbnTo(1),Tc0NodTo(1)
      real*8 Tc0zt(1),Tc0AlG23(1),Tc0AlG43(1)
     *,Tc0G43(1),Tc0G23(1),Tc0G34(1),Tc0CpWl(1)
     *,Tc0TtEnv(1),Tc0TtWlk(1),Tc0TtWl(1),Tc0AlSqEnv(1),Tc0AlEnv(1)
     *,Tc0AlSqPhWl(1,nf),Tc0MasCpWl(1),Tc0CfAlEnv(1)
     *,Tc0CfAlGaWl(1),Tc0CfAlWpUpWl(1)
     *,Tc0CfAlWtWl(1),Tc0CfAlWpDoWl(1)
     *,Tc0EvUpWl(1),Tc0EvDoWl(1)
     *,Tc0dG23dp(1),Tc0dG34dp(1),Tc0dG43dp(1)
     *,Tc0CfGgWpUpWt(1),Tc0CcGgWtWpDo(1),Tc0CfGgWpDoWt(1)
     *,Tc0G32(1),Tc0dG32dp(1),Tc0CfGgWt(1),Tc0SqWl(1),Tc0TettaQ(1,nf)
     *,Tc0QqIsp(1),Tc0AlIsp(1)
     *,Tc0dQIspdP(1),Tc0TtTnIsp(1),Tc0AlSqIsp(1),TC0ResWl(1)
     *,Tc0dQG23dp(1),Tc0TettaQ2(1,nf),Tc0DeltaTs(1)
     *,Tc0AlG23From(1),Tc0AlG43From(1)
     *,Tc0G23From(1),Tc0G34From(1),Tc0G32From(1),Tc0G43From(1)
     *,Tc0dG23dpFrom(1),Tc0dG34dpFrom(1),Tc0dG43dpFrom(1),
     >Tc0dG32dpFrom(1)
     *,Tc0dQIspdPFrom(1),Tc0TtTnIspFrom(1),Tc0AlSqIspFrom(1)
     *,Tc0CcGgWtWpDoFrom(1),Tc0CfGgWpUpWtFrom(1),Tc0CfGgWpDoWtFrom(1)
     *,Tc0QqIspFrom(1),Tc0AlIspFrom(1),Tc0CfGgWtFrom(1),Tc0TettaQFrom(1,
     >nf)
     *,Tc0AlSqPhWlFrom(1,nf),Tc0CfRelAlIsp(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 5642 
C     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 5671 

****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'

      integer NTc2
      parameter(NTc2=0)
      integer*4 Tc2Ibn(1),Tc2Node(1)
      real*8 Tc2zt(1),Tc2QqEnv(1),Tc2AlG23(1),Tc2G23(1),Tc2G34(1)
     *,Tc2AlG43(1),Tc2G43(1),Tc2CpWl(1)
     *,Tc2TtEnv(1),Tc2TtWlk(1),Tc2TtWl(1),Tc2AlSqEnv(1),Tc2AlEnv(1)
     *,Tc2AlSqPhWl(1,nf),Tc2MasCpWl(1),Tc2CfAlEnv(1)
     *,Tc2CfAlGaWl(1),Tc2CfAlWpUpWl(1),Tc2CfAlWtWl(1),Tc2CfAlWpDoWl(1)
     *,Tc2EvUpWl(1),Tc2EvDoWl(1)
     *,Tc2dG23dp(1),Tc2dG34dp(1),Tc2dG43dp(1)
     *,Tc2CfGgWpUpWt(1),Tc2CcGgWtWpDo(1),Tc2CfGgWpDoWt(1)
     *,Tc2G32(1),Tc2dG32dp(1),Tc2CfGgWt(1),Tc2SqWl(1),Tc2TettaQ(1,nf)
     *,Tc2QqIsp(1),Tc2AlIsp(1),Tc2DeltaTs(1)
     *,Tc2dQIspdP(1),Tc2TtTnIsp(1),Tc2AlSqIsp(1),TC2ResWl(1),
     * Tc2LlWl(1),Tc2LaWl(1),Tc2ResWl0(1),Tc2CfRelAlIsp(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 5694 
C     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 5708 

****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'

C     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 5718 

      integer*4 NTCR,TcRNode(1)
      parameter ( NTCR=0 )
      real*8 TcR_XQ(1),TcR_BQ(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 5724 

      integer*4 Nwall2O
      parameter ( Nwall2O=0 )
      real*8  Wl2OTt(6,1),Wl2ORAw(1),Wl2OSqEnv(1),Wl2OQqBout(1),
     &        Wl2ODout(1)

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 5732 

****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 5738 

****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'

      integer NTen
      parameter(NTen=0)
      integer*4 TenIbn(1),TenNode(1)
      real*8 Tenzt(1),TenQqIn(1),TenAlG23(1),TenG23(1),TenG34(1)
     *,TenAlG43(1),TenG43(1),TenCpWl(1),TenTtWlk(1),TenTtWl(1)
     *,TenAlSqPhWl(1,nf),TenMasCpWl(1),TenCfAlGaWl(1),TenCfAlWpUpWl(1)
     *,TenCfAlWtWl(1),TenCfAlWpDoWl(1),TenEvUpWl(1),TenEvDoWl(1)
     *,TendG23dp(1),TendG34dp(1),TendG43dp(1)
     *,TenCfGgWpUpWt(1),TenCcGgWtWpDo(1),TenCfGgWpDoWt(1)
     *,TenG32(1),TendG32dp(1),TenCfGgWt(1),TenSqWl(1),TenTettaQ(1,nf)
     *,TenQqIsp(1),TenAlIsp(1),TenDeltaTs(1)
     *,TendQIspdP(1),TenTtTnIsp(1),TenAlSqIsp(1),TenResWl(1),
     >TenTcwAlCulc(1),TenTcwQq(1)
     *,TenLawL(1),TenQqInt(1),TenQqExt(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 5761 
C     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 5775 
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'

      integer n03
      parameter(n03=0)
      integer*4 Tn3RelP(2,1),Tn3RelE(4,1)
      real*8 Tn3RoWt(1),Tn3Cp(1,1),Tn3Lev(1),
     *       Tn3Ek(1,1),Tn3Rok(1,Tn3Ne),Tn3PpWt(1),Tn3Pk(1,1),Tn3Rot(1,
     >Tn3Ne),
     *       Tn3Xxk(1,Tn3Ne),Tn3MasBor(1),Tn3ccBor(1),Tn3VolWt(1),
     *       Tn3dp(1,Tn3Ne),Tn3MasWt(1),Tn3GIspSum(1),Tn3Pp(1),
     *       Tn3GCondSum(1),Tn3G32sum(1),Tn3G34sum(1),Tn3G23sum(1),
     *       Tn3G43sum(1),Tn3TettaQ(1,Tn3Ne),Tn3Ev_t(1,1),Tn3Dim_t(1),
     *       Tn3Dim_tmax/1/,Tn3EvDo(1),Tn3EvDo0(1),Tn3FlwDdMin(1),
     >Tn3MasWpUp(1),
     *       Tn3MasWpDo(1),Tn3MasGa(1),Tn3LevMas(1),Tn3LevMask(1),
     >Tn3PpGa(1),
     *       Tn3PpWpUp(1),Tn3EeGa(1),Tn3EeWpUp(1),Tn3EeWt(1),
     *       Tn3EeWpDo(1),Tn3LevRelax(1),Tn3VsGa(1),Tn3VsVpUp(1),
     *       Tn3VsWt(1),Tn3VsVpDo(1),Tn3EeWtSatWt(1),Tn3EeWpSatWt(1),
     *       Tn3RoGa(1),Tn3ROWpUp(1),Tn3RoWpDo(1),Tn3LevRel(1),Tn3Hi(1),
     *       Tn3Volk(1,Tn3Ne),Tn3Mask(1,1),Tn3CcGas(1,1),
     *       Tn3CfMasGasMin(1)/1.0d-7/,
     *       Tn3CcUp(1,1),Tn3FlwHUp(1,1),Tn3FlwHDo(1,1),Tn3Ee(1,1),
     *       Tn3TtGa(1),Tn3TtWpUp(1),Tn3TtWt(1),Tn3TtWpDo(1),
     >Tn3MasGaRealk(1),
     *       Tn3GCorr(1,1),Tn3drdp(1,1),Tn3drdh(1,1),Tn3vk(1,1),
     *       Tn3Waste_T(20,1),Tn3Dmint(1),Tn3RoDo(1),Tn3dEInt(1,1),
     *       Tn3WlUpQqOut(1),Tn3WlDoQqOut(1),Tn3TcwAlCulc(1,1),
     *       Tn3TtWlUp(1),Tn3TtWlDo(1),Tn3LaWl(1),
     *       Tn3QqWlEnvUp(1),Tn3QqWlEnvDo(1),Tn3RoomUpInd(1),
     >Tn3RoomDoInd(1),Tn3Tt(1,1),
     *       Tn3CcWpDo(1),Tn3CfGgWpDo_tmp(1),Tn3pmax(1),Tn3TtUp(1),
     >Tn3AlSqPhWl(4,1),Tn3AlWlUpDo(1),
     *       Tn3Levadd(1),Tn3cfdebGg(1),
     *       Tn3PressCorr(1),TN3VOL(1),TN3VOLGA(1),TN3VOLCORRUP(1),
     >TN3VOLCORRDO(1),Tn3FlwIntCoef(1),
     *       Tn3VolVpUpLim(1),Tn3dmintsum(1),Tn3CfGCorr(1),
     >Tn3LevTunG(1),Tn3LevTunPpRegG(6,1)
      integer*4 Tn3FlwNum(1),Tn3CcSetInd(1)
      logical*1 Tn3CcBorLb(1),Tn3VpInFlag(1),Tn3BorFl(1),
     >Tn3EntOut_Key(1),Tn3FlagGCorr(1),
     * Tn3FlagGCorrDn(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 5813 

C     common/kts_hC     common/kts_hC     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 5955 


****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'

      integer*4 n07,Tn7RelP(1,1),Tn7RelE(2,1),Tn7Homo(1),Tn7CcSetInd(1)
      real*8    Tn7RoGa(1),Tn7RoWt(1),Tn7Rok(1,2),Tn7XxWt(1),Tn7Xk(1,2),
     *          Tn7Lev(1),Tn7EeWt(1),Tn7VsGa(1),Tn7VsWt(1),Tn7PpGa(1),
     *          Tn7EeGa(1),Tn7EeWtSat(1),Tn7EeVpSat(1),Tn7LevRel(1),
     *          Tn7Hi(1),Tn7PpWt(1),Tn7TtWt(1),Tn7Ee(2,1),
     *          Tn7Ek(2,1),Tn7CcVolk(1),Tn7CcMask(1)
     *         ,Tn7XxBalWt(1),Tn7Levk(1),Tn7Lev_Rlx(1),Tn7Acon(1),
     *          Tn7Sq(1),Tn7CcBor(1),Tn7MasWt(1),Tn7MasGa(1),
     *          Tn7MasBor(1),Tn7GCorrWt(1),
     *          Tn7dRdPk(2,1),Tn7dRdP(2,1),Tn7dRdHk(2,1),Tn7dRdH(2,1),
     *          dP_Tn7(1),Tn7Dcon(1),Tn7LsMin(1),Tn7dMLoc(2,1),
     *          Tn7CfRoGaCorr(1),Tn7CfRoWtCorr(1),
     *          Tn7Waste_T(20,1),Tn7Rowttrue(1),Tn7RoWtSat(1),
     *          Tn7RoVpSat(1),Tn7Dmint(1),Tn7Gcorr(1),
     *          Tn7WlUpQqOut(1),Tn7WlDoQqOut(1),Tn7TcwAlCulc(1,1),
     *          Tn7TtWlUp(1),Tn7TtWlDo(1),Tn7LaWl(1),Tn7QqWlEnv(1),
     >Tn7RoomInd(1)
     *          ,Tn7EvDo(1),Tn7EvDo0(1),Tn7TtGa(1),Tn7AlSqPhWl(2,1),
     >Tn7AlWlUpDo(1)
     *          ,Tn7dMIntGa(1),Tn7dMIntWt(1),Tn7GCorrGa(1),
     >Tn7LevTunG(1),Tn7LevTunPpRegG(1)
      logical*1 Tn7_Flag_Mas_Deb(1),Tn7CfCorr_On(1)


      logical*1 Tn7CcBorLb(1)

      parameter(n07=0)
      real*8 Tn7Pk(2,1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 5989 

C     common/kts_hC     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 6046 

****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
      integer n08
      parameter(n08=0)
      integer*4 Tn8RelP(1,0),Tn8RelE(2,0),Tn8CcSetInd(1)
      real*8 Tn8RoWt(1),Tn8RoGa(1),Tn8GSumWt1(1),Tn8GSumWt2(1),
     *       Tn8Lev(1),Tn8RoWtk(1),Tn8PpWt(1),Tn8MasBor(1),Tn8ccBor(1),
     *       Tn8VolWt(1),Tn8XxWt(1),Tn8XxBalWt(1),
     *       Tn8MasWt(1),Tn8MasWtk(1),
     *       Tn8RoGak(1),Tn8Gsliv(1),Tn8EeGa(1),
     *       Tn8EeWt(1),Tn8PpGa(1),Tn8VsWt(1),Tn8VsGa(1),Tn8VsVp(1),
     *       Tn8EeWtSat(1),Tn8EeVpSat(1),Tn8LevRel(1),Tn8Hi(1),
     *       Tn8TtGa(1),Tn8TtWt(1),Tn8GgXx(1),
     *       Tn8dRdP1(1),Tn8dRdH1(1),Tn8dRdP2(1),Tn8dRdH2(1),
     *       Tn8Waste_T(20,1),Tn8RowtSat(1),Tn8RovpSat(1),
     *       Tn8QqEnv(1),Tn8RoomInd(1),Tn8TtMeDo(1)
     *       ,Tn8EvDo(1),Tn8EvDo0(1),Tn8GInOut(1),Tn8LevTunG(1)

      logical*1 Tn8CcBorLb(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 6070 

C     common/kts_hC     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 6107 

****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'

      integer n09
      parameter(n09=0)
      integer*4 Tn9RelP(2,1),Tn9RelE(Tn9Ne,1),Tn9CcSetInd(1)
      real*8 Tn9RoWt(1),Tn9Cp(1,1),Tn9Lev(1),
     *       Tn9Ek(1,1),Tn9Rok(1,Tn9Ne),Tn9PpWt(1),Tn9Pk(1,1),Tn9Rot(1,
     >Tn9Ne),
     *       Tn9Xxk(1,Tn9Ne),Tn9MasBor(1),Tn9ccBor(1),Tn9VolWt(1),
     *       Tn9dp(1,Tn9Ne),Tn9MasWt(1),Tn9GIspSum(1),Tn9Pp(1),
     *       Tn9GCondSum(1),Tn9G32sum(1),Tn9G34sum(1),Tn9G23sum(1),
     *       Tn9G43sum(1),Tn9TettaQ(1,Tn9Ne),Tn9Ev_t(1,1),Tn9Dim_t(1),
     *       Tn9Dim_tmax/1/,Tn9EvDo(1),Tn9EvDo0(1),Tn9FlwDdMin(1),
     >Tn9MasVpUp(1),
     *       Tn9MasVpDo(1),Tn9MasGa(1),Tn9LevMas(1),Tn9LevMask(1),
     >Tn9PpGa(1),
     *       Tn9PpVpUp(1),Tn9EeGa(1),Tn9EeVpUp(1),Tn9EeWt(1),
     *       Tn9EeVpDo(1),Tn9LevRelax(1),Tn9VsGa(1),Tn9VsVpUp(1),
     *       Tn9VsWt(1),Tn9VsVpDo(1),Tn9EeWtSatWt(1),Tn9EeVpSatWt(1),
     *       Tn9RoGa(1),Tn9ROVpUp(1),Tn9RoVpDo(1),Tn9LevRel(1),Tn9Hi(1),
     *       Tn9Volk(1,Tn9Ne),  ! ������ ��� �� ��������
     *       Tn9VsWtUp(1),Tn9VsWtJa(1),Tn9Ee(1,1),Tn9TtGa(1),
     *       Tn9TtVpUp(1),Tn9TtWt(1),Tn9TtVpDo(1),
     *       Tn9Mask(1,1),Tn9CcGas(1,1),
     *       Tn9CfMasGasMin(1)/7.5d-7/,
     *       Tn9CcUp(1,1),Tn9FlwHUp(1,1),Tn9FlwHDo(1,1),Tn9GSepSum(1),
     *       Tn9GFlwWtSum(1),Tn9MasGaRealk(1),Tn9dEInt(1,1),
     *       Tn9GCorr(1,1),Tn9drdp(1,1),Tn9drdh(1,1),Tn9vk(1,1),
     *       Tn9Waste_T(20,1),Tn9Dmint(1),Tn9MasWtUp(1),Tn9MasWtJa(1),
     >Tn9RoDo(1),
     *       Tn9WlUpQqOut(1),Tn9WlDoQqOut(1),Tn9TcwAlCulc(1,1),
     *       Tn9TtWlUp(1),Tn9TtWlDo(1),Tn9LaWl(1),
     *       Tn9QqWlEnvUp(1),Tn9QqWlEnvDo(1),Tn9RoomUpInd(1),
     >Tn9RoomDoInd(1),
     *       Tn9CcVpDo(1),Tn9pmax(1),Tn9TtUp(1),Tn9AlSqPhWl(6,1),
     >Tn9AlWlUpDo(1),
     *       Tn9TtSatWt_(1),Tn9Levadd(1),Tn9vol(1),Tn9VolCorrUp(1),
     >Tn9VolCorrDo(1),
     *       Tn9PressCorr(1),Tn9VolGa(1),Tn9FlwIntCoef(1),
     >Tn9VolVpUpLim(1),
     *       Tn9LevTunG(1),Tn9LevTunPpRegG(6,1)

      integer*4 Tn9FlwNum(1)
      logical*1 Tn9CcBorLb(1),Tn9VpInFlag(1),Tn9BorFl(1),
     >Tn9EntOut_Key(1),
     *          Tn9FlagGCorr(1),Tn9FlagGCorrDn(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 6150 

C     common/kts_hC     common/kts_hC     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 6294 


****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
      integer*4 Ntur1,Tr1Lin(1),tr2RLinTyp(1)
      parameter(ntur1=0)
      real*8    tr2FlwKsi1(1)
      real*8 Tr1Oin(1),Tr1Qsd(1),Tr1NdP(1),Tr1genTyp(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 6306 
      integer*4 Ntur3
      parameter(Ntur3=0)
         integer*4 Tr3beaTyp(1),Tr3beaInd(1)
      real*8    Tr3Omdl(1),Tr3Qsd(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 6312 
      integer*4 Ntur4
      parameter(Ntur4=0)
         integer*4 Tr4beaTyp(1),Tr4beaInd(1)
      real*8    Tr4Omdl(1),Tr4Qsd(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 6318 



****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'

      real*8 C1serv(nwall) !����. ��� ������� ������. ������� ���
      real*8 C2serv(nwall) !����������� ���� � ���. ��. ����� ������
      real*8 wl1_QqOut(nwall)  !��������� �������� ����� � ������� ��������
      real*8 wl1AlCulcWl(nwall)   !��������� �������� ����� � ������� ��������
      real*8 wl1_Tt_t(nwall)
      real*8 wl1_XQ(nwall),wl1_BQ(nwall)  ! �-�� � ������� ���������
      common/kts_hWALL1/ C1serv,C2serv,wl1_QqOut,wl1AlCulcWl,
     *  wl1_Tt_t,wl1_XQ,wl1_BQ
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 6338 

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 6352 

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 6358 

****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'

C     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 6378 

      integer*4 Nwall2
      parameter ( Nwall2=0 )
      real*8 wl2_QqOut(1)  !��������� �������� ����� � ������� ��������
      real*8 Wl2Tt_t(1)
      real*8 WL2mass(1),WL2Cp(1),WL2Tt(1)
      real*8 Wl2LamdC(1)
      integer*4 Wl2IndRoom(1),Wl2Ndet(1),Wl2Node(1)
      real*8 Wl2QqEnv(1),Wl2AlCulcWl2(1)
      real*8 Wl2Leff(1)
      real*8 Wl2Rb(1)
      real*8 Wl2Raw(1), Wl2SqEnv(1), Wl2QqBout(1), Wl2DOut(1)
      real*8 Wl2_XQ(1),Wl2_BQ(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 6393 

****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
      integer*4 Nwlcon2
      parameter ( Nwlcon2=0 )
      integer*4 WLC2Iwl(1)
      real*8 WLC2_Qq(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 6402 




****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
      
      kts_h_it=.true. !�������, ��� ���� ���������� ��������

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 6413 

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 6417 

      deltat=deltat1
      call kts_h_conInQ
c   ������ ��������
!      IC=0
!      go=.true.
!      DO while(go.and.(IC.lt.MaxIternew(1)))
c ���������� �� 1, ����������� ������ �� 0 ��������!!!!
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 6434 
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 6439 
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 6445 
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 6479 
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 6513 
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 6522 
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 6528 
c
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 6599 
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 6669 

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 6693 

      call FlwRes_new(nv,Gk,Rovk,FlwLl,
     &             FlwRg,FlwSq,FlwDd,FlwCon,
     &             FlwRes_Iter,FlwRes0,FlwResTyp,FlwResDel,
     &             FlwVs,FlwRoFrom,FlwRoTo,
     &             np,VNKZ,PumRes,
     &             np2,Pm2Nb,Pm2Res,     
     &             nFan,FanFlw,FanRes,
     &             nFan2,Fan2Flw,Fan2Res,
     &             np4,Pm4Nb,Pm4Res,     
     &             FlwSumRes,FlwResCf,FlwKeyVp,
     &             FlwXxBalFrom,FlwXxBalTo,
     &             nf,FlwRedGa,FlwADd,FlwASq2,
     &             Rov0,LbResRo
     &            ,FlwType,FlwRe,Flwltr,FlwKform
     &            ,FlwCfResL
     &            ,FlwResFric,FlwResAcc,FlwResLoc
     &            ,FlwdPResdG,FlwdltrdG
     &            ,FlwdPFricdG,FlwFlowType
     &            ,FlwdPLocdG
     &            ,FlwBndLam,FlwBndTur,FlwdPRes
     &            ,FlwResTur,FlwResLam,Flwksi2,Flwksi1
     &            ,GzIni,FlwRo0,FlwksiSAR
     &            ,LinIBNfro,LinIBNto      
     &            ,FlwSqMin
     &            ,ComGamFrom,ComGamTo
     &            ,IFROKZ,ITONKZ
     &            ,Ntur1,Tr1Lin,tr2RLinTyp
     &            ,FlwCfSum,Tr2Flwksi1,FlwRen)




      do i = 1,nv ! ������ �������������
       FlwRes_tmp = FlwRes(i)+FlwRes_Iter(i)
       if(FlwRes_tmp.le.1d-25) then
         FlwCon(i)=1d25
       else
         FlwCon(i)=1d0/FlwRes_tmp
       endif
      enddo
      do i=1,nfwv ! ��������� ������������� ��� ������� �������
        FlwCon(FWVFlowW(i))=FlwCon(FWVFlowW(i))*.25
        FlwCon(FWVFlowV(i))=FlwCon(FWVFlowV(i))*.25
      enddo

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 6848 
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 6857 
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 6863 

      ICint=0
      goint=.true.
      time_tmp = time_df1()
        call Node_Vol(nu,Npres,VOLUKZ,NodVolAl(1),NodVolPp0(1),
     *                Pk,NodRelP,NodVolk,NoddVdPk,NodVolPp1(1),
     *                NodFast(1))
      tick(46,1) = time_df1()-time_tmp
      ErrorNum(1)=46

      if(ic.lt.ItIntStart(1)) goint=.false.
      DO while(GoInt.and.(MaxIterInt(1).ne.0))
c --- ���������� �������� ---                         !Internal
      InternalIter(1)=.true.

      call Clr_Y(nv,FlwdPdGPm)
c      call Clr_Y(nv,FlwdPPm)
      FlwdPPm=FlwdPdop
      time_tmp = time_df1()
c        call correctkv(nv,LinIbnFro,LinIbnTo
c     *,ifrokz,itonkz,FlwCon,Tn3Volk,n03,FlwCfCorCon,FlwCon,Tn3Ne)
      tick(47,1) = time_df1()-time_tmp
      ErrorNum(1)=47
        if((SrvLb1(1).ne.0).and.(icint.ne.0)) then ! ������� �� �����������
      time_tmp = time_df1()
        call Node_GgSum(nu,nv,Rok,NodRoMasPos,Gk,IFROKZ,ITONKZ,
     *                  LinIbnFro,LinIbnTo,NodGgSum,NodGgSumIn,
     *                  dtInt,NodVolk,NodSum1,NodSum2,FlwC7work3,
     >FlwC7work4,
     *                  dRoMas,FlwC7work5,NodRoMas,FlwRoMas,ROVZ,
     *                  InternalIter,FlwType)
      tick(48,1) = time_df1()-time_tmp
      ErrorNum(1)=48
        endif
      time_tmp = time_df1()

**************************************************
      time_tmp = time_df1()
      call clr_FlwKge(FlwKge,nf,nv,LinIbnFro,LinIbnTo)
      tick(54,1) = time_df1()-time_tmp
      ErrorNum(1)=54
      time_tmp = time_df1()
      call Tn9_kG(Gk,Tn9Lev,FlwKgp,FlwKgg,FlwKge
     *,Tn9Mask,nv,ifrokz
     *,itonkz,n09,Tn9CcGas,LinIbnFro,LinIbnTo,nu
     *,NodBodyTyp,Tn9Pk,Tn9Rok
     *,Tn9LevMask,dtint,Tn9CcUp,FlwXxFrom,FlwXxTo,nf
     *,FlwHTnFromDo,FlwHTnFromUp,FlwHTnToDo,FlwHTnToUp
     *,Tn9LevRelax,Tn9EvDo
     *,Tn9Ne,n03,Tn3LevRelax
     *,Tn9Volk
     *,FlwType,FlwCfGgJa,FlwGgVel,ComGamFrom,ComGamTo,FlwRoFrom,FlwRoTo
     *,NodXx,Tn8XxWt,Tn7Xk,n08,n07,Tn9GSepSum
     *,FlwGaSlFrom,FlwGaSlTo,Tn9GFlwWtSum,Tn9MasGaRealk
     *,Tn9drdp,FlwdRodPFrom,FlwdRodPTo,Tn9Vk
     *,Tn9drdh,FlwCfAdFrom,FlwCfAdTo
     *,FlwRoFromSt,FlwRoToSt,FlwCcGgWtJa
     *,FlwdRodPSndFrom,FlwdRodPSndTo
     *,FlwVpInFlagFrom,FlwVpInFlagTo
     *,Tn9Vol,Tn9VolCorrUp,Tn9VolCorrDo
     *,FlwFromSoc,FlwToSoc,FlwSp)
      tick(62,1) = time_df1()-time_tmp
      ErrorNum(1)=62

      time_tmp = time_df1()
      call Tn3_kG(Gk,Tn3Lev,FlwKgp,FlwKgg,FlwKge
     *,Tn3Mask,nv,ifrokz
     *,itonkz,n03,Tn3CcGas,LinIbnFro,LinIbnTo,nu
     *,NodBodyTyp,Tn3Pk,Tn3Rok
     *,Tn3LevMask,dtint,Tn3CcUp,FlwXxFrom,FlwXxTo,nf
     *,FlwHTnFromDo,FlwHTnFromUp,FlwHTnToDo,FlwHTnToUp
     *,Tn3LevRelax,Tn3EvDo
     *,Tn3Ne,n09,Tn9LevRelax
     *,Tn3Volk,FlwType
     *,ComGamFrom,ComGamTo,FlwRoFrom,FlwRoTo
     *,NodXx,Tn8XxWt,Tn7Xk,n08,n07
     *,FlwGaSlFrom,FlwGaSlTo,Tn3MasGaRealk
     *,Tn3drdp,FlwdRodPFrom,FlwdRodPTo,Tn3Vk
     *,Tn3drdh,FlwCfAdFrom,FlwCfAdTo
     *,FlwRoFromSt,FlwRoToSt,FlwCcGgWtJa
     *,FlwdRodPSndFrom,FlwdRodPSndTo
     *,FlwVpInFlagFrom,FlwVpInFlagTo
     *,FlwFromSoc,FlwToSoc,FlwSp)
      tick(61,1) = time_df1()-time_tmp
      ErrorNum(1)=61
*****************************************

         call ABconsqrt(nv,nu,Npres,n08,n03,
     *                 FlwCon,C7,AconFrom,AconTo,Bcon,
     *                 FlwType,Flwsq,FlwSqMin,Gk,
     *                 LinIBNfro,LinIBNto,NodRelP,Tn8RelP,
     *                 Tn3RelP,
     *                 FlwdPdGPm,FlwGrdPp,FlwRoFrom,FlwRoTo,
     *                 IFROKZ,ITONKZ,Pk,Flwsq2,
     *                 rovk,FlwGgVel,
     *                 InternalIter,flwbcon,
     *                 FlwLnSq,atau,Gz,FlwdPStat,FlwDpPm,FlwKsi1,
     *                 FlwResTyp,
     *                 FlwC7work1,FlwC7work2,FlwC7work3,FlwC7work4,
     *                 n07,Tn7RelP,
     *                 rovz,n09,Tn9RelP,
     *                 FlwGgVeCor1,FlwCorAccel,
     *                 NodGgSum,NodGgSumIn,aconrovk,
     *                 FlwCfTime,FlwdRodPFrom,FlwdRodPTo,
     *                 Flwcfadfrom,FlwcfadTo,FlwC7work7,ic,
     *                 flwdcon,flwfrco,Tn7dcon
     *,FlwHTnFromDo,FlwHTnToDo,Tn7Lev,FlwTnSqFrom,FlwTnSqTo
     *,FlwCorSound,Gzz,flwsound
     *,FlwdRodPSndFrom,FlwdRodPSndTo,Gk_ext,Gk_INT,FlwCondRegul
     *,Tn3FlwIntCoef,Tn9FlwIntCoef,FlwCondRegul1
     *,FlwCondRegul0,MasdtIntegr_Ga,MasdtIntegr_Wv
     *,NodMasGaMax,NodMasWvMax,NodRo,Volukz,dtint
     *,NodGgCompGas,NodGgCompWvp
     *,HCOFlMod,HCOCfMod,HCOOFg,HCOONodVol,HCOONoddRodP,NHCO,HCOIndFlw,
     >Pzt)

      tick(49,1) = time_df1()-time_tmp
      ErrorNum(1)=49
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 7109 

c   ��������� ��������� ������� Dmp (����� ������� ���������)
      time_tmp = time_df1()
c        call SetMusor8(Dmp,Npres*Npres)
        call CLR_S_e(Dmp,Npres,ILNZp,INZSUBp,NZSUBp,PIntIter)
      tick(52,1) = time_df1()-time_tmp
      ErrorNum(1)=52

      time_tmp = time_df1()
c        call SetMusor8(Yp,Npres)
        call clr_Y(Npres,Yp) !��������� ������ ������ ��� ��������
      tick(53,1) = time_df1()-time_tmp
      ErrorNum(1)=53

* ���������������� �������  ��� Tn3 � Tn9
****** Begin section 'call_PreNp4Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'call_PreNp4Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'call_PreNp4Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'call_PreNp4Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'call_PreNp4Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'call_PreNp4Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'call_PreNp4Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'call_PreNp4Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'call_PreNp4Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'call_PreNp4Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'call_PreNp4Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'call_PreNp4Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'call_PreNp4Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'call_PreNp4Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'call_PreNp4Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'call_PreNp4Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'call_PreNp4Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'call_PreNp4Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'call_PreNp4Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'call_PreNp4Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'call_PreNp4Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'call_PreNp4Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'call_PreNp4Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'call_PreNp4Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'call_PreNp4Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'call_PreNp4Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'call_PreNp4Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'call_PreNp4Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'call_PreNp4Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'call_PreNp4Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'call_PreNp4Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'call_PreNp4Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'call_PreNp4Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'call_PreNp4Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'call_PreNp4Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'call_PreNp4Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'call_PreNp4Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'call_PreNp4Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'call_PreNp4Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'call_PreNp4Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'call_PreNp4Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'call_PreNp4Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'call_PreNp4Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 7260 


****** End   section 'call_PreNp4Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'call_PreNp4Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'call_PreNp4Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'call_PreNp4Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'call_PreNp4Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'call_PreNp4Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'call_PreNp4Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'call_PreNp4Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'call_PreNp4Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'call_PreNp4Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'call_PreNp4Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'call_PreNp4Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 7334 


****** End   section 'call_PreNp4Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'call_PreNp4Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'call_PreNp4Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'call_PreNp4Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'call_PreNp4Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'call_PreNp4Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'call_PreNp4Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'call_PreNp4Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'call_PreNp4Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'call_PreNp4Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'call_PreNp4Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'call_PreNp4Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'call_PreNp4Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'call_PreNp4Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'call_PreNp4Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'call_PreNp4Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'call_PreNp4Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'call_PreNp4Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'call_PreNp4Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

c   ������������ ������� ��������� ��� ��������       !Internal 
****** Begin section 'CALL_NP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'CALL_NP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'CALL_NP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'CALL_NP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'CALL_NP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'CALL_NP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'CALL_NP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'CALL_NP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'CALL_NP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'CALL_NP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'CALL_NP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'CALL_NP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'CALL_NP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'CALL_NP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'CALL_NP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'CALL_NP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'CALL_NP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'CALL_NP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'CALL_NP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'CALL_NP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'CALL_NP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'CALL_NP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'CALL_NP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'CALL_NP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'CALL_NP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'CALL_NP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'CALL_NP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'CALL_NP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'CALL_NP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'CALL_NP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'CALL_NP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'CALL_NP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'CALL_NP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'CALL_NP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'CALL_NP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'CALL_NP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'CALL_NP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'CALL_NP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'CALL_NP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'CALL_NP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'CALL_NP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'CALL_NP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'CALL_NP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 7424 


****** End   section 'CALL_NP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'CALL_NP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'CALL_NP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'CALL_NP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'CALL_NP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'CALL_NP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'CALL_NP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'CALL_NP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'CALL_NP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'CALL_NP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'CALL_NP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'CALL_NP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 7452 


****** End   section 'CALL_NP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'CALL_NP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'CALL_NP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'CALL_NP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'CALL_NP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'CALL_NP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'CALL_NP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'CALL_NP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'CALL_NP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'CALL_NP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'CALL_NP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'CALL_NP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'CALL_NP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'CALL_NP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'CALL_NP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'CALL_NP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'CALL_NP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'CALL_NP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'CALL_NP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

****** Begin section 'CALL_GP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'CALL_GP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'CALL_GP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'CALL_GP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'CALL_GP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'CALL_GP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'CALL_GP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'CALL_GP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'CALL_GP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'CALL_GP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'CALL_GP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'CALL_GP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'CALL_GP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'CALL_GP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'CALL_GP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'CALL_GP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'CALL_GP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'CALL_GP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'CALL_GP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'CALL_GP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'CALL_GP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'CALL_GP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'CALL_GP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'CALL_GP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'CALL_GP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'CALL_GP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'CALL_GP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'CALL_GP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'CALL_GP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'CALL_GP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'CALL_GP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'CALL_GP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'CALL_GP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'CALL_GP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'CALL_GP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'CALL_GP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'CALL_GP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'CALL_GP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'CALL_GP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'CALL_GP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'CALL_GP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'CALL_GP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'CALL_GP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 7531 


****** End   section 'CALL_GP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'CALL_GP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'CALL_GP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'CALL_GP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'CALL_GP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'CALL_GP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'CALL_GP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'CALL_GP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'CALL_GP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'CALL_GP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'CALL_GP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'CALL_GP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 7557 


****** End   section 'CALL_GP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'CALL_GP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'CALL_GP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'CALL_GP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'CALL_GP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'CALL_GP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'CALL_GP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'CALL_GP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'CALL_GP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'CALL_GP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'CALL_GP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'CALL_GP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'CALL_GP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'CALL_GP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'CALL_GP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'CALL_GP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'CALL_GP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'CALL_GP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'CALL_GP4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

c   ���������� �������� ��� �������� (dP - ������ ��������)
      time_tmp = time_df1()
        call Clr_Y(Npres,dP)
      tick(55,1) = time_df1()-time_tmp
      ErrorNum(1)=55
      time_tmp = time_df1()
c        call ERRA(Dmp,Npres,ILNZp,INZSUBp,NZSUBp,InvperP,PIntIter,1)
        call LINE_S_e(Npres,Dmp,Yp,dP,ILNZp,INZSUBp,NZSUBp,PIntIter)
      tick(56,1) = time_df1()-time_tmp
      ErrorNum(1)=56

c   ��������� ��������                                !Internal
      time_tmp = time_df1()
        CALL CorrectP1(Npres,nu,n08,n03,
     *                NodTyp,Pk,dP,Pk1,invperp,
     *                NodRelP,Tn8RelP,Tn3RelP,
     *                Tn3Pk,Tn3dp,Tn3CfPp,n07,Tn7RelP,Tn7Pk,
     *                n09,Tn9RelP,Tn9Pk,Tn9dp,Tn9CfPp,NodCfPp,
     *                Tn7CfPp,ic,NodCfPpUp,
     *                Tn3pmax,Tn9pmax,Tn9Pp,Tn3Pp,Tn9CfRelax(1),
     *                InternalIter,NodCfPpType)

      tick(57,1) = time_df1()-time_tmp
      ErrorNum(1)=57

C   ���������� �������� �� ������ ����������������� ��������
      time_tmp = time_df1()
        CALL Flowssqrt(nv,nu,n08,n03,
     *             Npres,Gk,Bcon,Pk1,
     *             IFROKZ,ITONKZ,FlwType,c7,
     *             LinIBNfro,LinIBNto,
     *             NodRelP,Tn8RelP,Tn3RelP,
     *             InternalIter,
     *             flwbcon,
     *             FlwC7work3,FlwC7work4,
     *             n07,Tn7RelP,n09,Tn9RelP,
     *             FlwdPLevFrom,FlwdPLevTo,FlwPpFrom,FlwPpTo,FlwGrdPp,
     *             Gk_int,Gk_ext,
     *             Tn3FlwIntCoef,Tn9FlwIntCoef)


         call Correctp2(Npres,nu,NodTyp,Pk,dP,Pk1,invperP,NodRelP,
     *      NodPp,Tn9CfRelax(1))

      tick(58,1) = time_df1()-time_tmp
      ErrorNum(1)=58

      time_tmp = time_df1()
      tick(60,1) = time_df1()-time_tmp
      ErrorNum(1)=60

*******************************
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 7633 

c   ------------------------------------------------------------------
c   ��������� ��������� ������� Dmh (����� ������� ���������)
c        call CLR_S(Dmh, Nent, ILNZe, INZSUBe, NZSUBe)
      time_tmp = time_df1()
c        call SetMusor8(Dmh,Nent*Nent)  !!!�����
        call CLR_S_e(Dmh,Nent,ILNZe,INZSUBe,NZSUBe,EIntIter)
      tick(63,1) = time_df1()-time_tmp
      ErrorNum(1)=63
      time_tmp = time_df1()
        call clr_Y(Nent,Ye)  !��������� ������ ������ ��� ���������
      tick(64,1) = time_df1()-time_tmp
      ErrorNum(1)=64

****** Begin section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 7683 


****** End   section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 7700 


****** End   section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
c   ��������� ��������� �������� ������� �� ����. ���. Wl2
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 7748 

****** End   section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

c   ������������ ������� ��������� ��� ���������       !Internal 
****** Begin section 'CALL_NH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'CALL_NH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'CALL_NH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'CALL_NH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'CALL_NH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'CALL_NH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'CALL_NH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'CALL_NH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'CALL_NH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'CALL_NH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'CALL_NH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'CALL_NH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'CALL_NH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'CALL_NH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'CALL_NH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'CALL_NH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'CALL_NH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'CALL_NH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'CALL_NH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'CALL_NH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'CALL_NH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'CALL_NH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'CALL_NH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'CALL_NH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'CALL_NH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'CALL_NH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'CALL_NH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'CALL_NH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'CALL_NH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'CALL_NH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'CALL_NH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'CALL_NH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'CALL_NH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'CALL_NH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'CALL_NH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'CALL_NH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'CALL_NH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'CALL_NH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'CALL_NH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'CALL_NH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'CALL_NH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'CALL_NH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'CALL_NH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 7897 


****** End   section 'CALL_NH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'CALL_NH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'CALL_NH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'CALL_NH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'CALL_NH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'CALL_NH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'CALL_NH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'CALL_NH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'CALL_NH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'CALL_NH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'CALL_NH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'CALL_NH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 7998 


****** End   section 'CALL_NH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'CALL_NH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'CALL_NH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'CALL_NH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'CALL_NH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'CALL_NH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'CALL_NH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'CALL_NH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'CALL_NH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'CALL_NH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'CALL_NH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'CALL_NH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'CALL_NH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'CALL_NH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'CALL_NH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'CALL_NH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'CALL_NH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'CALL_NH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'CALL_NH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 8026 

c   ������������ ������� ��������� ��� ���������       !Internal 
****** Begin section 'CALL_GH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'CALL_GH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'CALL_GH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'CALL_GH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'CALL_GH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'CALL_GH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'CALL_GH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'CALL_GH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'CALL_GH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'CALL_GH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'CALL_GH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'CALL_GH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'CALL_GH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'CALL_GH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'CALL_GH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'CALL_GH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'CALL_GH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'CALL_GH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'CALL_GH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'CALL_GH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'CALL_GH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'CALL_GH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'CALL_GH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'CALL_GH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'CALL_GH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'CALL_GH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'CALL_GH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'CALL_GH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'CALL_GH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'CALL_GH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'CALL_GH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'CALL_GH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'CALL_GH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'CALL_GH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'CALL_GH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'CALL_GH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'CALL_GH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'CALL_GH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'CALL_GH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'CALL_GH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'CALL_GH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'CALL_GH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'CALL_GH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 8087 


****** End   section 'CALL_GH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'CALL_GH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'CALL_GH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'CALL_GH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'CALL_GH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'CALL_GH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'CALL_GH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'CALL_GH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'CALL_GH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'CALL_GH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'CALL_GH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'CALL_GH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 8117 


****** End   section 'CALL_GH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'CALL_GH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'CALL_GH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'CALL_GH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'CALL_GH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'CALL_GH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'CALL_GH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'CALL_GH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'CALL_GH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'CALL_GH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'CALL_GH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'CALL_GH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'CALL_GH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'CALL_GH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'CALL_GH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'CALL_GH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'CALL_GH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'CALL_GH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'CALL_GH4_INT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

c   ������ ������� � ��������� ������� ��� ��������� � � ������ �������
c   �� ����������, ��������� � ������������ ����� ������ ��� �������
c   �����
c        call Wl1_HhEqv(nu,Nwall,Nent,Wl1Node,Wl1Mass,
c     *                 Wl1AlNode,Wl1Sq,
c     *               NodCpk,NodXxBal,Ek,NodTtk,Wl1Cp,Wl1Tt,
c     *               Dmh,Ye,C1serv,C2serv,invperE,Wl1TtEnv,Wl1AlEnv,
c     *               Wl1SqEnv,
c     *               IC,MaxIternew(1),Wl1decr_alf,
c     *               falfa,falfa_e)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 8151 

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 8159 


c   ���������� �������� ��� ��������� (dE - ������ ��������)
      time_tmp = time_df1()
        call Clr_Y(Nent,dE)
      tick(67,1) = time_df1()-time_tmp
      ErrorNum(1)=67
      time_tmp = time_df1()
        call ERRA(Dmh,Nent,ILNZe,INZSUBe,NZSUBe,InvperE,EIntIter,2)
        call LINE_S_e(Nent,Dmh,Ye,dE,ILNZe,INZSUBe,NZSUBe,EIntIter)
      tick(68,1) = time_df1()-time_tmp
      ErrorNum(1)=68

c   ��������� ���������                           !Internal
***************************************************************
****** Begin section 'CorH_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'CorH_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'CorH_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'CorH_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'CorH_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'CorH_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'CorH_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'CorH_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'CorH_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'CorH_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'CorH_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'CorH_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'CorH_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'CorH_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'CorH_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'CorH_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'CorH_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'CorH_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'CorH_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'CorH_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'CorH_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'CorH_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'CorH_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'CorH_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'CorH_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'CorH_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'CorH_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'CorH_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'CorH_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'CorH_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'CorH_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'CorH_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'CorH_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'CorH_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'CorH_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'CorH_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'CorH_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'CorH_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'CorH_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'CorH_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'CorH_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'CorH_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'CorH_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 8225 


****** End   section 'CorH_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'CorH_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'CorH_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'CorH_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'CorH_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'CorH_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'CorH_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'CorH_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'CorH_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'CorH_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'CorH_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'CorH_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 8248 


****** End   section 'CorH_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'CorH_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'CorH_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'CorH_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'CorH_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'CorH_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'CorH_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'CorH_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'CorH_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'CorH_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'CorH_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'CorH_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'CorH_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'CorH_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'CorH_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'CorH_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'CorH_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'CorH_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'CorH_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** Begin section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 8322 


****** End   section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 8344 


****** End   section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

c *******************************************************************
c     �������� ����, �������, ���������� �������
****** Begin section 'Mas_Vol_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'Mas_Vol_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'Mas_Vol_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'Mas_Vol_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'Mas_Vol_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'Mas_Vol_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'Mas_Vol_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'Mas_Vol_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'Mas_Vol_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'Mas_Vol_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'Mas_Vol_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'Mas_Vol_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'Mas_Vol_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'Mas_Vol_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'Mas_Vol_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'Mas_Vol_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'Mas_Vol_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'Mas_Vol_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'Mas_Vol_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'Mas_Vol_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'Mas_Vol_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'Mas_Vol_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'Mas_Vol_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'Mas_Vol_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'Mas_Vol_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'Mas_Vol_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'Mas_Vol_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'Mas_Vol_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'Mas_Vol_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'Mas_Vol_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'Mas_Vol_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'Mas_Vol_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'Mas_Vol_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'Mas_Vol_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'Mas_Vol_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'Mas_Vol_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'Mas_Vol_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'Mas_Vol_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'Mas_Vol_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'Mas_Vol_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'Mas_Vol_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'Mas_Vol_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'Mas_Vol_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 8484 


****** End   section 'Mas_Vol_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'Mas_Vol_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'Mas_Vol_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'Mas_Vol_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'Mas_Vol_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'Mas_Vol_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'Mas_Vol_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'Mas_Vol_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'Mas_Vol_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'Mas_Vol_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'Mas_Vol_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'Mas_Vol_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 8563 


****** End   section 'Mas_Vol_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'Mas_Vol_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'Mas_Vol_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'Mas_Vol_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'Mas_Vol_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'Mas_Vol_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'Mas_Vol_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'Mas_Vol_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'Mas_Vol_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'Mas_Vol_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'Mas_Vol_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'Mas_Vol_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'Mas_Vol_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'Mas_Vol_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'Mas_Vol_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'Mas_Vol_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'Mas_Vol_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'Mas_Vol_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'Mas_Vol_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** Begin section 'WallTemp_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'WallTemp_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'WallTemp_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'WallTemp_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'WallTemp_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'WallTemp_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'WallTemp_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'WallTemp_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'WallTemp_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'WallTemp_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'WallTemp_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'WallTemp_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'WallTemp_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'WallTemp_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'WallTemp_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'WallTemp_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'WallTemp_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'WallTemp_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'WallTemp_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'WallTemp_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'WallTemp_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'WallTemp_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'WallTemp_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'WallTemp_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'WallTemp_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'WallTemp_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'WallTemp_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'WallTemp_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'WallTemp_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'WallTemp_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'WallTemp_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'WallTemp_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'WallTemp_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'WallTemp_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'WallTemp_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'WallTemp_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'WallTemp_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'WallTemp_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'WallTemp_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'WallTemp_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'WallTemp_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'WallTemp_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'WallTemp_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 8664 


****** End   section 'WallTemp_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'WallTemp_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'WallTemp_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'WallTemp_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'WallTemp_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'WallTemp_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'WallTemp_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'WallTemp_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'WallTemp_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'WallTemp_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'WallTemp_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'WallTemp_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 8712 


****** End   section 'WallTemp_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'WallTemp_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'WallTemp_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'WallTemp_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'WallTemp_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'WallTemp_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'WallTemp_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'WallTemp_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'WallTemp_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'WallTemp_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'WallTemp_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'WallTemp_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'WallTemp_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'WallTemp_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'WallTemp_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'WallTemp_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'WallTemp_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'WallTemp_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'WallTemp_Int' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'


c   ���� �������� ����� � ������� �� ������           !Internal
      time_tmp = time_df1()
      call Volume(nv,LinIBNFro,LinIBNTo,IFROKZ,ITONKZ,
     *            HFrom,HTo,
     *            Tn8Lev,Tn3LevRel,Tn9LevRel,
     *            Tn8RoWtk,Tn3Rok(1,3),Tn9Rok(1,3),
     *            dHLev,FlwdPStat,
     *            FlwRoFrc,
     *            InternalIter,Tn3EvDo,
     *            Tn7Levk,Tn7Rok(1,2),Tn9EvDo,
     *            Tn8RoGak,Tn3Rok(1,1),Tn9Rok(1,1),
     *            Tn7Rok(1,1),Gk,aconRovk,
     *            FlwdPLevFrom,FlwdPLevTo,
     *            Flwdd,FlwAngFrom,FlwAngTo,Tn3RoDo,Tn9RoDo
     *                 ,FlwHTnFromUp,FlwHTnFromDo,FlwHTnToUp,FlwHTnToDo
     *                 ,FlwRoFrom,FlwRoTo,n08,n03,n07,n09,FlwKkdh,
     >FlwFlagm2,
     *            NodCcGa,NodRoWtSat,NodRoGa,Tn3RoWpUp,Tn9RoVpUp)
      tick(69,1) = time_df1()-time_tmp
      ErrorNum(1)=69
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 8756 
c                                                     !Internal
c   ���� ������ �������
c --- ����� ��� ������ �������
        ICint=ICint+1
      time_tmp = time_df1()
        call stopiter(goint,icint,maxiterint,npres,pk,pk1,dpstop)
      tick(71,1) = time_df1()-time_tmp
      ErrorNum(1)=71
c        call stopiter(goint,icint,maxiterint,npres,pk,pk1,dpstop,dpmaxin)
c   ���������� � ��������� ��������
      time_tmp = time_df1()
        do i=1,Npres
        if(dabs(Pk1(i)).lt.1.d-20) Pk1(i)=1.d-20
            Pk(i)=Pk1(i)
          enddo
      enddo  !����� ���������� ��������
      tick(72,1) = time_df1()-time_tmp
      ErrorNum(1)=72

c --- ����� ���������� �������� ---                   !Internal

      icint=0
      InternalIter(1)=.false.
      time_tmp = time_df1()
      call Clr_Y(nv,FlwdPdGPm)
      tick(73,1) = time_df1()-time_tmp
      ErrorNum(1)=73
      time_tmp = time_df1()
c      call Clr_Y(nv,FlwdPPm)
      FlwdPPm=FlwdPdop
      tick(74,1) = time_df1()-time_tmp
      ErrorNum(1)=74
      time_tmp = time_df1()
      call Pump1(Np,FlwRoFrom,VNKZ,DPN,DPN0,OMPN,rovk,PumKRo
     &          ,Gk,PumG0,PumPow0,PumdPow,PumPow,PumdPowdom,nv
     &          ,Nodromas,IFROKZ,nu,Pum_flag,PumNRo,NodPp,
     &       PumRes,PumRes0,PumOm0,PumResKk2,PumPowPol,PumKkOm,
     &       PumOmLoc,PumUnlin,PumTUnlin,0,dtint)
      tick(75,1) = time_df1()-time_tmp
      ErrorNum(1)=75
      time_tmp = time_df1()
      do i=1,np
        FlwdPPm(VNKZ(i))=FlwdPPm(VNKZ(i))+DPN(i)
      enddo
      tick(76,1) = time_df1()-time_tmp
      ErrorNum(1)=76

****** Begin section 'CompNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'CompNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'CompNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
c ��������� �������������� ��������� ������� ��������� 
c ��� ���������� ������������� �� �������� �����������
c

****** End   section 'CompNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'CompNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'CompNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'CompNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'CompNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'CompNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'CompNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'CompNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'CompNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'CompNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'CompNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'CompNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'CompNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'CompNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'CompNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'CompNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'CompNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'CompNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'CompNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'CompNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'CompNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'CompNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'CompNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'CompNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'CompNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'CompNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'CompNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'CompNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'CompNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'CompNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'CompNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'CompNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'CompNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'CompNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'CompNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'CompNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'CompNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'CompNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'CompNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'CompNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** End   section 'CompNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'CompNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'CompNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'CompNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'CompNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'CompNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'CompNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'CompNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'CompNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'CompNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'CompNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'CompNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** End   section 'CompNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'CompNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'CompNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'CompNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'CompNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'CompNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'CompNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'CompNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'CompNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'CompNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'CompNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'CompNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'CompNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'CompNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'CompNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'CompNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'CompNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'CompNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'CompNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'


****** Begin section 'TurPumpI' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'TurPumpI' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'TurPumpI' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'TurPumpI' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'TurPumpI' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'TurPumpI' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'TurPumpI' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'TurPumpI' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'TurPumpI' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'TurPumpI' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'TurPumpI' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'TurPumpI' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'TurPumpI' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'TurPumpI' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'TurPumpI' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'TurPumpI' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'TurPumpI' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'TurPumpI' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'TurPumpI' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'TurPumpI' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'TurPumpI' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'TurPumpI' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'TurPumpI' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'TurPumpI' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'TurPumpI' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'TurPumpI' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'TurPumpI' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'TurPumpI' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'TurPumpI' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'TurPumpI' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'TurPumpI' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'TurPumpI' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'TurPumpI' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'TurPumpI' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'TurPumpI' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'TurPumpI' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'TurPumpI' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'TurPumpI' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'TurPumpI' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'TurPumpI' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'TurPumpI' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'TurPumpI' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'TurPumpI' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** End   section 'TurPumpI' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'TurPumpI' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'TurPumpI' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'TurPumpI' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'TurPumpI' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'TurPumpI' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'TurPumpI' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'TurPumpI' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'TurPumpI' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'TurPumpI' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'TurPumpI' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'TurPumpI' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** End   section 'TurPumpI' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'TurPumpI' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'TurPumpI' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'TurPumpI' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'TurPumpI' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'TurPumpI' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'


****** End   section 'TurPumpI' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'TurPumpI' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'TurPumpI' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'TurPumpI' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'TurPumpI' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'TurPumpI' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'TurPumpI' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'TurPumpI' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'TurPumpI' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'TurPumpI' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'TurPumpI' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'TurPumpI' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'TurPumpI' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

      time_tmp = time_df1()
c        call correctkv(nv,LinIbnFro,LinIbnTo
c     *,ifrokz,itonkz,FlwCon,Tn3Volk,n03,FlwCfCorCon,FlwCon,Tn3Ne)
      tick(80,1) = time_df1()-time_tmp
      ErrorNum(1)=80
        if((SrvLb1(1).ne.0).and.(ic.ne.0)) then ! ������� �� �����������
      time_tmp = time_df1()
        call Node_GgSum(nu,nv,Rok,NodRoMasPos,Gk,IFROKZ,ITONKZ,
     *                  LinIbnFro,LinIbnTo,NodGgSum,NodGgSumIn,
     *                  dtInt,NodVolk,NodSum1,NodSum2,FlwC7work3,
     >FlwC7work4,
     *                  dRoMas,FlwC7work5,NodRoMas,FlwRoMas,ROVZ,
     *                  InternalIter,FlwType)
      tick(81,1) = time_df1()-time_tmp
      ErrorNum(1)=81
        endif
      time_tmp = time_df1()

************************************************************
      time_tmp = time_df1()
      call clr_FlwKge(FlwKge,nf,nv,LinIbnFro,LinIbnTo)
      tick(92,1) = time_df1()-time_tmp
      ErrorNum(1)=92
      time_tmp = time_df1()
      call Com_Gam_Ro(nv,nu,n08,nf,IFROKZ,ITONKZ,LinIBNto,
     *                LinIBNfro,HFrom,HTo,FlwDd,NodBodyTyp,ComGamFrom,
     *                ComGamTo,Tn8Lev,FlwAngFrom,
     *                FlwAngTo,
     *                FlwRoInUpDo,
     *                FlwCcSqUpDo,n07,Tn7Levk,Tn7Rok,Tn7CcVolk,
     >FlwRoFrom,
     *                FlwRoTo,NodRo,Tn8RoGa,Tn8RoWt,
     *                Tn7homo,Tn7CcMask,
     *                Nodccga,
     *                FlwHTnFromDo,FlwHTnFromUp,FlwHTnToDo,FlwHTnToUp,
     *                Tn7Lev_Rlx,ic,FlwRoFromSt,FlwRoToSt,
     *                FlwdRodPFrom,FlwdRodPTo,
     *                NodDroDpk,Tn7dRdPk,Tn7dRdHk,Flwcfadfrom,
     *                FlwcfadTo,Nodcfad,Tn8dRdP1,Tn8dRdH1,
     *                Tn8dRdP2,Tn8dRdH2,
     *                NodCgGa,NodCgMa,NodCgMa0,NodCgEe,NodRoGg,  
     *                nsep1,Sep1FlwS,  
     *                Tn7CfRoGaCorr,Tn7CfRoWtCorr,  
     *                ComEeFrom,ComEeTo,FlwSlGaFrom,FlwSlGaFrom0,
     *                FlwSlGaTo,FlwSlGaTo0,  
     *                NodRoWv,NodRoGa,NodEeWv,NodEeGa,
     *                Nair,AirFlwInd,FlwXxFrom,FlwXxTo,
     *                ComRoFrom, ComRoTo, 
     *                NodRoTrue,NodRoWtSat,NodRoVpSat,
     *                Tn7RoWtTrue,Tn7RoWtSat,Tn7RoVpSat,
     *                Tn8RoWtSat,Tn8RoVpSat,FlwType,
     *                AirCf,NodCgVp,EeWtSatk,EeVpSatk,NodPropsKey1,
     *                FlwdRoDpSndFrom,FlwdRoDpSndTo,NoddRoDpSnd,
     *                NodRoTot,CoolerType,
     *                FlwdCgdCcFrom,FlwdCgdCcTo,NoddCgdCc,NodNumEe(1),
     *                FlwFromSoc,FlwToSoc,FlwSlip
     *                     ,nfwv,FWVFLOWW,FWVFLOWV,NodXxk1,NodXx,
     >NodCcGat
     *             ,FwvRoFrom1,FwvRoFrom2,FwvRoTo1,FwvRoTo2
     *             ,nfwv1,Fwv1Flow,NodEv1)

      tick(99,1) = time_df1()-time_tmp
      ErrorNum(1)=99

      time_tmp = time_df1()
      call Tn9_kG(Gk,Tn9Lev,FlwKgp,FlwKgg,FlwKge
     *,Tn9Mask,nv,ifrokz
     *,itonkz,n09,Tn9CcGas,LinIbnFro,LinIbnTo,nu
     *,NodBodyTyp,Tn9Pk,Tn9Rok
     *,Tn9LevMask,dtint,Tn9CcUp,FlwXxFrom,FlwXxTo,nf
     *,FlwHTnFromDo,FlwHTnFromUp,FlwHTnToDo,FlwHTnToUp
     *,Tn9LevRelax,Tn9EvDo
     *,Tn9Ne,n03,Tn3LevRelax
     *,Tn9Volk
     *,FlwType,FlwCfGgJa,FlwGgVel,ComGamFrom,ComGamTo,FlwRoFrom,FlwRoTo
     *,NodXx,Tn8XxWt,Tn7Xk,n08,n07,Tn9GSepSum
     *,FlwGaSlFrom,FlwGaSlTo,Tn9GFlwWtSum,Tn9MasGaRealk
     *,Tn9drdp,FlwdRodPFrom,FlwdRodPTo,Tn9Vk
     *,Tn9drdh,FlwCfAdFrom,FlwCfAdTo
     *,FlwRoFromSt,FlwRoToSt,FlwCcGgWtJa
     *,FlwdRodPSndFrom,FlwdRodPSndTo
     *,FlwVpInFlagFrom,FlwVpInFlagTo
     *,Tn9Vol,Tn9VolCorrUp,Tn9VolCorrDo
     *,FlwFromSoc,FlwToSoc,FlwSp)

      time_tmp = time_df1()
      call Tn3_kG(Gk,Tn3Lev,FlwKgp,FlwKgg,FlwKge
     *,Tn3Mask,nv,ifrokz
     *,itonkz,n03,Tn3CcGas,LinIbnFro,LinIbnTo,nu
     *,NodBodyTyp,Tn3Pk,Tn3Rok
     *,Tn3LevMask,dtint,Tn3CcUp,FlwXxFrom,FlwXxTo,nf
     *,FlwHTnFromDo,FlwHTnFromUp,FlwHTnToDo,FlwHTnToUp
     *,Tn3LevRelax,Tn3EvDo
     *,Tn3Ne,n09,Tn9LevRelax
     *,Tn3Volk,FlwType
     *,ComGamFrom,ComGamTo,FlwRoFrom,FlwRoTo
     *,NodXx,Tn8XxWt,Tn7Xk,n08,n07
     *,FlwGaSlFrom,FlwGaSlTo,Tn3MasGaRealk
     *,Tn3drdp,FlwdRodPFrom,FlwdRodPTo,Tn3Vk
     *,Tn3drdh,FlwCfAdFrom,FlwCfAdTo
     *,FlwRoFromSt,FlwRoToSt,FlwCcGgWtJa
     *,FlwdRodPSndFrom,FlwdRodPSndTo
     *,FlwVpInFlagFrom,FlwVpInFlagTo
     *,FlwFromSoc,FlwToSoc,FlwSp)
      tick(100,1) = time_df1()-time_tmp
      ErrorNum(1)=100
**************************************************



         call ABconsqrt(nv,nu,Npres,n08,n03,
     *                 FlwCon,C7,AconFrom,AconTo,Bcon,
     *                 FlwType,Flwsq,FlwSqMin,Gk,
     *                 LinIBNfro,LinIBNto,NodRelP,Tn8RelP,
     *                 Tn3RelP,
     *                 FlwdPdGPm,FlwGrdPp,FlwRoFrom,FlwRoTo,
     *                 IFROKZ,ITONKZ,Pk,Flwsq2,
     *                 rovk,FlwGgVel,
     *                 InternalIter,flwbcon,
     *                 FlwLnSq,atau,Gz,FlwdPStat,FlwDpPm,FlwKsi1,
     *                 FlwResTyp,
     *                 FlwC7work1,FlwC7work2,FlwC7work3,FlwC7work4,
     *                 n07,Tn7RelP,
     *                 rovz,n09,Tn9RelP,
     *                 FlwGgVeCor1,FlwCorAccel,
     *                 NodGgSum,NodGgSumIn,aconrovk,
     *                 FlwCfTime,FlwdRodPFrom,FlwdRodPTo,
     *                 Flwcfadfrom,FlwcfadTo,FlwC7work7,ic,
     *                 flwdcon,flwfrco,Tn7dcon
     *,FlwHTnFromDo,FlwHTnToDo,Tn7Lev,FlwTnSqFrom,FlwTnSqTo
     *,FlwCorSound,Gzz,flwsound
     *,FlwdRodPSndFrom,FlwdRodPSndTo,Gk_ext,Gk_INT,FlwCondRegul
     *,Tn3FlwIntCoef,Tn9FlwIntCoef,FlwCondRegul1
     *,FlwCondRegul0,MasdtIntegr_Ga,MasdtIntegr_Wv
     *,NodMasGaMax,NodMasWvMax,NodRo,Volukz,dtint
     *,NodGgCompGas,NodGgCompWvp
     *,HCOFlMod,HCOCfMod,HCOOFg,HCOONodVol,HCOONoddRodP,NHCO,HCOIndFlw,
     >Pzt)

      tick(82,1) = time_df1()-time_tmp
      ErrorNum(1)=82

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 9361 

c   ��������� ��������� ������� Dmp (����� ������� ���������)
      time_tmp = time_df1()
c        call SetMusor8(Dmp,Npres*Npres)
        call CLR_S_e(Dmp,Npres,ILNZp,INZSUBp,NZSUBp,PExtIter_)
      tick(85,1) = time_df1()-time_tmp
      ErrorNum(1)=85
      time_tmp = time_df1()
        call clr_Y(Npres,Yp) !��������� ������ ������ ��� ��������
      tick(86,1) = time_df1()-time_tmp
      ErrorNum(1)=86

****** Begin section 'call_PreNp4Ext' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'call_PreNp4Ext' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'call_PreNp4Ext' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'call_PreNp4Ext' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'call_PreNp4Ext' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'call_PreNp4Ext' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'call_PreNp4Ext' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'call_PreNp4Ext' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'call_PreNp4Ext' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'call_PreNp4Ext' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'call_PreNp4Ext' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'call_PreNp4Ext' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'call_PreNp4Ext' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'call_PreNp4Ext' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'call_PreNp4Ext' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'call_PreNp4Ext' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'call_PreNp4Ext' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'call_PreNp4Ext' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'call_PreNp4Ext' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'call_PreNp4Ext' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'call_PreNp4Ext' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'call_PreNp4Ext' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'call_PreNp4Ext' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'call_PreNp4Ext' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'call_PreNp4Ext' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'call_PreNp4Ext' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'call_PreNp4Ext' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'call_PreNp4Ext' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'call_PreNp4Ext' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'call_PreNp4Ext' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'call_PreNp4Ext' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'call_PreNp4Ext' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'call_PreNp4Ext' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'call_PreNp4Ext' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'call_PreNp4Ext' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'call_PreNp4Ext' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'call_PreNp4Ext' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'call_PreNp4Ext' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'call_PreNp4Ext' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'call_PreNp4Ext' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'call_PreNp4Ext' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'call_PreNp4Ext' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'call_PreNp4Ext' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 9497 



****** End   section 'call_PreNp4Ext' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'call_PreNp4Ext' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'call_PreNp4Ext' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'call_PreNp4Ext' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'call_PreNp4Ext' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'call_PreNp4Ext' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'call_PreNp4Ext' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'call_PreNp4Ext' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 9529 


****** End   section 'call_PreNp4Ext' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'call_PreNp4Ext' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'call_PreNp4Ext' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'call_PreNp4Ext' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 9592 


****** End   section 'call_PreNp4Ext' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'call_PreNp4Ext' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'call_PreNp4Ext' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'call_PreNp4Ext' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'call_PreNp4Ext' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'call_PreNp4Ext' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'call_PreNp4Ext' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'call_PreNp4Ext' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'call_PreNp4Ext' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'call_PreNp4Ext' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'call_PreNp4Ext' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'call_PreNp4Ext' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'call_PreNp4Ext' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'call_PreNp4Ext' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'call_PreNp4Ext' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'call_PreNp4Ext' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'call_PreNp4Ext' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'call_PreNp4Ext' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'call_PreNp4Ext' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

c   ��������������� ������� ��� ������� �����
      time_tmp = time_df1()
        call Node_Srv1(nv,nu,IFROKZ,ITONKZ,Gk,LinIBNto,
     *                 LinIBNfro,Vol1,NoddRodE,NodRo,NodCfSrv1,
     *                 NodCfdE)
      tick(93,1) = time_df1()-time_tmp
      ErrorNum(1)=93

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 9624 
c ---  ������������ ������� ��������� ��� �������� ---

c ---  ���������� ������������ ��������� ������� ��������
****** Begin section 'CALL_NP4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'CALL_NP4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'CALL_NP4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'CALL_NP4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'CALL_NP4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'CALL_NP4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'CALL_NP4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'CALL_NP4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'CALL_NP4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'CALL_NP4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'CALL_NP4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'CALL_NP4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'CALL_NP4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'CALL_NP4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'CALL_NP4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'CALL_NP4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'CALL_NP4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'CALL_NP4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'CALL_NP4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'

c       ������. ����. ��������� ��� ������� �����
      time_tmp = time_df1()

       call Node_np4_1new(nu,Npres,invperp,NodTyp,Dmp,NodRo,Rok,
     >NoddRodPk,
     *                    Vol1,Yp,NodGgCompWvp,NodGgCompGas,RotVol,
     >RokVol,
     *                    NodCfRo1,NodCfRo2,NodCfRo,NodCcga,NodCcgat,
     *                    NodGgSum,NodGgSumin,NodFreez,
     *                    dtint,Volukz,relaxold,NodTypTnk,
     *                    NodCfCorr_On,NODFREEZPH,NodPp,masdt)

      tick(163,1) = time_df1()-time_tmp
      ErrorNum(1)=163
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 9661 

****** End   section 'CALL_NP4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'CALL_NP4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'CALL_NP4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'CALL_NP4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'CALL_NP4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'CALL_NP4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'CALL_NP4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'CALL_NP4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'CALL_NP4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'CALL_NP4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'CALL_NP4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'CALL_NP4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'CALL_NP4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'CALL_NP4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'CALL_NP4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'CALL_NP4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'CALL_NP4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'CALL_NP4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'CALL_NP4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'CALL_NP4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'CALL_NP4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'CALL_NP4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'CALL_NP4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'CALL_NP4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** End   section 'CALL_NP4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'CALL_NP4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'CALL_NP4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'CALL_NP4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'CALL_NP4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'CALL_NP4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'CALL_NP4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'CALL_NP4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'CALL_NP4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'CALL_NP4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'CALL_NP4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'CALL_NP4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** End   section 'CALL_NP4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'CALL_NP4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'CALL_NP4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'CALL_NP4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'CALL_NP4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'CALL_NP4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'CALL_NP4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'CALL_NP4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'CALL_NP4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'CALL_NP4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'CALL_NP4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'CALL_NP4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'CALL_NP4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'CALL_NP4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'CALL_NP4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'CALL_NP4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'CALL_NP4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'CALL_NP4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'CALL_NP4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

****** Begin section 'CALL_NP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'CALL_NP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'CALL_NP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'CALL_NP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'CALL_NP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'CALL_NP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'CALL_NP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'CALL_NP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'CALL_NP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'CALL_NP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'CALL_NP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'CALL_NP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'CALL_NP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'CALL_NP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'CALL_NP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'CALL_NP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'CALL_NP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'CALL_NP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'CALL_NP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'CALL_NP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'CALL_NP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'CALL_NP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'CALL_NP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'


****** End   section 'CALL_NP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'CALL_NP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'CALL_NP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'CALL_NP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'


****** End   section 'CALL_NP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'CALL_NP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'CALL_NP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'CALL_NP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'CALL_NP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'CALL_NP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'CALL_NP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'CALL_NP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'CALL_NP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'CALL_NP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'CALL_NP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'CALL_NP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'CALL_NP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'CALL_NP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'CALL_NP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'CALL_NP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 9789 

****** End   section 'CALL_NP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'CALL_NP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'CALL_NP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'CALL_NP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 9807 

****** End   section 'CALL_NP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'CALL_NP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'CALL_NP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'CALL_NP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 9820 

****** End   section 'CALL_NP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'CALL_NP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'CALL_NP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'CALL_NP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 9839 


****** End   section 'CALL_NP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'CALL_NP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'CALL_NP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'CALL_NP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'CALL_NP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'CALL_NP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'CALL_NP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'CALL_NP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'CALL_NP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'CALL_NP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'CALL_NP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'CALL_NP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'CALL_NP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'CALL_NP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'CALL_NP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'CALL_NP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'CALL_NP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'CALL_NP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'CALL_NP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

c --- ���������� �������������� ��������� ������� ��������
****** Begin section 'CALL_GP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'CALL_GP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'CALL_GP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'CALL_GP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'CALL_GP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'CALL_GP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'CALL_GP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'CALL_GP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'CALL_GP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'CALL_GP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'CALL_GP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'CALL_GP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'CALL_GP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'CALL_GP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'CALL_GP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'CALL_GP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'CALL_GP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'CALL_GP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'CALL_GP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'

      time_tmp = time_df1()

        call Node_Gp4_1new(nf,nv,nu,n03,n08,Npres,
     *                  Tn3Np,IFROKZ,ITONKZ,Pk,Gk,
     *                  Dmp,AconFrom,AconTo,Bcon,Yp,invperP,LinIBNto,
     *                  LinIBNfro,
     *                  Tn3RelP,FlwIndFromPp,
     *                  FlwIndToPp,FlwIndFromPp1,FlwIndToPp1,
     *                  NodRelE,Tn3RelE,
     *                  Tn8RelE,Tn3Ne,Ek,Nent,
     *                  NodCfdE,Ye,FlwType,n09,Tn9RelP,
     *                  ComGamFrom,ComGamTo,NodCfRo1,NodCfRo2,NodFreez,
     >NODFREEZPH)


      tick(164,1) = time_df1()-time_tmp
      ErrorNum(1)=164

****** End   section 'CALL_GP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'CALL_GP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'CALL_GP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'CALL_GP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'


****** End   section 'CALL_GP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'CALL_GP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'CALL_GP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'CALL_GP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'


****** End   section 'CALL_GP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'CALL_GP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'CALL_GP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'CALL_GP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'CALL_GP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'CALL_GP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'CALL_GP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'CALL_GP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'CALL_GP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'CALL_GP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'CALL_GP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'CALL_GP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'CALL_GP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'CALL_GP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'CALL_GP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'CALL_GP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'




****** End   section 'CALL_GP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'CALL_GP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'CALL_GP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'CALL_GP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'



****** End   section 'CALL_GP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'CALL_GP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'CALL_GP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'CALL_GP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'


****** End   section 'CALL_GP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'CALL_GP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'CALL_GP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'CALL_GP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'


****** End   section 'CALL_GP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'CALL_GP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'CALL_GP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'CALL_GP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'CALL_GP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'CALL_GP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'CALL_GP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'CALL_GP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'CALL_GP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'CALL_GP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'CALL_GP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'CALL_GP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'CALL_GP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'CALL_GP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'CALL_GP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'CALL_GP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'CALL_GP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'CALL_GP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'CALL_GP4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 10011 

c   ���������� �������� ��� �������� (dP - ������ ��������)
      time_tmp = time_df1()
        call Clr_Y(Npres,dP)
      tick(94,1) = time_df1()-time_tmp
      ErrorNum(1)=94
      time_tmp = time_df1()
        call LINE_S_e(Npres,Dmp,Yp,dP,ILNZp,INZSUBp,NZSUBp,PExtIter_)
      tick(95,1) = time_df1()-time_tmp
      ErrorNum(1)=95


c      call relaxP(Npres,nu,n07,NodRelP,Tn7RelP,invperP,
c     *          Pk,dP,NodCfPp,Tn7CfPp,NodCfPpUp,NodCfPpUp_,dp_Tn7,
c     *          dp_nod,NodTypTnk,DpmaxNorm)

c        call Clr_Y(Npres,dP)
c   ��������� ��������
      time_tmp = time_df1()
        CALL CorrectP1(Npres,nu,n08,n03,
     *                NodTyp,Pk,dP,Pk1,invperp,
     *                NodRelP,Tn8RelP,Tn3RelP,
     *                Tn3Pk,Tn3dp,Tn3CfPp,n07,Tn7RelP,Tn7Pk,
     *                n09,Tn9RelP,Tn9Pk,Tn9dp,Tn9CfPp,NodCfPp,
     *                Tn7CfPp,ic,NodCfPpUp,
     *                Tn3pmax,Tn9pmax,Tn9Pp,Tn3Pp,Tn9CfRelax(1),
     *                InternalIter,NodCfPpType)
      tick(96,1) = time_df1()-time_tmp
      ErrorNum(1)=96
C   ���������� �������� �� ������ ����������������� ��������
      time_tmp = time_df1()


         call Correctp2(Npres,nu,NodTyp,Pk,dP,Pk1,invperP,NodRelP,
     *      NodPp,Tn9CfRelax(1))


        CALL Flowssqrt(nv,nu,n08,n03,
     *             Npres,Gk,Bcon,Pk1,
     *             IFROKZ,ITONKZ,FlwType,c7,
     *             LinIBNfro,LinIBNto,
     *             NodRelP,Tn8RelP,Tn3RelP,
     *             InternalIter,
     *             flwbcon,
     *             FlwC7work3,FlwC7work4,
     *             n07,Tn7RelP,n09,Tn9RelP,
     *             FlwdPLevFrom,FlwdPLevTo,FlwPpFrom,FlwPpTo,FlwGrdPp,
     *             Gk_int,Gk_ext,
     *             Tn3FlwIntCoef,Tn9FlwIntCoef)


      tick(97,1) = time_df1()-time_tmp
      ErrorNum(1)=97

**************************************************
      call Com_Gam_Gg(nv,nu,nf,n03,n09,Tn3ne,tn9ne,IFROkz,ITONkz,
     >LinIBNto,
     *                LinIBNfro,NodBodyTyp,ComGamFrom, 
     *                ComGamTo,n07,
     *                aconfROM,aconto,GK,Tn7acon,Tn7Sq,dtint,
     *                FlwGgGa,FlwGgWv,
     *                NodGgSumZeroin,flwdcon,flwfrco,Tn7dcon,
     *                NodGgSum,NodGgSumIn,NodGgSumWvin,FlwXxFrom,
     *                FlwXxTo,FlwGgVp,ComRoFrom,ComRoTo,
     *                Tn3Rok,Tn9Rok,AirG,AirGw,AirGg,AirGv,
     *                Nair,AirFlwInd)


      tick(101,1) = time_df1()-time_tmp
      ErrorNum(1)=101



* ���� �������� �� �������
      call Flw_QqAd(nu,nv,ifrokz,itonkz,LinIbnFro,LinIbnTo,Gk
     *,NodQqAd,FlwCfQqAd,FlwQqAd,FlwPpFrom,FlwPpTo,rovk,NodTt)

      time_tmp = time_df1()
      call Tn258_GgSum_v1(nv,n08,Gk,IFROKZ,
     *                 ITONKZ,
     *                 Tn8GSumWt1,Tn8GSumWt2,
     *                 InternalIter,Nf,LinIbnFro,LinIbnTo,
     *                 ComGamFrom,ComGamTo
     *,FlwHTnFRomUp,FlwHTnToUp,Tn8Lev,Tn8GInOut)
      tick(102,1) = time_df1()-time_tmp
      ErrorNum(1)=102
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 10133 

C ���������� � ������� �������� ������������
****** Begin section 'CcMas_Pre' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'CcMas_Pre' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'CcMas_Pre' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'CcMas_Pre' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'CcMas_Pre' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'CcMas_Pre' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'CcMas_Pre' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'CcMas_Pre' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'CcMas_Pre' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'CcMas_Pre' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'CcMas_Pre' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'CcMas_Pre' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'CcMas_Pre' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'CcMas_Pre' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'CcMas_Pre' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'CcMas_Pre' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'CcMas_Pre' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'CcMas_Pre' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'CcMas_Pre' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'


             call Node_CcMas_Pre(nu,ic,NodTyp,RotVol,
     *                           NodCcGa_Beq,NodCcGa_Beq0,
     *                           NodCcGa_Aeq,NodCcGa_Deq,NodccGat,
     *                           NodGgCompGas,NodGgCompWvp,NodGgSumIn,
     *                           NodFreez,Nodccga,
     *                           NodCcGa_Aeq0,NodCcGa_Deq0,NODFREEZPH)  

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 10165 
                               
****** End   section 'CcMas_Pre' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'CcMas_Pre' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'CcMas_Pre' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'CcMas_Pre' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'CcMas_Pre' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'CcMas_Pre' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'CcMas_Pre' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'CcMas_Pre' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'CcMas_Pre' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'CcMas_Pre' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'CcMas_Pre' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'CcMas_Pre' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'CcMas_Pre' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'CcMas_Pre' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'CcMas_Pre' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'CcMas_Pre' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'CcMas_Pre' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'CcMas_Pre' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'CcMas_Pre' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'CcMas_Pre' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'CcMas_Pre' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'CcMas_Pre' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'CcMas_Pre' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'CcMas_Pre' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** End   section 'CcMas_Pre' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'CcMas_Pre' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'CcMas_Pre' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'CcMas_Pre' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'CcMas_Pre' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'CcMas_Pre' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'CcMas_Pre' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'CcMas_Pre' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'CcMas_Pre' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'CcMas_Pre' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'CcMas_Pre' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'CcMas_Pre' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** End   section 'CcMas_Pre' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'CcMas_Pre' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'CcMas_Pre' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'CcMas_Pre' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'CcMas_Pre' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'CcMas_Pre' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'CcMas_Pre' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'CcMas_Pre' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'CcMas_Pre' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'CcMas_Pre' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'CcMas_Pre' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'CcMas_Pre' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'CcMas_Pre' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'CcMas_Pre' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'CcMas_Pre' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'CcMas_Pre' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'CcMas_Pre' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'CcMas_Pre' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'CcMas_Pre' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

* ������ �������� ������������ �� ���������� ���������
      do i=1,1
****** Begin section 'CcMas_IterInt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'CcMas_IterInt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'CcMas_IterInt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'CcMas_IterInt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'CcMas_IterInt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'CcMas_IterInt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'CcMas_IterInt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'CcMas_IterInt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'CcMas_IterInt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'CcMas_IterInt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'CcMas_IterInt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'CcMas_IterInt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'CcMas_IterInt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'CcMas_IterInt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'CcMas_IterInt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'CcMas_IterInt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'CcMas_IterInt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'CcMas_IterInt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'CcMas_IterInt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'

      time_tmp = time_df1()

             call Node_CcMas(nf,nu,nv,NodTyp,NodBodyTyp,
     *                       IFROKZ,ITONKZ,
     *                       LinIBNfro,LinIBNto,RotVol,RokVol,
     *                       NodCcGa_Beq,NodCcGa_Beq0,
     *                       NodCcGa_Aeq,NodCcGa_Deq,NodCcGa_Deq0,
     *                       NodCcga,NodFreez,
     *                       nsep1,Sep1FlwS,Sep1Node,
     *                       FlwSlGaFrom, FlwSlGaTo ,
     *                       FlwSlGaFrom0,FlwSlGaTo0,
     *                       FlwGgGa,FlwGgWv,Gk,NODFREEZPH)

      tick(165,1) = time_df1()-time_tmp
      ErrorNum(1)=165
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 10261 

****** End   section 'CcMas_IterInt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'CcMas_IterInt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'CcMas_IterInt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'CcMas_IterInt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'CcMas_IterInt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'CcMas_IterInt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'CcMas_IterInt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'CcMas_IterInt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'CcMas_IterInt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'CcMas_IterInt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'CcMas_IterInt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'CcMas_IterInt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'CcMas_IterInt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'CcMas_IterInt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'CcMas_IterInt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'CcMas_IterInt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'CcMas_IterInt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'CcMas_IterInt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'CcMas_IterInt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'CcMas_IterInt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'CcMas_IterInt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'CcMas_IterInt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'CcMas_IterInt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'CcMas_IterInt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** End   section 'CcMas_IterInt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'CcMas_IterInt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'CcMas_IterInt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'CcMas_IterInt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 10315 
      


****** End   section 'CcMas_IterInt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'CcMas_IterInt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'CcMas_IterInt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'CcMas_IterInt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'CcMas_IterInt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'CcMas_IterInt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'CcMas_IterInt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'CcMas_IterInt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** End   section 'CcMas_IterInt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'CcMas_IterInt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'CcMas_IterInt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'CcMas_IterInt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'CcMas_IterInt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'CcMas_IterInt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'CcMas_IterInt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'CcMas_IterInt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'CcMas_IterInt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'CcMas_IterInt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'CcMas_IterInt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'CcMas_IterInt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'CcMas_IterInt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'CcMas_IterInt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'CcMas_IterInt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'CcMas_IterInt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'CcMas_IterInt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'CcMas_IterInt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'CcMas_IterInt' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
      enddo


****** Begin section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 10384 


****** End   section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 10401 


****** End   section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
c   ��������� ��������� �������� ������� �� ����. ���. Wl2
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 10449 

****** End   section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

****** Begin section 'CALL_TcW_Qq' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'CALL_TcW_Qq' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'CALL_TcW_Qq' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'CALL_TcW_Qq' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'CALL_TcW_Qq' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'CALL_TcW_Qq' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'CALL_TcW_Qq' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'CALL_TcW_Qq' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'CALL_TcW_Qq' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'CALL_TcW_Qq' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'CALL_TcW_Qq' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'CALL_TcW_Qq' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'CALL_TcW_Qq' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'CALL_TcW_Qq' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'CALL_TcW_Qq' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'CALL_TcW_Qq' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'CALL_TcW_Qq' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'CALL_TcW_Qq' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'CALL_TcW_Qq' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'CALL_TcW_Qq' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'CALL_TcW_Qq' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'CALL_TcW_Qq' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'CALL_TcW_Qq' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'CALL_TcW_Qq' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'CALL_TcW_Qq' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'CALL_TcW_Qq' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'CALL_TcW_Qq' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'CALL_TcW_Qq' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'CALL_TcW_Qq' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'CALL_TcW_Qq' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'CALL_TcW_Qq' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 10511 

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 10517 


****** End   section 'CALL_TcW_Qq' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'CALL_TcW_Qq' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'CALL_TcW_Qq' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'CALL_TcW_Qq' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'

      call Tcw_0all(Nwall,NMtl,NMtlO
     *   ,Wl1_QqOut,Mtl_QqOut,Wl1AlCulcWl,MtlAlCulc
     *   ,Tn3WlDoQqOut,Tn3WlUpQqOut,Tn3TcwAlCulc,n03
     *   ,Tn9WlDoQqOut,Tn9WlUpQqOut,Tn9TcwAlCulc,n09
     *   ,Tn7WlDoQqOut,Tn7WlUpQqOut,Tn7TcwAlCulc,n07
     *   ,TenTcwQq,TenTcwAlCulc,nTen
     &   ,WLC2_Qq,WLC2Iwl,Nwlcon2
     &   ,Nwall2,Wl2Ndet,wl2_QqOut,wl2AlCulcWl2
     &   ,MtlO_QqOut,MtlOAlCulcWl1)


c   ������ ������� � ��������� ������� ��� ��������� � � ������ �������
c   �� ����������, ��������� � ������������ ����� ������ ��� �������
c   �����
        call TcW_Qq(Nwall,NMtl,NMtlO,Ntcw,dtInt
     &                 ,TcWFrom,TcWTo,TcWTypFrom,TcWTypTo
     &                 ,TcWThi,TcWSq,TcWQq,TcWLamd
     &                 ,Wl1Mass,Wl1Cp,Wl1LamdC,Wl1Tt,wl1_QqOut
     &                 ,MtlMass,MtlCp,MtlLamdC,MtlTt,Mtl_QqOut
     &                 ,MtlOMass,MtlOCp,MtlOLamdC,MtlOTt,MtlO_QqOut
     &                 ,Wl1AlCulcWl,MtlAlCulc,Wl1mult
     *         ,Tn3TtWlDo,Tn3TtWlUp,Tn3WlDoQqOut,Tn3WlUpQqOut,
     >Tn3TcwAlCulc,n03
     *         ,Tn9TtWlDo,Tn9TtWlUp,Tn9WlDoQqOut,Tn9WlUpQqOut,
     >Tn9TcwAlCulc,n09
     *         ,Tn7TtWlDo,Tn7TtWlUp,Tn7WlDoQqOut,Tn7WlUpQqOut,
     >Tn7TcwAlCulc,n07
     *         ,TenTtWl,TenTcwQq,TenTcwAlCulc,TenLaWl,nTen
     *         ,TcWFromUp,TcWToUp,Tn3LaWl,Tn9LaWl,Tn7LaWl
     &   ,TcWCfAllEnv,TcWSqCul,TcWIniType
     &   ,TcWdx,TcWdy,MtlOAlCulcWl1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 10554 

****** End   section 'CALL_TcW_Qq' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'CALL_TcW_Qq' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 10578 

****** End   section 'CALL_TcW_Qq' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'CALL_TcW_Qq' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'CALL_TcW_Qq' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'CALL_TcW_Qq' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'CALL_TcW_Qq' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'CALL_TcW_Qq' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** End   section 'CALL_TcW_Qq' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'CALL_TcW_Qq' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'CALL_TcW_Qq' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'CALL_TcW_Qq' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'CALL_TcW_Qq' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'CALL_TcW_Qq' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'CALL_TcW_Qq' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'CALL_TcW_Qq' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'CALL_TcW_Qq' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'CALL_TcW_Qq' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'CALL_TcW_Qq' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'CALL_TcW_Qq' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** End   section 'CALL_TcW_Qq' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'CALL_TcW_Qq' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'CALL_TcW_Qq' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'CALL_TcW_Qq' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'CALL_TcW_Qq' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'CALL_TcW_Qq' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'CALL_TcW_Qq' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'CALL_TcW_Qq' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'CALL_TcW_Qq' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'CALL_TcW_Qq' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'CALL_TcW_Qq' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'CALL_TcW_Qq' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'CALL_TcW_Qq' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'CALL_TcW_Qq' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'CALL_TcW_Qq' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'CALL_TcW_Qq' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'CALL_TcW_Qq' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'CALL_TcW_Qq' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'CALL_TcW_Qq' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

****** Begin section 'call_PreNh4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'call_PreNh4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'call_PreNh4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'call_PreNh4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'call_PreNh4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'call_PreNh4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'call_PreNh4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'call_PreNh4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'call_PreNh4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'call_PreNh4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'call_PreNh4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'call_PreNh4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'call_PreNh4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'call_PreNh4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'call_PreNh4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'call_PreNh4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'call_PreNh4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'call_PreNh4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'call_PreNh4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'call_PreNh4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'call_PreNh4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'call_PreNh4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'call_PreNh4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'call_PreNh4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'call_PreNh4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'call_PreNh4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'call_PreNh4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'call_PreNh4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'call_PreNh4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'call_PreNh4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'call_PreNh4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'call_PreNh4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'call_PreNh4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'call_PreNh4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'call_PreNh4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'call_PreNh4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'call_PreNh4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'call_PreNh4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'call_PreNh4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'call_PreNh4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'call_PreNh4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'call_PreNh4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'call_PreNh4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** End   section 'call_PreNh4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'call_PreNh4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'call_PreNh4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'call_PreNh4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'call_PreNh4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'call_PreNh4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'call_PreNh4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'call_PreNh4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'call_PreNh4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'call_PreNh4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'call_PreNh4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'call_PreNh4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** End   section 'call_PreNh4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'call_PreNh4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'call_PreNh4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'call_PreNh4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'call_PreNh4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'call_PreNh4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'call_PreNh4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'call_PreNh4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'call_PreNh4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'call_PreNh4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'

      call Wl1_Flw(Nwall,nv,nu,
     *  Wl1Tt,Wl1Lamd,Wl1_QqOut,Wl1AlCulcWl,
     *  FlwFlQqWl1,FlwDdExt,FlwThi,ifrokz,itonkz,
     *  FlwQqWl1,FlwKkQqWl1,FlwLl,NodIndWl1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 10689 

c   ������ ������� � ��������� ������� ��� ��������� � � ������ �������
c   �� ����������, ��������� � ������������ ����� ������ ��� �������
c   �����
      time_tmp = time_df1()
        call Wl1_HhEqv(nu,Nwall,Nent,Wl1Node,Wl1Mass,
     &               NodCp,NodTtk,Wl1Cp,wl1_Tt_t,
     &               Dmh,Ye,C1serv,C2serv,invperE,Wl1TtEnv,
     &               Pk,NoddRodPk,NoddRodEk,NodXxBalk1,
     &               Wl1AlCulcNode,Wl1AlCulcEnv,WL1Qq,dtInt,
     &               wl1_QqOut,Wl1CpC,wl1AlCulcWl,wl1Tt,
     &               Wl1CfCulcNode,Wl1CfCulcEnv,Wl1CfAllEnv,
     &               Wl1_XQ,Wl1_BQ,Wl1CfMasMe) 
  
      tick(379,1) = time_df1()-time_tmp
      ErrorNum(1)=3791
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 10707 


****** End   section 'call_PreNh4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'call_PreNh4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'call_PreNh4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'call_PreNh4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 10738 



****** End   section 'call_PreNh4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'call_PreNh4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'call_PreNh4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'call_PreNh4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'call_PreNh4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

      do i=1,ItZei(1)

c   ------------------------------------------------------------------
c   ��������� ��������� ������� Dmh (����� ������� ���������)
      time_tmp = time_df1()
c        call SetMusor8(Dmh,Nent*Nent)  !!!�����
        call CLR_S_e(Dmh,Nent,ILNZe,INZSUBe,NZSUBe,EExtIter_)
        call CLR_S_e(DmhG,Nent,ILNZe,INZSUBe,NZSUBe,EExtIter_)
      tick(103,1) = time_df1()-time_tmp
      ErrorNum(1)=103
      time_tmp = time_df1()
        call clr_Y(Nent,Ye)  !��������� ������ ������ ��� ���������
        call clr_Y(Nent,YeG)  !��������� ������ ������ ��� ���������
      tick(104,1) = time_df1()-time_tmp
      ErrorNum(1)=104

c --- ������������ ����. ��������� ������� ���������
****** Begin section 'CALL_NH4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'CALL_NH4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'CALL_NH4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'CALL_NH4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'CALL_NH4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'CALL_NH4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'CALL_NH4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'CALL_NH4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'CALL_NH4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'CALL_NH4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'CALL_NH4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'CALL_NH4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'CALL_NH4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'CALL_NH4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'CALL_NH4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'CALL_NH4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'CALL_NH4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'CALL_NH4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'CALL_NH4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'

      call clr_Y(Nu,NodYe)
      call clr_Y(Nu,NodDmh)
      time_tmp = time_df1()
        call Node_nh4_new(nu,Npres,Nent,Dmh,Pk1,NodPp,NodEe,Ek,Vol1,
     *                NodQqAdd,NodDmhAdd,Ye,invperE,NodRelP,NodRelE,
     >NodQqPm,
     *                NodTyp,NodQqComp,RotVol,RokVol,NodFreez,
     *                NodCgEe,NodCgEe0,NodGgSum,NodGgSumIn,
     *                NodRoTot,NodRoTot0,NodCfdPdt,NODFREEZPH,NodQqAd,
     *                WasteNoGas(6,1),NodWaste_T,NodCfEeAsh,RadIndSA,
     *                NodTtk,NodCp,AshCfCp,
     *                NodGgCompWvp,NodGgCompGas,NodEeWv,NodEeGa,
     *                Masdt_Ga,Masdt_Wv,dtint,NodCcGa,NodCcGat,NodVvGa,
     >NodNumEe(1),
     *                NodEeGat,NodEeWvt,DmhG,YeG,EGk)

      tick(166,1) = time_df1()-time_tmp
      ErrorNum(1)=166
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 10802 

****** End   section 'CALL_NH4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'CALL_NH4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'CALL_NH4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'CALL_NH4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'CALL_NH4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'CALL_NH4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'CALL_NH4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'CALL_NH4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'CALL_NH4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'CALL_NH4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'CALL_NH4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'CALL_NH4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'CALL_NH4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'CALL_NH4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'CALL_NH4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'CALL_NH4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'CALL_NH4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'CALL_NH4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'CALL_NH4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'CALL_NH4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'CALL_NH4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'CALL_NH4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'CALL_NH4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'CALL_NH4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** End   section 'CALL_NH4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'CALL_NH4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'CALL_NH4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'CALL_NH4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'CALL_NH4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'CALL_NH4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'CALL_NH4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'CALL_NH4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'CALL_NH4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'CALL_NH4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'CALL_NH4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'CALL_NH4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** End   section 'CALL_NH4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'CALL_NH4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'CALL_NH4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'CALL_NH4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'CALL_NH4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'CALL_NH4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'CALL_NH4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'CALL_NH4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'CALL_NH4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'CALL_NH4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'CALL_NH4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'CALL_NH4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'CALL_NH4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'CALL_NH4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'CALL_NH4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'CALL_NH4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'CALL_NH4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'CALL_NH4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'CALL_NH4_NODE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

****** Begin section 'CALL_NH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'CALL_NH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'CALL_NH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'CALL_NH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'CALL_NH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'CALL_NH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'CALL_NH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'CALL_NH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'CALL_NH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 10903 


****** End   section 'CALL_NH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'CALL_NH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'CALL_NH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'CALL_NH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'



****** End   section 'CALL_NH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'CALL_NH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'CALL_NH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'CALL_NH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'CALL_NH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'CALL_NH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'CALL_NH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'CALL_NH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'CALL_NH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'CALL_NH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'CALL_NH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'CALL_NH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'CALL_NH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'CALL_NH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'CALL_NH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'CALL_NH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'CALL_NH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'CALL_NH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 10942 
****** Begin section 'CALL_NH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'CALL_NH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'CALL_NH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'CALL_NH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'CALL_NH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'CALL_NH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'CALL_NH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'CALL_NH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'CALL_NH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'CALL_NH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'CALL_NH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 11049 


****** End   section 'CALL_NH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'CALL_NH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'CALL_NH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'CALL_NH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 11130 

****** End   section 'CALL_NH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'CALL_NH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'CALL_NH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'CALL_NH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 11167 


****** End   section 'CALL_NH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'CALL_NH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'CALL_NH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'CALL_NH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 11261 


****** End   section 'CALL_NH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'CALL_NH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'CALL_NH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'CALL_NH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'CALL_NH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'CALL_NH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'CALL_NH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'CALL_NH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'CALL_NH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'CALL_NH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'

c   ������ ������� � ��������� ������� ��� ��������� � � ������ �������
c   �� ����������, ��������� � ������������ ����� ������ ��� �������
c   �����
c      time_tmp = time_df1()
      if(NodNumEe(1).eq.1) then
        call Wl1_nh4(nu,nwall,Nent,Wl1Node,Dmh,Ye,invperE,Wl1_XQ,
     >Wl1_BQ) 
      endif
c      tick(379,1) = time_df1()-time_tmp
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 11284 


****** End   section 'CALL_NH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'CALL_NH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'CALL_NH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'CALL_NH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 11301 



****** End   section 'CALL_NH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'CALL_NH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'CALL_NH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'CALL_NH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'CALL_NH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

****** Begin section 'Tn8Tc*' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'Tn8Tc*' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'Tn8Tc*' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'Tn8Tc*' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'Tn8Tc*' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'Tn8Tc*' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'Tn8Tc*' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'Tn8Tc*' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'Tn8Tc*' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'Tn8Tc*' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'Tn8Tc*' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'Tn8Tc*' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'Tn8Tc*' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'Tn8Tc*' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'Tn8Tc*' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'Tn8Tc*' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'Tn8Tc*' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'Tn8Tc*' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'Tn8Tc*' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'Tn8Tc*' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'Tn8Tc*' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'Tn8Tc*' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'Tn8Tc*' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 11354 

****** End   section 'Tn8Tc*' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'Tn8Tc*' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'Tn8Tc*' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'Tn8Tc*' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 11381 

****** End   section 'Tn8Tc*' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'Tn8Tc*' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'Tn8Tc*' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'Tn8Tc*' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'Tn8Tc*' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'Tn8Tc*' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'Tn8Tc*' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'Tn8Tc*' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'Tn8Tc*' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'Tn8Tc*' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'Tn8Tc*' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'Tn8Tc*' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'Tn8Tc*' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'Tn8Tc*' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'Tn8Tc*' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'Tn8Tc*' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** End   section 'Tn8Tc*' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'Tn8Tc*' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'Tn8Tc*' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'Tn8Tc*' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'Tn8Tc*' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'Tn8Tc*' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'Tn8Tc*' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'Tn8Tc*' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'Tn8Tc*' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'Tn8Tc*' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'Tn8Tc*' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'Tn8Tc*' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** End   section 'Tn8Tc*' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'Tn8Tc*' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'Tn8Tc*' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'Tn8Tc*' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'Tn8Tc*' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'Tn8Tc*' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'Tn8Tc*' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'Tn8Tc*' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'Tn8Tc*' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'Tn8Tc*' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'Tn8Tc*' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'Tn8Tc*' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'Tn8Tc*' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'Tn8Tc*' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'Tn8Tc*' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'Tn8Tc*' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'Tn8Tc*' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'Tn8Tc*' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'Tn8Tc*' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 11438 

c --- ���������� �������������� ��������� ������� ���������
****** Begin section 'CALL_GH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'CALL_GH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'CALL_GH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'CALL_GH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'CALL_GH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'CALL_GH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'CALL_GH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'CALL_GH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'CALL_GH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'CALL_GH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'CALL_GH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'CALL_GH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'CALL_GH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'CALL_GH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'CALL_GH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'CALL_GH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'CALL_GH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'CALL_GH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'CALL_GH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'

      time_tmp = time_df1()
      if(NodNumEe(1).eq.1) then
        call Node_Gh4_v1(nu,nv,n08,n03,Nent,
     *                Tn3Ne,IFROKZ,ITONKZ,
     *                LinIBNfro,LinIBNto,Gk,Ek,Dmh,Ye,invperE,
     *                NodRelE,Tn8RelE,Tn3RelE,
     *                Tn3Ek,Nf,
     *                n07,Tn7RelE,n09,Tn9RelE,Tn9Ek,Tn9Ne,
     *                Tn7Ek,Tn3Cp,ComGamFrom,ComGamTo,Tn9Cp,
     *                NodFreez,
     *                NodEeWv,NodEeGa,FlwGgWv,FlwGgGa,
     *                ComEeFrom,ComEeTo,NodTt,NodCp,FlwLaFrc,SchEnt,
     >NODFREEZPH,NodCfEeAsh,
     *                Tn3EntOut_Key,Tn9EntOut_Key)
      else
        call Node_Gh4_e2(nu,nv,n08,n03,Nent,
     *                Tn3Ne,IFROKZ,ITONKZ,
     *                LinIBNfro,LinIBNto,Gk,Ek,Dmh,Ye,invperE,
     *                NodRelE,Tn8RelE,Tn3RelE,
     *                Tn3Ek,Nf,
     *                n07,Tn7RelE,n09,Tn9RelE,Tn9Ek,Tn9Ne,
     *                Tn7Ek,Tn3Cp,ComGamFrom,ComGamTo,Tn9Cp,
     *                NodFreez,
     *                NodEeWv,NodEeGa,FlwGgWv,FlwGgGa,
     *                ComEeFrom,ComEeTo,NodTt,NodCp,FlwLaFrc,SchEnt,
     >NODFREEZPH,NodCfEeAsh,
     *                Tn3EntOut_Key,Tn9EntOut_Key,NodYe,NodDmh,YeG,DmhG,
     >EGk)
      endif
      tick(167,1) = time_df1()-time_tmp
      ErrorNum(1)=167
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 11492 

****** End   section 'CALL_GH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'CALL_GH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'CALL_GH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'CALL_GH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'CALL_GH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'CALL_GH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'CALL_GH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'CALL_GH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'CALL_GH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'CALL_GH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'CALL_GH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'CALL_GH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'CALL_GH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'CALL_GH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'CALL_GH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'CALL_GH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'CALL_GH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'CALL_GH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'CALL_GH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'CALL_GH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'CALL_GH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'CALL_GH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'CALL_GH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'CALL_GH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 11532 


****** End   section 'CALL_GH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'CALL_GH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'CALL_GH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'CALL_GH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 11551 

****** End   section 'CALL_GH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'CALL_GH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'CALL_GH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'CALL_GH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 11571 

****** End   section 'CALL_GH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'CALL_GH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'CALL_GH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'CALL_GH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 11592 

****** End   section 'CALL_GH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'CALL_GH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'CALL_GH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'CALL_GH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'CALL_GH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'CALL_GH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'CALL_GH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'CALL_GH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'CALL_GH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'CALL_GH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'CALL_GH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'CALL_GH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'CALL_GH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'CALL_GH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'CALL_GH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'CALL_GH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'CALL_GH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'CALL_GH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'CALL_GH4' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

****** Begin section 'TurNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'TurNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'TurNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'TurNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'TurNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'TurNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'TurNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'TurNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'TurNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'TurNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'TurNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'TurNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'TurNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'TurNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'TurNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'TurNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'TurNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'TurNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'TurNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'TurNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'TurNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'TurNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'TurNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'TurNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'TurNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'TurNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'TurNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'TurNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'TurNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'TurNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'TurNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'TurNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'TurNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'TurNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'TurNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'TurNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'TurNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'TurNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'TurNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'TurNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'TurNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'TurNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'TurNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** End   section 'TurNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'TurNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'TurNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'TurNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'TurNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'TurNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'TurNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'TurNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'TurNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'TurNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'TurNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'TurNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** End   section 'TurNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'TurNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'TurNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'TurNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'TurNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'TurNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
c ��������� �������������� ��������� ������� ��������� 
c ��� ������������ ������������� �� �������� �������
c
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 11701 

****** End   section 'TurNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'TurNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'TurNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'TurNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'TurNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'TurNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'TurNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'TurNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'TurNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'TurNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'TurNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'TurNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'TurNodeGH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

****** Begin section 'CALL_NH4_NODE_FINAL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'CALL_NH4_NODE_FINAL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'CALL_NH4_NODE_FINAL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'CALL_NH4_NODE_FINAL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'CALL_NH4_NODE_FINAL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'CALL_NH4_NODE_FINAL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'CALL_NH4_NODE_FINAL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'CALL_NH4_NODE_FINAL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'CALL_NH4_NODE_FINAL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'CALL_NH4_NODE_FINAL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'CALL_NH4_NODE_FINAL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'CALL_NH4_NODE_FINAL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'CALL_NH4_NODE_FINAL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'CALL_NH4_NODE_FINAL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'CALL_NH4_NODE_FINAL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'CALL_NH4_NODE_FINAL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'CALL_NH4_NODE_FINAL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'CALL_NH4_NODE_FINAL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'CALL_NH4_NODE_FINAL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
      if(NodNumEe(1).ne.1) then
        call Node_nh4_disp(nu,NodCcGa,NodCpGa,NodCpWv,Volukzt,NodRo
     *  ,NodAlSqGaWv,NodYe,NodQqAdd,NodDmhAdd,NodQqAd,NodQqPm,NodDmh,
     >NodXxBal
     *  ,NodXxBalk1,NodRelE,invperE,RotVol,Dmh,Ye,nent,NodCcGat
     *  ,NodEeWtSat,NodEeVpSat
     *  ,NodTtGa,NodTtWv,Pk,NoddRodPk,NoddRodEk,nwall,nwall2,Wl1Node
     *  ,Wl2Node,Wl1_XQ,Wl1_BQ,Wl2_XQ,Wl2_BQ,NodGgSumIn,dtint,NodVvGat
     *  ,DmhG,YeG,ntcr,TcRNode,TcR_XQ,TcR_BQ)
      endif
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 11746 

****** End   section 'CALL_NH4_NODE_FINAL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'CALL_NH4_NODE_FINAL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'CALL_NH4_NODE_FINAL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'CALL_NH4_NODE_FINAL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'CALL_NH4_NODE_FINAL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'CALL_NH4_NODE_FINAL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'CALL_NH4_NODE_FINAL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'CALL_NH4_NODE_FINAL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'CALL_NH4_NODE_FINAL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'CALL_NH4_NODE_FINAL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'CALL_NH4_NODE_FINAL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'CALL_NH4_NODE_FINAL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'CALL_NH4_NODE_FINAL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'CALL_NH4_NODE_FINAL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'CALL_NH4_NODE_FINAL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'CALL_NH4_NODE_FINAL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'CALL_NH4_NODE_FINAL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'CALL_NH4_NODE_FINAL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'CALL_NH4_NODE_FINAL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'CALL_NH4_NODE_FINAL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'CALL_NH4_NODE_FINAL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'CALL_NH4_NODE_FINAL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'CALL_NH4_NODE_FINAL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'CALL_NH4_NODE_FINAL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** End   section 'CALL_NH4_NODE_FINAL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'CALL_NH4_NODE_FINAL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'CALL_NH4_NODE_FINAL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'CALL_NH4_NODE_FINAL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'CALL_NH4_NODE_FINAL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'CALL_NH4_NODE_FINAL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'CALL_NH4_NODE_FINAL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'CALL_NH4_NODE_FINAL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'CALL_NH4_NODE_FINAL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'CALL_NH4_NODE_FINAL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'CALL_NH4_NODE_FINAL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'CALL_NH4_NODE_FINAL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** End   section 'CALL_NH4_NODE_FINAL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'CALL_NH4_NODE_FINAL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'CALL_NH4_NODE_FINAL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'CALL_NH4_NODE_FINAL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'CALL_NH4_NODE_FINAL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'CALL_NH4_NODE_FINAL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'CALL_NH4_NODE_FINAL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'CALL_NH4_NODE_FINAL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'CALL_NH4_NODE_FINAL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'CALL_NH4_NODE_FINAL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'CALL_NH4_NODE_FINAL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'CALL_NH4_NODE_FINAL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'CALL_NH4_NODE_FINAL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'CALL_NH4_NODE_FINAL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'CALL_NH4_NODE_FINAL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'CALL_NH4_NODE_FINAL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'CALL_NH4_NODE_FINAL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'CALL_NH4_NODE_FINAL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'CALL_NH4_NODE_FINAL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 11808 

      time_tmp = time_df1()
      if(IC.eq.MaxIternew(1)-1) then
        call Node_YeSave(Nu,Nent,Ye,Qdt,invperE,NodRelE,NodTyp)
      endif
      tick(107,1) = time_df1()-time_tmp
      ErrorNum(1)=107
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 11817 


c   ���������� �������� ��� ��������� (dE - ������ ��������)
      time_tmp = time_df1()
        call Clr_Y(Nent,dE)
        call Clr_Y(Nent,dEG)
      tick(108,1) = time_df1()-time_tmp
      ErrorNum(1)=108
      time_tmp = time_df1()
      if(SchEnt(1).ne.1) then
        call LINE_S_e(Nent,Dmh,Ye,dE,ILNZe,INZSUBe,NZSUBe,EExtIter_)
        if (NodNumEe(1).ne.1) 
     *    call LINE_S_e(Nent,DmhG,YeG,dEG,ILNZe,INZSUBe,NZSUBe,
     >EExtIter_)
      else
        call line_zeidel(Nent,Dmh,Ye,dE,EExtIter_)
      endif
      tick(109,1) = time_df1()-time_tmp
      ErrorNum(1)=109

c   ��������� ���������
      time_tmp = time_df1()
        CALL PreCorH(Nent,nu,n08,n07,
     *                NodTyp,Ek,dE,Ek1,invperE,
     *                NodRelE,Tn8RelE,
     *                Tn7RelE,NodNumEe(1),EGk,EGk1,dEG)
      tick(110,1) = time_df1()-time_tmp
      ErrorNum(1)=110

****** Begin section 'CorH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'CorH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'CorH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'CorH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'CorH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'CorH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'CorH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'CorH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'CorH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'CorH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'CorH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'CorH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'CorH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'CorH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'CorH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'CorH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'CorH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'CorH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'CorH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'

      time_tmp = time_df1()
      if(NodNumEe(1).eq.1) then
        call Node_CorH(nu,Nent,NodTyp,NodBodyTyp,
     *                    Ek,Ek1,NodEeGa,NodTtk,
     *                    NodCp,NodCfEe,
     *                    Nodccga,
     *                    NodEeWv,
     *                    NodTtWv
     *                   ,NodAlfaH,NodFreez,NodEe,Tn9CfRelax(1))
      else
        call Node_CorH_e2(nu,Nent,NodTyp,NodBodyTyp,
     *                    Ek,Ek1,NodTtk,
     *                    NodCfEe,
     *                    NodEeWv,NodCpWv,
     *                    NodTtWv
     *                   ,NodAlfaH,NodFreez,Tn9CfRelax(1)
     *                   ,NodEeGat,NodeeWvt,EGk,EGk1)
      endif      

      tick(111,1) = time_df1()-time_tmp
      ErrorNum(1)=111
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 11888 

****** End   section 'CorH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'CorH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'CorH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'CorH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'CorH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'CorH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'CorH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'CorH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'CorH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'CorH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'CorH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'CorH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'CorH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'CorH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'CorH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'CorH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'CorH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'CorH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'CorH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'CorH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'CorH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'CorH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'CorH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'CorH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 11922 


****** End   section 'CorH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'CorH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'CorH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'CorH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 11948 

****** End   section 'CorH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'CorH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'CorH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'CorH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 11961 

****** End   section 'CorH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'CorH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'CorH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'CorH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 11975 


****** End   section 'CorH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'CorH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'CorH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'CorH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'CorH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'CorH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'CorH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'CorH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'CorH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'CorH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'CorH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'CorH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'CorH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'CorH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'CorH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'CorH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'CorH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'CorH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'CorH' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

        do j=1,Nent
          Ek(j)=Ek1(j)
          EGk(j)=EGk1(j)
        enddo
      enddo

c   ������ �������
****** Begin section 'Prop' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'Prop' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'Prop' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'Prop' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'Prop' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'Prop' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'Prop' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'Prop' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'Prop' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'Prop' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'Prop' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'Prop' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'Prop' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'Prop' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'Prop' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'Prop' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'Prop' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'Prop' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'Prop' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'

      time_tmp = time_df1()
      if(NodNumEe(1).eq.1) then
        call 
     *   Node_Prop(nu,Npres,Nent,NodTyp,NodBodyTyp
     *           ,Pk,Pk1,Ek,Ek1,NodEega
     *           ,nodDRDPWv,NoddRodPk1,nodDRDHWv,NoddRodEk1
     *           ,nodRoWv,Rok,nodRoGa,NodTtga,NodTtk,NodXxk1
     *           ,EeWtSatk,EeVpSatk,NodCp,NodRoWtSat
     *           ,NodTtSat,NodCfAd,Nodccga
     *           ,Nodvvga,NodGaType
     *           ,NodCfTur1,NodCfTur2,NodXxRel
     *           ,NodXxBalk1
     *           ,NodEeWv,NodCpGa,NodCfRo1,NodCfRo2,NodCfRo
     *           ,NodCpWv,NodTtWv,Masdt,NodRotrue
     *           ,NodCgGa,NodCgMa,NodCgEe
     *           ,NodRoGg,NodCgSqrt,NodalphMax
     *           ,NodGgSumZero,NodeSlipCor(1)
     *           ,Nodccgat,NodCgMa0
     *           ,NodKeyProps,NodRoVpSat
     *           ,NodTypIni,TypeBut
     *           ,NodAlphXbTun,NoddrdpWv_true
     *           ,NodXb_tmp,NodCcga_tmp,NodTt_tmp
     *           ,NodPp0,NodRoTot
     *           ,WasteNoGas,NodWaste_T,NodExhBet,NodSigmaSurf      
     *           ,NodPropsKey,NodPpVp,NodPp
     *           ,PpMax_iter(1),flag_first(1),MaxPpIterIn(1)
     *           ,PpPrecision(1),NodEe_tmp,NoddRodC
     *           ,NodePropsKey(1),NodFreez
     *           ,NodeSlipVp(1),NodCgVp
     *           ,NodTur2Key,PartpreIntFlag,NodExhBet_tmp,NoddRodPsnd
     *           ,NoddCgdCc,NodNumEe(1)
     *           ,NodPsw,NodRoWvTmp,Nod_ghm_flag,NodPropRelaxFlag
     *           ,NoddRdPGas,NoddRdHGas
     *           ,NodTtWvTmp,NodCpWvTmp,NodXxTmp,NoddRdPWvTmp,
     >NoddRdHWvTmp
     *           ,TypedROdH(1),OildROdH(1)
     *           ,WasteNoGl)
      else
        call Node_Prop_e2(nu,Npres,Nent,NodTyp,NodBodyTyp
     *           ,Pk,Pk1,Ek,Ek1,NodEega
     *           ,nodDRDPWv,NoddRodPk1,nodDRDHWv,NoddRodEk1
     *           ,nodRoWv,Rok,nodRoGa,NodTtga,NodTtk,NodXxk1
     *           ,EeWtSatk,EeVpSatk,NodCp,NodRoWtSat
     *           ,NodTtSat,NodCfAd,Nodccga
     *           ,Nodvvga,NodGaType
     *           ,NodCfTur1,NodCfTur2,NodXxRel
     *           ,NodXxBalk1
     *           ,NodEeWv,NodCpGa,NodCfRo1,NodCfRo2,NodCfRo
     *           ,NodCpWv,NodTtWv,NodRotrue
     *           ,NodCgGa,NodCgMa,NodCgEe
     *           ,NodRoGg,NodalphMax
     *           ,NodGgSumZero
     *           ,Nodccgat
     *           ,NodKeyProps,NodRoVpSat
     *           ,NodTypIni,TypeBut
     *           ,NodAlphXbTun,NoddrdpWv_true
     *           ,NodXb_tmp,NodCcga_tmp,NodTt_tmp
     *           ,NodPp0,NodRoTot
     *           ,WasteNoGas,NodWaste_T,NodExhBet,NodSigmaSurf      
     *           ,NodPropsKey,NodPpVp,NodPp
     *           ,PpMax_iter(1),flag_first(1),MaxPpIterIn(1)
     *           ,PpPrecision(1),NodEe_tmp,NodEeGa_tmp,NoddRodC
     *           ,NodePropsKey(1),NodFreez
     *           ,NodeSlipVp(1),NodCgVp
     *           ,NodTur2Key,PartpreIntFlag,NodExhBet_tmp,NoddRodPsnd
     *           ,NodEe,EGk,EGk1,NoddCgdCc,NodNumEe(1)
     *           ,NodPsw,NodRoWvTmp,Nod_ghm_flag,NodPropRelaxFlag
     *           ,NoddRdPGas,NoddRdHGas
     *           ,NodTtWvTmp,NodCpWvTmp,NodXxTmp,NoddRdPWvTmp,
     >NoddRdHWvTmp
     *           ,TypedROdH(1),OildROdH(1)
     *           ,WasteNoGl)
      endif
      tick(382,1) = time_df1()-time_tmp
      ErrorNum(1)=382

      time_tmp = time_df1()
      call Node_PropRelax(nu,NodPropRelaxFlag
     *,NoddRdPWvTmp,NoddRdPWv_true,Nodvvga,NodXxBalk1
     *,NodalphMax,NodPsw,masdt,NodRoWv,NodRoWtSat
     *,NodAlphXbTun,nodalph1,NodRelaxNo,NodCcGa
     *,NodMassFreez,Mass_Freez,NoddrdpWv,NodRoWvTmp,NodRoTrue
     *,NoddRdHWv,NoddRdHWvTmp,NodRoGa,NoddRdPGas,NoddRdHGas
     *,NodXxRel,NodTtWv,NodTtWvTmp,NodCpWv,NodCpWvTmp
     *,NodXxTmp,NodZalivDef,NodXxBal,NodCgasFreez)
      tick(383,1) = time_df1()-time_tmp
      ErrorNum(1)=383

      time_tmp = time_df1()
      call Nod_ghm(nu,Nod_ghm_flag,NodPsw,Nodccga,NodEega,Nodcpga
     *,NodRoGa,NodEewv,NodCpWv,NodRoWv,NodVvga,Rok,NoddRdPWv,NoddRdHwv
     *,NoddRodPk1,NoddRodEk1,NoddRdPGas,NoddRdHGas,NodCp
     *,NodCfRo1,NodCfRo2,NodCfRo,NodRoTrue
     *,NodCgGa,NodCgMa,NodCgEe,NodRoGg,NodCgSqrt
     *,NodGgSumZero,NodeSlipCor,NodBodyTyp
     *,NodCcgat,NodCgMa0,NodRoTot,NodDroDc
     *,NodXxBalk1,EeWtSatk,EeVpSatk,NodeSlipVp,NodCgVp,NoddRodPsnd
     *,NoddCgdCc,NodNumEe,NodRowvtmp,NoddRdPWvTmp,NoddRdHWvTmp
     *,NodCfTur1,NodCfTur2,NodCfad,NodXxk1)
      tick(384,1) = time_df1()-time_tmp
      ErrorNum(1)=384
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 12125 

****** End   section 'Prop' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'Prop' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'Prop' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'Prop' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'Prop' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'Prop' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'Prop' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'Prop' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'Prop' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'Prop' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'Prop' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'Prop' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'Prop' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'Prop' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'Prop' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'Prop' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'Prop' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'Prop' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'Prop' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'Prop' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'Prop' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'Prop' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'Prop' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'Prop' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** End   section 'Prop' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'Prop' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'Prop' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'Prop' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 12171 

****** End   section 'Prop' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'Prop' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'Prop' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'Prop' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'Prop' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'Prop' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'Prop' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'Prop' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** End   section 'Prop' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'Prop' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'Prop' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'Prop' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'Prop' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'Prop' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'Prop' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'Prop' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'Prop' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'Prop' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'Prop' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'Prop' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'Prop' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'Prop' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'Prop' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'Prop' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'Prop' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'Prop' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'Prop' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'


c --- ���������� ����. �������������� ���������� ��� ����. � ���.
      time_tmp = time_df1()
      call Deviation(MaxDevP,MaxDevH,Npres,Nent,Pk1,Pk,Ek1,Ek)
      tick(115,1) = time_df1()-time_tmp
      ErrorNum(1)=115

      !��� ������� �����
      time_tmp = time_df1()
      call DerivCor(nu,MaxDevP,MaxDevH,NodXxBal,NodXxBalk1,NoddRodPk,
     *              NoddRodPk1,NoddRodEk,NoddRodEk1)
      tick(116,1) = time_df1()-time_tmp
      ErrorNum(1)=116
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 12215 

****** Begin section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 12269 


****** End   section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 12291 


****** End   section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'Restore' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

****** Begin section 'Mas_Vol' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'Mas_Vol' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'Mas_Vol' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'Mas_Vol' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'Mas_Vol' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'Mas_Vol' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'Mas_Vol' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'Mas_Vol' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'Mas_Vol' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'Mas_Vol' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'Mas_Vol' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'Mas_Vol' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'Mas_Vol' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'Mas_Vol' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'Mas_Vol' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'Mas_Vol' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'Mas_Vol' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'Mas_Vol' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'Mas_Vol' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'Mas_Vol' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'Mas_Vol' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'Mas_Vol' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'Mas_Vol' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'Mas_Vol' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'Mas_Vol' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'Mas_Vol' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'Mas_Vol' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'Mas_Vol' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'Mas_Vol' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'Mas_Vol' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'Mas_Vol' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'Mas_Vol' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'Mas_Vol' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'Mas_Vol' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'Mas_Vol' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'Mas_Vol' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'Mas_Vol' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'Mas_Vol' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'Mas_Vol' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'Mas_Vol' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'Mas_Vol' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'Mas_Vol' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'Mas_Vol' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 12425 


****** End   section 'Mas_Vol' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'Mas_Vol' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'Mas_Vol' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'Mas_Vol' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 12470 


****** End   section 'Mas_Vol' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'Mas_Vol' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'Mas_Vol' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'Mas_Vol' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 12491 

****** End   section 'Mas_Vol' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'Mas_Vol' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'Mas_Vol' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'Mas_Vol' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 12561 


****** End   section 'Mas_Vol' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'Mas_Vol' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'Mas_Vol' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'Mas_Vol' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'Mas_Vol' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'Mas_Vol' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'Mas_Vol' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'Mas_Vol' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'Mas_Vol' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'Mas_Vol' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'Mas_Vol' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'Mas_Vol' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'Mas_Vol' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'Mas_Vol' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'Mas_Vol' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'Mas_Vol' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'Mas_Vol' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'Mas_Vol' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'Mas_Vol' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

****** Begin section 'WallTemp' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'WallTemp' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'WallTemp' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'WallTemp' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'WallTemp' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'WallTemp' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'WallTemp' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'WallTemp' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'WallTemp' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'WallTemp' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'WallTemp' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'WallTemp' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'WallTemp' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'WallTemp' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'WallTemp' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'WallTemp' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'WallTemp' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'WallTemp' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'WallTemp' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'WallTemp' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'WallTemp' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'WallTemp' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'WallTemp' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'WallTemp' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'WallTemp' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'WallTemp' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'WallTemp' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'WallTemp' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'WallTemp' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'WallTemp' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'WallTemp' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'WallTemp' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'WallTemp' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'WallTemp' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'WallTemp' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'WallTemp' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'WallTemp' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'WallTemp' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'WallTemp' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'WallTemp' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'WallTemp' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'WallTemp' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'WallTemp' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 12663 


****** End   section 'WallTemp' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'WallTemp' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'WallTemp' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'WallTemp' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 12700 

****** End   section 'WallTemp' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'WallTemp' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'WallTemp' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'WallTemp' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 12719 

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 12742 

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 12763 


****** End   section 'WallTemp' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'WallTemp' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'WallTemp' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'WallTemp' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 12803 


****** End   section 'WallTemp' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'WallTemp' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'WallTemp' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'WallTemp' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'WallTemp' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'WallTemp' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'WallTemp' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'WallTemp' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'WallTemp' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'WallTemp' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'WallTemp' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'WallTemp' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'WallTemp' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'WallTemp' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'WallTemp' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'WallTemp' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'WallTemp' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'WallTemp' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'WallTemp' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

c   ���������� � ��������� ��������
      time_tmp = time_df1()
      do i=1,Npres
        if(dabs(Pk1(i)).lt.1.d-20) Pk1(i)=1.d-20
          Pk(i)=Pk1(i)
        enddo
      tick(134,1) = time_df1()-time_tmp
      ErrorNum(1)=134
      time_tmp = time_df1()
      do i=1,Nent
        Ek(i)=Ek1(i)
        EGk(i)=EGk1(i)
      enddo
      tick(135,1) = time_df1()-time_tmp
      ErrorNum(1)=135

c --- ���������� ������������ ��������
****** Begin section 'Viscosity' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'Viscosity' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'Viscosity' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'Viscosity' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'Viscosity' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'Viscosity' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'Viscosity' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'Viscosity' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'Viscosity' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'Viscosity' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'Viscosity' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'Viscosity' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'Viscosity' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'Viscosity' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'Viscosity' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'Viscosity' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'Viscosity' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'Viscosity' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'Viscosity' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'

      !���������� ��������� � ������� �����
      time_tmp = time_df1()
      call Node_Vs(nu,NodPp,NodTt,NodVsVp,NodVsWt,
     *             NodVsFr,NodXx,NodBodyTyp,TypeBut,
     *             NodLaFr,NodVvga,NodRoVpSat,NodRoWtSat,NodLaCf
     *            ,NodGaType,EXbetmid,NodExhBet,NodXb_prev,NodXb_next,
     >NodVsGa,NodRo
     *                  ,NodRoWv,NodWaste_T,WasteNoGl)
      tick(169,1) = time_df1()-time_tmp
      ErrorNum(1)=169
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 12873 
****** End   section 'Viscosity' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'Viscosity' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'Viscosity' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'Viscosity' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'Viscosity' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'Viscosity' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'Viscosity' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'Viscosity' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'Viscosity' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'Viscosity' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'Viscosity' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'Viscosity' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'Viscosity' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'Viscosity' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'Viscosity' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'Viscosity' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'Viscosity' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'Viscosity' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'Viscosity' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'Viscosity' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'Viscosity' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'Viscosity' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'Viscosity' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'Viscosity' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 12906 
****** End   section 'Viscosity' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'Viscosity' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'Viscosity' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'Viscosity' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 12920 
****** End   section 'Viscosity' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'Viscosity' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'Viscosity' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'Viscosity' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 12934 
****** End   section 'Viscosity' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'Viscosity' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'Viscosity' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'Viscosity' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 12947 
****** End   section 'Viscosity' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'Viscosity' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'Viscosity' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'Viscosity' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'Viscosity' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'Viscosity' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'Viscosity' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'Viscosity' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'Viscosity' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'Viscosity' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'Viscosity' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'Viscosity' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'Viscosity' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'Viscosity' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'Viscosity' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'Viscosity' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'Viscosity' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'Viscosity' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'Viscosity' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

c     ���������� ������� ������������ ��������� �� ������
****** Begin section 'Flow_Visc' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'Flow_Visc' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'Flow_Visc' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'Flow_Visc' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'Flow_Visc' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'

c --- ����������� ��������� �� ������ ������
      time_tmp = time_df1()
      call Flw_Viscos_v1(nv,IFROKZ,ITONKz,LinIBNfro,LinIBNto,
     *                   NodVsWt,Tn3VsGa,
     *                   Tn3VsVpUp,Tn3VsWt,Tn3VsVpDo,
     *                   Tn8VsGa,Tn8VsWt,
     *                   FlwVsFrom,
     *                   FlwVsTo,Nf,nu,n03,n08,
     *                   n07,Tn7VsGa,Tn7VsWt,Tn9VsGa,Tn9VsVpUp,Tn9VsWt,
     *                   Tn9VsVpDo,Tn9VsWtUp,Tn9VsWtJa,n09,ComGamFrom,
     *                   ComGamTo,NodVsVp,NodVsFr,Rovz,NodRoGa,NodRoWv,
     *                   Tn7RoGa,Tn7RoWt,NodBodyTyp,NodVsGa)

      tick(155,1) = time_df1()-time_tmp
      ErrorNum(1)=155
c --- ����������� ������� �������� �� �����
      time_tmp = time_df1()
c       call Viscosity(nv,FlwVsFrom,FlwVsTo,FlwVs)
      tick(156,1) = time_df1()-time_tmp
      ErrorNum(1)=156
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 12996 
****** End   section 'Flow_Visc' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'Flow_Visc' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'Flow_Visc' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'Flow_Visc' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'Flow_Visc' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'Flow_Visc' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'Flow_Visc' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'Flow_Visc' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'Flow_Visc' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'Flow_Visc' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'Flow_Visc' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'Flow_Visc' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'Flow_Visc' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'Flow_Visc' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'Flow_Visc' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'Flow_Visc' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'Flow_Visc' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'Flow_Visc' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'Flow_Visc' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'Flow_Visc' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'Flow_Visc' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'Flow_Visc' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'Flow_Visc' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'Flow_Visc' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'Flow_Visc' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'Flow_Visc' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'Flow_Visc' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'Flow_Visc' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'Flow_Visc' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'Flow_Visc' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'Flow_Visc' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'Flow_Visc' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'Flow_Visc' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'Flow_Visc' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'Flow_Visc' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'Flow_Visc' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'Flow_Visc' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'Flow_Visc' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** End   section 'Flow_Visc' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'Flow_Visc' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'Flow_Visc' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'Flow_Visc' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'Flow_Visc' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'Flow_Visc' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'Flow_Visc' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'Flow_Visc' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'Flow_Visc' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'Flow_Visc' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'Flow_Visc' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'Flow_Visc' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** End   section 'Flow_Visc' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'Flow_Visc' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'Flow_Visc' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'Flow_Visc' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'Flow_Visc' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'Flow_Visc' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'Flow_Visc' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'Flow_Visc' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'Flow_Visc' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'Flow_Visc' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'Flow_Visc' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'Flow_Visc' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'Flow_Visc' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'Flow_Visc' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'Flow_Visc' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'Flow_Visc' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'Flow_Visc' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'Flow_Visc' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'Flow_Visc' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

c   ������ ������� ��������� � ����. ������ �������� �� ������
      time_tmp = time_df1()
        call Flw_Xx_v1(nv,n03,Nf,IFROKZ,ITONKZ,LinIBNfro,
     *                 LinIBNto,NodXxBalk1,Tn8XxBalWt,
     *                 Tn3Xxk,
     *                 FlwXxBalFrom,FlwXxBalTo,FlwXxFrom,
     *                 FlwXxTo,Tn7Xk(1,2),n09,Tn9Xxk,
     *                 ComGamFrom,ComGamTo,nu,n07,n08,
     *                 NodXb_prev,NodXb_next,Nodxb_filter,Gk)
      tick(138,1) = time_df1()-time_tmp
      ErrorNum(1)=138
      time_tmp = time_df1()
        call Density_new(nv,nu,
     *               n07,n03,n08,n09,
     *               LinIBNfro,LinIBNto,FlwRoFrom,
     *               IFROKZ,ITONKZ,
     *               FlwRoTo,
     *               ROVz,Rovk,
     *               FlwLl,FlwSq,Gk,FlwRo,
     *               dtInt,FlwRoFrc,Hfrom,Hto,
     *               AconRovk,Tn7lev,Tn8lev,Tn3Lev,Tn9Lev,NodTypTnk,
     >FlwResTyp,
     *               FlwHTnToDo,FlwHTnToUp,
     *               FlwHTnFromDo,FlwHTnFromUp,
     *               FlwVsFrom,FlwVsTo,FlwVsk,FlwVs,
     *               FlwRoFromSt,
     *               FlwRoToSt,NodLaFr,FlwLaFrc,FlwRoFlag,FlwCfdh,
     *               NodGgSumZero,NodGgSumZeroin,NodLaFr_cor(1),
     *                 FlwDensCfL,FlwRoC)

      tick(139,1) = time_df1()-time_tmp
      ErrorNum(1)=139
      time_tmp = time_df1()
      call Volume(nv,LinIBNFro,LinIBNTo,IFROKZ,ITONKZ,
     *            HFrom,HTo,
     *            Tn8Lev,Tn3LevRel,Tn9LevRel,
     *            Tn8RoWtk,Tn3Rok(1,3),Tn9Rok(1,3),
     *            dHLev,FlwdPStat,
     *            FlwRoFrc,
     *            InternalIter,Tn3EvDo,
     *            Tn7Levk,Tn7Rok(1,2),Tn9EvDo,
     *            Tn8RoGak,Tn3Rok(1,1),Tn9Rok(1,1),
     *            Tn7Rok(1,1),Gk,aconRovk,
     *            FlwdPLevFrom,FlwdPLevTo,
     *            Flwdd,FlwAngFrom,FlwAngTo,Tn3RoDo,Tn9RoDo
     *                 ,FlwHTnFromUp,FlwHTnFromDo,FlwHTnToUp,FlwHTnToDo
     *                 ,FlwRoFrom,FlwRoTo,n08,n03,n07,n09,FlwKkdh,
     >FlwFlagm2,
     *            NodCcGa,NodRoWtSat,NodRoGa,Tn3RoWpUp,Tn9RoVpUp)
      tick(140,1) = time_df1()-time_tmp
      ErrorNum(1)=140

c   ���� �������� ����� � ������� �� ������
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 13119 

****** Begin section 'CALL_TEMP' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'CALL_TEMP' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'CALL_TEMP' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'CALL_TEMP' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'CALL_TEMP' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'CALL_TEMP' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'CALL_TEMP' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'CALL_TEMP' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'CALL_TEMP' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 13149 
****** End   section 'CALL_TEMP' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'CALL_TEMP' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'CALL_TEMP' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'CALL_TEMP' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'CALL_TEMP' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'CALL_TEMP' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'CALL_TEMP' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'CALL_TEMP' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'CALL_TEMP' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'CALL_TEMP' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'CALL_TEMP' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'CALL_TEMP' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'CALL_TEMP' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'CALL_TEMP' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'CALL_TEMP' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'CALL_TEMP' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'CALL_TEMP' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'CALL_TEMP' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'CALL_TEMP' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'CALL_TEMP' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'CALL_TEMP' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'CALL_TEMP' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'CALL_TEMP' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'CALL_TEMP' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'CALL_TEMP' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'CALL_TEMP' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'CALL_TEMP' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'CALL_TEMP' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'CALL_TEMP' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'CALL_TEMP' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'CALL_TEMP' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'CALL_TEMP' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'CALL_TEMP' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'CALL_TEMP' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** End   section 'CALL_TEMP' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'CALL_TEMP' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'CALL_TEMP' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'CALL_TEMP' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'CALL_TEMP' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'CALL_TEMP' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'CALL_TEMP' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'CALL_TEMP' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'CALL_TEMP' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'CALL_TEMP' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'CALL_TEMP' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'CALL_TEMP' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** End   section 'CALL_TEMP' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'CALL_TEMP' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'CALL_TEMP' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'CALL_TEMP' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'CALL_TEMP' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'CALL_TEMP' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'CALL_TEMP' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'CALL_TEMP' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'CALL_TEMP' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'CALL_TEMP' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'CALL_TEMP' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'CALL_TEMP' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'CALL_TEMP' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'CALL_TEMP' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 13244 
****** Begin section 'CALL_TEMP' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'CALL_TEMP' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'CALL_TEMP' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'CALL_TEMP' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

      if(AlfaItFlag(1).eq.1) Then
****** Begin section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'

c   ���������� ����������� ������ ��� ������� ���������� �����
      time_tmp = time_df1()
        call WL1_Temp(nu,nwall,Wl1Node,C1serv,C2serv
     &               ,Wl1AlCulcNode,Wl1AlCulcEnv
     &               ,Wl1QqCl,Wl1QqEnv,NodQqWl
     &               ,NodTtk,Wl1Tt,Wl1TtEnv
     &               ,Wl1CfCulcEnv,Wl1CfCulcNode,Wl1CfAllEnv,
     &                Wl1IndRoom,NRm1,Rm1Qq,Wl1QqNodeSum)
      tick(380,1) = time_df1()-time_tmp
      ErrorNum(1)=380
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 13328 
c   ���������� ������������� ������������� ���������� �� �������
      time_tmp = time_df1()
C        call WL1_alfaSq(Nv,Nu,Nwall,WL1Node
C     &                 ,Wl1CulcTyp,Wl1AlCulcNode,Wl1AlCulcEnv,Wl1QqCl
C     &            ,NodTt,NodPp,NodVsWt,NodVsVp,NodTtSat,NodXxBal,NodGgSumIn
C     &            ,WL1Thick,WL1DdEqv,WL1Sq,WL1SqEnv,Wl1SqCros
C     &            ,WL1AlfaNode,WL1AlfaEnv,WL1Tt,WL1Lamd,Wl1LamdC
C     &            ,NodBodyTyp,TypeBut )
       CALL WL1_alfaSq(Nv,Nu,Nwall,WL1Node
     &            ,Wl1CulcTyp,Wl1AlCulcNode,Wl1AlCulcEnv,Wl1QqCl
     &            ,NodTtk,NodPp,NodVsWt,NodVsVp,NodTtSat,NodXxBal,
     >NodGgSumIn
     &            ,WL1Thick,WL1DdEqv,WL1Sq,WL1SqEnv,Wl1SqCros
     &            ,WL1AlfaNode,WL1AlfaEnv,WL1Tt,WL1Lamd,Wl1LamdC
     &            ,NodBodyTyp,TypeBut
     - ,NODEEWV,NodEeWtSat,NodEeVPSat,NODRO,NodRoVpSat
     - ,NODROWTSAT,NodRoGa,NodcpGa,NodCgGa,VOLUKZ,NODSIGMASURF
     -,NODFLOWREGIME,ALFAFLAG(1),NodLaFr,ATYPPCRIT(1),Wl1alprev,NodVsGa
     &            ,Wl1CfCulcNode,Wl1CfCulcEnv,Wl1CfAllEnv,NodAlphaMin
     &            ,Wl1CfAllAlf
     & ,Wl1AlfaMinNode,Wl1DecCond,Wl1Decboil
     *          ,NodGgSumZeroIn,Wl1Robn,NodWaste_T,WasteNoGl)

      tick(381,1) = time_df1()-time_tmp
      ErrorNum(1)=381
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 13354 
****** End   section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
      end if


****** Begin section 'CALL_TEMP_Met' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'CALL_TEMP_Met' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'CALL_TEMP_Met' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'CALL_TEMP_Met' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'CALL_TEMP_Met' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'CALL_TEMP_Met' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'CALL_TEMP_Met' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'CALL_TEMP_Met' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'CALL_TEMP_Met' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'CALL_TEMP_Met' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'CALL_TEMP_Met' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'CALL_TEMP_Met' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'CALL_TEMP_Met' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'

c   ���������� ����������� ������ ��� ������� ���������� �����
        call Mtl_Temp(NMtl,Mtl_QqOut,MtlTt,MtlMass,MtlCp
     &               ,MtlQq,MtlTt_t,dtInt,MtlAlCulc
     &               ,MtlType,MtlintTt,MtlCfMasMe)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 13386 

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 13394 
****** End   section 'CALL_TEMP_Met' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'CALL_TEMP_Met' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'CALL_TEMP_Met' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'CALL_TEMP_Met' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'CALL_TEMP_Met' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'CALL_TEMP_Met' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'CALL_TEMP_Met' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'CALL_TEMP_Met' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'CALL_TEMP_Met' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'CALL_TEMP_Met' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'CALL_TEMP_Met' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'CALL_TEMP_Met' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'CALL_TEMP_Met' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'CALL_TEMP_Met' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'CALL_TEMP_Met' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'CALL_TEMP_Met' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'CALL_TEMP_Met' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'CALL_TEMP_Met' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'CALL_TEMP_Met' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'CALL_TEMP_Met' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'CALL_TEMP_Met' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'CALL_TEMP_Met' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'CALL_TEMP_Met' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'CALL_TEMP_Met' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'CALL_TEMP_Met' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'CALL_TEMP_Met' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'CALL_TEMP_Met' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'CALL_TEMP_Met' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'CALL_TEMP_Met' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'CALL_TEMP_Met' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** End   section 'CALL_TEMP_Met' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'CALL_TEMP_Met' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'CALL_TEMP_Met' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'CALL_TEMP_Met' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'CALL_TEMP_Met' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'CALL_TEMP_Met' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'CALL_TEMP_Met' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'CALL_TEMP_Met' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'CALL_TEMP_Met' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'CALL_TEMP_Met' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'CALL_TEMP_Met' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'CALL_TEMP_Met' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** End   section 'CALL_TEMP_Met' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'CALL_TEMP_Met' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'CALL_TEMP_Met' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'CALL_TEMP_Met' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'CALL_TEMP_Met' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'CALL_TEMP_Met' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'CALL_TEMP_Met' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'CALL_TEMP_Met' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'CALL_TEMP_Met' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'CALL_TEMP_Met' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'CALL_TEMP_Met' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'CALL_TEMP_Met' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'CALL_TEMP_Met' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'CALL_TEMP_Met' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'CALL_TEMP_Met' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'CALL_TEMP_Met' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'CALL_TEMP_Met' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'CALL_TEMP_Met' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'CALL_TEMP_Met' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

c   ����� ��� ���� ������ �������
        IC=IC+1
        if(IC.lt.MaxIternew(1)) go=.true.
c\/\/\/\ ��������� �.�. (������) /\/\/\/\/\/\/\/\/\/\
      time_tmp = time_df1()
      if(dPMaxFlag(1)) then
       call dP_Max(Npres,nu,n03,n08,
     *      Pk,dP,ic,icint,11,NumMax,TypMax,dPMax,
     *      invperP,NodRelP,Tn3RelP,Tn8RelP
     *      ,n07,n09,Tn7RelP,Tn9RelP)
      call dM_Max(nu,Masdt,Volukz,ic,icint,2,
     *            NumMaxM,dMMax)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 13474 
      endif
      tick(141,1) = time_df1()-time_tmp
      ErrorNum(1)=141
c\/\/\/\ ��������� �.�. (�����) /\/\/\/\/\/\/\/\/\/\

!      enddo
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 13539 
      tick(146,1) = time_df1()-time_tmp
      ErrorNum(1)=146
C     call kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 13553 
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 13628 
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 13700 

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 13704 

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 13710 

      end

C-----THIS IS THE END
****** End   file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET42.fs'
****** Begin file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET43.fs'

        subroutine kts_h_end
        implicit none
      include 'kts_h.fh'
      REAL*8 FlwTnSqFrom(894)
      COMMON/kts_h_nowrt/ FlwTnSqFrom
      REAL*8 FlwHTnFromUp(894)
      COMMON/kts_h_nowrt/ FlwHTnFromUp
      REAL*8 FlwTnSqTo(894)
      COMMON/kts_h_nowrt/ FlwTnSqTo
      REAL*8 FlwHTnFromDo(894)
      COMMON/kts_h_nowrt/ FlwHTnFromDo
      REAL*8 FlwHTnToUp(894)
      COMMON/kts_h_nowrt/ FlwHTnToUp
      REAL*8 FlwHTnToDo(894)
      COMMON/kts_h_nowrt/ FlwHTnToDo
      INTEGER*4 Fan2NTN2N
      PARAMETER (Fan2NTN2N=0)
      INTEGER*4 Fan2NTN0N
      PARAMETER (Fan2NTN0N=0)
      INTEGER*4 Fan2NTauNN
      PARAMETER (Fan2NTauNN=0)
      INTEGER*4 Fan2NdPG5N
      PARAMETER (Fan2NdPG5N=0)
      INTEGER*4 Fan2NdPG4N
      PARAMETER (Fan2NdPG4N=0)
      INTEGER*4 Fan2NdPG3N
      PARAMETER (Fan2NdPG3N=0)
      INTEGER*4 Fan2NdPG2N
      PARAMETER (Fan2NdPG2N=0)
      INTEGER*4 Fan2NdPG0N
      PARAMETER (Fan2NdPG0N=0)
      INTEGER*4 Fan2TauNN
      PARAMETER (Fan2TauNN=0)
      INTEGER*4 FanDim_TauNmax
      PARAMETER (FanDim_TauNmax=0)
      INTEGER*4 FanDim_Taumax
      PARAMETER (FanDim_Taumax=0)
      INTEGER*4 FanDim_KMax
      PARAMETER (FanDim_KMax=0)
      INTEGER*4 FanDim_PMax
      PARAMETER (FanDim_PMax=0)
      INTEGER*4 Tn8Ne
      PARAMETER (Tn8Ne=2)
      INTEGER*4 Tn3Np
      PARAMETER (Tn3Np=2)
      INTEGER*4 Tn3Ne
      PARAMETER (Tn3Ne=4)
      INTEGER*4 INVPERe(551)
      COMMON/kts_h_nowrt/ INVPERe
      INTEGER*4 NZSUBe(4335)
      COMMON/kts_h_nowrt/ NZSUBe
      INTEGER*4 INZSUBe(552)
      COMMON/kts_h_nowrt/ INZSUBe
      INTEGER*4 ILNZe(552)
      COMMON/kts_h_nowrt/ ILNZe
      INTEGER*4 INVPERp(551)
      COMMON/kts_h_nowrt/ INVPERp
      INTEGER*4 NZSUBp(4335)
      COMMON/kts_h_nowrt/ NZSUBp
      INTEGER*4 INZSUBp(552)
      COMMON/kts_h_nowrt/ INZSUBp
      INTEGER*4 ILNZp(552)
      COMMON/kts_h_nowrt/ ILNZp
      INTEGER*4 Nthermo
      PARAMETER (Nthermo=894)
      INTEGER*4 Nhydro
      PARAMETER (Nhydro=894)
      INTEGER*4 Nent
      PARAMETER (Nent=551)
      INTEGER*4 Npc
      PARAMETER (Npc=1102)
      INTEGER*4 Npres
      PARAMETER (Npres=551)
      INTEGER*4 Nf
      PARAMETER (Nf=6)
      INTEGER*4 Tn7Ne
      PARAMETER (Tn7Ne=1)
      INTEGER*4 Tn7Np
      PARAMETER (Tn7Np=1)
      INTEGER*4 Tn9Ne
      PARAMETER (Tn9Ne=6)
      INTEGER*4 Tn9Np
      PARAMETER (Tn9Np=2)
      INTEGER*4 Fan2NTN3N
      PARAMETER (Fan2NTN3N=0)
      INTEGER*4 Fan2NTN4N
      PARAMETER (Fan2NTN4N=0)
      INTEGER*4 Fan2NTN5N
      PARAMETER (Fan2NTN5N=0)
      INTEGER*4 Tr1Dim_Nu
      PARAMETER (Tr1Dim_Nu=-111)
      INTEGER*4 Tr2Dim_Ksi
      PARAMETER (Tr2Dim_Ksi=-111)
      INTEGER*4 Tn8Np
      PARAMETER (Tn8Np=1)
      LOGICAL*1 PIntIter(551)
      COMMON/kts_h_nowrt/ PIntIter
      LOGICAL*1 EIntIter(551)
      COMMON/kts_h_nowrt/ EIntIter
      LOGICAL*1 EextIter(551)
      COMMON/kts_h_nowrt/ EextIter
      LOGICAL*1 PExtIter(551)
      COMMON/kts_h_nowrt/ PExtIter
      !DEC$PSECT/kts_h_NOWRT/ NoWRT
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 13723 
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
      integer*4 Ncomp,CompLin(1),CompOin(1),CompGenTyp(1)
      parameter(Ncomp=0)
      real*8 CompQq(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 13731 
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 13736 


****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'

c      real*8 FlwCondRegul(nv)
c      common/kts_hFlwL/ FlwCondRegul
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 13747 

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 13782 
      integer*4 nfwv
      parameter(nfwv=0)
      integer*4 FwvFloWw(1),FwvFloWv(1)
      real*8 FwvRoFrom1(1),FwvRoFrom2(1),FwvRoTo1(1),FwvRoTo2(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 13788 
      integer*4 nfwv1
      parameter(nfwv1=0)
      integer*4 Fwv1Flow(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 13793 
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
      integer NHe2
      parameter(NHe2=0)
* ���������� ��� ��������������� ��������� ����������
      real*8 C0He2(1) !����. ��� ������� ����������� ���� ��
      real*8 C1He2(1) !����. ��� ������� ����������� ���� ��
      real*8 C2He2(1) !����. ��� ������� ����������� ���� ��
      real*8 DTHe2
      real*8 C0He2b1(1),C1He2b1(1),C2He2b1(1)
      real*8 C0He2b2(1),C1He2b2(1),C2He2b2(1)
      real*8 He2T1t(2,1),He2T2t(2,1),He2decQt(1),He2_gar2(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 13809 
C     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 13823 
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'

      real*8 Mtl_QqOut(NMtl)  !��������� �������� ����� � ������� ��������
      real*8 MtlTt_t(NMtl)
      real*8 MtlAlCulc(nMtl)
      common/kts_hMetal/ Mtl_QqOut,MtlTt_t,MtlAlCulc
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 13834 

C     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 13841 

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 13849 

      integer*4 NMtlO,MtlOIndRoom(1)
      parameter ( NMtlO=0 )
      real*8 MtlOTt(1),MtlOMass(1),MtlOCp(1),MtlOAlCulcWl1(1)
      real*8 MtlO_QqOut(1)  !��������� �������� ����� � ������� ��������
      real*8 MtlOLamdC(1),MtlOLamd(1),MtlOQqEnv(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 13857 
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'

      integer*4 NUCH
      parameter (NUCH=0)
      integer*4 CoalHConNodInd(1)
      real*8 CoalHNodMoistG(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 13866 

      integer*4 NFlwWasteSet
      parameter(NFlwWasteSet=0)
      integer*4 FlwWasteSetConnInd(1),FlwWasteSetNum(1)
      real*8 FlwWasteSetWaste(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 13873 

      integer*4 FlwConnFlwInd(1),NFlwConn
      parameter(NFlwConn=0)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 13878 

C     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 13883 

      integer*4 NHCO
      parameter(NHCO=0)
      integer*4  HCOFlMod(1),HCOIndFlw(1)
      real*8 HCOCfMod(1),HCOOFg(1),HCOONodVol(1),HCOONoddRodP(1)
      common/kts_hConFlOut/ HCOFlMod,HCOCfMod,HCOOFg,HCOONodVol,
     >HCOONoddRodP,HCOIndFlw
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 13891 

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 13898 

      integer*4 nCrv
      parameter(nCrv=0)
      real*8 CrvRes(1)
      integer*4 CrvFlw(nCrv)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 13905 

      integer*4 nWst
      parameter(nWst=0)
      integer*4 WstNode(1)
      real*8    WSTConsN2(1),WSTConsO2(1),WSTConsH2O(1),
     *          WSTConsCO2(1),WSTConsSoot(1),WSTConsAsh(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 13913 

      integer*4 NXS
      parameter(NXS=0)
      integer*4 XSFlagSetCc(1),XSFlSet(30,1),XSIndSetCc(1),
     >XSIndSetCc0(1)
      real*8    XSCcstr(30,1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 13920 

      integer*4 ntFrB
      parameter(ntFrB=0)
      integer*4 tFrBNdInd(1),tFrBi(1)
      real*8    tFrBCcOld(100,1),tFrBTau(1)
      real*8    tFrBGIn(1),tFrBAa(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 13928 

      integer*4 np4
      parameter(np4=0)
      integer*4 Pm4Nb(1)
      real*8 Pm4Res(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 13935 

c ��������� �����. ��� ������� � ���������������, ���� �� (�������) ���
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 13943 
      integer*4 np2
      parameter(np2=0)
      integer*4 Pm2Nb(1)
      real*8 Pm2Res(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 13949 
      integer*4 nFan
      parameter(nFan=0)
      integer*4 FanFlw(1)
      real*8 FanRes(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 13955 
      integer*4 nFan2
      parameter(nFan2=0)
      integer*4 Fan2Flw(1)
      real*8 Fan2Res(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 13961 
      integer*4 np3
      parameter(np3=0)
      real*8 Pm3QqNod(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 13966 
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 13976 

      real*8 time_df1  !������� ��� ������� ������� (� �����)
      real*8 time_tmp

      real   deltat      !��� ����������
      real   deltat1      !��� ����������
      REAL*8 Atau        !1/dt
c      save Atau
      real*4 dtInt       !dt*Accel
      real*8 TimeSrv     !dt/Niter
      character*100 fn1  ! ��� ������������� .cbf ������
c      common/kts_hLocal1/ time_tmp,deltat,Atau,dtInt,TimeSrv,fn1
      common/kts_hLocal1/ Atau,time_tmp,deltat1,dtInt,TimeSrv,fn1
      real*8 VOL1(nu) !V/dt
      real*8 NodCfdE(nu) !����. � ��. ����. ��� ����� ���������
      real*8 NodTtk(nu)  !����������� ��������. � ������� ����� �� k-�� ��������
      real*8 RotVol(nu) !��������� ������
      real*8 RokVol(nu) !��������� ������
      real*8 NodVolk(nu)  !������������ ����� �� k-�� ��������
      real*8 NoddVdPk(nu) !����������� ������ � ���� �� �������� �� k-�� ����.

c ---  ������ �. (29.08.02) - ������
      real*8 NodSum1(nu)
      real*8 NodSum2(nu)
c ---  ������ �. (29.08.02) - �����

      real*8 NodQqPmAd(nu)

      common/kts_hNODE1/ VOL1,NodCfdE,NodTtk,RotVol,RokVol,
     * NodVolk,NoddVdPk,NodSum1,NodSum2,NodQqPmAd
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 14008 

      real*8 C7(nv)   !��������� ������������
      real*8 Gk(nv)   !������� �� k-�� ��������
      real*8 dHLev(nv)!������ �� ����� �� ���� ������� � �������
      real*8 Rovk(nv)    !��. ��������� �� k-�� ��������
      real*8 AconRovk(nv)!��������� ��� ��������
c###### ��������� ####################################
      real*8 FlwGaVeSq(nv)  ! ��������� ������ ��� �������� ������������� �����
      real*8 flwbcon(nv)    ! �������� ��������� �������� �������� �� ����� 
      real*8 FlwC7work1(nv)   ! ��������� ������ ��� �������� ������������� �����
      real*8 FlwC7work2(nv)   ! ��������� ������ ��� �������� ������������� �����
      real*8 FlwC7work3(nv)   ! ��������� ������ ��� �������� ������������� �����
      real*8 FlwC7work4(nv)   ! ��������� ������ ��� �������� ������������� �����
      real*8 FlwC7work5(nv)   ! ��������� ������ ��� �������� ������������� �����
      real*8 FlwC7work7(nv)
      real*8 FlwVsk    (nv)   ! �������� �� ����� �� ���������
      real*8 ComEeFrom  (nv)   ! ��������� ��������� ��������� � ��������
      real*8 ComEeTo    (nv)   ! ��������� ��������� ��������� � ��������  
      real*8 FlwSlGaFrom(nv)   ! �������� ��� ��������������� ���� �� �����
      real*8 FlwSlGaFrom0(nv)  ! �������� ��� ��������������� ���� �� �����
      real*8 FlwSlGaTo   (nv)  ! �������� ��� ��������������� ���� �� �����
      real*8 FlwSlGaTo0  (nv)  ! �������� ��� ��������������� ���� �� �����
      real*8 FlwCcFrom(nv) ! ��������� ������ ��� ������������ ���� �� ������ �����
      real*8 FlwCcTo(nv)   ! ��������� ������ ��� ������������ ���� �� ������ �����
      real*8 flwdhz0(nv)
      real*8 ComRoFrom(3,nv)
      real*8 ComRoTo  (3,nv)
c###### ��������� ####################################
      real*8 FlwKgp(nf,2,nv)    ! 
      real*8 FlwKge(nf,nf,nv)
      real*8 FlwKgg(3,nv)
      real*8 FlwKgpt(2,nv)
      real*8 FlwdPLevFrom(nv),FlwdPLevTo(nv)
      real*8 FlwCcGgWtJa(nv)
      real*8 FlwRes_Iter(nv)
      real*8 FlwGG_TMP(nv)
      real*8 FlwRes_Tmp
      real*8 FlwdCgDCcFrom(nv),FlwdCgDCcTo(nv)
c      real*8 frco0(nv)
      common/kts_hFLOW/ C7,Gk,dHLev,Rovk,AconRovk,FlwGaVeSq,
     * flwbcon,FlwC7work1,
     * FlwC7work2,FlwC7work3,FlwC7work4,FlwC7work5,FlwC7work7,
     * FlwVsk,ComEeFrom,ComEeTo,FlwSlGaFrom,FlwSlGaFrom0,
     * FlwSlGaTo,FlwSlGaTo0,FlwCcFrom,FlwCcTo,flwdhz0,ComRoFrom,
     * ComRoTo,FlwKgp,FlwKge,FlwKgg,FlwKgpt,
     * FlwdPLevFrom,FlwdPLevTo,FlwCcGgWtJa,
     * FlwRes_Iter,FlwRes_Tmp,FlwGG_TMP,FlwdCgDCcFrom,FlwdCgDCcTo
c    * ,frco0
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 14058 

C     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 14063 

C     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 14068 


c      !������� ����. ����� ����� � ���. ��������� ��� ��������
       real*8 Dmp(Npres,Npres)
C       real*8 Dmpc(Npc,Npc) ! �������� 
c      !������� ����. ����� ����� � ������� ��������� ��� ���������
       real*8 Dmh(Nent,Nent)
       real*8 DmhG(Nent,Nent)
c      !������ ����� ��� ���������� ������� �� ����.
      Real*8 Yp(Npres)
c      Real*8 Ypc(Npc)
c      !������ ����� ��� ���������� ������� �� ���.
      real*8 Ye(Nent)
      real*8 YeG(Nent)
c      !������� �� ���������
      real*8 dP(Npres)
c      real*8 dPC(Npc)
c      !������� �� ����������
      real*8 dE(Nent)
      real*8 dEG(Nent)
c      !������ ������ �������� �� k-�� ��������
      real*8 Pk(Npres)
c      !������ ������ �������� �� (k+1)-�� ��������
      real*8 Pk1(Npres)
c      !������ ������ ��������� �� k-�� ��������
      real*8 Ek(Nent)
      real*8 EGk(Nent)
c      !������ ������ ��������� �� (k+1)-�� ��������
      real*8 Ek1(Nent)
      real*8 EGk1(Nent)
      real*8 Rok(Nu) !�����. �\� ����� � ������� ����� �� k-�� ����.
      !������ ������ ��������� �����. ����
      real*8 EeWtSatk(Nent)
      !������ ������ ��������� �����. ����
      real*8 EeVpSatk(Nent)
      real*8 MaxDevP     !����. �����. ����. �������� �� ��������
      real*8 MaxDevH     !����. �����. ����. ��������� �� ��������
      INTEGER*4 I,j     !���������� �����
      integer*4 IC        !������� ��������
      integer*4 ICInt !������� ���������� ��������
      common/kts_hLocal2/ Dmp,Dmh,Yp,Ye,dP,dE,Pk,Pk1,Ek,Ek1,Rok,
     *  EeWtSatk,EeVpSatk,MaxDevP,MaxDevH,IC,ICInt,DmhG,YeG,dEG,
     *  EGk,EGk1       !,dPC,Ypc,Dmpc <=������ ��������!!!!

C     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 14116 

      INTEGER*4 nvlgpos !����� ����� � �������������� ���������� � ����������
      common/kts_hVlvGeo/ nvlgpos
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 14121 

C     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 14126 

C     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 14131 

C     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 14136 

C     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 14141 

      integer*4 nsep1,Sep1FlwS(1),Sep1Node(1)
        parameter(nsep1=0)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 14146 

      integer*4 nInWst,InWstNum(1),InWstNode(1),InWstNdTp(1)
      real*8    InWstdt(1)
      integer*4 InWstTyp(1)
      real*8    InWstCc(1)
      real*8 InWstExtCon(1),InWstExtWstdt(1)
      parameter(nInWst=0)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 14155 

C     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 14160 


      LOGICAL*1 go,goint  !������� ����������� ��������
      DATA go/.true./     !����������� �������� �������. ��������
      common/kts_hLocal4/ go,goint
      real*8  RDTSC
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 14199 
      real*8 NoddRodPk(nu) !dRo/dP ��� ������� ����� �� ����. ����.
      real*8 NoddRodEk(nu) !dRo/d� ��� ������� ����� �� ����. ����.
      real*8 NoddRodPk1(nu) !dRo/dP ��� ������� ����� �� ���. ����.
      real*8 NoddRodEk1(nu) !dRo/d� ��� ������� ����� �� ���. ����.
      real*8 nodrorelax(nu)
      real*8 NodRoGa0(nu)
      real*8 NodCcGa_Aeq0(nu) 
      real*8 NodCcGa_Deq0(nu) 
      real*8 NodCcGa_Beq (nu)
      real*8 NodCcGa_Beq0(nu)
      real*8 NodCcGa_Aeq (nu)
      real*8 NodCcGa_Deq (nu)
      real*8 NodXxk1(nu)    !���. �������. � ������� ����� �� ���. ����.
      real*8 NodXxBalk1(nu) !���. �������. � ������� ����� �� ���. ����.
      real*8 NodQqAd(nu)
      real*8 NodCfEeAsh(nu)
      real*8 NodEeWvt(nu)
      real*8 NodAlSqGaWv(nu),NodYe(nu),NodDmh(nu)
      real*8 NodPsw(nu),NodRoWvTmp(nu),NoddRdHGas(nu)
     *,NoddRdPWvTmp(nu),NoddRdHWvTmp(nu)
     *,NodTtWvTmp(nu),NodCpWvTmp(nu),NodXxTmp(nu)
      integer*4 Nod_ghm_flag(nu),NodPropRelaxFlag(nu)
      logical*1 EExtIter_(Nent)
      logical*1 PExtIter_(Npres)
      common /kts_hNODE/ NoddRodPk,NoddRodEk,NoddRodPk1,NoddRodEk1,
     *  nodrorelax,NodRoGa0,NodCcGa_Aeq0,NodCcGa_Deq0,NodCcGa_Beq,
     *  NodCcGa_Beq0,NodCcGa_Aeq,NodCcGa_Deq,NodXxk1,NodXxBalk1,
     *  NodQqAd,NodCfEeAsh,NodEeWvt,NodAlSqGaWv,NodYe,NodDmh,
     *  NodPsw,NodRoWvTmp,NoddRdHGas,NodTtWvTmp,NodCpWvTmp,NodXxTmp
      common /kts_hNODE_I/ Nod_ghm_flag,NodPropRelaxFlag
      common /kts_hNODE_L/ EExtIter_,PExtIter_
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 14232 
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'

      integer NTc0
      parameter(NTc0=0)
      integer*4 Tc0IbnFro(1),Tc0NodFro(1),Tc0IbnTo(1),Tc0NodTo(1)
      real*8 Tc0zt(1),Tc0AlG23(1),Tc0AlG43(1)
     *,Tc0G43(1),Tc0G23(1),Tc0G34(1),Tc0CpWl(1)
     *,Tc0TtEnv(1),Tc0TtWlk(1),Tc0TtWl(1),Tc0AlSqEnv(1),Tc0AlEnv(1)
     *,Tc0AlSqPhWl(1,nf),Tc0MasCpWl(1),Tc0CfAlEnv(1)
     *,Tc0CfAlGaWl(1),Tc0CfAlWpUpWl(1)
     *,Tc0CfAlWtWl(1),Tc0CfAlWpDoWl(1)
     *,Tc0EvUpWl(1),Tc0EvDoWl(1)
     *,Tc0dG23dp(1),Tc0dG34dp(1),Tc0dG43dp(1)
     *,Tc0CfGgWpUpWt(1),Tc0CcGgWtWpDo(1),Tc0CfGgWpDoWt(1)
     *,Tc0G32(1),Tc0dG32dp(1),Tc0CfGgWt(1),Tc0SqWl(1),Tc0TettaQ(1,nf)
     *,Tc0QqIsp(1),Tc0AlIsp(1)
     *,Tc0dQIspdP(1),Tc0TtTnIsp(1),Tc0AlSqIsp(1),TC0ResWl(1)
     *,Tc0dQG23dp(1),Tc0TettaQ2(1,nf),Tc0DeltaTs(1)
     *,Tc0AlG23From(1),Tc0AlG43From(1)
     *,Tc0G23From(1),Tc0G34From(1),Tc0G32From(1),Tc0G43From(1)
     *,Tc0dG23dpFrom(1),Tc0dG34dpFrom(1),Tc0dG43dpFrom(1),
     >Tc0dG32dpFrom(1)
     *,Tc0dQIspdPFrom(1),Tc0TtTnIspFrom(1),Tc0AlSqIspFrom(1)
     *,Tc0CcGgWtWpDoFrom(1),Tc0CfGgWpUpWtFrom(1),Tc0CfGgWpDoWtFrom(1)
     *,Tc0QqIspFrom(1),Tc0AlIspFrom(1),Tc0CfGgWtFrom(1),Tc0TettaQFrom(1,
     >nf)
     *,Tc0AlSqPhWlFrom(1,nf),Tc0CfRelAlIsp(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 14262 
C     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 14291 

****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'

      integer NTc2
      parameter(NTc2=0)
      integer*4 Tc2Ibn(1),Tc2Node(1)
      real*8 Tc2zt(1),Tc2QqEnv(1),Tc2AlG23(1),Tc2G23(1),Tc2G34(1)
     *,Tc2AlG43(1),Tc2G43(1),Tc2CpWl(1)
     *,Tc2TtEnv(1),Tc2TtWlk(1),Tc2TtWl(1),Tc2AlSqEnv(1),Tc2AlEnv(1)
     *,Tc2AlSqPhWl(1,nf),Tc2MasCpWl(1),Tc2CfAlEnv(1)
     *,Tc2CfAlGaWl(1),Tc2CfAlWpUpWl(1),Tc2CfAlWtWl(1),Tc2CfAlWpDoWl(1)
     *,Tc2EvUpWl(1),Tc2EvDoWl(1)
     *,Tc2dG23dp(1),Tc2dG34dp(1),Tc2dG43dp(1)
     *,Tc2CfGgWpUpWt(1),Tc2CcGgWtWpDo(1),Tc2CfGgWpDoWt(1)
     *,Tc2G32(1),Tc2dG32dp(1),Tc2CfGgWt(1),Tc2SqWl(1),Tc2TettaQ(1,nf)
     *,Tc2QqIsp(1),Tc2AlIsp(1),Tc2DeltaTs(1)
     *,Tc2dQIspdP(1),Tc2TtTnIsp(1),Tc2AlSqIsp(1),TC2ResWl(1),
     * Tc2LlWl(1),Tc2LaWl(1),Tc2ResWl0(1),Tc2CfRelAlIsp(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 14314 
C     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 14328 

****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'

C     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 14338 

      integer*4 NTCR,TcRNode(1)
      parameter ( NTCR=0 )
      real*8 TcR_XQ(1),TcR_BQ(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 14344 

      integer*4 Nwall2O
      parameter ( Nwall2O=0 )
      real*8  Wl2OTt(6,1),Wl2ORAw(1),Wl2OSqEnv(1),Wl2OQqBout(1),
     &        Wl2ODout(1)

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 14352 

****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 14358 

****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'

      integer NTen
      parameter(NTen=0)
      integer*4 TenIbn(1),TenNode(1)
      real*8 Tenzt(1),TenQqIn(1),TenAlG23(1),TenG23(1),TenG34(1)
     *,TenAlG43(1),TenG43(1),TenCpWl(1),TenTtWlk(1),TenTtWl(1)
     *,TenAlSqPhWl(1,nf),TenMasCpWl(1),TenCfAlGaWl(1),TenCfAlWpUpWl(1)
     *,TenCfAlWtWl(1),TenCfAlWpDoWl(1),TenEvUpWl(1),TenEvDoWl(1)
     *,TendG23dp(1),TendG34dp(1),TendG43dp(1)
     *,TenCfGgWpUpWt(1),TenCcGgWtWpDo(1),TenCfGgWpDoWt(1)
     *,TenG32(1),TendG32dp(1),TenCfGgWt(1),TenSqWl(1),TenTettaQ(1,nf)
     *,TenQqIsp(1),TenAlIsp(1),TenDeltaTs(1)
     *,TendQIspdP(1),TenTtTnIsp(1),TenAlSqIsp(1),TenResWl(1),
     >TenTcwAlCulc(1),TenTcwQq(1)
     *,TenLawL(1),TenQqInt(1),TenQqExt(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 14381 
C     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 14395 
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'

      integer n03
      parameter(n03=0)
      integer*4 Tn3RelP(2,1),Tn3RelE(4,1)
      real*8 Tn3RoWt(1),Tn3Cp(1,1),Tn3Lev(1),
     *       Tn3Ek(1,1),Tn3Rok(1,Tn3Ne),Tn3PpWt(1),Tn3Pk(1,1),Tn3Rot(1,
     >Tn3Ne),
     *       Tn3Xxk(1,Tn3Ne),Tn3MasBor(1),Tn3ccBor(1),Tn3VolWt(1),
     *       Tn3dp(1,Tn3Ne),Tn3MasWt(1),Tn3GIspSum(1),Tn3Pp(1),
     *       Tn3GCondSum(1),Tn3G32sum(1),Tn3G34sum(1),Tn3G23sum(1),
     *       Tn3G43sum(1),Tn3TettaQ(1,Tn3Ne),Tn3Ev_t(1,1),Tn3Dim_t(1),
     *       Tn3Dim_tmax/1/,Tn3EvDo(1),Tn3EvDo0(1),Tn3FlwDdMin(1),
     >Tn3MasWpUp(1),
     *       Tn3MasWpDo(1),Tn3MasGa(1),Tn3LevMas(1),Tn3LevMask(1),
     >Tn3PpGa(1),
     *       Tn3PpWpUp(1),Tn3EeGa(1),Tn3EeWpUp(1),Tn3EeWt(1),
     *       Tn3EeWpDo(1),Tn3LevRelax(1),Tn3VsGa(1),Tn3VsVpUp(1),
     *       Tn3VsWt(1),Tn3VsVpDo(1),Tn3EeWtSatWt(1),Tn3EeWpSatWt(1),
     *       Tn3RoGa(1),Tn3ROWpUp(1),Tn3RoWpDo(1),Tn3LevRel(1),Tn3Hi(1),
     *       Tn3Volk(1,Tn3Ne),Tn3Mask(1,1),Tn3CcGas(1,1),
     *       Tn3CfMasGasMin(1)/1.0d-7/,
     *       Tn3CcUp(1,1),Tn3FlwHUp(1,1),Tn3FlwHDo(1,1),Tn3Ee(1,1),
     *       Tn3TtGa(1),Tn3TtWpUp(1),Tn3TtWt(1),Tn3TtWpDo(1),
     >Tn3MasGaRealk(1),
     *       Tn3GCorr(1,1),Tn3drdp(1,1),Tn3drdh(1,1),Tn3vk(1,1),
     *       Tn3Waste_T(20,1),Tn3Dmint(1),Tn3RoDo(1),Tn3dEInt(1,1),
     *       Tn3WlUpQqOut(1),Tn3WlDoQqOut(1),Tn3TcwAlCulc(1,1),
     *       Tn3TtWlUp(1),Tn3TtWlDo(1),Tn3LaWl(1),
     *       Tn3QqWlEnvUp(1),Tn3QqWlEnvDo(1),Tn3RoomUpInd(1),
     >Tn3RoomDoInd(1),Tn3Tt(1,1),
     *       Tn3CcWpDo(1),Tn3CfGgWpDo_tmp(1),Tn3pmax(1),Tn3TtUp(1),
     >Tn3AlSqPhWl(4,1),Tn3AlWlUpDo(1),
     *       Tn3Levadd(1),Tn3cfdebGg(1),
     *       Tn3PressCorr(1),TN3VOL(1),TN3VOLGA(1),TN3VOLCORRUP(1),
     >TN3VOLCORRDO(1),Tn3FlwIntCoef(1),
     *       Tn3VolVpUpLim(1),Tn3dmintsum(1),Tn3CfGCorr(1),
     >Tn3LevTunG(1),Tn3LevTunPpRegG(6,1)
      integer*4 Tn3FlwNum(1),Tn3CcSetInd(1)
      logical*1 Tn3CcBorLb(1),Tn3VpInFlag(1),Tn3BorFl(1),
     >Tn3EntOut_Key(1),Tn3FlagGCorr(1),
     * Tn3FlagGCorrDn(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 14433 

C     common/kts_hC     common/kts_hC     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 14575 


****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'

      integer*4 n07,Tn7RelP(1,1),Tn7RelE(2,1),Tn7Homo(1),Tn7CcSetInd(1)
      real*8    Tn7RoGa(1),Tn7RoWt(1),Tn7Rok(1,2),Tn7XxWt(1),Tn7Xk(1,2),
     *          Tn7Lev(1),Tn7EeWt(1),Tn7VsGa(1),Tn7VsWt(1),Tn7PpGa(1),
     *          Tn7EeGa(1),Tn7EeWtSat(1),Tn7EeVpSat(1),Tn7LevRel(1),
     *          Tn7Hi(1),Tn7PpWt(1),Tn7TtWt(1),Tn7Ee(2,1),
     *          Tn7Ek(2,1),Tn7CcVolk(1),Tn7CcMask(1)
     *         ,Tn7XxBalWt(1),Tn7Levk(1),Tn7Lev_Rlx(1),Tn7Acon(1),
     *          Tn7Sq(1),Tn7CcBor(1),Tn7MasWt(1),Tn7MasGa(1),
     *          Tn7MasBor(1),Tn7GCorrWt(1),
     *          Tn7dRdPk(2,1),Tn7dRdP(2,1),Tn7dRdHk(2,1),Tn7dRdH(2,1),
     *          dP_Tn7(1),Tn7Dcon(1),Tn7LsMin(1),Tn7dMLoc(2,1),
     *          Tn7CfRoGaCorr(1),Tn7CfRoWtCorr(1),
     *          Tn7Waste_T(20,1),Tn7Rowttrue(1),Tn7RoWtSat(1),
     *          Tn7RoVpSat(1),Tn7Dmint(1),Tn7Gcorr(1),
     *          Tn7WlUpQqOut(1),Tn7WlDoQqOut(1),Tn7TcwAlCulc(1,1),
     *          Tn7TtWlUp(1),Tn7TtWlDo(1),Tn7LaWl(1),Tn7QqWlEnv(1),
     >Tn7RoomInd(1)
     *          ,Tn7EvDo(1),Tn7EvDo0(1),Tn7TtGa(1),Tn7AlSqPhWl(2,1),
     >Tn7AlWlUpDo(1)
     *          ,Tn7dMIntGa(1),Tn7dMIntWt(1),Tn7GCorrGa(1),
     >Tn7LevTunG(1),Tn7LevTunPpRegG(1)
      logical*1 Tn7_Flag_Mas_Deb(1),Tn7CfCorr_On(1)


      logical*1 Tn7CcBorLb(1)

      parameter(n07=0)
      real*8 Tn7Pk(2,1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 14609 

C     common/kts_hC     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 14666 

****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
      integer n08
      parameter(n08=0)
      integer*4 Tn8RelP(1,0),Tn8RelE(2,0),Tn8CcSetInd(1)
      real*8 Tn8RoWt(1),Tn8RoGa(1),Tn8GSumWt1(1),Tn8GSumWt2(1),
     *       Tn8Lev(1),Tn8RoWtk(1),Tn8PpWt(1),Tn8MasBor(1),Tn8ccBor(1),
     *       Tn8VolWt(1),Tn8XxWt(1),Tn8XxBalWt(1),
     *       Tn8MasWt(1),Tn8MasWtk(1),
     *       Tn8RoGak(1),Tn8Gsliv(1),Tn8EeGa(1),
     *       Tn8EeWt(1),Tn8PpGa(1),Tn8VsWt(1),Tn8VsGa(1),Tn8VsVp(1),
     *       Tn8EeWtSat(1),Tn8EeVpSat(1),Tn8LevRel(1),Tn8Hi(1),
     *       Tn8TtGa(1),Tn8TtWt(1),Tn8GgXx(1),
     *       Tn8dRdP1(1),Tn8dRdH1(1),Tn8dRdP2(1),Tn8dRdH2(1),
     *       Tn8Waste_T(20,1),Tn8RowtSat(1),Tn8RovpSat(1),
     *       Tn8QqEnv(1),Tn8RoomInd(1),Tn8TtMeDo(1)
     *       ,Tn8EvDo(1),Tn8EvDo0(1),Tn8GInOut(1),Tn8LevTunG(1)

      logical*1 Tn8CcBorLb(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 14690 

C     common/kts_hC     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 14727 

****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'

      integer n09
      parameter(n09=0)
      integer*4 Tn9RelP(2,1),Tn9RelE(Tn9Ne,1),Tn9CcSetInd(1)
      real*8 Tn9RoWt(1),Tn9Cp(1,1),Tn9Lev(1),
     *       Tn9Ek(1,1),Tn9Rok(1,Tn9Ne),Tn9PpWt(1),Tn9Pk(1,1),Tn9Rot(1,
     >Tn9Ne),
     *       Tn9Xxk(1,Tn9Ne),Tn9MasBor(1),Tn9ccBor(1),Tn9VolWt(1),
     *       Tn9dp(1,Tn9Ne),Tn9MasWt(1),Tn9GIspSum(1),Tn9Pp(1),
     *       Tn9GCondSum(1),Tn9G32sum(1),Tn9G34sum(1),Tn9G23sum(1),
     *       Tn9G43sum(1),Tn9TettaQ(1,Tn9Ne),Tn9Ev_t(1,1),Tn9Dim_t(1),
     *       Tn9Dim_tmax/1/,Tn9EvDo(1),Tn9EvDo0(1),Tn9FlwDdMin(1),
     >Tn9MasVpUp(1),
     *       Tn9MasVpDo(1),Tn9MasGa(1),Tn9LevMas(1),Tn9LevMask(1),
     >Tn9PpGa(1),
     *       Tn9PpVpUp(1),Tn9EeGa(1),Tn9EeVpUp(1),Tn9EeWt(1),
     *       Tn9EeVpDo(1),Tn9LevRelax(1),Tn9VsGa(1),Tn9VsVpUp(1),
     *       Tn9VsWt(1),Tn9VsVpDo(1),Tn9EeWtSatWt(1),Tn9EeVpSatWt(1),
     *       Tn9RoGa(1),Tn9ROVpUp(1),Tn9RoVpDo(1),Tn9LevRel(1),Tn9Hi(1),
     *       Tn9Volk(1,Tn9Ne),  ! ������ ��� �� ��������
     *       Tn9VsWtUp(1),Tn9VsWtJa(1),Tn9Ee(1,1),Tn9TtGa(1),
     *       Tn9TtVpUp(1),Tn9TtWt(1),Tn9TtVpDo(1),
     *       Tn9Mask(1,1),Tn9CcGas(1,1),
     *       Tn9CfMasGasMin(1)/7.5d-7/,
     *       Tn9CcUp(1,1),Tn9FlwHUp(1,1),Tn9FlwHDo(1,1),Tn9GSepSum(1),
     *       Tn9GFlwWtSum(1),Tn9MasGaRealk(1),Tn9dEInt(1,1),
     *       Tn9GCorr(1,1),Tn9drdp(1,1),Tn9drdh(1,1),Tn9vk(1,1),
     *       Tn9Waste_T(20,1),Tn9Dmint(1),Tn9MasWtUp(1),Tn9MasWtJa(1),
     >Tn9RoDo(1),
     *       Tn9WlUpQqOut(1),Tn9WlDoQqOut(1),Tn9TcwAlCulc(1,1),
     *       Tn9TtWlUp(1),Tn9TtWlDo(1),Tn9LaWl(1),
     *       Tn9QqWlEnvUp(1),Tn9QqWlEnvDo(1),Tn9RoomUpInd(1),
     >Tn9RoomDoInd(1),
     *       Tn9CcVpDo(1),Tn9pmax(1),Tn9TtUp(1),Tn9AlSqPhWl(6,1),
     >Tn9AlWlUpDo(1),
     *       Tn9TtSatWt_(1),Tn9Levadd(1),Tn9vol(1),Tn9VolCorrUp(1),
     >Tn9VolCorrDo(1),
     *       Tn9PressCorr(1),Tn9VolGa(1),Tn9FlwIntCoef(1),
     >Tn9VolVpUpLim(1),
     *       Tn9LevTunG(1),Tn9LevTunPpRegG(6,1)

      integer*4 Tn9FlwNum(1)
      logical*1 Tn9CcBorLb(1),Tn9VpInFlag(1),Tn9BorFl(1),
     >Tn9EntOut_Key(1),
     *          Tn9FlagGCorr(1),Tn9FlagGCorrDn(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 14770 

C     common/kts_hC     common/kts_hC     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 14914 


****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
      integer*4 Ntur1,Tr1Lin(1),tr2RLinTyp(1)
      parameter(ntur1=0)
      real*8    tr2FlwKsi1(1)
      real*8 Tr1Oin(1),Tr1Qsd(1),Tr1NdP(1),Tr1genTyp(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 14926 
      integer*4 Ntur3
      parameter(Ntur3=0)
         integer*4 Tr3beaTyp(1),Tr3beaInd(1)
      real*8    Tr3Omdl(1),Tr3Qsd(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 14932 
      integer*4 Ntur4
      parameter(Ntur4=0)
         integer*4 Tr4beaTyp(1),Tr4beaInd(1)
      real*8    Tr4Omdl(1),Tr4Qsd(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 14938 



****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'

      real*8 C1serv(nwall) !����. ��� ������� ������. ������� ���
      real*8 C2serv(nwall) !����������� ���� � ���. ��. ����� ������
      real*8 wl1_QqOut(nwall)  !��������� �������� ����� � ������� ��������
      real*8 wl1AlCulcWl(nwall)   !��������� �������� ����� � ������� ��������
      real*8 wl1_Tt_t(nwall)
      real*8 wl1_XQ(nwall),wl1_BQ(nwall)  ! �-�� � ������� ���������
      common/kts_hWALL1/ C1serv,C2serv,wl1_QqOut,wl1AlCulcWl,
     *  wl1_Tt_t,wl1_XQ,wl1_BQ
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 14958 

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 14972 

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 14978 

****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'

C     common/kts_hC file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 14998 

      integer*4 Nwall2
      parameter ( Nwall2=0 )
      real*8 wl2_QqOut(1)  !��������� �������� ����� � ������� ��������
      real*8 Wl2Tt_t(1)
      real*8 WL2mass(1),WL2Cp(1),WL2Tt(1)
      real*8 Wl2LamdC(1)
      integer*4 Wl2IndRoom(1),Wl2Ndet(1),Wl2Node(1)
      real*8 Wl2QqEnv(1),Wl2AlCulcWl2(1)
      real*8 Wl2Leff(1)
      real*8 Wl2Rb(1)
      real*8 Wl2Raw(1), Wl2SqEnv(1), Wl2QqBout(1), Wl2DOut(1)
      real*8 Wl2_XQ(1),Wl2_BQ(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 15013 

****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
      integer*4 Nwlcon2
      parameter ( Nwlcon2=0 )
      integer*4 WLC2Iwl(1)
      real*8 WLC2_Qq(1)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 15022 




****** End   section 'DESCRIPTION' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
      deltat=deltat1
ccc      call kts_hconnin

      InpMass (1) = 0.d0
      InpMass0(1) = 0.d0

      if(AlfaItFlag(1).eq.0) Then
****** Begin section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'

c   ���������� ����������� ������ ��� ������� ���������� �����
      time_tmp = time_df1()
        call WL1_Temp(nu,nwall,Wl1Node,C1serv,C2serv
     &               ,Wl1AlCulcNode,Wl1AlCulcEnv
     &               ,Wl1QqCl,Wl1QqEnv,NodQqWl
     &               ,NodTtk,Wl1Tt,Wl1TtEnv
     &               ,Wl1CfCulcEnv,Wl1CfCulcNode,Wl1CfAllEnv,
     &                Wl1IndRoom,NRm1,Rm1Qq,Wl1QqNodeSum)
      tick(380,1) = time_df1()-time_tmp
      ErrorNum(1)=380
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 15112 
c   ���������� ������������� ������������� ���������� �� �������
      time_tmp = time_df1()
C        call WL1_alfaSq(Nv,Nu,Nwall,WL1Node
C     &                 ,Wl1CulcTyp,Wl1AlCulcNode,Wl1AlCulcEnv,Wl1QqCl
C     &            ,NodTt,NodPp,NodVsWt,NodVsVp,NodTtSat,NodXxBal,NodGgSumIn
C     &            ,WL1Thick,WL1DdEqv,WL1Sq,WL1SqEnv,Wl1SqCros
C     &            ,WL1AlfaNode,WL1AlfaEnv,WL1Tt,WL1Lamd,Wl1LamdC
C     &            ,NodBodyTyp,TypeBut )
       CALL WL1_alfaSq(Nv,Nu,Nwall,WL1Node
     &            ,Wl1CulcTyp,Wl1AlCulcNode,Wl1AlCulcEnv,Wl1QqCl
     &            ,NodTtk,NodPp,NodVsWt,NodVsVp,NodTtSat,NodXxBal,
     >NodGgSumIn
     &            ,WL1Thick,WL1DdEqv,WL1Sq,WL1SqEnv,Wl1SqCros
     &            ,WL1AlfaNode,WL1AlfaEnv,WL1Tt,WL1Lamd,Wl1LamdC
     &            ,NodBodyTyp,TypeBut
     - ,NODEEWV,NodEeWtSat,NodEeVPSat,NODRO,NodRoVpSat
     - ,NODROWTSAT,NodRoGa,NodcpGa,NodCgGa,VOLUKZ,NODSIGMASURF
     -,NODFLOWREGIME,ALFAFLAG(1),NodLaFr,ATYPPCRIT(1),Wl1alprev,NodVsGa
     &            ,Wl1CfCulcNode,Wl1CfCulcEnv,Wl1CfAllEnv,NodAlphaMin
     &            ,Wl1CfAllAlf
     & ,Wl1AlfaMinNode,Wl1DecCond,Wl1Decboil
     *          ,NodGgSumZeroIn,Wl1Robn,NodWaste_T,WasteNoGl)

      tick(381,1) = time_df1()-time_tmp
      ErrorNum(1)=381
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 15138 
****** End   section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
      end if


C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 15164 

****** Begin section 'Flw_BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'Flw_BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'Flw_BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'Flw_BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'Flw_BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'

      time_tmp = time_df1()
      call Flw_BackEx(nv,GZ,Gk,ROVZ,Rovk,FlwVsk,FlwVs,FlwRo,FlwRoFrc,
     >Gzz)
      tick(154,1) = time_df1()-time_tmp
      ErrorNum(1)=154
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 15177 


****** End   section 'Flw_BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'Flw_BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'Flw_BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'Flw_BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'Flw_BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'Flw_BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'Flw_BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'Flw_BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'Flw_BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'Flw_BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'Flw_BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'Flw_BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'Flw_BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'Flw_BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'Flw_BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'Flw_BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'Flw_BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'Flw_BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'Flw_BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'Flw_BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'Flw_BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'Flw_BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'Flw_BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'Flw_BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'Flw_BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'Flw_BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'Flw_BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'Flw_BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'Flw_BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'Flw_BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'Flw_BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'Flw_BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'Flw_BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'Flw_BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'Flw_BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'Flw_BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'Flw_BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'Flw_BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** End   section 'Flw_BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'Flw_BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'Flw_BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'Flw_BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'Flw_BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'Flw_BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'Flw_BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'Flw_BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'Flw_BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'Flw_BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'Flw_BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'Flw_BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** End   section 'Flw_BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'Flw_BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'Flw_BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'Flw_BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'Flw_BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'Flw_BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'Flw_BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'Flw_BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'Flw_BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'Flw_BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'Flw_BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'Flw_BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'Flw_BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'Flw_BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'Flw_BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'Flw_BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'Flw_BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'Flw_BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'Flw_BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

****** Begin section 'FlwdPPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'FlwdPPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'FlwdPPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'FlwdPPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'FlwdPPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'FlwdPPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'FlwdPPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'FlwdPPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'FlwdPPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'FlwdPPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'FlwdPPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'FlwdPPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'FlwdPPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'FlwdPPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'FlwdPPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'FlwdPPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'FlwdPPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'FlwdPPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'FlwdPPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'FlwdPPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'FlwdPPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'FlwdPPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'FlwdPPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'FlwdPPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'FlwdPPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'FlwdPPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'FlwdPPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'FlwdPPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'FlwdPPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'FlwdPPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'FlwdPPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'FlwdPPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'FlwdPPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'FlwdPPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'FlwdPPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'FlwdPPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'FlwdPPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'FlwdPPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'FlwdPPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'FlwdPPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'FlwdPPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'FlwdPPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'FlwdPPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** End   section 'FlwdPPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'FlwdPPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'FlwdPPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'FlwdPPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'FlwdPPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'FlwdPPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'FlwdPPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'FlwdPPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'FlwdPPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'FlwdPPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'FlwdPPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'FlwdPPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** End   section 'FlwdPPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'FlwdPPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'FlwdPPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'FlwdPPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'FlwdPPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'FlwdPPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'FlwdPPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'FlwdPPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'FlwdPPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'FlwdPPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'FlwdPPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'FlwdPPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'FlwdPPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'FlwdPPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'FlwdPPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'FlwdPPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'FlwdPPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'FlwdPPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'FlwdPPump' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

****** Begin section 'BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'


      time_tmp = time_df1()
      call Node_BackEx(nu,Npres,Nent,NodRelP,NodRelE,NodPp,
     *                 Pk,NodEe,Ek,NodRo,Rok,NodEeWtSat,EeWtSatk,
     *                 NodEeVpSat,EeVpSatk,NodTyp,NodGgSum,Pzt,
     *                 Ezt,NodQqSum,NodTt,NodTtk,NodCp,NodCp,
     *                 NoddRodP,NoddRodPk,NoddRodE,NoddRodEk,
     *                 EnrdtIntegr,Qdt,nodEeGa,
     *                 nodccga,nodvvga,
     *                 NodEeGat,NodCcGat,NodVvGat,
     *                 VOLUKZ0,NodVolk,NodGgSum_ga,NodGgSum_Wv,
     *                 NodXx,NodXxk1,NodXxBal,NodXxBalk1,NodNumEe(1),
     *                 NodTtGa,NodTtGat,NodTtWv,NodTtWvt)

      tick(168,1) = time_df1()-time_tmp
      ErrorNum(1)=168


C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 15364 

****** End   section 'BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 15382 
****** End   section 'BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 15395 
****** End   section 'BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 15416 
****** End   section 'BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 15499 

****** End   section 'BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 15557 

****** End   section 'BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 15584 

****** End   section 'BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 15673 


****** End   section 'BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'

c   ���������� 

        call TransTH_BackEx(NTRTH,TransTHiWl,TransTHdeep,TransTH_Tt
     &                     ,nwall,Wl1Thick,Wl1LamdC,Wl1Sq,Wl1SqEnv
     &                     ,Wl1QqCl,Wl1QqEnv,Wl1Tt
     &                     ,Wl1CfCulcEnv,Wl1CfAllEnv,Wl1CfCulcNode
     *                     ,nu,NodTt,Wl1Node,TransTHsource,TransTHtau,
     >dtint
     &                         ,TransTHTypTo
     &                         ,nwall2,Wl2Ndet,Wl2Rb,Wl2Tt,Wl2Node,
     &                          NMtl,MtlTt,NMtlO,MtlOTt
     &                         ,NodGgSumIn,TransTHKeyG,TransTHtau1
     &                         ,TransTHtau2,TransTHGg1,TransTHGg2
     &                         ,TransThTauCalc,TransTHofset
     *                   ,TenResWl,TenTtWl,TenSqWl,TenQqInt,TenQqExt,
     >NTen)

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 15697 

****** End   section 'BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 15719 

****** End   section 'BackEx' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

      time_tmp = time_df1()
      call Flw_Ee_v1(nv,n03,Nf,IFROKZ,ITONKZ,LinIBNfro,
     *               LinIBNto,NodEe,Tn8EeWt,Tn3Ek,
     *               FlwEeFrom,FlwEeTo,
     *               Tn7EeWt,n09,Tn9Ee,
     *               Tn7EeGa,ComGamFrom,ComGamTo,nu,n07,n08,
     *               NodEeGa,NodEeWv,NodEeVpSat,NodEeWtSat,
     *               FlwXxFrom,FlwXxTo)
      tick(142,1) = time_df1()-time_tmp
      ErrorNum(1)=142
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 15734 

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 15739 

c --- �������� ��������� ���������� ��� �������
      time_tmp = time_df1()
        call kts_hMQNEW(dtInt)
      tick(144,1) = time_df1()-time_tmp
      ErrorNum(1)=144

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 15757 
      do i = 1,nv
       FlwRes(i) = FlwRes(i)+FlwRes_Iter(i)
      enddo
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 15762 

c --- ������������� �������� ����
****** Begin section 'BOR' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'

      if(Bor(1).ne.0) then
c   ��������� ��������� ������� Dmp (����� ������� ���������)
      time_tmp = time_df1()
      call CLR_S(Dmp, Npres, ILNZp, INZSUBp, NZSUBp)
      tick(149,1) = time_df1()-time_tmp
      ErrorNum(1)=149
      time_tmp = time_df1()
      call Bor_node_II(atau,AccelBor(1),Npres,nv,
     *     nu,n03,n07,n08,n09,
     *     IFROKZ,ITONKZ,LinIBNfro,LinIBNto,Gz,
     *     dmp,yp,NodCcBor,
     *     Tn3CcBor,Tn7CcBor,Tn8CcBor,
     *     Tn9CcBor,
     *     volukz,
     *     Tn3MasWt,Tn3MasWpDo,Tn3MasWpUp,Tn3MasGa,
     *     Tn3GIspSum,Tn3GCondSum,
     *     Tn7MasWt,Tn7MasGa,
     *     Tn8MasWt,
     *     Tn9MasWt,Tn9MasVpDo,Tn9MasVpUp,Tn9MasGa,
     *     Tn9GIspSum,Tn9GCondSum,
     *     NodRo,
     *     FlwXxFrom,FlwXxTo,ComGamFrom,ComGamTo,
     *     Nf,Bor,
     *     invperp,
     *     NodRelP,Tn3RelP,Tn7RelP,Tn8RelP,Tn9RelP,
     *     nfr1,Fr1Flw,Fr1Bor,
     *     Tn3BorFl,Tn9BorFl,
     *   ntFrB,tFrBNdInd,tFrBCcOld,tFrBGIn,tFrBAa,tFrBTau)
c      call Bor_comp(AccelBor(1),Npres,nu,n03,n07,n08,
c     *              n09,dmp,invperp,Bor,
c     *              NodRelP,NodXx,NodGgCompWvp,MasdtIntegr_Wv,
c     *              Tn3RelP,Tn3GCorr,
c     *              Tn7RelP,Tn7XxWt,Tn7GCorrWt,
c     *              Tn8RelP,Tn8XxWt,Tn8GgXx,Tn8GSliv,
c     *              Tn9RelP,Tn9GCorr)
      tick(150,1) = time_df1()-time_tmp
      ErrorNum(1)=150
      time_tmp = time_df1()

      call LINE_S(Npres,Dmp,Yp,dP,ILNZp,INZSUBp,NZSUBp)
      tick(151,1) = time_df1()-time_tmp
      ErrorNum(1)=151
      time_tmp = time_df1()

      call BorInNod_II(Npres,
     *nu,n03,n07,n08,n09,
     *NodTyp,dP,
     *NodCcBor,Tn3CcBor,
     *Tn7CcBor,Tn8CcBor,Tn9CcBor,
     *NodMasBor,Tn3MasBor,
     *Tn7MasBor,Tn8MasBor,Tn9MasBor,
     *NodRelP,Tn3RelP,
     *Tn7RelP,Tn8RelP,Tn9RelP,
     *invperp,ExtCcBor(1),ExtCcBorLb(1),NodCcBorLb,
     *Tn3CcBorLb,
     *Tn7CcBorLb,Tn8CcBorLb,Tn9CcBorLb)
      tick(152,1) = time_df1()-time_tmp
      ErrorNum(1)=152
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 15830 
      endif
      call BorSetNod(Npres,
     *nu,n03,n07,n08,n09,
     *NodCcBor,Tn3CcBor,
     *Tn7CcBor,Tn8CcBor,Tn9CcBor,
     *NodMasBor,Tn3MasBor,
     *Tn7MasBor,Tn8MasBor,Tn9MasBor,
     *ExtCcBor(1),ExtCcBorLb(1),NodCcBorLb,
     *Tn3CcBorLb,
     *Tn7CcBorLb,Tn8CcBorLb,Tn9CcBorLb)

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 15845 
****** End   section 'BOR' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'BOR' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'BOR' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'BOR' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'BOR' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'BOR' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'BOR' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'BOR' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'BOR' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'BOR' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'BOR' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'BOR' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'BOR' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'BOR' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'BOR' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'BOR' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'BOR' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'BOR' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'BOR' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'BOR' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'BOR' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'BOR' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'BOR' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'BOR' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'BOR' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'BOR' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'BOR' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'BOR' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'BOR' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'BOR' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'BOR' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'BOR' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'BOR' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'BOR' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'BOR' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'BOR' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'BOR' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'BOR' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'BOR' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'BOR' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'BOR' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'BOR' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** End   section 'BOR' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'BOR' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'BOR' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'BOR' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'BOR' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'BOR' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'BOR' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'BOR' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'BOR' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'BOR' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'BOR' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'BOR' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** End   section 'BOR' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'BOR' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'BOR' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'BOR' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'BOR' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'BOR' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'BOR' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'BOR' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'BOR' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'BOR' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'BOR' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'BOR' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'BOR' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'BOR' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'BOR' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'BOR' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'BOR' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'BOR' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'BOR' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

****** Begin section 'WASTEINOUT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'WASTEINOUT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'WASTEINOUT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'WASTEINOUT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'WASTEINOUT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'WASTEINOUT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'WASTEINOUT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'WASTEINOUT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'WASTEINOUT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'WASTEINOUT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'WASTEINOUT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'WASTEINOUT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'WASTEINOUT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'WASTEINOUT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'WASTEINOUT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'WASTEINOUT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'WASTEINOUT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'WASTEINOUT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'WASTEINOUT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'WASTEINOUT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'WASTEINOUT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'WASTEINOUT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'WASTEINOUT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'WASTEINOUT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'WASTEINOUT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'WASTEINOUT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'WASTEINOUT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'WASTEINOUT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'WASTEINOUT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'WASTEINOUT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'WASTEINOUT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'WASTEINOUT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'WASTEINOUT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'WASTEINOUT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'WASTEINOUT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'WASTEINOUT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'WASTEINOUT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'WASTEINOUT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'WASTEINOUT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'WASTEINOUT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'WASTEINOUT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'WASTEINOUT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'WASTEINOUT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** End   section 'WASTEINOUT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'WASTEINOUT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'WASTEINOUT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'WASTEINOUT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'WASTEINOUT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'WASTEINOUT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'WASTEINOUT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'WASTEINOUT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'WASTEINOUT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'WASTEINOUT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'WASTEINOUT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'WASTEINOUT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** End   section 'WASTEINOUT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'WASTEINOUT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'WASTEINOUT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'WASTEINOUT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'WASTEINOUT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'WASTEINOUT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'WASTEINOUT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'WASTEINOUT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'


      call TransWst(NTRWST,nu,TransWstNod,TransWstType,
     *      TransWst01,TransWst02,TransWst03,TransWst04,
     *      TransWst05,TransWst06,TransWst07,TransWst08,
     *      TransWst09,TransWst10,
     *      TransWst11,TransWst12,TransWst13,TransWst14,
     *      TransWst15,TransWst16,TransWst17,TransWst18,
     *      TransWst19,TransWst20,
     *      TransWst21,TransWst22,TransWst23,TransWst24,
     *      TransWst25,TransWst26,TransWst27,TransWst28,
     *      TransWst29,TransWst30,
     *      TransWst31,TransWst32,TransWst33,TransWst34,
     *      TransWst35,TransWst36,TransWst37,TransWst38,
     *      TransWst39,TransWst40,
     *      TransWst41,TransWst42,TransWst43,TransWst44,
     *      TransWst45,TransWst46,TransWst47,TransWst48,
     *      TransWst49,TransWst50,
     *      TransWst51,TransWst52,TransWst53,TransWst54,
     *      TransWst55,TransWst56,TransWst57,TransWst58,
     *      TransWst59,TransWst60,
     *      TransWst61,TransWst62,TransWst63,TransWst64,
     *      TransWst65,TransWst66,TransWst67,TransWst68,
     *      TransWst69,TransWst70,
     *      TransWst71,TransWst72,TransWst73,TransWst74,
     *      TransWst75,TransWst76,TransWst77,TransWst78,
     *      TransWst79,TransWst80,
     *      TransWst81,TransWst82,TransWst83,TransWst84,
     *      TransWst85,TransWst86,TransWst87,TransWst88,
     *      TransWst89,TransWst90,
     *      TransWst91,TransWst92,TransWst93,TransWst94,
     *      TransWst95,TransWst96,TransWst97,TransWst98,
     *      TransWst99,TransWst100,
     *      NodWaste_T,TransSumWst,NodRo,NodCcGa,TransSumWstV,
     *      WasteTyp_T,TransWstFlRo,TransWstRo,
     *            n08,Tn8Waste_T,Tn8RoWt,Tn8RoGa,Tn8LevRel,
     *            n07,Tn7Waste_T,Tn7RoWt,Tn7RoGa,Tn7LevRel,
     *            n03,Tn3Waste_T,Tn3RoDo,Tn3RoWpUp,Tn3RoGa,Tn3LevRel,
     *            n09,Tn9Waste_T,Tn9RoDo,Tn9RoVpUp,Tn9RoGa,Tn9LevRel,
     >TransWstL)
      ErrorNum(1)=1523
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 16024 
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 16059 
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 16095 
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 16131 
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 16167 
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 16203 
****** End   section 'WASTEINOUT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'WASTEINOUT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'WASTEINOUT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'WASTEINOUT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'WASTEINOUT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'WASTEINOUT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'WASTEINOUT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'WASTEINOUT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'WASTEINOUT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'WASTEINOUT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'WASTEINOUT' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

****** Begin section 'WASTE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'WASTE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'WASTE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'WASTE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'WASTE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'WASTE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'WASTE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'WASTE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'WASTE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'WASTE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'WASTE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'WASTE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'WASTE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'WASTE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'WASTE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'WASTE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'WASTE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'WASTE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'WASTE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'WASTE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'WASTE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'WASTE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'WASTE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'WASTE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'WASTE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'WASTE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'WASTE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'WASTE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'WASTE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'WASTE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'WASTE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'WASTE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'WASTE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'WASTE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'WASTE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'WASTE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'WASTE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'WASTE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'WASTE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'WASTE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'WASTE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'WASTE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'WASTE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** End   section 'WASTE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'WASTE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'WASTE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'WASTE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'WASTE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'WASTE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'WASTE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'WASTE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'WASTE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'WASTE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'WASTE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'WASTE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** End   section 'WASTE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'WASTE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'WASTE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'WASTE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'WASTE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'WASTE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'WASTE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'WASTE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'

       call wasteset(NumWaste,nu,nv,n03,n07,n08,n09,Nf,
     *             NodTyp,IFROKZ,ITONKZ,LinIBNfro,LinIBNto,GZ,
     *             RovZ,FlwLl,FlwSq,dtInt, 
     *             WasteTyp_T,WasteKkVp_T,WasteMax_T,
     *             ComGamFrom,ComGamTo,
     *             NodWaste_T,Tn3Waste_T,Tn7Waste_T,Tn8Waste_T,
     *             Tn9Waste_T,
     *             NodRo,Volukz,NodCcGa,
     *             Tn3MasWt,Tn3MasWpDo,Tn3MasWpUp,Tn3MasGa,
     *             Tn7MasWt,Tn7MasGa,Tn8MasWt,
     *             Tn9MasWt,Tn9MasVpDo,Tn9MasVpUp,Tn9MasGa,
     *             Waste_dt_1,Waste_accel,
     *             nInWst,InWstNum,InWstNode,InWstNdTp,InWstdt,
     *             WasteIO_T,
     *             Nwst,WSTNode,WasteNoGas,
     *             WSTConsN2,WSTConsO2,WSTConsH2O,WSTConsCO2,
     *             WSTConsSoot,WSTConsAsh,
     *       nfr1,Fr1flw,Fr1Waste_T,InWstTyp,InWstCc,Fr1WstMass_T,
     *             NFlwWasteSet,FlwWasteSetConnInd,FlwWasteSetNum,
     *             FlwWasteSetWaste,FlwConnFlwInd,Fr1KkFullWst,
     * NUCH,CoalHConNodInd,CoalHNodMoistG,InWstExtCon,InWstExtWstdt,
     *             WasteAa_T,WasteFlAa_T,Fr1SumMas_0,Fr1FlSumMas,
     *             Fr1F0SumMas,
     * NXS,XSFlagSetCc,XSFlSet,XSCcstr,NodCcSetInd,XSIndSetCc0,
     * Tn3CcSetInd,Tn7CcSetInd,Tn8CcSetInd,Tn9CcSetInd,XSIndSetCc,
     * Tn3GispSum,Tn3GcondSum,Tn9GispSum,Tn9GcondSum)
      ErrorNum(1)=1521
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 16308 

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 16320 


C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 16332 

****** End   section 'WASTE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'WASTE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'WASTE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'WASTE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'WASTE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'WASTE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'WASTE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'WASTE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'WASTE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'WASTE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'WASTE' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

****** Begin section 'TurPostStep' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'TurPostStep' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'TurPostStep' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'TurPostStep' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'TurPostStep' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'TurPostStep' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'TurPostStep' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'TurPostStep' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'TurPostStep' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'TurPostStep' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'TurPostStep' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'TurPostStep' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'TurPostStep' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'TurPostStep' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'TurPostStep' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'TurPostStep' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'TurPostStep' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'TurPostStep' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'TurPostStep' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'TurPostStep' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'TurPostStep' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'TurPostStep' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'TurPostStep' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'TurPostStep' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'TurPostStep' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'TurPostStep' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'TurPostStep' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'TurPostStep' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'TurPostStep' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'TurPostStep' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'TurPostStep' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'TurPostStep' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'TurPostStep' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'TurPostStep' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'TurPostStep' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'TurPostStep' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'TurPostStep' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'TurPostStep' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'TurPostStep' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'TurPostStep' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'TurPostStep' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'TurPostStep' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'TurPostStep' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** End   section 'TurPostStep' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'TurPostStep' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'TurPostStep' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'TurPostStep' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'TurPostStep' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'TurPostStep' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'TurPostStep' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'TurPostStep' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'TurPostStep' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'TurPostStep' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'TurPostStep' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'TurPostStep' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** End   section 'TurPostStep' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'TurPostStep' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'TurPostStep' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'TurPostStep' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'TurPostStep' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'TurPostStep' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
c ��������� � �������� ����������� �������
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 16412 

c ������ ���������� �������� ������� ����� ������� �� ���������
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 16424 

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 16431 
****** End   section 'TurPostStep' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'TurPostStep' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'TurPostStep' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'TurPostStep' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'TurPostStep' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'TurPostStep' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'TurPostStep' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'TurPostStep' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'TurPostStep' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'TurPostStep' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'TurPostStep' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'TurPostStep' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'TurPostStep' of file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

      if(dPMaxFlag(1)) 
     *      call ColorNode(nu,
     *           ClrType,ClrScale,ClrKey,ClrTypeold,
     *           ClrNum0,ClrNum1,NodClr,NodClr1,
     *           NodPp,NodEv,NodTt,ClrPmax,ClrPmin,ClrHmax,ClrHmin,
     *           ClrTmax,ClrTmin,ClrSmax,ClrSmin,NodCcGa,NodWaste_T)

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 16512 
c      tick(146,1) = time_df1()-time_tmp
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 16524 
c --- �������� �������� ���������� ��� ����� � ��������
      time_tmp = time_df1()
      call kts_hconnOut
      tick(148,1) = time_df1()-time_tmp
      ErrorNum(1)=148

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 16539 

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 16548 

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 16561 

      call TransTN_Back_Ex(ntrtn,nu,TransTNTemp,TransTNNodInd,
     &     NodTt,NodPp,NodEe,NodCcga,NodEeWv,NodRo,NodEeGa,NodCpWv,
     &     TransTNCorSl,TransTNFlag,TransTNTau,dtint,
     *     NodPropsKey,NodePropsKey)

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 16569 

      call Trans__dP(nu,NTRdP,TransdPNod1,TransdPNod2,
     *                     Trans_dP,TransOffSet,NodPp,
     *                     Trans_1P,Trans_2P)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 16575 

      call TransP_nod(nu,NTRPN,TransPNone,TransPNTyp,
     *           TransPNRoomFl,TransPN_Pp,TransPNTtEnv,TransPNPpEnv,
     *           NodPp,NodEv,TransPNEvRel,NodRoTot)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 16581 

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 16586 

      call Trans_WvTn3(NTRWvT3,n03,TransWvIndTn3,TransWvTn3Type,
     *           Tn3MasGaRealk,Tn3MasWpUp,TransWvT3,n09,Tn9MasGaRealk,
     >Tn9MasVpUp,nu,NodCcGa,
     *           Tn3LevRel,Tn9LevRel,TransWvH,NodXx,TransOfS)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 16592 

      call Room_Qsum(nRm1,Rm1Qq
     *,n03,Tn3QqWlEnvUp,Tn3QqWlEnvDo,Tn3RoomUpInd,Tn3RoomDoInd
     *,n09,Tn9QqWlEnvUp,Tn9QqWlEnvDo,Tn9RoomUpInd,Tn9RoomDoInd
     *,n07,Tn7QqWlEnv,Tn7RoomInd
     *,n08,Tn8QqEnv,Tn8RoomInd
     *,nWall,Wl1QqEnv,Wl1IndRoom
     *,nWall2,Wl2QqEnv,Wl2IndRoom
     *,nMtlO,MtlOQqEnv,MtlOIndRoom)
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 16603 

c-------------���㫥��� ���࣮�뤥�����-----vvvvvvv
      if(FlagNulQq(1)) call Nul_Qq(nu,NodQqAdd)   !
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 16608 
      if(FlagNulQq(1)) call Nul_Qq(nwall,Wl1Qq)   !
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 16611 
      if(FlagNulQq(1)) FlagNulQq(1)=.false.       !
c-------------���㫥��� ���࣮�뤥�����-----^^^^^^^

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 16627 
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 16643 

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 16706 

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 16768 

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 16822 

C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 16847 
c
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 16852 
c
C file D:\ptkatom\ENICAD\MODEL\SHK_PART\PROCESS\SHK\kts\work\kts_h.tms str 16857 
      ErrorNum(1)=0

      return
      END



****** End   file 'D:\ptkatom\ENICAD\ENICAD\TG_CAD\HYDRO\NET43.fs'

      subroutine kts_hConnIn
      implicit none
      include 'kts_h.fh'

      Call kts_h_ConIn
      return
      end

      subroutine kts_hConnOut
      Implicit none
      include 'kts_h.fh'

      x_20645=GZ(219) !kts_h$35Igor_G
      x_20646=ROVZ(219) !kts_h$35Igor_Ro
      x_21024=GZ(222) !kts_h$41Igor_G
      x_21025=ROVZ(222) !kts_h$41Igor_Ro
      x_21416=GZ(226) !kts_h$51Igor_G
      x_21417=ROVZ(226) !kts_h$51Igor_Ro
      x_21633=GZ(229) !kts_h$61Igor_G
      x_21634=ROVZ(229) !kts_h$61Igor_Ro
      x_21815=GZ(231) !kts_h$71Igor_G
      x_21816=ROVZ(231) !kts_h$71Igor_Ro
      x_21997=GZ(233) !kts_h$81Igor_G
      x_21998=ROVZ(233) !kts_h$81Igor_Ro
      x_22179=GZ(235) !kts_h$91Igor_G
      x_22180=ROVZ(235) !kts_h$91Igor_Ro
      x_22361=GZ(237) !kts_h$101Igor_G
      x_22362=ROVZ(237) !kts_h$101Igor_Ro
      x_22543=GZ(239) !kts_h$111Igor_G
      x_22544=ROVZ(239) !kts_h$111Igor_Ro
      x_22725=GZ(241) !kts_h$121Igor_G
      x_22726=ROVZ(241) !kts_h$121Igor_Ro
      x_22907=GZ(243) !kts_h$131Igor_G
      x_22908=ROVZ(243) !kts_h$131Igor_Ro
      x_23089=GZ(245) !kts_h$141Igor_G
      x_23090=ROVZ(245) !kts_h$141Igor_Ro
      x_23271=GZ(247) !kts_h$151Igor_G
      x_23272=ROVZ(247) !kts_h$151Igor_Ro
      x_23453=GZ(249) !kts_h$161Igor_G
      x_23454=ROVZ(249) !kts_h$161Igor_Ro
      x_23635=GZ(251) !kts_h$176Igor_G
      x_23636=ROVZ(251) !kts_h$176Igor_Ro
      x_23817=GZ(253) !kts_h$186Igor_G
      x_23818=ROVZ(253) !kts_h$186Igor_Ro
      x_23999=GZ(255) !kts_h$196Igor_G
      x_24000=ROVZ(255) !kts_h$196Igor_Ro
      x_24181=GZ(257) !kts_h$206Igor_G
      x_24182=ROVZ(257) !kts_h$206Igor_Ro
      x_24363=GZ(259) !kts_h$216Igor_G
      x_24364=ROVZ(259) !kts_h$216Igor_Ro
      x_24545=GZ(261) !kts_h$226Igor_G
      x_24546=ROVZ(261) !kts_h$226Igor_Ro
      x_24727=GZ(263) !kts_h$236Igor_G
      x_24728=ROVZ(263) !kts_h$236Igor_Ro
      x_24909=GZ(265) !kts_h$246Igor_G
      x_24910=ROVZ(265) !kts_h$246Igor_Ro
      x_25091=GZ(267) !20FDA61CU027_G
      x_25092=ROVZ(267) !20FDA61CU027_Ro
      x_25273=GZ(269) !20FDA61CU028_G
      x_25274=ROVZ(269) !20FDA61CU028_Ro
      x_25665=GZ(273) !kts_h$1415Ispol_G
      x_25666=ROVZ(273) !kts_h$1415Ispol_Ro
      x_26044=GZ(276) !kts_h$1428Ispol_G
      x_26045=ROVZ(276) !kts_h$1428Ispol_Ro
      x_26436=GZ(280) !kts_h$1442Ispol_G
      x_26437=ROVZ(280) !kts_h$1442Ispol_Ro
      x_26653=GZ(283) !kts_h$1453Ispol_G
      x_26654=ROVZ(283) !kts_h$1453Ispol_Ro
      x_26835=GZ(285) !kts_h$1463Ispol_G
      x_26836=ROVZ(285) !kts_h$1463Ispol_Ro
      x_27017=GZ(287) !kts_h$1473Ispol_G
      x_27018=ROVZ(287) !kts_h$1473Ispol_Ro
      x_27199=GZ(289) !kts_h$1483Ispol_G
      x_27200=ROVZ(289) !kts_h$1483Ispol_Ro
      x_27381=GZ(291) !kts_h$1493Ispol_G
      x_27382=ROVZ(291) !kts_h$1493Ispol_Ro
      x_27563=GZ(293) !kts_h$1503Ispol_G
      x_27564=ROVZ(293) !kts_h$1503Ispol_Ro
      x_27745=GZ(295) !kts_h$1513Ispol_G
      x_27746=ROVZ(295) !kts_h$1513Ispol_Ro
      x_27927=GZ(297) !kts_h$1523Ispol_G
      x_27928=ROVZ(297) !kts_h$1523Ispol_Ro
      x_28109=GZ(299) !kts_h$1533Ispol_G
      x_28110=ROVZ(299) !kts_h$1533Ispol_Ro
      x_28291=GZ(301) !kts_h$1543Ispol_G
      x_28292=ROVZ(301) !kts_h$1543Ispol_Ro
      x_28473=GZ(303) !kts_h$1553Ispol_G
      x_28474=ROVZ(303) !kts_h$1553Ispol_Ro
      x_28655=GZ(305) !kts_h$1563Ispol_G
      x_28656=ROVZ(305) !kts_h$1563Ispol_Ro
      x_28837=GZ(307) !kts_h$1573Ispol_G
      x_28838=ROVZ(307) !kts_h$1573Ispol_Ro
      x_29019=GZ(309) !kts_h$1583Ispol_G
      x_29020=ROVZ(309) !kts_h$1583Ispol_Ro
      x_29201=GZ(311) !kts_h$1593Ispol_G
      x_29202=ROVZ(311) !kts_h$1593Ispol_Ro
      x_29383=GZ(313) !kts_h$1603Ispol_G
      x_29384=ROVZ(313) !kts_h$1603Ispol_Ro
      x_29565=GZ(315) !kts_h$1613Ispol_G
      x_29566=ROVZ(315) !kts_h$1613Ispol_Ro
      x_29747=GZ(317) !kts_h$1623Ispol_G
      x_29748=ROVZ(317) !kts_h$1623Ispol_Ro
      x_29929=GZ(319) !kts_h$1633Ispol_G
      x_29930=ROVZ(319) !kts_h$1633Ispol_Ro
      x_30111=GZ(321) !20FDA62CU027_G
      x_30112=ROVZ(321) !20FDA62CU027_Ro
      x_30293=GZ(323) !20FDA62CU028_G
      x_30294=ROVZ(323) !20FDA62CU028_Ro
      x_30685=GZ(327) !kts_h$1663Ispol_G
      x_30686=ROVZ(327) !kts_h$1663Ispol_Ro
      x_31064=GZ(330) !kts_h$1676Ispol_G
      x_31065=ROVZ(330) !kts_h$1676Ispol_Ro
      x_31456=GZ(334) !kts_h$1690Ispol_G
      x_31457=ROVZ(334) !kts_h$1690Ispol_Ro
      x_31673=GZ(337) !kts_h$1701Ispol_G
      x_31674=ROVZ(337) !kts_h$1701Ispol_Ro
      x_31855=GZ(339) !kts_h$1711Ispol_G
      x_31856=ROVZ(339) !kts_h$1711Ispol_Ro
      x_32037=GZ(341) !kts_h$1721Ispol_G
      x_32038=ROVZ(341) !kts_h$1721Ispol_Ro
      x_32219=GZ(343) !kts_h$1731Ispol_G
      x_32220=ROVZ(343) !kts_h$1731Ispol_Ro
      x_32401=GZ(345) !kts_h$1741Ispol_G
      x_32402=ROVZ(345) !kts_h$1741Ispol_Ro
      x_32583=GZ(347) !kts_h$1751Ispol_G
      x_32584=ROVZ(347) !kts_h$1751Ispol_Ro
      x_32765=GZ(349) !kts_h$1761Ispol_G
      x_32766=ROVZ(349) !kts_h$1761Ispol_Ro
      x_32947=GZ(351) !kts_h$1771Ispol_G
      x_32948=ROVZ(351) !kts_h$1771Ispol_Ro
      x_33129=GZ(353) !kts_h$1781Ispol_G
      x_33130=ROVZ(353) !kts_h$1781Ispol_Ro
      x_33311=GZ(355) !kts_h$1791Ispol_G
      x_33312=ROVZ(355) !kts_h$1791Ispol_Ro
      x_33493=GZ(357) !kts_h$1801Ispol_G
      x_33494=ROVZ(357) !kts_h$1801Ispol_Ro
      x_33675=GZ(359) !kts_h$1811Ispol_G
      x_33676=ROVZ(359) !kts_h$1811Ispol_Ro
      x_33857=GZ(361) !kts_h$1821Ispol_G
      x_33858=ROVZ(361) !kts_h$1821Ispol_Ro
      x_34039=GZ(363) !kts_h$1831Ispol_G
      x_34040=ROVZ(363) !kts_h$1831Ispol_Ro
      x_34221=GZ(365) !kts_h$1841Ispol_G
      x_34222=ROVZ(365) !kts_h$1841Ispol_Ro
      x_34403=GZ(367) !kts_h$1851Ispol_G
      x_34404=ROVZ(367) !kts_h$1851Ispol_Ro
      x_34585=GZ(369) !kts_h$1861Ispol_G
      x_34586=ROVZ(369) !kts_h$1861Ispol_Ro
      x_34767=GZ(371) !kts_h$1871Ispol_G
      x_34768=ROVZ(371) !kts_h$1871Ispol_Ro
      x_34949=GZ(373) !kts_h$1881Ispol_G
      x_34950=ROVZ(373) !kts_h$1881Ispol_Ro
      x_35131=GZ(375) !20FDA63CU027_G
      x_35132=ROVZ(375) !20FDA63CU027_Ro
      x_35313=GZ(377) !20FDA63CU028_G
      x_35314=ROVZ(377) !20FDA63CU028_Ro
      x_35705=GZ(381) !kts_h$1911Ispol_G
      x_35706=ROVZ(381) !kts_h$1911Ispol_Ro
      x_36084=GZ(384) !kts_h$1924Ispol_G
      x_36085=ROVZ(384) !kts_h$1924Ispol_Ro
      x_36476=GZ(388) !kts_h$1938Ispol_G
      x_36477=ROVZ(388) !kts_h$1938Ispol_Ro
      x_36693=GZ(391) !kts_h$1949Ispol_G
      x_36694=ROVZ(391) !kts_h$1949Ispol_Ro
      x_36875=GZ(393) !kts_h$1959Ispol_G
      x_36876=ROVZ(393) !kts_h$1959Ispol_Ro
      x_37057=GZ(395) !kts_h$1969Ispol_G
      x_37058=ROVZ(395) !kts_h$1969Ispol_Ro
      x_37239=GZ(397) !kts_h$1979Ispol_G
      x_37240=ROVZ(397) !kts_h$1979Ispol_Ro
      x_37421=GZ(399) !kts_h$1989Ispol_G
      x_37422=ROVZ(399) !kts_h$1989Ispol_Ro
      x_37603=GZ(401) !kts_h$1999Ispol_G
      x_37604=ROVZ(401) !kts_h$1999Ispol_Ro
      x_37785=GZ(403) !kts_h$2009Ispol_G
      x_37786=ROVZ(403) !kts_h$2009Ispol_Ro
      x_37967=GZ(405) !kts_h$2019Ispol_G
      x_37968=ROVZ(405) !kts_h$2019Ispol_Ro
      x_38149=GZ(407) !kts_h$2029Ispol_G
      x_38150=ROVZ(407) !kts_h$2029Ispol_Ro
      x_38331=GZ(409) !kts_h$2039Ispol_G
      x_38332=ROVZ(409) !kts_h$2039Ispol_Ro
      x_38513=GZ(411) !kts_h$2049Ispol_G
      x_38514=ROVZ(411) !kts_h$2049Ispol_Ro
      x_38695=GZ(413) !kts_h$2059Ispol_G
      x_38696=ROVZ(413) !kts_h$2059Ispol_Ro
      x_38877=GZ(415) !kts_h$2069Ispol_G
      x_38878=ROVZ(415) !kts_h$2069Ispol_Ro
      x_39059=GZ(417) !kts_h$2079Ispol_G
      x_39060=ROVZ(417) !kts_h$2079Ispol_Ro
      x_39241=GZ(419) !kts_h$2089Ispol_G
      x_39242=ROVZ(419) !kts_h$2089Ispol_Ro
      x_39423=GZ(421) !kts_h$2099Ispol_G
      x_39424=ROVZ(421) !kts_h$2099Ispol_Ro
      x_39605=GZ(423) !kts_h$2109Ispol_G
      x_39606=ROVZ(423) !kts_h$2109Ispol_Ro
      x_39787=GZ(425) !kts_h$2119Ispol_G
      x_39788=ROVZ(425) !kts_h$2119Ispol_Ro
      x_39969=GZ(427) !kts_h$2129Ispol_G
      x_39970=ROVZ(427) !kts_h$2129Ispol_Ro
      x_40151=GZ(429) !20FDA64CU027_G
      x_40152=ROVZ(429) !20FDA64CU027_Ro
      x_40333=GZ(431) !20FDA64CU028_G
      x_40334=ROVZ(431) !20FDA64CU028_Ro
      x_40725=GZ(435) !kts_h$2159Ispol_G
      x_40726=ROVZ(435) !kts_h$2159Ispol_Ro
      x_41104=GZ(438) !kts_h$2172Ispol_G
      x_41105=ROVZ(438) !kts_h$2172Ispol_Ro
      x_41496=GZ(442) !kts_h$2186Ispol_G
      x_41497=ROVZ(442) !kts_h$2186Ispol_Ro
      x_41713=GZ(445) !kts_h$2197Ispol_G
      x_41714=ROVZ(445) !kts_h$2197Ispol_Ro
      x_41895=GZ(447) !kts_h$2207Ispol_G
      x_41896=ROVZ(447) !kts_h$2207Ispol_Ro
      x_42077=GZ(449) !kts_h$2217Ispol_G
      x_42078=ROVZ(449) !kts_h$2217Ispol_Ro
      x_42259=GZ(451) !kts_h$2227Ispol_G
      x_42260=ROVZ(451) !kts_h$2227Ispol_Ro
      x_42441=GZ(453) !kts_h$2237Ispol_G
      x_42442=ROVZ(453) !kts_h$2237Ispol_Ro
      x_42623=GZ(455) !kts_h$2247Ispol_G
      x_42624=ROVZ(455) !kts_h$2247Ispol_Ro
      x_42805=GZ(457) !kts_h$2257Ispol_G
      x_42806=ROVZ(457) !kts_h$2257Ispol_Ro
      x_42987=GZ(459) !kts_h$2267Ispol_G
      x_42988=ROVZ(459) !kts_h$2267Ispol_Ro
      x_43169=GZ(461) !kts_h$2277Ispol_G
      x_43170=ROVZ(461) !kts_h$2277Ispol_Ro
      x_43351=GZ(463) !kts_h$2287Ispol_G
      x_43352=ROVZ(463) !kts_h$2287Ispol_Ro
      x_43533=GZ(465) !kts_h$2297Ispol_G
      x_43534=ROVZ(465) !kts_h$2297Ispol_Ro
      x_43715=GZ(467) !kts_h$2307Ispol_G
      x_43716=ROVZ(467) !kts_h$2307Ispol_Ro
      x_43897=GZ(469) !kts_h$2317Ispol_G
      x_43898=ROVZ(469) !kts_h$2317Ispol_Ro
      x_44079=GZ(471) !kts_h$2327Ispol_G
      x_44080=ROVZ(471) !kts_h$2327Ispol_Ro
      x_44261=GZ(473) !kts_h$2337Ispol_G
      x_44262=ROVZ(473) !kts_h$2337Ispol_Ro
      x_44443=GZ(475) !kts_h$2347Ispol_G
      x_44444=ROVZ(475) !kts_h$2347Ispol_Ro
      x_44625=GZ(477) !kts_h$2357Ispol_G
      x_44626=ROVZ(477) !kts_h$2357Ispol_Ro
      x_44807=GZ(479) !kts_h$2367Ispol_G
      x_44808=ROVZ(479) !kts_h$2367Ispol_Ro
      x_44989=GZ(481) !kts_h$2377Ispol_G
      x_44990=ROVZ(481) !kts_h$2377Ispol_Ro
      x_45171=GZ(483) !20FDA65CU027_G
      x_45172=ROVZ(483) !20FDA65CU027_Ro
      x_45353=GZ(485) !20FDA65CU028_G
      x_45354=ROVZ(485) !20FDA65CU028_Ro
      x_45745=GZ(489) !kts_h$2407Ispol_G
      x_45746=ROVZ(489) !kts_h$2407Ispol_Ro
      x_46124=GZ(492) !kts_h$2420Ispol_G
      x_46125=ROVZ(492) !kts_h$2420Ispol_Ro
      x_46516=GZ(496) !kts_h$2434Ispol_G
      x_46517=ROVZ(496) !kts_h$2434Ispol_Ro
      x_46733=GZ(499) !kts_h$2445Ispol_G
      x_46734=ROVZ(499) !kts_h$2445Ispol_Ro
      x_46915=GZ(501) !kts_h$2455Ispol_G
      x_46916=ROVZ(501) !kts_h$2455Ispol_Ro
      x_47097=GZ(503) !kts_h$2465Ispol_G
      x_47098=ROVZ(503) !kts_h$2465Ispol_Ro
      x_47279=GZ(505) !kts_h$2475Ispol_G
      x_47280=ROVZ(505) !kts_h$2475Ispol_Ro
      x_47461=GZ(507) !kts_h$2485Ispol_G
      x_47462=ROVZ(507) !kts_h$2485Ispol_Ro
      x_47643=GZ(509) !kts_h$2495Ispol_G
      x_47644=ROVZ(509) !kts_h$2495Ispol_Ro
      x_47825=GZ(511) !kts_h$2505Ispol_G
      x_47826=ROVZ(511) !kts_h$2505Ispol_Ro
      x_48007=GZ(513) !kts_h$2515Ispol_G
      x_48008=ROVZ(513) !kts_h$2515Ispol_Ro
      x_48189=GZ(515) !kts_h$2525Ispol_G
      x_48190=ROVZ(515) !kts_h$2525Ispol_Ro
      x_48371=GZ(517) !kts_h$2535Ispol_G
      x_48372=ROVZ(517) !kts_h$2535Ispol_Ro
      x_48553=GZ(519) !kts_h$2545Ispol_G
      x_48554=ROVZ(519) !kts_h$2545Ispol_Ro
      x_48735=GZ(521) !kts_h$2555Ispol_G
      x_48736=ROVZ(521) !kts_h$2555Ispol_Ro
      x_48917=GZ(523) !kts_h$2565Ispol_G
      x_48918=ROVZ(523) !kts_h$2565Ispol_Ro
      x_49099=GZ(525) !kts_h$2575Ispol_G
      x_49100=ROVZ(525) !kts_h$2575Ispol_Ro
      x_49281=GZ(527) !kts_h$2585Ispol_G
      x_49282=ROVZ(527) !kts_h$2585Ispol_Ro
      x_49463=GZ(529) !kts_h$2595Ispol_G
      x_49464=ROVZ(529) !kts_h$2595Ispol_Ro
      x_49645=GZ(531) !kts_h$2605Ispol_G
      x_49646=ROVZ(531) !kts_h$2605Ispol_Ro
      x_49827=GZ(533) !kts_h$2615Ispol_G
      x_49828=ROVZ(533) !kts_h$2615Ispol_Ro
      x_50009=GZ(535) !kts_h$2625Ispol_G
      x_50010=ROVZ(535) !kts_h$2625Ispol_Ro
      x_50191=GZ(537) !20FDA66CU027_G
      x_50192=ROVZ(537) !20FDA66CU027_Ro
      x_50373=GZ(539) !20FDA66CU028_G
      x_50374=ROVZ(539) !20FDA66CU028_Ro
      x_50747=GZ(545) !20FDA61CF019_G
      x_50748=ROVZ(545) !20FDA61CF019_Ro
      x_54045=GZ(580) !20FDA62CF019_G
      x_54046=ROVZ(580) !20FDA62CF019_Ro
      x_57343=GZ(615) !20FDA63CF019_G
      x_57344=ROVZ(615) !20FDA63CF019_Ro
      x_60641=GZ(650) !20FDA64CF019_G
      x_60642=ROVZ(650) !20FDA64CF019_Ro
      x_63939=GZ(685) !20FDA65CF019_G
      x_63940=ROVZ(685) !20FDA65CF019_Ro
      x_67237=GZ(720) !20FDA66CF019_G
      x_67238=ROVZ(720) !20FDA66CF019_Ro
      x_70323=NodPp(463) !20FDA60CP002_P
      x_70324=NodPp(464) !20FDA60CP003_P
      x_70357=NodPp(465) !20FDA60CP001_P
      x_71081=NodPp(470) !20FDA60CP005_P
      x_71082=NodPp(471) !20FDA60CP006_P
      x_71083=NodPp(472) !20FDA60CP007_P
      x_71612=GZ(760) !20FDA60CF035_G
      x_71613=ROVZ(760) !20FDA60CF035_Ro
      x_71614=GZ(761) !20FDA60CF036_G
      x_71615=ROVZ(761) !20FDA60CF036_Ro
      x_71616=GZ(762) !20FDA60CF037_G
      x_71617=ROVZ(762) !20FDA60CF037_Ro
      x_72097=NodPp(476) !20FDA62CP018_P
      x_72098=NodPp(477) !20FDA63CP018_P
      x_72169=NodPp(478) !20FDA61CP018_P
      x_74594=NodPp(494) !20FDA60CP025_P
      x_74757=NodPp(496) !20FDA60CP031_P
      x_75136=NodPp(497) !20FDA60CP011_P
      x_76392=NodPp(503) !20FDA60CP029_P
      x_76603=NodPp(504) !20FDA60CP017_P
      x_76604=NodPp(505) !20FDA60CP009_P
      x_76605=NodPp(506) !20FDA60CP035_P
      x_76803=NodPp(507) !20FDA60CP023_P
      x_78622=NodPp(520) !20FDA65CP018_P
      x_78623=NodPp(521) !20FDA66CP018_P
      x_79047=NodPp(526) !20FDA64CP018_P
      return
      end
