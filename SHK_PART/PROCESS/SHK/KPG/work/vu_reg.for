      Subroutine vu_reg(ext_deltat)
      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      Include 'vu_reg.fh'

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      if(R8_ad.le.R0_u) then
         R8_ed=R0_o
      elseif(R8_ad.gt.R0_i) then
         R8_ed=R0_e
      else
         R8_ed=R0_o+(R8_ad-(R0_u))*(R0_e-(R0_o))/(R0_i-(R0_u
     &))
      endif
C vu_reg.fgi(  58, 208):��������������� ���������
      !Constant L_(1) = vu_regClJ25 /.false./
      L_(1)=L0_id
C vu_reg.fgi( 103, 263):��������� ���. ������������� 
      L0_ol=btest(I0_ul,0)
C vu_reg.fgi(  56, 250):���������� ������ � ���������,47
      L_(2) = (.NOT.L0_ol)
C vu_reg.fgi( 149, 250):�
      R_(2) = (-R8_om) + R8_im
C vu_reg.fgi(  54, 272):��������,41
      R_(1)=R0_il*R_(2)
C vu_reg.fgi(  73, 271):����������� ��������
      Call PIDmod(deltat,L_ek,REAL(R_ok,4),L_ud,REAL(R8_em
     &,8),L_ak,
     & R8_am,REAL(R_ef,4),REAL(R_af,4),R_uk,REAL(R_if,4),REAL
     &(R_od,4),
     & REAL(R_of,4),R_ik,REAL(R_uf,4),R_el,L_(1),R_al,
     & REAL(R_(1),4))
C vu_reg.fgi( 114, 263):��������� ����������� ����������������,rgro
C label 14  try14=try14-1
      if(L_(2)) then
         R8_em=R8_em
      else
         R8_em=R8_am
      endif
C vu_reg.fgi( 155, 270):���� RE IN LO CH7,43
      End
