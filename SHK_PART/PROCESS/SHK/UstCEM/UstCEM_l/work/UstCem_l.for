      Subroutine UstCem_l(ext_deltat)
      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      Include 'UstCem_l.fh'

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      R8_of = R8_if
C KPW_log.fgi(  39, 281):��������
      !��������� R_(1) = KPW_logC?? /1.0/
      R_(1)=R0_e
C KPW_log.fgi( 148, 287):���������
      R8_i = (-R8_o) + R_(1)
C KPW_log.fgi( 153, 288):��������
      !��������� R_(2) = KPW_logC?? /0.01/
      R_(2)=R0_u
C KPW_log.fgi(  38, 261):���������
      L_(1)=R8_ad.lt.R_(2)
C KPW_log.fgi(  42, 262):���������� <
      L_od=L_id.or.(L_od.and..not.(L_(1)))
      L_(2)=.not.L_od
C KPW_log.fgi(  84, 264):RS �������
      L_af=L_od
C KPW_log.fgi( 109, 266):������,20KPW21AM001_filling
      !��������� R_(4) = KPW_logC?? /0.0/
      R_(4)=R0_ed
C KPW_log.fgi(  38, 249):���������
      !��������� R_(3) = KPW_logC?? /1.0/
      R_(3)=R0_ud
C KPW_log.fgi(  38, 247):���������
      if(L_af) then
         R8_ef=R_(3)
      else
         R8_ef=R_(4)
      endif
C KPW_log.fgi(  41, 248):���� RE IN LO CH7
      R_(8)=R0_uf*R8_uk
C KPJ_log.fgi( 149, 228):����������� ��������
      !��������� R_(6) = KPJ_logC?? /0.0/
      R_(6)=R0_ak
C KPJ_log.fgi( 137, 274):���������
      !��������� R_(5) = KPJ_logC?? /0.25/
      R_(5)=R0_ek
C KPJ_log.fgi( 137, 257):���������
      !��������� R_(7) = KPJ_logC?? /0.2/
      R_(7)=R0_ik
C KPJ_log.fgi( 137, 261):���������
      if(R8_al.le.R_(7)) then
         R_(9)=R_(6)
      elseif(R8_al.gt.R_(5)) then
         R_(9)=R8_ok
      else
         R_(9)=R_(6)+(R8_al-(R_(7)))*(R8_ok-(R_(6)))/(R_(5
     &)-(R_(7)))
      endif
C KPJ_log.fgi( 146, 267):��������������� ��������� �����������
      R8_il = R_(9) * R8_ep * R8_el * R_(8)
C KPJ_log.fgi( 157, 248):����������
      R_(13)=R0_ol*R8_om
C KPJ_log.fgi(  50, 228):����������� ��������
      !��������� R_(11) = KPJ_logC?? /0.0/
      R_(11)=R0_ul
C KPJ_log.fgi(  38, 274):���������
      !��������� R_(10) = KPJ_logC?? /0.25/
      R_(10)=R0_am
C KPJ_log.fgi(  38, 257):���������
      !��������� R_(12) = KPJ_logC?? /0.2/
      R_(12)=R0_em
C KPJ_log.fgi(  38, 261):���������
      if(R8_um.le.R_(12)) then
         R_(14)=R_(11)
      elseif(R8_um.gt.R_(10)) then
         R_(14)=R8_im
      else
         R_(14)=R_(11)+(R8_um-(R_(12)))*(R8_im-(R_(11)))/
     &(R_(10)-(R_(12)))
      endif
C KPJ_log.fgi(  47, 267):��������������� ��������� �����������
      R8_ip = R_(14) * R8_ep * R8_ap * R_(13)
C KPJ_log.fgi(  58, 248):����������
      !��������� R_(15) = KPV_logC?? /1.0/
      R_(15)=R0_op
C KPV_log.fgi(  34, 163):���������
      R8_up = R_(15) + R8_ar
C KPV_log.fgi(  37, 162):��������
      R_er = R8_ur
C KPV_log.fgi( 313, 195):��������
      R8_ode = R8_ide
C KPV_log.fgi( 120, 280):��������
      R8_afe = R8_ude
C KPV_log.fgi(  37, 280):��������
      !��������� R_(27) = KPV_logC?? /0.0/
      R_(27)=R0_ir
C KPV_log.fgi(  69, 236):���������
      !��������� R_(16) = KPV_logC?? /1.0/
      R_(16)=R0_or
C KPV_log.fgi( 336, 221):���������
      L_(3)=R8_ur.lt.R_(16)
C KPV_log.fgi( 340, 222):���������� <
      !��������� R_(18) = KPV_logC?? /0.0/
      R_(18)=R0_es
C KPV_log.fgi( 344, 239):���������
      if(R8_et.le.R0_at) then
         R_(19)=R0_us
      elseif(R8_et.gt.R0_os) then
         R_(19)=R0_is
      else
         R_(19)=R0_us+(R8_et-(R0_at))*(R0_is-(R0_us))/(R0_os
     &-(R0_at))
      endif
C KPV_log.fgi( 322, 252):��������������� ���������
      R_(20) = R_(19) * R8_ot * R8_it
C KPV_log.fgi( 334, 241):����������
      if(L_(3)) then
         R_(17)=R_(18)
      else
         R_(17)=R_(20)
      endif
C KPV_log.fgi( 347, 239):���� RE IN LO CH7
      R8_ut=R0_as*R_(17)
C KPV_log.fgi( 361, 239):����������� ��������
      !��������� R_(21) = KPV_logC?? /1.0/
      R_(21)=R0_av
C KPV_log.fgi( 199, 227):���������
      L_(4)=R8_ev.lt.R_(21)
C KPV_log.fgi( 203, 228):���������� <
      !��������� R_(23) = KPV_logC?? /0.0/
      R_(23)=R0_ov
C KPV_log.fgi( 206, 245):���������
      R_(24) = R8_ade * R8_uv
C KPV_log.fgi( 197, 247):����������
      if(L_(4)) then
         R_(22)=R_(23)
      else
         R_(22)=R_(24)
      endif
C KPV_log.fgi( 209, 245):���� RE IN LO CH7
      R8_ax=R0_iv*R_(22)
C KPV_log.fgi( 223, 245):����������� ��������
      !��������� R_(25) = KPV_logC?? /2.0/
      R_(25)=R0_ex
C KPV_log.fgi(  61, 218):���������
      L_(5)=R8_ix.lt.R_(25)
C KPV_log.fgi(  65, 219):���������� <
      if(R8_obe.le.R0_ibe) then
         R_(28)=R0_ebe
      elseif(R8_obe.gt.R0_abe) then
         R_(28)=R0_ux
      else
         R_(28)=R0_ebe+(R8_obe-(R0_ibe))*(R0_ux-(R0_ebe))
     &/(R0_abe-(R0_ibe))
      endif
C KPV_log.fgi(  47, 249):��������������� ���������
      R_(29) = R_(28) * R8_ade * R8_ube
C KPV_log.fgi(  59, 238):����������
      if(L_(5)) then
         R_(26)=R_(27)
      else
         R_(26)=R_(29)
      endif
C KPV_log.fgi(  72, 236):���� RE IN LO CH7
      R8_ede=R0_ox*R_(26)
C KPV_log.fgi(  86, 236):����������� ��������
      End
