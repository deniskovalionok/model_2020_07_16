      subroutine tpo_hData!
      implicit none
      include 'tpo_h.fh'

      REAL*8 FlwTnSqFrom(11)
      COMMON/tpo_h_nowrt/ FlwTnSqFrom
      REAL*8 FlwHTnFromUp(11)
      COMMON/tpo_h_nowrt/ FlwHTnFromUp
      REAL*8 FlwTnSqTo(11)
      COMMON/tpo_h_nowrt/ FlwTnSqTo
      REAL*8 FlwHTnFromDo(11)
      COMMON/tpo_h_nowrt/ FlwHTnFromDo
      REAL*8 FlwHTnToUp(11)
      COMMON/tpo_h_nowrt/ FlwHTnToUp
      REAL*8 FlwHTnToDo(11)
      COMMON/tpo_h_nowrt/ FlwHTnToDo
      INTEGER*4 Fan2NTN2N
      PARAMETER (Fan2NTN2N=0)
      INTEGER*4 Fan2NTN0N
      PARAMETER (Fan2NTN0N=0)
      INTEGER*4 Fan2NTauNN
      PARAMETER (Fan2NTauNN=0)
      INTEGER*4 Fan2NdPG5N
      PARAMETER (Fan2NdPG5N=0)
      INTEGER*4 Fan2NdPG4N
      PARAMETER (Fan2NdPG4N=0)
      INTEGER*4 Fan2NdPG3N
      PARAMETER (Fan2NdPG3N=0)
      INTEGER*4 Fan2NdPG2N
      PARAMETER (Fan2NdPG2N=0)
      INTEGER*4 Fan2NdPG0N
      PARAMETER (Fan2NdPG0N=0)
      INTEGER*4 Fan2TauNN
      PARAMETER (Fan2TauNN=0)
      INTEGER*4 FanDim_TauNmax
      PARAMETER (FanDim_TauNmax=0)
      INTEGER*4 FanDim_Taumax
      PARAMETER (FanDim_Taumax=0)
      INTEGER*4 FanDim_KMax
      PARAMETER (FanDim_KMax=0)
      INTEGER*4 FanDim_PMax
      PARAMETER (FanDim_PMax=0)
      INTEGER*4 Tn8Ne
      PARAMETER (Tn8Ne=2)
      INTEGER*4 Tn3Np
      PARAMETER (Tn3Np=2)
      INTEGER*4 Tn3Ne
      PARAMETER (Tn3Ne=4)
      INTEGER*4 INVPERe(10)
      COMMON/tpo_h_nowrt/ INVPERe
      INTEGER*4 NZSUBe(63)
      COMMON/tpo_h_nowrt/ NZSUBe
      INTEGER*4 INZSUBe(11)
      COMMON/tpo_h_nowrt/ INZSUBe
      INTEGER*4 ILNZe(11)
      COMMON/tpo_h_nowrt/ ILNZe
      INTEGER*4 INVPERp(10)
      COMMON/tpo_h_nowrt/ INVPERp
      INTEGER*4 NZSUBp(63)
      COMMON/tpo_h_nowrt/ NZSUBp
      INTEGER*4 INZSUBp(11)
      COMMON/tpo_h_nowrt/ INZSUBp
      INTEGER*4 ILNZp(11)
      COMMON/tpo_h_nowrt/ ILNZp
      INTEGER*4 Nthermo
      PARAMETER (Nthermo=11)
      INTEGER*4 Nhydro
      PARAMETER (Nhydro=11)
      INTEGER*4 Nent
      PARAMETER (Nent=10)
      INTEGER*4 Npc
      PARAMETER (Npc=20)
      INTEGER*4 Npres
      PARAMETER (Npres=10)
      INTEGER*4 Nf
      PARAMETER (Nf=6)
      INTEGER*4 Tn7Ne
      PARAMETER (Tn7Ne=1)
      INTEGER*4 Tn7Np
      PARAMETER (Tn7Np=1)
      INTEGER*4 Tn9Ne
      PARAMETER (Tn9Ne=6)
      INTEGER*4 Tn9Np
      PARAMETER (Tn9Np=2)
      INTEGER*4 Fan2NTN3N
      PARAMETER (Fan2NTN3N=0)
      INTEGER*4 Fan2NTN4N
      PARAMETER (Fan2NTN4N=0)
      INTEGER*4 Fan2NTN5N
      PARAMETER (Fan2NTN5N=0)
      INTEGER*4 Tr1Dim_Nu
      PARAMETER (Tr1Dim_Nu=-111)
      INTEGER*4 Tr2Dim_Ksi
      PARAMETER (Tr2Dim_Ksi=-111)
      INTEGER*4 Tn8Np
      PARAMETER (Tn8Np=1)
      LOGICAL*1 PIntIter(10)
      COMMON/tpo_h_nowrt/ PIntIter
      LOGICAL*1 EIntIter(10)
      COMMON/tpo_h_nowrt/ EIntIter
      LOGICAL*1 EextIter(10)
      COMMON/tpo_h_nowrt/ EextIter
      LOGICAL*1 PExtIter(10)
      COMMON/tpo_h_nowrt/ PExtIter
      !DEC$PSECT/tpo_h_NOWRT/ NoWRT
      DATA FlwTnSqTo(1:11)/0,0,0,0,0,0,0,0,0,0,0/
      DATA FlwTnSqFrom(1:11)/0,0,0,0,0,0,0,0,0,0,0/
      DATA FlwHTnToDo(1:11)/0,0,0,0,0,0,0,0,0,0,0/
      DATA FlwHTnToUp(1:11)/0,0,0,0,0,0,0,0,0,0,0/
      DATA FlwHTnFromDo(1:11)/0,0,0,0,0,0,0,0,0,0,0/
      DATA FlwHTnFromUp(1:11)/0,0,0,0,0,0,0,0,0,0,0/
      DATA EextIter(1:10)/.true.,.false.,.true.,.true.,.true.,.true.,
     >.true.,.false.,.false.,.true./
      DATA PExtIter(1:10)/.true.,.false.,.true.,.true.,.true.,.true.,
     >.true.,.false.,.false.,.true./
      DATA EIntIter(1:10)/.false.,.false.,.false.,.false.,.false.,
     >.false.,.false.,.false.,.false.,.false./
      DATA PIntIter(1:10)/.false.,.false.,.false.,.false.,.false.,
     >.false.,.false.,.false.,.false.,.false./
      DATA INVPERe(1:10)/8,9,2,5,10,7,3,1,6,4/
      DATA NZSUBe(1:63)/8,3,5,4,5,8,6,8,9,10,1268654864,1268654784,12,0,
     >1072693248,0,1086556288,0,1086556160,0,1072693248,1268654944,11,10
     >,1,1,0,1,10,1268672776,416530372,0,0,0,1268672792,416530372,
     >421944236,421944232,0,49,415320896,421944572,183248352,1268655356,
     >71297666,71383748,1268655368,71297846,71383748,1268655384,71300271
     >,40,421724320,1268655396,71299790,49,0,1268655432,71295906,
     >415320904,120,1268672352,419817464/
      DATA INZSUBe(1:11)/1,2,4,5,7,8,8,9,10,10,10/
      DATA ILNZe(1:11)/1,2,4,6,8,10,12,14,16,17,17/
      DATA INVPERp(1:10)/8,9,2,5,10,7,3,1,6,4/
      DATA NZSUBp(1:63)/8,3,5,4,5,8,6,8,9,10,1268654864,1268654784,12,0,
     >1072693248,0,1086556288,0,1086556160,0,1072693248,1268654944,11,10
     >,1,1,0,1,10,1268672776,416530372,0,0,0,1268672792,416530372,
     >421944236,421944232,421944372,421944592,421944324,421944572,
     >183248352,183248720,183248144,183247568,183264192,183264320,
     >183264080,183264432,0,0,0,0,0,0,101116720,183272496,183272000,
     >183247344,101116608,1268672344,419817464/
      DATA INZSUBp(1:11)/1,2,4,5,7,8,8,9,10,10,10/
      DATA ILNZp(1:11)/1,2,4,6,8,10,12,14,16,17,17/
      return
      end

