      Subroutine FDB60(ext_deltat)
      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      Include 'FDB60.fh'

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      Call FDB60_ConIn
      L_(18)=f
C FDB60_logic.fgi(1297, 783):pull: ���������� ������� ���������,20FDB60EC005
      L_(20)=f
C FDB60_logic.fgi(1051, 779):pull: ���������� ������� ���������,20FDB60EC004
      L_(21)=f
C FDB60_logic.fgi( 821, 781):pull: ���������� ������� ���������,20FDB60EC003
      L_(24)=f
C FDB60_logic.fgi( 614, 791):pull: ���������� ������� ���������,20FDB60EC002
      L_(103)=f
C FDB60_logic.fgi( 582, 292):pull: ���������� ������� ���������,20FDB60EC007
      L_(127)=f
C FDB60_logic.fgi( 370, 325):pull: ���������� ������� ���������,20FDB60EC006
      L_(352)=f
C FDB60_logic.fgi( 377, 792):pull: ���������� ������� ���������,20FDB60EC001
      R_(6)=R8_ef
C FDB60_vent_log.fgi(  37, 267):pre: �������������� �����  
      R_(15)=R0_odud
C FDB60_logic.fgi( 578, 770):pre: �������� ��������� ������
      R_(14)=R0_ixod
C FDB60_logic.fgi( 577, 679):pre: �������� ��������� ������
      R_(13)=R0_atod
C FDB60_logic.fgi( 577, 653):pre: �������� ��������� ������
      R_(12)=R0_opod
C FDB60_logic.fgi(1014, 672):pre: �������� ��������� ������
      R_(11)=R0_ilod
C FDB60_logic.fgi(1014, 628):pre: �������� ��������� ������
      R_(10)=R0_akod
C FDB60_logic.fgi(1012, 763):pre: �������� ��������� ������
      R_ex = R8_e + (-R8_o)
C FDB60_vent_log.fgi( 111, 137):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_av,R_ube,REAL(1,4),
     & REAL(R_ov,4),REAL(R_uv,4),
     & REAL(R_ut,4),REAL(R_ot,4),I_obe,
     & REAL(R_ix,4),L_ox,REAL(R_ux,4),L_abe,L_ebe,R_ax,
     & REAL(R_iv,4),REAL(R_ev,4),L_ibe,REAL(R_ex,4))
      !}
C FDB60_vlv.fgi( 100,  80):���������� �������,20FDB60CP001XQ03
      R_(1) = R8_o + (-R8_i)
C FDB60_vent_log.fgi( 111, 231):��������
      R_ape = R_(1)
C FDB60_vent_log.fgi( 117, 231):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ule,R_ore,REAL(1,4),
     & REAL(R_ime,4),REAL(R_ome,4),
     & REAL(R_ole,4),REAL(R_ile,4),I_ire,
     & REAL(R_epe,4),L_ipe,REAL(R_ope,4),L_upe,L_are,R_ume
     &,
     & REAL(R_eme,4),REAL(R_ame,4),L_ere,REAL(R_ape,4))
      !}
C FDB60_vlv.fgi(  31,  66):���������� �������,20FDB60CP001XQ01
      R_(2) = 100
C FDB60_vent_log.fgi(  37, 165):��������� (RE4) (�������)
      R_(3) = 100
C FDB60_vent_log.fgi(  37, 171):��������� (RE4) (�������)
      R_ofe = R8_u
C FDB60_vent_log.fgi(  37, 146):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ide,R_ele,REAL(1,4),
     & REAL(R_afe,4),REAL(R_efe,4),
     & REAL(R_ede,4),REAL(R_ade,4),I_ale,
     & REAL(R_ufe,4),L_ake,REAL(R_eke,4),L_ike,L_oke,R_ife
     &,
     & REAL(R_ude,4),REAL(R_ode,4),L_uke,REAL(R_ofe,4))
      !}
C FDB60_vlv.fgi(  64,  80):���������� �������,20FDB60CP001XQ02
      R_(4) = 100
C FDB60_vent_log.fgi(  37, 177):��������� (RE4) (�������)
      R_(5) = 100
C FDB60_vent_log.fgi(  37, 185):��������� (RE4) (�������)
      R_ared = R8_ad
C FDB60_vent_log.fgi(  37, 232):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_umed,R_osed,REAL(1,4)
     &,
     & REAL(R_iped,4),REAL(R_oped,4),
     & REAL(R_omed,4),REAL(R_imed,4),I_ised,
     & REAL(R_ered,4),L_ired,REAL(R_ored,4),L_ured,L_ased
     &,R_uped,
     & REAL(R_eped,4),REAL(R_aped,4),L_esed,REAL(R_ared,4
     &))
      !}
C FDB60_vlv.fgi(  29, 150):���������� �������,20FDB60CF001XQ01
      R_ebod = R8_ed
C FDB60_vent_log.fgi(  37, 201):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_axid,R_udod,REAL(1,4)
     &,
     & REAL(R_oxid,4),REAL(R_uxid,4),
     & REAL(R_uvid,4),REAL(R_ovid,4),I_odod,
     & REAL(R_ibod,4),L_obod,REAL(R_ubod,4),L_adod,L_edod
     &,R_abod,
     & REAL(R_ixid,4),REAL(R_exid,4),L_idod,REAL(R_ebod,4
     &))
      !}
C FDB60_vlv.fgi(  29, 136):���������� �������,20FDB60CM001XQ01
      R_usid = R8_id
C FDB60_vent_log.fgi(  37, 214):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_orid,R_ivid,REAL(1,4)
     &,
     & REAL(R_esid,4),REAL(R_isid,4),
     & REAL(R_irid,4),REAL(R_erid,4),I_evid,
     & REAL(R_atid,4),L_etid,REAL(R_itid,4),L_otid,L_utid
     &,R_osid,
     & REAL(R_asid,4),REAL(R_urid,4),L_avid,REAL(R_usid,4
     &))
      !}
C FDB60_vlv.fgi(  59, 136):���������� �������,20FDB60CQ001XQ01
      R_ite = R8_od
C FDB60_vent_log.fgi(  36, 225):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ese,R_axe,REAL(1,4),
     & REAL(R_use,4),REAL(R_ate,4),
     & REAL(R_ase,4),REAL(R_ure,4),I_uve,
     & REAL(R_ote,4),L_ute,REAL(R_ave,4),L_eve,L_ive,R_ete
     &,
     & REAL(R_ose,4),REAL(R_ise,4),L_ove,REAL(R_ite,4))
      !}
C FDB60_vlv.fgi(  31,  80):���������� �������,20FDB60CT001XQ01
      !��������� R_(9) = FDB60_vent_logC?? /0.0/
      R_(9)=R0_if
C FDB60_vent_log.fgi(  26, 268):���������
      !��������� R_(8) = FDB60_vent_logC?? /0.001/
      R_(8)=R0_of
C FDB60_vent_log.fgi(  26, 266):���������
      L_(36) = L_ukud.AND.L_afod
C FDB60_logic.fgi(1012, 590):�
      L_(56) = L_ipud.AND.L_ifod
C FDB60_logic.fgi(1013, 745):�
      if(.not.L_ikod) then
         R0_akod=0.0
      elseif(.not.L0_ekod) then
         R0_akod=R0_ufod
      else
         R0_akod=max(R_(10)-deltat,0.0)
      endif
      L_(6)=L_ikod.and.R0_akod.le.0.0
      L0_ekod=L_ikod
C FDB60_logic.fgi(1012, 763):�������� ��������� ������
      if(.not.L_ulod) then
         R0_ilod=0.0
      elseif(.not.L0_olod) then
         R0_ilod=R0_elod
      else
         R0_ilod=max(R_(11)-deltat,0.0)
      endif
      L_(7)=L_ulod.and.R0_ilod.le.0.0
      L0_olod=L_ulod
C FDB60_logic.fgi(1014, 628):�������� ��������� ������
      L_(9) = L_omod.AND.(.NOT.L_arod)
C FDB60_logic.fgi(1002, 651):�
      L_(10) = L_erod.AND.L_arod
C FDB60_logic.fgi(1002, 672):�
      if(.not.L_(10)) then
         R0_opod=0.0
      elseif(.not.L0_upod) then
         R0_opod=R0_ipod
      else
         R0_opod=max(R_(12)-deltat,0.0)
      endif
      L_(12)=L_(10).and.R0_opod.le.0.0
      L0_upod=L_(10)
C FDB60_logic.fgi(1014, 672):�������� ��������� ������
      L_(84) = L_ivud.AND.L_urod
C FDB60_logic.fgi( 565, 619):�
      L_(13) = L_utod.AND.L_otod.AND.(.NOT.L_uxod).AND.L_itod
C FDB60_logic.fgi( 565, 653):�
      if(.not.L_(13)) then
         R0_atod=0.0
      elseif(.not.L0_etod) then
         R0_atod=R0_usod
      else
         R0_atod=max(R_(13)-deltat,0.0)
      endif
      L_(14)=L_(13).and.R0_atod.le.0.0
      L0_etod=L_(13)
C FDB60_logic.fgi( 577, 653):�������� ��������� ������
      L_(15) = L_ebud.AND.L_abud.AND.L_uxod
C FDB60_logic.fgi( 565, 679):�
      if(.not.L_(15)) then
         R0_ixod=0.0
      elseif(.not.L0_oxod) then
         R0_ixod=R0_exod
      else
         R0_ixod=max(R_(14)-deltat,0.0)
      endif
      L_(17)=L_(15).and.R0_ixod.le.0.0
      L0_oxod=L_(15)
C FDB60_logic.fgi( 577, 679):�������� ��������� ������
      L_(99) = L_ebaf.AND.L_edud
C FDB60_logic.fgi( 567, 752):�
      if(.not.L_afud) then
         R0_odud=0.0
      elseif(.not.L0_udud) then
         R0_odud=R0_idud
      else
         R0_odud=max(R_(15)-deltat,0.0)
      endif
      L_(22)=L_afud.and.R0_odud.le.0.0
      L0_udud=L_afud
C FDB60_logic.fgi( 578, 770):�������� ��������� ������
      L_(110)=R_atef.gt.R0_edaf
C FDB60_logic.fgi( 526, 239):���������� >
      L_(111)=R_atef.lt.R0_idaf
C FDB60_logic.fgi( 526, 247):���������� <
      L_(116) = L_(111).AND.L_(110)
C FDB60_logic.fgi( 534, 243):�
      L_ikaf = L_ofaf.AND.L_akaf
C FDB60_logic.fgi( 615, 326):�
      L_ifaf=L_ikaf
C FDB60_logic.fgi( 637, 326):������,FDB50_MOUNT_TO_STORE
      L_(150)=R_atef.gt.R0_ilaf
C FDB60_logic.fgi( 332, 764):���������� >
      L_(151)=R_atef.lt.R0_olaf
C FDB60_logic.fgi( 332, 772):���������� <
      L_(149) = L_(151).AND.L_(150)
C FDB60_logic.fgi( 340, 768):�
      L_(155) = L_(149).AND.L_otef
C FDB60_logic.fgi( 350, 765):�
      L_(159)=R_atef.gt.R0_imaf
C FDB60_logic.fgi( 320, 191):���������� >
      L_(160)=R_atef.lt.R0_omaf
C FDB60_logic.fgi( 320, 199):���������� <
      L_(165) = L_(160).AND.L_(159)
C FDB60_logic.fgi( 328, 195):�
      L_(173)=R_atef.gt.R0_opaf
C FDB60_logic.fgi( 321, 266):���������� >
      L_(174)=R_atef.lt.R0_upaf
C FDB60_logic.fgi( 321, 274):���������� <
      L_(179) = L_(174).AND.L_(173)
C FDB60_logic.fgi( 329, 270):�
      L_(188)=R_atef.gt.R0_uraf
C FDB60_logic.fgi(1248, 411):���������� >
      L_(189)=R_atef.lt.R0_asaf
C FDB60_logic.fgi(1248, 419):���������� <
      L_(194) = L_(189).AND.L_(188)
C FDB60_logic.fgi(1256, 415):�
      L_(207)=R_atef.gt.R0_utaf
C FDB60_logic.fgi(1248, 508):���������� >
      L_(208)=R_atef.lt.R0_avaf
C FDB60_logic.fgi(1248, 516):���������� <
      L_(213) = L_(208).AND.L_(207)
C FDB60_logic.fgi(1256, 512):�
      L_(222)=R_atef.gt.R0_exaf
C FDB60_logic.fgi(1247, 587):���������� >
      L_(223)=R_atef.lt.R0_ixaf
C FDB60_logic.fgi(1247, 595):���������� <
      L_(228) = L_(223).AND.L_(222)
C FDB60_logic.fgi(1255, 591):�
      L_(229)=R_ebef.gt.R0_uxaf
C FDB60_logic.fgi(1262, 612):���������� >
      L_(230)=R_ebef.lt.R0_abef
C FDB60_logic.fgi(1262, 620):���������� <
      L_(235) = L_(230).AND.L_(229)
C FDB60_logic.fgi(1270, 616):�
      L_(244)=R_atef.gt.R0_idef
C FDB60_logic.fgi(1262, 671):���������� >
      L_(245)=R_atef.lt.R0_odef
C FDB60_logic.fgi(1262, 679):���������� <
      L_(243) = L_(245).AND.L_(244)
C FDB60_logic.fgi(1270, 675):�
      L_(253) = L_ikif.AND.L_(243)
C FDB60_logic.fgi(1274, 687):�
      L_(258) = L_alef.AND.L_afef
C FDB60_logic.fgi(1278, 766):�
      L_(290)=R_atef.gt.R0_epef
C FDB60_logic.fgi( 327, 540):���������� >
      L_(291)=R_atef.lt.R0_ipef
C FDB60_logic.fgi( 327, 548):���������� <
      L_(296) = L_(291).AND.L_(290)
C FDB60_logic.fgi( 335, 544):�
      L_(304)=R_atef.gt.R0_iref
C FDB60_logic.fgi( 327, 609):���������� >
      L_(305)=R_atef.lt.R0_oref
C FDB60_logic.fgi( 327, 617):���������� <
      L_(310) = L_(305).AND.L_(304)
C FDB60_logic.fgi( 335, 613):�
      L_(318)=R_atef.gt.R0_osef
C FDB60_logic.fgi( 332, 680):���������� >
      L_(319)=R_atef.lt.R0_usef
C FDB60_logic.fgi( 332, 688):���������� <
      L_(324) = L_(319).AND.L_(318)
C FDB60_logic.fgi( 340, 684):�
      I_(2) = z'01000007'
C FDB60_logic.fgi( 215, 772):��������� ������������� IN (�������)
      I_(1) = z'0100000A'
C FDB60_logic.fgi( 215, 770):��������� ������������� IN (�������)
      I_(4) = z'01000007'
C FDB60_logic.fgi( 215, 782):��������� ������������� IN (�������)
      I_(3) = z'0100000A'
C FDB60_logic.fgi( 215, 780):��������� ������������� IN (�������)
      I_(6) = z'01000007'
C FDB60_logic.fgi( 215, 792):��������� ������������� IN (�������)
      I_(5) = z'0100000A'
C FDB60_logic.fgi( 215, 790):��������� ������������� IN (�������)
      C30_ovef = '������������������'
C FDB60_logic.fgi( 176, 802):��������� ���������� CH20 (CH30) (�������)
      C30_uvef = '������������'
C FDB60_logic.fgi( 197, 806):��������� ���������� CH20 (CH30) (�������)
      C30_axef = '������'
C FDB60_logic.fgi( 191, 801):��������� ���������� CH20 (CH30) (�������)
      C30_oxef = '�� ������'
C FDB60_logic.fgi( 176, 804):��������� ���������� CH20 (CH30) (�������)
      L_(342)=.true.
C FDB60_logic.fgi( 143, 778):��������� ���������� (�������)
      L0_ibif=R0_ubif.ne.R0_obif
      R0_obif=R0_ubif
C FDB60_logic.fgi( 130, 771):���������� ������������� ������
      if(L0_ibif) then
         L_(339)=L_(342)
      else
         L_(339)=.false.
      endif
C FDB60_logic.fgi( 147, 777):���� � ������������� �������
      L_(3)=L_(339)
C FDB60_logic.fgi( 147, 777):������-�������: ���������� ��� �������������� ������
      L_(343)=.true.
C FDB60_logic.fgi( 143, 795):��������� ���������� (�������)
      L0_idif=R0_udif.ne.R0_odif
      R0_odif=R0_udif
C FDB60_logic.fgi( 130, 788):���������� ������������� ������
      if(L0_idif) then
         L_(340)=L_(343)
      else
         L_(340)=.false.
      endif
C FDB60_logic.fgi( 147, 794):���� � ������������� �������
      L_(2)=L_(340)
C FDB60_logic.fgi( 147, 794):������-�������: ���������� ��� �������������� ������
      L_(337) = L_(2).OR.L_(3)
C FDB60_logic.fgi( 157, 807):���
      L_(344)=.true.
C FDB60_logic.fgi( 143, 811):��������� ���������� (�������)
      L0_ifif=R0_ufif.ne.R0_ofif
      R0_ofif=R0_ufif
C FDB60_logic.fgi( 130, 804):���������� ������������� ������
      if(L0_ifif) then
         L_(341)=L_(344)
      else
         L_(341)=.false.
      endif
C FDB60_logic.fgi( 147, 810):���� � ������������� �������
      L_(1)=L_(341)
C FDB60_logic.fgi( 147, 810):������-�������: ���������� ��� �������������� ������
      L_(333) = L_(1).OR.L_(2)
C FDB60_logic.fgi( 157, 774):���
      L_ebif=(L_(3).or.L_ebif).and..not.(L_(333))
      L_(334)=.not.L_ebif
C FDB60_logic.fgi( 165, 776):RS �������
      L_abif=L_ebif
C FDB60_logic.fgi( 181, 778):������,FDB6_tech_mode
      if(L_ebif) then
         I_avef=I_(1)
      else
         I_avef=I_(2)
      endif
C FDB60_logic.fgi( 218, 770):���� RE IN LO CH7
      L_(335) = L_(1).OR.L_(3)
C FDB60_logic.fgi( 157, 791):���
      L_edif=(L_(2).or.L_edif).and..not.(L_(335))
      L_(336)=.not.L_edif
C FDB60_logic.fgi( 165, 793):RS �������
      L_adif=L_edif
C FDB60_logic.fgi( 181, 795):������,FDB6_ruch_mode
      if(L_edif) then
         I_evef=I_(3)
      else
         I_evef=I_(4)
      endif
C FDB60_logic.fgi( 218, 780):���� RE IN LO CH7
      L_efif=(L_(1).or.L_efif).and..not.(L_(337))
      L_(338)=.not.L_efif
C FDB60_logic.fgi( 165, 809):RS �������
      L_afif=L_efif
C FDB60_logic.fgi( 181, 811):������,FDB6_avt_mode
      !{
      Call BVALVE_MAN(deltat,REAL(R_ivo,4),L_udu,L_afu,R8_iso
     &,L_abif,
     & L_adif,L_afif,I_ubu,I_adu,R_ipo,
     & REAL(R_upo,4),R_oro,REAL(R_aso,4),
     & R_aro,REAL(R_iro,4),I_abu,I_edu,I_obu,I_ibu,L_eso,
     & L_idu,L_ufu,L_uro,L_ero,
     & L_eto,L_ato,L_efu,L_opo,L_oto,L_iku,L_odu,
     & L_avo,L_evo,REAL(R8_used,8),REAL(1.0,4),R8_uxed,L_uso
     &,
     & L_oso,L_aku,R_uxo,REAL(R_ito,4),L_eku,L_oku,L_ovo,
     & L_uvo,L_axo,L_ixo,L_oxo,L_exo)
      !}

      if(L_oxo.or.L_ixo.or.L_exo.or.L_axo.or.L_uvo.or.L_ovo
     &) then      
                  I_ebu = ior(z'000000FF',z'04000000')   
     &                         
                else
                   I_ebu = z'40000000'
      endif
C FDB60_vlv.fgi( 124, 165):���� ���������� ������ �������,20FDB60AA102
      R_ur = R_uxo * R_(3)
C FDB60_vent_log.fgi(  40, 173):����������
      !{
      Call DAT_ANA_HANDLER(deltat,R_op,R_it,REAL(1,4),
     & REAL(R_er,4),REAL(R_ir,4),
     & REAL(R_ip,4),REAL(R_ep,4),I_et,
     & REAL(R_as,4),L_es,REAL(R_is,4),L_os,L_us,R_or,
     & REAL(R_ar,4),REAL(R_up,4),L_at,REAL(R_ur,4))
      !}
C FDB60_vlv.fgi(  64,  65):���������� �������,20FDB60AA102XQ01
      !{
      Call BVALVE_MAN(deltat,REAL(R_emad,4),L_osad,L_usad
     &,R8_ekad,L_abif,
     & L_adif,L_afif,I_orad,I_urad,R_edad,
     & REAL(R_odad,4),R_ifad,REAL(R_ufad,4),
     & R_udad,REAL(R_efad,4),I_upad,I_asad,I_irad,I_erad,L_akad
     &,
     & L_esad,L_otad,L_ofad,L_afad,
     & L_alad,L_ukad,L_atad,L_idad,L_ilad,L_evad,L_isad,
     & L_ulad,L_amad,REAL(R8_used,8),REAL(1.0,4),R8_uxed,L_okad
     &,
     & L_ikad,L_utad,R_opad,REAL(R_elad,4),L_avad,L_ivad,L_imad
     &,
     & L_omad,L_umad,L_epad,L_ipad,L_apad)
      !}

      if(L_ipad.or.L_epad.or.L_apad.or.L_umad.or.L_omad.or.L_imad
     &) then      
                  I_arad = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_arad = z'40000000'
      endif
C FDB60_vlv.fgi(  94, 165):���� ���������� ������ �������,20FDB60AA100
      R_oked = R_opad * R_(5)
C FDB60_vent_log.fgi(  40, 187):����������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ifed,R_emed,REAL(1,4)
     &,
     & REAL(R_aked,4),REAL(R_eked,4),
     & REAL(R_efed,4),REAL(R_afed,4),I_amed,
     & REAL(R_uked,4),L_aled,REAL(R_eled,4),L_iled,L_oled
     &,R_iked,
     & REAL(R_ufed,4),REAL(R_ofed,4),L_uled,REAL(R_oked,4
     &))
      !}
C FDB60_vlv.fgi(  59, 150):���������� �������,20FDB60AA100XQ01
      !{Call KN_VLVR(deltat,REAL(R_eki,4),L_opi,L_upi,R8_edi
C ,
      Call SOLENOIDVALVE_MAN(deltat,REAL(R_eki,4),L_opi,L_upi
     &,R8_edi,C30_ofi,
     & L_abif,L_adif,L_afif,I_omi,I_umi,R_exe,
     & REAL(R_oxe,4),R_ibi,REAL(R_ubi,4),
     & R_uxe,REAL(R_ebi,4),I_uli,I_api,I_imi,I_emi,
     & L_obi,L_abi,L_afi,L_udi,L_ari,
     & L_ixe,L_ifi,L_esi,L_odi,L_idi,L_asi,L_isi,
     & L_adi,L_epi,L_ori,L_ipi,L_ufi,L_aki,
     & REAL(R8_used,8),REAL(1.0,4),R8_uxed,L_uri,R_oli,
     & REAL(R_efi,4),L_iki,L_oki,L_uki,L_eli,L_ili,L_ali)
      !}

      if(L_ili.or.L_eli.or.L_ali.or.L_uki.or.L_oki.or.L_iki
     &) then      
                  I_ami = ior(z'000000FF',z'04000000')   
     &                         
                else
                   I_ami = z'40000000'
      endif
C FDB60_vlv.fgi(  94, 141):���� ���������� ���������������� ��������,20FDB60AA104
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_ado,4),L_ilo,L_olo,R8_ovi
     &,C30_abo,
     & L_abif,L_adif,L_afif,I_iko,I_oko,R_ibo,R_ebo,
     & R_osi,REAL(R_ati,4),R_uti,
     & REAL(R_evi,4),R_eti,REAL(R_oti,4),I_ofo,
     & I_uko,I_eko,I_ako,L_ivi,L_alo,L_imo,L_avi,
     & L_iti,L_ixi,L_exi,L_ulo,L_usi,L_uxi,
     & L_apo,L_elo,L_obo,L_ubo,REAL(R8_used,8),REAL(1.0,4
     &),R8_uxed,
     & L_axi,L_uvi,L_omo,R_ifo,REAL(R_oxi,4),L_umo,L_epo,
     & L_edo,L_ido,L_odo,L_afo,L_efo,L_udo)
      !}

      if(L_efo.or.L_afo.or.L_udo.or.L_odo.or.L_ido.or.L_edo
     &) then      
                  I_ufo = ior(z'000000FF',z'04000000')   
     &                         
                else
                   I_ufo = z'40000000'
      endif
C FDB60_vlv.fgi( 140, 165):���� ���������� �������� ��������,20FDB60AA103
      R_il = R_ifo * R_(2)
C FDB60_vent_log.fgi(  40, 167):����������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ek,R_ap,REAL(1,4),
     & REAL(R_uk,4),REAL(R_al,4),
     & REAL(R_ak,4),REAL(R_uf,4),I_um,
     & REAL(R_ol,4),L_ul,REAL(R_am,4),L_em,L_im,R_el,
     & REAL(R_ok,4),REAL(R_ik,4),L_om,REAL(R_il,4))
      !}
C FDB60_vlv.fgi( 100,  65):���������� �������,20FDB60AA103XQ01
      !{
      Call BVALVE_MAN(deltat,REAL(R_uru,4),L_exu,L_ixu,R8_umu
     &,L_abif,
     & L_adif,L_afif,I_evu,I_ivu,R_uku,
     & REAL(R_elu,4),R_amu,REAL(R_imu,4),
     & R_ilu,REAL(R_ulu,4),I_itu,I_ovu,I_avu,I_utu,L_omu,
     & L_uvu,L_ebad,L_emu,L_olu,
     & L_opu,L_ipu,L_oxu,L_alu,L_aru,L_ubad,L_axu,
     & L_iru,L_oru,REAL(R8_used,8),REAL(1.0,4),R8_uxed,L_epu
     &,
     & L_apu,L_ibad,R_etu,REAL(R_upu,4),L_obad,L_adad,L_asu
     &,
     & L_esu,L_isu,L_usu,L_atu,L_osu)
      !}

      if(L_atu.or.L_usu.or.L_osu.or.L_isu.or.L_esu.or.L_asu
     &) then      
                  I_otu = ior(z'000000FF',z'04000000')   
     &                         
                else
                   I_otu = z'40000000'
      endif
C FDB60_vlv.fgi( 109, 165):���� ���������� ������ �������,20FDB60AA101
      R_ebed = R_etu * R_(4)
C FDB60_vent_log.fgi(  40, 179):����������
      !{
      Call DAT_ANA_HANDLER(deltat,R_axad,R_uded,REAL(1,4)
     &,
     & REAL(R_oxad,4),REAL(R_uxad,4),
     & REAL(R_uvad,4),REAL(R_ovad,4),I_oded,
     & REAL(R_ibed,4),L_obed,REAL(R_ubed,4),L_aded,L_eded
     &,R_abed,
     & REAL(R_ixad,4),REAL(R_exad,4),L_ided,REAL(R_ebed,4
     &))
      !}
C FDB60_vlv.fgi(  59, 164):���������� �������,20FDB60AA101XQ01
      if(L_efif) then
         C30_ixef=C30_ovef
      else
         C30_ixef=C30_oxef
      endif
C FDB60_logic.fgi( 180, 802):���� RE IN LO CH20
      if(L_edif) then
         C30_exef=C30_axef
      else
         C30_exef=C30_ixef
      endif
C FDB60_logic.fgi( 195, 801):���� RE IN LO CH20
      if(L_ebif) then
         C30_uxef=C30_uvef
      else
         C30_uxef=C30_exef
      endif
C FDB60_logic.fgi( 204, 800):���� RE IN LO CH20
      if(L_efif) then
         I_ivef=I_(5)
      else
         I_ivef=I_(6)
      endif
C FDB60_logic.fgi( 218, 790):���� RE IN LO CH7
      L_(354)=I_olif.ne.0
C FDB60_logic.fgi( 329, 800):��������� 1->LO
      Call PUMP_HANDLER(deltat,C30_oxed,I_idid,L_abif,L_adif
     &,L_afif,
     & I_odid,I_edid,R_ived,REAL(R_uved,4),
     & R_uted,REAL(R_eved,4),I_udid,L_ilid,L_ikid,L_epid,
     & L_ipid,L_obid,L_ofid,L_akid,L_ufid,L_ekid,L_alid,L_oted
     &,
     & L_oved,L_ited,L_aved,L_omid,L_(5),
     & L_umid,L_(4),L_eted,L_ated,L_ubid,I_afid,R_ulid,R_amid
     &,
     & L_arid,L_elid,L_apid,REAL(R8_used,8),L_ukid,
     & REAL(R8_okid,8),R_opid,REAL(R_efid,4),R_ifid,REAL(R8_ibid
     &,8),R_ebid,
     & R8_uxed,R_upid,R8_okid,REAL(R_axed,4),REAL(R_exed,4
     &))
C FDB60_vlv.fgi( 109, 141):���������� ���������� �������,20FDB60KN001
C label 257  try257=try257-1
C sav1=R_upid
C sav2=R_opid
C sav3=L_ilid
C sav4=L_alid
C sav5=L_ikid
C sav6=R8_okid
C sav7=R_ifid
C sav8=I_afid
C sav9=I_udid
C sav10=I_odid
C sav11=I_idid
C sav12=I_edid
C sav13=I_adid
C sav14=L_ubid
C sav15=L_obid
C sav16=R_ebid
C sav17=C30_oxed
C sav18=L_oved
C sav19=L_aved
      Call PUMP_HANDLER(deltat,C30_oxed,I_idid,L_abif,L_adif
     &,L_afif,
     & I_odid,I_edid,R_ived,REAL(R_uved,4),
     & R_uted,REAL(R_eved,4),I_udid,L_ilid,L_ikid,L_epid,
     & L_ipid,L_obid,L_ofid,L_akid,L_ufid,L_ekid,L_alid,L_oted
     &,
     & L_oved,L_ited,L_aved,L_omid,L_(5),
     & L_umid,L_(4),L_eted,L_ated,L_ubid,I_afid,R_ulid,R_amid
     &,
     & L_arid,L_elid,L_apid,REAL(R8_used,8),L_ukid,
     & REAL(R8_okid,8),R_opid,REAL(R_efid,4),R_ifid,REAL(R8_ibid
     &,8),R_ebid,
     & R8_uxed,R_upid,R8_okid,REAL(R_axed,4),REAL(R_exed,4
     &))
C FDB60_vlv.fgi( 109, 141):recalc:���������� ���������� �������,20FDB60KN001
C if(sav1.ne.R_upid .and. try257.gt.0) goto 257
C if(sav2.ne.R_opid .and. try257.gt.0) goto 257
C if(sav3.ne.L_ilid .and. try257.gt.0) goto 257
C if(sav4.ne.L_alid .and. try257.gt.0) goto 257
C if(sav5.ne.L_ikid .and. try257.gt.0) goto 257
C if(sav6.ne.R8_okid .and. try257.gt.0) goto 257
C if(sav7.ne.R_ifid .and. try257.gt.0) goto 257
C if(sav8.ne.I_afid .and. try257.gt.0) goto 257
C if(sav9.ne.I_udid .and. try257.gt.0) goto 257
C if(sav10.ne.I_odid .and. try257.gt.0) goto 257
C if(sav11.ne.I_idid .and. try257.gt.0) goto 257
C if(sav12.ne.I_edid .and. try257.gt.0) goto 257
C if(sav13.ne.I_adid .and. try257.gt.0) goto 257
C if(sav14.ne.L_ubid .and. try257.gt.0) goto 257
C if(sav15.ne.L_obid .and. try257.gt.0) goto 257
C if(sav16.ne.R_ebid .and. try257.gt.0) goto 257
C if(sav17.ne.C30_oxed .and. try257.gt.0) goto 257
C if(sav18.ne.L_oved .and. try257.gt.0) goto 257
C if(sav19.ne.L_aved .and. try257.gt.0) goto 257
      if(L_alid) then
         R_(7)=R_(8)
      else
         R_(7)=R_(9)
      endif
C FDB60_vent_log.fgi(  29, 266):���� RE IN LO CH7
      R8_ef=(R0_ud*R_(6)+deltat*R_(7))/(R0_ud+deltat)
C FDB60_vent_log.fgi(  37, 267):�������������� �����  
      R8_af=R8_ef
C FDB60_vent_log.fgi(  52, 267):������,F_FDB60KN001_G
      L_(353) = L_(354).AND.(.NOT.L_okaf)
C FDB60_logic.fgi( 342, 799):�
C label 265  try265=try265-1
      L_(122) = L_ufaf.AND.L_akaf.AND.L_okaf
C FDB60_logic.fgi( 575, 306):�
      if (.not.L_udaf.and.(L_obaf.or.L_(122))) then
          I_afaf = 0
          L_udaf = .true.
          L_(120) = .true.
      endif
      if (I_afaf .gt. 0) then
         L_(120) = .false.
      endif
      if(L_(103))then
          I_afaf = 0
          L_udaf = .false.
          L_(120) = .false.
      endif
C FDB60_logic.fgi( 582, 292):���������� ������� ���������,20FDB60EC007
      if(L_udaf) then
          if (L_(120)) then
              I_afaf = 01
              L_efaf = .true.
              L_(121) = .false.
          endif
          if (L_otef) then
              L_(121) = .true.
              L_efaf = .false.
          endif
          if (I_afaf.ne.01) then
              L_efaf = .false.
              L_(121) = .false.
          endif
      else
          L_(121) = .false.
      endif
C FDB60_logic.fgi( 582, 273):��� ������� ���������,20FDB60EC007
      if(L_udaf) then
          if (L_(121)) then
              I_afaf = 02
              L_odaf = .true.
              L_(115) = .false.
          endif
          if (L_(116)) then
              L_(115) = .true.
              L_odaf = .false.
          endif
          if (I_afaf.ne.02) then
              L_odaf = .false.
              L_(115) = .false.
          endif
      else
          L_(115) = .false.
      endif
C FDB60_logic.fgi( 582, 243):��� ������� ���������,20FDB60EC007
      if(L_udaf) then
          if (L_(115)) then
              I_afaf = 03
              L_adaf = .true.
              L_ekaf = .false.
          endif
          if (L_ikif) then
              L_ekaf = .true.
              L_adaf = .false.
          endif
          if (I_afaf.ne.03) then
              L_adaf = .false.
              L_ekaf = .false.
          endif
      else
          L_ekaf = .false.
      endif
C FDB60_logic.fgi( 582, 217):��� ������� ���������,20FDB60EC007
      L_ukaf = L_ikaf.OR.L_ekaf
C FDB60_logic.fgi( 425, 338):���
      if (.not.L_alif.and.(L_elif.or.L_(353))) then
          I_ilif = 0
          L_alif = .true.
          L_(351) = .true.
      endif
      if (I_ilif .gt. 0) then
         L_(351) = .false.
      endif
      if(L_(352))then
          I_ilif = 0
          L_alif = .false.
          L_(351) = .false.
      endif
C FDB60_logic.fgi( 377, 792):���������� ������� ���������,20FDB60EC001
      if(L_alif) then
          if (L_(351)) then
              I_ilif = 01
              L_ulaf = .true.
              L_(345) = .false.
          endif
          if (L_(155)) then
              L_(345) = .true.
              L_ulaf = .false.
          endif
          if (I_ilif.ne.01) then
              L_ulaf = .false.
              L_(345) = .false.
          endif
      else
          L_(345) = .false.
      endif
C FDB60_logic.fgi( 377, 765):��� ������� ���������,20FDB60EC001
      if(L_alif) then
          if (L_(345)) then
              I_ilif = 02
              L_okif = .true.
              L_(346) = .false.
          endif
          if (L_ikif) then
              L_(346) = .true.
              L_okif = .false.
          endif
          if (I_ilif.ne.02) then
              L_okif = .false.
              L_(346) = .false.
          endif
      else
          L_(346) = .false.
      endif
C FDB60_logic.fgi( 377, 750):��� ������� ���������,20FDB60EC001
      if(L_alif) then
          if (L_(346)) then
              I_ilif = 03
              L_ukif = .true.
              L_(329) = .false.
          endif
          if (L_ekif) then
              L_(329) = .true.
              L_ukif = .false.
          endif
          if (I_ilif.ne.03) then
              L_ukif = .false.
              L_(329) = .false.
          endif
      else
          L_(329) = .false.
      endif
C FDB60_logic.fgi( 377, 731):��� ������� ���������,20FDB60EC001
      if(L_alif) then
          if (L_(329)) then
              I_ilif = 04
              L_utef = .true.
              L_(328) = .false.
          endif
          if (L_otef) then
              L_(328) = .true.
              L_utef = .false.
          endif
          if (I_ilif.ne.04) then
              L_utef = .false.
              L_(328) = .false.
          endif
      else
          L_(328) = .false.
      endif
C FDB60_logic.fgi( 377, 714):��� ������� ���������,20FDB60EC001
      if(L_alif) then
          if (L_(328)) then
              I_ilif = 05
              L_etef = .true.
              L_(323) = .false.
          endif
          if (L_(324)) then
              L_(323) = .true.
              L_etef = .false.
          endif
          if (I_ilif.ne.05) then
              L_etef = .false.
              L_(323) = .false.
          endif
      else
          L_(323) = .false.
      endif
C FDB60_logic.fgi( 377, 684):��� ������� ���������,20FDB60EC001
      if(L_alif) then
          if (L_(323)) then
              I_ilif = 06
              L_isef = .true.
              L_esef = .false.
          endif
          if (L_ikif) then
              L_esef = .true.
              L_isef = .false.
          endif
          if (I_ilif.ne.06) then
              L_isef = .false.
              L_esef = .false.
          endif
      else
          L_esef = .false.
      endif
C FDB60_logic.fgi( 377, 658):��� ������� ���������,20FDB60EC001
      if(L_alif) then
          if (L_esef) then
              I_ilif = 07
              L_asef = .true.
              L_(314) = .false.
          endif
          if (L_otef) then
              L_(314) = .true.
              L_asef = .false.
          endif
          if (I_ilif.ne.07) then
              L_asef = .false.
              L_(314) = .false.
          endif
      else
          L_(314) = .false.
      endif
C FDB60_logic.fgi( 377, 632):��� ������� ���������,20FDB60EC001
      if(L_alif) then
          if (L_(314)) then
              I_ilif = 08
              L_uref = .true.
              L_(309) = .false.
          endif
          if (L_(310)) then
              L_(309) = .true.
              L_uref = .false.
          endif
          if (I_ilif.ne.08) then
              L_uref = .false.
              L_(309) = .false.
          endif
      else
          L_(309) = .false.
      endif
C FDB60_logic.fgi( 377, 613):��� ������� ���������,20FDB60EC001
      if(L_alif) then
          if (L_(309)) then
              I_ilif = 09
              L_eref = .true.
              L_aref = .false.
          endif
          if (L_ikif) then
              L_aref = .true.
              L_eref = .false.
          endif
          if (I_ilif.ne.09) then
              L_eref = .false.
              L_aref = .false.
          endif
      else
          L_aref = .false.
      endif
C FDB60_logic.fgi( 377, 594):��� ������� ���������,20FDB60EC001
      if(L_alif) then
          if (L_aref) then
              I_ilif = 10
              L_upef = .true.
              L_(300) = .false.
          endif
          if (L_otef) then
              L_(300) = .true.
              L_upef = .false.
          endif
          if (I_ilif.ne.10) then
              L_upef = .false.
              L_(300) = .false.
          endif
      else
          L_(300) = .false.
      endif
C FDB60_logic.fgi( 377, 565):��� ������� ���������,20FDB60EC001
      if(L_alif) then
          if (L_(300)) then
              I_ilif = 11
              L_opef = .true.
              L_(295) = .false.
          endif
          if (L_(296)) then
              L_(295) = .true.
              L_opef = .false.
          endif
          if (I_ilif.ne.11) then
              L_opef = .false.
              L_(295) = .false.
          endif
      else
          L_(295) = .false.
      endif
C FDB60_logic.fgi( 377, 544):��� ������� ���������,20FDB60EC001
      if(L_alif) then
          if (L_(295)) then
              I_ilif = 12
              L_apef = .true.
              L_(289) = .false.
          endif
          if (L_umef) then
              L_(289) = .true.
              L_apef = .false.
          endif
          if (I_ilif.ne.12) then
              L_apef = .false.
              L_(289) = .false.
          endif
      else
          L_(289) = .false.
      endif
C FDB60_logic.fgi( 377, 526):��� ������� ���������,20FDB60EC001
      if(L_alif) then
          if (L_(289)) then
              I_ilif = 13
              L_omef = .true.
              L_(285) = .false.
          endif
          if (L_imef) then
              L_(285) = .true.
              L_omef = .false.
          endif
          if (I_ilif.ne.13) then
              L_omef = .false.
              L_(285) = .false.
          endif
      else
          L_(285) = .false.
      endif
C FDB60_logic.fgi( 377, 504):��� ������� ���������,20FDB60EC001
      if(L_alif) then
          if (L_(285)) then
              I_ilif = 14
              L_amef = .true.
              L_(278) = .false.
          endif
          if (L_ulef) then
              L_(278) = .true.
              L_amef = .false.
          endif
          if (I_ilif.ne.14) then
              L_amef = .false.
              L_(278) = .false.
          endif
      else
          L_(278) = .false.
      endif
C FDB60_logic.fgi( 377, 487):��� ������� ���������,20FDB60EC001
      if(L_alif) then
          if (L_(278)) then
              I_ilif = 15
              L_olef = .true.
              L_akif = .false.
          endif
          if (L_ilef) then
              L_akif = .true.
              L_olef = .false.
          endif
          if (I_ilif.ne.15) then
              L_olef = .false.
              L_akif = .false.
          endif
      else
          L_akif = .false.
      endif
C FDB60_logic.fgi( 377, 472):��� ������� ���������,20FDB60EC001
      if (.not.L_uxud.and.(L_ifud.or.L_akif)) then
          I_abaf = 0
          L_uxud = .true.
          L_(23) = .true.
      endif
      if (I_abaf .gt. 0) then
         L_(23) = .false.
      endif
      if(L_(24))then
          I_abaf = 0
          L_uxud = .false.
          L_(23) = .false.
      endif
C FDB60_logic.fgi( 614, 791):���������� ������� ���������,20FDB60EC002
      if(L_uxud) then
          if (L_(23)) then
              I_abaf = 01
              L_efud = .true.
              L_(97) = .false.
          endif
          if (L_(22)) then
              L_(97) = .true.
              L_efud = .false.
          endif
          if (I_abaf.ne.01) then
              L_efud = .false.
              L_(97) = .false.
          endif
      else
          L_(97) = .false.
      endif
C FDB60_logic.fgi( 614, 770):��� ������� ���������,20FDB60EC002
      if(L_uxud) then
          if (L_(97)) then
              I_abaf = 02
              L_ibaf = .true.
              L_(98) = .false.
          endif
          if (L_(99)) then
              L_(98) = .true.
              L_ibaf = .false.
          endif
          if (I_abaf.ne.02) then
              L_ibaf = .false.
              L_(98) = .false.
          endif
      else
          L_(98) = .false.
      endif
C FDB60_logic.fgi( 614, 752):��� ������� ���������,20FDB60EC002
      if(L_uxud) then
          if (L_(98)) then
              I_abaf = 03
              L_oxud = .true.
              L_(96) = .false.
          endif
          if (L_ixud) then
              L_(96) = .true.
              L_oxud = .false.
          endif
          if (I_abaf.ne.03) then
              L_oxud = .false.
              L_(96) = .false.
          endif
      else
          L_(96) = .false.
      endif
C FDB60_logic.fgi( 614, 734):��� ������� ���������,20FDB60EC002
      if(L_uxud) then
          if (L_(96)) then
              I_abaf = 04
              L_otud = .true.
              L_(88) = .false.
          endif
          if (L_itud) then
              L_(88) = .true.
              L_otud = .false.
          endif
          if (I_abaf.ne.04) then
              L_otud = .false.
              L_(88) = .false.
          endif
      else
          L_(88) = .false.
      endif
C FDB60_logic.fgi( 614, 716):��� ������� ���������,20FDB60EC002
      if(L_uxud) then
          if (L_(88)) then
              I_abaf = 05
              L_axud = .true.
              L_(89) = .false.
          endif
          if (L_uvud) then
              L_(89) = .true.
              L_axud = .false.
          endif
          if (I_abaf.ne.05) then
              L_axud = .false.
              L_(89) = .false.
          endif
      else
          L_(89) = .false.
      endif
C FDB60_logic.fgi( 614, 698):��� ������� ���������,20FDB60EC002
      if(L_uxud) then
          if (L_(89)) then
              I_abaf = 06
              L_ibud = .true.
              L_(16) = .false.
          endif
          if (L_(17)) then
              L_(16) = .true.
              L_ibud = .false.
          endif
          if (I_abaf.ne.06) then
              L_ibud = .false.
              L_(16) = .false.
          endif
      else
          L_(16) = .false.
      endif
C FDB60_logic.fgi( 614, 679):��� ������� ���������,20FDB60EC002
      if(L_uxud) then
          if (L_(16)) then
              I_abaf = 07
              L_avod = .true.
              L_(82) = .false.
          endif
          if (L_(14)) then
              L_(82) = .true.
              L_avod = .false.
          endif
          if (I_abaf.ne.07) then
              L_avod = .false.
              L_(82) = .false.
          endif
      else
          L_(82) = .false.
      endif
C FDB60_logic.fgi( 614, 653):��� ������� ���������,20FDB60EC002
      if(L_uxud) then
          if (L_(82)) then
              I_abaf = 08
              L_ovud = .true.
              L_(83) = .false.
          endif
          if (L_(84)) then
              L_(83) = .true.
              L_ovud = .false.
          endif
          if (I_abaf.ne.08) then
              L_ovud = .false.
              L_(83) = .false.
          endif
      else
          L_(83) = .false.
      endif
C FDB60_logic.fgi( 614, 619):��� ������� ���������,20FDB60EC002
      if(L_uxud) then
          if (L_(83)) then
              I_abaf = 09
              L_evud = .true.
              L_(81) = .false.
          endif
          if (L_avud) then
              L_(81) = .true.
              L_evud = .false.
          endif
          if (I_abaf.ne.09) then
              L_evud = .false.
              L_(81) = .false.
          endif
      else
          L_(81) = .false.
      endif
C FDB60_logic.fgi( 614, 599):��� ������� ���������,20FDB60EC002
      if(L_uxud) then
          if (L_(81)) then
              I_abaf = 10
              L_etud = .true.
              L_(74) = .false.
          endif
          if (L_atud) then
              L_(74) = .true.
              L_etud = .false.
          endif
          if (I_abaf.ne.10) then
              L_etud = .false.
              L_(74) = .false.
          endif
      else
          L_(74) = .false.
      endif
C FDB60_logic.fgi( 614, 582):��� ������� ���������,20FDB60EC002
      if(L_uxud) then
          if (L_(74)) then
              I_abaf = 11
              L_usud = .true.
              L_isud = .false.
          endif
          if (L_osud) then
              L_isud = .true.
              L_usud = .false.
          endif
          if (I_abaf.ne.11) then
              L_usud = .false.
              L_isud = .false.
          endif
      else
          L_isud = .false.
      endif
C FDB60_logic.fgi( 614, 565):��� ������� ���������,20FDB60EC002
      if (.not.L_orud.and.(L_adud.or.L_isud)) then
          I_urud = 0
          L_orud = .true.
          L_(66) = .true.
      endif
      if (I_urud .gt. 0) then
         L_(66) = .false.
      endif
      if(L_(21))then
          I_urud = 0
          L_orud = .false.
          L_(66) = .false.
      endif
C FDB60_logic.fgi( 821, 781):���������� ������� ���������,20FDB60EC003
      if(L_orud) then
          if (L_(66)) then
              I_urud = 01
              L_esud = .true.
              L_(67) = .false.
          endif
          if (L_asud) then
              L_(67) = .true.
              L_esud = .false.
          endif
          if (I_urud.ne.01) then
              L_esud = .false.
              L_(67) = .false.
          endif
      else
          L_(67) = .false.
      endif
C FDB60_logic.fgi( 821, 761):��� ������� ���������,20FDB60EC003
      if(L_orud) then
          if (L_(67)) then
              I_urud = 02
              L_erud = .true.
              L_upud = .false.
          endif
          if (L_arud) then
              L_upud = .true.
              L_erud = .false.
          endif
          if (I_urud.ne.02) then
              L_erud = .false.
              L_upud = .false.
          endif
      else
          L_upud = .false.
      endif
C FDB60_logic.fgi( 821, 740):��� ������� ���������,20FDB60EC003
      if (.not.L_apud.and.(L_ubud.or.L_upud)) then
          I_epud = 0
          L_apud = .true.
          L_(19) = .true.
      endif
      if (I_epud .gt. 0) then
         L_(19) = .false.
      endif
      if(L_(20))then
          I_epud = 0
          L_apud = .false.
          L_(19) = .false.
      endif
C FDB60_logic.fgi(1051, 779):���������� ������� ���������,20FDB60EC004
      if(L_apud) then
          if (L_(19)) then
              I_epud = 01
              L_ukod = .true.
              L_(54) = .false.
          endif
          if (L_(6)) then
              L_(54) = .true.
              L_ukod = .false.
          endif
          if (I_epud.ne.01) then
              L_ukod = .false.
              L_(54) = .false.
          endif
      else
          L_(54) = .false.
      endif
C FDB60_logic.fgi(1051, 763):��� ������� ���������,20FDB60EC004
      if(L_apud) then
          if (L_(54)) then
              I_epud = 02
              L_opud = .true.
              L_(55) = .false.
          endif
          if (L_(56)) then
              L_(55) = .true.
              L_opud = .false.
          endif
          if (I_epud.ne.02) then
              L_opud = .false.
              L_(55) = .false.
          endif
      else
          L_(55) = .false.
      endif
C FDB60_logic.fgi(1051, 745):��� ������� ���������,20FDB60EC004
      if(L_apud) then
          if (L_(55)) then
              I_epud = 03
              L_umud = .true.
              L_(53) = .false.
          endif
          if (L_omud) then
              L_(53) = .true.
              L_umud = .false.
          endif
          if (I_epud.ne.03) then
              L_umud = .false.
              L_(53) = .false.
          endif
      else
          L_(53) = .false.
      endif
C FDB60_logic.fgi(1051, 727):��� ������� ���������,20FDB60EC004
      if(L_apud) then
          if (L_(53)) then
              I_epud = 04
              L_ulud = .true.
              L_(45) = .false.
          endif
          if (L_olud) then
              L_(45) = .true.
              L_ulud = .false.
          endif
          if (I_epud.ne.04) then
              L_ulud = .false.
              L_(45) = .false.
          endif
      else
          L_(45) = .false.
      endif
C FDB60_logic.fgi(1051, 709):��� ������� ���������,20FDB60EC004
      if(L_apud) then
          if (L_(45)) then
              I_epud = 05
              L_emud = .true.
              L_(46) = .false.
          endif
          if (L_amud) then
              L_(46) = .true.
              L_emud = .false.
          endif
          if (I_epud.ne.05) then
              L_emud = .false.
              L_(46) = .false.
          endif
      else
          L_(46) = .false.
      endif
C FDB60_logic.fgi(1051, 691):��� ������� ���������,20FDB60EC004
      if(L_apud) then
          if (L_(46)) then
              I_epud = 06
              L_irod = .true.
              L_(11) = .false.
          endif
          if (L_(12)) then
              L_(11) = .true.
              L_irod = .false.
          endif
          if (I_epud.ne.06) then
              L_irod = .false.
              L_(11) = .false.
          endif
      else
          L_(11) = .false.
      endif
C FDB60_logic.fgi(1051, 672):��� ������� ���������,20FDB60EC004
      if(L_apud) then
          if (L_(11)) then
              I_epud = 07
              L_umod = .true.
              L_(8) = .false.
          endif
          if (L_(9)) then
              L_(8) = .true.
              L_umod = .false.
          endif
          if (I_epud.ne.07) then
              L_umod = .false.
              L_(8) = .false.
          endif
      else
          L_(8) = .false.
      endif
C FDB60_logic.fgi(1051, 651):��� ������� ���������,20FDB60EC004
      if(L_apud) then
          if (L_(8)) then
              I_epud = 08
              L_amod = .true.
              L_(37) = .false.
          endif
          if (L_(7)) then
              L_(37) = .true.
              L_amod = .false.
          endif
          if (I_epud.ne.08) then
              L_amod = .false.
              L_(37) = .false.
          endif
      else
          L_(37) = .false.
      endif
C FDB60_logic.fgi(1051, 628):��� ������� ���������,20FDB60EC004
      if(L_apud) then
          if (L_(37)) then
              I_epud = 09
              L_ilud = .true.
              L_(38) = .false.
          endif
          if (L_elud) then
              L_(38) = .true.
              L_ilud = .false.
          endif
          if (I_epud.ne.09) then
              L_ilud = .false.
              L_(38) = .false.
          endif
      else
          L_(38) = .false.
      endif
C FDB60_logic.fgi(1051, 608):��� ������� ���������,20FDB60EC004
      if(L_apud) then
          if (L_(38)) then
              I_epud = 10
              L_alud = .true.
              L_(35) = .false.
          endif
          if (L_(36)) then
              L_(35) = .true.
              L_alud = .false.
          endif
          if (I_epud.ne.10) then
              L_alud = .false.
              L_(35) = .false.
          endif
      else
          L_(35) = .false.
      endif
C FDB60_logic.fgi(1051, 590):��� ������� ���������,20FDB60EC004
      if(L_apud) then
          if (L_(35)) then
              I_epud = 11
              L_ufud = .true.
              L_(28) = .false.
          endif
          if (L_ofud) then
              L_(28) = .true.
              L_ufud = .false.
          endif
          if (I_epud.ne.11) then
              L_ufud = .false.
              L_(28) = .false.
          endif
      else
          L_(28) = .false.
      endif
C FDB60_logic.fgi(1051, 572):��� ������� ���������,20FDB60EC004
      if(L_apud) then
          if (L_(28)) then
              I_epud = 12
              L_ikud = .true.
              L_akud = .false.
          endif
          if (L_ekud) then
              L_akud = .true.
              L_ikud = .false.
          endif
          if (I_epud.ne.12) then
              L_ikud = .false.
              L_akud = .false.
          endif
      else
          L_akud = .false.
      endif
C FDB60_logic.fgi(1051, 554):��� ������� ���������,20FDB60EC004
      if (.not.L_ekef.and.(L_obud.or.L_akud)) then
          I_ikef = 0
          L_ekef = .true.
          L_(257) = .true.
      endif
      if (I_ikef .gt. 0) then
         L_(257) = .false.
      endif
      if(L_(18))then
          I_ikef = 0
          L_ekef = .false.
          L_(257) = .false.
      endif
C FDB60_logic.fgi(1297, 783):���������� ������� ���������,20FDB60EC005
      if(L_ekef) then
          if (L_(257)) then
              I_ikef = 01
              L_elef = .true.
              L_(270) = .false.
          endif
          if (L_(258)) then
              L_(270) = .true.
              L_elef = .false.
          endif
          if (I_ikef.ne.01) then
              L_elef = .false.
              L_(270) = .false.
          endif
      else
          L_(270) = .false.
      endif
C FDB60_logic.fgi(1297, 766):��� ������� ���������,20FDB60EC005
      if(L_ekef) then
          if (L_(270)) then
              I_ikef = 02
              L_ukef = .true.
              L_(271) = .false.
          endif
          if (L_okef) then
              L_(271) = .true.
              L_ukef = .false.
          endif
          if (I_ikef.ne.02) then
              L_ukef = .false.
              L_(271) = .false.
          endif
      else
          L_(271) = .false.
      endif
C FDB60_logic.fgi(1297, 742):��� ������� ���������,20FDB60EC005
      if(L_ekef) then
          if (L_(271)) then
              I_ikef = 03
              L_ifef = .true.
              L_(262) = .false.
          endif
          if (L_efef) then
              L_(262) = .true.
              L_ifef = .false.
          endif
          if (I_ikef.ne.03) then
              L_ifef = .false.
              L_(262) = .false.
          endif
      else
          L_(262) = .false.
      endif
C FDB60_logic.fgi(1297, 724):��� ������� ���������,20FDB60EC005
      if(L_ekef) then
          if (L_(262)) then
              I_ikef = 04
              L_ufef = .true.
              L_(263) = .false.
          endif
          if (L_ofef) then
              L_(263) = .true.
              L_ufef = .false.
          endif
          if (I_ikef.ne.04) then
              L_ufef = .false.
              L_(263) = .false.
          endif
      else
          L_(263) = .false.
      endif
C FDB60_logic.fgi(1297, 706):��� ������� ���������,20FDB60EC005
      if(L_ekef) then
          if (L_(263)) then
              I_ikef = 05
              L_udef = .true.
              L_(252) = .false.
          endif
          if (L_(253)) then
              L_(252) = .true.
              L_udef = .false.
          endif
          if (I_ikef.ne.05) then
              L_udef = .false.
              L_(252) = .false.
          endif
      else
          L_(252) = .false.
      endif
C FDB60_logic.fgi(1297, 687):��� ������� ���������,20FDB60EC005
      if(L_ekef) then
          if (L_(252)) then
              I_ikef = 06
              L_edef = .true.
              L_ubef = .false.
          endif
          if (L_adef) then
              L_ubef = .true.
              L_edef = .false.
          endif
          if (I_ikef.ne.06) then
              L_edef = .false.
              L_ubef = .false.
          endif
      else
          L_ubef = .false.
      endif
C FDB60_logic.fgi(1297, 655):��� ������� ���������,20FDB60EC005
      if(L_ekef) then
          if (L_ubef) then
              I_ikef = 07
              L_obef = .true.
              L_(239) = .false.
          endif
          if (L_otef) then
              L_(239) = .true.
              L_obef = .false.
          endif
          if (I_ikef.ne.07) then
              L_obef = .false.
              L_(239) = .false.
          endif
      else
          L_(239) = .false.
      endif
C FDB60_logic.fgi(1297, 632):��� ������� ���������,20FDB60EC005
      if(L_ekef) then
          if (L_(239)) then
              I_ikef = 08
              L_ibef = .true.
              L_(234) = .false.
          endif
          if (L_(235)) then
              L_(234) = .true.
              L_ibef = .false.
          endif
          if (I_ikef.ne.08) then
              L_ibef = .false.
              L_(234) = .false.
          endif
      else
          L_(234) = .false.
      endif
C FDB60_logic.fgi(1297, 616):��� ������� ���������,20FDB60EC005
      if(L_ekef) then
          if (L_(234)) then
              I_ikef = 09
              L_oxaf = .true.
              L_(227) = .false.
          endif
          if (L_(228)) then
              L_(227) = .true.
              L_oxaf = .false.
          endif
          if (I_ikef.ne.09) then
              L_oxaf = .false.
              L_(227) = .false.
          endif
      else
          L_(227) = .false.
      endif
C FDB60_logic.fgi(1297, 591):��� ������� ���������,20FDB60EC005
      if(L_ekef) then
          if (L_(227)) then
              I_ikef = 10
              L_axaf = .true.
              L_(221) = .false.
          endif
          if (L_ikif) then
              L_(221) = .true.
              L_axaf = .false.
          endif
          if (I_ikef.ne.10) then
              L_axaf = .false.
              L_(221) = .false.
          endif
      else
          L_(221) = .false.
      endif
C FDB60_logic.fgi(1297, 571):��� ������� ���������,20FDB60EC005
      if(L_ekef) then
          if (L_(221)) then
              I_ikef = 11
              L_ovaf = .true.
              L_(217) = .false.
          endif
          if (L_otef) then
              L_(217) = .true.
              L_ovaf = .false.
          endif
          if (I_ikef.ne.11) then
              L_ovaf = .false.
              L_(217) = .false.
          endif
      else
          L_(217) = .false.
      endif
C FDB60_logic.fgi(1297, 542):��� ������� ���������,20FDB60EC005
      if(L_ekef) then
          if (L_(217)) then
              I_ikef = 12
              L_evaf = .true.
              L_(212) = .false.
          endif
          if (L_(213)) then
              L_(212) = .true.
              L_evaf = .false.
          endif
          if (I_ikef.ne.12) then
              L_evaf = .false.
              L_(212) = .false.
          endif
      else
          L_(212) = .false.
      endif
C FDB60_logic.fgi(1297, 512):��� ������� ���������,20FDB60EC005
      if(L_ekef) then
          if (L_(212)) then
              I_ikef = 13
              L_otaf = .true.
              L_(206) = .false.
          endif
          if (L_ikif) then
              L_(206) = .true.
              L_otaf = .false.
          endif
          if (I_ikef.ne.13) then
              L_otaf = .false.
              L_(206) = .false.
          endif
      else
          L_(206) = .false.
      endif
C FDB60_logic.fgi(1297, 486):��� ������� ���������,20FDB60EC005
      if(L_ekef) then
          if (L_(206)) then
              I_ikef = 14
              L_etaf = .true.
              L_(202) = .false.
          endif
          if (L_otef) then
              L_(202) = .true.
              L_etaf = .false.
          endif
          if (I_ikef.ne.14) then
              L_etaf = .false.
              L_(202) = .false.
          endif
      else
          L_(202) = .false.
      endif
C FDB60_logic.fgi(1297, 460):��� ������� ���������,20FDB60EC005
      if(L_ekef) then
          if (L_(202)) then
              I_ikef = 15
              L_ataf = .true.
              L_(195) = .false.
          endif
          if (L_ekif) then
              L_(195) = .true.
              L_ataf = .false.
          endif
          if (I_ikef.ne.15) then
              L_ataf = .false.
              L_(195) = .false.
          endif
      else
          L_(195) = .false.
      endif
C FDB60_logic.fgi(1297, 433):��� ������� ���������,20FDB60EC005
      if(L_ekef) then
          if (L_(195)) then
              I_ikef = 16
              L_usaf = .true.
              L_(193) = .false.
          endif
          if (L_(194)) then
              L_(193) = .true.
              L_usaf = .false.
          endif
          if (I_ikef.ne.16) then
              L_usaf = .false.
              L_(193) = .false.
          endif
      else
          L_(193) = .false.
      endif
C FDB60_logic.fgi(1297, 415):��� ������� ���������,20FDB60EC005
      if(L_ekef) then
          if (L_(193)) then
              I_ikef = 17
              L_osaf = .true.
              L_esaf = .false.
          endif
          if (L_ikif) then
              L_esaf = .true.
              L_osaf = .false.
          endif
          if (I_ikef.ne.17) then
              L_osaf = .false.
              L_esaf = .false.
          endif
      else
          L_esaf = .false.
      endif
C FDB60_logic.fgi(1297, 388):��� ������� ���������,20FDB60EC005
      if (.not.L_eraf.and.(L_elaf.or.L_esaf)) then
          I_iraf = 0
          L_eraf = .true.
          L_(183) = .true.
      endif
      if (I_iraf .gt. 0) then
         L_(183) = .false.
      endif
      if(L_(127))then
          I_iraf = 0
          L_eraf = .false.
          L_(183) = .false.
      endif
C FDB60_logic.fgi( 370, 325):���������� ������� ���������,20FDB60EC006
      L_alaf=(L_(183).or.L_alaf).and..not.(L_ukaf)
      L_(126)=.not.L_alaf
C FDB60_logic.fgi( 419, 318):RS �������
      L_okaf=L_alaf
C FDB60_logic.fgi( 447, 320):������,FDB50_MOUNT_IS_WATCHING
C sav1=L_(353)
      L_(353) = L_(354).AND.(.NOT.L_okaf)
C FDB60_logic.fgi( 342, 799):recalc:�
C if(sav1.ne.L_(353) .and. try265.gt.0) goto 265
C sav1=L_(122)
      L_(122) = L_ufaf.AND.L_akaf.AND.L_okaf
C FDB60_logic.fgi( 575, 306):recalc:�
C if(sav1.ne.L_(122) .and. try267.gt.0) goto 267
      if(L_eraf) then
          if (L_(183)) then
              I_iraf = 01
              L_oraf = .true.
              L_(184) = .false.
          endif
          if (L_otef) then
              L_(184) = .true.
              L_oraf = .false.
          endif
          if (I_iraf.ne.01) then
              L_oraf = .false.
              L_(184) = .false.
          endif
      else
          L_(184) = .false.
      endif
C FDB60_logic.fgi( 370, 300):��� ������� ���������,20FDB60EC006
      if(L_eraf) then
          if (L_(184)) then
              I_iraf = 02
              L_araf = .true.
              L_(178) = .false.
          endif
          if (L_(179)) then
              L_(178) = .true.
              L_araf = .false.
          endif
          if (I_iraf.ne.02) then
              L_araf = .false.
              L_(178) = .false.
          endif
      else
          L_(178) = .false.
      endif
C FDB60_logic.fgi( 370, 270):��� ������� ���������,20FDB60EC006
      if(L_eraf) then
          if (L_(178)) then
              I_iraf = 03
              L_ipaf = .true.
              L_epaf = .false.
          endif
          if (L_ikif) then
              L_epaf = .true.
              L_ipaf = .false.
          endif
          if (I_iraf.ne.03) then
              L_ipaf = .false.
              L_epaf = .false.
          endif
      else
          L_epaf = .false.
      endif
C FDB60_logic.fgi( 370, 244):��� ������� ���������,20FDB60EC006
      if(L_eraf) then
          if (L_epaf) then
              I_iraf = 04
              L_apaf = .true.
              L_(169) = .false.
          endif
          if (L_otef) then
              L_(169) = .true.
              L_apaf = .false.
          endif
          if (I_iraf.ne.04) then
              L_apaf = .false.
              L_(169) = .false.
          endif
      else
          L_(169) = .false.
      endif
C FDB60_logic.fgi( 370, 218):��� ������� ���������,20FDB60EC006
      if(L_eraf) then
          if (L_(169)) then
              I_iraf = 05
              L_umaf = .true.
              L_(164) = .false.
          endif
          if (L_(165)) then
              L_(164) = .true.
              L_umaf = .false.
          endif
          if (I_iraf.ne.05) then
              L_umaf = .false.
              L_(164) = .false.
          endif
      else
          L_(164) = .false.
      endif
C FDB60_logic.fgi( 370, 195):��� ������� ���������,20FDB60EC006
      if(L_eraf) then
          if (L_(164)) then
              I_iraf = 06
              L_emaf = .true.
              L_amaf = .false.
          endif
          if (L_ikif) then
              L_amaf = .true.
              L_emaf = .false.
          endif
          if (I_iraf.ne.06) then
              L_emaf = .false.
              L_amaf = .false.
          endif
      else
          L_amaf = .false.
      endif
C FDB60_logic.fgi( 370, 169):��� ������� ���������,20FDB60EC006
      if(L_amaf) then
         I_iraf=0
         L_eraf=.false.
      endif
C FDB60_logic.fgi( 370, 149):����� ������� ���������,20FDB60EC006
      L_(158)=.not.L_(349) .and.L_emaf
      L_(156)=L_emaf.and..not.L_(157)
C FDB60_logic.fgi( 419, 162):��������� ������
      L_(130)=.not.L_(349) .and.L_emaf
      L_(128)=L_emaf.and..not.L_(129)
C FDB60_logic.fgi( 414, 170):��������� ������
      L_(163)=.not.L_(349) .and.L_umaf
      L_(161)=L_umaf.and..not.L_(162)
C FDB60_logic.fgi( 425, 196):��������� ������
      L_(168)=.not.L_(349) .and.L_apaf
      L_(166)=L_apaf.and..not.L_(167)
C FDB60_logic.fgi( 420, 218):��������� ������
      L_(172)=.not.L_(349) .and.L_ipaf
      L_(170)=L_ipaf.and..not.L_(171)
C FDB60_logic.fgi( 419, 238):��������� ������
      L_(133)=.not.L_(349) .and.L_ipaf
      L_(131)=L_ipaf.and..not.L_(132)
C FDB60_logic.fgi( 415, 244):��������� ������
      L_(177)=.not.L_(349) .and.L_araf
      L_(175)=L_araf.and..not.L_(176)
C FDB60_logic.fgi( 426, 270):��������� ������
      L_(182)=.not.L_(349) .and.L_oraf
      L_(180)=L_oraf.and..not.L_(181)
C FDB60_logic.fgi( 415, 300):��������� ������
      if(L_esaf) then
         I_ikef=0
         L_ekef=.false.
      endif
C FDB60_logic.fgi(1297, 368):����� ������� ���������,20FDB60EC005
      L_(192)=.not.L_(349) .and.L_osaf
      L_(190)=L_osaf.and..not.L_(191)
C FDB60_logic.fgi(1342, 388):��������� ������
      L_isaf=L_osaf
C FDB60_logic.fgi(1347, 382):������,FDB50AE403_UNCATCH_BOAT
      L_(187)=.not.L_(349) .and.L_usaf
      L_(185)=L_usaf.and..not.L_(186)
C FDB60_logic.fgi(1348, 416):��������� ������
      L_(198)=.not.L_(349) .and.L_ataf
      L_(196)=L_ataf.and..not.L_(197)
C FDB60_logic.fgi(1343, 434):��������� ������
      L_(201)=.not.L_(349) .and.L_etaf
      L_(199)=L_etaf.and..not.L_(200)
C FDB60_logic.fgi(1347, 460):��������� ������
      L_itaf=L_otaf
C FDB60_logic.fgi(1353, 472):������,FDB50AE403_WEIGHT_1
      L_(205)=.not.L_(349) .and.L_otaf
      L_(203)=L_otaf.and..not.L_(204)
C FDB60_logic.fgi(1346, 480):��������� ������
      L_(136)=.not.L_(349) .and.L_otaf
      L_(134)=L_otaf.and..not.L_(135)
C FDB60_logic.fgi(1339, 486):��������� ������
      L_(211)=.not.L_(349) .and.L_evaf
      L_(209)=L_evaf.and..not.L_(210)
C FDB60_logic.fgi(1344, 512):��������� ������
      L_(216)=.not.L_(349) .and.L_ovaf
      L_(214)=L_ovaf.and..not.L_(215)
C FDB60_logic.fgi(1342, 542):��������� ������
      L_ivaf=L_ovaf
C FDB60_logic.fgi(1347, 534):������,FDB50AE403_CATCH_BOAT_1
      L_(220)=.not.L_(349) .and.L_axaf
      L_(218)=L_axaf.and..not.L_(219)
C FDB60_logic.fgi(1345, 564):��������� ������
      L_uvaf=L_axaf
C FDB60_logic.fgi(1353, 557):������,FDB50AE403_UNMOUNT
      L_(139)=.not.L_(349) .and.L_axaf
      L_(137)=L_axaf.and..not.L_(138)
C FDB60_logic.fgi(1344, 572):��������� ������
      L_(226)=.not.L_(349) .and.L_oxaf
      L_(224)=L_oxaf.and..not.L_(225)
C FDB60_logic.fgi(1349, 592):��������� ������
      L_(233)=.not.L_(349) .and.L_ibef
      L_(231)=L_ibef.and..not.L_(232)
C FDB60_logic.fgi(1353, 616):��������� ������
      L_(238)=.not.L_(349) .and.L_obef
      L_(236)=L_obef.and..not.L_(237)
C FDB60_logic.fgi(1345, 632):��������� ������
      L_(242)=.not.L_(349) .and.L_edef
      L_(240)=L_edef.and..not.L_(241)
C FDB60_logic.fgi(1353, 656):��������� ������
      iv2=0
      if(L_(242)) iv2=ibset(iv2,0)
C FDB60_logic.fgi(1353, 656):������-�������: ������� ������ ������/����������/����� ���������
      L_(251)=.not.L_(349) .and.L_udef
      L_(249)=L_udef.and..not.L_(250)
C FDB60_logic.fgi(1344, 680):��������� ������
      L_(248)=.not.L_(349) .and.L_udef
      L_(246)=L_udef.and..not.L_(247)
C FDB60_logic.fgi(1345, 688):��������� ������
      L_(266)=.not.L_(349) .and.L_ufef
      L_(264)=L_ufef.and..not.L_(265)
C FDB60_logic.fgi(1344, 706):��������� ������
      iv2=0
      if(L_(266)) iv2=ibset(iv2,0)
C FDB60_logic.fgi(1344, 706):������-�������: ������� ������ ������/����������/����� ���������
      L_(261)=.not.L_(349) .and.L_ifef
      L_(259)=L_ifef.and..not.L_(260)
C FDB60_logic.fgi(1344, 724):��������� ������
      iv2=0
      if(L_(261)) iv2=ibset(iv2,0)
C FDB60_logic.fgi(1344, 724):������-�������: ������� ������ ������/����������/����� ���������
      L_(269)=.not.L_(349) .and.L_ukef
      L_(267)=L_ukef.and..not.L_(268)
C FDB60_logic.fgi(1344, 742):��������� ������
      iv2=0
      if(L_(269)) iv2=ibset(iv2,0)
C FDB60_logic.fgi(1344, 742):������-�������: ������� ������ ������/����������/����� ���������
      L_akef=L_ukef
C FDB60_logic.fgi(1353, 735):������,FDB50AE506_PUSH
      L_(274)=.not.L_(349) .and.L_elef
      L_(272)=L_elef.and..not.L_(273)
C FDB60_logic.fgi(1345, 766):��������� ������
      iv2=0
      if(L_(274)) iv2=ibset(iv2,0)
C FDB60_logic.fgi(1345, 766):������-�������: ������� ������ ������/����������/����� ���������
      L_(256)=.not.L_(349) .and.L_elef
      L_(254)=L_elef.and..not.L_(255)
C FDB60_logic.fgi(1353, 760):��������� ������
      iv2=0
      if(L_(256)) iv2=ibset(iv2,0)
      if(L_(233)) iv2=ibset(iv2,1)
C FDB60_logic.fgi(1353, 760):������-�������: ������� ������ ������/����������/����� ���������
      if(L_akud) then
         I_epud=0
         L_apud=.false.
      endif
C FDB60_logic.fgi(1051, 532):����� ������� ���������,20FDB60EC004
      L_(31)=.not.L_(349) .and.L_ikud
      L_(29)=L_ikud.and..not.L_(30)
C FDB60_logic.fgi(1098, 554):��������� ������
      iv2=0
      if(L_(31)) iv2=ibset(iv2,0)
C FDB60_logic.fgi(1098, 554):������-�������: ������� ������ ������/����������/����� ���������
      L_(27)=.not.L_(349) .and.L_ufud
      L_(25)=L_ufud.and..not.L_(26)
C FDB60_logic.fgi(1098, 572):��������� ������
      iv2=0
      if(L_(27)) iv2=ibset(iv2,0)
C FDB60_logic.fgi(1098, 572):������-�������: ������� ������ ������/����������/����� ���������
      L_(34)=.not.L_(349) .and.L_alud
      L_(32)=L_alud.and..not.L_(33)
C FDB60_logic.fgi(1098, 590):��������� ������
      iv2=0
      if(L_(34)) iv2=ibset(iv2,0)
C FDB60_logic.fgi(1098, 590):������-�������: ������� ������ ������/����������/����� ���������
      L_okud=L_alud
C FDB60_logic.fgi(1107, 583):������,FDB50AE505_PUSH
      L_(41)=.not.L_(349) .and.L_ilud
      L_(39)=L_ilud.and..not.L_(40)
C FDB60_logic.fgi(1099, 608):��������� ������
      iv2=0
      if(L_(41)) iv2=ibset(iv2,0)
C FDB60_logic.fgi(1099, 608):������-�������: ������� ������ ������/����������/����� ���������
      L_efod=L_ilud
C FDB60_logic.fgi(1108, 602):������,20FDB50AA120YA22
      L_alod=L_amod
C FDB60_logic.fgi(1108, 628):������,20FDB50AA120YA21
      L_imod=L_umod
C FDB60_logic.fgi(1108, 651):������,20FDB50AA121YA22
      L_emod=L_umod
C FDB60_logic.fgi(1108, 647):������,20FDB50AN006_uluoff
      L_epod=L_irod
C FDB60_logic.fgi(1108, 672):������,20FDB50AA121YA21
      L_apod=L_irod
C FDB60_logic.fgi(1108, 668):������,20FDB50AN006_uluon
      Call FDB60_1(ext_deltat)
      End
      Subroutine FDB60_1(ext_deltat)
      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      Include 'FDB60.fh'

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      L_(49)=.not.L_(349) .and.L_emud
      L_(47)=L_emud.and..not.L_(48)
C FDB60_logic.fgi(1099, 690):��������� ������
      iv2=0
      if(L_(49)) iv2=ibset(iv2,0)
C FDB60_logic.fgi(1099, 690):������-�������: ������� ������ ������/����������/����� ���������
      L_(44)=.not.L_(349) .and.L_ulud
      L_(42)=L_ulud.and..not.L_(43)
C FDB60_logic.fgi(1099, 708):��������� ������
      iv2=0
      if(L_(44)) iv2=ibset(iv2,0)
C FDB60_logic.fgi(1099, 708):������-�������: ������� ������ ������/����������/����� ���������
      L_(52)=.not.L_(349) .and.L_umud
      L_(50)=L_umud.and..not.L_(51)
C FDB60_logic.fgi(1099, 726):��������� ������
      iv2=0
      if(L_(52)) iv2=ibset(iv2,0)
C FDB60_logic.fgi(1099, 726):������-�������: ������� ������ ������/����������/����� ���������
      L_imud=L_umud
C FDB60_logic.fgi(1108, 720):������,FDB50AE504_PUSH
      L_(59)=.not.L_(349) .and.L_opud
      L_(57)=L_opud.and..not.L_(58)
C FDB60_logic.fgi(1099, 744):��������� ������
      iv2=0
      if(L_(59)) iv2=ibset(iv2,0)
C FDB60_logic.fgi(1099, 744):������-�������: ������� ������ ������/����������/����� ���������
      L_ofod=L_opud
C FDB60_logic.fgi(1108, 739):������,20FDB50AA119YA22
      L_okod=L_ukod
C FDB60_logic.fgi(1108, 763):������,20FDB50AA119YA21
      if(L_upud) then
         I_urud=0
         L_orud=.false.
      endif
C FDB60_logic.fgi( 821, 717):����� ������� ���������,20FDB60EC003
      L_(62)=.not.L_(349) .and.L_erud
      L_(60)=L_erud.and..not.L_(61)
C FDB60_logic.fgi( 870, 740):��������� ������
      iv2=0
      if(L_(62)) iv2=ibset(iv2,0)
C FDB60_logic.fgi( 870, 740):������-�������: ������� ������ ������/����������/����� ���������
      L_(65)=.not.L_(349) .and.L_esud
      L_(63)=L_esud.and..not.L_(64)
C FDB60_logic.fgi( 870, 762):��������� ������
      iv2=0
      if(L_(65)) iv2=ibset(iv2,0)
C FDB60_logic.fgi( 870, 762):������-�������: ������� ������ ������/����������/����� ���������
      L_irud=L_esud
C FDB60_logic.fgi( 877, 754):������,FDB50AE503_PUSH
      if(L_isud) then
         I_abaf=0
         L_uxud=.false.
      endif
C FDB60_logic.fgi( 614, 544):����� ������� ���������,20FDB60EC002
      L_(70)=.not.L_(349) .and.L_usud
      L_(68)=L_usud.and..not.L_(69)
C FDB60_logic.fgi( 662, 566):��������� ������
      iv2=0
      if(L_(70)) iv2=ibset(iv2,0)
C FDB60_logic.fgi( 662, 566):������-�������: ������� ������ ������/����������/����� ���������
      L_(73)=.not.L_(349) .and.L_etud
      L_(71)=L_etud.and..not.L_(72)
C FDB60_logic.fgi( 661, 582):��������� ������
      iv2=0
      if(L_(73)) iv2=ibset(iv2,0)
C FDB60_logic.fgi( 661, 582):������-�������: ������� ������ ������/����������/����� ���������
      L_(80)=.not.L_(349) .and.L_evud
      L_(78)=L_evud.and..not.L_(79)
C FDB60_logic.fgi( 661, 600):��������� ������
      iv2=0
      if(L_(80)) iv2=ibset(iv2,0)
C FDB60_logic.fgi( 661, 600):������-�������: ������� ������ ������/����������/����� ���������
      L_utud=L_evud
C FDB60_logic.fgi( 670, 592):������,FDB50AE502_PUSH
      L_(87)=.not.L_(349) .and.L_ovud
      L_(85)=L_ovud.and..not.L_(86)
C FDB60_logic.fgi( 662, 620):��������� ������
      iv2=0
      if(L_(87)) iv2=ibset(iv2,0)
C FDB60_logic.fgi( 662, 620):������-�������: ������� ������ ������/����������/����� ���������
      L_orod=L_ovud
C FDB60_logic.fgi( 669, 614):������,20FDB50AA115YA22
      L_osod=L_avod
C FDB60_logic.fgi( 673, 653):������,20FDB50AA113YA22
      L_isod=L_avod
C FDB60_logic.fgi( 673, 649):������,20FDB50AA116YA22
      L_esod=L_avod
C FDB60_logic.fgi( 673, 645):������,20FDB50AN003_uluoff
      L_asod=L_avod
C FDB60_logic.fgi( 673, 639):������,20FDB50AA115YA21
      L_ovod=L_ibud
C FDB60_logic.fgi( 673, 679):������,20FDB50AA113YA21
      L_evod=L_ibud
C FDB60_logic.fgi( 673, 671):������,20FDB50AN003_uluon
      L_ivod=L_ibud
C FDB60_logic.fgi( 673, 675):������,20FDB50AA116YA21
      L_(92)=.not.L_(349) .and.L_axud
      L_(90)=L_axud.and..not.L_(91)
C FDB60_logic.fgi( 662, 698):��������� ������
      iv2=0
      if(L_(92)) iv2=ibset(iv2,0)
C FDB60_logic.fgi( 662, 698):������-�������: ������� ������ ������/����������/����� ���������
      L_(77)=.not.L_(349) .and.L_otud
      L_(75)=L_otud.and..not.L_(76)
C FDB60_logic.fgi( 661, 716):��������� ������
      iv2=0
      if(L_(77)) iv2=ibset(iv2,0)
C FDB60_logic.fgi( 661, 716):������-�������: ������� ������ ������/����������/����� ���������
      L_(95)=.not.L_(349) .and.L_oxud
      L_(93)=L_oxud.and..not.L_(94)
C FDB60_logic.fgi( 661, 734):��������� ������
      iv2=0
      if(L_(95)) iv2=ibset(iv2,0)
C FDB60_logic.fgi( 661, 734):������-�������: ������� ������ ������/����������/����� ���������
      L_exud=L_oxud
C FDB60_logic.fgi( 670, 727):������,FDB50AE501_PUSH
      L_(102)=.not.L_(349) .and.L_ibaf
      L_(100)=L_ibaf.and..not.L_(101)
C FDB60_logic.fgi( 662, 752):��������� ������
      iv2=0
      if(L_(102)) iv2=ibset(iv2,0)
C FDB60_logic.fgi( 662, 752):������-�������: ������� ������ ������/����������/����� ���������
      L_axod=L_ibaf
C FDB60_logic.fgi( 671, 747):������,20FDB50AA114YA22
      L_uvod=L_efud
C FDB60_logic.fgi( 672, 770):������,20FDB50AA114YA21
      if(L_akif) then
         I_ilif=0
         L_alif=.false.
      endif
C FDB60_logic.fgi( 377, 454):����� ������� ���������,20FDB60EC001
      L_(277)=.not.L_(349) .and.L_olef
      L_(275)=L_olef.and..not.L_(276)
C FDB60_logic.fgi( 425, 472):��������� ������
      iv2=0
      if(L_(277)) iv2=ibset(iv2,0)
C FDB60_logic.fgi( 425, 472):������-�������: ������� ������ ������/����������/����� ���������
      L_(281)=.not.L_(349) .and.L_amef
      L_(279)=L_amef.and..not.L_(280)
C FDB60_logic.fgi( 425, 486):��������� ������
      iv2=0
      if(L_(281)) iv2=ibset(iv2,0)
C FDB60_logic.fgi( 425, 486):������-�������: ������� ������ ������/����������/����� ���������
      L_(284)=.not.L_(349) .and.L_omef
      L_(282)=L_omef.and..not.L_(283)
C FDB60_logic.fgi( 424, 504):��������� ������
      iv2=0
      if(L_(284)) iv2=ibset(iv2,0)
C FDB60_logic.fgi( 424, 504):������-�������: ������� ������ ������/����������/����� ���������
      L_emef=L_omef
C FDB60_logic.fgi( 433, 497):������,FDB50AE500_PUSH
      L_(288)=.not.L_(349) .and.L_apef
      L_(286)=L_apef.and..not.L_(287)
C FDB60_logic.fgi( 424, 520):��������� ������
      iv2=0
      if(L_(288)) iv2=ibset(iv2,0)
C FDB60_logic.fgi( 424, 520):������-�������: ������� ������ ������/����������/����� ���������
      L_(142)=.not.L_(349) .and.L_apef
      L_(140)=L_apef.and..not.L_(141)
C FDB60_logic.fgi( 424, 526):��������� ������
      L_(294)=.not.L_(349) .and.L_opef
      L_(292)=L_opef.and..not.L_(293)
C FDB60_logic.fgi( 425, 544):��������� ������
      L_(299)=.not.L_(349) .and.L_upef
      L_(297)=L_upef.and..not.L_(298)
C FDB60_logic.fgi( 425, 566):��������� ������
      L_(303)=.not.L_(349) .and.L_eref
      L_(301)=L_eref.and..not.L_(302)
C FDB60_logic.fgi( 425, 588):��������� ������
      L_(145)=.not.L_(349) .and.L_eref
      L_(143)=L_eref.and..not.L_(144)
C FDB60_logic.fgi( 424, 594):��������� ������
      L_(308)=.not.L_(349) .and.L_uref
      L_(306)=L_uref.and..not.L_(307)
C FDB60_logic.fgi( 429, 614):��������� ������
      L_(313)=.not.L_(349) .and.L_asef
      L_(311)=L_asef.and..not.L_(312)
C FDB60_logic.fgi( 427, 632):��������� ������
      L_(317)=.not.L_(349) .and.L_isef
      L_(315)=L_isef.and..not.L_(316)
C FDB60_logic.fgi( 426, 652):��������� ������
      L_(148)=.not.L_(349) .and.L_isef
      L_(146)=L_isef.and..not.L_(147)
C FDB60_logic.fgi( 419, 658):��������� ������
      L_(322)=.not.L_(349) .and.L_etef
      L_(320)=L_etef.and..not.L_(321)
C FDB60_logic.fgi( 423, 684):��������� ������
      L_(327)=.not.L_(349) .and.L_utef
      L_(325)=L_utef.and..not.L_(326)
C FDB60_logic.fgi( 423, 714):��������� ������
      L_itef=L_utef
C FDB60_logic.fgi( 429, 708):������,FDB50AE403_CATCH_BOAT
      L_(350)=.not.L_(349) .and.L_ukif
      L_(347)=L_ukif.and..not.L_(348)
C FDB60_logic.fgi( 419, 730):��������� ������
      iv2=0
      if(L_(350)) iv2=ibset(iv2,0)
C FDB60_logic.fgi( 419, 730):������-�������: ������� ������ ������/����������/����� ���������
      iv2=0
      if(L_(350)) iv2=ibset(iv2,0)
      if(L_(198)) iv2=ibset(iv2,1)
C FDB60_logic.fgi( 419, 730):������-�������: ������� ������ ������/����������/����� ���������
      L_(332)=.not.L_(349) .and.L_okif
      L_(330)=L_okif.and..not.L_(331)
C FDB60_logic.fgi( 419, 748):��������� ������
      L_(154)=.not.L_(349) .and.L_ulaf
      L_(152)=L_ulaf.and..not.L_(153)
C FDB60_logic.fgi( 433, 766):��������� ������
      iv2=0
      if(L_(294)) iv2=ibset(iv2,0)
      if(L_(226)) iv2=ibset(iv2,1)
      if(L_(211)) iv2=ibset(iv2,2)
      if(L_(187)) iv2=ibset(iv2,3)
      if(L_(163)) iv2=ibset(iv2,4)
      if(L_(154)) iv2=ibset(iv2,5)
C FDB60_logic.fgi( 425, 544):������-�������: ������� ������ ������/����������/����� ���������
      L_(125)=.not.L_(349) .and.L_ulaf
      L_(123)=L_ulaf.and..not.L_(124)
C FDB60_logic.fgi( 425, 760):��������� ������
      L_ubaf=L_ekaf
C FDB60_logic.fgi( 636, 203):������,FDB50AE403_MOUNT_UNCATCH
      if(L_ekaf) then
         I_afaf=0
         L_udaf=.false.
      endif
C FDB60_logic.fgi( 582, 184):����� ������� ���������,20FDB60EC007
      L_(109)=.not.L_(349) .and.L_adaf
      L_(107)=L_adaf.and..not.L_(108)
C FDB60_logic.fgi( 631, 210):��������� ������
      iv2=0
      if(L_(332)) iv2=ibset(iv2,0)
      if(L_(317)) iv2=ibset(iv2,1)
      if(L_(303)) iv2=ibset(iv2,2)
      if(L_(248)) iv2=ibset(iv2,3)
      if(L_(220)) iv2=ibset(iv2,4)
      if(L_(205)) iv2=ibset(iv2,5)
      if(L_(192)) iv2=ibset(iv2,6)
      if(L_(172)) iv2=ibset(iv2,7)
      if(L_(158)) iv2=ibset(iv2,8)
      if(L_(109)) iv2=ibset(iv2,9)
C FDB60_logic.fgi( 419, 748):������-�������: ������� ������ ������/����������/����� ���������
      L_(106)=.not.L_(349) .and.L_adaf
      L_(104)=L_adaf.and..not.L_(105)
C FDB60_logic.fgi( 627, 218):��������� ������
      iv2=0
      if(L_(332)) iv2=ibset(iv2,0)
      if(L_(148)) iv2=ibset(iv2,1)
      if(L_(145)) iv2=ibset(iv2,2)
      if(L_(142)) iv2=ibset(iv2,3)
      if(L_(139)) iv2=ibset(iv2,4)
      if(L_(136)) iv2=ibset(iv2,5)
      if(L_(133)) iv2=ibset(iv2,6)
      if(L_(130)) iv2=ibset(iv2,7)
      if(L_(106)) iv2=ibset(iv2,8)
C FDB60_logic.fgi( 419, 748):������-�������: ������� ������ ������/����������/����� ���������
      L_(114)=.not.L_(349) .and.L_odaf
      L_(112)=L_odaf.and..not.L_(113)
C FDB60_logic.fgi( 638, 244):��������� ������
      iv2=0
      if(L_(322)) iv2=ibset(iv2,0)
      if(L_(308)) iv2=ibset(iv2,1)
      if(L_(251)) iv2=ibset(iv2,2)
      if(L_(177)) iv2=ibset(iv2,3)
      if(L_(114)) iv2=ibset(iv2,4)
C FDB60_logic.fgi( 423, 684):������-�������: ������� ������ ������/����������/����� ���������
      L_(119)=.not.L_(349) .and.L_efaf
      L_(117)=L_efaf.and..not.L_(118)
C FDB60_logic.fgi( 627, 274):��������� ������
      iv2=0
      if(L_(327)) iv2=ibset(iv2,0)
      if(L_(313)) iv2=ibset(iv2,1)
      if(L_(299)) iv2=ibset(iv2,2)
      if(L_(238)) iv2=ibset(iv2,3)
      if(L_(216)) iv2=ibset(iv2,4)
      if(L_(201)) iv2=ibset(iv2,5)
      if(L_(182)) iv2=ibset(iv2,6)
      if(L_(168)) iv2=ibset(iv2,7)
      if(L_(125)) iv2=ibset(iv2,8)
      if(L_(119)) iv2=ibset(iv2,9)
C FDB60_logic.fgi( 423, 714):������-�������: ������� ������ ������/����������/����� ���������
      End
