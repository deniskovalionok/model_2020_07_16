      Subroutine FDA20(ext_deltat)
      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      Include 'FDA20.fh'

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      Call FDA20_ConIn
      ! ��������� ������ ����� FDA20 ����� FDA20_SP1
      L_(325)=.true.
C FDA20_SP1.fgi( 498,  15):pre: ����� 0
      R_(64)=R0_aku
C FDA20_SP2_UVR.fgi( 160,  42):pre: ������������  �� T
      R_(62)=R0_odu
C FDA20_SP2_UVR.fgi( 160,  22):pre: ������������  �� T
      R_(63)=R0_ofu
C FDA20_SP2_UVR.fgi(  45,  37):pre: �������� ��������� ������
      R_(79)=R0_ekad
C FDA20_SP2_UVR.fgi( 150, 227):pre: ������������  �� T
      R_(168)=R0_ifuf
C FDA20_SP2_UVR.fgi( 174, 740):pre: ������������  �� T
      R_(153)=R0_itef
C FDA20_SP2_UVR.fgi( 162, 740):pre: �������� ��������� ������
      R_(156)=R0_oxef
C FDA20_SP2_UVR.fgi( 241, 744):pre: ������������  �� T
      R_(35)=R0_ute
C FDA20_SP2_UVR.fgi( 170, 713):pre: ������������  �� T
      R_(34)=R0_ete
C FDA20_SP2_UVR.fgi( 174, 709):pre: ������������  �� T
      R_(31)=R0_ire
C FDA20_SP2_UVR.fgi( 170, 697):pre: ������������  �� T
      R_(30)=R0_upe
C FDA20_SP2_UVR.fgi( 174, 693):pre: ������������  �� T
      R_(29)=R0_epe
C FDA20_SP2_UVR.fgi( 170, 689):pre: ������������  �� T
      R_(28)=R0_ome
C FDA20_SP2_UVR.fgi( 174, 685):pre: ������������  �� T
      R_(27)=R0_ame
C FDA20_SP2_UVR.fgi( 170, 681):pre: ������������  �� T
      R_(32)=R0_ase
C FDA20_SP2_UVR.fgi( 174, 701):pre: ������������  �� T
      R_(33)=R0_ose
C FDA20_SP2_UVR.fgi( 170, 705):pre: ������������  �� T
      R_(36)=R0_ive
C FDA20_SP2_UVR.fgi( 174, 717):pre: ������������  �� T
      R_(155)=R0_axef
C FDA20_SP2_UVR.fgi( 241, 738):pre: ������������  �� T
      R_(166)=R0_odof
C FDA20_SP2_UVR.fgi( 170, 655):pre: ������������  �� T
      R_(165)=R0_ubof
C FDA20_SP2_UVR.fgi( 174, 651):pre: ������������  �� T
      R_(162)=R0_ivif
C FDA20_SP2_UVR.fgi( 170, 639):pre: ������������  �� T
      R_(161)=R0_otif
C FDA20_SP2_UVR.fgi( 174, 635):pre: ������������  �� T
      R_(160)=R0_usif
C FDA20_SP2_UVR.fgi( 170, 631):pre: ������������  �� T
      R_(159)=R0_asif
C FDA20_SP2_UVR.fgi( 174, 627):pre: ������������  �� T
      R_(158)=R0_erif
C FDA20_SP2_UVR.fgi( 170, 623):pre: ������������  �� T
      R_(163)=R0_exif
C FDA20_SP2_UVR.fgi( 174, 643):pre: ������������  �� T
      R_(164)=R0_abof
C FDA20_SP2_UVR.fgi( 170, 647):pre: ������������  �� T
      R_(167)=R0_ifof
C FDA20_SP2_UVR.fgi( 174, 659):pre: ������������  �� T
      R_(157)=R0_efif
C FDA20_SP2_UVR.fgi( 119, 616):pre: ������������  �� T
      R_(24)=R0_ake
C FDA20_SP2_UVR.fgi( 167, 594):pre: ������������  �� T
      R_(154)=R0_avef
C FDA20_SP2_UVR.fgi( 167, 600):pre: ������������  �� T
      R_(23)=R0_ife
C FDA20_SP2_UVR.fgi( 167, 582):pre: ������������  �� T
      R_(120)=R0_uxod
C FDA20_SP2_UVR.fgi( 167, 588):pre: ������������  �� T
      R_(193)=R0_ilek
C FDA20_SP1.fgi( 415, 800):pre: ������������  �� T
      R_(22)=R0_ude
C FDA20_SP2_UVR.fgi( 167, 570):pre: ������������  �� T
      R_(119)=R0_ovod
C FDA20_SP2_UVR.fgi( 167, 576):pre: ������������  �� T
      R_(192)=R0_ifek
C FDA20_SP1.fgi( 415, 771):pre: ������������  �� T
      R_(21)=R0_ede
C FDA20_SP2_UVR.fgi( 167, 558):pre: ������������  �� T
      R_(118)=R0_itod
C FDA20_SP2_UVR.fgi( 167, 564):pre: ������������  �� T
      R_(191)=R0_ibek
C FDA20_SP1.fgi( 415, 742):pre: ������������  �� T
      R_(115)=R0_efod
C FDA20_SP2_UVR.fgi( 174, 543):pre: ������������  �� T
      R_(114)=R0_odod
C FDA20_SP2_UVR.fgi( 170, 539):pre: ������������  �� T
      R_(113)=R0_adod
C FDA20_SP2_UVR.fgi( 174, 535):pre: ������������  �� T
      R_(112)=R0_ibod
C FDA20_SP2_UVR.fgi( 170, 531):pre: ������������  �� T
      R_(116)=R0_ufod
C FDA20_SP2_UVR.fgi( 170, 547):pre: ������������  �� T
      R_(117)=R0_ikod
C FDA20_SP2_UVR.fgi( 174, 551):pre: ������������  �� T
      R_(190)=R0_ovak
C FDA20_SP1.fgi( 415, 713):pre: ������������  �� T
      R_(16)=R0_ev
C FDA20_SP2_UVR.fgi( 169, 475):pre: ������������  �� T
      R_(96)=R0_ised
C FDA20_SP2_UVR.fgi( 169, 481):pre: ������������  �� T
      R_(108)=R0_itid
C FDA20_SP2_UVR.fgi( 169, 487):pre: ������������  �� T
      R_(109)=R0_evid
C FDA20_SP2_UVR.fgi( 169, 493):pre: ������������  �� T
      R_(20)=R0_ube
C FDA20_SP2_UVR.fgi( 143, 492):pre: �������� ��������� ������
      R_(110)=R0_uvid
C FDA20_SP2_UVR.fgi( 169, 499):pre: ������������  �� T
      R_(12)=R0_as
C FDA20_SP2_UVR.fgi( 123, 499):pre: �������� ������� ������
      R_(106)=R0_asid
C FDA20_SP2_UVR.fgi( 169, 461):pre: ������������  �� T
      R_(19)=R0_ebe
C FDA20_SP2_UVR.fgi( 143, 460):pre: �������� ��������� ������
      R_(15)=R0_ot
C FDA20_SP2_UVR.fgi( 169, 443):pre: ������������  �� T
      R_(95)=R0_ored
C FDA20_SP2_UVR.fgi( 169, 449):pre: ������������  �� T
      R_(105)=R0_erid
C FDA20_SP2_UVR.fgi( 169, 455):pre: ������������  �� T
      R_(107)=R0_osid
C FDA20_SP2_UVR.fgi( 169, 467):pre: ������������  �� T
      R_(11)=R0_ir
C FDA20_SP2_UVR.fgi( 123, 467):pre: �������� ������� ������
      R_(103)=R0_umid
C FDA20_SP2_UVR.fgi( 169, 430):pre: ������������  �� T
      R_(18)=R0_ox
C FDA20_SP2_UVR.fgi( 143, 429):pre: �������� ��������� ������
      R_(14)=R0_at
C FDA20_SP2_UVR.fgi( 169, 412):pre: ������������  �� T
      R_(94)=R0_uped
C FDA20_SP2_UVR.fgi( 169, 418):pre: ������������  �� T
      R_(102)=R0_amid
C FDA20_SP2_UVR.fgi( 169, 424):pre: ������������  �� T
      R_(104)=R0_ipid
C FDA20_SP2_UVR.fgi( 169, 436):pre: ������������  �� T
      R_(10)=R0_up
C FDA20_SP2_UVR.fgi( 123, 436):pre: �������� ������� ������
      R_(100)=R0_okid
C FDA20_SP2_UVR.fgi( 169, 399):pre: ������������  �� T
      R_(17)=R0_ax
C FDA20_SP2_UVR.fgi( 143, 398):pre: �������� ��������� ������
      R_(13)=R0_is
C FDA20_SP2_UVR.fgi( 169, 381):pre: ������������  �� T
      R_(93)=R0_aped
C FDA20_SP2_UVR.fgi( 169, 387):pre: ������������  �� T
      R_(99)=R0_ufid
C FDA20_SP2_UVR.fgi( 169, 393):pre: ������������  �� T
      R_(101)=R0_elid
C FDA20_SP2_UVR.fgi( 169, 405):pre: ������������  �� T
      R_(9)=R0_ep
C FDA20_SP2_UVR.fgi( 123, 405):pre: �������� ������� ������
      R_(83)=R0_uxad
C FDA20_SP2_UVR.fgi( 174, 304):pre: ������������  �� T
      R_(84)=R0_ibed
C FDA20_SP2_UVR.fgi( 174, 310):pre: ������������  �� T
      R_(85)=R0_aded
C FDA20_SP2_UVR.fgi( 174, 316):pre: ������������  �� T
      R_(86)=R0_oded
C FDA20_SP2_UVR.fgi( 174, 322):pre: ������������  �� T
      R_(87)=R0_efed
C FDA20_SP2_UVR.fgi( 174, 328):pre: ������������  �� T
      R_(88)=R0_ufed
C FDA20_SP2_UVR.fgi( 174, 334):pre: ������������  �� T
      R_(89)=R0_iked
C FDA20_SP2_UVR.fgi( 174, 340):pre: ������������  �� T
      R_(90)=R0_aled
C FDA20_SP2_UVR.fgi( 174, 346):pre: ������������  �� T
      R_(91)=R0_oled
C FDA20_SP2_UVR.fgi( 174, 352):pre: ������������  �� T
      R_(92)=R0_emed
C FDA20_SP2_UVR.fgi( 174, 358):pre: ������������  �� T
      R_(82)=R0_upad
C FDA20_SP2_UVR.fgi( 144, 295):pre: ������������  �� T
      R_(25)=R0_oke
C FDA20_SP2_UVR.fgi( 144, 268):pre: ������������  �� T
      R_(76)=R0_idad
C FDA20_SP2_UVR.fgi( 167, 217):pre: ������������  �� T
      R_(75)=R0_ubad
C FDA20_SP2_UVR.fgi( 167, 211):pre: ������������  �� T
      R_(78)=R0_ufad
C FDA20_SP2_UVR.fgi( 166, 223):pre: �������� ��������� ������
      R_(80)=R0_ukad
C FDA20_SP2_UVR.fgi( 137, 235):pre: ������������  �� T
      R_(73)=R0_exu
C FDA20_SP2_UVR.fgi( 167, 195):pre: ������������  �� T
      R_(74)=R0_uxu
C FDA20_SP2_UVR.fgi( 167, 201):pre: ������������  �� T
      R_(61)=R0_ubu
C FDA20_SP2_UVR.fgi( 167, 184):pre: ������������  �� T
      R_(70)=R0_esu
C FDA20_SP2_UVR.fgi( 160, 124):pre: ������������  �� T
      R_(69)=R0_iru
C FDA20_SP2_UVR.fgi( 160, 113):pre: ������������  �� T
      R_(68)=R0_opu
C FDA20_SP2_UVR.fgi( 160, 102):pre: ������������  �� T
      R_(67)=R0_omu
C FDA20_SP2_UVR.fgi( 160,  91):pre: ������������  �� T
      R_(66)=R0_ulu
C FDA20_SP2_UVR.fgi( 160,  80):pre: ������������  �� T
      R_(7)=R0_ol
C FDA20_SP2_UVR.fgi( 160,  69):pre: ������������  �� T
      R_(65)=R0_uku
C FDA20_SP2_UVR.fgi( 160,  58):pre: ������������  �� T
      R_(58)=R0_umo
C FDA20_SP2_UVR.fgi( 369, 298):pre: ������������  �� T
      R_(57)=R0_amo
C FDA20_SP2_UVR.fgi( 369, 287):pre: ������������  �� T
      R_(56)=R0_alo
C FDA20_SP2_UVR.fgi( 369, 276):pre: ������������  �� T
      R_(55)=R0_ako
C FDA20_SP2_UVR.fgi( 369, 265):pre: ������������  �� T
      R_(54)=R0_efo
C FDA20_SP2_UVR.fgi( 369, 254):pre: ������������  �� T
      R_(53)=R0_ido
C FDA20_SP2_UVR.fgi( 369, 243):pre: ������������  �� T
      R_(52)=R0_obo
C FDA20_SP2_UVR.fgi( 369, 232):pre: ������������  �� T
      R_(51)=R0_oxi
C FDA20_SP2_UVR.fgi( 369, 221):pre: ������������  �� T
      R_(50)=R0_uvi
C FDA20_SP2_UVR.fgi( 369, 210):pre: ������������  �� T
      R_(49)=R0_avi
C FDA20_SP2_UVR.fgi( 369, 199):pre: ������������  �� T
      R_(48)=R0_eti
C FDA20_SP2_UVR.fgi( 369, 188):pre: ������������  �� T
      R_(46)=R0_uri
C FDA20_SP2_UVR.fgi( 369, 173):pre: ������������  �� T
      R_(45)=R0_ari
C FDA20_SP2_UVR.fgi( 369, 162):pre: ������������  �� T
      R_(44)=R0_epi
C FDA20_SP2_UVR.fgi( 369, 151):pre: ������������  �� T
      R_(43)=R0_imi
C FDA20_SP2_UVR.fgi( 369, 140):pre: ������������  �� T
      R_(42)=R0_oli
C FDA20_SP2_UVR.fgi( 369, 129):pre: ������������  �� T
      R_(41)=R0_uki
C FDA20_SP2_UVR.fgi( 369, 118):pre: ������������  �� T
      R_(8)=R0_em
C FDA20_SP2_UVR.fgi( 369, 107):pre: ������������  �� T
      R_(40)=R0_aki
C FDA20_SP2_UVR.fgi( 369,  96):pre: ������������  �� T
      R_(39)=R0_efi
C FDA20_SP2_UVR.fgi( 369,  85):pre: ������������  �� T
      R_(6)=R8_e !CopyBack
C FDA20_SP2_UVR.fgi( 394,  66):pre: ������-�������: ���������� ��� �������������� ������
      R_(3)=R0_af
C FDA20_SP2_UVR.fgi( 386,  58):pre: ������������  �� T
      R_(2)=R0_ed
C FDA20_SP2_UVR.fgi( 369,  40):pre: ������������  �� T
      R_(60)=R0_exo
C FDA20_SP2_UVR.fgi( 319, 320):pre: ������������  �� T
      R_(71)=R0_etu
C FDA20_SP2_UVR.fgi( 122, 165):pre: ������������  �� T
      R_(37)=R0_exe
C FDA20_SP2_UVR.fgi(  45, 769):pre: �������� ��������� ������
      R_(26)=R0_ole
C FDA20_SP2_UVR.fgi(  45, 653):pre: �������� ��������� ������
      R_(97)=R0_ited
C FDA20_SP2_UVR.fgi(  69, 523):pre: �������� ��������� ������
      R_(98)=R0_afid
C FDA20_SP2_UVR.fgi(  69, 369):pre: �������� ��������� ������
      R_(81)=R0_ipad
C FDA20_SP2_UVR.fgi(  44, 282):pre: �������� ��������� ������
      R_(77)=R0_efad
C FDA20_SP2_UVR.fgi( 138, 227):pre: �������� ��������� ������
      R_(72)=R0_uvu
C FDA20_SP2_UVR.fgi(  44, 197):pre: �������� ��������� ������
      R_(38)=R0_odi
C FDA20_SP2_UVR.fgi( 370,  20):pre: �������� ��������� ������
      R_(47)=R0_usi
C FDA20_SP2_UVR.fgi( 370, 182):pre: �������� ��������� ������
      R_(5)=R0_ok
C FDA20_SP2_UVR.fgi( 370,  68):pre: �������� ��������� ������
      R_(4)=R0_uf
C FDA20_SP2_UVR.fgi( 370,  51):pre: �������� ��������� ������
      R_(214)=R0_oxik
C FDA20_SP1.fgi( 445, 272):pre: ������������  �� T
      R_(212)=R0_avik
C FDA20_SP1.fgi( 445, 250):pre: ������������  �� T
      R_(211)=R0_etik
C FDA20_SP1.fgi( 445, 239):pre: ������������  �� T
      R_(210)=R0_isik
C FDA20_SP1.fgi( 445, 228):pre: ������������  �� T
      R_(208)=R0_upik
C FDA20_SP1.fgi( 445, 206):pre: ������������  �� T
      R_(226)=R0_avok
C FDA20_SP1.fgi( 445, 393):pre: ������������  �� T
      R_(225)=R0_atok
C FDA20_SP1.fgi( 445, 382):pre: ������������  �� T
      R_(224)=R0_urok
C FDA20_SP1.fgi( 445, 371):pre: ������������  �� T
      R_(203)=R0_ekik
C FDA20_SP1.fgi( 445, 169):pre: ������������  �� T
      R_(221)=R0_imok
C FDA20_SP1.fgi( 445, 338):pre: ������������  �� T
      R_(220)=R0_olok
C FDA20_SP1.fgi( 445, 327):pre: ������������  �� T
      R_(219)=R0_okok
C FDA20_SP1.fgi( 445, 316):pre: ������������  �� T
      R_(217)=R0_udok
C FDA20_SP1.fgi( 445, 294):pre: ������������  �� T
      R_(266)=R0_edil
C FDA20_SP1.fgi( 541, 776):pre: ������������  �� T
      R_(267)=R0_udil
C FDA20_SP1.fgi( 534, 786):pre: ������������  �� T
      R_(198)=R0_ovek
C FDA20_SP1.fgi( 125, 194):pre: ������������  �� T
      R_(261)=R0_avel
C FDA20_SP1.fgi( 534, 780):pre: ������������  �� T
      R_(265)=R0_ibil
C FDA20_SP1.fgi( 541, 760):pre: ������������  �� T
      R_(186)=R0_ipak
C FDA20_SP1.fgi( 465, 561):pre: ������������  �� T
      R_(178)=R0_ebak
C FDA20_SP1.fgi( 456, 561):pre: �������� ��������� ������
      R_(174)=R0_otuf
C FDA20_SP1.fgi( 465, 532):pre: ������������  �� T
      R_(187)=R0_arak
C FDA20_SP1.fgi( 465, 567):pre: ������������  �� T
      R_(179)=R0_ubak
C FDA20_SP1.fgi( 456, 567):pre: �������� ��������� ������
      R_(175)=R0_evuf
C FDA20_SP1.fgi( 465, 538):pre: ������������  �� T
      R_(188)=R0_orak
C FDA20_SP1.fgi( 465, 573):pre: ������������  �� T
      R_(180)=R0_idak
C FDA20_SP1.fgi( 456, 573):pre: �������� ��������� ������
      R_(176)=R0_uvuf
C FDA20_SP1.fgi( 465, 544):pre: ������������  �� T
      R_(189)=R0_esak
C FDA20_SP1.fgi( 465, 579):pre: ������������  �� T
      R_(181)=R0_afak
C FDA20_SP1.fgi( 456, 579):pre: �������� ��������� ������
      R_(177)=R0_ixuf
C FDA20_SP1.fgi( 465, 550):pre: ������������  �� T
      R_(199)=R0_abik
C FDA20_SP1.fgi( 158, 139):pre: ������������  �� T
      R_(262)=R0_ovel
C FDA20_SP1.fgi( 541, 748):pre: ������������  �� T
      R_(202)=R0_ofik
C FDA20_SP1.fgi( 445, 287):pre: ������������  �� T
      R_(201)=R0_afik
C FDA20_SP1.fgi( 445, 162):pre: ������������  �� T
      R_(230)=R0_ubuk
C FDA20_SP1.fgi( 551, 441):pre: �������� ��������� ������
      R_(222)=R0_epok
C FDA20_SP1.fgi( 445, 349):pre: ������������  �� T
      R_(231)=R0_okuk
C FDA20_SP1.fgi( 445, 436):pre: ������������  �� T
      R_(229)=R0_ebuk
C FDA20_SP1.fgi( 551, 426):pre: �������� ��������� ������
      R_(205)=R0_ulik
C FDA20_SP1.fgi( 445, 199):pre: ������������  �� T
      R_(209)=R0_orik
C FDA20_SP1.fgi( 445, 217):pre: ������������  �� T
      R_(207)=R0_apik
C FDA20_SP1.fgi( 462, 191):pre: ������������  �� T
      R_(206)=R0_omik
C FDA20_SP1.fgi( 380, 191):pre: �������� ��������� ������
      R_(223)=R0_arok
C FDA20_SP1.fgi( 445, 360):pre: ������������  �� T
      R_(228)=R0_oxok
C FDA20_SP1.fgi( 551, 410):pre: �������� ��������� ������
      R_(218)=R0_ufok
C FDA20_SP1.fgi( 445, 305):pre: ������������  �� T
      R_(213)=R0_uvik
C FDA20_SP1.fgi( 445, 261):pre: ������������  �� T
      R_(216)=R0_adok
C FDA20_SP1.fgi( 392, 497):pre: ������������  �� T
      R_(215)=R0_ibok
C FDA20_SP1.fgi( 392, 503):pre: ������������  �� T
      R_(232)=R0_iluk
C FDA20_SP1.fgi( 392, 486):pre: ������������  �� T
      R_(227)=R0_uvok
C FDA20_SP1.fgi( 445, 404):pre: ������������  �� T
      R_(204)=R0_alik
C FDA20_SP1.fgi( 445, 180):pre: ������������  �� T
      R_(200)=R0_adik
C FDA20_SP1.fgi(  57, 145):pre: �������� ��������� ������
      R_(247)=R0_aral
C FDA20_SP1.fgi( 118, 327):pre: ������������  �� T
      R_(248)=R0_osal
C FDA20_SP1.fgi( 118, 316):pre: ������������  �� T
      R_(246)=R0_omal
C FDA20_SP1.fgi( 118, 305):pre: ������������  �� T
      R_(239)=R0_ituk
C FDA20_SP1.fgi( 118, 283):pre: ������������  �� T
      R_(184)=R0_olak
C FDA20_SP1.fgi( 118, 253):pre: ������������  �� T
      R_(241)=R0_axuk
C FDA20_SP1.fgi( 118, 340):pre: ������������  �� T
      R_(240)=R0_ovuk
C FDA20_SP1.fgi(  68, 340):pre: �������� ��������� ������
      R_(242)=R0_uxuk
C FDA20_SP1.fgi( 118, 355):pre: ������������  �� T
      R_(249)=R0_aval
C FDA20_SP1.fgi( 125, 426):pre: ������������  �� T
      R_(171)=R0_uruf
C FDA20_SP1.fgi( 422, 560):pre: ������������  �� T
      R_(234)=R0_ipuk
C FDA20_SP1.fgi( 225, 360):pre: ������������  �� T
      R_(252)=R0_ebel
C FDA20_SP1.fgi( 158, 388):pre: ������������  �� T
      R_(253)=R0_edel
C FDA20_SP1.fgi(  57, 394):pre: �������� ��������� ������
      R_(172)=R0_isuf
C FDA20_SP1.fgi( 422, 566):pre: ������������  �� T
      R_(173)=R0_etuf
C FDA20_SP1.fgi( 252, 711):pre: �������� ������� ������
      R_(236)=R0_oruk
C FDA20_SP1.fgi( 215, 764):pre: ������������  �� T
      R_(274)=R0_ukol
C FDA20_SP1.fgi( 118, 735):pre: ������������  �� T
      R_(244)=R0_ifal
C FDA20_SP1.fgi( 118, 724):pre: ������������  �� T
      R_(243)=R0_adal
C FDA20_SP1.fgi( 118, 691):pre: ������������  �� T
      R_(273)=R0_ekol
C FDA20_SP1.fgi( 118, 680):pre: ������������  �� T
      R_(185)=R0_imak
C FDA20_SP1.fgi( 118, 653):pre: ������������  �� T
      R_(270)=R0_omil
C FDA20_SP1.fgi( 118, 614):pre: ������������  �� T
      R_(269)=R0_ulil
C FDA20_SP1.fgi( 118, 603):pre: ������������  �� T
      R_(268)=R0_alil
C FDA20_SP1.fgi( 118, 592):pre: ������������  �� T
      R_(251)=R0_oxal
C FDA20_SP1.fgi( 125, 521):pre: ������������  �� T
      R_(250)=R0_uval
C FDA20_SP1.fgi( 125, 473):pre: ������������  �� T
      R_(254)=R0_ekel
C FDA20_SP1.fgi( 158, 435):pre: ������������  �� T
      R_(255)=R0_elel
C FDA20_SP1.fgi(  57, 441):pre: �������� ��������� ������
      R_(245)=R0_okal
C FDA20_SP1.fgi(  65, 746):pre: �������� ��������� ������
      R_(238)=R0_usuk
C FDA20_SP1.fgi( 118, 746):pre: ������������  �� T
      R_(257)=R0_umel
C FDA20_SP1.fgi( 158, 483):pre: ������������  �� T
      R_(258)=R0_orel
C FDA20_SP1.fgi(  57, 489):pre: �������� ��������� ������
      R_(328)=R8_iles
C FDA20_vent_log.fgi( 101, 271):pre: �������������� �����  
      R_(324)=R8_ikes
C FDA20_vent_log.fgi( 177, 271):pre: �������������� �����  
      R_(289)=R8_etas
C FDA20_vent_log.fgi( 105, 212):pre: �������������� �����  
      R_(276)=R0_abar
C FDA20_lamp.fgi( 248, 613):pre: ������������  �� T
      R_(275)=R0_ixup
C FDA20_lamp.fgi( 248, 621):pre: ������������  �� T
      R_(271)=R0_odol
C FDA20_SP1.fgi(  74, 815):pre: ������������  �� T
      R_(256)=R0_emel
C FDA20_SP1.fgi( 118, 586):pre: ������������  �� T
      R_(264)=R0_uxel
C FDA20_SP1.fgi( 541, 792):pre: ������������  �� T
      R_(263)=R0_exel
C FDA20_SP1.fgi( 541, 754):pre: ������������  �� T
      R_(260)=R0_itel
C FDA20_SP1.fgi( 534, 770):pre: ������������  �� T
      R_(259)=R0_usel
C FDA20_SP1.fgi( 534, 764):pre: ������������  �� T
      R_(237)=R0_esuk
C FDA20_SP1.fgi( 126, 685):pre: ������������  �� T
      R_(235)=R0_aruk
C FDA20_SP1.fgi( 222, 707):pre: ������������  �� T
      R_(233)=R0_umuk
C FDA20_SP1.fgi( 236, 355):pre: ������������  �� T
      R_(272)=R0_efol
C FDA20_SP1.fgi(  74, 809):pre: ������������  �� T
      R_(170)=R0_apuf
C FDA20_SP2_UVR.fgi(  54, 826):pre: ������������  �� T
      R_(169)=R0_imuf
C FDA20_SP2_UVR.fgi(  54, 832):pre: ������������  �� T
      R_(111)=R0_oxid
C FDA20_SP2_UVR.fgi( 148, 516):pre: ������������  �� T
      R_(143)=R0_opud
C FDA20_SP2_UVR.fgi( 565, 356):pre: �������� ��������� ������
      R_(59)=R0_ovo
C FDA20_SP2_UVR.fgi( 319, 326):pre: ������������  �� T
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_umefe,4),R8_ukefe
     &,I_esefe,I_usefe,I_asefe,
     & C8_ilefe,I_osefe,R_emefe,R_amefe,R_udefe,
     & REAL(R_efefe,4),R_akefe,REAL(R_ikefe,4),
     & R_ifefe,REAL(R_ufefe,4),I_orefe,I_atefe,I_isefe,I_irefe
     &,L_okefe,
     & L_itefe,L_ivefe,L_ekefe,L_ofefe,
     & L_elefe,L_alefe,L_utefe,L_afefe,L_ulefe,L_axefe,L_imefe
     &,
     & L_omefe,REAL(R8_iseke,8),REAL(1.0,4),R8_ebike,L_(555
     &),L_adefe,L_(556),
     & L_edefe,L_otefe,I_etefe,L_ovefe,R_erefe,REAL(R_olefe
     &,4),L_uvefe,L_idefe,
     & L_exefe,L_odefe,L_apefe,L_epefe,L_ipefe,L_upefe,L_arefe
     &,L_opefe)
      !}

      if(L_arefe.or.L_upefe.or.L_opefe.or.L_ipefe.or.L_epefe.or.L_apefe
     &) then      
                  I_urefe = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_urefe = z'40000000'
      endif
C FDA20_vlv.fgi( 157, 176):���� ���������� �������� � ���������������� ��������,20FDA23BR003KA12
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_amade,4),R8_akade
     &,I_irade,I_asade,I_erade,
     & C8_okade,I_urade,R_ilade,R_elade,R_adade,
     & REAL(R_idade,4),R_efade,REAL(R_ofade,4),
     & R_odade,REAL(R_afade,4),I_upade,I_esade,I_orade,I_opade
     &,L_ufade,
     & L_osade,L_otade,L_ifade,L_udade,
     & L_ikade,L_ekade,L_atade,L_edade,L_alade,L_evade,L_olade
     &,
     & L_ulade,REAL(R8_iseke,8),REAL(1.0,4),R8_ebike,L_(541
     &),L_ebade,L_(542),
     & L_ibade,L_usade,I_isade,L_utade,R_ipade,REAL(R_ukade
     &,4),L_avade,L_obade,
     & L_ivade,L_ubade,L_emade,L_imade,L_omade,L_apade,L_epade
     &,L_umade)
      !}

      if(L_epade.or.L_apade.or.L_umade.or.L_omade.or.L_imade.or.L_emade
     &) then      
                  I_arade = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_arade = z'40000000'
      endif
C FDA20_vlv.fgi( 212, 150):���� ���������� �������� � ���������������� ��������,20FDA21BR110KA03
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_ebode,4),R8_evide
     &,I_ofode,I_ekode,I_ifode,
     & C8_uvide,I_akode,R_oxide,R_ixide,R_eside,
     & REAL(R_oside,4),R_itide,REAL(R_utide,4),
     & R_uside,REAL(R_etide,4),I_afode,I_ikode,I_ufode,I_udode
     &,L_avide,
     & L_ukode,L_ulode,L_otide,L_atide,
     & L_ovide,L_ivide,L_elode,L_iside,L_exide,L_imode,L_uxide
     &,
     & L_abode,REAL(R8_iseke,8),REAL(1.0,4),R8_ebike,L_(547
     &),L_iride,L_(548),
     & L_oride,L_alode,I_okode,L_amode,R_odode,REAL(R_axide
     &,4),L_emode,L_uride,
     & L_omode,L_aside,L_ibode,L_obode,L_ubode,L_edode,L_idode
     &,L_adode)
      !}

      if(L_idode.or.L_edode.or.L_adode.or.L_ubode.or.L_obode.or.L_ibode
     &) then      
                  I_efode = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_efode = z'40000000'
      endif
C FDA20_vlv.fgi( 157, 150):���� ���������� �������� � ���������������� ��������,20FDA21BR107KA03
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_irafe,4),R8_imafe
     &,I_utafe,I_ivafe,I_otafe,
     & C8_apafe,I_evafe,R_upafe,R_opafe,R_ikafe,
     & REAL(R_ukafe,4),R_olafe,REAL(R_amafe,4),
     & R_alafe,REAL(R_ilafe,4),I_etafe,I_ovafe,I_avafe,I_atafe
     &,L_emafe,
     & L_axafe,L_abefe,L_ulafe,L_elafe,
     & L_umafe,L_omafe,L_ixafe,L_okafe,L_ipafe,L_obefe,L_arafe
     &,
     & L_erafe,REAL(R8_iseke,8),REAL(1.0,4),R8_ebike,L_(553
     &),L_ofafe,L_(554),
     & L_ufafe,L_exafe,I_uvafe,L_ebefe,R_usafe,REAL(R_epafe
     &,4),L_ibefe,L_akafe,
     & L_ubefe,L_ekafe,L_orafe,L_urafe,L_asafe,L_isafe,L_osafe
     &,L_esafe)
      !}

      if(L_osafe.or.L_isafe.or.L_esafe.or.L_asafe.or.L_urafe.or.L_orafe
     &) then      
                  I_itafe = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_itafe = z'40000000'
      endif
C FDA20_vlv.fgi( 175, 176):���� ���������� �������� � ���������������� ��������,20FDA23BR004KA12
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_udide,4),R8_uxede
     &,I_elide,I_ulide,I_alide,
     & C8_ibide,I_olide,R_edide,R_adide,R_utede,
     & REAL(R_evede,4),R_axede,REAL(R_ixede,4),
     & R_ivede,REAL(R_uvede,4),I_okide,I_amide,I_ilide,I_ikide
     &,L_oxede,
     & L_imide,L_ipide,L_exede,L_ovede,
     & L_ebide,L_abide,L_umide,L_avede,L_ubide,L_aride,L_idide
     &,
     & L_odide,REAL(R8_iseke,8),REAL(1.0,4),R8_ebike,L_(545
     &),L_atede,L_(546),
     & L_etede,L_omide,I_emide,L_opide,R_ekide,REAL(R_obide
     &,4),L_upide,L_itede,
     & L_eride,L_otede,L_afide,L_efide,L_ifide,L_ufide,L_akide
     &,L_ofide)
      !}

      if(L_akide.or.L_ufide.or.L_ofide.or.L_ifide.or.L_efide.or.L_afide
     &) then      
                  I_ukide = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ukide = z'40000000'
      endif
C FDA20_vlv.fgi( 175, 150):���� ���������� �������� � ���������������� ��������,20FDA21BR108KA03
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_atude,4),R8_arude
     &,I_ixude,I_abafe,I_exude,
     & C8_orude,I_uxude,R_isude,R_esude,R_amude,
     & REAL(R_imude,4),R_epude,REAL(R_opude,4),
     & R_omude,REAL(R_apude,4),I_uvude,I_ebafe,I_oxude,I_ovude
     &,L_upude,
     & L_obafe,L_odafe,L_ipude,L_umude,
     & L_irude,L_erude,L_adafe,L_emude,L_asude,L_efafe,L_osude
     &,
     & L_usude,REAL(R8_iseke,8),REAL(1.0,4),R8_ebike,L_(551
     &),L_elude,L_(552),
     & L_ilude,L_ubafe,I_ibafe,L_udafe,R_ivude,REAL(R_urude
     &,4),L_afafe,L_olude,
     & L_ifafe,L_ulude,L_etude,L_itude,L_otude,L_avude,L_evude
     &,L_utude)
      !}

      if(L_evude.or.L_avude.or.L_utude.or.L_otude.or.L_itude.or.L_etude
     &) then      
                  I_axude = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_axude = z'40000000'
      endif
C FDA20_vlv.fgi( 194, 176):���� ���������� �������� � ���������������� ��������,20FDA23BR005KA12
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_ikede,4),R8_idede
     &,I_umede,I_ipede,I_omede,
     & C8_afede,I_epede,R_ufede,R_ofede,R_ixade,
     & REAL(R_uxade,4),R_obede,REAL(R_adede,4),
     & R_abede,REAL(R_ibede,4),I_emede,I_opede,I_apede,I_amede
     &,L_edede,
     & L_arede,L_asede,L_ubede,L_ebede,
     & L_udede,L_odede,L_irede,L_oxade,L_ifede,L_osede,L_akede
     &,
     & L_ekede,REAL(R8_iseke,8),REAL(1.0,4),R8_ebike,L_(543
     &),L_ovade,L_(544),
     & L_uvade,L_erede,I_upede,L_esede,R_ulede,REAL(R_efede
     &,4),L_isede,L_axade,
     & L_usede,L_exade,L_okede,L_ukede,L_alede,L_ilede,L_olede
     &,L_elede)
      !}

      if(L_olede.or.L_ilede.or.L_elede.or.L_alede.or.L_ukede.or.L_okede
     &) then      
                  I_imede = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_imede = z'40000000'
      endif
C FDA20_vlv.fgi( 194, 150):���� ���������� �������� � ���������������� ��������,20FDA21BR109KA03
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_ovode,4),R8_osode
     &,I_adude,I_odude,I_ubude,
     & C8_etode,I_idude,R_avode,R_utode,R_opode,
     & REAL(R_arode,4),R_urode,REAL(R_esode,4),
     & R_erode,REAL(R_orode,4),I_ibude,I_udude,I_edude,I_ebude
     &,L_isode,
     & L_efude,L_ekude,L_asode,L_irode,
     & L_atode,L_usode,L_ofude,L_upode,L_otode,L_ukude,L_evode
     &,
     & L_ivode,REAL(R8_iseke,8),REAL(1.0,4),R8_ebike,L_(549
     &),L_umode,L_(550),
     & L_apode,L_ifude,I_afude,L_ikude,R_abude,REAL(R_itode
     &,4),L_okude,L_epode,
     & L_alude,L_ipode,L_uvode,L_axode,L_exode,L_oxode,L_uxode
     &,L_ixode)
      !}

      if(L_uxode.or.L_oxode.or.L_ixode.or.L_exode.or.L_axode.or.L_uvode
     &) then      
                  I_obude = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_obude = z'40000000'
      endif
C FDA20_vlv.fgi( 212, 176):���� ���������� �������� � ���������������� ��������,20FDA23BR006KA12
      !��������� R_(1) = FDA20_SP2_UVRC?? /4.0/
      R_(1)=R0_u
C FDA20_SP2_UVR.fgi( 388,  67):���������
      L_(121)=.false.
C FDA20_SP2_UVR.fgi( 186,  46):��������� ���������� (�������)
      L_(94)=.true.
C FDA20_SP2_UVR.fgi(  52,  42):��������� ���������� (�������)
      L_(123)=.false.
C FDA20_SP2_UVR.fgi( 163, 293):��������� ���������� (�������)
      L_(24) = L_upif.AND.L_opif.AND.L_ipif.AND.L_epif.AND.L_apif.AND.L_
     &umif.AND.L_omif.AND.L_imif.AND.L_emif.AND.L_amif
C FDA20_SP2_UVR.fgi(  57, 268):�
      if(L_uxo.and..not.L0_axo) then
         R0_ovo=R0_uvo
      else
         R0_ovo=max(R_(59)-deltat,0.0)
      endif
      L_obu=R0_ovo.gt.0.0
      L0_axo=L_uxo
C FDA20_SP2_UVR.fgi( 319, 326):������������  �� T
      L_eraf=.false.
C FDA20_SP2_UVR.fgi( 170, 161):��������� ���������� (�������)
      L_iraf=.true.
C FDA20_SP2_UVR.fgi( 170, 157):��������� ���������� (�������)
      L_oraf=.false.
C FDA20_SP2_UVR.fgi( 170, 153):��������� ���������� (�������)
      L_uraf=.true.
C FDA20_SP2_UVR.fgi( 170, 149):��������� ���������� (�������)
      L_ilad=.false.
C FDA20_SP2_UVR.fgi( 170, 244):��������� ���������� (�������)
      L_imad=.false.
C FDA20_SP2_UVR.fgi( 142, 244):��������� ���������� (�������)
      L_(134) = L_eduf.AND.L_olod.AND.L_aduf.AND.L_ilod.AND.L_ubuf.AND.L
     &_elod.AND.L_obuf.AND.L_alod.AND.L_ibuf.AND.L_ekof
C FDA20_SP2_UVR.fgi(  55, 358):�
      L_(162) = L_oped.AND.L_ikid.AND.L_oduf
C FDA20_SP2_UVR.fgi(  55, 405):�
      L_(170) = L_ired.AND.L_omid.AND.L_uduf
C FDA20_SP2_UVR.fgi(  55, 436):�
      L_(178) = L_esed.AND.L_urid.AND.L_afuf
C FDA20_SP2_UVR.fgi(  55, 467):�
      L_(187) = L_ated.AND.L_avid.AND.L_efuf
C FDA20_SP2_UVR.fgi(  55, 499):�
      L_(231)=.false.
C FDA20_SP2_UVR.fgi( 377, 835):��������� ���������� (�������)
      L_asod = L_(231).OR.L_(231).OR.L_(231)
C FDA20_SP2_UVR.fgi( 404, 797):���
      L_urod = L_(231).OR.L_(231).OR.L_(231)
C FDA20_SP2_UVR.fgi( 404, 791):���
      L_orod = L_(231).OR.L_(231).OR.L_(231)
C FDA20_SP2_UVR.fgi( 404, 785):���
      L_irod = L_(231).OR.L_(231).OR.L_(231)
C FDA20_SP2_UVR.fgi( 404, 779):���
      L_(139) = L_(231).OR.L_(231).OR.L_(231)
C FDA20_SP2_UVR.fgi( 400, 608):���
      L_uved = L_(231).OR.L_(231).OR.L_(231).OR.L_(139)
C FDA20_SP2_UVR.fgi( 406, 567):���
      L_oved = L_(231).OR.L_(231).OR.L_(231).OR.L_(139)
C FDA20_SP2_UVR.fgi( 406, 559):���
      L_ived = L_(231).OR.L_(231).OR.L_(231).OR.L_(139)
C FDA20_SP2_UVR.fgi( 406, 551):���
      L_eved = L_(231).OR.L_(231).OR.L_(231).OR.L_(139)
C FDA20_SP2_UVR.fgi( 406, 543):���
      L_aved = L_(231).OR.L_(231).OR.L_(231).OR.L_(139)
C FDA20_SP2_UVR.fgi( 406, 535):���
      L_uted = L_(231).OR.L_(231).OR.L_(231).OR.L_(139)
C FDA20_SP2_UVR.fgi( 406, 527):���
      !��������� R_(121) = FDA20_SP2_UVRCmarginweight /0.5
C /
      R_(121)=R0_ubud
C FDA20_SP2_UVR.fgi( 543, 474):���������,marginweight
      L_esaf=.false.
C FDA20_SP2_UVR.fgi( 565, 494):��������� ���������� (�������)
      L_etud = L_otaf.OR.L_utaf.OR.L_avaf.OR.L_evaf.OR.L_ivaf.OR.L_ovaf.
     &OR.L_uvaf.OR.L_axaf.OR.L_exaf
C FDA20_SP2_UVR.fgi( 554, 517):���
      !��������� R_(124) = FDA20_SP2_UVRCfweight /10.5/
      R_(124)=R0_adud
C FDA20_SP2_UVR.fgi( 536, 276):���������,fweight
      R_(133) = 0.0
C FDA20_SP2_UVR.fgi( 580, 242):��������� (RE4) (�������)
      if(L_axud) then
         R_(132)=R_ufud
      else
         R_(132)=R_(133)
      endif
C FDA20_SP2_UVR.fgi( 579, 239):���� RE IN LO CH7
      if(L_uvud) then
         R_(131)=R_ofud
      else
         R_(131)=R_(132)
      endif
C FDA20_SP2_UVR.fgi( 575, 235):���� RE IN LO CH7
      if(L_ovud) then
         R_(130)=R_ifud
      else
         R_(130)=R_(131)
      endif
C FDA20_SP2_UVR.fgi( 571, 231):���� RE IN LO CH7
      if(L_ivud) then
         R_(129)=R_efud
      else
         R_(129)=R_(130)
      endif
C FDA20_SP2_UVR.fgi( 567, 227):���� RE IN LO CH7
      if(L_evud) then
         R_(128)=R_afud
      else
         R_(128)=R_(129)
      endif
C FDA20_SP2_UVR.fgi( 563, 223):���� RE IN LO CH7
      if(L_avud) then
         R_(127)=R_udud
      else
         R_(127)=R_(128)
      endif
C FDA20_SP2_UVR.fgi( 559, 219):���� RE IN LO CH7
      if(L_utud) then
         R_(126)=R_odud
      else
         R_(126)=R_(127)
      endif
C FDA20_SP2_UVR.fgi( 555, 215):���� RE IN LO CH7
      if(L_otud) then
         R_(125)=R_idud
      else
         R_(125)=R_(126)
      endif
C FDA20_SP2_UVR.fgi( 551, 211):���� RE IN LO CH7
      if(L_itud) then
         R_oxis=R_edud
      else
         R_oxis=R_(125)
      endif
C FDA20_SP2_UVR.fgi( 547, 207):���� RE IN LO CH7
      !{
      Call DAT_ANA_HANDLER(deltat,R_ivis,R_edos,REAL(1,4)
     &,
     & REAL(R_axis,4),REAL(R_exis,4),
     & REAL(R_evis,4),REAL(R_avis,4),I_ados,
     & REAL(R_uxis,4),L_abos,REAL(R_ebos,4),L_ibos,L_obos
     &,R_ixis,
     & REAL(R_uvis,4),REAL(R_ovis,4),L_ubos,REAL(R_oxis,4
     &))
      !}
C FDA20_vlv.fgi( 316,  71):���������� �������,20FDA22CT001XQ01
      R_(142) = 0.0
C FDA20_SP2_UVR.fgi( 553, 309):��������� (RE4) (�������)
      L_epud=.false.
C FDA20_SP2_UVR.fgi( 550, 344):��������� ���������� (�������)
      L_ipaf = L_usud.OR.L_osud.OR.L_isud.OR.L_esud.OR.L_asud.OR.L_urud.
     &OR.L_orud.OR.L_irud.OR.L_erud
C FDA20_SP2_UVR.fgi( 548, 363):���
      if(.not.L_ipaf) then
         R0_opud=0.0
      elseif(.not.L0_arud) then
         R0_opud=R0_ipud
      else
         R0_opud=max(R_(143)-deltat,0.0)
      endif
      L_upud=L_ipaf.and.R0_opud.le.0.0
      L0_arud=L_ipaf
C FDA20_SP2_UVR.fgi( 565, 356):�������� ��������� ������
      L_olaf = L_axud.OR.L_uvud.OR.L_ovud.OR.L_ivud.OR.L_evud.OR.L_avud.
     &OR.L_utud.OR.L_otud.OR.L_itud
C FDA20_SP2_UVR.fgi( 548, 384):���
      L_ulaf = L_ubaf.OR.L_obaf.OR.L_ibaf.OR.L_ebaf.OR.L_abaf.OR.L_uxud.
     &OR.L_oxud.OR.L_ixud.OR.L_exud
C FDA20_SP2_UVR.fgi( 548, 404):���
      L_amaf = L_ofaf.OR.L_ifaf.OR.L_efaf.OR.L_afaf.OR.L_udaf.OR.L_odaf.
     &OR.L_idaf.OR.L_edaf.OR.L_adaf
C FDA20_SP2_UVR.fgi( 548, 424):���
      L_emaf = L_ilaf.OR.L_elaf.OR.L_alaf.OR.L_ukaf.OR.L_okaf.OR.L_ikaf.
     &OR.L_ekaf.OR.L_akaf.OR.L_ufaf
C FDA20_SP2_UVR.fgi( 548, 444):���
      L_(188) =.NOT.(L_emaf.AND.L_amaf.AND.L_ulaf.AND.L_olaf
     &)
C FDA20_SP2_UVR.fgi( 139, 516):�
      if(L_(188).and..not.L0_abod) then
         R0_oxid=R0_uxid
      else
         R0_oxid=max(R_(111)-deltat,0.0)
      endif
      L_(189)=R0_oxid.gt.0.0
      L0_abod=L_(188)
C FDA20_SP2_UVR.fgi( 148, 516):������������  �� T
      L_apaf=L_epaf
C FDA20_SP2_UVR.fgi( 440, 157):������,unloading_pos
      R_(152) = 0.0
C FDA20_SP2_UVR.fgi( 549, 507):��������� (RE4) (�������)
      if(L_exaf) then
         R_(151)=R_adef
      else
         R_(151)=R_(152)
      endif
C FDA20_SP2_UVR.fgi( 548, 504):���� RE IN LO CH7
      if(L_axaf) then
         R_(150)=R_ubef
      else
         R_(150)=R_(151)
      endif
C FDA20_SP2_UVR.fgi( 544, 500):���� RE IN LO CH7
      if(L_uvaf) then
         R_(149)=R_obef
      else
         R_(149)=R_(150)
      endif
C FDA20_SP2_UVR.fgi( 540, 496):���� RE IN LO CH7
      if(L_ovaf) then
         R_(148)=R_ibef
      else
         R_(148)=R_(149)
      endif
C FDA20_SP2_UVR.fgi( 536, 492):���� RE IN LO CH7
      if(L_ivaf) then
         R_(147)=R_ebef
      else
         R_(147)=R_(148)
      endif
C FDA20_SP2_UVR.fgi( 532, 488):���� RE IN LO CH7
      if(L_evaf) then
         R_(146)=R_abef
      else
         R_(146)=R_(147)
      endif
C FDA20_SP2_UVR.fgi( 528, 484):���� RE IN LO CH7
      if(L_avaf) then
         R_(145)=R_uxaf
      else
         R_(145)=R_(146)
      endif
C FDA20_SP2_UVR.fgi( 524, 480):���� RE IN LO CH7
      if(L_utaf) then
         R_(144)=R_oxaf
      else
         R_(144)=R_(145)
      endif
C FDA20_SP2_UVR.fgi( 520, 476):���� RE IN LO CH7
      if(L_otaf) then
         R_emof=R_ixaf
      else
         R_emof=R_(144)
      endif
C FDA20_SP2_UVR.fgi( 516, 472):���� RE IN LO CH7
      R_itaf=R_emof
C FDA20_SP2_UVR.fgi( 553, 470):������,fda_uvr_w1_input
      !{
      Call DAT_ANA_HANDLER(deltat,R_ikud,R_apud,REAL(1,4)
     &,
     & REAL(R_alud,4),REAL(R_elud,4),
     & REAL(R_ekud,4),REAL(R_akud,4),I_umud,
     & REAL(R_olud,4),L_ulud,REAL(R_amud,4),L_emud,L_imud
     &,R_ilud,
     & REAL(R_ukud,4),REAL(R_okud,4),L_omud,REAL(R_itaf,4
     &))
      !}
C FDA20_SP2_UVR.fgi( 583, 456):���������� �������,fda_uvr_w1
      !{
      Call DAT_ANA_HANDLER(deltat,R_alof,R_upof,REAL(1,4)
     &,
     & REAL(R_olof,4),REAL(R_ulof,4),
     & REAL(R_ukof,4),REAL(R_okof,4),I_opof,
     & REAL(R_imof,4),L_omof,REAL(R_umof,4),L_apof,L_epof
     &,R_amof,
     & REAL(R_ilof,4),REAL(R_elof,4),L_ipof,REAL(R_emof,4
     &))
      !}
C FDA20_SP2_UVR.fgi( 583, 470):���������� �������,fda_uvr_w3
      R_(123) = (-R_emof) + R_emof
C FDA20_SP2_UVR.fgi( 536, 480):��������
      R_(122)=abs(R_(123))
C FDA20_SP2_UVR.fgi( 542, 480):���������� ��������
      L_(239)=R_(122).lt.R_(121)
C FDA20_SP2_UVR.fgi( 554, 479):���������� <
      L_atud = L_etud.AND.L_(239)
C FDA20_SP2_UVR.fgi( 563, 490):�
      L_asaf = L_etud.AND.(.NOT.L_(239))
C FDA20_SP2_UVR.fgi( 563, 486):�
      L_edef = L_akef.OR.L_ufef.OR.L_ofef.OR.L_ifef.OR.L_efef.OR.L_afef.
     &OR.L_udef.OR.L_odef.OR.L_idef
C FDA20_SP2_UVR.fgi( 536, 763):���
      L_ekef = L_amef.OR.L_ulef.OR.L_olef.OR.L_ilef.OR.L_elef.OR.L_alef.
     &OR.L_ukef.OR.L_okef.OR.L_ikef
C FDA20_SP2_UVR.fgi( 536, 783):���
      L_emef = L_aref.OR.L_upef.OR.L_opef.OR.L_ipef.OR.L_epef.OR.L_apef.
     &OR.L_umef.OR.L_omef.OR.L_imef
C FDA20_SP2_UVR.fgi( 536, 803):���
      L_eref = L_atef.OR.L_usef.OR.L_osef.OR.L_isef.OR.L_esef.OR.L_asef.
     &OR.L_uref.OR.L_oref.OR.L_iref
C FDA20_SP2_UVR.fgi( 536, 823):���
      L_(252) = L_efuf.AND.L_afuf.AND.L_uduf.AND.L_oduf.AND.L_iduf.AND.L
     &_eduf.AND.L_aduf.AND.L_ubuf.AND.L_obuf.AND.L_ibuf
C FDA20_SP2_UVR.fgi(  57, 730):�
      L_(258) = L_afif.AND.L_udif.AND.L_odif.AND.L_idif.AND.L_edif.AND.L
     &_adif.AND.L_ubif.AND.L_obif.AND.L_ibif.AND.L_ebif
C FDA20_SP2_UVR.fgi(  57, 616):�
      L_(261) = L_upif.AND.L_opif.AND.L_ipif.AND.L_epif.AND.L_apif.AND.L
     &_umif.AND.L_omif.AND.L_imif.AND.L_emif.AND.L_amif
C FDA20_SP2_UVR.fgi(  57, 672):�
      L_(283) = L_ebuf.AND.L_abuf.AND.L_uxof.AND.L_oxof.AND.L_ixof.AND.L
     &_exof.AND.L_axof.AND.L_uvof.AND.L_ovof.AND.L_ivof
C FDA20_SP2_UVR.fgi(  57, 750):�
      L_(287) = L_(283).AND.L_(252)
C FDA20_SP2_UVR.fgi(  68, 740):�
      if(L_opuf.and..not.L0_umuf) then
         R0_imuf=R0_omuf
      else
         R0_imuf=max(R_(169)-deltat,0.0)
      endif
      L_oruf=R0_imuf.gt.0.0
      L0_umuf=L_opuf
C FDA20_SP2_UVR.fgi(  54, 832):������������  �� T
      if(L_upuf.and..not.L0_ipuf) then
         R0_apuf=R0_epuf
      else
         R0_apuf=max(R_(170)-deltat,0.0)
      endif
      L_(290)=R0_apuf.gt.0.0
      L0_ipuf=L_upuf
C FDA20_SP2_UVR.fgi(  54, 826):������������  �� T
      R_(182) = 0.5
C FDA20_SP1.fgi( 339, 655):��������� (RE4) (�������)
      R_(183) = 0.9
C FDA20_SP1.fgi( 128,  79):��������� (RE4) (�������)
      L_epak = L_itek.OR.L_atek.OR.L_etek
C FDA20_SP1.fgi( 428, 642):���
      L_ivak =.NOT.(L_itek.OR.L_etek.OR.L_atek.OR.L_otek)
C FDA20_SP1.fgi( 428, 623):���
      I_(1) = 1
C FDA20_SP1.fgi( 427, 704):��������� ������������� IN (�������)
      L_(313)=.false.
C FDA20_SP1.fgi( 418, 709):��������� ���������� (�������)
      I_(2) = 1
C FDA20_SP1.fgi( 427, 733):��������� ������������� IN (�������)
      L_(316)=.false.
C FDA20_SP1.fgi( 418, 738):��������� ���������� (�������)
      I_(3) = 1
C FDA20_SP1.fgi( 427, 762):��������� ������������� IN (�������)
      L_(319)=.false.
C FDA20_SP1.fgi( 418, 767):��������� ���������� (�������)
      I_(4) = 1
C FDA20_SP1.fgi( 427, 791):��������� ������������� IN (�������)
      L_(322)=.false.
C FDA20_SP1.fgi( 418, 796):��������� ���������� (�������)
      L_erek = L_etek.AND.L_atek.AND.L_itek.AND.L_otek
C FDA20_SP1.fgi( 428, 635):�
      L_(497) = L_akol.AND.L_erek
C FDA20_SP1.fgi(  45, 809):�
      if(L_(497).and..not.L0_ofol) then
         R0_efol=R0_ifol
      else
         R0_efol=max(R_(272)-deltat,0.0)
      endif
      L_(503)=R0_efol.gt.0.0
      L0_ofol=L_(497)
C FDA20_SP1.fgi(  74, 809):������������  �� T
      R_(194) = 1000000
C FDA20_SP1.fgi(  38, 331):��������� (RE4) (�������)
      R_(197) = -12
C FDA20_SP1.fgi( 242, 243):��������� (RE4) (�������)
      R_(196) = 1200000
C FDA20_SP1.fgi( 240, 243):��������� (RE4) (�������)
      if(L_ubor.and..not.L0_epuk) then
         R0_umuk=R0_apuk
      else
         R0_umuk=max(R_(233)-deltat,0.0)
      endif
      L_(412)=R0_umuk.gt.0.0
      L0_epuk=L_ubor
C FDA20_SP1.fgi( 236, 355):������������  �� T
      if(L_ubor.and..not.L0_iruk) then
         R0_aruk=R0_eruk
      else
         R0_aruk=max(R_(235)-deltat,0.0)
      endif
      L_(415)=R0_aruk.gt.0.0
      L0_iruk=L_ubor
C FDA20_SP1.fgi( 222, 707):������������  �� T
      if(L_erur.and..not.L0_osuk) then
         R0_esuk=R0_isuk
      else
         R0_esuk=max(R_(237)-deltat,0.0)
      endif
      L_(420)=R0_esuk.gt.0.0
      L0_osuk=L_erur
C FDA20_SP1.fgi( 126, 685):������������  �� T
      L_(444)=.true.
C FDA20_SP1.fgi(  83, 540):��������� ���������� (�������)
      L_(446)=.true.
C FDA20_SP1.fgi(  79, 554):��������� ���������� (�������)
      L_(459)=.true.
C FDA20_SP1.fgi(  75, 570):��������� ���������� (�������)
      if(L_ofor.and..not.L0_etel) then
         R0_usel=R0_atel
      else
         R0_usel=max(R_(259)-deltat,0.0)
      endif
      L_(468)=R0_usel.gt.0.0
      L0_etel=L_ofor
C FDA20_SP1.fgi( 534, 764):������������  �� T
      if(L_ubor.and..not.L0_utel) then
         R0_itel=R0_otel
      else
         R0_itel=max(R_(260)-deltat,0.0)
      endif
      L_(469)=R0_itel.gt.0.0
      L0_utel=L_ubor
C FDA20_SP1.fgi( 534, 770):������������  �� T
      L_ufil = L_(469).OR.L_(468)
C FDA20_SP1.fgi( 541, 769):���
      L_(466) = L_ofor.AND.L_obol
C FDA20_SP1.fgi( 534, 754):�
      if(L_(466).and..not.L0_oxel) then
         R0_exel=R0_ixel
      else
         R0_exel=max(R_(263)-deltat,0.0)
      endif
      L_atil=R0_exel.gt.0.0
      L0_oxel=L_(466)
C FDA20_SP1.fgi( 541, 754):������������  �� T
      L_(467) = L_obol.AND.L_ubor
C FDA20_SP1.fgi( 534, 792):�
      if(L_(467).and..not.L0_ebil) then
         R0_uxel=R0_abil
      else
         R0_uxel=max(R_(264)-deltat,0.0)
      endif
      L_ixil=R0_uxel.gt.0.0
      L0_ebil=L_(467)
C FDA20_SP1.fgi( 541, 792):������������  �� T
      if(L_ixil.and..not.L0_omel) then
         R0_emel=R0_imel
      else
         R0_emel=max(R_(256)-deltat,0.0)
      endif
      L_opil=R0_emel.gt.0.0
      L0_omel=L_ixil
C FDA20_SP1.fgi( 118, 586):������������  �� T
      L_(486) = L_utep.AND.L_ipep.AND.L_akep.AND.L_ulap.AND.L_esap.AND.L
     &_oxap.AND.L_idap.AND.L_avum
C FDA20_SP1.fgi(  61, 614):�
      L_(480) = (.NOT.L_(486))
C FDA20_SP1.fgi(  69, 592):���
      L_(496) = L_ubol.AND.L_obol.AND.L_ibol
C FDA20_SP1.fgi(  70, 770):�
      L_ilor=.false.
C FDA20_SP1.fgi( 431, 673):��������� ���������� (�������)
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ikor,L_alor,R_ukor,
     & REAL(R_elor,4),L_ilor,L_ekor,I_okor)
      !}
C FDA20_lamp.fgi(  82, 738):���������� ������� ���������,20FDA21CG95
      L_utor =.NOT.(L_idol)
C FDA20_SP1.fgi( 426, 677):���
      !{
      Call DAT_DISCR_HANDLER(deltat,I_usor,L_itor,R_etor,
     & REAL(R_otor,4),L_utor,L_osor,I_ator)
      !}
C FDA20_lamp.fgi(  82, 760):���������� ������� ���������,20FDA21CG89
      L_asor=L_idol
C FDA20_SP1.fgi( 448, 681):������,20FDA21CG90YU10
      !{
      Call DAT_DISCR_HANDLER(deltat,I_aror,L_oror,R_iror,
     & REAL(R_uror,4),L_asor,L_upor,I_eror)
      !}
C FDA20_lamp.fgi(  82, 748):���������� ������� ���������,20FDA21CG90
      L_epor=L_idol
C FDA20_SP1.fgi( 448, 685):������,20FDA21CG91YU10
      !{
      Call DAT_DISCR_HANDLER(deltat,I_emor,L_umor,R_omor,
     & REAL(R_apor,4),L_epor,L_amor,I_imor)
      !}
C FDA20_lamp.fgi(  82, 770):���������� ������� ���������,20FDA21CG91
      if(L_ufol.and..not.L0_afol) then
         R0_odol=R0_udol
      else
         R0_odol=max(R_(271)-deltat,0.0)
      endif
      L_imol=R0_odol.gt.0.0
      L0_afol=L_ufol
C FDA20_SP1.fgi(  74, 815):������������  �� T
      if (.not.L_omol.and.(L_emol.or.L_(503))) then
          I_umol = 0
          L_omol = .true.
          L_(502) = .true.
      endif
      if (I_umol .gt. 0) then
         L_(502) = .false.
      endif
      if(L_imol)then
          I_umol = 0
          L_omol = .false.
          L_(502) = .false.
      endif
C FDA20_SP1.fgi(  96, 801):���������� ������� ���������,cset
      !{
      Call LIMIT_SWITCH_HANDLER(deltat,L_axol,R_uvol,
     & REAL(R_exol,4),R_evol,REAL(R_ovol,4),L_ibul,
     & L_ivol,L_obul,L_ixol,I_otol)
      !}
C FDA20_lamp.fgi( 106, 116):���������� ��������� �����������,20FDA21AE703QB03
      !{
      Call LIMIT_SWITCH_HANDLER(deltat,L_iful,R_eful,
     & REAL(R_oful,4),R_odul,REAL(R_aful,4),L_ukul,
     & L_udul,L_alul,L_uful,I_adul)
      !}
C FDA20_lamp.fgi( 106,  70):���������� ��������� �����������,20FDA21AE703QB06
      !{
      Call LIMIT_SWITCH_HANDLER(deltat,L_umul,R_omul,
     & REAL(R_apul,4),R_amul,REAL(R_imul,4),L_erul,
     & L_emul,L_irul,L_epul,I_ilul)
      !}
C FDA20_lamp.fgi( 106,  85):���������� ��������� �����������,20FDA21AE703QB05
      !{
      Call LIMIT_SWITCH_HANDLER(deltat,L_etul,R_atul,
     & REAL(R_itul,4),R_isul,REAL(R_usul,4),L_ovul,
     & L_osul,L_uvul,L_otul,I_urul)
      !}
C FDA20_lamp.fgi( 106, 100):���������� ��������� �����������,20FDA21AE703QB04
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ixul,L_abam,R_uxul,
     & REAL(R_ebam,4),L_ibam,L_exul,I_oxul)
      !}
C FDA20_lamp.fgi(  84, 102):���������� ������� ���������,20FDA21AB001BQ83
      !{
      Call DAT_DISCR_HANDLER(deltat,I_edam,L_udam,R_odam,
     & REAL(R_afam,4),L_efam,L_adam,I_idam)
      !}
C FDA20_lamp.fgi(  84, 116):���������� ������� ���������,20FDA21AB001BQ82
      !{
      Call DAT_DISCR_HANDLER(deltat,I_akam,L_okam,R_ikam,
     & REAL(R_ukam,4),L_alam,L_ufam,I_ekam)
      !}
C FDA20_lamp.fgi(  62, 116):���������� ������� ���������,20FDA21AE701BQ70
      !{
      Call DAT_DISCR_HANDLER(deltat,I_uxam,L_ibem,R_ebem,
     & REAL(R_obem,4),L_ubem,L_oxam,I_abem)
      !}
C FDA20_lamp.fgi( 236, 180):���������� ������� ���������,20FDA21AL003BQ56
      !{
      Call DAT_DISCR_HANDLER(deltat,I_odem,L_efem,R_afem,
     & REAL(R_ifem,4),L_ofem,L_idem,I_udem)
      !}
C FDA20_lamp.fgi( 236, 192):���������� ������� ���������,20FDA21AL003BQ55
      !{
      Call DAT_DISCR_HANDLER(deltat,I_odim,L_efim,R_afim,
     & REAL(R_ifim,4),L_ofim,L_idim,I_udim)
      !}
C FDA20_lamp.fgi( 238, 278):���������� ������� ���������,20FDA21AL002BQ26
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ikim,L_alim,R_ukim,
     & REAL(R_elim,4),L_ilim,L_ekim,I_okim)
      !}
C FDA20_lamp.fgi( 238, 290):���������� ������� ���������,20FDA21AL002BQ25
      !{
      Call DAT_DISCR_HANDLER(deltat,I_usom,L_itom,R_etom,
     & REAL(R_otom,4),L_utom,L_osom,I_atom)
      !}
C FDA20_lamp.fgi(  70, 222):���������� ������� ���������,20FDA21AL001BQ40
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ovom,L_exom,R_axom,
     & REAL(R_ixom,4),L_oxom,L_ivom,I_uvom)
      !}
C FDA20_lamp.fgi(  70, 234):���������� ������� ���������,20FDA21AL001BQ39
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ibum,L_adum,R_ubum,
     & REAL(R_edum,4),L_idum,L_ebum,I_obum)
      !}
C FDA20_lamp.fgi(  70, 264):���������� ������� ���������,20FDA21AL001BQ38
      !{
      Call DAT_DISCR_HANDLER(deltat,I_efum,L_ufum,R_ofum,
     & REAL(R_akum,4),L_ekum,L_afum,I_ifum)
      !}
C FDA20_lamp.fgi(  70, 250):���������� ������� ���������,20FDA21AL001BQ37
      L_(516) = (.NOT.L_ulap)
C FDA20_lamp.fgi( 269, 412):���
      !{
      Call DAT_DISCR_HANDLER(deltat,I_omap,L_epap,R_apap,
     & REAL(R_ipap,4),L_(516),L_imap,I_umap)
      !}
C FDA20_lamp.fgi( 282, 410):���������� ������� ���������,20FDA21AE702QB11
      L_(515) = (.NOT.L_idap)
C FDA20_lamp.fgi( 269, 438):���
      !{
      Call DAT_DISCR_HANDLER(deltat,I_efap,L_ufap,R_ofap,
     & REAL(R_akap,4),L_(515),L_afap,I_ifap)
      !}
C FDA20_lamp.fgi( 282, 436):���������� ������� ���������,20FDA21AE702QB19
      L_(514) = (.NOT.L_avum)
C FDA20_lamp.fgi( 269, 464):���
      !{
      Call DAT_DISCR_HANDLER(deltat,I_uvum,L_ixum,R_exum,
     & REAL(R_oxum,4),L_(514),L_ovum,I_axum)
      !}
C FDA20_lamp.fgi( 282, 462):���������� ������� ���������,20FDA21AE702QB84
      L_(521) = (.NOT.L_utep)
C FDA20_lamp.fgi( 269, 490):���
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ovep,L_exep,R_axep,
     & REAL(R_ixep,4),L_(521),L_ivep,I_uvep)
      !}
C FDA20_lamp.fgi( 282, 488):���������� ������� ���������,20FDA21AE702QB13
      L_(518) = (.NOT.L_oxap)
C FDA20_lamp.fgi( 215, 412):���
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ibep,L_adep,R_ubep,
     & REAL(R_edep,4),L_(518),L_ebep,I_obep)
      !}
C FDA20_lamp.fgi( 228, 410):���������� ������� ���������,20FDA21AE702QB07
      L_(517) = (.NOT.L_esap)
C FDA20_lamp.fgi( 215, 438):���
      !{
      Call DAT_DISCR_HANDLER(deltat,I_atap,L_otap,R_itap,
     & REAL(R_utap,4),L_(517),L_usap,I_etap)
      !}
C FDA20_lamp.fgi( 228, 436):���������� ������� ���������,20FDA21AE702QB09
      L_(520) = (.NOT.L_ipep)
C FDA20_lamp.fgi( 215, 464):���
      !{
      Call DAT_DISCR_HANDLER(deltat,I_erep,L_urep,R_orep,
     & REAL(R_asep,4),L_(520),L_arep,I_irep)
      !}
C FDA20_lamp.fgi( 228, 462):���������� ������� ���������,20FDA21AE702QB15
      L_(519) = (.NOT.L_akep)
C FDA20_lamp.fgi( 215, 490):���
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ukep,L_ilep,R_elep,
     & REAL(R_olep,4),L_(519),L_okep,I_alep)
      !}
C FDA20_lamp.fgi( 228, 488):���������� ������� ���������,20FDA21AE702QB17
      !{
      Call DAT_DISCR_HANDLER(deltat,I_atum,L_otum,R_itum,
     & REAL(R_utum,4),L_avum,L_usum,I_etum)
      !}
C FDA20_lamp.fgi( 282, 476):���������� ������� ���������,20FDA21AE702QB85
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ibap,L_adap,R_ubap,
     & REAL(R_edap,4),L_idap,L_ebap,I_obap)
      !}
C FDA20_lamp.fgi( 282, 450):���������� ������� ���������,20FDA21AE702QB20
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ukap,L_ilap,R_elap,
     & REAL(R_olap,4),L_ulap,L_okap,I_alap)
      !}
C FDA20_lamp.fgi( 282, 424):���������� ������� ���������,20FDA21AE702QB12
      !{
      Call DAT_DISCR_HANDLER(deltat,I_erap,L_urap,R_orap,
     & REAL(R_asap,4),L_esap,L_arap,I_irap)
      !}
C FDA20_lamp.fgi( 228, 450):���������� ������� ���������,20FDA21AE702QB10
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ovap,L_exap,R_axap,
     & REAL(R_ixap,4),L_oxap,L_ivap,I_uvap)
      !}
C FDA20_lamp.fgi( 228, 424):���������� ������� ���������,20FDA21AE702QB08
      !{
      Call DAT_DISCR_HANDLER(deltat,I_afep,L_ofep,R_ifep,
     & REAL(R_ufep,4),L_akep,L_udep,I_efep)
      !}
C FDA20_lamp.fgi( 228, 502):���������� ������� ���������,20FDA21AE702QB18
      !{
      Call DAT_DISCR_HANDLER(deltat,I_imep,L_apep,R_umep,
     & REAL(R_epep,4),L_ipep,L_emep,I_omep)
      !}
C FDA20_lamp.fgi( 228, 476):���������� ������� ���������,20FDA21AE702QB16
      !{
      Call DAT_DISCR_HANDLER(deltat,I_usep,L_itep,R_etep,
     & REAL(R_otep,4),L_utep,L_osep,I_atep)
      !}
C FDA20_lamp.fgi( 282, 502):���������� ������� ���������,20FDA21AE702QB14
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ebip,L_ubip,R_obip,
     & REAL(R_adip,4),L_edip,L_abip,I_ibip)
      !}
C FDA20_lamp.fgi( 228, 398):���������� ������� ���������,20FDA21AE702QB87
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ukip,L_ilip,R_elip,
     & REAL(R_olip,4),L_ulip,L_okip,I_alip)
      !}
C FDA20_lamp.fgi(  90, 344):���������� ������� ���������,20FDA21AL003BQ58
      !{
      Call DAT_DISCR_HANDLER(deltat,I_omip,L_epip,R_apip,
     & REAL(R_ipip,4),L_opip,L_imip,I_umip)
      !}
C FDA20_lamp.fgi(  90, 356):���������� ������� ���������,20FDA21AL003BQ57
      !{
      Call DAT_DISCR_HANDLER(deltat,I_irip,L_asip,R_urip,
     & REAL(R_esip,4),L_isip,L_erip,I_orip)
      !}
C FDA20_lamp.fgi(  90, 370):���������� ������� ���������,20FDA21AL002BQ30
      !{
      Call DAT_DISCR_HANDLER(deltat,I_etip,L_utip,R_otip,
     & REAL(R_avip,4),L_evip,L_atip,I_itip)
      !}
C FDA20_lamp.fgi(  90, 382):���������� ������� ���������,20FDA21AL002BQ29
      !{
      Call DAT_DISCR_HANDLER(deltat,I_itop,L_avop,R_utop,
     & REAL(R_evop,4),L_ivop,L_etop,I_otop)
      !}
C FDA20_lamp.fgi(  74, 466):���������� ������� ���������,20FDA21AL004BQ46
      !{
      Call DAT_DISCR_HANDLER(deltat,I_exop,L_uxop,R_oxop,
     & REAL(R_abup,4),L_ebup,L_axop,I_ixop)
      !}
C FDA20_lamp.fgi(  74, 454):���������� ������� ���������,20FDA21AL004BQ45
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ilup,L_amup,R_ulup,
     & REAL(R_emup,4),L_imup,L_elup,I_olup)
      !}
C FDA20_lamp.fgi(  74, 610):���������� ������� ���������,20FDA21AB001BQ81
      !{
      Call DAT_DISCR_HANDLER(deltat,I_epup,L_upup,R_opup,
     & REAL(R_arup,4),L_erup,L_apup,I_ipup)
      !}
C FDA20_lamp.fgi(  74, 622):���������� ������� ���������,20FDA21AB001BQ80
      !{
      Call DAT_DISCR_HANDLER(deltat,I_asup,L_osup,R_isup,
     & REAL(R_usup,4),L_atup,L_urup,I_esup)
      !}
C FDA20_lamp.fgi(  74, 650):���������� ������� ���������,20FDA21AB001BQ79
      !{
      Call DAT_DISCR_HANDLER(deltat,I_utup,L_ivup,R_evup,
     & REAL(R_ovup,4),L_uvup,L_otup,I_avup)
      !}
C FDA20_lamp.fgi(  74, 636):���������� ������� ���������,20FDA21AB001BQ78
      if(.NOT.L_omer.and..not.L0_uxup) then
         R0_ixup=R0_oxup
      else
         R0_ixup=max(R_(275)-deltat,0.0)
      endif
      L_(525)=R0_ixup.gt.0.0
      L0_uxup=.NOT.L_omer
C FDA20_lamp.fgi( 248, 621):������������  �� T
      if(.NOT.L_iber.and..not.L0_ibar) then
         R0_abar=R0_ebar
      else
         R0_abar=max(R_(276)-deltat,0.0)
      endif
      L_(524)=R0_abar.gt.0.0
      L0_ibar=.NOT.L_iber
C FDA20_lamp.fgi( 248, 613):������������  �� T
      L_uker=(L_(524).or.L_uker).and..not.(L_(525))
      L_(526)=.not.L_uker
C FDA20_lamp.fgi( 258, 617):RS �������
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ufer,L_iker,R_eker,
     & REAL(R_oker,4),L_uker,L_ofer,I_aker)
      !}
C FDA20_lamp.fgi( 274, 618):���������� ������� ���������,20FDA21AE701BQ66
      !{
      Call DAT_DISCR_HANDLER(deltat,I_eder,L_uder,R_oder,
     & REAL(R_afer,4),L_uker,L_ader,I_ider)
      !}
C FDA20_lamp.fgi( 274, 604):���������� ������� ���������,20FDA21AE701BQ67
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ubar,L_idar,R_edar,
     & REAL(R_odar,4),L_udar,L_obar,I_adar)
      !}
C FDA20_lamp.fgi( 154, 548):���������� ������� ���������,20FDA21AE701BQ75
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ofar,L_ekar,R_akar,
     & REAL(R_ikar,4),L_okar,L_ifar,I_ufar)
      !}
C FDA20_lamp.fgi( 154, 562):���������� ������� ���������,20FDA21AE701BQ74
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ilar,L_amar,R_ular,
     & REAL(R_emar,4),L_imar,L_elar,I_olar)
      !}
C FDA20_lamp.fgi( 154, 574):���������� ������� ���������,20FDA21AE701BQ73
      !{
      Call DAT_DISCR_HANDLER(deltat,I_epar,L_upar,R_opar,
     & REAL(R_arar,4),L_erar,L_apar,I_ipar)
      !}
C FDA20_lamp.fgi( 154, 588):���������� ������� ���������,20FDA21AE701BQ72
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ixar,L_aber,R_uxar,
     & REAL(R_eber,4),L_iber,L_exar,I_oxar)
      !}
C FDA20_lamp.fgi( 274, 592):���������� ������� ���������,20FDA21AE701BQ68
      !{
      Call DAT_DISCR_HANDLER(deltat,I_oler,L_emer,R_amer,
     & REAL(R_imer,4),L_omer,L_iler,I_uler)
      !}
C FDA20_lamp.fgi( 274, 630):���������� ������� ���������,20FDA21AE701BQ65
      !{
      Call DAT_DISCR_HANDLER(deltat,I_uter,L_iver,R_ever,
     & REAL(R_over,4),L_uver,L_oter,I_aver)
      !}
C FDA20_lamp.fgi( 154, 618):���������� ������� ���������,20FDA21AE701BQ77
      !{
      Call DAT_DISCR_HANDLER(deltat,I_oxer,L_ebir,R_abir,
     & REAL(R_ibir,4),L_obir,L_ixer,I_uxer)
      !}
C FDA20_lamp.fgi( 154, 630):���������� ������� ���������,20FDA21AE701BQ76
      !{
      Call DAT_DISCR_HANDLER(deltat,I_idir,L_afir,R_udir,
     & REAL(R_efir,4),L_ifir,L_edir,I_odir)
      !}
C FDA20_lamp.fgi( 210, 618):���������� ������� ���������,20FDA21AE701BQ62
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ekir,L_ukir,R_okir,
     & REAL(R_alir,4),L_elir,L_akir,I_ikir)
      !}
C FDA20_lamp.fgi( 210, 630):���������� ������� ���������,20FDA21AE701BQ61
      L_(530) = (.NOT.L_ubor).AND.(.NOT.L_ofor)
C FDA20_lamp.fgi(  67, 698):�
      !{
      Call DAT_DISCR_HANDLER(deltat,I_isir,L_atir,R_usir,
     & REAL(R_etir,4),L_(530),L_esir,I_osir)
      !}
C FDA20_lamp.fgi(  82, 696):���������� ������� ���������,20FDA21AE704BQ93
      !{
      Call DAT_DISCR_HANDLER(deltat,I_uxir,L_ibor,R_ebor,
     & REAL(R_obor,4),L_ubor,L_oxir,I_abor)
      !}
C FDA20_lamp.fgi(  82, 710):���������� ������� ���������,20FDA21AE704BQ94
      !{
      Call DAT_DISCR_HANDLER(deltat,I_odor,L_efor,R_afor,
     & REAL(R_ifor,4),L_ofor,L_idor,I_udor)
      !}
C FDA20_lamp.fgi(  82, 722):���������� ������� ���������,20FDA21AE704BQ92
      !{
      Call DAT_DISCR_HANDLER(deltat,I_epur,L_upur,R_opur,
     & REAL(R_arur,4),L_erur,L_apur,I_ipur)
      !}
C FDA20_lamp.fgi(  82, 808):���������� ������� ���������,20FDA21AB002BQ06
      !{
      Call DAT_DISCR_HANDLER(deltat,I_asur,L_osur,R_isur,
     & REAL(R_usur,4),L_atur,L_urur,I_esur)
      !}
C FDA20_lamp.fgi(  82, 820):���������� ������� ���������,20FDA21AB002BQ05
      R_(277) = 0.001
C FDA20_vent_log.fgi(  93,  78):��������� (RE4) (�������)
      R_isiv = R8_elas
C FDA20_vent_log.fgi(  96,  71):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_eriv,R_aviv,REAL(1,4)
     &,
     & REAL(R_uriv,4),REAL(R_asiv,4),
     & REAL(R_ariv,4),REAL(R_upiv,4),I_utiv,
     & REAL(R_osiv,4),L_usiv,REAL(R_ativ,4),L_etiv,L_itiv
     &,R_esiv,
     & REAL(R_oriv,4),REAL(R_iriv,4),L_otiv,REAL(R_isiv,4
     &))
      !}
C FDA20_vlv.fgi( 289, 110):���������� �������,20FDA21CU001XQ01
      R_amiv = R8_ilas
C FDA20_vent_log.fgi(  96,  75):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ukiv,R_opiv,REAL(1,4)
     &,
     & REAL(R_iliv,4),REAL(R_oliv,4),
     & REAL(R_okiv,4),REAL(R_ikiv,4),I_ipiv,
     & REAL(R_emiv,4),L_imiv,REAL(R_omiv,4),L_umiv,L_apiv
     &,R_uliv,
     & REAL(R_eliv,4),REAL(R_aliv,4),L_epiv,REAL(R_amiv,4
     &))
      !}
C FDA20_vlv.fgi( 316, 110):���������� �������,20FDA21CM001XQ01
      R_otix = R8_ulas
C FDA20_vent_log.fgi( 171,  29):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_isix,R_exix,REAL(1,4)
     &,
     & REAL(R_atix,4),REAL(R_etix,4),
     & REAL(R_esix,4),REAL(R_asix,4),I_axix,
     & REAL(R_utix,4),L_avix,REAL(R_evix,4),L_ivix,L_ovix
     &,R_itix,
     & REAL(R_usix,4),REAL(R_osix,4),L_uvix,REAL(R_otix,4
     &))
      !}
C FDA20_vlv.fgi( 346, 170):���������� �������,20FDA23CM001XQ01
      R_adox = R8_amas
C FDA20_vent_log.fgi( 171,  33):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_uxix,R_ofox,REAL(1,4)
     &,
     & REAL(R_ibox,4),REAL(R_obox,4),
     & REAL(R_oxix,4),REAL(R_ixix,4),I_ifox,
     & REAL(R_edox,4),L_idox,REAL(R_odox,4),L_udox,L_afox
     &,R_ubox,
     & REAL(R_ebox,4),REAL(R_abox,4),L_efox,REAL(R_adox,4
     &))
      !}
C FDA20_vlv.fgi( 316, 170):���������� �������,20FDA23CU001XQ01
      R_ivut = R8_emas
C FDA20_vent_log.fgi(  96, 109):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_etut,R_abav,REAL(1,4)
     &,
     & REAL(R_utut,4),REAL(R_avut,4),
     & REAL(R_atut,4),REAL(R_usut,4),I_uxut,
     & REAL(R_ovut,4),L_uvut,REAL(R_axut,4),L_exut,L_ixut
     &,R_evut,
     & REAL(R_otut,4),REAL(R_itut,4),L_oxut,REAL(R_ivut,4
     &))
      !}
C FDA20_vlv.fgi( 465, 155):���������� �������,20FDA21CF102XQ01
      R_(278) = 60000
C FDA20_vent_log.fgi( 169,  59):��������� (RE4) (�������)
      if(R8_imas.ge.0.0) then
         R_(279)=R8_omas/max(R8_imas,1.0e-10)
      else
         R_(279)=R8_omas/min(R8_imas,-1.0e-10)
      endif
C FDA20_vent_log.fgi( 159,  62):�������� ����������
      R_akax = R_(279) * R_(278)
C FDA20_vent_log.fgi( 172,  61):����������
      !{
      Call DAT_ANA_HANDLER(deltat,R_udax,R_olax,REAL(1,4)
     &,
     & REAL(R_ifax,4),REAL(R_ofax,4),
     & REAL(R_odax,4),REAL(R_idax,4),I_ilax,
     & REAL(R_ekax,4),L_ikax,REAL(R_okax,4),L_ukax,L_alax
     &,R_ufax,
     & REAL(R_efax,4),REAL(R_afax,4),L_elax,REAL(R_akax,4
     &))
      !}
C FDA20_vlv.fgi( 316, 140):���������� �������,20FDA23CF106XQ01
      R_(280) = 60000
C FDA20_vent_log.fgi( 169,  75):��������� (RE4) (�������)
      if(R8_umas.ge.0.0) then
         R_(281)=R8_apas/max(R8_umas,1.0e-10)
      else
         R_(281)=R8_apas/min(R8_umas,-1.0e-10)
      endif
C FDA20_vent_log.fgi( 159,  78):�������� ����������
      R_ipax = R_(281) * R_(280)
C FDA20_vent_log.fgi( 172,  77):����������
      !{
      Call DAT_ANA_HANDLER(deltat,R_emax,R_asax,REAL(1,4)
     &,
     & REAL(R_umax,4),REAL(R_apax,4),
     & REAL(R_amax,4),REAL(R_ulax,4),I_urax,
     & REAL(R_opax,4),L_upax,REAL(R_arax,4),L_erax,L_irax
     &,R_epax,
     & REAL(R_omax,4),REAL(R_imax,4),L_orax,REAL(R_ipax,4
     &))
      !}
C FDA20_vlv.fgi( 289, 140):���������� �������,20FDA23CF105XQ01
      R_(282) = 60000
C FDA20_vent_log.fgi( 169,  90):��������� (RE4) (�������)
      if(R8_epas.ge.0.0) then
         R_(283)=R8_ipas/max(R8_epas,1.0e-10)
      else
         R_(283)=R8_ipas/min(R8_epas,-1.0e-10)
      endif
C FDA20_vent_log.fgi( 159,  93):�������� ����������
      R_utax = R_(283) * R_(282)
C FDA20_vent_log.fgi( 172,  92):����������
      !{
      Call DAT_ANA_HANDLER(deltat,R_osax,R_ixax,REAL(1,4)
     &,
     & REAL(R_etax,4),REAL(R_itax,4),
     & REAL(R_isax,4),REAL(R_esax,4),I_exax,
     & REAL(R_avax,4),L_evax,REAL(R_ivax,4),L_ovax,L_uvax
     &,R_otax,
     & REAL(R_atax,4),REAL(R_usax,4),L_axax,REAL(R_utax,4
     &))
      !}
C FDA20_vlv.fgi( 408, 155):���������� �������,20FDA23CF104XQ01
      R_(284) = 60000
C FDA20_vent_log.fgi( 169, 102):��������� (RE4) (�������)
      if(R8_opas.ge.0.0) then
         R_(285)=R8_upas/max(R8_opas,1.0e-10)
      else
         R_(285)=R8_upas/min(R8_opas,-1.0e-10)
      endif
C FDA20_vent_log.fgi( 159, 105):�������� ����������
      R_edex = R_(285) * R_(284)
C FDA20_vent_log.fgi( 172, 104):����������
      !{
      Call DAT_ANA_HANDLER(deltat,R_abex,R_ufex,REAL(1,4)
     &,
     & REAL(R_obex,4),REAL(R_ubex,4),
     & REAL(R_uxax,4),REAL(R_oxax,4),I_ofex,
     & REAL(R_idex,4),L_odex,REAL(R_udex,4),L_afex,L_efex
     &,R_adex,
     & REAL(R_ibex,4),REAL(R_ebex,4),L_ifex,REAL(R_edex,4
     &))
      !}
C FDA20_vlv.fgi( 377, 155):���������� �������,20FDA23CF103XQ01
      !��������� R_(286) = FDA20_vent_logC?? /0.0005/
      R_(286)=R0_eras
C FDA20_vent_log.fgi( 169, 209):���������
      L_(531)=R8_iras.gt.R_(286)
C FDA20_vent_log.fgi( 173, 210):���������� >
      !��������� R_(288) = FDA20_vent_logC?? /0.0/
      R_(288)=R0_oras
C FDA20_vent_log.fgi( 181, 216):���������
      !��������� R_(287) = FDA20_vent_logC?? /500/
      R_(287)=R0_uras
C FDA20_vent_log.fgi( 181, 214):���������
      if(L_(531)) then
         R_(296)=R_(287)
      else
         R_(296)=R_(288)
      endif
C FDA20_vent_log.fgi( 184, 214):���� RE IN LO CH7
      R_(292) = 1.0
C FDA20_vent_log.fgi(  92, 210):��������� (RE4) (�������)
      R_(293) = 0.0
C FDA20_vent_log.fgi(  92, 212):��������� (RE4) (�������)
      !��������� R_(290) = FDA20_vent_logC?? /2.6/
      R_(290)=R0_esas
C FDA20_vent_log.fgi(  88, 233):���������
      !��������� R_(294) = FDA20_vent_logC?? /1/
      R_(294)=R0_itas
C FDA20_vent_log.fgi( 183, 232):���������
      L_(535)=R8_oxas.lt.R_(294)
C FDA20_vent_log.fgi( 187, 233):���������� <
      L_utas=L_otas.or.(L_utas.and..not.(L_(535)))
      L_(536)=.not.L_utas
C FDA20_vent_log.fgi( 217, 235):RS �������
      L_evas=L_utas
C FDA20_vent_log.fgi( 232, 237):������,20FDA23CW001_OUT
      !��������� R_(295) = FDA20_vent_logC?? /-50000/
      R_(295)=R0_avas
C FDA20_vent_log.fgi( 212, 213):���������
      if(L_evas) then
         R8_ivas=R_(295)
      else
         R8_ivas=R_(296)
      endif
C FDA20_vent_log.fgi( 215, 213):���� RE IN LO CH7
      if(R8_oxas.le.R0_exas) then
         R8_ixas=R0_axas
      elseif(R8_oxas.gt.R0_uvas) then
         R8_ixas=R0_ovas
      else
         R8_ixas=R0_axas+(R8_oxas-(R0_exas))*(R0_ovas-(R0_axas
     &))/(R0_uvas-(R0_exas))
      endif
C FDA20_vent_log.fgi( 169,  48):��������������� ���������
      L_(532)=R8_ixas.gt.R_(290)
C FDA20_vent_log.fgi(  92, 234):���������� >
      L_(533) = L_(532).OR.L_aras
C FDA20_vent_log.fgi( 106, 233):���
      L_osas=L_isas.or.(L_osas.and..not.(L_(533)))
      L_(534)=.not.L_osas
C FDA20_vent_log.fgi( 122, 236):RS �������
      L_usas=L_osas
C FDA20_vent_log.fgi( 136, 238):������,FDA23dust_start
      if(L_usas) then
         R_(291)=R_(292)
      else
         R_(291)=R_(293)
      endif
C FDA20_vent_log.fgi(  95, 211):���� RE IN LO CH7
      R8_etas=(R0_asas*R_(289)+deltat*R_(291))/(R0_asas+deltat
     &)
C FDA20_vent_log.fgi( 105, 212):�������������� �����  
      R8_atas=R8_etas
C FDA20_vent_log.fgi( 123, 212):������,20FDA23AN002KA11
      R_(297) = 60
C FDA20_vent_log.fgi(  94,  91):��������� (RE4) (�������)
      R_(298) = 1000
C FDA20_vent_log.fgi(  87,  94):��������� (RE4) (�������)
      R_(299) = R8_uxas * R_(298)
C FDA20_vent_log.fgi(  89,  96):����������
      if(R_(297).ge.0.0) then
         R_odiv=R_(299)/max(R_(297),1.0e-10)
      else
         R_odiv=R_(299)/min(R_(297),-1.0e-10)
      endif
C FDA20_vent_log.fgi(  98,  94):�������� ����������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ibiv,R_ekiv,REAL(1,4)
     &,
     & REAL(R_adiv,4),REAL(R_ediv,4),
     & REAL(R_ebiv,4),REAL(R_abiv,4),I_akiv,
     & REAL(R_udiv,4),L_afiv,REAL(R_efiv,4),L_ifiv,L_ofiv
     &,R_idiv,
     & REAL(R_ubiv,4),REAL(R_obiv,4),L_ufiv,REAL(R_odiv,4
     &))
      !}
C FDA20_vlv.fgi( 346, 110):���������� �������,20FDA21CF001XQ01
      R_ixex = R8_abes
C FDA20_vent_log.fgi( 169, 115):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_evex,R_adix,REAL(1,4)
     &,
     & REAL(R_uvex,4),REAL(R_axex,4),
     & REAL(R_avex,4),REAL(R_utex,4),I_ubix,
     & REAL(R_oxex,4),L_uxex,REAL(R_abix,4),L_ebix,L_ibix
     &,R_exex,
     & REAL(R_ovex,4),REAL(R_ivex,4),L_obix,REAL(R_ixex,4
     &))
      !}
C FDA20_vlv.fgi( 289, 155):���������� �������,20FDA23CF001XQ01
      R_udav = R8_ebes
C FDA20_vent_log.fgi(  96, 115):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_obav,R_ikav,REAL(1,4)
     &,
     & REAL(R_edav,4),REAL(R_idav,4),
     & REAL(R_ibav,4),REAL(R_ebav,4),I_ekav,
     & REAL(R_afav,4),L_efav,REAL(R_ifav,4),L_ofav,L_ufav
     &,R_odav,
     & REAL(R_adav,4),REAL(R_ubav,4),L_akav,REAL(R_udav,4
     &))
      !}
C FDA20_vlv.fgi( 465, 169):���������� �������,20FDA21CF101XQ01
      R_epix = R8_ibes
C FDA20_vent_log.fgi( 176, 165):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_amix,R_urix,REAL(1,4)
     &,
     & REAL(R_omix,4),REAL(R_umix,4),
     & REAL(R_ulix,4),REAL(R_olix,4),I_orix,
     & REAL(R_ipix,4),L_opix,REAL(R_upix,4),L_arix,L_erix
     &,R_apix,
     & REAL(R_imix,4),REAL(R_emix,4),L_irix,REAL(R_epix,4
     &))
      !}
C FDA20_vlv.fgi( 377, 170):���������� �������,20FDA23CP101XQ01
      R_opov = R8_obes
C FDA20_vent_log.fgi( 102, 165):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_imov,R_esov,REAL(1,4)
     &,
     & REAL(R_apov,4),REAL(R_epov,4),
     & REAL(R_emov,4),REAL(R_amov,4),I_asov,
     & REAL(R_upov,4),L_arov,REAL(R_erov,4),L_irov,L_orov
     &,R_ipov,
     & REAL(R_umov,4),REAL(R_omov,4),L_urov,REAL(R_opov,4
     &))
      !}
C FDA20_vlv.fgi( 346, 125):���������� �������,20FDA21CP002XQ01
      R_(301) = R8_ubes + (-R8_ufes)
C FDA20_vent_log.fgi( 161, 130):��������
      R_(300) = 1e-3
C FDA20_vent_log.fgi( 169, 127):��������� (RE4) (�������)
      R_(302) = R_(301) * R_(300)
C FDA20_vent_log.fgi( 172, 129):����������
      R_iduv = R_(302)
C FDA20_vent_log.fgi( 176, 129):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ebuv,R_akuv,REAL(1,4)
     &,
     & REAL(R_ubuv,4),REAL(R_aduv,4),
     & REAL(R_abuv,4),REAL(R_uxov,4),I_ufuv,
     & REAL(R_oduv,4),L_uduv,REAL(R_afuv,4),L_efuv,L_ifuv
     &,R_eduv,
     & REAL(R_obuv,4),REAL(R_ibuv,4),L_ofuv,REAL(R_iduv,4
     &))
      !}
C FDA20_vlv.fgi( 289, 125):���������� �������,20FDA23CP105XQ01
      R_(304) = R8_ades + (-R8_ufes)
C FDA20_vent_log.fgi( 161, 140):��������
      R_(303) = 1e-3
C FDA20_vent_log.fgi( 169, 137):��������� (RE4) (�������)
      R_(305) = R_(304) * R_(303)
C FDA20_vent_log.fgi( 172, 139):����������
      R_uluv = R_(305)
C FDA20_vent_log.fgi( 176, 139):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_okuv,R_ipuv,REAL(1,4)
     &,
     & REAL(R_eluv,4),REAL(R_iluv,4),
     & REAL(R_ikuv,4),REAL(R_ekuv,4),I_epuv,
     & REAL(R_amuv,4),L_emuv,REAL(R_imuv,4),L_omuv,L_umuv
     &,R_oluv,
     & REAL(R_aluv,4),REAL(R_ukuv,4),L_apuv,REAL(R_uluv,4
     &))
      !}
C FDA20_vlv.fgi( 408, 140):���������� �������,20FDA23CP104XQ01
      R_(307) = R8_edes + (-R8_ufes)
C FDA20_vent_log.fgi( 161, 148):��������
      R_(306) = 1e-3
C FDA20_vent_log.fgi( 169, 145):��������� (RE4) (�������)
      R_(308) = R_(307) * R_(306)
C FDA20_vent_log.fgi( 172, 147):����������
      R_esuv = R_(308)
C FDA20_vent_log.fgi( 176, 147):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_aruv,R_utuv,REAL(1,4)
     &,
     & REAL(R_oruv,4),REAL(R_uruv,4),
     & REAL(R_upuv,4),REAL(R_opuv,4),I_otuv,
     & REAL(R_isuv,4),L_osuv,REAL(R_usuv,4),L_atuv,L_etuv
     &,R_asuv,
     & REAL(R_iruv,4),REAL(R_eruv,4),L_ituv,REAL(R_esuv,4
     &))
      !}
C FDA20_vlv.fgi( 377, 140):���������� �������,20FDA23CP103XQ01
      R_(310) = R8_ides + (-R8_ufes)
C FDA20_vent_log.fgi( 161, 157):��������
      R_(309) = 1e-3
C FDA20_vent_log.fgi( 169, 154):��������� (RE4) (�������)
      R_(311) = R_(310) * R_(309)
C FDA20_vent_log.fgi( 172, 156):����������
      R_oxuv = R_(311)
C FDA20_vent_log.fgi( 176, 156):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ivuv,R_edax,REAL(1,4)
     &,
     & REAL(R_axuv,4),REAL(R_exuv,4),
     & REAL(R_evuv,4),REAL(R_avuv,4),I_adax,
     & REAL(R_uxuv,4),L_abax,REAL(R_ebax,4),L_ibax,L_obax
     &,R_ixuv,
     & REAL(R_uvuv,4),REAL(R_ovuv,4),L_ubax,REAL(R_oxuv,4
     &))
      !}
C FDA20_vlv.fgi( 346, 140):���������� �������,20FDA23CP102XQ01
      R_(313) = R8_odes + (-R8_ufes)
C FDA20_vent_log.fgi(  87, 130):��������
      R_(312) = 1e-3
C FDA20_vent_log.fgi(  95, 127):��������� (RE4) (�������)
      R_(314) = R_(313) * R_(312)
C FDA20_vent_log.fgi(  98, 129):����������
      R_abev = R_(314)
C FDA20_vent_log.fgi( 102, 129):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_uvav,R_odev,REAL(1,4)
     &,
     & REAL(R_ixav,4),REAL(R_oxav,4),
     & REAL(R_ovav,4),REAL(R_ivav,4),I_idev,
     & REAL(R_ebev,4),L_ibev,REAL(R_obev,4),L_ubev,L_adev
     &,R_uxav,
     & REAL(R_exav,4),REAL(R_axav,4),L_edev,REAL(R_abev,4
     &))
      !}
C FDA20_vlv.fgi( 437, 110):���������� �������,20FDA21CP110XQ01
      R_(316) = R8_udes + (-R8_ufes)
C FDA20_vent_log.fgi(  87, 140):��������
      R_(315) = 1e-3
C FDA20_vent_log.fgi(  95, 137):��������� (RE4) (�������)
      R_(317) = R_(316) * R_(315)
C FDA20_vent_log.fgi(  98, 139):����������
      R_ikev = R_(317)
C FDA20_vent_log.fgi( 102, 139):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_efev,R_amev,REAL(1,4)
     &,
     & REAL(R_ufev,4),REAL(R_akev,4),
     & REAL(R_afev,4),REAL(R_udev,4),I_ulev,
     & REAL(R_okev,4),L_ukev,REAL(R_alev,4),L_elev,L_ilev
     &,R_ekev,
     & REAL(R_ofev,4),REAL(R_ifev,4),L_olev,REAL(R_ikev,4
     &))
      !}
C FDA20_vlv.fgi( 437, 125):���������� �������,20FDA21CP109XQ01
      R_(319) = R8_afes + (-R8_ufes)
C FDA20_vent_log.fgi(  87, 148):��������
      R_(318) = 1e-3
C FDA20_vent_log.fgi(  95, 145):��������� (RE4) (�������)
      R_(320) = R_(319) * R_(318)
C FDA20_vent_log.fgi(  98, 147):����������
      R_upev = R_(320)
C FDA20_vent_log.fgi( 102, 147):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_omev,R_isev,REAL(1,4)
     &,
     & REAL(R_epev,4),REAL(R_ipev,4),
     & REAL(R_imev,4),REAL(R_emev,4),I_esev,
     & REAL(R_arev,4),L_erev,REAL(R_irev,4),L_orev,L_urev
     &,R_opev,
     & REAL(R_apev,4),REAL(R_umev,4),L_asev,REAL(R_upev,4
     &))
      !}
C FDA20_vlv.fgi( 408, 110):���������� �������,20FDA21CP108XQ01
      R_(322) = R8_efes + (-R8_ufes)
C FDA20_vent_log.fgi(  87, 157):��������
      R_(321) = 1e-3
C FDA20_vent_log.fgi(  95, 154):��������� (RE4) (�������)
      R_(323) = R_(322) * R_(321)
C FDA20_vent_log.fgi(  98, 156):����������
      R_evev = R_(323)
C FDA20_vent_log.fgi( 102, 156):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_atev,R_uxev,REAL(1,4)
     &,
     & REAL(R_otev,4),REAL(R_utev,4),
     & REAL(R_usev,4),REAL(R_osev,4),I_oxev,
     & REAL(R_ivev,4),L_ovev,REAL(R_uvev,4),L_axev,L_exev
     &,R_avev,
     & REAL(R_itev,4),REAL(R_etev,4),L_ixev,REAL(R_evev,4
     &))
      !}
C FDA20_vlv.fgi( 377, 110):���������� �������,20FDA21CP107XQ01
      R_ufix = R8_ufes + (-R8_ifes)
C FDA20_vent_log.fgi( 161, 181):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_odix,R_ilix,REAL(1,4)
     &,
     & REAL(R_efix,4),REAL(R_ifix,4),
     & REAL(R_idix,4),REAL(R_edix,4),I_elix,
     & REAL(R_akix,4),L_ekix,REAL(R_ikix,4),L_okix,L_ukix
     &,R_ofix,
     & REAL(R_afix,4),REAL(R_udix,4),L_alix,REAL(R_ufix,4
     &))
      !}
C FDA20_vlv.fgi( 408, 170):���������� �������,20FDA23CP001XQ01
      R_okut = R8_ufes + (-R8_ofes)
C FDA20_vent_log.fgi(  87, 181):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ifut,R_emut,REAL(1,4)
     &,
     & REAL(R_akut,4),REAL(R_ekut,4),
     & REAL(R_efut,4),REAL(R_afut,4),I_amut,
     & REAL(R_ukut,4),L_alut,REAL(R_elut,4),L_ilut,L_olut
     &,R_ikut,
     & REAL(R_ufut,4),REAL(R_ofut,4),L_ulut,REAL(R_okut,4
     &))
      !}
C FDA20_vlv.fgi( 465, 110):���������� �������,20FDA21CP001XQ01
      !��������� R_(327) = FDA20_vent_logC?? /0.0/
      R_(327)=R0_okes
C FDA20_vent_log.fgi( 164, 272):���������
      !��������� R_(326) = FDA20_vent_logC?? /0.001/
      R_(326)=R0_ukes
C FDA20_vent_log.fgi( 164, 270):���������
      !��������� R_(331) = FDA20_vent_logC?? /0.0/
      R_(331)=R0_oles
C FDA20_vent_log.fgi(  86, 272):���������
      !��������� R_(330) = FDA20_vent_logC?? /0.001/
      R_(330)=R0_ules
C FDA20_vent_log.fgi(  86, 270):���������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ebis,R_akis,REAL(1,4)
     &,
     & REAL(R_ubis,4),REAL(R_adis,4),
     & REAL(R_abis,4),REAL(R_uxes,4),I_ufis,
     & REAL(R_odis,4),L_udis,REAL(R_afis,4),L_efis,L_ifis
     &,R_edis,
     & REAL(R_obis,4),REAL(R_ibis,4),L_ofis,REAL(R_idis,4
     &))
      !}
C FDA20_vlv.fgi( 289,  56):���������� �������,20FDA22CF002XQ01
      !{
      Call DAT_ANA_HANDLER(deltat,R_okis,R_ipis,REAL(1,4)
     &,
     & REAL(R_elis,4),REAL(R_ilis,4),
     & REAL(R_ikis,4),REAL(R_ekis,4),I_epis,
     & REAL(R_amis,4),L_emis,REAL(R_imis,4),L_omis,L_umis
     &,R_olis,
     & REAL(R_alis,4),REAL(R_ukis,4),L_apis,REAL(R_ulis,4
     &))
      !}
C FDA20_vlv.fgi( 377,  71):���������� �������,20FDA22CW002XQ01
      !{
      Call DAT_ANA_HANDLER(deltat,R_aris,R_utis,REAL(1,4)
     &,
     & REAL(R_oris,4),REAL(R_uris,4),
     & REAL(R_upis,4),REAL(R_opis,4),I_otis,
     & REAL(R_isis,4),L_osis,REAL(R_usis,4),L_atis,L_etis
     &,R_asis,
     & REAL(R_iris,4),REAL(R_eris,4),L_itis,REAL(R_esis,4
     &))
      !}
C FDA20_vlv.fgi( 345,  71):���������� �������,20FDA22CW001XQ01
      !{
      Call DAT_ANA_HANDLER(deltat,R_udos,R_olos,REAL(1,4)
     &,
     & REAL(R_ifos,4),REAL(R_ofos,4),
     & REAL(R_odos,4),REAL(R_idos,4),I_ilos,
     & REAL(R_ekos,4),L_ikos,REAL(R_okos,4),L_ukos,L_alos
     &,R_ufos,
     & REAL(R_efos,4),REAL(R_afos,4),L_elos,REAL(R_akos,4
     &))
      !}
C FDA20_vlv.fgi( 289,  71):���������� �������,20FDA22CF001XQ01
      !{
      Call DAT_ANA_HANDLER(deltat,R_emos,R_asos,REAL(1,4)
     &,
     & REAL(R_umos,4),REAL(R_apos,4),
     & REAL(R_amos,4),REAL(R_ulos,4),I_uros,
     & REAL(R_opos,4),L_upos,REAL(R_aros,4),L_eros,L_iros
     &,R_epos,
     & REAL(R_omos,4),REAL(R_imos,4),L_oros,REAL(R_ipos,4
     &))
      !}
C FDA20_vlv.fgi( 377,  86):���������� �������,20FDA22CU001XQ01
      !{
      Call BVALVE2_MAN(deltat,REAL(R_edus,4),R8_axos,I_okus
     &,I_elus,I_ikus,C8_uxos,
     & I_alus,R_obus,R_ibus,R_atos,REAL(R_itos,4),
     & R_evos,REAL(R_ovos,4),R_otos,
     & REAL(R_avos,4),I_akus,I_ilus,I_ukus,I_ufus,L_uvos,L_ulus
     &,
     & L_umus,L_ivos,L_oxos,L_ixos,L_emus,
     & L_utos,L_etos,L_ebus,L_ipus,L_ubus,L_adus,
     & REAL(R8_iseke,8),REAL(1.0,4),R8_ebike,L_esos,L_isos
     &,L_amus,I_olus,
     & L_apus,R8_exos,R_ofus,REAL(R_abus,4),L_epus,L_osos
     &,L_opus,L_usos,
     & L_idus,L_odus,L_udus,L_efus,L_ifus,L_afus)
      !}

      if(L_ifus.or.L_efus.or.L_afus.or.L_udus.or.L_odus.or.L_idus
     &) then      
                  I_ekus = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_ekus = z'40000000'
      endif
C FDA20_vlv.fgi( 161, 124):���� ���������� �������� �������� ��� 2,20FDA22AA004
      !{
      Call BVALVE2_MAN(deltat,REAL(R_uxus,4),R8_otus,I_efat
     &,I_ufat,I_afat,C8_ivus,
     & I_ofat,R_exus,R_axus,R_orus,REAL(R_asus,4),
     & R_usus,REAL(R_etus,4),R_esus,
     & REAL(R_osus,4),I_odat,I_akat,I_ifat,I_idat,L_itus,L_ikat
     &,
     & L_ilat,L_atus,L_evus,L_avus,L_ukat,
     & L_isus,L_urus,L_uvus,L_amat,L_ixus,L_oxus,
     & REAL(R8_iseke,8),REAL(1.0,4),R8_ebike,L_upus,L_arus
     &,L_okat,I_ekat,
     & L_olat,R8_utus,R_edat,REAL(R_ovus,4),L_ulat,L_erus
     &,L_emat,L_irus,
     & L_abat,L_ebat,L_ibat,L_ubat,L_adat,L_obat)
      !}

      if(L_adat.or.L_ubat.or.L_obat.or.L_ibat.or.L_ebat.or.L_abat
     &) then      
                  I_udat = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_udat = z'40000000'
      endif
C FDA20_vlv.fgi( 145, 124):���� ���������� �������� �������� ��� 2,20FDA22AA003
      !{
      Call BVALVE2_MAN(deltat,REAL(R_ivat,4),R8_esat,I_ubet
     &,I_idet,I_obet,C8_atat,
     & I_edet,R_utat,R_otat,R_epat,REAL(R_opat,4),
     & R_irat,REAL(R_urat,4),R_upat,
     & REAL(R_erat,4),I_ebet,I_odet,I_adet,I_abet,L_asat,L_afet
     &,
     & L_aket,L_orat,L_usat,L_osat,L_ifet,
     & L_arat,L_ipat,L_itat,L_oket,L_avat,L_evat,
     & REAL(R8_iseke,8),REAL(1.0,4),R8_ebike,L_imat,L_omat
     &,L_efet,I_udet,
     & L_eket,R8_isat,R_uxat,REAL(R_etat,4),L_iket,L_umat
     &,L_uket,L_apat,
     & L_ovat,L_uvat,L_axat,L_ixat,L_oxat,L_exat)
      !}

      if(L_oxat.or.L_ixat.or.L_exat.or.L_axat.or.L_uvat.or.L_ovat
     &) then      
                  I_ibet = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_ibet = z'40000000'
      endif
C FDA20_vlv.fgi( 129, 124):���� ���������� �������� �������� ��� 2,20FDA22AA002
      !{
      Call BVALVE2_MAN(deltat,REAL(R_atet,4),R8_upet,I_ixet
     &,I_abit,I_exet,C8_oret,
     & I_uxet,R_iset,R_eset,R_ulet,REAL(R_emet,4),
     & R_apet,REAL(R_ipet,4),R_imet,
     & REAL(R_umet,4),I_uvet,I_ebit,I_oxet,I_ovet,L_opet,L_obit
     &,
     & L_odit,L_epet,L_iret,L_eret,L_adit,
     & L_omet,L_amet,L_aset,L_efit,L_oset,L_uset,
     & REAL(R8_iseke,8),REAL(1.0,4),R8_ebike,L_alet,L_elet
     &,L_ubit,I_ibit,
     & L_udit,R8_aret,R_ivet,REAL(R_uret,4),L_afit,L_ilet
     &,L_ifit,L_olet,
     & L_etet,L_itet,L_otet,L_avet,L_evet,L_utet)
      !}

      if(L_evet.or.L_avet.or.L_utet.or.L_otet.or.L_itet.or.L_etet
     &) then      
                  I_axet = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_axet = z'40000000'
      endif
C FDA20_vlv.fgi( 113, 124):���� ���������� �������� �������� ��� 2,20FDA22AA001
      !{
      Call DAT_ANA_HANDLER(deltat,R_ubot,R_okot,REAL(1,4)
     &,
     & REAL(R_idot,4),REAL(R_odot,4),
     & REAL(R_obot,4),REAL(R_ibot,4),I_ikot,
     & REAL(R_efot,4),L_ifot,REAL(R_ofot,4),L_ufot,L_akot
     &,R_udot,
     & REAL(R_edot,4),REAL(R_adot,4),L_ekot,REAL(R_afot,4
     &))
      !}
C FDA20_vlv.fgi( 345,  86):���������� �������,20FDA22CM001XQ01
      !{
      Call DAT_ANA_HANDLER(deltat,R_elot,R_arot,REAL(1,4)
     &,
     & REAL(R_ulot,4),REAL(R_amot,4),
     & REAL(R_alot,4),REAL(R_ukot,4),I_upot,
     & REAL(R_omot,4),L_umot,REAL(R_apot,4),L_epot,L_ipot
     &,R_emot,
     & REAL(R_olot,4),REAL(R_ilot,4),L_opot,REAL(R_imot,4
     &))
      !}
C FDA20_vlv.fgi( 316,  86):���������� �������,20FDA22CP002XQ01
      !{
      Call DAT_ANA_HANDLER(deltat,R_orot,R_ivot,REAL(1,4)
     &,
     & REAL(R_esot,4),REAL(R_isot,4),
     & REAL(R_irot,4),REAL(R_erot,4),I_evot,
     & REAL(R_atot,4),L_etot,REAL(R_itot,4),L_otot,L_utot
     &,R_osot,
     & REAL(R_asot,4),REAL(R_urot,4),L_avot,REAL(R_usot,4
     &))
      !}
C FDA20_vlv.fgi( 289,  86):���������� �������,20FDA22CP001XQ01
      !{
      Call DAT_ANA_HANDLER(deltat,R_axot,R_udut,REAL(1,4)
     &,
     & REAL(R_oxot,4),REAL(R_uxot,4),
     & REAL(R_uvot,4),REAL(R_ovot,4),I_odut,
     & REAL(R_ibut,4),L_obut,REAL(R_ubut,4),L_adut,L_edut
     &,R_abut,
     & REAL(R_ixot,4),REAL(R_exot,4),L_idut,REAL(R_ebut,4
     &))
      !}
C FDA20_vlv.fgi( 465, 140):���������� �������,20FDA23CW002XQ01
      !{
      Call DAT_ANA_HANDLER(deltat,R_umut,R_osut,REAL(1,4)
     &,
     & REAL(R_iput,4),REAL(R_oput,4),
     & REAL(R_omut,4),REAL(R_imut,4),I_isut,
     & REAL(R_erut,4),L_irut,REAL(R_orut,4),L_urut,L_asut
     &,R_uput,
     & REAL(R_eput,4),REAL(R_aput,4),L_esut,REAL(R_arut,4
     &))
      !}
C FDA20_vlv.fgi( 437, 170):���������� �������,20FDA21CT001XQ01
      !{
      Call DAT_ANA_HANDLER(deltat,R_oviv,R_idov,REAL(1,4)
     &,
     & REAL(R_exiv,4),REAL(R_ixiv,4),
     & REAL(R_iviv,4),REAL(R_eviv,4),I_edov,
     & REAL(R_abov,4),L_ebov,REAL(R_ibov,4),L_obov,L_ubov
     &,R_oxiv,
     & REAL(R_axiv,4),REAL(R_uviv,4),L_adov,REAL(R_uxiv,4
     &))
      !}
C FDA20_vlv.fgi( 408, 125):���������� �������,20FDA21CW002XQ01
      !{
      Call DAT_ANA_HANDLER(deltat,R_ikex,R_epex,REAL(1,4)
     &,
     & REAL(R_alex,4),REAL(R_elex,4),
     & REAL(R_ekex,4),REAL(R_akex,4),I_apex,
     & REAL(R_ulex,4),L_amex,REAL(R_emex,4),L_imex,L_omex
     &,R_ilex,
     & REAL(R_ukex,4),REAL(R_okex,4),L_umex,REAL(R_olex,4
     &))
      !}
C FDA20_vlv.fgi( 346, 155):���������� �������,20FDA23CW001XQ01
      !{
      Call DAT_ANA_HANDLER(deltat,R_upex,R_otex,REAL(1,4)
     &,
     & REAL(R_irex,4),REAL(R_orex,4),
     & REAL(R_opex,4),REAL(R_ipex,4),I_itex,
     & REAL(R_esex,4),L_isex,REAL(R_osex,4),L_usex,L_atex
     &,R_urex,
     & REAL(R_erex,4),REAL(R_arex,4),L_etex,REAL(R_asex,4
     &))
      !}
C FDA20_vlv.fgi( 316, 155):���������� �������,20FDA23CT001XQ01
      C8_opike = 'regim5'
C FDA20_logic.fgi(  36, 144):��������� ���������� CH8 (�������)
      call chcomp(C8_utike,C8_opike,L_(559))
C FDA20_logic.fgi(  41, 145):���������� ���������
      C8_upike = 'regim4'
C FDA20_logic.fgi(  36, 150):��������� ���������� CH8 (�������)
      call chcomp(C8_utike,C8_upike,L_(560))
C FDA20_logic.fgi(  41, 151):���������� ���������
      C8_arike = 'regim3'
C FDA20_logic.fgi(  36, 156):��������� ���������� CH8 (�������)
      call chcomp(C8_utike,C8_arike,L_(561))
C FDA20_logic.fgi(  41, 157):���������� ���������
      C8_erike = 'regim2'
C FDA20_logic.fgi(  36, 162):��������� ���������� CH8 (�������)
      call chcomp(C8_utike,C8_erike,L_(562))
C FDA20_logic.fgi(  41, 163):���������� ���������
      C30_irike = '�������������� �����'
C FDA20_logic.fgi(  89, 153):��������� ���������� CH20 (CH30) (�������)
      C30_urike = '������������������ �����'
C FDA20_logic.fgi(  61, 154):��������� ���������� CH20 (CH30) (�������)
      C30_esike = '������ �����'
C FDA20_logic.fgi( 101, 171):��������� ���������� CH20 (CH30) (�������)
      C30_osike = '����� �������'
C FDA20_logic.fgi(  80, 172):��������� ���������� CH20 (CH30) (�������)
      C30_atike = '��������� ���������'
C FDA20_logic.fgi(  61, 173):��������� ���������� CH20 (CH30) (�������)
      C30_etike = ''
C FDA20_logic.fgi(  61, 175):��������� ���������� CH20 (CH30) (�������)
      C8_otike = 'regim1'
C FDA20_logic.fgi(  36, 168):��������� ���������� CH8 (�������)
      call chcomp(C8_utike,C8_otike,L_(563))
C FDA20_logic.fgi(  41, 169):���������� ���������
      if(L_(563)) then
         C30_usike=C30_atike
      else
         C30_usike=C30_etike
      endif
C FDA20_logic.fgi(  65, 173):���� RE IN LO CH20
      if(L_(562)) then
         C30_isike=C30_osike
      else
         C30_isike=C30_usike
      endif
C FDA20_logic.fgi(  85, 172):���� RE IN LO CH20
      if(L_(561)) then
         C30_asike=C30_esike
      else
         C30_asike=C30_isike
      endif
C FDA20_logic.fgi( 106, 171):���� RE IN LO CH20
      if(L_(560)) then
         C30_orike=C30_urike
      else
         C30_orike=C30_asike
      endif
C FDA20_logic.fgi(  66, 154):���� RE IN LO CH20
      if(L_(559)) then
         C30_itike=C30_irike
      else
         C30_itike=C30_orike
      endif
C FDA20_logic.fgi(  94, 153):���� RE IN LO CH20
      L_(573)=.true.
C FDA20_logic.fgi(  52,  63):��������� ���������� (�������)
      L0_ivike=R0_uvike.ne.R0_ovike
      R0_ovike=R0_uvike
C FDA20_logic.fgi(  39,  56):���������� ������������� ������
      if(L0_ivike) then
         L_(570)=L_(573)
      else
         L_(570)=.false.
      endif
C FDA20_logic.fgi(  56,  62):���� � ������������� �������
      L_(3)=L_(570)
C FDA20_logic.fgi(  56,  62):������-�������: ���������� ��� �������������� ������
      L_(574)=.true.
C FDA20_logic.fgi(  52,  80):��������� ���������� (�������)
      L0_ixike=R0_uxike.ne.R0_oxike
      R0_oxike=R0_uxike
C FDA20_logic.fgi(  39,  73):���������� ������������� ������
      if(L0_ixike) then
         L_(571)=L_(574)
      else
         L_(571)=.false.
      endif
C FDA20_logic.fgi(  56,  79):���� � ������������� �������
      L_(2)=L_(571)
C FDA20_logic.fgi(  56,  79):������-�������: ���������� ��� �������������� ������
      L_(568) = L_(2).OR.L_(3)
C FDA20_logic.fgi(  66,  92):���
      L_(575)=.true.
C FDA20_logic.fgi(  52,  96):��������� ���������� (�������)
      L0_iboke=R0_uboke.ne.R0_oboke
      R0_oboke=R0_uboke
C FDA20_logic.fgi(  39,  89):���������� ������������� ������
      if(L0_iboke) then
         L_(572)=L_(575)
      else
         L_(572)=.false.
      endif
C FDA20_logic.fgi(  56,  95):���� � ������������� �������
      L_(1)=L_(572)
C FDA20_logic.fgi(  56,  95):������-�������: ���������� ��� �������������� ������
      L_(564) = L_(1).OR.L_(2)
C FDA20_logic.fgi(  66,  59):���
      L_evike=(L_(3).or.L_evike).and..not.(L_(564))
      L_(565)=.not.L_evike
C FDA20_logic.fgi(  74,  61):RS �������
      L_avike=L_evike
C FDA20_logic.fgi(  90,  63):������,FDA2_tech_mode
      L_(566) = L_(1).OR.L_(3)
C FDA20_logic.fgi(  66,  76):���
      L_exike=(L_(2).or.L_exike).and..not.(L_(566))
      L_(567)=.not.L_exike
C FDA20_logic.fgi(  74,  78):RS �������
      L_axike=L_exike
C FDA20_logic.fgi(  90,  80):������,FDA2_ruch_mode
      L_eboke=(L_(1).or.L_eboke).and..not.(L_(568))
      L_(569)=.not.L_eboke
C FDA20_logic.fgi(  74,  94):RS �������
      L_aboke=L_eboke
C FDA20_logic.fgi(  90,  96):������,FDA2_avt_mode
      !{
      Call BVALVE_MAN(deltat,REAL(R_orux,4),L_axux,L_exux
     &,R8_omux,L_avike,
     & L_axike,L_aboke,I_avux,I_evux,R_okux,
     & REAL(R_alux,4),R_ulux,REAL(R_emux,4),
     & R_elux,REAL(R_olux,4),I_etux,I_ivux,I_utux,I_otux,L_imux
     &,
     & L_ovux,L_ababe,L_amux,L_ilux,
     & L_ipux,L_epux,L_ixux,L_ukux,L_upux,L_obabe,L_uvux,
     & L_erux,L_irux,REAL(R8_iseke,8),REAL(1.0,4),R8_ebike
     &,L_apux,
     & L_umux,L_ebabe,R_atux,REAL(R_opux,4),L_ibabe,L_ubabe
     &,L_urux,
     & L_asux,L_esux,L_osux,L_usux,L_isux)
      !}

      if(L_usux.or.L_osux.or.L_isux.or.L_esux.or.L_asux.or.L_urux
     &) then      
                  I_itux = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_itux = z'40000000'
      endif
C FDA20_vlv.fgi(  56, 150):���� ���������� ������ �������,20FDA21AA102
      !{
      Call BVALVE_MAN(deltat,REAL(R_evofe,4),L_odufe,L_udufe
     &,R8_esofe,L_avike,
     & L_axike,L_aboke,I_obufe,I_ubufe,R_epofe,
     & REAL(R_opofe,4),R_irofe,REAL(R_urofe,4),
     & R_upofe,REAL(R_erofe,4),I_uxofe,I_adufe,I_ibufe,I_ebufe
     &,L_asofe,
     & L_edufe,L_ofufe,L_orofe,L_arofe,
     & L_atofe,L_usofe,L_afufe,L_ipofe,L_itofe,L_ekufe,L_idufe
     &,
     & L_utofe,L_avofe,REAL(R8_iseke,8),REAL(1.0,4),R8_ebike
     &,L_osofe,
     & L_isofe,L_ufufe,R_oxofe,REAL(R_etofe,4),L_akufe,L_ikufe
     &,L_ivofe,
     & L_ovofe,L_uvofe,L_exofe,L_ixofe,L_axofe)
      !}

      if(L_ixofe.or.L_exofe.or.L_axofe.or.L_uvofe.or.L_ovofe.or.L_ivofe
     &) then      
                  I_abufe = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_abufe = z'40000000'
      endif
C FDA20_vlv.fgi(  73, 150):���� ���������� ������ �������,20FDA23AA103
      !{
      Call BVALVE_MAN(deltat,REAL(R_itobe,4),L_ubube,L_adube
     &,R8_irobe,L_avike,
     & L_axike,L_aboke,I_uxobe,I_abube,R_imobe,
     & REAL(R_umobe,4),R_opobe,REAL(R_arobe,4),
     & R_apobe,REAL(R_ipobe,4),I_axobe,I_ebube,I_oxobe,I_ixobe
     &,L_erobe,
     & L_ibube,L_udube,L_upobe,L_epobe,
     & L_esobe,L_asobe,L_edube,L_omobe,L_osobe,L_ifube,L_obube
     &,
     & L_atobe,L_etobe,REAL(R8_iseke,8),REAL(1.0,4),R8_ebike
     &,L_urobe,
     & L_orobe,L_afube,R_uvobe,REAL(R_isobe,4),L_efube,L_ofube
     &,L_otobe,
     & L_utobe,L_avobe,L_ivobe,L_ovobe,L_evobe)
      !}

      if(L_ovobe.or.L_ivobe.or.L_evobe.or.L_avobe.or.L_utobe.or.L_otobe
     &) then      
                  I_exobe = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_exobe = z'40000000'
      endif
C FDA20_vlv.fgi( 123, 150):���� ���������� ������ �������,20FDA21AA101
      !{
      Call BVALVE_MAN(deltat,REAL(R_orufe,4),L_axufe,L_exufe
     &,R8_omufe,L_avike,
     & L_axike,L_aboke,I_avufe,I_evufe,R_okufe,
     & REAL(R_alufe,4),R_ulufe,REAL(R_emufe,4),
     & R_elufe,REAL(R_olufe,4),I_etufe,I_ivufe,I_utufe,I_otufe
     &,L_imufe,
     & L_ovufe,L_abake,L_amufe,L_ilufe,
     & L_ipufe,L_epufe,L_ixufe,L_ukufe,L_upufe,L_obake,L_uvufe
     &,
     & L_erufe,L_irufe,REAL(R8_iseke,8),REAL(1.0,4),R8_ebike
     &,L_apufe,
     & L_umufe,L_ebake,R_atufe,REAL(R_opufe,4),L_ibake,L_ubake
     &,L_urufe,
     & L_asufe,L_esufe,L_osufe,L_usufe,L_isufe)
      !}

      if(L_usufe.or.L_osufe.or.L_isufe.or.L_esufe.or.L_asufe.or.L_urufe
     &) then      
                  I_itufe = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_itufe = z'40000000'
      endif
C FDA20_vlv.fgi( 105, 176):���� ���������� ������ �������,20FDA23AA102
      !{
      Call BVALVE_MAN(deltat,REAL(R_akeke,4),L_ipeke,L_opeke
     &,R8_adeke,L_avike,
     & L_axike,L_aboke,I_imeke,I_omeke,R_axake,
     & REAL(R_ixake,4),R_ebeke,REAL(R_obeke,4),
     & R_oxake,REAL(R_abeke,4),I_oleke,I_umeke,I_emeke,I_ameke
     &,L_ubeke,
     & L_apeke,L_ireke,L_ibeke,L_uxake,
     & L_udeke,L_odeke,L_upeke,L_exake,L_efeke,L_aseke,L_epeke
     &,
     & L_ofeke,L_ufeke,REAL(R8_iseke,8),REAL(1.0,4),R8_ebike
     &,L_ideke,
     & L_edeke,L_oreke,R_ileke,REAL(R_afeke,4),L_ureke,L_eseke
     &,L_ekeke,
     & L_ikeke,L_okeke,L_aleke,L_eleke,L_ukeke)
      !}

      if(L_eleke.or.L_aleke.or.L_ukeke.or.L_okeke.or.L_ikeke.or.L_ekeke
     &) then      
                  I_uleke = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_uleke = z'40000000'
      endif
C FDA20_vlv.fgi(  88, 176):���� ���������� ������ �������,20FDA23AA101
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_abobe,4),L_ikobe,L_okobe
     &,R8_otibe,C30_axibe,
     & L_avike,L_axike,L_aboke,I_ifobe,I_ofobe,R_ixibe,R_exibe
     &,
     & R_oribe,REAL(R_asibe,4),R_usibe,
     & REAL(R_etibe,4),R_esibe,REAL(R_osibe,4),I_odobe,
     & I_ufobe,I_efobe,I_afobe,L_itibe,L_akobe,L_ilobe,L_atibe
     &,
     & L_isibe,L_ivibe,L_evibe,L_ukobe,L_uribe,L_uvibe,
     & L_amobe,L_ekobe,L_oxibe,L_uxibe,REAL(R8_iseke,8),REAL
     &(1.0,4),R8_ebike,
     & L_avibe,L_utibe,L_olobe,R_idobe,REAL(R_ovibe,4),L_ulobe
     &,L_emobe,
     & L_ebobe,L_ibobe,L_obobe,L_adobe,L_edobe,L_ubobe)
      !}

      if(L_edobe.or.L_adobe.or.L_ubobe.or.L_obobe.or.L_ibobe.or.L_ebobe
     &) then      
                  I_udobe = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_udobe = z'40000000'
      endif
C FDA20_vlv.fgi( 105, 150):���� ���������� �������� ��������,20FDA21AB003
      R_ekov=R_exibe
C FDA20_vent_log.fgi( 115,  58):������,20FDA21AB003XQ01_input
      !{
      Call DAT_ANA_HANDLER(deltat,R_afov,R_ulov,REAL(1,4)
     &,
     & REAL(R_ofov,4),REAL(R_ufov,4),
     & REAL(R_udov,4),REAL(R_odov,4),I_olov,
     & REAL(R_ikov,4),L_okov,REAL(R_ukov,4),L_alov,L_elov
     &,R_akov,
     & REAL(R_ifov,4),REAL(R_efov,4),L_ilov,REAL(R_ekov,4
     &))
      !}
C FDA20_vlv.fgi( 377, 125):���������� �������,20FDA21AB003XQ01
      !{
      Call BVALVE_MAN(deltat,REAL(R_upube,4),L_evube,L_ivube
     &,R8_ulube,L_avike,
     & L_axike,L_aboke,I_etube,I_itube,R_ufube,
     & REAL(R_ekube,4),R_alube,REAL(R_ilube,4),
     & R_ikube,REAL(R_ukube,4),I_isube,I_otube,I_atube,I_usube
     &,L_olube,
     & L_utube,L_exube,L_elube,L_okube,
     & L_omube,L_imube,L_ovube,L_akube,L_apube,L_uxube,L_avube
     &,
     & L_ipube,L_opube,REAL(R8_iseke,8),REAL(1.0,4),R8_ebike
     &,L_emube,
     & L_amube,L_ixube,R_esube,REAL(R_umube,4),L_oxube,L_abade
     &,L_arube,
     & L_erube,L_irube,L_urube,L_asube,L_orube)
      !}

      if(L_asube.or.L_urube.or.L_orube.or.L_irube.or.L_erube.or.L_arube
     &) then      
                  I_osube = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_osube = z'40000000'
      endif
C FDA20_vlv.fgi( 140, 150):���� ���������� ������ �������,20FDA21AA104
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_edike,4),L_olike,L_ulike
     &,R8_oveke,C30_abike,
     & L_avike,L_axike,L_aboke,I_okike,I_ukike,R_obike,R_ibike
     &,
     & R_oseke,REAL(R_ateke,4),R_uteke,
     & REAL(R_eveke,4),R_eteke,REAL(R_oteke,4),I_ufike,
     & I_alike,I_ikike,I_ekike,L_iveke,L_elike,L_omike,L_aveke
     &,
     & L_iteke,L_ixeke,L_exeke,L_amike,L_useke,L_uxeke,
     & L_epike,L_ilike,L_ubike,L_adike,REAL(R8_iseke,8),REAL
     &(1.0,4),R8_ebike,
     & L_axeke,L_uveke,L_umike,R_ofike,REAL(R_oxeke,4),L_apike
     &,L_ipike,
     & L_idike,L_odike,L_udike,L_efike,L_ifike,L_afike)
      !}

      if(L_ifike.or.L_efike.or.L_afike.or.L_udike.or.L_odike.or.L_idike
     &) then      
                  I_akike = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_akike = z'40000000'
      endif
C FDA20_vlv.fgi(  73, 176):���� ���������� �������� ��������,20FDA23AA201
      R_ilox=R_ibike
C FDA20_vent_log.fgi( 195,  37):������,20FDA23AA201XQ01_input
      !{
      Call DAT_ANA_HANDLER(deltat,R_ekox,R_apox,REAL(1,4)
     &,
     & REAL(R_ukox,4),REAL(R_alox,4),
     & REAL(R_akox,4),REAL(R_ufox,4),I_umox,
     & REAL(R_olox,4),L_ulox,REAL(R_amox,4),L_emox,L_imox
     &,R_elox,
     & REAL(R_okox,4),REAL(R_ikox,4),L_omox,REAL(R_ilox,4
     &))
      !}
C FDA20_vlv.fgi( 465, 183):���������� �������,20FDA23AA201XQ01
      !{
      Call BVALVE_MAN(deltat,REAL(R_evox,4),L_odux,L_udux
     &,R8_esox,L_avike,
     & L_axike,L_aboke,I_obux,I_ubux,R_epox,
     & REAL(R_opox,4),R_irox,REAL(R_urox,4),
     & R_upox,REAL(R_erox,4),I_uxox,I_adux,I_ibux,I_ebux,L_asox
     &,
     & L_edux,L_ofux,L_orox,L_arox,
     & L_atox,L_usox,L_afux,L_ipox,L_itox,L_ekux,L_idux,
     & L_utox,L_avox,REAL(R8_iseke,8),REAL(1.0,4),R8_ebike
     &,L_osox,
     & L_isox,L_ufux,R_oxox,REAL(R_etox,4),L_akux,L_ikux,L_ivox
     &,
     & L_ovox,L_uvox,L_exox,L_ixox,L_axox)
      !}

      if(L_ixox.or.L_exox.or.L_axox.or.L_uvox.or.L_ovox.or.L_ivox
     &) then      
                  I_abux = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_abux = z'40000000'
      endif
C FDA20_vlv.fgi(  88, 150):���� ���������� ������ �������,20FDA21AA103
      Call PUMP_HANDLER(deltat,C30_okake,I_emake,L_avike,L_axike
     &,L_aboke,
     & I_imake,I_amake,R_ifake,REAL(R_ufake,4),
     & R_udake,REAL(R_efake,4),I_omake,L_esake,L_erake,L_avake
     &,
     & L_evake,L_ilake,L_ipake,L_upake,L_opake,L_arake,L_urake
     &,L_odake,
     & L_ofake,L_idake,L_afake,L_itake,L_(558),
     & L_otake,L_(557),L_edake,L_adake,L_olake,I_umake,R_osake
     &,R_usake,
     & L_uvake,L_asake,L_utake,REAL(R8_iseke,8),L_orake,
     & REAL(R8_irake,8),R_ivake,REAL(R_apake,4),R_epake,REAL
     &(R8_elake,8),R_alake,
     & R8_ebike,R_ovake,R8_irake,REAL(R_akake,4),REAL(R_ekake
     &,4))
C FDA20_vlv.fgi(  55, 124):���������� ���������� �������,20FDA23CU001KN01
C label 1127  try1127=try1127-1
C sav1=R_ovake
C sav2=R_ivake
C sav3=L_esake
C sav4=L_urake
C sav5=L_erake
C sav6=R8_irake
C sav7=R_epake
C sav8=I_umake
C sav9=I_omake
C sav10=I_imake
C sav11=I_emake
C sav12=I_amake
C sav13=I_ulake
C sav14=L_olake
C sav15=L_ilake
C sav16=R_alake
C sav17=C30_okake
C sav18=L_ofake
C sav19=L_afake
      Call PUMP_HANDLER(deltat,C30_okake,I_emake,L_avike,L_axike
     &,L_aboke,
     & I_imake,I_amake,R_ifake,REAL(R_ufake,4),
     & R_udake,REAL(R_efake,4),I_omake,L_esake,L_erake,L_avake
     &,
     & L_evake,L_ilake,L_ipake,L_upake,L_opake,L_arake,L_urake
     &,L_odake,
     & L_ofake,L_idake,L_afake,L_itake,L_(558),
     & L_otake,L_(557),L_edake,L_adake,L_olake,I_umake,R_osake
     &,R_usake,
     & L_uvake,L_asake,L_utake,REAL(R8_iseke,8),L_orake,
     & REAL(R8_irake,8),R_ivake,REAL(R_apake,4),R_epake,REAL
     &(R8_elake,8),R_alake,
     & R8_ebike,R_ovake,R8_irake,REAL(R_akake,4),REAL(R_ekake
     &,4))
C FDA20_vlv.fgi(  55, 124):recalc:���������� ���������� �������,20FDA23CU001KN01
C if(sav1.ne.R_ovake .and. try1127.gt.0) goto 1127
C if(sav2.ne.R_ivake .and. try1127.gt.0) goto 1127
C if(sav3.ne.L_esake .and. try1127.gt.0) goto 1127
C if(sav4.ne.L_urake .and. try1127.gt.0) goto 1127
C if(sav5.ne.L_erake .and. try1127.gt.0) goto 1127
C if(sav6.ne.R8_irake .and. try1127.gt.0) goto 1127
C if(sav7.ne.R_epake .and. try1127.gt.0) goto 1127
C if(sav8.ne.I_umake .and. try1127.gt.0) goto 1127
C if(sav9.ne.I_omake .and. try1127.gt.0) goto 1127
C if(sav10.ne.I_imake .and. try1127.gt.0) goto 1127
C if(sav11.ne.I_emake .and. try1127.gt.0) goto 1127
C if(sav12.ne.I_amake .and. try1127.gt.0) goto 1127
C if(sav13.ne.I_ulake .and. try1127.gt.0) goto 1127
C if(sav14.ne.L_olake .and. try1127.gt.0) goto 1127
C if(sav15.ne.L_ilake .and. try1127.gt.0) goto 1127
C if(sav16.ne.R_alake .and. try1127.gt.0) goto 1127
C if(sav17.ne.C30_okake .and. try1127.gt.0) goto 1127
C if(sav18.ne.L_ofake .and. try1127.gt.0) goto 1127
C if(sav19.ne.L_afake .and. try1127.gt.0) goto 1127
      if(L_urake) then
         R_(325)=R_(326)
      else
         R_(325)=R_(327)
      endif
C FDA20_vent_log.fgi( 167, 270):���� RE IN LO CH7
      R8_ikes=(R0_akes*R_(324)+deltat*R_(325))/(R0_akes+deltat
     &)
C FDA20_vent_log.fgi( 177, 271):�������������� �����  
      R8_ekes=R8_ikes
C FDA20_vent_log.fgi( 196, 271):������,F_FDA23CU001_G
      Call PUMP_HANDLER(deltat,C30_ibibe,I_afibe,L_avike,L_axike
     &,L_aboke,
     & I_efibe,I_udibe,R_exebe,REAL(R_oxebe,4),
     & R_ovebe,REAL(R_axebe,4),I_ifibe,L_ulibe,L_alibe,L_opibe
     &,
     & L_upibe,L_edibe,L_ekibe,L_okibe,L_ikibe,L_ukibe,L_olibe
     &,L_ivebe,
     & L_ixebe,L_evebe,L_uvebe,L_apibe,L_(540),
     & L_epibe,L_(539),L_avebe,L_utebe,L_idibe,I_ofibe,R_emibe
     &,R_imibe,
     & L_iribe,L_asake,L_ipibe,REAL(R8_iseke,8),L_ilibe,
     & REAL(R8_elibe,8),R_aribe,REAL(R_ufibe,4),R_akibe,REAL
     &(R8_adibe,8),R_ubibe,
     & R8_ebike,R_eribe,R8_elibe,REAL(R_uxebe,4),REAL(R_abibe
     &,4))
C FDA20_vlv.fgi(  73, 124):���������� ���������� �������,20FDA21AX001KN01
C label 1135  try1135=try1135-1
C sav1=R_eribe
C sav2=R_aribe
C sav3=L_ulibe
C sav4=L_olibe
C sav5=L_alibe
C sav6=R8_elibe
C sav7=R_akibe
C sav8=I_ofibe
C sav9=I_ifibe
C sav10=I_efibe
C sav11=I_afibe
C sav12=I_udibe
C sav13=I_odibe
C sav14=L_idibe
C sav15=L_edibe
C sav16=R_ubibe
C sav17=C30_ibibe
C sav18=L_ixebe
C sav19=L_uvebe
      Call PUMP_HANDLER(deltat,C30_ibibe,I_afibe,L_avike,L_axike
     &,L_aboke,
     & I_efibe,I_udibe,R_exebe,REAL(R_oxebe,4),
     & R_ovebe,REAL(R_axebe,4),I_ifibe,L_ulibe,L_alibe,L_opibe
     &,
     & L_upibe,L_edibe,L_ekibe,L_okibe,L_ikibe,L_ukibe,L_olibe
     &,L_ivebe,
     & L_ixebe,L_evebe,L_uvebe,L_apibe,L_(540),
     & L_epibe,L_(539),L_avebe,L_utebe,L_idibe,I_ofibe,R_emibe
     &,R_imibe,
     & L_iribe,L_asake,L_ipibe,REAL(R8_iseke,8),L_ilibe,
     & REAL(R8_elibe,8),R_aribe,REAL(R_ufibe,4),R_akibe,REAL
     &(R8_adibe,8),R_ubibe,
     & R8_ebike,R_eribe,R8_elibe,REAL(R_uxebe,4),REAL(R_abibe
     &,4))
C FDA20_vlv.fgi(  73, 124):recalc:���������� ���������� �������,20FDA21AX001KN01
C if(sav1.ne.R_eribe .and. try1135.gt.0) goto 1135
C if(sav2.ne.R_aribe .and. try1135.gt.0) goto 1135
C if(sav3.ne.L_ulibe .and. try1135.gt.0) goto 1135
C if(sav4.ne.L_olibe .and. try1135.gt.0) goto 1135
C if(sav5.ne.L_alibe .and. try1135.gt.0) goto 1135
C if(sav6.ne.R8_elibe .and. try1135.gt.0) goto 1135
C if(sav7.ne.R_akibe .and. try1135.gt.0) goto 1135
C if(sav8.ne.I_ofibe .and. try1135.gt.0) goto 1135
C if(sav9.ne.I_ifibe .and. try1135.gt.0) goto 1135
C if(sav10.ne.I_efibe .and. try1135.gt.0) goto 1135
C if(sav11.ne.I_afibe .and. try1135.gt.0) goto 1135
C if(sav12.ne.I_udibe .and. try1135.gt.0) goto 1135
C if(sav13.ne.I_odibe .and. try1135.gt.0) goto 1135
C if(sav14.ne.L_idibe .and. try1135.gt.0) goto 1135
C if(sav15.ne.L_edibe .and. try1135.gt.0) goto 1135
C if(sav16.ne.R_ubibe .and. try1135.gt.0) goto 1135
C if(sav17.ne.C30_ibibe .and. try1135.gt.0) goto 1135
C if(sav18.ne.L_ixebe .and. try1135.gt.0) goto 1135
C if(sav19.ne.L_uvebe .and. try1135.gt.0) goto 1135
      if(L_olibe) then
         R_(329)=R_(330)
      else
         R_(329)=R_(331)
      endif
C FDA20_vent_log.fgi(  89, 270):���� RE IN LO CH7
      R8_iles=(R0_ales*R_(328)+deltat*R_(329))/(R0_ales+deltat
     &)
C FDA20_vent_log.fgi( 101, 271):�������������� �����  
      R8_eles=R8_iles
C FDA20_vent_log.fgi( 118, 271):������,F_FDA21AX001_G
      Call PUMP_HANDLER(deltat,C30_emit,I_upit,L_avike,L_axike
     &,L_aboke,
     & I_arit,I_opit,R_alit,REAL(R_ilit,4),
     & R_ikit,REAL(R_ukit,4),I_erit,L_otit,L_usit,L_ixit,
     & L_oxit,L_apit,L_asit,L_isit,L_esit,L_osit,L_itit,L_ekit
     &,
     & L_elit,L_akit,L_okit,L_uvit,L_(538),
     & L_axit,L_(537),L_ufit,L_ofit,L_epit,I_irit,R_avit,R_evit
     &,
     & L_ebot,L_asake,L_exit,REAL(R8_iseke,8),L_etit,
     & REAL(R8_atit,8),R_uxit,REAL(R_orit,4),R_urit,REAL(R8_umit
     &,8),R_omit,
     & R8_ebike,R_abot,R8_atit,REAL(R_olit,4),REAL(R_ulit
     &,4))
C FDA20_vlv.fgi(  91, 124):���������� ���������� �������,20FDA22AX001KN01
C label 1143  try1143=try1143-1
C sav1=R_abot
C sav2=R_uxit
C sav3=L_otit
C sav4=L_itit
C sav5=L_usit
C sav6=R8_atit
C sav7=R_urit
C sav8=I_irit
C sav9=I_erit
C sav10=I_arit
C sav11=I_upit
C sav12=I_opit
C sav13=I_ipit
C sav14=L_epit
C sav15=L_apit
C sav16=R_omit
C sav17=C30_emit
C sav18=L_elit
C sav19=L_okit
      Call PUMP_HANDLER(deltat,C30_emit,I_upit,L_avike,L_axike
     &,L_aboke,
     & I_arit,I_opit,R_alit,REAL(R_ilit,4),
     & R_ikit,REAL(R_ukit,4),I_erit,L_otit,L_usit,L_ixit,
     & L_oxit,L_apit,L_asit,L_isit,L_esit,L_osit,L_itit,L_ekit
     &,
     & L_elit,L_akit,L_okit,L_uvit,L_(538),
     & L_axit,L_(537),L_ufit,L_ofit,L_epit,I_irit,R_avit,R_evit
     &,
     & L_ebot,L_asake,L_exit,REAL(R8_iseke,8),L_etit,
     & REAL(R8_atit,8),R_uxit,REAL(R_orit,4),R_urit,REAL(R8_umit
     &,8),R_omit,
     & R8_ebike,R_abot,R8_atit,REAL(R_olit,4),REAL(R_ulit
     &,4))
C FDA20_vlv.fgi(  91, 124):recalc:���������� ���������� �������,20FDA22AX001KN01
C if(sav1.ne.R_abot .and. try1143.gt.0) goto 1143
C if(sav2.ne.R_uxit .and. try1143.gt.0) goto 1143
C if(sav3.ne.L_otit .and. try1143.gt.0) goto 1143
C if(sav4.ne.L_itit .and. try1143.gt.0) goto 1143
C if(sav5.ne.L_usit .and. try1143.gt.0) goto 1143
C if(sav6.ne.R8_atit .and. try1143.gt.0) goto 1143
C if(sav7.ne.R_urit .and. try1143.gt.0) goto 1143
C if(sav8.ne.I_irit .and. try1143.gt.0) goto 1143
C if(sav9.ne.I_erit .and. try1143.gt.0) goto 1143
C if(sav10.ne.I_arit .and. try1143.gt.0) goto 1143
C if(sav11.ne.I_upit .and. try1143.gt.0) goto 1143
C if(sav12.ne.I_opit .and. try1143.gt.0) goto 1143
C if(sav13.ne.I_ipit .and. try1143.gt.0) goto 1143
C if(sav14.ne.L_epit .and. try1143.gt.0) goto 1143
C if(sav15.ne.L_apit .and. try1143.gt.0) goto 1143
C if(sav16.ne.R_omit .and. try1143.gt.0) goto 1143
C if(sav17.ne.C30_emit .and. try1143.gt.0) goto 1143
C if(sav18.ne.L_elit .and. try1143.gt.0) goto 1143
C if(sav19.ne.L_okit .and. try1143.gt.0) goto 1143
      if(.not.L_isel) then
         R0_orel=0.0
      elseif(.not.L0_urel) then
         R0_orel=R0_irel
      else
         R0_orel=max(R_(258)-deltat,0.0)
      endif
      L_(460)=L_isel.and.R0_orel.le.0.0
      L0_urel=L_isel
C FDA20_SP1.fgi(  57, 489):�������� ��������� ������
C label 1144  try1144=try1144-1
      if(L_isel.and..not.L0_epel) then
         R0_umel=R0_apel
      else
         R0_umel=max(R_(257)-deltat,0.0)
      endif
      L_(453)=R0_umel.gt.0.0
      L0_epel=L_isel
C FDA20_SP1.fgi( 158, 483):������������  �� T
      L_ipel=(L_(453).or.L_ipel).and..not.(L_imol)
      L_(454)=.not.L_ipel
C FDA20_SP1.fgi( 170, 481):RS �������
      L_arel=L_ipel
C FDA20_SP1.fgi( 193, 483):������,containerY1set
      if(L_alal.and..not.L0_etuk) then
         R0_usuk=R0_atuk
      else
         R0_usuk=max(R_(238)-deltat,0.0)
      endif
      L_(419)=R0_usuk.gt.0.0
      L0_etuk=L_alal
C FDA20_SP1.fgi( 118, 746):������������  �� T
      L0_elal=(L_(419).or.L0_elal).and..not.(L_(420))
      L_okas=.not.L0_elal
C FDA20_SP1.fgi( 240, 744):RS �������
      if(.not.L0_elal) then
         R0_okal=0.0
      elseif(.not.L0_ukal) then
         R0_okal=R0_ikal
      else
         R0_okal=max(R_(245)-deltat,0.0)
      endif
      L_(418)=L0_elal.and.R0_okal.le.0.0
      L0_ukal=L0_elal
C FDA20_SP1.fgi(  65, 746):�������� ��������� ������
      if(.not.L_olel) then
         R0_elel=0.0
      elseif(.not.L0_ilel) then
         R0_elel=R0_alel
      else
         R0_elel=max(R_(255)-deltat,0.0)
      endif
      L_(449)=L_olel.and.R0_elel.le.0.0
      L0_ilel=L_olel
C FDA20_SP1.fgi(  57, 441):�������� ��������� ������
      if(L_olel.and..not.L0_okel) then
         R0_ekel=R0_ikel
      else
         R0_ekel=max(R_(254)-deltat,0.0)
      endif
      L_(447)=R0_ekel.gt.0.0
      L0_okel=L_olel
C FDA20_SP1.fgi( 158, 435):������������  �� T
      L_ukel=(L_(447).or.L_ukel).and..not.(L_imol)
      L_(448)=.not.L_ukel
C FDA20_SP1.fgi( 170, 433):RS �������
      L_akel=L_ukel
C FDA20_SP1.fgi( 193, 435):������,containerY2set
      L_(457) = L_(502).OR.L_upel.OR.L_esel
C FDA20_SP1.fgi( 102, 780):���
      if (L_(457)) then
          I_umol = 1
          L_adol = .true.
      endif
      if (L_ivak) then
          L_(311) = .true.
          L_adol = .false.
      elseif (L_(496)) then
          L_(417) = .true.
          L_adol = .false.
      endif
      if (I_umol.ne.1) then
          L_adol = .false.
          L_(417) = .false.
          L_(311) = .false.
      endif
C FDA20_SP1.fgi(  94, 768):��� ������� ��������� � ����������,cset
      if(L_omol) then
          if (L_(417)) then
              I_umol = 2
              L_alal = .true.
              L_(500) = .false.
          endif
          if (L_(418)) then
              L_(500) = .true.
              L_alal = .false.
          endif
          if (I_umol.ne.2) then
              L_alal = .false.
              L_(500) = .false.
          endif
      else
          L_(500) = .false.
      endif
C FDA20_SP1.fgi(  94, 746):��� ������� ���������,cset
C sav1=L_(419)
      if(L_alal.and..not.L0_etuk) then
         R0_usuk=R0_atuk
      else
         R0_usuk=max(R_(238)-deltat,0.0)
      endif
      L_(419)=R0_usuk.gt.0.0
      L0_etuk=L_alal
C FDA20_SP1.fgi( 118, 746):recalc:������������  �� T
C if(sav1.ne.L_(419) .and. try1160.gt.0) goto 1160
      if(L_omol) then
          if (L_(500)) then
              I_umol = 3
              L_amol = .true.
              L_(501) = .false.
          endif
          if (L_atur) then
              L_(501) = .true.
              L_amol = .false.
          endif
          if (I_umol.ne.3) then
              L_amol = .false.
              L_(501) = .false.
          endif
      else
          L_(501) = .false.
      endif
C FDA20_SP1.fgi(  94, 735):��� ������� ���������,cset
      if(L_omol) then
          if (L_(501)) then
              I_umol = 4
              L_ekal = .true.
              L_(494) = .false.
          endif
          if (L_idol) then
              L_(494) = .true.
              L_ekal = .false.
          endif
          if (I_umol.ne.4) then
              L_ekal = .false.
              L_(494) = .false.
          endif
      else
          L_(494) = .false.
      endif
C FDA20_SP1.fgi(  94, 724):��� ������� ���������,cset
      if(L_omol) then
          if (L_(494)) then
              I_umol = 5
              L_ebol = .true.
              L_(495) = .false.
          endif
          if (L_abol) then
              L_(495) = .true.
              L_ebol = .false.
          endif
          if (I_umol.ne.5) then
              L_ebol = .false.
              L_(495) = .false.
          endif
      else
          L_(495) = .false.
      endif
C FDA20_SP1.fgi(  94, 713):��� ������� ���������,cset
      if(L_omol) then
          if (L_(495)) then
              I_umol = 6
              L_oxil = .true.
              L_(493) = .false.
          endif
          if (L_ixil) then
              L_(493) = .true.
              L_oxil = .false.
          endif
          if (I_umol.ne.6) then
              L_oxil = .false.
              L_(493) = .false.
          endif
      else
          L_(493) = .false.
      endif
C FDA20_SP1.fgi(  94, 702):��� ������� ���������,cset
      if(L_omol) then
          if (L_(493)) then
              I_umol = 7
              L_afal = .true.
              L_(498) = .false.
          endif
          if (L_udal) then
              L_(498) = .true.
              L_afal = .false.
          endif
          if (I_umol.ne.7) then
              L_afal = .false.
              L_(498) = .false.
          endif
      else
          L_(498) = .false.
      endif
C FDA20_SP1.fgi(  94, 691):��� ������� ���������,cset
      if(L_omol) then
          if (L_(498)) then
              I_umol = 8
              L_olol = .true.
              L_(499) = .false.
          endif
          if (L_erur) then
              L_(499) = .true.
              L_olol = .false.
          endif
          if (I_umol.ne.8) then
              L_olol = .false.
              L_(499) = .false.
          endif
      else
          L_(499) = .false.
      endif
C FDA20_SP1.fgi(  94, 680):��� ������� ���������,cset
      if(L_omol) then
          if (L_(499)) then
              I_umol = 9
              L_itil = .true.
              L_(490) = .false.
          endif
          if (L_abol) then
              L_(490) = .true.
              L_itil = .false.
          endif
          if (I_umol.ne.9) then
              L_itil = .false.
              L_(490) = .false.
          endif
      else
          L_(490) = .false.
      endif
C FDA20_SP1.fgi(  94, 669):��� ������� ���������,cset
      if(L_omol) then
          if (L_(490)) then
              I_umol = 10
              L_ivil = .true.
              L_(489) = .false.
          endif
          if (L_atil) then
              L_(489) = .true.
              L_ivil = .false.
          endif
          if (I_umol.ne.10) then
              L_ivil = .false.
              L_(489) = .false.
          endif
      else
          L_(489) = .false.
      endif
C FDA20_SP1.fgi(  94, 658):��� ������� ���������,cset
      if(L_omol) then
          if (L_(489)) then
              I_umol = 11
              L_axil = .true.
              L_(487) = .false.
          endif
          if (L_ixil) then
              L_(487) = .true.
              L_axil = .false.
          endif
          if (I_umol.ne.11) then
              L_axil = .false.
              L_(487) = .false.
          endif
      else
          L_(487) = .false.
      endif
C FDA20_SP1.fgi(  94, 647):��� ������� ���������,cset
      if(L_omol) then
          if (L_(487)) then
              I_umol = 12
              L_usil = .true.
              L_(488) = .false.
          endif
          if (L_abol) then
              L_(488) = .true.
              L_usil = .false.
          endif
          if (I_umol.ne.12) then
              L_usil = .false.
              L_(488) = .false.
          endif
      else
          L_(488) = .false.
      endif
C FDA20_SP1.fgi(  94, 636):��� ������� ���������,cset
      if(L_omol) then
          if (L_(488)) then
              I_umol = 13
              L_evil = .true.
              L_(484) = .false.
          endif
          if (L_atil) then
              L_(484) = .true.
              L_evil = .false.
          endif
          if (I_umol.ne.13) then
              L_evil = .false.
              L_(484) = .false.
          endif
      else
          L_(484) = .false.
      endif
C FDA20_SP1.fgi(  94, 625):��� ������� ���������,cset
      if(L_omol) then
          if (L_(484)) then
              I_umol = 14
              L_epil = .true.
              L_(485) = .false.
          endif
          if (L_(486)) then
              L_(485) = .true.
              L_epil = .false.
          endif
          if (I_umol.ne.14) then
              L_epil = .false.
              L_(485) = .false.
          endif
      else
          L_(485) = .false.
      endif
C FDA20_SP1.fgi(  94, 614):��� ������� ���������,cset
      if(L_omol) then
          if (L_(485)) then
              I_umol = 15
              L_imil = .true.
              L_(481) = .false.
          endif
          if (L_edip) then
              L_(481) = .true.
              L_imil = .false.
          endif
          if (I_umol.ne.15) then
              L_imil = .false.
              L_(481) = .false.
          endif
      else
          L_(481) = .false.
      endif
C FDA20_SP1.fgi(  94, 603):��� ������� ���������,cset
      if(L_omol) then
          if (L_(481)) then
              I_umol = 16
              L_olil = .true.
              L_(479) = .false.
          endif
          if (L_(480)) then
              L_(479) = .true.
              L_olil = .false.
          endif
          if (I_umol.ne.16) then
              L_olil = .false.
              L_(479) = .false.
          endif
      else
          L_(479) = .false.
      endif
C FDA20_SP1.fgi(  94, 592):��� ������� ���������,cset
      if(L_omol) then
          if (L_(479)) then
              I_umol = 17
              L_uvil = .true.
              L_(464) = .false.
          endif
          if (L_ixil) then
              L_(464) = .true.
              L_uvil = .false.
          endif
          if (I_umol.ne.17) then
              L_uvil = .false.
              L_(464) = .false.
          endif
      else
          L_(464) = .false.
      endif
C FDA20_SP1.fgi(  94, 581):��� ������� ���������,cset
      if (L_(464)) then
          I_umol = 18
          L_erel = .true.
      endif
      if (L_arel) then
          L_(458) = .true.
          L_erel = .false.
      elseif (L_(459)) then
          L_(477) = .true.
          L_erel = .false.
      endif
      if (I_umol.ne.18) then
          L_erel = .false.
          L_(477) = .false.
          L_(458) = .false.
      endif
C FDA20_SP1.fgi(  94, 568):��� ������� ��������� � ����������,cset
      if(L_omol) then
          if (L_(477)) then
              I_umol = 19
              L_ukil = .true.
              L_(478) = .false.
          endif
          if (L_abol) then
              L_(478) = .true.
              L_ukil = .false.
          endif
          if (I_umol.ne.19) then
              L_ukil = .false.
              L_(478) = .false.
          endif
      else
          L_(478) = .false.
      endif
C FDA20_SP1.fgi(  94, 517):��� ������� ���������,cset
      if(L_omol) then
          if (L_(478)) then
              I_umol = 20
              L_avil = .true.
              L_(463) = .false.
          endif
          if (L_atil) then
              L_(463) = .true.
              L_avil = .false.
          endif
          if (I_umol.ne.20) then
              L_avil = .false.
              L_(463) = .false.
          endif
      else
          L_(463) = .false.
      endif
C FDA20_SP1.fgi(  94, 506):��� ������� ���������,cset
      if(L_omol) then
          if (L_(463)) then
              I_umol = 21
              L_isel = .true.
              L_esel = .false.
          endif
          if (L_(460)) then
              L_esel = .true.
              L_isel = .false.
          endif
          if (I_umol.ne.21) then
              L_isel = .false.
              L_esel = .false.
          endif
      else
          L_esel = .false.
      endif
C FDA20_SP1.fgi(  94, 489):��� ������� ���������,cset
C sav1=L_(460)
      if(.not.L_isel) then
         R0_orel=0.0
      elseif(.not.L0_urel) then
         R0_orel=R0_irel
      else
         R0_orel=max(R_(258)-deltat,0.0)
      endif
      L_(460)=L_isel.and.R0_orel.le.0.0
      L0_urel=L_isel
C FDA20_SP1.fgi(  57, 489):recalc:�������� ��������� ������
C if(sav1.ne.L_(460) .and. try1144.gt.0) goto 1144
C sav1=L_(453)
      if(L_isel.and..not.L0_epel) then
         R0_umel=R0_apel
      else
         R0_umel=max(R_(257)-deltat,0.0)
      endif
      L_(453)=R0_umel.gt.0.0
      L0_epel=L_isel
C FDA20_SP1.fgi( 158, 483):recalc:������������  �� T
C if(sav1.ne.L_(453) .and. try1146.gt.0) goto 1146
      if (L_(458)) then
          I_umol = 22
          L_ufel = .true.
      endif
      if (L_akel) then
          L_(445) = .true.
          L_ufel = .false.
      elseif (L_(446)) then
          L_(455) = .true.
          L_ufel = .false.
      endif
      if (I_umol.ne.22) then
          L_ufel = .false.
          L_(455) = .false.
          L_(445) = .false.
      endif
C FDA20_SP1.fgi(  98, 552):��� ������� ��������� � ����������,cset
      if(L_omol) then
          if (L_(455)) then
              I_umol = 23
              L_opel = .true.
              L_(456) = .false.
          endif
          if (L_abol) then
              L_(456) = .true.
              L_opel = .false.
          endif
          if (I_umol.ne.23) then
              L_opel = .false.
              L_(456) = .false.
          endif
      else
          L_(456) = .false.
      endif
C FDA20_SP1.fgi(  94, 469):��� ������� ���������,cset
      if(L_omol) then
          if (L_(456)) then
              I_umol = 24
              L_util = .true.
              L_(452) = .false.
          endif
          if (L_atil) then
              L_(452) = .true.
              L_util = .false.
          endif
          if (I_umol.ne.24) then
              L_util = .false.
              L_(452) = .false.
          endif
      else
          L_(452) = .false.
      endif
C FDA20_SP1.fgi(  94, 458):��� ������� ���������,cset
      if(L_omol) then
          if (L_(452)) then
              I_umol = 25
              L_olel = .true.
              L_upel = .false.
          endif
          if (L_(449)) then
              L_upel = .true.
              L_olel = .false.
          endif
          if (I_umol.ne.25) then
              L_olel = .false.
              L_upel = .false.
          endif
      else
          L_upel = .false.
      endif
C FDA20_SP1.fgi(  94, 441):��� ������� ���������,cset
C sav1=L_(447)
      if(L_olel.and..not.L0_okel) then
         R0_ekel=R0_ikel
      else
         R0_ekel=max(R_(254)-deltat,0.0)
      endif
      L_(447)=R0_ekel.gt.0.0
      L0_okel=L_olel
C FDA20_SP1.fgi( 158, 435):recalc:������������  �� T
C if(sav1.ne.L_(447) .and. try1175.gt.0) goto 1175
C sav1=L_(449)
      if(.not.L_olel) then
         R0_elel=0.0
      elseif(.not.L0_ilel) then
         R0_elel=R0_alel
      else
         R0_elel=max(R_(255)-deltat,0.0)
      endif
      L_(449)=L_olel.and.R0_elel.le.0.0
      L0_ilel=L_olel
C FDA20_SP1.fgi(  57, 441):recalc:�������� ��������� ������
C if(sav1.ne.L_(449) .and. try1173.gt.0) goto 1173
C sav1=L_(457)
      L_(457) = L_(502).OR.L_upel.OR.L_esel
C FDA20_SP1.fgi( 102, 780):recalc:���
C if(sav1.ne.L_(457) .and. try1189.gt.0) goto 1189
      L_oram=L_olel
C FDA20_SP1.fgi( 173, 441):������,vint_p1
      !{
      Call DAT_DISCR_HANDLER(deltat,I_opam,L_eram,R_aram,
     & REAL(R_iram,4),L_oram,L_ipam,I_upam)
      !}
C FDA20_lamp.fgi( 290, 258):���������� ������� ���������,20FDA21AL002KF02BQ28
      L_(450) = L_util.AND.L_ikil
C FDA20_SP1.fgi( 165, 452):�
      L_ulel=(L_(450).or.L_ulel).and..not.(L_imol)
      L_(451)=.not.L_ulel
C FDA20_SP1.fgi( 181, 450):RS �������
      L_epim=L_ulel
C FDA20_SP1.fgi( 197, 460):������,cyl27
      L_(509) = (.NOT.L_epim)
C FDA20_lamp.fgi( 224, 311):���
      !{
      Call DAT_DISCR_HANDLER(deltat,I_arim,L_orim,R_irim,
     & REAL(R_urim,4),L_(509),L_upim,I_erim)
      !}
C FDA20_lamp.fgi( 238, 310):���������� ������� ���������,20FDA21AL002BQ23
      !{
      Call DAT_DISCR_HANDLER(deltat,I_emim,L_umim,R_omim,
     & REAL(R_apim,4),L_epim,L_amim,I_imim)
      !}
C FDA20_lamp.fgi( 238, 322):���������� ������� ���������,20FDA21AL002BQ24
      L_otim=L_ulel
C FDA20_SP1.fgi( 197, 456):������,cyl28
      L_(510) = (.NOT.L_otim)
C FDA20_lamp.fgi( 277, 311):���
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ivim,L_axim,R_uvim,
     & REAL(R_exim,4),L_(510),L_evim,I_ovim)
      !}
C FDA20_lamp.fgi( 290, 310):���������� ������� ���������,20FDA21AL002BQ21
      !{
      Call DAT_DISCR_HANDLER(deltat,I_osim,L_etim,R_atim,
     & REAL(R_itim,4),L_otim,L_isim,I_usim)
      !}
C FDA20_lamp.fgi( 290, 322):���������� ������� ���������,20FDA21AL002BQ22
      L_ubim=L_ulel
C FDA20_SP1.fgi( 197, 452):������,cyl29
      L_(508) = (.NOT.L_ubim)
C FDA20_lamp.fgi( 277, 279):���
      !{
      Call DAT_DISCR_HANDLER(deltat,I_evem,L_uvem,R_ovem,
     & REAL(R_axem,4),L_(508),L_avem,I_ivem)
      !}
C FDA20_lamp.fgi( 290, 278):���������� ������� ���������,20FDA21AL002BQ28
      !{
      Call DAT_DISCR_HANDLER(deltat,I_uxem,L_ibim,R_ebim,
     & REAL(R_obim,4),L_ubim,L_oxem,I_abim)
      !}
C FDA20_lamp.fgi( 290, 290):���������� ������� ���������,20FDA21AL002BQ27
      L_amel=L_opel
C FDA20_SP1.fgi( 140, 469):������,move_to_y2
      if(L_opel.and..not.L0_exal) then
         R0_uval=R0_axal
      else
         R0_uval=max(R_(250)-deltat,0.0)
      endif
      L_ixal=R0_uval.gt.0.0
      L0_exal=L_opel
C FDA20_SP1.fgi( 125, 473):������������  �� T
      L_ufom=L_isel
C FDA20_SP1.fgi( 173, 489):������,cyl25
      !{
      Call DAT_DISCR_HANDLER(deltat,I_udom,L_ifom,R_efom,
     & REAL(R_ofom,4),L_ufom,L_odom,I_afom)
      !}
C FDA20_lamp.fgi( 122, 234):���������� ������� ���������,20FDA21AL001KE25BQ31
      L_(461) = L_avil.AND.L_ikil
C FDA20_SP1.fgi( 165, 500):�
      L_osel=(L_(461).or.L_osel).and..not.(L_imol)
      L_(462)=.not.L_osel
C FDA20_SP1.fgi( 181, 498):RS �������
      L_ipom=L_osel
C FDA20_SP1.fgi( 197, 508):������,cyl24
      L_(511) = (.NOT.L_ipom)
C FDA20_lamp.fgi( 109, 253):���
      !{
      Call DAT_DISCR_HANDLER(deltat,I_erom,L_urom,R_orom,
     & REAL(R_asom,4),L_(511),L_arom,I_irom)
      !}
C FDA20_lamp.fgi( 122, 252):���������� ������� ���������,20FDA21AL001BQ31
      !{
      Call DAT_DISCR_HANDLER(deltat,I_imom,L_apom,R_umom,
     & REAL(R_epom,4),L_ipom,L_emom,I_omom)
      !}
C FDA20_lamp.fgi( 122, 264):���������� ������� ���������,20FDA21AL001BQ32
      L_olom=L_osel
C FDA20_SP1.fgi( 197, 504):������,cyl23
      L_(513) = (.NOT.L_olom)
C FDA20_lamp.fgi( 109, 283):���
      !{
      Call DAT_DISCR_HANDLER(deltat,I_irum,L_asum,R_urum,
     & REAL(R_esum,4),L_(513),L_erum,I_orum)
      !}
C FDA20_lamp.fgi( 122, 282):���������� ������� ���������,20FDA21AL001BQ33
      !{
      Call DAT_DISCR_HANDLER(deltat,I_okom,L_elom,R_alom,
     & REAL(R_ilom,4),L_olom,L_ikom,I_ukom)
      !}
C FDA20_lamp.fgi( 122, 294):���������� ������� ���������,20FDA21AL001BQ34
      L_amum=L_osel
C FDA20_SP1.fgi( 197, 500):������,cyl22
      L_(512) = (.NOT.L_amum)
C FDA20_lamp.fgi(  56, 283):���
      !{
      Call DAT_DISCR_HANDLER(deltat,I_umum,L_ipum,R_epum,
     & REAL(R_opum,4),L_(512),L_omum,I_apum)
      !}
C FDA20_lamp.fgi(  70, 282):���������� ������� ���������,20FDA21AL001BQ35
      !{
      Call DAT_DISCR_HANDLER(deltat,I_alum,L_olum,R_ilum,
     & REAL(R_ulum,4),L_amum,L_ukum,I_elum)
      !}
C FDA20_lamp.fgi(  70, 294):���������� ������� ���������,20FDA21AL001BQ36
      L_okil=L_ukil
C FDA20_SP1.fgi( 140, 517):������,move_to_y1
      if(L_ukil.and..not.L0_abel) then
         R0_oxal=R0_uxal
      else
         R0_oxal=max(R_(251)-deltat,0.0)
      endif
      L_asel=R0_oxal.gt.0.0
      L0_abel=L_ukil
C FDA20_SP1.fgi( 125, 521):������������  �� T
      if(L_olil.and..not.L0_ilil) then
         R0_alil=R0_elil
      else
         R0_alil=max(R_(268)-deltat,0.0)
      endif
      L_(483)=R0_alil.gt.0.0
      L0_ilil=L_olil
C FDA20_SP1.fgi( 118, 592):������������  �� T
      if(L_imil.and..not.L0_emil) then
         R0_ulil=R0_amil
      else
         R0_ulil=max(R_(269)-deltat,0.0)
      endif
      L_ipil=R0_ulil.gt.0.0
      L0_emil=L_imil
C FDA20_SP1.fgi( 118, 603):������������  �� T
      if(L_epil.and..not.L0_apil) then
         R0_omil=R0_umil
      else
         R0_omil=max(R_(270)-deltat,0.0)
      endif
      L_(482)=R0_omil.gt.0.0
      L0_apil=L_epil
C FDA20_SP1.fgi( 118, 614):������������  �� T
      L_isil=(L_(482).or.L_isil).and..not.(L_(483))
      L_akip=.not.L_isil
C FDA20_SP1.fgi( 181, 612):RS �������
      L_esil=L_isil
C FDA20_SP1.fgi( 197, 642):������,20FDA21AE702VP01_B
      L_asil=L_isil
C FDA20_SP1.fgi( 197, 638):������,20FDA21AE702VP02_B
      L_uril=L_isil
C FDA20_SP1.fgi( 197, 634):������,20FDA21AE702VP03_B
      L_oril=L_isil
C FDA20_SP1.fgi( 197, 630):������,20FDA21AE702VP04_B
      L_iril=L_isil
C FDA20_SP1.fgi( 197, 626):������,20FDA21AE702VP05_B
      L_eril=L_isil
C FDA20_SP1.fgi( 197, 622):������,20FDA21AE702VP06_B
      L_aril=L_isil
C FDA20_SP1.fgi( 197, 618):������,20FDA21AE702VP07_B
      L_upil=L_isil
C FDA20_SP1.fgi( 197, 614):������,20FDA21AE702VP08_B
      !{
      Call DAT_DISCR_HANDLER(deltat,I_afip,L_ofip,R_ifip,
     & REAL(R_ufip,4),L_akip,L_udip,I_efip)
      !}
C FDA20_lamp.fgi( 228, 384):���������� ������� ���������,20FDA21AE702QB86
      L_osil=L_usil
C FDA20_SP1.fgi( 140, 636):������,move_to_kt2
      if(L_(489).and..not.L0_umak) then
         R0_imak=R0_omak
      else
         R0_imak=max(R_(185)-deltat,0.0)
      endif
      L_apak=R0_imak.gt.0.0
      L0_umak=L_(489)
C FDA20_SP1.fgi( 118, 653):������������  �� T
      L_etil=L_itil
C FDA20_SP1.fgi( 140, 669):������,move_to_bvp
      if(L_olol.and..not.L0_okol) then
         R0_ekol=R0_ikol
      else
         R0_ekol=max(R_(273)-deltat,0.0)
      endif
      L_ilol=R0_ekol.gt.0.0
      L0_okol=L_olol
C FDA20_SP1.fgi( 118, 680):������������  �� T
      if(L_afal.and..not.L0_idal) then
         R0_adal=R0_edal
      else
         R0_adal=max(R_(243)-deltat,0.0)
      endif
      L_odal=R0_adal.gt.0.0
      L0_idal=L_afal
C FDA20_SP1.fgi( 118, 691):������������  �� T
      L_uxil=L_ebol
C FDA20_SP1.fgi( 197, 713):������,move_to_tel
      if(L_ekal.and..not.L0_ufal) then
         R0_ifal=R0_ofal
      else
         R0_ifal=max(R_(244)-deltat,0.0)
      endif
      L_akal=R0_ifal.gt.0.0
      L0_ufal=L_ekal
C FDA20_SP1.fgi( 118, 724):������������  �� T
      if(L_amol.and..not.L0_elol) then
         R0_ukol=R0_alol
      else
         R0_ukol=max(R_(274)-deltat,0.0)
      endif
      L_ulol=R0_ukol.gt.0.0
      L0_elol=L_amol
C FDA20_SP1.fgi( 118, 735):������������  �� T
      if(L_adol.and..not.L0_asuk) then
         R0_oruk=R0_uruk
      else
         R0_oruk=max(R_(236)-deltat,0.0)
      endif
      L_(404)=R0_oruk.gt.0.0
      L0_asuk=L_adol
C FDA20_SP1.fgi( 215, 764):������������  �� T
      L_(414) = L_(404).AND.L_epak
C FDA20_SP1.fgi( 227, 763):�
      L0_efal=(L_(414).or.L0_efal).and..not.(L_(415))
      L_(416)=.not.L0_efal
C FDA20_SP1.fgi( 240, 709):RS �������
      if(L0_efal) then
         R0_etuf=0.0
      elseif(L0_ituf) then
         R0_etuf=R0_atuf
      else
         R0_etuf=max(R_(173)-deltat,0.0)
      endif
      L_axir=L0_efal.or.R0_etuf.gt.0.0
      L0_ituf=L0_efal
C FDA20_SP1.fgi( 252, 711):�������� ������� ������
      !{
      Call DAT_DISCR_HANDLER(deltat,I_avir,L_ovir,R_ivir,
     & REAL(R_uvir,4),L_axir,L_utir,I_evir)
      !}
C FDA20_lamp.fgi(  82, 782):���������� ������� ���������,20FDA21CG88
      if(.NOT.L_axir.and..not.L0_usuf) then
         R0_isuf=R0_osuf
      else
         R0_isuf=max(R_(172)-deltat,0.0)
      endif
      L_(309)=R0_isuf.gt.0.0
      L0_usuf=.NOT.L_axir
C FDA20_SP1.fgi( 422, 566):������������  �� T
      !{
      Call DAT_DISCR_HANDLER(deltat,I_utur,L_ivur,R_evur,
     & REAL(R_ovur,4),L_okas,L_otur,I_avur)
      !}
C FDA20_lamp.fgi(  83, 843):���������� ������� ���������,20FDA21AB002BQ04
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ixur,L_abas,R_uxur,
     & REAL(R_ebas,4),L_okas,L_exur,I_oxur)
      !}
C FDA20_lamp.fgi(  83, 854):���������� ������� ���������,20FDA21AB002BQ03
      !{
      Call DAT_DISCR_HANDLER(deltat,I_adas,L_odas,R_idas,
     & REAL(R_udas,4),L_okas,L_ubas,I_edas)
      !}
C FDA20_lamp.fgi(  83, 865):���������� ������� ���������,20FDA21AB002BQ02
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ofas,L_ekas,R_akas,
     & REAL(R_ikas,4),L_okas,L_ifas,I_ufas)
      !}
C FDA20_lamp.fgi(  83, 876):���������� ������� ���������,20FDA21AB002BQ01
      if(.not.L_odel) then
         R0_edel=0.0
      elseif(.not.L0_idel) then
         R0_edel=R0_adel
      else
         R0_edel=max(R_(253)-deltat,0.0)
      endif
      L_(437)=L_odel.and.R0_edel.le.0.0
      L0_idel=L_odel
C FDA20_SP1.fgi(  57, 394):�������� ��������� ������
C label 1401  try1401=try1401-1
      if(L_odel.and..not.L0_obel) then
         R0_ebel=R0_ibel
      else
         R0_ebel=max(R_(252)-deltat,0.0)
      endif
      L_(434)=R0_ebel.gt.0.0
      L0_obel=L_odel
C FDA20_SP1.fgi( 158, 388):������������  �� T
      L_ubel=(L_(434).or.L_ubel).and..not.(L_imol)
      L_(435)=.not.L_ubel
C FDA20_SP1.fgi( 170, 386):RS �������
      L_ifel=L_ubel
C FDA20_SP1.fgi( 193, 388):������,containerY3set
      if (L_(445)) then
          I_umol = 26
          L_ofel = .true.
      endif
      if (L_ifel) then
          L_(442) = .true.
          L_ofel = .false.
      elseif (L_(444)) then
          L_(443) = .true.
          L_ofel = .false.
      endif
      if (I_umol.ne.26) then
          L_ofel = .false.
          L_(443) = .false.
          L_(442) = .false.
      endif
C FDA20_SP1.fgi( 102, 538):��� ������� ��������� � ����������,cset
      if(L_omol) then
          if (L_(443)) then
              I_umol = 27
              L_efel = .true.
              L_(441) = .false.
          endif
          if (L_abol) then
              L_(441) = .true.
              L_efel = .false.
          endif
          if (I_umol.ne.27) then
              L_efel = .false.
              L_(441) = .false.
          endif
      else
          L_(441) = .false.
      endif
C FDA20_SP1.fgi(  94, 422):��� ������� ���������,cset
      if(L_omol) then
          if (L_(441)) then
              I_umol = 28
              L_otil = .true.
              L_(440) = .false.
          endif
          if (L_atil) then
              L_(440) = .true.
              L_otil = .false.
          endif
          if (I_umol.ne.28) then
              L_otil = .false.
              L_(440) = .false.
          endif
      else
          L_(440) = .false.
      endif
C FDA20_SP1.fgi(  94, 411):��� ������� ���������,cset
      if(L_omol) then
          if (L_(440)) then
              I_umol = 29
              L_odel = .true.
              L_(436) = .false.
          endif
          if (L_(437)) then
              L_(436) = .true.
              L_odel = .false.
          endif
          if (I_umol.ne.29) then
              L_odel = .false.
              L_(436) = .false.
          endif
      else
          L_(436) = .false.
      endif
C FDA20_SP1.fgi(  94, 394):��� ������� ���������,cset
C sav1=L_(437)
      if(.not.L_odel) then
         R0_edel=0.0
      elseif(.not.L0_idel) then
         R0_edel=R0_adel
      else
         R0_edel=max(R_(253)-deltat,0.0)
      endif
      L_(437)=L_odel.and.R0_edel.le.0.0
      L0_idel=L_odel
C FDA20_SP1.fgi(  57, 394):recalc:�������� ��������� ������
C if(sav1.ne.L_(437) .and. try1401.gt.0) goto 1401
C sav1=L_(434)
      if(L_odel.and..not.L0_obel) then
         R0_ebel=R0_ibel
      else
         R0_ebel=max(R_(252)-deltat,0.0)
      endif
      L_(434)=R0_ebel.gt.0.0
      L0_obel=L_odel
C FDA20_SP1.fgi( 158, 388):recalc:������������  �� T
C if(sav1.ne.L_(434) .and. try1403.gt.0) goto 1403
      if(L_omol) then
          if (L_(436)) then
              I_umol = 30
              L_utal = .true.
              L_(433) = .false.
          endif
          if (L_abol) then
              L_(433) = .true.
              L_utal = .false.
          endif
          if (I_umol.ne.30) then
              L_utal = .false.
              L_(433) = .false.
          endif
      else
          L_(433) = .false.
      endif
C FDA20_SP1.fgi(  94, 366):��� ������� ���������,cset
      L_otal=L_utal
C FDA20_SP1.fgi( 140, 366):������,move_to_ush1
      if(L_utal.and..not.L0_upuk) then
         R0_ipuk=R0_opuk
      else
         R0_ipuk=max(R_(234)-deltat,0.0)
      endif
      L_(403)=R0_ipuk.gt.0.0
      L0_upuk=L_utal
C FDA20_SP1.fgi( 225, 360):������������  �� T
      L_(411) = L_otek.AND.L_(403)
C FDA20_SP1.fgi( 235, 361):�
      L_ubal=(L_(411).or.L_ubal).and..not.(L_(412))
      L_(413)=.not.L_ubal
C FDA20_SP1.fgi( 246, 359):RS �������
      L_obal=L_ubal
C FDA20_SP1.fgi( 277, 361):������,pu_cont_pres
      if(.NOT.L_obal.and..not.L0_esuf) then
         R0_uruf=R0_asuf
      else
         R0_uruf=max(R_(171)-deltat,0.0)
      endif
      L_(302)=R0_uruf.gt.0.0
      L0_esuf=.NOT.L_obal
C FDA20_SP1.fgi( 422, 560):������������  �� T
      L_umam=L_odel
C FDA20_SP1.fgi( 173, 394):������,cyl16
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ulam,L_imam,R_emam,
     & REAL(R_omam,4),L_umam,L_olam,I_amam)
      !}
C FDA20_lamp.fgi( 290, 158):���������� ������� ���������,20FDA21AL003KE16BQ31
      L_(438) = L_otil.AND.L_ikil
C FDA20_SP1.fgi( 165, 405):�
      L_udel=(L_(438).or.L_udel).and..not.(L_imol)
      L_(439)=.not.L_udel
C FDA20_SP1.fgi( 181, 403):RS �������
      L_ilem=L_udel
C FDA20_SP1.fgi( 197, 413):������,cyl13
      L_(506) = (.NOT.L_ilem)
C FDA20_lamp.fgi( 223, 210):���
      !{
      Call DAT_DISCR_HANDLER(deltat,I_emem,L_umem,R_omem,
     & REAL(R_apem,4),L_(506),L_amem,I_imem)
      !}
C FDA20_lamp.fgi( 236, 208):���������� ������� ���������,20FDA21AL003BQ53
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ikem,L_alem,R_ukem,
     & REAL(R_elem,4),L_ilem,L_ekem,I_okem)
      !}
C FDA20_lamp.fgi( 236, 222):���������� ������� ���������,20FDA21AL003BQ54
      L_urem=L_udel
C FDA20_SP1.fgi( 197, 409):������,cyl14
      L_(507) = (.NOT.L_urem)
C FDA20_lamp.fgi( 276, 210):���
      !{
      Call DAT_DISCR_HANDLER(deltat,I_osem,L_etem,R_atem,
     & REAL(R_item,4),L_(507),L_isem,I_usem)
      !}
C FDA20_lamp.fgi( 290, 208):���������� ������� ���������,20FDA21AL003BQ51
      !{
      Call DAT_DISCR_HANDLER(deltat,I_upem,L_irem,R_erem,
     & REAL(R_orem,4),L_urem,L_opem,I_arem)
      !}
C FDA20_lamp.fgi( 290, 222):���������� ������� ���������,20FDA21AL003BQ52
      L_itam=L_udel
C FDA20_SP1.fgi( 197, 405):������,cyl15
      L_(505) = (.NOT.L_itam)
C FDA20_lamp.fgi( 276, 178):���
      !{
      Call DAT_DISCR_HANDLER(deltat,I_evam,L_uvam,R_ovam,
     & REAL(R_axam,4),L_(505),L_avam,I_ivam)
      !}
C FDA20_lamp.fgi( 290, 176):���������� ������� ���������,20FDA21AL003BQ49
      !{
      Call DAT_DISCR_HANDLER(deltat,I_isam,L_atam,R_usam,
     & REAL(R_etam,4),L_itam,L_esam,I_osam)
      !}
C FDA20_lamp.fgi( 290, 190):���������� ������� ���������,20FDA21AL003BQ50
      L_afel=L_efel
C FDA20_SP1.fgi( 140, 422):������,move_to_y3
      if(L_efel.and..not.L0_ival) then
         R0_aval=R0_eval
      else
         R0_aval=max(R_(249)-deltat,0.0)
      endif
      L_oval=R0_aval.gt.0.0
      L0_ival=L_efel
C FDA20_SP1.fgi( 125, 426):������������  �� T
      L_(408)=R8_olas.gt.R_(194)
C FDA20_SP1.fgi(  53, 348):���������� >
C label 1485  try1485=try1485-1
      L_(410) = L_uxabe.AND.L_odabe.AND.L_(408)
C FDA20_SP1.fgi(  69, 355):�
      if(L_omol) then
          if (L_(433)) then
              I_umol = 31
              L_ibal = .true.
              L_(409) = .false.
          endif
          if (L_(410)) then
              L_(409) = .true.
              L_ibal = .false.
          endif
          if (I_umol.ne.31) then
              L_ibal = .false.
              L_(409) = .false.
          endif
      else
          L_(409) = .false.
      endif
C FDA20_SP1.fgi(  94, 355):��� ������� ���������,cset
      if(L_ibal.and..not.L0_ebal) then
         R0_uxuk=R0_abal
      else
         R0_uxuk=max(R_(242)-deltat,0.0)
      endif
      L_ixabe=R0_uxuk.gt.0.0
      L0_ebal=L_ibal
C FDA20_SP1.fgi( 118, 355):������������  �� T
      L_edabe=L_ixabe
C FDA20_SP1.fgi( 197, 351):������,20FDA21AA202YA21
      L_adabe=L_oxabe
C FDA20_SP1.fgi( 197, 336):������,20FDA21AA202YA22
      !{
      Call KLAPAN_VNB_MAN(deltat,REAL(R_umabe,4),R8_ukabe
     &,I_esabe,I_usabe,I_asabe,
     & C8_ilabe,I_osabe,R_emabe,R_amabe,R_udabe,
     & REAL(R_efabe,4),R_akabe,REAL(R_ikabe,4),
     & R_ifabe,REAL(R_ufabe,4),I_orabe,I_atabe,I_isabe,I_irabe
     &,L_okabe,
     & L_itabe,L_ivabe,L_ekabe,L_ofabe,
     & L_elabe,L_alabe,L_utabe,L_afabe,L_ulabe,L_axabe,L_imabe
     &,
     & L_omabe,REAL(R8_iseke,8),REAL(1.0,4),R8_ebike,L_adabe
     &,L_edabe,L_otabe,
     & I_etabe,L_ovabe,R_erabe,REAL(R_olabe,4),L_uvabe,L_idabe
     &,L_exabe,L_odabe,
     & L_apabe,L_epabe,L_ipabe,L_upabe,L_arabe,L_opabe)
      !}

      if(L_arabe.or.L_upabe.or.L_opabe.or.L_ipabe.or.L_epabe.or.L_apabe
     &) then      
                  I_urabe = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_urabe = z'40000000'
      endif
C FDA20_vlv.fgi( 229, 149):���� ���������� �������� VNB,20FDA21AA202
      L_(405)=R8_olas.gt.R_(194)
C FDA20_SP1.fgi(  53, 333):���������� >
      L_(406) = L_abebe.AND.L_idabe.AND.L_(405)
C FDA20_SP1.fgi(  60, 340):�
      if(.not.L_(406)) then
         R0_ovuk=0.0
      elseif(.not.L0_uvuk) then
         R0_ovuk=R0_ivuk
      else
         R0_ovuk=max(R_(240)-deltat,0.0)
      endif
      L_(407)=L_(406).and.R0_ovuk.le.0.0
      L0_uvuk=L_(406)
C FDA20_SP1.fgi(  68, 340):�������� ��������� ������
      if(L_omol) then
          if (L_(409)) then
              I_umol = 32
              L_oxuk = .true.
              L_(426) = .false.
          endif
          if (L_(407)) then
              L_(426) = .true.
              L_oxuk = .false.
          endif
          if (I_umol.ne.32) then
              L_oxuk = .false.
              L_(426) = .false.
          endif
      else
          L_(426) = .false.
      endif
C FDA20_SP1.fgi(  94, 340):��� ������� ���������,cset
      if(L_oxuk.and..not.L0_ixuk) then
         R0_axuk=R0_exuk
      else
         R0_axuk=max(R_(241)-deltat,0.0)
      endif
      L_oxabe=R0_axuk.gt.0.0
      L0_ixuk=L_oxuk
C FDA20_SP1.fgi( 118, 340):������������  �� T
C sav1=L_adabe
C FDA20_SP1.fgi( 197, 336):recalc:������,20FDA21AA202YA22
C if(sav1.ne.L_adabe .and. try1497.gt.0) goto 1497
      !{
      Call KLAPAN_VNB_MAN(deltat,REAL(R_elebe,4),R8_efebe
     &,I_opebe,I_erebe,I_ipebe,
     & C8_ufebe,I_arebe,R_okebe,R_ikebe,R_ebebe,
     & REAL(R_obebe,4),R_idebe,REAL(R_udebe,4),
     & R_ubebe,REAL(R_edebe,4),I_apebe,I_irebe,I_upebe,I_umebe
     &,L_afebe,
     & L_urebe,L_usebe,L_odebe,L_adebe,
     & L_ofebe,L_ifebe,L_esebe,L_ibebe,L_ekebe,L_itebe,L_ukebe
     &,
     & L_alebe,REAL(R8_iseke,8),REAL(1.0,4),R8_ebike,L_ixabe
     &,L_oxabe,L_asebe,
     & I_orebe,L_atebe,R_omebe,REAL(R_akebe,4),L_etebe,L_uxabe
     &,L_otebe,L_abebe,
     & L_ilebe,L_olebe,L_ulebe,L_emebe,L_imebe,L_amebe)
      !}

      if(L_imebe.or.L_emebe.or.L_amebe.or.L_ulebe.or.L_olebe.or.L_ilebe
     &) then      
                  I_epebe = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_epebe = z'40000000'
      endif
C FDA20_vlv.fgi( 229, 176):���� ���������� �������� VNB,20FDA21AA201
C sav1=L_(406)
      L_(406) = L_abebe.AND.L_idabe.AND.L_(405)
C FDA20_SP1.fgi(  60, 340):recalc:�
C if(sav1.ne.L_(406) .and. try1507.gt.0) goto 1507
C sav1=L_(410)
      L_(410) = L_uxabe.AND.L_odabe.AND.L_(408)
C FDA20_SP1.fgi(  69, 355):recalc:�
C if(sav1.ne.L_(410) .and. try1487.gt.0) goto 1487
      L_(324) = L_uxabe.AND.L_odabe
C FDA20_SP1.fgi( 236, 240):�
      if(L_(324)) then
         R_(195)=R_(196)
      else
         R_(195)=R_(197)
      endif
C FDA20_SP1.fgi( 241, 240):���� RE IN LO CH7
      R8_evek=R8_evek+deltat/R0_avek*R_(195)
      if(R8_evek.gt.R0_utek) then
         R8_evek=R0_utek
      elseif(R8_evek.lt.R0_ivek) then
         R8_evek=R0_ivek
      endif
C FDA20_SP1.fgi( 252, 234):����������
      R8_olas=R8_evek
C FDA20_SP1.fgi( 272, 236):������,20FDA21CP003QP01_P
      R_avov = R8_olas * R_(277)
C FDA20_vent_log.fgi(  96,  80):����������
      !{
      Call DAT_ANA_HANDLER(deltat,R_usov,R_oxov,REAL(1,4)
     &,
     & REAL(R_itov,4),REAL(R_otov,4),
     & REAL(R_osov,4),REAL(R_isov,4),I_ixov,
     & REAL(R_evov,4),L_ivov,REAL(R_ovov,4),L_uvov,L_axov
     &,R_utov,
     & REAL(R_etov,4),REAL(R_atov,4),L_exov,REAL(R_avov,4
     &))
      !}
C FDA20_vlv.fgi( 316, 125):���������� �������,20FDA21CP003XQ01
      if(L_omol) then
          if (L_(426)) then
              I_umol = 33
              L_ural = .true.
              L_(431) = .false.
          endif
          if (L_atup) then
              L_(431) = .true.
              L_ural = .false.
          endif
          if (I_umol.ne.33) then
              L_ural = .false.
              L_(431) = .false.
          endif
      else
          L_(431) = .false.
      endif
C FDA20_SP1.fgi(  94, 327):��� ������� ���������,cset
      if(L_omol) then
          if (L_(431)) then
              I_umol = 34
              L_ital = .true.
              L_(432) = .false.
          endif
          if (L_erup) then
              L_(432) = .true.
              L_ital = .false.
          endif
          if (I_umol.ne.34) then
              L_ital = .false.
              L_(432) = .false.
          endif
      else
          L_(432) = .false.
      endif
C FDA20_SP1.fgi(  94, 316):��� ������� ���������,cset
      if(L_omol) then
          if (L_(432)) then
              I_umol = 35
              L_ipal = .true.
              L_(429) = .false.
          endif
          if (L_uvup) then
              L_(429) = .true.
              L_ipal = .false.
          endif
          if (I_umol.ne.35) then
              L_ipal = .false.
              L_(429) = .false.
          endif
      else
          L_(429) = .false.
      endif
C FDA20_SP1.fgi(  94, 305):��� ������� ���������,cset
      if(L_omol) then
          if (L_(429)) then
              I_umol = 36
              L_isal = .true.
              L_(430) = .false.
          endif
          if (L_ixil) then
              L_(430) = .true.
              L_isal = .false.
          endif
          if (I_umol.ne.36) then
              L_isal = .false.
              L_(430) = .false.
          endif
      else
          L_(430) = .false.
      endif
C FDA20_SP1.fgi(  94, 294):��� ������� ���������,cset
      if(L_omol) then
          if (L_(430)) then
              I_umol = 37
              L_evuk = .true.
              L_(427) = .false.
          endif
          if (L_imup) then
              L_(427) = .true.
              L_evuk = .false.
          endif
          if (I_umol.ne.37) then
              L_evuk = .false.
              L_(427) = .false.
          endif
      else
          L_(427) = .false.
      endif
C FDA20_SP1.fgi(  94, 283):��� ������� ���������,cset
      if(L_omol) then
          if (L_(427)) then
              I_umol = 38
              L_esal = .true.
              L_(428) = .false.
          endif
          if (L_abol) then
              L_(428) = .true.
              L_esal = .false.
          endif
          if (I_umol.ne.38) then
              L_esal = .false.
              L_(428) = .false.
          endif
      else
          L_(428) = .false.
      endif
C FDA20_SP1.fgi(  94, 272):��� ������� ���������,cset
      if(L_omol) then
          if (L_(428)) then
              I_umol = 39
              L_imal = .true.
              L_(425) = .false.
          endif
          if (L_atil) then
              L_(425) = .true.
              L_imal = .false.
          endif
          if (I_umol.ne.39) then
              L_imal = .false.
              L_(425) = .false.
          endif
      else
          L_(425) = .false.
      endif
C FDA20_SP1.fgi(  94, 261):��� ������� ���������,cset
      if(L_omol) then
          if (L_(425)) then
              I_umol = 40
              L_upal = .true.
              L_(424) = .false.
          endif
          if (L_ixil) then
              L_(424) = .true.
              L_upal = .false.
          endif
          if (I_umol.ne.40) then
              L_upal = .false.
              L_(424) = .false.
          endif
      else
          L_(424) = .false.
      endif
C FDA20_SP1.fgi(  94, 245):��� ������� ���������,cset
      if(L_omol) then
          if (L_(424)) then
              I_umol = 41
              L_ulal = .true.
              L_(423) = .false.
          endif
          if (L_abol) then
              L_(423) = .true.
              L_ulal = .false.
          endif
          if (I_umol.ne.41) then
              L_ulal = .false.
              L_(423) = .false.
          endif
      else
          L_(423) = .false.
      endif
C FDA20_SP1.fgi(  94, 234):��� ������� ���������,cset
      if(L_omol) then
          if (L_(423)) then
              I_umol = 42
              L_emal = .true.
              L_(422) = .false.
          endif
          if (L_atil) then
              L_(422) = .true.
              L_emal = .false.
          endif
          if (I_umol.ne.42) then
              L_emal = .false.
              L_(422) = .false.
          endif
      else
          L_(422) = .false.
      endif
C FDA20_SP1.fgi(  94, 223):��� ������� ���������,cset
      L_olal=L_ulal
C FDA20_SP1.fgi( 140, 234):������,move_to_kt1
      if(L_(425).and..not.L0_amak) then
         R0_olak=R0_ulak
      else
         R0_olak=max(R_(184)-deltat,0.0)
      endif
      L_emak=R0_olak.gt.0.0
      L0_amak=L_(425)
C FDA20_SP1.fgi( 118, 253):������������  �� T
      L_asal=L_esal
C FDA20_SP1.fgi( 140, 272):������,move_to_mvp
      if(L_evuk.and..not.L0_avuk) then
         R0_ituk=R0_otuk
      else
         R0_ituk=max(R_(239)-deltat,0.0)
      endif
      L_utuk=R0_ituk.gt.0.0
      L0_avuk=L_evuk
C FDA20_SP1.fgi( 118, 283):������������  �� T
      if(L_ipal.and..not.L0_apal) then
         R0_omal=R0_umal
      else
         R0_omal=max(R_(246)-deltat,0.0)
      endif
      L_epal=R0_omal.gt.0.0
      L0_apal=L_ipal
C FDA20_SP1.fgi( 118, 305):������������  �� T
      if(L_ital.and..not.L0_atal) then
         R0_osal=R0_usal
      else
         R0_osal=max(R_(248)-deltat,0.0)
      endif
      L_etal=R0_osal.gt.0.0
      L0_atal=L_ital
C FDA20_SP1.fgi( 118, 316):������������  �� T
      if(L_ural.and..not.L0_iral) then
         R0_aral=R0_eral
      else
         R0_aral=max(R_(247)-deltat,0.0)
      endif
      L_oral=R0_aral.gt.0.0
      L0_iral=L_ural
C FDA20_SP1.fgi( 118, 327):������������  �� T
      if(.not.L_idik) then
         R0_adik=0.0
      elseif(.not.L0_edik) then
         R0_adik=R0_ubik
      else
         R0_adik=max(R_(200)-deltat,0.0)
      endif
      L_(332)=L_idik.and.R0_adik.le.0.0
      L0_edik=L_idik
C FDA20_SP1.fgi(  57, 145):�������� ��������� ������
C label 1580  try1580=try1580-1
      if(L_olik.and..not.L0_ilik) then
         R0_alik=R0_elik
      else
         R0_alik=max(R_(204)-deltat,0.0)
      endif
      L_(338)=R0_alik.gt.0.0
      L0_ilik=L_olik
C FDA20_SP1.fgi( 445, 180):������������  �� T
      L_(377) =.NOT.(L_ater)
C FDA20_SP1.fgi( 393, 360):���
      if(L_eduk.and..not.L0_exok) then
         R0_uvok=R0_axok
      else
         R0_uvok=max(R_(227)-deltat,0.0)
      endif
      L_(383)=R0_uvok.gt.0.0
      L0_exok=L_eduk
C FDA20_SP1.fgi( 445, 404):������������  �� T
      L_(358) =.NOT.(L_ovar)
C FDA20_SP1.fgi( 393, 261):���
      L_amuk=L_udik
C FDA20_SP1.fgi( 140, 212):������,KT1_start
      if(L_amuk.and..not.L0_uluk) then
         R0_iluk=R0_oluk
      else
         R0_iluk=max(R_(232)-deltat,0.0)
      endif
      L_(402)=R0_iluk.gt.0.0
      L0_uluk=L_amuk
C FDA20_SP1.fgi( 392, 486):������������  �� T
      if(L_oduk.and..not.L0_ubok) then
         R0_ibok=R0_obok
      else
         R0_ibok=max(R_(215)-deltat,0.0)
      endif
      L_(361)=R0_ibok.gt.0.0
      L0_ubok=L_oduk
C FDA20_SP1.fgi( 392, 503):������������  �� T
      if(L_iduk.and..not.L0_idok) then
         R0_adok=R0_edok
      else
         R0_adok=max(R_(216)-deltat,0.0)
      endif
      L_(362)=R0_adok.gt.0.0
      L0_idok=L_iduk
C FDA20_SP1.fgi( 392, 497):������������  �� T
      L_(401) = L_(361).OR.L_(362)
C FDA20_SP1.fgi( 401, 502):���
      if (.not.L_emuk.and.(L_imuk.or.L_(402))) then
          I_omuk = 0
          L_emuk = .true.
          L_(400) = .true.
      endif
      if (I_omuk .gt. 0) then
         L_(400) = .false.
      endif
      if(L_(401))then
          I_omuk = 0
          L_emuk = .false.
          L_(400) = .false.
      endif
C FDA20_SP1.fgi( 414, 478):���������� ������� ���������,kt1sp
      if (L_(400)) then
          I_omuk = 13
          L_uduk = .true.
      endif
      if (L_iduk) then
          L_(386) = .true.
          L_uduk = .false.
      elseif (L_oduk) then
          L_(397) = .true.
          L_uduk = .false.
      endif
      if (I_omuk.ne.13) then
          L_uduk = .false.
          L_(397) = .false.
          L_(386) = .false.
      endif
C FDA20_SP1.fgi( 414, 448):��� ������� ��������� � ����������,kt1sp
      if(L_emuk) then
          if (L_(386)) then
              I_omuk = 14
              L_ebok = .true.
              L_(359) = .false.
          endif
          if (L_iber) then
              L_(359) = .true.
              L_ebok = .false.
          endif
          if (I_omuk.ne.14) then
              L_ebok = .false.
              L_(359) = .false.
          endif
      else
          L_(359) = .false.
      endif
C FDA20_SP1.fgi( 418, 272):��� ������� ���������,kt1sp
      if(L_emuk) then
          if (L_(359)) then
              I_omuk = 15
              L_ixik = .true.
              L_(357) = .false.
          endif
          if (L_(358)) then
              L_(357) = .true.
              L_ixik = .false.
          endif
          if (I_omuk.ne.15) then
              L_ixik = .false.
              L_(357) = .false.
          endif
      else
          L_(357) = .false.
      endif
C FDA20_SP1.fgi( 418, 261):��� ������� ���������,kt1sp
      if(L_ixik.and..not.L0_exik) then
         R0_uvik=R0_axik
      else
         R0_uvik=max(R_(213)-deltat,0.0)
      endif
      L_(356)=R0_uvik.gt.0.0
      L0_exik=L_ixik
C FDA20_SP1.fgi( 445, 261):������������  �� T
      L_(388) = L_(383).OR.L_(356)
C FDA20_SP1.fgi( 489, 403):���
      if(L_ikok.and..not.L0_ekok) then
         R0_ufok=R0_akok
      else
         R0_ufok=max(R_(218)-deltat,0.0)
      endif
      L_(367)=R0_ufok.gt.0.0
      L0_ekok=L_ikok
C FDA20_SP1.fgi( 445, 305):������������  �� T
      if(.not.L0_ifuk) then
         R0_oxok=0.0
      elseif(.not.L0_uxok) then
         R0_oxok=R0_ixok
      else
         R0_oxok=max(R_(228)-deltat,0.0)
      endif
      L_ovar=L0_ifuk.and.R0_oxok.le.0.0
      L0_uxok=L0_ifuk
C FDA20_SP1.fgi( 551, 410):�������� ��������� ������
C sav1=L_(358)
      L_(358) =.NOT.(L_ovar)
C FDA20_SP1.fgi( 393, 261):recalc:���
C if(sav1.ne.L_(358) .and. try1588.gt.0) goto 1588
      L_(385) =.NOT.(L_ovar)
C FDA20_SP1.fgi( 393, 404):���
      if(L_orok.and..not.L0_irok) then
         R0_arok=R0_erok
      else
         R0_arok=max(R_(223)-deltat,0.0)
      endif
      L_(375)=R0_arok.gt.0.0
      L0_irok=L_orok
C FDA20_SP1.fgi( 445, 360):������������  �� T
      if(.not..NOT.L_ater) then
         R0_omik=0.0
      elseif(.not.L0_umik) then
         R0_omik=R0_imik
      else
         R0_omik=max(R_(206)-deltat,0.0)
      endif
      L_(344)=.NOT.L_ater.and.R0_omik.le.0.0
      L0_umik=.NOT.L_ater
C FDA20_SP1.fgi( 380, 191):�������� ��������� ������
      if(L_emuk) then
          if (L_(357)) then
              I_omuk = 16
              L_ovik = .true.
              L_(355) = .false.
          endif
          if (L_obir) then
              L_(355) = .true.
              L_ovik = .false.
          endif
          if (I_omuk.ne.16) then
              L_ovik = .false.
              L_(355) = .false.
          endif
      else
          L_(355) = .false.
      endif
C FDA20_SP1.fgi( 418, 250):��� ������� ���������,kt1sp
      if(L_emuk) then
          if (L_(355)) then
              I_omuk = 17
              L_utik = .true.
              L_(353) = .false.
          endif
          if (L_udar) then
              L_(353) = .true.
              L_utik = .false.
          endif
          if (I_omuk.ne.17) then
              L_utik = .false.
              L_(353) = .false.
          endif
      else
          L_(353) = .false.
      endif
C FDA20_SP1.fgi( 418, 239):��� ������� ���������,kt1sp
      if(L_emuk) then
          if (L_(353)) then
              I_omuk = 18
              L_atik = .true.
              L_(351) = .false.
          endif
          if (L_imar) then
              L_(351) = .true.
              L_atik = .false.
          endif
          if (I_omuk.ne.18) then
              L_atik = .false.
              L_(351) = .false.
          endif
      else
          L_(351) = .false.
      endif
C FDA20_SP1.fgi( 418, 228):��� ������� ���������,kt1sp
      if(L_emuk) then
          if (L_(351)) then
              I_omuk = 19
              L_esik = .true.
              L_(348) = .false.
          endif
          if (L_ater) then
              L_(348) = .true.
              L_esik = .false.
          endif
          if (I_omuk.ne.19) then
              L_esik = .false.
              L_(348) = .false.
          endif
      else
          L_(348) = .false.
      endif
C FDA20_SP1.fgi( 418, 217):��� ������� ���������,kt1sp
      if(L_emuk) then
          if (L_(348)) then
              I_omuk = 20
              L_irik = .true.
              L_(345) = .false.
          endif
          if (L_elir) then
              L_(345) = .true.
              L_irik = .false.
          endif
          if (I_omuk.ne.20) then
              L_irik = .false.
              L_(345) = .false.
          endif
      else
          L_(345) = .false.
      endif
C FDA20_SP1.fgi( 418, 206):��� ������� ���������,kt1sp
      if(L_emuk) then
          if (L_(345)) then
              I_omuk = 21
              L_opik = .true.
              L_(343) = .false.
          endif
          if (L_(344)) then
              L_(343) = .true.
              L_opik = .false.
          endif
          if (I_omuk.ne.21) then
              L_opik = .false.
              L_(343) = .false.
          endif
      else
          L_(343) = .false.
      endif
C FDA20_SP1.fgi( 418, 191):��� ������� ���������,kt1sp
      if(L_opik.and..not.L0_ipik) then
         R0_apik=R0_epik
      else
         R0_apik=max(R_(207)-deltat,0.0)
      endif
      L_(349)=R0_apik.gt.0.0
      L0_ipik=L_opik
C FDA20_SP1.fgi( 462, 191):������������  �� T
      L_(391) = L_(375).OR.L_(349)
C FDA20_SP1.fgi( 489, 359):���
      if(L_esik.and..not.L0_asik) then
         R0_orik=R0_urik
      else
         R0_orik=max(R_(209)-deltat,0.0)
      endif
      L_(347)=R0_orik.gt.0.0
      L0_asik=L_esik
C FDA20_SP1.fgi( 445, 217):������������  �� T
      if(L_(344).and..not.L0_emik) then
         R0_ulik=R0_amik
      else
         R0_ulik=max(R_(205)-deltat,0.0)
      endif
      L_(341)=R0_ulik.gt.0.0
      L0_emik=L_(344)
C FDA20_SP1.fgi( 445, 199):������������  �� T
      L_(342) = L_(341).AND.L_opik
C FDA20_SP1.fgi( 456, 198):�
      if(.not.L0_ofuk) then
         R0_ebuk=0.0
      elseif(.not.L0_ibuk) then
         R0_ebuk=R0_abuk
      else
         R0_ebuk=max(R_(229)-deltat,0.0)
      endif
      L_ater=L0_ofuk.and.R0_ebuk.le.0.0
      L0_ibuk=L0_ofuk
C FDA20_SP1.fgi( 551, 426):�������� ��������� ������
C sav1=L_(377)
      L_(377) =.NOT.(L_ater)
C FDA20_SP1.fgi( 393, 360):recalc:���
C if(sav1.ne.L_(377) .and. try1584.gt.0) goto 1584
C sav1=L_(344)
      if(.not..NOT.L_ater) then
         R0_omik=0.0
      elseif(.not.L0_umik) then
         R0_omik=R0_imik
      else
         R0_omik=max(R_(206)-deltat,0.0)
      endif
      L_(344)=.NOT.L_ater.and.R0_omik.le.0.0
      L0_umik=.NOT.L_ater
C FDA20_SP1.fgi( 380, 191):recalc:�������� ��������� ������
C if(sav1.ne.L_(344) .and. try1633.gt.0) goto 1633
C sav1=L_esik
C sav2=L_(348)
      if(L_emuk) then
          if (L_(351)) then
              I_omuk = 19
              L_esik = .true.
              L_(348) = .false.
          endif
          if (L_ater) then
              L_(348) = .true.
              L_esik = .false.
          endif
          if (I_omuk.ne.19) then
              L_esik = .false.
              L_(348) = .false.
          endif
      else
          L_(348) = .false.
      endif
C FDA20_SP1.fgi( 418, 217):recalc:��� ������� ���������,kt1sp
C if(sav1.ne.L_esik .and. try1642.gt.0) goto 1642
C if(sav2.ne.L_(348) .and. try1642.gt.0) goto 1642
      L_(399) = L_okar.AND.L_erar.AND.(.NOT.L_orir).AND.L_elir.AND.L_ate
     &r.AND.L_obir.AND.L_iber.AND.L_ovar
C FDA20_SP1.fgi( 393, 431):�
      if(L_emuk) then
          if (L_(397)) then
              I_omuk = 1
              L_eluk = .true.
              L_(398) = .false.
          endif
          if (L_(399)) then
              L_(398) = .true.
              L_eluk = .false.
          endif
          if (I_omuk.ne.1) then
              L_eluk = .false.
              L_(398) = .false.
          endif
      else
          L_(398) = .false.
      endif
C FDA20_SP1.fgi( 414, 436):��� ������� ���������,kt1sp
      if(L_eluk.and..not.L0_aluk) then
         R0_okuk=R0_ukuk
      else
         R0_okuk=max(R_(231)-deltat,0.0)
      endif
      L_(396)=R0_okuk.gt.0.0
      L0_aluk=L_eluk
C FDA20_SP1.fgi( 445, 436):������������  �� T
      L_(394) = L_(338).OR.L_(396)
C FDA20_SP1.fgi( 489, 437):���
      if(L_emuk) then
          if (L_(398)) then
              I_omuk = 2
              L_eduk = .true.
              L_(384) = .false.
          endif
          if (L_(385)) then
              L_(384) = .true.
              L_eduk = .false.
          endif
          if (I_omuk.ne.2) then
              L_eduk = .false.
              L_(384) = .false.
          endif
      else
          L_(384) = .false.
      endif
C FDA20_SP1.fgi( 414, 404):��� ������� ���������,kt1sp
C sav1=L_(383)
      if(L_eduk.and..not.L0_exok) then
         R0_uvok=R0_axok
      else
         R0_uvok=max(R_(227)-deltat,0.0)
      endif
      L_(383)=R0_uvok.gt.0.0
      L0_exok=L_eduk
C FDA20_SP1.fgi( 445, 404):recalc:������������  �� T
C if(sav1.ne.L_(383) .and. try1586.gt.0) goto 1586
      if(L_emuk) then
          if (L_(384)) then
              I_omuk = 3
              L_ovok = .true.
              L_(382) = .false.
          endif
          if (L_udar) then
              L_(382) = .true.
              L_ovok = .false.
          endif
          if (I_omuk.ne.3) then
              L_ovok = .false.
              L_(382) = .false.
          endif
      else
          L_(382) = .false.
      endif
C FDA20_SP1.fgi( 414, 393):��� ������� ���������,kt1sp
      if(L_emuk) then
          if (L_(382)) then
              I_omuk = 4
              L_otok = .true.
              L_(380) = .false.
          endif
          if (L_imar) then
              L_(380) = .true.
              L_otok = .false.
          endif
          if (I_omuk.ne.4) then
              L_otok = .false.
              L_(380) = .false.
          endif
      else
          L_(380) = .false.
      endif
C FDA20_SP1.fgi( 414, 382):��� ������� ���������,kt1sp
      if(L_emuk) then
          if (L_(380)) then
              I_omuk = 5
              L_osok = .true.
              L_(378) = .false.
          endif
          if (L_ifir) then
              L_(378) = .true.
              L_osok = .false.
          endif
          if (I_omuk.ne.5) then
              L_osok = .false.
              L_(378) = .false.
          endif
      else
          L_(378) = .false.
      endif
C FDA20_SP1.fgi( 414, 371):��� ������� ���������,kt1sp
      if(L_emuk) then
          if (L_(378)) then
              I_omuk = 6
              L_orok = .true.
              L_(376) = .false.
          endif
          if (L_(377)) then
              L_(376) = .true.
              L_orok = .false.
          endif
          if (I_omuk.ne.6) then
              L_orok = .false.
              L_(376) = .false.
          endif
      else
          L_(376) = .false.
      endif
C FDA20_SP1.fgi( 414, 360):��� ������� ���������,kt1sp
C sav1=L_(375)
      if(L_orok.and..not.L0_irok) then
         R0_arok=R0_erok
      else
         R0_arok=max(R_(223)-deltat,0.0)
      endif
      L_(375)=R0_arok.gt.0.0
      L0_irok=L_orok
C FDA20_SP1.fgi( 445, 360):recalc:������������  �� T
C if(sav1.ne.L_(375) .and. try1631.gt.0) goto 1631
      if(L_emuk) then
          if (L_(376)) then
              I_omuk = 7
              L_upok = .true.
              L_(374) = .false.
          endif
          if (L_orir) then
              L_(374) = .true.
              L_upok = .false.
          endif
          if (I_omuk.ne.7) then
              L_upok = .false.
              L_(374) = .false.
          endif
      else
          L_(374) = .false.
      endif
C FDA20_SP1.fgi( 414, 349):��� ������� ���������,kt1sp
      if(L_upok.and..not.L0_opok) then
         R0_epok=R0_ipok
      else
         R0_epok=max(R_(222)-deltat,0.0)
      endif
      L_(393)=R0_epok.gt.0.0
      L0_opok=L_upok
C FDA20_SP1.fgi( 445, 349):������������  �� T
      L0_akuk=(L_(393).or.L0_akuk).and..not.(L_(394))
      L_(395)=.not.L0_akuk
C FDA20_SP1.fgi( 535, 439):RS �������
      if(.not.L0_akuk) then
         R0_ubuk=0.0
      elseif(.not.L0_aduk) then
         R0_ubuk=R0_obuk
      else
         R0_ubuk=max(R_(230)-deltat,0.0)
      endif
      L_orir=L0_akuk.and.R0_ubuk.le.0.0
      L0_aduk=L0_akuk
C FDA20_SP1.fgi( 551, 441):�������� ��������� ������
C sav1=L_upok
C sav2=L_(374)
      if(L_emuk) then
          if (L_(376)) then
              I_omuk = 7
              L_upok = .true.
              L_(374) = .false.
          endif
          if (L_orir) then
              L_(374) = .true.
              L_upok = .false.
          endif
          if (I_omuk.ne.7) then
              L_upok = .false.
              L_(374) = .false.
          endif
      else
          L_(374) = .false.
      endif
C FDA20_SP1.fgi( 414, 349):recalc:��� ������� ���������,kt1sp
C if(sav1.ne.L_upok .and. try1701.gt.0) goto 1701
C if(sav2.ne.L_(374) .and. try1701.gt.0) goto 1701
C sav1=L_(399)
      L_(399) = L_okar.AND.L_erar.AND.(.NOT.L_orir).AND.L_elir.AND.L_ate
     &r.AND.L_obir.AND.L_iber.AND.L_ovar
C FDA20_SP1.fgi( 393, 431):recalc:�
C if(sav1.ne.L_(399) .and. try1674.gt.0) goto 1674
      L_(340) =.NOT.(L_orir)
C FDA20_SP1.fgi( 393, 180):���
      if(L_emuk) then
          if (L_(343)) then
              I_omuk = 22
              L_olik = .true.
              L_(339) = .false.
          endif
          if (L_(340)) then
              L_(339) = .true.
              L_olik = .false.
          endif
          if (I_omuk.ne.22) then
              L_olik = .false.
              L_(339) = .false.
          endif
      else
          L_(339) = .false.
      endif
C FDA20_SP1.fgi( 418, 180):��� ������� ���������,kt1sp
C sav1=L_(338)
      if(L_olik.and..not.L0_ilik) then
         R0_alik=R0_elik
      else
         R0_alik=max(R_(204)-deltat,0.0)
      endif
      L_(338)=R0_alik.gt.0.0
      L0_ilik=L_olik
C FDA20_SP1.fgi( 445, 180):recalc:������������  �� T
C if(sav1.ne.L_(338) .and. try1582.gt.0) goto 1582
      if(L_emuk) then
          if (L_(339)) then
              I_omuk = 23
              L_ukik = .true.
              L_(337) = .false.
          endif
          if (L_okar) then
              L_(337) = .true.
              L_ukik = .false.
          endif
          if (I_omuk.ne.23) then
              L_ukik = .false.
              L_(337) = .false.
          endif
      else
          L_(337) = .false.
      endif
C FDA20_SP1.fgi( 418, 169):��� ������� ���������,kt1sp
      if(L_(337).and..not.L0_ifik) then
         R0_afik=R0_efik
      else
         R0_afik=max(R_(201)-deltat,0.0)
      endif
      L_(364)=R0_afik.gt.0.0
      L0_ifik=L_(337)
C FDA20_SP1.fgi( 445, 162):������������  �� T
      if(L_emuk) then
          if (L_(374)) then
              I_omuk = 8
              L_apok = .true.
              L_(372) = .false.
          endif
          if (L_erar) then
              L_(372) = .true.
              L_apok = .false.
          endif
          if (I_omuk.ne.8) then
              L_apok = .false.
              L_(372) = .false.
          endif
      else
          L_(372) = .false.
      endif
C FDA20_SP1.fgi( 414, 338):��� ������� ���������,kt1sp
      if(L_emuk) then
          if (L_(372)) then
              I_omuk = 9
              L_emok = .true.
              L_(369) = .false.
          endif
          if (L_okar) then
              L_(369) = .true.
              L_emok = .false.
          endif
          if (I_omuk.ne.9) then
              L_emok = .false.
              L_(369) = .false.
          endif
      else
          L_(369) = .false.
      endif
C FDA20_SP1.fgi( 414, 327):��� ������� ���������,kt1sp
      if(L_emuk) then
          if (L_(369)) then
              I_omuk = 10
              L_ilok = .true.
              L_(368) = .false.
          endif
          if (L_uver) then
              L_(368) = .true.
              L_ilok = .false.
          endif
          if (I_omuk.ne.10) then
              L_ilok = .false.
              L_(368) = .false.
          endif
      else
          L_(368) = .false.
      endif
C FDA20_SP1.fgi( 414, 316):��� ������� ���������,kt1sp
      if(L_emuk) then
          if (L_(368)) then
              I_omuk = 11
              L_ikok = .true.
              L_(366) = .false.
          endif
          if (L_ovar) then
              L_(366) = .true.
              L_ikok = .false.
          endif
          if (I_omuk.ne.11) then
              L_ikok = .false.
              L_(366) = .false.
          endif
      else
          L_(366) = .false.
      endif
C FDA20_SP1.fgi( 414, 305):��� ������� ���������,kt1sp
C sav1=L_(367)
      if(L_ikok.and..not.L0_ekok) then
         R0_ufok=R0_akok
      else
         R0_ufok=max(R_(218)-deltat,0.0)
      endif
      L_(367)=R0_ufok.gt.0.0
      L0_ekok=L_ikok
C FDA20_SP1.fgi( 445, 305):recalc:������������  �� T
C if(sav1.ne.L_(367) .and. try1613.gt.0) goto 1613
      if(L_emuk) then
          if (L_(366)) then
              I_omuk = 12
              L_ofok = .true.
              L_(365) = .false.
          endif
          if (L_omer) then
              L_(365) = .true.
              L_ofok = .false.
          endif
          if (I_omuk.ne.12) then
              L_ofok = .false.
              L_(365) = .false.
          endif
      else
          L_(365) = .false.
      endif
C FDA20_SP1.fgi( 414, 294):��� ������� ���������,kt1sp
      if(L_(365).and..not.L0_akik) then
         R0_ofik=R0_ufik
      else
         R0_ofik=max(R_(202)-deltat,0.0)
      endif
      L_(363)=R0_ofik.gt.0.0
      L0_akik=L_(365)
C FDA20_SP1.fgi( 445, 287):������������  �� T
      L_odok=(L_(363).or.L_odok).and..not.(L_(364))
      L_oduk=.not.L_odok
C FDA20_SP1.fgi( 528, 285):RS �������
C sav1=L_(361)
      if(L_oduk.and..not.L0_ubok) then
         R0_ibok=R0_obok
      else
         R0_ibok=max(R_(215)-deltat,0.0)
      endif
      L_(361)=R0_ibok.gt.0.0
      L0_ubok=L_oduk
C FDA20_SP1.fgi( 392, 503):recalc:������������  �� T
C if(sav1.ne.L_(361) .and. try1595.gt.0) goto 1595
      L_iduk=L_odok
C FDA20_SP1.fgi( 548, 287):������,from_y4
C sav1=L_uduk
C sav2=L_(397)
C sav3=L_(386)
      if (L_(400)) then
          I_omuk = 13
          L_uduk = .true.
      endif
      if (L_iduk) then
          L_(386) = .true.
          L_uduk = .false.
      elseif (L_oduk) then
          L_(397) = .true.
          L_uduk = .false.
      endif
      if (I_omuk.ne.13) then
          L_uduk = .false.
          L_(397) = .false.
          L_(386) = .false.
      endif
C FDA20_SP1.fgi( 414, 448):recalc:��� ������� ��������� � ����������,kt1sp
C if(sav1.ne.L_uduk .and. try1603.gt.0) goto 1603
C if(sav2.ne.L_(397) .and. try1603.gt.0) goto 1603
C if(sav3.ne.L_(386) .and. try1603.gt.0) goto 1603
C sav1=L_(362)
      if(L_iduk.and..not.L0_idok) then
         R0_adok=R0_edok
      else
         R0_adok=max(R_(216)-deltat,0.0)
      endif
      L_(362)=R0_adok.gt.0.0
      L0_idok=L_iduk
C FDA20_SP1.fgi( 392, 497):recalc:������������  �� T
C if(sav1.ne.L_(362) .and. try1597.gt.0) goto 1597
      if(L_omol) then
          if (L_(422)) then
              I_umol = 43
              L_udik = .true.
              L_(335) = .false.
          endif
          if (L_iduk) then
              L_(335) = .true.
              L_udik = .false.
          endif
          if (I_umol.ne.43) then
              L_udik = .false.
              L_(335) = .false.
          endif
      else
          L_(335) = .false.
      endif
C FDA20_SP1.fgi(  94, 212):��� ������� ���������,cset
C sav1=L_amuk
C FDA20_SP1.fgi( 140, 212):recalc:������,KT1_start
C if(sav1.ne.L_amuk .and. try1590.gt.0) goto 1590
      if(L_omol) then
          if (L_(335)) then
              I_umol = 44
              L_opal = .true.
              L_(327) = .false.
          endif
          if (L_ixil) then
              L_(327) = .true.
              L_opal = .false.
          endif
          if (I_umol.ne.44) then
              L_opal = .false.
              L_(327) = .false.
          endif
      else
          L_(327) = .false.
      endif
C FDA20_SP1.fgi(  94, 201):��� ������� ���������,cset
      if(L_omol) then
          if (L_(327)) then
              I_umol = 45
              L_oxek = .true.
              L_(326) = .false.
          endif
          if (L_abol) then
              L_(326) = .true.
              L_oxek = .false.
          endif
          if (I_umol.ne.45) then
              L_oxek = .false.
              L_(326) = .false.
          endif
      else
          L_(326) = .false.
      endif
C FDA20_SP1.fgi(  94, 190):��� ������� ���������,cset
      if(L_omol) then
          if (L_(326)) then
              I_umol = 46
              L_amal = .true.
              L_(330) = .false.
          endif
          if (L_atil) then
              L_(330) = .true.
              L_amal = .false.
          endif
          if (I_umol.ne.46) then
              L_amal = .false.
              L_(330) = .false.
          endif
      else
          L_(330) = .false.
      endif
C FDA20_SP1.fgi(  94, 179):��� ������� ���������,cset
      if(L_omol) then
          if (L_(330)) then
              I_umol = 47
              L_idik = .true.
              L_(331) = .false.
          endif
          if (L_(332)) then
              L_(331) = .true.
              L_idik = .false.
          endif
          if (I_umol.ne.47) then
              L_idik = .false.
              L_(331) = .false.
          endif
      else
          L_(331) = .false.
      endif
C FDA20_SP1.fgi(  94, 145):��� ������� ���������,cset
C sav1=L_(332)
      if(.not.L_idik) then
         R0_adik=0.0
      elseif(.not.L0_edik) then
         R0_adik=R0_ubik
      else
         R0_adik=max(R_(200)-deltat,0.0)
      endif
      L_(332)=L_idik.and.R0_adik.le.0.0
      L0_edik=L_idik
C FDA20_SP1.fgi(  57, 145):recalc:�������� ��������� ������
C if(sav1.ne.L_(332) .and. try1580.gt.0) goto 1580
      L_(387) = L_(396).OR.L_(367)
C FDA20_SP1.fgi( 489, 410):���
      L0_ifuk=(L_(387).or.L0_ifuk).and..not.(L_(388))
      L_(389)=.not.L0_ifuk
C FDA20_SP1.fgi( 535, 408):RS �������
C sav1=L_ovar
      if(.not.L0_ifuk) then
         R0_oxok=0.0
      elseif(.not.L0_uxok) then
         R0_oxok=R0_ixok
      else
         R0_oxok=max(R_(228)-deltat,0.0)
      endif
      L_ovar=L0_ifuk.and.R0_oxok.le.0.0
      L0_uxok=L0_ifuk
C FDA20_SP1.fgi( 551, 410):recalc:�������� ��������� ������
C if(sav1.ne.L_ovar .and. try1615.gt.0) goto 1615
      L_(390) = L_(396).OR.L_(347).OR.L_(342)
C FDA20_SP1.fgi( 489, 426):���
      L0_ofuk=(L_(390).or.L0_ofuk).and..not.(L_(391))
      L_(392)=.not.L0_ofuk
C FDA20_SP1.fgi( 535, 424):RS �������
C sav1=L_ater
      if(.not.L0_ofuk) then
         R0_ebuk=0.0
      elseif(.not.L0_ibuk) then
         R0_ebuk=R0_abuk
      else
         R0_ebuk=max(R_(229)-deltat,0.0)
      endif
      L_ater=L0_ofuk.and.R0_ebuk.le.0.0
      L0_ibuk=L0_ofuk
C FDA20_SP1.fgi( 551, 426):recalc:�������� ��������� ������
C if(sav1.ne.L_ater .and. try1660.gt.0) goto 1660
      L_(421) = L_(311).OR.L_(331)
C FDA20_SP1.fgi( 103, 129):���
      if(L_omol) then
          if (L_(421)) then
              I_umol = 48
              L_ilal = .true.
              L_(504) = .false.
          endif
          if (L_abol) then
              L_(504) = .true.
              L_ilal = .false.
          endif
          if (I_umol.ne.48) then
              L_ilal = .false.
              L_(504) = .false.
          endif
      else
          L_(504) = .false.
      endif
C FDA20_SP1.fgi(  95, 120):��� ������� ���������,cset
      if(L_(504)) then
         I_umol=0
         L_omol=.false.
      endif
C FDA20_SP1.fgi(  95, 109):����� ������� ���������,cset
      L_edol = L_ilal.OR.L_adol
C FDA20_SP1.fgi( 181, 770):���
      if(L_edol.and..not.L0_axel) then
         R0_ovel=R0_uvel
      else
         R0_ovel=max(R_(262)-deltat,0.0)
      endif
      L_(465)=R0_ovel.gt.0.0
      L0_axel=L_edol
C FDA20_SP1.fgi( 541, 748):������������  �� T
      L_odop=L_idik
C FDA20_SP1.fgi( 173, 145):������,vint_p2
      !{
      Call DAT_DISCR_HANDLER(deltat,I_obop,L_edop,R_adop,
     & REAL(R_idop,4),L_odop,L_ibop,I_ubop)
      !}
C FDA20_lamp.fgi( 122, 434):���������� ������� ���������,20FDA21AL004KF02Q41
      if(L_idik.and..not.L0_ibik) then
         R0_abik=R0_ebik
      else
         R0_abik=max(R_(199)-deltat,0.0)
      endif
      L_(328)=R0_abik.gt.0.0
      L0_ibik=L_idik
C FDA20_SP1.fgi( 158, 139):������������  �� T
      L_obik=(L_(328).or.L_obik).and..not.(L_imol)
      L_(329)=.not.L_obik
C FDA20_SP1.fgi( 170, 137):RS �������
      L_uxek=L_obik
C FDA20_SP1.fgi( 193, 139):������,containerY4set
      L_evak =.NOT.(L_arel.OR.L_akel.OR.L_ifel.OR.L_uxek)
C FDA20_SP1.fgi( 428, 610):���
      L_(310) = L_evak.AND.L_(309)
C FDA20_SP1.fgi( 440, 579):�
      if(L_(310).and..not.L0_uxuf) then
         R0_ixuf=R0_oxuf
      else
         R0_ixuf=max(R_(177)-deltat,0.0)
      endif
      L_usak=R0_ixuf.gt.0.0
      L0_uxuf=L_(310)
C FDA20_SP1.fgi( 465, 550):������������  �� T
      L_(321) = L_usak.AND.L_evak
C FDA20_SP1.fgi( 414, 781):�
      if(.not.L_(310)) then
         R0_afak=0.0
      elseif(.not.L0_efak) then
         R0_afak=R0_udak
      else
         R0_afak=max(R_(181)-deltat,0.0)
      endif
      L_(308)=L_(310).and.R0_afak.le.0.0
      L0_efak=L_(310)
C FDA20_SP1.fgi( 456, 579):�������� ��������� ������
      if(L_(308).and..not.L0_osak) then
         R0_esak=R0_isak
      else
         R0_esak=max(R_(189)-deltat,0.0)
      endif
      L_orek=R0_esak.gt.0.0
      L0_osak=L_(308)
C FDA20_SP1.fgi( 465, 579):������������  �� T
      L_avak = L_arel.AND.(.NOT.L_akel).AND.(.NOT.L_ifel).AND.
     &(.NOT.L_uxek)
C FDA20_SP1.fgi( 428, 602):�
      L_(307) = L_avak.AND.L_(309)
C FDA20_SP1.fgi( 440, 573):�
      if(L_(307).and..not.L0_exuf) then
         R0_uvuf=R0_axuf
      else
         R0_uvuf=max(R_(176)-deltat,0.0)
      endif
      L_atak=R0_uvuf.gt.0.0
      L0_exuf=L_(307)
C FDA20_SP1.fgi( 465, 544):������������  �� T
      L_(318) = L_atak.AND.L_avak
C FDA20_SP1.fgi( 414, 752):�
      if(.not.L_(307)) then
         R0_idak=0.0
      elseif(.not.L0_odak) then
         R0_idak=R0_edak
      else
         R0_idak=max(R_(180)-deltat,0.0)
      endif
      L_(306)=L_(307).and.R0_idak.le.0.0
      L0_odak=L_(307)
C FDA20_SP1.fgi( 456, 573):�������� ��������� ������
      if(L_(306).and..not.L0_asak) then
         R0_orak=R0_urak
      else
         R0_orak=max(R_(188)-deltat,0.0)
      endif
      L_irek=R0_orak.gt.0.0
      L0_asak=L_(306)
C FDA20_SP1.fgi( 465, 573):������������  �� T
      L_utak = L_arel.AND.L_akel.AND.(.NOT.L_ifel).AND.(.NOT.L_uxek
     &)
C FDA20_SP1.fgi( 428, 594):�
      L_(305) = L_utak.AND.L_(309)
C FDA20_SP1.fgi( 440, 567):�
      if(L_(305).and..not.L0_ovuf) then
         R0_evuf=R0_ivuf
      else
         R0_evuf=max(R_(175)-deltat,0.0)
      endif
      L_etak=R0_evuf.gt.0.0
      L0_ovuf=L_(305)
C FDA20_SP1.fgi( 465, 538):������������  �� T
      L_(315) = L_etak.AND.L_utak
C FDA20_SP1.fgi( 414, 723):�
      if(.not.L_(305)) then
         R0_ubak=0.0
      elseif(.not.L0_adak) then
         R0_ubak=R0_obak
      else
         R0_ubak=max(R_(179)-deltat,0.0)
      endif
      L_(304)=L_(305).and.R0_ubak.le.0.0
      L0_adak=L_(305)
C FDA20_SP1.fgi( 456, 567):�������� ��������� ������
      if(L_(304).and..not.L0_irak) then
         R0_arak=R0_erak
      else
         R0_arak=max(R_(187)-deltat,0.0)
      endif
      L_urek=R0_arak.gt.0.0
      L0_irak=L_(304)
C FDA20_SP1.fgi( 465, 567):������������  �� T
      L_otak = L_arel.AND.L_akel.AND.L_ifel.AND.(.NOT.L_uxek
     &)
C FDA20_SP1.fgi( 428, 586):�
      L_(303) = L_otak.AND.L_(302)
C FDA20_SP1.fgi( 440, 561):�
      if(L_(303).and..not.L0_avuf) then
         R0_otuf=R0_utuf
      else
         R0_otuf=max(R_(174)-deltat,0.0)
      endif
      L_itak=R0_otuf.gt.0.0
      L0_avuf=L_(303)
C FDA20_SP1.fgi( 465, 532):������������  �� T
      L_(312) = L_itak.AND.L_otak
C FDA20_SP1.fgi( 414, 694):�
      if(.not.L_(303)) then
         R0_ebak=0.0
      elseif(.not.L0_ibak) then
         R0_ebak=R0_abak
      else
         R0_ebak=max(R_(178)-deltat,0.0)
      endif
      L_(301)=L_(303).and.R0_ebak.le.0.0
      L0_ibak=L_(303)
C FDA20_SP1.fgi( 456, 561):�������� ��������� ������
      if(L_(301).and..not.L0_upak) then
         R0_ipak=R0_opak
      else
         R0_ipak=max(R_(186)-deltat,0.0)
      endif
      L_asek=R0_ipak.gt.0.0
      L0_upak=L_(301)
C FDA20_SP1.fgi( 465, 561):������������  �� T
      L_(491) = L_imal.OR.L_emal.OR.L_amal
C FDA20_SP1.fgi( 173, 259):���
      L_ovil = L_ivil.OR.L_evil.OR.L_avil.OR.L_util.OR.L_otil.OR.L_
     &(491)
C FDA20_SP1.fgi( 183, 653):���
      L0_adil=(L_ovil.or.L0_adil).and..not.(L_ofor)
      L_(472)=.not.L0_adil
C FDA20_SP1.fgi( 523, 759):RS �������
      L_(471) = L0_adil.AND.L_ikil
C FDA20_SP1.fgi( 534, 760):�
      if(L_(471).and..not.L0_ubil) then
         R0_ibil=R0_obil
      else
         R0_ibil=max(R_(265)-deltat,0.0)
      endif
      L_(470)=R0_ibil.gt.0.0
      L0_ubil=L_(471)
C FDA20_SP1.fgi( 541, 760):������������  �� T
      L_ofil = L_(470).OR.L_(465)
C FDA20_SP1.fgi( 550, 759):���
      if(L0_adil.and..not.L0_ivel) then
         R0_avel=R0_evel
      else
         R0_avel=max(R_(261)-deltat,0.0)
      endif
      L_(473)=R0_avel.gt.0.0
      L0_ivel=L0_adil
C FDA20_SP1.fgi( 534, 780):������������  �� T
      L_(333) = L_amal.AND.L_ikil
C FDA20_SP1.fgi( 165, 156):�
      L_odik=(L_(333).or.L_odik).and..not.(L_imol)
      L_(334)=.not.L_odik
C FDA20_SP1.fgi( 181, 154):RS �������
      L_afup=L_odik
C FDA20_SP1.fgi( 197, 160):������,cyl18
      L_(523) = (.NOT.L_afup)
C FDA20_lamp.fgi(  60, 485):���
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ufup,L_ikup,R_ekup,
     & REAL(R_okup,4),L_(523),L_ofup,I_akup)
      !}
C FDA20_lamp.fgi(  74, 484):���������� ������� ���������,20FDA21AL004BQ43
      !{
      Call DAT_DISCR_HANDLER(deltat,I_adup,L_odup,R_idup,
     & REAL(R_udup,4),L_afup,L_ubup,I_edup)
      !}
C FDA20_lamp.fgi(  74, 496):���������� ������� ���������,20FDA21AL004BQ44
      L_emop=L_odik
C FDA20_SP1.fgi( 197, 156):������,cyl20
      L_(522) = (.NOT.L_emop)
C FDA20_lamp.fgi( 109, 455):���
      !{
      Call DAT_DISCR_HANDLER(deltat,I_apop,L_opop,R_ipop,
     & REAL(R_upop,4),L_(522),L_umop,I_epop)
      !}
C FDA20_lamp.fgi( 122, 454):���������� ������� ���������,20FDA21AL004BQ41
      !{
      Call DAT_DISCR_HANDLER(deltat,I_elop,L_ulop,R_olop,
     & REAL(R_amop,4),L_emop,L_alop,I_ilop)
      !}
C FDA20_lamp.fgi( 122, 466):���������� ������� ���������,20FDA21AL004BQ42
      L_ixek=L_oxek
C FDA20_SP1.fgi( 140, 190):������,move_to_y4
      if(L_oxek.and..not.L0_axek) then
         R0_ovek=R0_uvek
      else
         R0_ovek=max(R_(198)-deltat,0.0)
      endif
      L_exek=R0_ovek.gt.0.0
      L0_axek=L_oxek
C FDA20_SP1.fgi( 125, 194):������������  �� T
      L_(492) = L_isal.OR.L_upal.OR.L_opal
C FDA20_SP1.fgi( 173, 292):���
      L_exil = L_oxil.OR.L_axil.OR.L_uvil.OR.L_(492)
C FDA20_SP1.fgi( 183, 699):���
      L0_ifil=(L_exil.or.L0_ifil).and..not.(L_ubor)
      L_(476)=.not.L0_ifil
C FDA20_SP1.fgi( 523, 784):RS �������
      if(L0_ifil.and..not.L0_efil) then
         R0_udil=R0_afil
      else
         R0_udil=max(R_(267)-deltat,0.0)
      endif
      L_(475)=R0_udil.gt.0.0
      L0_efil=L0_ifil
C FDA20_SP1.fgi( 534, 786):������������  �� T
      L_ekil = L_(475).OR.L_(473)
C FDA20_SP1.fgi( 541, 785):���
      L_(474) = L0_ifil.AND.L_ikil
C FDA20_SP1.fgi( 534, 776):�
      if(L_(474).and..not.L0_odil) then
         R0_edil=R0_idil
      else
         R0_edil=max(R_(266)-deltat,0.0)
      endif
      L_akil=R0_edil.gt.0.0
      L0_odil=L_(474)
C FDA20_SP1.fgi( 541, 776):������������  �� T
      L_(336) = L_(365).OR.L_(337)
C FDA20_SP1.fgi( 465, 152):���
      if(L_(336)) then
         I_omuk=0
         L_emuk=.false.
      endif
C FDA20_SP1.fgi( 474, 144):����� ������� ���������,kt1sp
      if(L_ofok.and..not.L0_ifok) then
         R0_udok=R0_afok
      else
         R0_udok=max(R_(217)-deltat,0.0)
      endif
      L_efok=R0_udok.gt.0.0
      L0_ifok=L_ofok
C FDA20_SP1.fgi( 445, 294):������������  �� T
      if(L_ilok.and..not.L0_elok) then
         R0_okok=R0_ukok
      else
         R0_okok=max(R_(219)-deltat,0.0)
      endif
      L_alok=R0_okok.gt.0.0
      L0_elok=L_ilok
C FDA20_SP1.fgi( 445, 316):������������  �� T
      if(L_emok.and..not.L0_amok) then
         R0_olok=R0_ulok
      else
         R0_olok=max(R_(220)-deltat,0.0)
      endif
      L_(371)=R0_olok.gt.0.0
      L0_amok=L_emok
C FDA20_SP1.fgi( 445, 327):������������  �� T
      if(L_apok.and..not.L0_umok) then
         R0_imok=R0_omok
      else
         R0_imok=max(R_(221)-deltat,0.0)
      endif
      L_(373)=R0_imok.gt.0.0
      L0_umok=L_apok
C FDA20_SP1.fgi( 445, 338):������������  �� T
      L_ekuk = L_(396).OR.L_(373)
C FDA20_SP1.fgi( 489, 443):���
      if(L_ukik.and..not.L0_okik) then
         R0_ekik=R0_ikik
      else
         R0_ekik=max(R_(203)-deltat,0.0)
      endif
      L_(370)=R0_ekik.gt.0.0
      L0_okik=L_ukik
C FDA20_SP1.fgi( 445, 169):������������  �� T
      L_ikuk = L_(396).OR.L_(371).OR.L_(370)
C FDA20_SP1.fgi( 489, 448):���
      L_(529) = (.NOT.L_orir)
C FDA20_lamp.fgi( 196, 649):���
      !{
      Call DAT_DISCR_HANDLER(deltat,I_amir,L_omir,R_imir,
     & REAL(R_umir,4),L_(529),L_ulir,I_emir)
      !}
C FDA20_lamp.fgi( 210, 648):���������� ������� ���������,20FDA21AE701BQ60
      !{
      Call DAT_DISCR_HANDLER(deltat,I_opir,L_erir,R_arir,
     & REAL(R_irir,4),L_orir,L_ipir,I_upir)
      !}
C FDA20_lamp.fgi( 210, 660):���������� ������� ���������,20FDA21AE701BQ59
      if(L_osok.and..not.L0_isok) then
         R0_urok=R0_asok
      else
         R0_urok=max(R_(224)-deltat,0.0)
      endif
      L_esok=R0_urok.gt.0.0
      L0_isok=L_osok
C FDA20_SP1.fgi( 445, 371):������������  �� T
      if(L_otok.and..not.L0_itok) then
         R0_atok=R0_etok
      else
         R0_atok=max(R_(225)-deltat,0.0)
      endif
      L_(379)=R0_atok.gt.0.0
      L0_itok=L_otok
C FDA20_SP1.fgi( 445, 382):������������  �� T
      if(L_ovok.and..not.L0_ivok) then
         R0_avok=R0_evok
      else
         R0_avok=max(R_(226)-deltat,0.0)
      endif
      L_(381)=R0_avok.gt.0.0
      L0_ivok=L_ovok
C FDA20_SP1.fgi( 445, 393):������������  �� T
      L_(528) = (.NOT.L_ater)
C FDA20_lamp.fgi( 196, 590):���
      !{
      Call DAT_DISCR_HANDLER(deltat,I_iper,L_arer,R_uper,
     & REAL(R_erer,4),L_(528),L_eper,I_oper)
      !}
C FDA20_lamp.fgi( 210, 588):���������� ������� ���������,20FDA21AE701BQ64
      !{
      Call DAT_DISCR_HANDLER(deltat,I_aser,L_oser,R_iser,
     & REAL(R_user,4),L_ater,L_urer,I_eser)
      !}
C FDA20_lamp.fgi( 210, 602):���������� ������� ���������,20FDA21AE701BQ63
      if(L_irik.and..not.L0_erik) then
         R0_upik=R0_arik
      else
         R0_upik=max(R_(208)-deltat,0.0)
      endif
      L_(346)=R0_upik.gt.0.0
      L0_erik=L_irik
C FDA20_SP1.fgi( 445, 206):������������  �� T
      L_ufuk = L_(396).OR.L_(346)
C FDA20_SP1.fgi( 489, 433):���
      if(L_atik.and..not.L0_usik) then
         R0_isik=R0_osik
      else
         R0_isik=max(R_(210)-deltat,0.0)
      endif
      L_(350)=R0_isik.gt.0.0
      L0_usik=L_atik
C FDA20_SP1.fgi( 445, 228):������������  �� T
      L_usok = L_(379).OR.L_(350)
C FDA20_SP1.fgi( 489, 381):���
      if(L_utik.and..not.L0_otik) then
         R0_etik=R0_itik
      else
         R0_etik=max(R_(211)-deltat,0.0)
      endif
      L_(352)=R0_etik.gt.0.0
      L0_otik=L_utik
C FDA20_SP1.fgi( 445, 239):������������  �� T
      L_utok = L_(381).OR.L_(352)
C FDA20_SP1.fgi( 489, 392):���
      if(L_ovik.and..not.L0_ivik) then
         R0_avik=R0_evik
      else
         R0_avik=max(R_(212)-deltat,0.0)
      endif
      L_(354)=R0_avik.gt.0.0
      L0_ivik=L_ovik
C FDA20_SP1.fgi( 445, 250):������������  �� T
      L_efuk = L_(396).OR.L_(354)
C FDA20_SP1.fgi( 489, 418):���
      L_(527) = (.NOT.L_ovar)
C FDA20_lamp.fgi( 196, 561):���
      !{
      Call DAT_DISCR_HANDLER(deltat,I_asar,L_osar,R_isar,
     & REAL(R_usar,4),L_(527),L_urar,I_esar)
      !}
C FDA20_lamp.fgi( 210, 560):���������� ������� ���������,20FDA21AE701BQ71
      !{
      Call DAT_DISCR_HANDLER(deltat,I_otar,L_evar,R_avar,
     & REAL(R_ivar,4),L_ovar,L_itar,I_utar)
      !}
C FDA20_lamp.fgi( 210, 572):���������� ������� ���������,20FDA21AE701BQ69
      if(L_ebok.and..not.L0_abok) then
         R0_oxik=R0_uxik
      else
         R0_oxik=max(R_(214)-deltat,0.0)
      endif
      L_(360)=R0_oxik.gt.0.0
      L0_abok=L_ebok
C FDA20_SP1.fgi( 445, 272):������������  �� T
      L_afuk = L_(396).OR.L_(360)
C FDA20_SP1.fgi( 489, 414):���
      if(L_(321)) then
         R_opek=R_isek
      else
         R_opek=R_opek
      endif
C FDA20_SP1.fgi( 417, 788):���� RE IN LO CH7
C label 2030  try2030=try2030-1
      R_emav = R_opek
C FDA20_SP1.fgi( 438, 785):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_alav,R_upav,REAL(1,4)
     &,
     & REAL(R_olav,4),REAL(R_ulav,4),
     & REAL(R_ukav,4),REAL(R_okav,4),I_opav,
     & REAL(R_imav,4),L_omav,REAL(R_umav,4),L_apav,L_epav
     &,R_amav,
     & REAL(R_ilav,4),REAL(R_elav,4),L_ipav,REAL(R_emav,4
     &))
      !}
C FDA20_vlv.fgi( 469,  79):���������� �������,20FDA21CW001XQ01
      L_(296)=R_opek.lt.R_(182)
C FDA20_SP1.fgi( 362, 672):���������� <
      L_(300)=R_opek.lt.R_(183)
C FDA20_SP1.fgi( 148,  95):���������� <
      if(L_(318)) then
         R_ipek=R_esek
      else
         R_ipek=R_ipek
      endif
C FDA20_SP1.fgi( 417, 759):���� RE IN LO CH7
C label 2049  try2049=try2049-1
      R_osav = R_ipek
C FDA20_SP1.fgi( 438, 756):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_irav,R_evav,REAL(1,4)
     &,
     & REAL(R_asav,4),REAL(R_esav,4),
     & REAL(R_erav,4),REAL(R_arav,4),I_avav,
     & REAL(R_usav,4),L_atav,REAL(R_etav,4),L_itav,L_otav
     &,R_isav,
     & REAL(R_urav,4),REAL(R_orav,4),L_utav,REAL(R_osav,4
     &))
      !}
C FDA20_vlv.fgi( 440,  79):���������� �������,20FDA21CW003XQ01
      L_(299)=R_ipek.lt.R_(183)
C FDA20_SP1.fgi( 148,  91):���������� <
      L_(295)=R_ipek.lt.R_(182)
C FDA20_SP1.fgi( 362, 668):���������� <
      if(L_(315)) then
         R_upek=R_osek
      else
         R_upek=R_upek
      endif
C FDA20_SP1.fgi( 417, 730):���� RE IN LO CH7
C label 2068  try2068=try2068-1
      R_aves = R_upek
C FDA20_SP1.fgi( 438, 727):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_uses,R_oxes,REAL(1,4)
     &,
     & REAL(R_ites,4),REAL(R_otes,4),
     & REAL(R_oses,4),REAL(R_ises,4),I_ixes,
     & REAL(R_eves,4),L_ives,REAL(R_oves,4),L_uves,L_axes
     &,R_utes,
     & REAL(R_etes,4),REAL(R_ates,4),L_exes,REAL(R_aves,4
     &))
      !}
C FDA20_vlv.fgi( 441,  66):���������� �������,20FDA21CW005XQ01
      L_(294)=R_upek.lt.R_(182)
C FDA20_SP1.fgi( 362, 664):���������� <
      L_(298)=R_upek.lt.R_(183)
C FDA20_SP1.fgi( 148,  87):���������� <
      if(L_(312)) then
         R_arek=R_usek
      else
         R_arek=R_arek
      endif
C FDA20_SP1.fgi( 417, 701):���� RE IN LO CH7
C label 2087  try2087=try2087-1
      R_opes = R_arek
C FDA20_SP1.fgi( 438, 698):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_imes,R_eses,REAL(1,4)
     &,
     & REAL(R_apes,4),REAL(R_epes,4),
     & REAL(R_emes,4),REAL(R_ames,4),I_ases,
     & REAL(R_upes,4),L_ares,REAL(R_eres,4),L_ires,L_ores
     &,R_ipes,
     & REAL(R_umes,4),REAL(R_omes,4),L_ures,REAL(R_opes,4
     &))
      !}
C FDA20_vlv.fgi( 469,  66):���������� �������,20FDA21CW007XQ01
      L_(293)=R_arek.lt.R_(182)
C FDA20_SP1.fgi( 362, 660):���������� <
      L_ifak = L_(296).AND.L_(295).AND.L_(294).AND.L_(293
     &)
C FDA20_SP1.fgi( 376, 669):�
      L_(297)=R_arek.lt.R_(183)
C FDA20_SP1.fgi( 148,  83):���������� <
      L_ilak = L_(300).OR.L_(299).OR.L_(298).OR.L_(297).OR.
     &(.NOT.L_arel).OR.(.NOT.L_akel).OR.(.NOT.L_ifel).OR.
     &(.NOT.L_uxek)
C FDA20_SP1.fgi( 180,  88):���
      L_(292) = L_(290).AND.(.NOT.L_ilak)
C FDA20_SP2_UVR.fgi(  66, 825):�
      if (.not.L_aruf.and.(L_eruf.or.L_(292))) then
          I_iruf = 0
          L_aruf = .true.
          L_(291) = .true.
      endif
      if (I_iruf .gt. 0) then
         L_(291) = .false.
      endif
      if(L_oruf)then
          I_iruf = 0
          L_aruf = .false.
          L_(291) = .false.
      endif
C FDA20_SP2_UVR.fgi(  93, 818):���������� ������� ���������,uvr
      !{Call KN_VLVR(deltat,REAL(R_ikife,4),L_upife,L_arife
C ,R8_idife,
      Call SOLENOIDVALVE_MAN(deltat,REAL(R_ikife,4),L_upife
     &,L_arife,R8_idife,C30_ufife,
     & L_avike,L_axike,L_aboke,I_umife,I_apife,R_ixefe,
     & REAL(R_uxefe,4),R_obife,REAL(R_adife,4),
     & R_abife,REAL(R_ibife,4),I_amife,I_epife,I_omife,I_imife
     &,
     & L_ubife,L_ebife,L_efife,L_afife,L_erife,
     & L_oxefe,L_ofife,L_isife,L_udife,L_odife,L_esife,L_osife
     &,
     & L_edife,L_ipife,L_urife,L_opife,L_akife,L_ekife,
     & REAL(R8_iseke,8),REAL(1.0,4),R8_ebike,L_asife,R_ulife
     &,
     & REAL(R_ifife,4),L_okife,L_ukife,L_alife,L_ilife,L_olife
     &,L_elife)
      !}

      if(L_olife.or.L_ilife.or.L_elife.or.L_alife.or.L_ukife.or.L_okife
     &) then      
                  I_emife = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_emife = z'40000000'
      endif
C FDA20_vlv.fgi( 140, 176):���� ���������� ���������������� ��������,20FDA23AB002
C label 2118  try2118=try2118-1
      if(.not.L_ek) then
         R0_uf=0.0
      elseif(.not.L0_ak) then
         R0_uf=R0_of
      else
         R0_uf=max(R_(4)-deltat,0.0)
      endif
      L_(5)=L_ek.and.R0_uf.le.0.0
      L0_ak=L_ek
C FDA20_SP2_UVR.fgi( 370,  51):�������� ��������� ������
      L_(7) = L_upife.AND.L_(5)
C FDA20_SP2_UVR.fgi( 323,  57):�
      if(.not.L_el) then
         R0_ok=0.0
      elseif(.not.L0_uk) then
         R0_ok=R0_ik
      else
         R0_ok=max(R_(5)-deltat,0.0)
      endif
      L_(9)=L_el.and.R0_ok.le.0.0
      L0_uk=L_el
C FDA20_SP2_UVR.fgi( 370,  68):�������� ��������� ������
      if(.not.L_uti) then
         R0_usi=0.0
      elseif(.not.L0_ati) then
         R0_usi=R0_osi
      else
         R0_usi=max(R_(47)-deltat,0.0)
      endif
      L_(52)=L_uti.and.R0_usi.le.0.0
      L0_ati=L_uti
C FDA20_SP2_UVR.fgi( 370, 182):�������� ��������� ������
      L_(55) = L_oro.AND.L_(52)
C FDA20_SP2_UVR.fgi( 308, 188):�
      if(.not.L_afi) then
         R0_odi=0.0
      elseif(.not.L0_udi) then
         R0_odi=R0_idi
      else
         R0_odi=max(R_(38)-deltat,0.0)
      endif
      L_(37)=L_afi.and.R0_odi.le.0.0
      L0_udi=L_afi
C FDA20_SP2_UVR.fgi( 370,  20):�������� ��������� ������
      L_evu=L_afi
C FDA20_SP2_UVR.fgi( 416,  26):������,tilter_complete
      if(.not.L_ibad) then
         R0_uvu=0.0
      elseif(.not.L0_axu) then
         R0_uvu=R0_ovu
      else
         R0_uvu=max(R_(72)-deltat,0.0)
      endif
      L_(100)=L_ibad.and.R0_uvu.le.0.0
      L0_axu=L_ibad
C FDA20_SP2_UVR.fgi(  44, 197):�������� ��������� ������
      L_(103) = L_axif.AND.L_(100)
C FDA20_SP2_UVR.fgi(  57, 201):�
      if(.not.L_apad) then
         R0_efad=0.0
      elseif(.not.L0_ifad) then
         R0_efad=R0_afad
      else
         R0_efad=max(R_(77)-deltat,0.0)
      endif
      L_(106)=L_apad.and.R0_efad.le.0.0
      L0_ifad=L_apad
C FDA20_SP2_UVR.fgi( 138, 227):�������� ��������� ������
      L_(110) = L_ulod.AND.L_(106)
C FDA20_SP2_UVR.fgi(  59, 235):�
      if(.not.L_orad) then
         R0_ipad=0.0
      elseif(.not.L0_opad) then
         R0_ipad=R0_epad
      else
         R0_ipad=max(R_(81)-deltat,0.0)
      endif
      L_(111)=L_orad.and.R0_ipad.le.0.0
      L0_opad=L_orad
C FDA20_SP2_UVR.fgi(  44, 282):�������� ��������� ������
      L_(113) = L_efof.AND.L_idof.AND.L_obof.AND.L_uxif.AND.L_axif.AND.L
     &_evif.AND.L_itif.AND.L_osif.AND.L_urif.AND.L_irad.AND.L_
     &(111)
C FDA20_SP2_UVR.fgi(  57, 295):�
      if(.not.L_ifid) then
         R0_afid=0.0
      elseif(.not.L0_efid) then
         R0_afid=R0_udid
      else
         R0_afid=max(R_(98)-deltat,0.0)
      endif
      L_(154)=L_ifid.and.R0_afid.le.0.0
      L0_efid=L_ifid
C FDA20_SP2_UVR.fgi(  69, 369):�������� ��������� ������
      if(.not.L_ebod) then
         R0_ited=0.0
      elseif(.not.L0_oted) then
         R0_ited=R0_eted
      else
         R0_ited=max(R_(97)-deltat,0.0)
      endif
      L_(192)=L_ebod.and.R0_ited.le.0.0
      L0_oted=L_ebod
C FDA20_SP2_UVR.fgi(  69, 523):�������� ��������� ������
      if(.not.L_akof) then
         R0_ole=0.0
      elseif(.not.L0_ule) then
         R0_ole=R0_ile
      else
         R0_ole=max(R_(26)-deltat,0.0)
      endif
      L_(25)=L_akof.and.R0_ole.le.0.0
      L0_ule=L_akof
C FDA20_SP2_UVR.fgi(  45, 653):�������� ��������� ������
      L_(262) = L_irad.AND.L_(25)
C FDA20_SP2_UVR.fgi(  57, 659):�
      Call FDA20_1(ext_deltat)
      End
      Subroutine FDA20_1(ext_deltat)
      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      Include 'FDA20.fh'

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      if(.not.L_emuf) then
         R0_exe=0.0
      elseif(.not.L0_ixe) then
         R0_exe=R0_axe
      else
         R0_exe=max(R_(37)-deltat,0.0)
      endif
      L_(35)=L_emuf.and.R0_exe.le.0.0
      L0_ixe=L_emuf
C FDA20_SP2_UVR.fgi(  45, 769):�������� ��������� ������
      L_(289) = L_edi.AND.L_adi.AND.L_ubi.AND.L_obi.AND.L_ibi.AND.L_ebi.
     &AND.L_abi.AND.L_uxe.AND.L_oxe.AND.L_usu.AND.L_efof.AND.L_idof.AND.
     &L_obof.AND.L_uxif.AND.L_axif.AND.L_evif.AND.L_itif.AND.L_osif.AND.
     &L_urif.AND.L_irad.AND.L_(35)
C FDA20_SP2_UVR.fgi(  58, 792):�
      if(L_aruf) then
          if (L_(291)) then
              I_iruf = 1
              L_emuf = .true.
              L_(288) = .false.
          endif
          if (L_(289)) then
              L_(288) = .true.
              L_emuf = .false.
          endif
          if (I_iruf.ne.1) then
              L_emuf = .false.
              L_(288) = .false.
          endif
      else
          L_(288) = .false.
      endif
C FDA20_SP2_UVR.fgi(  93, 804):��� ������� ���������,uvr
C sav1=L_(35)
      if(.not.L_emuf) then
         R0_exe=0.0
      elseif(.not.L0_ixe) then
         R0_exe=R0_axe
      else
         R0_exe=max(R_(37)-deltat,0.0)
      endif
      L_(35)=L_emuf.and.R0_exe.le.0.0
      L0_ixe=L_emuf
C FDA20_SP2_UVR.fgi(  45, 769):recalc:�������� ��������� ������
C if(sav1.ne.L_(35) .and. try2163.gt.0) goto 2163
      if(L_aruf) then
          if (L_(288)) then
              I_iruf = 2
              L_amuf = .true.
              L_(286) = .false.
          endif
          if (L_(287)) then
              L_(286) = .true.
              L_amuf = .false.
          endif
          if (I_iruf.ne.2) then
              L_amuf = .false.
              L_(286) = .false.
          endif
      else
          L_(286) = .false.
      endif
C FDA20_SP2_UVR.fgi(  93, 740):��� ������� ���������,uvr
      if(L_aruf) then
          if (L_(286)) then
              I_iruf = 3
              L_ikof = .true.
              L_(282) = .false.
          endif
          if (L_ekof) then
              L_(282) = .true.
              L_ikof = .false.
          endif
          if (I_iruf.ne.3) then
              L_ikof = .false.
              L_(282) = .false.
          endif
      else
          L_(282) = .false.
      endif
C FDA20_SP2_UVR.fgi(  93, 717):��� ������� ���������,uvr
      if(L_aruf) then
          if (L_(282)) then
              I_iruf = 4
              L_arif = .true.
              L_(260) = .false.
          endif
          if (L_(261)) then
              L_(260) = .true.
              L_arif = .false.
          endif
          if (I_iruf.ne.4) then
              L_arif = .false.
              L_(260) = .false.
          endif
      else
          L_(260) = .false.
      endif
C FDA20_SP2_UVR.fgi(  93, 672):��� ������� ���������,uvr
      if(L_aruf) then
          if (L_(260)) then
              I_iruf = 5
              L_akof = .true.
              L_(259) = .false.
          endif
          if (L_(262)) then
              L_(259) = .true.
              L_akof = .false.
          endif
          if (I_iruf.ne.5) then
              L_akof = .false.
              L_(259) = .false.
          endif
      else
          L_(259) = .false.
      endif
C FDA20_SP2_UVR.fgi(  93, 659):��� ������� ���������,uvr
C sav1=L_(25)
      if(.not.L_akof) then
         R0_ole=0.0
      elseif(.not.L0_ule) then
         R0_ole=R0_ile
      else
         R0_ole=max(R_(26)-deltat,0.0)
      endif
      L_(25)=L_akof.and.R0_ole.le.0.0
      L0_ule=L_akof
C FDA20_SP2_UVR.fgi(  45, 653):recalc:�������� ��������� ������
C if(sav1.ne.L_(25) .and. try2159.gt.0) goto 2159
      if(L_aruf) then
          if (L_(259)) then
              I_iruf = 6
              L_ulif = .true.
              L_(257) = .false.
          endif
          if (L_(258)) then
              L_(257) = .true.
              L_ulif = .false.
          endif
          if (I_iruf.ne.6) then
              L_ulif = .false.
              L_(257) = .false.
          endif
      else
          L_(257) = .false.
      endif
C FDA20_SP2_UVR.fgi(  93, 616):��� ������� ���������,uvr
      if(L_aruf) then
          if (L_(257)) then
              I_iruf = 7
              L_uvef = .true.
              L_(251) = .false.
          endif
          if (L_ovef) then
              L_(251) = .true.
              L_uvef = .false.
          endif
          if (I_iruf.ne.7) then
              L_uvef = .false.
              L_(251) = .false.
          endif
      else
          L_(251) = .false.
      endif
C FDA20_SP2_UVR.fgi(  93, 600):��� ������� ���������,uvr
      if(L_aruf) then
          if (L_(251)) then
              I_iruf = 8
              L_obud = .true.
              L_(238) = .false.
          endif
          if (L_ibud) then
              L_(238) = .true.
              L_obud = .false.
          endif
          if (I_iruf.ne.8) then
              L_obud = .false.
              L_(238) = .false.
          endif
      else
          L_(238) = .false.
      endif
C FDA20_SP2_UVR.fgi(  93, 588):��� ������� ���������,uvr
      if(L_aruf) then
          if (L_(238)) then
              I_iruf = 9
              L_ixod = .true.
              L_(236) = .false.
          endif
          if (L_exod) then
              L_(236) = .true.
              L_ixod = .false.
          endif
          if (I_iruf.ne.9) then
              L_ixod = .false.
              L_(236) = .false.
          endif
      else
          L_(236) = .false.
      endif
C FDA20_SP2_UVR.fgi(  93, 576):��� ������� ���������,uvr
      if(L_aruf) then
          if (L_(236)) then
              I_iruf = 10
              L_evod = .true.
              L_(234) = .false.
          endif
          if (L_avod) then
              L_(234) = .true.
              L_evod = .false.
          endif
          if (I_iruf.ne.10) then
              L_evod = .false.
              L_(234) = .false.
          endif
      else
          L_(234) = .false.
      endif
C FDA20_SP2_UVR.fgi(  93, 564):��� ������� ���������,uvr
      if(L_aruf) then
          if (L_(234)) then
              I_iruf = 11
              L_amod = .true.
              L_(198) = .false.
          endif
          if (L_ekof) then
              L_(198) = .true.
              L_amod = .false.
          endif
          if (I_iruf.ne.11) then
              L_amod = .false.
              L_(198) = .false.
          endif
      else
          L_(198) = .false.
      endif
C FDA20_SP2_UVR.fgi(  93, 551):��� ������� ���������,uvr
      if(L_aruf) then
          if (L_(198)) then
              I_iruf = 12
              L_ebod = .true.
              L_(191) = .false.
          endif
          if (L_(192)) then
              L_(191) = .true.
              L_ebod = .false.
          endif
          if (I_iruf.ne.12) then
              L_ebod = .false.
              L_(191) = .false.
          endif
      else
          L_(191) = .false.
      endif
C FDA20_SP2_UVR.fgi(  93, 523):��� ������� ���������,uvr
C sav1=L_(192)
      if(.not.L_ebod) then
         R0_ited=0.0
      elseif(.not.L0_oted) then
         R0_ited=R0_eted
      else
         R0_ited=max(R_(97)-deltat,0.0)
      endif
      L_(192)=L_ebod.and.R0_ited.le.0.0
      L0_oted=L_ebod
C FDA20_SP2_UVR.fgi(  69, 523):recalc:�������� ��������� ������
C if(sav1.ne.L_(192) .and. try2157.gt.0) goto 2157
      if(L_aruf) then
          if (L_(191)) then
              I_iruf = 13
              L_ixid = .true.
              L_(186) = .false.
          endif
          if (L_(187)) then
              L_(186) = .true.
              L_ixid = .false.
          endif
          if (I_iruf.ne.13) then
              L_ixid = .false.
              L_(186) = .false.
          endif
      else
          L_(186) = .false.
      endif
C FDA20_SP2_UVR.fgi(  93, 499):��� ������� ���������,uvr
      if(L_aruf) then
          if (L_(186)) then
              I_iruf = 14
              L_etid = .true.
              L_(177) = .false.
          endif
          if (L_(178)) then
              L_(177) = .true.
              L_etid = .false.
          endif
          if (I_iruf.ne.14) then
              L_etid = .false.
              L_(177) = .false.
          endif
      else
          L_(177) = .false.
      endif
C FDA20_SP2_UVR.fgi(  93, 467):��� ������� ���������,uvr
      if(L_aruf) then
          if (L_(177)) then
              I_iruf = 15
              L_arid = .true.
              L_(169) = .false.
          endif
          if (L_(170)) then
              L_(169) = .true.
              L_arid = .false.
          endif
          if (I_iruf.ne.15) then
              L_arid = .false.
              L_(169) = .false.
          endif
      else
          L_(169) = .false.
      endif
C FDA20_SP2_UVR.fgi(  93, 436):��� ������� ���������,uvr
      if(L_aruf) then
          if (L_(169)) then
              I_iruf = 16
              L_ulid = .true.
              L_(161) = .false.
          endif
          if (L_(162)) then
              L_(161) = .true.
              L_ulid = .false.
          endif
          if (I_iruf.ne.16) then
              L_ulid = .false.
              L_(161) = .false.
          endif
      else
          L_(161) = .false.
      endif
C FDA20_SP2_UVR.fgi(  93, 405):��� ������� ���������,uvr
      if(L_aruf) then
          if (L_(161)) then
              I_iruf = 17
              L_ifid = .true.
              L_(153) = .false.
          endif
          if (L_(154)) then
              L_(153) = .true.
              L_ifid = .false.
          endif
          if (I_iruf.ne.17) then
              L_ifid = .false.
              L_(153) = .false.
          endif
      else
          L_(153) = .false.
      endif
C FDA20_SP2_UVR.fgi(  93, 369):��� ������� ���������,uvr
C sav1=L_(154)
      if(.not.L_ifid) then
         R0_afid=0.0
      elseif(.not.L0_efid) then
         R0_afid=R0_udid
      else
         R0_afid=max(R_(98)-deltat,0.0)
      endif
      L_(154)=L_ifid.and.R0_afid.le.0.0
      L0_efid=L_ifid
C FDA20_SP2_UVR.fgi(  69, 369):recalc:�������� ��������� ������
C if(sav1.ne.L_(154) .and. try2155.gt.0) goto 2155
      if(L_aruf) then
          if (L_(153)) then
              I_iruf = 18
              L_umed = .true.
              L_(133) = .false.
          endif
          if (L_(134)) then
              L_(133) = .true.
              L_umed = .false.
          endif
          if (I_iruf.ne.18) then
              L_umed = .false.
              L_(133) = .false.
          endif
      else
          L_(133) = .false.
      endif
C FDA20_SP2_UVR.fgi(  93, 358):��� ������� ���������,uvr
      if(L_aruf) then
          if (L_(133)) then
              I_iruf = 19
              L_orad = .true.
              L_(112) = .false.
          endif
          if (L_(113)) then
              L_(112) = .true.
              L_orad = .false.
          endif
          if (I_iruf.ne.19) then
              L_orad = .false.
              L_(112) = .false.
          endif
      else
          L_(112) = .false.
      endif
C FDA20_SP2_UVR.fgi(  93, 295):��� ������� ���������,uvr
C sav1=L_(111)
      if(.not.L_orad) then
         R0_ipad=0.0
      elseif(.not.L0_opad) then
         R0_ipad=R0_epad
      else
         R0_ipad=max(R_(81)-deltat,0.0)
      endif
      L_(111)=L_orad.and.R0_ipad.le.0.0
      L0_opad=L_orad
C FDA20_SP2_UVR.fgi(  44, 282):recalc:�������� ��������� ������
C if(sav1.ne.L_(111) .and. try2151.gt.0) goto 2151
      if(L_aruf) then
          if (L_(112)) then
              I_iruf = 20
              L_ele = .true.
              L_(108) = .false.
          endif
          if (L_(24)) then
              L_(108) = .true.
              L_ele = .false.
          endif
          if (I_iruf.ne.20) then
              L_ele = .false.
              L_(108) = .false.
          endif
      else
          L_(108) = .false.
      endif
C FDA20_SP2_UVR.fgi(  93, 268):��� ������� ���������,uvr
      if(L_aruf) then
          if (L_(108)) then
              I_iruf = 21
              L_apad = .true.
              L_(109) = .false.
          endif
          if (L_(110)) then
              L_(109) = .true.
              L_apad = .false.
          endif
          if (I_iruf.ne.21) then
              L_apad = .false.
              L_(109) = .false.
          endif
      else
          L_(109) = .false.
      endif
C FDA20_SP2_UVR.fgi(  93, 235):��� ������� ���������,uvr
C sav1=L_(106)
      if(.not.L_apad) then
         R0_efad=0.0
      elseif(.not.L0_ifad) then
         R0_efad=R0_afad
      else
         R0_efad=max(R_(77)-deltat,0.0)
      endif
      L_(106)=L_apad.and.R0_efad.le.0.0
      L0_ifad=L_apad
C FDA20_SP2_UVR.fgi( 138, 227):recalc:�������� ��������� ������
C if(sav1.ne.L_(106) .and. try2145.gt.0) goto 2145
      if(L_aruf) then
          if (L_(109)) then
              I_iruf = 22
              L_ibad = .true.
              L_(102) = .false.
          endif
          if (L_(103)) then
              L_(102) = .true.
              L_ibad = .false.
          endif
          if (I_iruf.ne.22) then
              L_ibad = .false.
              L_(102) = .false.
          endif
      else
          L_(102) = .false.
      endif
C FDA20_SP2_UVR.fgi(  93, 201):��� ������� ���������,uvr
C sav1=L_(100)
      if(.not.L_ibad) then
         R0_uvu=0.0
      elseif(.not.L0_axu) then
         R0_uvu=R0_ovu
      else
         R0_uvu=max(R_(72)-deltat,0.0)
      endif
      L_(100)=L_ibad.and.R0_uvu.le.0.0
      L0_axu=L_ibad
C FDA20_SP2_UVR.fgi(  44, 197):recalc:�������� ��������� ������
C if(sav1.ne.L_(100) .and. try2141.gt.0) goto 2141
      if(L_aruf) then
          if (L_(102)) then
              I_iruf = 23
              L_idu = .true.
              L_(98) = .false.
          endif
          if (L_apif) then
              L_(98) = .true.
              L_idu = .false.
          endif
          if (I_iruf.ne.23) then
              L_idu = .false.
              L_(98) = .false.
          endif
      else
          L_(98) = .false.
      endif
C FDA20_SP2_UVR.fgi(  93, 184):��� ������� ���������,uvr
      if(L_aruf) then
          if (L_(98)) then
              I_iruf = 24
              L_ivu = .true.
              L_(99) = .false.
          endif
          if (L_evu) then
              L_(99) = .true.
              L_ivu = .false.
          endif
          if (I_iruf.ne.24) then
              L_ivu = .false.
              L_(99) = .false.
          endif
      else
          L_(99) = .false.
      endif
C FDA20_SP2_UVR.fgi(  93, 165):��� ������� ���������,uvr
      if(L_ivu.and..not.L0_otu) then
         R0_etu=R0_itu
      else
         R0_etu=max(R_(71)-deltat,0.0)
      endif
      L_avu=R0_etu.gt.0.0
      L0_otu=L_ivu
C FDA20_SP2_UVR.fgi( 122, 165):������������  �� T
      if(L_avu.and..not.L0_oxo) then
         R0_exo=R0_ixo
      else
         R0_exo=max(R_(60)-deltat,0.0)
      endif
      L_(79)=R0_exo.gt.0.0
      L0_oxo=L_avu
C FDA20_SP2_UVR.fgi( 319, 320):������������  �� T
      if (.not.L_abu.and.(L_ebu.or.L_(79))) then
          I_ibu = 0
          L_abu = .true.
          L_(78) = .true.
      endif
      if (I_ibu .gt. 0) then
         L_(78) = .false.
      endif
      if(L_obu)then
          I_ibu = 0
          L_abu = .false.
          L_(78) = .false.
      endif
C FDA20_SP2_UVR.fgi( 342, 312):���������� ������� ���������,uvrkant
      if(L_abu) then
          if (L_(78)) then
              I_ibu = 1
              L_ivo = .true.
              L_(77) = .false.
          endif
          if (L_evo) then
              L_(77) = .true.
              L_ivo = .false.
          endif
          if (I_ibu.ne.1) then
              L_ivo = .false.
              L_(77) = .false.
          endif
      else
          L_(77) = .false.
      endif
C FDA20_SP2_UVR.fgi( 342, 298):��� ������� ���������,uvrkant
      if(L_abu) then
          if (L_(77)) then
              I_ibu = 2
              L_omo = .true.
              L_(75) = .false.
          endif
          if (L_opo) then
              L_(75) = .true.
              L_omo = .false.
          endif
          if (I_ibu.ne.2) then
              L_omo = .false.
              L_(75) = .false.
          endif
      else
          L_(75) = .false.
      endif
C FDA20_SP2_UVR.fgi( 342, 287):��� ������� ���������,uvrkant
      if(L_abu) then
          if (L_(75)) then
              I_ibu = 3
              L_ulo = .true.
              L_(73) = .false.
          endif
          if (L_olo) then
              L_(73) = .true.
              L_ulo = .false.
          endif
          if (I_ibu.ne.3) then
              L_ulo = .false.
              L_(73) = .false.
          endif
      else
          L_(73) = .false.
      endif
C FDA20_SP2_UVR.fgi( 342, 276):��� ������� ���������,uvrkant
      if(L_abu) then
          if (L_(73)) then
              I_ibu = 4
              L_oko = .true.
              L_(71) = .false.
          endif
          if (L_uro) then
              L_(71) = .true.
              L_oko = .false.
          endif
          if (I_ibu.ne.4) then
              L_oko = .false.
              L_(71) = .false.
          endif
      else
          L_(71) = .false.
      endif
C FDA20_SP2_UVR.fgi( 342, 265):��� ������� ���������,uvrkant
      if(L_abu) then
          if (L_(71)) then
              I_ibu = 5
              L_ufo = .true.
              L_(69) = .false.
          endif
          if (L_uko) then
              L_(69) = .true.
              L_ufo = .false.
          endif
          if (I_ibu.ne.5) then
              L_ufo = .false.
              L_(69) = .false.
          endif
      else
          L_(69) = .false.
      endif
C FDA20_SP2_UVR.fgi( 342, 254):��� ������� ���������,uvrkant
      if(L_abu) then
          if (L_(69)) then
              I_ibu = 6
              L_afo = .true.
              L_(67) = .false.
          endif
          if (L_ipo) then
              L_(67) = .true.
              L_afo = .false.
          endif
          if (I_ibu.ne.6) then
              L_afo = .false.
              L_(67) = .false.
          endif
      else
          L_(67) = .false.
      endif
C FDA20_SP2_UVR.fgi( 342, 243):��� ������� ���������,uvrkant
      if(L_abu) then
          if (L_(67)) then
              I_ibu = 7
              L_edo = .true.
              L_(63) = .false.
          endif
          if (L_olo) then
              L_(63) = .true.
              L_edo = .false.
          endif
          if (I_ibu.ne.7) then
              L_edo = .false.
              L_(63) = .false.
          endif
      else
          L_(63) = .false.
      endif
C FDA20_SP2_UVR.fgi( 342, 232):��� ������� ���������,uvrkant
      if(L_abu) then
          if (L_(63)) then
              I_ibu = 8
              L_ibo = .true.
              L_(61) = .false.
          endif
          if (L_ebo) then
              L_(61) = .true.
              L_ibo = .false.
          endif
          if (I_ibu.ne.8) then
              L_ibo = .false.
              L_(61) = .false.
          endif
      else
          L_(61) = .false.
      endif
C FDA20_SP2_UVR.fgi( 342, 221):��� ������� ���������,uvrkant
      if(L_abu) then
          if (L_(61)) then
              I_ibu = 9
              L_ixi = .true.
              L_(57) = .false.
          endif
          if (L_uko) then
              L_(57) = .true.
              L_ixi = .false.
          endif
          if (I_ibu.ne.9) then
              L_ixi = .false.
              L_(57) = .false.
          endif
      else
          L_(57) = .false.
      endif
C FDA20_SP2_UVR.fgi( 342, 210):��� ������� ���������,uvrkant
      if(L_abu) then
          if (L_(57)) then
              I_ibu = 10
              L_ovi = .true.
              L_(56) = .false.
          endif
          if (L_iso) then
              L_(56) = .true.
              L_ovi = .false.
          endif
          if (I_ibu.ne.10) then
              L_ovi = .false.
              L_(56) = .false.
          endif
      else
          L_(56) = .false.
      endif
C FDA20_SP2_UVR.fgi( 342, 199):��� ������� ���������,uvrkant
      if(L_abu) then
          if (L_(56)) then
              I_ibu = 11
              L_uti = .true.
              L_(54) = .false.
          endif
          if (L_(55)) then
              L_(54) = .true.
              L_uti = .false.
          endif
          if (I_ibu.ne.11) then
              L_uti = .false.
              L_(54) = .false.
          endif
      else
          L_(54) = .false.
      endif
C FDA20_SP2_UVR.fgi( 342, 188):��� ������� ���������,uvrkant
C sav1=L_(52)
      if(.not.L_uti) then
         R0_usi=0.0
      elseif(.not.L0_ati) then
         R0_usi=R0_osi
      else
         R0_usi=max(R_(47)-deltat,0.0)
      endif
      L_(52)=L_uti.and.R0_usi.le.0.0
      L0_ati=L_uti
C FDA20_SP2_UVR.fgi( 370, 182):recalc:�������� ��������� ������
C if(sav1.ne.L_(52) .and. try2132.gt.0) goto 2132
      if(L_abu) then
          if (L_(54)) then
              I_ibu = 12
              L_isi = .true.
              L_(51) = .false.
          endif
          if (L_iro) then
              L_(51) = .true.
              L_isi = .false.
          endif
          if (I_ibu.ne.12) then
              L_isi = .false.
              L_(51) = .false.
          endif
      else
          L_(51) = .false.
      endif
C FDA20_SP2_UVR.fgi( 342, 173):��� ������� ���������,uvrkant
      if(L_abu) then
          if (L_(51)) then
              I_ibu = 13
              L_ori = .true.
              L_(49) = .false.
          endif
          if (L_eso) then
              L_(49) = .true.
              L_ori = .false.
          endif
          if (I_ibu.ne.13) then
              L_ori = .false.
              L_(49) = .false.
          endif
      else
          L_(49) = .false.
      endif
C FDA20_SP2_UVR.fgi( 342, 162):��� ������� ���������,uvrkant
      if(L_abu) then
          if (L_(49)) then
              I_ibu = 14
              L_upi = .true.
              L_(48) = .false.
          endif
          if (L_olo) then
              L_(48) = .true.
              L_upi = .false.
          endif
          if (I_ibu.ne.14) then
              L_upi = .false.
              L_(48) = .false.
          endif
      else
          L_(48) = .false.
      endif
C FDA20_SP2_UVR.fgi( 342, 151):��� ������� ���������,uvrkant
      if(L_abu) then
          if (L_(48)) then
              I_ibu = 15
              L_api = .true.
              L_(46) = .false.
          endif
          if (L_ero) then
              L_(46) = .true.
              L_api = .false.
          endif
          if (I_ibu.ne.15) then
              L_api = .false.
              L_(46) = .false.
          endif
      else
          L_(46) = .false.
      endif
C FDA20_SP2_UVR.fgi( 342, 140):��� ������� ���������,uvrkant
      if(L_abu) then
          if (L_(46)) then
              I_ibu = 16
              L_emi = .true.
              L_(45) = .false.
          endif
          if (L_uko) then
              L_(45) = .true.
              L_emi = .false.
          endif
          if (I_ibu.ne.16) then
              L_emi = .false.
              L_(45) = .false.
          endif
      else
          L_(45) = .false.
      endif
C FDA20_SP2_UVR.fgi( 342, 129):��� ������� ���������,uvrkant
      if(L_abu) then
          if (L_(45)) then
              I_ibu = 17
              L_ili = .true.
              L_(44) = .false.
          endif
          if (L_opo) then
              L_(44) = .true.
              L_ili = .false.
          endif
          if (I_ibu.ne.17) then
              L_ili = .false.
              L_(44) = .false.
          endif
      else
          L_(44) = .false.
      endif
C FDA20_SP2_UVR.fgi( 342, 118):��� ������� ���������,uvrkant
      if(L_abu) then
          if (L_(44)) then
              I_ibu = 18
              L_um = .true.
              L_(41) = .false.
          endif
          if (L_olo) then
              L_(41) = .true.
              L_um = .false.
          endif
          if (I_ibu.ne.18) then
              L_um = .false.
              L_(41) = .false.
          endif
      else
          L_(41) = .false.
      endif
C FDA20_SP2_UVR.fgi( 342, 107):��� ������� ���������,uvrkant
      if(L_abu) then
          if (L_(41)) then
              I_ibu = 19
              L_oki = .true.
              L_(42) = .false.
          endif
          if (L_aso) then
              L_(42) = .true.
              L_oki = .false.
          endif
          if (I_ibu.ne.19) then
              L_oki = .false.
              L_(42) = .false.
          endif
      else
          L_(42) = .false.
      endif
C FDA20_SP2_UVR.fgi( 342,  96):��� ������� ���������,uvrkant
      if(L_abu) then
          if (L_(42)) then
              I_ibu = 20
              L_ufi = .true.
              L_(39) = .false.
          endif
          if (L_evo) then
              L_(39) = .true.
              L_ufi = .false.
          endif
          if (I_ibu.ne.20) then
              L_ufi = .false.
              L_(39) = .false.
          endif
      else
          L_(39) = .false.
      endif
C FDA20_SP2_UVR.fgi( 342,  85):��� ������� ���������,uvrkant
      if(L_abu) then
          if (L_(39)) then
              I_ibu = 21
              L_el = .true.
              L_(8) = .false.
          endif
          if (L_(9)) then
              L_(8) = .true.
              L_el = .false.
          endif
          if (I_ibu.ne.21) then
              L_el = .false.
              L_(8) = .false.
          endif
      else
          L_(8) = .false.
      endif
C FDA20_SP2_UVR.fgi( 342,  74):��� ������� ���������,uvrkant
C sav1=L_(9)
      if(.not.L_el) then
         R0_ok=0.0
      elseif(.not.L0_uk) then
         R0_ok=R0_ik
      else
         R0_ok=max(R_(5)-deltat,0.0)
      endif
      L_(9)=L_el.and.R0_ok.le.0.0
      L0_uk=L_el
C FDA20_SP2_UVR.fgi( 370,  68):recalc:�������� ��������� ������
C if(sav1.ne.L_(9) .and. try2130.gt.0) goto 2130
      if(L_abu) then
          if (L_(8)) then
              I_ibu = 22
              L_ek = .true.
              L_(6) = .false.
          endif
          if (L_(7)) then
              L_(6) = .true.
              L_ek = .false.
          endif
          if (I_ibu.ne.22) then
              L_ek = .false.
              L_(6) = .false.
          endif
      else
          L_(6) = .false.
      endif
C FDA20_SP2_UVR.fgi( 342,  57):��� ������� ���������,uvrkant
C sav1=L_(5)
      if(.not.L_ek) then
         R0_uf=0.0
      elseif(.not.L0_ak) then
         R0_uf=R0_of
      else
         R0_uf=max(R_(4)-deltat,0.0)
      endif
      L_(5)=L_ek.and.R0_uf.le.0.0
      L0_ak=L_ek
C FDA20_SP2_UVR.fgi( 370,  51):recalc:�������� ��������� ������
C if(sav1.ne.L_(5) .and. try2127.gt.0) goto 2127
      if(L_abu) then
          if (L_(6)) then
              I_ibu = 23
              L_ud = .true.
              L_(36) = .false.
          endif
          if (L_arife) then
              L_(36) = .true.
              L_ud = .false.
          endif
          if (I_ibu.ne.23) then
              L_ud = .false.
              L_(36) = .false.
          endif
      else
          L_(36) = .false.
      endif
C FDA20_SP2_UVR.fgi( 342,  40):��� ������� ���������,uvrkant
      if(L_ud.and..not.L0_od) then
         R0_ed=R0_id
      else
         R0_ed=max(R_(2)-deltat,0.0)
      endif
      L_il=R0_ed.gt.0.0
      L0_od=L_ud
C FDA20_SP2_UVR.fgi( 369,  40):������������  �� T
      L_udife=L_il
C FDA20_SP2_UVR.fgi( 419,  40):������,20FDA23AB002_ulucl
      L_(4) = L_ad.AND.L_ek
C FDA20_SP2_UVR.fgi( 367,  58):�
      if(L_(4).and..not.L0_if) then
         R0_af=R0_ef
      else
         R0_af=max(R_(3)-deltat,0.0)
      endif
      L_odife=R0_af.gt.0.0
      L0_if=L_(4)
C FDA20_SP2_UVR.fgi( 386,  58):������������  �� T
      if(L_odife) then
         R_(6)=R_(1)
      endif
C FDA20_SP2_UVR.fgi( 394,  66):���� � ������������� �������
      R8_e=R_(6)
C FDA20_SP2_UVR.fgi( 394,  66):������-�������: ���������� ��� �������������� ������
      R_i = R8_e
C FDA20_SP2_UVR.fgi( 446,  67):��������
      if(L_abu) then
          if (L_(36)) then
              I_ibu = 24
              L_afi = .true.
              L_(38) = .false.
          endif
          if (L_(37)) then
              L_(38) = .true.
              L_afi = .false.
          endif
          if (I_ibu.ne.24) then
              L_afi = .false.
              L_(38) = .false.
          endif
      else
          L_(38) = .false.
      endif
C FDA20_SP2_UVR.fgi( 342,  26):��� ������� ���������,uvrkant
C sav1=L_(37)
      if(.not.L_afi) then
         R0_odi=0.0
      elseif(.not.L0_udi) then
         R0_odi=R0_idi
      else
         R0_odi=max(R_(38)-deltat,0.0)
      endif
      L_(37)=L_afi.and.R0_odi.le.0.0
      L0_udi=L_afi
C FDA20_SP2_UVR.fgi( 370,  20):recalc:�������� ��������� ������
C if(sav1.ne.L_(37) .and. try2136.gt.0) goto 2136
C sav1=L_evu
C FDA20_SP2_UVR.fgi( 416,  26):recalc:������,tilter_complete
C if(sav1.ne.L_evu .and. try2138.gt.0) goto 2138
      if(L_(38)) then
         I_ibu=0
         L_abu=.false.
      endif
C FDA20_SP2_UVR.fgi( 342,  10):����� ������� ���������,uvrkant
      L_al=L_el
C FDA20_SP2_UVR.fgi( 416,  74):������,g_proc
      if(L_ufi.and..not.L0_ofi) then
         R0_efi=R0_ifi
      else
         R0_efi=max(R_(39)-deltat,0.0)
      endif
      L_(40)=R0_efi.gt.0.0
      L0_ofi=L_ufi
C FDA20_SP2_UVR.fgi( 369,  85):������������  �� T
      if(L_oki.and..not.L0_iki) then
         R0_aki=R0_eki
      else
         R0_aki=max(R_(40)-deltat,0.0)
      endif
      L_(62)=R0_aki.gt.0.0
      L0_iki=L_oki
C FDA20_SP2_UVR.fgi( 369,  96):������������  �� T
      if(L_um.and..not.L0_om) then
         R0_em=R0_im
      else
         R0_em=max(R_(8)-deltat,0.0)
      endif
      L_(64)=R0_em.gt.0.0
      L0_om=L_um
C FDA20_SP2_UVR.fgi( 369, 107):������������  �� T
      if(L_ili.and..not.L0_eli) then
         R0_uki=R0_ali
      else
         R0_uki=max(R_(41)-deltat,0.0)
      endif
      L_(43)=R0_uki.gt.0.0
      L0_eli=L_ili
C FDA20_SP2_UVR.fgi( 369, 118):������������  �� T
      if(L_emi.and..not.L0_ami) then
         R0_oli=R0_uli
      else
         R0_oli=max(R_(42)-deltat,0.0)
      endif
      L_(58)=R0_oli.gt.0.0
      L0_ami=L_emi
C FDA20_SP2_UVR.fgi( 369, 129):������������  �� T
      if(L_api.and..not.L0_umi) then
         R0_imi=R0_omi
      else
         R0_imi=max(R_(43)-deltat,0.0)
      endif
      L_(47)=R0_imi.gt.0.0
      L0_umi=L_api
C FDA20_SP2_UVR.fgi( 369, 140):������������  �� T
      if(L_upi.and..not.L0_opi) then
         R0_epi=R0_ipi
      else
         R0_epi=max(R_(44)-deltat,0.0)
      endif
      L_(65)=R0_epi.gt.0.0
      L0_opi=L_upi
C FDA20_SP2_UVR.fgi( 369, 151):������������  �� T
      L_omaf=(L_(65).or.L_omaf).and..not.(L_afi)
      L_(10)=.not.L_omaf
C FDA20_SP2_UVR.fgi( 409, 149):RS �������,tunl
      L_imaf=L_omaf
C FDA20_SP2_UVR.fgi( 440, 151):������,unloading_complete
      if(L_ori.and..not.L0_iri) then
         R0_ari=R0_eri
      else
         R0_ari=max(R_(45)-deltat,0.0)
      endif
      L_exife=R0_ari.gt.0.0
      L0_iri=L_ori
C FDA20_SP2_UVR.fgi( 369, 162):������������  �� T
      if(L_isi.and..not.L0_esi) then
         R0_uri=R0_asi
      else
         R0_uri=max(R_(46)-deltat,0.0)
      endif
      L_(50)=R0_uri.gt.0.0
      L0_esi=L_isi
C FDA20_SP2_UVR.fgi( 369, 173):������������  �� T
      if(L_uti.and..not.L0_oti) then
         R0_eti=R0_iti
      else
         R0_eti=max(R_(48)-deltat,0.0)
      endif
      L_(53)=R0_eti.gt.0.0
      L0_oti=L_uti
C FDA20_SP2_UVR.fgi( 369, 188):������������  �� T
      L_uto = L_(53).OR.L_(50)
C FDA20_SP2_UVR.fgi( 404, 187):���
      if(L_ovi.and..not.L0_ivi) then
         R0_avi=R0_evi
      else
         R0_avi=max(R_(49)-deltat,0.0)
      endif
      L_axife=R0_avi.gt.0.0
      L0_ivi=L_ovi
C FDA20_SP2_UVR.fgi( 369, 199):������������  �� T
      L_oto = L_axife.OR.L_exife
C FDA20_SP2_UVR.fgi( 404, 198):���
      !{Call KN_VLVR(deltat,REAL(R_ubofe,4),L_elofe,L_ilofe
C ,R8_uvife,
      Call SOLENOIDVALVE_MAN(deltat,REAL(R_ubofe,4),L_elofe
     &,L_ilofe,R8_uvife,C30_ebofe,
     & L_avike,L_axike,L_aboke,I_ekofe,I_ikofe,R_usife,
     & REAL(R_etife,4),R_avife,REAL(R_ivife,4),
     & R_itife,REAL(R_utife,4),I_ifofe,I_okofe,I_akofe,I_ufofe
     &,
     & L_evife,L_otife,L_oxife,L_ixife,L_olofe,
     & L_atife,L_abofe,L_umofe,L_exife,L_axife,L_omofe,L_apofe
     &,
     & L_ovife,L_ukofe,L_emofe,L_alofe,L_ibofe,L_obofe,
     & REAL(R8_iseke,8),REAL(1.0,4),R8_ebike,L_imofe,R_efofe
     &,
     & REAL(R_uxife,4),L_adofe,L_edofe,L_idofe,L_udofe,L_afofe
     &,L_odofe)
      !}

      if(L_afofe.or.L_udofe.or.L_odofe.or.L_idofe.or.L_edofe.or.L_adofe
     &) then      
                  I_ofofe = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ofofe = z'40000000'
      endif
C FDA20_vlv.fgi( 123, 176):���� ���������� ���������������� ��������,20FDA23AB001
      L_ekur=L_elofe
C FDA20_lamp.fgi( 154, 893):������,20FDA23AB001YV10
      !{
      Call DAT_DISCR_HANDLER(deltat,I_efur,L_ufur,R_ofur,
     & REAL(R_akur,4),L_ekur,L_afur,I_ifur)
      !}
C FDA20_lamp.fgi( 152, 878):���������� ������� ���������,20FDA23AB001QH01
      L_oxor=L_ilofe
C FDA20_lamp.fgi( 154, 889):������,20FDA23AB001YV11
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ovor,L_exor,R_axor,
     & REAL(R_ixor,4),L_oxor,L_ivor,I_uvor)
      !}
C FDA20_lamp.fgi( 152, 864):���������� ������� ���������,20FDA23AB001QH01_1
      L_umaf=L_ilofe
C FDA20_SP2_UVR.fgi( 490, 162):������,gran_v_closed
      if(L_ixi.and..not.L0_exi) then
         R0_uvi=R0_axi
      else
         R0_uvi=max(R_(50)-deltat,0.0)
      endif
      L_(59)=R0_uvi.gt.0.0
      L0_exi=L_ixi
C FDA20_SP2_UVR.fgi( 369, 210):������������  �� T
      if(L_ibo.and..not.L0_abo) then
         R0_oxi=R0_uxi
      else
         R0_oxi=max(R_(51)-deltat,0.0)
      endif
      L_(60)=R0_oxi.gt.0.0
      L0_abo=L_ibo
C FDA20_SP2_UVR.fgi( 369, 221):������������  �� T
      L_ito = L_(60).OR.L_(47)
C FDA20_SP2_UVR.fgi( 404, 220):���
      if(L_edo.and..not.L0_ado) then
         R0_obo=R0_ubo
      else
         R0_obo=max(R_(52)-deltat,0.0)
      endif
      L_(66)=R0_obo.gt.0.0
      L0_ado=L_edo
C FDA20_SP2_UVR.fgi( 369, 232):������������  �� T
      if(L_afo.and..not.L0_udo) then
         R0_ido=R0_odo
      else
         R0_ido=max(R_(53)-deltat,0.0)
      endif
      L_eto=R0_ido.gt.0.0
      L0_udo=L_afo
C FDA20_SP2_UVR.fgi( 369, 243):������������  �� T
      if(L_ufo.and..not.L0_ofo) then
         R0_efo=R0_ifo
      else
         R0_efo=max(R_(54)-deltat,0.0)
      endif
      L_(68)=R0_efo.gt.0.0
      L0_ofo=L_ufo
C FDA20_SP2_UVR.fgi( 369, 254):������������  �� T
      L_oso = L_(68).OR.L_(59).OR.L_(58)
C FDA20_SP2_UVR.fgi( 400, 252):���
      if(L_oko.and..not.L0_iko) then
         R0_ako=R0_eko
      else
         R0_ako=max(R_(55)-deltat,0.0)
      endif
      L_(70)=R0_ako.gt.0.0
      L0_iko=L_oko
C FDA20_SP2_UVR.fgi( 369, 265):������������  �� T
      L_avo = L_(70).OR.L_(62)
C FDA20_SP2_UVR.fgi( 400, 264):���
      if(L_ulo.and..not.L0_ilo) then
         R0_alo=R0_elo
      else
         R0_alo=max(R_(56)-deltat,0.0)
      endif
      L_(72)=R0_alo.gt.0.0
      L0_ilo=L_ulo
C FDA20_SP2_UVR.fgi( 369, 276):������������  �� T
      L_uso = L_(72).OR.L_(66).OR.L_(65).OR.L_(64)
C FDA20_SP2_UVR.fgi( 400, 273):���
      if(L_omo.and..not.L0_imo) then
         R0_amo=R0_emo
      else
         R0_amo=max(R_(57)-deltat,0.0)
      endif
      L_(74)=R0_amo.gt.0.0
      L0_imo=L_omo
C FDA20_SP2_UVR.fgi( 369, 287):������������  �� T
      L_ato = L_(74).OR.L_(43)
C FDA20_SP2_UVR.fgi( 400, 286):���
      if(L_ivo.and..not.L0_epo) then
         R0_umo=R0_apo
      else
         R0_umo=max(R_(58)-deltat,0.0)
      endif
      L_(76)=R0_umo.gt.0.0
      L0_epo=L_ivo
C FDA20_SP2_UVR.fgi( 369, 298):������������  �� T
      L_aro = L_(76).OR.L_(40)
C FDA20_SP2_UVR.fgi( 400, 297):���
      L_upo=L_aro
C FDA20_SP2_UVR.fgi( 424, 297):������,20FDA20KANT01_INIT
      L_utu=L_avu
C FDA20_SP2_UVR.fgi( 184, 165):������,tilter_run
      if(L_aruf) then
          if (L_(99)) then
              I_iruf = 25
              L_atu = .true.
              L_(97) = .false.
          endif
          if (L_ivof) then
              L_(97) = .true.
              L_atu = .false.
          endif
          if (I_iruf.ne.25) then
              L_atu = .false.
              L_(97) = .false.
          endif
      else
          L_(97) = .false.
      endif
C FDA20_SP2_UVR.fgi(  93, 124):��� ������� ���������,uvr
      if(L_aruf) then
          if (L_(97)) then
              I_iruf = 26
              L_asu = .true.
              L_(91) = .false.
          endif
          if (L_ekof) then
              L_(91) = .true.
              L_asu = .false.
          endif
          if (I_iruf.ne.26) then
              L_asu = .false.
              L_(91) = .false.
          endif
      else
          L_(91) = .false.
      endif
C FDA20_SP2_UVR.fgi(  93, 113):��� ������� ���������,uvr
      if(L_aruf) then
          if (L_(91)) then
              I_iruf = 27
              L_eru = .true.
              L_(89) = .false.
          endif
          if (L_amif) then
              L_(89) = .true.
              L_eru = .false.
          endif
          if (I_iruf.ne.27) then
              L_eru = .false.
              L_(89) = .false.
          endif
      else
          L_(89) = .false.
      endif
C FDA20_SP2_UVR.fgi(  93, 102):��� ������� ���������,uvr
      if(L_aruf) then
          if (L_(89)) then
              I_iruf = 28
              L_ipu = .true.
              L_(85) = .false.
          endif
          if (L_epu) then
              L_(85) = .true.
              L_ipu = .false.
          endif
          if (I_iruf.ne.28) then
              L_ipu = .false.
              L_(85) = .false.
          endif
      else
          L_(85) = .false.
      endif
C FDA20_SP2_UVR.fgi(  93,  91):��� ������� ���������,uvr
      if(L_aruf) then
          if (L_(85)) then
              I_iruf = 29
              L_imu = .true.
              L_(84) = .false.
          endif
          if (L_ebif) then
              L_(84) = .true.
              L_imu = .false.
          endif
          if (I_iruf.ne.29) then
              L_imu = .false.
              L_(84) = .false.
          endif
      else
          L_(84) = .false.
      endif
C FDA20_SP2_UVR.fgi(  93,  80):��� ������� ���������,uvr
      if(L_aruf) then
          if (L_(84)) then
              I_iruf = 30
              L_olu = .true.
              L_(83) = .false.
          endif
          if (L_ekof) then
              L_(83) = .true.
              L_olu = .false.
          endif
          if (I_iruf.ne.30) then
              L_olu = .false.
              L_(83) = .false.
          endif
      else
          L_(83) = .false.
      endif
C FDA20_SP2_UVR.fgi(  93,  69):��� ������� ���������,uvr
      if(L_aruf) then
          if (L_(83)) then
              I_iruf = 31
              L_ilu = .true.
              L_(81) = .false.
          endif
          if (L_ibuf) then
              L_(81) = .true.
              L_ilu = .false.
          endif
          if (I_iruf.ne.31) then
              L_ilu = .false.
              L_(81) = .false.
          endif
      else
          L_(81) = .false.
      endif
C FDA20_SP2_UVR.fgi(  93,  58):��� ������� ���������,uvr
      if(L_ilu.and..not.L0_elu) then
         R0_uku=R0_alu
      else
         R0_uku=max(R_(65)-deltat,0.0)
      endif
      L_(87)=R0_uku.gt.0.0
      L0_elu=L_ilu
C FDA20_SP2_UVR.fgi( 160,  58):������������  �� T
      if(L_olu.and..not.L0_am) then
         R0_ol=R0_ul
      else
         R0_ol=max(R_(7)-deltat,0.0)
      endif
      L_(11)=R0_ol.gt.0.0
      L0_am=L_olu
C FDA20_SP2_UVR.fgi( 160,  69):������������  �� T
      if(L_imu.and..not.L0_emu) then
         R0_ulu=R0_amu
      else
         R0_ulu=max(R_(66)-deltat,0.0)
      endif
      L_(96)=R0_ulu.gt.0.0
      L0_emu=L_imu
C FDA20_SP2_UVR.fgi( 160,  80):������������  �� T
      if(L_ipu.and..not.L0_apu) then
         R0_omu=R0_umu
      else
         R0_omu=max(R_(67)-deltat,0.0)
      endif
      L_(226)=R0_omu.gt.0.0
      L0_apu=L_ipu
C FDA20_SP2_UVR.fgi( 160,  91):������������  �� T
      L_erod = L_(231).OR.L_(231).OR.L_(226)
C FDA20_SP2_UVR.fgi( 404, 773):���
      if(L_eru.and..not.L0_aru) then
         R0_opu=R0_upu
      else
         R0_opu=max(R_(68)-deltat,0.0)
      endif
      L_(88)=R0_opu.gt.0.0
      L0_aru=L_eru
C FDA20_SP2_UVR.fgi( 160, 102):������������  �� T
      if(L_asu.and..not.L0_uru) then
         R0_iru=R0_oru
      else
         R0_iru=max(R_(69)-deltat,0.0)
      endif
      L_(90)=R0_iru.gt.0.0
      L0_uru=L_asu
C FDA20_SP2_UVR.fgi( 160, 113):������������  �� T
      L_(200) = L_(90).OR.L_(11)
C FDA20_SP2_UVR.fgi( 175, 112):���
      if(L_atu.and..not.L0_osu) then
         R0_esu=R0_isu
      else
         R0_esu=max(R_(70)-deltat,0.0)
      endif
      L_(92)=R0_esu.gt.0.0
      L0_osu=L_atu
C FDA20_SP2_UVR.fgi( 160, 124):������������  �� T
      if(L_idu.and..not.L0_edu) then
         R0_ubu=R0_adu
      else
         R0_ubu=max(R_(61)-deltat,0.0)
      endif
      L_(80)=R0_ubu.gt.0.0
      L0_edu=L_idu
C FDA20_SP2_UVR.fgi( 167, 184):������������  �� T
      if(L_ibad.and..not.L0_ebad) then
         R0_uxu=R0_abad
      else
         R0_uxu=max(R_(74)-deltat,0.0)
      endif
      L_(104)=R0_uxu.gt.0.0
      L0_ebad=L_ibad
C FDA20_SP2_UVR.fgi( 167, 201):������������  �� T
      L_(146) = L_(104).OR.L_(80)
C FDA20_SP2_UVR.fgi( 179, 200):���
      L_(101) = L_ibad.AND.L_iduf
C FDA20_SP2_UVR.fgi( 153, 195):�
      if(L_(101).and..not.L0_oxu) then
         R0_exu=R0_ixu
      else
         R0_exu=max(R_(73)-deltat,0.0)
      endif
      L_(116)=R0_exu.gt.0.0
      L0_oxu=L_(101)
C FDA20_SP2_UVR.fgi( 167, 195):������������  �� T
      L_olad=L_apad
C FDA20_SP2_UVR.fgi( 184, 240):������,mixing_requ
      if(L_apad.and..not.L0_elad) then
         R0_ukad=R0_alad
      else
         R0_ukad=max(R_(80)-deltat,0.0)
      endif
      L_(107)=R0_ukad.gt.0.0
      L0_elad=L_apad
C FDA20_SP2_UVR.fgi( 137, 235):������������  �� T
      L_amad=(L_(107).or.L_amad).and..not.(L_(110))
      L_emad=.not.L_amad
C FDA20_SP2_UVR.fgi( 155, 233):RS �������,ABC
      L_ulad=L_amad
C FDA20_SP2_UVR.fgi( 184, 235):������,abc_run
      if(.not.L_emad) then
         R0_ufad=0.0
      elseif(.not.L0_akad) then
         R0_ufad=R0_ofad
      else
         R0_ufad=max(R_(78)-deltat,0.0)
      endif
      L_umad=L_emad.and.R0_ufad.le.0.0
      L0_akad=L_emad
C FDA20_SP2_UVR.fgi( 166, 223):�������� ��������� ������
      L_(105) = L_apad.AND.L_obad
C FDA20_SP2_UVR.fgi( 153, 211):�
      if(L_(105).and..not.L0_edad) then
         R0_ubad=R0_adad
      else
         R0_ubad=max(R_(75)-deltat,0.0)
      endif
      L_(216)=R0_ubad.gt.0.0
      L0_edad=L_(105)
C FDA20_SP2_UVR.fgi( 167, 211):������������  �� T
      if(L_apad.and..not.L0_udad) then
         R0_idad=R0_odad
      else
         R0_idad=max(R_(76)-deltat,0.0)
      endif
      L_(227)=R0_idad.gt.0.0
      L0_udad=L_apad
C FDA20_SP2_UVR.fgi( 167, 217):������������  �� T
      L_esod = L_(231).OR.L_(231).OR.L_(227)
C FDA20_SP2_UVR.fgi( 404, 803):���
      if(L_ele.and..not.L0_ale) then
         R0_oke=R0_uke
      else
         R0_oke=max(R_(25)-deltat,0.0)
      endif
      L_(151)=R0_oke.gt.0.0
      L0_ale=L_ele
C FDA20_SP2_UVR.fgi( 144, 268):������������  �� T
      if(L_orad.and..not.L0_erad) then
         R0_upad=R0_arad
      else
         R0_upad=max(R_(82)-deltat,0.0)
      endif
      L_(114)=R0_upad.gt.0.0
      L0_erad=L_orad
C FDA20_SP2_UVR.fgi( 144, 295):������������  �� T
      L_(120) = L_(114).OR.L_(231).OR.L_(231)
C FDA20_SP2_UVR.fgi( 400, 419):���
      if(L_umed.and..not.L0_omed) then
         R0_emed=R0_imed
      else
         R0_emed=max(R_(92)-deltat,0.0)
      endif
      L_(145)=R0_emed.gt.0.0
      L0_omed=L_umed
C FDA20_SP2_UVR.fgi( 174, 358):������������  �� T
      L_(132) = L_umed.AND.L_eduf
C FDA20_SP2_UVR.fgi( 160, 352):�
      if(L_(132).and..not.L0_amed) then
         R0_oled=R0_uled
      else
         R0_oled=max(R_(91)-deltat,0.0)
      endif
      L_(213)=R0_oled.gt.0.0
      L0_amed=L_(132)
C FDA20_SP2_UVR.fgi( 174, 352):������������  �� T
      L_(131) = L_umed.AND.L_olod
C FDA20_SP2_UVR.fgi( 160, 346):�
      if(L_(131).and..not.L0_iled) then
         R0_aled=R0_eled
      else
         R0_aled=max(R_(90)-deltat,0.0)
      endif
      L_(144)=R0_aled.gt.0.0
      L0_iled=L_(131)
C FDA20_SP2_UVR.fgi( 174, 346):������������  �� T
      L_(130) = L_umed.AND.L_aduf
C FDA20_SP2_UVR.fgi( 160, 340):�
      if(L_(130).and..not.L0_uked) then
         R0_iked=R0_oked
      else
         R0_iked=max(R_(89)-deltat,0.0)
      endif
      L_(210)=R0_iked.gt.0.0
      L0_uked=L_(130)
C FDA20_SP2_UVR.fgi( 174, 340):������������  �� T
      L_(129) = L_umed.AND.L_ilod
C FDA20_SP2_UVR.fgi( 160, 334):�
      if(L_(129).and..not.L0_eked) then
         R0_ufed=R0_aked
      else
         R0_ufed=max(R_(88)-deltat,0.0)
      endif
      L_(143)=R0_ufed.gt.0.0
      L0_eked=L_(129)
C FDA20_SP2_UVR.fgi( 174, 334):������������  �� T
      L_(128) = L_umed.AND.L_ubuf
C FDA20_SP2_UVR.fgi( 160, 328):�
      if(L_(128).and..not.L0_ofed) then
         R0_efed=R0_ifed
      else
         R0_efed=max(R_(87)-deltat,0.0)
      endif
      L_(207)=R0_efed.gt.0.0
      L0_ofed=L_(128)
C FDA20_SP2_UVR.fgi( 174, 328):������������  �� T
      L_(127) = L_umed.AND.L_elod
C FDA20_SP2_UVR.fgi( 160, 322):�
      if(L_(127).and..not.L0_afed) then
         R0_oded=R0_uded
      else
         R0_oded=max(R_(86)-deltat,0.0)
      endif
      L_(142)=R0_oded.gt.0.0
      L0_afed=L_(127)
C FDA20_SP2_UVR.fgi( 174, 322):������������  �� T
      L_(126) = L_umed.AND.L_obuf
C FDA20_SP2_UVR.fgi( 160, 316):�
      if(L_(126).and..not.L0_ided) then
         R0_aded=R0_eded
      else
         R0_aded=max(R_(85)-deltat,0.0)
      endif
      L_(204)=R0_aded.gt.0.0
      L0_ided=L_(126)
C FDA20_SP2_UVR.fgi( 174, 316):������������  �� T
      L_(125) = L_umed.AND.L_alod
C FDA20_SP2_UVR.fgi( 160, 310):�
      if(L_(125).and..not.L0_ubed) then
         R0_ibed=R0_obed
      else
         R0_ibed=max(R_(84)-deltat,0.0)
      endif
      L_(141)=R0_ibed.gt.0.0
      L0_ubed=L_(125)
C FDA20_SP2_UVR.fgi( 174, 310):������������  �� T
      L_(124) = L_umed.AND.L_ibuf
C FDA20_SP2_UVR.fgi( 160, 304):�
      if(L_(124).and..not.L0_ebed) then
         R0_uxad=R0_abed
      else
         R0_uxad=max(R_(83)-deltat,0.0)
      endif
      L_(201)=R0_uxad.gt.0.0
      L0_ebed=L_(124)
C FDA20_SP2_UVR.fgi( 174, 304):������������  �� T
      L_ofid=L_ifid
C FDA20_SP2_UVR.fgi( 194, 369):������,box5meas
      L_(242) = L_otud.AND.L_ofid
C FDA20_SP2_UVR.fgi( 512, 278):�
      L_(243) = L_utud.AND.L_ofid
C FDA20_SP2_UVR.fgi( 512, 282):�
      L_(244) = L_avud.AND.L_ofid
C FDA20_SP2_UVR.fgi( 512, 286):�
      L_(245) = L_evud.AND.L_ofid
C FDA20_SP2_UVR.fgi( 512, 290):�
      L_(246) = L_ivud.AND.L_ofid
C FDA20_SP2_UVR.fgi( 512, 294):�
      L_(247) = L_ovud.AND.L_ofid
C FDA20_SP2_UVR.fgi( 512, 298):�
      L_(248) = L_uvud.AND.L_ofid
C FDA20_SP2_UVR.fgi( 512, 302):�
      L_(249) = L_axud.AND.L_ofid
C FDA20_SP2_UVR.fgi( 512, 306):�
      if(L_(249)) then
         R_(141)=R_adef
      else
         R_(141)=R_(142)
      endif
C FDA20_SP2_UVR.fgi( 552, 306):���� RE IN LO CH7
      if(L_(248)) then
         R_(140)=R_ubef
      else
         R_(140)=R_(141)
      endif
C FDA20_SP2_UVR.fgi( 548, 302):���� RE IN LO CH7
      if(L_(247)) then
         R_(139)=R_obef
      else
         R_(139)=R_(140)
      endif
C FDA20_SP2_UVR.fgi( 544, 298):���� RE IN LO CH7
      if(L_(246)) then
         R_(138)=R_ibef
      else
         R_(138)=R_(139)
      endif
C FDA20_SP2_UVR.fgi( 540, 294):���� RE IN LO CH7
      if(L_(245)) then
         R_(137)=R_ebef
      else
         R_(137)=R_(138)
      endif
C FDA20_SP2_UVR.fgi( 536, 290):���� RE IN LO CH7
      if(L_(244)) then
         R_(136)=R_abef
      else
         R_(136)=R_(137)
      endif
C FDA20_SP2_UVR.fgi( 532, 286):���� RE IN LO CH7
      if(L_(243)) then
         R_(135)=R_uxaf
      else
         R_(135)=R_(136)
      endif
C FDA20_SP2_UVR.fgi( 528, 282):���� RE IN LO CH7
      if(L_(242)) then
         R_(134)=R_oxaf
      else
         R_(134)=R_(135)
      endif
C FDA20_SP2_UVR.fgi( 524, 278):���� RE IN LO CH7
      L_(241) = L_itud.AND.L_ofid
C FDA20_SP2_UVR.fgi( 512, 274):�
      if(L_(241)) then
         R_osof=R_ixaf
      else
         R_osof=R_(134)
      endif
C FDA20_SP2_UVR.fgi( 520, 274):���� RE IN LO CH7
      L_(240)=R_osof.gt.R_(124)
C FDA20_SP2_UVR.fgi( 542, 277):���������� >
      !{
      Call DAT_ANA_HANDLER(deltat,R_irof,R_evof,REAL(1,4)
     &,
     & REAL(R_asof,4),REAL(R_esof,4),
     & REAL(R_erof,4),REAL(R_arof,4),I_avof,
     & REAL(R_usof,4),L_atof,REAL(R_etof,4),L_itof,L_otof
     &,R_isof,
     & REAL(R_urof,4),REAL(R_orof,4),L_utof,REAL(R_osof,4
     &))
      !}
C FDA20_SP2_UVR.fgi( 573, 269):���������� �������,fda_uvr_w2
      L_araf = L_(241).OR.L_(242).OR.L_(243).OR.L_(244).OR.L_
     &(245).OR.L_(246).OR.L_(247).OR.L_(248).OR.L_(249)
C FDA20_SP2_UVR.fgi( 558, 319):���
      L_upaf = L_araf.AND.L_(240)
C FDA20_SP2_UVR.fgi( 564, 292):�
      L_opaf = L_araf.AND.(.NOT.L_(240))
C FDA20_SP2_UVR.fgi( 564, 288):�
      if(L_ulid) then
         R0_ep=0.0
      elseif(L0_ip) then
         R0_ep=R0_ap
      else
         R0_ep=max(R_(9)-deltat,0.0)
      endif
      L_(160)=L_ulid.or.R0_ep.gt.0.0
      L0_ip=L_ulid
C FDA20_SP2_UVR.fgi( 123, 405):�������� ������� ������
      if(L_(160).and..not.L0_olid) then
         R0_elid=R0_ilid
      else
         R0_elid=max(R_(101)-deltat,0.0)
      endif
      L_(159)=R0_elid.gt.0.0
      L0_olid=L_(160)
C FDA20_SP2_UVR.fgi( 169, 405):������������  �� T
      L_(155) = L_(160).AND.L_ikid
C FDA20_SP2_UVR.fgi( 160, 393):�
      if(L_(155).and..not.L0_ekid) then
         R0_ufid=R0_akid
      else
         R0_ufid=max(R_(99)-deltat,0.0)
      endif
      L_(218)=R0_ufid.gt.0.0
      L0_ekid=L_(155)
C FDA20_SP2_UVR.fgi( 169, 393):������������  �� T
      L_(135) = L_(160).AND.L_oped
C FDA20_SP2_UVR.fgi( 160, 387):�
      if(L_(135).and..not.L0_iped) then
         R0_aped=R0_eped
      else
         R0_aped=max(R_(93)-deltat,0.0)
      endif
      L_(147)=R0_aped.gt.0.0
      L0_iped=L_(135)
C FDA20_SP2_UVR.fgi( 169, 387):������������  �� T
      L_(12) = L_(160).AND.L_oduf
C FDA20_SP2_UVR.fgi( 160, 381):�
      if(L_(12).and..not.L0_us) then
         R0_is=R0_os
      else
         R0_is=max(R_(13)-deltat,0.0)
      endif
      L_(16)=R0_is.gt.0.0
      L0_us=L_(12)
C FDA20_SP2_UVR.fgi( 169, 381):������������  �� T
      L_(117) = L_(159).OR.L_(16)
C FDA20_SP2_UVR.fgi( 180, 404):���
      if(.not.L_(160)) then
         R0_ax=0.0
      elseif(.not.L0_ex) then
         R0_ax=R0_uv
      else
         R0_ax=max(R_(17)-deltat,0.0)
      endif
      L_(157)=L_(160).and.R0_ax.le.0.0
      L0_ex=L_(160)
C FDA20_SP2_UVR.fgi( 143, 398):�������� ��������� ������
      L_(158) = L_(160).AND.L_(157)
C FDA20_SP2_UVR.fgi( 160, 399):�
      if(L_(158).and..not.L0_alid) then
         R0_okid=R0_ukid
      else
         R0_okid=max(R_(100)-deltat,0.0)
      endif
      L_(156)=R0_okid.gt.0.0
      L0_alid=L_(158)
C FDA20_SP2_UVR.fgi( 169, 399):������������  �� T
      if(L_arid) then
         R0_up=0.0
      elseif(L0_ar) then
         R0_up=R0_op
      else
         R0_up=max(R_(10)-deltat,0.0)
      endif
      L_(168)=L_arid.or.R0_up.gt.0.0
      L0_ar=L_arid
C FDA20_SP2_UVR.fgi( 123, 436):�������� ������� ������
      if(L_(168).and..not.L0_upid) then
         R0_ipid=R0_opid
      else
         R0_ipid=max(R_(104)-deltat,0.0)
      endif
      L_(167)=R0_ipid.gt.0.0
      L0_upid=L_(168)
C FDA20_SP2_UVR.fgi( 169, 436):������������  �� T
      L_(163) = L_(168).AND.L_omid
C FDA20_SP2_UVR.fgi( 160, 424):�
      if(L_(163).and..not.L0_imid) then
         R0_amid=R0_emid
      else
         R0_amid=max(R_(102)-deltat,0.0)
      endif
      L_(220)=R0_amid.gt.0.0
      L0_imid=L_(163)
C FDA20_SP2_UVR.fgi( 169, 424):������������  �� T
      L_(136) = L_(168).AND.L_ired
C FDA20_SP2_UVR.fgi( 160, 418):�
      if(L_(136).and..not.L0_ered) then
         R0_uped=R0_ared
      else
         R0_uped=max(R_(94)-deltat,0.0)
      endif
      L_(148)=R0_uped.gt.0.0
      L0_ered=L_(136)
C FDA20_SP2_UVR.fgi( 169, 418):������������  �� T
      L_(13) = L_(168).AND.L_uduf
C FDA20_SP2_UVR.fgi( 160, 412):�
      if(L_(13).and..not.L0_it) then
         R0_at=R0_et
      else
         R0_at=max(R_(14)-deltat,0.0)
      endif
      L_(17)=R0_at.gt.0.0
      L0_it=L_(13)
C FDA20_SP2_UVR.fgi( 169, 412):������������  �� T
      L_(118) = L_(167).OR.L_(17)
C FDA20_SP2_UVR.fgi( 180, 435):���
      if(.not.L_(168)) then
         R0_ox=0.0
      elseif(.not.L0_ux) then
         R0_ox=R0_ix
      else
         R0_ox=max(R_(18)-deltat,0.0)
      endif
      L_(165)=L_(168).and.R0_ox.le.0.0
      L0_ux=L_(168)
C FDA20_SP2_UVR.fgi( 143, 429):�������� ��������� ������
      L_(166) = L_(168).AND.L_(165)
C FDA20_SP2_UVR.fgi( 160, 430):�
      if(L_(166).and..not.L0_epid) then
         R0_umid=R0_apid
      else
         R0_umid=max(R_(103)-deltat,0.0)
      endif
      L_(164)=R0_umid.gt.0.0
      L0_epid=L_(166)
C FDA20_SP2_UVR.fgi( 169, 430):������������  �� T
      if(L_etid) then
         R0_ir=0.0
      elseif(L0_or) then
         R0_ir=R0_er
      else
         R0_ir=max(R_(11)-deltat,0.0)
      endif
      L_(176)=L_etid.or.R0_ir.gt.0.0
      L0_or=L_etid
C FDA20_SP2_UVR.fgi( 123, 467):�������� ������� ������
      if(L_(176).and..not.L0_atid) then
         R0_osid=R0_usid
      else
         R0_osid=max(R_(107)-deltat,0.0)
      endif
      L_(175)=R0_osid.gt.0.0
      L0_atid=L_(176)
C FDA20_SP2_UVR.fgi( 169, 467):������������  �� T
      L_(171) = L_(176).AND.L_urid
C FDA20_SP2_UVR.fgi( 160, 455):�
      if(L_(171).and..not.L0_orid) then
         R0_erid=R0_irid
      else
         R0_erid=max(R_(105)-deltat,0.0)
      endif
      L_(222)=R0_erid.gt.0.0
      L0_orid=L_(171)
C FDA20_SP2_UVR.fgi( 169, 455):������������  �� T
      L_(137) = L_(176).AND.L_esed
C FDA20_SP2_UVR.fgi( 160, 449):�
      if(L_(137).and..not.L0_ased) then
         R0_ored=R0_ured
      else
         R0_ored=max(R_(95)-deltat,0.0)
      endif
      L_(149)=R0_ored.gt.0.0
      L0_ased=L_(137)
C FDA20_SP2_UVR.fgi( 169, 449):������������  �� T
      L_(14) = L_(176).AND.L_afuf
C FDA20_SP2_UVR.fgi( 160, 443):�
      if(L_(14).and..not.L0_av) then
         R0_ot=R0_ut
      else
         R0_ot=max(R_(15)-deltat,0.0)
      endif
      L_(18)=R0_ot.gt.0.0
      L0_av=L_(14)
C FDA20_SP2_UVR.fgi( 169, 443):������������  �� T
      L_(119) = L_(175).OR.L_(18)
C FDA20_SP2_UVR.fgi( 180, 466):���
      if(.not.L_(176)) then
         R0_ebe=0.0
      elseif(.not.L0_ibe) then
         R0_ebe=R0_abe
      else
         R0_ebe=max(R_(19)-deltat,0.0)
      endif
      L_(173)=L_(176).and.R0_ebe.le.0.0
      L0_ibe=L_(176)
C FDA20_SP2_UVR.fgi( 143, 460):�������� ��������� ������
      L_(174) = L_(176).AND.L_(173)
C FDA20_SP2_UVR.fgi( 160, 461):�
      if(L_(174).and..not.L0_isid) then
         R0_asid=R0_esid
      else
         R0_asid=max(R_(106)-deltat,0.0)
      endif
      L_(172)=R0_asid.gt.0.0
      L0_isid=L_(174)
C FDA20_SP2_UVR.fgi( 169, 461):������������  �� T
      if(L_ixid) then
         R0_as=0.0
      elseif(L0_es) then
         R0_as=R0_ur
      else
         R0_as=max(R_(12)-deltat,0.0)
      endif
      L_(185)=L_ixid.or.R0_as.gt.0.0
      L0_es=L_ixid
C FDA20_SP2_UVR.fgi( 123, 499):�������� ������� ������
      if(L_(185).and..not.L0_exid) then
         R0_uvid=R0_axid
      else
         R0_uvid=max(R_(110)-deltat,0.0)
      endif
      L_(184)=R0_uvid.gt.0.0
      L0_exid=L_(185)
C FDA20_SP2_UVR.fgi( 169, 499):������������  �� T
      if(.not.L_(185)) then
         R0_ube=0.0
      elseif(.not.L0_ade) then
         R0_ube=R0_obe
      else
         R0_ube=max(R_(20)-deltat,0.0)
      endif
      L_(181)=L_(185).and.R0_ube.le.0.0
      L0_ade=L_(185)
C FDA20_SP2_UVR.fgi( 143, 492):�������� ��������� ������
      L_(182) = L_(185).AND.L_(181)
C FDA20_SP2_UVR.fgi( 160, 493):�
      if(L_(182).and..not.L0_ovid) then
         R0_evid=R0_ivid
      else
         R0_evid=max(R_(109)-deltat,0.0)
      endif
      L_(180)=R0_evid.gt.0.0
      L0_ovid=L_(182)
C FDA20_SP2_UVR.fgi( 169, 493):������������  �� T
      L_(179) = L_(185).AND.L_avid
C FDA20_SP2_UVR.fgi( 160, 487):�
      if(L_(179).and..not.L0_utid) then
         R0_itid=R0_otid
      else
         R0_itid=max(R_(108)-deltat,0.0)
      endif
      L_(224)=R0_itid.gt.0.0
      L0_utid=L_(179)
C FDA20_SP2_UVR.fgi( 169, 487):������������  �� T
      L_(138) = L_(185).AND.L_ated
C FDA20_SP2_UVR.fgi( 160, 481):�
      if(L_(138).and..not.L0_used) then
         R0_ised=R0_osed
      else
         R0_ised=max(R_(96)-deltat,0.0)
      endif
      L_(152)=R0_ised.gt.0.0
      L0_used=L_(138)
C FDA20_SP2_UVR.fgi( 169, 481):������������  �� T
      L_(15) = L_(185).AND.L_efuf
C FDA20_SP2_UVR.fgi( 160, 475):�
      if(L_(15).and..not.L0_ov) then
         R0_ev=R0_iv
      else
         R0_ev=max(R_(16)-deltat,0.0)
      endif
      L_(19)=R0_ev.gt.0.0
      L0_ov=L_(15)
C FDA20_SP2_UVR.fgi( 169, 475):������������  �� T
      L_(183) = L_(184).OR.L_(19)
C FDA20_SP2_UVR.fgi( 180, 498):���
      L_etaf=(L_ebod.or.L_etaf).and..not.(L_(189))
      L_(190)=.not.L_etaf
C FDA20_SP2_UVR.fgi( 158, 521):RS �������
      L_ataf=L_etaf
C FDA20_SP2_UVR.fgi( 180, 511):������,box02complete
      L_ikop=L_etaf
C FDA20_SP2_UVR.fgi( 210, 511):������,bd_pu_close
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ifop,L_akop,R_ufop,
     & REAL(R_ekop,4),L_ikop,L_efop,I_ofop)
      !}
C FDA20_lamp.fgi(  74, 432):���������� ������� ���������,20FDA21AL004BQ48
      L_usaf=L_etaf
C FDA20_SP2_UVR.fgi( 180, 515):������,box03complete
      L_ukak=L_etaf
C FDA20_SP2_UVR.fgi( 210, 515):������,bd_scrap_close
      L_ikak=L_ukak
C FDA20_SP1.fgi( 358, 738):������,20FDA21AL001C17_ulucl
      L_osaf=L_etaf
C FDA20_SP2_UVR.fgi( 180, 519):������,box04complete
      L_alak=L_etaf
C FDA20_SP2_UVR.fgi( 210, 519):������,bd_carbon_close
      L_akak=L_alak
C FDA20_SP1.fgi( 358, 767):������,20FDA21AL001C30_ulucl
      L_isaf=L_etaf
C FDA20_SP2_UVR.fgi( 180, 523):������,box05complete
      L_elak=L_etaf
C FDA20_SP2_UVR.fgi( 210, 523):������,bd_u2_close
      L_ofak=L_elak
C FDA20_SP1.fgi( 358, 796):������,20FDA21AL001C26_ulucl
      L_osop=L_amod
C FDA20_SP2_UVR.fgi( 210, 555):������,bd_pu_open
      !{
      Call DAT_DISCR_HANDLER(deltat,I_orop,L_esop,R_asop,
     & REAL(R_isop,4),L_osop,L_irop,I_urop)
      !}
C FDA20_lamp.fgi(  74, 420):���������� ������� ���������,20FDA21AL004BQ47
      !{
      Call DAT_DISCR_HANDLER(deltat,I_axip,L_oxip,R_ixip,
     & REAL(R_uxip,4),L_osop,L_uvip,I_exip)
      !}
C FDA20_lamp.fgi( 122, 420):���������� ������� ���������,20FDA21AL004KF01Q41
      if(L_osop.and..not.L0_axak) then
         R0_ovak=R0_uvak
      else
         R0_ovak=max(R_(190)-deltat,0.0)
      endif
      L_(314)=R0_ovak.gt.0.0
      L0_axak=L_osop
C FDA20_SP1.fgi( 415, 713):������������  �� T
      if(I_(1).ne.0) then
        iv1=I_(1)
        if(0) then
          if(L_(313).and..not.L0_abek) then
            R0_oxak=R0_ixak
            R_arek=R_arek+1
          elseif(L_(314).and..not.L0_ebek) then
            R0_oxak=R0_ixak
            R_arek=R_arek-1
          elseif(L_(313)) then
            R0_oxak=R0_oxak-deltat
            if(R0_oxak.lt.0.0) then
              R0_oxak=R0_ixak
              R_arek=R_arek+1
            endif
          elseif(L_(314)) then
            R0_oxak=R0_oxak-deltat
            if(R0_oxak.lt.0.0) then
              R0_oxak=R0_ixak
              R_arek=R_arek-1
            endif
          endif
          L0_abek=L_(313)
          L0_ebek=L_(314)
        else
            R0_oxak=R0_exak-R0_uxak
            if(L_(313).and..not.L_(314)) then
               R_arek=R_arek+R0_oxak*deltat/max(deltat,R0_ixak
     &)
            elseif(L_(314).and..not.L_(313)) then
               R_arek=R_arek-R0_oxak*deltat/max(deltat,R0_ixak
     &)
            endif   
         endif
         if(0) then
            if(R_arek.gt.R0_exak) then
               R_arek=R0_uxak
            elseif(R_arek.lt.R0_uxak) then
               R_arek=R0_exak
            endif
         else
            if(R_arek.gt.R0_exak) then
               R_arek=R0_exak
            elseif(R_arek.lt.R0_uxak) then
               R_arek=R0_uxak
            endif
         endif
      endif
C FDA20_SP1.fgi( 426, 712):��������� ���������
      L_o = L_evod.OR.L_ixod.OR.L_obud.OR.L_amod
C FDA20_SP2_UVR.fgi( 193, 587):���
      if(L_amod.and..not.L0_ukod) then
         R0_ikod=R0_okod
      else
         R0_ikod=max(R_(117)-deltat,0.0)
      endif
      L_(215)=R0_ikod.gt.0.0
      L0_ukod=L_amod
C FDA20_SP2_UVR.fgi( 174, 551):������������  �� T
      L_(197) = L_amod.AND.L_ulod
C FDA20_SP2_UVR.fgi( 160, 547):�
      if(L_(197).and..not.L0_ekod) then
         R0_ufod=R0_akod
      else
         R0_ufod=max(R_(116)-deltat,0.0)
      endif
      L_(212)=R0_ufod.gt.0.0
      L0_ekod=L_(197)
C FDA20_SP2_UVR.fgi( 170, 547):������������  �� T
      L_(193) = L_amod.AND.L_alod
C FDA20_SP2_UVR.fgi( 160, 531):�
      if(L_(193).and..not.L0_ubod) then
         R0_ibod=R0_obod
      else
         R0_ibod=max(R_(112)-deltat,0.0)
      endif
      L_(199)=R0_ibod.gt.0.0
      L0_ubod=L_(193)
C FDA20_SP2_UVR.fgi( 170, 531):������������  �� T
      L_(194) = L_amod.AND.L_elod
C FDA20_SP2_UVR.fgi( 160, 535):�
      if(L_(194).and..not.L0_idod) then
         R0_adod=R0_edod
      else
         R0_adod=max(R_(113)-deltat,0.0)
      endif
      L_(203)=R0_adod.gt.0.0
      L0_idod=L_(194)
C FDA20_SP2_UVR.fgi( 174, 535):������������  �� T
      L_(195) = L_amod.AND.L_ilod
C FDA20_SP2_UVR.fgi( 160, 539):�
      if(L_(195).and..not.L0_afod) then
         R0_odod=R0_udod
      else
         R0_odod=max(R_(114)-deltat,0.0)
      endif
      L_(206)=R0_odod.gt.0.0
      L0_afod=L_(195)
C FDA20_SP2_UVR.fgi( 170, 539):������������  �� T
      L_(196) = L_amod.AND.L_olod
C FDA20_SP2_UVR.fgi( 160, 543):�
      if(L_(196).and..not.L0_ofod) then
         R0_efod=R0_ifod
      else
         R0_efod=max(R_(115)-deltat,0.0)
      endif
      L_(209)=R0_efod.gt.0.0
      L0_ofod=L_(196)
C FDA20_SP2_UVR.fgi( 174, 543):������������  �� T
      L_adek=L_evod
C FDA20_SP2_UVR.fgi( 210, 605):������,bd_scrap_open
      L_okak=L_adek
C FDA20_SP1.fgi( 358, 745):������,20FDA21AL001C17_uluop
      L_atol=L_adek
C FDA20_SP1.fgi( 358, 749):������,vd2
      !{
      Call DAT_DISCR_HANDLER(deltat,I_asol,L_osol,R_isol,
     & REAL(R_usol,4),L_atol,L_urol,I_esol)
      !}
C FDA20_lamp.fgi(  90, 332):���������� ������� ���������,20FDA21AL001KF02BQ31
      if(L_adek.and..not.L0_ubek) then
         R0_ibek=R0_obek
      else
         R0_ibek=max(R_(191)-deltat,0.0)
      endif
      L_(317)=R0_ibek.gt.0.0
      L0_ubek=L_adek
C FDA20_SP1.fgi( 415, 742):������������  �� T
      if(I_(2).ne.0) then
        iv1=I_(2)
        if(0) then
          if(L_(316).and..not.L0_afek) then
            R0_odek=R0_idek
            R_upek=R_upek+1
          elseif(L_(317).and..not.L0_efek) then
            R0_odek=R0_idek
            R_upek=R_upek-1
          elseif(L_(316)) then
            R0_odek=R0_odek-deltat
            if(R0_odek.lt.0.0) then
              R0_odek=R0_idek
              R_upek=R_upek+1
            endif
          elseif(L_(317)) then
            R0_odek=R0_odek-deltat
            if(R0_odek.lt.0.0) then
              R0_odek=R0_idek
              R_upek=R_upek-1
            endif
          endif
          L0_afek=L_(316)
          L0_efek=L_(317)
        else
            R0_odek=R0_edek-R0_udek
            if(L_(316).and..not.L_(317)) then
               R_upek=R_upek+R0_odek*deltat/max(deltat,R0_idek
     &)
            elseif(L_(317).and..not.L_(316)) then
               R_upek=R_upek-R0_odek*deltat/max(deltat,R0_idek
     &)
            endif   
         endif
         if(0) then
            if(R_upek.gt.R0_edek) then
               R_upek=R0_udek
            elseif(R_upek.lt.R0_udek) then
               R_upek=R0_edek
            endif
         else
            if(R_upek.gt.R0_edek) then
               R_upek=R0_edek
            elseif(R_upek.lt.R0_udek) then
               R_upek=R0_udek
            endif
         endif
      endif
C FDA20_SP1.fgi( 426, 741):��������� ���������
      if(L_evod.and..not.L0_utod) then
         R0_itod=R0_otod
      else
         R0_itod=max(R_(118)-deltat,0.0)
      endif
      L_(233)=R0_itod.gt.0.0
      L0_utod=L_evod
C FDA20_SP2_UVR.fgi( 167, 564):������������  �� T
      L_axed = L_(233).OR.L_(156).OR.L_(231).OR.L_(139)
C FDA20_SP2_UVR.fgi( 406, 575):���
      L_(20) = L_evod.AND.L_etod
C FDA20_SP2_UVR.fgi( 156, 558):�
      if(L_(20).and..not.L0_ode) then
         R0_ede=R0_ide
      else
         R0_ede=max(R_(21)-deltat,0.0)
      endif
      L_(228)=R0_ede.gt.0.0
      L0_ode=L_(20)
C FDA20_SP2_UVR.fgi( 167, 558):������������  �� T
      L_isod = L_(228).OR.L_(231).OR.L_(231)
C FDA20_SP2_UVR.fgi( 404, 809):���
      L_akek=L_ixod
C FDA20_SP2_UVR.fgi( 210, 609):������,bd_carbon_open
      L_ekak=L_akek
C FDA20_SP1.fgi( 358, 774):������,20FDA21AL001C30_uluop
      L_erol=L_akek
C FDA20_SP1.fgi( 358, 778):������,vd3
      !{
      Call DAT_DISCR_HANDLER(deltat,I_epol,L_upol,R_opol,
     & REAL(R_arol,4),L_erol,L_apol,I_ipol)
      !}
C FDA20_lamp.fgi(  90, 394):���������� ������� ���������,20FDA21AL004KF03Q41
      if(L_akek.and..not.L0_ufek) then
         R0_ifek=R0_ofek
      else
         R0_ifek=max(R_(192)-deltat,0.0)
      endif
      L_(320)=R0_ifek.gt.0.0
      L0_ufek=L_akek
C FDA20_SP1.fgi( 415, 771):������������  �� T
      if(I_(3).ne.0) then
        iv1=I_(3)
        if(0) then
          if(L_(319).and..not.L0_alek) then
            R0_okek=R0_ikek
            R_ipek=R_ipek+1
          elseif(L_(320).and..not.L0_elek) then
            R0_okek=R0_ikek
            R_ipek=R_ipek-1
          elseif(L_(319)) then
            R0_okek=R0_okek-deltat
            if(R0_okek.lt.0.0) then
              R0_okek=R0_ikek
              R_ipek=R_ipek+1
            endif
          elseif(L_(320)) then
            R0_okek=R0_okek-deltat
            if(R0_okek.lt.0.0) then
              R0_okek=R0_ikek
              R_ipek=R_ipek-1
            endif
          endif
          L0_alek=L_(319)
          L0_elek=L_(320)
        else
            R0_okek=R0_ekek-R0_ukek
            if(L_(319).and..not.L_(320)) then
               R_ipek=R_ipek+R0_okek*deltat/max(deltat,R0_ikek
     &)
            elseif(L_(320).and..not.L_(319)) then
               R_ipek=R_ipek-R0_okek*deltat/max(deltat,R0_ikek
     &)
            endif   
         endif
         if(0) then
            if(R_ipek.gt.R0_ekek) then
               R_ipek=R0_ukek
            elseif(R_ipek.lt.R0_ukek) then
               R_ipek=R0_ekek
            endif
         else
            if(R_ipek.gt.R0_ekek) then
               R_ipek=R0_ekek
            elseif(R_ipek.lt.R0_ukek) then
               R_ipek=R0_ukek
            endif
         endif
      endif
C FDA20_SP1.fgi( 426, 770):��������� ���������
      if(L_ixod.and..not.L0_axod) then
         R0_ovod=R0_uvod
      else
         R0_ovod=max(R_(119)-deltat,0.0)
      endif
      L_(235)=R0_ovod.gt.0.0
      L0_axod=L_ixod
C FDA20_SP2_UVR.fgi( 167, 576):������������  �� T
      L_exed = L_(235).OR.L_(164).OR.L_(231).OR.L_(139)
C FDA20_SP2_UVR.fgi( 406, 583):���
      L_(21) = L_ixod.AND.L_ivod
C FDA20_SP2_UVR.fgi( 156, 570):�
      if(L_(21).and..not.L0_efe) then
         R0_ude=R0_afe
      else
         R0_ude=max(R_(22)-deltat,0.0)
      endif
      L_(229)=R0_ude.gt.0.0
      L0_efe=L_(21)
C FDA20_SP2_UVR.fgi( 167, 570):������������  �� T
      L_osod = L_(229).OR.L_(231).OR.L_(231)
C FDA20_SP2_UVR.fgi( 404, 815):���
      L_amek=L_obud
C FDA20_SP2_UVR.fgi( 210, 613):������,bd_u2_open
      L_ufak=L_amek
C FDA20_SP1.fgi( 358, 803):������,20FDA21AL001C26_uluop
      L_adom=L_amek
C FDA20_SP1.fgi( 358, 807):������,vd1
      !{
      Call DAT_DISCR_HANDLER(deltat,I_abom,L_obom,R_ibom,
     & REAL(R_ubom,4),L_adom,L_uxim,I_ebom)
      !}
C FDA20_lamp.fgi( 122, 218):���������� ������� ���������,20FDA21AL001KF01BQ31
      if(L_amek.and..not.L0_ulek) then
         R0_ilek=R0_olek
      else
         R0_ilek=max(R_(193)-deltat,0.0)
      endif
      L_(323)=R0_ilek.gt.0.0
      L0_ulek=L_amek
C FDA20_SP1.fgi( 415, 800):������������  �� T
      if(I_(4).ne.0) then
        iv1=I_(4)
        if(0) then
          if(L_(322).and..not.L0_apek) then
            R0_omek=R0_imek
            R_opek=R_opek+1
          elseif(L_(323).and..not.L0_epek) then
            R0_omek=R0_imek
            R_opek=R_opek-1
          elseif(L_(322)) then
            R0_omek=R0_omek-deltat
            if(R0_omek.lt.0.0) then
              R0_omek=R0_imek
              R_opek=R_opek+1
            endif
          elseif(L_(323)) then
            R0_omek=R0_omek-deltat
            if(R0_omek.lt.0.0) then
              R0_omek=R0_imek
              R_opek=R_opek-1
            endif
          endif
          L0_apek=L_(322)
          L0_epek=L_(323)
        else
            R0_omek=R0_emek-R0_umek
            if(L_(322).and..not.L_(323)) then
               R_opek=R_opek+R0_omek*deltat/max(deltat,R0_imek
     &)
            elseif(L_(323).and..not.L_(322)) then
               R_opek=R_opek-R0_omek*deltat/max(deltat,R0_imek
     &)
            endif   
         endif
         if(0) then
            if(R_opek.gt.R0_emek) then
               R_opek=R0_umek
            elseif(R_opek.lt.R0_umek) then
               R_opek=R0_emek
            endif
         else
            if(R_opek.gt.R0_emek) then
               R_opek=R0_emek
            elseif(R_opek.lt.R0_umek) then
               R_opek=R0_umek
            endif
         endif
      endif
C FDA20_SP1.fgi( 426, 799):��������� ���������
      if(L_obud.and..not.L0_ebud) then
         R0_uxod=R0_abud
      else
         R0_uxod=max(R_(120)-deltat,0.0)
      endif
      L_(237)=R0_uxod.gt.0.0
      L0_ebud=L_obud
C FDA20_SP2_UVR.fgi( 167, 588):������������  �� T
      L_ixed = L_(237).OR.L_(172).OR.L_(231).OR.L_(139)
C FDA20_SP2_UVR.fgi( 406, 591):���
      L_(22) = L_obud.AND.L_oxod
C FDA20_SP2_UVR.fgi( 156, 582):�
      if(L_(22).and..not.L0_ufe) then
         R0_ife=R0_ofe
      else
         R0_ife=max(R_(23)-deltat,0.0)
      endif
      L_(230)=R0_ife.gt.0.0
      L0_ufe=L_(22)
C FDA20_SP2_UVR.fgi( 167, 582):������������  �� T
      L_usod = L_(230).OR.L_(231).OR.L_(231)
C FDA20_SP2_UVR.fgi( 404, 821):���
      if(L_uvef.and..not.L0_ivef) then
         R0_avef=R0_evef
      else
         R0_avef=max(R_(154)-deltat,0.0)
      endif
      L_(250)=R0_avef.gt.0.0
      L0_ivef=L_uvef
C FDA20_SP2_UVR.fgi( 167, 600):������������  �� T
      L_oxed = L_(250).OR.L_(180).OR.L_(231).OR.L_(139)
C FDA20_SP2_UVR.fgi( 406, 599):���
      L_(23) = L_uvef.AND.L_utef
C FDA20_SP2_UVR.fgi( 156, 594):�
      if(L_(23).and..not.L0_ike) then
         R0_ake=R0_eke
      else
         R0_ake=max(R_(24)-deltat,0.0)
      endif
      L_(232)=R0_ake.gt.0.0
      L0_ike=L_(23)
C FDA20_SP2_UVR.fgi( 167, 594):������������  �� T
      L_atod = L_(232).OR.L_(231).OR.L_(231)
C FDA20_SP2_UVR.fgi( 404, 827):���
      if(L_ulif.and..not.L0_ofif) then
         R0_efif=R0_ifif
      else
         R0_efif=max(R_(157)-deltat,0.0)
      endif
      L_olif=R0_efif.gt.0.0
      L0_ofif=L_ulif
C FDA20_SP2_UVR.fgi( 119, 616):������������  �� T
      L_ilif=L_olif
C FDA20_SP2_UVR.fgi( 252, 616):������,20FDA20TRAN01_NEXT
      L_elif=L_olif
C FDA20_SP2_UVR.fgi( 252, 614):������,20FDA20TRAN02_NEXT
      L_alif=L_olif
C FDA20_SP2_UVR.fgi( 252, 612):������,20FDA20TRAN03_NEXT
      L_ukif=L_olif
C FDA20_SP2_UVR.fgi( 252, 610):������,20FDA20TRAN04_NEXT
      L_okif=L_olif
C FDA20_SP2_UVR.fgi( 252, 608):������,20FDA20TRAN05_NEXT
      L_ikif=L_olif
C FDA20_SP2_UVR.fgi( 252, 606):������,20FDA20TRAN06_NEXT
      L_ekif=L_olif
C FDA20_SP2_UVR.fgi( 252, 604):������,20FDA20TRAN07_NEXT
      L_akif=L_olif
C FDA20_SP2_UVR.fgi( 252, 602):������,20FDA20TRAN08_NEXT
      L_ufif = L_olif.OR.L_(96)
C FDA20_SP2_UVR.fgi( 238, 597):���
      if(L_akof.and..not.L0_ufof) then
         R0_ifof=R0_ofof
      else
         R0_ifof=max(R_(167)-deltat,0.0)
      endif
      L_(281)=R0_ifof.gt.0.0
      L0_ufof=L_akof
C FDA20_SP2_UVR.fgi( 174, 659):������������  �� T
      L_otad = L_(281).OR.L_(183).OR.L_(231).OR.L_(120)
C FDA20_SP2_UVR.fgi( 406, 410):���
      L_(276) = L_akof.AND.L_obof
C FDA20_SP2_UVR.fgi( 160, 647):�
      if(L_(276).and..not.L0_ibof) then
         R0_abof=R0_ebof
      else
         R0_abof=max(R_(164)-deltat,0.0)
      endif
      L_(275)=R0_abof.gt.0.0
      L0_ibof=L_(276)
C FDA20_SP2_UVR.fgi( 170, 647):������������  �� T
      L_atad = L_(275).OR.L_(117).OR.L_(231).OR.L_(120)
C FDA20_SP2_UVR.fgi( 406, 386):���
      L_(274) = L_akof.AND.L_uxif
C FDA20_SP2_UVR.fgi( 160, 643):�
      if(L_(274).and..not.L0_oxif) then
         R0_exif=R0_ixif
      else
         R0_exif=max(R_(163)-deltat,0.0)
      endif
      L_(273)=R0_exif.gt.0.0
      L0_oxif=L_(274)
C FDA20_SP2_UVR.fgi( 174, 643):������������  �� T
      L_usad = L_(116).OR.L_(273).OR.L_(231).OR.L_(120)
C FDA20_SP2_UVR.fgi( 406, 378):���
      L_(264) = L_akof.AND.L_urif
C FDA20_SP2_UVR.fgi( 160, 623):�
      if(L_(264).and..not.L0_orif) then
         R0_erif=R0_irif
      else
         R0_erif=max(R_(158)-deltat,0.0)
      endif
      L_(263)=R0_erif.gt.0.0
      L0_orif=L_(264)
C FDA20_SP2_UVR.fgi( 170, 623):������������  �� T
      L_(266) = L_akof.AND.L_osif
C FDA20_SP2_UVR.fgi( 160, 627):�
      if(L_(266).and..not.L0_isif) then
         R0_asif=R0_esif
      else
         R0_asif=max(R_(159)-deltat,0.0)
      endif
      L_(265)=R0_asif.gt.0.0
      L0_isif=L_(266)
C FDA20_SP2_UVR.fgi( 174, 627):������������  �� T
      L_asad = L_(265).OR.L_(231).OR.L_(231).OR.L_(120)
C FDA20_SP2_UVR.fgi( 406, 346):���
      L_(268) = L_akof.AND.L_itif
C FDA20_SP2_UVR.fgi( 160, 631):�
      if(L_(268).and..not.L0_etif) then
         R0_usif=R0_atif
      else
         R0_usif=max(R_(160)-deltat,0.0)
      endif
      L_(267)=R0_usif.gt.0.0
      L0_etif=L_(268)
C FDA20_SP2_UVR.fgi( 170, 631):������������  �� T
      L_esad = L_(267).OR.L_(231).OR.L_(231).OR.L_(120)
C FDA20_SP2_UVR.fgi( 406, 354):���
      L_(270) = L_akof.AND.L_evif
C FDA20_SP2_UVR.fgi( 160, 635):�
      if(L_(270).and..not.L0_avif) then
         R0_otif=R0_utif
      else
         R0_otif=max(R_(161)-deltat,0.0)
      endif
      L_(269)=R0_otif.gt.0.0
      L0_avif=L_(270)
C FDA20_SP2_UVR.fgi( 174, 635):������������  �� T
      L_isad = L_(269).OR.L_(231).OR.L_(231).OR.L_(120)
C FDA20_SP2_UVR.fgi( 406, 362):���
      L_(272) = L_akof.AND.L_axif
C FDA20_SP2_UVR.fgi( 160, 639):�
      if(L_(272).and..not.L0_uvif) then
         R0_ivif=R0_ovif
      else
         R0_ivif=max(R_(162)-deltat,0.0)
      endif
      L_(271)=R0_ivif.gt.0.0
      L0_uvif=L_(272)
C FDA20_SP2_UVR.fgi( 170, 639):������������  �� T
      L_osad = L_(271).OR.L_(231).OR.L_(231).OR.L_(120)
C FDA20_SP2_UVR.fgi( 406, 370):���
      L_(278) = L_akof.AND.L_idof
C FDA20_SP2_UVR.fgi( 160, 651):�
      if(L_(278).and..not.L0_edof) then
         R0_ubof=R0_adof
      else
         R0_ubof=max(R_(165)-deltat,0.0)
      endif
      L_(277)=R0_ubof.gt.0.0
      L0_edof=L_(278)
C FDA20_SP2_UVR.fgi( 174, 651):������������  �� T
      L_etad = L_(277).OR.L_(118).OR.L_(231).OR.L_(120)
C FDA20_SP2_UVR.fgi( 406, 394):���
      L_(280) = L_akof.AND.L_efof
C FDA20_SP2_UVR.fgi( 160, 655):�
      if(L_(280).and..not.L0_afof) then
         R0_odof=R0_udof
      else
         R0_odof=max(R_(166)-deltat,0.0)
      endif
      L_(279)=R0_odof.gt.0.0
      L0_afof=L_(280)
C FDA20_SP2_UVR.fgi( 170, 655):������������  �� T
      L_itad = L_(279).OR.L_(119).OR.L_(231).OR.L_(120)
C FDA20_SP2_UVR.fgi( 406, 402):���
      if(L_arif.and..not.L0_ixef) then
         R0_axef=R0_exef
      else
         R0_axef=max(R_(155)-deltat,0.0)
      endif
      L_(253)=R0_axef.gt.0.0
      L0_ixef=L_arif
C FDA20_SP2_UVR.fgi( 241, 738):������������  �� T
      if(L_ikof.and..not.L0_uve) then
         R0_ive=R0_ove
      else
         R0_ive=max(R_(36)-deltat,0.0)
      endif
      L_(225)=R0_ive.gt.0.0
      L0_uve=L_ikof
C FDA20_SP2_UVR.fgi( 174, 717):������������  �� T
      L_arod = L_(225).OR.L_(224).OR.L_(231)
C FDA20_SP2_UVR.fgi( 404, 765):���
      L_(32) = L_ikof.AND.L_ired
C FDA20_SP2_UVR.fgi( 160, 705):�
      if(L_(32).and..not.L0_ate) then
         R0_ose=R0_use
      else
         R0_ose=max(R_(33)-deltat,0.0)
      endif
      L_(219)=R0_ose.gt.0.0
      L0_ate=L_(32)
C FDA20_SP2_UVR.fgi( 170, 705):������������  �� T
      L_ipod = L_(219).OR.L_(218).OR.L_(231)
C FDA20_SP2_UVR.fgi( 404, 747):���
      L_(31) = L_ikof.AND.L_oped
C FDA20_SP2_UVR.fgi( 160, 701):�
      if(L_(31).and..not.L0_ise) then
         R0_ase=R0_ese
      else
         R0_ase=max(R_(32)-deltat,0.0)
      endif
      L_(217)=R0_ase.gt.0.0
      L0_ise=L_(31)
C FDA20_SP2_UVR.fgi( 174, 701):������������  �� T
      L_epod = L_(217).OR.L_(216).OR.L_(215)
C FDA20_SP2_UVR.fgi( 404, 741):���
      L_(26) = L_ikof.AND.L_alod
C FDA20_SP2_UVR.fgi( 160, 681):�
      if(L_(26).and..not.L0_ime) then
         R0_ame=R0_eme
      else
         R0_ame=max(R_(27)-deltat,0.0)
      endif
      L_(202)=R0_ame.gt.0.0
      L0_ime=L_(26)
C FDA20_SP2_UVR.fgi( 170, 681):������������  �� T
      L_emod = L_(202).OR.L_(201).OR.L_(200).OR.L_(199)
C FDA20_SP2_UVR.fgi( 404, 710):���
      L_(27) = L_ikof.AND.L_elod
C FDA20_SP2_UVR.fgi( 160, 685):�
      if(L_(27).and..not.L0_ape) then
         R0_ome=R0_ume
      else
         R0_ome=max(R_(28)-deltat,0.0)
      endif
      L_(205)=R0_ome.gt.0.0
      L0_ape=L_(27)
C FDA20_SP2_UVR.fgi( 174, 685):������������  �� T
      L_imod = L_(205).OR.L_(204).OR.L_(203)
C FDA20_SP2_UVR.fgi( 404, 717):���
      L_(28) = L_ikof.AND.L_ilod
C FDA20_SP2_UVR.fgi( 160, 689):�
      if(L_(28).and..not.L0_ope) then
         R0_epe=R0_ipe
      else
         R0_epe=max(R_(29)-deltat,0.0)
      endif
      L_(208)=R0_epe.gt.0.0
      L0_ope=L_(28)
C FDA20_SP2_UVR.fgi( 170, 689):������������  �� T
      L_omod = L_(208).OR.L_(207).OR.L_(206)
C FDA20_SP2_UVR.fgi( 404, 723):���
      L_(29) = L_ikof.AND.L_olod
C FDA20_SP2_UVR.fgi( 160, 693):�
      if(L_(29).and..not.L0_ere) then
         R0_upe=R0_are
      else
         R0_upe=max(R_(30)-deltat,0.0)
      endif
      L_(211)=R0_upe.gt.0.0
      L0_ere=L_(29)
C FDA20_SP2_UVR.fgi( 174, 693):������������  �� T
      L_umod = L_(211).OR.L_(210).OR.L_(209)
C FDA20_SP2_UVR.fgi( 404, 729):���
      L_(30) = L_ikof.AND.L_ulod
C FDA20_SP2_UVR.fgi( 160, 697):�
      if(L_(30).and..not.L0_ure) then
         R0_ire=R0_ore
      else
         R0_ire=max(R_(31)-deltat,0.0)
      endif
      L_(214)=R0_ire.gt.0.0
      L0_ure=L_(30)
C FDA20_SP2_UVR.fgi( 170, 697):������������  �� T
      L_apod = L_(214).OR.L_(213).OR.L_(212)
C FDA20_SP2_UVR.fgi( 404, 735):���
      L_(33) = L_ikof.AND.L_esed
C FDA20_SP2_UVR.fgi( 160, 709):�
      if(L_(33).and..not.L0_ote) then
         R0_ete=R0_ite
      else
         R0_ete=max(R_(34)-deltat,0.0)
      endif
      L_(221)=R0_ete.gt.0.0
      L0_ote=L_(33)
C FDA20_SP2_UVR.fgi( 174, 709):������������  �� T
      L_opod = L_(221).OR.L_(220).OR.L_(231)
C FDA20_SP2_UVR.fgi( 404, 753):���
      L_(34) = L_ikof.AND.L_ated
C FDA20_SP2_UVR.fgi( 160, 713):�
      if(L_(34).and..not.L0_eve) then
         R0_ute=R0_ave
      else
         R0_ute=max(R_(35)-deltat,0.0)
      endif
      L_(223)=R0_ute.gt.0.0
      L0_eve=L_(34)
C FDA20_SP2_UVR.fgi( 170, 713):������������  �� T
      L_upod = L_(223).OR.L_(222).OR.L_(231)
C FDA20_SP2_UVR.fgi( 404, 759):���
      L_(256) = L_amuf.AND.(.NOT.L_(252))
C FDA20_SP2_UVR.fgi( 152, 744):�
      if(L_(256).and..not.L0_abif) then
         R0_oxef=R0_uxef
      else
         R0_oxef=max(R_(156)-deltat,0.0)
      endif
      L_(255)=R0_oxef.gt.0.0
      L0_abif=L_(256)
C FDA20_SP2_UVR.fgi( 241, 744):������������  �� T
      L_(254) = L_(255).OR.L_(253)
C FDA20_SP2_UVR.fgi( 251, 743):���
      L_(150) = L_(254).OR.L_(231).OR.L_(231)
C FDA20_SP2_UVR.fgi( 400, 699):���
      L_odid = L_(152).OR.L_(151).OR.L_(231).OR.L_(150)
C FDA20_SP2_UVR.fgi( 406, 690):���
      L_idid = L_(149).OR.L_(151).OR.L_(231).OR.L_(150)
C FDA20_SP2_UVR.fgi( 406, 682):���
      L_edid = L_(148).OR.L_(151).OR.L_(231).OR.L_(150)
C FDA20_SP2_UVR.fgi( 406, 674):���
      L_adid = L_(147).OR.L_(151).OR.L_(231).OR.L_(150)
C FDA20_SP2_UVR.fgi( 406, 666):���
      L_ubid = L_(231).OR.L_(146).OR.L_(231).OR.L_(150)
C FDA20_SP2_UVR.fgi( 406, 658):���
      L_obid = L_(145).OR.L_(151).OR.L_(231).OR.L_(150)
C FDA20_SP2_UVR.fgi( 406, 650):���
      L_ibid = L_(144).OR.L_(151).OR.L_(231).OR.L_(150)
C FDA20_SP2_UVR.fgi( 406, 642):���
      L_ebid = L_(143).OR.L_(151).OR.L_(231).OR.L_(150)
C FDA20_SP2_UVR.fgi( 406, 634):���
      L_abid = L_(142).OR.L_(151).OR.L_(231).OR.L_(150)
C FDA20_SP2_UVR.fgi( 406, 626):���
      if(.not.L_amuf) then
         R0_itef=0.0
      elseif(.not.L0_otef) then
         R0_itef=R0_etef
      else
         R0_itef=max(R_(153)-deltat,0.0)
      endif
      L_(285)=L_amuf.and.R0_itef.le.0.0
      L0_otef=L_amuf
C FDA20_SP2_UVR.fgi( 162, 740):�������� ��������� ������
      if(L_(285).and..not.L0_ufuf) then
         R0_ifuf=R0_ofuf
      else
         R0_ifuf=max(R_(168)-deltat,0.0)
      endif
      L_uluf=R0_ifuf.gt.0.0
      L0_ufuf=L_(285)
C FDA20_SP2_UVR.fgi( 174, 740):������������  �� T
      L_oluf=L_uluf
C FDA20_SP2_UVR.fgi( 194, 740):������,20FDA20TRAN01_PREV
      L_iluf=L_uluf
C FDA20_SP2_UVR.fgi( 194, 738):������,20FDA20TRAN02_PREV
      L_eluf=L_uluf
C FDA20_SP2_UVR.fgi( 194, 736):������,20FDA20TRAN03_PREV
      L_aluf=L_uluf
C FDA20_SP2_UVR.fgi( 194, 734):������,20FDA20TRAN04_PREV
      L_ukuf=L_uluf
C FDA20_SP2_UVR.fgi( 194, 732):������,20FDA20TRAN05_PREV
      L_okuf=L_uluf
C FDA20_SP2_UVR.fgi( 194, 730):������,20FDA20TRAN06_PREV
      L_ikuf=L_uluf
C FDA20_SP2_UVR.fgi( 194, 728):������,20FDA20TRAN07_PREV
      L_ekuf=L_uluf
C FDA20_SP2_UVR.fgi( 194, 726):������,20FDA20TRAN08_PREV
      L_akuf = L_uluf.OR.L_(92)
C FDA20_SP2_UVR.fgi( 240, 721):���
      L_(122) = L_emuf.OR.L_(231).OR.L_(231)
C FDA20_SP2_UVR.fgi( 400, 512):���
      L_oxad = L_(123).OR.L_(231).OR.L_(231).OR.L_(122)
C FDA20_SP2_UVR.fgi( 406, 503):���
      L_ixad = L_(123).OR.L_(231).OR.L_(231).OR.L_(122)
C FDA20_SP2_UVR.fgi( 406, 495):���
      L_exad = L_(123).OR.L_(231).OR.L_(231).OR.L_(122)
C FDA20_SP2_UVR.fgi( 406, 487):���
      L_axad = L_(123).OR.L_(231).OR.L_(231).OR.L_(122)
C FDA20_SP2_UVR.fgi( 406, 479):���
      L_uvad = L_(231).OR.L_(231).OR.L_(231).OR.L_(122)
C FDA20_SP2_UVR.fgi( 406, 471):���
      L_ovad = L_(123).OR.L_(231).OR.L_(231).OR.L_(122)
C FDA20_SP2_UVR.fgi( 406, 463):���
      L_ivad = L_(123).OR.L_(231).OR.L_(231).OR.L_(122)
C FDA20_SP2_UVR.fgi( 406, 455):���
      L_evad = L_(123).OR.L_(231).OR.L_(231).OR.L_(122)
C FDA20_SP2_UVR.fgi( 406, 447):���
      L_avad = L_(123).OR.L_(231).OR.L_(231).OR.L_(122)
C FDA20_SP2_UVR.fgi( 406, 439):���
      L_utad = L_(121).OR.L_(123).OR.L_(231).OR.L_(122)
C FDA20_SP2_UVR.fgi( 406, 431):���
      if(L_(106).and..not.L0_okad) then
         R0_ekad=R0_ikad
      else
         R0_ekad=max(R_(79)-deltat,0.0)
      endif
      L_omad=R0_ekad.gt.0.0
      L0_okad=L_(106)
C FDA20_SP2_UVR.fgi( 150, 227):������������  �� T
      L_amur=L_upife
C FDA20_lamp.fgi( 154, 858):������,20FDA23AB002YV11
      !{
      Call DAT_DISCR_HANDLER(deltat,I_alur,L_olur,R_ilur,
     & REAL(R_ulur,4),L_amur,L_ukur,I_elur)
      !}
C FDA20_lamp.fgi( 152, 844):���������� ������� ���������,20FDA23AB002QH01
      L_idur=L_arife
C FDA20_lamp.fgi( 154, 854):������,20FDA23AB002YV10
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ibur,L_adur,R_ubur,
     & REAL(R_edur,4),L_idur,L_ebur,I_obur)
      !}
C FDA20_lamp.fgi( 152, 830):���������� ������� ���������,20FDA23AB002QH01_1
      if(.not.L_oku) then
         R0_ofu=0.0
      elseif(.not.L0_ufu) then
         R0_ofu=R0_ifu
      else
         R0_ofu=max(R_(63)-deltat,0.0)
      endif
      L_(93)=L_oku.and.R0_ofu.le.0.0
      L0_ufu=L_oku
C FDA20_SP2_UVR.fgi(  45,  37):�������� ��������� ������
C label 3438  try3438=try3438-1
      L_(95) = L_irad.AND.L_(94).AND.L_(93)
C FDA20_SP2_UVR.fgi(  56,  42):�
      if(L_aruf) then
          if (L_(81)) then
              I_iruf = 32
              L_oku = .true.
              L_(82) = .false.
          endif
          if (L_(95)) then
              L_(82) = .true.
              L_oku = .false.
          endif
          if (I_iruf.ne.32) then
              L_oku = .false.
              L_(82) = .false.
          endif
      else
          L_(82) = .false.
      endif
C FDA20_SP2_UVR.fgi(  93,  42):��� ������� ���������,uvr
C sav1=L_(93)
      if(.not.L_oku) then
         R0_ofu=0.0
      elseif(.not.L0_ufu) then
         R0_ofu=R0_ifu
      else
         R0_ofu=max(R_(63)-deltat,0.0)
      endif
      L_(93)=L_oku.and.R0_ofu.le.0.0
      L0_ufu=L_oku
C FDA20_SP2_UVR.fgi(  45,  37):recalc:�������� ��������� ������
C if(sav1.ne.L_(93) .and. try3438.gt.0) goto 3438
      if(L_aruf) then
          if (L_(82)) then
              I_iruf = 33
              L_efu = .true.
              L_(284) = .false.
          endif
          if (L_amif) then
              L_(284) = .true.
              L_efu = .false.
          endif
          if (I_iruf.ne.33) then
              L_efu = .false.
              L_(284) = .false.
          endif
      else
          L_(284) = .false.
      endif
C FDA20_SP2_UVR.fgi(  93,  22):��� ������� ���������,uvr
      if(L_(284)) then
         I_iruf=0
         L_aruf=.false.
      endif
C FDA20_SP2_UVR.fgi(  93,  11):����� ������� ���������,uvr
      if(L_efu.and..not.L0_afu) then
         R0_odu=R0_udu
      else
         R0_odu=max(R_(62)-deltat,0.0)
      endif
      L_(86)=R0_odu.gt.0.0
      L0_afu=L_efu
C FDA20_SP2_UVR.fgi( 160,  22):������������  �� T
      L_(140) = L_avu.OR.L_(88).OR.L_(87).OR.L_(86)
C FDA20_SP2_UVR.fgi( 175, 101):���
      L_uxed = L_(141).OR.L_(151).OR.L_(140).OR.L_(150)
C FDA20_SP2_UVR.fgi( 406, 618):���
      if(L_oku.and..not.L0_iku) then
         R0_aku=R0_eku
      else
         R0_aku=max(R_(64)-deltat,0.0)
      endif
      L_(115)=R0_aku.gt.0.0
      L0_iku=L_oku
C FDA20_SP2_UVR.fgi( 160,  42):������������  �� T
      L_urad = L_(115).OR.L_(263).OR.L_(231).OR.L_(120)
C FDA20_SP2_UVR.fgi( 406, 338):���
      End
