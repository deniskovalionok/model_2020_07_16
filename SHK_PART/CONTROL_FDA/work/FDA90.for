      Subroutine FDA90(ext_deltat)
      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      Include 'FDA90.fh'

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      Call FDA90_ConIn
      R_(15)=R8_efif
C FDA90_vent_log.fgi( 186, 268):pre: �������������� �����  
      R_(23)=R8_elif
C FDA90_vent_log.fgi(  46, 268):pre: �������������� �����  
      R_(11)=R8_edif
C FDA90_vent_log.fgi( 255, 268):pre: �������������� �����  
      R_(19)=R8_ekif
C FDA90_vent_log.fgi( 115, 268):pre: �������������� �����  
      R_(266)=R0_oxabe
C FDA90_logic.fgi( 164, 959):pre: ������������  �� T
      R_(255)=R0_opabe
C FDA90_logic.fgi( 161, 841):pre: ������������  �� T
      R_(253)=R0_umabe
C FDA90_logic.fgi( 161, 822):pre: ������������  �� T
      R_(245)=R0_udabe
C FDA90_logic.fgi( 161, 735):pre: ������������  �� T
      R_(206)=R0_opox
C FDA90_logic.fgi( 347, 723):pre: ������������  �� T
      R_(189)=R0_avix
C FDA90_logic.fgi( 347, 626):pre: ������������  �� T
      R_(183)=R0_upix
C FDA90_logic.fgi( 347, 551):pre: ������������  �� T
      R_(173)=R0_afix
C FDA90_logic.fgi( 347, 504):pre: ������������  �� T
      R_(107)=R0_apuv
C FDA90_logic.fgi( 534, 911):pre: ������������  �� T
      R_(145)=R0_oxax
C FDA90_logic.fgi( 534, 763):pre: ������������  �� T
      R_(141)=R0_utax
C FDA90_logic.fgi( 534, 735):pre: ������������  �� T
      R_(135)=R0_arax
C FDA90_logic.fgi( 534, 653):pre: ������������  �� T
      R_(127)=R0_akax
C FDA90_logic.fgi( 534, 570):pre: ������������  �� T
      R_(71)=R0_ibov
C FDA90_logic.fgi( 336,1495):pre: ������������  �� T
      R_(61)=R0_oriv
C FDA90_logic.fgi( 336,1406):pre: ������������  �� T
      R_(60)=R0_upiv
C FDA90_logic.fgi( 336,1390):pre: ������������  �� T
      R_(50)=R0_ufiv
C FDA90_logic.fgi( 336,1309):pre: ������������  �� T
      R_(28)=R0_olev
C FDA90_logic.fgi( 336,1156):pre: ������������  �� T
      R_(335)=R0_elube
C FDA90_logic.fgi( 531,1315):pre: ������������  �� T
      R_(337)=R0_emube
C FDA90_logic.fgi( 531,1301):pre: ������������  �� T
      R_(291)=R0_olibe
C FDA90_logic.fgi( 161,1302):pre: ������������  �� T
      R_(285)=R0_idibe
C FDA90_logic.fgi( 161,1288):pre: ������������  �� T
      R_(319)=R0_irobe
C FDA90_logic.fgi( 531,1226):pre: ������������  �� T
      R_(276)=R0_usebe
C FDA90_logic.fgi( 161,1214):pre: ������������  �� T
      R_(77)=R0_omov
C FDA90_logic.fgi( 161,1199):pre: ������������  �� T
      R_(151)=R0_ifex
C FDA90_logic.fgi( 534, 836):pre: ������������  �� T
      R_(193)=R0_abox
C FDA90_logic.fgi( 347, 675):pre: ������������  �� T
      R_(211)=R0_osox
C FDA90_logic.fgi( 347, 839):pre: ������������  �� T
      R_(293)=R0_imibe
C FDA90_logic.fgi( 161,1375):pre: ������������  �� T
      R_(332)=R0_afube
C FDA90_logic.fgi( 531,1389):pre: ������������  �� T
      R_(48)=R0_afiv
C FDA90_logic.fgi( 336,1292):pre: ������������  �� T
      R_(65)=R0_otiv
C FDA90_logic.fgi( 336,1438):pre: ������������  �� T
      R_(79)=R0_asov
C FDA90_logic.fgi( 531,1212):pre: ������������  �� T
      R_(125)=R0_efax
C FDA90_logic.fgi( 534, 553):pre: ������������  �� T
      R_(139)=R0_atax
C FDA90_logic.fgi( 534, 719):pre: ������������  �� T
      R_(178)=R0_ilix
C FDA90_logic.fgi( 347, 535):pre: ������������  �� T
      R_(243)=R0_adabe
C FDA90_logic.fgi( 161, 718):pre: ������������  �� T
      R_(259)=R0_osabe
C FDA90_logic.fgi( 161, 877):pre: ������������  �� T
      R_(289)=R0_ukibe
C FDA90_logic.fgi( 161,1329):pre: ������������  �� T
      R_(330)=R0_edube
C FDA90_logic.fgi( 531,1342):pre: ������������  �� T
      R_(55)=R0_amiv
C FDA90_logic.fgi( 336,1357):pre: ������������  �� T
      R_(67)=R0_oviv
C FDA90_logic.fgi( 336,1512):pre: ������������  �� T
      R_(132)=R0_emax
C FDA90_logic.fgi( 534, 618):pre: ������������  �� T
      R_(153)=R0_ikex
C FDA90_logic.fgi( 534, 853):pre: ������������  �� T
      R_(195)=R0_edox
C FDA90_logic.fgi( 347, 690):pre: ������������  �� T
      R_(250)=R0_alabe
C FDA90_logic.fgi( 161, 783):pre: ������������  �� T
      R_(264)=R0_uvabe
C FDA90_logic.fgi( 161, 938):pre: ������������  �� T
      R_(281)=R0_ixebe
C FDA90_logic.fgi( 161,1257):pre: ������������  �� T
      R_(295)=R0_upibe
C FDA90_logic.fgi( 161,1393):pre: ������������  �� T
      R_(324)=R0_avobe
C FDA90_logic.fgi( 531,1274):pre: ������������  �� T
      R_(343)=R0_irube
C FDA90_logic.fgi( 531,1426):pre: ������������  �� T
      R_(53)=R0_ukiv
C FDA90_logic.fgi( 336,1325):pre: ������������  �� T
      R_(130)=R0_alax
C FDA90_logic.fgi( 534, 586):pre: ������������  �� T
      R_(203)=R0_ulox
C FDA90_logic.fgi( 347, 737):pre: ������������  �� T
      R_(248)=R0_ufabe
C FDA90_logic.fgi( 161, 751):pre: ������������  �� T
      R_(288)=R0_ufibe
C FDA90_logic.fgi( 161,1316):pre: ������������  �� T
      R_(306)=R0_ixibe
C FDA90_logic.fgi( 161,1429):pre: ������������  �� T
      R_(329)=R0_ebube
C FDA90_logic.fgi( 531,1329):pre: ������������  �� T
      R_(45)=R0_abiv
C FDA90_logic.fgi( 336,1258):pre: ������������  �� T
      R_(58)=R0_apiv
C FDA90_logic.fgi( 336,1375):pre: ������������  �� T
      R_(86)=R0_evov
C FDA90_logic.fgi( 532,1484):pre: ������������  �� T
      R_(134)=R0_epax
C FDA90_logic.fgi( 534, 636):pre: ������������  �� T
      R_(122)=R0_ebax
C FDA90_logic.fgi( 534, 519):pre: ������������  �� T
      R_(252)=R0_amabe
C FDA90_logic.fgi( 161, 801):pre: ������������  �� T
      R_(209)=R0_urox
C FDA90_logic.fgi( 347, 821):pre: ������������  �� T
      R_(218)=R0_exox
C FDA90_logic.fgi( 348, 930):pre: ������������  �� T
      R_(240)=R0_axux
C FDA90_logic.fgi( 161, 684):pre: ������������  �� T
      R_(301)=R0_usibe
C FDA90_logic.fgi( 161,1485):pre: ������������  �� T
      R_(303)=R0_ivibe
C FDA90_logic.fgi( 161,1361):pre: ������������  �� T
      R_(34)=R0_irev
C FDA90_logic.fgi( 336,1208):pre: ������������  �� T
      R_(112)=R0_oruv
C FDA90_logic.fgi( 534, 469):pre: ������������  �� T
      R_(214)=R0_avox
C FDA90_logic.fgi( 347, 870):pre: ������������  �� T
      R_(229)=R0_epux
C FDA90_logic.fgi( 161, 634):pre: ������������  �� T
      R_(297)=R0_oribe
C FDA90_logic.fgi( 161,1410):pre: ������������  �� T
      R_(47)=R0_ubiv
C FDA90_logic.fgi( 336,1274):pre: ������������  �� T
      R_(124)=R0_adax
C FDA90_logic.fgi( 534, 535):pre: ������������  �� T
      R_(219)=R0_abux
C FDA90_logic.fgi( 347, 909):pre: ������������  �� T
      R_(242)=R0_uxux
C FDA90_logic.fgi( 161, 700):pre: ������������  �� T
      R_(308)=R0_ebobe
C FDA90_logic.fgi( 161,1464):pre: ������������  �� T
      R_(78)=R0_erov
C FDA90_logic.fgi( 531,1461):pre: ������������  �� T
      R_(29)=R0_umev
C FDA90_logic.fgi( 336,1127):pre: ������������  �� T
      R_(39)=R0_utev
C FDA90_logic.fgi( 336,1169):pre: ������������  �� T
      R_(89)=R0_abuv
C FDA90_logic.fgi( 531,1538):pre: ������������  �� T
      R_(116)=R0_avuv
C FDA90_logic.fgi( 534, 430):pre: ������������  �� T
      R_(223)=R0_afux
C FDA90_logic.fgi( 347, 989):pre: ������������  �� T
      R_(234)=R0_usux
C FDA90_logic.fgi( 161, 595):pre: ������������  �� T
      R_(310)=R0_odobe
C FDA90_logic.fgi( 161,1539):pre: ������������  �� T
      R_(38)=R0_atev
C FDA90_logic.fgi( 336,1228):pre: ������������  �� T
      R_(40)=R0_ivev
C FDA90_logic.fgi( 336,1143):pre: ������������  �� T
      R_(73)=R0_idov
C FDA90_logic.fgi( 336,1527):pre: ������������  �� T
      R_(64)=R0_osiv
C FDA90_logic.fgi( 336,1422):pre: ������������  �� T
      R_(279)=R0_avebe
C FDA90_logic.fgi( 161,1228):pre: ������������  �� T
      R_(69)=R0_oxiv
C FDA90_logic.fgi( 336,1476):pre: ������������  �� T
      R_(76)=R0_ufov
C FDA90_logic.fgi( 336,1562):pre: ������������  �� T
      R_(275)=R0_asebe
C FDA90_logic.fgi( 161,1180):pre: ������������  �� T
      R_(283)=R0_obibe
C FDA90_logic.fgi( 161,1277):pre: ������������  �� T
      R_(87)=R0_exov
C FDA90_logic.fgi( 531,1518):pre: ������������  �� T
      R_(90)=R0_obuv
C FDA90_logic.fgi( 531,1568):pre: ������������  �� T
      R_(115)=R0_etuv
C FDA90_logic.fgi( 534, 489):pre: ������������  �� T
      R_(117)=R0_ovuv
C FDA90_logic.fgi( 534, 415):pre: ������������  �� T
      R_(204)=R0_umox
C FDA90_logic.fgi( 347, 775):pre: ������������  �� T
      R_(144)=R0_uvax
C FDA90_logic.fgi( 534, 749):pre: ������������  �� T
      R_(186)=R0_asix
C FDA90_logic.fgi( 347, 565):pre: ������������  �� T
      R_(198)=R0_efox
C FDA90_logic.fgi( 347, 707):pre: ������������  �� T
      R_(138)=R0_asax
C FDA90_logic.fgi( 534, 703):pre: ������������  �� T
      R_(150)=R0_odex
C FDA90_logic.fgi( 534, 821):pre: ������������  �� T
      R_(180)=R0_omix
C FDA90_logic.fgi( 347, 596):pre: ������������  �� T
      R_(101)=R0_ukuv
C FDA90_logic.fgi( 534, 926):pre: ������������  �� T
      R_(27)=R0_elev
C FDA90_logic.fgi( 288, 410):pre: �������� ��������� ������
      R_(148)=R0_obex
C FDA90_logic.fgi( 534, 778):pre: ������������  �� T
      R_(159)=R0_omex
C FDA90_logic.fgi( 534, 960):pre: ������������  �� T
      R_(176)=R0_akix
C FDA90_logic.fgi( 347, 519):pre: ������������  �� T
      R_(106)=R0_emuv
C FDA90_logic.fgi( 534, 890):pre: ������������  �� T
      R_(163)=R0_orex
C FDA90_logic.fgi( 534,1011):pre: ������������  �� T
      R_(170)=R0_ebix
C FDA90_logic.fgi( 347, 473):pre: ������������  �� T
      R_(167)=R0_uvex
C FDA90_logic.fgi( 347, 433):pre: ������������  �� T
      R_(156)=R0_ilex
C FDA90_logic.fgi( 534, 940):pre: ������������  �� T
      R_(171)=R0_edix
C FDA90_logic.fgi( 347, 488):pre: ������������  �� T
      R_(160)=R0_opex
C FDA90_logic.fgi( 534, 992):pre: ������������  �� T
      R_(188)=R0_etix
C FDA90_logic.fgi( 347, 611):pre: ������������  �� T
      R_(99)=R0_ufuv
C FDA90_logic.fgi( 534, 805):pre: ������������  �� T
      R_(192)=R0_exix
C FDA90_logic.fgi( 347, 660):pre: ������������  �� T
      R_(201)=R0_okox
C FDA90_logic.fgi( 347, 789):pre: ������������  �� T
      R_(220)=R0_edux
C FDA90_logic.fgi( 347, 969):pre: ������������  �� T
      R_(224)=R0_ofux
C FDA90_logic.fgi( 347,1019):pre: ������������  �� T
      R_(233)=R0_asux
C FDA90_logic.fgi( 161, 654):pre: ������������  �� T
      R_(235)=R0_itux
C FDA90_logic.fgi( 161, 580):pre: ������������  �� T
      R_(258)=R0_orabe
C FDA90_logic.fgi( 161, 856):pre: ������������  �� T
      R_(269)=R0_obebe
C FDA90_logic.fgi( 161, 976):pre: ������������  �� T
      R_(322)=R0_usobe
C FDA90_logic.fgi( 531,1241):pre: ������������  �� T
      R_(262)=R0_utabe
C FDA90_logic.fgi( 161, 918):pre: ������������  �� T
      R_(272)=R0_udebe
C FDA90_logic.fgi( 161,1015):pre: ������������  �� T
      R_(318)=R0_opobe
C FDA90_logic.fgi( 531,1197):pre: ������������  �� T
      R_(326)=R0_exobe
C FDA90_logic.fgi( 531,1288):pre: ������������  �� T
      R_(302)=R0_utibe
C FDA90_logic.fgi( 161,1519):pre: ������������  �� T
      R_(311)=R0_efobe
C FDA90_logic.fgi( 161,1569):pre: ������������  �� T
      R_(340)=R0_epube
C FDA90_logic.fgi( 531,1375):pre: ������������  �� T
      Call TELEGKA_TRANSP_HANDLER(deltat,L_ifuk,L_(174),L_uluk
     &,L_(175),R_asuk,
     & R_esuk,R_apuk,R_omuk,L_oruk,R_epuk,R_umuk,
     & L_uruk,L_esok,L_isok,L_(176),L_iluk,R_emuk,R_imuk,
     & R_iruk,R_ikuk,R_eruk,L_aluk,L_ukuk,R_uvok,
     & REAL(R_ovok,4),R_iduk,REAL(R_uduk,4),L_aduk,
     & R_ubuk,REAL(R_eduk,4),L_exok,R_axok,
     & REAL(R_ixok,4),L_amuk,L_(179),L_(180),L_oduk,
     & L_akuk,L_ipuk,L_(178),L_(177),L_opuk,L_aruk,L_okuk
     &)
C FDA90_vlv.fgi( 140,  29):���������� ������� ������������,20FDA91AE018KE01
      Call PEREGRUZ_USTR_HANDLER(deltat,L_epof,L_(160),L_urof
     &,L_(161),R_uvof,
     & R_axof,R_usof,R_isof,L_ivof,R_atof,R_osof,
     & L_ovof,L_opof,L_(162),L_orof,R_asof,R_esof,
     & R_evof,R_upof,R_avof,L_irof,L_erof,R_afof,
     & REAL(60.0,4),R_emof,REAL(R_omof,4),R_alof,
     & REAL(R_ilof,4),L_ulof,R_olof,REAL(R_amof,4),
     & L_elof,L_apof,L_imof,L_ipof,L_etof,L_(164),L_(163)
     &,
     & L_itof,L_utof,L_arof)
C FDA90_vlv.fgi( 417, 146):���������� ������. ����.,FDA70_LIFT
      !{
      Call DAT_DISCR_HANDLER(deltat,I_i,L_ad,R_u,
     & REAL(R_ed,4),L_id,L_e,I_o)
      !}
C FDA90_lamp.fgi(  80,  62):���������� ������� ���������,20FDA91AE001QH09
      L_(4)=R_ibe.gt.R0_af
C FDA90_lamp.fgi( 383, 303):���������� >
      L_(5)=R_ibe.lt.R0_ef
C FDA90_lamp.fgi( 383, 311):���������� <
      L_(6) = L_(5).AND.L_(4)
C FDA90_lamp.fgi( 388, 310):�
      !{
      Call DAT_DISCR_HANDLER(deltat,I_of,L_ek,R_ak,
     & REAL(R_ik,4),L_(6),L_if,I_uf)
      !}
C FDA90_lamp.fgi( 406, 308):���������� ������� ���������,20FDA91AE013QH05
      L_(7)=R_ibe.gt.R0_al
C FDA90_lamp.fgi( 383, 267):���������� >
      L_(8)=R_ibe.lt.R0_el
C FDA90_lamp.fgi( 383, 275):���������� <
      L_(20) = L_(8).AND.L_(7)
C FDA90_lamp.fgi( 388, 274):�
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ax,L_ox,R_ix,
     & REAL(R_ux,4),L_(20),L_uv,I_ex)
      !}
C FDA90_lamp.fgi( 406, 272):���������� ������� ���������,20FDA91AE013QH06
      L_(9)=R_ibe.gt.R0_il
C FDA90_lamp.fgi( 383, 284):���������� >
      L_(10)=R_ibe.lt.R0_ol
C FDA90_lamp.fgi( 383, 292):���������� <
      L_(19) = L_(10).AND.L_(9)
C FDA90_lamp.fgi( 388, 291):�
      !{
      Call DAT_DISCR_HANDLER(deltat,I_it,L_av,R_ut,
     & REAL(R_ev,4),L_(19),L_et,I_ot)
      !}
C FDA90_lamp.fgi( 406, 290):���������� ������� ���������,20FDA91AE013QH03
      L_(11)=R_ibe.gt.R0_ul
C FDA90_lamp.fgi( 383, 319):���������� >
      L_(12)=R_ibe.lt.R0_am
C FDA90_lamp.fgi( 383, 327):���������� <
      L_(18) = L_(12).AND.L_(11)
C FDA90_lamp.fgi( 388, 326):�
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ur,L_is,R_es,
     & REAL(R_os,4),L_(18),L_or,I_as)
      !}
C FDA90_lamp.fgi( 406, 324):���������� ������� ���������,20FDA91AE013QH02
      L_(13)=R_ibe.gt.R0_em
C FDA90_lamp.fgi( 383, 334):���������� >
      L_(14)=R_ibe.lt.R0_im
C FDA90_lamp.fgi( 383, 342):���������� <
      L_(17) = L_(14).AND.L_(13)
C FDA90_lamp.fgi( 388, 341):�
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ep,L_up,R_op,
     & REAL(R_ar,4),L_(17),L_ap,I_ip)
      !}
C FDA90_lamp.fgi( 406, 340):���������� ������� ���������,20FDA91AE013QH04
      L_(15)=R_ibe.gt.R0_om
C FDA90_lamp.fgi( 383, 349):���������� >
      L_(16)=R_ibe.lt.R0_um
C FDA90_lamp.fgi( 383, 357):���������� <
      L_(21) = L_(16).AND.L_(15)
C FDA90_lamp.fgi( 388, 356):�
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ube,L_ide,R_ede,
     & REAL(R_ode,4),L_(21),L_obe,I_ade)
      !}
C FDA90_lamp.fgi( 406, 354):���������� ������� ���������,20FDA91AE013QH01
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ife,L_ake,R_ufe,
     & REAL(R_eke,4),L_ike,L_efe,I_ofe)
      !}
C FDA90_lamp.fgi( 514, 226):���������� ������� ���������,20FDA91AE001QH07
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ele,L_ule,R_ole,
     & REAL(R_ame,4),L_eme,L_ale,I_ile)
      !}
C FDA90_lamp.fgi( 514, 238):���������� ������� ���������,20FDA91AE001QH06
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ape,L_ope,R_ipe,
     & REAL(R_upe,4),L_are,L_ume,I_epe)
      !}
C FDA90_lamp.fgi( 534, 214):���������� ������� ���������,20FDA91AE001QH03
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ure,L_ise,R_ese,
     & REAL(R_ose,4),L_use,L_ore,I_ase)
      !}
C FDA90_lamp.fgi( 514, 214):���������� ������� ���������,20FDA91AE001QH02
      L_(22)=R_ako.gt.R0_odo
C FDA90_lamp.fgi( 127,  42):���������� >
      L_(28)=R_ako.gt.R0_udo
C FDA90_lamp.fgi( 127,  72):���������� >
      L_(24)=R_ako.lt.R0_afo
C FDA90_lamp.fgi( 127,  48):���������� <
      L_(23) = L_(24).AND.L_(22)
C FDA90_lamp.fgi( 133,  47):�
      L_(30)=R_ako.lt.R0_efo
C FDA90_lamp.fgi( 127,  56):���������� <
      L_(33)=R_apo.gt.R0_amo
C FDA90_lamp.fgi( 127,  91):���������� >
      L_(39)=R_apo.gt.R0_emo
C FDA90_lamp.fgi( 127, 121):���������� >
      L_(35)=R_apo.lt.R0_imo
C FDA90_lamp.fgi( 127,  97):���������� <
      L_(34) = L_(35).AND.L_(33)
C FDA90_lamp.fgi( 133,  96):�
      L_(40)=R_apo.lt.R0_omo
C FDA90_lamp.fgi( 127, 105):���������� <
      L_(44)=R_axo.gt.R0_eso
C FDA90_lamp.fgi( 127, 140):���������� >
      L_(50)=R_axo.gt.R0_iso
C FDA90_lamp.fgi( 127, 170):���������� >
      L_(45)=R_axo.lt.R0_oso
C FDA90_lamp.fgi( 127, 146):���������� <
      L_(49) = L_(45).AND.L_(44)
C FDA90_lamp.fgi( 133, 145):�
      L_(51)=R_axo.lt.R0_uso
C FDA90_lamp.fgi( 127, 154):���������� <
      !{
      Call LIMIT_SWITCH_HANDLER(deltat,L_amad,R_ulad,
     & REAL(R_emad,4),R_elad,REAL(R_olad,4),L_epad,
     & L_ilad,L_ipad,L_obox,I_okad)
      !}
C FDA90_lamp.fgi( 532, 322):���������� ��������� �����������,20FDA91AB003QH01
      !{
      Call LIMIT_SWITCH_HANDLER(deltat,L_esad,R_asad,
     & REAL(R_isad,4),R_irad,REAL(R_urad,4),L_itad,
     & L_orad,L_otad,L_afibe,I_upad)
      !}
C FDA90_lamp.fgi( 532, 282):���������� ��������� �����������,20FDA91AB005QH01
      !{
      Call LIMIT_SWITCH_HANDLER(deltat,L_ixad,R_exad,
     & REAL(R_oxad,4),R_ovad,REAL(R_axad,4),L_obed,
     & L_uvad,L_ubed,L_ovix,I_avad)
      !}
C FDA90_lamp.fgi( 532, 303):���������� ��������� �����������,20FDA91AB004QH01
      !{
      Call LIMIT_SWITCH_HANDLER(deltat,L_ofed,R_ifed,
     & REAL(R_ufed,4),R_uded,REAL(R_efed,4),L_uked,
     & L_afed,L_aled,L_ofube,I_eded)
      !}
C FDA90_lamp.fgi( 532, 358):���������� ��������� �����������,20FDA91AB001QH01
      !{
      Call LIMIT_SWITCH_HANDLER(deltat,L_umed,R_omed,
     & REAL(R_aped,4),R_amed,REAL(R_imed,4),L_ared,
     & L_emed,L_ered,L_umube,I_iled)
      !}
C FDA90_lamp.fgi( 532, 340):���������� ��������� �����������,20FDA91AB002QH01
      R_(1) = 100
C FDA90_vent_log.fgi( 170, 158):��������� (RE4) (�������)
      R_(2) = 100
C FDA90_vent_log.fgi( 170, 165):��������� (RE4) (�������)
      R_ikum = R8_oref
C FDA90_vent_log.fgi(  94, 164):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_efum,R_amum,REAL(1.0,4
     &),
     & REAL(R_ufum,4),REAL(R_akum,4),
     & REAL(R_afum,4),REAL(R_udum,4),I_ulum,
     & REAL(R_okum,4),L_ukum,REAL(R_alum,4),L_elum,L_ilum
     &,R_ekum,
     & REAL(R_ofum,4),REAL(R_ifum,4),L_olum,REAL(R_ikum,4
     &))
      !}
C FDA90_vlv.fgi( 292, 128):���������� �������,20FDA91CM002XQ01
      R_avep = R8_uref
C FDA90_vent_log.fgi(  94, 168):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_usep,R_oxep,REAL(1.0,4
     &),
     & REAL(R_itep,4),REAL(R_otep,4),
     & REAL(R_osep,4),REAL(R_isep,4),I_ixep,
     & REAL(R_evep,4),L_ivep,REAL(R_ovep,4),L_uvep,L_axep
     &,R_utep,
     & REAL(R_etep,4),REAL(R_atep,4),L_exep,REAL(R_avep,4
     &))
      !}
C FDA90_vlv.fgi( 198, 113):���������� �������,20FDA91CM001XQ01
      R_abum = R8_asef
C FDA90_vent_log.fgi(  38, 164):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_uvom,R_odum,REAL(1.0,4
     &),
     & REAL(R_ixom,4),REAL(R_oxom,4),
     & REAL(R_ovom,4),REAL(R_ivom,4),I_idum,
     & REAL(R_ebum,4),L_ibum,REAL(R_obum,4),L_ubum,L_adum
     &,R_uxom,
     & REAL(R_exom,4),REAL(R_axom,4),L_edum,REAL(R_abum,4
     &))
      !}
C FDA90_vlv.fgi( 198, 158):���������� �������,20FDA91CU002XQ01
      R_opep = R8_esef
C FDA90_vent_log.fgi(  38, 168):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_imep,R_esep,REAL(1.0,4
     &),
     & REAL(R_apep,4),REAL(R_epep,4),
     & REAL(R_emep,4),REAL(R_amep,4),I_asep,
     & REAL(R_upep,4),L_arep,REAL(R_erep,4),L_irep,L_orep
     &,R_ipep,
     & REAL(R_umep,4),REAL(R_omep,4),L_urep,REAL(R_opep,4
     &))
      !}
C FDA90_vlv.fgi( 229, 113):���������� �������,20FDA91CU001XQ01
      R_(4) = R8_isef + (-R8_itef)
C FDA90_vent_log.fgi( 340, 205):��������
      R_(3) = 1e-3
C FDA90_vent_log.fgi( 346, 202):��������� (RE4) (�������)
      R_olup = R_(4) * R_(3)
C FDA90_vent_log.fgi( 349, 204):����������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ikup,R_epup,REAL(1.0,4
     &),
     & REAL(R_alup,4),REAL(R_elup,4),
     & REAL(R_ekup,4),REAL(R_akup,4),I_apup,
     & REAL(R_ulup,4),L_amup,REAL(R_emup,4),L_imup,L_omup
     &,R_ilup,
     & REAL(R_ukup,4),REAL(R_okup,4),L_umup,REAL(R_olup,4
     &))
      !}
C FDA90_vlv.fgi( 291, 173):���������� �������,20FDA91CP006XQ01
      R_(6) = R8_osef + (-R8_itef)
C FDA90_vent_log.fgi( 340, 213):��������
      R_(5) = 1e-3
C FDA90_vent_log.fgi( 346, 210):��������� (RE4) (�������)
      R_ixup = R_(6) * R_(5)
C FDA90_vent_log.fgi( 349, 212):����������
      !{
      Call DAT_ANA_HANDLER(deltat,R_evup,R_adar,REAL(1.0,4
     &),
     & REAL(R_uvup,4),REAL(R_axup,4),
     & REAL(R_avup,4),REAL(R_utup,4),I_ubar,
     & REAL(R_oxup,4),L_uxup,REAL(R_abar,4),L_ebar,L_ibar
     &,R_exup,
     & REAL(R_ovup,4),REAL(R_ivup,4),L_obar,REAL(R_ixup,4
     &))
      !}
C FDA90_vlv.fgi( 229, 173):���������� �������,20FDA91CP005XQ01
      R_evum = R8_usef + (-R8_itef)
C FDA90_vent_log.fgi( 207, 194):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_atum,R_uxum,REAL(1.0,4
     &),
     & REAL(R_otum,4),REAL(R_utum,4),
     & REAL(R_usum,4),REAL(R_osum,4),I_oxum,
     & REAL(R_ivum,4),L_ovum,REAL(R_uvum,4),L_axum,L_exum
     &,R_avum,
     & REAL(R_itum,4),REAL(R_etum,4),L_ixum,REAL(R_evum,4
     &))
      !}
C FDA90_vlv.fgi( 229, 158):���������� �������,20FDA91CP051XQ01
      R_akop = R8_atef + (-R8_itef)
C FDA90_vent_log.fgi( 207, 200):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_udop,R_olop,REAL(1.0,4
     &),
     & REAL(R_ifop,4),REAL(R_ofop,4),
     & REAL(R_odop,4),REAL(R_idop,4),I_ilop,
     & REAL(R_ekop,4),L_ikop,REAL(R_okop,4),L_ukop,L_alop
     &,R_ufop,
     & REAL(R_efop,4),REAL(R_afop,4),L_elop,REAL(R_akop,4
     &))
      !}
C FDA90_vlv.fgi( 171, 143):���������� �������,20FDA91CP031XQ01
      R_osom = R8_etef + (-R8_itef)
C FDA90_vent_log.fgi( 207, 207):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_irom,R_evom,REAL(1.0,4
     &),
     & REAL(R_asom,4),REAL(R_esom,4),
     & REAL(R_erom,4),REAL(R_arom,4),I_avom,
     & REAL(R_usom,4),L_atom,REAL(R_etom,4),L_itom,L_otom
     &,R_isom,
     & REAL(R_urom,4),REAL(R_orom,4),L_utom,REAL(R_osom,4
     &))
      !}
C FDA90_vlv.fgi( 229, 143):���������� �������,20FDA91CP021XQ01
      R_utop = R8_otef + (-R8_itef)
C FDA90_vent_log.fgi( 207, 214):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_osop,R_ixop,REAL(1.0,4
     &),
     & REAL(R_etop,4),REAL(R_itop,4),
     & REAL(R_isop,4),REAL(R_esop,4),I_exop,
     & REAL(R_avop,4),L_evop,REAL(R_ivop,4),L_ovop,L_uvop
     &,R_otop,
     & REAL(R_atop,4),REAL(R_usop,4),L_axop,REAL(R_utop,4
     &))
      !}
C FDA90_vlv.fgi( 198, 143):���������� �������,20FDA91CP011XQ01
      R_(7) = 60000
C FDA90_vent_log.fgi( 277, 204):��������� (RE4) (�������)
      if(R8_utef.ge.0.0) then
         R_(8)=R8_avef/max(R8_utef,1.0e-10)
      else
         R_(8)=R8_avef/min(R8_utef,-1.0e-10)
      endif
C FDA90_vent_log.fgi( 267, 207):�������� ����������
      R_asup = R_(8) * R_(7)
C FDA90_vent_log.fgi( 280, 206):����������
      !{
      Call DAT_ANA_HANDLER(deltat,R_upup,R_otup,REAL(1.0,4
     &),
     & REAL(R_irup,4),REAL(R_orup,4),
     & REAL(R_opup,4),REAL(R_ipup,4),I_itup,
     & REAL(R_esup,4),L_isup,REAL(R_osup,4),L_usup,L_atup
     &,R_urup,
     & REAL(R_erup,4),REAL(R_arup,4),L_etup,REAL(R_asup,4
     &))
      !}
C FDA90_vlv.fgi( 260, 173):���������� �������,20FDA91CF006XQ01
      R_(9) = 60000
C FDA90_vent_log.fgi( 277, 212):��������� (RE4) (�������)
      if(R8_evef.ge.0.0) then
         R_(10)=R8_ivef/max(R8_evef,1.0e-10)
      else
         R_(10)=R8_ivef/min(R8_evef,-1.0e-10)
      endif
C FDA90_vent_log.fgi( 267, 215):�������� ����������
      R_ufar = R_(10) * R_(9)
C FDA90_vent_log.fgi( 280, 214):����������
      !{
      Call DAT_ANA_HANDLER(deltat,R_odar,R_ilar,REAL(1.0,4
     &),
     & REAL(R_efar,4),REAL(R_ifar,4),
     & REAL(R_idar,4),REAL(R_edar,4),I_elar,
     & REAL(R_akar,4),L_ekar,REAL(R_ikar,4),L_okar,L_ukar
     &,R_ofar,
     & REAL(R_afar,4),REAL(R_udar,4),L_alar,REAL(R_ufar,4
     &))
      !}
C FDA90_vlv.fgi( 199, 173):���������� �������,20FDA91CF005XQ01
      R_ekep = R8_ovef
C FDA90_vent_log.fgi( 150, 202):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_afep,R_ulep,REAL(1.0,4
     &),
     & REAL(R_ofep,4),REAL(R_ufep,4),
     & REAL(R_udep,4),REAL(R_odep,4),I_olep,
     & REAL(R_ikep,4),L_okep,REAL(R_ukep,4),L_alep,L_elep
     &,R_akep,
     & REAL(R_ifep,4),REAL(R_efep,4),L_ilep,REAL(R_ekep,4
     &))
      !}
C FDA90_vlv.fgi( 230,  98):���������� �������,20FDA91CF041XQ01
      R_amap = R8_uvef
C FDA90_vent_log.fgi( 150, 207):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ukap,R_opap,REAL(1.0,4
     &),
     & REAL(R_ilap,4),REAL(R_olap,4),
     & REAL(R_okap,4),REAL(R_ikap,4),I_ipap,
     & REAL(R_emap,4),L_imap,REAL(R_omap,4),L_umap,L_apap
     &,R_ulap,
     & REAL(R_elap,4),REAL(R_alap,4),L_epap,REAL(R_amap,4
     &))
      !}
C FDA90_vlv.fgi( 292,  98):���������� �������,20FDA91CF031XQ01
      R_udom = R8_axef
C FDA90_vent_log.fgi( 150, 212):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_obom,R_ikom,REAL(1.0,4
     &),
     & REAL(R_edom,4),REAL(R_idom,4),
     & REAL(R_ibom,4),REAL(R_ebom,4),I_ekom,
     & REAL(R_afom,4),L_efom,REAL(R_ifom,4),L_ofom,L_ufom
     &,R_odom,
     & REAL(R_adom,4),REAL(R_ubom,4),L_akom,REAL(R_udom,4
     &))
      !}
C FDA90_vlv.fgi( 291, 158):���������� �������,20FDA91CF021XQ01
      R_upum = R8_exef
C FDA90_vent_log.fgi(  93, 212):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_omum,R_isum,REAL(1.0,4
     &),
     & REAL(R_epum,4),REAL(R_ipum,4),
     & REAL(R_imum,4),REAL(R_emum,4),I_esum,
     & REAL(R_arum,4),L_erum,REAL(R_irum,4),L_orum,L_urum
     &,R_opum,
     & REAL(R_apum,4),REAL(R_umum,4),L_asum,REAL(R_upum,4
     &))
      !}
C FDA90_vlv.fgi( 260, 143):���������� �������,20FDA91CP002XQ01
      R_odap = R8_ixef
C FDA90_vent_log.fgi(  39, 197):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ibap,R_ekap,REAL(1.0,4
     &),
     & REAL(R_adap,4),REAL(R_edap,4),
     & REAL(R_ebap,4),REAL(R_abap,4),I_akap,
     & REAL(R_udap,4),L_afap,REAL(R_efap,4),L_ifap,L_ofap
     &,R_idap,
     & REAL(R_ubap,4),REAL(R_obap,4),L_ufap,REAL(R_odap,4
     &))
      !}
C FDA90_vlv.fgi( 292, 113):���������� �������,20FDA91CT051XQ01
      R_oxip = R8_oxef
C FDA90_vent_log.fgi(  39, 202):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ivip,R_edop,REAL(1.0,4
     &),
     & REAL(R_axip,4),REAL(R_exip,4),
     & REAL(R_evip,4),REAL(R_avip,4),I_adop,
     & REAL(R_uxip,4),L_abop,REAL(R_ebop,4),L_ibop,L_obop
     &,R_ixip,
     & REAL(R_uvip,4),REAL(R_ovip,4),L_ubop,REAL(R_oxip,4
     &))
      !}
C FDA90_vlv.fgi( 198, 128):���������� �������,20FDA91CT031XQ01
      R_uxap = R8_uxef
C FDA90_vent_log.fgi(  39, 207):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ovap,R_idep,REAL(1.0,4
     &),
     & REAL(R_exap,4),REAL(R_ixap,4),
     & REAL(R_ivap,4),REAL(R_evap,4),I_edep,
     & REAL(R_abep,4),L_ebep,REAL(R_ibep,4),L_obep,L_ubep
     &,R_oxap,
     & REAL(R_axap,4),REAL(R_uvap,4),L_adep,REAL(R_uxap,4
     &))
      !}
C FDA90_vlv.fgi( 260, 158):���������� �������,20FDA91CT022XQ01
      R_isap = R8_abif
C FDA90_vent_log.fgi(  39, 212):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_erap,R_avap,REAL(1.0,4
     &),
     & REAL(R_urap,4),REAL(R_asap,4),
     & REAL(R_arap,4),REAL(R_upap,4),I_utap,
     & REAL(R_osap,4),L_usap,REAL(R_atap,4),L_etap,L_itap
     &,R_esap,
     & REAL(R_orap,4),REAL(R_irap,4),L_otap,REAL(R_isap,4
     &))
      !}
C FDA90_vlv.fgi( 171, 113):���������� �������,20FDA91CT021XQ01
      R_esip = R8_ebif
C FDA90_vent_log.fgi( 150, 217):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_arip,R_utip,REAL(1000
     &,4),
     & REAL(R_orip,4),REAL(R_urip,4),
     & REAL(R_upip,4),REAL(R_opip,4),I_otip,
     & REAL(R_isip,4),L_osip,REAL(R_usip,4),L_atip,L_etip
     &,R_asip,
     & REAL(R_irip,4),REAL(R_erip,4),L_itip,REAL(R_esip,4
     &))
      !}
C FDA90_vlv.fgi( 229, 128):���������� �������,20FDA91CF011XQ01
      R_idip = R8_ibif
C FDA90_vent_log.fgi(  93, 217):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ebip,R_akip,REAL(1.0,4
     &),
     & REAL(R_ubip,4),REAL(R_adip,4),
     & REAL(R_abip,4),REAL(R_uxep,4),I_ufip,
     & REAL(R_odip,4),L_udip,REAL(R_afip,4),L_efip,L_ifip
     &,R_edip,
     & REAL(R_obip,4),REAL(R_ibip,4),L_ofip,REAL(R_idip,4
     &))
      !}
C FDA90_vlv.fgi( 171, 128):���������� �������,20FDA91CP001XQ01
      R_ipop = R8_obif
C FDA90_vent_log.fgi(  39, 217):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_emop,R_asop,REAL(1.0,4
     &),
     & REAL(R_umop,4),REAL(R_apop,4),
     & REAL(R_amop,4),REAL(R_ulop,4),I_urop,
     & REAL(R_opop,4),L_upop,REAL(R_arop,4),L_erop,L_irop
     &,R_epop,
     & REAL(R_omop,4),REAL(R_imop,4),L_orop,REAL(R_ipop,4
     &))
      !}
C FDA90_vlv.fgi( 291, 143):���������� �������,20FDA91CT011XQ01
      !��������� R_(14) = FDA90_vent_logC?? /0.0/
      R_(14)=R0_idif
C FDA90_vent_log.fgi( 243, 269):���������
      !��������� R_(13) = FDA90_vent_logC?? /0.001/
      R_(13)=R0_odif
C FDA90_vent_log.fgi( 243, 267):���������
      !��������� R_(18) = FDA90_vent_logC?? /0.0/
      R_(18)=R0_ifif
C FDA90_vent_log.fgi( 174, 269):���������
      !��������� R_(17) = FDA90_vent_logC?? /0.001/
      R_(17)=R0_ofif
C FDA90_vent_log.fgi( 174, 267):���������
      !��������� R_(22) = FDA90_vent_logC?? /0.0/
      R_(22)=R0_ikif
C FDA90_vent_log.fgi( 103, 269):���������
      !��������� R_(21) = FDA90_vent_logC?? /0.001/
      R_(21)=R0_okif
C FDA90_vent_log.fgi( 103, 267):���������
      !��������� R_(26) = FDA90_vent_logC?? /0.0/
      R_(26)=R0_ilif
C FDA90_vent_log.fgi(  34, 269):���������
      !��������� R_(25) = FDA90_vent_logC?? /0.001/
      R_(25)=R0_olif
C FDA90_vent_log.fgi(  34, 267):���������
      !{
      Call PNEVMO_STOP_HANDLER(deltat,REAL(R_umuf,4),R8_ufuf
     &,C30_oluf,
     & R_afuf,REAL(R_ifuf,4),R_iduf,
     & REAL(R_uduf,4),R_ibuf,REAL(R_ubuf,4),R_amuf,
     & R_uluf,L_ofuf,L_iruf,L_isuf,L_efuf,
     & REAL(R_okuf,4),L_ekuf,L_akuf,L_uruf,L_oduf,
     & L_obuf,L_aluf,L_atuf,L_oruf,L_imuf,L_omuf,
     & REAL(R8_epav,8),REAL(1.0,4),R8_avav,L_oxof,L_abuf,L_uxof
     &,L_ebuf,
     & L_osuf,R_eruf,REAL(R_ikuf,4),L_usuf,L_exof,L_etuf,L_ixof
     &,L_apuf,
     & L_epuf,L_ipuf,L_upuf,L_aruf,L_opuf)
      !}
C FDA90_vlv.fgi( 275,  55):���������� ����������� ������������ �������,20FDA91AE001KE07
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ote,L_eve,R_ave,
     & REAL(R_ive,4),L_exof,L_ite,I_ute)
      !}
C FDA90_lamp.fgi( 528,  52):���������� ������� ���������,20FDA91AE001QH22
      !{
      Call DAT_DISCR_HANDLER(deltat,I_exe,L_uxe,R_oxe,
     & REAL(R_abi,4),L_ixof,L_axe,I_ixe)
      !}
C FDA90_lamp.fgi( 528,  64):���������� ������� ���������,20FDA91AE001QH21
      !{
      Call PNEVMO_STOP_HANDLER(deltat,REAL(R_alak,4),R8_adak
     &,C30_ufak,
     & R_ebak,REAL(R_obak,4),R_oxuf,
     & REAL(R_abak,4),R_ovuf,REAL(R_axuf,4),R_ekak,
     & R_akak,L_ubak,L_omak,L_opak,L_ibak,
     & REAL(R_udak,4),L_idak,L_edak,L_apak,L_uxuf,
     & L_uvuf,L_efak,L_erak,L_umak,L_okak,L_ukak,
     & REAL(R8_epav,8),REAL(1.0,4),R8_avav,L_utuf,L_evuf,L_avuf
     &,L_ivuf,
     & L_upak,R_imak,REAL(R_odak,4),L_arak,L_ituf,L_irak,L_otuf
     &,L_elak,
     & L_ilak,L_olak,L_amak,L_emak,L_ulak)
      !}
C FDA90_vlv.fgi( 257,  55):���������� ����������� ������������ �������,20FDA91AE001KE06
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ubi,L_idi,R_edi,
     & REAL(R_odi,4),L_ituf,L_obi,I_adi)
      !}
C FDA90_lamp.fgi( 528,  82):���������� ������� ���������,20FDA91AE001QH20
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ifi,L_aki,R_ufi,
     & REAL(R_eki,4),L_otuf,L_efi,I_ofi)
      !}
C FDA90_lamp.fgi( 528,  94):���������� ������� ���������,20FDA91AE001QH19
      !{
      Call PNEVMO_STOP_HANDLER(deltat,REAL(R_efek,4),R8_exak
     &,C30_adek,
     & R_ivak,REAL(R_uvak,4),R_utak,
     & REAL(R_evak,4),R_usak,REAL(R_etak,4),R_idek,
     & R_edek,L_axak,L_ukek,L_ulek,L_ovak,
     & REAL(R_abek,4),L_oxak,L_ixak,L_elek,L_avak,
     & L_atak,L_ibek,L_imek,L_alek,L_udek,L_afek,
     & REAL(R8_epav,8),REAL(1.0,4),R8_avav,L_asak,L_isak,L_esak
     &,L_osak,
     & L_amek,R_okek,REAL(R_uxak,4),L_emek,L_orak,L_omek,L_urak
     &,L_ifek,
     & L_ofek,L_ufek,L_ekek,L_ikek,L_akek)
      !}
C FDA90_vlv.fgi( 239,  55):���������� ����������� ������������ �������,20FDA91AE001KE03
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ali,L_oli,R_ili,
     & REAL(R_uli,4),L_orak,L_uki,I_eli)
      !}
C FDA90_lamp.fgi( 528, 112):���������� ������� ���������,20FDA91AE001QH14
      !{
      Call DAT_DISCR_HANDLER(deltat,I_omi,L_epi,R_api,
     & REAL(R_ipi,4),L_urak,L_imi,I_umi)
      !}
C FDA90_lamp.fgi( 528, 124):���������� ������� ���������,20FDA91AE001QH13
      !{
      Call PNEVMO_STOP_HANDLER(deltat,REAL(R_ibik,4),R8_itek
     &,C30_exek,
     & R_osek,REAL(R_atek,4),R_asek,
     & REAL(R_isek,4),R_arek,REAL(R_irek,4),R_oxek,
     & R_ixek,L_etek,L_afik,L_akik,L_usek,
     & REAL(R_evek,4),L_utek,L_otek,L_ifik,L_esek,
     & L_erek,L_ovek,L_okik,L_efik,L_abik,L_ebik,
     & REAL(R8_epav,8),REAL(1.0,4),R8_avav,L_epek,L_opek,L_ipek
     &,L_upek,
     & L_ekik,R_udik,REAL(R_avek,4),L_ikik,L_umek,L_ukik,L_apek
     &,L_obik,
     & L_ubik,L_adik,L_idik,L_odik,L_edik)
      !}
C FDA90_vlv.fgi( 221,  55):���������� ����������� ������������ �������,20FDA91AE001KE02
      !{
      Call DAT_DISCR_HANDLER(deltat,I_eri,L_uri,R_ori,
     & REAL(R_asi,4),L_umek,L_ari,I_iri)
      !}
C FDA90_lamp.fgi( 528, 144):���������� ������� ���������,20FDA91AE001QH12
      !{
      Call DAT_DISCR_HANDLER(deltat,I_usi,L_iti,R_eti,
     & REAL(R_oti,4),L_apek,L_osi,I_ati)
      !}
C FDA90_lamp.fgi( 528, 156):���������� ������� ���������,20FDA91AE001QH11
      !{
      Call DAT_ANA_HANDLER(deltat,R_odok,R_ilok,REAL(1.0,4
     &),
     & REAL(R_efok,4),REAL(R_ifok,4),
     & REAL(R_idok,4),REAL(R_edok,4),I_elok,
     & REAL(R_akok,4),L_ekok,REAL(R_ikok,4),L_okok,L_ukok
     &,R_ofok,
     & REAL(R_afok,4),REAL(R_udok,4),L_alok,REAL(R_ufok,4
     &))
      !}
C FDA90_vlv.fgi( 198,  97):���������� �������,20FDA91CT181XQ01
      !{
      Call DAT_ANA_HANDLER(deltat,R_amok,R_urok,REAL(1.0,4
     &),
     & REAL(R_omok,4),REAL(R_umok,4),
     & REAL(R_ulok,4),REAL(R_olok,4),I_orok,
     & REAL(R_ipok,4),L_opok,REAL(R_upok,4),L_arok,L_erok
     &,R_apok,
     & REAL(R_imok,4),REAL(R_emok,4),L_irok,REAL(R_epok,4
     &))
      !}
C FDA90_vlv.fgi( 171,  97):���������� �������,20FDA91CP181XQ01
      !{
      Call DAT_ANA_HANDLER(deltat,R_usuk,R_oxuk,REAL(1.0,4
     &),
     & REAL(R_ituk,4),REAL(R_otuk,4),
     & REAL(R_osuk,4),REAL(R_isuk,4),I_ixuk,
     & REAL(R_evuk,4),L_ivuk,REAL(R_ovuk,4),L_uvuk,L_axuk
     &,R_utuk,
     & REAL(R_etuk,4),REAL(R_atuk,4),L_exuk,REAL(R_avuk,4
     &))
      !}
C FDA90_vlv.fgi( 318, 158):���������� �������,20FDA91CF102XQ01
      !{
      Call DAT_ANA_HANDLER(deltat,R_ebal,R_akal,REAL(1.0,4
     &),
     & REAL(R_ubal,4),REAL(R_adal,4),
     & REAL(R_abal,4),REAL(R_uxuk,4),I_ufal,
     & REAL(R_odal,4),L_udal,REAL(R_afal,4),L_efal,L_ifal
     &,R_edal,
     & REAL(R_obal,4),REAL(R_ibal,4),L_ofal,REAL(R_idal,4
     &))
      !}
C FDA90_vlv.fgi( 318, 173):���������� �������,20FDA91CF101XQ01
      !{
      Call DAT_ANA_HANDLER(deltat,R_abup,R_ufup,REAL(1.0,4
     &),
     & REAL(R_obup,4),REAL(R_ubup,4),
     & REAL(R_uxop,4),REAL(R_oxop,4),I_ofup,
     & REAL(R_idup,4),L_odup,REAL(R_udup,4),L_afup,L_efup
     &,R_adup,
     & REAL(R_ibup,4),REAL(R_ebup,4),L_ifup,REAL(R_edup,4
     &))
      !}
C FDA90_vlv.fgi( 172, 158):���������� �������,20FDA91CP012XQ02
      L_ulov=R0_ukov.ne.R0_okov
      R0_okov=R0_ukov
C FDA90_logic.fgi( 233,1610):���������� ������������� ������
      if (.not.L_alov.and.(L_elov.or.L_ulov)) then
          I_ilov = 0
          L_alov = .true.
          L_(401) = .true.
      endif
      if (I_ilov .gt. 0) then
         L_(401) = .false.
      endif
      if(L_olov)then
          I_ilov = 0
          L_alov = .false.
          L_(401) = .false.
      endif
C FDA90_logic.fgi( 310,1594):���������� ������� ���������,EC002
      L_ivex=R0_itex.ne.R0_etex
      R0_etex=R0_itex
C FDA90_logic.fgi( 431,1069):���������� ������������� ������
      if (.not.L_otex.and.(L_utex.or.L_ivex)) then
          I_avex = 0
          L_otex = .true.
          L_(683) = .true.
      endif
      if (I_avex .gt. 0) then
         L_(683) = .false.
      endif
      if(L_evex)then
          I_avex = 0
          L_otex = .false.
          L_(683) = .false.
      endif
C FDA90_logic.fgi( 508,1053):���������� ������� ���������,EC006
      L_omux=R0_olux.ne.R0_ilux
      R0_ilux=R0_olux
C FDA90_logic.fgi( 244,1066):���������� ������������� ������
      if (.not.L_ulux.and.(L_amux.or.L_omux)) then
          I_emux = 0
          L_ulux = .true.
          L_(872) = .true.
      endif
      if (I_emux .gt. 0) then
         L_(872) = .false.
      endif
      if(L_imux)then
          I_emux = 0
          L_ulux = .false.
          L_(872) = .false.
      endif
C FDA90_logic.fgi( 321,1050):���������� ������� ���������,EC005
      L_ukebe=R0_ufebe.ne.R0_ofebe
      R0_ofebe=R0_ufebe
C FDA90_logic.fgi(  58,1066):���������� ������������� ������
      if (.not.L_akebe.and.(L_ekebe.or.L_ukebe)) then
          I_ikebe = 0
          L_akebe = .true.
          L_(1013) = .true.
      endif
      if (I_ikebe .gt. 0) then
         L_(1013) = .false.
      endif
      if(L_okebe)then
          I_ikebe = 0
          L_akebe = .false.
          L_(1013) = .false.
      endif
C FDA90_logic.fgi( 135,1050):���������� ������� ���������,EC004
      L_utube=R0_elebe.ne.R0_alebe
      R0_alebe=R0_elebe
C FDA90_logic.fgi( 428,1610):���������� ������������� ������
      if (.not.L_itube.and.(L_urov.or.L_utube)) then
          I_otube = 0
          L_itube = .true.
          L_(1253) = .true.
      endif
      if (I_otube .gt. 0) then
         L_(1253) = .false.
      endif
      if(L_etube)then
          I_otube = 0
          L_itube = .false.
          L_(1253) = .false.
      endif
C FDA90_logic.fgi( 505,1594):���������� ������� ���������,EC003
      L_ulobe=R0_olebe.ne.R0_ilebe
      R0_ilebe=R0_olebe
C FDA90_logic.fgi(  58,1611):���������� ������������� ������
      if (.not.L_alobe.and.(L_elobe.or.L_ulobe)) then
          I_ilobe = 0
          L_alobe = .true.
          L_(1148) = .true.
      endif
      if (I_ilobe .gt. 0) then
         L_(1148) = .false.
      endif
      if(L_olobe)then
          I_ilobe = 0
          L_alobe = .false.
          L_(1148) = .false.
      endif
C FDA90_logic.fgi( 135,1595):���������� ������� ���������,EC001
      I_ulebe = z'01000010'
C FDA90_logic.fgi( 118,1866):��������� ������������� IN (�������)
      L0_apebe=R0_emebe.ne.R0_amebe
      R0_amebe=R0_emebe
C FDA90_logic.fgi(  55,1877):���������� ������������� ������
      L0_umebe=R0_omebe.ne.R0_imebe
      R0_imebe=R0_omebe
C FDA90_logic.fgi(  55,1894):���������� ������������� ������
      L0_epebe=(L0_umebe.or.L0_epebe).and..not.(L0_apebe)
      L_(1014)=.not.L0_epebe
C FDA90_logic.fgi(  88,1889):RS �������
      I_(2) = z'01000023'
C FDA90_logic.fgi( 117,1901):��������� ������������� IN (�������)
      I_(1) = z'01000010'
C FDA90_logic.fgi( 117,1899):��������� ������������� IN (�������)
      if(L0_epebe) then
         I_ipebe=I_(1)
      else
         I_ipebe=I_(2)
      endif
C FDA90_logic.fgi( 120,1899):���� RE IN LO CH7
      I_(4) = z'01000007'
C FDA90_logic.fgi( 124,1919):��������� ������������� IN (�������)
      I_(3) = z'0100000A'
C FDA90_logic.fgi( 124,1917):��������� ������������� IN (�������)
      I_(6) = z'01000007'
C FDA90_logic.fgi( 124,1929):��������� ������������� IN (�������)
      I_(5) = z'0100000A'
C FDA90_logic.fgi( 124,1927):��������� ������������� IN (�������)
      I_(8) = z'01000007'
C FDA90_logic.fgi( 124,1938):��������� ������������� IN (�������)
      I_(7) = z'0100000A'
C FDA90_logic.fgi( 124,1936):��������� ������������� IN (�������)
      C30_ovube = '������������������'
C FDA90_logic.fgi(  95,1952):��������� ���������� CH20 (CH30) (�������)
      C30_uvube = '������������'
C FDA90_logic.fgi( 116,1956):��������� ���������� CH20 (CH30) (�������)
      C30_axube = '������'
C FDA90_logic.fgi( 110,1951):��������� ���������� CH20 (CH30) (�������)
      C30_oxube = '�� ������'
C FDA90_logic.fgi(  95,1954):��������� ���������� CH20 (CH30) (�������)
      L_(1263)=.true.
C FDA90_logic.fgi(  64,1928):��������� ���������� (�������)
      L0_ibade=R0_ubade.ne.R0_obade
      R0_obade=R0_ubade
C FDA90_logic.fgi(  51,1921):���������� ������������� ������
      if(L0_ibade) then
         L_(1260)=L_(1263)
      else
         L_(1260)=.false.
      endif
C FDA90_logic.fgi(  68,1927):���� � ������������� �������
      L_(3)=L_(1260)
C FDA90_logic.fgi(  68,1927):������-�������: ���������� ��� �������������� ������
      L_(1264)=.true.
C FDA90_logic.fgi(  64,1945):��������� ���������� (�������)
      L0_idade=R0_udade.ne.R0_odade
      R0_odade=R0_udade
C FDA90_logic.fgi(  51,1938):���������� ������������� ������
      if(L0_idade) then
         L_(1261)=L_(1264)
      else
         L_(1261)=.false.
      endif
C FDA90_logic.fgi(  68,1944):���� � ������������� �������
      L_(2)=L_(1261)
C FDA90_logic.fgi(  68,1944):������-�������: ���������� ��� �������������� ������
      L_(1258) = L_(2).OR.L_(3)
C FDA90_logic.fgi(  78,1957):���
      L_(1265)=.true.
C FDA90_logic.fgi(  64,1961):��������� ���������� (�������)
      L0_ifade=R0_ufade.ne.R0_ofade
      R0_ofade=R0_ufade
C FDA90_logic.fgi(  51,1954):���������� ������������� ������
      if(L0_ifade) then
         L_(1262)=L_(1265)
      else
         L_(1262)=.false.
      endif
C FDA90_logic.fgi(  68,1960):���� � ������������� �������
      L_(1)=L_(1262)
C FDA90_logic.fgi(  68,1960):������-�������: ���������� ��� �������������� ������
      L_(1254) = L_(1).OR.L_(2)
C FDA90_logic.fgi(  78,1924):���
      L_ebade=(L_(3).or.L_ebade).and..not.(L_(1254))
      L_(1255)=.not.L_ebade
C FDA90_logic.fgi(  86,1926):RS �������
      L_abade=L_ebade
C FDA90_logic.fgi( 102,1928):������,FDA9_tech_mode
      if(L_ebade) then
         I_avube=I_(3)
      else
         I_avube=I_(4)
      endif
C FDA90_logic.fgi( 127,1917):���� RE IN LO CH7
      L_(1256) = L_(1).OR.L_(3)
C FDA90_logic.fgi(  78,1941):���
      L_edade=(L_(2).or.L_edade).and..not.(L_(1256))
      L_(1257)=.not.L_edade
C FDA90_logic.fgi(  86,1943):RS �������
      L_adade=L_edade
C FDA90_logic.fgi( 102,1945):������,FDA9_ruch_mode
      if(L_edade) then
         I_evube=I_(5)
      else
         I_evube=I_(6)
      endif
C FDA90_logic.fgi( 127,1927):���� RE IN LO CH7
      L_efade=(L_(1).or.L_efade).and..not.(L_(1258))
      L_(1259)=.not.L_efade
C FDA90_logic.fgi(  86,1959):RS �������
      L_afade=L_efade
C FDA90_logic.fgi( 102,1961):������,FDA9_avt_mode
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_atar,4),L_iber,L_ober
     &,R8_opar,C30_asar,
     & L_abade,L_adade,L_afade,I_ixar,I_oxar,R_isar,R_esar
     &,
     & R_olar,REAL(R_amar,4),R_umar,
     & REAL(R_epar,4),R_emar,REAL(R_omar,4),I_ovar,
     & I_uxar,I_exar,I_axar,L_ipar,L_aber,L_ider,L_apar,
     & L_imar,L_irar,L_erar,L_uber,L_ular,L_urar,
     & L_afer,L_eber,L_osar,L_usar,REAL(R8_epav,8),REAL(1.0
     &,4),R8_avav,
     & L_arar,L_upar,L_oder,R_ivar,REAL(R_orar,4),L_uder,L_efer
     &,
     & L_etar,L_itar,L_otar,L_avar,L_evar,L_utar)
      !}

      if(L_evar.or.L_avar.or.L_utar.or.L_otar.or.L_itar.or.L_etar
     &) then      
                  I_uvar = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_uvar = z'40000000'
      endif
C FDA90_vlv.fgi(  55,  96):���� ���������� �������� ��������,20FDA91AA123
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_ifor,4),L_umor,L_apor
     &,R8_abor,C30_idor,
     & L_abade,L_adade,L_afade,I_ulor,I_amor,R_udor,R_odor
     &,
     & R_avir,REAL(R_ivir,4),R_exir,
     & REAL(R_oxir,4),R_ovir,REAL(R_axir,4),I_alor,
     & I_emor,I_olor,I_ilor,L_uxir,L_imor,L_upor,L_ixir,
     & L_uvir,L_ubor,L_obor,L_epor,L_evir,L_edor,
     & L_iror,L_omor,L_afor,L_efor,REAL(R8_epav,8),REAL(1.0
     &,4),R8_avav,
     & L_ibor,L_ebor,L_aror,R_ukor,REAL(R_ador,4),L_eror,L_oror
     &,
     & L_ofor,L_ufor,L_akor,L_ikor,L_okor,L_ekor)
      !}

      if(L_okor.or.L_ikor.or.L_ekor.or.L_akor.or.L_ufor.or.L_ofor
     &) then      
                  I_elor = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_elor = z'40000000'
      endif
C FDA90_vlv.fgi(  55, 120):���� ���������� �������� ��������,20FDA91AA202
      R_emom = R_ukor * R_(1)
C FDA90_vent_log.fgi( 173, 160):����������
      !{
      Call DAT_ANA_HANDLER(deltat,R_alom,R_upom,REAL(1.0,4
     &),
     & REAL(R_olom,4),REAL(R_ulom,4),
     & REAL(R_ukom,4),REAL(R_okom,4),I_opom,
     & REAL(R_imom,4),L_omom,REAL(R_umom,4),L_apom,L_epom
     &,R_amom,
     & REAL(R_ilom,4),REAL(R_elom,4),L_ipom,REAL(R_emom,4
     &))
      !}
C FDA90_vlv.fgi( 260, 128):���������� �������,20FDA91AA202XQ01
      !{
      Call BVALVE_MAN(deltat,REAL(R_uxor,4),L_ekur,L_ikur
     &,R8_utor,L_abade,
     & L_adade,L_afade,I_efur,I_ifur,R_uror,
     & REAL(R_esor,4),R_ator,REAL(R_itor,4),
     & R_isor,REAL(R_usor,4),I_idur,I_ofur,I_afur,I_udur,L_otor
     &,
     & L_ufur,L_elur,L_etor,L_osor,
     & L_ovor,L_ivor,L_okur,L_asor,L_axor,L_ulur,L_akur,
     & L_ixor,L_oxor,REAL(R8_epav,8),REAL(1.0,4),R8_avav,L_evor
     &,
     & L_avor,L_ilur,R_edur,REAL(R_uvor,4),L_olur,L_amur,L_abur
     &,
     & L_ebur,L_ibur,L_ubur,L_adur,L_obur)
      !}

      if(L_adur.or.L_ubur.or.L_obur.or.L_ibur.or.L_ebur.or.L_abur
     &) then      
                  I_odur = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_odur = z'40000000'
      endif
C FDA90_vlv.fgi(  69, 120):���� ���������� ������ �������,20FDA91AA125
      !{
      Call BVALVE_MAN(deltat,REAL(R_etur,4),L_obas,L_ubas
     &,R8_erur,L_abade,
     & L_adade,L_afade,I_oxur,I_uxur,R_emur,
     & REAL(R_omur,4),R_ipur,REAL(R_upur,4),
     & R_umur,REAL(R_epur,4),I_uvur,I_abas,I_ixur,I_exur,L_arur
     &,
     & L_ebas,L_odas,L_opur,L_apur,
     & L_asur,L_urur,L_adas,L_imur,L_isur,L_efas,L_ibas,
     & L_usur,L_atur,REAL(R8_epav,8),REAL(1.0,4),R8_avav,L_orur
     &,
     & L_irur,L_udas,R_ovur,REAL(R_esur,4),L_afas,L_ifas,L_itur
     &,
     & L_otur,L_utur,L_evur,L_ivur,L_avur)
      !}

      if(L_ivur.or.L_evur.or.L_avur.or.L_utur.or.L_otur.or.L_itur
     &) then      
                  I_axur = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_axur = z'40000000'
      endif
C FDA90_vlv.fgi(  84, 120):���� ���������� ������ �������,20FDA91AA124
      !{
      Call BVALVE_MAN(deltat,REAL(R_opas,4),L_avas,L_evas
     &,R8_olas,L_abade,
     & L_adade,L_afade,I_atas,I_etas,R_ofas,
     & REAL(R_akas,4),R_ukas,REAL(R_elas,4),
     & R_ekas,REAL(R_okas,4),I_esas,I_itas,I_usas,I_osas,L_ilas
     &,
     & L_otas,L_axas,L_alas,L_ikas,
     & L_imas,L_emas,L_ivas,L_ufas,L_umas,L_oxas,L_utas,
     & L_epas,L_ipas,REAL(R8_epav,8),REAL(1.0,4),R8_avav,L_amas
     &,
     & L_ulas,L_exas,R_asas,REAL(R_omas,4),L_ixas,L_uxas,L_upas
     &,
     & L_aras,L_eras,L_oras,L_uras,L_iras)
      !}

      if(L_uras.or.L_oras.or.L_iras.or.L_eras.or.L_aras.or.L_upas
     &) then      
                  I_isas = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_isas = z'40000000'
      endif
C FDA90_vlv.fgi( 101, 120):���� ���������� ������ �������,20FDA91AA121
      !{Call KN_VLVR(deltat,REAL(R_ovav,4),L_afev,L_efev,R8_isav
C ,
      Call SOLENOIDVALVE_MAN(deltat,REAL(R_ovav,4),L_afev
     &,L_efev,R8_isav,C30_utav,
     & L_abade,L_adade,L_afade,I_adev,I_edev,R_ipav,
     & REAL(R_upav,4),R_orav,REAL(R_asav,4),
     & R_arav,REAL(R_irav,4),I_ebev,I_idev,I_ubev,I_obev,
     & L_urav,L_erav,L_etav,L_atav,L_ifev,
     & L_opav,L_otav,L_okev,L_usav,L_osav,L_ikev,L_ukev,
     & L_esav,L_odev,L_akev,L_udev,L_evav,L_ivav,
     & REAL(R8_epav,8),REAL(1.0,4),R8_avav,L_ekev,R_abev,
     & REAL(R_itav,4),L_uvav,L_axav,L_exav,L_oxav,L_uxav,L_ixav
     &)
      !}

      if(L_uxav.or.L_oxav.or.L_ixav.or.L_exav.or.L_axav.or.L_uvav
     &) then      
                  I_ibev = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_ibev = z'40000000'
      endif
C FDA90_vlv.fgi(  39, 175):���� ���������� ���������������� ��������,20FDA91AX005Y01
      !{
      Call BVALVE_MAN(deltat,REAL(R_ales,4),L_ires,L_ores
     &,R8_afes,L_abade,
     & L_adade,L_afade,I_ipes,I_opes,R_abes,
     & REAL(R_ibes,4),R_edes,REAL(R_odes,4),
     & R_obes,REAL(R_ades,4),I_omes,I_upes,I_epes,I_apes,L_udes
     &,
     & L_ares,L_ises,L_ides,L_ubes,
     & L_ufes,L_ofes,L_ures,L_ebes,L_ekes,L_ates,L_eres,
     & L_okes,L_ukes,REAL(R8_epav,8),REAL(1.0,4),R8_avav,L_ifes
     &,
     & L_efes,L_oses,R_imes,REAL(R_akes,4),L_uses,L_etes,L_eles
     &,
     & L_iles,L_oles,L_ames,L_emes,L_ules)
      !}

      if(L_emes.or.L_ames.or.L_ules.or.L_oles.or.L_iles.or.L_eles
     &) then      
                  I_umes = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_umes = z'40000000'
      endif
C FDA90_vlv.fgi( 101, 175):���� ���������� ������ �������,20FDA91AA126
      !{
      Call BVALVE_MAN(deltat,REAL(R_idis,4),L_ulis,L_amis
     &,R8_ixes,L_abade,
     & L_adade,L_afade,I_ukis,I_alis,R_ites,
     & REAL(R_utes,4),R_oves,REAL(R_axes,4),
     & R_aves,REAL(R_ives,4),I_akis,I_elis,I_okis,I_ikis,L_exes
     &,
     & L_ilis,L_umis,L_uves,L_eves,
     & L_ebis,L_abis,L_emis,L_otes,L_obis,L_ipis,L_olis,
     & L_adis,L_edis,REAL(R8_epav,8),REAL(1.0,4),R8_avav,L_uxes
     &,
     & L_oxes,L_apis,R_ufis,REAL(R_ibis,4),L_epis,L_opis,L_odis
     &,
     & L_udis,L_afis,L_ifis,L_ofis,L_efis)
      !}

      if(L_ofis.or.L_ifis.or.L_efis.or.L_afis.or.L_udis.or.L_odis
     &) then      
                  I_ekis = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_ekis = z'40000000'
      endif
C FDA90_vlv.fgi(  84, 175):���� ���������� ������ �������,20FDA91AA122
      !{Call KN_VLVR(deltat,REAL(R_ubav,4),L_elav,L_ilav,R8_uvut
C ,
      Call SOLENOIDVALVE_MAN(deltat,REAL(R_ubav,4),L_elav
     &,L_ilav,R8_uvut,C30_ebav,
     & L_abade,L_adade,L_afade,I_ekav,I_ikav,R_usut,
     & REAL(R_etut,4),R_avut,REAL(R_ivut,4),
     & R_itut,REAL(R_utut,4),I_ifav,I_okav,I_akav,I_ufav,
     & L_evut,L_otut,L_oxut,L_ixut,L_olav,
     & L_atut,L_abav,L_umav,L_exut,L_axut,L_omav,L_apav,
     & L_ovut,L_ukav,L_emav,L_alav,L_ibav,L_obav,
     & REAL(R8_epav,8),REAL(1.0,4),R8_avav,L_imav,R_efav,
     & REAL(R_uxut,4),L_adav,L_edav,L_idav,L_udav,L_afav,L_odav
     &)
      !}

      if(L_afav.or.L_udav.or.L_odav.or.L_idav.or.L_edav.or.L_adav
     &) then      
                  I_ofav = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_ofav = z'40000000'
      endif
C FDA90_vlv.fgi(  55, 175):���� ���������� ���������������� ��������,20FDA91AX006Y02
      !{
      Call BVALVE_MAN(deltat,REAL(R_opus,4),L_avus,L_evus
     &,R8_olus,L_abade,
     & L_adade,L_afade,I_atus,I_etus,R_ofus,
     & REAL(R_akus,4),R_ukus,REAL(R_elus,4),
     & R_ekus,REAL(R_okus,4),I_esus,I_itus,I_usus,I_osus,L_ilus
     &,
     & L_otus,L_axus,L_alus,L_ikus,
     & L_imus,L_emus,L_ivus,L_ufus,L_umus,L_oxus,L_utus,
     & L_epus,L_ipus,REAL(R8_epav,8),REAL(1.0,4),R8_avav,L_amus
     &,
     & L_ulus,L_exus,R_asus,REAL(R_omus,4),L_ixus,L_uxus,L_upus
     &,
     & L_arus,L_erus,L_orus,L_urus,L_irus)
      !}

      if(L_urus.or.L_orus.or.L_irus.or.L_erus.or.L_arus.or.L_upus
     &) then      
                  I_isus = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_isus = z'40000000'
      endif
C FDA90_vlv.fgi( 101, 149):���� ���������� ������ �������,20FDA91AA111
      !{
      Call BVALVE_MAN(deltat,REAL(R_alat,4),L_irat,L_orat
     &,R8_afat,L_abade,
     & L_adade,L_afade,I_ipat,I_opat,R_abat,
     & REAL(R_ibat,4),R_edat,REAL(R_odat,4),
     & R_obat,REAL(R_adat,4),I_omat,I_upat,I_epat,I_apat,L_udat
     &,
     & L_arat,L_isat,L_idat,L_ubat,
     & L_ufat,L_ofat,L_urat,L_ebat,L_ekat,L_atat,L_erat,
     & L_okat,L_ukat,REAL(R8_epav,8),REAL(1.0,4),R8_avav,L_ifat
     &,
     & L_efat,L_osat,R_imat,REAL(R_akat,4),L_usat,L_etat,L_elat
     &,
     & L_ilat,L_olat,L_amat,L_emat,L_ulat)
      !}

      if(L_emat.or.L_amat.or.L_ulat.or.L_olat.or.L_ilat.or.L_elat
     &) then      
                  I_umat = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_umat = z'40000000'
      endif
C FDA90_vlv.fgi(  84, 149):���� ���������� ������ �������,20FDA91AA116
      !{
      Call BVALVE_MAN(deltat,REAL(R_idet,4),L_ulet,L_amet
     &,R8_ixat,L_abade,
     & L_adade,L_afade,I_uket,I_alet,R_itat,
     & REAL(R_utat,4),R_ovat,REAL(R_axat,4),
     & R_avat,REAL(R_ivat,4),I_aket,I_elet,I_oket,I_iket,L_exat
     &,
     & L_ilet,L_umet,L_uvat,L_evat,
     & L_ebet,L_abet,L_emet,L_otat,L_obet,L_ipet,L_olet,
     & L_adet,L_edet,REAL(R8_epav,8),REAL(1.0,4),R8_avav,L_uxat
     &,
     & L_oxat,L_apet,R_ufet,REAL(R_ibet,4),L_epet,L_opet,L_odet
     &,
     & L_udet,L_afet,L_ifet,L_ofet,L_efet)
      !}

      if(L_ofet.or.L_ifet.or.L_efet.or.L_afet.or.L_udet.or.L_odet
     &) then      
                  I_eket = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_eket = z'40000000'
      endif
C FDA90_vlv.fgi(  69, 149):���� ���������� ������ �������,20FDA91AA115
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_exet,4),L_ofit,L_ufit
     &,R8_uset,C30_evet,
     & L_abade,L_adade,L_afade,I_odit,I_udit,R_ovet,R_ivet
     &,
     & R_upet,REAL(R_eret,4),R_aset,
     & REAL(R_iset,4),R_iret,REAL(R_uret,4),I_ubit,
     & I_afit,I_idit,I_edit,L_oset,L_efit,L_okit,L_eset,
     & L_oret,L_otet,L_itet,L_akit,L_aret,L_avet,
     & L_elit,L_ifit,L_uvet,L_axet,REAL(R8_epav,8),REAL(1.0
     &,4),R8_avav,
     & L_etet,L_atet,L_ukit,R_obit,REAL(R_utet,4),L_alit,L_ilit
     &,
     & L_ixet,L_oxet,L_uxet,L_ebit,L_ibit,L_abit)
      !}

      if(L_ibit.or.L_ebit.or.L_abit.or.L_uxet.or.L_oxet.or.L_ixet
     &) then      
                  I_adit = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_adit = z'40000000'
      endif
C FDA90_vlv.fgi(  69, 175):���� ���������� �������� ��������,20FDA91AA201
      R_ulip = R_obit * R_(2)
C FDA90_vent_log.fgi( 173, 167):����������
      !{
      Call DAT_ANA_HANDLER(deltat,R_okip,R_ipip,REAL(1.0,4
     &),
     & REAL(R_elip,4),REAL(R_ilip,4),
     & REAL(R_ikip,4),REAL(R_ekip,4),I_epip,
     & REAL(R_amip,4),L_emip,REAL(R_imip,4),L_omip,L_umip
     &,R_olip,
     & REAL(R_alip,4),REAL(R_ukip,4),L_apip,REAL(R_ulip,4
     &))
      !}
C FDA90_vlv.fgi( 260, 113):���������� �������,20FDA91AA201XQ01
      !{
      Call BVALVE_MAN(deltat,REAL(R_ikut,4),L_uput,L_arut
     &,R8_idut,L_abade,
     & L_adade,L_afade,I_umut,I_aput,R_ixot,
     & REAL(R_uxot,4),R_obut,REAL(R_adut,4),
     & R_abut,REAL(R_ibut,4),I_amut,I_eput,I_omut,I_imut,L_edut
     &,
     & L_iput,L_urut,L_ubut,L_ebut,
     & L_efut,L_afut,L_erut,L_oxot,L_ofut,L_isut,L_oput,
     & L_akut,L_ekut,REAL(R8_epav,8),REAL(1.0,4),R8_avav,L_udut
     &,
     & L_odut,L_asut,R_ulut,REAL(R_ifut,4),L_esut,L_osut,L_okut
     &,
     & L_ukut,L_alut,L_ilut,L_olut,L_elut)
      !}

      if(L_olut.or.L_ilut.or.L_elut.or.L_alut.or.L_ukut.or.L_okut
     &) then      
                  I_emut = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_emut = z'40000000'
      endif
C FDA90_vlv.fgi(  55, 149):���� ���������� ������ �������,20FDA91AA112
      !{
      Call BVALVE_MAN(deltat,REAL(R_apot,4),L_itot,L_otot
     &,R8_alot,L_abade,
     & L_adade,L_afade,I_isot,I_osot,R_afot,
     & REAL(R_ifot,4),R_ekot,REAL(R_okot,4),
     & R_ofot,REAL(R_akot,4),I_orot,I_usot,I_esot,I_asot,L_ukot
     &,
     & L_atot,L_ivot,L_ikot,L_ufot,
     & L_ulot,L_olot,L_utot,L_efot,L_emot,L_axot,L_etot,
     & L_omot,L_umot,REAL(R8_epav,8),REAL(1.0,4),R8_avav,L_ilot
     &,
     & L_elot,L_ovot,R_irot,REAL(R_amot,4),L_uvot,L_exot,L_epot
     &,
     & L_ipot,L_opot,L_arot,L_erot,L_upot)
      !}

      if(L_erot.or.L_arot.or.L_upot.or.L_opot.or.L_ipot.or.L_epot
     &) then      
                  I_urot = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_urot = z'40000000'
      endif
C FDA90_vlv.fgi(  39, 149):���� ���������� ������ �������,20FDA91AA113
      !{
      Call BVALVE_MAN(deltat,REAL(R_osit,4),L_abot,L_ebot
     &,R8_opit,L_abade,
     & L_adade,L_afade,I_axit,I_exit,R_olit,
     & REAL(R_amit,4),R_umit,REAL(R_epit,4),
     & R_emit,REAL(R_omit,4),I_evit,I_ixit,I_uvit,I_ovit,L_ipit
     &,
     & L_oxit,L_adot,L_apit,L_imit,
     & L_irit,L_erit,L_ibot,L_ulit,L_urit,L_odot,L_uxit,
     & L_esit,L_isit,REAL(R8_epav,8),REAL(1.0,4),R8_avav,L_arit
     &,
     & L_upit,L_edot,R_avit,REAL(R_orit,4),L_idot,L_udot,L_usit
     &,
     & L_atit,L_etit,L_otit,L_utit,L_itit)
      !}

      if(L_utit.or.L_otit.or.L_itit.or.L_etit.or.L_atit.or.L_usit
     &) then      
                  I_ivit = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_ivit = z'40000000'
      endif
C FDA90_vlv.fgi(  21, 149):���� ���������� ������ �������,20FDA91AA114
      if(L_efade) then
         C30_ixube=C30_ovube
      else
         C30_ixube=C30_oxube
      endif
C FDA90_logic.fgi(  99,1952):���� RE IN LO CH20
      if(L_edade) then
         C30_exube=C30_axube
      else
         C30_exube=C30_ixube
      endif
C FDA90_logic.fgi( 114,1951):���� RE IN LO CH20
      if(L_ebade) then
         C30_uxube=C30_uvube
      else
         C30_uxube=C30_exube
      endif
C FDA90_logic.fgi( 123,1950):���� RE IN LO CH20
      if(L_efade) then
         I_ivube=I_(7)
      else
         I_ivube=I_(8)
      endif
C FDA90_logic.fgi( 127,1936):���� RE IN LO CH7
      L_(1017)=R_ukube.gt.R0_irebe
C FDA90_logic.fgi(  99,1142):���������� >
C label 451  try451=try451-1
      L_(1236)=R_erube.lt.R0_ikube
C FDA90_logic.fgi( 479,1375):���������� <
      if(L_upube.and..not.L0_opube) then
         R0_epube=R0_ipube
      else
         R0_epube=max(R_(340)-deltat,0.0)
      endif
      L_(1233)=R0_epube.gt.0.0
      L0_opube=L_upube
C FDA90_logic.fgi( 531,1375):������������  �� T
      L_(1232)=.not.L_(1246) .and.L_(1233)
      L_(1230)=L_(1233).and..not.L_(1231)
C FDA90_logic.fgi( 551,1376):��������� ������
      L_(1111)=R_erube.lt.R0_ipibe
C FDA90_logic.fgi( 109,1361):���������� <
      L_(1085)=R_ukube.lt.R0_opibe
C FDA90_logic.fgi( 109,1393):���������� <
      L_(1251)=R_atube.lt.R0_usube
C FDA90_logic.fgi( 479,1461):���������� <
      L_(427)=R_erube.lt.R0_avov
C FDA90_logic.fgi( 479,1484):���������� <
      L_(1147)=R_emobe.lt.R0_okobe
C FDA90_logic.fgi( 109,1569):���������� <
      if(L_alobe) then
          if (L_(1148)) then
              I_ilobe = 1
              L_ukobe = .true.
              L_(1146) = .false.
          endif
          if (L_(1147)) then
              L_(1146) = .true.
              L_ukobe = .false.
          endif
          if (I_ilobe.ne.1) then
              L_ukobe = .false.
              L_(1146) = .false.
          endif
      else
          L_(1146) = .false.
      endif
C FDA90_logic.fgi( 135,1569):��� ������� ���������,EC001
      if(L_ukobe.and..not.L0_ofobe) then
         R0_efobe=R0_ifobe
      else
         R0_efobe=max(R_(311)-deltat,0.0)
      endif
      L_(1145)=R0_efobe.gt.0.0
      L0_ofobe=L_ukobe
C FDA90_logic.fgi( 161,1569):������������  �� T
      L_(1144)=.not.L_(1246) .and.L_(1145)
      L_(1142)=L_(1145).and..not.L_(1143)
C FDA90_logic.fgi( 181,1570):��������� ������
      L_(1130)=R_emobe.lt.R0_otibe
C FDA90_logic.fgi( 109,1519):���������� <
      L_(1141)=R_emobe.gt.R0_ekobe
C FDA90_logic.fgi( 109,1539):���������� >
      if(L_alobe) then
          if (L_(1146)) then
              I_ilobe = 2
              L_akobe = .true.
              L_(1139) = .false.
          endif
          if (L_ufobe) then
              L_(1139) = .true.
              L_akobe = .false.
          endif
          if (I_ilobe.ne.2) then
              L_akobe = .false.
              L_(1139) = .false.
          endif
      else
          L_(1139) = .false.
      endif
C FDA90_logic.fgi( 135,1552):��� ������� ���������,EC001
      if(L_alobe) then
          if (L_(1139)) then
              I_ilobe = 3
              L_ikobe = .true.
              L_(1140) = .false.
          endif
          if (L_(1141)) then
              L_(1140) = .true.
              L_ikobe = .false.
          endif
          if (I_ilobe.ne.3) then
              L_ikobe = .false.
              L_(1140) = .false.
          endif
      else
          L_(1140) = .false.
      endif
C FDA90_logic.fgi( 135,1539):��� ������� ���������,EC001
      if(L_alobe) then
          if (L_(1140)) then
              I_ilobe = 4
              L_idobe = .true.
              L_(1129) = .false.
          endif
          if (L_(1130)) then
              L_(1129) = .true.
              L_idobe = .false.
          endif
          if (I_ilobe.ne.4) then
              L_idobe = .false.
              L_(1129) = .false.
          endif
      else
          L_(1129) = .false.
      endif
C FDA90_logic.fgi( 135,1519):��� ������� ���������,EC001
      if(L_idobe.and..not.L0_evibe) then
         R0_utibe=R0_avibe
      else
         R0_utibe=max(R_(302)-deltat,0.0)
      endif
      L_(1138)=R0_utibe.gt.0.0
      L0_evibe=L_idobe
C FDA90_logic.fgi( 161,1519):������������  �� T
      L_(1137)=.not.L_(1246) .and.L_(1138)
      L_(1135)=L_(1138).and..not.L_(1136)
C FDA90_logic.fgi( 181,1520):��������� ������
      L_(911)=R_emobe.lt.R0_ivux
C FDA90_logic.fgi( 108, 580):���������� <
      L_(904)=R_emobe.gt.R0_avux
C FDA90_logic.fgi( 108, 595):���������� >
      L_(880)=R_atube.gt.R0_arux
C FDA90_logic.fgi( 109, 634):���������� >
      L_(894)=R_emobe.lt.R0_urux
C FDA90_logic.fgi( 109, 654):���������� <
      L_(917)=R_erube.lt.R0_uvux
C FDA90_logic.fgi( 109, 684):���������� <
      L_(923)=R_atube.lt.R0_obabe
C FDA90_logic.fgi( 109, 700):���������� <
      L_(925)=R_ukube.gt.R0_ubabe
C FDA90_logic.fgi( 109, 718):���������� >
      L_(940)=R_erube.gt.R0_ofabe
C FDA90_logic.fgi( 109, 751):���������� >
      L_(946)=R_ukube.lt.R0_ukabe
C FDA90_logic.fgi( 109, 783):���������� <
      L_(953)=R_erube.lt.R0_ulabe
C FDA90_logic.fgi( 109, 801):���������� <
      L_(1193)=R_axobe.lt.R0_uvobe
C FDA90_logic.fgi( 480,1288):���������� <
      L_(1198)=R_erube.gt.R0_abube
C FDA90_logic.fgi( 479,1329):���������� >
      L_(1200)=R_ukube.gt.R0_adube
C FDA90_logic.fgi( 479,1342):���������� >
      if(L_uxobe.and..not.L0_oxobe) then
         R0_exobe=R0_ixobe
      else
         R0_exobe=max(R_(326)-deltat,0.0)
      endif
      L_(1191)=R0_exobe.gt.0.0
      L0_oxobe=L_uxobe
C FDA90_logic.fgi( 531,1288):������������  �� T
      L_(1190)=.not.L_(1246) .and.L_(1191)
      L_(1188)=L_(1191).and..not.L_(1189)
C FDA90_logic.fgi( 551,1288):��������� ������
      L_(1166)=R_axobe.lt.R0_ipobe
C FDA90_logic.fgi( 481,1197):���������� <
      L_(1151)=R_ukube.gt.R0_omobe
C FDA90_logic.fgi( 481,1212):���������� >
      L_(1177)=R_axobe.gt.R0_osobe
C FDA90_logic.fgi( 481,1241):���������� >
      L_(1187)=R_ukube.lt.R0_isobe
C FDA90_logic.fgi( 480,1274):���������� <
      if(L_erobe.and..not.L0_arobe) then
         R0_opobe=R0_upobe
      else
         R0_opobe=max(R_(318)-deltat,0.0)
      endif
      L_(1163)=R0_opobe.gt.0.0
      L0_arobe=L_erobe
C FDA90_logic.fgi( 531,1197):������������  �� T
      L_(1162)=.not.L_(1246) .and.L_(1163)
      L_(1160)=L_(1163).and..not.L_(1161)
C FDA90_logic.fgi( 552,1198):��������� ������
      L_(1012)=R_axobe.lt.R0_odebe
C FDA90_logic.fgi( 111,1015):���������� <
      if(L_akebe) then
          if (L_(1013)) then
              I_ikebe = 1
              L_ifebe = .true.
              L_(1011) = .false.
          endif
          if (L_(1012)) then
              L_(1011) = .true.
              L_ifebe = .false.
          endif
          if (I_ikebe.ne.1) then
              L_ifebe = .false.
              L_(1011) = .false.
          endif
      else
          L_(1011) = .false.
      endif
C FDA90_logic.fgi( 135,1015):��� ������� ���������,EC004
      if(L_ifebe.and..not.L0_efebe) then
         R0_udebe=R0_afebe
      else
         R0_udebe=max(R_(272)-deltat,0.0)
      endif
      L_(1010)=R0_udebe.gt.0.0
      L0_efebe=L_ifebe
C FDA90_logic.fgi( 161,1015):������������  �� T
      L_(1009)=.not.L_(1246) .and.L_(1010)
      L_(1007)=L_(1010).and..not.L_(1008)
C FDA90_logic.fgi( 181,1016):��������� ������
      L_(985)=R_axobe.lt.R0_otabe
C FDA90_logic.fgi( 109, 918):���������� <
      L_(987)=R_ukube.lt.R0_ovabe
C FDA90_logic.fgi( 109, 938):���������� <
      L_(1002)=R_axobe.gt.R0_ibebe
C FDA90_logic.fgi( 111, 976):���������� >
      if(L_akebe) then
          if (L_(1011)) then
              I_ikebe = 2
              L_idebe = .true.
              L_(1003) = .false.
          endif
          if (L_otobe) then
              L_(1003) = .true.
              L_idebe = .false.
          endif
          if (I_ikebe.ne.2) then
              L_idebe = .false.
              L_(1003) = .false.
          endif
      else
          L_(1003) = .false.
      endif
C FDA90_logic.fgi( 135, 996):��� ������� ���������,EC004
      if(L_akebe) then
          if (L_(1003)) then
              I_ikebe = 3
              L_edebe = .true.
              L_(1001) = .false.
          endif
          if (L_(1002)) then
              L_(1001) = .true.
              L_edebe = .false.
          endif
          if (I_ikebe.ne.3) then
              L_edebe = .false.
              L_(1001) = .false.
          endif
      else
          L_(1001) = .false.
      endif
C FDA90_logic.fgi( 135, 976):��� ������� ���������,EC004
      if(L_akebe) then
          if (L_(1001)) then
              I_ikebe = 4
              L_ebebe = .true.
              L_(996) = .false.
          endif
          if (L_umube) then
              L_(996) = .true.
              L_ebebe = .false.
          endif
          if (I_ikebe.ne.4) then
              L_ebebe = .false.
              L_(996) = .false.
          endif
      else
          L_(996) = .false.
      endif
C FDA90_logic.fgi( 135, 959):��� ������� ���������,EC004
      if(L_akebe) then
          if (L_(996)) then
              I_ikebe = 5
              L_ixabe = .true.
              L_(986) = .false.
          endif
          if (L_(987)) then
              L_(986) = .true.
              L_ixabe = .false.
          endif
          if (I_ikebe.ne.5) then
              L_ixabe = .false.
              L_(986) = .false.
          endif
      else
          L_(986) = .false.
      endif
C FDA90_logic.fgi( 135, 938):��� ������� ���������,EC004
      if(L_akebe) then
          if (L_(986)) then
              I_ikebe = 6
              L_ivabe = .true.
              L_(984) = .false.
          endif
          if (L_(985)) then
              L_(984) = .true.
              L_ivabe = .false.
          endif
          if (I_ikebe.ne.6) then
              L_ivabe = .false.
              L_(984) = .false.
          endif
      else
          L_(984) = .false.
      endif
C FDA90_logic.fgi( 135, 918):��� ������� ���������,EC004
      if(L_ivabe.and..not.L0_evabe) then
         R0_utabe=R0_avabe
      else
         R0_utabe=max(R_(262)-deltat,0.0)
      endif
      L_(983)=R0_utabe.gt.0.0
      L0_evabe=L_ivabe
C FDA90_logic.fgi( 161, 918):������������  �� T
      L_(982)=.not.L_(1246) .and.L_(983)
      L_(980)=L_(983).and..not.L_(981)
C FDA90_logic.fgi( 184, 916):��������� ������
      iv2=0
      if(L_(1190)) iv2=ibset(iv2,0)
      if(L_(1162)) iv2=ibset(iv2,1)
      if(L_(1009)) iv2=ibset(iv2,2)
      if(L_(982)) iv2=ibset(iv2,3)
      L_(220)=L_(220).or.iv2.ne.0
C FDA90_logic.fgi( 551,1288):������-�������: ������� ������ ������/����������/����� ���������
      if(L_itobe.and..not.L0_etobe) then
         R0_usobe=R0_atobe
      else
         R0_usobe=max(R_(322)-deltat,0.0)
      endif
      L_(1175)=R0_usobe.gt.0.0
      L0_etobe=L_itobe
C FDA90_logic.fgi( 531,1241):������������  �� T
      L_(1174)=.not.L_(1246) .and.L_(1175)
      L_(1172)=L_(1175).and..not.L_(1173)
C FDA90_logic.fgi( 551,1242):��������� ������
      L_(1150)=R_axobe.gt.R0_imobe
C FDA90_logic.fgi( 475,1155):���������� >
      L_(1149)=R_emobe.gt.R0_amobe
C FDA90_logic.fgi( 475,1148):���������� >
      L_(1155) = L_(1150).AND.L_(1149)
C FDA90_logic.fgi( 484,1154):�
      L_(1154)=.not.L_(1246) .and.L_umobe
      L_(1152)=L_umobe.and..not.L_(1153)
C FDA90_logic.fgi( 552,1154):��������� ������
      if(L_edebe.and..not.L0_adebe) then
         R0_obebe=R0_ubebe
      else
         R0_obebe=max(R_(269)-deltat,0.0)
      endif
      L_(1000)=R0_obebe.gt.0.0
      L0_adebe=L_edebe
C FDA90_logic.fgi( 161, 976):������������  �� T
      L_(999)=.not.L_(1246) .and.L_(1000)
      L_(997)=L_(1000).and..not.L_(998)
C FDA90_logic.fgi( 181, 976):��������� ������
      if(L_esabe.and..not.L0_asabe) then
         R0_orabe=R0_urabe
      else
         R0_orabe=max(R_(258)-deltat,0.0)
      endif
      L_(967)=R0_orabe.gt.0.0
      L0_asabe=L_esabe
C FDA90_logic.fgi( 161, 856):������������  �� T
      L_(966)=.not.L_(1246) .and.L_(967)
      L_(964)=L_(967).and..not.L_(965)
C FDA90_logic.fgi( 181, 856):��������� ������
      iv2=0
      if(L_(1174)) iv2=ibset(iv2,0)
      if(L_(1154)) iv2=ibset(iv2,1)
      if(L_(999)) iv2=ibset(iv2,2)
      if(L_(966)) iv2=ibset(iv2,3)
      L_(219)=L_(219).or.iv2.ne.0
C FDA90_logic.fgi( 551,1242):������-�������: ������� ������ ������/����������/����� ���������
      L_(1159)=.not.L_(1246) .and.L_epobe
      L_(1157)=L_epobe.and..not.L_(1158)
C FDA90_logic.fgi( 555,1178):��������� ������
      L_(1006)=.not.L_(1246) .and.L_idebe
      L_(1004)=L_idebe.and..not.L_(1005)
C FDA90_logic.fgi( 181, 994):��������� ������
      if(L_akebe) then
          if (L_(984)) then
              I_ikebe = 7
              L_itabe = .true.
              L_(976) = .false.
          endif
          if (L_akube) then
              L_(976) = .true.
              L_itabe = .false.
          endif
          if (I_ikebe.ne.7) then
              L_itabe = .false.
              L_(976) = .false.
          endif
      else
          L_(976) = .false.
      endif
C FDA90_logic.fgi( 135, 898):��� ������� ���������,EC004
      L_(979)=.not.L_(1246) .and.L_itabe
      L_(977)=L_itabe.and..not.L_(978)
C FDA90_logic.fgi( 184, 896):��������� ������
      iv2=0
      if(L_(1159)) iv2=ibset(iv2,0)
      if(L_(1006)) iv2=ibset(iv2,1)
      if(L_(979)) iv2=ibset(iv2,2)
      L_(218)=L_(218).or.iv2.ne.0
C FDA90_logic.fgi( 555,1178):������-�������: ������� ������ ������/����������/����� ���������
      Call FDA90_1(ext_deltat)
      Call FDA90_2(ext_deltat)
      Call FDA90_3(ext_deltat)
      Call FDA90_4(ext_deltat)
      End
      Subroutine FDA90_1(ext_deltat)
      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      Include 'FDA90.fh'

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      L_(1181)=.not.L_(1246) .and.L_utobe
      L_(1179)=L_utobe.and..not.L_(1180)
C FDA90_logic.fgi( 554,1254):��������� ������
      iv2=0
      if(L_(1006)) iv2=ibset(iv2,0)
      if(L_(1181)) iv2=ibset(iv2,1)
      L_(217)=L_(217).or.iv2.ne.0
C FDA90_logic.fgi( 181, 994):������-�������: ������� ������ ������/����������/����� ���������
      Call PEREGRUZ_USTR_HANDLER(deltat,L_olam,L_(216),L_otobe
     &,L_(217),R_usam,
     & R_atam,R_upam,R_ipam,L_isam,R_aram,R_opam,
     & L_osam,L_amam,L_(218),L_umam,R_apam,R_epam,
     & R_esam,R_axobe,R_asam,L_omam,L_imam,R_ibam,
     & REAL(60.0,4),R_okam,REAL(R_alam,4),R_ifam,
     & REAL(R_ufam,4),L_ekam,R_akam,REAL(R_ikam,4),
     & L_ofam,L_ilam,L_ukam,L_ulam,L_eram,L_(220),L_(219)
     &,
     & L_iram,L_uram,L_emam)
C FDA90_vlv.fgi( 417, 171):���������� ������. ����.,20FDA91AE007
C sav1=L_idebe
C sav2=L_(1003)
      if(L_akebe) then
          if (L_(1011)) then
              I_ikebe = 2
              L_idebe = .true.
              L_(1003) = .false.
          endif
          if (L_otobe) then
              L_(1003) = .true.
              L_idebe = .false.
          endif
          if (I_ikebe.ne.2) then
              L_idebe = .false.
              L_(1003) = .false.
          endif
      else
          L_(1003) = .false.
      endif
C FDA90_logic.fgi( 135, 996):recalc:��� ������� ���������,EC004
C if(sav1.ne.L_idebe .and. try544.gt.0) goto 544
C if(sav2.ne.L_(1003) .and. try544.gt.0) goto 544
      L_(969)=R_axobe.gt.R0_irabe
C FDA90_logic.fgi( 109, 856):���������� >
      L_(971)=R_ukube.gt.R0_isabe
C FDA90_logic.fgi( 109, 877):���������� >
      if(L_akebe) then
          if (L_(976)) then
              I_ikebe = 8
              L_etabe = .true.
              L_(970) = .false.
          endif
          if (L_(971)) then
              L_(970) = .true.
              L_etabe = .false.
          endif
          if (I_ikebe.ne.8) then
              L_etabe = .false.
              L_(970) = .false.
          endif
      else
          L_(970) = .false.
      endif
C FDA90_logic.fgi( 135, 877):��� ������� ���������,EC004
      if(L_akebe) then
          if (L_(970)) then
              I_ikebe = 9
              L_esabe = .true.
              L_(968) = .false.
          endif
          if (L_(969)) then
              L_(968) = .true.
              L_esabe = .false.
          endif
          if (I_ikebe.ne.9) then
              L_esabe = .false.
              L_(968) = .false.
          endif
      else
          L_(968) = .false.
      endif
C FDA90_logic.fgi( 135, 856):��� ������� ���������,EC004
C sav1=L_(967)
      if(L_esabe.and..not.L0_asabe) then
         R0_orabe=R0_urabe
      else
         R0_orabe=max(R_(258)-deltat,0.0)
      endif
      L_(967)=R0_orabe.gt.0.0
      L0_asabe=L_esabe
C FDA90_logic.fgi( 161, 856):recalc:������������  �� T
C if(sav1.ne.L_(967) .and. try580.gt.0) goto 580
      if(L_akebe) then
          if (L_(968)) then
              I_ikebe = 10
              L_erabe = .true.
              L_(963) = .false.
          endif
          if (L_asobe) then
              L_(963) = .true.
              L_erabe = .false.
          endif
          if (I_ikebe.ne.10) then
              L_erabe = .false.
              L_(963) = .false.
          endif
      else
          L_(963) = .false.
      endif
C FDA90_logic.fgi( 135, 841):��� ������� ���������,EC004
      if(L_akebe) then
          if (L_(963)) then
              I_ikebe = 11
              L_ipabe = .true.
              L_(958) = .false.
          endif
          if (L_ofube) then
              L_(958) = .true.
              L_ipabe = .false.
          endif
          if (I_ikebe.ne.11) then
              L_ipabe = .false.
              L_(958) = .false.
          endif
      else
          L_(958) = .false.
      endif
C FDA90_logic.fgi( 135, 822):��� ������� ���������,EC004
      if(L_akebe) then
          if (L_(958)) then
              I_ikebe = 12
              L_omabe = .true.
              L_(952) = .false.
          endif
          if (L_(953)) then
              L_(952) = .true.
              L_omabe = .false.
          endif
          if (I_ikebe.ne.12) then
              L_omabe = .false.
              L_(952) = .false.
          endif
      else
          L_(952) = .false.
      endif
C FDA90_logic.fgi( 135, 801):��� ������� ���������,EC004
      if(L_akebe) then
          if (L_(952)) then
              I_ikebe = 13
              L_olabe = .true.
              L_(945) = .false.
          endif
          if (L_(946)) then
              L_(945) = .true.
              L_olabe = .false.
          endif
          if (I_ikebe.ne.13) then
              L_olabe = .false.
              L_(945) = .false.
          endif
      else
          L_(945) = .false.
      endif
C FDA90_logic.fgi( 135, 783):��� ������� ���������,EC004
      if(L_akebe) then
          if (L_(945)) then
              I_ikebe = 14
              L_okabe = .true.
              L_(944) = .false.
          endif
          if (L_isube) then
              L_(944) = .true.
              L_okabe = .false.
          endif
          if (I_ikebe.ne.14) then
              L_okabe = .false.
              L_(944) = .false.
          endif
      else
          L_(944) = .false.
      endif
C FDA90_logic.fgi( 135, 767):��� ������� ���������,EC004
      if(L_akebe) then
          if (L_(944)) then
              I_ikebe = 15
              L_ikabe = .true.
              L_(939) = .false.
          endif
          if (L_(940)) then
              L_(939) = .true.
              L_ikabe = .false.
          endif
          if (I_ikebe.ne.15) then
              L_ikabe = .false.
              L_(939) = .false.
          endif
      else
          L_(939) = .false.
      endif
C FDA90_logic.fgi( 135, 751):��� ������� ���������,EC004
      if(L_akebe) then
          if (L_(939)) then
              I_ikebe = 16
              L_ifabe = .true.
              L_(934) = .false.
          endif
          if (L_ulube) then
              L_(934) = .true.
              L_ifabe = .false.
          endif
          if (I_ikebe.ne.16) then
              L_ifabe = .false.
              L_(934) = .false.
          endif
      else
          L_(934) = .false.
      endif
C FDA90_logic.fgi( 135, 735):��� ������� ���������,EC004
      if(L_akebe) then
          if (L_(934)) then
              I_ikebe = 17
              L_odabe = .true.
              L_(924) = .false.
          endif
          if (L_(925)) then
              L_(924) = .true.
              L_odabe = .false.
          endif
          if (I_ikebe.ne.17) then
              L_odabe = .false.
              L_(924) = .false.
          endif
      else
          L_(924) = .false.
      endif
C FDA90_logic.fgi( 135, 718):��� ������� ���������,EC004
      if(L_akebe) then
          if (L_(924)) then
              I_ikebe = 18
              L_ibabe = .true.
              L_(922) = .false.
          endif
          if (L_(923)) then
              L_(922) = .true.
              L_ibabe = .false.
          endif
          if (I_ikebe.ne.18) then
              L_ibabe = .false.
              L_(922) = .false.
          endif
      else
          L_(922) = .false.
      endif
C FDA90_logic.fgi( 135, 700):��� ������� ���������,EC004
      if(L_akebe) then
          if (L_(922)) then
              I_ikebe = 19
              L_oxux = .true.
              L_(916) = .false.
          endif
          if (L_(917)) then
              L_(916) = .true.
              L_oxux = .false.
          endif
          if (I_ikebe.ne.19) then
              L_oxux = .false.
              L_(916) = .false.
          endif
      else
          L_(916) = .false.
      endif
C FDA90_logic.fgi( 135, 684):��� ������� ���������,EC004
      if(L_akebe) then
          if (L_(916)) then
              I_ikebe = 20
              L_irux = .true.
              L_(892) = .false.
          endif
          if (L_erux) then
              L_(892) = .true.
              L_irux = .false.
          endif
          if (I_ikebe.ne.20) then
              L_irux = .false.
              L_(892) = .false.
          endif
      else
          L_(892) = .false.
      endif
C FDA90_logic.fgi( 135, 671):��� ������� ���������,EC004
      if(L_akebe) then
          if (L_(892)) then
              I_ikebe = 21
              L_osux = .true.
              L_(893) = .false.
          endif
          if (L_(894)) then
              L_(893) = .true.
              L_osux = .false.
          endif
          if (I_ikebe.ne.21) then
              L_osux = .false.
              L_(893) = .false.
          endif
      else
          L_(893) = .false.
      endif
C FDA90_logic.fgi( 135, 654):��� ������� ���������,EC004
      if(L_akebe) then
          if (L_(893)) then
              I_ikebe = 22
              L_upux = .true.
              L_(891) = .false.
          endif
          if (L_(880)) then
              L_(891) = .true.
              L_upux = .false.
          endif
          if (I_ikebe.ne.22) then
              L_upux = .false.
              L_(891) = .false.
          endif
      else
          L_(891) = .false.
      endif
C FDA90_logic.fgi( 135, 634):��� ������� ���������,EC004
      if(L_akebe) then
          if (L_(891)) then
              I_ikebe = 23
              L_orux = .true.
              L_(903) = .false.
          endif
          if (L_ufobe) then
              L_(903) = .true.
              L_orux = .false.
          endif
          if (I_ikebe.ne.23) then
              L_orux = .false.
              L_(903) = .false.
          endif
      else
          L_(903) = .false.
      endif
C FDA90_logic.fgi( 135, 614):��� ������� ���������,EC004
      if(L_akebe) then
          if (L_(903)) then
              I_ikebe = 24
              L_evux = .true.
              L_(909) = .false.
          endif
          if (L_(904)) then
              L_(909) = .true.
              L_evux = .false.
          endif
          if (I_ikebe.ne.24) then
              L_evux = .false.
              L_(909) = .false.
          endif
      else
          L_(909) = .false.
      endif
C FDA90_logic.fgi( 135, 595):��� ������� ���������,EC004
      if(L_akebe) then
          if (L_(909)) then
              I_ikebe = 25
              L_ovux = .true.
              L_(910) = .false.
          endif
          if (L_(911)) then
              L_(910) = .true.
              L_ovux = .false.
          endif
          if (I_ikebe.ne.25) then
              L_ovux = .false.
              L_(910) = .false.
          endif
      else
          L_(910) = .false.
      endif
C FDA90_logic.fgi( 135, 580):��� ������� ���������,EC004
      if(L_ovux.and..not.L0_utux) then
         R0_itux=R0_otux
      else
         R0_itux=max(R_(235)-deltat,0.0)
      endif
      L_(908)=R0_itux.gt.0.0
      L0_utux=L_ovux
C FDA90_logic.fgi( 161, 580):������������  �� T
      L_(907)=.not.L_(1246) .and.L_(908)
      L_(905)=L_(908).and..not.L_(906)
C FDA90_logic.fgi( 181, 580):��������� ������
      if(L_osux.and..not.L0_isux) then
         R0_asux=R0_esux
      else
         R0_asux=max(R_(233)-deltat,0.0)
      endif
      L_(902)=R0_asux.gt.0.0
      L0_isux=L_osux
C FDA90_logic.fgi( 161, 654):������������  �� T
      L_(901)=.not.L_(1246) .and.L_(902)
      L_(899)=L_(902).and..not.L_(900)
C FDA90_logic.fgi( 181, 654):��������� ������
      L_(871)=R_emobe.lt.R0_alux
C FDA90_logic.fgi( 293,1019):���������� <
      if(L_ulux) then
          if (L_(872)) then
              I_emux = 1
              L_elux = .true.
              L_(870) = .false.
          endif
          if (L_(871)) then
              L_(870) = .true.
              L_elux = .false.
          endif
          if (I_emux.ne.1) then
              L_elux = .false.
              L_(870) = .false.
          endif
      else
          L_(870) = .false.
      endif
C FDA90_logic.fgi( 321,1019):��� ������� ���������,EC005
      if(L_elux.and..not.L0_akux) then
         R0_ofux=R0_ufux
      else
         R0_ofux=max(R_(224)-deltat,0.0)
      endif
      L_(869)=R0_ofux.gt.0.0
      L0_akux=L_elux
C FDA90_logic.fgi( 347,1019):������������  �� T
      L_(868)=.not.L_(1246) .and.L_(869)
      L_(866)=L_(869).and..not.L_(867)
C FDA90_logic.fgi( 367,1020):��������� ������
      L_(852)=R_emobe.lt.R0_adux
C FDA90_logic.fgi( 293, 969):���������� <
      L_(865)=R_emobe.gt.R0_okux
C FDA90_logic.fgi( 293, 989):���������� >
      if(L_ulux) then
          if (L_(870)) then
              I_emux = 2
              L_ekux = .true.
              L_(863) = .false.
          endif
          if (L_ufobe) then
              L_(863) = .true.
              L_ekux = .false.
          endif
          if (I_emux.ne.2) then
              L_ekux = .false.
              L_(863) = .false.
          endif
      else
          L_(863) = .false.
      endif
C FDA90_logic.fgi( 321,1002):��� ������� ���������,EC005
      if(L_ulux) then
          if (L_(863)) then
              I_emux = 3
              L_ukux = .true.
              L_(864) = .false.
          endif
          if (L_(865)) then
              L_(864) = .true.
              L_ukux = .false.
          endif
          if (I_emux.ne.3) then
              L_ukux = .false.
              L_(864) = .false.
          endif
      else
          L_(864) = .false.
      endif
C FDA90_logic.fgi( 321, 989):��� ������� ���������,EC005
      if(L_ulux) then
          if (L_(864)) then
              I_emux = 4
              L_udux = .true.
              L_(861) = .false.
          endif
          if (L_(852)) then
              L_(861) = .true.
              L_udux = .false.
          endif
          if (I_emux.ne.4) then
              L_udux = .false.
              L_(861) = .false.
          endif
      else
          L_(861) = .false.
      endif
C FDA90_logic.fgi( 321, 969):��� ������� ���������,EC005
      if(L_udux.and..not.L0_odux) then
         R0_edux=R0_idux
      else
         R0_edux=max(R_(220)-deltat,0.0)
      endif
      L_(860)=R0_edux.gt.0.0
      L0_odux=L_udux
C FDA90_logic.fgi( 347, 969):������������  �� T
      L_(859)=.not.L_(1246) .and.L_(860)
      L_(857)=L_(860).and..not.L_(858)
C FDA90_logic.fgi( 367, 970):��������� ������
      L_(537)=R_emobe.lt.R0_oxuv
C FDA90_logic.fgi( 481, 415):���������� <
      L_(530)=R_emobe.gt.R0_exuv
C FDA90_logic.fgi( 481, 430):���������� >
      L_(509)=R_atube.gt.R0_isuv
C FDA90_logic.fgi( 482, 469):���������� >
      L_(520)=R_emobe.lt.R0_atuv
C FDA90_logic.fgi( 482, 489):���������� <
      L_(543)=R_erube.lt.R0_abax
C FDA90_logic.fgi( 482, 519):���������� <
      L_(549)=R_atube.lt.R0_udax
C FDA90_logic.fgi( 482, 535):���������� <
      L_(551)=R_ukube.gt.R0_afax
C FDA90_logic.fgi( 482, 553):���������� >
      L_(566)=R_erube.gt.R0_ukax
C FDA90_logic.fgi( 482, 586):���������� >
      L_(572)=R_ukube.lt.R0_amax
C FDA90_logic.fgi( 482, 618):���������� <
      L_(579)=R_erube.lt.R0_apax
C FDA90_logic.fgi( 482, 636):���������� <
      L_(786)=R_ilox.lt.R0_ikox
C FDA90_logic.fgi( 295, 789):���������� <
      L_(815)=R_erube.lt.R0_orox
C FDA90_logic.fgi( 295, 821):���������� <
      L_(823)=R_erube.gt.R0_utox
C FDA90_logic.fgi( 293, 872):���������� >
      L_(822)=R_atube.gt.R0_otox
C FDA90_logic.fgi( 293, 864):���������� >
      L_(821)=R_ukube.lt.R0_itox
C FDA90_logic.fgi( 293, 855):���������� <
      L_(829) = L_(823).AND.L_(822).AND.L_(821)
C FDA90_logic.fgi( 301, 870):�
      L_(845)=R_atube.lt.R0_ubux
C FDA90_logic.fgi( 293, 909):���������� <
      L_(841)=R_erube.lt.R0_axox
C FDA90_logic.fgi( 293, 930):���������� <
      if(L_ulux) then
          if (L_(861)) then
              I_emux = 5
              L_ikux = .true.
              L_(862) = .false.
          endif
          if (L_erux) then
              L_(862) = .true.
              L_ikux = .false.
          endif
          if (I_emux.ne.5) then
              L_ikux = .false.
              L_(862) = .false.
          endif
      else
          L_(862) = .false.
      endif
C FDA90_logic.fgi( 321, 950):��� ������� ���������,EC005
      if(L_ulux) then
          if (L_(862)) then
              I_emux = 6
              L_uxox = .true.
              L_(843) = .false.
          endif
          if (L_(841)) then
              L_(843) = .true.
              L_uxox = .false.
          endif
          if (I_emux.ne.6) then
              L_uxox = .false.
              L_(843) = .false.
          endif
      else
          L_(843) = .false.
      endif
C FDA90_logic.fgi( 321, 930):��� ������� ���������,EC005
      if(L_ulux) then
          if (L_(843)) then
              I_emux = 7
              L_obux = .true.
              L_(844) = .false.
          endif
          if (L_(845)) then
              L_(844) = .true.
              L_obux = .false.
          endif
          if (I_emux.ne.7) then
              L_obux = .false.
              L_(844) = .false.
          endif
      else
          L_(844) = .false.
      endif
C FDA90_logic.fgi( 321, 909):��� ������� ���������,EC005
      if(L_ulux) then
          if (L_(844)) then
              I_emux = 8
              L_uvox = .true.
              L_(833) = .false.
          endif
          if (L_isube) then
              L_(833) = .true.
              L_uvox = .false.
          endif
          if (I_emux.ne.8) then
              L_uvox = .false.
              L_(833) = .false.
          endif
      else
          L_(833) = .false.
      endif
C FDA90_logic.fgi( 321, 890):��� ������� ���������,EC005
      if(L_ulux) then
          if (L_(833)) then
              I_emux = 9
              L_ovox = .true.
              L_(828) = .false.
          endif
          if (L_(829)) then
              L_(828) = .true.
              L_ovox = .false.
          endif
          if (I_emux.ne.9) then
              L_ovox = .false.
              L_(828) = .false.
          endif
      else
          L_(828) = .false.
      endif
C FDA90_logic.fgi( 321, 870):��� ������� ���������,EC005
      if(L_ulux) then
          if (L_(828)) then
              I_emux = 10
              L_etox = .true.
              L_(820) = .false.
          endif
          if (L_ofube) then
              L_(820) = .true.
              L_etox = .false.
          endif
          if (I_emux.ne.10) then
              L_etox = .false.
              L_(820) = .false.
          endif
      else
          L_(820) = .false.
      endif
C FDA90_logic.fgi( 321, 839):��� ������� ���������,EC005
      if(L_ulux) then
          if (L_(820)) then
              I_emux = 11
              L_isox = .true.
              L_(814) = .false.
          endif
          if (L_(815)) then
              L_(814) = .true.
              L_isox = .false.
          endif
          if (I_emux.ne.11) then
              L_isox = .false.
              L_(814) = .false.
          endif
      else
          L_(814) = .false.
      endif
C FDA90_logic.fgi( 321, 821):��� ������� ���������,EC005
      if(L_ulux) then
          if (L_(814)) then
              I_emux = 12
              L_irox = .true.
              L_(809) = .false.
          endif
          if (L_akube) then
              L_(809) = .true.
              L_irox = .false.
          endif
          if (I_emux.ne.12) then
              L_irox = .false.
              L_(809) = .false.
          endif
      else
          L_(809) = .false.
      endif
C FDA90_logic.fgi( 321, 808):��� ������� ���������,EC005
      if(L_ulux) then
          if (L_(809)) then
              I_emux = 13
              L_elox = .true.
              L_(793) = .false.
          endif
          if (L_(786)) then
              L_(793) = .true.
              L_elox = .false.
          endif
          if (I_emux.ne.13) then
              L_elox = .false.
              L_(793) = .false.
          endif
      else
          L_(793) = .false.
      endif
C FDA90_logic.fgi( 321, 789):��� ������� ���������,EC005
      if(L_elox.and..not.L0_alox) then
         R0_okox=R0_ukox
      else
         R0_okox=max(R_(201)-deltat,0.0)
      endif
      L_(785)=R0_okox.gt.0.0
      L0_alox=L_elox
C FDA90_logic.fgi( 347, 789):������������  �� T
      L_(784)=.not.L_(1246) .and.L_(785)
      L_(782)=L_(785).and..not.L_(783)
C FDA90_logic.fgi( 367, 790):��������� ������
      L_(761)=R_ilox.lt.R0_axix
C FDA90_logic.fgi( 295, 660):���������� <
      L_(772)=R_ukube.lt.R0_adox
C FDA90_logic.fgi( 296, 690):���������� <
      L_(778)=R_ilox.gt.R0_afox
C FDA90_logic.fgi( 295, 707):���������� >
      L_(792)=R_erube.gt.R0_olox
C FDA90_logic.fgi( 295, 737):���������� >
      L_(795)=R_ukube.gt.R0_omox
C FDA90_logic.fgi( 295, 775):���������� >
      if(L_ulux) then
          if (L_(793)) then
              I_emux = 15
              L_ipox = .true.
              L_(794) = .false.
          endif
          if (L_(795)) then
              L_(794) = .true.
              L_ipox = .false.
          endif
          if (I_emux.ne.15) then
              L_ipox = .false.
              L_(794) = .false.
          endif
      else
          L_(794) = .false.
      endif
C FDA90_logic.fgi( 321, 775):��� ������� ���������,EC005
      if(L_ulux) then
          if (L_(794)) then
              I_emux = 14
              L_akox = .true.
              L_(791) = .false.
          endif
          if (L_ekox) then
              L_(791) = .true.
              L_akox = .false.
          endif
          if (I_emux.ne.14) then
              L_akox = .false.
              L_(791) = .false.
          endif
      else
          L_(791) = .false.
      endif
C FDA90_logic.fgi( 321, 759):��� ������� ���������,EC005
      if(L_ulux) then
          if (L_(791)) then
              I_emux = 16
              L_imox = .true.
              L_(804) = .false.
          endif
          if (L_(792)) then
              L_(804) = .true.
              L_imox = .false.
          endif
          if (I_emux.ne.16) then
              L_imox = .false.
              L_(804) = .false.
          endif
      else
          L_(804) = .false.
      endif
C FDA90_logic.fgi( 321, 737):��� ������� ���������,EC005
      if(L_ulux) then
          if (L_(804)) then
              I_emux = 17
              L_erox = .true.
              L_(805) = .false.
          endif
          if (L_ulube) then
              L_(805) = .true.
              L_erox = .false.
          endif
          if (I_emux.ne.17) then
              L_erox = .false.
              L_(805) = .false.
          endif
      else
          L_(805) = .false.
      endif
C FDA90_logic.fgi( 321, 723):��� ������� ���������,EC005
      if(L_ulux) then
          if (L_(805)) then
              I_emux = 18
              L_ufox = .true.
              L_(777) = .false.
          endif
          if (L_(778)) then
              L_(777) = .true.
              L_ufox = .false.
          endif
          if (I_emux.ne.18) then
              L_ufox = .false.
              L_(777) = .false.
          endif
      else
          L_(777) = .false.
      endif
C FDA90_logic.fgi( 321, 707):��� ������� ���������,EC005
      if(L_ulux) then
          if (L_(777)) then
              I_emux = 19
              L_udox = .true.
              L_(771) = .false.
          endif
          if (L_(772)) then
              L_(771) = .true.
              L_udox = .false.
          endif
          if (I_emux.ne.19) then
              L_udox = .false.
              L_(771) = .false.
          endif
      else
          L_(771) = .false.
      endif
C FDA90_logic.fgi( 321, 690):��� ������� ���������,EC005
      if(L_ulux) then
          if (L_(771)) then
              I_emux = 20
              L_ubox = .true.
              L_(766) = .false.
          endif
          if (L_obox) then
              L_(766) = .true.
              L_ubox = .false.
          endif
          if (I_emux.ne.20) then
              L_ubox = .false.
              L_(766) = .false.
          endif
      else
          L_(766) = .false.
      endif
C FDA90_logic.fgi( 321, 675):��� ������� ���������,EC005
      if(L_ulux) then
          if (L_(766)) then
              I_emux = 21
              L_uxix = .true.
              L_(760) = .false.
          endif
          if (L_(761)) then
              L_(760) = .true.
              L_uxix = .false.
          endif
          if (I_emux.ne.21) then
              L_uxix = .false.
              L_(760) = .false.
          endif
      else
          L_(760) = .false.
      endif
C FDA90_logic.fgi( 321, 660):��� ������� ���������,EC005
      if(L_uxix.and..not.L0_oxix) then
         R0_exix=R0_ixix
      else
         R0_exix=max(R_(192)-deltat,0.0)
      endif
      L_(759)=R0_exix.gt.0.0
      L0_oxix=L_uxix
C FDA90_logic.fgi( 347, 660):������������  �� T
      L_(758)=.not.L_(1246) .and.L_(759)
      L_(756)=L_(759).and..not.L_(757)
C FDA90_logic.fgi( 367, 660):��������� ������
      L_(626)=R_ilox.lt.R0_idex
C FDA90_logic.fgi( 482, 821):���������� <
      L_(638)=R_ukube.lt.R0_ekex
C FDA90_logic.fgi( 483, 853):���������� <
      L_(465)=R_imix.lt.R0_ipov
C FDA90_logic.fgi( 482, 805):���������� <
      if(L_ikuv.and..not.L0_ekuv) then
         R0_ufuv=R0_akuv
      else
         R0_ufuv=max(R_(99)-deltat,0.0)
      endif
      L_(469)=R0_ufuv.gt.0.0
      L0_ekuv=L_ikuv
C FDA90_logic.fgi( 534, 805):������������  �� T
      L_(468)=.not.L_(1246) .and.L_(469)
      L_(466)=L_(469).and..not.L_(467)
C FDA90_logic.fgi( 556, 806):��������� ������
      L_(674)=R_imix.lt.R0_opov
C FDA90_logic.fgi( 288, 398):���������� <
      L_(676)=R_emobe.gt.R0_usex
C FDA90_logic.fgi( 288, 391):���������� >
      L_(749)=R_atix.lt.R0_usix
C FDA90_logic.fgi( 295, 611):���������� <
      if(L_ulux) then
          if (L_(760)) then
              I_emux = 22
              L_opix = .true.
              L_(754) = .false.
          endif
          if (L_ipix) then
              L_(754) = .true.
              L_opix = .false.
          endif
          if (I_emux.ne.22) then
              L_opix = .false.
              L_(754) = .false.
          endif
      else
          L_(754) = .false.
      endif
C FDA90_logic.fgi( 321, 645):��� ������� ���������,EC005
      if(L_ulux) then
          if (L_(754)) then
              I_emux = 23
              L_uvix = .true.
              L_(755) = .false.
          endif
          if (L_ovix) then
              L_(755) = .true.
              L_uvix = .false.
          endif
          if (I_emux.ne.23) then
              L_uvix = .false.
              L_(755) = .false.
          endif
      else
          L_(755) = .false.
      endif
C FDA90_logic.fgi( 321, 626):��� ������� ���������,EC005
      if(L_ulux) then
          if (L_(755)) then
              I_emux = 24
              L_utix = .true.
              L_(748) = .false.
          endif
          if (L_(749)) then
              L_(748) = .true.
              L_utix = .false.
          endif
          if (I_emux.ne.24) then
              L_utix = .false.
              L_(748) = .false.
          endif
      else
          L_(748) = .false.
      endif
C FDA90_logic.fgi( 321, 611):��� ������� ���������,EC005
      if(L_utix.and..not.L0_otix) then
         R0_etix=R0_itix
      else
         R0_etix=max(R_(188)-deltat,0.0)
      endif
      L_(747)=R0_etix.gt.0.0
      L0_otix=L_utix
C FDA90_logic.fgi( 347, 611):������������  �� T
      L_(746)=.not.L_(1246) .and.L_(747)
      L_(744)=L_(747).and..not.L_(745)
C FDA90_logic.fgi( 367, 612):��������� ������
      L_(696)=R_atix.lt.R0_abix
C FDA90_logic.fgi( 295, 473):���������� <
      L_(656)=R_adix.lt.R0_ipex
C FDA90_logic.fgi( 482, 992):���������� <
      L_(667)=R_atix.lt.R0_irex
C FDA90_logic.fgi( 482,1011):���������� <
      if(L_otex) then
          if (L_(683)) then
              I_avex = 1
              L_ifuv = .true.
              L_(665) = .false.
          endif
          if (L_oxex) then
              L_(665) = .true.
              L_ifuv = .false.
          endif
          if (I_avex.ne.1) then
              L_ifuv = .false.
              L_(665) = .false.
          endif
      else
          L_(665) = .false.
      endif
C FDA90_logic.fgi( 508,1025):��� ������� ���������,EC006
      if(L_otex) then
          if (L_(665)) then
              I_avex = 2
              L_esex = .true.
              L_(666) = .false.
          endif
          if (L_(667)) then
              L_(666) = .true.
              L_esex = .false.
          endif
          if (I_avex.ne.2) then
              L_esex = .false.
              L_(666) = .false.
          endif
      else
          L_(666) = .false.
      endif
C FDA90_logic.fgi( 508,1011):��� ������� ���������,EC006
      if(L_otex) then
          if (L_(666)) then
              I_avex = 3
              L_erex = .true.
              L_(655) = .false.
          endif
          if (L_(656)) then
              L_(655) = .true.
              L_erex = .false.
          endif
          if (I_avex.ne.3) then
              L_erex = .false.
              L_(655) = .false.
          endif
      else
          L_(655) = .false.
      endif
C FDA90_logic.fgi( 508, 992):��� ������� ���������,EC006
      if(L_erex.and..not.L0_arex) then
         R0_opex=R0_upex
      else
         R0_opex=max(R_(160)-deltat,0.0)
      endif
      L_(660)=R0_opex.gt.0.0
      L0_arex=L_erex
C FDA90_logic.fgi( 534, 992):������������  �� T
      L_(659)=.not.L_(1246) .and.L_(660)
      L_(657)=L_(660).and..not.L_(658)
C FDA90_logic.fgi( 556, 990):��������� ������
      if(L_udix.and..not.L0_odix) then
         R0_edix=R0_idix
      else
         R0_edix=max(R_(171)-deltat,0.0)
      endif
      L_(702)=R0_edix.gt.0.0
      L0_odix=L_udix
C FDA90_logic.fgi( 347, 488):������������  �� T
      L_(701)=.not.L_(1246) .and.L_(702)
      L_(699)=L_(702).and..not.L_(700)
C FDA90_logic.fgi( 369, 488):��������� ������
      iv2=0
      if(L_(659)) iv2=ibset(iv2,0)
      if(L_(701)) iv2=ibset(iv2,1)
      L_(185)=L_(185).or.iv2.ne.0
C FDA90_logic.fgi( 556, 990):������-�������: ������� ������ ������/����������/����� ���������
      L_(644)=R_adix.gt.R0_elex
C FDA90_logic.fgi( 482, 940):���������� >
      L_(654)=R_atix.gt.R0_imex
C FDA90_logic.fgi( 482, 960):���������� >
      if(L_otex) then
          if (L_(655)) then
              I_avex = 4
              L_emex = .true.
              L_(652) = .false.
          endif
          if (L_alix) then
              L_(652) = .true.
              L_emex = .false.
          endif
          if (I_avex.ne.4) then
              L_emex = .false.
              L_(652) = .false.
          endif
      else
          L_(652) = .false.
      endif
C FDA90_logic.fgi( 508, 977):��� ������� ���������,EC006
      if(L_otex) then
          if (L_(652)) then
              I_avex = 5
              L_epex = .true.
              L_(653) = .false.
          endif
          if (L_(654)) then
              L_(653) = .true.
              L_epex = .false.
          endif
          if (I_avex.ne.5) then
              L_epex = .false.
              L_(653) = .false.
          endif
      else
          L_(653) = .false.
      endif
C FDA90_logic.fgi( 508, 960):��� ������� ���������,EC006
      if(L_otex) then
          if (L_(653)) then
              I_avex = 6
              L_amex = .true.
              L_(643) = .false.
          endif
          if (L_(644)) then
              L_(643) = .true.
              L_amex = .false.
          endif
          if (I_avex.ne.6) then
              L_amex = .false.
              L_(643) = .false.
          endif
      else
          L_(643) = .false.
      endif
C FDA90_logic.fgi( 508, 940):��� ������� ���������,EC006
      if(L_amex.and..not.L0_ulex) then
         R0_ilex=R0_olex
      else
         R0_ilex=max(R_(156)-deltat,0.0)
      endif
      L_(642)=R0_ilex.gt.0.0
      L0_ulex=L_amex
C FDA90_logic.fgi( 534, 940):������������  �� T
      L_(641)=.not.L_(1246) .and.L_(642)
      L_(639)=L_(642).and..not.L_(640)
C FDA90_logic.fgi( 557, 940):��������� ������
      L_(686)=R_adix.gt.R0_ovex
C FDA90_logic.fgi( 295, 433):���������� >
      if(L_ixex.and..not.L0_exex) then
         R0_uvex=R0_axex
      else
         R0_uvex=max(R_(167)-deltat,0.0)
      endif
      L_(684)=R0_uvex.gt.0.0
      L0_exex=L_ixex
C FDA90_logic.fgi( 347, 433):������������  �� T
      L_(670)=.not.L_(1246) .and.L_(684)
      L_(668)=L_(684).and..not.L_(669)
C FDA90_logic.fgi( 369, 434):��������� ������
      iv2=0
      if(L_(641)) iv2=ibset(iv2,0)
      if(L_(670)) iv2=ibset(iv2,1)
      L_(184)=L_(184).or.iv2.ne.0
C FDA90_logic.fgi( 557, 940):������-�������: ������� ������ ������/����������/����� ���������
      L_(461)=.not.L_(1246) .and.L_ifuv
      L_(459)=L_ifuv.and..not.L_(460)
C FDA90_logic.fgi( 556,1026):��������� ������
      L_(689)=.not.L_(1246) .and.L_uxex
      L_(687)=L_uxex.and..not.L_(688)
C FDA90_logic.fgi( 370, 454):��������� ������
      iv2=0
      if(L_(461)) iv2=ibset(iv2,0)
      if(L_(689)) iv2=ibset(iv2,1)
      L_(182)=L_(182).or.iv2.ne.0
C FDA90_logic.fgi( 556,1026):������-�������: ������� ������ ������/����������/����� ���������
      Call TELEGKA_TRANSP_HANDLER(deltat,L_otal,L_(181),L_oxex
     &,L_(182),R_ufel,
     & R_akel,R_ubel,R_ibel,L_ifel,R_adel,R_obel,
     & L_ofel,L_ikal,L_okal,L_(183),L_ixal,R_abel,R_ebel,
     & R_efel,R_adix,R_afel,L_axal,L_uval,R_apal,
     & REAL(R_umal,4),R_osal,REAL(R_atal,4),L_esal,
     & R_asal,REAL(R_isal,4),L_ipal,R_epal,
     & REAL(R_opal,4),L_uxal,L_(186),L_(187),L_usal,
     & L_eval,L_edel,L_(185),L_(184),L_idel,L_udel,L_oval
     &)
C FDA90_vlv.fgi( 194,  56):���������� ������� ������������,20FDA91AE005KE01
C sav1=L_ifuv
C sav2=L_(665)
      if(L_otex) then
          if (L_(683)) then
              I_avex = 1
              L_ifuv = .true.
              L_(665) = .false.
          endif
          if (L_oxex) then
              L_(665) = .true.
              L_ifuv = .false.
          endif
          if (I_avex.ne.1) then
              L_ifuv = .false.
              L_(665) = .false.
          endif
      else
          L_(665) = .false.
      endif
C FDA90_logic.fgi( 508,1025):recalc:��� ������� ���������,EC006
C if(sav1.ne.L_ifuv .and. try878.gt.0) goto 878
C if(sav2.ne.L_(665) .and. try878.gt.0) goto 878
      L_(698)=R_adix.lt.R0_isex
C FDA90_logic.fgi( 295, 488):���������� <
      L_(713)=R_atix.gt.R0_upov
C FDA90_logic.fgi( 295, 519):���������� >
      L_(718)=R_ukube.gt.R0_elix
C FDA90_logic.fgi( 295, 535):���������� >
      L_(743)=R_ilox.gt.R0_urix
C FDA90_logic.fgi( 295, 565):���������� >
      L_(724)=R_imix.gt.R0_emix
C FDA90_logic.fgi( 295, 596):���������� >
      if(L_ulux) then
          if (L_(748)) then
              I_emux = 25
              L_epix = .true.
              L_(723) = .false.
          endif
          if (L_(724)) then
              L_(723) = .true.
              L_epix = .false.
          endif
          if (I_emux.ne.25) then
              L_epix = .false.
              L_(723) = .false.
          endif
      else
          L_(723) = .false.
      endif
C FDA90_logic.fgi( 321, 596):��� ������� ���������,EC005
      if(L_ulux) then
          if (L_(723)) then
              I_emux = 26
              L_ukix = .true.
              L_(741) = .false.
          endif
          if (L_alix) then
              L_(741) = .true.
              L_ukix = .false.
          endif
          if (I_emux.ne.26) then
              L_ukix = .false.
              L_(741) = .false.
          endif
      else
          L_(741) = .false.
      endif
C FDA90_logic.fgi( 321, 580):��� ������� ���������,EC005
      if(L_ulux) then
          if (L_(741)) then
              I_emux = 27
              L_osix = .true.
              L_(742) = .false.
          endif
          if (L_(743)) then
              L_(742) = .true.
              L_osix = .false.
          endif
          if (I_emux.ne.27) then
              L_osix = .false.
              L_(742) = .false.
          endif
      else
          L_(742) = .false.
      endif
C FDA90_logic.fgi( 321, 565):��� ������� ���������,EC005
      if(L_ulux) then
          if (L_(742)) then
              I_emux = 28
              L_orix = .true.
              L_(736) = .false.
          endif
          if (L_irix) then
              L_(736) = .true.
              L_orix = .false.
          endif
          if (I_emux.ne.28) then
              L_orix = .false.
              L_(736) = .false.
          endif
      else
          L_(736) = .false.
      endif
C FDA90_logic.fgi( 321, 551):��� ������� ���������,EC005
      if(L_ulux) then
          if (L_(736)) then
              I_emux = 29
              L_amix = .true.
              L_(717) = .false.
          endif
          if (L_(718)) then
              L_(717) = .true.
              L_amix = .false.
          endif
          if (I_emux.ne.29) then
              L_amix = .false.
              L_(717) = .false.
          endif
      else
          L_(717) = .false.
      endif
C FDA90_logic.fgi( 321, 535):��� ������� ���������,EC005
      if(L_ulux) then
          if (L_(717)) then
              I_emux = 30
              L_okix = .true.
              L_(712) = .false.
          endif
          if (L_(713)) then
              L_(712) = .true.
              L_okix = .false.
          endif
          if (I_emux.ne.30) then
              L_okix = .false.
              L_(712) = .false.
          endif
      else
          L_(712) = .false.
      endif
C FDA90_logic.fgi( 321, 519):��� ������� ���������,EC005
      if(L_ulux) then
          if (L_(712)) then
              I_emux = 31
              L_ufix = .true.
              L_(707) = .false.
          endif
          if (L_ofix) then
              L_(707) = .true.
              L_ufix = .false.
          endif
          if (I_emux.ne.31) then
              L_ufix = .false.
              L_(707) = .false.
          endif
      else
          L_(707) = .false.
      endif
C FDA90_logic.fgi( 321, 504):��� ������� ���������,EC005
      if(L_ulux) then
          if (L_(707)) then
              I_emux = 32
              L_udix = .true.
              L_(697) = .false.
          endif
          if (L_(698)) then
              L_(697) = .true.
              L_udix = .false.
          endif
          if (I_emux.ne.32) then
              L_udix = .false.
              L_(697) = .false.
          endif
      else
          L_(697) = .false.
      endif
C FDA90_logic.fgi( 321, 488):��� ������� ���������,EC005
C sav1=L_(702)
      if(L_udix.and..not.L0_odix) then
         R0_edix=R0_idix
      else
         R0_edix=max(R_(171)-deltat,0.0)
      endif
      L_(702)=R0_edix.gt.0.0
      L0_odix=L_udix
C FDA90_logic.fgi( 347, 488):recalc:������������  �� T
C if(sav1.ne.L_(702) .and. try892.gt.0) goto 892
      if(L_ulux) then
          if (L_(697)) then
              I_emux = 33
              L_ubix = .true.
              L_(695) = .false.
          endif
          if (L_(696)) then
              L_(695) = .true.
              L_ubix = .false.
          endif
          if (I_emux.ne.33) then
              L_ubix = .false.
              L_(695) = .false.
          endif
      else
          L_(695) = .false.
      endif
C FDA90_logic.fgi( 321, 473):��� ������� ���������,EC005
      if(L_ubix.and..not.L0_obix) then
         R0_ebix=R0_ibix
      else
         R0_ebix=max(R_(170)-deltat,0.0)
      endif
      L_(694)=R0_ebix.gt.0.0
      L0_obix=L_ubix
C FDA90_logic.fgi( 347, 473):������������  �� T
      L_(693)=.not.L_(1246) .and.L_(694)
      L_(691)=L_(694).and..not.L_(692)
C FDA90_logic.fgi( 367, 474):��������� ������
      if(L_esex.and..not.L0_asex) then
         R0_orex=R0_urex
      else
         R0_orex=max(R_(163)-deltat,0.0)
      endif
      L_(664)=R0_orex.gt.0.0
      L0_asex=L_esex
C FDA90_logic.fgi( 534,1011):������������  �� T
      L_(663)=.not.L_(1246) .and.L_(664)
      L_(661)=L_(664).and..not.L_(662)
C FDA90_logic.fgi( 554,1012):��������� ������
      L_(486)=R_atix.lt.R0_amuv
C FDA90_logic.fgi( 482, 890):���������� <
      L_(470)=R_imix.gt.R0_okuv
C FDA90_logic.fgi( 482, 926):���������� >
      if(L_otex) then
          if (L_(643)) then
              I_avex = 7
              L_iluv = .true.
              L_(491) = .false.
          endif
          if (L_(470)) then
              L_(491) = .true.
              L_iluv = .false.
          endif
          if (I_avex.ne.7) then
              L_iluv = .false.
              L_(491) = .false.
          endif
      else
          L_(491) = .false.
      endif
C FDA90_logic.fgi( 508, 926):��� ������� ���������,EC006
      if(L_otex) then
          if (L_(491)) then
              I_avex = 8
              L_opuv = .true.
              L_(492) = .false.
          endif
          if (L_ovix) then
              L_(492) = .true.
              L_opuv = .false.
          endif
          if (I_avex.ne.8) then
              L_opuv = .false.
              L_(492) = .false.
          endif
      else
          L_(492) = .false.
      endif
C FDA90_logic.fgi( 508, 911):��� ������� ���������,EC006
      if(L_otex) then
          if (L_(492)) then
              I_avex = 9
              L_umuv = .true.
              L_(496) = .false.
          endif
          if (L_(486)) then
              L_(496) = .true.
              L_umuv = .false.
          endif
          if (I_avex.ne.9) then
              L_umuv = .false.
              L_(496) = .false.
          endif
      else
          L_(496) = .false.
      endif
C FDA90_logic.fgi( 508, 890):��� ������� ���������,EC006
      if(L_umuv.and..not.L0_omuv) then
         R0_emuv=R0_imuv
      else
         R0_emuv=max(R_(106)-deltat,0.0)
      endif
      L_(485)=R0_emuv.gt.0.0
      L0_omuv=L_umuv
C FDA90_logic.fgi( 534, 890):������������  �� T
      L_(484)=.not.L_(1246) .and.L_(485)
      L_(482)=L_(485).and..not.L_(483)
C FDA90_logic.fgi( 554, 890):��������� ������
      iv2=0
      if(L_(746)) iv2=ibset(iv2,0)
      if(L_(693)) iv2=ibset(iv2,1)
      if(L_(663)) iv2=ibset(iv2,2)
      if(L_(484)) iv2=ibset(iv2,3)
      L_(227)=L_(227).or.iv2.ne.0
C FDA90_logic.fgi( 367, 612):������-�������: ������� ������ ������/����������/����� ���������
      if(L_okix.and..not.L0_ikix) then
         R0_akix=R0_ekix
      else
         R0_akix=max(R_(176)-deltat,0.0)
      endif
      L_(711)=R0_akix.gt.0.0
      L0_ikix=L_okix
C FDA90_logic.fgi( 347, 519):������������  �� T
      L_(710)=.not.L_(1246) .and.L_(711)
      L_(708)=L_(711).and..not.L_(709)
C FDA90_logic.fgi( 367, 520):��������� ������
      if(L_epex.and..not.L0_apex) then
         R0_omex=R0_umex
      else
         R0_omex=max(R_(159)-deltat,0.0)
      endif
      L_(651)=R0_omex.gt.0.0
      L0_apex=L_epex
C FDA90_logic.fgi( 534, 960):������������  �� T
      L_(650)=.not.L_(1246) .and.L_(651)
      L_(648)=L_(651).and..not.L_(649)
C FDA90_logic.fgi( 555, 960):��������� ������
      L_(620)=R_atix.lt.R0_ibex
C FDA90_logic.fgi( 482, 778):���������� <
      if(L_edex.and..not.L0_adex) then
         R0_obex=R0_ubex
      else
         R0_obex=max(R_(148)-deltat,0.0)
      endif
      L_(617)=R0_obex.gt.0.0
      L0_adex=L_edex
C FDA90_logic.fgi( 534, 778):������������  �� T
      L_(616)=.not.L_(1246) .and.L_(617)
      L_(614)=L_(617).and..not.L_(615)
C FDA90_logic.fgi( 554, 778):��������� ������
      L_(495)=.not.L_(1246) .and.L_upuv
      L_(493)=L_upuv.and..not.L_(494)
C FDA90_logic.fgi( 557, 872):��������� ������
      iv2=0
      if(L_(689)) iv2=ibset(iv2,0)
      if(L_(659)) iv2=ibset(iv2,1)
      if(L_(495)) iv2=ibset(iv2,2)
      L_(225)=L_(225).or.iv2.ne.0
C FDA90_logic.fgi( 370, 454):������-�������: ������� ������ ������/����������/����� ���������
      L_(647)=.not.L_(1246) .and.L_emex
      L_(645)=L_emex.and..not.L_(646)
C FDA90_logic.fgi( 555, 978):��������� ������
      L_(716)=.not.L_(1246) .and.L_ukix
      L_(714)=L_ukix.and..not.L_(715)
C FDA90_logic.fgi( 367, 580):��������� ������
      iv2=0
      if(L_(647)) iv2=ibset(iv2,0)
      if(L_(716)) iv2=ibset(iv2,1)
      L_(224)=L_(224).or.iv2.ne.0
C FDA90_logic.fgi( 555, 978):������-�������: ������� ������ ������/����������/����� ���������
      L_(675)=R_atix.gt.R0_osex
C FDA90_logic.fgi( 288, 384):���������� >
      L_(682) = L_(674).AND.L_(676).AND.L_(675)
C FDA90_logic.fgi( 300, 391):�
      if(.not.L_etov) then
         R0_elev=0.0
      elseif(.not.L0_ilev) then
         R0_elev=R0_alev
      else
         R0_elev=max(R_(27)-deltat,0.0)
      endif
      L_(252)=L_etov.and.R0_elev.le.0.0
      L0_ilev=L_etov
C FDA90_logic.fgi( 288, 410):�������� ��������� ������
      L_(415) = L_atov.AND.L_(252)
C FDA90_logic.fgi( 298, 413):�
      if(L_ulux) then
          if (L_(695)) then
              I_emux = 34
              L_uxex = .true.
              L_(690) = .false.
          endif
          if (L_oxex) then
              L_(690) = .true.
              L_uxex = .false.
          endif
          if (I_emux.ne.34) then
              L_uxex = .false.
              L_(690) = .false.
          endif
      else
          L_(690) = .false.
      endif
C FDA90_logic.fgi( 321, 455):��� ������� ���������,EC005
C sav1=L_(689)
C sav2=L_(688)
C sav3=L_(687)
C sav4=R_(168)
C FDA90_logic.fgi( 370, 454):recalc:��������� ������
C if(sav1.ne.L_(689) .and. try924.gt.0) goto 924
C if(sav2.ne.L_(688) .and. try924.gt.0) goto 924
C if(sav3.ne.L_(687) .and. try924.gt.0) goto 924
C if(sav4.ne.R_(168) .and. try924.gt.0) goto 924
      if(L_ulux) then
          if (L_(690)) then
              I_emux = 35
              L_ixex = .true.
              L_(685) = .false.
          endif
          if (L_(686)) then
              L_(685) = .true.
              L_ixex = .false.
          endif
          if (I_emux.ne.35) then
              L_ixex = .false.
              L_(685) = .false.
          endif
      else
          L_(685) = .false.
      endif
C FDA90_logic.fgi( 321, 433):��� ������� ���������,EC005
C sav1=L_(684)
      if(L_ixex.and..not.L0_exex) then
         R0_uvex=R0_axex
      else
         R0_uvex=max(R_(167)-deltat,0.0)
      endif
      L_(684)=R0_uvex.gt.0.0
      L0_exex=L_ixex
C FDA90_logic.fgi( 347, 433):recalc:������������  �� T
C if(sav1.ne.L_(684) .and. try918.gt.0) goto 918
      if(L_ulux) then
          if (L_(685)) then
              I_emux = 36
              L_etov = .true.
              L_(680) = .false.
          endif
          if (L_(415)) then
              L_(680) = .true.
              L_etov = .false.
          endif
          if (I_emux.ne.36) then
              L_etov = .false.
              L_(680) = .false.
          endif
      else
          L_(680) = .false.
      endif
C FDA90_logic.fgi( 321, 413):��� ������� ���������,EC005
C sav1=L_(252)
      if(.not.L_etov) then
         R0_elev=0.0
      elseif(.not.L0_ilev) then
         R0_elev=R0_alev
      else
         R0_elev=max(R_(27)-deltat,0.0)
      endif
      L_(252)=L_etov.and.R0_elev.le.0.0
      L0_ilev=L_etov
C FDA90_logic.fgi( 288, 410):recalc:�������� ��������� ������
C if(sav1.ne.L_(252) .and. try1033.gt.0) goto 1033
      if(L_ulux) then
          if (L_(680)) then
              I_emux = 37
              L_atex = .true.
              L_(681) = .false.
          endif
          if (L_(682)) then
              L_(681) = .true.
              L_atex = .false.
          endif
          if (I_emux.ne.37) then
              L_atex = .false.
              L_(681) = .false.
          endif
      else
          L_(681) = .false.
      endif
C FDA90_logic.fgi( 321, 391):��� ������� ���������,EC005
      L_(679)=.not.L_(1246) .and.L_atex
      L_(677)=L_atex.and..not.L_(678)
C FDA90_logic.fgi( 370, 388):��������� ������
      iv2=0
      if(L_(468)) iv2=ibset(iv2,0)
      if(L_(679)) iv2=ibset(iv2,1)
      L_(192)=L_(192).or.iv2.ne.0
C FDA90_logic.fgi( 556, 806):������-�������: ������� ������ ������/����������/����� ���������
      if(L_iluv.and..not.L0_eluv) then
         R0_ukuv=R0_aluv
      else
         R0_ukuv=max(R_(101)-deltat,0.0)
      endif
      L_(474)=R0_ukuv.gt.0.0
      L0_eluv=L_iluv
C FDA90_logic.fgi( 534, 926):������������  �� T
      L_(473)=.not.L_(1246) .and.L_(474)
      L_(471)=L_(474).and..not.L_(472)
C FDA90_logic.fgi( 557, 926):��������� ������
      if(L_epix.and..not.L0_apix) then
         R0_omix=R0_umix
      else
         R0_omix=max(R_(180)-deltat,0.0)
      endif
      L_(728)=R0_omix.gt.0.0
      L0_apix=L_epix
C FDA90_logic.fgi( 347, 596):������������  �� T
      L_(727)=.not.L_(1246) .and.L_(728)
      L_(725)=L_(728).and..not.L_(726)
C FDA90_logic.fgi( 369, 596):��������� ������
      iv2=0
      if(L_(473)) iv2=ibset(iv2,0)
      if(L_(727)) iv2=ibset(iv2,1)
      L_(191)=L_(191).or.iv2.ne.0
C FDA90_logic.fgi( 557, 926):������-�������: ������� ������ ������/����������/����� ���������
      L_(731)=.not.L_(1246) .and.L_opix
      L_(729)=L_opix.and..not.L_(730)
C FDA90_logic.fgi( 369, 646):��������� ������
      iv2=0
      if(L_(495)) iv2=ibset(iv2,0)
      if(L_(731)) iv2=ibset(iv2,1)
      L_(189)=L_(189).or.iv2.ne.0
C FDA90_logic.fgi( 557, 872):������-�������: ������� ������ ������/����������/����� ���������
      Call TELEGKA_TRANSP_HANDLER(deltat,L_otel,L_(188),L_ipix
     &,L_(189),R_ufil,
     & R_akil,R_ubil,R_ibil,L_ifil,R_adil,R_obil,
     & L_ofil,L_ikel,L_okel,L_(190),L_ixel,R_abil,R_ebil,
     & R_efil,R_imix,R_afil,L_axel,L_uvel,R_apel,
     & REAL(R_umel,4),R_osel,REAL(R_atel,4),L_esel,
     & R_asel,REAL(R_isel,4),L_ipel,R_epel,
     & REAL(R_opel,4),L_uxel,L_(193),L_(194),L_usel,
     & L_evel,L_edil,L_(192),L_(191),L_idil,L_udil,L_ovel
     &)
C FDA90_vlv.fgi( 158,  56):���������� ������� ������������,20FDA91AE002KE01
C sav1=L_opix
C sav2=L_(754)
      if(L_ulux) then
          if (L_(760)) then
              I_emux = 22
              L_opix = .true.
              L_(754) = .false.
          endif
          if (L_ipix) then
              L_(754) = .true.
              L_opix = .false.
          endif
          if (I_emux.ne.22) then
              L_opix = .false.
              L_(754) = .false.
          endif
      else
          L_(754) = .false.
      endif
C FDA90_logic.fgi( 321, 645):recalc:��� ������� ���������,EC005
C if(sav1.ne.L_opix .and. try856.gt.0) goto 856
C if(sav2.ne.L_(754) .and. try856.gt.0) goto 856
      if(L_otex) then
          if (L_(496)) then
              I_avex = 10
              L_upuv = .true.
              L_(636) = .false.
          endif
          if (L_ipix) then
              L_(636) = .true.
              L_upuv = .false.
          endif
          if (I_avex.ne.10) then
              L_upuv = .false.
              L_(636) = .false.
          endif
      else
          L_(636) = .false.
      endif
C FDA90_logic.fgi( 508, 873):��� ������� ���������,EC006
C sav1=L_(495)
C sav2=L_(494)
C sav3=L_(493)
C sav4=R_(109)
C FDA90_logic.fgi( 557, 872):recalc:��������� ������
C if(sav1.ne.L_(495) .and. try1024.gt.0) goto 1024
C if(sav2.ne.L_(494) .and. try1024.gt.0) goto 1024
C if(sav3.ne.L_(493) .and. try1024.gt.0) goto 1024
C if(sav4.ne.R_(109) .and. try1024.gt.0) goto 1024
      if(L_otex) then
          if (L_(636)) then
              I_avex = 11
              L_alex = .true.
              L_(637) = .false.
          endif
          if (L_(638)) then
              L_(637) = .true.
              L_alex = .false.
          endif
          if (I_avex.ne.11) then
              L_alex = .false.
              L_(637) = .false.
          endif
      else
          L_(637) = .false.
      endif
C FDA90_logic.fgi( 508, 853):��� ������� ���������,EC006
      if(L_otex) then
          if (L_(637)) then
              I_avex = 12
              L_akex = .true.
              L_(631) = .false.
          endif
          if (L_obox) then
              L_(631) = .true.
              L_akex = .false.
          endif
          if (I_avex.ne.12) then
              L_akex = .false.
              L_(631) = .false.
          endif
      else
          L_(631) = .false.
      endif
C FDA90_logic.fgi( 508, 836):��� ������� ���������,EC006
      if(L_otex) then
          if (L_(631)) then
              I_avex = 13
              L_efex = .true.
              L_(625) = .false.
          endif
          if (L_(626)) then
              L_(625) = .true.
              L_efex = .false.
          endif
          if (I_avex.ne.13) then
              L_efex = .false.
              L_(625) = .false.
          endif
      else
          L_(625) = .false.
      endif
C FDA90_logic.fgi( 508, 821):��� ������� ���������,EC006
      if(L_efex.and..not.L0_afex) then
         R0_odex=R0_udex
      else
         R0_odex=max(R_(150)-deltat,0.0)
      endif
      L_(624)=R0_odex.gt.0.0
      L0_afex=L_efex
C FDA90_logic.fgi( 534, 821):������������  �� T
      L_(623)=.not.L_(1246) .and.L_(624)
      L_(621)=L_(624).and..not.L_(622)
C FDA90_logic.fgi( 554, 822):��������� ������
      if(L_osax.and..not.L0_isax) then
         R0_asax=R0_esax
      else
         R0_asax=max(R_(138)-deltat,0.0)
      endif
      L_(589)=R0_asax.gt.0.0
      L0_isax=L_osax
C FDA90_logic.fgi( 534, 703):������������  �� T
      L_(588)=.not.L_(1246) .and.L_(589)
      L_(586)=L_(589).and..not.L_(587)
C FDA90_logic.fgi( 554, 704):��������� ������
      iv2=0
      if(L_(784)) iv2=ibset(iv2,0)
      if(L_(758)) iv2=ibset(iv2,1)
      if(L_(623)) iv2=ibset(iv2,2)
      if(L_(588)) iv2=ibset(iv2,3)
      L_(213)=L_(213).or.iv2.ne.0
C FDA90_logic.fgi( 367, 790):������-�������: ������� ������ ������/����������/����� ���������
      if(L_ufox.and..not.L0_ofox) then
         R0_efox=R0_ifox
      else
         R0_efox=max(R_(198)-deltat,0.0)
      endif
      L_(776)=R0_efox.gt.0.0
      L0_ofox=L_ufox
C FDA90_logic.fgi( 347, 707):������������  �� T
      L_(775)=.not.L_(1246) .and.L_(776)
      L_(773)=L_(776).and..not.L_(774)
C FDA90_logic.fgi( 368, 708):��������� ������
      if(L_osix.and..not.L0_isix) then
         R0_asix=R0_esix
      else
         R0_asix=max(R_(186)-deltat,0.0)
      endif
      L_(740)=R0_asix.gt.0.0
      L0_isix=L_osix
C FDA90_logic.fgi( 347, 565):������������  �� T
      L_(739)=.not.L_(1246) .and.L_(740)
      L_(737)=L_(740).and..not.L_(738)
C FDA90_logic.fgi( 367, 566):��������� ������
      L_(608)=R_ilox.gt.R0_ovax
C FDA90_logic.fgi( 482, 749):���������� >
      if(L_otex) then
          if (L_(625)) then
              I_avex = 14
              L_ikuv = .true.
              L_(481) = .false.
          endif
          if (L_(465)) then
              L_(481) = .true.
              L_ikuv = .false.
          endif
          if (I_avex.ne.14) then
              L_ikuv = .false.
              L_(481) = .false.
          endif
      else
          L_(481) = .false.
      endif
C FDA90_logic.fgi( 508, 805):��� ������� ���������,EC006
C sav1=L_(469)
      if(L_ikuv.and..not.L0_ekuv) then
         R0_ufuv=R0_akuv
      else
         R0_ufuv=max(R_(99)-deltat,0.0)
      endif
      L_(469)=R0_ufuv.gt.0.0
      L0_ekuv=L_ikuv
C FDA90_logic.fgi( 534, 805):recalc:������������  �� T
C if(sav1.ne.L_(469) .and. try847.gt.0) goto 847
      if(L_otex) then
          if (L_(481)) then
              I_avex = 15
              L_uluv = .true.
              L_(618) = .false.
          endif
          if (L_ekox) then
              L_(618) = .true.
              L_uluv = .false.
          endif
          if (I_avex.ne.15) then
              L_uluv = .false.
              L_(618) = .false.
          endif
      else
          L_(618) = .false.
      endif
C FDA90_logic.fgi( 508, 792):��� ������� ���������,EC006
      if(L_otex) then
          if (L_(618)) then
              I_avex = 16
              L_edex = .true.
              L_(619) = .false.
          endif
          if (L_(620)) then
              L_(619) = .true.
              L_edex = .false.
          endif
          if (I_avex.ne.16) then
              L_edex = .false.
              L_(619) = .false.
          endif
      else
          L_(619) = .false.
      endif
C FDA90_logic.fgi( 508, 778):��� ������� ���������,EC006
C sav1=L_(617)
      if(L_edex.and..not.L0_adex) then
         R0_obex=R0_ubex
      else
         R0_obex=max(R_(148)-deltat,0.0)
      endif
      L_(617)=R0_obex.gt.0.0
      L0_adex=L_edex
C FDA90_logic.fgi( 534, 778):recalc:������������  �� T
C if(sav1.ne.L_(617) .and. try1021.gt.0) goto 1021
      if(L_otex) then
          if (L_(619)) then
              I_avex = 17
              L_ebex = .true.
              L_(613) = .false.
          endif
          if (L_ofix) then
              L_(613) = .true.
              L_ebex = .false.
          endif
          if (I_avex.ne.17) then
              L_ebex = .false.
              L_(613) = .false.
          endif
      else
          L_(613) = .false.
      endif
C FDA90_logic.fgi( 508, 763):��� ������� ���������,EC006
      if(L_otex) then
          if (L_(613)) then
              I_avex = 18
              L_ixax = .true.
              L_(607) = .false.
          endif
          if (L_(608)) then
              L_(607) = .true.
              L_ixax = .false.
          endif
          if (I_avex.ne.18) then
              L_ixax = .false.
              L_(607) = .false.
          endif
      else
          L_(607) = .false.
      endif
C FDA90_logic.fgi( 508, 749):��� ������� ���������,EC006
      if(L_ixax.and..not.L0_exax) then
         R0_uvax=R0_axax
      else
         R0_uvax=max(R_(144)-deltat,0.0)
      endif
      L_(606)=R0_uvax.gt.0.0
      L0_exax=L_ixax
C FDA90_logic.fgi( 534, 749):������������  �� T
      L_(605)=.not.L_(1246) .and.L_(606)
      L_(603)=L_(606).and..not.L_(604)
C FDA90_logic.fgi( 555, 750):��������� ������
      L_(500)=R_erube.gt.R0_ofuv
C FDA90_logic.fgi( 478, 382):���������� >
      L_(502)=R_ilox.gt.R0_eruv
C FDA90_logic.fgi( 478, 375):���������� >
      L_(501)=R_emobe.gt.R0_aruv
C FDA90_logic.fgi( 478, 368):���������� >
      L_(508) = L_(500).AND.L_(502).AND.L_(501)
C FDA90_logic.fgi( 487, 375):�
      L_(411)=.not.L_(1246) .and.L_usov
      L_(409)=L_usov.and..not.L_(410)
C FDA90_logic.fgi( 557, 392):��������� ������
      if(L_akebe) then
          if (L_(910)) then
              I_ikebe = 26
              L_otov = .true.
              L_(877) = .false.
          endif
          if (L_itov) then
              L_(877) = .true.
              L_otov = .false.
          endif
          if (I_ikebe.ne.26) then
              L_otov = .false.
              L_(877) = .false.
          endif
      else
          L_(877) = .false.
      endif
C FDA90_logic.fgi( 135, 559):��� ������� ���������,EC004
      L_(418)=.not.L_(1246) .and.L_otov
      L_(416)=L_otov.and..not.L_(417)
C FDA90_logic.fgi( 182, 558):��������� ������
      iv2=0
      if(L_(411)) iv2=ibset(iv2,0)
      if(L_(418)) iv2=ibset(iv2,1)
      L_(168)=L_(168).or.iv2.ne.0
C FDA90_logic.fgi( 557, 392):������-�������: ������� ������ ������/����������/����� ���������
      Call PEREGRUZ_USTR_HANDLER(deltat,L_isik,L_(167),L_itov
     &,L_(168),R_ubok,
     & R_adok,R_uvik,R_ivik,L_ibok,R_axik,R_ovik,
     & L_obok,L_usik,L_(169),L_utik,R_avik,R_evik,
     & R_ebok,R_atik,R_abok,L_otik,L_itik,R_elik,
     & REAL(60.0,4),R_irik,REAL(R_urik,4),R_epik,
     & REAL(R_opik,4),L_arik,R_upik,REAL(R_erik,4),
     & L_ipik,L_esik,L_orik,L_osik,L_exik,L_(171),L_(170)
     &,
     & L_ixik,L_uxik,L_etik)
C FDA90_vlv.fgi( 403, 146):���������� ������. ����.,20FDA90AE999
C sav1=L_otov
C sav2=L_(877)
      if(L_akebe) then
          if (L_(910)) then
              I_ikebe = 26
              L_otov = .true.
              L_(877) = .false.
          endif
          if (L_itov) then
              L_(877) = .true.
              L_otov = .false.
          endif
          if (I_ikebe.ne.26) then
              L_otov = .false.
              L_(877) = .false.
          endif
      else
          L_(877) = .false.
      endif
C FDA90_logic.fgi( 135, 559):recalc:��� ������� ���������,EC004
C if(sav1.ne.L_otov .and. try1155.gt.0) goto 1155
C if(sav2.ne.L_(877) .and. try1155.gt.0) goto 1155
      L_(505)=.not.L_(1246) .and.L_iruv
      L_(503)=L_iruv.and..not.L_(504)
C FDA90_logic.fgi( 554, 374):��������� ������
      iv2=0
      if(L_(775)) iv2=ibset(iv2,0)
      if(L_(739)) iv2=ibset(iv2,1)
      if(L_(605)) iv2=ibset(iv2,2)
      if(L_(505)) iv2=ibset(iv2,3)
      L_(212)=L_(212).or.iv2.ne.0
C FDA90_logic.fgi( 368, 708):������-�������: ������� ������ ������/����������/����� ���������
      L_(477)=.not.L_(1246) .and.L_oluv
      L_(475)=L_oluv.and..not.L_(476)
C FDA90_logic.fgi( 558, 680):��������� ������
      if(L_ipox.and..not.L0_epox) then
         R0_umox=R0_apox
      else
         R0_umox=max(R_(204)-deltat,0.0)
      endif
      L_(799)=R0_umox.gt.0.0
      L0_epox=L_ipox
C FDA90_logic.fgi( 347, 775):������������  �� T
      L_(798)=.not.L_(1246) .and.L_(799)
      L_(796)=L_(799).and..not.L_(797)
C FDA90_logic.fgi( 368, 774):��������� ������
      iv2=0
      if(L_(477)) iv2=ibset(iv2,0)
      if(L_(798)) iv2=ibset(iv2,1)
      L_(211)=L_(211).or.iv2.ne.0
C FDA90_logic.fgi( 554,1360):������-�������: ������� ������ ������/����������/����� ���������
      L_(480)=.not.L_(1246) .and.L_uluv
      L_(478)=L_uluv.and..not.L_(479)
C FDA90_logic.fgi( 555, 792):��������� ������
      L_(781)=.not.L_(1246) .and.L_akox
      L_(779)=L_akox.and..not.L_(780)
C FDA90_logic.fgi( 367, 760):��������� ������
      iv2=0
      if(L_(480)) iv2=ibset(iv2,0)
      if(L_(781)) iv2=ibset(iv2,1)
      L_(210)=L_(210).or.iv2.ne.0
C FDA90_logic.fgi( 555, 792):������-�������: ������� ������ ������/����������/����� ���������
      Call PEREGRUZ_USTR_HANDLER(deltat,L_orul,L_(209),L_ekox
     &,L_(210),R_uxul,
     & R_abam,R_utul,R_itul,L_ixul,R_avul,R_otul,
     & L_oxul,L_asul,L_(211),L_usul,R_atul,R_etul,
     & R_exul,R_ilox,R_axul,L_osul,L_isul,R_ikul,
     & REAL(60.0,4),R_opul,REAL(R_arul,4),R_imul,
     & REAL(R_umul,4),L_epul,R_apul,REAL(R_ipul,4),
     & L_omul,L_irul,L_upul,L_urul,L_evul,L_(213),L_(212)
     &,
     & L_ivul,L_uvul,L_esul)
C FDA90_vlv.fgi( 431, 171):���������� ������. ����.,20FDA91AE008
C sav1=L_uluv
C sav2=L_(618)
      if(L_otex) then
          if (L_(481)) then
              I_avex = 15
              L_uluv = .true.
              L_(618) = .false.
          endif
          if (L_ekox) then
              L_(618) = .true.
              L_uluv = .false.
          endif
          if (I_avex.ne.15) then
              L_uluv = .false.
              L_(618) = .false.
          endif
      else
          L_(618) = .false.
      endif
C FDA90_logic.fgi( 508, 792):recalc:��� ������� ���������,EC006
C if(sav1.ne.L_uluv .and. try1128.gt.0) goto 1128
C if(sav2.ne.L_(618) .and. try1128.gt.0) goto 1128
C sav1=L_akox
C sav2=L_(791)
      if(L_ulux) then
          if (L_(794)) then
              I_emux = 14
              L_akox = .true.
              L_(791) = .false.
          endif
          if (L_ekox) then
              L_(791) = .true.
              L_akox = .false.
          endif
          if (I_emux.ne.14) then
              L_akox = .false.
              L_(791) = .false.
          endif
      else
          L_(791) = .false.
      endif
C FDA90_logic.fgi( 321, 759):recalc:��� ������� ���������,EC005
C if(sav1.ne.L_akox .and. try814.gt.0) goto 814
C if(sav2.ne.L_(791) .and. try814.gt.0) goto 814
      L_(591)=R_ilox.lt.R0_urax
C FDA90_logic.fgi( 482, 703):���������� <
      L_(593)=R_ukube.gt.R0_usax
C FDA90_logic.fgi( 482, 719):���������� >
      if(L_otex) then
          if (L_(607)) then
              I_avex = 19
              L_ivax = .true.
              L_(602) = .false.
          endif
          if (L_irix) then
              L_(602) = .true.
              L_ivax = .false.
          endif
          if (I_avex.ne.19) then
              L_ivax = .false.
              L_(602) = .false.
          endif
      else
          L_(602) = .false.
      endif
C FDA90_logic.fgi( 508, 735):��� ������� ���������,EC006
      if(L_otex) then
          if (L_(602)) then
              I_avex = 20
              L_otax = .true.
              L_(592) = .false.
          endif
          if (L_(593)) then
              L_(592) = .true.
              L_otax = .false.
          endif
          if (I_avex.ne.20) then
              L_otax = .false.
              L_(592) = .false.
          endif
      else
          L_(592) = .false.
      endif
C FDA90_logic.fgi( 508, 719):��� ������� ���������,EC006
      if(L_otex) then
          if (L_(592)) then
              I_avex = 21
              L_osax = .true.
              L_(590) = .false.
          endif
          if (L_(591)) then
              L_(590) = .true.
              L_osax = .false.
          endif
          if (I_avex.ne.21) then
              L_osax = .false.
              L_(590) = .false.
          endif
      else
          L_(590) = .false.
      endif
C FDA90_logic.fgi( 508, 703):��� ������� ���������,EC006
C sav1=L_(589)
      if(L_osax.and..not.L0_isax) then
         R0_asax=R0_esax
      else
         R0_asax=max(R_(138)-deltat,0.0)
      endif
      L_(589)=R0_asax.gt.0.0
      L0_isax=L_osax
C FDA90_logic.fgi( 534, 703):recalc:������������  �� T
C if(sav1.ne.L_(589) .and. try1111.gt.0) goto 1111
      if(L_otex) then
          if (L_(590)) then
              I_avex = 22
              L_oluv = .true.
              L_(584) = .false.
          endif
          if (L_akube) then
              L_(584) = .true.
              L_oluv = .false.
          endif
          if (I_avex.ne.22) then
              L_oluv = .false.
              L_(584) = .false.
          endif
      else
          L_(584) = .false.
      endif
C FDA90_logic.fgi( 508, 682):��� ������� ���������,EC006
C sav1=L_(477)
C sav2=L_(476)
C sav3=L_(475)
C sav4=R_(103)
C FDA90_logic.fgi( 558, 680):recalc:��������� ������
C if(sav1.ne.L_(477) .and. try1173.gt.0) goto 1173
C if(sav2.ne.L_(476) .and. try1173.gt.0) goto 1173
C if(sav3.ne.L_(475) .and. try1173.gt.0) goto 1173
C if(sav4.ne.R_(103) .and. try1173.gt.0) goto 1173
      if(L_otex) then
          if (L_(584)) then
              I_avex = 23
              L_orax = .true.
              L_(585) = .false.
          endif
          if (L_ofube) then
              L_(585) = .true.
              L_orax = .false.
          endif
          if (I_avex.ne.23) then
              L_orax = .false.
              L_(585) = .false.
          endif
      else
          L_(585) = .false.
      endif
C FDA90_logic.fgi( 508, 653):��� ������� ���������,EC006
      if(L_otex) then
          if (L_(585)) then
              I_avex = 24
              L_upax = .true.
              L_(578) = .false.
          endif
          if (L_(579)) then
              L_(578) = .true.
              L_upax = .false.
          endif
          if (I_avex.ne.24) then
              L_upax = .false.
              L_(578) = .false.
          endif
      else
          L_(578) = .false.
      endif
C FDA90_logic.fgi( 508, 636):��� ������� ���������,EC006
      if(L_otex) then
          if (L_(578)) then
              I_avex = 25
              L_umax = .true.
              L_(571) = .false.
          endif
          if (L_(572)) then
              L_(571) = .true.
              L_umax = .false.
          endif
          if (I_avex.ne.25) then
              L_umax = .false.
              L_(571) = .false.
          endif
      else
          L_(571) = .false.
      endif
C FDA90_logic.fgi( 508, 618):��� ������� ���������,EC006
      if(L_otex) then
          if (L_(571)) then
              I_avex = 26
              L_ulax = .true.
              L_(570) = .false.
          endif
          if (L_isube) then
              L_(570) = .true.
              L_ulax = .false.
          endif
          if (I_avex.ne.26) then
              L_ulax = .false.
              L_(570) = .false.
          endif
      else
          L_(570) = .false.
      endif
C FDA90_logic.fgi( 508, 602):��� ������� ���������,EC006
      if(L_otex) then
          if (L_(570)) then
              I_avex = 27
              L_olax = .true.
              L_(565) = .false.
          endif
          if (L_(566)) then
              L_(565) = .true.
              L_olax = .false.
          endif
          if (I_avex.ne.27) then
              L_olax = .false.
              L_(565) = .false.
          endif
      else
          L_(565) = .false.
      endif
C FDA90_logic.fgi( 508, 586):��� ������� ���������,EC006
      if(L_otex) then
          if (L_(565)) then
              I_avex = 28
              L_okax = .true.
              L_(560) = .false.
          endif
          if (L_ulube) then
              L_(560) = .true.
              L_okax = .false.
          endif
          if (I_avex.ne.28) then
              L_okax = .false.
              L_(560) = .false.
          endif
      else
          L_(560) = .false.
      endif
C FDA90_logic.fgi( 508, 570):��� ������� ���������,EC006
      if(L_otex) then
          if (L_(560)) then
              I_avex = 29
              L_ufax = .true.
              L_(550) = .false.
          endif
          if (L_(551)) then
              L_(550) = .true.
              L_ufax = .false.
          endif
          if (I_avex.ne.29) then
              L_ufax = .false.
              L_(550) = .false.
          endif
      else
          L_(550) = .false.
      endif
C FDA90_logic.fgi( 508, 553):��� ������� ���������,EC006
      if(L_otex) then
          if (L_(550)) then
              I_avex = 30
              L_odax = .true.
              L_(548) = .false.
          endif
          if (L_(549)) then
              L_(548) = .true.
              L_odax = .false.
          endif
          if (I_avex.ne.30) then
              L_odax = .false.
              L_(548) = .false.
          endif
      else
          L_(548) = .false.
      endif
C FDA90_logic.fgi( 508, 535):��� ������� ���������,EC006
      if(L_otex) then
          if (L_(548)) then
              I_avex = 31
              L_ubax = .true.
              L_(542) = .false.
          endif
          if (L_(543)) then
              L_(542) = .true.
              L_ubax = .false.
          endif
          if (I_avex.ne.31) then
              L_ubax = .false.
              L_(542) = .false.
          endif
      else
          L_(542) = .false.
      endif
C FDA90_logic.fgi( 508, 519):��� ������� ���������,EC006
      if(L_otex) then
          if (L_(542)) then
              I_avex = 32
              L_osuv = .true.
              L_(518) = .false.
          endif
          if (L_erux) then
              L_(518) = .true.
              L_osuv = .false.
          endif
          if (I_avex.ne.32) then
              L_osuv = .false.
              L_(518) = .false.
          endif
      else
          L_(518) = .false.
      endif
C FDA90_logic.fgi( 508, 506):��� ������� ���������,EC006
      if(L_otex) then
          if (L_(518)) then
              I_avex = 33
              L_utuv = .true.
              L_(519) = .false.
          endif
          if (L_(520)) then
              L_(519) = .true.
              L_utuv = .false.
          endif
          if (I_avex.ne.33) then
              L_utuv = .false.
              L_(519) = .false.
          endif
      else
          L_(519) = .false.
      endif
C FDA90_logic.fgi( 508, 489):��� ������� ���������,EC006
      if(L_otex) then
          if (L_(519)) then
              I_avex = 34
              L_esuv = .true.
              L_(517) = .false.
          endif
          if (L_(509)) then
              L_(517) = .true.
              L_esuv = .false.
          endif
          if (I_avex.ne.34) then
              L_esuv = .false.
              L_(517) = .false.
          endif
      else
          L_(517) = .false.
      endif
C FDA90_logic.fgi( 508, 469):��� ������� ���������,EC006
      if(L_otex) then
          if (L_(517)) then
              I_avex = 35
              L_usuv = .true.
              L_(529) = .false.
          endif
          if (L_ufobe) then
              L_(529) = .true.
              L_usuv = .false.
          endif
          if (I_avex.ne.35) then
              L_usuv = .false.
              L_(529) = .false.
          endif
      else
          L_(529) = .false.
      endif
C FDA90_logic.fgi( 508, 449):��� ������� ���������,EC006
      if(L_otex) then
          if (L_(529)) then
              I_avex = 36
              L_ixuv = .true.
              L_(535) = .false.
          endif
          if (L_(530)) then
              L_(535) = .true.
              L_ixuv = .false.
          endif
          if (I_avex.ne.36) then
              L_ixuv = .false.
              L_(535) = .false.
          endif
      else
          L_(535) = .false.
      endif
C FDA90_logic.fgi( 508, 430):��� ������� ���������,EC006
      if(L_otex) then
          if (L_(535)) then
              I_avex = 37
              L_uxuv = .true.
              L_(536) = .false.
          endif
          if (L_(537)) then
              L_(536) = .true.
              L_uxuv = .false.
          endif
          if (I_avex.ne.37) then
              L_uxuv = .false.
              L_(536) = .false.
          endif
      else
          L_(536) = .false.
      endif
C FDA90_logic.fgi( 508, 415):��� ������� ���������,EC006
      if(L_uxuv.and..not.L0_axuv) then
         R0_ovuv=R0_uvuv
      else
         R0_ovuv=max(R_(117)-deltat,0.0)
      endif
      L_(534)=R0_ovuv.gt.0.0
      L0_axuv=L_uxuv
C FDA90_logic.fgi( 534, 415):������������  �� T
      End
      Subroutine FDA90_2(ext_deltat)
      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      Include 'FDA90.fh'

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      L_(533)=.not.L_(1246) .and.L_(534)
      L_(531)=L_(534).and..not.L_(532)
C FDA90_logic.fgi( 554, 416):��������� ������
      if(L_utuv.and..not.L0_otuv) then
         R0_etuv=R0_ituv
      else
         R0_etuv=max(R_(115)-deltat,0.0)
      endif
      L_(528)=R0_etuv.gt.0.0
      L0_otuv=L_utuv
C FDA90_logic.fgi( 534, 489):������������  �� T
      L_(527)=.not.L_(1246) .and.L_(528)
      L_(525)=L_(528).and..not.L_(526)
C FDA90_logic.fgi( 556, 490):��������� ������
      L_(449)=R_emobe.lt.R0_uduv
C FDA90_logic.fgi( 475,1568):���������� <
      if(L_itube) then
          if (L_(1253)) then
              I_otube = 1
              L_afuv = .true.
              L_(448) = .false.
          endif
          if (L_(449)) then
              L_(448) = .true.
              L_afuv = .false.
          endif
          if (I_otube.ne.1) then
              L_afuv = .false.
              L_(448) = .false.
          endif
      else
          L_(448) = .false.
      endif
C FDA90_logic.fgi( 505,1568):��� ������� ���������,EC003
      if(L_afuv.and..not.L0_aduv) then
         R0_obuv=R0_ubuv
      else
         R0_obuv=max(R_(90)-deltat,0.0)
      endif
      L_(447)=R0_obuv.gt.0.0
      L0_aduv=L_afuv
C FDA90_logic.fgi( 531,1568):������������  �� T
      L_(446)=.not.L_(1246) .and.L_(447)
      L_(444)=L_(447).and..not.L_(445)
C FDA90_logic.fgi( 551,1568):��������� ������
      if(L_uxov.and..not.L0_oxov) then
         R0_exov=R0_ixov
      else
         R0_exov=max(R_(87)-deltat,0.0)
      endif
      L_(440)=R0_exov.gt.0.0
      L0_oxov=L_uxov
C FDA90_logic.fgi( 531,1518):������������  �� T
      L_(439)=.not.L_(1246) .and.L_(440)
      L_(437)=L_(440).and..not.L_(438)
C FDA90_logic.fgi( 551,1518):��������� ������
      L_(302)=R_emobe.lt.R0_ixev
C FDA90_logic.fgi( 283,1143):���������� <
      L_(295)=R_emobe.gt.R0_axev
C FDA90_logic.fgi( 283,1169):���������� >
      L_(270)=R_atube.gt.R0_esev
C FDA90_logic.fgi( 284,1208):���������� >
      L_(284)=R_emobe.lt.R0_usev
C FDA90_logic.fgi( 284,1228):���������� <
      L_(308)=R_erube.lt.R0_uxev
C FDA90_logic.fgi( 284,1258):���������� <
      L_(314)=R_atube.lt.R0_odiv
C FDA90_logic.fgi( 284,1274):���������� <
      L_(316)=R_ukube.gt.R0_udiv
C FDA90_logic.fgi( 284,1292):���������� >
      L_(331)=R_erube.gt.R0_okiv
C FDA90_logic.fgi( 284,1325):���������� >
      L_(337)=R_ukube.lt.R0_uliv
C FDA90_logic.fgi( 284,1357):���������� <
      L_(347)=R_erube.lt.R0_umiv
C FDA90_logic.fgi( 284,1375):���������� <
      L_(1056)=R_ibibe.lt.R0_ebibe
C FDA90_logic.fgi( 109,1277):���������� <
      L_(1066)=R_erube.gt.R0_ofibe
C FDA90_logic.fgi( 109,1316):���������� >
      L_(1068)=R_ukube.gt.R0_okibe
C FDA90_logic.fgi( 109,1329):���������� >
      if(L_edibe.and..not.L0_adibe) then
         R0_obibe=R0_ubibe
      else
         R0_obibe=max(R_(283)-deltat,0.0)
      endif
      L_(1054)=R0_obibe.gt.0.0
      L0_adibe=L_edibe
C FDA90_logic.fgi( 161,1277):������������  �� T
      L_(1053)=.not.L_(1246) .and.L_(1054)
      L_(1051)=L_(1054).and..not.L_(1052)
C FDA90_logic.fgi( 181,1278):��������� ������
      L_(1029)=R_ibibe.lt.R0_urebe
C FDA90_logic.fgi( 109,1180):���������� <
      L_(1040)=R_ibibe.gt.R0_utebe
C FDA90_logic.fgi( 109,1228):���������� >
      L_(1050)=R_ukube.lt.R0_exebe
C FDA90_logic.fgi( 109,1257):���������� <
      if(L_osebe.and..not.L0_isebe) then
         R0_asebe=R0_esebe
      else
         R0_asebe=max(R_(275)-deltat,0.0)
      endif
      L_(1026)=R0_asebe.gt.0.0
      L0_isebe=L_osebe
C FDA90_logic.fgi( 161,1180):������������  �� T
      L_(1025)=.not.L_(1246) .and.L_(1026)
      L_(1023)=L_(1026).and..not.L_(1024)
C FDA90_logic.fgi( 186,1180):��������� ������
      L_(400)=R_ibibe.lt.R0_ofov
C FDA90_logic.fgi( 286,1562):���������� <
      if(L_alov) then
          if (L_(401)) then
              I_ilov = 1
              L_ikov = .true.
              L_(399) = .false.
          endif
          if (L_(400)) then
              L_(399) = .true.
              L_ikov = .false.
          endif
          if (I_ilov.ne.1) then
              L_ikov = .false.
              L_(399) = .false.
          endif
      else
          L_(399) = .false.
      endif
C FDA90_logic.fgi( 310,1562):��� ������� ���������,EC002
      if(L_ikov.and..not.L0_ekov) then
         R0_ufov=R0_akov
      else
         R0_ufov=max(R_(76)-deltat,0.0)
      endif
      L_(398)=R0_ufov.gt.0.0
      L0_ekov=L_ikov
C FDA90_logic.fgi( 336,1562):������������  �� T
      L_(397)=.not.L_(1246) .and.L_(398)
      L_(395)=L_(398).and..not.L_(396)
C FDA90_logic.fgi( 357,1562):��������� ������
      L_(379)=R_ibibe.lt.R0_ixiv
C FDA90_logic.fgi( 285,1476):���������� <
      L_(374)=R_ukube.lt.R0_iviv
C FDA90_logic.fgi( 285,1512):���������� <
      L_(388)=R_ibibe.gt.R0_edov
C FDA90_logic.fgi( 285,1527):���������� >
      if(L_alov) then
          if (L_(399)) then
              I_ilov = 2
              L_ifov = .true.
              L_(391) = .false.
          endif
          if (L_uvebe) then
              L_(391) = .true.
              L_ifov = .false.
          endif
          if (I_ilov.ne.2) then
              L_ifov = .false.
              L_(391) = .false.
          endif
      else
          L_(391) = .false.
      endif
C FDA90_logic.fgi( 310,1546):��� ������� ���������,EC002
      if(L_alov) then
          if (L_(391)) then
              I_ilov = 3
              L_afov = .true.
              L_(387) = .false.
          endif
          if (L_(388)) then
              L_(387) = .true.
              L_afov = .false.
          endif
          if (I_ilov.ne.3) then
              L_afov = .false.
              L_(387) = .false.
          endif
      else
          L_(387) = .false.
      endif
C FDA90_logic.fgi( 310,1527):��� ������� ���������,EC002
      if(L_alov) then
          if (L_(387)) then
              I_ilov = 4
              L_exiv = .true.
              L_(384) = .false.
          endif
          if (L_(374)) then
              L_(384) = .true.
              L_exiv = .false.
          endif
          if (I_ilov.ne.4) then
              L_exiv = .false.
              L_(384) = .false.
          endif
      else
          L_(384) = .false.
      endif
C FDA90_logic.fgi( 310,1512):��� ������� ���������,EC002
      if(L_alov) then
          if (L_(384)) then
              I_ilov = 5
              L_adov = .true.
              L_(385) = .false.
          endif
          if (L_afibe) then
              L_(385) = .true.
              L_adov = .false.
          endif
          if (I_ilov.ne.5) then
              L_adov = .false.
              L_(385) = .false.
          endif
      else
          L_(385) = .false.
      endif
C FDA90_logic.fgi( 310,1495):��� ������� ���������,EC002
      if(L_alov) then
          if (L_(385)) then
              I_ilov = 6
              L_ebov = .true.
              L_(389) = .false.
          endif
          if (L_(379)) then
              L_(389) = .true.
              L_ebov = .false.
          endif
          if (I_ilov.ne.6) then
              L_ebov = .false.
              L_(389) = .false.
          endif
      else
          L_(389) = .false.
      endif
C FDA90_logic.fgi( 310,1476):��� ������� ���������,EC002
      if(L_ebov.and..not.L0_abov) then
         R0_oxiv=R0_uxiv
      else
         R0_oxiv=max(R_(69)-deltat,0.0)
      endif
      L_(378)=R0_oxiv.gt.0.0
      L0_abov=L_ebov
C FDA90_logic.fgi( 336,1476):������������  �� T
      L_(377)=.not.L_(1246) .and.L_(378)
      L_(375)=L_(378).and..not.L_(376)
C FDA90_logic.fgi( 357,1476):��������� ������
      iv2=0
      if(L_(1053)) iv2=ibset(iv2,0)
      if(L_(1025)) iv2=ibset(iv2,1)
      if(L_(397)) iv2=ibset(iv2,2)
      if(L_(377)) iv2=ibset(iv2,3)
      L_(157)=L_(157).or.iv2.ne.0
C FDA90_logic.fgi( 181,1278):������-�������: ������� ������ ������/����������/����� ���������
      if(L_ovebe.and..not.L0_ivebe) then
         R0_avebe=R0_evebe
      else
         R0_avebe=max(R_(279)-deltat,0.0)
      endif
      L_(1038)=R0_avebe.gt.0.0
      L0_ivebe=L_ovebe
C FDA90_logic.fgi( 161,1228):������������  �� T
      L_(1037)=.not.L_(1246) .and.L_(1038)
      L_(1035)=L_(1038).and..not.L_(1036)
C FDA90_logic.fgi( 181,1228):��������� ������
      L_(1016)=R_ibibe.gt.R0_erebe
C FDA90_logic.fgi(  99,1135):���������� >
      L_(1015)=R_emobe.gt.R0_arebe
C FDA90_logic.fgi(  99,1128):���������� >
      L_(1022) = L_(1017).AND.L_(1016).AND.L_(1015)
C FDA90_logic.fgi( 113,1135):�
      L_(1020)=.not.L_(1246) .and.L_orebe
      L_(1018)=L_orebe.and..not.L_(1019)
C FDA90_logic.fgi( 184,1132):��������� ������
      if(L_etiv.and..not.L0_ativ) then
         R0_osiv=R0_usiv
      else
         R0_osiv=max(R_(64)-deltat,0.0)
      endif
      L_(361)=R0_osiv.gt.0.0
      L0_ativ=L_etiv
C FDA90_logic.fgi( 336,1422):������������  �� T
      L_(360)=.not.L_(1246) .and.L_(361)
      L_(358)=L_(361).and..not.L_(359)
C FDA90_logic.fgi( 357,1422):��������� ������
      if(L_afov.and..not.L0_udov) then
         R0_idov=R0_odov
      else
         R0_idov=max(R_(73)-deltat,0.0)
      endif
      L_(386)=R0_idov.gt.0.0
      L0_udov=L_afov
C FDA90_logic.fgi( 336,1527):������������  �� T
      L_(255)=.not.L_(1246) .and.L_(386)
      L_(253)=L_(386).and..not.L_(254)
C FDA90_logic.fgi( 358,1528):��������� ������
      iv2=0
      if(L_(1037)) iv2=ibset(iv2,0)
      if(L_(1020)) iv2=ibset(iv2,1)
      if(L_(360)) iv2=ibset(iv2,2)
      if(L_(255)) iv2=ibset(iv2,3)
      L_(156)=L_(156).or.iv2.ne.0
C FDA90_logic.fgi( 181,1228):������-�������: ������� ������ ������/����������/����� ���������
      L_(394)=.not.L_(1246) .and.L_ifov
      L_(392)=L_ifov.and..not.L_(393)
C FDA90_logic.fgi( 358,1544):��������� ������
      L_(452)=.not.L_(1246) .and.L_upebe
      L_(450)=L_upebe.and..not.L_(451)
C FDA90_logic.fgi( 186,1154):��������� ������
      iv2=0
      if(L_(394)) iv2=ibset(iv2,0)
      if(L_(452)) iv2=ibset(iv2,1)
      L_(155)=L_(155).or.iv2.ne.0
C FDA90_logic.fgi( 358,1544):������-�������: ������� ������ ������/����������/����� ���������
      L_(1044)=.not.L_(1246) .and.L_axebe
      L_(1042)=L_axebe.and..not.L_(1043)
C FDA90_logic.fgi( 184,1240):��������� ������
      iv2=0
      if(L_(394)) iv2=ibset(iv2,0)
      if(L_(1044)) iv2=ibset(iv2,1)
      L_(154)=L_(154).or.iv2.ne.0
C FDA90_logic.fgi( 358,1544):������-�������: ������� ������ ������/����������/����� ���������
      Call PEREGRUZ_USTR_HANDLER(deltat,L_etif,L_(153),L_uvebe
     &,L_(154),R_idof,
     & R_odof,R_ixif,R_axif,L_adof,R_oxif,R_exif,
     & L_edof,L_otif,L_(155),L_ivif,R_ovif,R_uvif,
     & R_ubof,R_ibibe,R_obof,L_evif,L_avif,R_amif,
     & REAL(60.0,4),R_esif,REAL(R_osif,4),R_arif,
     & REAL(R_irif,4),L_urif,R_orif,REAL(R_asif,4),
     & L_erif,L_atif,L_isif,L_itif,L_uxif,L_(157),L_(156)
     &,
     & L_abof,L_ibof,L_utif)
C FDA90_vlv.fgi( 387, 171):���������� ������. ����.,20FDA91AE012
C sav1=L_ifov
C sav2=L_(391)
      if(L_alov) then
          if (L_(399)) then
              I_ilov = 2
              L_ifov = .true.
              L_(391) = .false.
          endif
          if (L_uvebe) then
              L_(391) = .true.
              L_ifov = .false.
          endif
          if (I_ilov.ne.2) then
              L_ifov = .false.
              L_(391) = .false.
          endif
      else
          L_(391) = .false.
      endif
C FDA90_logic.fgi( 310,1546):recalc:��� ������� ���������,EC002
C if(sav1.ne.L_ifov .and. try1358.gt.0) goto 1358
C if(sav2.ne.L_(391) .and. try1358.gt.0) goto 1358
      L_(363)=R_ibibe.gt.R0_isiv
C FDA90_logic.fgi( 285,1422):���������� >
      L_(365)=R_ukube.gt.R0_itiv
C FDA90_logic.fgi( 284,1438):���������� >
      if(L_alov) then
          if (L_(389)) then
              I_ilov = 7
              L_efov = .true.
              L_(390) = .false.
          endif
          if (L_akube) then
              L_(390) = .true.
              L_efov = .false.
          endif
          if (I_ilov.ne.7) then
              L_efov = .false.
              L_(390) = .false.
          endif
      else
          L_(390) = .false.
      endif
C FDA90_logic.fgi( 310,1460):��� ������� ���������,EC002
      if(L_alov) then
          if (L_(390)) then
              I_ilov = 8
              L_eviv = .true.
              L_(364) = .false.
          endif
          if (L_(365)) then
              L_(364) = .true.
              L_eviv = .false.
          endif
          if (I_ilov.ne.8) then
              L_eviv = .false.
              L_(364) = .false.
          endif
      else
          L_(364) = .false.
      endif
C FDA90_logic.fgi( 310,1438):��� ������� ���������,EC002
      if(L_alov) then
          if (L_(364)) then
              I_ilov = 9
              L_etiv = .true.
              L_(362) = .false.
          endif
          if (L_(363)) then
              L_(362) = .true.
              L_etiv = .false.
          endif
          if (I_ilov.ne.9) then
              L_etiv = .false.
              L_(362) = .false.
          endif
      else
          L_(362) = .false.
      endif
C FDA90_logic.fgi( 310,1422):��� ������� ���������,EC002
C sav1=L_(361)
      if(L_etiv.and..not.L0_ativ) then
         R0_osiv=R0_usiv
      else
         R0_osiv=max(R_(64)-deltat,0.0)
      endif
      L_(361)=R0_osiv.gt.0.0
      L0_ativ=L_etiv
C FDA90_logic.fgi( 336,1422):recalc:������������  �� T
C if(sav1.ne.L_(361) .and. try1390.gt.0) goto 1390
      if(L_alov) then
          if (L_(362)) then
              I_ilov = 10
              L_esiv = .true.
              L_(357) = .false.
          endif
          if (L_itebe) then
              L_(357) = .true.
              L_esiv = .false.
          endif
          if (I_ilov.ne.10) then
              L_esiv = .false.
              L_(357) = .false.
          endif
      else
          L_(357) = .false.
      endif
C FDA90_logic.fgi( 310,1406):��� ������� ���������,EC002
      if(L_alov) then
          if (L_(357)) then
              I_ilov = 11
              L_iriv = .true.
              L_(352) = .false.
          endif
          if (L_ofube) then
              L_(352) = .true.
              L_iriv = .false.
          endif
          if (I_ilov.ne.11) then
              L_iriv = .false.
              L_(352) = .false.
          endif
      else
          L_(352) = .false.
      endif
C FDA90_logic.fgi( 310,1390):��� ������� ���������,EC002
      if(L_alov) then
          if (L_(352)) then
              I_ilov = 13
              L_opiv = .true.
              L_(346) = .false.
          endif
          if (L_(347)) then
              L_(346) = .true.
              L_opiv = .false.
          endif
          if (I_ilov.ne.13) then
              L_opiv = .false.
              L_(346) = .false.
          endif
      else
          L_(346) = .false.
      endif
C FDA90_logic.fgi( 310,1375):��� ������� ���������,EC002
      if(L_alov) then
          if (L_(346)) then
              I_ilov = 14
              L_omiv = .true.
              L_(336) = .false.
          endif
          if (L_(337)) then
              L_(336) = .true.
              L_omiv = .false.
          endif
          if (I_ilov.ne.14) then
              L_omiv = .false.
              L_(336) = .false.
          endif
      else
          L_(336) = .false.
      endif
C FDA90_logic.fgi( 310,1357):��� ������� ���������,EC002
      if(L_alov) then
          if (L_(336)) then
              I_ilov = 15
              L_oliv = .true.
              L_(335) = .false.
          endif
          if (L_isube) then
              L_(335) = .true.
              L_oliv = .false.
          endif
          if (I_ilov.ne.15) then
              L_oliv = .false.
              L_(335) = .false.
          endif
      else
          L_(335) = .false.
      endif
C FDA90_logic.fgi( 310,1341):��� ������� ���������,EC002
      if(L_alov) then
          if (L_(335)) then
              I_ilov = 16
              L_iliv = .true.
              L_(330) = .false.
          endif
          if (L_(331)) then
              L_(330) = .true.
              L_iliv = .false.
          endif
          if (I_ilov.ne.16) then
              L_iliv = .false.
              L_(330) = .false.
          endif
      else
          L_(330) = .false.
      endif
C FDA90_logic.fgi( 310,1325):��� ������� ���������,EC002
      if(L_alov) then
          if (L_(330)) then
              I_ilov = 17
              L_ikiv = .true.
              L_(325) = .false.
          endif
          if (L_ulube) then
              L_(325) = .true.
              L_ikiv = .false.
          endif
          if (I_ilov.ne.17) then
              L_ikiv = .false.
              L_(325) = .false.
          endif
      else
          L_(325) = .false.
      endif
C FDA90_logic.fgi( 310,1309):��� ������� ���������,EC002
      if(L_alov) then
          if (L_(325)) then
              I_ilov = 18
              L_ofiv = .true.
              L_(315) = .false.
          endif
          if (L_(316)) then
              L_(315) = .true.
              L_ofiv = .false.
          endif
          if (I_ilov.ne.18) then
              L_ofiv = .false.
              L_(315) = .false.
          endif
      else
          L_(315) = .false.
      endif
C FDA90_logic.fgi( 310,1292):��� ������� ���������,EC002
      if(L_alov) then
          if (L_(315)) then
              I_ilov = 19
              L_idiv = .true.
              L_(313) = .false.
          endif
          if (L_(314)) then
              L_(313) = .true.
              L_idiv = .false.
          endif
          if (I_ilov.ne.19) then
              L_idiv = .false.
              L_(313) = .false.
          endif
      else
          L_(313) = .false.
      endif
C FDA90_logic.fgi( 310,1274):��� ������� ���������,EC002
      if(L_alov) then
          if (L_(313)) then
              I_ilov = 20
              L_obiv = .true.
              L_(307) = .false.
          endif
          if (L_(308)) then
              L_(307) = .true.
              L_obiv = .false.
          endif
          if (I_ilov.ne.20) then
              L_obiv = .false.
              L_(307) = .false.
          endif
      else
          L_(307) = .false.
      endif
C FDA90_logic.fgi( 310,1258):��� ������� ���������,EC002
      if(L_alov) then
          if (L_(307)) then
              I_ilov = 21
              L_isev = .true.
              L_(282) = .false.
          endif
          if (L_erux) then
              L_(282) = .true.
              L_isev = .false.
          endif
          if (I_ilov.ne.21) then
              L_isev = .false.
              L_(282) = .false.
          endif
      else
          L_(282) = .false.
      endif
C FDA90_logic.fgi( 310,1245):��� ������� ���������,EC002
      if(L_alov) then
          if (L_(282)) then
              I_ilov = 22
              L_otev = .true.
              L_(283) = .false.
          endif
          if (L_(284)) then
              L_(283) = .true.
              L_otev = .false.
          endif
          if (I_ilov.ne.22) then
              L_otev = .false.
              L_(283) = .false.
          endif
      else
          L_(283) = .false.
      endif
C FDA90_logic.fgi( 310,1228):��� ������� ���������,EC002
      if(L_alov) then
          if (L_(283)) then
              I_ilov = 23
              L_asev = .true.
              L_(281) = .false.
          endif
          if (L_(270)) then
              L_(281) = .true.
              L_asev = .false.
          endif
          if (I_ilov.ne.23) then
              L_asev = .false.
              L_(281) = .false.
          endif
      else
          L_(281) = .false.
      endif
C FDA90_logic.fgi( 310,1208):��� ������� ���������,EC002
      if(L_alov) then
          if (L_(281)) then
              I_ilov = 24
              L_osev = .true.
              L_(293) = .false.
          endif
          if (L_ufobe) then
              L_(293) = .true.
              L_osev = .false.
          endif
          if (I_ilov.ne.24) then
              L_osev = .false.
              L_(293) = .false.
          endif
      else
          L_(293) = .false.
      endif
C FDA90_logic.fgi( 310,1188):��� ������� ���������,EC002
      if(L_alov) then
          if (L_(293)) then
              I_ilov = 25
              L_exev = .true.
              L_(294) = .false.
          endif
          if (L_(295)) then
              L_(294) = .true.
              L_exev = .false.
          endif
          if (I_ilov.ne.25) then
              L_exev = .false.
              L_(294) = .false.
          endif
      else
          L_(294) = .false.
      endif
C FDA90_logic.fgi( 310,1169):��� ������� ���������,EC002
      if(L_alov) then
          if (L_(294)) then
              I_ilov = 26
              L_omev = .true.
              L_(300) = .false.
          endif
          if (L_imev) then
              L_(300) = .true.
              L_omev = .false.
          endif
          if (I_ilov.ne.26) then
              L_omev = .false.
              L_(300) = .false.
          endif
      else
          L_(300) = .false.
      endif
C FDA90_logic.fgi( 310,1156):��� ������� ���������,EC002
      if(L_alov) then
          if (L_(300)) then
              I_ilov = 27
              L_oxev = .true.
              L_(301) = .false.
          endif
          if (L_(302)) then
              L_(301) = .true.
              L_oxev = .false.
          endif
          if (I_ilov.ne.27) then
              L_oxev = .false.
              L_(301) = .false.
          endif
      else
          L_(301) = .false.
      endif
C FDA90_logic.fgi( 310,1143):��� ������� ���������,EC002
      if(L_oxev.and..not.L0_uvev) then
         R0_ivev=R0_ovev
      else
         R0_ivev=max(R_(40)-deltat,0.0)
      endif
      L_(299)=R0_ivev.gt.0.0
      L0_uvev=L_oxev
C FDA90_logic.fgi( 336,1143):������������  �� T
      L_(298)=.not.L_(1246) .and.L_(299)
      L_(296)=L_(299).and..not.L_(297)
C FDA90_logic.fgi( 357,1144):��������� ������
      if(L_otev.and..not.L0_itev) then
         R0_atev=R0_etev
      else
         R0_atev=max(R_(38)-deltat,0.0)
      endif
      L_(292)=R0_atev.gt.0.0
      L0_itev=L_otev
C FDA90_logic.fgi( 336,1228):������������  �� T
      L_(291)=.not.L_(1246) .and.L_(292)
      L_(289)=L_(292).and..not.L_(290)
C FDA90_logic.fgi( 357,1228):��������� ������
      iv2=0
      if(L_(1144)) iv2=ibset(iv2,0)
      if(L_(1137)) iv2=ibset(iv2,1)
      if(L_(907)) iv2=ibset(iv2,2)
      if(L_(901)) iv2=ibset(iv2,3)
      if(L_(868)) iv2=ibset(iv2,4)
      if(L_(859)) iv2=ibset(iv2,5)
      if(L_(533)) iv2=ibset(iv2,6)
      if(L_(527)) iv2=ibset(iv2,7)
      if(L_(446)) iv2=ibset(iv2,8)
      if(L_(439)) iv2=ibset(iv2,9)
      if(L_(298)) iv2=ibset(iv2,10)
      if(L_(291)) iv2=ibset(iv2,11)
      L_(241)=L_(241).or.iv2.ne.0
C FDA90_logic.fgi( 181,1570):������-�������: ������� ������ ������/����������/����� ���������
      if(L_ikobe.and..not.L0_afobe) then
         R0_odobe=R0_udobe
      else
         R0_odobe=max(R_(310)-deltat,0.0)
      endif
      L_(1134)=R0_odobe.gt.0.0
      L0_afobe=L_ikobe
C FDA90_logic.fgi( 161,1539):������������  �� T
      L_(1133)=.not.L_(1246) .and.L_(1134)
      L_(1131)=L_(1134).and..not.L_(1132)
C FDA90_logic.fgi( 181,1540):��������� ������
      if(L_evux.and..not.L0_etux) then
         R0_usux=R0_atux
      else
         R0_usux=max(R_(234)-deltat,0.0)
      endif
      L_(898)=R0_usux.gt.0.0
      L0_etux=L_evux
C FDA90_logic.fgi( 161, 595):������������  �� T
      L_(897)=.not.L_(1246) .and.L_(898)
      L_(895)=L_(898).and..not.L_(896)
C FDA90_logic.fgi( 181, 596):��������� ������
      L_(873)=R_emobe.gt.R0_umux
C FDA90_logic.fgi( 104, 541):���������� >
      L_(404)=R_erube.gt.R0_arov
C FDA90_logic.fgi( 104, 533):���������� >
      L_(879) = L_(873).AND.L_(404)
C FDA90_logic.fgi( 112, 540):�
      if(L_akebe) then
          if (L_(877)) then
              I_ikebe = 27
              L_apux = .true.
              L_(878) = .false.
          endif
          if (L_(879)) then
              L_(878) = .true.
              L_apux = .false.
          endif
          if (I_ikebe.ne.27) then
              L_apux = .false.
              L_(878) = .false.
          endif
      else
          L_(878) = .false.
      endif
C FDA90_logic.fgi( 135, 540):��� ������� ���������,EC004
      L_(876)=.not.L_(1246) .and.L_apux
      L_(874)=L_apux.and..not.L_(875)
C FDA90_logic.fgi( 182, 540):��������� ������
      if(L_ukux.and..not.L0_ifux) then
         R0_afux=R0_efux
      else
         R0_afux=max(R_(223)-deltat,0.0)
      endif
      L_(856)=R0_afux.gt.0.0
      L0_ifux=L_ukux
C FDA90_logic.fgi( 347, 989):������������  �� T
      L_(855)=.not.L_(1246) .and.L_(856)
      L_(853)=L_(856).and..not.L_(854)
C FDA90_logic.fgi( 367, 990):��������� ������
      if(L_ixuv.and..not.L0_ivuv) then
         R0_avuv=R0_evuv
      else
         R0_avuv=max(R_(116)-deltat,0.0)
      endif
      L_(524)=R0_avuv.gt.0.0
      L0_ivuv=L_ixuv
C FDA90_logic.fgi( 534, 430):������������  �� T
      L_(523)=.not.L_(1246) .and.L_(524)
      L_(521)=L_(524).and..not.L_(522)
C FDA90_logic.fgi( 554, 430):��������� ������
      L_(443)=R_emobe.gt.R0_iduv
C FDA90_logic.fgi( 475,1538):���������� >
      if(L_itube) then
          if (L_(448)) then
              I_otube = 2
              L_eduv = .true.
              L_(441) = .false.
          endif
          if (L_ufobe) then
              L_(441) = .true.
              L_eduv = .false.
          endif
          if (I_otube.ne.2) then
              L_eduv = .false.
              L_(441) = .false.
          endif
      else
          L_(441) = .false.
      endif
C FDA90_logic.fgi( 505,1551):��� ������� ���������,EC003
      if(L_itube) then
          if (L_(441)) then
              I_otube = 3
              L_oduv = .true.
              L_(442) = .false.
          endif
          if (L_(443)) then
              L_(442) = .true.
              L_oduv = .false.
          endif
          if (I_otube.ne.3) then
              L_oduv = .false.
              L_(442) = .false.
          endif
      else
          L_(442) = .false.
      endif
C FDA90_logic.fgi( 505,1538):��� ������� ���������,EC003
      if(L_oduv.and..not.L0_ibuv) then
         R0_abuv=R0_ebuv
      else
         R0_abuv=max(R_(89)-deltat,0.0)
      endif
      L_(436)=R0_abuv.gt.0.0
      L0_ibuv=L_oduv
C FDA90_logic.fgi( 531,1538):������������  �� T
      L_(435)=.not.L_(1246) .and.L_(436)
      L_(433)=L_(436).and..not.L_(434)
C FDA90_logic.fgi( 551,1538):��������� ������
      if(L_exev.and..not.L0_evev) then
         R0_utev=R0_avev
      else
         R0_utev=max(R_(39)-deltat,0.0)
      endif
      L_(288)=R0_utev.gt.0.0
      L0_evev=L_exev
C FDA90_logic.fgi( 336,1169):������������  �� T
      L_(287)=.not.L_(1246) .and.L_(288)
      L_(285)=L_(288).and..not.L_(286)
C FDA90_logic.fgi( 357,1170):��������� ������
      L_(260)=R_erube.gt.R0_opev
C FDA90_logic.fgi( 278,1104):���������� >
      L_(259)=R_emobe.gt.R0_ipev
C FDA90_logic.fgi( 278,1097):���������� >
      L_(265) = L_(260).AND.L_(259)
C FDA90_logic.fgi( 287,1103):�
      if(L_erev.and..not.L0_epev) then
         R0_umev=R0_apev
      else
         R0_umev=max(R_(29)-deltat,0.0)
      endif
      L_arev=R0_umev.gt.0.0
      L0_epev=L_erev
C FDA90_logic.fgi( 336,1127):������������  �� T
      if(L_alov) then
          if (L_(301)) then
              I_ilov = 28
              L_erev = .true.
              L_(269) = .false.
          endif
          if (L_arev) then
              L_(269) = .true.
              L_erev = .false.
          endif
          if (I_ilov.ne.28) then
              L_erev = .false.
              L_(269) = .false.
          endif
      else
          L_(269) = .false.
      endif
C FDA90_logic.fgi( 310,1127):��� ������� ���������,EC002
C sav1=L_arev
      if(L_erev.and..not.L0_epev) then
         R0_umev=R0_apev
      else
         R0_umev=max(R_(29)-deltat,0.0)
      endif
      L_arev=R0_umev.gt.0.0
      L0_epev=L_erev
C FDA90_logic.fgi( 336,1127):recalc:������������  �� T
C if(sav1.ne.L_arev .and. try1560.gt.0) goto 1560
      if(L_alov) then
          if (L_(269)) then
              I_ilov = 29
              L_upev = .true.
              L_(264) = .false.
          endif
          if (L_(265)) then
              L_(264) = .true.
              L_upev = .false.
          endif
          if (I_ilov.ne.29) then
              L_upev = .false.
              L_(264) = .false.
          endif
      else
          L_(264) = .false.
      endif
C FDA90_logic.fgi( 310,1103):��� ������� ���������,EC002
      L_(263)=.not.L_(1246) .and.L_upev
      L_(261)=L_upev.and..not.L_(262)
C FDA90_logic.fgi( 360,1100):��������� ������
      iv2=0
      if(L_(1154)) iv2=ibset(iv2,0)
      if(L_(1133)) iv2=ibset(iv2,1)
      if(L_(1020)) iv2=ibset(iv2,2)
      if(L_(897)) iv2=ibset(iv2,3)
      if(L_(876)) iv2=ibset(iv2,4)
      if(L_(855)) iv2=ibset(iv2,5)
      if(L_(679)) iv2=ibset(iv2,6)
      if(L_(523)) iv2=ibset(iv2,7)
      if(L_(505)) iv2=ibset(iv2,8)
      if(L_(435)) iv2=ibset(iv2,9)
      if(L_(287)) iv2=ibset(iv2,10)
      if(L_(263)) iv2=ibset(iv2,11)
      L_(240)=L_(240).or.iv2.ne.0
C FDA90_logic.fgi( 551,1242):������-�������: ������� ������ ������/����������/����� ���������
      L_(1128)=.not.L_(1246) .and.L_akobe
      L_(1126)=L_akobe.and..not.L_(1127)
C FDA90_logic.fgi( 183,1550):��������� ������
      L_(848)=.not.L_(1246) .and.L_ekux
      L_(846)=L_ekux.and..not.L_(847)
C FDA90_logic.fgi( 369,1000):��������� ������
      L_(430)=.not.L_(1246) .and.L_eduv
      L_(428)=L_eduv.and..not.L_(429)
C FDA90_logic.fgi( 552,1550):��������� ������
      L_(268)=.not.L_(1246) .and.L_arev
      L_(266)=L_arev.and..not.L_(267)
C FDA90_logic.fgi( 358,1128):��������� ������
      iv2=0
      if(L_(1128)) iv2=ibset(iv2,0)
      if(L_(848)) iv2=ibset(iv2,1)
      if(L_(430)) iv2=ibset(iv2,2)
      if(L_(418)) iv2=ibset(iv2,3)
      if(L_(411)) iv2=ibset(iv2,4)
      if(L_(268)) iv2=ibset(iv2,5)
      L_(239)=L_(239).or.iv2.ne.0
C FDA90_logic.fgi( 183,1550):������-�������: ������� ������ ������/����������/����� ���������
      L_(890)=.not.L_(1246) .and.L_orux
      L_(888)=L_orux.and..not.L_(889)
C FDA90_logic.fgi( 181, 614):��������� ������
      L_(516)=.not.L_(1246) .and.L_usuv
      L_(514)=L_usuv.and..not.L_(515)
C FDA90_logic.fgi( 555, 450):��������� ������
      L_(280)=.not.L_(1246) .and.L_osev
      L_(278)=L_osev.and..not.L_(279)
C FDA90_logic.fgi( 357,1188):��������� ������
      iv2=0
      if(L_(1128)) iv2=ibset(iv2,0)
      if(L_(890)) iv2=ibset(iv2,1)
      if(L_(848)) iv2=ibset(iv2,2)
      if(L_(516)) iv2=ibset(iv2,3)
      if(L_(430)) iv2=ibset(iv2,4)
      if(L_(280)) iv2=ibset(iv2,5)
      L_(238)=L_(238).or.iv2.ne.0
C FDA90_logic.fgi( 183,1550):������-�������: ������� ������ ������/����������/����� ���������
      Call PEREGRUZ_USTR_HANDLER(deltat,L_orim,L_(237),L_ufobe
     &,L_(238),R_uxim,
     & R_abom,R_utim,R_itim,L_ixim,R_avim,R_otim,
     & L_oxim,L_asim,L_(239),L_usim,R_atim,R_etim,
     & R_exim,R_emobe,R_axim,L_osim,L_isim,R_ikim,
     & REAL(60.0,4),R_opim,REAL(R_arim,4),R_imim,
     & REAL(R_umim,4),L_epim,R_apim,REAL(R_ipim,4),
     & L_omim,L_irim,L_upim,L_urim,L_evim,L_(241),L_(240)
     &,
     & L_ivim,L_uvim,L_esim)
C FDA90_vlv.fgi( 459, 171):���������� ������. ����.,20FDA91AE014
C sav1=L_usuv
C sav2=L_(529)
      if(L_otex) then
          if (L_(517)) then
              I_avex = 35
              L_usuv = .true.
              L_(529) = .false.
          endif
          if (L_ufobe) then
              L_(529) = .true.
              L_usuv = .false.
          endif
          if (I_avex.ne.35) then
              L_usuv = .false.
              L_(529) = .false.
          endif
      else
          L_(529) = .false.
      endif
C FDA90_logic.fgi( 508, 449):recalc:��� ������� ���������,EC006
C if(sav1.ne.L_usuv .and. try1277.gt.0) goto 1277
C if(sav2.ne.L_(529) .and. try1277.gt.0) goto 1277
C sav1=L_orux
C sav2=L_(903)
      if(L_akebe) then
          if (L_(891)) then
              I_ikebe = 23
              L_orux = .true.
              L_(903) = .false.
          endif
          if (L_ufobe) then
              L_(903) = .true.
              L_orux = .false.
          endif
          if (I_ikebe.ne.23) then
              L_orux = .false.
              L_(903) = .false.
          endif
      else
          L_(903) = .false.
      endif
C FDA90_logic.fgi( 135, 614):recalc:��� ������� ���������,EC004
C if(sav1.ne.L_orux .and. try682.gt.0) goto 682
C if(sav2.ne.L_(903) .and. try682.gt.0) goto 682
C sav1=L_eduv
C sav2=L_(441)
      if(L_itube) then
          if (L_(448)) then
              I_otube = 2
              L_eduv = .true.
              L_(441) = .false.
          endif
          if (L_ufobe) then
              L_(441) = .true.
              L_eduv = .false.
          endif
          if (I_otube.ne.2) then
              L_eduv = .false.
              L_(441) = .false.
          endif
      else
          L_(441) = .false.
      endif
C FDA90_logic.fgi( 505,1551):recalc:��� ������� ���������,EC003
C if(sav1.ne.L_eduv .and. try1542.gt.0) goto 1542
C if(sav2.ne.L_(441) .and. try1542.gt.0) goto 1542
C sav1=L_ekux
C sav2=L_(863)
      if(L_ulux) then
          if (L_(870)) then
              I_emux = 2
              L_ekux = .true.
              L_(863) = .false.
          endif
          if (L_ufobe) then
              L_(863) = .true.
              L_ekux = .false.
          endif
          if (I_emux.ne.2) then
              L_ekux = .false.
              L_(863) = .false.
          endif
      else
          L_(863) = .false.
      endif
C FDA90_logic.fgi( 321,1002):recalc:��� ������� ���������,EC005
C if(sav1.ne.L_ekux .and. try712.gt.0) goto 712
C if(sav2.ne.L_(863) .and. try712.gt.0) goto 712
C sav1=L_osev
C sav2=L_(293)
      if(L_alov) then
          if (L_(281)) then
              I_ilov = 24
              L_osev = .true.
              L_(293) = .false.
          endif
          if (L_ufobe) then
              L_(293) = .true.
              L_osev = .false.
          endif
          if (I_ilov.ne.24) then
              L_osev = .false.
              L_(293) = .false.
          endif
      else
          L_(293) = .false.
      endif
C FDA90_logic.fgi( 310,1188):recalc:��� ������� ���������,EC002
C if(sav1.ne.L_osev .and. try1495.gt.0) goto 1495
C if(sav2.ne.L_(293) .and. try1495.gt.0) goto 1495
C sav1=L_akobe
C sav2=L_(1139)
      if(L_alobe) then
          if (L_(1146)) then
              I_ilobe = 2
              L_akobe = .true.
              L_(1139) = .false.
          endif
          if (L_ufobe) then
              L_(1139) = .true.
              L_akobe = .false.
          endif
          if (I_ilobe.ne.2) then
              L_akobe = .false.
              L_(1139) = .false.
          endif
      else
          L_(1139) = .false.
      endif
C FDA90_logic.fgi( 135,1552):recalc:��� ������� ���������,EC001
C if(sav1.ne.L_akobe .and. try478.gt.0) goto 478
C if(sav2.ne.L_(1139) .and. try478.gt.0) goto 478
      L_(432)=R_emobe.lt.R0_axov
C FDA90_logic.fgi( 475,1518):���������� <
      if(L_itube) then
          if (L_(442)) then
              I_otube = 4
              L_uxov = .true.
              L_(431) = .false.
          endif
          if (L_(432)) then
              L_(431) = .true.
              L_uxov = .false.
          endif
          if (I_otube.ne.4) then
              L_uxov = .false.
              L_(431) = .false.
          endif
      else
          L_(431) = .false.
      endif
C FDA90_logic.fgi( 505,1518):��� ������� ���������,EC003
C sav1=L_(440)
      if(L_uxov.and..not.L0_oxov) then
         R0_exov=R0_ixov
      else
         R0_exov=max(R_(87)-deltat,0.0)
      endif
      L_(440)=R0_exov.gt.0.0
      L0_oxov=L_uxov
C FDA90_logic.fgi( 531,1518):recalc:������������  �� T
C if(sav1.ne.L_(440) .and. try1303.gt.0) goto 1303
      if(L_itube) then
          if (L_(431)) then
              I_otube = 5
              L_utov = .true.
              L_(426) = .false.
          endif
          if (L_erux) then
              L_(426) = .true.
              L_utov = .false.
          endif
          if (I_otube.ne.5) then
              L_utov = .false.
              L_(426) = .false.
          endif
      else
          L_(426) = .false.
      endif
C FDA90_logic.fgi( 505,1504):��� ������� ���������,EC003
      if(L_itube) then
          if (L_(426)) then
              I_otube = 6
              L_uvov = .true.
              L_(1248) = .false.
          endif
          if (L_(427)) then
              L_(1248) = .true.
              L_uvov = .false.
          endif
          if (I_otube.ne.6) then
              L_uvov = .false.
              L_(1248) = .false.
          endif
      else
          L_(1248) = .false.
      endif
C FDA90_logic.fgi( 505,1484):��� ������� ���������,EC003
      if(L_itube) then
          if (L_(1248)) then
              I_otube = 7
              L_esube = .true.
              L_(1249) = .false.
          endif
          if (L_(1251)) then
              L_(1249) = .true.
              L_esube = .false.
          endif
          if (I_otube.ne.7) then
              L_esube = .false.
              L_(1249) = .false.
          endif
      else
          L_(1249) = .false.
      endif
C FDA90_logic.fgi( 505,1461):��� ������� ���������,EC003
      if(L_esube.and..not.L0_orov) then
         R0_erov=R0_irov
      else
         R0_erov=max(R_(78)-deltat,0.0)
      endif
      L_(1229)=R0_erov.gt.0.0
      L0_orov=L_esube
C FDA90_logic.fgi( 531,1461):������������  �� T
      L_(1228)=.not.L_(1246) .and.L_(1229)
      L_(1226)=L_(1229).and..not.L_(1227)
C FDA90_logic.fgi( 554,1460):��������� ������
      L_(1125)=R_atube.lt.R0_edobe
C FDA90_logic.fgi( 109,1464):���������� <
      L_(1104)=R_erube.lt.R0_osibe
C FDA90_logic.fgi( 109,1485):���������� <
      if(L_alobe) then
          if (L_(1129)) then
              I_ilobe = 5
              L_efuv = .true.
              L_(1103) = .false.
          endif
          if (L_erux) then
              L_(1103) = .true.
              L_efuv = .false.
          endif
          if (I_ilobe.ne.5) then
              L_efuv = .false.
              L_(1103) = .false.
          endif
      else
          L_(1103) = .false.
      endif
C FDA90_logic.fgi( 135,1505):��� ������� ���������,EC001
      if(L_alobe) then
          if (L_(1103)) then
              I_ilobe = 6
              L_itibe = .true.
              L_(1122) = .false.
          endif
          if (L_(1104)) then
              L_(1122) = .true.
              L_itibe = .false.
          endif
          if (I_ilobe.ne.6) then
              L_itibe = .false.
              L_(1122) = .false.
          endif
      else
          L_(1122) = .false.
      endif
C FDA90_logic.fgi( 135,1485):��� ������� ���������,EC001
      if(L_alobe) then
          if (L_(1122)) then
              I_ilobe = 7
              L_ubobe = .true.
              L_(1123) = .false.
          endif
          if (L_(1125)) then
              L_(1123) = .true.
              L_ubobe = .false.
          endif
          if (I_ilobe.ne.7) then
              L_ubobe = .false.
              L_(1123) = .false.
          endif
      else
          L_(1123) = .false.
      endif
C FDA90_logic.fgi( 135,1464):��� ������� ���������,EC001
      if(L_ubobe.and..not.L0_obobe) then
         R0_ebobe=R0_ibobe
      else
         R0_ebobe=max(R_(308)-deltat,0.0)
      endif
      L_(1121)=R0_ebobe.gt.0.0
      L0_obobe=L_ubobe
C FDA90_logic.fgi( 161,1464):������������  �� T
      L_(1098)=.not.L_(1246) .and.L_(1121)
      L_(1096)=L_(1121).and..not.L_(1097)
C FDA90_logic.fgi( 184,1462):��������� ������
      if(L_ibabe.and..not.L0_ebabe) then
         R0_uxux=R0_ababe
      else
         R0_uxux=max(R_(242)-deltat,0.0)
      endif
      L_(921)=R0_uxux.gt.0.0
      L0_ebabe=L_ibabe
C FDA90_logic.fgi( 161, 700):������������  �� T
      L_(920)=.not.L_(1246) .and.L_(921)
      L_(918)=L_(921).and..not.L_(919)
C FDA90_logic.fgi( 183, 700):��������� ������
      if(L_obux.and..not.L0_ibux) then
         R0_abux=R0_ebux
      else
         R0_abux=max(R_(219)-deltat,0.0)
      endif
      L_(842)=R0_abux.gt.0.0
      L0_ibux=L_obux
C FDA90_logic.fgi( 347, 909):������������  �� T
      L_(836)=.not.L_(1246) .and.L_(842)
      L_(834)=L_(842).and..not.L_(835)
C FDA90_logic.fgi( 370, 908):��������� ������
      if(L_odax.and..not.L0_idax) then
         R0_adax=R0_edax
      else
         R0_adax=max(R_(124)-deltat,0.0)
      endif
      L_(547)=R0_adax.gt.0.0
      L0_idax=L_odax
C FDA90_logic.fgi( 534, 535):������������  �� T
      L_(546)=.not.L_(1246) .and.L_(547)
      L_(544)=L_(547).and..not.L_(545)
C FDA90_logic.fgi( 557, 536):��������� ������
      if(L_idiv.and..not.L0_ediv) then
         R0_ubiv=R0_adiv
      else
         R0_ubiv=max(R_(47)-deltat,0.0)
      endif
      L_(312)=R0_ubiv.gt.0.0
      L0_ediv=L_idiv
C FDA90_logic.fgi( 336,1274):������������  �� T
      L_(311)=.not.L_(1246) .and.L_(312)
      L_(309)=L_(312).and..not.L_(310)
C FDA90_logic.fgi( 359,1274):��������� ������
      iv2=0
      if(L_(1228)) iv2=ibset(iv2,0)
      if(L_(1098)) iv2=ibset(iv2,1)
      if(L_(920)) iv2=ibset(iv2,2)
      if(L_(836)) iv2=ibset(iv2,3)
      if(L_(546)) iv2=ibset(iv2,4)
      if(L_(311)) iv2=ibset(iv2,5)
      L_(206)=L_(206).or.iv2.ne.0
C FDA90_logic.fgi( 554,1460):������-�������: ������� ������ ������/����������/����� ���������
      if(L_esibe.and..not.L0_asibe) then
         R0_oribe=R0_uribe
      else
         R0_oribe=max(R_(297)-deltat,0.0)
      endif
      L_(1095)=R0_oribe.gt.0.0
      L0_asibe=L_esibe
C FDA90_logic.fgi( 161,1410):������������  �� T
      L_(1094)=.not.L_(1246) .and.L_(1095)
      L_(1092)=L_(1095).and..not.L_(1093)
C FDA90_logic.fgi( 183,1410):��������� ������
      if(L_upux.and..not.L0_opux) then
         R0_epux=R0_ipux
      else
         R0_epux=max(R_(229)-deltat,0.0)
      endif
      L_(884)=R0_epux.gt.0.0
      L0_opux=L_upux
C FDA90_logic.fgi( 161, 634):������������  �� T
      L_(883)=.not.L_(1246) .and.L_(884)
      L_(881)=L_(884).and..not.L_(882)
C FDA90_logic.fgi( 183, 634):��������� ������
      if(L_ovox.and..not.L0_ivox) then
         R0_avox=R0_evox
      else
         R0_avox=max(R_(214)-deltat,0.0)
      endif
      L_(827)=R0_avox.gt.0.0
      L0_ivox=L_ovox
C FDA90_logic.fgi( 347, 870):������������  �� T
      L_(826)=.not.L_(1246) .and.L_(827)
      L_(824)=L_(827).and..not.L_(825)
C FDA90_logic.fgi( 375, 866):��������� ������
      if(L_esuv.and..not.L0_asuv) then
         R0_oruv=R0_uruv
      else
         R0_oruv=max(R_(112)-deltat,0.0)
      endif
      L_(513)=R0_oruv.gt.0.0
      L0_asuv=L_esuv
C FDA90_logic.fgi( 534, 469):������������  �� T
      L_(512)=.not.L_(1246) .and.L_(513)
      L_(510)=L_(513).and..not.L_(511)
C FDA90_logic.fgi( 556, 470):��������� ������
      if(L_asev.and..not.L0_urev) then
         R0_irev=R0_orev
      else
         R0_irev=max(R_(34)-deltat,0.0)
      endif
      L_(274)=R0_irev.gt.0.0
      L0_urev=L_asev
C FDA90_logic.fgi( 336,1208):������������  �� T
      L_(273)=.not.L_(1246) .and.L_(274)
      L_(271)=L_(274).and..not.L_(272)
C FDA90_logic.fgi( 359,1208):��������� ������
      if(L_itube) then
          if (L_(1249)) then
              I_otube = 8
              L_osube = .true.
              L_(1250) = .false.
          endif
          if (L_isube) then
              L_(1250) = .true.
              L_osube = .false.
          endif
          if (I_otube.ne.8) then
              L_osube = .false.
              L_(1250) = .false.
          endif
      else
          L_(1250) = .false.
      endif
C FDA90_logic.fgi( 505,1444):��� ������� ���������,EC003
      L_(1247)=.not.L_(1246) .and.L_osube
      L_(1244)=L_osube.and..not.L_(1245)
C FDA90_logic.fgi( 554,1442):��������� ������
      if(L_alobe) then
          if (L_(1123)) then
              I_ilobe = 8
              L_adobe = .true.
              L_(1124) = .false.
          endif
          if (L_isube) then
              L_(1124) = .true.
              L_adobe = .false.
          endif
          if (I_ilobe.ne.8) then
              L_adobe = .false.
              L_(1124) = .false.
          endif
      else
          L_(1124) = .false.
      endif
C FDA90_logic.fgi( 135,1447):��� ������� ���������,EC001
      End
      Subroutine FDA90_3(ext_deltat)
      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      Include 'FDA90.fh'

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      L_(1120)=.not.L_(1246) .and.L_adobe
      L_(1118)=L_adobe.and..not.L_(1119)
C FDA90_logic.fgi( 184,1446):��������� ������
      L_(832)=.not.L_(1246) .and.L_uvox
      L_(830)=L_uvox.and..not.L_(831)
C FDA90_logic.fgi( 370, 888):��������� ������
      iv2=0
      if(L_(1247)) iv2=ibset(iv2,0)
      if(L_(1120)) iv2=ibset(iv2,1)
      if(L_(832)) iv2=ibset(iv2,2)
      L_(204)=L_(204).or.iv2.ne.0
C FDA90_logic.fgi( 554,1442):������-�������: ������� ������ ������/����������/����� ���������
      L_(887)=.not.L_(1246) .and.L_irux
      L_(885)=L_irux.and..not.L_(886)
C FDA90_logic.fgi( 184, 670):��������� ������
      L_(851)=.not.L_(1246) .and.L_ikux
      L_(849)=L_ikux.and..not.L_(850)
C FDA90_logic.fgi( 369, 950):��������� ������
      L_(464)=.not.L_(1246) .and.L_osuv
      L_(462)=L_osuv.and..not.L_(463)
C FDA90_logic.fgi( 558, 504):��������� ������
      L_(458)=.not.L_(1246) .and.L_efuv
      L_(456)=L_efuv.and..not.L_(457)
C FDA90_logic.fgi( 183,1506):��������� ������
      L_(421)=.not.L_(1246) .and.L_utov
      L_(419)=L_utov.and..not.L_(420)
C FDA90_logic.fgi( 553,1504):��������� ������
      L_(277)=.not.L_(1246) .and.L_isev
      L_(275)=L_isev.and..not.L_(276)
C FDA90_logic.fgi( 360,1244):��������� ������
      iv2=0
      if(L_(887)) iv2=ibset(iv2,0)
      if(L_(851)) iv2=ibset(iv2,1)
      if(L_(464)) iv2=ibset(iv2,2)
      if(L_(458)) iv2=ibset(iv2,3)
      if(L_(421)) iv2=ibset(iv2,4)
      if(L_(277)) iv2=ibset(iv2,5)
      L_(203)=L_(203).or.iv2.ne.0
C FDA90_logic.fgi( 184, 670):������-�������: ������� ������ ������/����������/����� ���������
      L_(1091)=R_atube.gt.R0_isibe
C FDA90_logic.fgi( 109,1410):���������� >
      L_(1117)=R_erube.gt.R0_exibe
C FDA90_logic.fgi( 109,1429):���������� >
      if(L_alobe) then
          if (L_(1124)) then
              I_ilobe = 9
              L_abobe = .true.
              L_(1116) = .false.
          endif
          if (L_(1117)) then
              L_(1116) = .true.
              L_abobe = .false.
          endif
          if (I_ilobe.ne.9) then
              L_abobe = .false.
              L_(1116) = .false.
          endif
      else
          L_(1116) = .false.
      endif
C FDA90_logic.fgi( 135,1429):��� ������� ���������,EC001
      if(L_alobe) then
          if (L_(1116)) then
              I_ilobe = 10
              L_esibe = .true.
              L_(1090) = .false.
          endif
          if (L_(1091)) then
              L_(1090) = .true.
              L_esibe = .false.
          endif
          if (I_ilobe.ne.10) then
              L_esibe = .false.
              L_(1090) = .false.
          endif
      else
          L_(1090) = .false.
      endif
C FDA90_logic.fgi( 135,1410):��� ������� ���������,EC001
C sav1=L_(1095)
      if(L_esibe.and..not.L0_asibe) then
         R0_oribe=R0_uribe
      else
         R0_oribe=max(R_(297)-deltat,0.0)
      endif
      L_(1095)=R0_oribe.gt.0.0
      L0_asibe=L_esibe
C FDA90_logic.fgi( 161,1410):recalc:������������  �� T
C if(sav1.ne.L_(1095) .and. try1713.gt.0) goto 1713
      if(L_alobe) then
          if (L_(1090)) then
              I_ilobe = 11
              L_iribe = .true.
              L_(1084) = .false.
          endif
          if (L_(1085)) then
              L_(1084) = .true.
              L_iribe = .false.
          endif
          if (I_ilobe.ne.11) then
              L_iribe = .false.
              L_(1084) = .false.
          endif
      else
          L_(1084) = .false.
      endif
C FDA90_logic.fgi( 135,1393):��� ������� ���������,EC001
      if(L_alobe) then
          if (L_(1084)) then
              I_ilobe = 12
              L_apibe = .true.
              L_(1109) = .false.
          endif
          if (L_ofube) then
              L_(1109) = .true.
              L_apibe = .false.
          endif
          if (I_ilobe.ne.12) then
              L_apibe = .false.
              L_(1109) = .false.
          endif
      else
          L_(1109) = .false.
      endif
C FDA90_logic.fgi( 135,1375):��� ������� ���������,EC001
      if(L_alobe) then
          if (L_(1109)) then
              I_ilobe = 13
              L_axibe = .true.
              L_(1110) = .false.
          endif
          if (L_(1111)) then
              L_(1110) = .true.
              L_axibe = .false.
          endif
          if (I_ilobe.ne.13) then
              L_axibe = .false.
              L_(1110) = .false.
          endif
      else
          L_(1110) = .false.
      endif
C FDA90_logic.fgi( 135,1361):��� ������� ���������,EC001
      if(L_axibe.and..not.L0_uvibe) then
         R0_ivibe=R0_ovibe
      else
         R0_ivibe=max(R_(303)-deltat,0.0)
      endif
      L_(1108)=R0_ivibe.gt.0.0
      L0_uvibe=L_axibe
C FDA90_logic.fgi( 161,1361):������������  �� T
      L_(1107)=.not.L_(1246) .and.L_(1108)
      L_(1105)=L_(1108).and..not.L_(1106)
C FDA90_logic.fgi( 181,1362):��������� ������
      if(L_itibe.and..not.L0_etibe) then
         R0_usibe=R0_atibe
      else
         R0_usibe=max(R_(301)-deltat,0.0)
      endif
      L_(1102)=R0_usibe.gt.0.0
      L0_etibe=L_itibe
C FDA90_logic.fgi( 161,1485):������������  �� T
      L_(1101)=.not.L_(1246) .and.L_(1102)
      L_(1099)=L_(1102).and..not.L_(1100)
C FDA90_logic.fgi( 181,1486):��������� ������
      if(L_oxux.and..not.L0_ixux) then
         R0_axux=R0_exux
      else
         R0_axux=max(R_(240)-deltat,0.0)
      endif
      L_(915)=R0_axux.gt.0.0
      L0_ixux=L_oxux
C FDA90_logic.fgi( 161, 684):������������  �� T
      L_(914)=.not.L_(1246) .and.L_(915)
      L_(912)=L_(915).and..not.L_(913)
C FDA90_logic.fgi( 181, 684):��������� ������
      if(L_uxox.and..not.L0_oxox) then
         R0_exox=R0_ixox
      else
         R0_exox=max(R_(218)-deltat,0.0)
      endif
      L_(840)=R0_exox.gt.0.0
      L0_oxox=L_uxox
C FDA90_logic.fgi( 348, 930):������������  �� T
      L_(839)=.not.L_(1246) .and.L_(840)
      L_(837)=L_(840).and..not.L_(838)
C FDA90_logic.fgi( 368, 930):��������� ������
      if(L_isox.and..not.L0_esox) then
         R0_urox=R0_asox
      else
         R0_urox=max(R_(209)-deltat,0.0)
      endif
      L_(813)=R0_urox.gt.0.0
      L0_esox=L_isox
C FDA90_logic.fgi( 347, 821):������������  �� T
      L_(812)=.not.L_(1246) .and.L_(813)
      L_(810)=L_(813).and..not.L_(811)
C FDA90_logic.fgi( 367, 822):��������� ������
      if(L_omabe.and..not.L0_imabe) then
         R0_amabe=R0_emabe
      else
         R0_amabe=max(R_(252)-deltat,0.0)
      endif
      L_(951)=R0_amabe.gt.0.0
      L0_imabe=L_omabe
C FDA90_logic.fgi( 161, 801):������������  �� T
      L_(673)=.not.L_(1246) .and.L_(951)
      L_(671)=L_(951).and..not.L_(672)
C FDA90_logic.fgi( 182, 802):��������� ������
      if(L_ubax.and..not.L0_obax) then
         R0_ebax=R0_ibax
      else
         R0_ebax=max(R_(122)-deltat,0.0)
      endif
      L_(541)=R0_ebax.gt.0.0
      L0_obax=L_ubax
C FDA90_logic.fgi( 534, 519):������������  �� T
      L_(540)=.not.L_(1246) .and.L_(541)
      L_(538)=L_(541).and..not.L_(539)
C FDA90_logic.fgi( 555, 520):��������� ������
      if(L_upax.and..not.L0_opax) then
         R0_epax=R0_ipax
      else
         R0_epax=max(R_(134)-deltat,0.0)
      endif
      L_(577)=R0_epax.gt.0.0
      L0_opax=L_upax
C FDA90_logic.fgi( 534, 636):������������  �� T
      L_(499)=.not.L_(1246) .and.L_(577)
      L_(497)=L_(577).and..not.L_(498)
C FDA90_logic.fgi( 555, 636):��������� ������
      if(L_uvov.and..not.L0_ovov) then
         R0_evov=R0_ivov
      else
         R0_evov=max(R_(86)-deltat,0.0)
      endif
      L_(425)=R0_evov.gt.0.0
      L0_ovov=L_uvov
C FDA90_logic.fgi( 532,1484):������������  �� T
      L_(424)=.not.L_(1246) .and.L_(425)
      L_(422)=L_(425).and..not.L_(423)
C FDA90_logic.fgi( 551,1484):��������� ������
      if(L_opiv.and..not.L0_ipiv) then
         R0_apiv=R0_epiv
      else
         R0_apiv=max(R_(58)-deltat,0.0)
      endif
      L_(345)=R0_apiv.gt.0.0
      L0_ipiv=L_opiv
C FDA90_logic.fgi( 336,1375):������������  �� T
      L_(344)=.not.L_(1246) .and.L_(345)
      L_(342)=L_(345).and..not.L_(343)
C FDA90_logic.fgi( 357,1376):��������� ������
      if(L_obiv.and..not.L0_ibiv) then
         R0_abiv=R0_ebiv
      else
         R0_abiv=max(R_(45)-deltat,0.0)
      endif
      L_(306)=R0_abiv.gt.0.0
      L0_ibiv=L_obiv
C FDA90_logic.fgi( 336,1258):������������  �� T
      L_(305)=.not.L_(1246) .and.L_(306)
      L_(303)=L_(306).and..not.L_(304)
C FDA90_logic.fgi( 357,1258):��������� ������
      iv2=0
      if(L_(1232)) iv2=ibset(iv2,0)
      if(L_(1107)) iv2=ibset(iv2,1)
      if(L_(1101)) iv2=ibset(iv2,2)
      if(L_(914)) iv2=ibset(iv2,3)
      if(L_(839)) iv2=ibset(iv2,4)
      if(L_(812)) iv2=ibset(iv2,5)
      if(L_(673)) iv2=ibset(iv2,6)
      if(L_(540)) iv2=ibset(iv2,7)
      if(L_(499)) iv2=ibset(iv2,8)
      if(L_(424)) iv2=ibset(iv2,9)
      if(L_(344)) iv2=ibset(iv2,10)
      if(L_(305)) iv2=ibset(iv2,11)
      L_(234)=L_(234).or.iv2.ne.0
C FDA90_logic.fgi( 551,1376):������-�������: ������� ������ ������/����������/����� ���������
      if(L_ubube.and..not.L0_obube) then
         R0_ebube=R0_ibube
      else
         R0_ebube=max(R_(329)-deltat,0.0)
      endif
      L_(1197)=R0_ebube.gt.0.0
      L0_obube=L_ubube
C FDA90_logic.fgi( 531,1329):������������  �� T
      L_(1196)=.not.L_(1246) .and.L_(1197)
      L_(1194)=L_(1197).and..not.L_(1195)
C FDA90_logic.fgi( 551,1330):��������� ������
      if(L_abobe.and..not.L0_uxibe) then
         R0_ixibe=R0_oxibe
      else
         R0_ixibe=max(R_(306)-deltat,0.0)
      endif
      L_(1115)=R0_ixibe.gt.0.0
      L0_uxibe=L_abobe
C FDA90_logic.fgi( 161,1429):������������  �� T
      L_(1114)=.not.L_(1246) .and.L_(1115)
      L_(1112)=L_(1115).and..not.L_(1113)
C FDA90_logic.fgi( 181,1430):��������� ������
      if(L_alobe) then
          if (L_(1110)) then
              I_ilobe = 14
              L_epibe = .true.
              L_(1083) = .false.
          endif
          if (L_akube) then
              L_(1083) = .true.
              L_epibe = .false.
          endif
          if (I_ilobe.ne.14) then
              L_epibe = .false.
              L_(1083) = .false.
          endif
      else
          L_(1083) = .false.
      endif
C FDA90_logic.fgi( 135,1348):��� ������� ���������,EC001
      if(L_alobe) then
          if (L_(1083)) then
              I_ilobe = 15
              L_ilibe = .true.
              L_(1067) = .false.
          endif
          if (L_(1068)) then
              L_(1067) = .true.
              L_ilibe = .false.
          endif
          if (I_ilobe.ne.15) then
              L_ilibe = .false.
              L_(1067) = .false.
          endif
      else
          L_(1067) = .false.
      endif
C FDA90_logic.fgi( 135,1329):��� ������� ���������,EC001
      if(L_alobe) then
          if (L_(1067)) then
              I_ilobe = 16
              L_ikibe = .true.
              L_(1077) = .false.
          endif
          if (L_(1066)) then
              L_(1077) = .true.
              L_ikibe = .false.
          endif
          if (I_ilobe.ne.16) then
              L_ikibe = .false.
              L_(1077) = .false.
          endif
      else
          L_(1077) = .false.
      endif
C FDA90_logic.fgi( 135,1316):��� ������� ���������,EC001
      if(L_ikibe.and..not.L0_ekibe) then
         R0_ufibe=R0_akibe
      else
         R0_ufibe=max(R_(288)-deltat,0.0)
      endif
      L_(1065)=R0_ufibe.gt.0.0
      L0_ekibe=L_ikibe
C FDA90_logic.fgi( 161,1316):������������  �� T
      L_(1064)=.not.L_(1246) .and.L_(1065)
      L_(1062)=L_(1065).and..not.L_(1063)
C FDA90_logic.fgi( 181,1316):��������� ������
      if(L_ikabe.and..not.L0_ekabe) then
         R0_ufabe=R0_akabe
      else
         R0_ufabe=max(R_(248)-deltat,0.0)
      endif
      L_(938)=R0_ufabe.gt.0.0
      L0_ekabe=L_ikabe
C FDA90_logic.fgi( 161, 751):������������  �� T
      L_(937)=.not.L_(1246) .and.L_(938)
      L_(935)=L_(938).and..not.L_(936)
C FDA90_logic.fgi( 181, 752):��������� ������
      if(L_imox.and..not.L0_emox) then
         R0_ulox=R0_amox
      else
         R0_ulox=max(R_(203)-deltat,0.0)
      endif
      L_(790)=R0_ulox.gt.0.0
      L0_emox=L_imox
C FDA90_logic.fgi( 347, 737):������������  �� T
      L_(789)=.not.L_(1246) .and.L_(790)
      L_(787)=L_(790).and..not.L_(788)
C FDA90_logic.fgi( 367, 738):��������� ������
      if(L_olax.and..not.L0_ilax) then
         R0_alax=R0_elax
      else
         R0_alax=max(R_(130)-deltat,0.0)
      endif
      L_(564)=R0_alax.gt.0.0
      L0_ilax=L_olax
C FDA90_logic.fgi( 534, 586):������������  �� T
      L_(563)=.not.L_(1246) .and.L_(564)
      L_(561)=L_(564).and..not.L_(562)
C FDA90_logic.fgi( 556, 586):��������� ������
      if(L_iliv.and..not.L0_eliv) then
         R0_ukiv=R0_aliv
      else
         R0_ukiv=max(R_(53)-deltat,0.0)
      endif
      L_(329)=R0_ukiv.gt.0.0
      L0_eliv=L_iliv
C FDA90_logic.fgi( 336,1325):������������  �� T
      L_(328)=.not.L_(1246) .and.L_(329)
      L_(326)=L_(329).and..not.L_(327)
C FDA90_logic.fgi( 357,1326):��������� ������
      L_(1211)=.not.L_(1246) .and.L_ekube
      L_(1209)=L_ekube.and..not.L_(1210)
C FDA90_logic.fgi( 554,1360):��������� ������
      L_(808)=.not.L_(1246) .and.L_irox
      L_(806)=L_irox.and..not.L_(807)
C FDA90_logic.fgi( 370, 806):��������� ������
      iv2=0
      if(L_(1228)) iv2=ibset(iv2,0)
      if(L_(1211)) iv2=ibset(iv2,1)
      if(L_(1098)) iv2=ibset(iv2,2)
      if(L_(887)) iv2=ibset(iv2,3)
      if(L_(836)) iv2=ibset(iv2,4)
      if(L_(808)) iv2=ibset(iv2,5)
      if(L_(464)) iv2=ibset(iv2,6)
      if(L_(277)) iv2=ibset(iv2,7)
      L_(232)=L_(232).or.iv2.ne.0
C FDA90_logic.fgi( 554,1460):������-�������: ������� ������ ������/����������/����� ���������
      L_(943)=.not.L_(1246) .and.L_okabe
      L_(941)=L_okabe.and..not.L_(942)
C FDA90_logic.fgi( 184, 766):��������� ������
      L_(569)=.not.L_(1246) .and.L_ulax
      L_(567)=L_ulax.and..not.L_(568)
C FDA90_logic.fgi( 557, 600):��������� ������
      L_(334)=.not.L_(1246) .and.L_oliv
      L_(332)=L_oliv.and..not.L_(333)
C FDA90_logic.fgi( 360,1340):��������� ������
      iv2=0
      if(L_(1247)) iv2=ibset(iv2,0)
      if(L_(1120)) iv2=ibset(iv2,1)
      if(L_(943)) iv2=ibset(iv2,2)
      if(L_(832)) iv2=ibset(iv2,3)
      if(L_(569)) iv2=ibset(iv2,4)
      if(L_(334)) iv2=ibset(iv2,5)
      L_(231)=L_(231).or.iv2.ne.0
C FDA90_logic.fgi( 554,1442):������-�������: ������� ������ ������/����������/����� ���������
      L_(1237)=R_erube.gt.R0_arube
C FDA90_logic.fgi( 474,1426):���������� >
      L_(1214)=R_atube.gt.R0_alube
C FDA90_logic.fgi( 474,1417):���������� >
      L_(1213)=R_ukube.lt.R0_okube
C FDA90_logic.fgi( 474,1406):���������� <
      L_(1243) = L_(1237).AND.L_(1214).AND.L_(1213)
C FDA90_logic.fgi( 482,1424):�
      if(L_itube) then
          if (L_(1250)) then
              I_otube = 9
              L_asube = .true.
              L_(1242) = .false.
          endif
          if (L_(1243)) then
              L_(1242) = .true.
              L_asube = .false.
          endif
          if (I_otube.ne.9) then
              L_asube = .false.
              L_(1242) = .false.
          endif
      else
          L_(1242) = .false.
      endif
C FDA90_logic.fgi( 505,1426):��� ������� ���������,EC003
      if(L_asube.and..not.L0_urube) then
         R0_irube=R0_orube
      else
         R0_irube=max(R_(343)-deltat,0.0)
      endif
      L_(1241)=R0_irube.gt.0.0
      L0_urube=L_asube
C FDA90_logic.fgi( 531,1426):������������  �� T
      L_(1240)=.not.L_(1246) .and.L_(1241)
      L_(1238)=L_(1241).and..not.L_(1239)
C FDA90_logic.fgi( 558,1422):��������� ������
      if(L_itube) then
          if (L_(1242)) then
              I_otube = 10
              L_ufube = .true.
              L_(1234) = .false.
          endif
          if (L_ofube) then
              L_(1234) = .true.
              L_ufube = .false.
          endif
          if (I_otube.ne.10) then
              L_ufube = .false.
              L_(1234) = .false.
          endif
      else
          L_(1234) = .false.
      endif
C FDA90_logic.fgi( 505,1389):��� ������� ���������,EC003
      if(L_itube) then
          if (L_(1234)) then
              I_otube = 11
              L_upube = .true.
              L_(1235) = .false.
          endif
          if (L_(1236)) then
              L_(1235) = .true.
              L_upube = .false.
          endif
          if (I_otube.ne.11) then
              L_upube = .false.
              L_(1235) = .false.
          endif
      else
          L_(1235) = .false.
      endif
C FDA90_logic.fgi( 505,1375):��� ������� ���������,EC003
C sav1=L_(1233)
      if(L_upube.and..not.L0_opube) then
         R0_epube=R0_ipube
      else
         R0_epube=max(R_(340)-deltat,0.0)
      endif
      L_(1233)=R0_epube.gt.0.0
      L0_opube=L_upube
C FDA90_logic.fgi( 531,1375):recalc:������������  �� T
C if(sav1.ne.L_(1233) .and. try455.gt.0) goto 455
      if(L_itube) then
          if (L_(1235)) then
              I_otube = 12
              L_ekube = .true.
              L_(1212) = .false.
          endif
          if (L_akube) then
              L_(1212) = .true.
              L_ekube = .false.
          endif
          if (I_otube.ne.12) then
              L_ekube = .false.
              L_(1212) = .false.
          endif
      else
          L_(1212) = .false.
      endif
C FDA90_logic.fgi( 505,1362):��� ������� ���������,EC003
C sav1=L_(1211)
C sav2=L_(1210)
C sav3=L_(1209)
C sav4=R_(334)
C FDA90_logic.fgi( 554,1360):recalc:��������� ������
C if(sav1.ne.L_(1211) .and. try1837.gt.0) goto 1837
C if(sav2.ne.L_(1210) .and. try1837.gt.0) goto 1837
C if(sav3.ne.L_(1209) .and. try1837.gt.0) goto 1837
C if(sav4.ne.R_(334) .and. try1837.gt.0) goto 1837
      if(L_itube) then
          if (L_(1212)) then
              I_otube = 13
              L_udube = .true.
              L_(1199) = .false.
          endif
          if (L_(1200)) then
              L_(1199) = .true.
              L_udube = .false.
          endif
          if (I_otube.ne.13) then
              L_udube = .false.
              L_(1199) = .false.
          endif
      else
          L_(1199) = .false.
      endif
C FDA90_logic.fgi( 505,1342):��� ������� ���������,EC003
      if(L_itube) then
          if (L_(1199)) then
              I_otube = 14
              L_ubube = .true.
              L_(1219) = .false.
          endif
          if (L_(1198)) then
              L_(1219) = .true.
              L_ubube = .false.
          endif
          if (I_otube.ne.14) then
              L_ubube = .false.
              L_(1219) = .false.
          endif
      else
          L_(1219) = .false.
      endif
C FDA90_logic.fgi( 505,1329):��� ������� ���������,EC003
C sav1=L_(1197)
      if(L_ubube.and..not.L0_obube) then
         R0_ebube=R0_ibube
      else
         R0_ebube=max(R_(329)-deltat,0.0)
      endif
      L_(1197)=R0_ebube.gt.0.0
      L0_obube=L_ubube
C FDA90_logic.fgi( 531,1329):recalc:������������  �� T
C if(sav1.ne.L_(1197) .and. try1804.gt.0) goto 1804
      if(L_itube) then
          if (L_(1219)) then
              I_otube = 15
              L_amube = .true.
              L_(1224) = .false.
          endif
          if (L_ulube) then
              L_(1224) = .true.
              L_amube = .false.
          endif
          if (I_otube.ne.15) then
              L_amube = .false.
              L_(1224) = .false.
          endif
      else
          L_(1224) = .false.
      endif
C FDA90_logic.fgi( 505,1315):��� ������� ���������,EC003
      if(L_itube) then
          if (L_(1224)) then
              I_otube = 16
              L_apube = .true.
              L_(1225) = .false.
          endif
          if (L_umube) then
              L_(1225) = .true.
              L_apube = .false.
          endif
          if (I_otube.ne.16) then
              L_apube = .false.
              L_(1225) = .false.
          endif
      else
          L_(1225) = .false.
      endif
C FDA90_logic.fgi( 505,1301):��� ������� ���������,EC003
      if(L_itube) then
          if (L_(1225)) then
              I_otube = 17
              L_uxobe = .true.
              L_(1192) = .false.
          endif
          if (L_(1193)) then
              L_(1192) = .true.
              L_uxobe = .false.
          endif
          if (I_otube.ne.17) then
              L_uxobe = .false.
              L_(1192) = .false.
          endif
      else
          L_(1192) = .false.
      endif
C FDA90_logic.fgi( 505,1288):��� ������� ���������,EC003
C sav1=L_(1191)
      if(L_uxobe.and..not.L0_oxobe) then
         R0_exobe=R0_ixobe
      else
         R0_exobe=max(R_(326)-deltat,0.0)
      endif
      L_(1191)=R0_exobe.gt.0.0
      L0_oxobe=L_uxobe
C FDA90_logic.fgi( 531,1288):recalc:������������  �� T
C if(sav1.ne.L_(1191) .and. try516.gt.0) goto 516
      if(L_itube) then
          if (L_(1192)) then
              I_otube = 18
              L_ovobe = .true.
              L_(1186) = .false.
          endif
          if (L_(1187)) then
              L_(1186) = .true.
              L_ovobe = .false.
          endif
          if (I_otube.ne.18) then
              L_ovobe = .false.
              L_(1186) = .false.
          endif
      else
          L_(1186) = .false.
      endif
C FDA90_logic.fgi( 505,1274):��� ������� ���������,EC003
      if(L_ovobe.and..not.L0_ivobe) then
         R0_avobe=R0_evobe
      else
         R0_avobe=max(R_(324)-deltat,0.0)
      endif
      L_(1185)=R0_avobe.gt.0.0
      L0_ivobe=L_ovobe
C FDA90_logic.fgi( 531,1274):������������  �� T
      L_(1184)=.not.L_(1246) .and.L_(1185)
      L_(1182)=L_(1185).and..not.L_(1183)
C FDA90_logic.fgi( 553,1274):��������� ������
      if(L_iribe.and..not.L0_eribe) then
         R0_upibe=R0_aribe
      else
         R0_upibe=max(R_(295)-deltat,0.0)
      endif
      L_(1089)=R0_upibe.gt.0.0
      L0_eribe=L_iribe
C FDA90_logic.fgi( 161,1393):������������  �� T
      L_(1088)=.not.L_(1246) .and.L_(1089)
      L_(1086)=L_(1089).and..not.L_(1087)
C FDA90_logic.fgi( 183,1394):��������� ������
      if(L_alobe) then
          if (L_(1077)) then
              I_ilobe = 17
              L_emibe = .true.
              L_(1078) = .false.
          endif
          if (L_ulube) then
              L_(1078) = .true.
              L_emibe = .false.
          endif
          if (I_ilobe.ne.17) then
              L_emibe = .false.
              L_(1078) = .false.
          endif
      else
          L_(1078) = .false.
      endif
C FDA90_logic.fgi( 135,1302):��� ������� ���������,EC001
      if(L_alobe) then
          if (L_(1078)) then
              I_ilobe = 18
              L_efibe = .true.
              L_(1061) = .false.
          endif
          if (L_afibe) then
              L_(1061) = .true.
              L_efibe = .false.
          endif
          if (I_ilobe.ne.18) then
              L_efibe = .false.
              L_(1061) = .false.
          endif
      else
          L_(1061) = .false.
      endif
C FDA90_logic.fgi( 135,1288):��� ������� ���������,EC001
      if(L_alobe) then
          if (L_(1061)) then
              I_ilobe = 19
              L_edibe = .true.
              L_(1055) = .false.
          endif
          if (L_(1056)) then
              L_(1055) = .true.
              L_edibe = .false.
          endif
          if (I_ilobe.ne.19) then
              L_edibe = .false.
              L_(1055) = .false.
          endif
      else
          L_(1055) = .false.
      endif
C FDA90_logic.fgi( 135,1277):��� ������� ���������,EC001
C sav1=L_(1054)
      if(L_edibe.and..not.L0_adibe) then
         R0_obibe=R0_ubibe
      else
         R0_obibe=max(R_(283)-deltat,0.0)
      endif
      L_(1054)=R0_obibe.gt.0.0
      L0_adibe=L_edibe
C FDA90_logic.fgi( 161,1277):recalc:������������  �� T
C if(sav1.ne.L_(1054) .and. try1332.gt.0) goto 1332
      if(L_alobe) then
          if (L_(1055)) then
              I_ilobe = 20
              L_abibe = .true.
              L_(1049) = .false.
          endif
          if (L_(1050)) then
              L_(1049) = .true.
              L_abibe = .false.
          endif
          if (I_ilobe.ne.20) then
              L_abibe = .false.
              L_(1049) = .false.
          endif
      else
          L_(1049) = .false.
      endif
C FDA90_logic.fgi( 135,1257):��� ������� ���������,EC001
      if(L_abibe.and..not.L0_uxebe) then
         R0_ixebe=R0_oxebe
      else
         R0_ixebe=max(R_(281)-deltat,0.0)
      endif
      L_(1048)=R0_ixebe.gt.0.0
      L0_uxebe=L_abibe
C FDA90_logic.fgi( 161,1257):������������  �� T
      L_(1047)=.not.L_(1246) .and.L_(1048)
      L_(1045)=L_(1048).and..not.L_(1046)
C FDA90_logic.fgi( 183,1258):��������� ������
      if(L_ixabe.and..not.L0_exabe) then
         R0_uvabe=R0_axabe
      else
         R0_uvabe=max(R_(264)-deltat,0.0)
      endif
      L_(991)=R0_uvabe.gt.0.0
      L0_exabe=L_ixabe
C FDA90_logic.fgi( 161, 938):������������  �� T
      L_(990)=.not.L_(1246) .and.L_(991)
      L_(988)=L_(991).and..not.L_(989)
C FDA90_logic.fgi( 183, 938):��������� ������
      if(L_olabe.and..not.L0_ilabe) then
         R0_alabe=R0_elabe
      else
         R0_alabe=max(R_(250)-deltat,0.0)
      endif
      L_(950)=R0_alabe.gt.0.0
      L0_ilabe=L_olabe
C FDA90_logic.fgi( 161, 783):������������  �� T
      L_(949)=.not.L_(1246) .and.L_(950)
      L_(947)=L_(950).and..not.L_(948)
C FDA90_logic.fgi( 183, 784):��������� ������
      if(L_udox.and..not.L0_odox) then
         R0_edox=R0_idox
      else
         R0_edox=max(R_(195)-deltat,0.0)
      endif
      L_(770)=R0_edox.gt.0.0
      L0_odox=L_udox
C FDA90_logic.fgi( 347, 690):������������  �� T
      L_(769)=.not.L_(1246) .and.L_(770)
      L_(767)=L_(770).and..not.L_(768)
C FDA90_logic.fgi( 369, 690):��������� ������
      if(L_alex.and..not.L0_ukex) then
         R0_ikex=R0_okex
      else
         R0_ikex=max(R_(153)-deltat,0.0)
      endif
      L_(635)=R0_ikex.gt.0.0
      L0_ukex=L_alex
C FDA90_logic.fgi( 534, 853):������������  �� T
      L_(634)=.not.L_(1246) .and.L_(635)
      L_(632)=L_(635).and..not.L_(633)
C FDA90_logic.fgi( 556, 854):��������� ������
      if(L_umax.and..not.L0_omax) then
         R0_emax=R0_imax
      else
         R0_emax=max(R_(132)-deltat,0.0)
      endif
      L_(576)=R0_emax.gt.0.0
      L0_omax=L_umax
C FDA90_logic.fgi( 534, 618):������������  �� T
      L_(575)=.not.L_(1246) .and.L_(576)
      L_(573)=L_(576).and..not.L_(574)
C FDA90_logic.fgi( 556, 618):��������� ������
      if(L_exiv.and..not.L0_axiv) then
         R0_oviv=R0_uviv
      else
         R0_oviv=max(R_(67)-deltat,0.0)
      endif
      L_(373)=R0_oviv.gt.0.0
      L0_axiv=L_exiv
C FDA90_logic.fgi( 336,1512):������������  �� T
      L_(372)=.not.L_(1246) .and.L_(373)
      L_(370)=L_(373).and..not.L_(371)
C FDA90_logic.fgi( 359,1512):��������� ������
      if(L_omiv.and..not.L0_imiv) then
         R0_amiv=R0_emiv
      else
         R0_amiv=max(R_(55)-deltat,0.0)
      endif
      L_(341)=R0_amiv.gt.0.0
      L0_imiv=L_omiv
C FDA90_logic.fgi( 336,1357):������������  �� T
      L_(340)=.not.L_(1246) .and.L_(341)
      L_(338)=L_(341).and..not.L_(339)
C FDA90_logic.fgi( 359,1358):��������� ������
      iv2=0
      if(L_(1240)) iv2=ibset(iv2,0)
      if(L_(1184)) iv2=ibset(iv2,1)
      if(L_(1088)) iv2=ibset(iv2,2)
      if(L_(1047)) iv2=ibset(iv2,3)
      if(L_(990)) iv2=ibset(iv2,4)
      if(L_(949)) iv2=ibset(iv2,5)
      if(L_(826)) iv2=ibset(iv2,6)
      if(L_(769)) iv2=ibset(iv2,7)
      if(L_(634)) iv2=ibset(iv2,8)
      if(L_(575)) iv2=ibset(iv2,9)
      if(L_(372)) iv2=ibset(iv2,10)
      if(L_(340)) iv2=ibset(iv2,11)
      L_(199)=L_(199).or.iv2.ne.0
C FDA90_logic.fgi( 558,1422):������-�������: ������� ������ ������/����������/����� ���������
      if(L_udube.and..not.L0_odube) then
         R0_edube=R0_idube
      else
         R0_edube=max(R_(330)-deltat,0.0)
      endif
      L_(1204)=R0_edube.gt.0.0
      L0_odube=L_udube
C FDA90_logic.fgi( 531,1342):������������  �� T
      L_(1203)=.not.L_(1246) .and.L_(1204)
      L_(1201)=L_(1204).and..not.L_(1202)
C FDA90_logic.fgi( 553,1342):��������� ������
      if(L_ilibe.and..not.L0_elibe) then
         R0_ukibe=R0_alibe
      else
         R0_ukibe=max(R_(289)-deltat,0.0)
      endif
      L_(1072)=R0_ukibe.gt.0.0
      L0_elibe=L_ilibe
C FDA90_logic.fgi( 161,1329):������������  �� T
      L_(1071)=.not.L_(1246) .and.L_(1072)
      L_(1069)=L_(1072).and..not.L_(1070)
C FDA90_logic.fgi( 183,1330):��������� ������
      if(L_etabe.and..not.L0_atabe) then
         R0_osabe=R0_usabe
      else
         R0_osabe=max(R_(259)-deltat,0.0)
      endif
      L_(975)=R0_osabe.gt.0.0
      L0_atabe=L_etabe
C FDA90_logic.fgi( 161, 877):������������  �� T
      L_(974)=.not.L_(1246) .and.L_(975)
      L_(972)=L_(975).and..not.L_(973)
C FDA90_logic.fgi( 183, 878):��������� ������
      if(L_odabe.and..not.L0_idabe) then
         R0_adabe=R0_edabe
      else
         R0_adabe=max(R_(243)-deltat,0.0)
      endif
      L_(929)=R0_adabe.gt.0.0
      L0_idabe=L_odabe
C FDA90_logic.fgi( 161, 718):������������  �� T
      L_(928)=.not.L_(1246) .and.L_(929)
      L_(926)=L_(929).and..not.L_(927)
C FDA90_logic.fgi( 183, 718):��������� ������
      if(L_amix.and..not.L0_ulix) then
         R0_ilix=R0_olix
      else
         R0_ilix=max(R_(178)-deltat,0.0)
      endif
      L_(722)=R0_ilix.gt.0.0
      L0_ulix=L_amix
C FDA90_logic.fgi( 347, 535):������������  �� T
      End
      Subroutine FDA90_4(ext_deltat)
      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      Include 'FDA90.fh'

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      L_(721)=.not.L_(1246) .and.L_(722)
      L_(719)=L_(722).and..not.L_(720)
C FDA90_logic.fgi( 369, 536):��������� ������
      if(L_otax.and..not.L0_itax) then
         R0_atax=R0_etax
      else
         R0_atax=max(R_(139)-deltat,0.0)
      endif
      L_(597)=R0_atax.gt.0.0
      L0_itax=L_otax
C FDA90_logic.fgi( 534, 719):������������  �� T
      L_(596)=.not.L_(1246) .and.L_(597)
      L_(594)=L_(597).and..not.L_(595)
C FDA90_logic.fgi( 556, 720):��������� ������
      if(L_ufax.and..not.L0_ofax) then
         R0_efax=R0_ifax
      else
         R0_efax=max(R_(125)-deltat,0.0)
      endif
      L_(555)=R0_efax.gt.0.0
      L0_ofax=L_ufax
C FDA90_logic.fgi( 534, 553):������������  �� T
      L_(554)=.not.L_(1246) .and.L_(555)
      L_(552)=L_(555).and..not.L_(553)
C FDA90_logic.fgi( 556, 554):��������� ������
      if(L_itube) then
          if (L_(1186)) then
              I_otube = 19
              L_utobe = .true.
              L_(1178) = .false.
          endif
          if (L_otobe) then
              L_(1178) = .true.
              L_utobe = .false.
          endif
          if (I_otube.ne.19) then
              L_utobe = .false.
              L_(1178) = .false.
          endif
      else
          L_(1178) = .false.
      endif
C FDA90_logic.fgi( 505,1255):��� ������� ���������,EC003
C sav1=L_(1181)
C sav2=L_(1180)
C sav3=L_(1179)
C sav4=R_(323)
C FDA90_logic.fgi( 554,1254):recalc:��������� ������
C if(sav1.ne.L_(1181) .and. try596.gt.0) goto 596
C if(sav2.ne.L_(1180) .and. try596.gt.0) goto 596
C if(sav3.ne.L_(1179) .and. try596.gt.0) goto 596
C if(sav4.ne.R_(323) .and. try596.gt.0) goto 596
      if(L_itube) then
          if (L_(1178)) then
              I_otube = 20
              L_itobe = .true.
              L_(1176) = .false.
          endif
          if (L_(1177)) then
              L_(1176) = .true.
              L_itobe = .false.
          endif
          if (I_otube.ne.20) then
              L_itobe = .false.
              L_(1176) = .false.
          endif
      else
          L_(1176) = .false.
      endif
C FDA90_logic.fgi( 505,1241):��� ������� ���������,EC003
C sav1=L_(1175)
      if(L_itobe.and..not.L0_etobe) then
         R0_usobe=R0_atobe
      else
         R0_usobe=max(R_(322)-deltat,0.0)
      endif
      L_(1175)=R0_usobe.gt.0.0
      L0_etobe=L_itobe
C FDA90_logic.fgi( 531,1241):recalc:������������  �� T
C if(sav1.ne.L_(1175) .and. try567.gt.0) goto 567
      if(L_itube) then
          if (L_(1176)) then
              I_otube = 21
              L_esobe = .true.
              L_(1171) = .false.
          endif
          if (L_asobe) then
              L_(1171) = .true.
              L_esobe = .false.
          endif
          if (I_otube.ne.21) then
              L_esobe = .false.
              L_(1171) = .false.
          endif
      else
          L_(1171) = .false.
      endif
C FDA90_logic.fgi( 505,1226):��� ������� ���������,EC003
      if(L_itube) then
          if (L_(1171)) then
              I_otube = 22
              L_osov = .true.
              L_(1164) = .false.
          endif
          if (L_(1151)) then
              L_(1164) = .true.
              L_osov = .false.
          endif
          if (I_otube.ne.22) then
              L_osov = .false.
              L_(1164) = .false.
          endif
      else
          L_(1164) = .false.
      endif
C FDA90_logic.fgi( 505,1212):��� ������� ���������,EC003
      if(L_osov.and..not.L0_isov) then
         R0_asov=R0_esov
      else
         R0_asov=max(R_(79)-deltat,0.0)
      endif
      L_(408)=R0_asov.gt.0.0
      L0_isov=L_osov
C FDA90_logic.fgi( 531,1212):������������  �� T
      L_(407)=.not.L_(1246) .and.L_(408)
      L_(405)=L_(408).and..not.L_(406)
C FDA90_logic.fgi( 554,1212):��������� ������
      if(L_eviv.and..not.L0_aviv) then
         R0_otiv=R0_utiv
      else
         R0_otiv=max(R_(65)-deltat,0.0)
      endif
      L_(369)=R0_otiv.gt.0.0
      L0_aviv=L_eviv
C FDA90_logic.fgi( 336,1438):������������  �� T
      L_(368)=.not.L_(1246) .and.L_(369)
      L_(366)=L_(369).and..not.L_(367)
C FDA90_logic.fgi( 359,1438):��������� ������
      if(L_ofiv.and..not.L0_ifiv) then
         R0_afiv=R0_efiv
      else
         R0_afiv=max(R_(48)-deltat,0.0)
      endif
      L_(320)=R0_afiv.gt.0.0
      L0_ifiv=L_ofiv
C FDA90_logic.fgi( 336,1292):������������  �� T
      L_(319)=.not.L_(1246) .and.L_(320)
      L_(317)=L_(320).and..not.L_(318)
C FDA90_logic.fgi( 359,1292):��������� ������
      iv2=0
      if(L_(1203)) iv2=ibset(iv2,0)
      if(L_(1071)) iv2=ibset(iv2,1)
      if(L_(1020)) iv2=ibset(iv2,2)
      if(L_(974)) iv2=ibset(iv2,3)
      if(L_(928)) iv2=ibset(iv2,4)
      if(L_(798)) iv2=ibset(iv2,5)
      if(L_(721)) iv2=ibset(iv2,6)
      if(L_(596)) iv2=ibset(iv2,7)
      if(L_(554)) iv2=ibset(iv2,8)
      if(L_(407)) iv2=ibset(iv2,9)
      if(L_(368)) iv2=ibset(iv2,10)
      if(L_(319)) iv2=ibset(iv2,11)
      if(L_(263)) iv2=ibset(iv2,12)
      L_(198)=L_(198).or.iv2.ne.0
C FDA90_logic.fgi( 553,1342):������-�������: ������� ������ ������/����������/����� ���������
      if(L_ufube.and..not.L0_ifube) then
         R0_afube=R0_efube
      else
         R0_afube=max(R_(332)-deltat,0.0)
      endif
      L_(1208)=R0_afube.gt.0.0
      L0_ifube=L_ufube
C FDA90_logic.fgi( 531,1389):������������  �� T
      L_(1207)=.not.L_(1246) .and.L_(1208)
      L_(1205)=L_(1208).and..not.L_(1206)
C FDA90_logic.fgi( 554,1388):��������� ������
      if(L_apibe.and..not.L0_umibe) then
         R0_imibe=R0_omibe
      else
         R0_imibe=max(R_(293)-deltat,0.0)
      endif
      L_(1082)=R0_imibe.gt.0.0
      L0_umibe=L_apibe
C FDA90_logic.fgi( 161,1375):������������  �� T
      L_(1081)=.not.L_(1246) .and.L_(1082)
      L_(1079)=L_(1082).and..not.L_(1080)
C FDA90_logic.fgi( 185,1374):��������� ������
      if(L_etox.and..not.L0_atox) then
         R0_osox=R0_usox
      else
         R0_osox=max(R_(211)-deltat,0.0)
      endif
      L_(819)=R0_osox.gt.0.0
      L0_atox=L_etox
C FDA90_logic.fgi( 347, 839):������������  �� T
      L_(818)=.not.L_(1246) .and.L_(819)
      L_(816)=L_(819).and..not.L_(817)
C FDA90_logic.fgi( 371, 838):��������� ������
      if(L_ubox.and..not.L0_ibox) then
         R0_abox=R0_ebox
      else
         R0_abox=max(R_(193)-deltat,0.0)
      endif
      L_(765)=R0_abox.gt.0.0
      L0_ibox=L_ubox
C FDA90_logic.fgi( 347, 675):������������  �� T
      L_(764)=.not.L_(1246) .and.L_(765)
      L_(762)=L_(765).and..not.L_(763)
C FDA90_logic.fgi( 369, 674):��������� ������
      if(L_akex.and..not.L0_ufex) then
         R0_ifex=R0_ofex
      else
         R0_ifex=max(R_(151)-deltat,0.0)
      endif
      L_(630)=R0_ifex.gt.0.0
      L0_ufex=L_akex
C FDA90_logic.fgi( 534, 836):������������  �� T
      L_(629)=.not.L_(1246) .and.L_(630)
      L_(627)=L_(630).and..not.L_(628)
C FDA90_logic.fgi( 556, 834):��������� ������
      iv2=0
      if(L_(1207)) iv2=ibset(iv2,0)
      if(L_(1181)) iv2=ibset(iv2,1)
      if(L_(1081)) iv2=ibset(iv2,2)
      if(L_(1044)) iv2=ibset(iv2,3)
      if(L_(982)) iv2=ibset(iv2,4)
      if(L_(943)) iv2=ibset(iv2,5)
      if(L_(818)) iv2=ibset(iv2,6)
      if(L_(764)) iv2=ibset(iv2,7)
      if(L_(629)) iv2=ibset(iv2,8)
      if(L_(569)) iv2=ibset(iv2,9)
      if(L_(334)) iv2=ibset(iv2,10)
      L_(197)=L_(197).or.iv2.ne.0
C FDA90_logic.fgi( 554,1388):������-�������: ������� ������ ������/����������/����� ���������
      L_(455)=.not.L_(1246) .and.L_epibe
      L_(453)=L_epibe.and..not.L_(454)
C FDA90_logic.fgi( 183,1348):��������� ������
      L_(258)=.not.L_(1246) .and.L_efov
      L_(256)=L_efov.and..not.L_(257)
C FDA90_logic.fgi( 359,1460):��������� ������
      iv2=0
      if(L_(1211)) iv2=ibset(iv2,0)
      if(L_(979)) iv2=ibset(iv2,1)
      if(L_(808)) iv2=ibset(iv2,2)
      if(L_(477)) iv2=ibset(iv2,3)
      if(L_(455)) iv2=ibset(iv2,4)
      if(L_(258)) iv2=ibset(iv2,5)
      L_(196)=L_(196).or.iv2.ne.0
C FDA90_logic.fgi( 554,1360):������-�������: ������� ������ ������/����������/����� ���������
      Call TELEGKA_TRANSP_HANDLER(deltat,L_otil,L_(195),L_akube
     &,L_(196),R_ufol,
     & R_akol,R_ubol,R_ibol,L_ifol,R_adol,R_obol,
     & L_ofol,L_ikil,L_okil,L_(197),L_ixil,R_abol,R_ebol,
     & R_efol,R_ukube,R_afol,L_axil,L_uvil,R_apil,
     & REAL(R_umil,4),R_osil,REAL(R_atil,4),L_esil,
     & R_asil,REAL(R_isil,4),L_ipil,R_epil,
     & REAL(R_opil,4),L_uxil,L_(200),L_(201),L_usil,
     & L_evil,L_edol,L_(199),L_(198),L_idol,L_udol,L_ovil
     &)
C FDA90_vlv.fgi( 140,  56):���������� ������� ������������,20FDA91AE001KE01
C sav1=L_epibe
C sav2=L_(1083)
      if(L_alobe) then
          if (L_(1110)) then
              I_ilobe = 14
              L_epibe = .true.
              L_(1083) = .false.
          endif
          if (L_akube) then
              L_(1083) = .true.
              L_epibe = .false.
          endif
          if (I_ilobe.ne.14) then
              L_epibe = .false.
              L_(1083) = .false.
          endif
      else
          L_(1083) = .false.
      endif
C FDA90_logic.fgi( 135,1348):recalc:��� ������� ���������,EC001
C if(sav1.ne.L_epibe .and. try1810.gt.0) goto 1810
C if(sav2.ne.L_(1083) .and. try1810.gt.0) goto 1810
C sav1=L_ekube
C sav2=L_(1212)
      if(L_itube) then
          if (L_(1235)) then
              I_otube = 12
              L_ekube = .true.
              L_(1212) = .false.
          endif
          if (L_akube) then
              L_(1212) = .true.
              L_ekube = .false.
          endif
          if (I_otube.ne.12) then
              L_ekube = .false.
              L_(1212) = .false.
          endif
      else
          L_(1212) = .false.
      endif
C FDA90_logic.fgi( 505,1362):recalc:��� ������� ���������,EC003
C if(sav1.ne.L_ekube .and. try1867.gt.0) goto 1867
C if(sav2.ne.L_(1212) .and. try1867.gt.0) goto 1867
C sav1=L_itabe
C sav2=L_(976)
      if(L_akebe) then
          if (L_(984)) then
              I_ikebe = 7
              L_itabe = .true.
              L_(976) = .false.
          endif
          if (L_akube) then
              L_(976) = .true.
              L_itabe = .false.
          endif
          if (I_ikebe.ne.7) then
              L_itabe = .false.
              L_(976) = .false.
          endif
      else
          L_(976) = .false.
      endif
C FDA90_logic.fgi( 135, 898):recalc:��� ������� ���������,EC004
C if(sav1.ne.L_itabe .and. try586.gt.0) goto 586
C if(sav2.ne.L_(976) .and. try586.gt.0) goto 586
C sav1=L_oluv
C sav2=L_(584)
      if(L_otex) then
          if (L_(590)) then
              I_avex = 22
              L_oluv = .true.
              L_(584) = .false.
          endif
          if (L_akube) then
              L_(584) = .true.
              L_oluv = .false.
          endif
          if (I_avex.ne.22) then
              L_oluv = .false.
              L_(584) = .false.
          endif
      else
          L_(584) = .false.
      endif
C FDA90_logic.fgi( 508, 682):recalc:��� ������� ���������,EC006
C if(sav1.ne.L_oluv .and. try1229.gt.0) goto 1229
C if(sav2.ne.L_(584) .and. try1229.gt.0) goto 1229
C sav1=L_efov
C sav2=L_(390)
      if(L_alov) then
          if (L_(389)) then
              I_ilov = 7
              L_efov = .true.
              L_(390) = .false.
          endif
          if (L_akube) then
              L_(390) = .true.
              L_efov = .false.
          endif
          if (I_ilov.ne.7) then
              L_efov = .false.
              L_(390) = .false.
          endif
      else
          L_(390) = .false.
      endif
C FDA90_logic.fgi( 310,1460):recalc:��� ������� ���������,EC002
C if(sav1.ne.L_efov .and. try1440.gt.0) goto 1440
C if(sav2.ne.L_(390) .and. try1440.gt.0) goto 1440
C sav1=L_irox
C sav2=L_(809)
      if(L_ulux) then
          if (L_(814)) then
              I_emux = 12
              L_irox = .true.
              L_(809) = .false.
          endif
          if (L_akube) then
              L_(809) = .true.
              L_irox = .false.
          endif
          if (I_emux.ne.12) then
              L_irox = .false.
              L_(809) = .false.
          endif
      else
          L_(809) = .false.
      endif
C FDA90_logic.fgi( 321, 808):recalc:��� ������� ���������,EC005
C if(sav1.ne.L_irox .and. try790.gt.0) goto 790
C if(sav2.ne.L_(809) .and. try790.gt.0) goto 790
      if(L_itube) then
          if (L_(1164)) then
              I_otube = 23
              L_erobe = .true.
              L_(1165) = .false.
          endif
          if (L_(1166)) then
              L_(1165) = .true.
              L_erobe = .false.
          endif
          if (I_otube.ne.23) then
              L_erobe = .false.
              L_(1165) = .false.
          endif
      else
          L_(1165) = .false.
      endif
C FDA90_logic.fgi( 505,1197):��� ������� ���������,EC003
C sav1=L_(1163)
      if(L_erobe.and..not.L0_arobe) then
         R0_opobe=R0_upobe
      else
         R0_opobe=max(R_(318)-deltat,0.0)
      endif
      L_(1163)=R0_opobe.gt.0.0
      L0_arobe=L_erobe
C FDA90_logic.fgi( 531,1197):recalc:������������  �� T
C if(sav1.ne.L_(1163) .and. try527.gt.0) goto 527
      if(L_itube) then
          if (L_(1165)) then
              I_otube = 24
              L_epobe = .true.
              L_(1156) = .false.
          endif
          if (L_apobe) then
              L_(1156) = .true.
              L_epobe = .false.
          endif
          if (I_otube.ne.24) then
              L_epobe = .false.
              L_(1156) = .false.
          endif
      else
          L_(1156) = .false.
      endif
C FDA90_logic.fgi( 505,1180):��� ������� ���������,EC003
C sav1=L_(1159)
C sav2=L_(1158)
C sav3=L_(1157)
C sav4=R_(316)
C FDA90_logic.fgi( 555,1178):recalc:��������� ������
C if(sav1.ne.L_(1159) .and. try584.gt.0) goto 584
C if(sav2.ne.L_(1158) .and. try584.gt.0) goto 584
C if(sav3.ne.L_(1157) .and. try584.gt.0) goto 584
C if(sav4.ne.R_(316) .and. try584.gt.0) goto 584
      iv2=0
      if(L_(710)) iv2=ibset(iv2,0)
      if(L_(679)) iv2=ibset(iv2,1)
      if(L_(650)) iv2=ibset(iv2,2)
      if(L_(616)) iv2=ibset(iv2,3)
      L_(226)=L_(226).or.iv2.ne.0
C FDA90_logic.fgi( 367, 520):������-�������: ������� ������ ������/����������/����� ���������
      Call PEREGRUZ_USTR_HANDLER(deltat,L_odem,L_(223),L_alix
     &,L_(224),R_umem,
     & R_apem,R_ukem,R_ikem,L_imem,R_alem,R_okem,
     & L_omem,L_afem,L_(225),L_ufem,R_akem,R_ekem,
     & R_emem,R_atix,R_amem,L_ofem,L_ifem,R_itam,
     & REAL(60.0,4),R_obem,REAL(R_adem,4),R_ixam,
     & REAL(R_uxam,4),L_ebem,R_abem,REAL(R_ibem,4),
     & L_oxam,L_idem,L_ubem,L_udem,L_elem,L_(227),L_(226)
     &,
     & L_ilem,L_ulem,L_efem)
C FDA90_vlv.fgi( 445, 171):���������� ������. ����.,20FDA91AE009
C sav1=L_emex
C sav2=L_(652)
      if(L_otex) then
          if (L_(655)) then
              I_avex = 4
              L_emex = .true.
              L_(652) = .false.
          endif
          if (L_alix) then
              L_(652) = .true.
              L_emex = .false.
          endif
          if (I_avex.ne.4) then
              L_emex = .false.
              L_(652) = .false.
          endif
      else
          L_(652) = .false.
      endif
C FDA90_logic.fgi( 508, 977):recalc:��� ������� ���������,EC006
C if(sav1.ne.L_emex .and. try900.gt.0) goto 900
C if(sav2.ne.L_(652) .and. try900.gt.0) goto 900
C sav1=L_ukix
C sav2=L_(741)
      if(L_ulux) then
          if (L_(723)) then
              I_emux = 26
              L_ukix = .true.
              L_(741) = .false.
          endif
          if (L_alix) then
              L_(741) = .true.
              L_ukix = .false.
          endif
          if (I_emux.ne.26) then
              L_ukix = .false.
              L_(741) = .false.
          endif
      else
          L_(741) = .false.
      endif
C FDA90_logic.fgi( 321, 580):recalc:��� ������� ���������,EC005
C if(sav1.ne.L_ukix .and. try962.gt.0) goto 962
C if(sav2.ne.L_(741) .and. try962.gt.0) goto 962
      if(L_otex) then
          if (L_(536)) then
              I_avex = 38
              L_usov = .true.
              L_(506) = .false.
          endif
          if (L_itov) then
              L_(506) = .true.
              L_usov = .false.
          endif
          if (I_avex.ne.38) then
              L_usov = .false.
              L_(506) = .false.
          endif
      else
          L_(506) = .false.
      endif
C FDA90_logic.fgi( 508, 394):��� ������� ���������,EC006
C sav1=L_(411)
C sav2=L_(410)
C sav3=L_(409)
C sav4=R_(81)
C FDA90_logic.fgi( 557, 392):recalc:��������� ������
C if(sav1.ne.L_(411) .and. try1154.gt.0) goto 1154
C if(sav2.ne.L_(410) .and. try1154.gt.0) goto 1154
C if(sav3.ne.L_(409) .and. try1154.gt.0) goto 1154
C if(sav4.ne.R_(81) .and. try1154.gt.0) goto 1154
      if(L_otex) then
          if (L_(506)) then
              I_avex = 39
              L_iruv = .true.
              L_(507) = .false.
          endif
          if (L_(508)) then
              L_(507) = .true.
              L_iruv = .false.
          endif
          if (I_avex.ne.39) then
              L_iruv = .false.
              L_(507) = .false.
          endif
      else
          L_(507) = .false.
      endif
C FDA90_logic.fgi( 508, 375):��� ������� ���������,EC006
C sav1=L_(505)
C sav2=L_(504)
C sav3=L_(503)
C sav4=R_(111)
C FDA90_logic.fgi( 554, 374):recalc:��������� ������
C if(sav1.ne.L_(505) .and. try1171.gt.0) goto 1171
C if(sav2.ne.L_(504) .and. try1171.gt.0) goto 1171
C if(sav3.ne.L_(503) .and. try1171.gt.0) goto 1171
C if(sav4.ne.R_(111) .and. try1171.gt.0) goto 1171
      if(L_alobe) then
          if (L_(1049)) then
              I_ilobe = 21
              L_axebe = .true.
              L_(1041) = .false.
          endif
          if (L_uvebe) then
              L_(1041) = .true.
              L_axebe = .false.
          endif
          if (I_ilobe.ne.21) then
              L_axebe = .false.
              L_(1041) = .false.
          endif
      else
          L_(1041) = .false.
      endif
C FDA90_logic.fgi( 135,1242):��� ������� ���������,EC001
C sav1=L_(1044)
C sav2=L_(1043)
C sav3=L_(1042)
C sav4=R_(280)
C FDA90_logic.fgi( 184,1240):recalc:��������� ������
C if(sav1.ne.L_(1044) .and. try1401.gt.0) goto 1401
C if(sav2.ne.L_(1043) .and. try1401.gt.0) goto 1401
C if(sav3.ne.L_(1042) .and. try1401.gt.0) goto 1401
C if(sav4.ne.R_(280) .and. try1401.gt.0) goto 1401
      if(L_alobe) then
          if (L_(1041)) then
              I_ilobe = 22
              L_ovebe = .true.
              L_(1039) = .false.
          endif
          if (L_(1040)) then
              L_(1039) = .true.
              L_ovebe = .false.
          endif
          if (I_ilobe.ne.22) then
              L_ovebe = .false.
              L_(1039) = .false.
          endif
      else
          L_(1039) = .false.
      endif
C FDA90_logic.fgi( 135,1228):��� ������� ���������,EC001
C sav1=L_(1038)
      if(L_ovebe.and..not.L0_ivebe) then
         R0_avebe=R0_evebe
      else
         R0_avebe=max(R_(279)-deltat,0.0)
      endif
      L_(1038)=R0_avebe.gt.0.0
      L0_ivebe=L_ovebe
C FDA90_logic.fgi( 161,1228):recalc:������������  �� T
C if(sav1.ne.L_(1038) .and. try1380.gt.0) goto 1380
      if(L_alobe) then
          if (L_(1039)) then
              I_ilobe = 23
              L_otebe = .true.
              L_(1034) = .false.
          endif
          if (L_itebe) then
              L_(1034) = .true.
              L_otebe = .false.
          endif
          if (I_ilobe.ne.23) then
              L_otebe = .false.
              L_(1034) = .false.
          endif
      else
          L_(1034) = .false.
      endif
C FDA90_logic.fgi( 135,1214):��� ������� ���������,EC001
      if(L_alobe) then
          if (L_(1034)) then
              I_ilobe = 24
              L_epov = .true.
              L_(1027) = .false.
          endif
          if (L_amov) then
              L_(1027) = .true.
              L_epov = .false.
          endif
          if (I_ilobe.ne.24) then
              L_epov = .false.
              L_(1027) = .false.
          endif
      else
          L_(1027) = .false.
      endif
C FDA90_logic.fgi( 135,1199):��� ������� ���������,EC001
      if(L_alobe) then
          if (L_(1027)) then
              I_ilobe = 25
              L_osebe = .true.
              L_(1028) = .false.
          endif
          if (L_(1029)) then
              L_(1028) = .true.
              L_osebe = .false.
          endif
          if (I_ilobe.ne.25) then
              L_osebe = .false.
              L_(1028) = .false.
          endif
      else
          L_(1028) = .false.
      endif
C FDA90_logic.fgi( 135,1180):��� ������� ���������,EC001
C sav1=L_(1026)
      if(L_osebe.and..not.L0_isebe) then
         R0_asebe=R0_esebe
      else
         R0_asebe=max(R_(275)-deltat,0.0)
      endif
      L_(1026)=R0_asebe.gt.0.0
      L0_isebe=L_osebe
C FDA90_logic.fgi( 161,1180):recalc:������������  �� T
C if(sav1.ne.L_(1026) .and. try1341.gt.0) goto 1341
      if(L_alobe) then
          if (L_(1028)) then
              I_ilobe = 26
              L_upebe = .true.
              L_(1021) = .false.
          endif
          if (L_opebe) then
              L_(1021) = .true.
              L_upebe = .false.
          endif
          if (I_ilobe.ne.26) then
              L_upebe = .false.
              L_(1021) = .false.
          endif
      else
          L_(1021) = .false.
      endif
C FDA90_logic.fgi( 135,1157):��� ������� ���������,EC001
C sav1=L_(452)
C sav2=L_(451)
C sav3=L_(450)
C sav4=R_(94)
C FDA90_logic.fgi( 186,1154):recalc:��������� ������
C if(sav1.ne.L_(452) .and. try1399.gt.0) goto 1399
C if(sav2.ne.L_(451) .and. try1399.gt.0) goto 1399
C if(sav3.ne.L_(450) .and. try1399.gt.0) goto 1399
C if(sav4.ne.R_(94) .and. try1399.gt.0) goto 1399
      iv2=0
      if(L_(1240)) iv2=ibset(iv2,0)
      if(L_(1094)) iv2=ibset(iv2,1)
      if(L_(883)) iv2=ibset(iv2,2)
      if(L_(826)) iv2=ibset(iv2,3)
      if(L_(512)) iv2=ibset(iv2,4)
      if(L_(273)) iv2=ibset(iv2,5)
      L_(205)=L_(205).or.iv2.ne.0
C FDA90_logic.fgi( 558,1422):������-�������: ������� ������ ������/����������/����� ���������
      Call TELEGKA_TRANSP_HANDLER(deltat,L_otol,L_(202),L_erux
     &,L_(203),R_uful,
     & R_akul,R_ubul,R_ibul,L_iful,R_adul,R_obul,
     & L_oful,L_ikol,L_okol,L_(204),L_ixol,R_abul,R_ebul,
     & R_eful,R_atube,R_aful,L_axol,L_uvol,R_apol,
     & REAL(R_umol,4),R_osol,REAL(R_atol,4),L_esol,
     & R_asol,REAL(R_isol,4),L_ipol,R_epol,
     & REAL(R_opol,4),L_uxol,L_(207),L_(208),L_usol,
     & L_evol,L_edul,L_(206),L_(205),L_idul,L_udul,L_ovol
     &)
C FDA90_vlv.fgi( 176,  56):���������� ������� ������������,20FDA91AE003KE01
C sav1=L_irux
C sav2=L_(892)
      if(L_akebe) then
          if (L_(916)) then
              I_ikebe = 20
              L_irux = .true.
              L_(892) = .false.
          endif
          if (L_erux) then
              L_(892) = .true.
              L_irux = .false.
          endif
          if (I_ikebe.ne.20) then
              L_irux = .false.
              L_(892) = .false.
          endif
      else
          L_(892) = .false.
      endif
C FDA90_logic.fgi( 135, 671):recalc:��� ������� ���������,EC004
C if(sav1.ne.L_irux .and. try671.gt.0) goto 671
C if(sav2.ne.L_(892) .and. try671.gt.0) goto 671
C sav1=L_efuv
C sav2=L_(1103)
      if(L_alobe) then
          if (L_(1129)) then
              I_ilobe = 5
              L_efuv = .true.
              L_(1103) = .false.
          endif
          if (L_erux) then
              L_(1103) = .true.
              L_efuv = .false.
          endif
          if (I_ilobe.ne.5) then
              L_efuv = .false.
              L_(1103) = .false.
          endif
      else
          L_(1103) = .false.
      endif
C FDA90_logic.fgi( 135,1505):recalc:��� ������� ���������,EC001
C if(sav1.ne.L_efuv .and. try1685.gt.0) goto 1685
C if(sav2.ne.L_(1103) .and. try1685.gt.0) goto 1685
C sav1=L_utov
C sav2=L_(426)
      if(L_itube) then
          if (L_(431)) then
              I_otube = 5
              L_utov = .true.
              L_(426) = .false.
          endif
          if (L_erux) then
              L_(426) = .true.
              L_utov = .false.
          endif
          if (I_otube.ne.5) then
              L_utov = .false.
              L_(426) = .false.
          endif
      else
          L_(426) = .false.
      endif
C FDA90_logic.fgi( 505,1504):recalc:��� ������� ���������,EC003
C if(sav1.ne.L_utov .and. try1665.gt.0) goto 1665
C if(sav2.ne.L_(426) .and. try1665.gt.0) goto 1665
C sav1=L_osuv
C sav2=L_(518)
      if(L_otex) then
          if (L_(542)) then
              I_avex = 32
              L_osuv = .true.
              L_(518) = .false.
          endif
          if (L_erux) then
              L_(518) = .true.
              L_osuv = .false.
          endif
          if (I_avex.ne.32) then
              L_osuv = .false.
              L_(518) = .false.
          endif
      else
          L_(518) = .false.
      endif
C FDA90_logic.fgi( 508, 506):recalc:��� ������� ���������,EC006
C if(sav1.ne.L_osuv .and. try1265.gt.0) goto 1265
C if(sav2.ne.L_(518) .and. try1265.gt.0) goto 1265
C sav1=L_isev
C sav2=L_(282)
      if(L_alov) then
          if (L_(307)) then
              I_ilov = 21
              L_isev = .true.
              L_(282) = .false.
          endif
          if (L_erux) then
              L_(282) = .true.
              L_isev = .false.
          endif
          if (I_ilov.ne.21) then
              L_isev = .false.
              L_(282) = .false.
          endif
      else
          L_(282) = .false.
      endif
C FDA90_logic.fgi( 310,1245):recalc:��� ������� ���������,EC002
C if(sav1.ne.L_isev .and. try1484.gt.0) goto 1484
C if(sav2.ne.L_(282) .and. try1484.gt.0) goto 1484
C sav1=L_ikux
C sav2=L_(862)
      if(L_ulux) then
          if (L_(861)) then
              I_emux = 5
              L_ikux = .true.
              L_(862) = .false.
          endif
          if (L_erux) then
              L_(862) = .true.
              L_ikux = .false.
          endif
          if (I_emux.ne.5) then
              L_ikux = .false.
              L_(862) = .false.
          endif
      else
          L_(862) = .false.
      endif
C FDA90_logic.fgi( 321, 950):recalc:��� ������� ���������,EC005
C if(sav1.ne.L_ikux .and. try760.gt.0) goto 760
C if(sav2.ne.L_(862) .and. try760.gt.0) goto 760
      iv2=0
      if(L_(1240)) iv2=ibset(iv2,0)
      if(L_(1196)) iv2=ibset(iv2,1)
      if(L_(1114)) iv2=ibset(iv2,2)
      if(L_(1064)) iv2=ibset(iv2,3)
      if(L_(937)) iv2=ibset(iv2,4)
      if(L_(876)) iv2=ibset(iv2,5)
      if(L_(826)) iv2=ibset(iv2,6)
      if(L_(789)) iv2=ibset(iv2,7)
      if(L_(563)) iv2=ibset(iv2,8)
      if(L_(505)) iv2=ibset(iv2,9)
      if(L_(328)) iv2=ibset(iv2,10)
      if(L_(263)) iv2=ibset(iv2,11)
      L_(233)=L_(233).or.iv2.ne.0
C FDA90_logic.fgi( 558,1422):������-�������: ������� ������ ������/����������/����� ���������
      Call PEREGRUZ_USTR_HANDLER(deltat,L_ovem,L_(230),L_isube
     &,L_(231),R_ufim,
     & R_akim,R_ubim,R_ibim,L_ifim,R_adim,R_obim,
     & L_ofim,L_axem,L_(232),L_uxem,R_abim,R_ebim,
     & R_efim,R_erube,R_afim,L_oxem,L_ixem,R_ipem,
     & REAL(60.0,4),R_otem,REAL(R_avem,4),R_isem,
     & REAL(R_usem,4),L_etem,R_atem,REAL(R_item,4),
     & L_osem,L_ivem,L_utem,L_uvem,L_edim,L_(234),L_(233)
     &,
     & L_idim,L_udim,L_exem)
C FDA90_vlv.fgi( 403, 171):���������� ������. ����.,20FDA91AE006
C sav1=L_ulax
C sav2=L_(570)
      if(L_otex) then
          if (L_(571)) then
              I_avex = 26
              L_ulax = .true.
              L_(570) = .false.
          endif
          if (L_isube) then
              L_(570) = .true.
              L_ulax = .false.
          endif
          if (I_avex.ne.26) then
              L_ulax = .false.
              L_(570) = .false.
          endif
      else
          L_(570) = .false.
      endif
C FDA90_logic.fgi( 508, 602):recalc:��� ������� ���������,EC006
C if(sav1.ne.L_ulax .and. try1244.gt.0) goto 1244
C if(sav2.ne.L_(570) .and. try1244.gt.0) goto 1244
C sav1=L_okabe
C sav2=L_(944)
      if(L_akebe) then
          if (L_(945)) then
              I_ikebe = 14
              L_okabe = .true.
              L_(944) = .false.
          endif
          if (L_isube) then
              L_(944) = .true.
              L_okabe = .false.
          endif
          if (I_ikebe.ne.14) then
              L_okabe = .false.
              L_(944) = .false.
          endif
      else
          L_(944) = .false.
      endif
C FDA90_logic.fgi( 135, 767):recalc:��� ������� ���������,EC004
C if(sav1.ne.L_okabe .and. try650.gt.0) goto 650
C if(sav2.ne.L_(944) .and. try650.gt.0) goto 650
C sav1=L_uvox
C sav2=L_(833)
      if(L_ulux) then
          if (L_(844)) then
              I_emux = 8
              L_uvox = .true.
              L_(833) = .false.
          endif
          if (L_isube) then
              L_(833) = .true.
              L_uvox = .false.
          endif
          if (I_emux.ne.8) then
              L_uvox = .false.
              L_(833) = .false.
          endif
      else
          L_(833) = .false.
      endif
C FDA90_logic.fgi( 321, 890):recalc:��� ������� ���������,EC005
C if(sav1.ne.L_uvox .and. try773.gt.0) goto 773
C if(sav2.ne.L_(833) .and. try773.gt.0) goto 773
C sav1=L_oliv
C sav2=L_(335)
      if(L_alov) then
          if (L_(336)) then
              I_ilov = 15
              L_oliv = .true.
              L_(335) = .false.
          endif
          if (L_isube) then
              L_(335) = .true.
              L_oliv = .false.
          endif
          if (I_ilov.ne.15) then
              L_oliv = .false.
              L_(335) = .false.
          endif
      else
          L_(335) = .false.
      endif
C FDA90_logic.fgi( 310,1341):recalc:��� ������� ���������,EC002
C if(sav1.ne.L_oliv .and. try1463.gt.0) goto 1463
C if(sav2.ne.L_(335) .and. try1463.gt.0) goto 1463
C sav1=L_adobe
C sav2=L_(1124)
      if(L_alobe) then
          if (L_(1123)) then
              I_ilobe = 8
              L_adobe = .true.
              L_(1124) = .false.
          endif
          if (L_isube) then
              L_(1124) = .true.
              L_adobe = .false.
          endif
          if (I_ilobe.ne.8) then
              L_adobe = .false.
              L_(1124) = .false.
          endif
      else
          L_(1124) = .false.
      endif
C FDA90_logic.fgi( 135,1447):recalc:��� ������� ���������,EC001
C if(sav1.ne.L_adobe .and. try1734.gt.0) goto 1734
C if(sav2.ne.L_(1124) .and. try1734.gt.0) goto 1734
C sav1=L_osube
C sav2=L_(1250)
      if(L_itube) then
          if (L_(1249)) then
              I_otube = 8
              L_osube = .true.
              L_(1250) = .false.
          endif
          if (L_isube) then
              L_(1250) = .true.
              L_osube = .false.
          endif
          if (I_otube.ne.8) then
              L_osube = .false.
              L_(1250) = .false.
          endif
      else
          L_(1250) = .false.
      endif
C FDA90_logic.fgi( 505,1444):recalc:��� ������� ���������,EC003
C if(sav1.ne.L_osube .and. try1728.gt.0) goto 1728
C if(sav2.ne.L_(1250) .and. try1728.gt.0) goto 1728
      L_(111)=R_erube.gt.R0_emid
C FDA90_lamp.fgi( 180, 349):���������� >
      L_(110)=R_erube.lt.R0_amid
C FDA90_lamp.fgi( 180, 342):���������� <
      L_(109)=R_erube.gt.R0_ulid
C FDA90_lamp.fgi( 180, 334):���������� >
      L_(121) = L_(110).AND.L_(109)
C FDA90_lamp.fgi( 185, 341):�
      !{
      Call DAT_DISCR_HANDLER(deltat,I_irid,L_asid,R_urid,
     & REAL(R_esid,4),L_(121),L_erid,I_orid)
      !}
C FDA90_lamp.fgi( 198, 340):���������� ������� ���������,20FDA91AE006QH04
      L_(108)=R_erube.lt.R0_olid
C FDA90_lamp.fgi( 180, 327):���������� <
      L_(107)=R_erube.gt.R0_ilid
C FDA90_lamp.fgi( 180, 319):���������� >
      L_(122) = L_(108).AND.L_(107)
C FDA90_lamp.fgi( 185, 326):�
      !{
      Call DAT_DISCR_HANDLER(deltat,I_atid,L_otid,R_itid,
     & REAL(R_utid,4),L_(122),L_usid,I_etid)
      !}
C FDA90_lamp.fgi( 198, 324):���������� ������� ���������,20FDA91AE006QH02
      L_(106)=R_erube.lt.R0_elid
C FDA90_lamp.fgi( 180, 310):���������� <
      L_(105)=R_erube.gt.R0_alid
C FDA90_lamp.fgi( 180, 302):���������� >
      L_(123) = L_(106).AND.L_(105)
C FDA90_lamp.fgi( 185, 309):�
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ovid,L_exid,R_axid,
     & REAL(R_ixid,4),L_(123),L_ivid,I_uvid)
      !}
C FDA90_lamp.fgi( 198, 308):���������� ������� ���������,20FDA91AE006QH03
      L_(104)=R_erube.lt.R0_ukid
C FDA90_lamp.fgi( 180, 293):���������� <
      L_(103)=R_erube.gt.R0_okid
C FDA90_lamp.fgi( 180, 285):���������� >
      L_(124) = L_(104).AND.L_(103)
C FDA90_lamp.fgi( 185, 292):�
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ebod,L_ubod,R_obod,
     & REAL(R_adod,4),L_(124),L_abod,I_ibod)
      !}
C FDA90_lamp.fgi( 198, 290):���������� ������� ���������,20FDA91AE006QH06
      L_(112)=R_erube.lt.R0_imid
C FDA90_lamp.fgi( 180, 357):���������� <
      L_(125) = L_(112).AND.L_(111)
C FDA90_lamp.fgi( 185, 356):�
      !{
      Call DAT_DISCR_HANDLER(deltat,I_udod,L_ifod,R_efod,
     & REAL(R_ofod,4),L_(125),L_odod,I_afod)
      !}
C FDA90_lamp.fgi( 198, 354):���������� ������� ���������,20FDA91AE006QH01
      L_(48)=R_atube.gt.R0_eto
C FDA90_lamp.fgi( 127, 162):���������� >
      L0_ato=(L_(48).or.L0_ato).and..not.(L_(50))
      L_(46)=.not.L0_ato
C FDA90_lamp.fgi( 140, 160):RS �������
      L_(53)=R_atube.lt.R0_ixo
C FDA90_lamp.fgi( 127, 180):���������� <
      L_(47) = L0_ato.AND.L_(51).AND.(.NOT.L_(49)).AND.(.NOT.L_
     &(53))
C FDA90_lamp.fgi( 152, 159):�
      !{
      Call DAT_DISCR_HANDLER(deltat,I_oto,L_evo,R_avo,
     & REAL(R_ivo,4),L_(47),L_ito,I_uto)
      !}
C FDA90_lamp.fgi( 164, 158):���������� ������� ���������,20FDA91AE003QH04
      L0_exo=(L_(53).or.L0_exo).and..not.(L_(51))
      L_(52)=.not.L0_exo
C FDA90_lamp.fgi( 140, 178):RS �������
      L_(54) = L0_exo.AND.L_(50).AND.(.NOT.L_(49)).AND.(.NOT.L_
     &(48))
C FDA90_lamp.fgi( 152, 177):�
      !{
      Call DAT_DISCR_HANDLER(deltat,I_uxo,L_ibu,R_ebu,
     & REAL(R_obu,4),L_(54),L_oxo,I_abu)
      !}
C FDA90_lamp.fgi( 164, 176):���������� ������� ���������,20FDA91AE003QH03
      L_(60)=R_atube.lt.R0_evu
C FDA90_lamp.fgi(  62, 124):���������� <
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ovu,L_exu,R_axu,
     & REAL(R_ixu,4),L_(60),L_ivu,I_uvu)
      !}
C FDA90_lamp.fgi(  80, 122):���������� ������� ���������,20FDA91AE003QH01
      L_(59)=R_atube.gt.R0_avu
C FDA90_lamp.fgi(  62, 111):���������� >
      !{
      Call DAT_DISCR_HANDLER(deltat,I_osu,L_etu,R_atu,
     & REAL(R_itu,4),L_(59),L_isu,I_usu)
      !}
C FDA90_lamp.fgi(  80, 110):���������� ������� ���������,20FDA91AE003QH02
      if(L_alobe) then
          if (L_(1021)) then
              I_ilobe = 27
              L_orebe = .true.
              L_ifibe = .false.
          endif
          if (L_(1022)) then
              L_ifibe = .true.
              L_orebe = .false.
          endif
          if (I_ilobe.ne.27) then
              L_orebe = .false.
              L_ifibe = .false.
          endif
      else
          L_ifibe = .false.
      endif
C FDA90_logic.fgi( 135,1135):��� ������� ���������,EC001
C sav1=L_(1020)
C sav2=L_(1019)
C sav3=L_(1018)
C sav4=R_(273)
C FDA90_logic.fgi( 184,1132):recalc:��������� ������
C if(sav1.ne.L_(1020) .and. try1389.gt.0) goto 1389
C if(sav2.ne.L_(1019) .and. try1389.gt.0) goto 1389
C if(sav3.ne.L_(1018) .and. try1389.gt.0) goto 1389
C if(sav4.ne.R_(273) .and. try1389.gt.0) goto 1389
      if(L_ifibe) then
         I_ilobe=0
         L_alobe=.false.
      endif
C FDA90_logic.fgi( 135,1110):����� ������� ���������,EC001
      if(L_epov.and..not.L0_apov) then
         R0_omov=R0_umov
      else
         R0_omov=max(R_(77)-deltat,0.0)
      endif
      L_(403)=R0_omov.gt.0.0
      L0_apov=L_epov
C FDA90_logic.fgi( 161,1199):������������  �� T
      L_imov=(L_(403).or.L_imov).and..not.(L_amov)
      L_(402)=.not.L_imov
C FDA90_logic.fgi( 174,1197):RS �������
      L_emov=L_imov
C FDA90_logic.fgi( 192,1199):������,20FDA91_SP
      if(L_otebe.and..not.L0_etebe) then
         R0_usebe=R0_atebe
      else
         R0_usebe=max(R_(276)-deltat,0.0)
      endif
      L_(1033)=R0_usebe.gt.0.0
      L0_etebe=L_otebe
C FDA90_logic.fgi( 161,1214):������������  �� T
      L_(1032)=.not.L_(1246) .and.L_(1033)
      L_(1030)=L_(1033).and..not.L_(1031)
C FDA90_logic.fgi( 181,1214):��������� ������
      if(L_(507)) then
         I_avex=0
         L_otex=.false.
      endif
C FDA90_logic.fgi( 508, 350):����� ������� ���������,EC006
      L_(81)=R_atix.gt.R0_exed
C FDA90_lamp.fgi( 181, 256):���������� >
      L_(80)=R_atix.lt.R0_axed
C FDA90_lamp.fgi( 181, 249):���������� <
      L_(79)=R_atix.gt.R0_uved
C FDA90_lamp.fgi( 181, 241):���������� >
      L_(126) = L_(80).AND.L_(79)
C FDA90_lamp.fgi( 186, 248):�
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ikod,L_alod,R_ukod,
     & REAL(R_elod,4),L_(126),L_ekod,I_okod)
      !}
C FDA90_lamp.fgi( 198, 246):���������� ������� ���������,20FDA91AE009QH04
      L_(78)=R_atix.lt.R0_oved
C FDA90_lamp.fgi( 181, 234):���������� <
      L_(77)=R_atix.gt.R0_ived
C FDA90_lamp.fgi( 181, 226):���������� >
      L_(127) = L_(78).AND.L_(77)
C FDA90_lamp.fgi( 186, 233):�
      !{
      Call DAT_DISCR_HANDLER(deltat,I_amod,L_omod,R_imod,
     & REAL(R_umod,4),L_(127),L_ulod,I_emod)
      !}
C FDA90_lamp.fgi( 198, 232):���������� ������� ���������,20FDA91AE009QH02
      L_(76)=R_atix.lt.R0_eved
C FDA90_lamp.fgi( 181, 217):���������� <
      L_(75)=R_atix.gt.R0_aved
C FDA90_lamp.fgi( 181, 209):���������� >
      L_(128) = L_(76).AND.L_(75)
C FDA90_lamp.fgi( 186, 216):�
      !{
      Call DAT_DISCR_HANDLER(deltat,I_opod,L_erod,R_arod,
     & REAL(R_irod,4),L_(128),L_ipod,I_upod)
      !}
C FDA90_lamp.fgi( 198, 214):���������� ������� ���������,20FDA91AE009QH03
      L_(74)=R_atix.lt.R0_uted
C FDA90_lamp.fgi( 181, 200):���������� <
      L_(73)=R_atix.gt.R0_oted
C FDA90_lamp.fgi( 181, 192):���������� >
      L_(129) = L_(74).AND.L_(73)
C FDA90_lamp.fgi( 186, 199):�
      !{
      Call DAT_DISCR_HANDLER(deltat,I_esod,L_usod,R_osod,
     & REAL(R_atod,4),L_(129),L_asod,I_isod)
      !}
C FDA90_lamp.fgi( 198, 198):���������� ������� ���������,20FDA91AE009QH06
      L_(82)=R_atix.lt.R0_ixed
C FDA90_lamp.fgi( 181, 264):���������� <
      L_(130) = L_(82).AND.L_(81)
C FDA90_lamp.fgi( 186, 263):�
      !{
      Call DAT_DISCR_HANDLER(deltat,I_utod,L_ivod,R_evod,
     & REAL(R_ovod,4),L_(130),L_otod,I_avod)
      !}
C FDA90_lamp.fgi( 198, 262):���������� ������� ���������,20FDA91AE009QH01
      if(L_itube) then
          if (L_(1156)) then
              I_otube = 25
              L_umobe = .true.
              L_(1252) = .false.
          endif
          if (L_(1155)) then
              L_(1252) = .true.
              L_umobe = .false.
          endif
          if (I_otube.ne.25) then
              L_umobe = .false.
              L_(1252) = .false.
          endif
      else
          L_(1252) = .false.
      endif
C FDA90_logic.fgi( 505,1154):��� ������� ���������,EC003
C sav1=L_(1154)
C sav2=L_(1153)
C sav3=L_(1152)
C sav4=R_(315)
C FDA90_logic.fgi( 552,1154):recalc:��������� ������
C if(sav1.ne.L_(1154) .and. try576.gt.0) goto 576
C if(sav2.ne.L_(1153) .and. try576.gt.0) goto 576
C if(sav3.ne.L_(1152) .and. try576.gt.0) goto 576
C if(sav4.ne.R_(315) .and. try576.gt.0) goto 576
      if(L_(1252)) then
         I_otube=0
         L_itube=.false.
      endif
C FDA90_logic.fgi( 505,1136):����� ������� ���������,EC003
      L_(62)=R_ukube.lt.R0_udad
C FDA90_lamp.fgi(  62, 179):���������� <
      !{
      Call DAT_DISCR_HANDLER(deltat,I_efad,L_ufad,R_ofad,
     & REAL(R_akad,4),L_(62),L_afad,I_ifad)
      !}
C FDA90_lamp.fgi(  80, 178):���������� ������� ���������,20FDA91AE001QH01
      L_(61)=R_ukube.gt.R0_abad
C FDA90_lamp.fgi(  62, 166):���������� >
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ibad,L_adad,R_ubad,
     & REAL(R_edad,4),L_(61),L_ebad,I_obad)
      !}
C FDA90_lamp.fgi(  80, 164):���������� ������� ���������,20FDA91AE001QH08
      iv2=0
      if(L_(764)) iv2=ibset(iv2,0)
      if(L_(629)) iv2=ibset(iv2,1)
C FDA90_logic.fgi( 554,1388):������-�������: ������� ������ ������/����������/����� ���������
      if(L_esobe.and..not.L0_urobe) then
         R0_irobe=R0_orobe
      else
         R0_irobe=max(R_(319)-deltat,0.0)
      endif
      L_(1170)=R0_irobe.gt.0.0
      L0_urobe=L_esobe
C FDA90_logic.fgi( 531,1226):������������  �� T
      L_(1169)=.not.L_(1246) .and.L_(1170)
      L_(1167)=L_(1170).and..not.L_(1168)
C FDA90_logic.fgi( 551,1226):��������� ������
      if(L_efibe.and..not.L0_udibe) then
         R0_idibe=R0_odibe
      else
         R0_idibe=max(R_(285)-deltat,0.0)
      endif
      L_(1060)=R0_idibe.gt.0.0
      L0_udibe=L_efibe
C FDA90_logic.fgi( 161,1288):������������  �� T
      L_(1059)=.not.L_(1246) .and.L_(1060)
      L_(1057)=L_(1060).and..not.L_(1058)
C FDA90_logic.fgi( 181,1288):��������� ������
      if(L_emibe.and..not.L0_amibe) then
         R0_olibe=R0_ulibe
      else
         R0_olibe=max(R_(291)-deltat,0.0)
      endif
      L_(1076)=R0_olibe.gt.0.0
      L0_amibe=L_emibe
C FDA90_logic.fgi( 161,1302):������������  �� T
      L_(1075)=.not.L_(1246) .and.L_(1076)
      L_(1073)=L_(1076).and..not.L_(1074)
C FDA90_logic.fgi( 181,1302):��������� ������
      if(L_apube.and..not.L0_omube) then
         R0_emube=R0_imube
      else
         R0_emube=max(R_(337)-deltat,0.0)
      endif
      L_(1223)=R0_emube.gt.0.0
      L0_omube=L_apube
C FDA90_logic.fgi( 531,1301):������������  �� T
      L_(1222)=.not.L_(1246) .and.L_(1223)
      L_(1220)=L_(1223).and..not.L_(1221)
C FDA90_logic.fgi( 551,1302):��������� ������
      if(L_amube.and..not.L0_olube) then
         R0_elube=R0_ilube
      else
         R0_elube=max(R_(335)-deltat,0.0)
      endif
      L_(1218)=R0_elube.gt.0.0
      L0_olube=L_amube
C FDA90_logic.fgi( 531,1315):������������  �� T
      L_(1217)=.not.L_(1246) .and.L_(1218)
      L_(1215)=L_(1218).and..not.L_(1216)
C FDA90_logic.fgi( 551,1316):��������� ������
      L_(71)=R_emobe.gt.R0_eted
C FDA90_lamp.fgi( 287, 255):���������� >
      L_(70)=R_emobe.lt.R0_ated
C FDA90_lamp.fgi( 287, 248):���������� <
      L_(69)=R_emobe.gt.R0_used
C FDA90_lamp.fgi( 287, 240):���������� >
      L_(136) = L_(70).AND.L_(69)
C FDA90_lamp.fgi( 292, 247):�
      !{
      Call DAT_DISCR_HANDLER(deltat,I_irud,L_asud,R_urud,
     & REAL(R_esud,4),L_(136),L_erud,I_orud)
      !}
C FDA90_lamp.fgi( 308, 246):���������� ������� ���������,20FDA91AE014QH04
      L_(68)=R_emobe.lt.R0_osed
C FDA90_lamp.fgi( 287, 233):���������� <
      L_(67)=R_emobe.gt.R0_ised
C FDA90_lamp.fgi( 287, 225):���������� >
      L_(137) = L_(68).AND.L_(67)
C FDA90_lamp.fgi( 292, 232):�
      !{
      Call DAT_DISCR_HANDLER(deltat,I_atud,L_otud,R_itud,
     & REAL(R_utud,4),L_(137),L_usud,I_etud)
      !}
C FDA90_lamp.fgi( 308, 230):���������� ������� ���������,20FDA91AE014QH02
      L_(66)=R_emobe.lt.R0_esed
C FDA90_lamp.fgi( 287, 216):���������� <
      L_(65)=R_emobe.gt.R0_ased
C FDA90_lamp.fgi( 287, 208):���������� >
      L_(138) = L_(66).AND.L_(65)
C FDA90_lamp.fgi( 292, 215):�
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ovud,L_exud,R_axud,
     & REAL(R_ixud,4),L_(138),L_ivud,I_uvud)
      !}
C FDA90_lamp.fgi( 308, 214):���������� ������� ���������,20FDA91AE014QH03
      L_(64)=R_emobe.lt.R0_ured
C FDA90_lamp.fgi( 287, 199):���������� <
      L_(63)=R_emobe.gt.R0_ored
C FDA90_lamp.fgi( 287, 191):���������� >
      L_(139) = L_(64).AND.L_(63)
C FDA90_lamp.fgi( 292, 198):�
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ebaf,L_ubaf,R_obaf,
     & REAL(R_adaf,4),L_(139),L_abaf,I_ibaf)
      !}
C FDA90_lamp.fgi( 308, 196):���������� ������� ���������,20FDA91AE014QH06
      L_(72)=R_emobe.lt.R0_ited
C FDA90_lamp.fgi( 287, 263):���������� <
      L_(140) = L_(72).AND.L_(71)
C FDA90_lamp.fgi( 292, 262):�
      !{
      Call DAT_DISCR_HANDLER(deltat,I_udaf,L_ifaf,R_efaf,
     & REAL(R_ofaf,4),L_(140),L_odaf,I_afaf)
      !}
C FDA90_lamp.fgi( 308, 260):���������� ������� ���������,20FDA91AE014QH01
      if(L_(264)) then
         I_ilov=0
         L_alov=.false.
      endif
C FDA90_logic.fgi( 310,1078):����� ������� ���������,EC002
      if(L_(878)) then
         I_ikebe=0
         L_akebe=.false.
      endif
C FDA90_logic.fgi( 135, 515):����� ������� ���������,EC004
      if(L_omev.and..not.L0_emev) then
         R0_olev=R0_ulev
      else
         R0_olev=max(R_(28)-deltat,0.0)
      endif
      L_amev=R0_olev.gt.0.0
      L0_emev=L_omev
C FDA90_logic.fgi( 336,1156):������������  �� T
      if(L_ikiv.and..not.L0_ekiv) then
         R0_ufiv=R0_akiv
      else
         R0_ufiv=max(R_(50)-deltat,0.0)
      endif
      L_(324)=R0_ufiv.gt.0.0
      L0_ekiv=L_ikiv
C FDA90_logic.fgi( 336,1309):������������  �� T
      L_(323)=.not.L_(1246) .and.L_(324)
      L_(321)=L_(324).and..not.L_(322)
C FDA90_logic.fgi( 357,1310):��������� ������
      if(L_iriv.and..not.L0_eriv) then
         R0_upiv=R0_ariv
      else
         R0_upiv=max(R_(60)-deltat,0.0)
      endif
      L_(351)=R0_upiv.gt.0.0
      L0_eriv=L_iriv
C FDA90_logic.fgi( 336,1390):������������  �� T
      L_(350)=.not.L_(1246) .and.L_(351)
      L_(348)=L_(351).and..not.L_(349)
C FDA90_logic.fgi( 357,1390):��������� ������
      if(L_esiv.and..not.L0_asiv) then
         R0_oriv=R0_uriv
      else
         R0_oriv=max(R_(61)-deltat,0.0)
      endif
      L_(356)=R0_oriv.gt.0.0
      L0_asiv=L_esiv
C FDA90_logic.fgi( 336,1406):������������  �� T
      L_(355)=.not.L_(1246) .and.L_(356)
      L_(353)=L_(356).and..not.L_(354)
C FDA90_logic.fgi( 357,1406):��������� ������
      iv2=0
      if(L_(1032)) iv2=ibset(iv2,0)
      if(L_(355)) iv2=ibset(iv2,1)
C FDA90_logic.fgi( 181,1214):������-�������: ������� ������ ������/����������/����� ���������
      L_(149)=R_ibibe.lt.R0_ofef
C FDA90_lamp.fgi(  70, 356):���������� <
      L_(148)=R_ibibe.gt.R0_ifef
C FDA90_lamp.fgi(  70, 348):���������� >
      L_(152) = L_(149).AND.L_(148)
C FDA90_lamp.fgi(  75, 355):�
      !{
      Call DAT_DISCR_HANDLER(deltat,I_epef,L_upef,R_opef,
     & REAL(R_aref,4),L_(152),L_apef,I_ipef)
      !}
C FDA90_lamp.fgi(  88, 354):���������� ������� ���������,20FDA91AE012QH01
      L_(120)=R_ibibe.lt.R0_arid
C FDA90_lamp.fgi(  70, 341):���������� <
      L_(119)=R_ibibe.gt.R0_upid
C FDA90_lamp.fgi(  70, 333):���������� >
      L_(146) = L_(120).AND.L_(119)
C FDA90_lamp.fgi(  75, 340):�
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ixaf,L_abef,R_uxaf,
     & REAL(R_ebef,4),L_(146),L_exaf,I_oxaf)
      !}
C FDA90_lamp.fgi(  88, 338):���������� ������� ���������,20FDA91AE012QH04
      L_(118)=R_ibibe.lt.R0_opid
C FDA90_lamp.fgi(  70, 326):���������� <
      L_(117)=R_ibibe.gt.R0_ipid
C FDA90_lamp.fgi(  70, 318):���������� >
      L_(147) = L_(118).AND.L_(117)
C FDA90_lamp.fgi(  75, 325):�
      !{
      Call DAT_DISCR_HANDLER(deltat,I_adef,L_odef,R_idef,
     & REAL(R_udef,4),L_(147),L_ubef,I_edef)
      !}
C FDA90_lamp.fgi(  88, 324):���������� ������� ���������,20FDA91AE012QH02
      L_(116)=R_ibibe.lt.R0_epid
C FDA90_lamp.fgi(  70, 309):���������� <
      L_(115)=R_ibibe.gt.R0_apid
C FDA90_lamp.fgi(  70, 301):���������� >
      L_(150) = L_(116).AND.L_(115)
C FDA90_lamp.fgi(  75, 308):�
      !{
      Call DAT_DISCR_HANDLER(deltat,I_akef,L_okef,R_ikef,
     & REAL(R_ukef,4),L_(150),L_ufef,I_ekef)
      !}
C FDA90_lamp.fgi(  88, 306):���������� ������� ���������,20FDA91AE012QH03
      L_(114)=R_ibibe.lt.R0_umid
C FDA90_lamp.fgi(  70, 292):���������� <
      L_(113)=R_ibibe.gt.R0_omid
C FDA90_lamp.fgi(  70, 284):���������� >
      L_(151) = L_(114).AND.L_(113)
C FDA90_lamp.fgi(  75, 291):�
      !{
      Call DAT_DISCR_HANDLER(deltat,I_olef,L_emef,R_amef,
     & REAL(R_imef,4),L_(151),L_ilef,I_ulef)
      !}
C FDA90_lamp.fgi(  88, 290):���������� ������� ���������,20FDA91AE012QH06
      iv2=0
      if(L_(452)) iv2=ibset(iv2,0)
C FDA90_logic.fgi( 186,1154):������-�������: ������� ������ ������/����������/����� ���������
      if(L_adov.and..not.L0_ubov) then
         R0_ibov=R0_obov
      else
         R0_ibov=max(R_(71)-deltat,0.0)
      endif
      L_(383)=R0_ibov.gt.0.0
      L0_ubov=L_adov
C FDA90_logic.fgi( 336,1495):������������  �� T
      L_(382)=.not.L_(1246) .and.L_(383)
      L_(380)=L_(383).and..not.L_(381)
C FDA90_logic.fgi( 357,1496):��������� ������
      iv2=0
      if(L_(1059)) iv2=ibset(iv2,0)
      if(L_(382)) iv2=ibset(iv2,1)
C FDA90_logic.fgi( 181,1288):������-�������: ������� ������ ������/����������/����� ���������
      if(L_okax.and..not.L0_ikax) then
         R0_akax=R0_ekax
      else
         R0_akax=max(R_(127)-deltat,0.0)
      endif
      L_(559)=R0_akax.gt.0.0
      L0_ikax=L_okax
C FDA90_logic.fgi( 534, 570):������������  �� T
      L_(558)=.not.L_(1246) .and.L_(559)
      L_(556)=L_(559).and..not.L_(557)
C FDA90_logic.fgi( 554, 570):��������� ������
      if(L_orax.and..not.L0_irax) then
         R0_arax=R0_erax
      else
         R0_arax=max(R_(135)-deltat,0.0)
      endif
      L_(583)=R0_arax.gt.0.0
      L0_irax=L_orax
C FDA90_logic.fgi( 534, 653):������������  �� T
      L_(582)=.not.L_(1246) .and.L_(583)
      L_(580)=L_(583).and..not.L_(581)
C FDA90_logic.fgi( 554, 654):��������� ������
      if(L_ivax.and..not.L0_evax) then
         R0_utax=R0_avax
      else
         R0_utax=max(R_(141)-deltat,0.0)
      endif
      L_(601)=R0_utax.gt.0.0
      L0_evax=L_ivax
C FDA90_logic.fgi( 534, 735):������������  �� T
      L_(600)=.not.L_(1246) .and.L_(601)
      L_(598)=L_(601).and..not.L_(599)
C FDA90_logic.fgi( 554, 736):��������� ������
      L_(91)=R_ilox.gt.R0_edid
C FDA90_lamp.fgi(  71, 257):���������� >
      L_(90)=R_ilox.lt.R0_adid
C FDA90_lamp.fgi(  71, 250):���������� <
      L_(89)=R_ilox.gt.R0_ubid
C FDA90_lamp.fgi(  71, 242):���������� >
      L_(131) = L_(90).AND.L_(89)
C FDA90_lamp.fgi(  76, 249):�
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ixod,L_abud,R_uxod,
     & REAL(R_ebud,4),L_(131),L_exod,I_oxod)
      !}
C FDA90_lamp.fgi(  90, 248):���������� ������� ���������,20FDA91AE008QH04
      L_(88)=R_ilox.lt.R0_obid
C FDA90_lamp.fgi(  71, 235):���������� <
      L_(87)=R_ilox.gt.R0_ibid
C FDA90_lamp.fgi(  71, 227):���������� >
      L_(132) = L_(88).AND.L_(87)
C FDA90_lamp.fgi(  76, 234):�
      !{
      Call DAT_DISCR_HANDLER(deltat,I_adud,L_odud,R_idud,
     & REAL(R_udud,4),L_(132),L_ubud,I_edud)
      !}
C FDA90_lamp.fgi(  90, 232):���������� ������� ���������,20FDA91AE008QH02
      L_(86)=R_ilox.lt.R0_ebid
C FDA90_lamp.fgi(  71, 218):���������� <
      L_(85)=R_ilox.gt.R0_abid
C FDA90_lamp.fgi(  71, 210):���������� >
      L_(133) = L_(86).AND.L_(85)
C FDA90_lamp.fgi(  76, 217):�
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ofud,L_ekud,R_akud,
     & REAL(R_ikud,4),L_(133),L_ifud,I_ufud)
      !}
C FDA90_lamp.fgi(  90, 216):���������� ������� ���������,20FDA91AE008QH03
      L_(84)=R_ilox.lt.R0_uxed
C FDA90_lamp.fgi(  71, 201):���������� <
      L_(83)=R_ilox.gt.R0_oxed
C FDA90_lamp.fgi(  71, 193):���������� >
      L_(134) = L_(84).AND.L_(83)
C FDA90_lamp.fgi(  76, 200):�
      !{
      Call DAT_DISCR_HANDLER(deltat,I_elud,L_ulud,R_olud,
     & REAL(R_amud,4),L_(134),L_alud,I_ilud)
      !}
C FDA90_lamp.fgi(  90, 198):���������� ������� ���������,20FDA91AE008QH06
      L_(92)=R_ilox.lt.R0_idid
C FDA90_lamp.fgi(  71, 265):���������� <
      L_(135) = L_(92).AND.L_(91)
C FDA90_lamp.fgi(  76, 264):�
      !{
      Call DAT_DISCR_HANDLER(deltat,I_umud,L_ipud,R_epud,
     & REAL(R_opud,4),L_(135),L_omud,I_apud)
      !}
C FDA90_lamp.fgi(  90, 262):���������� ������� ���������,20FDA91AE008QH01
      if(L_ebex.and..not.L0_abex) then
         R0_oxax=R0_uxax
      else
         R0_oxax=max(R_(145)-deltat,0.0)
      endif
      L_(612)=R0_oxax.gt.0.0
      L0_abex=L_ebex
C FDA90_logic.fgi( 534, 763):������������  �� T
      L_(611)=.not.L_(1246) .and.L_(612)
      L_(609)=L_(612).and..not.L_(610)
C FDA90_logic.fgi( 554, 764):��������� ������
      L_(58)=R_imix.lt.R0_opu
C FDA90_lamp.fgi(  62, 152):���������� <
      !{
      Call DAT_DISCR_HANDLER(deltat,I_aru,L_oru,R_iru,
     & REAL(R_uru,4),L_(58),L_upu,I_eru)
      !}
C FDA90_lamp.fgi(  80, 150):���������� ������� ���������,20FDA91AE002QH01
      L_(57)=R_imix.gt.R0_ipu
C FDA90_lamp.fgi(  62, 139):���������� >
      !{
      Call DAT_DISCR_HANDLER(deltat,I_amu,L_omu,R_imu,
     & REAL(R_umu,4),L_(57),L_ulu,I_emu)
      !}
C FDA90_lamp.fgi(  80, 138):���������� ������� ���������,20FDA91AE002QH02
      L_(36)=R_imix.gt.R0_ulo
C FDA90_lamp.fgi( 127, 113):���������� >
      L0_umo=(L_(36).or.L0_umo).and..not.(L_(39))
      L_(37)=.not.L0_umo
C FDA90_lamp.fgi( 140, 111):RS �������
      L_(42)=R_imix.lt.R0_ipo
C FDA90_lamp.fgi( 127, 131):���������� <
      L_(38) = L0_umo.AND.L_(40).AND.(.NOT.L_(34)).AND.(.NOT.L_
     &(42))
C FDA90_lamp.fgi( 152, 110):�
      !{
      Call DAT_DISCR_HANDLER(deltat,I_iko,L_alo,R_uko,
     & REAL(R_elo,4),L_(38),L_eko,I_oko)
      !}
C FDA90_lamp.fgi( 164, 108):���������� ������� ���������,20FDA91AE002QH04
      L0_epo=(L_(42).or.L0_epo).and..not.(L_(40))
      L_(41)=.not.L0_epo
C FDA90_lamp.fgi( 140, 129):RS �������
      L_(43) = L0_epo.AND.L_(39).AND.(.NOT.L_(34)).AND.(.NOT.L_
     &(36))
C FDA90_lamp.fgi( 152, 128):�
      !{
      Call DAT_DISCR_HANDLER(deltat,I_upo,L_iro,R_ero,
     & REAL(R_oro,4),L_(43),L_opo,I_aro)
      !}
C FDA90_lamp.fgi( 164, 126):���������� ������� ���������,20FDA91AE002QH03
      if(L_(681)) then
         I_emux=0
         L_ulux=.false.
      endif
C FDA90_logic.fgi( 321, 366):����� ������� ���������,EC005
      L_(414)=.not.L_(1246) .and.L_etov
      L_(412)=L_etov.and..not.L_(413)
C FDA90_logic.fgi( 367, 412):��������� ������
      iv2=0
      if(L_(414)) iv2=ibset(iv2,0)
C FDA90_logic.fgi( 367, 412):������-�������: ������� ������ ������/����������/����� ���������
      if(L_opuv.and..not.L0_ipuv) then
         R0_apuv=R0_epuv
      else
         R0_apuv=max(R_(107)-deltat,0.0)
      endif
      L_(490)=R0_apuv.gt.0.0
      L0_ipuv=L_opuv
C FDA90_logic.fgi( 534, 911):������������  �� T
      L_(489)=.not.L_(1246) .and.L_(490)
      L_(487)=L_(490).and..not.L_(488)
C FDA90_logic.fgi( 554, 912):��������� ������
      if(L_ufix.and..not.L0_ifix) then
         R0_afix=R0_efix
      else
         R0_afix=max(R_(173)-deltat,0.0)
      endif
      L_(706)=R0_afix.gt.0.0
      L0_ifix=L_ufix
C FDA90_logic.fgi( 347, 504):������������  �� T
      L_(705)=.not.L_(1246) .and.L_(706)
      L_(703)=L_(706).and..not.L_(704)
C FDA90_logic.fgi( 367, 504):��������� ������
      iv2=0
      if(L_(705)) iv2=ibset(iv2,0)
      if(L_(611)) iv2=ibset(iv2,1)
C FDA90_logic.fgi( 367, 504):������-�������: ������� ������ ������/����������/����� ���������
      if(L_orix.and..not.L0_erix) then
         R0_upix=R0_arix
      else
         R0_upix=max(R_(183)-deltat,0.0)
      endif
      L_(735)=R0_upix.gt.0.0
      L0_erix=L_orix
C FDA90_logic.fgi( 347, 551):������������  �� T
      L_(734)=.not.L_(1246) .and.L_(735)
      L_(732)=L_(735).and..not.L_(733)
C FDA90_logic.fgi( 367, 552):��������� ������
      iv2=0
      if(L_(734)) iv2=ibset(iv2,0)
      if(L_(600)) iv2=ibset(iv2,1)
C FDA90_logic.fgi( 367, 552):������-�������: ������� ������ ������/����������/����� ���������
      L_(25)=R_adix.gt.R0_ido
C FDA90_lamp.fgi( 127,  64):���������� >
      L0_ifo=(L_(25).or.L0_ifo).and..not.(L_(28))
      L_(26)=.not.L0_ifo
C FDA90_lamp.fgi( 140,  62):RS �������
      L_(32)=R_adix.lt.R0_ufo
C FDA90_lamp.fgi( 127,  82):���������� <
      L_(27) = L0_ifo.AND.L_(30).AND.(.NOT.L_(23)).AND.(.NOT.L_
     &(32))
C FDA90_lamp.fgi( 152,  61):�
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ivi,L_axi,R_uvi,
     & REAL(R_exi,4),L_(27),L_evi,I_ovi)
      !}
C FDA90_lamp.fgi( 166,  60):���������� ������� ���������,20FDA91AE005QH04
      L0_ofo=(L_(32).or.L0_ofo).and..not.(L_(30))
      L_(31)=.not.L0_ofo
C FDA90_lamp.fgi( 140,  80):RS �������
      L_(29) = L0_ofo.AND.L_(28).AND.(.NOT.L_(23)).AND.(.NOT.L_
     &(25))
C FDA90_lamp.fgi( 152,  79):�
      !{
      Call DAT_DISCR_HANDLER(deltat,I_abo,L_obo,R_ibo,
     & REAL(R_ubo,4),L_(29),L_uxi,I_ebo)
      !}
C FDA90_lamp.fgi( 166,  78):���������� ������� ���������,20FDA91AE005QH03
      L_(56)=R_adix.lt.R0_olu
C FDA90_lamp.fgi(  62,  96):���������� <
      !{
      Call DAT_DISCR_HANDLER(deltat,I_idu,L_afu,R_udu,
     & REAL(R_efu,4),L_(56),L_edu,I_odu)
      !}
C FDA90_lamp.fgi(  80,  94):���������� ������� ���������,20FDA91AE005QH01
      L_(55)=R_adix.gt.R0_ilu
C FDA90_lamp.fgi(  62,  83):���������� >
      !{
      Call DAT_DISCR_HANDLER(deltat,I_aku,L_oku,R_iku,
     & REAL(R_uku,4),L_(55),L_ufu,I_eku)
      !}
C FDA90_lamp.fgi(  80,  82):���������� ������� ���������,20FDA91AE005QH02
      if(L_uvix.and..not.L0_ivix) then
         R0_avix=R0_evix
      else
         R0_avix=max(R_(189)-deltat,0.0)
      endif
      L_(753)=R0_avix.gt.0.0
      L0_ivix=L_uvix
C FDA90_logic.fgi( 347, 626):������������  �� T
      L_(752)=.not.L_(1246) .and.L_(753)
      L_(750)=L_(753).and..not.L_(751)
C FDA90_logic.fgi( 367, 626):��������� ������
      iv2=0
      if(L_(752)) iv2=ibset(iv2,0)
      if(L_(489)) iv2=ibset(iv2,1)
C FDA90_logic.fgi( 367, 626):������-�������: ������� ������ ������/����������/����� ���������
      if(L_erox.and..not.L0_arox) then
         R0_opox=R0_upox
      else
         R0_opox=max(R_(206)-deltat,0.0)
      endif
      L_(803)=R0_opox.gt.0.0
      L0_arox=L_erox
C FDA90_logic.fgi( 347, 723):������������  �� T
      L_(802)=.not.L_(1246) .and.L_(803)
      L_(800)=L_(803).and..not.L_(801)
C FDA90_logic.fgi( 367, 724):��������� ������
      if(L_ifabe.and..not.L0_efabe) then
         R0_udabe=R0_afabe
      else
         R0_udabe=max(R_(245)-deltat,0.0)
      endif
      L_(933)=R0_udabe.gt.0.0
      L0_efabe=L_ifabe
C FDA90_logic.fgi( 161, 735):������������  �� T
      L_(932)=.not.L_(1246) .and.L_(933)
      L_(930)=L_(933).and..not.L_(931)
C FDA90_logic.fgi( 181, 736):��������� ������
      iv2=0
      if(L_(1217)) iv2=ibset(iv2,0)
      if(L_(1075)) iv2=ibset(iv2,1)
      if(L_(932)) iv2=ibset(iv2,2)
      if(L_(802)) iv2=ibset(iv2,3)
      if(L_(558)) iv2=ibset(iv2,4)
      if(L_(323)) iv2=ibset(iv2,5)
C FDA90_logic.fgi( 551,1316):������-�������: ������� ������ ������/����������/����� ���������
      if(L_ipabe.and..not.L0_epabe) then
         R0_umabe=R0_apabe
      else
         R0_umabe=max(R_(253)-deltat,0.0)
      endif
      L_(957)=R0_umabe.gt.0.0
      L0_epabe=L_ipabe
C FDA90_logic.fgi( 161, 822):������������  �� T
      L_(956)=.not.L_(1246) .and.L_(957)
      L_(954)=L_(957).and..not.L_(955)
C FDA90_logic.fgi( 181, 822):��������� ������
      iv2=0
      if(L_(1207)) iv2=ibset(iv2,0)
      if(L_(1081)) iv2=ibset(iv2,1)
      if(L_(956)) iv2=ibset(iv2,2)
      if(L_(818)) iv2=ibset(iv2,3)
      if(L_(582)) iv2=ibset(iv2,4)
      if(L_(350)) iv2=ibset(iv2,5)
C FDA90_logic.fgi( 554,1388):������-�������: ������� ������ ������/����������/����� ���������
      if(L_erabe.and..not.L0_arabe) then
         R0_opabe=R0_upabe
      else
         R0_opabe=max(R_(255)-deltat,0.0)
      endif
      L_(962)=R0_opabe.gt.0.0
      L0_arabe=L_erabe
C FDA90_logic.fgi( 161, 841):������������  �� T
      L_(961)=.not.L_(1246) .and.L_(962)
      L_(959)=L_(962).and..not.L_(960)
C FDA90_logic.fgi( 181, 842):��������� ������
      iv2=0
      if(L_(1169)) iv2=ibset(iv2,0)
      if(L_(961)) iv2=ibset(iv2,1)
C FDA90_logic.fgi( 551,1226):������-�������: ������� ������ ������/����������/����� ���������
      L_(101)=R_axobe.gt.R0_ekid
C FDA90_lamp.fgi( 282, 349):���������� >
      L_(100)=R_axobe.lt.R0_akid
C FDA90_lamp.fgi( 282, 342):���������� <
      L_(99)=R_axobe.gt.R0_ufid
C FDA90_lamp.fgi( 282, 334):���������� >
      L_(141) = L_(100).AND.L_(99)
C FDA90_lamp.fgi( 287, 341):�
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ikaf,L_alaf,R_ukaf,
     & REAL(R_elaf,4),L_(141),L_ekaf,I_okaf)
      !}
C FDA90_lamp.fgi( 306, 340):���������� ������� ���������,20FDA91AE007QH04
      L_(98)=R_axobe.lt.R0_ofid
C FDA90_lamp.fgi( 282, 327):���������� <
      L_(97)=R_axobe.gt.R0_ifid
C FDA90_lamp.fgi( 282, 319):���������� >
      L_(142) = L_(98).AND.L_(97)
C FDA90_lamp.fgi( 287, 326):�
      !{
      Call DAT_DISCR_HANDLER(deltat,I_amaf,L_omaf,R_imaf,
     & REAL(R_umaf,4),L_(142),L_ulaf,I_emaf)
      !}
C FDA90_lamp.fgi( 306, 324):���������� ������� ���������,20FDA91AE007QH02
      L_(96)=R_axobe.lt.R0_efid
C FDA90_lamp.fgi( 282, 310):���������� <
      L_(95)=R_axobe.gt.R0_afid
C FDA90_lamp.fgi( 282, 302):���������� >
      L_(143) = L_(96).AND.L_(95)
C FDA90_lamp.fgi( 287, 309):�
      !{
      Call DAT_DISCR_HANDLER(deltat,I_opaf,L_eraf,R_araf,
     & REAL(R_iraf,4),L_(143),L_ipaf,I_upaf)
      !}
C FDA90_lamp.fgi( 306, 308):���������� ������� ���������,20FDA91AE007QH03
      L_(94)=R_axobe.lt.R0_udid
C FDA90_lamp.fgi( 282, 293):���������� <
      L_(93)=R_axobe.gt.R0_odid
C FDA90_lamp.fgi( 282, 285):���������� >
      L_(144) = L_(94).AND.L_(93)
C FDA90_lamp.fgi( 287, 292):�
      !{
      Call DAT_DISCR_HANDLER(deltat,I_esaf,L_usaf,R_osaf,
     & REAL(R_ataf,4),L_(144),L_asaf,I_isaf)
      !}
C FDA90_lamp.fgi( 306, 290):���������� ������� ���������,20FDA91AE007QH06
      L_(102)=R_axobe.lt.R0_ikid
C FDA90_lamp.fgi( 282, 357):���������� <
      L_(145) = L_(102).AND.L_(101)
C FDA90_lamp.fgi( 287, 356):�
      !{
      Call DAT_DISCR_HANDLER(deltat,I_utaf,L_ivaf,R_evaf,
     & REAL(R_ovaf,4),L_(145),L_otaf,I_avaf)
      !}
C FDA90_lamp.fgi( 306, 354):���������� ������� ���������,20FDA91AE007QH01
      iv2=0
      if(L_(1159)) iv2=ibset(iv2,0)
C FDA90_logic.fgi( 555,1178):������-�������: ������� ������ ������/����������/����� ���������
      if(L_ebebe.and..not.L0_abebe) then
         R0_oxabe=R0_uxabe
      else
         R0_oxabe=max(R_(266)-deltat,0.0)
      endif
      L_(995)=R0_oxabe.gt.0.0
      L0_abebe=L_ebebe
C FDA90_logic.fgi( 164, 959):������������  �� T
      L_(994)=.not.L_(1246) .and.L_(995)
      L_(992)=L_(995).and..not.L_(993)
C FDA90_logic.fgi( 181, 960):��������� ������
      iv2=0
      if(L_(1222)) iv2=ibset(iv2,0)
      if(L_(994)) iv2=ibset(iv2,1)
C FDA90_logic.fgi( 551,1302):������-�������: ������� ������ ������/����������/����� ���������
      Call PUMP_HANDLER(deltat,C30_eros,I_usos,L_abade,L_adade
     &,L_afade,
     & I_atos,I_osos,R_apos,REAL(R_ipos,4),
     & R_imos,REAL(R_umos,4),I_etos,L_uxos,L_uvos,L_odus,
     & L_udus,L_asos,L_avos,L_ivos,L_evos,L_ovos,L_ixos,L_emos
     &,
     & L_epos,L_amos,L_omos,L_adus,L_(251),
     & L_edus,L_(250),L_ulos,L_olos,L_esos,I_itos,R_ebus,R_ibus
     &,
     & L_ifus,L_oxos,L_idus,REAL(R8_epav,8),L_exos,
     & REAL(R8_axos,8),R_afus,REAL(R_otos,4),R_utos,REAL(R8_uros
     &,8),R_oros,
     & R8_avav,R_efus,R8_axos,REAL(R_opos,4),REAL(R_upos,4
     &))
C FDA90_vlv.fgi(  21, 121):���������� ���������� �������,20FDA91CM001KN01
C label 2664  try2664=try2664-1
C sav1=R_efus
C sav2=R_afus
C sav3=L_uxos
C sav4=L_ixos
C sav5=L_uvos
C sav6=R8_axos
C sav7=R_utos
C sav8=I_itos
C sav9=I_etos
C sav10=I_atos
C sav11=I_usos
C sav12=I_osos
C sav13=I_isos
C sav14=L_esos
C sav15=L_asos
C sav16=R_oros
C sav17=C30_eros
C sav18=L_epos
C sav19=L_omos
      Call PUMP_HANDLER(deltat,C30_eros,I_usos,L_abade,L_adade
     &,L_afade,
     & I_atos,I_osos,R_apos,REAL(R_ipos,4),
     & R_imos,REAL(R_umos,4),I_etos,L_uxos,L_uvos,L_odus,
     & L_udus,L_asos,L_avos,L_ivos,L_evos,L_ovos,L_ixos,L_emos
     &,
     & L_epos,L_amos,L_omos,L_adus,L_(251),
     & L_edus,L_(250),L_ulos,L_olos,L_esos,I_itos,R_ebus,R_ibus
     &,
     & L_ifus,L_oxos,L_idus,REAL(R8_epav,8),L_exos,
     & REAL(R8_axos,8),R_afus,REAL(R_otos,4),R_utos,REAL(R8_uros
     &,8),R_oros,
     & R8_avav,R_efus,R8_axos,REAL(R_opos,4),REAL(R_upos,4
     &))
C FDA90_vlv.fgi(  21, 121):recalc:���������� ���������� �������,20FDA91CM001KN01
C if(sav1.ne.R_efus .and. try2664.gt.0) goto 2664
C if(sav2.ne.R_afus .and. try2664.gt.0) goto 2664
C if(sav3.ne.L_uxos .and. try2664.gt.0) goto 2664
C if(sav4.ne.L_ixos .and. try2664.gt.0) goto 2664
C if(sav5.ne.L_uvos .and. try2664.gt.0) goto 2664
C if(sav6.ne.R8_axos .and. try2664.gt.0) goto 2664
C if(sav7.ne.R_utos .and. try2664.gt.0) goto 2664
C if(sav8.ne.I_itos .and. try2664.gt.0) goto 2664
C if(sav9.ne.I_etos .and. try2664.gt.0) goto 2664
C if(sav10.ne.I_atos .and. try2664.gt.0) goto 2664
C if(sav11.ne.I_usos .and. try2664.gt.0) goto 2664
C if(sav12.ne.I_osos .and. try2664.gt.0) goto 2664
C if(sav13.ne.I_isos .and. try2664.gt.0) goto 2664
C if(sav14.ne.L_esos .and. try2664.gt.0) goto 2664
C if(sav15.ne.L_asos .and. try2664.gt.0) goto 2664
C if(sav16.ne.R_oros .and. try2664.gt.0) goto 2664
C if(sav17.ne.C30_eros .and. try2664.gt.0) goto 2664
C if(sav18.ne.L_epos .and. try2664.gt.0) goto 2664
C if(sav19.ne.L_omos .and. try2664.gt.0) goto 2664
      if(L_ixos) then
         R_(20)=R_(21)
      else
         R_(20)=R_(22)
      endif
C FDA90_vent_log.fgi( 106, 267):���� RE IN LO CH7
      R8_ekif=(R0_ufif*R_(19)+deltat*R_(20))/(R0_ufif+deltat
     &)
C FDA90_vent_log.fgi( 115, 268):�������������� �����  
      R8_akif=R8_ekif
C FDA90_vent_log.fgi( 131, 268):������,F_FDA91CM001KN01_G
      Call PUMP_HANDLER(deltat,C30_ufir,I_ilir,L_abade,L_adade
     &,L_afade,
     & I_olir,I_elir,R_odir,REAL(R_afir,4),
     & R_adir,REAL(R_idir,4),I_ulir,L_erir,L_ipir,L_atir,
     & L_etir,L_okir,L_omir,L_apir,L_umir,L_epir,L_arir,L_ubir
     &,
     & L_udir,L_obir,L_edir,L_isir,L_(247),
     & L_osir,L_(246),L_ibir,L_ebir,L_ukir,I_amir,R_orir,R_urir
     &,
     & L_utir,L_oxos,L_usir,REAL(R8_epav,8),L_upir,
     & REAL(R8_opir,8),R_itir,REAL(R_emir,4),R_imir,REAL(R8_ikir
     &,8),R_ekir,
     & R8_avav,R_otir,R8_opir,REAL(R_efir,4),REAL(R_ifir,4
     &))
C FDA90_vlv.fgi(  21,  97):���������� ���������� �������,20FDA91CM002KN01
C label 2672  try2672=try2672-1
C sav1=R_otir
C sav2=R_itir
C sav3=L_erir
C sav4=L_arir
C sav5=L_ipir
C sav6=R8_opir
C sav7=R_imir
C sav8=I_amir
C sav9=I_ulir
C sav10=I_olir
C sav11=I_ilir
C sav12=I_elir
C sav13=I_alir
C sav14=L_ukir
C sav15=L_okir
C sav16=R_ekir
C sav17=C30_ufir
C sav18=L_udir
C sav19=L_edir
      Call PUMP_HANDLER(deltat,C30_ufir,I_ilir,L_abade,L_adade
     &,L_afade,
     & I_olir,I_elir,R_odir,REAL(R_afir,4),
     & R_adir,REAL(R_idir,4),I_ulir,L_erir,L_ipir,L_atir,
     & L_etir,L_okir,L_omir,L_apir,L_umir,L_epir,L_arir,L_ubir
     &,
     & L_udir,L_obir,L_edir,L_isir,L_(247),
     & L_osir,L_(246),L_ibir,L_ebir,L_ukir,I_amir,R_orir,R_urir
     &,
     & L_utir,L_oxos,L_usir,REAL(R8_epav,8),L_upir,
     & REAL(R8_opir,8),R_itir,REAL(R_emir,4),R_imir,REAL(R8_ikir
     &,8),R_ekir,
     & R8_avav,R_otir,R8_opir,REAL(R_efir,4),REAL(R_ifir,4
     &))
C FDA90_vlv.fgi(  21,  97):recalc:���������� ���������� �������,20FDA91CM002KN01
C if(sav1.ne.R_otir .and. try2672.gt.0) goto 2672
C if(sav2.ne.R_itir .and. try2672.gt.0) goto 2672
C if(sav3.ne.L_erir .and. try2672.gt.0) goto 2672
C if(sav4.ne.L_arir .and. try2672.gt.0) goto 2672
C if(sav5.ne.L_ipir .and. try2672.gt.0) goto 2672
C if(sav6.ne.R8_opir .and. try2672.gt.0) goto 2672
C if(sav7.ne.R_imir .and. try2672.gt.0) goto 2672
C if(sav8.ne.I_amir .and. try2672.gt.0) goto 2672
C if(sav9.ne.I_ulir .and. try2672.gt.0) goto 2672
C if(sav10.ne.I_olir .and. try2672.gt.0) goto 2672
C if(sav11.ne.I_ilir .and. try2672.gt.0) goto 2672
C if(sav12.ne.I_elir .and. try2672.gt.0) goto 2672
C if(sav13.ne.I_alir .and. try2672.gt.0) goto 2672
C if(sav14.ne.L_ukir .and. try2672.gt.0) goto 2672
C if(sav15.ne.L_okir .and. try2672.gt.0) goto 2672
C if(sav16.ne.R_ekir .and. try2672.gt.0) goto 2672
C if(sav17.ne.C30_ufir .and. try2672.gt.0) goto 2672
C if(sav18.ne.L_udir .and. try2672.gt.0) goto 2672
C if(sav19.ne.L_edir .and. try2672.gt.0) goto 2672
      if(L_arir) then
         R_(12)=R_(13)
      else
         R_(12)=R_(14)
      endif
C FDA90_vent_log.fgi( 246, 267):���� RE IN LO CH7
      R8_edif=(R0_ubif*R_(11)+deltat*R_(12))/(R0_ubif+deltat
     &)
C FDA90_vent_log.fgi( 255, 268):�������������� �����  
      R8_adif=R8_edif
C FDA90_vent_log.fgi( 271, 268):������,F_FDA91CM002KN01_G
      Call PUMP_HANDLER(deltat,C30_itis,I_axis,L_abade,L_adade
     &,L_afade,
     & I_exis,I_uvis,R_esis,REAL(R_osis,4),
     & R_oris,REAL(R_asis,4),I_ixis,L_udos,L_ados,L_okos,
     & L_ukos,L_evis,L_ebos,L_obos,L_ibos,L_ubos,L_odos,L_iris
     &,
     & L_isis,L_eris,L_uris,L_akos,L_(249),
     & L_ekos,L_(248),L_aris,L_upis,L_ivis,I_oxis,R_efos,R_ifos
     &,
     & L_ilos,L_oxos,L_ikos,REAL(R8_epav,8),L_idos,
     & REAL(R8_edos,8),R_alos,REAL(R_uxis,4),R_abos,REAL(R8_avis
     &,8),R_utis,
     & R8_avav,R_elos,R8_edos,REAL(R_usis,4),REAL(R_atis,4
     &))
C FDA90_vlv.fgi(  39, 121):���������� ���������� �������,20FDA91CU001KN01
C label 2680  try2680=try2680-1
C sav1=R_elos
C sav2=R_alos
C sav3=L_udos
C sav4=L_odos
C sav5=L_ados
C sav6=R8_edos
C sav7=R_abos
C sav8=I_oxis
C sav9=I_ixis
C sav10=I_exis
C sav11=I_axis
C sav12=I_uvis
C sav13=I_ovis
C sav14=L_ivis
C sav15=L_evis
C sav16=R_utis
C sav17=C30_itis
C sav18=L_isis
C sav19=L_uris
      Call PUMP_HANDLER(deltat,C30_itis,I_axis,L_abade,L_adade
     &,L_afade,
     & I_exis,I_uvis,R_esis,REAL(R_osis,4),
     & R_oris,REAL(R_asis,4),I_ixis,L_udos,L_ados,L_okos,
     & L_ukos,L_evis,L_ebos,L_obos,L_ibos,L_ubos,L_odos,L_iris
     &,
     & L_isis,L_eris,L_uris,L_akos,L_(249),
     & L_ekos,L_(248),L_aris,L_upis,L_ivis,I_oxis,R_efos,R_ifos
     &,
     & L_ilos,L_oxos,L_ikos,REAL(R8_epav,8),L_idos,
     & REAL(R8_edos,8),R_alos,REAL(R_uxis,4),R_abos,REAL(R8_avis
     &,8),R_utis,
     & R8_avav,R_elos,R8_edos,REAL(R_usis,4),REAL(R_atis,4
     &))
C FDA90_vlv.fgi(  39, 121):recalc:���������� ���������� �������,20FDA91CU001KN01
C if(sav1.ne.R_elos .and. try2680.gt.0) goto 2680
C if(sav2.ne.R_alos .and. try2680.gt.0) goto 2680
C if(sav3.ne.L_udos .and. try2680.gt.0) goto 2680
C if(sav4.ne.L_odos .and. try2680.gt.0) goto 2680
C if(sav5.ne.L_ados .and. try2680.gt.0) goto 2680
C if(sav6.ne.R8_edos .and. try2680.gt.0) goto 2680
C if(sav7.ne.R_abos .and. try2680.gt.0) goto 2680
C if(sav8.ne.I_oxis .and. try2680.gt.0) goto 2680
C if(sav9.ne.I_ixis .and. try2680.gt.0) goto 2680
C if(sav10.ne.I_exis .and. try2680.gt.0) goto 2680
C if(sav11.ne.I_axis .and. try2680.gt.0) goto 2680
C if(sav12.ne.I_uvis .and. try2680.gt.0) goto 2680
C if(sav13.ne.I_ovis .and. try2680.gt.0) goto 2680
C if(sav14.ne.L_ivis .and. try2680.gt.0) goto 2680
C if(sav15.ne.L_evis .and. try2680.gt.0) goto 2680
C if(sav16.ne.R_utis .and. try2680.gt.0) goto 2680
C if(sav17.ne.C30_itis .and. try2680.gt.0) goto 2680
C if(sav18.ne.L_isis .and. try2680.gt.0) goto 2680
C if(sav19.ne.L_uris .and. try2680.gt.0) goto 2680
      if(L_odos) then
         R_(24)=R_(25)
      else
         R_(24)=R_(26)
      endif
C FDA90_vent_log.fgi(  37, 267):���� RE IN LO CH7
      R8_elif=(R0_ukif*R_(23)+deltat*R_(24))/(R0_ukif+deltat
     &)
C FDA90_vent_log.fgi(  46, 268):�������������� �����  
      R8_alif=R8_elif
C FDA90_vent_log.fgi(  62, 268):������,F_FDA91CU001KN01_G
      Call PUMP_HANDLER(deltat,C30_amer,I_oper,L_abade,L_adade
     &,L_afade,
     & I_uper,I_iper,R_uker,REAL(R_eler,4),
     & R_eker,REAL(R_oker,4),I_arer,L_iter,L_oser,L_exer,
     & L_ixer,L_umer,L_urer,L_eser,L_aser,L_iser,L_eter,L_aker
     &,
     & L_aler,L_ufer,L_iker,L_over,L_(245),
     & L_uver,L_(244),L_ofer,L_ifer,L_aper,I_erer,R_uter,R_aver
     &,
     & L_abir,L_oxos,L_axer,REAL(R8_epav,8),L_ater,
     & REAL(R8_user,8),R_oxer,REAL(R_irer,4),R_orer,REAL(R8_omer
     &,8),R_imer,
     & R8_avav,R_uxer,R8_user,REAL(R_iler,4),REAL(R_oler,4
     &))
C FDA90_vlv.fgi(  39,  97):���������� ���������� �������,20FDA91CU002KN01
C label 2688  try2688=try2688-1
C sav1=R_uxer
C sav2=R_oxer
C sav3=L_iter
C sav4=L_eter
C sav5=L_oser
C sav6=R8_user
C sav7=R_orer
C sav8=I_erer
C sav9=I_arer
C sav10=I_uper
C sav11=I_oper
C sav12=I_iper
C sav13=I_eper
C sav14=L_aper
C sav15=L_umer
C sav16=R_imer
C sav17=C30_amer
C sav18=L_aler
C sav19=L_iker
      Call PUMP_HANDLER(deltat,C30_amer,I_oper,L_abade,L_adade
     &,L_afade,
     & I_uper,I_iper,R_uker,REAL(R_eler,4),
     & R_eker,REAL(R_oker,4),I_arer,L_iter,L_oser,L_exer,
     & L_ixer,L_umer,L_urer,L_eser,L_aser,L_iser,L_eter,L_aker
     &,
     & L_aler,L_ufer,L_iker,L_over,L_(245),
     & L_uver,L_(244),L_ofer,L_ifer,L_aper,I_erer,R_uter,R_aver
     &,
     & L_abir,L_oxos,L_axer,REAL(R8_epav,8),L_ater,
     & REAL(R8_user,8),R_oxer,REAL(R_irer,4),R_orer,REAL(R8_omer
     &,8),R_imer,
     & R8_avav,R_uxer,R8_user,REAL(R_iler,4),REAL(R_oler,4
     &))
C FDA90_vlv.fgi(  39,  97):recalc:���������� ���������� �������,20FDA91CU002KN01
C if(sav1.ne.R_uxer .and. try2688.gt.0) goto 2688
C if(sav2.ne.R_oxer .and. try2688.gt.0) goto 2688
C if(sav3.ne.L_iter .and. try2688.gt.0) goto 2688
C if(sav4.ne.L_eter .and. try2688.gt.0) goto 2688
C if(sav5.ne.L_oser .and. try2688.gt.0) goto 2688
C if(sav6.ne.R8_user .and. try2688.gt.0) goto 2688
C if(sav7.ne.R_orer .and. try2688.gt.0) goto 2688
C if(sav8.ne.I_erer .and. try2688.gt.0) goto 2688
C if(sav9.ne.I_arer .and. try2688.gt.0) goto 2688
C if(sav10.ne.I_uper .and. try2688.gt.0) goto 2688
C if(sav11.ne.I_oper .and. try2688.gt.0) goto 2688
C if(sav12.ne.I_iper .and. try2688.gt.0) goto 2688
C if(sav13.ne.I_eper .and. try2688.gt.0) goto 2688
C if(sav14.ne.L_aper .and. try2688.gt.0) goto 2688
C if(sav15.ne.L_umer .and. try2688.gt.0) goto 2688
C if(sav16.ne.R_imer .and. try2688.gt.0) goto 2688
C if(sav17.ne.C30_amer .and. try2688.gt.0) goto 2688
C if(sav18.ne.L_aler .and. try2688.gt.0) goto 2688
C if(sav19.ne.L_iker .and. try2688.gt.0) goto 2688
      if(L_eter) then
         R_(16)=R_(17)
      else
         R_(16)=R_(18)
      endif
C FDA90_vent_log.fgi( 177, 267):���� RE IN LO CH7
      R8_efif=(R0_udif*R_(15)+deltat*R_(16))/(R0_udif+deltat
     &)
C FDA90_vent_log.fgi( 186, 268):�������������� �����  
      R8_afif=R8_efif
C FDA90_vent_log.fgi( 202, 268):������,F_FDA91CU002KN01_G
      End
