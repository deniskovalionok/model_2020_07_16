      Subroutine FDA40(ext_deltat)
      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      Include 'FDA40.fh'

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      Call FDA40_ConIn
      R_(113)=R8_ivud
C FDA40_vent_log.fgi( 122, 267):pre: �������������� �����  
      R_(236)=R0_efak
C FDA40_logic_press_1.fgi( 184,  83):pre: ������������  �� T
      R_(117)=R8_ixud
C FDA40_vent_log.fgi(  49, 267):pre: �������������� �����  
      R_(96)=R8_emud
C FDA40_vent_log.fgi(  48, 127):pre: �������������� �����  
      I_(199)=I_e !CopyBack
C FDA40_logic_press_step.fgi( 417, 961):pre: ������-�������: ���������� ��� �������������� ������
      R_(129)=R0_emaf
C FDA40_logic_press_step.fgi( 445, 355):pre: ������������  �� T
      R_(7)=R0_ok
C FDA40_logic_box_step.fgi(  59, 611):pre: �������� ������� ������
      R_(191)=R0_utif
C FDA40_logic_press_step.fgi( 442, 901):pre: ������������  �� T
      I_(30)=I_i !CopyBack
C FDA40_logic_box_step.fgi( 124, 171):pre: ������-�������: ���������� ��� �������������� ������
      R_(9)=R0_ul
C FDA40_logic_box_step.fgi( 132,  39):pre: �������� ��������� ������
      R_(40)=R0_ove
C FDA40_logic_box_step.fgi( 132, 418):pre: �������� ��������� ������
      R_(143)=R0_idef
C FDA40_logic_press_step.fgi( 409, 522):pre: �������� ������� ������
      R_(137)=R0_otaf
C FDA40_logic_press_step.fgi( 452, 424):pre: ������������  �� T
      R_(146)=R0_afef
C FDA40_logic_press_step.fgi( 446, 514):pre: ������������  �� T
      R_(153)=R0_ilef
C FDA40_logic_press_step.fgi( 446, 548):pre: ������������  �� T
      R_(134)=R0_esaf
C FDA40_logic_press_step.fgi( 404, 460):pre: �������� ������� ������
      R_(141)=R0_abef
C FDA40_logic_press_step.fgi( 409, 492):pre: �������� ������� ������
      R_(150)=R0_alef
C FDA40_logic_press_step.fgi( 409, 558):pre: �������� ������� ������
      R_(168)=R0_uxef
C FDA40_logic_press_step.fgi( 189, 872):pre: �������� ������� ������
      R_(163)=R0_otef
C FDA40_logic_press_step.fgi( 443, 692):pre: ������������  �� T
      R_(188)=R0_esif
C FDA40_logic_press_step.fgi( 443, 750):pre: ������������  �� T
      R_(136)=R0_osaf
C FDA40_logic_press_step.fgi( 446, 452):pre: ������������  �� T
      R_(142)=R0_ibef
C FDA40_logic_press_step.fgi( 446, 484):pre: ������������  �� T
      R_(181)=R0_arif
C FDA40_logic_press_step.fgi( 241, 891):pre: ������������  �� T
      R_(157)=R0_epef
C FDA40_logic_press_step.fgi( 442, 602):pre: ������������  �� T
      R_(165)=R0_ovef
C FDA40_logic_press_step.fgi( 443, 708):pre: ������������  �� T
      R_(190)=R0_etif
C FDA40_logic_press_step.fgi( 443, 795):pre: ������������  �� T
      R_(155)=R0_emef
C FDA40_logic_press_step.fgi( 451, 575):pre: ������������  �� T
      R_(147)=R0_ekef
C FDA40_logic_press_step.fgi( 441, 575):pre: �������� ��������� ������
      R_(159)=R0_uref
C FDA40_logic_press_step.fgi( 443, 655):pre: ������������  �� T
      R_(172)=R0_odif
C FDA40_logic_press_step.fgi( 443, 805):pre: ������������  �� T
      R_(174)=R0_ukif
C FDA40_logic_press_step.fgi( 210, 932):pre: �������� ������� ������
      R_(176)=R0_ilif
C FDA40_logic_press_step.fgi( 241, 940):pre: ������������  �� T
      R_(199)=R0_ifof
C FDA40_logic_press_step.fgi( 443, 879):pre: ������������  �� T
      R_(173)=R0_ifif
C FDA40_logic_press_step.fgi( 408, 907):pre: �������� ������� ������
      R_(179)=R0_epif
C FDA40_logic_press_step.fgi( 241, 951):pre: ������������  �� T
      R_(195)=R0_uxif
C FDA40_logic_press_step.fgi( 442, 933):pre: ������������  �� T
      R_(193)=R0_ovif
C FDA40_logic_press_step.fgi( 442, 916):pre: ������������  �� T
      R_(19)=R0_it
C FDA40_logic_box_step.fgi( 150,  91):pre: ������������  �� T
      R_(32)=R0_ule
C FDA40_logic_box_step.fgi( 150, 222):pre: ������������  �� T
      R_(72)=R0_alo
C FDA40_logic_box_step.fgi( 150, 630):pre: ������������  �� T
      R_(11)=R0_em
C FDA40_logic_box_step.fgi( 101,  40):pre: ������������  �� T
      R_(8)=R0_el
C FDA40_logic_box_step.fgi( 104,  54):pre: �������� ������� ������
      R_(17)=R0_os
C FDA40_logic_box_step.fgi(  99,  83):pre: ������������  �� T
      R_(15)=R0_es
C FDA40_logic_box_step.fgi( 103,  96):pre: �������� ������� ������
      R_(30)=R0_ale
C FDA40_logic_box_step.fgi( 101, 213):pre: ������������  �� T
      R_(27)=R0_ake
C FDA40_logic_box_step.fgi( 104, 227):pre: �������� ������� ������
      R_(50)=R0_uki
C FDA40_logic_box_step.fgi( 104, 431):pre: ������������  �� T
      R_(39)=R0_ave
C FDA40_logic_box_step.fgi( 105, 444):pre: �������� ������� ������
      R_(52)=R0_ili
C FDA40_logic_box_step.fgi( 101, 537):pre: ������������  �� T
      R_(42)=R0_uxe
C FDA40_logic_box_step.fgi( 104, 551):pre: �������� ������� ������
      R_(54)=R0_ami
C FDA40_logic_box_step.fgi(  99, 622):pre: ������������  �� T
      R_(48)=R0_iki
C FDA40_logic_box_step.fgi( 103, 635):pre: �������� ������� ������
      R_(13)=R0_ap
C FDA40_logic_box_step.fgi( 150,  49):pre: ������������  �� T
      R_(14)=R0_or
C FDA40_logic_box_step.fgi( 132,  81):pre: �������� ��������� ������
      R_(38)=R0_ese
C FDA40_logic_box_step.fgi( 150, 319):pre: ������������  �� T
      R_(62)=R0_uti
C FDA40_logic_box_step.fgi( 150, 439):pre: ������������  �� T
      R_(41)=R0_exe
C FDA40_logic_box_step.fgi( 132, 485):pre: �������� ��������� ������
      R_(43)=R0_ibi
C FDA40_logic_box_step.fgi( 132, 536):pre: �������� ��������� ������
      R_(70)=R0_ufo
C FDA40_logic_box_step.fgi( 150, 546):pre: ������������  �� T
      R_(44)=R0_adi
C FDA40_logic_box_step.fgi( 132, 614):pre: �������� ��������� ������
      R_(45)=R0_odi
C FDA40_logic_box_step.fgi( 132, 669):pre: �������� ��������� ������
      R_(75)=R0_umo
C FDA40_logic_box_step.fgi( 150, 688):pre: ������������  �� T
      R_(46)=R0_efi
C FDA40_logic_box_step.fgi( 132, 698):pre: �������� ��������� ������
      R_(47)=R0_ufi
C FDA40_logic_box_step.fgi( 132, 719):pre: �������� ��������� ������
      R_(26)=R0_afe
C FDA40_logic_box_step.fgi( 150, 196):pre: ������������  �� T
      R_(23)=R0_abe
C FDA40_logic_box_step.fgi( 150, 145):pre: ������������  �� T
      R_(21)=R0_ix
C FDA40_logic_box_step.fgi( 132, 183):pre: �������� ��������� ������
      R_(28)=R0_oke
C FDA40_logic_box_step.fgi( 132, 212):pre: �������� ��������� ������
      R_(34)=R0_ome
C FDA40_logic_box_step.fgi( 150, 248):pre: ������������  �� T
      R_(58)=R0_iri
C FDA40_logic_box_step.fgi( 150, 339):pre: ������������  �� T
      R_(79)=R0_oro
C FDA40_logic_box_step.fgi( 150, 730):pre: ������������  �� T
      R_(36)=R0_are
C FDA40_logic_box_step.fgi( 150, 291):pre: ������������  �� T
      R_(20)=R0_iv
C FDA40_logic_box_step.fgi(  87, 114):pre: �������� ��������� ������
      R_(60)=R0_osi
C FDA40_logic_box_step.fgi( 150, 404):pre: ������������  �� T
      R_(24)=R0_ube
C FDA40_logic_box_step.fgi( 151, 171):pre: ������������  �� T
      R_(64)=R0_uvi
C FDA40_logic_box_step.fgi( 150, 462):pre: ������������  �� T
      R_(73)=R0_amo
C FDA40_logic_box_step.fgi(  87, 653):pre: �������� ��������� ������
      R_(67)=R0_edo
C FDA40_logic_box_step.fgi( 151, 509):pre: ������������  �� T
      R_(65)=R0_oxi
C FDA40_logic_box_step.fgi( 151, 473):pre: ������������  �� T
      R_(77)=R0_upo
C FDA40_logic_box_step.fgi( 150, 708):pre: ������������  �� T
      R_(197)=R0_idof
C FDA40_logic_press_step.fgi( 443, 862):pre: ������������  �� T
      R_(170)=R0_edif
C FDA40_logic_press_step.fgi( 410, 784):pre: �������� ������� ������
      R_(169)=R0_obif
C FDA40_logic_press_step.fgi( 409, 739):pre: �������� ������� ������
      R_(160)=R0_usef
C FDA40_logic_press_step.fgi( 409, 681):pre: �������� ������� ������
      R_(88)=R8_ubud
C FDA40_vent_log.fgi( 172,  96):pre: �������������� �����  
      R_(3)=R0_id
C FDA40_logic_box_step.fgi(  69, 770):pre: ������������  �� T
      !��������� R_(1) = FDA40_logic_box_stepC?? /9/
      R_(1)=R0_o
C FDA40_logic_box_step.fgi( 353, 423):���������
      !��������� R_(2) = FDA40_logic_box_stepC?? /3/
      R_(2)=R0_u
C FDA40_logic_box_step.fgi( 353, 425):���������
      I_(1) = 2
C FDA40_logic_box_step.fgi(  70, 274):��������� ������������� IN (�������)
      I_(2) = I_(1) * I1_axaf
C FDA40_logic_box_step.fgi(  73, 278):����������
      !��������� I_(3) = FDA40_logic_box_stepC?? /1/
      I_(3)=I0_ed
C FDA40_logic_box_step.fgi( 327, 410):��������� ������������� IN
      L_(126)=I_isu.lt.I_(3)
C FDA40_logic_box_step.fgi( 331, 411):���������� �������������
      !��������� R_(4) = FDA40_logic_box_stepC?? /0/
      R_(4)=R0_af
C FDA40_logic_box_step.fgi( 288, 493):���������
      I_(4) = 21
C FDA40_logic_box_step.fgi( 326, 301):��������� ������������� IN (�������)
      I_(5) = 16
C FDA40_logic_box_step.fgi( 326, 312):��������� ������������� IN (�������)
      I_(8) = 0
C FDA40_logic_box_step.fgi( 336, 319):��������� ������������� IN (�������)
      I_(6) = 2
C FDA40_logic_box_step.fgi( 273, 318):��������� ������������� IN (�������)
      I_(7) = I_(6) * I1_axaf
C FDA40_logic_box_step.fgi( 276, 317):����������
      !��������� I_(10) = FDA40_logic_box_stepC?? /26/
      I_(10)=I0_ar
C FDA40_logic_box_step.fgi(  54,  71):��������� ������������� IN
      !��������� I_(11) = FDA40_logic_box_stepC?? /0/
      I_(11)=I0_ate
C FDA40_logic_box_step.fgi(  90, 521):��������� ������������� IN
      L_(166)=I_isu.gt.I_(11)
C FDA40_logic_box_step.fgi(  94, 522):���������� �������������
      !��������� R_(55) = FDA40_logic_box_stepC?? /9/
      R_(55)=R0_omi
C FDA40_logic_box_step.fgi( 309, 438):���������
      !��������� R_(56) = FDA40_logic_box_stepC?? /3/
      R_(56)=R0_umi
C FDA40_logic_box_step.fgi( 309, 440):���������
      I_(12) = z'100000A'
C FDA40_logic_parameters.fgi(  46, 394):��������� ������������� IN (�������)
      I_(13) = z'1000003'
C FDA40_logic_parameters.fgi(  46, 396):��������� ������������� IN (�������)
      I_(14) = z'100000A'
C FDA40_logic_parameters.fgi(  50, 166):��������� ������������� IN (�������)
      I_(15) = z'1000003'
C FDA40_logic_parameters.fgi(  50, 168):��������� ������������� IN (�������)
      I_(16) = z'100000A'
C FDA40_logic_parameters.fgi(  50, 178):��������� ������������� IN (�������)
      I_(17) = z'1000003'
C FDA40_logic_parameters.fgi(  50, 180):��������� ������������� IN (�������)
      I_(18) = z'100000A'
C FDA40_logic_parameters.fgi(  50, 190):��������� ������������� IN (�������)
      I_(19) = z'1000003'
C FDA40_logic_parameters.fgi(  50, 192):��������� ������������� IN (�������)
      !��������� I_(20) = FDA40_logic_parametersC?? /0/
      I_(20)=I0_esu
C FDA40_logic_parameters.fgi(  24, 185):��������� ������������� IN
      L_(246)=I_isu.gt.I_(20)
C FDA40_logic_parameters.fgi(  28, 186):���������� �������������
      if(L_(246)) then
         I_asu=I_(18)
      else
         I_asu=I_(19)
      endif
C FDA40_logic_parameters.fgi(  53, 190):���� RE IN LO CH7
      I_(22) = z'100000A'
C FDA40_logic_parameters.fgi(  50, 203):��������� ������������� IN (�������)
      I_(23) = z'1000003'
C FDA40_logic_parameters.fgi(  50, 205):��������� ������������� IN (�������)
      I_(21) = 26
C FDA40_logic_parameters.fgi(  42, 198):��������� ������������� IN (�������)
      I_(26) = z'100000A'
C FDA40_logic_parameters.fgi(  50, 226):��������� ������������� IN (�������)
      I_(27) = z'1000003'
C FDA40_logic_parameters.fgi(  50, 228):��������� ������������� IN (�������)
      I_(24) = 27
C FDA40_logic_parameters.fgi(  42, 210):��������� ������������� IN (�������)
      I_(25) = 14
C FDA40_logic_parameters.fgi(  42, 221):��������� ������������� IN (�������)
      I_(31) = z'100000A'
C FDA40_logic_parameters.fgi(  49, 250):��������� ������������� IN (�������)
      I_(32) = z'1000003'
C FDA40_logic_parameters.fgi(  49, 252):��������� ������������� IN (�������)
      I_(28) = 14
C FDA40_logic_parameters.fgi(  41, 234):��������� ������������� IN (�������)
      I_(29) = 0
C FDA40_logic_parameters.fgi(  41, 245):��������� ������������� IN (�������)
      I_(33) = z'100000A'
C FDA40_logic_parameters.fgi(  37, 278):��������� ������������� IN (�������)
      I_(34) = z'1000003'
C FDA40_logic_parameters.fgi(  37, 280):��������� ������������� IN (�������)
      I_(35) = z'100000A'
C FDA40_logic_parameters.fgi(  40, 293):��������� ������������� IN (�������)
      I_(36) = z'1000003'
C FDA40_logic_parameters.fgi(  40, 295):��������� ������������� IN (�������)
      I_(37) = z'100000A'
C FDA40_logic_parameters.fgi(  40, 311):��������� ������������� IN (�������)
      I_(38) = z'1000003'
C FDA40_logic_parameters.fgi(  40, 313):��������� ������������� IN (�������)
      I_(39) = z'100000A'
C FDA40_logic_parameters.fgi(  40, 328):��������� ������������� IN (�������)
      I_(40) = z'1000003'
C FDA40_logic_parameters.fgi(  40, 330):��������� ������������� IN (�������)
      I_(41) = z'100000A'
C FDA40_logic_parameters.fgi(  40, 346):��������� ������������� IN (�������)
      I_(42) = z'1000003'
C FDA40_logic_parameters.fgi(  40, 348):��������� ������������� IN (�������)
      I_(43) = z'100000A'
C FDA40_logic_parameters.fgi(  46, 412):��������� ������������� IN (�������)
      I_(44) = z'1000003'
C FDA40_logic_parameters.fgi(  46, 414):��������� ������������� IN (�������)
      I_(45) = z'100000A'
C FDA40_logic_parameters.fgi(  46, 429):��������� ������������� IN (�������)
      I_(46) = z'1000003'
C FDA40_logic_parameters.fgi(  46, 431):��������� ������������� IN (�������)
      I_(47) = z'100000A'
C FDA40_logic_parameters.fgi(  46, 446):��������� ������������� IN (�������)
      I_(48) = z'1000003'
C FDA40_logic_parameters.fgi(  46, 448):��������� ������������� IN (�������)
      I_(49) = z'100000A'
C FDA40_logic_parameters.fgi(  40, 462):��������� ������������� IN (�������)
      I_(50) = z'1000003'
C FDA40_logic_parameters.fgi(  40, 464):��������� ������������� IN (�������)
      I_(51) = z'100000A'
C FDA40_logic_parameters.fgi( 288, 380):��������� ������������� IN (�������)
      I_(52) = z'1000003'
C FDA40_logic_parameters.fgi( 288, 382):��������� ������������� IN (�������)
      I_(53) = z'100000A'
C FDA40_logic_parameters.fgi( 288, 397):��������� ������������� IN (�������)
      I_(54) = z'1000003'
C FDA40_logic_parameters.fgi( 288, 399):��������� ������������� IN (�������)
      I_(55) = z'100000A'
C FDA40_logic_parameters.fgi( 288, 413):��������� ������������� IN (�������)
      I_(56) = z'1000003'
C FDA40_logic_parameters.fgi( 288, 415):��������� ������������� IN (�������)
      I_(57) = z'100000A'
C FDA40_logic_parameters.fgi( 288, 428):��������� ������������� IN (�������)
      I_(58) = z'1000003'
C FDA40_logic_parameters.fgi( 288, 430):��������� ������������� IN (�������)
      I_(59) = z'100000A'
C FDA40_logic_parameters.fgi( 288, 445):��������� ������������� IN (�������)
      I_(60) = z'1000003'
C FDA40_logic_parameters.fgi( 288, 447):��������� ������������� IN (�������)
      I_(61) = z'100000A'
C FDA40_logic_parameters.fgi( 288, 456):��������� ������������� IN (�������)
      I_(62) = z'1000003'
C FDA40_logic_parameters.fgi( 288, 458):��������� ������������� IN (�������)
      I_(63) = z'100000A'
C FDA40_logic_parameters.fgi( 288, 477):��������� ������������� IN (�������)
      I_(64) = z'1000003'
C FDA40_logic_parameters.fgi( 288, 479):��������� ������������� IN (�������)
      I_(65) = z'100000A'
C FDA40_logic_parameters.fgi( 288, 489):��������� ������������� IN (�������)
      I_(66) = z'1000003'
C FDA40_logic_parameters.fgi( 288, 491):��������� ������������� IN (�������)
      I_(67) = z'100000A'
C FDA40_logic_parameters.fgi( 288, 504):��������� ������������� IN (�������)
      I_(68) = z'1000003'
C FDA40_logic_parameters.fgi( 288, 506):��������� ������������� IN (�������)
      I_(69) = z'100000A'
C FDA40_logic_parameters.fgi( 288, 521):��������� ������������� IN (�������)
      I_(70) = z'1000003'
C FDA40_logic_parameters.fgi( 288, 523):��������� ������������� IN (�������)
      I_(71) = z'100000A'
C FDA40_logic_parameters.fgi( 288, 538):��������� ������������� IN (�������)
      I_(72) = z'1000003'
C FDA40_logic_parameters.fgi( 288, 540):��������� ������������� IN (�������)
      I_(73) = z'100000A'
C FDA40_logic_parameters.fgi( 288, 555):��������� ������������� IN (�������)
      I_(74) = z'1000003'
C FDA40_logic_parameters.fgi( 288, 557):��������� ������������� IN (�������)
      I_(75) = z'100000A'
C FDA40_logic_parameters.fgi( 288, 573):��������� ������������� IN (�������)
      I_(76) = z'1000003'
C FDA40_logic_parameters.fgi( 288, 575):��������� ������������� IN (�������)
      I_(77) = z'100000A'
C FDA40_logic_parameters.fgi( 194, 315):��������� ������������� IN (�������)
      I_(78) = z'1000003'
C FDA40_logic_parameters.fgi( 194, 317):��������� ������������� IN (�������)
      I_(79) = z'100000A'
C FDA40_logic_parameters.fgi( 194, 337):��������� ������������� IN (�������)
      I_(80) = z'1000003'
C FDA40_logic_parameters.fgi( 194, 339):��������� ������������� IN (�������)
      I_(81) = z'100000A'
C FDA40_logic_parameters.fgi( 194, 348):��������� ������������� IN (�������)
      I_(82) = z'1000003'
C FDA40_logic_parameters.fgi( 194, 350):��������� ������������� IN (�������)
      I_(83) = z'100000A'
C FDA40_logic_parameters.fgi( 194, 364):��������� ������������� IN (�������)
      I_(84) = z'1000003'
C FDA40_logic_parameters.fgi( 194, 366):��������� ������������� IN (�������)
      I_(85) = z'100000A'
C FDA40_logic_parameters.fgi( 194, 384):��������� ������������� IN (�������)
      I_(86) = z'1000003'
C FDA40_logic_parameters.fgi( 194, 386):��������� ������������� IN (�������)
      I_(87) = z'100000A'
C FDA40_logic_parameters.fgi( 194, 407):��������� ������������� IN (�������)
      I_(88) = z'1000003'
C FDA40_logic_parameters.fgi( 194, 409):��������� ������������� IN (�������)
      I_(89) = z'100000A'
C FDA40_logic_parameters.fgi( 194, 424):��������� ������������� IN (�������)
      I_(90) = z'1000003'
C FDA40_logic_parameters.fgi( 194, 426):��������� ������������� IN (�������)
      I_(91) = z'100000A'
C FDA40_logic_parameters.fgi( 194, 443):��������� ������������� IN (�������)
      I_(92) = z'1000003'
C FDA40_logic_parameters.fgi( 194, 445):��������� ������������� IN (�������)
      I_(93) = z'100000A'
C FDA40_logic_parameters.fgi( 194, 466):��������� ������������� IN (�������)
      I_(94) = z'1000003'
C FDA40_logic_parameters.fgi( 194, 468):��������� ������������� IN (�������)
      I_(95) = z'100000A'
C FDA40_logic_parameters.fgi( 194, 478):��������� ������������� IN (�������)
      I_(96) = z'1000003'
C FDA40_logic_parameters.fgi( 194, 480):��������� ������������� IN (�������)
      I_(97) = z'100000A'
C FDA40_logic_parameters.fgi( 194, 490):��������� ������������� IN (�������)
      I_(98) = z'1000003'
C FDA40_logic_parameters.fgi( 194, 492):��������� ������������� IN (�������)
      I_(99) = z'100000A'
C FDA40_logic_parameters.fgi( 194, 509):��������� ������������� IN (�������)
      I_(100) = z'1000003'
C FDA40_logic_parameters.fgi( 194, 511):��������� ������������� IN (�������)
      I_(101) = z'100000A'
C FDA40_logic_parameters.fgi( 194, 521):��������� ������������� IN (�������)
      I_(102) = z'1000003'
C FDA40_logic_parameters.fgi( 194, 523):��������� ������������� IN (�������)
      I_(103) = z'100000A'
C FDA40_logic_parameters.fgi( 194, 538):��������� ������������� IN (�������)
      I_(104) = z'1000003'
C FDA40_logic_parameters.fgi( 194, 540):��������� ������������� IN (�������)
      I_(105) = z'100000A'
C FDA40_logic_parameters.fgi( 194, 550):��������� ������������� IN (�������)
      I_(106) = z'1000003'
C FDA40_logic_parameters.fgi( 194, 552):��������� ������������� IN (�������)
      I_(107) = z'100000A'
C FDA40_logic_parameters.fgi( 194, 562):��������� ������������� IN (�������)
      I_(108) = z'1000003'
C FDA40_logic_parameters.fgi( 194, 564):��������� ������������� IN (�������)
      I_(109) = z'100000A'
C FDA40_logic_parameters.fgi( 194, 574):��������� ������������� IN (�������)
      I_(110) = z'1000003'
C FDA40_logic_parameters.fgi( 194, 576):��������� ������������� IN (�������)
      I_(111) = z'1000034'
C FDA40_logic_parameters.fgi( 116, 445):��������� ������������� IN (�������)
      I_(112) = z'1000007'
C FDA40_logic_parameters.fgi( 116, 447):��������� ������������� IN (�������)
      if(L_ofuf) then
         I_otad=I_(111)
      else
         I_otad=I_(112)
      endif
C FDA40_logic_parameters.fgi( 119, 445):���� RE IN LO CH7
      I_(113) = z'1000034'
C FDA40_logic_parameters.fgi( 116, 457):��������� ������������� IN (�������)
      I_(114) = z'1000007'
C FDA40_logic_parameters.fgi( 116, 459):��������� ������������� IN (�������)
      I_(115) = z'1000034'
C FDA40_logic_parameters.fgi( 116, 469):��������� ������������� IN (�������)
      I_(116) = z'1000007'
C FDA40_logic_parameters.fgi( 116, 471):��������� ������������� IN (�������)
      I_(117) = z'1000034'
C FDA40_logic_parameters.fgi( 116, 482):��������� ������������� IN (�������)
      I_(118) = z'1000007'
C FDA40_logic_parameters.fgi( 116, 484):��������� ������������� IN (�������)
      I_(123) = z'1000034'
C FDA40_logic_parameters.fgi( 116, 497):��������� ������������� IN (�������)
      I_(124) = z'1000007'
C FDA40_logic_parameters.fgi( 116, 499):��������� ������������� IN (�������)
      I_(125) = z'1000034'
C FDA40_logic_parameters.fgi( 114, 546):��������� ������������� IN (�������)
      I_(126) = z'1000007'
C FDA40_logic_parameters.fgi( 114, 548):��������� ������������� IN (�������)
      I_(119) = z'1000034'
C FDA40_logic_parameters.fgi( 127, 534):��������� ������������� IN (�������)
      I_(120) = z'1000007'
C FDA40_logic_parameters.fgi( 127, 536):��������� ������������� IN (�������)
      C30_uvad = '������� � ������ ���������'
C FDA40_logic_parameters.fgi( 110, 505):��������� ���������� CH20 (CH30) (�������)
      C30_ovad = '������� � ��������� �������'
C FDA40_logic_parameters.fgi( 110, 507):��������� ���������� CH20 (CH30) (�������)
      I_(127) = z'1000034'
C FDA40_logic_parameters.fgi( 116, 566):��������� ������������� IN (�������)
      I_(128) = z'1000007'
C FDA40_logic_parameters.fgi( 116, 568):��������� ������������� IN (�������)
      I_(121) = z'1000034'
C FDA40_logic_parameters.fgi( 116, 428):��������� ������������� IN (�������)
      I_(122) = z'1000007'
C FDA40_logic_parameters.fgi( 116, 430):��������� ������������� IN (�������)
      I_(129) = z'1000034'
C FDA40_logic_parameters.fgi( 116, 579):��������� ������������� IN (�������)
      I_(130) = z'1000007'
C FDA40_logic_parameters.fgi( 116, 581):��������� ������������� IN (�������)
      I_(131) = z'1000034'
C FDA40_logic_parameters.fgi(  36, 508):��������� ������������� IN (�������)
      I_(132) = z'1000007'
C FDA40_logic_parameters.fgi(  36, 510):��������� ������������� IN (�������)
      I_(133) = z'1000034'
C FDA40_logic_parameters.fgi(  36, 532):��������� ������������� IN (�������)
      I_(134) = z'1000007'
C FDA40_logic_parameters.fgi(  36, 530):��������� ������������� IN (�������)
      I_(135) = z'1000034'
C FDA40_logic_parameters.fgi(  36, 544):��������� ������������� IN (�������)
      I_(136) = z'1000007'
C FDA40_logic_parameters.fgi(  36, 542):��������� ������������� IN (�������)
      I_(137) = z'1000034'
C FDA40_logic_parameters.fgi(  36, 555):��������� ������������� IN (�������)
      I_(138) = z'1000007'
C FDA40_logic_parameters.fgi(  36, 553):��������� ������������� IN (�������)
      I_(139) = z'1000034'
C FDA40_logic_parameters.fgi(  36, 568):��������� ������������� IN (�������)
      I_(140) = z'1000007'
C FDA40_logic_parameters.fgi(  36, 566):��������� ������������� IN (�������)
      I_(141) = z'1000034'
C FDA40_logic_parameters.fgi(  36, 581):��������� ������������� IN (�������)
      I_(142) = z'1000007'
C FDA40_logic_parameters.fgi(  36, 579):��������� ������������� IN (�������)
      I_(143) = z'01000003'
C FDA40_logic_modes.fgi( 227, 438):��������� ������������� IN (�������)
      I_(144) = z'100003C'
C FDA40_logic_modes.fgi( 227, 436):��������� ������������� IN (�������)
      I_(145) = z'1000034'
C FDA40_logic_modes.fgi( 224, 467):��������� ������������� IN (�������)
      I_(146) = z'1000007'
C FDA40_logic_modes.fgi( 224, 469):��������� ������������� IN (�������)
      C30_afed = '���� ����������� ����������'
C FDA40_logic_modes.fgi( 233, 546):��������� ���������� CH20 (CH30) (�������)
      L_(283)=.true.
C FDA40_logic_modes.fgi( 177, 505):��������� ���������� (�������)
      if(L_ofed) then
         L_(284)=L_(283)
      else
         L_(284)=.false.
      endif
C FDA40_logic_modes.fgi( 181, 504):���� � ������������� �������
      L_(17)=L_(284)
C FDA40_logic_modes.fgi( 181, 504):������-�������: ���������� ��� �������������� ������
      L_ifed=(L_(17).or.L_ifed).and..not.(L_emof)
      L_(282)=.not.L_ifed
C FDA40_logic_modes.fgi( 210, 503):RS �������
      L_efed=L_ifed
C FDA40_logic_modes.fgi( 229, 505):������,20FDA40_PRESS_STOPED
      if(L_efed) then
         I_oded=I_(144)
      else
         I_oded=I_(143)
      endif
C FDA40_logic_modes.fgi( 230, 436):���� RE IN LO CH7
      L_(288)=.true.
C FDA40_logic_modes.fgi( 177, 536):��������� ���������� (�������)
      if(L_elof) then
         L_(287)=L_(288)
      else
         L_(287)=.false.
      endif
C FDA40_logic_modes.fgi( 181, 535):���� � ������������� �������
      L_(16)=L_(287)
C FDA40_logic_modes.fgi( 181, 535):������-�������: ���������� ��� �������������� ������
      I_(147) = z'01000003'
C FDA40_logic_modes.fgi( 166, 450):��������� ������������� IN (�������)
      I_(148) = z'100003C'
C FDA40_logic_modes.fgi( 166, 448):��������� ������������� IN (�������)
      C30_iked = '����������� ���� �������������'
C FDA40_logic_modes.fgi( 186, 552):��������� ���������� CH20 (CH30) (�������)
      C30_uked = '����������� ���� �����������'
C FDA40_logic_modes.fgi( 223, 551):��������� ���������� CH20 (CH30) (�������)
      C30_iled = '�� ���� ���� �� �����������'
C FDA40_logic_modes.fgi( 208, 554):��������� ���������� CH20 (CH30) (�������)
      I_(149) = z'01000003'
C FDA40_logic_modes.fgi( 227, 450):��������� ������������� IN (�������)
      I_(150) = z'100003C'
C FDA40_logic_modes.fgi( 227, 448):��������� ������������� IN (�������)
      L_(291)=.true.
C FDA40_logic_modes.fgi( 177, 576):��������� ���������� (�������)
      if(L_emof) then
         L_(290)=L_(291)
      else
         L_(290)=.false.
      endif
C FDA40_logic_modes.fgi( 181, 575):���� � ������������� �������
      L_(15)=L_(290)
C FDA40_logic_modes.fgi( 181, 575):������-�������: ���������� ��� �������������� ������
      L_(285) = L_ufed.OR.L_(15).OR.L_(17)
C FDA40_logic_modes.fgi( 199, 520):���
      L_aled=(L_(16).or.L_aled).and..not.(L_(285))
      L_(286)=.not.L_aled
C FDA40_logic_modes.fgi( 210, 534):RS �������
      L_oled=L_aled
C FDA40_logic_modes.fgi( 229, 536):������,20FDA40_PRESS_RUN
      if(L_oled) then
         I_amed=I_(150)
      else
         I_amed=I_(149)
      endif
C FDA40_logic_modes.fgi( 230, 448):���� RE IN LO CH7
      L_(296) = L_imed
C FDA40_logic_modes.fgi( 367, 364):�
      L_(298) = L_omed
C FDA40_logic_modes.fgi( 367, 399):�
      I_(151) = z'01000003'
C FDA40_logic_modes.fgi( 363, 353):��������� ������������� IN (�������)
      I_(152) = z'100003C'
C FDA40_logic_modes.fgi( 363, 351):��������� ������������� IN (�������)
      C30_eped = '���� �������'
C FDA40_logic_modes.fgi( 389, 378):��������� ���������� CH20 (CH30) (�������)
      C30_iped = '���� ����������'
C FDA40_logic_modes.fgi( 404, 377):��������� ���������� CH20 (CH30) (�������)
      C30_uped = '�� ������'
C FDA40_logic_modes.fgi( 389, 380):��������� ���������� CH20 (CH30) (�������)
      I_(153) = z'01000003'
C FDA40_logic_modes.fgi( 419, 352):��������� ������������� IN (�������)
      I_(154) = z'100003C'
C FDA40_logic_modes.fgi( 419, 350):��������� ������������� IN (�������)
      L_(300)=.true.
C FDA40_logic_modes.fgi( 369, 370):��������� ���������� (�������)
      if(L_(296)) then
         L_(297)=L_(300)
      else
         L_(297)=.false.
      endif
C FDA40_logic_modes.fgi( 373, 369):���� � ������������� �������
      L_(14)=L_(297)
C FDA40_logic_modes.fgi( 373, 369):������-�������: ���������� ��� �������������� ������
      L_(294) = L_(14)
C FDA40_logic_modes.fgi( 383, 404):���
      L_(301)=.true.
C FDA40_logic_modes.fgi( 369, 408):��������� ���������� (�������)
      if(L_(298)) then
         L_(299)=L_(301)
      else
         L_(299)=.false.
      endif
C FDA40_logic_modes.fgi( 373, 407):���� � ������������� �������
      L_(13)=L_(299)
C FDA40_logic_modes.fgi( 373, 407):������-�������: ���������� ��� �������������� ������
      L_(292) = L_(13)
C FDA40_logic_modes.fgi( 383, 366):���
      L_ired=(L_(14).or.L_ired).and..not.(L_(292))
      L_(293)=.not.L_ired
C FDA40_logic_modes.fgi( 391, 368):RS �������
      L_ebuf=L_ired
C FDA40_logic_modes.fgi( 407, 370):������,FDA40_BOX_avt_stoped
      if(L_ebuf) then
         I_ered=I_(154)
      else
         I_ered=I_(153)
      endif
C FDA40_logic_modes.fgi( 422, 350):���� RE IN LO CH7
      L_ored=(L_(13).or.L_ored).and..not.(L_(294))
      L_(295)=.not.L_ored
C FDA40_logic_modes.fgi( 391, 406):RS �������
      L_aped=L_ored
C FDA40_logic_modes.fgi( 410, 408):������,FDA40_BOX_avt_started
      if(L_aped.and..not.L0_ud) then
         R0_id=R0_od
      else
         R0_id=max(R_(3)-deltat,0.0)
      endif
      L_(204)=R0_id.gt.0.0
      L0_ud=L_aped
C FDA40_logic_box_step.fgi(  69, 770):������������  �� T
      if(L_aped) then
         I_umed=I_(152)
      else
         I_umed=I_(151)
      endif
C FDA40_logic_modes.fgi( 366, 351):���� RE IN LO CH7
      if(L_ored) then
         C30_oped=C30_eped
      else
         C30_oped=C30_uped
      endif
C FDA40_logic_modes.fgi( 393, 378):���� RE IN LO CH20
      if(L_ired) then
         C30_ared=C30_iped
      else
         C30_ared=C30_oped
      endif
C FDA40_logic_modes.fgi( 408, 377):���� RE IN LO CH20
      I_(155) = z'1000024'
C FDA40_logic_modes.fgi( 231,  60):��������� ������������� IN (�������)
      I_(156) = z'100004B'
C FDA40_logic_modes.fgi( 231,  58):��������� ������������� IN (�������)
      L_(302)=.false.
C FDA40_logic_modes.fgi( 172,  80):��������� ���������� (�������)
      L_(303)=.false.
C FDA40_logic_modes.fgi( 172, 125):��������� ���������� (�������)
      C30_osed = '������ ���'
C FDA40_logic_modes.fgi( 223,  93):��������� ���������� CH20 (CH30) (�������)
      C30_ised = '������ �'
C FDA40_logic_modes.fgi( 192,  94):��������� ���������� CH20 (CH30) (�������)
      C30_ated = '�� ������'
C FDA40_logic_modes.fgi( 192,  96):��������� ���������� CH20 (CH30) (�������)
      L_(308)=.true.
C FDA40_logic_modes.fgi( 172,  82):��������� ���������� (�������)
      if(L_esed) then
         L_(306)=L_(302)
      else
         L_(306)=L_(308)
      endif
C FDA40_logic_modes.fgi( 176,  80):���� RE IN LO CH20
      L_(309)=.true.
C FDA40_logic_modes.fgi( 172, 123):��������� ���������� (�������)
      if(L_esed) then
         L_(305)=L_(309)
      else
         L_(305)=L_(303)
      endif
C FDA40_logic_modes.fgi( 176, 123):���� RE IN LO CH20
      L_oted=(L_(305).or.L_oted).and..not.(L_(306))
      L_(307)=.not.L_oted
C FDA40_logic_modes.fgi( 194, 122):RS �������
      L_uted=L_oted
C FDA40_logic_modes.fgi( 216, 124):������,20FDA40_PR_WITH_AGIT
      if(L_oted) then
         C30_used=C30_ised
      else
         C30_used=C30_ated
      endif
C FDA40_logic_modes.fgi( 196,  94):���� RE IN LO CH20
      L_ited=(L_(306).or.L_ited).and..not.(L_(305))
      L_(304)=.not.L_ited
C FDA40_logic_modes.fgi( 194,  79):RS �������
      L_ased=L_ited
C FDA40_logic_modes.fgi( 218,  81):������,20FDA40_PR_WITHOUT_AGIT
      if(L_ased) then
         I_ured=I_(156)
      else
         I_ured=I_(155)
      endif
C FDA40_logic_modes.fgi( 234,  58):���� RE IN LO CH7
      if(L_ited) then
         C30_eted=C30_osed
      else
         C30_eted=C30_used
      endif
C FDA40_logic_modes.fgi( 227,  93):���� RE IN LO CH20
      I_(157) = z'1000024'
C FDA40_logic_modes.fgi( 169,  60):��������� ������������� IN (�������)
      I_(158) = z'100004B'
C FDA40_logic_modes.fgi( 169,  58):��������� ������������� IN (�������)
      if(L_uted) then
         I_aved=I_(158)
      else
         I_aved=I_(157)
      endif
C FDA40_logic_modes.fgi( 172,  58):���� RE IN LO CH7
      I_(159) = z'1000024'
C FDA40_logic_modes.fgi( 231, 152):��������� ������������� IN (�������)
      I_(160) = z'100004B'
C FDA40_logic_modes.fgi( 231, 150):��������� ������������� IN (�������)
      L_(310)=.false.
C FDA40_logic_modes.fgi( 172, 172):��������� ���������� (�������)
      L_(311)=.false.
C FDA40_logic_modes.fgi( 172, 217):��������� ���������� (�������)
      C30_axed = '������ ���'
C FDA40_logic_modes.fgi( 223, 185):��������� ���������� CH20 (CH30) (�������)
      C30_uved = '������ �'
C FDA40_logic_modes.fgi( 192, 186):��������� ���������� CH20 (CH30) (�������)
      C30_ixed = '�� ������'
C FDA40_logic_modes.fgi( 192, 188):��������� ���������� CH20 (CH30) (�������)
      L_(316)=.true.
C FDA40_logic_modes.fgi( 172, 174):��������� ���������� (�������)
      if(L_oved) then
         L_(314)=L_(310)
      else
         L_(314)=L_(316)
      endif
C FDA40_logic_modes.fgi( 176, 172):���� RE IN LO CH20
      L_(317)=.true.
C FDA40_logic_modes.fgi( 172, 215):��������� ���������� (�������)
      if(L_oved) then
         L_(313)=L_(317)
      else
         L_(313)=L_(311)
      endif
C FDA40_logic_modes.fgi( 176, 215):���� RE IN LO CH20
      L_abid=(L_(313).or.L_abid).and..not.(L_(314))
      L_(315)=.not.L_abid
C FDA40_logic_modes.fgi( 194, 214):RS �������
      L_ebid=L_abid
C FDA40_logic_modes.fgi( 216, 216):������,20FDA40_PR_WITH_DUST
      if(L_abid) then
         C30_exed=C30_uved
      else
         C30_exed=C30_ixed
      endif
C FDA40_logic_modes.fgi( 196, 186):���� RE IN LO CH20
      L_uxed=(L_(314).or.L_uxed).and..not.(L_(313))
      L_(312)=.not.L_uxed
C FDA40_logic_modes.fgi( 194, 171):RS �������
      L_ived=L_uxed
C FDA40_logic_modes.fgi( 218, 173):������,20FDA40_PR_WITHOUT_DUST
      if(L_ived) then
         I_eved=I_(160)
      else
         I_eved=I_(159)
      endif
C FDA40_logic_modes.fgi( 234, 150):���� RE IN LO CH7
      if(L_uxed) then
         C30_oxed=C30_axed
      else
         C30_oxed=C30_exed
      endif
C FDA40_logic_modes.fgi( 227, 185):���� RE IN LO CH20
      I_(161) = z'1000024'
C FDA40_logic_modes.fgi( 169, 152):��������� ������������� IN (�������)
      I_(162) = z'100004B'
C FDA40_logic_modes.fgi( 169, 150):��������� ������������� IN (�������)
      if(L_ebid) then
         I_ibid=I_(162)
      else
         I_ibid=I_(161)
      endif
C FDA40_logic_modes.fgi( 172, 150):���� RE IN LO CH7
      I_(163) = z'1000024'
C FDA40_logic_modes.fgi( 231, 244):��������� ������������� IN (�������)
      I_(164) = z'100004B'
C FDA40_logic_modes.fgi( 231, 242):��������� ������������� IN (�������)
      L_(318)=.false.
C FDA40_logic_modes.fgi( 172, 264):��������� ���������� (�������)
      L_(319)=.false.
C FDA40_logic_modes.fgi( 172, 309):��������� ���������� (�������)
      C30_idid = '������ ���'
C FDA40_logic_modes.fgi( 223, 277):��������� ���������� CH20 (CH30) (�������)
      C30_edid = '������ �'
C FDA40_logic_modes.fgi( 192, 278):��������� ���������� CH20 (CH30) (�������)
      C30_udid = '�� ������'
C FDA40_logic_modes.fgi( 192, 280):��������� ���������� CH20 (CH30) (�������)
      L_(324)=.true.
C FDA40_logic_modes.fgi( 172, 266):��������� ���������� (�������)
      if(L_adid) then
         L_(322)=L_(318)
      else
         L_(322)=L_(324)
      endif
C FDA40_logic_modes.fgi( 176, 264):���� RE IN LO CH20
      L_(325)=.true.
C FDA40_logic_modes.fgi( 172, 307):��������� ���������� (�������)
      if(L_adid) then
         L_(321)=L_(325)
      else
         L_(321)=L_(319)
      endif
C FDA40_logic_modes.fgi( 176, 307):���� RE IN LO CH20
      L_ifid=(L_(321).or.L_ifid).and..not.(L_(322))
      L_(323)=.not.L_ifid
C FDA40_logic_modes.fgi( 194, 306):RS �������
      L_ofid=L_ifid
C FDA40_logic_modes.fgi( 216, 308):������,20FDA40_PR_WITH_VIBRO
      if(L_ifid) then
         C30_odid=C30_edid
      else
         C30_odid=C30_udid
      endif
C FDA40_logic_modes.fgi( 196, 278):���� RE IN LO CH20
      L_efid=(L_(322).or.L_efid).and..not.(L_(321))
      L_(320)=.not.L_efid
C FDA40_logic_modes.fgi( 194, 263):RS �������
      L_ubid=L_efid
C FDA40_logic_modes.fgi( 218, 265):������,20FDA40_PR_WITHOUT_VIBRO
      if(L_ubid) then
         I_obid=I_(164)
      else
         I_obid=I_(163)
      endif
C FDA40_logic_modes.fgi( 234, 242):���� RE IN LO CH7
      if(L_efid) then
         C30_afid=C30_idid
      else
         C30_afid=C30_odid
      endif
C FDA40_logic_modes.fgi( 227, 277):���� RE IN LO CH20
      I_(165) = z'1000024'
C FDA40_logic_modes.fgi( 169, 244):��������� ������������� IN (�������)
      I_(166) = z'100004B'
C FDA40_logic_modes.fgi( 169, 242):��������� ������������� IN (�������)
      if(L_ofid) then
         I_ufid=I_(166)
      else
         I_ufid=I_(165)
      endif
C FDA40_logic_modes.fgi( 172, 242):���� RE IN LO CH7
      I_(167) = z'1000024'
C FDA40_logic_modes.fgi( 231, 337):��������� ������������� IN (�������)
      I_(168) = z'100004B'
C FDA40_logic_modes.fgi( 231, 335):��������� ������������� IN (�������)
      L_(326)=.false.
C FDA40_logic_modes.fgi( 172, 357):��������� ���������� (�������)
      L_(327)=.false.
C FDA40_logic_modes.fgi( 172, 402):��������� ���������� (�������)
      C30_ukid = '������ ���'
C FDA40_logic_modes.fgi( 223, 370):��������� ���������� CH20 (CH30) (�������)
      C30_okid = '������ �'
C FDA40_logic_modes.fgi( 192, 371):��������� ���������� CH20 (CH30) (�������)
      C30_elid = '�� ������'
C FDA40_logic_modes.fgi( 192, 373):��������� ���������� CH20 (CH30) (�������)
      L_(332)=.true.
C FDA40_logic_modes.fgi( 172, 359):��������� ���������� (�������)
      if(L_ikid) then
         L_(330)=L_(326)
      else
         L_(330)=L_(332)
      endif
C FDA40_logic_modes.fgi( 176, 357):���� RE IN LO CH20
      L_(333)=.true.
C FDA40_logic_modes.fgi( 172, 400):��������� ���������� (�������)
      if(L_ikid) then
         L_(329)=L_(333)
      else
         L_(329)=L_(327)
      endif
C FDA40_logic_modes.fgi( 176, 400):���� RE IN LO CH20
      L_ulid=(L_(329).or.L_ulid).and..not.(L_(330))
      L_(331)=.not.L_ulid
C FDA40_logic_modes.fgi( 194, 399):RS �������
      L_amid=L_ulid
C FDA40_logic_modes.fgi( 216, 401):������,20FDA40_PR_WITH_SAFE_PUNCH
      if(L_ulid) then
         C30_alid=C30_okid
      else
         C30_alid=C30_elid
      endif
C FDA40_logic_modes.fgi( 196, 371):���� RE IN LO CH20
      L_olid=(L_(330).or.L_olid).and..not.(L_(329))
      L_(328)=.not.L_olid
C FDA40_logic_modes.fgi( 194, 356):RS �������
      L_ekid=L_olid
C FDA40_logic_modes.fgi( 218, 358):������,20FDA40_PR_WITHOUT_SAFE_PUNCH
      if(L_ekid) then
         I_akid=I_(168)
      else
         I_akid=I_(167)
      endif
C FDA40_logic_modes.fgi( 234, 335):���� RE IN LO CH7
      if(L_olid) then
         C30_ilid=C30_ukid
      else
         C30_ilid=C30_alid
      endif
C FDA40_logic_modes.fgi( 227, 370):���� RE IN LO CH20
      I_(169) = z'1000024'
C FDA40_logic_modes.fgi( 169, 337):��������� ������������� IN (�������)
      I_(170) = z'100004B'
C FDA40_logic_modes.fgi( 169, 335):��������� ������������� IN (�������)
      if(L_amid) then
         I_emid=I_(170)
      else
         I_emid=I_(169)
      endif
C FDA40_logic_modes.fgi( 172, 335):���� RE IN LO CH7
      I_(171) = z'1000024'
C FDA40_logic_modes.fgi(  99, 179):��������� ������������� IN (�������)
      I_(172) = z'100004B'
C FDA40_logic_modes.fgi(  99, 177):��������� ������������� IN (�������)
      L_(334)=.false.
C FDA40_logic_modes.fgi(  40, 199):��������� ���������� (�������)
      L_(335)=.false.
C FDA40_logic_modes.fgi(  40, 244):��������� ���������� (�������)
      C30_epid = '������� �����'
C FDA40_logic_modes.fgi(  91, 212):��������� ���������� CH20 (CH30) (�������)
      C30_apid = '����������� �����'
C FDA40_logic_modes.fgi(  60, 213):��������� ���������� CH20 (CH30) (�������)
      C30_opid = '�� ������'
C FDA40_logic_modes.fgi(  60, 215):��������� ���������� CH20 (CH30) (�������)
      L_(340)=.true.
C FDA40_logic_modes.fgi(  40, 201):��������� ���������� (�������)
      if(L_umid) then
         L_(338)=L_(334)
      else
         L_(338)=L_(340)
      endif
C FDA40_logic_modes.fgi(  44, 199):���� RE IN LO CH20
      L_(341)=.true.
C FDA40_logic_modes.fgi(  40, 242):��������� ���������� (�������)
      if(L_umid) then
         L_(337)=L_(341)
      else
         L_(337)=L_(335)
      endif
C FDA40_logic_modes.fgi(  44, 242):���� RE IN LO CH20
      L_erid=(L_(337).or.L_erid).and..not.(L_(338))
      L_(339)=.not.L_erid
C FDA40_logic_modes.fgi(  62, 241):RS �������
      L_adak=L_erid
C FDA40_logic_modes.fgi(  88, 243):������,20FDA40EC004_POS_MODE_WORK
      L_(627)=L_adak.and..not.L0_ubak
      L0_ubak=L_adak
C FDA40_logic_press_1.fgi( 165,  79):������������  �� 1 ���
      if(L_erid) then
         C30_ipid=C30_apid
      else
         C30_ipid=C30_opid
      endif
C FDA40_logic_modes.fgi(  64, 213):���� RE IN LO CH20
      L_arid=(L_(338).or.L_arid).and..not.(L_(337))
      L_(336)=.not.L_arid
C FDA40_logic_modes.fgi(  62, 198):RS �������
      L_omid=L_arid
C FDA40_logic_modes.fgi(  90, 200):������,20FDA40EC004_POW_MODE_WORK
      if(L_omid) then
         I_imid=I_(172)
      else
         I_imid=I_(171)
      endif
C FDA40_logic_modes.fgi( 102, 177):���� RE IN LO CH7
      if(L_arid) then
         C30_upid=C30_epid
      else
         C30_upid=C30_ipid
      endif
C FDA40_logic_modes.fgi(  95, 212):���� RE IN LO CH20
      I_(173) = z'1000024'
C FDA40_logic_modes.fgi(  37, 179):��������� ������������� IN (�������)
      I_(174) = z'100004B'
C FDA40_logic_modes.fgi(  37, 177):��������� ������������� IN (�������)
      if(L_adak) then
         I_irid=I_(174)
      else
         I_irid=I_(173)
      endif
C FDA40_logic_modes.fgi(  40, 177):���� RE IN LO CH7
      L_(353) = L_asid.OR.L_orid.OR.L_urid
C FDA40_logic_modes.fgi( 366, 513):���
      L_(355) = L_isid.AND.(.NOT.L_asid)
C FDA40_logic_modes.fgi( 367, 532):�
      L_(357) = L_osid.AND.(.NOT.L_asid).AND.(.NOT.L_orid
     &)
C FDA40_logic_modes.fgi( 367, 567):�
      L_(342) = L_esid.AND.(.NOT.L_asid).AND.(.NOT.L_urid
     &)
C FDA40_logic_modes.fgi( 367, 480):�
      I_(175) = z'01000003'
C FDA40_logic_modes.fgi( 390, 475):��������� ������������� IN (�������)
      I_(176) = z'100003C'
C FDA40_logic_modes.fgi( 390, 473):��������� ������������� IN (�������)
      C30_utid = '������������������'
C FDA40_logic_modes.fgi( 389, 546):��������� ���������� CH20 (CH30) (�������)
      L_(343)=.true.
C FDA40_logic_modes.fgi( 369, 489):��������� ���������� (�������)
      if(L_(342)) then
         L_(350)=L_(343)
      else
         L_(350)=.false.
      endif
C FDA40_logic_modes.fgi( 373, 488):���� � ������������� �������
      L_(12)=L_(350)
C FDA40_logic_modes.fgi( 373, 488):������-�������: ���������� ��� �������������� ������
      I_(177) = z'01000003'
C FDA40_logic_modes.fgi( 386, 464):��������� ������������� IN (�������)
      I_(178) = z'100003C'
C FDA40_logic_modes.fgi( 386, 462):��������� ������������� IN (�������)
      C30_evid = '������������'
C FDA40_logic_modes.fgi( 410, 550):��������� ���������� CH20 (CH30) (�������)
      C30_ivid = '����� ������� ����������'
C FDA40_logic_modes.fgi( 404, 545):��������� ���������� CH20 (CH30) (�������)
      C30_axid = '��������������'
C FDA40_logic_modes.fgi( 421, 556):��������� ���������� CH20 (CH30) (�������)
      C30_exid = '�� ������'
C FDA40_logic_modes.fgi( 389, 548):��������� ���������� CH20 (CH30) (�������)
      I_(179) = z'01000003'
C FDA40_logic_modes.fgi( 428, 475):��������� ������������� IN (�������)
      I_(180) = z'100003C'
C FDA40_logic_modes.fgi( 428, 473):��������� ������������� IN (�������)
      L_(359)=.true.
C FDA40_logic_modes.fgi( 369, 523):��������� ���������� (�������)
      if(L_(353)) then
         L_(354)=L_(359)
      else
         L_(354)=.false.
      endif
C FDA40_logic_modes.fgi( 373, 522):���� � ������������� �������
      L_(11)=L_(354)
C FDA40_logic_modes.fgi( 373, 522):������-�������: ���������� ��� �������������� ������
      L_(360)=.true.
C FDA40_logic_modes.fgi( 369, 538):��������� ���������� (�������)
      if(L_(355)) then
         L_(356)=L_(360)
      else
         L_(356)=.false.
      endif
C FDA40_logic_modes.fgi( 373, 537):���� � ������������� �������
      L_(10)=L_(356)
C FDA40_logic_modes.fgi( 373, 537):������-�������: ���������� ��� �������������� ������
      L_(351) = L_(10).OR.L_(11).OR.L_(12)
C FDA40_logic_modes.fgi( 383, 572):���
      L_(361)=.true.
C FDA40_logic_modes.fgi( 369, 576):��������� ���������� (�������)
      if(L_(357)) then
         L_(358)=L_(361)
      else
         L_(358)=.false.
      endif
C FDA40_logic_modes.fgi( 373, 575):���� � ������������� �������
      L_(9)=L_(358)
C FDA40_logic_modes.fgi( 373, 575):������-�������: ���������� ��� �������������� ������
      L_(346) = L_(9).OR.L_(10).OR.L_(12)
C FDA40_logic_modes.fgi( 383, 519):���
      L_abod=(L_(11).or.L_abod).and..not.(L_(346))
      L_(347)=.not.L_abod
C FDA40_logic_modes.fgi( 391, 521):RS �������
      L_itid=L_abod
C FDA40_logic_modes.fgi( 407, 523):������,FDA40_BOX_stop_mode
      L_(344) = L_(9).OR.L_(11).OR.L_(10)
C FDA40_logic_modes.fgi( 383, 485):���
      L_atid=(L_(12).or.L_atid).and..not.(L_(344))
      L_(345)=.not.L_atid
C FDA40_logic_modes.fgi( 391, 487):RS �������
      L_obuf=L_atid
C FDA40_logic_modes.fgi( 407, 489):������,FDA40_BOX_avt_mode
      L_ubuf = L_obuf.AND.L_oduf.AND.L_iduf.AND.L_eduf.AND.L_aduf.AND.L_
     &ibuf.AND.(.NOT.L_ebuf).AND.L_abuf
C FDA40_logic_press_step.fgi(  75, 768):�
      L_(209) = L_(204).AND.L_ubuf
C FDA40_logic_box_step.fgi(  79, 769):�
      if (.not.L_oso.and.(L_uso.or.L_(209))) then
          I_(30) = 0
          L_oso = .true.
          L_(208) = .true.
      endif
      if (I_(30) .gt. 0) then
         L_(208) = .false.
      endif
      if(L_ebuf)then
          I_(30) = 0
          L_oso = .false.
          L_(208) = .false.
      endif
C FDA40_logic_box_step.fgi( 124, 756):���������� ������� ���������,FDA40_BOX_AVT
      if(L_obuf) then
         I_usid=I_(176)
      else
         I_usid=I_(175)
      endif
C FDA40_logic_modes.fgi( 393, 473):���� RE IN LO CH7
      L_(348) = L_(9).OR.L_(11).OR.L_(12)
C FDA40_logic_modes.fgi( 383, 534):���
      L_ebod=(L_(10).or.L_ebod).and..not.(L_(348))
      L_(349)=.not.L_ebod
C FDA40_logic_modes.fgi( 391, 536):RS �������
      L_ixid=L_ebod
C FDA40_logic_modes.fgi( 407, 538):������,FDA40_BOX_ruch_mode
      if(L_ixid) then
         I_uxid=I_(180)
      else
         I_uxid=I_(179)
      endif
C FDA40_logic_modes.fgi( 431, 473):���� RE IN LO CH7
      L_ibod=(L_(9).or.L_ibod).and..not.(L_(351))
      L_(352)=.not.L_ibod
C FDA40_logic_modes.fgi( 391, 574):RS �������
      L_otid=L_ibod
C FDA40_logic_modes.fgi( 410, 576):������,FDA40_BOX_auto_mode
      if(L_otid) then
         I_etid=I_(178)
      else
         I_etid=I_(177)
      endif
C FDA40_logic_modes.fgi( 389, 462):���� RE IN LO CH7
      if(L_ibod) then
         C30_uvid=C30_utid
      else
         C30_uvid=C30_exid
      endif
C FDA40_logic_modes.fgi( 393, 546):���� RE IN LO CH20
      if(L_ebod) then
         C30_ovid=C30_ivid
      else
         C30_ovid=C30_uvid
      endif
C FDA40_logic_modes.fgi( 408, 545):���� RE IN LO CH20
      if(L_abod) then
         C30_avid=C30_evid
      else
         C30_avid=C30_ovid
      endif
C FDA40_logic_modes.fgi( 417, 544):���� RE IN LO CH20
      if(L_atid) then
         C30_oxid=C30_axid
      else
         C30_oxid=C30_avid
      endif
C FDA40_logic_modes.fgi( 427, 543):���� RE IN LO CH20
      I_(181) = z'1000024'
C FDA40_logic_modes.fgi(  99, 272):��������� ������������� IN (�������)
      I_(182) = z'100004B'
C FDA40_logic_modes.fgi(  99, 270):��������� ������������� IN (�������)
      L_(362)=.false.
C FDA40_logic_modes.fgi(  40, 292):��������� ���������� (�������)
      L_(363)=.false.
C FDA40_logic_modes.fgi(  40, 337):��������� ���������� (�������)
      C30_edod = '������ ��� �������'
C FDA40_logic_modes.fgi(  91, 305):��������� ���������� CH20 (CH30) (�������)
      C30_adod = '������ � ��������'
C FDA40_logic_modes.fgi(  60, 306):��������� ���������� CH20 (CH30) (�������)
      C30_odod = '�� ������'
C FDA40_logic_modes.fgi(  60, 308):��������� ���������� CH20 (CH30) (�������)
      L_(368)=.true.
C FDA40_logic_modes.fgi(  40, 294):��������� ���������� (�������)
      if(L_oxaf) then
         L_(366)=L_(362)
      else
         L_(366)=L_(368)
      endif
C FDA40_logic_modes.fgi(  44, 292):���� RE IN LO CH20
      L_(369)=.true.
C FDA40_logic_modes.fgi(  40, 335):��������� ���������� (�������)
      if(L_oxaf) then
         L_(365)=L_(369)
      else
         L_(365)=L_(363)
      endif
C FDA40_logic_modes.fgi(  44, 335):���� RE IN LO CH20
      L_efod=(L_(365).or.L_efod).and..not.(L_(366))
      L_(367)=.not.L_efod
C FDA40_logic_modes.fgi(  62, 334):RS �������
      L_evuf=L_efod
C FDA40_logic_modes.fgi(  88, 336):������,20FDA40_SVBU_WITH_MATRIX_CONTROL
      L_(621) = L_adak.AND.L_evuf
C FDA40_logic_press_1.fgi(  72,  31):�
      L_(620)=L_(621).and..not.L0_etuf
      L0_etuf=L_(621)
C FDA40_logic_press_1.fgi( 132,  47):������������  �� 1 ���
      if(L_efod) then
         C30_idod=C30_adod
      else
         C30_idod=C30_odod
      endif
C FDA40_logic_modes.fgi(  64, 306):���� RE IN LO CH20
      L_afod=(L_(366).or.L_afod).and..not.(L_(365))
      L_(364)=.not.L_afod
C FDA40_logic_modes.fgi(  62, 291):RS �������
      L_ubod=L_afod
C FDA40_logic_modes.fgi(  90, 293):������,20FDA40_SVBU_WITHOUT_MATRIX_CONTROL
      if(L_ubod) then
         I_obod=I_(182)
      else
         I_obod=I_(181)
      endif
C FDA40_logic_modes.fgi( 102, 270):���� RE IN LO CH7
      if(L_afod) then
         C30_udod=C30_edod
      else
         C30_udod=C30_idod
      endif
C FDA40_logic_modes.fgi(  95, 305):���� RE IN LO CH20
      I_(183) = z'1000024'
C FDA40_logic_modes.fgi(  37, 272):��������� ������������� IN (�������)
      I_(184) = z'100004B'
C FDA40_logic_modes.fgi(  37, 270):��������� ������������� IN (�������)
      if(L_evuf) then
         I_ifod=I_(184)
      else
         I_ifod=I_(183)
      endif
C FDA40_logic_modes.fgi(  40, 270):���� RE IN LO CH7
      I_(185) = z'1000024'
C FDA40_logic_modes.fgi(  99, 365):��������� ������������� IN (�������)
      I_(186) = z'100004B'
C FDA40_logic_modes.fgi(  99, 363):��������� ������������� IN (�������)
      L_(370)=.false.
C FDA40_logic_modes.fgi(  40, 385):��������� ���������� (�������)
      L_(371)=.false.
C FDA40_logic_modes.fgi(  40, 430):��������� ���������� (�������)
      C30_ekod = '������ ��� �������'
C FDA40_logic_modes.fgi(  91, 398):��������� ���������� CH20 (CH30) (�������)
      C30_akod = '������ � ��������'
C FDA40_logic_modes.fgi(  60, 399):��������� ���������� CH20 (CH30) (�������)
      C30_okod = '�� ������'
C FDA40_logic_modes.fgi(  60, 401):��������� ���������� CH20 (CH30) (�������)
      L_(376)=.true.
C FDA40_logic_modes.fgi(  40, 387):��������� ���������� (�������)
      if(L_ufod) then
         L_(374)=L_(370)
      else
         L_(374)=L_(376)
      endif
C FDA40_logic_modes.fgi(  44, 385):���� RE IN LO CH20
      L_(377)=.true.
C FDA40_logic_modes.fgi(  40, 428):��������� ���������� (�������)
      if(L_ufod) then
         L_(373)=L_(377)
      else
         L_(373)=L_(371)
      endif
C FDA40_logic_modes.fgi(  44, 428):���� RE IN LO CH20
      L_elod=(L_(373).or.L_elod).and..not.(L_(374))
      L_(375)=.not.L_elod
C FDA40_logic_modes.fgi(  62, 427):RS �������
      L_afuf=L_elod
C FDA40_logic_modes.fgi(  84, 429):������,20FDA40_PRESS_WITH_BOOT
      L_(378) = L_amod.AND.(.NOT.L_ulod).AND.(.NOT.L_olod
     &).AND.L_afuf
C FDA40_logic_modes.fgi(  37, 479):�
      if(L_elod) then
         C30_ikod=C30_akod
      else
         C30_ikod=C30_okod
      endif
C FDA40_logic_modes.fgi(  64, 399):���� RE IN LO CH20
      L_alod=(L_(374).or.L_alod).and..not.(L_(373))
      L_(372)=.not.L_alod
C FDA40_logic_modes.fgi(  62, 384):RS �������
      L_uduf=L_alod
C FDA40_logic_modes.fgi(  84, 386):������,20FDA40_PRESS_WITHOUT_BOOT
      if(L_uduf) then
         I_ofod=I_(186)
      else
         I_ofod=I_(185)
      endif
C FDA40_logic_modes.fgi( 102, 363):���� RE IN LO CH7
      if(L_uduf) then
         C30_axad=C30_ovad
      else
         C30_axad=C30_uvad
      endif
C FDA40_logic_parameters.fgi( 114, 507):���� RE IN LO CH20
      if(L_alod) then
         C30_ukod=C30_ekod
      else
         C30_ukod=C30_ikod
      endif
C FDA40_logic_modes.fgi(  95, 398):���� RE IN LO CH20
      L_(389) = L_ulod.OR.L_ilod.OR.L_olod
C FDA40_logic_modes.fgi(  36, 513):���
      L_(391) = L_emod.AND.(.NOT.L_ulod)
C FDA40_logic_modes.fgi(  37, 532):�
      L_(393) = L_imod.AND.(.NOT.L_ulod).AND.(.NOT.L_ilod
     &)
C FDA40_logic_modes.fgi(  37, 567):�
      I_(187) = z'01000003'
C FDA40_logic_modes.fgi(  60, 475):��������� ������������� IN (�������)
      I_(188) = z'100003C'
C FDA40_logic_modes.fgi(  60, 473):��������� ������������� IN (�������)
      C30_opod = '������������������'
C FDA40_logic_modes.fgi(  59, 546):��������� ���������� CH20 (CH30) (�������)
      L_(379)=.true.
C FDA40_logic_modes.fgi(  39, 489):��������� ���������� (�������)
      if(L_(378)) then
         L_(386)=L_(379)
      else
         L_(386)=.false.
      endif
C FDA40_logic_modes.fgi(  43, 488):���� � ������������� �������
      L_(8)=L_(386)
C FDA40_logic_modes.fgi(  43, 488):������-�������: ���������� ��� �������������� ������
      I_(189) = z'1000024'
C FDA40_logic_modes.fgi(  37, 365):��������� ������������� IN (�������)
      I_(190) = z'100004B'
C FDA40_logic_modes.fgi(  37, 363):��������� ������������� IN (�������)
      if(L_afuf) then
         I_apod=I_(190)
      else
         I_apod=I_(189)
      endif
C FDA40_logic_modes.fgi(  40, 363):���� RE IN LO CH7
      I_(191) = z'01000003'
C FDA40_logic_modes.fgi(  56, 464):��������� ������������� IN (�������)
      I_(192) = z'100003C'
C FDA40_logic_modes.fgi(  56, 462):��������� ������������� IN (�������)
      C30_arod = '������������'
C FDA40_logic_modes.fgi(  80, 550):��������� ���������� CH20 (CH30) (�������)
      C30_erod = '����� ������� ����������'
C FDA40_logic_modes.fgi(  74, 545):��������� ���������� CH20 (CH30) (�������)
      C30_urod = '��������������'
C FDA40_logic_modes.fgi(  91, 556):��������� ���������� CH20 (CH30) (�������)
      C30_asod = '�� ������'
C FDA40_logic_modes.fgi(  59, 548):��������� ���������� CH20 (CH30) (�������)
      I_(193) = z'01000003'
C FDA40_logic_modes.fgi(  98, 475):��������� ������������� IN (�������)
      I_(194) = z'100003C'
C FDA40_logic_modes.fgi(  98, 473):��������� ������������� IN (�������)
      L_(395)=.true.
C FDA40_logic_modes.fgi(  39, 523):��������� ���������� (�������)
      if(L_(389)) then
         L_(390)=L_(395)
      else
         L_(390)=.false.
      endif
C FDA40_logic_modes.fgi(  43, 522):���� � ������������� �������
      L_(7)=L_(390)
C FDA40_logic_modes.fgi(  43, 522):������-�������: ���������� ��� �������������� ������
      L_(396)=.true.
C FDA40_logic_modes.fgi(  39, 538):��������� ���������� (�������)
      if(L_(391)) then
         L_(392)=L_(396)
      else
         L_(392)=.false.
      endif
C FDA40_logic_modes.fgi(  43, 537):���� � ������������� �������
      L_(6)=L_(392)
C FDA40_logic_modes.fgi(  43, 537):������-�������: ���������� ��� �������������� ������
      L_(387) = L_(6).OR.L_(7).OR.L_(8)
C FDA40_logic_modes.fgi(  53, 572):���
      L_(397)=.true.
C FDA40_logic_modes.fgi(  39, 576):��������� ���������� (�������)
      if(L_(393)) then
         L_(394)=L_(397)
      else
         L_(394)=.false.
      endif
C FDA40_logic_modes.fgi(  43, 575):���� � ������������� �������
      L_(5)=L_(394)
C FDA40_logic_modes.fgi(  43, 575):������-�������: ���������� ��� �������������� ������
      L_(382) = L_(5).OR.L_(6).OR.L_(8)
C FDA40_logic_modes.fgi(  53, 519):���
      L_usod=(L_(7).or.L_usod).and..not.(L_(382))
      L_(383)=.not.L_usod
C FDA40_logic_modes.fgi(  61, 521):RS �������
      L_ipod=L_usod
C FDA40_logic_modes.fgi(  77, 523):������,FDA40_PR_stop_mode
      L_(380) = L_(5).OR.L_(7).OR.L_(6)
C FDA40_logic_modes.fgi(  53, 485):���
      L_umod=(L_(8).or.L_umod).and..not.(L_(380))
      L_(381)=.not.L_umod
C FDA40_logic_modes.fgi(  61, 487):RS �������
      L_eval=L_umod
C FDA40_logic_modes.fgi(  77, 489):������,FDA40_PR_avt_mode
      if(L_eval) then
         I_omod=I_(188)
      else
         I_omod=I_(187)
      endif
C FDA40_logic_modes.fgi(  63, 473):���� RE IN LO CH7
      L_(384) = L_(5).OR.L_(7).OR.L_(8)
C FDA40_logic_modes.fgi(  53, 534):���
      L_atod=(L_(6).or.L_atod).and..not.(L_(384))
      L_(385)=.not.L_atod
C FDA40_logic_modes.fgi(  61, 536):RS �������
      L_esod=L_atod
C FDA40_logic_modes.fgi(  77, 538):������,FDA40_PR_ruch_mode
      if(L_esod) then
         I_osod=I_(194)
      else
         I_osod=I_(193)
      endif
C FDA40_logic_modes.fgi( 101, 473):���� RE IN LO CH7
      L_etod=(L_(5).or.L_etod).and..not.(L_(387))
      L_(388)=.not.L_etod
C FDA40_logic_modes.fgi(  61, 574):RS �������
      L_ival=L_etod
C FDA40_logic_modes.fgi(  80, 576):������,FDA40_PR_auto_mode
      L_(668) = L_ival.OR.L_eval
C FDA40_vlv.fgi( 208, 126):���
      L_(595) = L_eval.OR.L_ival
C FDA40_logic_press_step.fgi( 361, 963):���
      L_(608) = L_eval.OR.L_ival
C FDA40_logic_press_step.fgi( 167, 963):���
      if(L_ival) then
         I_epod=I_(192)
      else
         I_epod=I_(191)
      endif
C FDA40_logic_modes.fgi(  59, 462):���� RE IN LO CH7
      if(L_etod) then
         C30_orod=C30_opod
      else
         C30_orod=C30_asod
      endif
C FDA40_logic_modes.fgi(  63, 546):���� RE IN LO CH20
      if(L_atod) then
         C30_irod=C30_erod
      else
         C30_irod=C30_orod
      endif
C FDA40_logic_modes.fgi(  78, 545):���� RE IN LO CH20
      if(L_usod) then
         C30_upod=C30_arod
      else
         C30_upod=C30_irod
      endif
C FDA40_logic_modes.fgi(  87, 544):���� RE IN LO CH20
      if(L_umod) then
         C30_isod=C30_urod
      else
         C30_isod=C30_upod
      endif
C FDA40_logic_modes.fgi(  97, 543):���� RE IN LO CH20
      R_(80) = 1
C FDA40_vent_log.fgi( 124, 234):��������� (RE4) (�������)
      R_amip = R8_itod * R_(80)
C FDA40_vent_log.fgi( 127, 236):����������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ukip,R_opip,REAL(1,4)
     &,
     & REAL(R_ilip,4),REAL(R_olip,4),
     & REAL(R_okip,4),REAL(R_ikip,4),I_ipip,
     & REAL(R_emip,4),L_imip,REAL(R_omip,4),L_umip,L_apip
     &,R_ulip,
     & REAL(R_elip,4),REAL(R_alip,4),L_epip,REAL(R_amip,4
     &))
      !}
C FDA40_vlv.fgi( 291,  51):���������� �������,20FDA40DF001ZQ01
      !��������� R_(109) = FDA40_vent_logC?? /4000/
      R_(109)=R0_otod
C FDA40_vent_log.fgi( 129, 214):���������
      !��������� R_(111) = FDA40_vent_logC?? /4000/
      R_(111)=R0_utod
C FDA40_vent_log.fgi( 129, 224):���������
      !��������� R_(81) = FDA40_vent_logC?? /1.0/
      R_(81)=R0_avod
C FDA40_vent_log.fgi( 219, 269):���������
      R8_evod = R_(81) + (-R8_ivod)
C FDA40_vent_log.fgi( 222, 268):��������
      R_(82) = 1e-3
C FDA40_vent_log.fgi(  48, 190):��������� (RE4) (�������)
      R_(83) = 1e-3
C FDA40_vent_log.fgi(  48, 200):��������� (RE4) (�������)
      R_(84) = 1e-6
C FDA40_vent_log.fgi(  48, 210):��������� (RE4) (�������)
      !��������� R_(85) = FDA40_vent_logC?? /0.0005/
      R_(85)=R0_ovod
C FDA40_vent_log.fgi( 151,  49):���������
      L_(398)=R8_uvod.gt.R_(85)
C FDA40_vent_log.fgi( 155,  50):���������� >
      !��������� R_(87) = FDA40_vent_logC?? /0.0/
      R_(87)=R0_axod
C FDA40_vent_log.fgi( 161,  56):���������
      !��������� R_(86) = FDA40_vent_logC?? /500/
      R_(86)=R0_exod
C FDA40_vent_log.fgi( 161,  54):���������
      if(L_(398)) then
         R_(95)=R_(86)
      else
         R_(95)=R_(87)
      endif
C FDA40_vent_log.fgi( 164,  54):���� RE IN LO CH7
      R_(91) = 1.0
C FDA40_vent_log.fgi( 159,  94):��������� (RE4) (�������)
      R_(92) = 0.0
C FDA40_vent_log.fgi( 159,  96):��������� (RE4) (�������)
      !��������� R_(89) = FDA40_vent_logC?? /1.3/
      R_(89)=R0_uxod
C FDA40_vent_log.fgi( 154, 115):���������
      !��������� R_(93) = FDA40_vent_logC?? /1/
      R_(93)=R0_adud
C FDA40_vent_log.fgi( 160,  66):���������
      L_(402)=R8_edud.lt.R_(93)
C FDA40_vent_log.fgi( 164,  67):���������� <
      L_odud=L_idud.or.(L_odud.and..not.(L_(402)))
      L_(403)=.not.L_odud
C FDA40_vent_log.fgi( 194,  69):RS �������
      L_afud=L_odud
C FDA40_vent_log.fgi( 208,  71):������,20FDA42CW001_OUT
      !��������� R_(94) = FDA40_vent_logC?? /-50000/
      R_(94)=R0_udud
C FDA40_vent_log.fgi( 189,  53):���������
      if(L_afud) then
         R8_efud=R_(94)
      else
         R8_efud=R_(95)
      endif
C FDA40_vent_log.fgi( 192,  53):���� RE IN LO CH7
      R_ekop = R8_ifud + (-R8_erud)
C FDA40_vent_log.fgi( 303,  54):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_afop,R_ulop,REAL(-1,4
     &),
     & REAL(R_ofop,4),REAL(R_ufop,4),
     & REAL(R_udop,4),REAL(R_odop,4),I_olop,
     & REAL(R_ikop,4),L_okop,REAL(R_ukop,4),L_alop,L_elop
     &,R_akop,
     & REAL(R_ifop,4),REAL(R_efop,4),L_ilop,REAL(R_ekop,4
     &))
      !}
C FDA40_vlv.fgi( 320,  66):���������� �������,20FDA42CP001XQ01
      R_isip = R8_ofud
C FDA40_vent_log.fgi( 306,  63):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_erip,R_avip,REAL(1,4)
     &,
     & REAL(R_urip,4),REAL(R_asip,4),
     & REAL(R_arip,4),REAL(R_upip,4),I_utip,
     & REAL(R_osip,4),L_usip,REAL(R_atip,4),L_etip,L_itip
     &,R_esip,
     & REAL(R_orip,4),REAL(R_irip,4),L_otip,REAL(R_isip,4
     &))
      !}
C FDA40_vlv.fgi( 379,  66):���������� �������,20FDA42CT001XQ01
      R_avop = R8_ufud
C FDA40_vent_log.fgi( 306,  71):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_usop,R_oxop,REAL(1,4)
     &,
     & REAL(R_itop,4),REAL(R_otop,4),
     & REAL(R_osop,4),REAL(R_isop,4),I_ixop,
     & REAL(R_evop,4),L_ivop,REAL(R_ovop,4),L_uvop,L_axop
     &,R_utop,
     & REAL(R_etop,4),REAL(R_atop,4),L_exop,REAL(R_avop,4
     &))
      !}
C FDA40_vlv.fgi( 255,  66):���������� �������,20FDA41CU001XQ01
      R_opop = R8_akud
C FDA40_vent_log.fgi( 304,  80):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_imop,R_esop,REAL(1,4)
     &,
     & REAL(R_apop,4),REAL(R_epop,4),
     & REAL(R_emop,4),REAL(R_amop,4),I_asop,
     & REAL(R_upop,4),L_arop,REAL(R_erop,4),L_irop,L_orop
     &,R_ipop,
     & REAL(R_umop,4),REAL(R_omop,4),L_urop,REAL(R_opop,4
     &))
      !}
C FDA40_vlv.fgi( 291,  66):���������� �������,20FDA42CF001XQ01
      if(R8_elud.le.R0_ukud) then
         R_alud=R0_okud
      elseif(R8_elud.gt.R0_ikud) then
         R_alud=R0_ekud
      else
         R_alud=R0_okud+(R8_elud-(R0_ukud))*(R0_ekud-(R0_okud
     &))/(R0_ikud-(R0_ukud))
      endif
C FDA40_vent_log.fgi( 305,  92):��������������� ���������
      L_(399)=R_alud.gt.R_(89)
C FDA40_vent_log.fgi( 158, 116):���������� >
      L_(400) = L_(399).OR.L_ixod
C FDA40_vent_log.fgi( 183, 115):���
      L_ebud=L_abud.or.(L_ebud.and..not.(L_(400)))
      L_(401)=.not.L_ebud
C FDA40_vent_log.fgi( 189, 117):RS �������
      L_ibud=L_ebud
C FDA40_vent_log.fgi( 203, 119):������,FDA42dust_start
      if(L_ibud) then
         R_(90)=R_(91)
      else
         R_(90)=R_(92)
      endif
C FDA40_vent_log.fgi( 162,  95):���� RE IN LO CH7
      R8_ubud=(R0_oxod*R_(88)+deltat*R_(90))/(R0_oxod+deltat
     &)
C FDA40_vent_log.fgi( 172,  96):�������������� �����  
      R8_obud=R8_ubud
C FDA40_vent_log.fgi( 191,  96):������,20FDA42AN101Y01
      R_ulup = R8_ilud
C FDA40_vent_log.fgi( 306, 100):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_okup,R_ipup,REAL(1,4)
     &,
     & REAL(R_elup,4),REAL(R_ilup,4),
     & REAL(R_ikup,4),REAL(R_ekup,4),I_epup,
     & REAL(R_amup,4),L_emup,REAL(R_imup,4),L_omup,L_umup
     &,R_olup,
     & REAL(R_alup,4),REAL(R_ukup,4),L_apup,REAL(R_ulup,4
     &))
      !}
C FDA40_vlv.fgi( 255,  96):���������� �������,20FDA41CM001XQ01
      R_uxip = R8_olud
C FDA40_vent_log.fgi( 308, 116):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ovip,R_idop,REAL(1,4)
     &,
     & REAL(R_exip,4),REAL(R_ixip,4),
     & REAL(R_ivip,4),REAL(R_evip,4),I_edop,
     & REAL(R_abop,4),L_ebop,REAL(R_ibop,4),L_obop,L_ubop
     &,R_oxip,
     & REAL(R_axip,4),REAL(R_uvip,4),L_adop,REAL(R_uxip,4
     &))
      !}
C FDA40_vlv.fgi( 349,  66):���������� �������,20FDA41CP002XQ01
      !��������� R_(99) = FDA40_vent_logC?? /0.0/
      R_(99)=R0_imud
C FDA40_vent_log.fgi(  33, 128):���������
      !��������� R_(98) = FDA40_vent_logC?? /0.001/
      R_(98)=R0_omud
C FDA40_vent_log.fgi(  33, 126):���������
      R_amer = R8_umud
C FDA40_vent_log.fgi( 220, 202):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_aler,R_uper,REAL(1,4
     &),
     & REAL(R_amer,4),I_oper,REAL(R_oler,4),
     & REAL(R_uler,4),REAL(R_uker,4),
     & REAL(R_ebev,4),REAL(R_imer,4),L_omer,REAL(R_umer,4
     &),L_aper,
     & L_eper,R_ibev,REAL(R_iler,4),REAL(R_eler,4),L_iper
     &)
      !}
C FDA40_vlv.fgi( 291, 113):���������� ������� ��� 2,20FDA40CQ002XQ01
      L_(742)=R_ibev.lt.R_ebev
C FDA40_logic.fgi( 180, 148):���������� <
      L_(741)=R_ibev.gt.R_ebev
C FDA40_logic.fgi( 180, 130):���������� >
      R_uder = R8_apud
C FDA40_vent_log.fgi( 220, 221):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_uber,R_oker,REAL(1,4
     &),
     & REAL(R_uder,4),I_iker,REAL(R_ider,4),
     & REAL(R_oder,4),REAL(R_ober,4),
     & REAL(R_adev,4),REAL(R_efer,4),L_ifer,REAL(R_ofer,4
     &),L_ufer,
     & L_aker,R_edev,REAL(R_eder,4),REAL(R_ader,4),L_eker
     &)
      !}
C FDA40_vlv.fgi( 291,  97):���������� ������� ��� 2,20FDA40CM002XQ01
      L_(740)=R_edev.lt.R_adev
C FDA40_logic.fgi( 180, 141):���������� <
      L_abev = L_(742).AND.L_(740)
C FDA40_logic.fgi( 191, 147):�
      L_uxav=L_abev
C FDA40_logic.fgi( 210, 147):������,20FDA40AA103YA22
      L_(739)=R_edev.gt.R_adev
C FDA40_logic.fgi( 180, 123):���������� >
      L_ubev = L_(741).OR.L_(739)
C FDA40_logic.fgi( 190, 129):���
      L_obev=L_ubev
C FDA40_logic.fgi( 210, 129):������,20FDA40AA103YA21
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_ubet,4),R8_uvat,I_eket
     &,I_uket,I_aket,
     & C8_ixat,I_oket,R_ebet,R_abet,R_usat,
     & REAL(R_etat,4),R_avat,REAL(R_ivat,4),
     & R_itat,REAL(R_utat,4),I_ofet,I_alet,I_iket,I_ifet,L_ovat
     &,
     & L_ilet,L_imet,L_evat,L_otat,
     & L_exat,L_axat,L_ulet,L_atat,L_uxat,L_apet,L_ibet,
     & L_obet,REAL(R8_okot,8),REAL(1.0,4),R8_arot,L_uxav,L_obev
     &,L_olet,
     & I_elet,L_omet,R_efet,REAL(R_oxat,4),L_umet,L_isat,L_epet
     &,L_osat,
     & L_adet,L_edet,L_idet,L_udet,L_afet,L_odet)
      !}

      if(L_afet.or.L_udet.or.L_odet.or.L_idet.or.L_edet.or.L_adet
     &) then      
                  I_ufet = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_ufet = z'40000000'
      endif
C FDA40_vlv.fgi( 161, 169):���� ���������� �������� ��������,20FDA40AA103
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_ufat,4),R8_ubat,I_emat
     &,I_umat,I_amat,
     & C8_idat,I_omat,R_efat,R_afat,R_uvus,
     & REAL(R_exus,4),R_abat,REAL(R_ibat,4),
     & R_ixus,REAL(R_uxus,4),I_olat,I_apat,I_imat,I_ilat,L_obat
     &,
     & L_ipat,L_irat,L_ebat,L_oxus,
     & L_edat,L_adat,L_upat,L_axus,L_udat,L_asat,L_ifat,
     & L_ofat,REAL(R8_okot,8),REAL(1.0,4),R8_arot,L_abev,L_ubev
     &,L_opat,
     & I_epat,L_orat,R_elat,REAL(R_odat,4),L_urat,L_ivus,L_esat
     &,L_ovus,
     & L_akat,L_ekat,L_ikat,L_ukat,L_alat,L_okat)
      !}

      if(L_alat.or.L_ukat.or.L_okat.or.L_ikat.or.L_ekat.or.L_akat
     &) then      
                  I_ulat = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_ulat = z'40000000'
      endif
C FDA40_vlv.fgi( 175, 169):���� ���������� �������� ��������,20FDA40AA104
      R_ebir = R8_epud
C FDA40_vent_log.fgi( 220, 212):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_axer,R_afir,REAL(1,4
     &),
     & REAL(R_ebir,4),I_udir,REAL(R_oxer,4),
     & REAL(R_uxer,4),REAL(R_uver,4),
     & REAL(R_over,4),REAL(R_obir,4),L_ubir,REAL(R_adir,4
     &),L_edir,
     & L_idir,R_abir,REAL(R_ixer,4),REAL(R_exer,4),L_odir
     &)
      !}
C FDA40_vlv.fgi( 291, 129):���������� ������� ��� 2,20FDA40CQ001XQ01
      R_ukir = R8_ipud
C FDA40_vent_log.fgi( 220, 227):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ofir,R_omir,REAL(1,4
     &),
     & REAL(R_ukir,4),I_imir,REAL(R_ekir,4),
     & REAL(R_ikir,4),REAL(R_ifir,4),
     & REAL(R_efir,4),REAL(R_elir,4),L_ilir,REAL(R_olir,4
     &),L_ulir,
     & L_amir,R_okir,REAL(R_akir,4),REAL(R_ufir,4),L_emir
     &)
      !}
C FDA40_vlv.fgi( 291, 145):���������� ������� ��� 2,20FDA40CM001XQ01
      R_(100) = R8_opud + (-R8_erud)
C FDA40_vent_log.fgi(  41, 213):��������
      R_ikar = R_(100) * R_(84)
C FDA40_vent_log.fgi(  50, 212):����������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_efar,R_emar,REAL(1,4
     &),
     & REAL(R_ikar,4),I_amar,REAL(R_ufar,4),
     & REAL(R_akar,4),REAL(R_afar,4),
     & REAL(R_udar,4),REAL(R_ukar,4),L_alar,REAL(R_elar,4
     &),L_ilar,
     & L_olar,R_ekar,REAL(R_ofar,4),REAL(R_ifar,4),L_ular
     &)
      !}
C FDA40_vlv.fgi( 349, 144):���������� ������� ��� 2,20FDA40CP002XQ01
      R_(101) = R8_upud + (-R8_erud)
C FDA40_vent_log.fgi(  41, 193):��������
      R_esup = R_(101) * R_(82)
C FDA40_vent_log.fgi(  50, 192):����������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_arup,R_avup,REAL(1,4
     &),
     & REAL(R_esup,4),I_utup,REAL(R_orup,4),
     & REAL(R_urup,4),REAL(R_upup,4),
     & REAL(R_opup,4),REAL(R_osup,4),L_usup,REAL(R_atup,4
     &),L_etup,
     & L_itup,R_asup,REAL(R_irup,4),REAL(R_erup,4),L_otup
     &)
      !}
C FDA40_vlv.fgi( 320,  81):���������� ������� ��� 2,20FDA40CP004XQ01
      R_(102) = R8_arud + (-R8_erud)
C FDA40_vent_log.fgi(  41, 203):��������
      R_arar = R_(102) * R_(83)
C FDA40_vent_log.fgi(  50, 202):����������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_umar,R_usar,REAL(1,4
     &),
     & REAL(R_arar,4),I_osar,REAL(R_ipar,4),
     & REAL(R_opar,4),REAL(R_omar,4),
     & REAL(R_imar,4),REAL(R_irar,4),L_orar,REAL(R_urar,4
     &),L_asar,
     & L_esar,R_upar,REAL(R_epar,4),REAL(R_apar,4),L_isar
     &)
      !}
C FDA40_vlv.fgi( 349,  81):���������� ������� ��� 2,20FDA40CP003XQ01
      R_(103) = 60000
C FDA40_vent_log.fgi( 129, 176):��������� (RE4) (�������)
      if(R8_irud.ge.0.0) then
         R_(104)=R8_orud/max(R8_irud,1.0e-10)
      else
         R_(104)=R8_orud/min(R8_irud,-1.0e-10)
      endif
C FDA40_vent_log.fgi( 119, 179):�������� ����������
      R_uxup = R_(104) * R_(103)
C FDA40_vent_log.fgi( 132, 178):����������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ovup,R_odar,REAL(1,4
     &),
     & REAL(R_uxup,4),I_idar,REAL(R_exup,4),
     & REAL(R_ixup,4),REAL(R_ivup,4),
     & REAL(R_evup,4),REAL(R_ebar,4),L_ibar,REAL(R_obar,4
     &),L_ubar,
     & L_adar,R_oxup,REAL(R_axup,4),REAL(R_uvup,4),L_edar
     &)
      !}
C FDA40_vlv.fgi( 291,  81):���������� ������� ��� 2,20FDA40CF005XQ01
      R_(105) = 60000
C FDA40_vent_log.fgi( 129, 187):��������� (RE4) (�������)
      if(R8_urud.ge.0.0) then
         R_(106)=R8_asud/max(R8_urud,1.0e-10)
      else
         R_(106)=R8_asud/min(R8_urud,-1.0e-10)
      endif
C FDA40_vent_log.fgi( 119, 190):�������� ����������
      R_otor = R_(106) * R_(105)
C FDA40_vent_log.fgi( 132, 189):����������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_isor,R_ixor,REAL(1,4
     &),
     & REAL(R_otor,4),I_exor,REAL(R_ator,4),
     & REAL(R_etor,4),REAL(R_esor,4),
     & REAL(R_asor,4),REAL(R_avor,4),L_evor,REAL(R_ivor,4
     &),L_ovor,
     & L_uvor,R_itor,REAL(R_usor,4),REAL(R_osor,4),L_axor
     &)
      !}
C FDA40_vlv.fgi( 320, 145):���������� ������� ��� 2,20FDA40CF004XQ01
      R_(107) = 60000
C FDA40_vent_log.fgi( 129, 199):��������� (RE4) (�������)
      if(R8_esud.ge.0.0) then
         R_(108)=R8_isud/max(R8_esud,1.0e-10)
      else
         R_(108)=R8_isud/min(R8_esud,-1.0e-10)
      endif
C FDA40_vent_log.fgi( 119, 202):�������� ����������
      R_ovar = R_(108) * R_(107)
C FDA40_vent_log.fgi( 132, 201):����������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_itar,R_iber,REAL(1,4
     &),
     & REAL(R_ovar,4),I_eber,REAL(R_avar,4),
     & REAL(R_evar,4),REAL(R_etar,4),
     & REAL(R_atar,4),REAL(R_axar,4),L_exar,REAL(R_ixar,4
     &),L_oxar,
     & L_uxar,R_ivar,REAL(R_utar,4),REAL(R_otar,4),L_aber
     &)
      !}
C FDA40_vlv.fgi( 320,  97):���������� ������� ��� 2,20FDA40CF003XQ01
      if(R8_osud.ge.0.0) then
         R_(110)=R8_usud/max(R8_osud,1.0e-10)
      else
         R_(110)=R8_usud/min(R8_osud,-1.0e-10)
      endif
C FDA40_vent_log.fgi( 119, 216):�������� ����������
      R_ifor = R_(110) * R_(109)
C FDA40_vent_log.fgi( 132, 215):����������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_edor,R_elor,REAL(1,4
     &),
     & REAL(R_ifor,4),I_alor,REAL(R_udor,4),
     & REAL(R_afor,4),REAL(R_ador,4),
     & REAL(R_ubor,4),REAL(R_ufor,4),L_akor,REAL(R_ekor,4
     &),L_ikor,
     & L_okor,R_efor,REAL(R_odor,4),REAL(R_idor,4),L_ukor
     &)
      !}
C FDA40_vlv.fgi( 320, 129):���������� ������� ��� 2,20FDA40CF002XQ01
      if(R8_atud.ge.0.0) then
         R_(112)=R8_etud/max(R8_atud,1.0e-10)
      else
         R_(112)=R8_etud/min(R8_atud,-1.0e-10)
      endif
C FDA40_vent_log.fgi( 119, 226):�������� ����������
      R_oser = R_(112) * R_(111)
C FDA40_vent_log.fgi( 132, 225):����������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_irer,R_iver,REAL(1,4
     &),
     & REAL(R_oser,4),I_ever,REAL(R_aser,4),
     & REAL(R_eser,4),REAL(R_erer,4),
     & REAL(R_arer,4),REAL(R_ater,4),L_eter,REAL(R_iter,4
     &),L_oter,
     & L_uter,R_iser,REAL(R_urer,4),REAL(R_orer,4),L_aver
     &)
      !}
C FDA40_vlv.fgi( 320, 113):���������� ������� ��� 2,20FDA40CF001XQ01
      R_apor = R8_itud
C FDA40_vent_log.fgi( 321, 216):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ulor,R_uror,REAL(1,4
     &),
     & REAL(R_apor,4),I_oror,REAL(R_imor,4),
     & REAL(R_omor,4),REAL(R_olor,4),
     & REAL(R_ilor,4),REAL(R_ipor,4),L_opor,REAL(R_upor,4
     &),L_aror,
     & L_eror,R_umor,REAL(R_emor,4),REAL(R_amor,4),L_iror
     &)
      !}
C FDA40_vlv.fgi( 349, 129):���������� ������� ��� 2,20FDA40CP100XQ01
      R_uvir = R8_otud
C FDA40_vent_log.fgi( 321, 225):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_otir,R_obor,REAL(1,4
     &),
     & REAL(R_uvir,4),I_ibor,REAL(R_evir,4),
     & REAL(R_ivir,4),REAL(R_itir,4),
     & REAL(R_etir,4),REAL(R_exir,4),L_ixir,REAL(R_oxir,4
     &),L_uxir,
     & L_abor,R_ovir,REAL(R_avir,4),REAL(R_utir,4),L_ebor
     &)
      !}
C FDA40_vlv.fgi( 349, 113):���������� ������� ��� 2,20FDA40CT001XQ01
      R_arir = R8_utud
C FDA40_vent_log.fgi(  43, 228):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_osir,4),REAL
     &(R_arir,4),R_apir,
     & R_usir,REAL(1,4),REAL(R_opir,4),
     & REAL(R_upir,4),REAL(R_udev,4),
     & REAL(R_umir,4),I_isir,REAL(R_erir,4),L_irir,
     & REAL(R_orir,4),L_urir,L_asir,R_afev,REAL(R_ipir,4)
     &,REAL(R_epir,4),L_esir)
      !}
C FDA40_vlv.fgi( 349,  97):���������� ������� ��������,20FDA40CP001XQ01
      L_odev=R_afev.gt.R_udev
C FDA40_logic.fgi( 180, 165):���������� >
      L_idev=R_afev.lt.R_udev
C FDA40_logic.fgi( 180, 159):���������� <
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_etit,4),R8_erit,I_oxit
     &,I_ebot,I_ixit,
     & C8_urit,I_abot,R_osit,R_isit,R_emit,
     & REAL(R_omit,4),R_ipit,REAL(R_upit,4),
     & R_umit,REAL(R_epit,4),I_axit,I_ibot,I_uxit,I_uvit,L_arit
     &,
     & L_ubot,L_udot,L_opit,L_apit,
     & L_orit,L_irit,L_edot,L_imit,L_esit,L_ifot,L_usit,
     & L_atit,REAL(R8_okot,8),REAL(1.0,4),R8_arot,L_idev,L_odev
     &,L_adot,
     & I_obot,L_afot,R_ovit,REAL(R_asit,4),L_efot,L_ulit,L_ofot
     &,L_amit,
     & L_itit,L_otit,L_utit,L_evit,L_ivit,L_avit)
      !}

      if(L_ivit.or.L_evit.or.L_avit.or.L_utit.or.L_otit.or.L_itit
     &) then      
                  I_exit = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_exit = z'40000000'
      endif
C FDA40_vlv.fgi( 133, 169):���� ���������� �������� ��������,20FDA40AA100
      Call REG_RAS_MAN(deltat,.TRUE.,L_akav,REAL(R_ufav,4
     &),REAL(R_abav,4),R_ikav,
     & REAL(R_exut,4),R_ekav,R_alav,R_ukav,R_ixut,REAL(R_uxut
     &,4),
     & R_avut,REAL(R_ivut,4),R_ovut,REAL(R_axut,4),L_oxut
     &,
     & R_ilav,R_olav,L_okav,L_evut,L_uvut,I_afav,R_ulav,R_udav
     &,
     & R_elav,R8_odav,REAL(100000,4),REAL(R_afev,4),REAL(R_ebav
     &,4),
     & REAL(R_ofav,4),REAL(R_ifav,4),REAL(R_efav,4))
C FDA40_logic.fgi( 170,  67):���������� ���������� �������,20FDA40DF001
      !��������� R_(116) = FDA40_vent_logC?? /0.0/
      R_(116)=R0_ovud
C FDA40_vent_log.fgi( 107, 268):���������
      !��������� R_(115) = FDA40_vent_logC?? /0.001/
      R_(115)=R0_uvud
C FDA40_vent_log.fgi( 107, 266):���������
      !��������� R_(120) = FDA40_vent_logC?? /0.0/
      R_(120)=R0_oxud
C FDA40_vent_log.fgi(  34, 268):���������
      !��������� R_(119) = FDA40_vent_logC?? /0.001/
      R_(119)=R0_uxud
C FDA40_vent_log.fgi(  34, 266):���������
      !��������� R_(125) = FDA40_logic_loadunloadC?? /0.001
C /
      R_(125)=R0_edaf
C FDA40_logic_loadunload.fgi( 105, 288):���������
      !��������� R_ulaf = FDA40_logic_loadunloadC?? /0.0/
      R_ulaf=R0_ilaf
C FDA40_logic_loadunload.fgi(  11, 285):���������
      R_olaf = 100.0
C FDA40_logic_loadunload.fgi(  11, 279):��������� (RE4) (�������)
      R_amaf = 100.0
C FDA40_logic_loadunload.fgi(  11, 289):��������� (RE4) (�������)
      !��������� I_(195) = FDA40_logic_press_stepC?? /16/
      I_(195)=I0_apaf
C FDA40_logic_press_step.fgi( 547, 506):��������� ������������� IN
      !��������� R_(130) = FDA40_logic_press_stepC?? /0.0
C /
      R_(130)=R0_ipaf
C FDA40_logic_press_step.fgi( 555, 526):���������
      !��������� I_(196) = FDA40_logic_press_stepC?? /3/
      I_(196)=I0_iraf
C FDA40_logic_press_step.fgi( 499, 393):��������� ������������� IN
      !��������� I_(197) = FDA40_logic_press_stepC?? /2/
      I_(197)=I0_ivaf
C FDA40_logic_press_step.fgi( 408, 373):��������� ������������� IN
      I_(198) = I1_axaf * I_(197)
C FDA40_logic_press_step.fgi( 414, 378):����������
      L_(597)=.false.
C FDA40_logic_press_step.fgi( 420, 970):��������� ���������� (�������)
      L_(610)=.false.
C FDA40_logic_press_step.fgi( 219, 968):��������� ���������� (�������)
      L_opof = L_irof.AND.L_irof.AND.L_erof.AND.L_arof.AND.L_upof.AND.L_
     &ipof.AND.L_epof.AND.L_apof
C FDA40_logic_press_step.fgi(  75, 662):�
      L_esof = L_atof.AND.L_atof.AND.L_usof.AND.L_osof.AND.L_isof.AND.L_
     &asof.AND.L_urof.AND.L_orof
C FDA40_logic_press_step.fgi( 191, 713):�
      L_otof = L_ivof.AND.L_ivof.AND.L_evof.AND.L_avof.AND.L_utof.AND.L_
     &itof.AND.L_etof
C FDA40_logic_press_step.fgi( 191, 769):�
      L_axof = L_uxof.AND.L_uxof.AND.L_oxof.AND.L_ixof.AND.L_exof.AND.L_
     &uvof.AND.L_ovof
C FDA40_logic_press_step.fgi(  75, 714):�
      !��������� R_osuf = FDA40_logic_press_1C?? /30.0/
      R_osuf=R0_isuf
C FDA40_logic_press_1.fgi(  11, 558):���������
      !��������� R_atuf = FDA40_logic_press_1C?? /67.0/
      R_atuf=R0_usuf
C FDA40_logic_press_1.fgi(  11, 569):���������
      !��������� R_(217) = FDA40_logic_press_1C?? /0.0/
      R_(217)=R0_ituf
C FDA40_logic_press_1.fgi( 186,  42):���������
      L_(622)=.false.
C FDA40_logic_press_1.fgi( 110,  33):��������� ���������� (�������)
      !��������� R_(219) = FDA40_logic_press_1C?? /0.0/
      R_(219)=R0_avuf
C FDA40_logic_press_1.fgi(  82,  40):���������
      if(L_(621)) then
         R_(222)=R_ekak
      else
         R_(222)=R_(219)
      endif
C FDA40_logic_press_1.fgi(  85,  38):���� RE IN LO CH7
      !��������� R_(221) = FDA40_logic_press_1C?? /0.0/
      R_(221)=R0_ivuf
C FDA40_logic_press_1.fgi( 115,  48):���������
      !��������� R_(223) = FDA40_logic_press_1C?? /1.0/
      R_(223)=R0_ovuf
C FDA40_logic_press_1.fgi( 111,  45):���������
      !��������� R_(226) = FDA40_logic_press_1C?? /0.0/
      R_(226)=R0_ebak
C FDA40_logic_press_1.fgi( 196, 113):���������
      !��������� R_(228) = FDA40_logic_press_1C?? /0.1/
      R_(228)=R0_ibak
C FDA40_logic_press_1.fgi( 145,  97):���������
      !��������� R_(233) = FDA40_logic_press_1C?? /0.0/
      R_(233)=R0_edak
C FDA40_logic_press_1.fgi( 116, 113):���������
      L_(630)=.false.
C FDA40_logic_press_1.fgi( 110,  99):��������� ���������� (�������)
      !��������� R_(235) = FDA40_logic_press_1C?? /1.0/
      R_(235)=R0_odak
C FDA40_logic_press_1.fgi( 110, 110):���������
      !��������� R_umak = FDA40_logic_press_1C?? /0.0/
      R_umak=R0_omak
C FDA40_logic_press_1.fgi( 275, 471):���������
      !��������� R_epak = FDA40_logic_press_1C?? /1.0/
      R_epak=R0_apak
C FDA40_logic_press_1.fgi( 275, 476):���������
      R_irak = 50.0
C FDA40_logic_press_1.fgi(  11, 231):��������� (RE4) (�������)
      !��������� R_ekek = FDA40_logic_press_1C?? /2.0e-3/
      R_ekek=R0_asak
C FDA40_logic_press_1.fgi(  11, 237):���������
      !��������� R_itak = FDA40_logic_press_1C?? /20/
      R_itak=R0_esak
C FDA40_logic_press_1.fgi(  11, 222):���������
      !��������� R_obek = FDA40_logic_press_1C?? /15/
      R_obek=R0_isak
C FDA40_logic_press_1.fgi(  11, 217):���������
      R_(220) = R_itak + (-R_obek)
C FDA40_logic_press_1.fgi( 104,  50):��������
      if(L_(622)) then
         R0_uvuf=R_(223)
      elseif(L_(622)) then
         R0_uvuf=R0_uvuf
      else
         R0_uvuf=R0_uvuf+deltat/R_(223)*R_(222)
      endif
      if(R0_uvuf.gt.R_(220)) then
         R0_uvuf=R_(220)
      elseif(R0_uvuf.lt.R_(221)) then
         R0_uvuf=R_(221)
      endif
C FDA40_logic_press_1.fgi( 117,  39):����������
      R_avak = 30.0
C FDA40_logic_press_1.fgi(  11, 226):��������� (RE4) (�������)
      !��������� R_(256) = FDA40_logic_press_1C?? /1e6/
      R_(256)=R0_utak
C FDA40_logic_press_1.fgi( 126, 228):���������
      !��������� R_(257) = FDA40_logic_press_1C?? /0.0/
      R_(257)=R0_evak
C FDA40_logic_press_1.fgi( 126, 230):���������
      !��������� R_(259) = FDA40_logic_press_1C?? /0.0/
      R_(259)=R0_ovak
C FDA40_logic_press_1.fgi( 116, 234):���������
      R_oxak = 0
C FDA40_logic_press_1.fgi(  11, 201):��������� (RE4) (�������)
      R_(265) = R_irak + (-R_oxak)
C FDA40_logic_press_1.fgi( 169, 190):��������
      !��������� R_abek = FDA40_logic_press_1C?? /5/
      R_abek=R0_uxak
C FDA40_logic_press_1.fgi(  11, 207):���������
      !��������� R_ebek = FDA40_logic_press_1C?? /8/
      R_ebek=R0_ibek
C FDA40_logic_press_1.fgi(  11, 212):���������
      !��������� R_(261) = FDA40_logic_press_1C?? /0.001/
      R_(261)=R0_adek
C FDA40_logic_press_1.fgi( 109, 197):���������
      !��������� R_opok = FDA40_logic_press_1C?? /2.0e-3/
      R_opok=R0_ofek
C FDA40_logic_press_1.fgi( 275, 577):���������
      !��������� R_(272) = FDA40_logic_press_1C?? /0.0/
      R_(272)=R0_ufek
C FDA40_logic_press_1.fgi( 390, 570):���������
      !��������� R_(274) = FDA40_logic_press_1C?? /0.0/
      R_(274)=R0_ikek
C FDA40_logic_press_1.fgi( 380, 574):���������
      R_utek = 150.0
C FDA40_logic_press_1.fgi( 275, 556):��������� (RE4) (�������)
      !��������� R_olek = FDA40_logic_press_1C?? /85.0/
      R_olek=R0_ilek
C FDA40_logic_press_1.fgi( 275, 562):���������
      R_emek = 0.0
C FDA40_logic_press_1.fgi( 275, 571):��������� (RE4) (�������)
      !��������� R_ulek = FDA40_logic_press_1C?? /25.0/
      R_ulek=R0_amek
C FDA40_logic_press_1.fgi( 275, 567):���������
      R_umek = 0.0
C FDA40_logic_press_1.fgi(  11, 546):��������� (RE4) (�������)
      R_apek = 3.0
C FDA40_logic_press_1.fgi(  11, 551):��������� (RE4) (�������)
      !��������� R_ipek = FDA40_logic_press_1C?? /60.0/
      R_ipek=R0_epek
C FDA40_logic_press_1.fgi(  11, 564):���������
      R_(232) = R_ipek + (-R_akak)
C FDA40_logic_press_1.fgi(  99, 115):��������
      R_(229) = R_(232) + (-R_(228))
C FDA40_logic_press_1.fgi( 148,  98):��������
      if(R_afak.ge.0.0) then
         R_(230)=R_ipek/max(R_afak,1.0e-10)
      else
         R_(230)=R_ipek/min(R_afak,-1.0e-10)
      endif
C FDA40_logic_press_1.fgi(  87, 105):�������� ����������
      R_(231) = (-R_(230))
C FDA40_logic_press_1.fgi(  94, 105):��������
      R_ukik = 100.0
C FDA40_logic_press_1.fgi(  11, 579):��������� (RE4) (�������)
      !��������� R_opek = FDA40_logic_press_1C?? /75.0/
      R_opek=R0_upek
C FDA40_logic_press_1.fgi(  11, 575):���������
      !��������� R_(279) = FDA40_logic_press_1C?? /0.001/
      R_(279)=R0_itek
C FDA40_logic_press_1.fgi( 373, 532):���������
      !��������� R_(283) = FDA40_logic_press_1C?? /0.1/
      R_(283)=R0_obik
C FDA40_logic_press_1.fgi( 377, 472):���������
      !��������� R_(287) = FDA40_logic_press_1C?? /0.001/
      R_(287)=R0_okik
C FDA40_logic_press_1.fgi( 105, 578):���������
      !��������� R_evik = FDA40_logic_pressC?? /80.0/
      R_evik=R0_etik
C FDA40_logic_press.fgi(  13, 237):���������
      !��������� R_ivik = FDA40_logic_pressC?? /70.0/
      R_ivik=R0_itik
C FDA40_logic_press.fgi(  13, 242):���������
      !��������� R_ovik = FDA40_logic_pressC?? /30.0/
      R_ovik=R0_otik
C FDA40_logic_press.fgi(  13, 247):���������
      !��������� R_avik = FDA40_logic_pressC?? /10.0/
      R_avik=R0_utik
C FDA40_logic_press.fgi(  13, 252):���������
      !��������� R_(301) = FDA40_logic_pressC?? /0.001/
      R_(301)=R0_edok
C FDA40_logic_press.fgi( 134, 211):���������
      !��������� R_(304) = FDA40_logic_pressC?? /100.0/
      R_(304)=R0_udok
C FDA40_logic_press.fgi( 166, 212):���������
      L_(636)=R_irok.gt.R0_ilok
C FDA40_logic_press.fgi( 113, 271):���������� >
      !��������� R_(307) = FDA40_logic_pressC?? /0.0/
      R_(307)=R0_ulok
C FDA40_logic_press.fgi( 141, 274):���������
      !��������� R_(309) = FDA40_logic_pressC?? /0.0/
      R_(309)=R0_amok
C FDA40_logic_press.fgi( 131, 277):���������
      if(L_(636)) then
         R_(308)=R_atok
      else
         R_(308)=R_(309)
      endif
C FDA40_logic_press.fgi( 135, 275):���� RE IN LO CH7
      !��������� R_(312) = FDA40_logic_pressC?? /0.0/
      R_(312)=R0_apok
C FDA40_logic_press.fgi( 148, 247):���������
      !��������� R_(314) = FDA40_logic_pressC?? /0.0/
      R_(314)=R0_arok
C FDA40_logic_press.fgi( 138, 251):���������
      L_erok=.false.
C FDA40_logic_press.fgi( 232, 132):��������� ���������� (�������)
      L_orok=.false.
C FDA40_logic_press.fgi( 232, 137):��������� ���������� (�������)
      I_(202) = z'01000003'
C FDA40_vlv.fgi( 248,  41):��������� ������������� IN (�������)
      I_(201) = z'0100000A'
C FDA40_vlv.fgi( 248,  39):��������� ������������� IN (�������)
      I_(204) = z'01000003'
C FDA40_vlv.fgi( 304,  16):��������� ������������� IN (�������)
      I_(203) = z'0100000A'
C FDA40_vlv.fgi( 304,  14):��������� ������������� IN (�������)
      I_(206) = z'01000003'
C FDA40_vlv.fgi( 236,  16):��������� ������������� IN (�������)
      I_(205) = z'0100000A'
C FDA40_vlv.fgi( 236,  14):��������� ������������� IN (�������)
      I_(208) = z'01000003'
C FDA40_vlv.fgi( 171,  16):��������� ������������� IN (�������)
      I_(207) = z'0100000A'
C FDA40_vlv.fgi( 171,  14):��������� ������������� IN (�������)
      I_(210) = z'01000003'
C FDA40_vlv.fgi( 105,  16):��������� ������������� IN (�������)
      I_(209) = z'0100000A'
C FDA40_vlv.fgi( 105,  14):��������� ������������� IN (�������)
      I_(212) = z'01000003'
C FDA40_vlv.fgi(  39,  16):��������� ������������� IN (�������)
      I_(211) = z'0100000A'
C FDA40_vlv.fgi(  39,  14):��������� ������������� IN (�������)
      I_(214) = z'01000003'
C FDA40_vlv.fgi( 183,  40):��������� ������������� IN (�������)
      I_(213) = z'0100000A'
C FDA40_vlv.fgi( 183,  38):��������� ������������� IN (�������)
      I_(216) = z'01000003'
C FDA40_vlv.fgi( 104,  40):��������� ������������� IN (�������)
      I_(215) = z'0100000A'
C FDA40_vlv.fgi( 104,  38):��������� ������������� IN (�������)
      I_(218) = z'01000003'
C FDA40_vlv.fgi(  39,  40):��������� ������������� IN (�������)
      I_(217) = z'0100000A'
C FDA40_vlv.fgi(  39,  38):��������� ������������� IN (�������)
      R_(324)=abs(R_oxel)
C FDA40_vlv.fgi(  26,  29):���������� ��������
      L_(676)=R_(324).gt.R0_ixel
C FDA40_vlv.fgi(  34,  29):���������� >
      if(L_(676)) then
         I_exel=I_(217)
      else
         I_exel=I_(218)
      endif
C FDA40_vlv.fgi(  42,  38):���� RE IN LO CH7
      I_(220) = z'01000003'
C FDA40_vlv.fgi( 199,  63):��������� ������������� IN (�������)
      I_(219) = z'0100000A'
C FDA40_vlv.fgi( 199,  61):��������� ������������� IN (�������)
      I_(222) = z'01000003'
C FDA40_vlv.fgi( 143,  63):��������� ������������� IN (�������)
      I_(221) = z'0100000A'
C FDA40_vlv.fgi( 143,  61):��������� ������������� IN (�������)
      I_(224) = z'01000003'
C FDA40_vlv.fgi(  87,  63):��������� ������������� IN (�������)
      I_(223) = z'0100000A'
C FDA40_vlv.fgi(  87,  61):��������� ������������� IN (�������)
      I_(226) = z'01000003'
C FDA40_vlv.fgi(  32,  64):��������� ������������� IN (�������)
      I_(225) = z'0100000A'
C FDA40_vlv.fgi(  32,  62):��������� ������������� IN (�������)
      !{
      Call REG_MAN(deltat,REAL(R_avep,4),R8_asep,R_odip,R_ubip
     &,
     & L_akip,R_udip,R_adip,L_ekip,R_etep,R_atep,
     & R_apep,REAL(R_ipep,4),R_erep,
     & REAL(R_orep,4),R_opep,REAL(R_arep,4),I_oxep,
     & L_urep,L_ebip,L_afip,L_irep,L_upep,
     & L_isep,L_esep,L_obip,L_epep,L_usep,L_ofip,L_otep,
     & L_utep,REAL(R8_okot,8),REAL(1.0,4),R8_arot,L_emep,L_imep
     &,L_ibip,
     & I_abip,L_efip,R_ixep,REAL(R_osep,4),L_omep,L_umep,L_evep
     &,L_ivep,
     & L_ovep,L_axep,L_exep,L_uvep)
      !}

      if(L_exep.or.L_axep.or.L_uvep.or.L_ovep.or.L_ivep.or.L_evep
     &) then      
                  I_uxep = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_uxep = z'40000000'
      endif


      R_itep=100*R8_asep
C FDA40_vlv.fgi( 190, 169):���� ���������� ������������ ��������,20FDA40AA200
      !{
      Call DAT_ANA_HANDLER(deltat,R_ebup,R_akup,REAL(1,4)
     &,
     & REAL(R_ubup,4),REAL(R_adup,4),
     & REAL(R_abup,4),REAL(R_uxop,4),I_ufup,
     & REAL(R_odup,4),L_udup,REAL(R_afup,4),L_efup,L_ifup
     &,R_edup,
     & REAL(R_obup,4),REAL(R_ibup,4),L_ofup,REAL(R_idup,4
     &))
      !}
C FDA40_vlv.fgi( 255,  81):���������� �������,20FDA41CG001XQ01
      !{
      Call BVALVE2_MAN(deltat,REAL(R_olur,4),R8_ifur,I_arur
     &,I_orur,I_upur,C8_ekur,
     & I_irur,R_alur,R_ukur,R_ibur,REAL(R_ubur,4),
     & R_odur,REAL(R_afur,4),R_adur,
     & REAL(R_idur,4),I_ipur,I_urur,I_erur,I_epur,L_efur,L_esur
     &,
     & L_etur,L_udur,L_akur,L_ufur,L_osur,
     & L_edur,L_obur,L_okur,L_utur,L_elur,L_ilur,
     & REAL(R8_okot,8),REAL(1.0,4),R8_arot,L_oxor,L_uxor,L_isur
     &,I_asur,
     & L_itur,R8_ofur,R_apur,REAL(R_ikur,4),L_otur,L_abur
     &,L_avur,L_ebur,
     & L_ulur,L_amur,L_emur,L_omur,L_umur,L_imur)
      !}

      if(L_umur.or.L_omur.or.L_imur.or.L_emur.or.L_amur.or.L_ulur
     &) then      
                  I_opur = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_opur = z'40000000'
      endif
C FDA40_vlv.fgi( 236, 144):���� ���������� �������� �������� ��� 2,20FDA42AA007
      !{
      Call BVALVE2_MAN(deltat,REAL(R_ekas,4),R8_adas,I_omas
     &,I_epas,I_imas,C8_udas,
     & I_apas,R_ofas,R_ifas,R_axur,REAL(R_ixur,4),
     & R_ebas,REAL(R_obas,4),R_oxur,
     & REAL(R_abas,4),I_amas,I_ipas,I_umas,I_ulas,L_ubas,L_upas
     &,
     & L_uras,L_ibas,L_odas,L_idas,L_eras,
     & L_uxur,L_exur,L_efas,L_isas,L_ufas,L_akas,
     & REAL(R8_okot,8),REAL(1.0,4),R8_arot,L_evur,L_ivur,L_aras
     &,I_opas,
     & L_asas,R8_edas,R_olas,REAL(R_afas,4),L_esas,L_ovur
     &,L_osas,L_uvur,
     & L_ikas,L_okas,L_ukas,L_elas,L_ilas,L_alas)
      !}

      if(L_ilas.or.L_elas.or.L_alas.or.L_ukas.or.L_okas.or.L_ikas
     &) then      
                  I_emas = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_emas = z'40000000'
      endif
C FDA40_vlv.fgi( 218, 144):���� ���������� �������� �������� ��� 2,20FDA42AA005
      !{
      Call BVALVE2_MAN(deltat,REAL(R_udes,4),R8_oxas,I_eles
     &,I_ules,I_ales,C8_ibes,
     & I_oles,R_edes,R_ades,R_otas,REAL(R_avas,4),
     & R_uvas,REAL(R_exas,4),R_evas,
     & REAL(R_ovas,4),I_okes,I_ames,I_iles,I_ikes,L_ixas,L_imes
     &,
     & L_ipes,L_axas,L_ebes,L_abes,L_umes,
     & L_ivas,L_utas,L_ubes,L_ares,L_ides,L_odes,
     & REAL(R8_okot,8),REAL(1.0,4),R8_arot,L_usas,L_atas,L_omes
     &,I_emes,
     & L_opes,R8_uxas,R_ekes,REAL(R_obes,4),L_upes,L_etas
     &,L_eres,L_itas,
     & L_afes,L_efes,L_ifes,L_ufes,L_akes,L_ofes)
      !}

      if(L_akes.or.L_ufes.or.L_ofes.or.L_ifes.or.L_efes.or.L_afes
     &) then      
                  I_ukes = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_ukes = z'40000000'
      endif
C FDA40_vlv.fgi( 236, 169):���� ���������� �������� �������� ��� 2,20FDA41AA002
      !{
      Call BVALVE2_MAN(deltat,REAL(R_ibis,4),R8_eves,I_ufis
     &,I_ikis,I_ofis,C8_axes,
     & I_ekis,R_uxes,R_oxes,R_eses,REAL(R_oses,4),
     & R_ites,REAL(R_utes,4),R_uses,
     & REAL(R_etes,4),I_efis,I_okis,I_akis,I_afis,L_aves,L_alis
     &,
     & L_amis,L_otes,L_uves,L_oves,L_ilis,
     & L_ates,L_ises,L_ixes,L_omis,L_abis,L_ebis,
     & REAL(R8_okot,8),REAL(1.0,4),R8_arot,L_ires,L_ores,L_elis
     &,I_ukis,
     & L_emis,R8_ives,R_udis,REAL(R_exes,4),L_imis,L_ures
     &,L_umis,L_ases,
     & L_obis,L_ubis,L_adis,L_idis,L_odis,L_edis)
      !}

      if(L_odis.or.L_idis.or.L_edis.or.L_adis.or.L_ubis.or.L_obis
     &) then      
                  I_ifis = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_ifis = z'40000000'
      endif
C FDA40_vlv.fgi( 218, 169):���� ���������� �������� �������� ��� 2,20FDA41AA001
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_exet,4),R8_etet,I_odit
     &,I_efit,I_idit,
     & C8_utet,I_afit,R_ovet,R_ivet,R_eret,
     & REAL(R_oret,4),R_iset,REAL(R_uset,4),
     & R_uret,REAL(R_eset,4),I_adit,I_ifit,I_udit,I_ubit,L_atet
     &,
     & L_ufit,L_ukit,L_oset,L_aset,
     & L_otet,L_itet,L_ekit,L_iret,L_evet,L_ilit,L_uvet,
     & L_axet,REAL(R8_okot,8),REAL(1.0,4),R8_arot,L_ipet,L_opet
     &,L_akit,
     & I_ofit,L_alit,R_obit,REAL(R_avet,4),L_elit,L_upet,L_olit
     &,L_aret,
     & L_ixet,L_oxet,L_uxet,L_ebit,L_ibit,L_abit)
      !}

      if(L_ibit.or.L_ebit.or.L_abit.or.L_uxet.or.L_oxet.or.L_ixet
     &) then      
                  I_edit = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_edit = z'40000000'
      endif
C FDA40_vlv.fgi( 147, 169):���� ���������� �������� ��������,20FDA40AA102
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_asot,4),R8_umot,I_ivot
     &,I_axot,I_evot,
     & C8_ipot,I_uvot,R_irot,R_erot,R_ukot,
     & REAL(R_elot,4),R_amot,REAL(R_imot,4),
     & R_ilot,REAL(R_ulot,4),I_utot,I_exot,I_ovot,I_otot,L_omot
     &,
     & L_oxot,L_obut,L_emot,L_olot,
     & L_epot,L_apot,L_abut,L_alot,L_upot,L_edut,L_orot,
     & L_urot,REAL(R8_okot,8),REAL(1.0,4),R8_arot,L_ufot,L_akot
     &,L_uxot,
     & I_ixot,L_ubut,R_itot,REAL(R_opot,4),L_adut,L_ekot,L_idut
     &,L_ikot,
     & L_esot,L_isot,L_osot,L_atot,L_etot,L_usot)
      !}

      if(L_etot.or.L_atot.or.L_usot.or.L_osot.or.L_isot.or.L_esot
     &) then      
                  I_avot = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_avot = z'40000000'
      endif
C FDA40_vlv.fgi( 119, 169):���� ���������� �������� ��������,20FDA40AA101
      !{
      Call BOX_HANDLER(deltat,L_akut,
     & R_ufut,REAL(R_ekut,4),
     & L_ukut,R_okut,
     & REAL(R_alut,4),L_efut,R_afut,
     & REAL(R_ifut,4),L_elut,I_odut,L_ikut,L_ofut,
     & I_udut)
      !}
C FDA40_vlv.fgi(  23, 172):���������� �����,20FDA40_SHLUZ_UST_PRESS
      !{
      Call BOX_HANDLER(deltat,L_umut,
     & R_omut,REAL(R_aput,4),
     & L_oput,R_iput,
     & REAL(R_uput,4),L_amut,R_ulut,
     & REAL(R_emut,4),L_arut,I_ilut,L_eput,L_imut,
     & I_olut)
      !}
C FDA40_vlv.fgi(  82, 172):���������� �����,20FDA40_BOX_ZAGR_LOD
      !{
      Call BOX_HANDLER(deltat,L_osut,
     & R_isut,REAL(R_usut,4),
     & L_itut,R_etut,
     & REAL(R_otut,4),L_urut,R_orut,
     & REAL(R_asut,4),L_utut,I_erut,L_atut,L_esut,
     & I_irut)
      !}
C FDA40_vlv.fgi(  54, 172):���������� �����,20FDA40_BOX_PRESS_INSTR
      I_(227) = z'01000003'
C FDA40_logic.fgi(  68, 108):��������� ������������� IN (�������)
      I_(228) = z'01000009'
C FDA40_logic.fgi(  68, 106):��������� ������������� IN (�������)
      C30_emav = '������������������'
C FDA40_logic.fgi(  98, 165):��������� ���������� CH20 (CH30) (�������)
      L_(722)=.true.
C FDA40_logic.fgi(  47, 120):��������� ���������� (�������)
      L0_omav=R0_apav.ne.R0_umav
      R0_umav=R0_apav
C FDA40_logic.fgi(  34, 113):���������� ������������� ������
      if(L0_omav) then
         L_(729)=L_(722)
      else
         L_(729)=.false.
      endif
C FDA40_logic.fgi(  51, 119):���� � ������������� �������
      L_(4)=L_(729)
C FDA40_logic.fgi(  51, 119):������-�������: ���������� ��� �������������� ������
      I_(229) = z'01000003'
C FDA40_logic.fgi( 104,  97):��������� ������������� IN (�������)
      I_(230) = z'01000009'
C FDA40_logic.fgi( 104,  95):��������� ������������� IN (�������)
      I_(231) = z'01000003'
C FDA40_logic.fgi(  64,  97):��������� ������������� IN (�������)
      I_(232) = z'01000009'
C FDA40_logic.fgi(  64,  95):��������� ������������� IN (�������)
      L0_evav=R0_upav.ne.R0_opav
      R0_opav=R0_upav
C FDA40_logic.fgi(  34, 128):���������� ������������� ������
      L0_ivav=R0_erav.ne.R0_arav
      R0_arav=R0_erav
C FDA40_logic.fgi(  34, 145):���������� ������������� ������
      L0_ovav=R0_orav.ne.R0_irav
      R0_irav=R0_orav
C FDA40_logic.fgi(  34, 163):���������� ������������� ������
      C30_asav = '������������'
C FDA40_logic.fgi(  88, 162):��������� ���������� CH20 (CH30) (�������)
      C30_esav = '������'
C FDA40_logic.fgi(  82, 157):��������� ���������� CH20 (CH30) (�������)
      C30_usav = '��������������'
C FDA40_logic.fgi(  67, 158):��������� ���������� CH20 (CH30) (�������)
      C30_atav = '�� ������'
C FDA40_logic.fgi(  67, 160):��������� ���������� CH20 (CH30) (�������)
      I_(233) = z'01000003'
C FDA40_logic.fgi( 106, 108):��������� ������������� IN (�������)
      I_(234) = z'01000009'
C FDA40_logic.fgi( 106, 106):��������� ������������� IN (�������)
      L_(735)=.true.
C FDA40_logic.fgi(  47, 135):��������� ���������� (�������)
      if(L0_evav) then
         L_(732)=L_(735)
      else
         L_(732)=.false.
      endif
C FDA40_logic.fgi(  51, 134):���� � ������������� �������
      L_(3)=L_(732)
C FDA40_logic.fgi(  51, 134):������-�������: ���������� ��� �������������� ������
      L_(736)=.true.
C FDA40_logic.fgi(  47, 150):��������� ���������� (�������)
      if(L0_ivav) then
         L_(733)=L_(736)
      else
         L_(733)=.false.
      endif
C FDA40_logic.fgi(  51, 149):���� � ������������� �������
      L_(2)=L_(733)
C FDA40_logic.fgi(  51, 149):������-�������: ���������� ��� �������������� ������
      L_(730) = L_(2).OR.L_(3).OR.L_(4)
C FDA40_logic.fgi(  61, 166):���
      L_(737)=.true.
C FDA40_logic.fgi(  47, 170):��������� ���������� (�������)
      if(L0_ovav) then
         L_(734)=L_(737)
      else
         L_(734)=.false.
      endif
C FDA40_logic.fgi(  51, 169):���� � ������������� �������
      L_(1)=L_(734)
C FDA40_logic.fgi(  51, 169):������-�������: ���������� ��� �������������� ������
      L_(725) = L_(1).OR.L_(2).OR.L_(4)
C FDA40_logic.fgi(  61, 131):���
      L_otav=(L_(3).or.L_otav).and..not.(L_(725))
      L_(726)=.not.L_otav
C FDA40_logic.fgi(  69, 133):RS �������
      L_axav=L_otav
C FDA40_logic.fgi(  85, 135):������,FDA4_tech_mode
      if(L_axav) then
         I_epav=I_(230)
      else
         I_epav=I_(229)
      endif
C FDA40_logic.fgi( 107,  95):���� RE IN LO CH7
      L_(723) = L_(1).OR.L_(3).OR.L_(2)
C FDA40_logic.fgi(  61, 116):���
      L_imav=(L_(4).or.L_imav).and..not.(L_(723))
      L_(724)=.not.L_imav
C FDA40_logic.fgi(  69, 118):RS �������
      L_ixav=L_imav
C FDA40_logic.fgi(  85, 120):������,FDA4_avt_mode
      if(L_ixav) then
         I_amav=I_(228)
      else
         I_amav=I_(227)
      endif
C FDA40_logic.fgi(  71, 106):���� RE IN LO CH7
      L_(727) = L_(1).OR.L_(3).OR.L_(4)
C FDA40_logic.fgi(  61, 146):���
      L_utav=(L_(2).or.L_utav).and..not.(L_(727))
      L_(728)=.not.L_utav
C FDA40_logic.fgi(  69, 148):RS �������
      L_exav=L_utav
C FDA40_logic.fgi(  85, 150):������,FDA4_ruch_mode
      if(L_exav) then
         I_itav=I_(234)
      else
         I_itav=I_(233)
      endif
C FDA40_logic.fgi( 109, 106):���� RE IN LO CH7
      L_avav=(L_(1).or.L_avav).and..not.(L_(730))
      L_(731)=.not.L_avav
C FDA40_logic.fgi(  69, 168):RS �������
      L_uvav=L_avav
C FDA40_logic.fgi(  87, 170):������,FDA4_automatic_mode
      L_(738) = L_ixav.OR.L_exav.OR.L_axav.OR.L_uvav
C FDA40_logic.fgi(  51,  75):���
      if(L_uvav) then
         I_ipav=I_(232)
      else
         I_ipav=I_(231)
      endif
C FDA40_logic.fgi(  67,  95):���� RE IN LO CH7
      if(L_avav) then
         C30_osav=C30_usav
      else
         C30_osav=C30_atav
      endif
C FDA40_logic.fgi(  71, 158):���� RE IN LO CH20
      if(L_utav) then
         C30_isav=C30_esav
      else
         C30_isav=C30_osav
      endif
C FDA40_logic.fgi(  86, 157):���� RE IN LO CH20
      if(L_otav) then
         C30_urav=C30_asav
      else
         C30_urav=C30_isav
      endif
C FDA40_logic.fgi(  95, 156):���� RE IN LO CH20
      if(L_imav) then
         C30_etav=C30_emav
      else
         C30_etav=C30_urav
      endif
C FDA40_logic.fgi( 105, 155):���� RE IN LO CH20
      I_(235) = z'01000021'
C FDA40_logic.fgi(  52,  83):��������� ������������� IN (�������)
      I_(236) = z'01000004'
C FDA40_logic.fgi(  52,  81):��������� ������������� IN (�������)
      if(L_(738)) then
         I_oxav=I_(236)
      else
         I_oxav=I_(235)
      endif
C FDA40_logic.fgi(  55,  81):���� RE IN LO CH7
      call lin_ext(REAL(R_ebep,4),I2_imuf,R_(201),R_amuf,R_uluf
     &)
      R0_emuf=R_(201)
C FDA40_logic_press_1.fgi(  34, 523):�������-�������� ������� (20)
C label 1553  try1553=try1553-1
      R_irap = R0_emuf
C FDA40_logic_press_1.fgi(  41, 529):��������
      L_(451)=R_ebep.lt.R0_ufef
C FDA40_logic_press_step.fgi( 350, 549):���������� <
      L_(604)=R_ebep.gt.R0_apif
C FDA40_logic_press_step.fgi( 148, 902):���������� >
      call lin_ext(REAL(R_ibim,4),I2_oluf,R_(200),R_eluf,R_aluf
     &)
      R0_iluf=R_(200)
C FDA40_logic_press_1.fgi( 281, 530):�������-�������� ������� (20)
      R_orem = R0_iluf
C FDA40_logic_press_1.fgi( 288, 536):��������
      L_(483)=R_ibim.lt.R0_oref
C FDA40_logic_press_step.fgi( 385, 655):���������� <
      if(L_etef) then
         R0_usef=0.0
      elseif(L0_atef) then
         R0_usef=R0_osef
      else
         R0_usef=max(R_(160)-deltat,0.0)
      endif
      L_(488)=L_etef.or.R0_usef.gt.0.0
      L0_atef=L_etef
C FDA40_logic_press_step.fgi( 409, 681):�������� ������� ������
      L_(494)=R_ubom.gt.R0_itef
C FDA40_logic_press_step.fgi( 391, 692):���������� >
      L_(501)=R_ibim.gt.R0_ivef
C FDA40_logic_press_step.fgi( 385, 708):���������� >
      if(L_orif) then
         R0_obif=0.0
      elseif(L0_ubif) then
         R0_obif=R0_ibif
      else
         R0_obif=max(R_(169)-deltat,0.0)
      endif
      L_(545)=L_orif.or.R0_obif.gt.0.0
      L0_ubif=L_orif
C FDA40_logic_press_step.fgi( 409, 739):�������� ������� ������
      L_(556)=R_ubom.gt.R0_asif
C FDA40_logic_press_step.fgi( 391, 750):���������� >
      if(L_urif) then
         R0_edif=0.0
      elseif(L0_idif) then
         R0_edif=R0_adif
      else
         R0_edif=max(R_(170)-deltat,0.0)
      endif
      L_(549)=L_urif.or.R0_edif.gt.0.0
      L0_idif=L_urif
C FDA40_logic_press_step.fgi( 410, 784):�������� ������� ������
      L_(589)=R_ibim.gt.R0_atif
C FDA40_logic_press_step.fgi( 391, 795):���������� >
      if(L_afof.and..not.L0_udof) then
         R0_idof=R0_odof
      else
         R0_idof=max(R_(197)-deltat,0.0)
      endif
      L_(579)=R0_idof.gt.0.0
      L0_udof=L_afof
C FDA40_logic_press_step.fgi( 443, 862):������������  �� T
      L_(578)=.not.L_(584) .and.L_(579)
      L_(576)=L_(579).and..not.L_(577)
C FDA40_logic_press_step.fgi( 464, 862):��������� ������
      L_(594)=R_abem.lt.R0_upif
C FDA40_logic_press_step.fgi( 197, 951):���������� <
      L_akuf=R_ebep.gt.R0_ilu
C FDA40_logic_parameters.fgi( 193, 473):���������� >
      L_ifek=R_ibim.lt.R0_imu
C FDA40_logic_parameters.fgi(  35, 561):���������� <
      L_(223)=R_ubom.gt.R0_ibu
C FDA40_logic_parameters.fgi( 279, 533):���������� >
      L_(222)=R_ubom.lt.R0_ebu
C FDA40_logic_parameters.fgi( 279, 527):���������� <
      L_ikuf = L_(223).AND.L_(222)
C FDA40_logic_parameters.fgi( 283, 531):�
      L_(568)=R_iful.lt.R0_exif
C FDA40_logic_press_step.fgi( 391, 916):���������� <
      L_(574)=R_iful.gt.R0_oxif
C FDA40_logic_press_step.fgi( 391, 933):���������� >
      L_(618)=R_ebep.gt.R0_ikif
C FDA40_logic_press_step.fgi(  39, 936):���������� >
      L_(617)=R_ibim.lt.R0_ekif
C FDA40_logic_press_step.fgi(  39, 929):���������� <
      L_(613) = L_ikuf.AND.L_afuf
C FDA40_logic_press_step.fgi(  48, 920):�
      L_(227)=R_ubom.gt.R0_edu
C FDA40_logic_parameters.fgi( 279, 568):���������� >
      L_(224)=R_ubom.lt.R0_obu
C FDA40_logic_parameters.fgi( 279, 561):���������� <
      L_arak = L_(227).AND.L_(224)
C FDA40_logic_parameters.fgi( 283, 565):�
      L_(612) = L_arak.AND.L_uduf
C FDA40_logic_press_step.fgi(  48, 909):�
      L_(616) = L_(613).OR.L_(612)
C FDA40_logic_press_step.fgi(  56, 919):���
      L_(600)=R_abem.gt.R0_akif
C FDA40_logic_press_step.fgi(  39, 899):���������� >
      L_(599)=R_abem.lt.R0_ufif
C FDA40_logic_press_step.fgi(  39, 893):���������� <
      L_(615) = L_(600).AND.L_(599)
C FDA40_logic_press_step.fgi(  48, 898):�
      L_utal=R_omok.gt.R0_olok
C FDA40_logic_press.fgi( 199, 274):���������� >
      L_(211)=R_abem.gt.R0_oto
C FDA40_logic_parameters.fgi( 278, 394):���������� >
      L_(210)=R_abem.lt.R0_ito
C FDA40_logic_parameters.fgi( 278, 388):���������� <
      L_otal = L_(211).AND.L_(210)
C FDA40_logic_parameters.fgi( 282, 392):�
      L_ital = (.NOT.L_asok)
C FDA40_logic_press.fgi( 223, 237):�
      L_apom = L_(668).AND.L_utal.AND.(.NOT.L_otal).AND.L_ital
C FDA40_vlv.fgi( 246, 116):�
      L_aval=R0_osok.gt.R0_epok
C FDA40_logic_press.fgi( 199, 248):���������� >
      L_umom = L_(668).AND.L_aval
C FDA40_vlv.fgi( 246, 125):�
      !{
      Call BVALVE2_MAN(deltat,REAL(R_uvom,4),R8_osom,I_edum
     &,I_udum,I_adum,C8_itom,
     & I_odum,R_evom,R_avom,R_opom,REAL(R_arom,4),
     & R_urom,REAL(R_esom,4),R_erom,
     & REAL(R_orom,4),I_obum,I_afum,I_idum,I_ibum,L_isom,L_ifum
     &,
     & L_ikum,L_asom,L_etom,L_atom,L_ufum,
     & L_irom,L_upom,L_utom,L_alum,L_ivom,L_ovom,
     & REAL(R8_okot,8),REAL(1.0,4),R8_arot,L_umom,L_apom,L_ofum
     &,I_efum,
     & L_okum,R8_usom,R_ebum,REAL(R_otom,4),L_ukum,L_epom
     &,L_elum,L_ipom,
     & L_axom,L_exom,L_ixom,L_uxom,L_abum,L_oxom)
      !}

      if(L_abum.or.L_uxom.or.L_oxom.or.L_ixom.or.L_exom.or.L_axom
     &) then      
                  I_ubum = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_ubum = z'40000000'
      endif
C FDA40_vlv.fgi( 172, 117):���� ���������� �������� �������� ��� 2,20FDA40_KLAP
      L_(637) = L_utal.AND.L_ipom
C FDA40_logic_press.fgi( 120, 238):�
      if(L_(637)) then
         R_(313)=R_upok
      else
         R_(313)=R_(314)
      endif
C FDA40_logic_press.fgi( 142, 249):���� RE IN LO CH7
      L_esik=R0_osok.gt.R0_asik
C FDA40_logic_press.fgi( 199, 224):���������� >
      L_(215)=R_abem.gt.R0_ivo
C FDA40_logic_parameters.fgi( 278, 424):���������� >
      L_(214)=R_abem.lt.R0_evo
C FDA40_logic_parameters.fgi( 278, 418):���������� <
      L_ukuf = L_(215).AND.L_(214)
C FDA40_logic_parameters.fgi( 282, 422):�
      L_(213)=R_abem.gt.R0_avo
C FDA40_logic_parameters.fgi( 278, 409):���������� >
      L_(212)=R_abem.lt.R0_uto
C FDA40_logic_parameters.fgi( 278, 403):���������� <
      L_okuf = L_(213).AND.L_(212)
C FDA40_logic_parameters.fgi( 282, 407):�
      L_(633) = L_ukuf.OR.L_okuf
C FDA40_logic_press_1.fgi( 357, 558):���
      L_ipok = L_esik.AND.L_(633).AND.L_ifek
C FDA40_logic_press_1.fgi( 378, 558):�
      if(L_ipok) then
         R_(311)=R_opok
      else
         R_(311)=R_(312)
      endif
C FDA40_logic_press.fgi( 153, 245):���� RE IN LO CH7
      R_(315) = R_(313) + (-R_(311))
C FDA40_logic_press.fgi( 161, 248):��������
      R0_osok=R0_osok+deltat/R0_isok*R_(315)
      if(R0_osok.gt.R0_esok) then
         R0_osok=R0_esok
      elseif(R0_osok.lt.R0_usok) then
         R0_osok=R0_usok
      endif
C FDA40_logic_press.fgi( 169, 246):����������
      L_asok=R0_osok.gt.R0_adok
C FDA40_logic_press.fgi( 199, 242):���������� >
C sav1=L_ital
      L_ital = (.NOT.L_asok)
C FDA40_logic_press.fgi( 223, 237):recalc:�
C if(sav1.ne.L_ital .and. try1673.gt.0) goto 1673
      L_(192)=R_iral.lt.R0_omo
C FDA40_logic_box_step.fgi(  54, 688):���������� <
      if(L_iro.and..not.L0_ero) then
         R0_upo=R0_aro
      else
         R0_upo=max(R_(77)-deltat,0.0)
      endif
      L_(196)=R0_upo.gt.0.0
      L0_ero=L_iro
C FDA40_logic_box_step.fgi( 150, 708):������������  �� T
      L_(195)=.not.L_(202) .and.L_(196)
      L_(193)=L_(196).and..not.L_(194)
C FDA40_logic_box_step.fgi( 170, 708):��������� ������
      L_(155)=R_eruk.lt.R0_ovi
C FDA40_logic_box_step.fgi(  53, 462):���������� <
      if(L_ibo.and..not.L0_abo) then
         R0_oxi=R0_uxi
      else
         R0_oxi=max(R_(65)-deltat,0.0)
      endif
      L_ebo=R0_oxi.gt.0.0
      L0_abo=L_ibo
C FDA40_logic_box_step.fgi( 151, 473):������������  �� T
      L_(162)=R_eruk.gt.R0_obo
C FDA40_logic_box_step.fgi(  54, 498):���������� >
      if(L_afo.and..not.L0_odo) then
         R0_edo=R0_ido
      else
         R0_edo=max(R_(67)-deltat,0.0)
      endif
      L_udo=R0_edo.gt.0.0
      L0_odo=L_afo
C FDA40_logic_box_step.fgi( 151, 509):������������  �� T
      L_(172)=R_iral.lt.R0_ofo
C FDA40_logic_box_step.fgi(  55, 546):���������� <
      L_(180)=R_iral.gt.R0_uko
C FDA40_logic_box_step.fgi(  53, 630):���������� >
      if(.not.L_imo) then
         R0_amo=0.0
      elseif(.not.L0_emo) then
         R0_amo=R0_ulo
      else
         R0_amo=max(R_(73)-deltat,0.0)
      endif
      L_(181)=L_imo.and.R0_amo.le.0.0
      L0_emo=L_imo
C FDA40_logic_box_step.fgi(  87, 653):�������� ��������� ������
      L_(184) = L_(181).AND.L_ete
C FDA40_logic_box_step.fgi( 105, 652):�
      if(L_ixi.and..not.L0_exi) then
         R0_uvi=R0_axi
      else
         R0_uvi=max(R_(64)-deltat,0.0)
      endif
      L_(153)=R0_uvi.gt.0.0
      L0_exi=L_ixi
C FDA40_logic_box_step.fgi( 150, 462):������������  �� T
      L_(152)=.not.L_(202) .and.L_(153)
      L_(150)=L_(153).and..not.L_(151)
C FDA40_logic_box_step.fgi( 170, 462):��������� ������
      L_(63)=R_eruk.lt.R0_ux
C FDA40_logic_box_step.fgi(  53, 145):���������� <
      if(L_ode.and..not.L0_ede) then
         R0_ube=R0_ade
      else
         R0_ube=max(R_(24)-deltat,0.0)
      endif
      L_ide=R0_ube.gt.0.0
      L0_ede=L_ode
C FDA40_logic_box_step.fgi( 151, 171):������������  �� T
      L_(71)=R_eruk.gt.R0_ude
C FDA40_logic_box_step.fgi(  54, 196):���������� >
      L_(84)=R_iral.gt.R0_ole
C FDA40_logic_box_step.fgi(  55, 222):���������� >
      L_(140)=R_uxum.lt.R0_eti
C FDA40_logic_box_step.fgi(  56, 405):���������� <
      L_ekof=I_exaf.eq.I_(198)
C FDA40_logic_press_step.fgi( 427, 379):���������� �������������
      L_(420)=R_ebep.gt.R0_itaf
C FDA40_logic_press_step.fgi( 381, 424):���������� >
      L_(414)=R_ubom.lt.R0_uraf
C FDA40_logic_press_step.fgi( 363, 452):���������� <
      L_(427)=R_ubom.lt.R0_ixaf
C FDA40_logic_press_step.fgi( 365, 475):���������� <
      L_(434) = L_(427).OR.(.NOT.L_oxaf)
C FDA40_logic_press_step.fgi( 372, 484):���
      L_(445)=R_ebep.lt.R0_udef
C FDA40_logic_press_step.fgi( 350, 514):���������� <
      I_ovaf=I_exaf
C FDA40_logic_press_step.fgi( 451, 384):������,FDA40_N_PRESS_CYCLES
      L_(21)=I_ovaf.gt.I1_axaf
C FDA40_logic_box_step.fgi(  88, 393):���������� �������������
      L_(143) = L_(140).AND.L_(21)
C FDA40_logic_box_step.fgi(  98, 404):�
      L_(149)=R_iral.lt.R0_oti
C FDA40_logic_box_step.fgi(  54, 439):���������� <
      if(L_iti.and..not.L0_ati) then
         R0_osi=R0_usi
      else
         R0_osi=max(R_(60)-deltat,0.0)
      endif
      L_(139)=R0_osi.gt.0.0
      L0_ati=L_iti
C FDA40_logic_box_step.fgi( 150, 404):������������  �� T
      L_(138)=.not.L_(202) .and.L_(139)
      L_(136)=L_(139).and..not.L_(137)
C FDA40_logic_box_step.fgi( 174, 404):��������� ������
      L_(97)=R_uxum.lt.R0_ore
C FDA40_logic_box_step.fgi(  57, 292):���������� <
      L_(22)=I_ovaf.eq.I_(2)
C FDA40_logic_box_step.fgi(  78, 283):���������� �������������
      L_(99) = L_(97).AND.L_(22)
C FDA40_logic_box_step.fgi(  96, 291):�
      L_(105)=R_iral.lt.R0_ase
C FDA40_logic_box_step.fgi(  54, 319):���������� <
      L_(135)=R_uxum.gt.R0_asi
C FDA40_logic_box_step.fgi(  54, 339):���������� >
      L_(92)=I_api.gt.I_ote
C FDA40_logic_box_step.fgi(  57, 264):���������� �������������
      if(.not.L_uv) then
         R0_iv=0.0
      elseif(.not.L0_ov) then
         R0_iv=R0_ev
      else
         R0_iv=max(R_(20)-deltat,0.0)
      endif
      L_(55)=L_uv.and.R0_iv.le.0.0
      L0_ov=L_uv
C FDA40_logic_box_step.fgi(  87, 114):�������� ��������� ������
      L_(54)=R_iral.gt.R0_et
C FDA40_logic_box_step.fgi(  53,  91):���������� >
      L0_ak=L_ek.or.(L0_ak.and..not.(L_up))
      L_(18)=.not.L0_ak
C FDA40_logic_box_step.fgi(  90, 107):RS �������
      L_(57) = L_(55).AND.L0_ak
C FDA40_logic_box_step.fgi( 105, 113):�
      L_(207)=R_uxum.gt.R0_eso
C FDA40_logic_box_step.fgi(  53, 730):���������� >
      if(L_oso) then
          if (L_(208)) then
              I_(30) = 1
              L_iso = .true.
              L_(206) = .false.
          endif
          if (L_(207)) then
              L_(206) = .true.
              L_iso = .false.
          endif
          if (I_(30).ne.1) then
              L_iso = .false.
              L_(206) = .false.
          endif
      else
          L_(206) = .false.
      endif
C FDA40_logic_box_step.fgi( 124, 730):��� ������� ���������,FDA40_BOX_AVT
      L_(39)=R_iral.lt.R0_um
C FDA40_logic_box_step.fgi(  55,  49):���������� <
      L_(26)=I_i.gt.I_(5)
C FDA40_logic_box_step.fgi( 330, 313):���������� �������������
      L_(25)=I_i.lt.I_(4)
C FDA40_logic_box_step.fgi( 330, 302):���������� �������������
      L_(27) = L_(26).AND.L_(25)
C FDA40_logic_box_step.fgi( 335, 308):�
      if(L_(27)) then
         I_(9)=I_(7)
      else
         I_(9)=I_(8)
      endif
C FDA40_logic_box_step.fgi( 339, 317):���� RE IN LO CH7
      if(L_eri) then
         I_ote=I1_axaf
      else
         I_ote=I_(9)
      endif
C FDA40_logic_box_step.fgi( 351, 294):���� RE IN LO CH7
      L_(128)=I_opi.gt.I_ote
C FDA40_logic_box_step.fgi( 346, 398):���������� �������������
      L_ari=L_eri
C FDA40_logic_box_step.fgi( 166, 371):������,FDA40_BOAT_START_LOAD1
      L_ope=L_upe
C FDA40_logic_box_step.fgi( 166, 264):������,FDA40_BOAT_START_LOAD2
      L_(127) = L_ari.OR.L_ope
C FDA40_logic_box_step.fgi( 283, 414):���
      L_upi=(L_(127).or.L_upi).and..not.(L_(128))
      L_(129)=.not.L_upi
C FDA40_logic_box_step.fgi( 297, 412):RS �������
      if (.not.L_(126).and.L_upi) then
          I_opi = I_opi+1
      endif
      if (L_(126)) then
          I_opi = 0
      endif
C FDA40_logic_box_step.fgi( 343, 411):�������
      I_api=I_opi
C FDA40_logic_box_step.fgi( 366, 404):������,FDA40_NUM_PRESS_CYCLES
      L_(106)=I_api.gt.I_ote
C FDA40_logic_box_step.fgi(  69, 371):���������� �������������
      if(L_oso) then
          if (L_esi) then
              I_(30) = 16
              L_isi = .true.
              L_(134) = .false.
          endif
          if (L_(135)) then
              L_(134) = .true.
              L_isi = .false.
          endif
          if (I_(30).ne.16) then
              L_isi = .false.
              L_(134) = .false.
          endif
      else
          L_(134) = .false.
      endif
C FDA40_logic_box_step.fgi( 124, 339):��� ������� ���������,FDA40_BOX_AVT
      if(L_oso) then
          if (L_(134)) then
              I_(30) = 17
              L_use = .true.
              L_(104) = .false.
          endif
          if (L_(105)) then
              L_(104) = .true.
              L_use = .false.
          endif
          if (I_(30).ne.17) then
              L_use = .false.
              L_(104) = .false.
          endif
      else
          L_(104) = .false.
      endif
C FDA40_logic_box_step.fgi( 124, 319):��� ������� ���������,FDA40_BOX_AVT
      if(L_oso) then
          if (L_(104)) then
              I_(30) = 18
              L_ure = .true.
              L_(98) = .false.
          endif
          if (L_(99)) then
              L_(98) = .true.
              L_ure = .false.
          endif
          if (I_(30).ne.18) then
              L_ure = .false.
              L_(98) = .false.
          endif
      else
          L_(98) = .false.
      endif
C FDA40_logic_box_step.fgi( 124, 291):��� ������� ���������,FDA40_BOX_AVT
      if(L_ure.and..not.L0_ire) then
         R0_are=R0_ere
      else
         R0_are=max(R_(36)-deltat,0.0)
      endif
      L_(96)=R0_are.gt.0.0
      L0_ire=L_ure
C FDA40_logic_box_step.fgi( 150, 291):������������  �� T
      L_(95)=.not.L_(202) .and.L_(96)
      L_(93)=L_(96).and..not.L_(94)
C FDA40_logic_box_step.fgi( 174, 292):��������� ������
      iv2=0
      if(L_(138)) iv2=ibset(iv2,0)
      if(L_(95)) iv2=ibset(iv2,1)
      L_(710)=L_(710).or.iv2.ne.0
C FDA40_logic_box_step.fgi( 174, 404):������-�������: ������� ������ ������/����������/����� ���������
      if(L_iso.and..not.L0_aso) then
         R0_oro=R0_uro
      else
         R0_oro=max(R_(79)-deltat,0.0)
      endif
      L_(205)=R0_oro.gt.0.0
      L0_aso=L_iso
C FDA40_logic_box_step.fgi( 150, 730):������������  �� T
      L_(203)=.not.L_(202) .and.L_(205)
      L_(200)=L_(205).and..not.L_(201)
C FDA40_logic_box_step.fgi( 174, 730):��������� ������
      if(L_isi.and..not.L0_uri) then
         R0_iri=R0_ori
      else
         R0_iri=max(R_(58)-deltat,0.0)
      endif
      L_(133)=R0_iri.gt.0.0
      L0_uri=L_isi
C FDA40_logic_box_step.fgi( 150, 339):������������  �� T
      L_(132)=.not.L_(202) .and.L_(133)
      L_(130)=L_(133).and..not.L_(131)
C FDA40_logic_box_step.fgi( 174, 340):��������� ������
      if(L_ipe.and..not.L0_ape) then
         R0_ome=R0_ume
      else
         R0_ome=max(R_(34)-deltat,0.0)
      endif
      L_(88)=R0_ome.gt.0.0
      L0_ape=L_ipe
C FDA40_logic_box_step.fgi( 150, 248):������������  �� T
      L_(87)=.not.L_(202) .and.L_(88)
      L_(85)=L_(88).and..not.L_(86)
C FDA40_logic_box_step.fgi( 174, 248):��������� ������
      iv2=0
      if(L_(203)) iv2=ibset(iv2,0)
      if(L_(132)) iv2=ibset(iv2,1)
      if(L_(87)) iv2=ibset(iv2,2)
      L_(709)=L_(709).or.iv2.ne.0
C FDA40_vlv.fgi(  91, 121):������-�������: ������� ������ ������/����������/����� ���������
      Call TELEGKA_TRANSP_HANDLER(deltat,L_uvum,L_(706),L_edap
     &,L_(707),R_ilap,
     & R_olap,R_ifap,R_afap,L_alap,R_ofap,R_efap,
     & L_elap,L_olum,L_ulum,L_(708),L_ubap,R_odap,R_udap,
     & R_ukap,R_uxum,R_okap,L_ibap,L_ebap,R_erum,
     & REAL(R_arum,4),R_utum,REAL(R_evum,4),L_itum,
     & R_etum,REAL(R_otum,4),L_orum,R_irum,
     & REAL(R_urum,4),L_idap,L_(711),L_(712),L_avum,
     & L_ixum,L_ufap,L_(710),L_(709),L_akap,L_ikap,L_abap
     &)
C FDA40_vlv.fgi(  91, 121):���������� ������� ������������,20FDA40_STICK_LOADER
      L_(90)=R_uxum.gt.R0_epe
C FDA40_logic_box_step.fgi(  54, 248):���������� >
      if(L_oso) then
          if (L_(98)) then
              I_(30) = 19
              L_upe = .true.
              L_(91) = .false.
          endif
          if (L_(92)) then
              L_(91) = .true.
              L_upe = .false.
          endif
          if (I_(30).ne.19) then
              L_upe = .false.
              L_(91) = .false.
          endif
      else
          L_(91) = .false.
      endif
C FDA40_logic_box_step.fgi( 124, 264):��� ������� ���������,FDA40_BOX_AVT
C sav1=L_ope
C FDA40_logic_box_step.fgi( 166, 264):recalc:������,FDA40_BOAT_START_LOAD2
C if(sav1.ne.L_ope .and. try1902.gt.0) goto 1902
      if(L_oso) then
          if (L_(91)) then
              I_(30) = 20
              L_ipe = .true.
              L_(89) = .false.
          endif
          if (L_(90)) then
              L_(89) = .true.
              L_ipe = .false.
          endif
          if (I_(30).ne.20) then
              L_ipe = .false.
              L_(89) = .false.
          endif
      else
          L_(89) = .false.
      endif
C FDA40_logic_box_step.fgi( 124, 248):��� ������� ���������,FDA40_BOX_AVT
C sav1=L_(88)
      if(L_ipe.and..not.L0_ape) then
         R0_ome=R0_ume
      else
         R0_ome=max(R_(34)-deltat,0.0)
      endif
      L_(88)=R0_ome.gt.0.0
      L0_ape=L_ipe
C FDA40_logic_box_step.fgi( 150, 248):recalc:������������  �� T
C if(sav1.ne.L_(88) .and. try1942.gt.0) goto 1942
      if(L_oso) then
          if (L_(89)) then
              I_(30) = 21
              L_ime = .true.
              L_(83) = .false.
          endif
          if (L_(84)) then
              L_(83) = .true.
              L_ime = .false.
          endif
          if (I_(30).ne.21) then
              L_ime = .false.
              L_(83) = .false.
          endif
      else
          L_(83) = .false.
      endif
C FDA40_logic_box_step.fgi( 124, 222):��� ������� ���������,FDA40_BOX_AVT
      if(.not.L_(83)) then
         R0_oke=0.0
      elseif(.not.L0_uke) then
         R0_oke=R0_ike
      else
         R0_oke=max(R_(28)-deltat,0.0)
      endif
      L_(72)=L_(83).and.R0_oke.le.0.0
      L0_uke=L_(83)
C FDA40_logic_box_step.fgi( 132, 212):�������� ��������� ������
      if(L_oso) then
          if (L_(72)) then
              I_(30) = 22
              L_ofe = .true.
              L_(70) = .false.
          endif
          if (L_(71)) then
              L_(70) = .true.
              L_ofe = .false.
          endif
          if (I_(30).ne.22) then
              L_ofe = .false.
              L_(70) = .false.
          endif
      else
          L_(70) = .false.
      endif
C FDA40_logic_box_step.fgi( 124, 196):��� ������� ���������,FDA40_BOX_AVT
      if(.not.L_(70)) then
         R0_ix=0.0
      elseif(.not.L0_ox) then
         R0_ix=R0_ex
      else
         R0_ix=max(R_(21)-deltat,0.0)
      endif
      L_(64)=L_(70).and.R0_ix.le.0.0
      L0_ox=L_(70)
C FDA40_logic_box_step.fgi( 132, 183):�������� ��������� ������
      if(L_oso) then
          if (L_(64)) then
              I_(30) = 23
              L_ode = .true.
              L_(65) = .false.
          endif
          if (L_ide) then
              L_(65) = .true.
              L_ode = .false.
          endif
          if (I_(30).ne.23) then
              L_ode = .false.
              L_(65) = .false.
          endif
      else
          L_(65) = .false.
      endif
C FDA40_logic_box_step.fgi( 124, 171):��� ������� ���������,FDA40_BOX_AVT
C sav1=L_ide
      if(L_ode.and..not.L0_ede) then
         R0_ube=R0_ade
      else
         R0_ube=max(R_(24)-deltat,0.0)
      endif
      L_ide=R0_ube.gt.0.0
      L0_ede=L_ode
C FDA40_logic_box_step.fgi( 151, 171):recalc:������������  �� T
C if(sav1.ne.L_ide .and. try1801.gt.0) goto 1801
      if(L_oso) then
          if (L_(65)) then
              I_(30) = 24
              L_obe = .true.
              L_(62) = .false.
          endif
          if (L_(63)) then
              L_(62) = .true.
              L_obe = .false.
          endif
          if (I_(30).ne.24) then
              L_obe = .false.
              L_(62) = .false.
          endif
      else
          L_(62) = .false.
      endif
C FDA40_logic_box_step.fgi( 124, 145):��� ������� ���������,FDA40_BOX_AVT
      if(L_obe.and..not.L0_ibe) then
         R0_abe=R0_ebe
      else
         R0_abe=max(R_(23)-deltat,0.0)
      endif
      L_(61)=R0_abe.gt.0.0
      L0_ibe=L_obe
C FDA40_logic_box_step.fgi( 150, 145):������������  �� T
      L_(60)=.not.L_(202) .and.L_(61)
      L_(58)=L_(61).and..not.L_(59)
C FDA40_logic_box_step.fgi( 170, 146):��������� ������
      iv2=0
      if(L_(195)) iv2=ibset(iv2,0)
      if(L_(152)) iv2=ibset(iv2,1)
      if(L_(60)) iv2=ibset(iv2,2)
      L_(652)=L_(652).or.iv2.ne.0
C FDA40_vlv.fgi(  63, 122):������-�������: ������� ������ ������/����������/����� ���������
      L_(160)=.not.L_(202) .and.L_ubo
      L_(158)=L_ubo.and..not.L_(159)
C FDA40_logic_box_step.fgi( 170, 498):��������� ������
      if(L_ofe.and..not.L0_ife) then
         R0_afe=R0_efe
      else
         R0_afe=max(R_(26)-deltat,0.0)
      endif
      L_(69)=R0_afe.gt.0.0
      L0_ife=L_ofe
C FDA40_logic_box_step.fgi( 150, 196):������������  �� T
      L_(68)=.not.L_(202) .and.L_(69)
      L_(66)=L_(69).and..not.L_(67)
C FDA40_logic_box_step.fgi( 170, 196):��������� ������
      iv2=0
      if(L_(160)) iv2=ibset(iv2,0)
      if(L_(68)) iv2=ibset(iv2,1)
      L_(640)=L_(640).or.iv2.ne.0
C FDA40_logic_box_step.fgi( 170, 498):������-�������: ������� ������ ������/����������/����� ���������
      Call MECHANIC_PRIVOD(deltat,REAL(950,4),L_imuk,L_(651
     &),
     & REAL(900,4),L_axok,L_(642),REAL(800,4),L_uxok,L_(643
     &),
     & REAL(700,4),L_obuk,L_(644),REAL(600,4),L_iduk,L_(645
     &),
     & REAL(500,4),L_efuk,L_(646),REAL(400,4),L_akuk,L_(647
     &),
     & REAL(300,4),L_ukuk,L_(648),REAL(200,4),L_oluk,L_(649
     &),
     & L_amuk,L_ovok,L_ixok,L_ebuk,L_aduk,L_omuk,L_exok,
     & L_abuk,L_ubuk,L_oduk,L_emuk,L_uvok,L_oxok,
     & L_ibuk,L_eduk,L_uduk,L_ifuk,L_afuk,L_ofuk,L_ikuk,
     & L_eluk,L_ekuk,L_aluk,L_uluk,L_ufuk,L_okuk,
     & L_iluk,REAL(50,4),L_apuk,L_umuk,L_ipuk,L_epuk,
     & L_(650),L_avok,L_(638),L_uruk,L_(639),R_ovel,L_asuk
     &,L_esuk,
     & REAL(20,4),L_aruk,L_(652),L_opuk,L_upuk,L_(641),
     & L_(640),INT(I_evok,4),L_ivok,INT(I_iruk,4),L_oruk,
     & INT(I_otok,4),L_utok,R_eruk,REAL(100,4),REAL(0,4),R_usuk
     &,L_etok,
     & L_itok,L_isuk,L_osuk)
C FDA40_vlv.fgi(  63, 122):������,20FDA91AE013
      L_(199)=R_eruk.lt.R0_opo
C FDA40_logic_box_step.fgi(  53, 708):���������� <
      if(.not.L_(206)) then
         R0_ufi=0.0
      elseif(.not.L0_aki) then
         R0_ufi=R0_ofi
      else
         R0_ufi=max(R_(47)-deltat,0.0)
      endif
      L_(197)=L_(206).and.R0_ufi.le.0.0
      L0_aki=L_(206)
C FDA40_logic_box_step.fgi( 132, 719):�������� ��������� ������
      if(L_oso) then
          if (L_(197)) then
              I_(30) = 2
              L_iro = .true.
              L_(198) = .false.
          endif
          if (L_(199)) then
              L_(198) = .true.
              L_iro = .false.
          endif
          if (I_(30).ne.2) then
              L_iro = .false.
              L_(198) = .false.
          endif
      else
          L_(198) = .false.
      endif
C FDA40_logic_box_step.fgi( 124, 708):��� ������� ���������,FDA40_BOX_AVT
C sav1=L_(196)
      if(L_iro.and..not.L0_ero) then
         R0_upo=R0_aro
      else
         R0_upo=max(R_(77)-deltat,0.0)
      endif
      L_(196)=R0_upo.gt.0.0
      L0_ero=L_iro
C FDA40_logic_box_step.fgi( 150, 708):recalc:������������  �� T
C if(sav1.ne.L_(196) .and. try1764.gt.0) goto 1764
      if(.not.L_(198)) then
         R0_efi=0.0
      elseif(.not.L0_ifi) then
         R0_efi=R0_afi
      else
         R0_efi=max(R_(46)-deltat,0.0)
      endif
      L_(190)=L_(198).and.R0_efi.le.0.0
      L0_ifi=L_(198)
C FDA40_logic_box_step.fgi( 132, 698):�������� ��������� ������
      if(L_oso) then
          if (L_(190)) then
              I_(30) = 3
              L_ipo = .true.
              L_(191) = .false.
          endif
          if (L_(192)) then
              L_(191) = .true.
              L_ipo = .false.
          endif
          if (I_(30).ne.3) then
              L_ipo = .false.
              L_(191) = .false.
          endif
      else
          L_(191) = .false.
      endif
C FDA40_logic_box_step.fgi( 124, 688):��� ������� ���������,FDA40_BOX_AVT
      if(L_ipo.and..not.L0_epo) then
         R0_umo=R0_apo
      else
         R0_umo=max(R_(75)-deltat,0.0)
      endif
      L_(189)=R0_umo.gt.0.0
      L0_epo=L_ipo
C FDA40_logic_box_step.fgi( 150, 688):������������  �� T
      L_(188)=.not.L_(202) .and.L_(189)
      L_(186)=L_(189).and..not.L_(187)
C FDA40_logic_box_step.fgi( 170, 688):��������� ������
      if(.not.L_(191)) then
         R0_odi=0.0
      elseif(.not.L0_udi) then
         R0_odi=R0_idi
      else
         R0_odi=max(R_(45)-deltat,0.0)
      endif
      L_(182)=L_(191).and.R0_odi.le.0.0
      L0_udi=L_(191)
C FDA40_logic_box_step.fgi( 132, 669):�������� ��������� ������
      if(L_oso) then
          if (L_(182)) then
              I_(30) = 4
              L_imo = .true.
              L_(183) = .false.
          endif
          if (L_(184)) then
              L_(183) = .true.
              L_imo = .false.
          endif
          if (I_(30).ne.4) then
              L_imo = .false.
              L_(183) = .false.
          endif
      else
          L_(183) = .false.
      endif
C FDA40_logic_box_step.fgi( 124, 652):��� ������� ���������,FDA40_BOX_AVT
C sav1=L_(181)
      if(.not.L_imo) then
         R0_amo=0.0
      elseif(.not.L0_emo) then
         R0_amo=R0_ulo
      else
         R0_amo=max(R_(73)-deltat,0.0)
      endif
      L_(181)=L_imo.and.R0_amo.le.0.0
      L0_emo=L_imo
C FDA40_logic_box_step.fgi(  87, 653):recalc:�������� ��������� ������
C if(sav1.ne.L_(181) .and. try1793.gt.0) goto 1793
      if(L_oso) then
          if (L_(183)) then
              I_(30) = 5
              L_olo = .true.
              L_(179) = .false.
          endif
          if (L_(180)) then
              L_(179) = .true.
              L_olo = .false.
          endif
          if (I_(30).ne.5) then
              L_olo = .false.
              L_(179) = .false.
          endif
      else
          L_(179) = .false.
      endif
C FDA40_logic_box_step.fgi( 124, 630):��� ������� ���������,FDA40_BOX_AVT
      if(.not.L_(179)) then
         R0_adi=0.0
      elseif(.not.L0_edi) then
         R0_adi=R0_ubi
      else
         R0_adi=max(R_(44)-deltat,0.0)
      endif
      L_(173)=L_(179).and.R0_adi.le.0.0
      L0_edi=L_(179)
C FDA40_logic_box_step.fgi( 132, 614):�������� ��������� ������
      if(L_oso) then
          if (L_(173)) then
              I_(30) = 6
              L_oko = .true.
              L_(174) = .false.
          endif
          if (L_asal) then
              L_(174) = .true.
              L_oko = .false.
          endif
          if (I_(30).ne.6) then
              L_oko = .false.
              L_(174) = .false.
          endif
      else
          L_(174) = .false.
      endif
C FDA40_logic_box_step.fgi( 124, 557):��� ������� ���������,FDA40_BOX_AVT
      if(L_oso) then
          if (L_(174)) then
              I_(30) = 7
              L_iko = .true.
              L_(171) = .false.
          endif
          if (L_(172)) then
              L_(171) = .true.
              L_iko = .false.
          endif
          if (I_(30).ne.7) then
              L_iko = .false.
              L_(171) = .false.
          endif
      else
          L_(171) = .false.
      endif
C FDA40_logic_box_step.fgi( 124, 546):��� ������� ���������,FDA40_BOX_AVT
      if(L_iko.and..not.L0_eko) then
         R0_ufo=R0_ako
      else
         R0_ufo=max(R_(70)-deltat,0.0)
      endif
      L_(170)=R0_ufo.gt.0.0
      L0_eko=L_iko
C FDA40_logic_box_step.fgi( 150, 546):������������  �� T
      L_(169)=.not.L_(202) .and.L_(170)
      L_(167)=L_(170).and..not.L_(168)
C FDA40_logic_box_step.fgi( 170, 546):��������� ������
      if(.not.L_(171)) then
         R0_ibi=0.0
      elseif(.not.L0_obi) then
         R0_ibi=R0_ebi
      else
         R0_ibi=max(R_(43)-deltat,0.0)
      endif
      L_(164)=L_(171).and.R0_ibi.le.0.0
      L0_obi=L_(171)
C FDA40_logic_box_step.fgi( 132, 536):�������� ��������� ������
      if(L_oso) then
          if (L_(164)) then
              I_(30) = 8
              L_ifo = .true.
              L_(165) = .false.
          endif
          if (L_(166)) then
              L_(165) = .true.
              L_ifo = .false.
          endif
          if (I_(30).ne.8) then
              L_ifo = .false.
              L_(165) = .false.
          endif
      else
          L_(165) = .false.
      endif
C FDA40_logic_box_step.fgi( 124, 522):��� ������� ���������,FDA40_BOX_AVT
      if(L_oso) then
          if (L_(165)) then
              I_(30) = 9
              L_afo = .true.
              L_(163) = .false.
          endif
          if (L_udo) then
              L_(163) = .true.
              L_afo = .false.
          endif
          if (I_(30).ne.9) then
              L_afo = .false.
              L_(163) = .false.
          endif
      else
          L_(163) = .false.
      endif
C FDA40_logic_box_step.fgi( 124, 509):��� ������� ���������,FDA40_BOX_AVT
C sav1=L_udo
      if(L_afo.and..not.L0_odo) then
         R0_edo=R0_ido
      else
         R0_edo=max(R_(67)-deltat,0.0)
      endif
      L_udo=R0_edo.gt.0.0
      L0_odo=L_afo
C FDA40_logic_box_step.fgi( 151, 509):recalc:������������  �� T
C if(sav1.ne.L_udo .and. try1777.gt.0) goto 1777
      if(L_oso) then
          if (L_(163)) then
              I_(30) = 10
              L_ubo = .true.
              L_(161) = .false.
          endif
          if (L_(162)) then
              L_(161) = .true.
              L_ubo = .false.
          endif
          if (I_(30).ne.10) then
              L_ubo = .false.
              L_(161) = .false.
          endif
      else
          L_(161) = .false.
      endif
C FDA40_logic_box_step.fgi( 124, 498):��� ������� ���������,FDA40_BOX_AVT
C sav1=L_(160)
C sav2=L_(159)
C sav3=L_(158)
C sav4=R_(66)
C FDA40_logic_box_step.fgi( 170, 498):recalc:��������� ������
C if(sav1.ne.L_(160) .and. try1992.gt.0) goto 1992
C if(sav2.ne.L_(159) .and. try1992.gt.0) goto 1992
C if(sav3.ne.L_(158) .and. try1992.gt.0) goto 1992
C if(sav4.ne.R_(66) .and. try1992.gt.0) goto 1992
      if(.not.L_(161)) then
         R0_exe=0.0
      elseif(.not.L0_ixe) then
         R0_exe=R0_axe
      else
         R0_exe=max(R_(41)-deltat,0.0)
      endif
      L_(156)=L_(161).and.R0_exe.le.0.0
      L0_ixe=L_(161)
C FDA40_logic_box_step.fgi( 132, 485):�������� ��������� ������
      if(L_oso) then
          if (L_(156)) then
              I_(30) = 11
              L_ibo = .true.
              L_(157) = .false.
          endif
          if (L_ebo) then
              L_(157) = .true.
              L_ibo = .false.
          endif
          if (I_(30).ne.11) then
              L_ibo = .false.
              L_(157) = .false.
          endif
      else
          L_(157) = .false.
      endif
C FDA40_logic_box_step.fgi( 124, 473):��� ������� ���������,FDA40_BOX_AVT
C sav1=L_ebo
      if(L_ibo.and..not.L0_abo) then
         R0_oxi=R0_uxi
      else
         R0_oxi=max(R_(65)-deltat,0.0)
      endif
      L_ebo=R0_oxi.gt.0.0
      L0_abo=L_ibo
C FDA40_logic_box_step.fgi( 151, 473):recalc:������������  �� T
C if(sav1.ne.L_ebo .and. try1769.gt.0) goto 1769
      if(L_oso) then
          if (L_(157)) then
              I_(30) = 12
              L_ixi = .true.
              L_(154) = .false.
          endif
          if (L_(155)) then
              L_(154) = .true.
              L_ixi = .false.
          endif
          if (I_(30).ne.12) then
              L_ixi = .false.
              L_(154) = .false.
          endif
      else
          L_(154) = .false.
      endif
C FDA40_logic_box_step.fgi( 124, 462):��� ������� ���������,FDA40_BOX_AVT
C sav1=L_(153)
      if(L_ixi.and..not.L0_exi) then
         R0_uvi=R0_axi
      else
         R0_uvi=max(R_(64)-deltat,0.0)
      endif
      L_(153)=R0_uvi.gt.0.0
      L0_exi=L_ixi
C FDA40_logic_box_step.fgi( 150, 462):recalc:������������  �� T
C if(sav1.ne.L_(153) .and. try1796.gt.0) goto 1796
      if(L_oso) then
          if (L_(154)) then
              I_(30) = 13
              L_ivi = .true.
              L_(148) = .false.
          endif
          if (L_(149)) then
              L_(148) = .true.
              L_ivi = .false.
          endif
          if (I_(30).ne.13) then
              L_ivi = .false.
              L_(148) = .false.
          endif
      else
          L_(148) = .false.
      endif
C FDA40_logic_box_step.fgi( 124, 439):��� ������� ���������,FDA40_BOX_AVT
      if(L_ivi.and..not.L0_evi) then
         R0_uti=R0_avi
      else
         R0_uti=max(R_(62)-deltat,0.0)
      endif
      L_(147)=R0_uti.gt.0.0
      L0_evi=L_ivi
C FDA40_logic_box_step.fgi( 150, 439):������������  �� T
      L_(146)=.not.L_(202) .and.L_(147)
      L_(144)=L_(147).and..not.L_(145)
C FDA40_logic_box_step.fgi( 170, 440):��������� ������
      if(L_use.and..not.L0_ose) then
         R0_ese=R0_ise
      else
         R0_ese=max(R_(38)-deltat,0.0)
      endif
      L_(103)=R0_ese.gt.0.0
      L0_ose=L_use
C FDA40_logic_box_step.fgi( 150, 319):������������  �� T
      L_(102)=.not.L_(202) .and.L_(103)
      L_(100)=L_(103).and..not.L_(101)
C FDA40_logic_box_step.fgi( 170, 320):��������� ������
      if(L_oso) then
          if (L_(62)) then
              I_(30) = 25
              L_uv = .true.
              L_(56) = .false.
          endif
          if (L_(57)) then
              L_(56) = .true.
              L_uv = .false.
          endif
          if (I_(30).ne.25) then
              L_uv = .false.
              L_(56) = .false.
          endif
      else
          L_(56) = .false.
      endif
C FDA40_logic_box_step.fgi( 124, 113):��� ������� ���������,FDA40_BOX_AVT
C sav1=L_(55)
      if(.not.L_uv) then
         R0_iv=0.0
      elseif(.not.L0_ov) then
         R0_iv=R0_ev
      else
         R0_iv=max(R_(20)-deltat,0.0)
      endif
      L_(55)=L_uv.and.R0_iv.le.0.0
      L0_ov=L_uv
C FDA40_logic_box_step.fgi(  87, 114):recalc:�������� ��������� ������
C if(sav1.ne.L_(55) .and. try1863.gt.0) goto 1863
      if(L_oso) then
          if (L_(56)) then
              I_(30) = 26
              L_av = .true.
              L_(53) = .false.
          endif
          if (L_(54)) then
              L_(53) = .true.
              L_av = .false.
          endif
          if (I_(30).ne.26) then
              L_av = .false.
              L_(53) = .false.
          endif
      else
          L_(53) = .false.
      endif
C FDA40_logic_box_step.fgi( 124,  91):��� ������� ���������,FDA40_BOX_AVT
      if(.not.L_(53)) then
         R0_or=0.0
      elseif(.not.L0_ur) then
         R0_or=R0_ir
      else
         R0_or=max(R_(14)-deltat,0.0)
      endif
      L_(42)=L_(53).and.R0_or.le.0.0
      L0_ur=L_(53)
C FDA40_logic_box_step.fgi( 132,  81):�������� ��������� ������
      if(L_oso) then
          if (L_(42)) then
              I_(30) = 27
              L_up = .true.
              L_(40) = .false.
          endif
          if (L_ipu) then
              L_(40) = .true.
              L_up = .false.
          endif
          if (I_(30).ne.27) then
              L_up = .false.
              L_(40) = .false.
          endif
      else
          L_(40) = .false.
      endif
C FDA40_logic_box_step.fgi( 124,  60):��� ������� ���������,FDA40_BOX_AVT
C sav1=L_(18)
C sav2=L0_ak
      L0_ak=L_ek.or.(L0_ak.and..not.(L_up))
      L_(18)=.not.L0_ak
C FDA40_logic_box_step.fgi(  90, 107):recalc:RS �������
C if(sav1.ne.L_(18) .and. try1869.gt.0) goto 1869
C if(sav2.ne.L0_ak .and. try1869.gt.0) goto 1869
      if(L_oso) then
          if (L_(40)) then
              I_(30) = 28
              L_op = .true.
              L_(38) = .false.
          endif
          if (L_(39)) then
              L_(38) = .true.
              L_op = .false.
          endif
          if (I_(30).ne.28) then
              L_op = .false.
              L_(38) = .false.
          endif
      else
          L_(38) = .false.
      endif
C FDA40_logic_box_step.fgi( 124,  49):��� ������� ���������,FDA40_BOX_AVT
      if(L_op.and..not.L0_ip) then
         R0_ap=R0_ep
      else
         R0_ap=max(R_(13)-deltat,0.0)
      endif
      L_(37)=R0_ap.gt.0.0
      L0_ip=L_op
C FDA40_logic_box_step.fgi( 150,  49):������������  �� T
      L_(36)=.not.L_(202) .and.L_(37)
      L_(34)=L_(37).and..not.L_(35)
C FDA40_logic_box_step.fgi( 170,  50):��������� ������
      iv2=0
      if(L_(188)) iv2=ibset(iv2,0)
      if(L_(169)) iv2=ibset(iv2,1)
      if(L_(146)) iv2=ibset(iv2,2)
      if(L_(102)) iv2=ibset(iv2,3)
      if(L_(36)) iv2=ibset(iv2,4)
      L_(667)=L_(667).or.iv2.ne.0
C FDA40_vlv.fgi(  34, 122):������-�������: ������� ������ ������/����������/����� ���������
      if(L_olo) then
         R0_iki=0.0
      elseif(L0_oki) then
         R0_iki=R0_eki
      else
         R0_iki=max(R_(48)-deltat,0.0)
      endif
      L_(110)=L_olo.or.R0_iki.gt.0.0
      L0_oki=L_olo
C FDA40_logic_box_step.fgi( 103, 635):�������� ������� ������
      L_(124) = L_(110).AND.L_(180)
C FDA40_logic_box_step.fgi(  93, 622):�
      if(L_(124).and..not.L0_imi) then
         R0_ami=R0_emi
      else
         R0_ami=max(R_(54)-deltat,0.0)
      endif
      L_(125)=R0_ami.gt.0.0
      L0_imi=L_(124)
C FDA40_logic_box_step.fgi(  99, 622):������������  �� T
      Call FDA40_1(ext_deltat)
      Call FDA40_2(ext_deltat)
      End
      Subroutine FDA40_1(ext_deltat)
      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      Include 'FDA40.fh'

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      L_(123)=.not.L_(202) .and.L_(125)
      L_(121)=L_(125).and..not.L_(122)
C FDA40_logic_box_step.fgi( 113, 622):��������� ������
      if(L_iko) then
         R0_uxe=0.0
      elseif(L0_abi) then
         R0_uxe=R0_oxe
      else
         R0_uxe=max(R_(42)-deltat,0.0)
      endif
      L_(109)=L_iko.or.R0_uxe.gt.0.0
      L0_abi=L_iko
C FDA40_logic_box_step.fgi( 104, 551):�������� ������� ������
      L_(119) = L_(109).AND.L_(172)
C FDA40_logic_box_step.fgi(  95, 537):�
      if(L_(119).and..not.L0_uli) then
         R0_ili=R0_oli
      else
         R0_ili=max(R_(52)-deltat,0.0)
      endif
      L_(120)=R0_ili.gt.0.0
      L0_uli=L_(119)
C FDA40_logic_box_step.fgi( 101, 537):������������  �� T
      L_(118)=.not.L_(202) .and.L_(120)
      L_(116)=L_(120).and..not.L_(117)
C FDA40_logic_box_step.fgi( 114, 538):��������� ������
      if(L_ivi) then
         R0_ave=0.0
      elseif(L0_eve) then
         R0_ave=R0_ute
      else
         R0_ave=max(R_(39)-deltat,0.0)
      endif
      L_(108)=L_ivi.or.R0_ave.gt.0.0
      L0_eve=L_ivi
C FDA40_logic_box_step.fgi( 105, 444):�������� ������� ������
      L_(115) = L_(108).AND.L_(149)
C FDA40_logic_box_step.fgi(  98, 431):�
      if(L_(115).and..not.L0_eli) then
         R0_uki=R0_ali
      else
         R0_uki=max(R_(50)-deltat,0.0)
      endif
      L_(114)=R0_uki.gt.0.0
      L0_eli=L_(115)
C FDA40_logic_box_step.fgi( 104, 431):������������  �� T
      L_(113)=.not.L_(202) .and.L_(114)
      L_(111)=L_(114).and..not.L_(112)
C FDA40_logic_box_step.fgi( 116, 432):��������� ������
      if(L_ime) then
         R0_ake=0.0
      elseif(L0_eke) then
         R0_ake=R0_ufe
      else
         R0_ake=max(R_(27)-deltat,0.0)
      endif
      L_(73)=L_ime.or.R0_ake.gt.0.0
      L0_eke=L_ime
C FDA40_logic_box_step.fgi( 104, 227):�������� ������� ������
      L_(77) = L_(73).AND.L_(84)
C FDA40_logic_box_step.fgi(  95, 213):�
      if(L_(77).and..not.L0_ile) then
         R0_ale=R0_ele
      else
         R0_ale=max(R_(30)-deltat,0.0)
      endif
      L_(78)=R0_ale.gt.0.0
      L0_ile=L_(77)
C FDA40_logic_box_step.fgi( 101, 213):������������  �� T
      L_(76)=.not.L_(202) .and.L_(78)
      L_(74)=L_(78).and..not.L_(75)
C FDA40_logic_box_step.fgi( 114, 214):��������� ������
      if(L_av) then
         R0_es=0.0
      elseif(L0_is) then
         R0_es=R0_as
      else
         R0_es=max(R_(15)-deltat,0.0)
      endif
      L_(43)=L_av.or.R0_es.gt.0.0
      L0_is=L_av
C FDA40_logic_box_step.fgi( 103,  96):�������� ������� ������
      L_(47) = L_(43).AND.L_(54)
C FDA40_logic_box_step.fgi(  93,  83):�
      if(L_(47).and..not.L0_at) then
         R0_os=R0_us
      else
         R0_os=max(R_(17)-deltat,0.0)
      endif
      L_(48)=R0_os.gt.0.0
      L0_at=L_(47)
C FDA40_logic_box_step.fgi(  99,  83):������������  �� T
      L_(46)=.not.L_(202) .and.L_(48)
      L_(44)=L_(48).and..not.L_(45)
C FDA40_logic_box_step.fgi( 113,  84):��������� ������
      if(L_op) then
         R0_el=0.0
      elseif(L0_il) then
         R0_el=R0_al
      else
         R0_el=max(R_(8)-deltat,0.0)
      endif
      L_(28)=L_op.or.R0_el.gt.0.0
      L0_il=L_op
C FDA40_logic_box_step.fgi( 104,  54):�������� ������� ������
      L_(32) = L_(28).AND.L_(39)
C FDA40_logic_box_step.fgi(  95,  40):�
      if(L_(32).and..not.L0_om) then
         R0_em=R0_im
      else
         R0_em=max(R_(11)-deltat,0.0)
      endif
      L_(33)=R0_em.gt.0.0
      L0_om=L_(32)
C FDA40_logic_box_step.fgi( 101,  40):������������  �� T
      L_(31)=.not.L_(202) .and.L_(33)
      L_(29)=L_(33).and..not.L_(30)
C FDA40_logic_box_step.fgi( 114,  40):��������� ������
      iv2=0
      if(L_(123)) iv2=ibset(iv2,0)
      if(L_(118)) iv2=ibset(iv2,1)
      if(L_(113)) iv2=ibset(iv2,2)
      if(L_(76)) iv2=ibset(iv2,3)
      if(L_(46)) iv2=ibset(iv2,4)
      if(L_(31)) iv2=ibset(iv2,5)
      L_(656)=L_(656).or.iv2.ne.0
C FDA40_vlv.fgi(  34, 122):������-�������: ������� ������ ������/����������/����� ���������
      if(L_olo.and..not.L0_ilo) then
         R0_alo=R0_elo
      else
         R0_alo=max(R_(72)-deltat,0.0)
      endif
      L_(178)=R0_alo.gt.0.0
      L0_ilo=L_olo
C FDA40_logic_box_step.fgi( 150, 630):������������  �� T
      L_(177)=.not.L_(202) .and.L_(178)
      L_(175)=L_(178).and..not.L_(176)
C FDA40_logic_box_step.fgi( 170, 630):��������� ������
      if(L_ime.and..not.L0_eme) then
         R0_ule=R0_ame
      else
         R0_ule=max(R_(32)-deltat,0.0)
      endif
      L_(82)=R0_ule.gt.0.0
      L0_eme=L_ime
C FDA40_logic_box_step.fgi( 150, 222):������������  �� T
      L_(81)=.not.L_(202) .and.L_(82)
      L_(79)=L_(82).and..not.L_(80)
C FDA40_logic_box_step.fgi( 170, 222):��������� ������
      if(L_av.and..not.L0_ut) then
         R0_it=R0_ot
      else
         R0_it=max(R_(19)-deltat,0.0)
      endif
      L_(52)=R0_it.gt.0.0
      L0_ut=L_av
C FDA40_logic_box_step.fgi( 150,  91):������������  �� T
      L_(51)=.not.L_(202) .and.L_(52)
      L_(49)=L_(52).and..not.L_(50)
C FDA40_logic_box_step.fgi( 170,  92):��������� ������
      iv2=0
      if(L_(177)) iv2=ibset(iv2,0)
      if(L_(81)) iv2=ibset(iv2,1)
      if(L_(51)) iv2=ibset(iv2,2)
      L_(655)=L_(655).or.iv2.ne.0
C FDA40_vlv.fgi(  34, 122):������-�������: ������� ������ ������/����������/����� ���������
      Call MECHANIC_PRIVOD(deltat,REAL(950,4),L_omal,L_(666
     &),
     & REAL(900,4),L_exuk,L_(657),REAL(800,4),L_abal,L_(658
     &),
     & REAL(700,4),L_ubal,L_(659),REAL(600,4),L_odal,L_(660
     &),
     & REAL(500,4),L_ifal,L_(661),REAL(400,4),L_ekal,L_(662
     &),
     & REAL(300,4),L_alal,L_(663),REAL(200,4),L_ulal,L_(664
     &),
     & L_emal,L_uvuk,L_oxuk,L_ibal,L_edal,L_umal,L_ixuk,
     & L_ebal,L_adal,L_udal,L_imal,L_axuk,L_uxuk,
     & L_obal,L_idal,L_afal,L_ofal,L_efal,L_ufal,L_okal,
     & L_ilal,L_ikal,L_elal,L_amal,L_akal,L_ukal,
     & L_olal,REAL(50,4),L_epal,L_apal,L_opal,L_ipal,
     & L_(665),L_avuk,L_(653),L_asal,L_(654),R_ovuk,L_esal
     &,L_isal,
     & REAL(70,4),L_eral,L_(667),L_upal,L_aral,L_(656),
     & L_(655),INT(I_evuk,4),L_ivuk,INT(I_oral,4),L_ural,
     & INT(I_otuk,4),L_utuk,R_iral,REAL(1000,4),REAL(0,4)
     &,R_atal,L_etuk,
     & L_ituk,L_osal,L_usal)
C FDA40_vlv.fgi(  34, 122):������,20FDA91AE018
C sav1=L_oko
C sav2=L_(174)
      if(L_oso) then
          if (L_(173)) then
              I_(30) = 6
              L_oko = .true.
              L_(174) = .false.
          endif
          if (L_asal) then
              L_(174) = .true.
              L_oko = .false.
          endif
          if (I_(30).ne.6) then
              L_oko = .false.
              L_(174) = .false.
          endif
      else
          L_(174) = .false.
      endif
C FDA40_logic_box_step.fgi( 124, 557):recalc:��� ������� ���������,FDA40_BOX_AVT
C if(sav1.ne.L_oko .and. try2041.gt.0) goto 2041
C if(sav2.ne.L_(174) .and. try2041.gt.0) goto 2041
      L_(614)=R_iral.lt.R0_akof
C FDA40_logic_press_step.fgi(  33, 878):���������� <
      L_etu=(L_asal.or.L_etu).and..not.(L_ipu)
      L_(242)=.not.L_etu
C FDA40_logic_parameters.fgi(  34, 273):RS �������
      L_efuf=L_etu
C FDA40_logic_parameters.fgi(  58, 275):������,20FDA40_LODKA_NA_TELEGE
      L_ifuf = L_(618).AND.L_(617).AND.L_(616).AND.L_(615
     &).AND.L_asok.AND.L_ufuf.AND.L_(614).AND.L_efuf.AND.L_ofuf
C FDA40_logic_press_step.fgi(  67, 903):�
      L_(598) = L_elof.AND.L_ifuf.AND.L_(595)
C FDA40_logic_press_step.fgi( 390, 969):�
      if (.not.L_ilof.and.(L_olof.or.L_(598))) then
          I_(199) = 0
          L_ilof = .true.
          L_(596) = .true.
      endif
      if (I_(199) .gt. 0) then
         L_(596) = .false.
      endif
      if(L_(597))then
          I_(199) = 0
          L_ilof = .false.
          L_(596) = .false.
      endif
C FDA40_logic_press_step.fgi( 417, 961):���������� ������� ���������,20FDA40EC002
      L_(421) =.NOT.(L_ekof)
C FDA40_logic_press_step.fgi( 421, 367):���
      L_(572) = L_(596).OR.L_uvaf
C FDA40_logic_press_step.fgi( 425, 941):���
      if(L_ilof) then
          if (L_(572)) then
              I_(199) = 1
              L_ibof = .true.
              L_(573) = .false.
          endif
          if (L_(574)) then
              L_(573) = .true.
              L_ibof = .false.
          endif
          if (I_(199).ne.1) then
              L_ibof = .false.
              L_(573) = .false.
          endif
      else
          L_(573) = .false.
      endif
C FDA40_logic_press_step.fgi( 417, 933):��� ������� ���������,20FDA40EC002
      if(L_ilof) then
          if (L_(573)) then
              I_(199) = 2
              L_ixif = .true.
              L_(567) = .false.
          endif
          if (L_(568)) then
              L_(567) = .true.
              L_ixif = .false.
          endif
          if (I_(199).ne.2) then
              L_ixif = .false.
              L_(567) = .false.
          endif
      else
          L_(567) = .false.
      endif
C FDA40_logic_press_step.fgi( 417, 916):��� ������� ���������,20FDA40EC002
      if(L_ixif.and..not.L0_axif) then
         R0_ovif=R0_uvif
      else
         R0_ovif=max(R_(193)-deltat,0.0)
      endif
      L_(566)=R0_ovif.gt.0.0
      L0_axif=L_ixif
C FDA40_logic_press_step.fgi( 442, 916):������������  �� T
      L_(565)=.not.L_(584) .and.L_(566)
      L_(563)=L_(566).and..not.L_(564)
C FDA40_logic_press_step.fgi( 463, 916):��������� ������
      if(L_ibof.and..not.L0_ebof) then
         R0_uxif=R0_abof
      else
         R0_uxif=max(R_(195)-deltat,0.0)
      endif
      L_orul=R0_uxif.gt.0.0
      L0_ebof=L_ibof
C FDA40_logic_press_step.fgi( 442, 933):������������  �� T
      L_(571)=.not.L_(584) .and.L_orul
      L_(569)=L_orul.and..not.L_(570)
C FDA40_logic_press_step.fgi( 463, 926):��������� ������
      iv2=0
      if(L_(571)) iv2=ibset(iv2,0)
      L_(681)=L_(681).or.iv2.ne.0
C FDA40_vlv.fgi( 110,  89):������-�������: ������� ������ ������/����������/����� ���������
      L_ufuf=R_iful.lt.R0_apu
C FDA40_logic_parameters.fgi( 193, 504):���������� <
C sav1=L_ifuf
      L_ifuf = L_(618).AND.L_(617).AND.L_(616).AND.L_(615
     &).AND.L_asok.AND.L_ufuf.AND.L_(614).AND.L_efuf.AND.L_ofuf
C FDA40_logic_press_step.fgi(  67, 903):recalc:�
C if(sav1.ne.L_ifuf .and. try2181.gt.0) goto 2181
      L_(619) = L_ukuf.OR.L_okuf
C FDA40_logic_press_step.fgi(  48, 951):���
      L_ekuf =.NOT.(L_akuf.AND.L_ifek.AND.L_ikuf.AND.L_ufuf.AND.L_
     &(619))
C FDA40_logic_press_step.fgi(  67, 962):�
      L_(611) = L_emof.AND.L_ekuf.AND.L_(608)
C FDA40_logic_press_step.fgi( 182, 969):�
      if (.not.L_imof.and.(L_omof.or.L_(611))) then
          I_umof = 0
          L_imof = .true.
          L_(609) = .true.
      endif
      if (I_umof .gt. 0) then
         L_(609) = .false.
      endif
      if(L_(610))then
          I_umof = 0
          L_imof = .false.
          L_(609) = .false.
      endif
C FDA40_logic_press_step.fgi( 216, 962):���������� ������� ���������,20FDA40EC001
      if(L_imof) then
          if (L_(609)) then
              I_umof = 1
              L_alof = .true.
              L_(593) = .false.
          endif
          if (L_(594)) then
              L_(593) = .true.
              L_alof = .false.
          endif
          if (I_umof.ne.1) then
              L_alof = .false.
              L_(593) = .false.
          endif
      else
          L_(593) = .false.
      endif
C FDA40_logic_press_step.fgi( 216, 951):��� ������� ���������,20FDA40EC001
      if(L_alof.and..not.L0_opif) then
         R0_epif=R0_ipif
      else
         R0_epif=max(R_(179)-deltat,0.0)
      endif
      L_(531)=R0_epif.gt.0.0
      L0_opif=L_alof
C FDA40_logic_press_step.fgi( 241, 951):������������  �� T
      L_(530)=.not.L_(584) .and.L_(531)
      L_(528)=L_(531).and..not.L_(529)
C FDA40_logic_press_step.fgi( 255, 952):��������� ������
      iv2=0
      if(L_(578)) iv2=ibset(iv2,0)
      if(L_(530)) iv2=ibset(iv2,1)
      L_(689)=L_(689).or.iv2.ne.0
C FDA40_logic_press_step.fgi( 464, 862):������-�������: ������� ������ ������/����������/����� ���������
      L_(592)=R_abem.gt.R0_efof
C FDA40_logic_press_step.fgi( 391, 879):���������� >
      if(L_ivif) then
         R0_ifif=0.0
      elseif(L0_ofif) then
         R0_ifif=R0_efif
      else
         R0_ifif=max(R_(173)-deltat,0.0)
      endif
      L_(562)=L_ivif.or.R0_ifif.gt.0.0
      L0_ofif=L_ivif
C FDA40_logic_press_step.fgi( 408, 907):�������� ������� ������
      if(L_ilof) then
          if (L_(567)) then
              I_(199) = 3
              L_ivif = .true.
              L_(561) = .false.
          endif
          if (L_(562)) then
              L_(561) = .true.
              L_ivif = .false.
          endif
          if (I_(199).ne.3) then
              L_ivif = .false.
              L_(561) = .false.
          endif
      else
          L_(561) = .false.
      endif
C FDA40_logic_press_step.fgi( 417, 901):��� ������� ���������,20FDA40EC002
C sav1=L_(562)
      if(L_ivif) then
         R0_ifif=0.0
      elseif(L0_ofif) then
         R0_ifif=R0_efif
      else
         R0_ifif=max(R_(173)-deltat,0.0)
      endif
      L_(562)=L_ivif.or.R0_ifif.gt.0.0
      L0_ofif=L_ivif
C FDA40_logic_press_step.fgi( 408, 907):recalc:�������� ������� ������
C if(sav1.ne.L_(562) .and. try2239.gt.0) goto 2239
      L_(575) =.NOT.(L_ikof)
C FDA40_logic_press_step.fgi( 422, 812):���
      L_(590) = L_(561).OR.L_obof
C FDA40_logic_press_step.fgi( 425, 887):���
      if(L_ilof) then
          if (L_(590)) then
              I_(199) = 4
              L_ukof = .true.
              L_(591) = .false.
          endif
          if (L_(592)) then
              L_(591) = .true.
              L_ukof = .false.
          endif
          if (I_(199).ne.4) then
              L_ukof = .false.
              L_(591) = .false.
          endif
      else
          L_(591) = .false.
      endif
C FDA40_logic_press_step.fgi( 417, 879):��� ������� ���������,20FDA40EC002
      if(L_ukof.and..not.L0_ufof) then
         R0_ifof=R0_ofof
      else
         R0_ifof=max(R_(199)-deltat,0.0)
      endif
      L_(586)=R0_ifof.gt.0.0
      L0_ufof=L_ukof
C FDA40_logic_press_step.fgi( 443, 879):������������  �� T
      L_(585)=.not.L_(584) .and.L_(586)
      L_(582)=L_(586).and..not.L_(583)
C FDA40_logic_press_step.fgi( 464, 880):��������� ������
      L_(524)=R_abem.gt.R0_amif
C FDA40_logic_press_step.fgi( 197, 940):���������� >
      if(L_imof) then
          if (L_(593)) then
              I_umof = 2
              L_emif = .true.
              L_(523) = .false.
          endif
          if (L_(524)) then
              L_(523) = .true.
              L_emif = .false.
          endif
          if (I_umof.ne.2) then
              L_emif = .false.
              L_(523) = .false.
          endif
      else
          L_(523) = .false.
      endif
C FDA40_logic_press_step.fgi( 216, 940):��� ������� ���������,20FDA40EC001
      if(L_emif.and..not.L0_ulif) then
         R0_ilif=R0_olif
      else
         R0_ilif=max(R_(176)-deltat,0.0)
      endif
      L_(522)=R0_ilif.gt.0.0
      L0_ulif=L_emif
C FDA40_logic_press_step.fgi( 241, 940):������������  �� T
      L_(521)=.not.L_(584) .and.L_(522)
      L_(519)=L_(522).and..not.L_(520)
C FDA40_logic_press_step.fgi( 255, 940):��������� ������
      iv2=0
      if(L_(585)) iv2=ibset(iv2,0)
      if(L_(521)) iv2=ibset(iv2,1)
      L_(688)=L_(688).or.iv2.ne.0
C FDA40_logic_press_step.fgi( 464, 880):������-�������: ������� ������ ������/����������/����� ���������
      if(L_elif) then
         R0_ukif=0.0
      elseif(L0_alif) then
         R0_ukif=R0_okif
      else
         R0_ukif=max(R_(174)-deltat,0.0)
      endif
      L_(518)=L_elif.or.R0_ukif.gt.0.0
      L0_alif=L_elif
C FDA40_logic_press_step.fgi( 210, 932):�������� ������� ������
      if(L_imof) then
          if (L_(523)) then
              I_umof = 3
              L_elif = .true.
              L_(605) = .false.
          endif
          if (L_(518)) then
              L_(605) = .true.
              L_elif = .false.
          endif
          if (I_umof.ne.3) then
              L_elif = .false.
              L_(605) = .false.
          endif
      else
          L_(605) = .false.
      endif
C FDA40_logic_press_step.fgi( 216, 926):��� ������� ���������,20FDA40EC001
C sav1=L_(518)
      if(L_elif) then
         R0_ukif=0.0
      elseif(L0_alif) then
         R0_ukif=R0_okif
      else
         R0_ukif=max(R_(174)-deltat,0.0)
      endif
      L_(518)=L_elif.or.R0_ukif.gt.0.0
      L0_alif=L_elif
C FDA40_logic_press_step.fgi( 210, 932):recalc:�������� ������� ������
C if(sav1.ne.L_(518) .and. try2262.gt.0) goto 2262
      L_(517)=.not.L_(584) .and.L_elif
      L_(515)=L_elif.and..not.L_(516)
C FDA40_logic_press_step.fgi( 255, 926):��������� ������
      if(L_ikof.and..not.L0_afif) then
         R0_odif=R0_udif
      else
         R0_odif=max(R_(172)-deltat,0.0)
      endif
      L_(514)=R0_odif.gt.0.0
      L0_afif=L_ikof
C FDA40_logic_press_step.fgi( 443, 805):������������  �� T
      L_(513)=.not.L_(584) .and.L_(514)
      L_(511)=L_(514).and..not.L_(512)
C FDA40_logic_press_step.fgi( 464, 806):��������� ������
      iv2=0
      if(L_(517)) iv2=ibset(iv2,0)
      if(L_(513)) iv2=ibset(iv2,1)
      L_(687)=L_(687).or.iv2.ne.0
C FDA40_logic_press_step.fgi( 255, 926):������-�������: ������� ������ ������/����������/����� ���������
      Call TELEGKA_TRANSP_HANDLER(deltat,L_axam,L_(685),L_idem
     &,L_(686),R_olem,
     & R_ulem,R_ofem,R_efem,L_elem,R_ufem,R_ifem,
     & L_ilem,L_ulam,L_amam,L_(687),L_adem,R_udem,R_afem,
     & R_alem,R_abem,R_ukem,L_obem,L_ibem,R_iram,
     & REAL(R_eram,4),R_avam,REAL(R_ivam,4),L_otam,
     & R_itam,REAL(R_utam,4),L_uram,R_oram,
     & REAL(R_asam,4),L_odem,L_(690),L_(691),L_evam,
     & L_oxam,L_akem,L_(689),L_(688),L_ekem,L_okem,L_ebem
     &)
C FDA40_vlv.fgi(  87,  89):���������� ������� ������������,20FDA40_FEEDER
      L_(581)=R_abem.lt.R0_edof
C FDA40_logic_press_step.fgi( 391, 862):���������� <,_
      if(L_ilof) then
          if (L_(591)) then
              I_(199) = 5
              L_afof = .true.
              L_(580) = .false.
          endif
          if (L_(581)) then
              L_(580) = .true.
              L_afof = .false.
          endif
          if (I_(199).ne.5) then
              L_afof = .false.
              L_(580) = .false.
          endif
      else
          L_(580) = .false.
      endif
C FDA40_logic_press_step.fgi( 417, 862):��� ������� ���������,20FDA40EC002
C sav1=L_(579)
      if(L_afof.and..not.L0_udof) then
         R0_idof=R0_odof
      else
         R0_idof=max(R_(197)-deltat,0.0)
      endif
      L_(579)=R0_idof.gt.0.0
      L0_udof=L_afof
C FDA40_logic_press_step.fgi( 443, 862):recalc:������������  �� T
C if(sav1.ne.L_(579) .and. try1584.gt.0) goto 1584
      if (.not.L_ikof.and.L_(580)) then
          I_adof = I_adof+1
      endif
      if (L_ikof) then
          I_adof = 0
      endif
C FDA40_logic_press_step.fgi( 425, 830):�������
      L_ikof=I_adof.eq.I1_ubof
C FDA40_logic_press_step.fgi( 428, 824):���������� �������������
C sav1=L_(514)
      if(L_ikof.and..not.L0_afif) then
         R0_odif=R0_udif
      else
         R0_odif=max(R_(172)-deltat,0.0)
      endif
      L_(514)=R0_odif.gt.0.0
      L0_afif=L_ikof
C FDA40_logic_press_step.fgi( 443, 805):recalc:������������  �� T
C if(sav1.ne.L_(514) .and. try2270.gt.0) goto 2270
C sav1=L_(575)
      L_(575) =.NOT.(L_ikof)
C FDA40_logic_press_step.fgi( 422, 812):recalc:���
C if(sav1.ne.L_(575) .and. try2245.gt.0) goto 2245
C sav1=I_adof
      if (.not.L_ikof.and.L_(580)) then
          I_adof = I_adof+1
      endif
      if (L_ikof) then
          I_adof = 0
      endif
C FDA40_logic_press_step.fgi( 425, 830):recalc:�������
C if(sav1.ne.I_adof .and. try2315.gt.0) goto 2315
      if(L_ilof) then
          if (L_ikof) then
              I_(199) = 6
              L_okof = .true.
              L_(588) = .false.
          endif
          if (L_(589)) then
              L_(588) = .true.
              L_okof = .false.
          endif
          if (I_(199).ne.6) then
              L_okof = .false.
              L_(588) = .false.
          endif
      else
          L_(588) = .false.
      endif
C FDA40_logic_press_step.fgi( 416, 795):��� ������� ���������,20FDA40EC002
      if(L_ilof) then
          if (L_(588)) then
              I_(199) = 7
              L_urif = .true.
              L_(554) = .false.
          endif
          if (L_(549)) then
              L_(554) = .true.
              L_urif = .false.
          endif
          if (I_(199).ne.7) then
              L_urif = .false.
              L_(554) = .false.
          endif
      else
          L_(554) = .false.
      endif
C FDA40_logic_press_step.fgi( 416, 776):��� ������� ���������,20FDA40EC002
C sav1=L_(549)
      if(L_urif) then
         R0_edif=0.0
      elseif(L0_idif) then
         R0_edif=R0_adif
      else
         R0_edif=max(R_(170)-deltat,0.0)
      endif
      L_(549)=L_urif.or.R0_edif.gt.0.0
      L0_idif=L_urif
C FDA40_logic_press_step.fgi( 410, 784):recalc:�������� ������� ������
C if(sav1.ne.L_(549) .and. try1580.gt.0) goto 1580
      if(L_ilof) then
          if (L_(554)) then
              I_(199) = 8
              L_usif = .true.
              L_(555) = .false.
          endif
          if (L_(556)) then
              L_(555) = .true.
              L_usif = .false.
          endif
          if (I_(199).ne.8) then
              L_usif = .false.
              L_(555) = .false.
          endif
      else
          L_(555) = .false.
      endif
C FDA40_logic_press_step.fgi( 416, 750):��� ������� ���������,20FDA40EC002
      if(L_ilof) then
          if (L_(555)) then
              I_(199) = 9
              L_orif = .true.
              L_(544) = .false.
          endif
          if (L_(545)) then
              L_(544) = .true.
              L_orif = .false.
          endif
          if (I_(199).ne.9) then
              L_orif = .false.
              L_(544) = .false.
          endif
      else
          L_(544) = .false.
      endif
C FDA40_logic_press_step.fgi( 416, 733):��� ������� ���������,20FDA40EC002
C sav1=L_(545)
      if(L_orif) then
         R0_obif=0.0
      elseif(L0_ubif) then
         R0_obif=R0_ibif
      else
         R0_obif=max(R_(169)-deltat,0.0)
      endif
      L_(545)=L_orif.or.R0_obif.gt.0.0
      L0_ubif=L_orif
C FDA40_logic_press_step.fgi( 409, 739):recalc:�������� ������� ������
C if(sav1.ne.L_(545) .and. try1576.gt.0) goto 1576
      L_eref=I_iref.eq.I1_aref
C FDA40_logic_press_step.fgi( 427, 621):���������� �������������
      L_(477) =.NOT.(L_eref)
C FDA40_logic_press_step.fgi( 421, 609):���
      L_(499) = L_(544).OR.L_upef
C FDA40_logic_press_step.fgi( 424, 719):���
      if(L_ilof) then
          if (L_(499)) then
              I_(199) = 10
              L_exef = .true.
              L_(500) = .false.
          endif
          if (L_(501)) then
              L_(500) = .true.
              L_exef = .false.
          endif
          if (I_(199).ne.10) then
              L_exef = .false.
              L_(500) = .false.
          endif
      else
          L_(500) = .false.
      endif
C FDA40_logic_press_step.fgi( 416, 708):��� ������� ���������,20FDA40EC002
      if(L_ilof) then
          if (L_(500)) then
              I_(199) = 11
              L_evef = .true.
              L_(493) = .false.
          endif
          if (L_(494)) then
              L_(493) = .true.
              L_evef = .false.
          endif
          if (I_(199).ne.11) then
              L_evef = .false.
              L_(493) = .false.
          endif
      else
          L_(493) = .false.
      endif
C FDA40_logic_press_step.fgi( 416, 692):��� ������� ���������,20FDA40EC002
      if(L_ilof) then
          if (L_(493)) then
              I_(199) = 12
              L_etef = .true.
              L_(487) = .false.
          endif
          if (L_(488)) then
              L_(487) = .true.
              L_etef = .false.
          endif
          if (I_(199).ne.12) then
              L_etef = .false.
              L_(487) = .false.
          endif
      else
          L_(487) = .false.
      endif
C FDA40_logic_press_step.fgi( 416, 675):��� ������� ���������,20FDA40EC002
C sav1=L_(488)
      if(L_etef) then
         R0_usef=0.0
      elseif(L0_atef) then
         R0_usef=R0_osef
      else
         R0_usef=max(R_(160)-deltat,0.0)
      endif
      L_(488)=L_etef.or.R0_usef.gt.0.0
      L0_atef=L_etef
C FDA40_logic_press_step.fgi( 409, 681):recalc:�������� ������� ������
C if(sav1.ne.L_(488) .and. try1570.gt.0) goto 1570
      if(L_ilof) then
          if (L_(487)) then
              I_(199) = 13
              L_isef = .true.
              L_(482) = .false.
          endif
          if (L_(483)) then
              L_(482) = .true.
              L_isef = .false.
          endif
          if (I_(199).ne.13) then
              L_isef = .false.
              L_(482) = .false.
          endif
      else
          L_(482) = .false.
      endif
C FDA40_logic_press_step.fgi( 416, 655):��� ������� ���������,20FDA40EC002
      if(L_isef.and..not.L0_esef) then
         R0_uref=R0_asef
      else
         R0_uref=max(R_(159)-deltat,0.0)
      endif
      L_(481)=R0_uref.gt.0.0
      L0_esef=L_isef
C FDA40_logic_press_step.fgi( 443, 655):������������  �� T
      L_(480)=.not.L_(584) .and.L_(481)
      L_(478)=L_(481).and..not.L_(479)
C FDA40_logic_press_step.fgi( 463, 656):��������� ������
      L_(472)=R_ibim.lt.R0_umef
C FDA40_logic_press_step.fgi( 390, 575):���������� <
      if(L_ilof) then
          if (L_eref) then
              I_(199) = 14
              L_apef = .true.
              L_(471) = .false.
          endif
          if (L_(472)) then
              L_(471) = .true.
              L_apef = .false.
          endif
          if (I_(199).ne.14) then
              L_apef = .false.
              L_(471) = .false.
          endif
      else
          L_(471) = .false.
      endif
C FDA40_logic_press_step.fgi( 416, 575):��� ������� ���������,20FDA40EC002
      if(.not.L_apef) then
         R0_ekef=0.0
      elseif(.not.L0_ikef) then
         R0_ekef=R0_akef
      else
         R0_ekef=max(R_(147)-deltat,0.0)
      endif
      L_(469)=L_apef.and.R0_ekef.le.0.0
      L0_ikef=L_apef
C FDA40_logic_press_step.fgi( 441, 575):�������� ��������� ������
      if(L_(469).and..not.L0_omef) then
         R0_emef=R0_imef
      else
         R0_emef=max(R_(155)-deltat,0.0)
      endif
      L_(470)=R0_emef.gt.0.0
      L0_omef=L_(469)
C FDA40_logic_press_step.fgi( 451, 575):������������  �� T
      L_(468)=.not.L_(584) .and.L_(470)
      L_(466)=L_(470).and..not.L_(467)
C FDA40_logic_press_step.fgi( 471, 576):��������� ������
      if(L_okof.and..not.L0_otif) then
         R0_etif=R0_itif
      else
         R0_etif=max(R_(190)-deltat,0.0)
      endif
      L_(560)=R0_etif.gt.0.0
      L0_otif=L_okof
C FDA40_logic_press_step.fgi( 443, 795):������������  �� T
      L_(559)=.not.L_(584) .and.L_(560)
      L_(557)=L_(560).and..not.L_(558)
C FDA40_logic_press_step.fgi( 463, 796):��������� ������
      if(L_exef.and..not.L0_axef) then
         R0_ovef=R0_uvef
      else
         R0_ovef=max(R_(165)-deltat,0.0)
      endif
      L_(498)=R0_ovef.gt.0.0
      L0_axef=L_exef
C FDA40_logic_press_step.fgi( 443, 708):������������  �� T
      L_(497)=.not.L_(584) .and.L_(498)
      L_(495)=L_(498).and..not.L_(496)
C FDA40_logic_press_step.fgi( 463, 708):��������� ������
      iv2=0
      if(L_(559)) iv2=ibset(iv2,0)
      if(L_(497)) iv2=ibset(iv2,1)
      L_(695)=L_(695).or.iv2.ne.0
C FDA40_logic_press_step.fgi( 463, 796):������-�������: ������� ������ ������/����������/����� ���������
      L_(548)=.not.L_(584) .and.L_urif
      L_(546)=L_urif.and..not.L_(547)
C FDA40_logic_press_step.fgi( 464, 776):��������� ������
      if(L_eref.and..not.L0_opef) then
         R0_epef=R0_ipef
      else
         R0_epef=max(R_(157)-deltat,0.0)
      endif
      L_(476)=R0_epef.gt.0.0
      L0_opef=L_eref
C FDA40_logic_press_step.fgi( 442, 602):������������  �� T
      L_(475)=.not.L_(584) .and.L_(476)
      L_(473)=L_(476).and..not.L_(474)
C FDA40_logic_press_step.fgi( 462, 602):��������� ������
      iv2=0
      if(L_(548)) iv2=ibset(iv2,0)
      if(L_(475)) iv2=ibset(iv2,1)
      L_(694)=L_(694).or.iv2.ne.0
C FDA40_logic_press_step.fgi( 464, 776):������-�������: ������� ������ ������/����������/����� ���������
      L_(603)=R_ibim.lt.R0_umif
C FDA40_logic_press_step.fgi( 148, 896):���������� <
      L_(602)=R_ubom.lt.R0_omif
C FDA40_logic_press_step.fgi( 148, 890):���������� <
      L_(601)=R_iful.lt.R0_imif
C FDA40_logic_press_step.fgi( 148, 882):���������� <
      L_(607) = L_(604).AND.L_(603).AND.L_(602).AND.L_(601
     &)
C FDA40_logic_press_step.fgi( 191, 891):�
      if(L_imof) then
          if (L_(605)) then
              I_umof = 4
              L_amof = .true.
              L_(606) = .false.
          endif
          if (L_(607)) then
              L_(606) = .true.
              L_amof = .false.
          endif
          if (I_umof.ne.4) then
              L_amof = .false.
              L_(606) = .false.
          endif
      else
          L_(606) = .false.
      endif
C FDA40_logic_press_step.fgi( 216, 891):��� ������� ���������,20FDA40EC001
      if(L_amof.and..not.L0_irif) then
         R0_arif=R0_erif
      else
         R0_arif=max(R_(181)-deltat,0.0)
      endif
      L_(587)=R0_arif.gt.0.0
      L0_irif=L_amof
C FDA40_logic_press_step.fgi( 241, 891):������������  �� T
      L_(527)=.not.L_(584) .and.L_(587)
      L_(525)=L_(587).and..not.L_(526)
C FDA40_logic_press_step.fgi( 256, 876):��������� ������
      if(L_adef.and..not.L0_ubef) then
         R0_ibef=R0_obef
      else
         R0_ibef=max(R_(142)-deltat,0.0)
      endif
      L_(432)=R0_ibef.gt.0.0
      L0_ubef=L_adef
C FDA40_logic_press_step.fgi( 446, 484):������������  �� T
      L_(430)=.not.L_(584) .and.L_(432)
      L_(428)=L_(432).and..not.L_(429)
C FDA40_logic_press_step.fgi( 464, 484):��������� ������
      if(L_etaf.and..not.L0_ataf) then
         R0_osaf=R0_usaf
      else
         R0_osaf=max(R_(136)-deltat,0.0)
      endif
      L_(413)=R0_osaf.gt.0.0
      L0_ataf=L_etaf
C FDA40_logic_press_step.fgi( 446, 452):������������  �� T
      L_(412)=.not.L_(584) .and.L_(413)
      L_(410)=L_(413).and..not.L_(411)
C FDA40_logic_press_step.fgi( 464, 452):��������� ������
      iv2=0
      if(L_(527)) iv2=ibset(iv2,0)
      if(L_(430)) iv2=ibset(iv2,1)
      if(L_(412)) iv2=ibset(iv2,2)
      L_(703)=L_(703).or.iv2.ne.0
C FDA40_vlv.fgi(  48,  89):������-�������: ������� ������ ������/����������/����� ���������
      if(L_usif.and..not.L0_osif) then
         R0_esif=R0_isif
      else
         R0_esif=max(R_(188)-deltat,0.0)
      endif
      L_(553)=R0_esif.gt.0.0
      L0_osif=L_usif
C FDA40_logic_press_step.fgi( 443, 750):������������  �� T
      L_(552)=.not.L_(584) .and.L_(553)
      L_(550)=L_(553).and..not.L_(551)
C FDA40_logic_press_step.fgi( 464, 750):��������� ������
      L_(509)=R_ubom.gt.R0_ixef
C FDA40_logic_press_step.fgi( 148, 859):���������� >
      if(L_imof) then
          if (L_(606)) then
              I_umof = 5
              L_ebif = .true.
              L_ulof = .false.
          endif
          if (L_(509)) then
              L_ulof = .true.
              L_ebif = .false.
          endif
          if (I_umof.ne.5) then
              L_ebif = .false.
              L_ulof = .false.
          endif
      else
          L_ulof = .false.
      endif
C FDA40_logic_press_step.fgi( 216, 859):��� ������� ���������,20FDA40EC001
      L_(508)=.not.L_(584) .and.L_ebif
      L_(506)=L_ebif.and..not.L_(507)
C FDA40_logic_press_step.fgi( 256, 860):��������� ������
      if(L_evef.and..not.L0_avef) then
         R0_otef=R0_utef
      else
         R0_otef=max(R_(163)-deltat,0.0)
      endif
      L_(492)=R0_otef.gt.0.0
      L0_avef=L_evef
C FDA40_logic_press_step.fgi( 443, 692):������������  �� T
      L_(491)=.not.L_(584) .and.L_(492)
      L_(489)=L_(492).and..not.L_(490)
C FDA40_logic_press_step.fgi( 464, 692):��������� ������
      L_(543)=.not.L_(584) .and.L_orif
      L_(541)=L_orif.and..not.L_(542)
C FDA40_logic_press_step.fgi( 464, 734):��������� ������
      if(L_ebif) then
         R0_uxef=0.0
      elseif(L0_abif) then
         R0_uxef=R0_oxef
      else
         R0_uxef=max(R_(168)-deltat,0.0)
      endif
      L_(510)=L_ebif.or.R0_uxef.gt.0.0
      L0_abif=L_ebif
C FDA40_logic_press_step.fgi( 189, 872):�������� ������� ������
      L_(505) = L_(510).AND.L_(509)
C FDA40_logic_press_step.fgi( 171, 851):�
      L_(504)=.not.L_(584) .and.L_(505)
      L_(502)=L_(505).and..not.L_(503)
C FDA40_logic_press_step.fgi( 185, 852):��������� ������
      L_(486)=.not.L_(584) .and.L_etef
      L_(484)=L_etef.and..not.L_(485)
C FDA40_logic_press_step.fgi( 464, 676):��������� ������
      if(L_amef) then
         R0_alef=0.0
      elseif(L0_elef) then
         R0_alef=R0_ukef
      else
         R0_alef=max(R_(150)-deltat,0.0)
      endif
      L_(456)=L_amef.or.R0_alef.gt.0.0
      L0_elef=L_amef
C FDA40_logic_press_step.fgi( 409, 558):�������� ������� ������
      if(L_adef) then
         R0_abef=0.0
      elseif(L0_ebef) then
         R0_abef=R0_uxaf
      else
         R0_abef=max(R_(141)-deltat,0.0)
      endif
      L_(431)=L_adef.or.R0_abef.gt.0.0
      L0_ebef=L_adef
C FDA40_logic_press_step.fgi( 409, 492):�������� ������� ������
      L_(426) = L_(431).AND.L_(427)
C FDA40_logic_press_step.fgi( 380, 476):�
      L_(425)=.not.L_(584) .and.L_(426)
      L_(423)=L_(426).and..not.L_(424)
C FDA40_logic_press_step.fgi( 390, 476):��������� ������
      if(L_etaf) then
         R0_esaf=0.0
      elseif(L0_isaf) then
         R0_esaf=R0_asaf
      else
         R0_esaf=max(R_(134)-deltat,0.0)
      endif
      L_(409)=L_etaf.or.R0_esaf.gt.0.0
      L0_isaf=L_etaf
C FDA40_logic_press_step.fgi( 404, 460):�������� ������� ������
      L_(408) = L_(409).AND.L_(414)
C FDA40_logic_press_step.fgi( 386, 444):�
      L_(407)=.not.L_(584) .and.L_(408)
      L_(405)=L_(408).and..not.L_(406)
C FDA40_logic_press_step.fgi( 400, 444):��������� ������
      L_(450)=R_ubom.gt.R0_okef
C FDA40_logic_press_step.fgi( 351, 533):���������� >
      L_(465) = L_(451).AND.L_(450)
C FDA40_logic_press_step.fgi( 391, 548):�
      if(L_ilof) then
          if (L_(471)) then
              I_(199) = 15
              L_amef = .true.
              L_(464) = .false.
          endif
          if (L_(465)) then
              L_(464) = .true.
              L_amef = .false.
          endif
          if (I_(199).ne.15) then
              L_amef = .false.
              L_(464) = .false.
          endif
      else
          L_(464) = .false.
      endif
C FDA40_logic_press_step.fgi( 416, 548):��� ������� ���������,20FDA40EC002
C sav1=L_(456)
      if(L_amef) then
         R0_alef=0.0
      elseif(L0_elef) then
         R0_alef=R0_ukef
      else
         R0_alef=max(R_(150)-deltat,0.0)
      endif
      L_(456)=L_amef.or.R0_alef.gt.0.0
      L0_elef=L_amef
C FDA40_logic_press_step.fgi( 409, 558):recalc:�������� ������� ������
C if(sav1.ne.L_(456) .and. try2443.gt.0) goto 2443
      if(L_amef.and..not.L0_ulef) then
         R0_ilef=R0_olef
      else
         R0_ilef=max(R_(153)-deltat,0.0)
      endif
      L_(463)=R0_ilef.gt.0.0
      L0_ulef=L_amef
C FDA40_logic_press_step.fgi( 446, 548):������������  �� T
      L_(462)=.not.L_(584) .and.L_(463)
      L_(460)=L_(463).and..not.L_(461)
C FDA40_logic_press_step.fgi( 464, 548):��������� ������
      if(L_ilof) then
          if (L_(464)) then
              I_(199) = 16
              L_ofef = .true.
              L_(444) = .false.
          endif
          if (L_(445)) then
              L_(444) = .true.
              L_ofef = .false.
          endif
          if (I_(199).ne.16) then
              L_ofef = .false.
              L_(444) = .false.
          endif
      else
          L_(444) = .false.
      endif
C FDA40_logic_press_step.fgi( 416, 514):��� ������� ���������,20FDA40EC002
      if(L_ofef.and..not.L0_ifef) then
         R0_afef=R0_efef
      else
         R0_afef=max(R_(146)-deltat,0.0)
      endif
      L_(443)=R0_afef.gt.0.0
      L0_ifef=L_ofef
C FDA40_logic_press_step.fgi( 446, 514):������������  �� T
      L_(442)=.not.L_(584) .and.L_(443)
      L_(440)=L_(443).and..not.L_(441)
C FDA40_logic_press_step.fgi( 464, 514):��������� ������
      iv2=0
      if(L_(462)) iv2=ibset(iv2,0)
      if(L_(442)) iv2=ibset(iv2,1)
      L_(717)=L_(717).or.iv2.ne.0
C FDA40_logic_press_step.fgi( 464, 548):������-�������: ������� ������ ������/����������/����� ���������
      L_(540)=.not.L_(584) .and.L_(587)
      L_(538)=L_(587).and..not.L_(539)
C FDA40_logic_press_step.fgi( 257, 892):��������� ������
      if(L_ilof) then
          if (L_(444)) then
              I_(199) = 17
              L_adef = .true.
              L_(433) = .false.
          endif
          if (L_(434)) then
              L_(433) = .true.
              L_adef = .false.
          endif
          if (I_(199).ne.17) then
              L_adef = .false.
              L_(433) = .false.
          endif
      else
          L_(433) = .false.
      endif
C FDA40_logic_press_step.fgi( 416, 484):��� ������� ���������,20FDA40EC002
C sav1=L_(431)
      if(L_adef) then
         R0_abef=0.0
      elseif(L0_ebef) then
         R0_abef=R0_uxaf
      else
         R0_abef=max(R_(141)-deltat,0.0)
      endif
      L_(431)=L_adef.or.R0_abef.gt.0.0
      L0_ebef=L_adef
C FDA40_logic_press_step.fgi( 409, 492):recalc:�������� ������� ������
C if(sav1.ne.L_(431) .and. try2447.gt.0) goto 2447
C sav1=L_(432)
      if(L_adef.and..not.L0_ubef) then
         R0_ibef=R0_obef
      else
         R0_ibef=max(R_(142)-deltat,0.0)
      endif
      L_(432)=R0_ibef.gt.0.0
      L0_ubef=L_adef
C FDA40_logic_press_step.fgi( 446, 484):recalc:������������  �� T
C if(sav1.ne.L_(432) .and. try2409.gt.0) goto 2409
      if(L_ilof) then
          if (L_(433)) then
              I_(199) = 18
              L_etaf = .true.
              L_(419) = .false.
          endif
          if (L_(414)) then
              L_(419) = .true.
              L_etaf = .false.
          endif
          if (I_(199).ne.18) then
              L_etaf = .false.
              L_(419) = .false.
          endif
      else
          L_(419) = .false.
      endif
C FDA40_logic_press_step.fgi( 416, 452):��� ������� ���������,20FDA40EC002
C sav1=L_(409)
      if(L_etaf) then
         R0_esaf=0.0
      elseif(L0_isaf) then
         R0_esaf=R0_asaf
      else
         R0_esaf=max(R_(134)-deltat,0.0)
      endif
      L_(409)=L_etaf.or.R0_esaf.gt.0.0
      L0_isaf=L_etaf
C FDA40_logic_press_step.fgi( 404, 460):recalc:�������� ������� ������
C if(sav1.ne.L_(409) .and. try2452.gt.0) goto 2452
C sav1=L_(413)
      if(L_etaf.and..not.L0_ataf) then
         R0_osaf=R0_usaf
      else
         R0_osaf=max(R_(136)-deltat,0.0)
      endif
      L_(413)=R0_osaf.gt.0.0
      L0_ataf=L_etaf
C FDA40_logic_press_step.fgi( 446, 452):recalc:������������  �� T
C if(sav1.ne.L_(413) .and. try2412.gt.0) goto 2412
      if(L_ilof) then
          if (L_(419)) then
              I_(199) = 19
              L_evaf = .true.
              L_(422) = .false.
          endif
          if (L_(420)) then
              L_(422) = .true.
              L_evaf = .false.
          endif
          if (I_(199).ne.19) then
              L_evaf = .false.
              L_(422) = .false.
          endif
      else
          L_(422) = .false.
      endif
C FDA40_logic_press_step.fgi( 416, 424):��� ������� ���������,20FDA40EC002
      if(L_evaf.and..not.L0_avaf) then
         R0_otaf=R0_utaf
      else
         R0_otaf=max(R_(137)-deltat,0.0)
      endif
      L_(418)=R0_otaf.gt.0.0
      L0_avaf=L_evaf
C FDA40_logic_press_step.fgi( 452, 424):������������  �� T
      L_(417)=.not.L_(584) .and.L_(418)
      L_(415)=L_(418).and..not.L_(416)
C FDA40_logic_press_step.fgi( 468, 424):��������� ������
      iv2=0
      if(L_(540)) iv2=ibset(iv2,0)
      if(L_(417)) iv2=ibset(iv2,1)
      L_(716)=L_(716).or.iv2.ne.0
C FDA40_logic_press_step.fgi( 257, 892):������-�������: ������� ������ ������/����������/����� ���������
      L_(455) = L_(456).AND.L_(451)
C FDA40_logic_press_step.fgi( 369, 542):�
      L_(454)=.not.L_(584) .and.L_(455)
      L_(452)=L_(455).and..not.L_(453)
C FDA40_logic_press_step.fgi( 379, 542):��������� ������
      if(L_ofef) then
         R0_idef=0.0
      elseif(L0_odef) then
         R0_idef=R0_edef
      else
         R0_idef=max(R_(143)-deltat,0.0)
      endif
      L_(435)=L_ofef.or.R0_idef.gt.0.0
      L0_odef=L_ofef
C FDA40_logic_press_step.fgi( 409, 522):�������� ������� ������
      L_(439) = L_(435).AND.L_(445)
C FDA40_logic_press_step.fgi( 369, 507):�
      L_(438)=.not.L_(584) .and.L_(439)
      L_(436)=L_(439).and..not.L_(437)
C FDA40_logic_press_step.fgi( 379, 508):��������� ������
      iv2=0
      if(L_(454)) iv2=ibset(iv2,0)
      if(L_(438)) iv2=ibset(iv2,1)
      L_(715)=L_(715).or.iv2.ne.0
C FDA40_logic_press_step.fgi( 379, 542):������-�������: ������� ������ ������/����������/����� ���������
      Call TELEGKA_TRANSP_HANDLER(deltat,L_exap,L_(713),L_odep
     &,L_(714),R_ulep,
     & R_amep,R_ufep,R_ifep,L_ilep,R_akep,R_ofep,
     & L_olep,L_amap,L_emap,L_(715),L_edep,R_afep,R_efep,
     & R_elep,R_ebep,R_alep,L_ubep,L_obep,R_orap,
     & REAL(R_irap,4),R_evap,REAL(R_ovap,4),L_utap,
     & R_otap,REAL(R_avap,4),L_asap,R_urap,
     & REAL(R_esap,4),L_udep,L_(718),L_(719),L_ivap,
     & L_uxap,L_ekep,L_(717),L_(716),L_ikep,L_ukep,L_ibep
     &)
C FDA40_vlv.fgi(  29,  89):���������� ������� ������������,20FDA40_PUNCH
      if(L_(637)) then
         R_(306)=R_upok
      else
         R_(306)=R_(307)
      endif
C FDA40_logic_press.fgi( 145, 272):���� RE IN LO CH7
      R_(310) = R_(308) + (-R_(306))
C FDA40_logic_press.fgi( 149, 274):��������
      if(.not.L_(148)) then
         R0_ove=0.0
      elseif(.not.L0_uve) then
         R0_ove=R0_ive
      else
         R0_ove=max(R_(40)-deltat,0.0)
      endif
      L_(141)=L_(148).and.R0_ove.le.0.0
      L0_uve=L_(148)
C FDA40_logic_box_step.fgi( 132, 418):�������� ��������� ������
      if(L_oso) then
          if (L_(141)) then
              I_(30) = 14
              L_iti = .true.
              L_(142) = .false.
          endif
          if (L_(143)) then
              L_(142) = .true.
              L_iti = .false.
          endif
          if (I_(30).ne.14) then
              L_iti = .false.
              L_(142) = .false.
          endif
      else
          L_(142) = .false.
      endif
C FDA40_logic_box_step.fgi( 124, 404):��� ������� ���������,FDA40_BOX_AVT
C sav1=L_(139)
      if(L_iti.and..not.L0_ati) then
         R0_osi=R0_usi
      else
         R0_osi=max(R_(60)-deltat,0.0)
      endif
      L_(139)=R0_osi.gt.0.0
      L0_ati=L_iti
C FDA40_logic_box_step.fgi( 150, 404):recalc:������������  �� T
C if(sav1.ne.L_(139) .and. try1848.gt.0) goto 1848
      if(.not.L_(38)) then
         R0_ul=0.0
      elseif(.not.L0_am) then
         R0_ul=R0_ol
      else
         R0_ul=max(R_(9)-deltat,0.0)
      endif
      L_(185)=L_(38).and.R0_ul.le.0.0
      L0_am=L_(38)
C FDA40_logic_box_step.fgi( 132,  39):�������� ��������� ������
      if(L_(185)) then
         I_(30)=0
         L_oso=.false.
      endif
C FDA40_logic_box_step.fgi( 124,  17):����� ������� ���������,FDA40_BOX_AVT
      if(L_oso) then
          if (L_(142)) then
              I_(30) = 15
              L_eri = .true.
              L_esi = .false.
          endif
          if (L_(106)) then
              L_esi = .true.
              L_eri = .false.
          endif
          if (I_(30).ne.15) then
              L_eri = .false.
              L_esi = .false.
          endif
      else
          L_esi = .false.
      endif
C FDA40_logic_box_step.fgi( 124, 371):��� ������� ���������,FDA40_BOX_AVT
C sav1=L_ari
C FDA40_logic_box_step.fgi( 166, 371):recalc:������,FDA40_BOAT_START_LOAD1
C if(sav1.ne.L_ari .and. try1900.gt.0) goto 1900
C sav1=L_isi
C sav2=L_(134)
      if(L_oso) then
          if (L_esi) then
              I_(30) = 16
              L_isi = .true.
              L_(134) = .false.
          endif
          if (L_(135)) then
              L_(134) = .true.
              L_isi = .false.
          endif
          if (I_(30).ne.16) then
              L_isi = .false.
              L_(134) = .false.
          endif
      else
          L_(134) = .false.
      endif
C FDA40_logic_box_step.fgi( 124, 339):recalc:��� ������� ���������,FDA40_BOX_AVT
C if(sav1.ne.L_isi .and. try1923.gt.0) goto 1923
C if(sav2.ne.L_(134) .and. try1923.gt.0) goto 1923
      I_i=I_(30)
C FDA40_logic_box_step.fgi( 124, 171):������-�������: ���������� ��� �������������� ������
      L_(534)=.not.L_(584) .and.L_(587)
      L_(532)=L_(587).and..not.L_(533)
C FDA40_logic_press_step.fgi( 258, 868):��������� ������
      iv2=0
      if(L_(565)) iv2=ibset(iv2,0)
      if(L_(534)) iv2=ibset(iv2,1)
      L_(682)=L_(682).or.iv2.ne.0
C FDA40_logic_press_step.fgi( 463, 916):������-�������: ������� ������ ������/����������/����� ���������
      L_(537)=.not.L_(584) .and.L_(587)
      L_(535)=L_(587).and..not.L_(536)
C FDA40_logic_press_step.fgi( 256, 884):��������� ������
      iv2=0
      if(L_(537)) iv2=ibset(iv2,0)
      if(L_(480)) iv2=ibset(iv2,1)
      if(L_(468)) iv2=ibset(iv2,2)
      L_(696)=L_(696).or.iv2.ne.0
C FDA40_vlv.fgi(  69,  89):������-�������: ������� ������ ������/����������/����� ���������
      L_(459)=.not.L_(584) .and.L_(463)
      L_(457)=L_(463).and..not.L_(458)
C FDA40_logic_press_step.fgi( 464, 540):��������� ������
      iv2=0
      if(L_(552)) iv2=ibset(iv2,0)
      if(L_(508)) iv2=ibset(iv2,1)
      if(L_(491)) iv2=ibset(iv2,2)
      if(L_(459)) iv2=ibset(iv2,3)
      L_(702)=L_(702).or.iv2.ne.0
C FDA40_vlv.fgi(  48,  89):������-�������: ������� ������ ������/����������/����� ���������
      L_(449) = L_(456).AND.L_(450)
C FDA40_logic_press_step.fgi( 367, 526):�
      L_(448)=.not.L_(584) .and.L_(449)
      L_(446)=L_(449).and..not.L_(447)
C FDA40_logic_press_step.fgi( 377, 526):��������� ������
      iv2=0
      if(L_(543)) iv2=ibset(iv2,0)
      if(L_(504)) iv2=ibset(iv2,1)
      if(L_(486)) iv2=ibset(iv2,2)
      if(L_(448)) iv2=ibset(iv2,3)
      if(L_(425)) iv2=ibset(iv2,4)
      if(L_(407)) iv2=ibset(iv2,5)
      L_(701)=L_(701).or.iv2.ne.0
C FDA40_vlv.fgi(  48,  89):������-�������: ������� ������ ������/����������/����� ���������
      Call TELEGKA_TRANSP_HANDLER(deltat,L_uxim,L_(699),L_efom
     &,L_(700),R_imom,
     & R_omom,R_ikom,R_akom,L_amom,R_okom,R_ekom,
     & L_emom,L_omim,L_umim,L_(701),L_udom,R_ofom,R_ufom,
     & R_ulom,R_ubom,R_olom,L_idom,L_edom,R_esim,
     & REAL(R_asim,4),R_uvim,REAL(R_exim,4),L_ivim,
     & R_evim,REAL(R_ovim,4),L_osim,R_isim,
     & REAL(R_usim,4),L_ifom,L_(704),L_(705),L_axim,
     & L_ibom,L_ukom,L_(703),L_(702),L_alom,L_ilom,L_adom
     &)
C FDA40_vlv.fgi(  48,  89):���������� ������� ������������,20FDA40_MATRIX
      R_(320)=abs(R_esim)
C FDA40_vlv.fgi(  92,   5):���������� ��������
      L_(672)=R_(320).gt.R0_otel
C FDA40_vlv.fgi( 100,   5):���������� >
      if(L_(672)) then
         I_itel=I_(209)
      else
         I_itel=I_(210)
      endif
C FDA40_vlv.fgi( 108,  14):���� RE IN LO CH7
      L_(268)=R_ubom.lt.R0_exo
C FDA40_logic_parameters.fgi( 279, 472):���������� <
      if(L_(268)) then
         I_ekad=I_(63)
      else
         I_ekad=I_(64)
      endif
C FDA40_logic_parameters.fgi( 290, 477):���� RE IN LO CH7
      L_(269)=R_ubom.gt.R0_ixo
C FDA40_logic_parameters.fgi( 279, 484):���������� >
      if(L_(269)) then
         I_ikad=I_(65)
      else
         I_ikad=I_(66)
      endif
C FDA40_logic_parameters.fgi( 290, 489):���� RE IN LO CH7
      L_(218)=R_ubom.lt.R0_oxo
C FDA40_logic_parameters.fgi( 279, 494):���������� <
      L_(219)=R_ubom.gt.R0_uxo
C FDA40_logic_parameters.fgi( 279, 500):���������� >
      L_(270) = L_(219).AND.L_(218)
C FDA40_logic_parameters.fgi( 283, 498):�
      if(L_(270)) then
         I_okad=I_(67)
      else
         I_okad=I_(68)
      endif
C FDA40_logic_parameters.fgi( 290, 504):���� RE IN LO CH7
      L_(220)=R_ubom.gt.R0_ato
C FDA40_logic_parameters.fgi( 279, 516):���������� >
      L_(221)=R_ubom.lt.R0_abu
C FDA40_logic_parameters.fgi( 279, 510):���������� <
      L_(271) = L_(220).AND.L_(221)
C FDA40_logic_parameters.fgi( 283, 514):�
      if(L_(271)) then
         I_ukad=I_(69)
      else
         I_ukad=I_(70)
      endif
C FDA40_logic_parameters.fgi( 290, 521):���� RE IN LO CH7
      L_(226)=R_ubom.gt.R0_adu
C FDA40_logic_parameters.fgi( 279, 550):���������� >
      L_(225)=R_ubom.lt.R0_ubu
C FDA40_logic_parameters.fgi( 279, 544):���������� <
      L_(272) = L_(226).AND.L_(225)
C FDA40_logic_parameters.fgi( 283, 548):�
      if(L_(272)) then
         I_elad=I_(73)
      else
         I_elad=I_(74)
      endif
C FDA40_logic_parameters.fgi( 290, 555):���� RE IN LO CH7
      Call TELEGKA_TRANSP_HANDLER(deltat,L_ixem,L_(692),L_udim
     &,L_(693),R_amim,
     & R_emim,R_akim,R_ofim,L_olim,R_ekim,R_ufim,
     & L_ulim,L_emem,L_imem,L_(694),L_idim,R_efim,R_ifim,
     & R_ilim,R_ibim,R_elim,L_adim,L_ubim,R_urem,
     & REAL(R_orem,4),R_ivem,REAL(R_uvem,4),L_avem,
     & R_utem,REAL(R_evem,4),L_esem,R_asem,
     & REAL(R_isem,4),L_afim,L_(697),L_(698),L_ovem,
     & L_abim,L_ikim,L_(696),L_(695),L_okim,L_alim,L_obim
     &)
C FDA40_vlv.fgi(  69,  89):���������� ������� ������������,20FDA40_BOOT
      R_(319)=abs(R_urem)
C FDA40_vlv.fgi( 158,   5):���������� ��������
      L_(671)=R_(319).gt.R0_etel
C FDA40_vlv.fgi( 166,   5):���������� >
      if(L_(671)) then
         I_atel=I_(207)
      else
         I_atel=I_(208)
      endif
C FDA40_vlv.fgi( 174,  14):���� RE IN LO CH7
      L_(228)=R_ibim.lt.R0_idu
C FDA40_logic_parameters.fgi( 189, 304):���������� <
      L_(229)=R_ibim.gt.R0_odu
C FDA40_logic_parameters.fgi( 189, 311):���������� >
      L_olad = L_(229).AND.L_(228)
C FDA40_logic_parameters.fgi( 193, 310):�
      if(L_olad) then
         I_ulad=I_(77)
      else
         I_ulad=I_(78)
      endif
C FDA40_logic_parameters.fgi( 196, 315):���� RE IN LO CH7
      L_(231)=R_ibim.gt.R0_afu
C FDA40_logic_parameters.fgi( 189, 332):���������� >
      L_(230)=R_ibim.lt.R0_udu
C FDA40_logic_parameters.fgi( 189, 325):���������� <
      L_erak = L_(231).AND.L_(230)
C FDA40_logic_parameters.fgi( 193, 331):�
      if(L_erak) then
         I_amad=I_(79)
      else
         I_amad=I_(80)
      endif
C FDA40_logic_parameters.fgi( 196, 337):���� RE IN LO CH7
      L_ivak=R_ibim.gt.R0_efu
C FDA40_logic_parameters.fgi( 189, 343):���������� >
      if(L_ivak) then
         R_(255)=R_(256)
      else
         R_(255)=R_(257)
      endif
C FDA40_logic_press_1.fgi( 131, 228):���� RE IN LO CH7
      if(L_ivak) then
         I_emad=I_(81)
      else
         I_emad=I_(82)
      endif
C FDA40_logic_parameters.fgi( 196, 348):���� RE IN LO CH7
      L_(273)=R_ibim.lt.R0_ifu
C FDA40_logic_parameters.fgi( 189, 359):���������� <
      if(L_(273)) then
         I_imad=I_(83)
      else
         I_imad=I_(84)
      endif
C FDA40_logic_parameters.fgi( 196, 364):���� RE IN LO CH7
      Call TELEGKA_TRANSP_HANDLER(deltat,L_idul,L_(678),L_ukul
     &,L_(679),R_arul,
     & R_erul,R_amul,R_olul,L_opul,R_emul,R_ulul,
     & L_upul,L_erol,L_irol,L_(680),L_ikul,R_elul,R_ilul,
     & R_ipul,R_iful,R_epul,L_akul,L_uful,R_utol,
     & REAL(R_otol,4),R_ibul,REAL(R_ubul,4),L_abul,
     & R_uxol,REAL(R_ebul,4),L_evol,R_avol,
     & REAL(R_ivol,4),L_alul,L_(683),L_(684),L_obul,
     & L_aful,L_imul,L_(682),L_(681),L_omul,L_apul,L_oful
     &)
C FDA40_vlv.fgi( 110,  89):���������� ������� ������������,20FDA40_EJECTOR
      R_(317)=abs(R_utol)
C FDA40_vlv.fgi( 291,   5):���������� ��������
      L_(669)=R_(317).gt.R0_isel
C FDA40_vlv.fgi( 299,   5):���������� >
      if(L_(669)) then
         I_esel=I_(203)
      else
         I_esel=I_(204)
      endif
C FDA40_vlv.fgi( 307,  14):���� RE IN LO CH7
      L_isad=R_iful.gt.R0_umu
C FDA40_logic_parameters.fgi( 193, 516):���������� >
      if(L_isad) then
         I_osad=I_(101)
      else
         I_osad=I_(102)
      endif
C FDA40_logic_parameters.fgi( 197, 521):���� RE IN LO CH7
      L_(280)=R_iful.lt.R0_emu
C FDA40_logic_parameters.fgi(  35, 537):���������� <
      if(L_(280)) then
         I_obed=I_(136)
      else
         I_obed=I_(135)
      endif
C FDA40_logic_parameters.fgi(  39, 542):���� RE IN LO CH7
      if(L_(280)) then
         I_oxad=I_(125)
      else
         I_oxad=I_(126)
      endif
C FDA40_logic_parameters.fgi( 117, 546):���� RE IN LO CH7
      L_(41)=I_i.eq.I_(10)
C FDA40_logic_box_step.fgi(  58,  72):���������� �������������
      L_er = L_(54).AND.L_(41)
C FDA40_logic_box_step.fgi(  70,  73):�
      L_(252)=I_i.gt.I_(29)
C FDA40_logic_parameters.fgi(  44, 246):���������� �������������
      L_(247)=I_i.gt.I_(21)
C FDA40_logic_parameters.fgi(  45, 199):���������� �������������
      if(L_(247)) then
         I_osu=I_(22)
      else
         I_osu=I_(23)
      endif
C FDA40_logic_parameters.fgi(  53, 203):���� RE IN LO CH7
      L_(249)=I_i.gt.I_(25)
C FDA40_logic_parameters.fgi(  45, 222):���������� �������������
      L_(248)=I_i.lt.I_(24)
C FDA40_logic_parameters.fgi(  45, 211):���������� �������������
      L_(250) = L_(249).AND.L_(248)
C FDA40_logic_parameters.fgi(  50, 217):�
      if(L_(250)) then
         I_usu=I_(26)
      else
         I_usu=I_(27)
      endif
C FDA40_logic_parameters.fgi(  53, 226):���� RE IN LO CH7
      L_(251)=I_i.lt.I_(28)
C FDA40_logic_parameters.fgi(  44, 235):���������� �������������
      L_(253) = L_(252).AND.L_(251)
C FDA40_logic_parameters.fgi(  49, 241):�
      if(L_(253)) then
         I_atu=I_(31)
      else
         I_atu=I_(32)
      endif
C FDA40_logic_parameters.fgi(  52, 250):���� RE IN LO CH7
      R_omok=R_omok+deltat/R0_imok*R_(310)
      if(R_omok.gt.R0_emok) then
         R_omok=R0_emok
      elseif(R_omok.lt.R0_umok) then
         R_omok=R0_umok
      endif
C FDA40_logic_press.fgi( 157, 272):����������
      R_orik=R_omok
C FDA40_logic_press.fgi( 218, 288):������,20FDA40_BUNKER_M
      R_(321)=abs(R_orap)
C FDA40_vlv.fgi(  26,   5):���������� ��������
      L_(673)=R_(321).gt.R0_avel
C FDA40_vlv.fgi(  34,   5):���������� >
      if(L_(673)) then
         I_utel=I_(211)
      else
         I_utel=I_(212)
      endif
C FDA40_vlv.fgi(  42,  14):���� RE IN LO CH7
      call lin_ext(REAL(R_ebep,4),I2_eraf,R_(132),R_upaf,R_opaf
     &)
      R0_araf=R_(132)
C FDA40_logic_press_step.fgi( 539, 518):�������-�������� ������� (20)
      R_(131) = R0_araf
C FDA40_logic_press_step.fgi( 546, 524):��������
      L_eded=R_ebep.gt.R0_omu
C FDA40_logic_parameters.fgi(  35, 574):���������� >
      if(L_eded) then
         I_ided=I_(142)
      else
         I_ided=I_(141)
      endif
C FDA40_logic_parameters.fgi(  39, 579):���� RE IN LO CH7
      if(L_eded) then
         I_abed=I_(129)
      else
         I_abed=I_(130)
      endif
C FDA40_logic_parameters.fgi( 119, 579):���� RE IN LO CH7
      L_urad=R_ebep.gt.R0_olu
C FDA40_logic_parameters.fgi( 193, 485):���������� >
      if(L_urad) then
         I_asad=I_(97)
      else
         I_asad=I_(98)
      endif
C FDA40_logic_parameters.fgi( 196, 490):���� RE IN LO CH7
      L_(239)=R_ebep.gt.R0_elu
C FDA40_logic_parameters.fgi( 188, 459):���������� >
      L_(238)=R_ebep.lt.R0_alu
C FDA40_logic_parameters.fgi( 188, 453):���������� <
      L_erad = L_(239).AND.L_(238)
C FDA40_logic_parameters.fgi( 192, 457):�
      if(L_erad) then
         I_irad=I_(93)
      else
         I_irad=I_(94)
      endif
C FDA40_logic_parameters.fgi( 197, 466):���� RE IN LO CH7
      L_(236)=R_ebep.lt.R0_oku
C FDA40_logic_parameters.fgi( 188, 432):���������� <
      L_(237)=R_ebep.gt.R0_uku
C FDA40_logic_parameters.fgi( 188, 438):���������� >
      L_upad = L_(237).AND.L_(236)
C FDA40_logic_parameters.fgi( 192, 436):�
      if(L_upad) then
         I_arad=I_(91)
      else
         I_arad=I_(92)
      endif
C FDA40_logic_parameters.fgi( 197, 443):���� RE IN LO CH7
      L_(235)=R_ebep.gt.R0_iku
C FDA40_logic_parameters.fgi( 188, 419):���������� >
      L_(234)=R_ebep.lt.R0_eku
C FDA40_logic_parameters.fgi( 188, 413):���������� <
      L_ipad = L_(235).AND.L_(234)
C FDA40_logic_parameters.fgi( 192, 417):�
      if(L_ipad) then
         I_opad=I_(89)
      else
         I_opad=I_(90)
      endif
C FDA40_logic_parameters.fgi( 197, 424):���� RE IN LO CH7
      L_(232)=R_ebep.lt.R0_ufu
C FDA40_logic_parameters.fgi( 188, 394):���������� <
      L_(233)=R_ebep.gt.R0_aku
C FDA40_logic_parameters.fgi( 188, 400):���������� >
      L_apad = L_(233).AND.L_(232)
C FDA40_logic_parameters.fgi( 192, 398):�
      if(L_apad) then
         I_epad=I_(87)
      else
         I_epad=I_(88)
      endif
C FDA40_logic_parameters.fgi( 197, 407):���� RE IN LO CH7
      L_omad=R_ebep.lt.R0_ofu
C FDA40_logic_parameters.fgi( 188, 379):���������� <
      if(L_omad) then
         I_umad=I_(85)
      else
         I_umad=I_(86)
      endif
C FDA40_logic_parameters.fgi( 197, 384):���� RE IN LO CH7
      if (.not.L_ekof.and.L_(422)) then
          I_exaf = I_exaf+1
      endif
      if (L_ekof) then
          I_exaf = 0
      endif
C FDA40_logic_press_step.fgi( 424, 388):�������
C sav1=I_ovaf
C FDA40_logic_press_step.fgi( 451, 384):recalc:������,FDA40_N_PRESS_CYCLES
C if(sav1.ne.I_ovaf .and. try1834.gt.0) goto 1834
      L_uvaf = L_(421).AND.L_(422)
C FDA40_logic_press_step.fgi( 375, 368):�
C sav1=L_(572)
      L_(572) = L_(596).OR.L_uvaf
C FDA40_logic_press_step.fgi( 425, 941):recalc:���
C if(sav1.ne.L_(572) .and. try2194.gt.0) goto 2194
      if(L_ulof) then
         I_umof=0
         L_imof=.false.
      endif
C FDA40_logic_press_step.fgi( 216, 830):����� ������� ���������,20FDA40EC001
      L_emed=(L_(15).or.L_emed).and..not.(L_ulof)
      L_(289)=.not.L_emed
C FDA40_logic_modes.fgi( 210, 574):RS �������
      L_eked=L_emed
C FDA40_logic_modes.fgi( 229, 576):������,20FDA40_INIT_RUN
      if(L_eked) then
         I_aked=I_(148)
      else
         I_aked=I_(147)
      endif
C FDA40_logic_modes.fgi( 169, 448):���� RE IN LO CH7
      L_(281) = L_oled.OR.L_eked
C FDA40_logic_modes.fgi( 221, 461):���
      if(L_(281)) then
         I_uded=I_(145)
      else
         I_uded=I_(146)
      endif
C FDA40_logic_modes.fgi( 227, 467):���� RE IN LO CH7
      if(L_emed) then
         C30_eled=C30_iked
      else
         C30_eled=C30_iled
      endif
C FDA40_logic_modes.fgi( 212, 552):���� RE IN LO CH20
      if(L_aled) then
         C30_oked=C30_uked
      else
         C30_oked=C30_eled
      endif
C FDA40_logic_modes.fgi( 227, 551):���� RE IN LO CH20
      if(L_ifed) then
         C30_uled=C30_afed
      else
         C30_uled=C30_oked
      endif
C FDA40_logic_modes.fgi( 237, 546):���� RE IN LO CH20
      if (.not.L_eref.and.L_(482)) then
          I_iref = I_iref+1
      endif
      if (L_eref) then
          I_iref = 0
      endif
C FDA40_logic_press_step.fgi( 424, 627):�������
      L_upef = L_(477).AND.L_(482)
C FDA40_logic_press_step.fgi( 375, 610):�
C sav1=L_(499)
      L_(499) = L_(544).OR.L_upef
C FDA40_logic_press_step.fgi( 424, 719):recalc:���
C if(sav1.ne.L_(499) .and. try2349.gt.0) goto 2349
      L_obof = L_(575).AND.L_(580)
C FDA40_logic_press_step.fgi( 376, 813):�
C sav1=L_(590)
      L_(590) = L_(561).OR.L_obof
C FDA40_logic_press_step.fgi( 425, 887):recalc:���
C if(sav1.ne.L_(590) .and. try2247.gt.0) goto 2247
      R_(318)=abs(R_iram)
C FDA40_vlv.fgi( 223,   5):���������� ��������
      L_(670)=R_(318).gt.R0_usel
C FDA40_vlv.fgi( 231,   5):���������� >
      if(L_(670)) then
         I_osel=I_(205)
      else
         I_osel=I_(206)
      endif
C FDA40_vlv.fgi( 239,  14):���� RE IN LO CH7
      call lin_ext(REAL(R_abem,4),I2_irik,R_(292),R_arik,R_upik
     &)
      R0_erik=R_(292)
C FDA40_logic_press.fgi(  31, 214):�������-�������� ������� (20)
      R_opik = R0_erik
C FDA40_logic_press.fgi(  38, 220):��������
      L_(240)=R_abem.lt.R0_ulu
C FDA40_logic_parameters.fgi(  30, 518):���������� <
      L_(241)=R_abem.gt.R0_amu
C FDA40_logic_parameters.fgi(  30, 524):���������� >
      L_(279) = L_(241).AND.L_(240)
C FDA40_logic_parameters.fgi(  34, 523):�
      if(L_(279)) then
         I_ibed=I_(134)
      else
         I_ibed=I_(133)
      endif
C FDA40_logic_parameters.fgi(  39, 530):���� RE IN LO CH7
      L_ufad=R_abem.lt.R0_axo
C FDA40_logic_parameters.fgi( 279, 451):���������� <
      if(L_ufad) then
         I_akad=I_(61)
      else
         I_akad=I_(62)
      endif
C FDA40_logic_parameters.fgi( 290, 456):���� RE IN LO CH7
      L_(217)=R_abem.gt.R0_uvo
C FDA40_logic_parameters.fgi( 278, 440):���������� >
      L_(216)=R_abem.lt.R0_ovo
C FDA40_logic_parameters.fgi( 278, 434):���������� <
      L_ifad = L_(217).AND.L_(216)
C FDA40_logic_parameters.fgi( 282, 438):�
      if(L_ifad) then
         I_ofad=I_(59)
      else
         I_ofad=I_(60)
      endif
C FDA40_logic_parameters.fgi( 290, 445):���� RE IN LO CH7
      L_idad=R_abem.gt.R0_eto
C FDA40_logic_parameters.fgi( 279, 375):���������� >
      if(L_idad) then
         I_odad=I_(51)
      else
         I_odad=I_(52)
      endif
C FDA40_logic_parameters.fgi( 290, 380):���� RE IN LO CH7
      if(L_ivif.and..not.L0_evif) then
         R0_utif=R0_avif
      else
         R0_utif=max(R_(191)-deltat,0.0)
      endif
      L_irul=R0_utif.gt.0.0
      L0_evif=L_ivif
C FDA40_logic_press_step.fgi( 442, 901):������������  �� T
      if(L_ekuf) then
         I_ebed=I_(131)
      else
         I_ebed=I_(132)
      endif
C FDA40_logic_parameters.fgi(  39, 508):���� RE IN LO CH7
      if(L_ufuf) then
         I_esad=I_(99)
      else
         I_esad=I_(100)
      endif
C FDA40_logic_parameters.fgi( 197, 509):���� RE IN LO CH7
      if(L_ifuf) then
         I_exad=I_(121)
      else
         I_exad=I_(122)
      endif
C FDA40_logic_parameters.fgi( 119, 428):���� RE IN LO CH7
      if(L_efuf) then
         I_utad=I_(113)
      else
         I_utad=I_(114)
      endif
C FDA40_logic_parameters.fgi( 119, 457):���� RE IN LO CH7
      if(L_etu) then
         I_itu=I_(33)
      else
         I_itu=I_(34)
      endif
C FDA40_logic_parameters.fgi(  40, 278):���� RE IN LO CH7
      L_(267)=R_iral.lt.R0_adad
C FDA40_logic_parameters.fgi(  36, 455):���������� <
      if(L_(267)) then
         I_edad=I_(49)
      else
         I_edad=I_(50)
      endif
C FDA40_logic_parameters.fgi(  43, 462):���� RE IN LO CH7
      L_(266)=R_iral.lt.R0_ubad
C FDA40_logic_parameters.fgi(  36, 444):���������� <
      L_(265)=R_iral.gt.R0_obad
C FDA40_logic_parameters.fgi(  36, 437):���������� >
      L_(264) = L_(266).AND.L_(265)
C FDA40_logic_parameters.fgi(  42, 443):�
      if(L_(264)) then
         I_ibad=I_(47)
      else
         I_ibad=I_(48)
      endif
C FDA40_logic_parameters.fgi(  49, 446):���� RE IN LO CH7
      L_(263)=R_iral.lt.R0_ebad
C FDA40_logic_parameters.fgi(  36, 427):���������� <
      L_(262)=R_iral.gt.R0_abad
C FDA40_logic_parameters.fgi(  36, 420):���������� >
      L_(261) = L_(263).AND.L_(262)
C FDA40_logic_parameters.fgi(  42, 426):�
      if(L_(261)) then
         I_uxu=I_(45)
      else
         I_uxu=I_(46)
      endif
C FDA40_logic_parameters.fgi(  49, 429):���� RE IN LO CH7
      L_(260)=R_iral.lt.R0_oxu
C FDA40_logic_parameters.fgi(  36, 410):���������� <
      L_(259)=R_iral.gt.R0_ixu
C FDA40_logic_parameters.fgi(  36, 403):���������� >
      L_(258) = L_(260).AND.L_(259)
C FDA40_logic_parameters.fgi(  42, 409):�
      if(L_(258)) then
         I_exu=I_(43)
      else
         I_exu=I_(44)
      endif
C FDA40_logic_parameters.fgi(  49, 412):���� RE IN LO CH7
      L_(245)=R_iral.lt.R0_aru
C FDA40_logic_parameters.fgi(  36, 392):���������� <
      L_(244)=R_iral.gt.R0_upu
C FDA40_logic_parameters.fgi(  36, 385):���������� >
      L_(243) = L_(245).AND.L_(244)
C FDA40_logic_parameters.fgi(  42, 391):�
      if(L_(243)) then
         I_opu=I_(12)
      else
         I_opu=I_(13)
      endif
C FDA40_logic_parameters.fgi(  49, 394):���� RE IN LO CH7
      L_(274)=R_iral.lt.R0_epu
C FDA40_logic_parameters.fgi( 114, 464):���������� <
      if(L_(274)) then
         I_avad=I_(115)
      else
         I_avad=I_(116)
      endif
C FDA40_logic_parameters.fgi( 119, 469):���� RE IN LO CH7
      L_efo=L_ifo
C FDA40_logic_box_step.fgi( 174, 522):������,FDA40_ASK_BOAT_NUMBER
      if(L_olo) then
         R0_ok=0.0
      elseif(L0_uk) then
         R0_ok=R0_ik
      else
         R0_ok=max(R_(7)-deltat,0.0)
      endif
      L_(107)=L_olo.or.R0_ok.gt.0.0
      L0_uk=L_olo
C FDA40_logic_box_step.fgi(  59, 611):�������� ������� ������
      L_ite = L_(180).AND.L_(107)
C FDA40_logic_box_step.fgi(  70, 612):�
      End
      Subroutine FDA40_2(ext_deltat)
      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      Include 'FDA40.fh'

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      R_(322)=abs(R_ovel)
C FDA40_vlv.fgi( 170,  29):���������� ��������
      L_(674)=R_(322).gt.R0_ivel
C FDA40_vlv.fgi( 178,  29):���������� >
      if(L_(674)) then
         I_evel=I_(213)
      else
         I_evel=I_(214)
      endif
C FDA40_vlv.fgi( 186,  38):���� RE IN LO CH7
      L_(256)=R_eruk.gt.R0_ivu
C FDA40_logic_parameters.fgi(  36, 321):���������� >
      if(L_(256)) then
         I_ovu=I_(39)
      else
         I_ovu=I_(40)
      endif
C FDA40_logic_parameters.fgi(  43, 328):���� RE IN LO CH7
      L_(257)=R_eruk.lt.R0_uvu
C FDA40_logic_parameters.fgi(  36, 339):���������� <
      if(L_(257)) then
         I_axu=I_(41)
      else
         I_axu=I_(42)
      endif
C FDA40_logic_parameters.fgi(  43, 346):���� RE IN LO CH7
      R_(323)=abs(R_erum)
C FDA40_vlv.fgi(  91,  29):���������� ��������
      L_(675)=R_(323).gt.R0_axel
C FDA40_vlv.fgi(  99,  29):���������� >
      if(L_(675)) then
         I_uvel=I_(215)
      else
         I_uvel=I_(216)
      endif
C FDA40_vlv.fgi( 107,  38):���� RE IN LO CH7
      L_(255)=R_uxum.lt.R0_avu
C FDA40_logic_parameters.fgi(  36, 304):���������� <
      if(L_(255)) then
         I_evu=I_(37)
      else
         I_evu=I_(38)
      endif
C FDA40_logic_parameters.fgi(  43, 311):���� RE IN LO CH7
      L_(254)=R_uxum.gt.R0_otu
C FDA40_logic_parameters.fgi(  36, 286):���������� >
      if(L_(254)) then
         I_utu=I_(35)
      else
         I_utu=I_(36)
      endif
C FDA40_logic_parameters.fgi(  43, 293):���� RE IN LO CH7
      R_(68) = I_api * R_(56) * R_(55)
C FDA40_logic_box_step.fgi( 312, 440):����������
      R_ax = R_ef + R_(68)
C FDA40_logic_box_step.fgi( 346, 441):��������
      I_oraf = I_ovaf * I_(196)
C FDA40_logic_press_step.fgi( 502, 398):����������
      if(L_ekof.and..not.L0_umaf) then
         R0_emaf=R0_imaf
      else
         R0_emaf=max(R_(129)-deltat,0.0)
      endif
      L_omaf=R0_emaf.gt.0.0
      L0_umaf=L_ekof
C FDA40_logic_press_step.fgi( 445, 355):������������  �� T
      if(L_ekof) then
         I_(199)=0
         L_ilof=.false.
      endif
C FDA40_logic_press_step.fgi( 416, 297):����� ������� ���������,20FDA40EC002
      I_e=I_(199)
C FDA40_logic_press_step.fgi( 417, 961):������-�������: ���������� ��� �������������� ������
      L_(404)=I_e.eq.I_(195)
C FDA40_logic_press_step.fgi( 551, 507):���������� �������������
      if(L_(404)) then
         R_epaf=R_(131)
      else
         R_epaf=R_(130)
      endif
C FDA40_logic_press_step.fgi( 558, 524):���� RE IN LO CH7
      L_if=(L_ide.or.L_if).and..not.(L_udo)
      L_(23)=.not.L_if
C FDA40_logic_box_step.fgi( 330, 458):RS �������
      L_eru=L_if
C FDA40_logic_box_step.fgi( 354, 460):������,FDA40_OUT_WEITH
      if(L_eru) then
         I_iru=I_(14)
      else
         I_iru=I_(15)
      endif
C FDA40_logic_parameters.fgi(  53, 166):���� RE IN LO CH7
      L_uf=(L_ebo.or.L_uf).and..not.(L_udo)
      L_(24)=.not.L_uf
C FDA40_logic_box_step.fgi( 282, 482):RS �������
      L_oru=L_uf
C FDA40_logic_box_step.fgi( 312, 484):������,FDA40_IN_WEITH
      if(L_oru) then
         I_uru=I_(16)
      else
         I_uru=I_(17)
      endif
C FDA40_logic_parameters.fgi(  53, 178):���� RE IN LO CH7
      if(L_uf) then
         R_(6)=R_ef
      else
         R_(6)=R_(4)
      endif
C FDA40_logic_box_step.fgi( 291, 491):���� RE IN LO CH7
      if(L_asok) then
         I_atad=I_(105)
      else
         I_atad=I_(106)
      endif
C FDA40_logic_parameters.fgi( 197, 550):���� RE IN LO CH7
      if(L_asok) then
         I_evad=I_(117)
      else
         I_evad=I_(118)
      endif
C FDA40_logic_parameters.fgi( 119, 482):���� RE IN LO CH7
      L_urok=R0_osok.lt.R0_urik
C FDA40_logic_press.fgi( 199, 230):���������� <
      if(L_ipok) then
         R_(273)=R_opok
      else
         R_(273)=R_(274)
      endif
C FDA40_logic_press_1.fgi( 384, 572):���� RE IN LO CH7
      L_(278) = L_ukuf.OR.L_okuf
C FDA40_logic_parameters.fgi( 115, 492):���
      if(L_(278)) then
         I_ixad=I_(123)
      else
         I_ixad=I_(124)
      endif
C FDA40_logic_parameters.fgi( 119, 497):���� RE IN LO CH7
      if(L_okuf) then
         I_afad=I_(55)
      else
         I_afad=I_(56)
      endif
C FDA40_logic_parameters.fgi( 290, 413):���� RE IN LO CH7
      if(L_ukuf) then
         I_efad=I_(57)
      else
         I_efad=I_(58)
      endif
C FDA40_logic_parameters.fgi( 290, 428):���� RE IN LO CH7
      if(L_esik) then
         I_usad=I_(103)
      else
         I_usad=I_(104)
      endif
C FDA40_logic_parameters.fgi( 197, 538):���� RE IN LO CH7
      R_(316)=abs(R8_usom)
C FDA40_vlv.fgi(  25,  55):���������� ��������
      L_(677)=R_(316).gt.R0_asel
C FDA40_vlv.fgi(  33,  55):���������� >
      if(L_(677)) then
         I_ibil=I_(225)
      else
         I_ibil=I_(226)
      endif
C FDA40_vlv.fgi(  35,  62):���� RE IN LO CH7
      if(L_aval) then
         I_etad=I_(107)
      else
         I_etad=I_(108)
      endif
C FDA40_logic_parameters.fgi( 197, 562):���� RE IN LO CH7
      if(L_otal) then
         I_udad=I_(53)
      else
         I_udad=I_(54)
      endif
C FDA40_logic_parameters.fgi( 290, 397):���� RE IN LO CH7
      if(L_utal) then
         I_itad=I_(109)
      else
         I_itad=I_(110)
      endif
C FDA40_logic_parameters.fgi( 197, 574):���� RE IN LO CH7
      if(L_arak) then
         I_ilad=I_(75)
      else
         I_ilad=I_(76)
      endif
C FDA40_logic_parameters.fgi( 290, 573):���� RE IN LO CH7
      L_(276) = L_arak.AND.L_uduf
C FDA40_logic_parameters.fgi( 118, 519):�
      if(L_ikuf) then
         I_alad=I_(71)
      else
         I_alad=I_(72)
      endif
C FDA40_logic_parameters.fgi( 290, 538):���� RE IN LO CH7
      if(L_ikuf) then
         I_ubed=I_(138)
      else
         I_ubed=I_(137)
      endif
C FDA40_logic_parameters.fgi(  39, 553):���� RE IN LO CH7
      L_(277) = L_ikuf.AND.L_afuf
C FDA40_logic_parameters.fgi( 118, 530):�
      L_(275) = L_(277).OR.L_(276)
C FDA40_logic_parameters.fgi( 122, 529):���
      if(L_(275)) then
         I_ivad=I_(119)
      else
         I_ivad=I_(120)
      endif
C FDA40_logic_parameters.fgi( 130, 534):���� RE IN LO CH7
      if(L_ifek) then
         I_uxad=I_(127)
      else
         I_uxad=I_(128)
      endif
C FDA40_logic_parameters.fgi( 119, 566):���� RE IN LO CH7
      if(L_ifek) then
         I_aded=I_(140)
      else
         I_aded=I_(139)
      endif
C FDA40_logic_parameters.fgi(  39, 566):���� RE IN LO CH7
      if(L_akuf) then
         I_orad=I_(95)
      else
         I_orad=I_(96)
      endif
C FDA40_logic_parameters.fgi( 197, 478):���� RE IN LO CH7
      L_(632)=R_efek.gt.R0_upak
C FDA40_logic_press_1.fgi( 101, 222):���������� >
C label 3190  try3190=try3190-1
      L_akek = L_erak.AND.L_(632).AND.L_arak
C FDA40_logic_press_1.fgi( 114, 222):�
      if(L_akek) then
         R_(271)=R_ekek
      else
         R_(271)=R_(272)
      endif
C FDA40_logic_press_1.fgi( 395, 568):���� RE IN LO CH7
      R_(275) = R_(273) + (-R_(271))
C FDA40_logic_press_1.fgi( 403, 571):��������
      R_alek=R_alek+deltat/R0_ukek*R_(275)
      if(R_alek.gt.R0_okek) then
         R_alek=R0_okek
      elseif(R_alek.lt.R0_elek) then
         R_alek=R0_elek
      endif
C FDA40_logic_press_1.fgi( 411, 569):����������
      R_efek=R_alek
C FDA40_logic_press_1.fgi( 486, 571):������,20FDA40_BOOT_M
      if(L_akek) then
         R_(258)=R_ekek
      else
         R_(258)=R_(259)
      endif
C FDA40_logic_press_1.fgi( 120, 232):���� RE IN LO CH7
      R_(260) = R_(258) + (-R_(255))
C FDA40_logic_press_1.fgi( 139, 231):��������
      R_exak=R_exak+deltat/R0_axak*R_(260)
      if(R_exak.gt.R0_uvak) then
         R_exak=R0_uvak
      elseif(R_exak.lt.R0_ixak) then
         R_exak=R0_ixak
      endif
C FDA40_logic_press_1.fgi( 147, 229):����������
      R_otak=R_exak
C FDA40_logic_press_1.fgi( 222, 231):������,20FDA40_MATRIX_M
      Call PUMP_HANDLER(deltat,C30_akus,I_olus,L_axav,L_exav
     &,L_ixav,
     & I_ulus,I_ilus,R_udus,REAL(R_efus,4),
     & R_edus,REAL(R_odus,4),I_amus,L_orus,L_opus,L_itus,
     & L_otus,L_ukus,L_umus,L_epus,L_apus,L_ipus,L_erus,L_adus
     &,
     & L_afus,L_ubus,L_idus,L_usus,L_(721),
     & L_atus,L_(720),L_obus,L_ibus,L_alus,I_emus,R_asus,R_esus
     &,
     & L_evus,L_irus,L_etus,REAL(R8_okot,8),L_arus,
     & REAL(R8_upus,8),R_utus,REAL(R_imus,4),R_omus,REAL(R8_okus
     &,8),R_ikus,
     & R8_arot,R_avus,R8_upus,REAL(R_ifus,4),REAL(R_ofus,4
     &))
C FDA40_vlv.fgi( 102, 170):���������� ���������� �������,20FDA41CU001KN01
C label 3215  try3215=try3215-1
C sav1=R_avus
C sav2=R_utus
C sav3=L_orus
C sav4=L_erus
C sav5=L_opus
C sav6=R8_upus
C sav7=R_omus
C sav8=I_emus
C sav9=I_amus
C sav10=I_ulus
C sav11=I_olus
C sav12=I_ilus
C sav13=I_elus
C sav14=L_alus
C sav15=L_ukus
C sav16=R_ikus
C sav17=C30_akus
C sav18=L_afus
C sav19=L_idus
      Call PUMP_HANDLER(deltat,C30_akus,I_olus,L_axav,L_exav
     &,L_ixav,
     & I_ulus,I_ilus,R_udus,REAL(R_efus,4),
     & R_edus,REAL(R_odus,4),I_amus,L_orus,L_opus,L_itus,
     & L_otus,L_ukus,L_umus,L_epus,L_apus,L_ipus,L_erus,L_adus
     &,
     & L_afus,L_ubus,L_idus,L_usus,L_(721),
     & L_atus,L_(720),L_obus,L_ibus,L_alus,I_emus,R_asus,R_esus
     &,
     & L_evus,L_irus,L_etus,REAL(R8_okot,8),L_arus,
     & REAL(R8_upus,8),R_utus,REAL(R_imus,4),R_omus,REAL(R8_okus
     &,8),R_ikus,
     & R8_arot,R_avus,R8_upus,REAL(R_ifus,4),REAL(R_ofus,4
     &))
C FDA40_vlv.fgi( 102, 170):recalc:���������� ���������� �������,20FDA41CU001KN01
C if(sav1.ne.R_avus .and. try3215.gt.0) goto 3215
C if(sav2.ne.R_utus .and. try3215.gt.0) goto 3215
C if(sav3.ne.L_orus .and. try3215.gt.0) goto 3215
C if(sav4.ne.L_erus .and. try3215.gt.0) goto 3215
C if(sav5.ne.L_opus .and. try3215.gt.0) goto 3215
C if(sav6.ne.R8_upus .and. try3215.gt.0) goto 3215
C if(sav7.ne.R_omus .and. try3215.gt.0) goto 3215
C if(sav8.ne.I_emus .and. try3215.gt.0) goto 3215
C if(sav9.ne.I_amus .and. try3215.gt.0) goto 3215
C if(sav10.ne.I_ulus .and. try3215.gt.0) goto 3215
C if(sav11.ne.I_olus .and. try3215.gt.0) goto 3215
C if(sav12.ne.I_ilus .and. try3215.gt.0) goto 3215
C if(sav13.ne.I_elus .and. try3215.gt.0) goto 3215
C if(sav14.ne.L_alus .and. try3215.gt.0) goto 3215
C if(sav15.ne.L_ukus .and. try3215.gt.0) goto 3215
C if(sav16.ne.R_ikus .and. try3215.gt.0) goto 3215
C if(sav17.ne.C30_akus .and. try3215.gt.0) goto 3215
C if(sav18.ne.L_afus .and. try3215.gt.0) goto 3215
C if(sav19.ne.L_idus .and. try3215.gt.0) goto 3215
      if(L_erus) then
         R_(97)=R_(98)
      else
         R_(97)=R_(99)
      endif
C FDA40_vent_log.fgi(  36, 126):���� RE IN LO CH7
      R8_emud=(R0_ulud*R_(96)+deltat*R_(97))/(R0_ulud+deltat
     &)
C FDA40_vent_log.fgi(  48, 127):�������������� �����  
      R8_amud=R8_emud
C FDA40_vent_log.fgi(  66, 127):������,F_FDA41CU001_G
      Call PUMP2_HANDLER(deltat,I_avis,I_utis,R_iris,
     & REAL(R_uris,4),R_upis,REAL(R_eris,4),L_ibos,
     & L_oxis,L_efos,L_ifos,L_etis,L_uvis,L_exis,L_axis,L_ixis
     &,L_ebos,
     & L_opis,L_oris,L_ipis,L_aris,L_odos,
     & L_udos,L_epis,L_apis,L_itis,I_evis,R_ubos,R_ados,L_akos
     &,
     & L_irus,L_afos,REAL(R8_okot,8),L_abos,REAL(R8_uxis,8
     &),R_ofos,
     & REAL(R_ivis,4),R_ovis,REAL(R8_atis,8),R_usis,R8_arot
     &,R_ufos,R8_uxis,
     & REAL(R_asis,4),REAL(R_esis,4))
C FDA40_vlv.fgi( 119, 145):���������� ���������� ������� 2,20FDA40AN001
C label 3223  try3223=try3223-1
C sav1=R_ufos
C sav2=R_ofos
C sav3=L_ibos
C sav4=L_ebos
C sav5=L_oxis
C sav6=R8_uxis
C sav7=R_ovis
C sav8=I_evis
C sav9=I_avis
C sav10=I_utis
C sav11=I_otis
C sav12=L_itis
C sav13=L_etis
C sav14=R_usis
C sav15=L_oris
C sav16=L_aris
      Call PUMP2_HANDLER(deltat,I_avis,I_utis,R_iris,
     & REAL(R_uris,4),R_upis,REAL(R_eris,4),L_ibos,
     & L_oxis,L_efos,L_ifos,L_etis,L_uvis,L_exis,L_axis,L_ixis
     &,L_ebos,
     & L_opis,L_oris,L_ipis,L_aris,L_odos,
     & L_udos,L_epis,L_apis,L_itis,I_evis,R_ubos,R_ados,L_akos
     &,
     & L_irus,L_afos,REAL(R8_okot,8),L_abos,REAL(R8_uxis,8
     &),R_ofos,
     & REAL(R_ivis,4),R_ovis,REAL(R8_atis,8),R_usis,R8_arot
     &,R_ufos,R8_uxis,
     & REAL(R_asis,4),REAL(R_esis,4))
C FDA40_vlv.fgi( 119, 145):recalc:���������� ���������� ������� 2,20FDA40AN001
C if(sav1.ne.R_ufos .and. try3223.gt.0) goto 3223
C if(sav2.ne.R_ofos .and. try3223.gt.0) goto 3223
C if(sav3.ne.L_ibos .and. try3223.gt.0) goto 3223
C if(sav4.ne.L_ebos .and. try3223.gt.0) goto 3223
C if(sav5.ne.L_oxis .and. try3223.gt.0) goto 3223
C if(sav6.ne.R8_uxis .and. try3223.gt.0) goto 3223
C if(sav7.ne.R_ovis .and. try3223.gt.0) goto 3223
C if(sav8.ne.I_evis .and. try3223.gt.0) goto 3223
C if(sav9.ne.I_avis .and. try3223.gt.0) goto 3223
C if(sav10.ne.I_utis .and. try3223.gt.0) goto 3223
C if(sav11.ne.I_otis .and. try3223.gt.0) goto 3223
C if(sav12.ne.L_itis .and. try3223.gt.0) goto 3223
C if(sav13.ne.L_etis .and. try3223.gt.0) goto 3223
C if(sav14.ne.R_usis .and. try3223.gt.0) goto 3223
C if(sav15.ne.L_oris .and. try3223.gt.0) goto 3223
C if(sav16.ne.L_aris .and. try3223.gt.0) goto 3223
      if(L_ebos) then
         R_(118)=R_(119)
      else
         R_(118)=R_(120)
      endif
C FDA40_vent_log.fgi(  37, 266):���� RE IN LO CH7
      R8_ixud=(R0_axud*R_(117)+deltat*R_(118))/(R0_axud+deltat
     &)
C FDA40_vent_log.fgi(  49, 267):�������������� �����  
      R8_exud=R8_ixud
C FDA40_vent_log.fgi(  66, 267):������,F_FDA40AN001_G
      L_(626)=R0_udak.gt.R_(229)
C FDA40_logic_press_1.fgi( 157,  99):���������� >
C label 3231  try3231=try3231-1
      L0_ufak=(L_(626).or.L0_ufak).and..not.(L_(627))
      L_(628)=.not.L0_ufak
C FDA40_logic_press_1.fgi( 173,  81):RS �������
      if(L0_ufak.and..not.L0_ofak) then
         R0_efak=R_ifak
      else
         R0_efak=max(R_(236)-deltat,0.0)
      endif
      L_(631)=R0_efak.gt.0.0
      L0_ofak=L0_ufak
C FDA40_logic_press_1.fgi( 184,  83):������������  �� T
      L0_obak=(L_(627).or.L0_obak).and..not.(L_(626))
      L_(625)=.not.L0_obak
C FDA40_logic_press_1.fgi( 184,  75):RS �������
      L_(629) = L_(631).OR.L0_obak
C FDA40_logic_press_1.fgi( 196,  82):���
      if(L_(629)) then
         R_(234)=R_ekak
      else
         R_(234)=R_(231)
      endif
C FDA40_logic_press_1.fgi(  98, 103):���� RE IN LO CH7
      if(L_(630)) then
         R0_udak=R_(235)
      elseif(L_(630)) then
         R0_udak=R0_udak
      else
         R0_udak=R0_udak+deltat/R_(235)*R_(234)
      endif
      if(R0_udak.gt.R_(232)) then
         R0_udak=R_(232)
      elseif(R0_udak.lt.R_(233)) then
         R0_udak=R_(233)
      endif
C FDA40_logic_press_1.fgi( 117, 104):����������
      I_(200)=R0_udak
C FDA40_logic_press_1.fgi( 141,  65):��������� RE->IN
      L_(624)=I_(200).le.I0_uxuf
C FDA40_logic_press_1.fgi( 148,  65):���������� �������������
      L0_axuf=(L_adak.or.L0_axuf).and..not.(L_(624))
      L_(623)=.not.L0_axuf
C FDA40_logic_press_1.fgi( 184,  67):RS �������
      L_abak=L_(623).and..not.L0_exuf
      L0_exuf=L_(623)
C FDA40_logic_press_1.fgi( 194,  65):������������  �� 1 ���
      R_(227) = R_ipek + (-R0_udak)
C FDA40_logic_press_1.fgi( 190, 111):��������
      if(L_adak) then
         R_idak=R_(227)
      else
         R_idak=R_(226)
      endif
C FDA40_logic_press_1.fgi( 199, 111):���� RE IN LO CH7
      R_(289) = R_ikik + R_idak
C FDA40_logic_press_1.fgi(  93, 572):��������
      R_(290) = (-R_alik) + R_(289)
C FDA40_logic_press_1.fgi(  98, 576):��������
C label 3268  try3268=try3268-1
      R_(288) = R_(287) * R_(290)
C FDA40_logic_press_1.fgi( 108, 577):����������
      R_(291) = R_(288)
C FDA40_logic_press_1.fgi( 112, 577):��������
      Call C_PID(deltat,L_omik,R8_olik,REAL(R_ulik,4),REAL
     &(R_ilik,4),R_apik,
     & REAL(R_amik,4),REAL(R_elik,4),REAL(R_emik,4),R_umik
     &,REAL(R_imik,4),R_ipik,R_epik,
     & REAL(R_(291),4))
C FDA40_logic_press_1.fgi( 123, 570):��������� �����������,UPPER_PUNCH
      R_alik = R8_olik * R_ukik
C FDA40_logic_press_1.fgi( 168, 576):����������
C sav1=R_(290)
      R_(290) = (-R_alik) + R_(289)
C FDA40_logic_press_1.fgi(  98, 576):recalc:��������
C if(sav1.ne.R_(290) .and. try3268.gt.0) goto 3268
      R_(215) = R_alik + (-R_ukik)
C FDA40_logic_press_1.fgi( 184, 590):��������
      R_(214)=abs(R_(215))
C FDA40_logic_press_1.fgi( 190, 590):���������� ��������
      L_asuf=R_(214).lt.R0_esuf
C FDA40_logic_press_1.fgi( 198, 590):���������� <
      R_(225) = R_alik + (-R_ipek)
C FDA40_logic_press_1.fgi( 184, 539):��������
      R_(224)=abs(R_(225))
C FDA40_logic_press_1.fgi( 190, 539):���������� ��������
      L_ixuf=R_(224).lt.R0_oxuf
C FDA40_logic_press_1.fgi( 198, 539):���������� <
      R_(211) = R_alik + (-R_apek)
C FDA40_logic_press_1.fgi( 184, 509):��������
      R_(210)=abs(R_(211))
C FDA40_logic_press_1.fgi( 190, 509):���������� ��������
      L_eruf=R_(210).lt.R0_iruf
C FDA40_logic_press_1.fgi( 198, 509):���������� <
      R_(209) = R_alik + (-R_umek)
C FDA40_logic_press_1.fgi( 184, 491):��������
      R_(208)=abs(R_(209))
C FDA40_logic_press_1.fgi( 190, 491):���������� ��������
      L_upuf=R_(208).lt.R0_aruf
C FDA40_logic_press_1.fgi( 198, 491):���������� <
      R_(207) = R_alik + (-R_osuf)
C FDA40_logic_press_1.fgi( 184, 523):��������
      R_(206)=abs(R_(207))
C FDA40_logic_press_1.fgi( 190, 523):���������� ��������
      L_ipuf=R_(206).lt.R0_opuf
C FDA40_logic_press_1.fgi( 198, 523):���������� <
      R_(213) = R_alik + (-R_atuf)
C FDA40_logic_press_1.fgi( 184, 552):��������
      R_(212)=abs(R_(213))
C FDA40_logic_press_1.fgi( 190, 552):���������� ��������
      L_oruf=R_(212).lt.R0_uruf
C FDA40_logic_press_1.fgi( 198, 552):���������� <
      R_(277) = R_alik + (-R_opek)
C FDA40_logic_press_1.fgi( 184, 566):��������
      R_(276)=abs(R_(277))
C FDA40_logic_press_1.fgi( 190, 566):���������� ��������
      L_imek=R_(276).lt.R0_omek
C FDA40_logic_press_1.fgi( 198, 566):���������� <
      R_(263) = R_ubek + R_otuf
C FDA40_logic_press_1.fgi(  97, 191):��������
C label 3326  try3326=try3326-1
      R_(264) = (-R_edek) + R_(263)
C FDA40_logic_press_1.fgi( 102, 195):��������
      R_(262) = R_(261) * R_(264)
C FDA40_logic_press_1.fgi( 112, 196):����������
      R_(278) = R_(262)
C FDA40_logic_press_1.fgi( 116, 196):��������
      Call C_PID(deltat,L_isek,R8_irek,REAL(R_orek,4),REAL
     &(R_erek,4),R_usek,
     & REAL(R_urek,4),REAL(R_arek,4),REAL(R_asek,4),R_osek
     &,REAL(R_esek,4),R_etek,R_atek,
     & REAL(R_(278),4))
C FDA40_logic_press_1.fgi( 127, 189):��������� �����������,MATRIX
      R_(266) = R8_irek * R_(265)
C FDA40_logic_press_1.fgi( 174, 195):����������
      R_edek = R_(266) + R_oxak
C FDA40_logic_press_1.fgi( 179, 194):��������
C sav1=R_(264)
      R_(264) = (-R_edek) + R_(263)
C FDA40_logic_press_1.fgi( 102, 195):recalc:��������
C if(sav1.ne.R_(264) .and. try3329.gt.0) goto 3329
      if(L_(620)) then
         R0_utuf=R_edek
      endif
      R_(218)=R0_utuf
C FDA40_logic_press_1.fgi( 139,  54):�������-��������
      R_(216) = R_(218) + (-R0_uvuf)
C FDA40_logic_press_1.fgi( 179,  40):��������
      if(L_(621)) then
         R_otuf=R_(216)
      else
         R_otuf=R_(217)
      endif
C FDA40_logic_press_1.fgi( 189,  40):���� RE IN LO CH7
C sav1=R_(263)
      R_(263) = R_ubek + R_otuf
C FDA40_logic_press_1.fgi(  97, 191):recalc:��������
C if(sav1.ne.R_(263) .and. try3326.gt.0) goto 3326
      R_(238) = R_edek + (-R_avak)
C FDA40_logic_press_1.fgi( 184, 154):��������
      R_(237)=abs(R_(238))
C FDA40_logic_press_1.fgi( 190, 154):���������� ��������
      L_ikak=R_(237).lt.R0_okak
C FDA40_logic_press_1.fgi( 198, 154):���������� <
      R_(203) = R_edek + (-R_oxak)
C FDA40_logic_press_1.fgi( 184, 139):��������
      R_(202)=abs(R_(203))
C FDA40_logic_press_1.fgi( 190, 139):���������� ��������
      L_omuf=R_(202).lt.R0_umuf
C FDA40_logic_press_1.fgi( 198, 139):���������� <
      R_(205) = R_edek + (-R_irak)
C FDA40_logic_press_1.fgi( 184, 146):��������
      R_(204)=abs(R_(205))
C FDA40_logic_press_1.fgi( 190, 146):���������� ��������
      L_apuf=R_(204).lt.R0_epuf
C FDA40_logic_press_1.fgi( 198, 146):���������� <
      R_(240) = R_edek + (-R_ebek)
C FDA40_logic_press_1.fgi( 184, 161):��������
      R_(239)=abs(R_(240))
C FDA40_logic_press_1.fgi( 190, 161):���������� ��������
      L_ukak=R_(239).lt.R0_alak
C FDA40_logic_press_1.fgi( 198, 161):���������� <
      R_(248) = R_edek + (-R_abek)
C FDA40_logic_press_1.fgi( 184, 168):��������
      R_(247)=abs(R_(248))
C FDA40_logic_press_1.fgi( 190, 168):���������� ��������
      L_ipak=R_(247).lt.R0_opak
C FDA40_logic_press_1.fgi( 198, 168):���������� <
      R_(252) = R_edek + (-R_obek)
C FDA40_logic_press_1.fgi( 184, 175):��������
      R_(251)=abs(R_(252))
C FDA40_logic_press_1.fgi( 190, 175):���������� ��������
      L_usak=R_(251).lt.R0_osak
C FDA40_logic_press_1.fgi( 198, 175):���������� <
      R_(254) = R_edek + (-R_itak)
C FDA40_logic_press_1.fgi( 184, 182):��������
      R_(253)=abs(R_(254))
C FDA40_logic_press_1.fgi( 190, 182):���������� ��������
      L_atak=R_(253).lt.R0_etak
C FDA40_logic_press_1.fgi( 198, 182):���������� <
      Call PUMP2_HANDLER(deltat,I_eros,I_aros,R_olos,
     & REAL(R_amos,4),R_alos,REAL(R_ilos,4),L_otos,
     & L_usos,L_ixos,L_oxos,L_ipos,L_asos,L_isos,L_esos,L_osos
     &,L_itos,
     & L_ukos,L_ulos,L_okos,L_elos,L_uvos,
     & L_axos,L_ikos,L_ekos,L_opos,I_iros,R_avos,R_evos,L_ebus
     &,
     & L_irus,L_exos,REAL(R8_okot,8),L_etos,REAL(R8_atos,8
     &),R_uxos,
     & REAL(R_oros,4),R_uros,REAL(R8_epos,8),R_apos,R8_arot
     &,R_abus,R8_atos,
     & REAL(R_emos,4),REAL(R_imos,4))
C FDA40_vlv.fgi( 102, 145):���������� ���������� ������� 2,20FDA40AN002
C label 3397  try3397=try3397-1
C sav1=R_abus
C sav2=R_uxos
C sav3=L_otos
C sav4=L_itos
C sav5=L_usos
C sav6=R8_atos
C sav7=R_uros
C sav8=I_iros
C sav9=I_eros
C sav10=I_aros
C sav11=I_upos
C sav12=L_opos
C sav13=L_ipos
C sav14=R_apos
C sav15=L_ulos
C sav16=L_elos
      Call PUMP2_HANDLER(deltat,I_eros,I_aros,R_olos,
     & REAL(R_amos,4),R_alos,REAL(R_ilos,4),L_otos,
     & L_usos,L_ixos,L_oxos,L_ipos,L_asos,L_isos,L_esos,L_osos
     &,L_itos,
     & L_ukos,L_ulos,L_okos,L_elos,L_uvos,
     & L_axos,L_ikos,L_ekos,L_opos,I_iros,R_avos,R_evos,L_ebus
     &,
     & L_irus,L_exos,REAL(R8_okot,8),L_etos,REAL(R8_atos,8
     &),R_uxos,
     & REAL(R_oros,4),R_uros,REAL(R8_epos,8),R_apos,R8_arot
     &,R_abus,R8_atos,
     & REAL(R_emos,4),REAL(R_imos,4))
C FDA40_vlv.fgi( 102, 145):recalc:���������� ���������� ������� 2,20FDA40AN002
C if(sav1.ne.R_abus .and. try3397.gt.0) goto 3397
C if(sav2.ne.R_uxos .and. try3397.gt.0) goto 3397
C if(sav3.ne.L_otos .and. try3397.gt.0) goto 3397
C if(sav4.ne.L_itos .and. try3397.gt.0) goto 3397
C if(sav5.ne.L_usos .and. try3397.gt.0) goto 3397
C if(sav6.ne.R8_atos .and. try3397.gt.0) goto 3397
C if(sav7.ne.R_uros .and. try3397.gt.0) goto 3397
C if(sav8.ne.I_iros .and. try3397.gt.0) goto 3397
C if(sav9.ne.I_eros .and. try3397.gt.0) goto 3397
C if(sav10.ne.I_aros .and. try3397.gt.0) goto 3397
C if(sav11.ne.I_upos .and. try3397.gt.0) goto 3397
C if(sav12.ne.L_opos .and. try3397.gt.0) goto 3397
C if(sav13.ne.L_ipos .and. try3397.gt.0) goto 3397
C if(sav14.ne.R_apos .and. try3397.gt.0) goto 3397
C if(sav15.ne.L_ulos .and. try3397.gt.0) goto 3397
C if(sav16.ne.L_elos .and. try3397.gt.0) goto 3397
      if(L_itos) then
         R_(114)=R_(115)
      else
         R_(114)=R_(116)
      endif
C FDA40_vent_log.fgi( 110, 266):���� RE IN LO CH7
      R8_ivud=(R0_avud*R_(113)+deltat*R_(114))/(R0_avud+deltat
     &)
C FDA40_vent_log.fgi( 122, 267):�������������� �����  
      R8_evud=R8_ivud
C FDA40_vent_log.fgi( 140, 267):������,F_FDA40AN002_G
      Call PUMP2_HANDLER(deltat,I_ixul,I_exul,R_usul,
     & REAL(R_etul,4),R_esul,REAL(R_osul,4),L_udam,
     & L_adam,L_okam,L_ukam,L_ovul,L_ebam,L_obam,L_ibam,L_ubam
     &,L_odam,
     & L_asul,L_atul,L_urul,L_isul,L_akam,
     & L_ekam,L_orul,L_irul,L_uvul,I_oxul,R_efam,R_ifam,L_ilam
     &,
     & L_irus,L_ikam,REAL(R8_okot,8),L_idam,REAL(R8_edam,8
     &),R_alam,
     & REAL(R_uxul,4),R_abam,REAL(R8_ivul,8),R_evul,R8_arot
     &,R_elam,R8_edam,
     & REAL(R_itul,4),REAL(R_otul,4))
C FDA40_vlv.fgi( 138,  88):���������� ���������� ������� 2,20FDA40_TRANSPORTER
C label 3405  try3405=try3405-1
C sav1=R_elam
C sav2=R_alam
C sav3=L_udam
C sav4=L_odam
C sav5=L_adam
C sav6=R8_edam
C sav7=R_abam
C sav8=I_oxul
C sav9=I_ixul
C sav10=I_exul
C sav11=I_axul
C sav12=L_uvul
C sav13=L_ovul
C sav14=R_evul
C sav15=L_atul
C sav16=L_isul
      Call PUMP2_HANDLER(deltat,I_ixul,I_exul,R_usul,
     & REAL(R_etul,4),R_esul,REAL(R_osul,4),L_udam,
     & L_adam,L_okam,L_ukam,L_ovul,L_ebam,L_obam,L_ibam,L_ubam
     &,L_odam,
     & L_asul,L_atul,L_urul,L_isul,L_akam,
     & L_ekam,L_orul,L_irul,L_uvul,I_oxul,R_efam,R_ifam,L_ilam
     &,
     & L_irus,L_ikam,REAL(R8_okot,8),L_idam,REAL(R8_edam,8
     &),R_alam,
     & REAL(R_uxul,4),R_abam,REAL(R8_ivul,8),R_evul,R8_arot
     &,R_elam,R8_edam,
     & REAL(R_itul,4),REAL(R_otul,4))
C FDA40_vlv.fgi( 138,  88):recalc:���������� ���������� ������� 2,20FDA40_TRANSPORTER
C if(sav1.ne.R_elam .and. try3405.gt.0) goto 3405
C if(sav2.ne.R_alam .and. try3405.gt.0) goto 3405
C if(sav3.ne.L_udam .and. try3405.gt.0) goto 3405
C if(sav4.ne.L_odam .and. try3405.gt.0) goto 3405
C if(sav5.ne.L_adam .and. try3405.gt.0) goto 3405
C if(sav6.ne.R8_edam .and. try3405.gt.0) goto 3405
C if(sav7.ne.R_abam .and. try3405.gt.0) goto 3405
C if(sav8.ne.I_oxul .and. try3405.gt.0) goto 3405
C if(sav9.ne.I_ixul .and. try3405.gt.0) goto 3405
C if(sav10.ne.I_exul .and. try3405.gt.0) goto 3405
C if(sav11.ne.I_axul .and. try3405.gt.0) goto 3405
C if(sav12.ne.L_uvul .and. try3405.gt.0) goto 3405
C if(sav13.ne.L_ovul .and. try3405.gt.0) goto 3405
C if(sav14.ne.R_evul .and. try3405.gt.0) goto 3405
C if(sav15.ne.L_atul .and. try3405.gt.0) goto 3405
C if(sav16.ne.L_isul .and. try3405.gt.0) goto 3405
      if(L_odam) then
         I_ebil=I_(223)
      else
         I_ebil=I_(224)
      endif
C FDA40_vlv.fgi(  90,  61):���� RE IN LO CH7
      R_(127) = (-R_udaf) + R_idaf
C FDA40_logic_loadunload.fgi(  98, 286):��������
C label 3411  try3411=try3411-1
      R_(126) = R_(125) * R_(127)
C FDA40_logic_loadunload.fgi( 108, 287):����������
      R_(128) = R_(126)
C FDA40_logic_loadunload.fgi( 112, 287):��������
      Call C_PID(deltat,L_ikaf,R8_ifaf,REAL(R_ofaf,4),REAL
     &(R_efaf,4),R_ukaf,
     & REAL(R_ufaf,4),REAL(R_afaf,4),REAL(R_akaf,4),R_okaf
     &,REAL(R_ekaf,4),R_elaf,R_alaf,
     & REAL(R_(128),4))
C FDA40_logic_loadunload.fgi( 123, 280):��������� �����������,INOUT
      R_udaf = R8_ifaf * R_odaf
C FDA40_logic_loadunload.fgi( 168, 286):����������
C sav1=R_(127)
      R_(127) = (-R_udaf) + R_idaf
C FDA40_logic_loadunload.fgi(  98, 286):recalc:��������
C if(sav1.ne.R_(127) .and. try3411.gt.0) goto 3411
      R_(122) = R_udaf + (-R_ibaf)
C FDA40_logic_loadunload.fgi( 184, 269):��������
      R_(121)=abs(R_(122))
C FDA40_logic_loadunload.fgi( 190, 269):���������� ��������
      L_abaf=R_(121).lt.R0_ebaf
C FDA40_logic_loadunload.fgi( 198, 269):���������� <
      R_(124) = R_udaf + (-R_adaf)
C FDA40_logic_loadunload.fgi( 184, 276):��������
      R_(123)=abs(R_(124))
C FDA40_logic_loadunload.fgi( 190, 276):���������� ��������
      L_obaf=R_(123).lt.R0_ubaf
C FDA40_logic_loadunload.fgi( 198, 276):���������� <
      Call PUMP2_HANDLER(deltat,I_udol,I_odol,R_exil,
     & REAL(R_oxil,4),R_ovil,REAL(R_axil,4),L_elol,
     & L_ikol,L_apol,L_epol,L_adol,L_ofol,L_akol,L_ufol,L_ekol
     &,L_alol,
     & L_ivil,L_ixil,L_evil,L_uvil,L_imol,
     & L_omol,L_avil,L_util,L_edol,I_afol,R_olol,R_ulol,L_upol
     &,
     & L_irus,L_umol,REAL(R8_okot,8),L_ukol,REAL(R8_okol,8
     &),R_ipol,
     & REAL(R_efol,4),R_ifol,REAL(R8_ubol,8),R_obol,R8_arot
     &,R_opol,R8_okol,
     & REAL(R_uxil,4),REAL(R_abol,4))
C FDA40_vlv.fgi( 163,  88):���������� ���������� ������� 2,20FDA40_AGITATOR
C label 3436  try3436=try3436-1
C sav1=R_opol
C sav2=R_ipol
C sav3=L_elol
C sav4=L_alol
C sav5=L_ikol
C sav6=R8_okol
C sav7=R_ifol
C sav8=I_afol
C sav9=I_udol
C sav10=I_odol
C sav11=I_idol
C sav12=L_edol
C sav13=L_adol
C sav14=R_obol
C sav15=L_ixil
C sav16=L_uvil
      Call PUMP2_HANDLER(deltat,I_udol,I_odol,R_exil,
     & REAL(R_oxil,4),R_ovil,REAL(R_axil,4),L_elol,
     & L_ikol,L_apol,L_epol,L_adol,L_ofol,L_akol,L_ufol,L_ekol
     &,L_alol,
     & L_ivil,L_ixil,L_evil,L_uvil,L_imol,
     & L_omol,L_avil,L_util,L_edol,I_afol,R_olol,R_ulol,L_upol
     &,
     & L_irus,L_umol,REAL(R8_okot,8),L_ukol,REAL(R8_okol,8
     &),R_ipol,
     & REAL(R_efol,4),R_ifol,REAL(R8_ubol,8),R_obol,R8_arot
     &,R_opol,R8_okol,
     & REAL(R_uxil,4),REAL(R_abol,4))
C FDA40_vlv.fgi( 163,  88):recalc:���������� ���������� ������� 2,20FDA40_AGITATOR
C if(sav1.ne.R_opol .and. try3436.gt.0) goto 3436
C if(sav2.ne.R_ipol .and. try3436.gt.0) goto 3436
C if(sav3.ne.L_elol .and. try3436.gt.0) goto 3436
C if(sav4.ne.L_alol .and. try3436.gt.0) goto 3436
C if(sav5.ne.L_ikol .and. try3436.gt.0) goto 3436
C if(sav6.ne.R8_okol .and. try3436.gt.0) goto 3436
C if(sav7.ne.R_ifol .and. try3436.gt.0) goto 3436
C if(sav8.ne.I_afol .and. try3436.gt.0) goto 3436
C if(sav9.ne.I_udol .and. try3436.gt.0) goto 3436
C if(sav10.ne.I_odol .and. try3436.gt.0) goto 3436
C if(sav11.ne.I_idol .and. try3436.gt.0) goto 3436
C if(sav12.ne.L_edol .and. try3436.gt.0) goto 3436
C if(sav13.ne.L_adol .and. try3436.gt.0) goto 3436
C if(sav14.ne.R_obol .and. try3436.gt.0) goto 3436
C if(sav15.ne.L_ixil .and. try3436.gt.0) goto 3436
C if(sav16.ne.L_uvil .and. try3436.gt.0) goto 3436
      if(L_alol) then
         I_abil=I_(221)
      else
         I_abil=I_(222)
      endif
C FDA40_vlv.fgi( 146,  61):���� RE IN LO CH7
      R_(303) = (-R_odok) + R_idok
C FDA40_logic_press.fgi( 126, 209):��������
C label 3442  try3442=try3442-1
      R_(302) = R_(301) * R_(303)
C FDA40_logic_press.fgi( 137, 210):����������
      R_(305) = R_(302)
C FDA40_logic_press.fgi( 141, 210):��������
      Call C_PID(deltat,L_ikok,R8_ifok,REAL(R_ofok,4),REAL
     &(R_efok,4),R_ukok,
     & REAL(R_ufok,4),REAL(R_afok,4),REAL(R_akok,4),R_okok
     &,REAL(R_ekok,4),R_elok,R_alok,
     & REAL(R_(305),4))
C FDA40_logic_press.fgi( 152, 203):��������� �����������,TEST
      R_odok = R_(304) * R8_ifok
C FDA40_logic_press.fgi( 169, 211):����������
C sav1=R_(303)
      R_(303) = (-R_odok) + R_idok
C FDA40_logic_press.fgi( 126, 209):recalc:��������
C if(sav1.ne.R_(303) .and. try3442.gt.0) goto 3442
      L_uvik=R_odok.gt.R0_axik
C FDA40_logic_press.fgi( 221, 161):���������� >
      R_(294) = R_odok + (-R_evik)
C FDA40_logic_press.fgi( 219, 169):��������
      R_(293)=abs(R_(294))
C FDA40_logic_press.fgi( 225, 169):���������� ��������
      L_exik=R_(293).lt.R0_isik
C FDA40_logic_press.fgi( 233, 169):���������� <
      R_(296) = R_odok + (-R_ivik)
C FDA40_logic_press.fgi( 219, 177):��������
      R_(295)=abs(R_(296))
C FDA40_logic_press.fgi( 225, 177):���������� ��������
      L_oxik=R_(295).lt.R0_osik
C FDA40_logic_press.fgi( 233, 177):���������� <
      L_(634)=R_odok.lt.R0_ebok
C FDA40_logic_press.fgi( 221, 184):���������� <
      L_(635)=R_odok.gt.R0_ibok
C FDA40_logic_press.fgi( 221, 190):���������� >
      L_ixik = L_(635).AND.L_(634)
C FDA40_logic_press.fgi( 231, 187):�
      R_(298) = R_odok + (-R_ovik)
C FDA40_logic_press.fgi( 219, 197):��������
      R_(297)=abs(R_(298))
C FDA40_logic_press.fgi( 225, 197):���������� ��������
      L_uxik=R_(297).lt.R0_usik
C FDA40_logic_press.fgi( 233, 197):���������� <
      R_(300) = R_odok + (-R_avik)
C FDA40_logic_press.fgi( 219, 205):��������
      R_(299)=abs(R_(300))
C FDA40_logic_press.fgi( 225, 205):���������� ��������
      L_abok=R_(299).lt.R0_atik
C FDA40_logic_press.fgi( 233, 205):���������� <
      L_obok=R_odok.lt.R0_ubok
C FDA40_logic_press.fgi( 221, 211):���������� <
      Call PUMP2_HANDLER(deltat,I_olil,I_ilil,R_afil,
     & REAL(R_ifil,4),R_idil,REAL(R_udil,4),L_aril,
     & L_epil,L_usil,L_atil,L_ukil,L_imil,L_umil,L_omil,L_apil
     &,L_upil,
     & L_edil,L_efil,L_adil,L_odil,L_esil,
     & L_isil,L_ubil,L_obil,L_alil,I_ulil,R_iril,R_oril,L_otil
     &,
     & L_irus,L_osil,REAL(R8_okot,8),L_opil,REAL(R8_ipil,8
     &),R_etil,
     & REAL(R_amil,4),R_emil,REAL(R8_okil,8),R_ikil,R8_arot
     &,R_itil,R8_ipil,
     & REAL(R_ofil,4),REAL(R_ufil,4))
C FDA40_vlv.fgi( 189,  88):���������� ���������� ������� 2,20FDA40_VIBRO
C label 3499  try3499=try3499-1
C sav1=R_itil
C sav2=R_etil
C sav3=L_aril
C sav4=L_upil
C sav5=L_epil
C sav6=R8_ipil
C sav7=R_emil
C sav8=I_ulil
C sav9=I_olil
C sav10=I_ilil
C sav11=I_elil
C sav12=L_alil
C sav13=L_ukil
C sav14=R_ikil
C sav15=L_efil
C sav16=L_odil
      Call PUMP2_HANDLER(deltat,I_olil,I_ilil,R_afil,
     & REAL(R_ifil,4),R_idil,REAL(R_udil,4),L_aril,
     & L_epil,L_usil,L_atil,L_ukil,L_imil,L_umil,L_omil,L_apil
     &,L_upil,
     & L_edil,L_efil,L_adil,L_odil,L_esil,
     & L_isil,L_ubil,L_obil,L_alil,I_ulil,R_iril,R_oril,L_otil
     &,
     & L_irus,L_osil,REAL(R8_okot,8),L_opil,REAL(R8_ipil,8
     &),R_etil,
     & REAL(R_amil,4),R_emil,REAL(R8_okil,8),R_ikil,R8_arot
     &,R_itil,R8_ipil,
     & REAL(R_ofil,4),REAL(R_ufil,4))
C FDA40_vlv.fgi( 189,  88):recalc:���������� ���������� ������� 2,20FDA40_VIBRO
C if(sav1.ne.R_itil .and. try3499.gt.0) goto 3499
C if(sav2.ne.R_etil .and. try3499.gt.0) goto 3499
C if(sav3.ne.L_aril .and. try3499.gt.0) goto 3499
C if(sav4.ne.L_upil .and. try3499.gt.0) goto 3499
C if(sav5.ne.L_epil .and. try3499.gt.0) goto 3499
C if(sav6.ne.R8_ipil .and. try3499.gt.0) goto 3499
C if(sav7.ne.R_emil .and. try3499.gt.0) goto 3499
C if(sav8.ne.I_ulil .and. try3499.gt.0) goto 3499
C if(sav9.ne.I_olil .and. try3499.gt.0) goto 3499
C if(sav10.ne.I_ilil .and. try3499.gt.0) goto 3499
C if(sav11.ne.I_elil .and. try3499.gt.0) goto 3499
C if(sav12.ne.L_alil .and. try3499.gt.0) goto 3499
C if(sav13.ne.L_ukil .and. try3499.gt.0) goto 3499
C if(sav14.ne.R_ikil .and. try3499.gt.0) goto 3499
C if(sav15.ne.L_efil .and. try3499.gt.0) goto 3499
C if(sav16.ne.L_odil .and. try3499.gt.0) goto 3499
      if(L_upil) then
         I_uxel=I_(219)
      else
         I_uxel=I_(220)
      endif
C FDA40_vlv.fgi( 202,  61):���� RE IN LO CH7
      Call PUMP2_HANDLER(deltat,I_ufel,I_ofel,R_ebel,
     & REAL(R_obel,4),R_oxal,REAL(R_abel,4),L_emel,
     & L_ilel,L_arel,L_erel,L_afel,L_okel,L_alel,L_ukel,L_elel
     &,L_amel,
     & L_ixal,L_ibel,L_exal,L_uxal,L_ipel,
     & L_opel,L_axal,L_uval,L_efel,I_akel,R_omel,R_umel,L_urel
     &,
     & L_irus,L_upel,REAL(R8_okot,8),L_ulel,REAL(R8_olel,8
     &),R_irel,
     & REAL(R_ekel,4),R_ikel,REAL(R8_udel,8),R_odel,R8_arot
     &,R_orel,R8_olel,
     & REAL(R_ubel,4),REAL(R_adel,4))
C FDA40_vlv.fgi( 214,  88):���������� ���������� ������� 2,20FDA40_DUST
C label 3505  try3505=try3505-1
C sav1=R_orel
C sav2=R_irel
C sav3=L_emel
C sav4=L_amel
C sav5=L_ilel
C sav6=R8_olel
C sav7=R_ikel
C sav8=I_akel
C sav9=I_ufel
C sav10=I_ofel
C sav11=I_ifel
C sav12=L_efel
C sav13=L_afel
C sav14=R_odel
C sav15=L_ibel
C sav16=L_uxal
      Call PUMP2_HANDLER(deltat,I_ufel,I_ofel,R_ebel,
     & REAL(R_obel,4),R_oxal,REAL(R_abel,4),L_emel,
     & L_ilel,L_arel,L_erel,L_afel,L_okel,L_alel,L_ukel,L_elel
     &,L_amel,
     & L_ixal,L_ibel,L_exal,L_uxal,L_ipel,
     & L_opel,L_axal,L_uval,L_efel,I_akel,R_omel,R_umel,L_urel
     &,
     & L_irus,L_upel,REAL(R8_okot,8),L_ulel,REAL(R8_olel,8
     &),R_irel,
     & REAL(R_ekel,4),R_ikel,REAL(R8_udel,8),R_odel,R8_arot
     &,R_orel,R8_olel,
     & REAL(R_ubel,4),REAL(R_adel,4))
C FDA40_vlv.fgi( 214,  88):recalc:���������� ���������� ������� 2,20FDA40_DUST
C if(sav1.ne.R_orel .and. try3505.gt.0) goto 3505
C if(sav2.ne.R_irel .and. try3505.gt.0) goto 3505
C if(sav3.ne.L_emel .and. try3505.gt.0) goto 3505
C if(sav4.ne.L_amel .and. try3505.gt.0) goto 3505
C if(sav5.ne.L_ilel .and. try3505.gt.0) goto 3505
C if(sav6.ne.R8_olel .and. try3505.gt.0) goto 3505
C if(sav7.ne.R_ikel .and. try3505.gt.0) goto 3505
C if(sav8.ne.I_akel .and. try3505.gt.0) goto 3505
C if(sav9.ne.I_ufel .and. try3505.gt.0) goto 3505
C if(sav10.ne.I_ofel .and. try3505.gt.0) goto 3505
C if(sav11.ne.I_ifel .and. try3505.gt.0) goto 3505
C if(sav12.ne.L_efel .and. try3505.gt.0) goto 3505
C if(sav13.ne.L_afel .and. try3505.gt.0) goto 3505
C if(sav14.ne.R_odel .and. try3505.gt.0) goto 3505
C if(sav15.ne.L_ibel .and. try3505.gt.0) goto 3505
C if(sav16.ne.L_uxal .and. try3505.gt.0) goto 3505
      if(L_amel) then
         I_oval=I_(201)
      else
         I_oval=I_(202)
      endif
C FDA40_vlv.fgi( 251,  39):���� RE IN LO CH7
      if (.not.L_udo.and.L_ad) then
          I_ipi = I_ipi+1
      endif
      if (L_udo) then
          I_ipi = 0
      endif
C FDA40_logic_box_step.fgi( 343, 378):�������
C label 3511  try3511=try3511-1
      L_(19)=I_ipi.gt.I_ote
C FDA40_logic_box_step.fgi( 346, 365):���������� �������������
      L_ad=(L_(127).or.L_ad).and..not.(L_(19))
      L_(20)=.not.L_ad
C FDA40_logic_box_step.fgi( 297, 379):RS �������
C sav1=I_ipi
      if (.not.L_udo.and.L_ad) then
          I_ipi = I_ipi+1
      endif
      if (L_udo) then
          I_ipi = 0
      endif
C FDA40_logic_box_step.fgi( 343, 378):recalc:�������
C if(sav1.ne.I_ipi .and. try3511.gt.0) goto 3511
      I_epi=I_ipi
C FDA40_logic_box_step.fgi( 364, 372):������,FDA40_NUM_BOX_CYCLES
      R_ado = I_epi * R_(2) * R_(1)
C FDA40_logic_box_step.fgi( 356, 425):����������
      R_(5) = R_ef + R_ado
C FDA40_logic_box_step.fgi( 320, 471):��������
      if(L_if) then
         R_of=R_(5)
      else
         R_of=R_(6)
      endif
C FDA40_logic_box_step.fgi( 334, 471):���� RE IN LO CH7
      R_(281) = (-R_avek) + R_otek
C FDA40_logic_press_1.fgi( 365, 530):��������
C label 3531  try3531=try3531-1
      R_(280) = R_(279) * R_(281)
C FDA40_logic_press_1.fgi( 376, 531):����������
      R_(282) = R_(280)
C FDA40_logic_press_1.fgi( 380, 531):��������
      Call C_PID(deltat,L_oxek,R8_ovek,REAL(R_uvek,4),REAL
     &(R_ivek,4),R_abik,
     & REAL(R_axek,4),REAL(R_evek,4),REAL(R_exek,4),R_uxek
     &,REAL(R_ixek,4),R_ibik,R_ebik,
     & REAL(R_(282),4))
C FDA40_logic_press_1.fgi( 391, 524):��������� �����������,BOOT
      R_avek = R8_ovek * R_utek
C FDA40_logic_press_1.fgi( 428, 530):����������
C sav1=R_(281)
      R_(281) = (-R_avek) + R_otek
C FDA40_logic_press_1.fgi( 365, 530):recalc:��������
C if(sav1.ne.R_(281) .and. try3531.gt.0) goto 3531
      R_(270) = R_avek + (-R_emek)
C FDA40_logic_press_1.fgi( 448, 523):��������
      R_(269)=abs(R_(270))
C FDA40_logic_press_1.fgi( 454, 523):���������� ��������
      L_udek=R_(269).lt.R0_afek
C FDA40_logic_press_1.fgi( 462, 523):���������� <
      R_(242) = R_avek + (-R_ulek)
C FDA40_logic_press_1.fgi( 448, 502):��������
      R_(241)=abs(R_(242))
C FDA40_logic_press_1.fgi( 454, 502):���������� ��������
      L_ilak=R_(241).lt.R0_elak
C FDA40_logic_press_1.fgi( 462, 502):���������� <
      R_(250) = R_avek + (-R_olek)
C FDA40_logic_press_1.fgi( 448, 509):��������
      R_(249)=abs(R_(250))
C FDA40_logic_press_1.fgi( 454, 509):���������� ��������
      L_urak=R_(249).lt.R0_orak
C FDA40_logic_press_1.fgi( 462, 509):���������� <
      R_(268) = R_avek + (-R_utek)
C FDA40_logic_press_1.fgi( 448, 516):��������
      R_(267)=abs(R_(268))
C FDA40_logic_press_1.fgi( 454, 516):���������� ��������
      L_odek=R_(267).lt.R0_idek
C FDA40_logic_press_1.fgi( 462, 516):���������� <
      R_(285) = R_ubik + (-R8_idik)
C FDA40_logic_press_1.fgi( 369, 470):��������
C label 3566  try3566=try3566-1
      R_(284) = R_(283) * R_(285)
C FDA40_logic_press_1.fgi( 380, 471):����������
      R_(286) = R_(284)
C FDA40_logic_press_1.fgi( 384, 471):��������
      Call C_PID(deltat,L_ifik,R8_idik,REAL(R_odik,4),REAL
     &(R_edik,4),R_ufik,
     & REAL(R_udik,4),REAL(R_adik,4),REAL(R_afik,4),R_ofik
     &,REAL(R_efik,4),R_ekik,R_akik,
     & REAL(R_(286),4))
C FDA40_logic_press_1.fgi( 395, 464):��������� �����������,EJECTOR
C sav1=R_(285)
      R_(285) = R_ubik + (-R8_idik)
C FDA40_logic_press_1.fgi( 369, 470):recalc:��������
C if(sav1.ne.R_(285) .and. try3566.gt.0) goto 3566
      R8_olak=R8_idik
C FDA40_logic_press_1.fgi( 486, 471):������,20FDA40_EJECTOR_CURRENT_POS
      R_(246) = R8_idik + (-R_umak)
C FDA40_logic_press_1.fgi( 448, 458):��������
      R_(245)=abs(R_(246))
C FDA40_logic_press_1.fgi( 454, 458):���������� ��������
      L_emak=R_(245).lt.R0_imak
C FDA40_logic_press_1.fgi( 462, 458):���������� <
      R_(244) = R8_idik + (-R_epak)
C FDA40_logic_press_1.fgi( 448, 465):��������
      R_(243)=abs(R_(244))
C FDA40_logic_press_1.fgi( 454, 465):���������� ��������
      L_ulak=R_(243).lt.R0_amak
C FDA40_logic_press_1.fgi( 462, 465):���������� <
      End
