       SUBROUTINE SPW_Init_tab(iTabVer)
       integer*4 iTabVer
       TYPE TSubr
         sequence
         integer*4 addr
         integer*4 period
         integer*4 phase
         integer*4 object
         integer*4 allowed
         integer*4 ncall
         real*8    time
         integer*4 individualScale
         integer*4 rezerv(2),flags,CPUTime(2)
         character(LEN=32) name
       END TYPE TSubr

       TYPE(TSubr) SPW_Table_Sub(10)
       Common/SPW_Table_Sub/ SPW_Table_Sub
       external FDA_GENERAL
       Data SPW_Table_Sub(1)/TSubr(0,1,0,2,0,0,0.,0,0,8,0,
     &   'FDA_GENERAL')/
       external FDA10
       Data SPW_Table_Sub(2)/TSubr(0,1,0,3,0,0,0.,0,0,8,0,
     &   'FDA10')/
       external FDA20
       Data SPW_Table_Sub(3)/TSubr(0,1,0,4,0,0,0.,0,0,8,0,
     &   'FDA20')/
       external FDA30
       Data SPW_Table_Sub(4)/TSubr(0,1,0,5,0,0,0.,0,0,8,0,
     &   'FDA30')/
       external FDA40
       Data SPW_Table_Sub(5)/TSubr(0,1,0,6,0,0,0.,0,0,8,0,
     &   'FDA40')/
       external FDA50
       Data SPW_Table_Sub(6)/TSubr(0,1,0,7,0,0,0.,0,0,8,0,
     &   'FDA50')/
       external FDA60
       Data SPW_Table_Sub(7)/TSubr(0,1,0,8,0,0,0.,0,0,8,0,
     &   'FDA60')/
       external FDA60B
       Data SPW_Table_Sub(8)/TSubr(0,1,0,9,0,0,0.,0,0,8,0,
     &   'FDA60B')/
       external FDA70
       Data SPW_Table_Sub(9)/TSubr(0,1,0,10,0,0,0.,0,0,8,0,
     &   'FDA70')/
       external FDA90
       Data SPW_Table_Sub(10)/TSubr(0,1,0,11,0,0,0.,0,0,8,0,
     &   'FDA90')/

       LOGICAL*1 SPW_0(0:163839)
       Common/SPW_0/ SPW_0 !
       CHARACTER*1 SPW_1(0:20479)
       Common/SPW_1/ SPW_1 !
       LOGICAL*1 SPW_2(0:20479)
       Common/SPW_2/ SPW_2 !
       CHARACTER*1 SPW_3(0:4095)
       Common/SPW_3/ SPW_3 !
       LOGICAL*1 SPW_4(0:16383)
       Common/SPW_4/ SPW_4 !
       CHARACTER*1 SPW_5(0:1279)
       Common/SPW_5/ SPW_5 !
       LOGICAL*1 SPW_6(0:28671)
       Common/SPW_6/ SPW_6 !
       CHARACTER*1 SPW_7(0:8191)
       Common/SPW_7/ SPW_7 !
       LOGICAL*1 SPW_8(0:14335)
       Common/SPW_8/ SPW_8 !
       CHARACTER*1 SPW_9(0:2559)
       Common/SPW_9/ SPW_9 !
       LOGICAL*1 SPW_10(0:4095)
       Common/SPW_10/ SPW_10 !
       CHARACTER*1 SPW_11(0:1791)
       Common/SPW_11/ SPW_11 !
       LOGICAL*1 SPW_12(0:57343)
       Common/SPW_12/ SPW_12 !
       CHARACTER*1 SPW_13(0:2047)
       Common/SPW_13/ SPW_13 !
       LOGICAL*1 SPW_14(0:32767)
       Common/SPW_14/ SPW_14 !
       LOGICAL*1 SPW_15(0:3583)
       Common/SPW_15/ SPW_15 !
       CHARACTER*1 SPW_16(0:191)
       Common/SPW_16/ SPW_16 !
       LOGICAL*1 SPW_17(0:14335)
       Common/SPW_17/ SPW_17 !
       CHARACTER*1 SPW_18(0:639)
       Common/SPW_18/ SPW_18 !
       LOGICAL*1 SPW_19(0:15)
       Common/SPW_19/ SPW_19 !
       LOGICAL*1 SPW_20(0:511)
       Common/SPW_20/ SPW_20 !
       LOGICAL*1 SPW_21(0:15)
       Common/SPW_21/ SPW_21 !
       CHARACTER*1 SPW_22(0:31)
       Common/SPW_22/ SPW_22 !
       LOGICAL*1 SPW_23(0:15)
       Common/SPW_23/ SPW_23 !
       LOGICAL*1 SPW_24(0:511)
       Common/SPW_24/ SPW_24 !
       LOGICAL*1 SPW_25(0:31)
       Common/SPW_25/ SPW_25 !
       LOGICAL*1 SPW_26(0:15)
       Common/SPW_26/ SPW_26 !
       LOGICAL*1 SPW_27(0:15)
       Common/SPW_27/ SPW_27 !
       LOGICAL*1 SPW_28(0:15)
       Common/SPW_28/ SPW_28 !
       LOGICAL*1 SPW_29(0:15)
       Common/SPW_29/ SPW_29 !
       LOGICAL*1 SPW_30(0:15)
       Common/SPW_30/ SPW_30 !
       LOGICAL*1 SPW_31(0:15)
       Common/SPW_31/ SPW_31 !
       LOGICAL*1 SPW_32(0:767)
       Common/SPW_32/ SPW_32 !
       LOGICAL*1 SPW_33(0:31)
       Common/SPW_33/ SPW_33 !

       real*4 info_kwant
       integer*4 info_siz
       common/spw_info/ info_siz,info_kwant
       !MS$ATTRIBUTES DLLEXPORT::spw_info
       Data info_siz/8/,info_kwant/0.125/
       integer*4 dispStateHdr
       integer*4 stepCounter
       real*8    subInfo(40)
       common/spw_dispState/ dispStateHdr,stepCounter,subInfo

       character*(32) ver(11)
       integer*4 nver,versiz
       common/spw_versions/ nver,versiz,ver
       !MS$ATTRIBUTES DLLEXPORT::spw_versions
       Data nver/11/,versiz/32/
       Data ver(1)/''/
       Data ver(2)/'v_FE4F2F74_FDA_GENERAL'/
       Data ver(3)/'v_18D6333A_FDA10'/
       Data ver(4)/'v_B5C6E309_FDA20'/
       Data ver(5)/'v_27A9E7C8_FDA30'/
       Data ver(6)/'v_761328F6_FDA40'/
       Data ver(7)/'v_B6FB06D5_FDA50'/
       Data ver(8)/'v_07F666F4_FDA60'/
       Data ver(9)/'v_028C3848_FDA60B'/
       Data ver(10)/'v_75D8C236_FDA70'/
       Data ver(11)/'v_6469668A_FDA90'/

       CALL SPW_SET_SIGN(865693333,0,-1580099709)
       iTabVer=iTabVer*1000+1088

       CALL SPW_SET_UIDTR(1852422010,-1840711315)
       CALL SPW_SET_KWANT(REAL(0.125))
       CALL SPW_SET_NCMN(34)
       CALL SPW_ADD_CMN(LOC(SPW_0),157459)
       CALL SPW_ADD_CMN(LOC(SPW_1),16590)
       CALL SPW_ADD_CMN(LOC(SPW_2),16704)
       CALL SPW_ADD_CMN(LOC(SPW_3),3768)
       CALL SPW_ADD_CMN(LOC(SPW_4),16234)
       CALL SPW_ADD_CMN(LOC(SPW_5),1256)
       CALL SPW_ADD_CMN(LOC(SPW_6),26801)
       CALL SPW_ADD_CMN(LOC(SPW_7),7394)
       CALL SPW_ADD_CMN(LOC(SPW_8),12693)
       CALL SPW_ADD_CMN(LOC(SPW_9),2420)
       CALL SPW_ADD_CMN(LOC(SPW_10),4069)
       CALL SPW_ADD_CMN(LOC(SPW_11),1618)
       CALL SPW_ADD_CMN(LOC(SPW_12),53749)
       CALL SPW_ADD_CMN(LOC(SPW_13),1982)
       CALL SPW_ADD_CMN(LOC(SPW_14),30058)
       CALL SPW_ADD_CMN(LOC(SPW_15),3098)
       CALL SPW_ADD_CMN(LOC(SPW_16),158)
       CALL SPW_ADD_CMN(LOC(SPW_17),13623)
       CALL SPW_ADD_CMN(LOC(SPW_18),632)
       CALL SPW_ADD_CMN(LOC(SPW_19),7)
       CALL SPW_ADD_CMN(LOC(SPW_20),435)
       CALL SPW_ADD_CMN(LOC(SPW_21),16)
       CALL SPW_ADD_CMN(LOC(SPW_22),30)
       CALL SPW_ADD_CMN(LOC(SPW_23),4)
       CALL SPW_ADD_CMN(LOC(SPW_24),414)
       CALL SPW_ADD_CMN(LOC(SPW_25),19)
       CALL SPW_ADD_CMN(LOC(SPW_26),11)
       CALL SPW_ADD_CMN(LOC(SPW_27),15)
       CALL SPW_ADD_CMN(LOC(SPW_28),7)
       CALL SPW_ADD_CMN(LOC(SPW_29),4)
       CALL SPW_ADD_CMN(LOC(SPW_30),4)
       CALL SPW_ADD_CMN(LOC(SPW_31),3)
       CALL SPW_ADD_CMN(LOC(SPW_32),708)
       CALL SPW_ADD_CMN(LOC(SPW_33),18)
       CALL SPW_SET_NAMES(192095,
     +    'control_fda_.vpt',
     +    'control_fda_.9248F96D.vpt')
       SPW_Table_Sub(1)%addr=LOC(FDA_GENERAL)
       SPW_Table_Sub(1)%allowed=LOC(SPW_0)+47458
       SPW_Table_Sub(1)%individualScale=LOC(SPW_0)+2552
       SPW_Table_Sub(2)%addr=LOC(FDA10)
       SPW_Table_Sub(2)%allowed=LOC(SPW_2)+10181
       SPW_Table_Sub(2)%individualScale=LOC(SPW_2)+784
       SPW_Table_Sub(3)%addr=LOC(FDA20)
       SPW_Table_Sub(3)%allowed=LOC(SPW_4)+13959
       SPW_Table_Sub(3)%individualScale=LOC(SPW_4)+616
       SPW_Table_Sub(4)%addr=LOC(FDA30)
       SPW_Table_Sub(4)%allowed=LOC(SPW_6)+23564
       SPW_Table_Sub(4)%individualScale=LOC(SPW_6)+1224
       SPW_Table_Sub(5)%addr=LOC(FDA40)
       SPW_Table_Sub(5)%allowed=LOC(SPW_8)+12684
       SPW_Table_Sub(5)%individualScale=LOC(SPW_8)+640
       SPW_Table_Sub(6)%addr=LOC(FDA50)
       SPW_Table_Sub(6)%allowed=LOC(SPW_10)+4067
       SPW_Table_Sub(6)%individualScale=LOC(SPW_10)+184
       SPW_Table_Sub(7)%addr=LOC(FDA60)
       SPW_Table_Sub(7)%allowed=LOC(SPW_12)+52754
       SPW_Table_Sub(7)%individualScale=LOC(SPW_12)+5432
       SPW_Table_Sub(8)%addr=LOC(FDA60B)
       SPW_Table_Sub(8)%allowed=LOC(SPW_14)+30057
       SPW_Table_Sub(8)%individualScale=LOC(SPW_14)+2256
       SPW_Table_Sub(9)%addr=LOC(FDA70)
       SPW_Table_Sub(9)%allowed=LOC(SPW_15)+3097
       SPW_Table_Sub(9)%individualScale=LOC(SPW_15)+336
       SPW_Table_Sub(10)%addr=LOC(FDA90)
       SPW_Table_Sub(10)%allowed=LOC(SPW_17)+13620
       SPW_Table_Sub(10)%individualScale=LOC(SPW_17)+488
       CALL SPW_SET_SUB_LST(10,SPW_Table_Sub,stepCounter)
       end

       subroutine spw_initDisp
       integer*4 dispStateHdr
       integer*4 stepCounter
       real*8    subInfo(40)
       common/spw_dispState/ dispStateHdr,stepCounter,subInfo
       stepCounter=0
       subInfo=0
       end

       subroutine FDA_GENERAL_ConInQ
       end

       subroutine FDA_GENERAL_ConIn
       LOGICAL*1 SPW_0(0:163839)
       Common/SPW_0/ SPW_0 !
       integer*4 SPW_0_4(0:40959)
       equivalence(SPW_0,SPW_0_4)
       LOGICAL*1 SPW_2(0:20479)
       Common/SPW_2/ SPW_2 !
       integer*4 SPW_2_4(0:5119)
       equivalence(SPW_2,SPW_2_4)
       LOGICAL*1 SPW_4(0:16383)
       Common/SPW_4/ SPW_4 !
       integer*4 SPW_4_4(0:4095)
       equivalence(SPW_4,SPW_4_4)
       LOGICAL*1 SPW_6(0:28671)
       Common/SPW_6/ SPW_6 !
       integer*4 SPW_6_4(0:7167)
       equivalence(SPW_6,SPW_6_4)
        Call FDA_GENERAL_ConInQ
!beg �����:20FDA20TRAN04_C6
        SPW_0(38100)=.false.	!L_(481) O
!end �����:20FDA20TRAN04_C6
!beg �����:20FDA20TRAN04_C7
        SPW_0(38097)=.false.	!L_(478) O
!end �����:20FDA20TRAN04_C7
!beg �������:20FDA91AB001
        SPW_0(37697)=SPW_0(154414)	!L_(78) O L_(323)
        SPW_0(37697)=SPW_0(37697).or.SPW_0(154649)	!L_(78) O L_(558)
        SPW_0(37697)=SPW_0(37697).or.SPW_0(154893)	!L_(78) O L_(802)
        SPW_0(37697)=SPW_0(37697).or.SPW_0(155023)	!L_(78) O L_(932)
        SPW_0(37697)=SPW_0(37697).or.SPW_0(155166)	!L_(78) O L_(1075)
        SPW_0(37697)=SPW_0(37697).or.SPW_0(155308)	!L_(78) O L_(1217)
!end �������:20FDA91AB001
!beg �������:20FDA91AB002
        SPW_0(37693)=SPW_0(155052)	!L_(74) O L_(961)
        SPW_0(37693)=SPW_0(37693).or.SPW_0(155260)	!L_(74) O L_(1169)
!end �������:20FDA91AB002
!beg �������:20FDA91AB003
        SPW_0(37689)=SPW_0(154691)	!L_(70) O L_(600)
        SPW_0(37689)=SPW_0(37689).or.SPW_0(154825)	!L_(70) O L_(734)
!end �������:20FDA91AB003
!beg �����:U
        SPW_0(37771)=.false.	!L_(152) O
!end �����:U
!beg �������:20FDA91AB004
        SPW_0(37691)=SPW_0(154702)	!L_(72) O L_(611)
        SPW_0(37691)=SPW_0(37691).or.SPW_0(154796)	!L_(72) O L_(705)
!end �������:20FDA91AB004
!beg �������:20FDA33AE002
        SPW_0(37704)=.false.	!L_(85) O
!end �������:20FDA33AE002
!beg �������:20FDA91AB005
        SPW_0(37695)=SPW_0(154446)	!L_(76) O L_(355)
        SPW_0(37695)=SPW_0(37695).or.SPW_0(155123)	!L_(76) O L_(1032)
!end �������:20FDA91AB005
!beg ������:U
        SPW_0(37770)=.false.	!L_(151) O
!end ������:U
!beg �����:20FDA20TRAN09_C3
        SPW_0(38019)=.false.	!L_(400) O
!end �����:20FDA20TRAN09_C3
!beg �����:20FDA20TRAN09_C4
        SPW_0(38016)=.false.	!L_(397) O
!end �����:20FDA20TRAN09_C4
!beg �����:20FDA20TRAN09_C5
        SPW_0(38013)=.false.	!L_(394) O
!end �����:20FDA20TRAN09_C5
!beg �����:20FDA20TRAN09_C6
        SPW_0(38010)=.false.	!L_(391) O
!end �����:20FDA20TRAN09_C6
!beg ����:20FDA66AE500
        SPW_0(37645)=.false.	!L_(26) O
!end ����:20FDA66AE500
!beg �������:20FDA21AB001VS
        SPW_0(37949)=.false.	!L_(330) O
!end �������:20FDA21AB001VS
!beg ����:20FDA20TRAN05_C3
        SPW_0(38077)=.false.	!L_(458) O
!end ����:20FDA20TRAN05_C3
!beg ����:20FDA20TRAN05_C4
        SPW_0(38074)=.false.	!L_(455) O
!end ����:20FDA20TRAN05_C4
!beg ����:20FDA20TRAN05_C5
        SPW_0(38071)=.false.	!L_(452) O
!end ����:20FDA20TRAN05_C5
!beg �������:20FDA21AB001VZ
        SPW_0(37951)=.false.	!L_(332) O
!end �������:20FDA21AB001VZ
!beg ����:20FDA20TRAN05_C6
        SPW_0(38068)=.false.	!L_(449) O
!end ����:20FDA20TRAN05_C6
!beg ����:20FDA20TRAN05_C7
        SPW_0(38053)=.false.	!L_(434) O
!end ����:20FDA20TRAN05_C7
!beg �������:20FDA20KANT01_C10
        SPW_0(37983)=.false.	!L_(364) O
!end �������:20FDA20KANT01_C10
!beg ����:20FDA20TRAN05_C8
        SPW_0(38050)=.false.	!L_(431) O
!end ����:20FDA20TRAN05_C8
!beg ������:20FDA64AE500
        SPW_0(37652)=SPW_0(149314)	!L_(33) O L_(1807)
        SPW_0(37652)=SPW_0(37652).or.SPW_0(149330)	!L_(33) O L_(1823)
        SPW_0(37652)=SPW_0(37652).or.SPW_0(149345)	!L_(33) O L_(1838)
        SPW_0(37652)=SPW_0(37652).or.SPW_0(149359)	!L_(33) O L_(1852)
        SPW_0(37652)=SPW_0(37652).or.SPW_0(149373)	!L_(33) O L_(1866)
!end ������:20FDA64AE500
!beg ����:20FDA63AE500
        SPW_0(37654)=.false.	!L_(35) O
!end ����:20FDA63AE500
!beg �������:20FDA66AB800
        SPW_0(37738)=SPW_0(147515)	!L_(119) O L_(8)
        SPW_0(37738)=SPW_0(37738).or.SPW_0(149129)	!L_(119) O L_(1622)
!end �������:20FDA66AB800
!beg �������:20FDA66AB801
        SPW_0(37736)=SPW_0(147515)	!L_(117) O L_(8)
        SPW_0(37736)=SPW_0(37736).or.SPW_0(148735)	!L_(117) O L_(1228)
!end �������:20FDA66AB801
!beg �������:20FDA33AE002
        SPW_0(37705)=.false.	!L_(86) O
!end �������:20FDA33AE002
!beg �����:20FDA64AE500
        SPW_0(37653)=SPW_0(149310)	!L_(34) O L_(1803)
        SPW_0(37653)=SPW_0(37653).or.SPW_0(149326)	!L_(34) O L_(1819)
        SPW_0(37653)=SPW_0(37653).or.SPW_0(149341)	!L_(34) O L_(1834)
        SPW_0(37653)=SPW_0(37653).or.SPW_0(149355)	!L_(34) O L_(1848)
        SPW_0(37653)=SPW_0(37653).or.SPW_0(149368)	!L_(34) O L_(1861)
!end �����:20FDA64AE500
!beg �������:20FDA66AB806
        SPW_0(37724)=SPW_0(148213)	!L_(105) O L_(706)
!end �������:20FDA66AB806
!beg ������:20FDA66AE401
        SPW_0(37664)=SPW_0(148208)	!L_(45) O L_(701)
        SPW_0(37664)=SPW_0(37664).or.SPW_0(148223)	!L_(45) O L_(716)
        SPW_0(37664)=SPW_0(37664).or.SPW_0(148232)	!L_(45) O L_(725)
        SPW_0(37664)=SPW_0(37664).or.SPW_0(148241)	!L_(45) O L_(734)
        SPW_0(37664)=SPW_0(37664).or.SPW_0(148256)	!L_(45) O L_(749)
!end ������:20FDA66AE401
!beg ������:20FDA66AE406
        SPW_0(37707)=SPW_0(148697)	!L_(88) O L_(1190)
        SPW_0(37707)=SPW_0(37707).or.SPW_0(148708)	!L_(88) O L_(1201)
        SPW_0(37707)=SPW_0(37707).or.SPW_0(148718)	!L_(88) O L_(1211)
        SPW_0(37707)=SPW_0(37707).or.SPW_0(148727)	!L_(88) O L_(1220)
        SPW_0(37707)=SPW_0(37707).or.SPW_0(148738)	!L_(88) O L_(1231)
!end ������:20FDA66AE406
!beg ����:20FDA65AE401
        SPW_0(37666)=.false.	!L_(47) O
!end ����:20FDA65AE401
!beg �����:20FDA66AE401
        SPW_0(37665)=SPW_0(148219)	!L_(46) O L_(712)
        SPW_0(37665)=SPW_0(37665).or.SPW_0(148227)	!L_(46) O L_(720)
        SPW_0(37665)=SPW_0(37665).or.SPW_0(148236)	!L_(46) O L_(729)
        SPW_0(37665)=SPW_0(37665).or.SPW_0(148245)	!L_(46) O L_(738)
        SPW_0(37665)=SPW_0(37665).or.SPW_0(148251)	!L_(46) O L_(744)
!end �����:20FDA66AE401
!beg ����:20FDA65AE406
        SPW_0(37709)=.false.	!L_(90) O
!end ����:20FDA65AE406
!beg �����:20FDA66AE406
        SPW_0(37708)=SPW_0(148693)	!L_(89) O L_(1186)
        SPW_0(37708)=SPW_0(37708).or.SPW_0(148704)	!L_(89) O L_(1197)
        SPW_0(37708)=SPW_0(37708).or.SPW_0(148714)	!L_(89) O L_(1207)
        SPW_0(37708)=SPW_0(37708).or.SPW_0(148723)	!L_(89) O L_(1216)
        SPW_0(37708)=SPW_0(37708).or.SPW_0(148731)	!L_(89) O L_(1224)
!end �����:20FDA66AE406
!beg ������:20FDA61AE500
        SPW_0(37661)=SPW_0(149575)	!L_(42) O L_(2068)
        SPW_0(37661)=SPW_0(37661).or.SPW_0(149591)	!L_(42) O L_(2084)
        SPW_0(37661)=SPW_0(37661).or.SPW_0(149606)	!L_(42) O L_(2099)
        SPW_0(37661)=SPW_0(37661).or.SPW_0(149620)	!L_(42) O L_(2113)
        SPW_0(37661)=SPW_0(37661).or.SPW_0(149634)	!L_(42) O L_(2127)
!end ������:20FDA61AE500
!beg ����:20FDA60AE500
        SPW_0(37701)=.false.	!L_(82) O
!end ����:20FDA60AE500
!beg �������:20FDA63AB800
        SPW_0(37750)=SPW_0(147593)	!L_(131) O L_(86)
        SPW_0(37750)=SPW_0(37750).or.SPW_0(149390)	!L_(131) O L_(1883)
!end �������:20FDA63AB800
!beg ����:20FDA60AE501
        SPW_0(37698)=.false.	!L_(79) O
!end ����:20FDA60AE501
!beg ����:20FDA60AE502
        SPW_0(37766)=.false.	!L_(147) O
!end ����:20FDA60AE502
!beg �������:20FDA63AB801
        SPW_0(37748)=SPW_0(147593)	!L_(129) O L_(86)
        SPW_0(37748)=SPW_0(37748).or.SPW_0(148906)	!L_(129) O L_(1399)
!end �������:20FDA63AB801
!beg �����:20FDA61AE500
        SPW_0(37662)=SPW_0(149455)	!L_(43) O L_(1948)
        SPW_0(37662)=SPW_0(37662).or.SPW_0(149571)	!L_(43) O L_(2064)
        SPW_0(37662)=SPW_0(37662).or.SPW_0(149587)	!L_(43) O L_(2080)
        SPW_0(37662)=SPW_0(37662).or.SPW_0(149602)	!L_(43) O L_(2095)
        SPW_0(37662)=SPW_0(37662).or.SPW_0(149616)	!L_(43) O L_(2109)
        SPW_0(37662)=SPW_0(37662).or.SPW_0(149629)	!L_(43) O L_(2122)
!end �����:20FDA61AE500
!beg ����:20FDA60AE503
        SPW_0(37630)=.false.	!L_(11) O
!end ����:20FDA60AE503
!beg ����:20FDA60AE504
        SPW_0(37633)=.false.	!L_(14) O
!end ����:20FDA60AE504
!beg �����:20FDA20KANT01_C3
        SPW_0(37995)=.false.	!L_(376) O
!end �����:20FDA20KANT01_C3
!beg �����:20FDA20KANT01_C4
        SPW_0(37992)=.false.	!L_(373) O
!end �����:20FDA20KANT01_C4
!beg �������:20FDA63AB806
        SPW_0(37730)=SPW_0(148444)	!L_(111) O L_(937)
!end �������:20FDA63AB806
!beg �������:20FDA21AB002
        SPW_0(38175)=.false.	!L_(556) O
!end �������:20FDA21AB002
!beg �����:20FDA20KANT01_C6
        SPW_0(37989)=.false.	!L_(370) O
!end �����:20FDA20KANT01_C6
!beg ����:20FDA60AE509
        SPW_0(37636)=.false.	!L_(17) O
!end ����:20FDA60AE509
!beg �����:20FDA20KANT01_C7
        SPW_0(37986)=.false.	!L_(367) O
!end �����:20FDA20KANT01_C7
!beg ������:20FDA20TRAN04_C3
        SPW_0(38108)=.false.	!L_(489) O
!end ������:20FDA20TRAN04_C3
!beg ������:20FDA20TRAN04_C4
        SPW_0(38105)=.false.	!L_(486) O
!end ������:20FDA20TRAN04_C4
!beg ������:20FDA20TRAN04_C5
        SPW_0(38102)=.false.	!L_(483) O
!end ������:20FDA20TRAN04_C5
!beg ������:20FDA20TRAN04_C6
        SPW_0(38099)=.false.	!L_(480) O
!end ������:20FDA20TRAN04_C6
!beg ������:20FDA20TRAN04_C7
        SPW_0(38096)=.false.	!L_(477) O
!end ������:20FDA20TRAN04_C7
!beg ������:20FDA63AE401
        SPW_0(37673)=SPW_0(148439)	!L_(54) O L_(932)
        SPW_0(37673)=SPW_0(37673).or.SPW_0(148454)	!L_(54) O L_(947)
        SPW_0(37673)=SPW_0(37673).or.SPW_0(148463)	!L_(54) O L_(956)
        SPW_0(37673)=SPW_0(37673).or.SPW_0(148472)	!L_(54) O L_(965)
        SPW_0(37673)=SPW_0(37673).or.SPW_0(148487)	!L_(54) O L_(980)
!end ������:20FDA63AE401
!beg ����:20FDA60AE514
        SPW_0(37642)=.false.	!L_(23) O
!end ����:20FDA60AE514
!beg ������:20FDA63AE406
        SPW_0(37716)=SPW_0(148868)	!L_(97) O L_(1361)
        SPW_0(37716)=SPW_0(37716).or.SPW_0(148879)	!L_(97) O L_(1372)
        SPW_0(37716)=SPW_0(37716).or.SPW_0(148889)	!L_(97) O L_(1382)
        SPW_0(37716)=SPW_0(37716).or.SPW_0(148898)	!L_(97) O L_(1391)
        SPW_0(37716)=SPW_0(37716).or.SPW_0(148909)	!L_(97) O L_(1402)
!end ������:20FDA63AE406
!beg ����:20FDA60AE515
        SPW_0(37639)=.false.	!L_(20) O
!end ����:20FDA60AE515
!beg ����:20FDA62AE401
        SPW_0(37675)=.false.	!L_(56) O
!end ����:20FDA62AE401
!beg �����:20FDA63AE401
        SPW_0(37674)=SPW_0(148450)	!L_(55) O L_(943)
        SPW_0(37674)=SPW_0(37674).or.SPW_0(148458)	!L_(55) O L_(951)
        SPW_0(37674)=SPW_0(37674).or.SPW_0(148467)	!L_(55) O L_(960)
        SPW_0(37674)=SPW_0(37674).or.SPW_0(148476)	!L_(55) O L_(969)
        SPW_0(37674)=SPW_0(37674).or.SPW_0(148482)	!L_(55) O L_(975)
!end �����:20FDA63AE401
!beg ����:20FDA62AE406
        SPW_0(37718)=.false.	!L_(99) O
!end ����:20FDA62AE406
!beg �����:20FDA63AE406
        SPW_0(37717)=SPW_0(148864)	!L_(98) O L_(1357)
        SPW_0(37717)=SPW_0(37717).or.SPW_0(148875)	!L_(98) O L_(1368)
        SPW_0(37717)=SPW_0(37717).or.SPW_0(148885)	!L_(98) O L_(1378)
        SPW_0(37717)=SPW_0(37717).or.SPW_0(148894)	!L_(98) O L_(1387)
        SPW_0(37717)=SPW_0(37717).or.SPW_0(148902)	!L_(98) O L_(1395)
!end �����:20FDA63AE406
!beg �����:20FDA20TRAN03_C3
        SPW_0(38094)=.false.	!L_(475) O
!end �����:20FDA20TRAN03_C3
!beg �����:20FDA20TRAN03_C4
        SPW_0(38091)=.false.	!L_(472) O
!end �����:20FDA20TRAN03_C4
!beg �����:20FDA20TRAN03_C5
        SPW_0(38088)=.false.	!L_(469) O
!end �����:20FDA20TRAN03_C5
!beg �����:20FDA20TRAN03_C6
        SPW_0(38085)=.false.	!L_(466) O
!end �����:20FDA20TRAN03_C6
!beg �����:20FDA20TRAN03_C7
        SPW_0(38082)=.false.	!L_(463) O
!end �����:20FDA20TRAN03_C7
!beg �������:20FDA60AB800
        SPW_0(37764)=SPW_0(149042)	!L_(145) O L_(1535)
!end �������:20FDA60AB800
!beg �������:20FDA60AB801
        SPW_0(37762)=SPW_0(149023)	!L_(143) O L_(1516)
!end �������:20FDA60AB801
!beg �������:20FDA20KANT01_C10
        SPW_0(37982)=.false.	!L_(363) O
!end �������:20FDA20KANT01_C10
!beg �������:20FDA60AB806
        SPW_0(37760)=.false.	!L_(141) O
!end �������:20FDA60AB806
!beg �������:20FDA60AB807
        SPW_0(37758)=.false.	!L_(139) O
!end �������:20FDA60AB807
!beg ������:20FDA60AE400
        SPW_0(37774)=.false.	!L_(155) O
!end ������:20FDA60AE400
!beg ������:20FDA60AE401
        SPW_0(37772)=.false.	!L_(153) O
!end ������:20FDA60AE401
!beg ������:20FDA60AE402
        SPW_0(37684)=SPW_0(149052)	!L_(65) O L_(1545)
!end ������:20FDA60AE402
!beg ������:20FDA60AE403
        SPW_0(37626)=SPW_0(149072)	!L_(7) O L_(1565)
        SPW_0(37626)=SPW_0(37626).or.SPW_0(149087)	!L_(7) O L_(1580)
!end ������:20FDA60AE403
!beg �������:20FDA66AB800
        SPW_0(37739)=SPW_0(149125)	!L_(120) O L_(1618)
!end �������:20FDA66AB800
!beg �����:20FDA60AE400
        SPW_0(37775)=.false.	!L_(156) O
!end �����:20FDA60AE400
!beg �������:20FDA66AB801
        SPW_0(37737)=SPW_0(148167)	!L_(118) O L_(660)
        SPW_0(37737)=SPW_0(37737).or.SPW_0(149125)	!L_(118) O L_(1618)
!end �������:20FDA66AB801
!beg �����:20FDA60AE401
        SPW_0(37773)=.false.	!L_(154) O
!end �����:20FDA60AE401
!beg �����:20FDA60AE402
        SPW_0(37685)=SPW_0(149049)	!L_(66) O L_(1542)
!end �����:20FDA60AE402
!beg �����:20FDA60AE403
        SPW_0(37627)=SPW_0(149067)	!L_(8) O L_(1560)
        SPW_0(37627)=SPW_0(37627).or.SPW_0(149112)	!L_(8) O L_(1605)
!end �����:20FDA60AE403
!beg �������:20FDA66AB806
        SPW_0(37725)=SPW_0(148185)	!L_(106) O L_(678)
!end �������:20FDA66AB806
!beg ������:20FDA20TRAN09_C3
        SPW_0(38018)=.false.	!L_(399) O
!end ������:20FDA20TRAN09_C3
!beg ������:20FDA20TRAN09_C4
        SPW_0(38015)=.false.	!L_(396) O
!end ������:20FDA20TRAN09_C4
!beg ������:20FDA20TRAN09_C5
        SPW_0(38012)=.false.	!L_(393) O
!end ������:20FDA20TRAN09_C5
!beg ������:20FDA20TRAN09_C6
        SPW_0(38009)=.false.	!L_(390) O
!end ������:20FDA20TRAN09_C6
!beg �����:20FDA20TRAN08_C3
        SPW_0(38031)=.false.	!L_(412) O
!end �����:20FDA20TRAN08_C3
!beg �����:20FDA20TRAN08_C4
        SPW_0(38028)=.false.	!L_(409) O
!end �����:20FDA20TRAN08_C4
!beg �����:20FDA20TRAN08_C5
        SPW_0(38025)=.false.	!L_(406) O
!end �����:20FDA20TRAN08_C5
!beg �����:20FDA20TRAN08_C6
        SPW_0(38022)=.false.	!L_(403) O
!end �����:20FDA20TRAN08_C6
!beg �������:20FDA63AB800
        SPW_0(37751)=SPW_0(149386)	!L_(132) O L_(1879)
!end �������:20FDA63AB800
!beg �������:20FDA63AB801
        SPW_0(37749)=SPW_0(148176)	!L_(130) O L_(669)
        SPW_0(37749)=SPW_0(37749).or.SPW_0(149386)	!L_(130) O L_(1879)
!end �������:20FDA63AB801
!beg �������:20FDA63AB806
        SPW_0(37731)=SPW_0(148416)	!L_(112) O L_(909)
!end �������:20FDA63AB806
!beg �������:20FDA21AB002
        SPW_0(38176)=.false.	!L_(557) O
!end �������:20FDA21AB002
!beg ����:20FDA20TRAN04_C3
        SPW_0(38107)=.false.	!L_(488) O
!end ����:20FDA20TRAN04_C3
!beg ����:20FDA20TRAN04_C4
        SPW_0(38104)=.false.	!L_(485) O
!end ����:20FDA20TRAN04_C4
!beg ����:20FDA20TRAN04_C5
        SPW_0(38101)=.false.	!L_(482) O
!end ����:20FDA20TRAN04_C5
!beg ����:20FDA20TRAN04_C6
        SPW_0(38098)=.false.	!L_(479) O
!end ����:20FDA20TRAN04_C6
!beg ����:20FDA20TRAN04_C7
        SPW_0(38095)=.false.	!L_(476) O
!end ����:20FDA20TRAN04_C7
!beg ���������:20FDA60AE402
        SPW_0(37681)=.false.	!L_(62) O
!end ���������:20FDA60AE402
!beg �������:20FDA60AB800
        SPW_0(37765)=SPW_0(149038)	!L_(146) O L_(1531)
!end �������:20FDA60AB800
!beg �������:20FDA60AB801
        SPW_0(37763)=SPW_0(149008)	!L_(144) O L_(1501)
!end �������:20FDA60AB801
!beg �������:20FDA60AB806
        SPW_0(37761)=.false.	!L_(142) O
!end �������:20FDA60AB806
!beg ����:20FDA60AE201
        SPW_0(37620)=SPW_0(149049)	!L_(1) O L_(1542)
!end ����:20FDA60AE201
!beg �������:20FDA60AB807
        SPW_0(37759)=.false.	!L_(140) O
!end �������:20FDA60AB807
!beg ����:20FDA60TELEZ
        SPW_0(38156)=.false.	!L_(537) O
!end ����:20FDA60TELEZ
!beg ������:20FDA20KANT01_C3
        SPW_0(37994)=.false.	!L_(375) O
!end ������:20FDA20KANT01_C3
!beg ������:20FDA20KANT01_C4
        SPW_0(37991)=.false.	!L_(372) O
!end ������:20FDA20KANT01_C4
!beg ������:20FDA20KANT01_C6
        SPW_0(37988)=.false.	!L_(369) O
!end ������:20FDA20KANT01_C6
!beg ������:20FDA20KANT01_C7
        SPW_0(37985)=.false.	!L_(366) O
!end ������:20FDA20KANT01_C7
!beg ����:20FDA20TRAN09_C3
        SPW_0(38017)=.false.	!L_(398) O
!end ����:20FDA20TRAN09_C3
!beg ����:20FDA20TRAN09_C4
        SPW_0(38014)=.false.	!L_(395) O
!end ����:20FDA20TRAN09_C4
!beg ����:20FDA20TRAN09_C5
        SPW_0(38011)=.false.	!L_(392) O
!end ����:20FDA20TRAN09_C5
!beg ����:20FDA20TRAN09_C6
        SPW_0(38008)=.false.	!L_(389) O
!end ����:20FDA20TRAN09_C6
!beg ������:20FDA20TRAN03_C3
        SPW_0(38093)=.false.	!L_(474) O
!end ������:20FDA20TRAN03_C3
!beg ������:20FDA20TRAN03_C4
        SPW_0(38090)=.false.	!L_(471) O
!end ������:20FDA20TRAN03_C4
!beg ������:20FDA20TRAN03_C5
        SPW_0(38087)=.false.	!L_(468) O
!end ������:20FDA20TRAN03_C5
!beg ������:20FDA20TRAN03_C6
        SPW_0(38084)=.false.	!L_(465) O
!end ������:20FDA20TRAN03_C6
!beg ������:20FDA20TRAN03_C7
        SPW_0(38081)=.false.	!L_(462) O
!end ������:20FDA20TRAN03_C7
!beg ������:20FDA60AE402
        SPW_0(37682)=SPW_0(155250)	!L_(63) O L_(1159)
!end ������:20FDA60AE402
!beg �������:20FDA21AB001VS
        SPW_0(37950)=.false.	!L_(331) O
!end �������:20FDA21AB001VS
!beg �����:20FDA20TRAN02_C3
        SPW_0(38124)=.false.	!L_(505) O
!end �����:20FDA20TRAN02_C3
!beg �����:20FDA20TRAN02_C4
        SPW_0(38121)=.false.	!L_(502) O
!end �����:20FDA20TRAN02_C4
!beg �����:20FDA20TRAN02_C5
        SPW_0(38118)=.false.	!L_(499) O
!end �����:20FDA20TRAN02_C5
!beg �����:20FDA20TRAN02_C6
        SPW_0(38115)=.false.	!L_(496) O
!end �����:20FDA20TRAN02_C6
!beg �����:20FDA20TRAN02_C7
        SPW_0(38112)=.false.	!L_(493) O
!end �����:20FDA20TRAN02_C7
!beg �������:20FDA21AB001VZ
        SPW_0(37952)=.false.	!L_(333) O
!end �������:20FDA21AB001VZ
!beg ������:20FDA66AE500
        SPW_0(37646)=SPW_0(149140)	!L_(27) O L_(1633)
        SPW_0(37646)=SPW_0(37646).or.SPW_0(149156)	!L_(27) O L_(1649)
        SPW_0(37646)=SPW_0(37646).or.SPW_0(149171)	!L_(27) O L_(1664)
        SPW_0(37646)=SPW_0(37646).or.SPW_0(149185)	!L_(27) O L_(1678)
        SPW_0(37646)=SPW_0(37646).or.SPW_0(149199)	!L_(27) O L_(1692)
!end ������:20FDA66AE500
!beg ������:20FDA20TRAN08_C3
        SPW_0(38030)=.false.	!L_(411) O
!end ������:20FDA20TRAN08_C3
!beg ������:20FDA20TRAN08_C4
        SPW_0(38027)=.false.	!L_(408) O
!end ������:20FDA20TRAN08_C4
!beg ������:20FDA20TRAN08_C5
        SPW_0(38024)=.false.	!L_(405) O
!end ������:20FDA20TRAN08_C5
!beg ������:20FDA20TRAN08_C6
        SPW_0(38021)=.false.	!L_(402) O
!end ������:20FDA20TRAN08_C6
!beg ����:20FDA65AE500
        SPW_0(37648)=.false.	!L_(29) O
!end ����:20FDA65AE500
!beg �����:20FDA66AE500
        SPW_0(37647)=SPW_0(149136)	!L_(28) O L_(1629)
        SPW_0(37647)=SPW_0(37647).or.SPW_0(149152)	!L_(28) O L_(1645)
        SPW_0(37647)=SPW_0(37647).or.SPW_0(149167)	!L_(28) O L_(1660)
        SPW_0(37647)=SPW_0(37647).or.SPW_0(149181)	!L_(28) O L_(1674)
        SPW_0(37647)=SPW_0(37647).or.SPW_0(149194)	!L_(28) O L_(1687)
!end �����:20FDA66AE500
!beg �������:20FDA60CONTS01VS1
        SPW_0(37979)=.false.	!L_(360) O
!end �������:20FDA60CONTS01VS1
!beg �������:20FDA60CONTS01VS2
        SPW_0(37975)=.false.	!L_(356) O
!end �������:20FDA60CONTS01VS2
!beg �����:20FDA20TRAN07_C3
        SPW_0(38043)=.false.	!L_(424) O
!end �����:20FDA20TRAN07_C3
!beg �����:20FDA20TRAN07_C4
        SPW_0(38040)=.false.	!L_(421) O
!end �����:20FDA20TRAN07_C4
!beg �����:20FDA20TRAN07_C5
        SPW_0(38037)=.false.	!L_(418) O
!end �����:20FDA20TRAN07_C5
!beg �����:20FDA20TRAN07_C6
        SPW_0(38034)=.false.	!L_(415) O
!end �����:20FDA20TRAN07_C6
!beg ����:20FDA20KANT01_C3
        SPW_0(37993)=.false.	!L_(374) O
!end ����:20FDA20KANT01_C3
!beg ����:20FDA20KANT01_C4
        SPW_0(37990)=.false.	!L_(371) O
!end ����:20FDA20KANT01_C4
!beg ����:20FDA20KANT01_C6
        SPW_0(37987)=.false.	!L_(368) O
!end ����:20FDA20KANT01_C6
!beg ����:20FDA20KANT01_C7
        SPW_0(37984)=.false.	!L_(365) O
!end ����:20FDA20KANT01_C7
!beg ������:20FDA63AE500
        SPW_0(37655)=SPW_0(149401)	!L_(36) O L_(1894)
        SPW_0(37655)=SPW_0(37655).or.SPW_0(149417)	!L_(36) O L_(1910)
        SPW_0(37655)=SPW_0(37655).or.SPW_0(149432)	!L_(36) O L_(1925)
        SPW_0(37655)=SPW_0(37655).or.SPW_0(149446)	!L_(36) O L_(1939)
        SPW_0(37655)=SPW_0(37655).or.SPW_0(149460)	!L_(36) O L_(1953)
!end ������:20FDA63AE500
!beg ����:20FDA62AE500
        SPW_0(37657)=.false.	!L_(38) O
!end ����:20FDA62AE500
!beg �������:20FDA65AB800
        SPW_0(37742)=SPW_0(147541)	!L_(123) O L_(34)
        SPW_0(37742)=SPW_0(37742).or.SPW_0(149216)	!L_(123) O L_(1709)
!end �������:20FDA65AB800
!beg �������:20FDA65AB801
        SPW_0(37740)=SPW_0(147541)	!L_(121) O L_(34)
        SPW_0(37740)=SPW_0(37740).or.SPW_0(148792)	!L_(121) O L_(1285)
!end �������:20FDA65AB801
!beg �����:20FDA63AE500
        SPW_0(37656)=SPW_0(149397)	!L_(37) O L_(1890)
        SPW_0(37656)=SPW_0(37656).or.SPW_0(149413)	!L_(37) O L_(1906)
        SPW_0(37656)=SPW_0(37656).or.SPW_0(149428)	!L_(37) O L_(1921)
        SPW_0(37656)=SPW_0(37656).or.SPW_0(149442)	!L_(37) O L_(1935)
!end �����:20FDA63AE500
!beg �������:20FDA65AB806
        SPW_0(37726)=SPW_0(148290)	!L_(107) O L_(783)
!end �������:20FDA65AB806
!beg ����:20FDA20TRAN03_C3
        SPW_0(38092)=.false.	!L_(473) O
!end ����:20FDA20TRAN03_C3
!beg ����:20FDA20TRAN03_C4
        SPW_0(38089)=.false.	!L_(470) O
!end ����:20FDA20TRAN03_C4
!beg ����:20FDA20TRAN03_C5
        SPW_0(38086)=.false.	!L_(467) O
!end ����:20FDA20TRAN03_C5
!beg ����:20FDA20TRAN03_C6
        SPW_0(38083)=.false.	!L_(464) O
!end ����:20FDA20TRAN03_C6
!beg ����:20FDA20TRAN03_C7
        SPW_0(38080)=.false.	!L_(461) O
!end ����:20FDA20TRAN03_C7
!beg ������:20FDA65AE401
        SPW_0(37667)=SPW_0(148285)	!L_(48) O L_(778)
        SPW_0(37667)=SPW_0(37667).or.SPW_0(148300)	!L_(48) O L_(793)
        SPW_0(37667)=SPW_0(37667).or.SPW_0(148309)	!L_(48) O L_(802)
        SPW_0(37667)=SPW_0(37667).or.SPW_0(148318)	!L_(48) O L_(811)
        SPW_0(37667)=SPW_0(37667).or.SPW_0(148333)	!L_(48) O L_(826)
!end ������:20FDA65AE401
!beg ������:20FDA65AE406
        SPW_0(37710)=SPW_0(148754)	!L_(91) O L_(1247)
        SPW_0(37710)=SPW_0(37710).or.SPW_0(148765)	!L_(91) O L_(1258)
        SPW_0(37710)=SPW_0(37710).or.SPW_0(148775)	!L_(91) O L_(1268)
        SPW_0(37710)=SPW_0(37710).or.SPW_0(148784)	!L_(91) O L_(1277)
        SPW_0(37710)=SPW_0(37710).or.SPW_0(148795)	!L_(91) O L_(1288)
!end ������:20FDA65AE406
!beg ����:20FDA64AE401
        SPW_0(37669)=.false.	!L_(50) O
!end ����:20FDA64AE401
!beg �����:20FDA65AE401
        SPW_0(37668)=SPW_0(148296)	!L_(49) O L_(789)
        SPW_0(37668)=SPW_0(37668).or.SPW_0(148304)	!L_(49) O L_(797)
        SPW_0(37668)=SPW_0(37668).or.SPW_0(148313)	!L_(49) O L_(806)
        SPW_0(37668)=SPW_0(37668).or.SPW_0(148322)	!L_(49) O L_(815)
        SPW_0(37668)=SPW_0(37668).or.SPW_0(148328)	!L_(49) O L_(821)
!end �����:20FDA65AE401
!beg ����:20FDA64AE406
        SPW_0(37712)=.false.	!L_(93) O
!end ����:20FDA64AE406
!beg �����:20FDA65AE406
        SPW_0(37711)=SPW_0(148750)	!L_(92) O L_(1243)
        SPW_0(37711)=SPW_0(37711).or.SPW_0(148761)	!L_(92) O L_(1254)
        SPW_0(37711)=SPW_0(37711).or.SPW_0(148771)	!L_(92) O L_(1264)
        SPW_0(37711)=SPW_0(37711).or.SPW_0(148780)	!L_(92) O L_(1273)
        SPW_0(37711)=SPW_0(37711).or.SPW_0(148788)	!L_(92) O L_(1281)
!end �����:20FDA65AE406
!beg ������:20FDA60AE500
        SPW_0(37702)=SPW_0(148164)	!L_(83) O L_(657)
        SPW_0(37702)=SPW_0(37702).or.SPW_0(149098)	!L_(83) O L_(1591)
        SPW_0(37702)=SPW_0(37702).or.SPW_0(149078)	!L_(83) O L_(1571)
!end ������:20FDA60AE500
!beg ������:20FDA60AE501
        SPW_0(37699)=.false.	!L_(80) O
!end ������:20FDA60AE501
!beg ������:20FDA60AE502
        SPW_0(37767)=SPW_0(149083)	!L_(148) O L_(1576)
!end ������:20FDA60AE502
!beg ������:20FDA60AE503
        SPW_0(37631)=.false.	!L_(12) O
!end ������:20FDA60AE503
!beg ������:20FDA60AE504
        SPW_0(37634)=SPW_0(149059)	!L_(15) O L_(1552)
!end ������:20FDA60AE504
!beg �������:20FDA62AB800
        SPW_0(37752)=SPW_0(147619)	!L_(133) O L_(112)
        SPW_0(37752)=SPW_0(37752).or.SPW_0(149477)	!L_(133) O L_(1970)
!end �������:20FDA62AB800
!beg ������:20FDA60AE509
        SPW_0(37637)=SPW_0(149029)	!L_(18) O L_(1522)
!end ������:20FDA60AE509
!beg �������:20FDA62AB801
        SPW_0(37623)=SPW_0(147619)	!L_(4) O L_(112)
        SPW_0(37623)=SPW_0(37623).or.SPW_0(148678)	!L_(4) O L_(1171)
!end �������:20FDA62AB801
!beg �����:20FDA60AE500
        SPW_0(37703)=SPW_0(149094)	!L_(84) O L_(1587)
        SPW_0(37703)=SPW_0(37703).or.SPW_0(149101)	!L_(84) O L_(1594)
!end �����:20FDA60AE500
!beg �����:20FDA60AE501
        SPW_0(37700)=.false.	!L_(81) O
!end �����:20FDA60AE501
!beg �����:20FDA60AE502
        SPW_0(37768)=SPW_0(149078)	!L_(149) O L_(1571)
!end �����:20FDA60AE502
!beg �����:20FDA60AE503
        SPW_0(37632)=.false.	!L_(13) O
!end �����:20FDA60AE503
!beg �������:20FDA62AB806
        SPW_0(37732)=SPW_0(148521)	!L_(113) O L_(1014)
!end �������:20FDA62AB806
!beg �����:20FDA60AE504
        SPW_0(37635)=SPW_0(149055)	!L_(16) O L_(1548)
!end �����:20FDA60AE504
!beg �����:20FDA60AE509
        SPW_0(37638)=SPW_0(149015)	!L_(19) O L_(1508)
!end �����:20FDA60AE509
!beg ������:20FDA60AE514
        SPW_0(37643)=.false.	!L_(24) O
!end ������:20FDA60AE514
!beg ������:20FDA60AE515
        SPW_0(37640)=.false.	!L_(21) O
!end ������:20FDA60AE515
!beg ������:20FDA62AE401
        SPW_0(37676)=SPW_0(148516)	!L_(57) O L_(1009)
        SPW_0(37676)=SPW_0(37676).or.SPW_0(148531)	!L_(57) O L_(1024)
        SPW_0(37676)=SPW_0(37676).or.SPW_0(148540)	!L_(57) O L_(1033)
        SPW_0(37676)=SPW_0(37676).or.SPW_0(148549)	!L_(57) O L_(1042)
        SPW_0(37676)=SPW_0(37676).or.SPW_0(148564)	!L_(57) O L_(1057)
!end ������:20FDA62AE401
!beg ������:20FDA62AE406
        SPW_0(37719)=SPW_0(148640)	!L_(100) O L_(1133)
        SPW_0(37719)=SPW_0(37719).or.SPW_0(148651)	!L_(100) O L_(1144)
        SPW_0(37719)=SPW_0(37719).or.SPW_0(148661)	!L_(100) O L_(1154)
        SPW_0(37719)=SPW_0(37719).or.SPW_0(148670)	!L_(100) O L_(1163)
        SPW_0(37719)=SPW_0(37719).or.SPW_0(148681)	!L_(100) O L_(1174)
!end ������:20FDA62AE406
!beg �����:20FDA60AE514
        SPW_0(37644)=.false.	!L_(25) O
!end �����:20FDA60AE514
!beg ����:20FDA61AE401
        SPW_0(37678)=.false.	!L_(59) O
!end ����:20FDA61AE401
!beg �����:20FDA60AE515
        SPW_0(37641)=.false.	!L_(22) O
!end �����:20FDA60AE515
!beg �������:20FDA60CONTS01VS1
        SPW_0(37978)=.false.	!L_(359) O
!end �������:20FDA60CONTS01VS1
!beg �����:20FDA62AE401
        SPW_0(37677)=SPW_0(148527)	!L_(58) O L_(1020)
        SPW_0(37677)=SPW_0(37677).or.SPW_0(148535)	!L_(58) O L_(1028)
        SPW_0(37677)=SPW_0(37677).or.SPW_0(148544)	!L_(58) O L_(1037)
        SPW_0(37677)=SPW_0(37677).or.SPW_0(148553)	!L_(58) O L_(1046)
        SPW_0(37677)=SPW_0(37677).or.SPW_0(148559)	!L_(58) O L_(1052)
!end �����:20FDA62AE401
!beg �������:20FDA60CONTS01VS2
        SPW_0(37974)=.false.	!L_(355) O
!end �������:20FDA60CONTS01VS2
!beg ����:20FDA20TRAN08_C3
        SPW_0(38029)=.false.	!L_(410) O
!end ����:20FDA20TRAN08_C3
!beg ����:20FDA61AE406
        SPW_0(37721)=.false.	!L_(102) O
!end ����:20FDA61AE406
!beg ����:20FDA20TRAN08_C4
        SPW_0(38026)=.false.	!L_(407) O
!end ����:20FDA20TRAN08_C4
!beg ����:20FDA20TRAN08_C5
        SPW_0(38023)=.false.	!L_(404) O
!end ����:20FDA20TRAN08_C5
!beg ����:20FDA20TRAN08_C6
        SPW_0(38020)=.false.	!L_(401) O
!end ����:20FDA20TRAN08_C6
!beg �����:20FDA62AE406
        SPW_0(37720)=SPW_0(148636)	!L_(101) O L_(1129)
        SPW_0(37720)=SPW_0(37720).or.SPW_0(148647)	!L_(101) O L_(1140)
        SPW_0(37720)=SPW_0(37720).or.SPW_0(148657)	!L_(101) O L_(1150)
        SPW_0(37720)=SPW_0(37720).or.SPW_0(148666)	!L_(101) O L_(1159)
        SPW_0(37720)=SPW_0(37720).or.SPW_0(148674)	!L_(101) O L_(1167)
!end �����:20FDA62AE406
!beg ������:20FDA20TRAN02_C3
        SPW_0(38123)=.false.	!L_(504) O
!end ������:20FDA20TRAN02_C3
!beg ������:20FDA20TRAN02_C4
        SPW_0(38120)=.false.	!L_(501) O
!end ������:20FDA20TRAN02_C4
!beg ������:20FDA20TRAN02_C5
        SPW_0(38117)=.false.	!L_(498) O
!end ������:20FDA20TRAN02_C5
!beg ������:20FDA20TRAN02_C6
        SPW_0(38114)=.false.	!L_(495) O
!end ������:20FDA20TRAN02_C6
!beg �������:20FDA60CONTS01VZ1
        SPW_0(37981)=.false.	!L_(362) O
!end �������:20FDA60CONTS01VZ1
!beg ������:20FDA20TRAN02_C7
        SPW_0(38111)=.false.	!L_(492) O
!end ������:20FDA20TRAN02_C7
!beg �������:20FDA60CONTS01VZ2
        SPW_0(37977)=.false.	!L_(358) O
!end �������:20FDA60CONTS01VZ2
!beg �����:20FDA20TRAN10_C3
        SPW_0(38007)=.false.	!L_(388) O
!end �����:20FDA20TRAN10_C3
!beg �����:20FDA20TRAN10_C4
        SPW_0(38004)=.false.	!L_(385) O
!end �����:20FDA20TRAN10_C4
!beg �����:20FDA20TRAN10_C5
        SPW_0(38001)=.false.	!L_(382) O
!end �����:20FDA20TRAN10_C5
!beg �����:20FDA20TRAN10_C6
        SPW_0(37998)=.false.	!L_(379) O
!end �����:20FDA20TRAN10_C6
!beg �����:20FDA20TRAN01_C3
        SPW_0(38139)=.false.	!L_(520) O
!end �����:20FDA20TRAN01_C3
!beg �����:20FDA20TRAN01_C4
        SPW_0(38136)=.false.	!L_(517) O
!end �����:20FDA20TRAN01_C4
!beg �������:20FDA65AB800
        SPW_0(37743)=SPW_0(149212)	!L_(124) O L_(1705)
!end �������:20FDA65AB800
!beg �����:20FDA20TRAN01_C5
        SPW_0(38133)=.false.	!L_(514) O
!end �����:20FDA20TRAN01_C5
!beg �������:20FDA65AB801
        SPW_0(37741)=SPW_0(148170)	!L_(122) O L_(663)
        SPW_0(37741)=SPW_0(37741).or.SPW_0(149212)	!L_(122) O L_(1705)
!end �������:20FDA65AB801
!beg �����:20FDA20TRAN01_C6
        SPW_0(38130)=.false.	!L_(511) O
!end �����:20FDA20TRAN01_C6
!beg �����:20FDA20TRAN01_C7
        SPW_0(38127)=.false.	!L_(508) O
!end �����:20FDA20TRAN01_C7
!beg �������:20FDA65AB806
        SPW_0(37727)=SPW_0(148262)	!L_(108) O L_(755)
!end �������:20FDA65AB806
!beg �������:20FDA21AE701
        SPW_0(38193)=.false.	!L_(574) O
!end �������:20FDA21AE701
!beg �������:20FDA21AE702
        SPW_0(38211)=.false.	!L_(592) O
!end �������:20FDA21AE702
!beg ������:20FDA20TRAN07_C3
        SPW_0(38042)=.false.	!L_(423) O
!end ������:20FDA20TRAN07_C3
!beg ������:20FDA20TRAN07_C4
        SPW_0(38039)=.false.	!L_(420) O
!end ������:20FDA20TRAN07_C4
!beg ������:20FDA20TRAN07_C5
        SPW_0(38036)=.false.	!L_(417) O
!end ������:20FDA20TRAN07_C5
!beg ������:20FDA20TRAN07_C6
        SPW_0(38033)=.false.	!L_(414) O
!end ������:20FDA20TRAN07_C6
!beg �������:20FDA62AB800
        SPW_0(37753)=SPW_0(149473)	!L_(134) O L_(1966)
!end �������:20FDA62AB800
!beg �������:20FDA62AB801
        SPW_0(37624)=SPW_0(148179)	!L_(5) O L_(672)
        SPW_0(37624)=SPW_0(37624).or.SPW_0(149473)	!L_(5) O L_(1966)
!end �������:20FDA62AB801
!beg �������:20FDA62AB806
        SPW_0(37733)=SPW_0(148493)	!L_(114) O L_(986)
!end �������:20FDA62AB806
!beg �����:20FDA20TRAN06_C3
        SPW_0(38067)=.false.	!L_(448) O
!end �����:20FDA20TRAN06_C3
!beg �����:20FDA20TRAN06_C4
        SPW_0(38064)=.false.	!L_(445) O
!end �����:20FDA20TRAN06_C4
!beg �����:20FDA20TRAN06_C5
        SPW_0(38061)=.false.	!L_(442) O
!end �����:20FDA20TRAN06_C5
!beg �����:20FDA20TRAN06_C6
        SPW_0(38058)=.false.	!L_(439) O
!end �����:20FDA20TRAN06_C6
!beg �����:20FDA20TRAN06_C7
        SPW_0(38049)=.false.	!L_(430) O
!end �����:20FDA20TRAN06_C7
!beg �����:20FDA20TRAN06_C8
        SPW_0(38046)=.false.	!L_(427) O
!end �����:20FDA20TRAN06_C8
!beg �������:20FDA60CONTS01VZ1
        SPW_0(37980)=.false.	!L_(361) O
!end �������:20FDA60CONTS01VZ1
!beg ������:20FDA60AE201
        SPW_0(37621)=SPW_0(149118)	!L_(2) O L_(1611)
!end ������:20FDA60AE201
!beg �������:20FDA60CONTS01VZ2
        SPW_0(37976)=.false.	!L_(357) O
!end �������:20FDA60CONTS01VZ2
!beg ������:20FDA60TELEZ
        SPW_0(38157)=.false.	!L_(538) O
!end ������:20FDA60TELEZ
!beg �����:20FDA60AE201
        SPW_0(37622)=SPW_0(149046)	!L_(3) O L_(1539)
!end �����:20FDA60AE201
!beg ����:20FDA20TRAN02_C3
        SPW_0(38122)=.false.	!L_(503) O
!end ����:20FDA20TRAN02_C3
!beg ����:20FDA20TRAN02_C4
        SPW_0(38119)=.false.	!L_(500) O
!end ����:20FDA20TRAN02_C4
!beg ����:20FDA20TRAN02_C5
        SPW_0(38116)=.false.	!L_(497) O
!end ����:20FDA20TRAN02_C5
!beg ����:20FDA20TRAN02_C6
        SPW_0(38113)=.false.	!L_(494) O
!end ����:20FDA20TRAN02_C6
!beg ����:20FDA20TRAN02_C7
        SPW_0(38110)=.false.	!L_(491) O
!end ����:20FDA20TRAN02_C7
!beg �����:20FDA60TELEZ
        SPW_0(38158)=.false.	!L_(539) O
!end �����:20FDA60TELEZ
!beg �������:20FDA21AE701
        SPW_0(38194)=.false.	!L_(575) O
!end �������:20FDA21AE701
!beg �������:20FDA21AE702
        SPW_0(38212)=.false.	!L_(593) O
!end �������:20FDA21AE702
!beg ����:20FDA20TRAN07_C3
        SPW_0(38041)=.false.	!L_(422) O
!end ����:20FDA20TRAN07_C3
!beg ����:20FDA20TRAN07_C4
        SPW_0(38038)=.false.	!L_(419) O
!end ����:20FDA20TRAN07_C4
!beg ����:20FDA20TRAN07_C5
        SPW_0(38035)=.false.	!L_(416) O
!end ����:20FDA20TRAN07_C5
!beg ����:20FDA20TRAN07_C6
        SPW_0(38032)=.false.	!L_(413) O
!end ����:20FDA20TRAN07_C6
!beg ������:20FDA20TRAN10_C3
        SPW_0(38006)=.false.	!L_(387) O
!end ������:20FDA20TRAN10_C3
!beg ������:20FDA20TRAN10_C4
        SPW_0(38003)=.false.	!L_(384) O
!end ������:20FDA20TRAN10_C4
!beg ������:20FDA20TRAN10_C5
        SPW_0(38000)=.false.	!L_(381) O
!end ������:20FDA20TRAN10_C5
!beg ������:20FDA20TRAN10_C6
        SPW_0(37997)=.false.	!L_(378) O
!end ������:20FDA20TRAN10_C6
!beg ������:20FDA20TRAN01_C3
        SPW_0(38138)=.false.	!L_(519) O
!end ������:20FDA20TRAN01_C3
!beg ������:20FDA20TRAN01_C4
        SPW_0(38135)=.false.	!L_(516) O
!end ������:20FDA20TRAN01_C4
!beg ������:20FDA20TRAN01_C5
        SPW_0(38132)=.false.	!L_(513) O
!end ������:20FDA20TRAN01_C5
!beg ������:20FDA20TRAN01_C6
        SPW_0(38129)=.false.	!L_(510) O
!end ������:20FDA20TRAN01_C6
!beg ������:20FDA20TRAN01_C7
        SPW_0(38126)=.false.	!L_(507) O
!end ������:20FDA20TRAN01_C7
!beg ������:20FDA65AE500
        SPW_0(37649)=SPW_0(149227)	!L_(30) O L_(1720)
        SPW_0(37649)=SPW_0(37649).or.SPW_0(149243)	!L_(30) O L_(1736)
        SPW_0(37649)=SPW_0(37649).or.SPW_0(149258)	!L_(30) O L_(1751)
        SPW_0(37649)=SPW_0(37649).or.SPW_0(149272)	!L_(30) O L_(1765)
        SPW_0(37649)=SPW_0(37649).or.SPW_0(149286)	!L_(30) O L_(1779)
!end ������:20FDA65AE500
!beg ����:20FDA64AE500
        SPW_0(37651)=.false.	!L_(32) O
!end ����:20FDA64AE500
!beg �����:20FDA65AE500
        SPW_0(37650)=SPW_0(149223)	!L_(31) O L_(1716)
        SPW_0(37650)=SPW_0(37650).or.SPW_0(149239)	!L_(31) O L_(1732)
        SPW_0(37650)=SPW_0(37650).or.SPW_0(149254)	!L_(31) O L_(1747)
        SPW_0(37650)=SPW_0(37650).or.SPW_0(149268)	!L_(31) O L_(1761)
        SPW_0(37650)=SPW_0(37650).or.SPW_0(149281)	!L_(31) O L_(1774)
!end �����:20FDA65AE500
!beg ������:20FDA20TRAN06_C3
        SPW_0(38066)=.false.	!L_(447) O
!end ������:20FDA20TRAN06_C3
!beg ������:20FDA20TRAN06_C4
        SPW_0(38063)=.false.	!L_(444) O
!end ������:20FDA20TRAN06_C4
!beg ����:20FDA66AE401
        SPW_0(37663)=.false.	!L_(44) O
!end ����:20FDA66AE401
!beg ������:20FDA20TRAN06_C5
        SPW_0(38060)=.false.	!L_(441) O
!end ������:20FDA20TRAN06_C5
!beg ������:20FDA20TRAN06_C6
        SPW_0(38057)=.false.	!L_(438) O
!end ������:20FDA20TRAN06_C6
!beg ������:20FDA20TRAN06_C7
        SPW_0(38048)=.false.	!L_(429) O
!end ������:20FDA20TRAN06_C7
!beg ������:20FDA20TRAN06_C8
        SPW_0(38045)=.false.	!L_(426) O
!end ������:20FDA20TRAN06_C8
!beg ����:20FDA66AE406
        SPW_0(37706)=.false.	!L_(87) O
!end ����:20FDA66AE406
!beg ���������:20FDA60AE402
        SPW_0(37687)=.true.	!L_(68) A
!end ���������:20FDA60AE402
!beg ������:20FDA62AE500
        SPW_0(37658)=SPW_0(149488)	!L_(39) O L_(1981)
        SPW_0(37658)=SPW_0(37658).or.SPW_0(149504)	!L_(39) O L_(1997)
        SPW_0(37658)=SPW_0(37658).or.SPW_0(149519)	!L_(39) O L_(2012)
        SPW_0(37658)=SPW_0(37658).or.SPW_0(149533)	!L_(39) O L_(2026)
        SPW_0(37658)=SPW_0(37658).or.SPW_0(149547)	!L_(39) O L_(2040)
!end ������:20FDA62AE500
!beg ���������:20FDA60AE403
        SPW_0(37629)=.true.	!L_(10) A
!end ���������:20FDA60AE403
!beg ����:20FDA61AE500
        SPW_0(37660)=.false.	!L_(41) O
!end ����:20FDA61AE500
!beg �������:20FDA64AB800
        SPW_0(37746)=SPW_0(147567)	!L_(127) O L_(60)
        SPW_0(37746)=SPW_0(37746).or.SPW_0(149303)	!L_(127) O L_(1796)
!end �������:20FDA64AB800
!beg �������:20FDA64AB801
        SPW_0(37744)=SPW_0(147567)	!L_(125) O L_(60)
        SPW_0(37744)=SPW_0(37744).or.SPW_0(148849)	!L_(125) O L_(1342)
!end �������:20FDA64AB801
!beg �����:20FDA62AE500
        SPW_0(37659)=SPW_0(149484)	!L_(40) O L_(1977)
        SPW_0(37659)=SPW_0(37659).or.SPW_0(149500)	!L_(40) O L_(1993)
        SPW_0(37659)=SPW_0(37659).or.SPW_0(149515)	!L_(40) O L_(2008)
        SPW_0(37659)=SPW_0(37659).or.SPW_0(149529)	!L_(40) O L_(2022)
        SPW_0(37659)=SPW_0(37659).or.SPW_0(149542)	!L_(40) O L_(2035)
!end �����:20FDA62AE500
!beg �����:20FDA20TRAN05_C3
        SPW_0(38079)=.false.	!L_(460) O
!end �����:20FDA20TRAN05_C3
!beg �����:20FDA20TRAN05_C4
        SPW_0(38076)=.false.	!L_(457) O
!end �����:20FDA20TRAN05_C4
!beg �������:20FDA64AB806
        SPW_0(37728)=SPW_0(148367)	!L_(109) O L_(860)
!end �������:20FDA64AB806
!beg �����:20FDA20TRAN05_C5
        SPW_0(38073)=.false.	!L_(454) O
!end �����:20FDA20TRAN05_C5
!beg �����:20FDA20TRAN05_C6
        SPW_0(38070)=.false.	!L_(451) O
!end �����:20FDA20TRAN05_C6
!beg �����:20FDA20TRAN05_C7
        SPW_0(38055)=.false.	!L_(436) O
!end �����:20FDA20TRAN05_C7
!beg �����:20FDA20TRAN05_C8
        SPW_0(38052)=.false.	!L_(433) O
!end �����:20FDA20TRAN05_C8
!beg ������:20FDA64AE401
        SPW_0(37670)=SPW_0(148362)	!L_(51) O L_(855)
        SPW_0(37670)=SPW_0(37670).or.SPW_0(148377)	!L_(51) O L_(870)
        SPW_0(37670)=SPW_0(37670).or.SPW_0(148386)	!L_(51) O L_(879)
        SPW_0(37670)=SPW_0(37670).or.SPW_0(148395)	!L_(51) O L_(888)
        SPW_0(37670)=SPW_0(37670).or.SPW_0(148410)	!L_(51) O L_(903)
!end ������:20FDA64AE401
!beg ������:20FDA64AE406
        SPW_0(37713)=SPW_0(148811)	!L_(94) O L_(1304)
        SPW_0(37713)=SPW_0(37713).or.SPW_0(148822)	!L_(94) O L_(1315)
        SPW_0(37713)=SPW_0(37713).or.SPW_0(148832)	!L_(94) O L_(1325)
        SPW_0(37713)=SPW_0(37713).or.SPW_0(148841)	!L_(94) O L_(1334)
        SPW_0(37713)=SPW_0(37713).or.SPW_0(148852)	!L_(94) O L_(1345)
!end ������:20FDA64AE406
!beg ����:20FDA63AE401
        SPW_0(37672)=.false.	!L_(53) O
!end ����:20FDA63AE401
!beg �����:20FDA64AE401
        SPW_0(37671)=SPW_0(148373)	!L_(52) O L_(866)
        SPW_0(37671)=SPW_0(37671).or.SPW_0(148381)	!L_(52) O L_(874)
        SPW_0(37671)=SPW_0(37671).or.SPW_0(148390)	!L_(52) O L_(883)
        SPW_0(37671)=SPW_0(37671).or.SPW_0(148399)	!L_(52) O L_(892)
        SPW_0(37671)=SPW_0(37671).or.SPW_0(148405)	!L_(52) O L_(898)
!end �����:20FDA64AE401
!beg ����:20FDA63AE406
        SPW_0(37715)=.false.	!L_(96) O
!end ����:20FDA63AE406
!beg ������:20FDA21AE704M3
        SPW_0(38230)=.false.	!L_(611) O
!end ������:20FDA21AE704M3
!beg ����:20FDA20TRAN10_C3
        SPW_0(38005)=.false.	!L_(386) O
!end ����:20FDA20TRAN10_C3
!beg ������:20FDA21AE704M4
        SPW_0(38227)=.false.	!L_(608) O
!end ������:20FDA21AE704M4
!beg ����:20FDA20TRAN10_C4
        SPW_0(38002)=.false.	!L_(383) O
!end ����:20FDA20TRAN10_C4
!beg ������:20FDA21AE704M5
        SPW_0(38215)=.false.	!L_(596) O
!end ������:20FDA21AE704M5
!beg ����:20FDA20TRAN10_C5
        SPW_0(37999)=.false.	!L_(380) O
!end ����:20FDA20TRAN10_C5
!beg �����:20FDA64AE406
        SPW_0(37714)=SPW_0(148807)	!L_(95) O L_(1300)
        SPW_0(37714)=SPW_0(37714).or.SPW_0(148818)	!L_(95) O L_(1311)
        SPW_0(37714)=SPW_0(37714).or.SPW_0(148828)	!L_(95) O L_(1321)
        SPW_0(37714)=SPW_0(37714).or.SPW_0(148837)	!L_(95) O L_(1330)
        SPW_0(37714)=SPW_0(37714).or.SPW_0(148845)	!L_(95) O L_(1338)
!end �����:20FDA64AE406
!beg ������:20FDA21AE704M6
        SPW_0(38213)=.false.	!L_(594) O
!end ������:20FDA21AE704M6
!beg ����:20FDA20TRAN10_C6
        SPW_0(37996)=.false.	!L_(377) O
!end ����:20FDA20TRAN10_C6
!beg ����:U
        SPW_0(37769)=.false.	!L_(150) O
!end ����:U
!beg ����:20FDA20TRAN01_C3
        SPW_0(38137)=.false.	!L_(518) O
!end ����:20FDA20TRAN01_C3
!beg ����:20FDA20TRAN01_C4
        SPW_0(38134)=.false.	!L_(515) O
!end ����:20FDA20TRAN01_C4
!beg ����:20FDA20TRAN01_C5
        SPW_0(38131)=.false.	!L_(512) O
!end ����:20FDA20TRAN01_C5
!beg �������:20FDA61AB800
        SPW_0(37756)=SPW_0(147645)	!L_(137) O L_(138)
        SPW_0(37756)=SPW_0(37756).or.SPW_0(149564)	!L_(137) O L_(2057)
!end �������:20FDA61AB800
!beg ����:20FDA20TRAN01_C6
        SPW_0(38128)=.false.	!L_(509) O
!end ����:20FDA20TRAN01_C6
!beg �������:20FDA61AB801
        SPW_0(37754)=SPW_0(147645)	!L_(135) O L_(138)
        SPW_0(37754)=SPW_0(37754).or.SPW_0(148621)	!L_(135) O L_(1114)
!end �������:20FDA61AB801
!beg ����:20FDA20TRAN01_C7
        SPW_0(38125)=.false.	!L_(506) O
!end ����:20FDA20TRAN01_C7
!beg �������:20FDA61AB806
        SPW_0(37734)=SPW_0(148940)	!L_(115) O L_(1433)
!end �������:20FDA61AB806
!beg ������:20FDA61AE401
        SPW_0(37679)=SPW_0(148935)	!L_(60) O L_(1428)
        SPW_0(37679)=SPW_0(37679).or.SPW_0(148950)	!L_(60) O L_(1443)
        SPW_0(37679)=SPW_0(37679).or.SPW_0(148959)	!L_(60) O L_(1452)
        SPW_0(37679)=SPW_0(37679).or.SPW_0(148968)	!L_(60) O L_(1461)
        SPW_0(37679)=SPW_0(37679).or.SPW_0(148983)	!L_(60) O L_(1476)
!end ������:20FDA61AE401
!beg ������:20FDA61AE406
        SPW_0(37722)=SPW_0(148583)	!L_(103) O L_(1076)
        SPW_0(37722)=SPW_0(37722).or.SPW_0(148594)	!L_(103) O L_(1087)
        SPW_0(37722)=SPW_0(37722).or.SPW_0(148604)	!L_(103) O L_(1097)
        SPW_0(37722)=SPW_0(37722).or.SPW_0(148613)	!L_(103) O L_(1106)
        SPW_0(37722)=SPW_0(37722).or.SPW_0(148624)	!L_(103) O L_(1117)
!end ������:20FDA61AE406
!beg ����:20FDA60AE402
        SPW_0(37683)=.false.	!L_(64) O
!end ����:20FDA60AE402
!beg ����:20FDA60AE403
        SPW_0(37625)=SPW_0(149059)	!L_(6) O L_(1552)
        SPW_0(37625)=SPW_0(37625).or.SPW_0(149083)	!L_(6) O L_(1576)
        SPW_0(37625)=SPW_0(37625).or.SPW_0(149098)	!L_(6) O L_(1591)
        SPW_0(37625)=SPW_0(37625).or.SPW_0(149118)	!L_(6) O L_(1611)
!end ����:20FDA60AE403
!beg �����:20FDA61AE401
        SPW_0(37680)=SPW_0(148946)	!L_(61) O L_(1439)
        SPW_0(37680)=SPW_0(37680).or.SPW_0(148954)	!L_(61) O L_(1447)
        SPW_0(37680)=SPW_0(37680).or.SPW_0(148963)	!L_(61) O L_(1456)
        SPW_0(37680)=SPW_0(37680).or.SPW_0(148972)	!L_(61) O L_(1465)
        SPW_0(37680)=SPW_0(37680).or.SPW_0(148978)	!L_(61) O L_(1471)
!end �����:20FDA61AE401
!beg �����:20FDA61AE406
        SPW_0(37723)=SPW_0(148579)	!L_(104) O L_(1072)
        SPW_0(37723)=SPW_0(37723).or.SPW_0(148590)	!L_(104) O L_(1083)
        SPW_0(37723)=SPW_0(37723).or.SPW_0(148600)	!L_(104) O L_(1093)
        SPW_0(37723)=SPW_0(37723).or.SPW_0(148609)	!L_(104) O L_(1102)
        SPW_0(37723)=SPW_0(37723).or.SPW_0(148617)	!L_(104) O L_(1110)
!end �����:20FDA61AE406
!beg ����:20FDA21AE704M3
        SPW_0(38229)=.false.	!L_(610) O
!end ����:20FDA21AE704M3
!beg ����:20FDA21AE704M4
        SPW_0(38226)=.false.	!L_(607) O
!end ����:20FDA21AE704M4
!beg �������:20FDA91AB001
        SPW_0(37696)=SPW_0(154441)	!L_(77) O L_(350)
        SPW_0(37696)=SPW_0(37696).or.SPW_0(154673)	!L_(77) O L_(582)
        SPW_0(37696)=SPW_0(37696).or.SPW_0(154909)	!L_(77) O L_(818)
        SPW_0(37696)=SPW_0(37696).or.SPW_0(155047)	!L_(77) O L_(956)
        SPW_0(37696)=SPW_0(37696).or.SPW_0(155172)	!L_(77) O L_(1081)
        SPW_0(37696)=SPW_0(37696).or.SPW_0(155298)	!L_(77) O L_(1207)
!end �������:20FDA91AB001
!beg �������:20FDA91AB002
        SPW_0(37692)=SPW_0(155085)	!L_(73) O L_(994)
        SPW_0(37692)=SPW_0(37692).or.SPW_0(155313)	!L_(73) O L_(1222)
!end �������:20FDA91AB002
!beg �������:20FDA91AB003
        SPW_0(37688)=SPW_0(154720)	!L_(69) O L_(629)
        SPW_0(37688)=SPW_0(37688).or.SPW_0(154855)	!L_(69) O L_(764)
!end �������:20FDA91AB003
!beg �������:20FDA91AB004
        SPW_0(37690)=SPW_0(154580)	!L_(71) O L_(489)
        SPW_0(37690)=SPW_0(37690).or.SPW_0(154843)	!L_(71) O L_(752)
!end �������:20FDA91AB004
!beg ����:20FDA20TRAN06_C3
        SPW_0(38065)=.false.	!L_(446) O
!end ����:20FDA20TRAN06_C3
!beg �������:20FDA91AB005
        SPW_0(37694)=SPW_0(154473)	!L_(75) O L_(382)
        SPW_0(37694)=SPW_0(37694).or.SPW_0(155150)	!L_(75) O L_(1059)
!end �������:20FDA91AB005
!beg ����:20FDA20TRAN06_C4
        SPW_0(38062)=.false.	!L_(443) O
!end ����:20FDA20TRAN06_C4
!beg ����:20FDA20TRAN06_C5
        SPW_0(38059)=.false.	!L_(440) O
!end ����:20FDA20TRAN06_C5
!beg ����:20FDA20TRAN06_C6
        SPW_0(38056)=.false.	!L_(437) O
!end ����:20FDA20TRAN06_C6
!beg ����:20FDA20TRAN06_C7
        SPW_0(38047)=.false.	!L_(428) O
!end ����:20FDA20TRAN06_C7
!beg ����:20FDA20TRAN06_C8
        SPW_0(38044)=.false.	!L_(425) O
!end ����:20FDA20TRAN06_C8
!beg �������:20FDA64AB800
        SPW_0(37747)=SPW_0(149299)	!L_(128) O L_(1792)
!end �������:20FDA64AB800
!beg �������:20FDA64AB801
        SPW_0(37745)=SPW_0(148173)	!L_(126) O L_(666)
        SPW_0(37745)=SPW_0(37745).or.SPW_0(149299)	!L_(126) O L_(1792)
!end �������:20FDA64AB801
!beg �������:20FDA64AB806
        SPW_0(37729)=SPW_0(148339)	!L_(110) O L_(832)
!end �������:20FDA64AB806
!beg �����:20FDA21AE704M3
        SPW_0(38231)=.false.	!L_(612) O
!end �����:20FDA21AE704M3
!beg �����:20FDA21AE704M4
        SPW_0(38228)=.false.	!L_(609) O
!end �����:20FDA21AE704M4
!beg �����:20FDA21AE704M5
        SPW_0(38216)=.false.	!L_(597) O
!end �����:20FDA21AE704M5
!beg �������:20FDA61AB800
        SPW_0(37757)=SPW_0(149560)	!L_(138) O L_(2053)
!end �������:20FDA61AB800
!beg �����:20FDA21AE704M6
        SPW_0(38214)=.false.	!L_(595) O
!end �����:20FDA21AE704M6
!beg �������:20FDA61AB801
        SPW_0(37755)=SPW_0(148182)	!L_(136) O L_(675)
        SPW_0(37755)=SPW_0(37755).or.SPW_0(149560)	!L_(136) O L_(2053)
!end �������:20FDA61AB801
!beg �������:20FDA61AB806
        SPW_0(37735)=SPW_0(148570)	!L_(116) O L_(1063)
!end �������:20FDA61AB806
!beg ����������:20FDA60AE402
        SPW_0(37686)=.true.	!L_(67) A
!end ����������:20FDA60AE402
!beg ����������:20FDA60AE403
        SPW_0(37628)=.true.	!L_(9) A
!end ����������:20FDA60AE403
!beg ������:20FDA20TRAN05_C3
        SPW_0(38078)=.false.	!L_(459) O
!end ������:20FDA20TRAN05_C3
!beg ������:20FDA20TRAN05_C4
        SPW_0(38075)=.false.	!L_(456) O
!end ������:20FDA20TRAN05_C4
!beg ������:20FDA20TRAN05_C5
        SPW_0(38072)=.false.	!L_(453) O
!end ������:20FDA20TRAN05_C5
!beg ������:20FDA20TRAN05_C6
        SPW_0(38069)=.false.	!L_(450) O
!end ������:20FDA20TRAN05_C6
!beg ������:20FDA20TRAN05_C7
        SPW_0(38054)=.false.	!L_(435) O
!end ������:20FDA20TRAN05_C7
!beg ������:20FDA20TRAN05_C8
        SPW_0(38051)=.false.	!L_(432) O
!end ������:20FDA20TRAN05_C8
!beg �����:20FDA20TRAN04_C3
        SPW_0(38109)=.false.	!L_(490) O
!end �����:20FDA20TRAN04_C3
!beg �����:20FDA20TRAN04_C4
        SPW_0(38106)=.false.	!L_(487) O
!end �����:20FDA20TRAN04_C4
!beg �����:20FDA20TRAN04_C5
        SPW_0(38103)=.false.	!L_(484) O
!end �����:20FDA20TRAN04_C5
       end

       subroutine FDA10_ConInQ
       end

       subroutine FDA10_ConIn
       LOGICAL*1 SPW_0(0:163839)
       Common/SPW_0/ SPW_0 !
       integer*4 SPW_0_4(0:40959)
       equivalence(SPW_0,SPW_0_4)
       LOGICAL*1 SPW_2(0:20479)
       Common/SPW_2/ SPW_2 !
       integer*4 SPW_2_4(0:5119)
       equivalence(SPW_2,SPW_2_4)
       LOGICAL*1 SPW_4(0:16383)
       Common/SPW_4/ SPW_4 !
       integer*4 SPW_4_4(0:4095)
       equivalence(SPW_4,SPW_4_4)
       LOGICAL*1 SPW_6(0:28671)
       Common/SPW_6/ SPW_6 !
       integer*4 SPW_6_4(0:7167)
       equivalence(SPW_6,SPW_6_4)
        Call FDA10_ConInQ
!beg ���������:20FDA13CU001KN01
        SPW_2(15806)=.false.	!L_(15) O
!end ���������:20FDA13CU001KN01
!beg �������:20FDA13AA201
        SPW_2(15811)=.false.	!L_(20) O
!end �������:20FDA13AA201
!beg �������:20FDA13AA202
        SPW_2(15809)=.false.	!L_(18) O
!end �������:20FDA13AA202
!beg �������:20FDA13AA201
        SPW_2(15812)=.false.	!L_(21) O
!end �������:20FDA13AA201
!beg �������:20FDA13AA202
        SPW_2(15810)=.false.	!L_(19) O
!end �������:20FDA13AA202
!beg �������:20FDA13AA101
        SPW_2(15807)=.false.	!L_(16) O
!end �������:20FDA13AA101
!beg ��������:20FDA13CU001KN01
        SPW_2(15805)=.false.	!L_(14) O
!end ��������:20FDA13CU001KN01
!beg �������:20FDA13AA101
        SPW_2(15808)=.false.	!L_(17) O
!end �������:20FDA13AA101
       end

       subroutine FDA20_ConInQ
       end

       subroutine FDA20_ConIn
       LOGICAL*1 SPW_0(0:163839)
       Common/SPW_0/ SPW_0 !
       integer*4 SPW_0_4(0:40959)
       equivalence(SPW_0,SPW_0_4)
       LOGICAL*1 SPW_2(0:20479)
       Common/SPW_2/ SPW_2 !
       integer*4 SPW_2_4(0:5119)
       equivalence(SPW_2,SPW_2_4)
       LOGICAL*1 SPW_4(0:16383)
       Common/SPW_4/ SPW_4 !
       integer*4 SPW_4_4(0:4095)
       equivalence(SPW_4,SPW_4_4)
       LOGICAL*1 SPW_6(0:28671)
       Common/SPW_6/ SPW_6 !
       integer*4 SPW_6_4(0:7167)
       equivalence(SPW_6,SPW_6_4)
        Call FDA20_ConInQ
!beg �������:20FDA23BR006KA12
        SPW_0(157257)=.false.	!L_(549) O
!end �������:20FDA23BR006KA12
!beg ��������:20FDA22AX001KN01
        SPW_0(157245)=.false.	!L_(537) O
!end ��������:20FDA22AX001KN01
!beg �������:20FDA23BR006KA12
        SPW_0(157258)=.false.	!L_(550) O
!end �������:20FDA23BR006KA12
!beg �������:20FDA21BR109KA03
        SPW_0(157251)=.false.	!L_(543) O
!end �������:20FDA21BR109KA03
!beg ���������:20FDA22AX001KN01
        SPW_0(157246)=.false.	!L_(538) O
!end ���������:20FDA22AX001KN01
!beg �������:20FDA21BR109KA03
        SPW_0(157252)=.false.	!L_(544) O
!end �������:20FDA21BR109KA03
!beg �������:20FDA23BR005KA12
        SPW_0(157259)=.false.	!L_(551) O
!end �������:20FDA23BR005KA12
!beg �������:20FDA23BR005KA12
        SPW_0(157260)=.false.	!L_(552) O
!end �������:20FDA23BR005KA12
!beg �������:20FDA21BR108KA03
        SPW_0(157253)=.false.	!L_(545) O
!end �������:20FDA21BR108KA03
!beg ��������:20FDA21AX001KN01
        SPW_0(157247)=.false.	!L_(539) O
!end ��������:20FDA21AX001KN01
!beg �������:20FDA21BR108KA03
        SPW_0(157254)=.false.	!L_(546) O
!end �������:20FDA21BR108KA03
!beg �������:20FDA23BR004KA12
        SPW_0(157261)=.false.	!L_(553) O
!end �������:20FDA23BR004KA12
!beg �������:20FDA23BR004KA12
        SPW_0(157262)=.false.	!L_(554) O
!end �������:20FDA23BR004KA12
!beg ��������:20FDA23CU001KN01
        SPW_0(157265)=.false.	!L_(557) O
!end ��������:20FDA23CU001KN01
!beg ���������:20FDA21AX001KN01
        SPW_0(157248)=.false.	!L_(540) O
!end ���������:20FDA21AX001KN01
!beg �������:20FDA21BR107KA03
        SPW_0(157255)=.false.	!L_(547) O
!end �������:20FDA21BR107KA03
!beg �������:20FDA21BR110KA03
        SPW_0(157249)=.false.	!L_(541) O
!end �������:20FDA21BR110KA03
!beg �������:20FDA21BR107KA03
        SPW_0(157256)=.false.	!L_(548) O
!end �������:20FDA21BR107KA03
!beg �������:20FDA23BR003KA12
        SPW_0(157263)=.false.	!L_(555) O
!end �������:20FDA23BR003KA12
!beg �������:20FDA21BR110KA03
        SPW_0(157250)=.false.	!L_(542) O
!end �������:20FDA21BR110KA03
!beg ���������:20FDA23CU001KN01
        SPW_0(157266)=.false.	!L_(558) O
!end ���������:20FDA23CU001KN01
!beg �������:20FDA23BR003KA12
        SPW_0(157264)=.false.	!L_(556) O
!end �������:20FDA23BR003KA12
       end

       subroutine FDA30_ConInQ
       end

       subroutine FDA30_ConIn
       LOGICAL*1 SPW_0(0:163839)
       Common/SPW_0/ SPW_0 !
       integer*4 SPW_0_4(0:40959)
       equivalence(SPW_0,SPW_0_4)
       LOGICAL*1 SPW_2(0:20479)
       Common/SPW_2/ SPW_2 !
       integer*4 SPW_2_4(0:5119)
       equivalence(SPW_2,SPW_2_4)
       LOGICAL*1 SPW_4(0:16383)
       Common/SPW_4/ SPW_4 !
       integer*4 SPW_4_4(0:4095)
       equivalence(SPW_4,SPW_4_4)
       LOGICAL*1 SPW_6(0:28671)
       Common/SPW_6/ SPW_6 !
       integer*4 SPW_6_4(0:7167)
       equivalence(SPW_6,SPW_6_4)
        Call FDA30_ConInQ
!beg ��������:20FDA34CU001KN01
        SPW_6(25116)=.false.	!L_(176) O
!end ��������:20FDA34CU001KN01
!beg ���������:20FDA31CU001KN01
        SPW_6(25123)=.false.	!L_(183) O
!end ���������:20FDA31CU001KN01
!beg ���������:20FDA34CU001KN01
        SPW_6(25117)=.false.	!L_(177) O
!end ���������:20FDA34CU001KN01
!beg ��������:20FDA33CU001KN01
        SPW_6(25118)=.false.	!L_(178) O
!end ��������:20FDA33CU001KN01
!beg ���������:20FDA33CU001KN01
        SPW_6(25119)=.false.	!L_(179) O
!end ���������:20FDA33CU001KN01
!beg ��������:20FDA32CU001KN01
        SPW_6(25120)=.false.	!L_(180) O
!end ��������:20FDA32CU001KN01
!beg ��������:20FDA35CU001KN01
        SPW_6(25114)=.false.	!L_(174) O
!end ��������:20FDA35CU001KN01
!beg ���������:20FDA32CU001KN01
        SPW_6(25121)=.false.	!L_(181) O
!end ���������:20FDA32CU001KN01
!beg ���������:20FDA35CU001KN01
        SPW_6(25115)=.false.	!L_(175) O
!end ���������:20FDA35CU001KN01
!beg ��������:20FDA31CU001KN01
        SPW_6(25122)=.false.	!L_(182) O
!end ��������:20FDA31CU001KN01
       end

       subroutine FDA40_ConInQ
       end

       subroutine FDA40_ConIn
       LOGICAL*1 SPW_0(0:163839)
       Common/SPW_0/ SPW_0 !
       integer*4 SPW_0_4(0:40959)
       equivalence(SPW_0,SPW_0_4)
       LOGICAL*1 SPW_2(0:20479)
       Common/SPW_2/ SPW_2 !
       integer*4 SPW_2_4(0:5119)
       equivalence(SPW_2,SPW_2_4)
       LOGICAL*1 SPW_4(0:16383)
       Common/SPW_4/ SPW_4 !
       integer*4 SPW_4_4(0:4095)
       equivalence(SPW_4,SPW_4_4)
       LOGICAL*1 SPW_6(0:28671)
       Common/SPW_6/ SPW_6 !
       integer*4 SPW_6_4(0:7167)
       equivalence(SPW_6,SPW_6_4)
        Call FDA40_ConInQ
!beg ������:20FDA40_PUNCH
        SPW_0(156633)=.false.	!L_(716) O
!end ������:20FDA40_PUNCH
!beg ������:20FDA40_STICK_LOADER
        SPW_0(156626)=.false.	!L_(709) O
!end ������:20FDA40_STICK_LOADER
!beg ������:20FDA91AE018
        SPW_0(156572)=.false.	!L_(655) O
!end ������:20FDA91AE018
!beg �����:20FDA91AE018
        SPW_0(156584)=.false.	!L_(667) O
!end �����:20FDA91AE018
!beg ����������:20FDA40_FEEDER
        SPW_0(156607)=.true.	!L_(690) A
!end ����������:20FDA40_FEEDER
!beg ���������:20FDA91AE013
        SPW_0(156555)=.false.	!L_(638) O
!end ���������:20FDA91AE013
!beg ���������:20FDA91AE018
        SPW_0(156570)=.false.	!L_(653) O
!end ���������:20FDA91AE018
!beg ���������4:20FDA91AE013
        SPW_0(156564)=.false.	!L_(647) O
!end ���������4:20FDA91AE013
!beg ���������4:20FDA91AE018
        SPW_0(156579)=.false.	!L_(662) O
!end ���������4:20FDA91AE018
!beg ������:20FDA40_FEEDER
        SPW_0(156603)=.false.	!L_(686) O
!end ������:20FDA40_FEEDER
!beg �����:20FDA40_PUNCH
        SPW_0(156634)=.false.	!L_(717) O
!end �����:20FDA40_PUNCH
!beg ���������9:20FDA91AE013
        SPW_0(156559)=.false.	!L_(642) O
!end ���������9:20FDA91AE013
!beg ���������9:20FDA91AE018
        SPW_0(156574)=.false.	!L_(657) O
!end ���������9:20FDA91AE018
!beg ������:20FDA91AE013
        SPW_0(156556)=.false.	!L_(639) O
!end ������:20FDA91AE013
!beg ������:20FDA91AE018
        SPW_0(156571)=SPW_0(154543)	!L_(654) O L_(452)
!end ������:20FDA91AE018
!beg ����:20FDA40_PUNCH
        SPW_0(156632)=.false.	!L_(715) O
!end ����:20FDA40_PUNCH
!beg ���������:20FDA40_EJECTOR
        SPW_0(156601)=.true.	!L_(684) A
!end ���������:20FDA40_EJECTOR
!beg ���������:20FDA40_FEEDER
        SPW_0(156608)=.true.	!L_(691) A
!end ���������:20FDA40_FEEDER
!beg ������:20FDA40_MATRIX
        SPW_0(156619)=.false.	!L_(702) O
!end ������:20FDA40_MATRIX
!beg �����:20FDA40_STICK_LOADER
        SPW_0(156627)=.false.	!L_(710) O
!end �����:20FDA40_STICK_LOADER
!beg ���������:20FDA40_PUNCH
        SPW_0(156630)=.false.	!L_(713) O
!end ���������:20FDA40_PUNCH
!beg ����:20FDA40_MATRIX
        SPW_0(156618)=.false.	!L_(701) O
!end ����:20FDA40_MATRIX
!beg ���������5:20FDA91AE013
        SPW_0(156563)=.false.	!L_(646) O
!end ���������5:20FDA91AE013
!beg ���������5:20FDA91AE018
        SPW_0(156578)=.false.	!L_(661) O
!end ���������5:20FDA91AE018
!beg ���������:20FDA40_FEEDER
        SPW_0(156602)=.false.	!L_(685) O
!end ���������:20FDA40_FEEDER
!beg �����:20FDA40_MATRIX
        SPW_0(156620)=.false.	!L_(703) O
!end �����:20FDA40_MATRIX
!beg ���������:20FDA40_BOOT
        SPW_0(156615)=.true.	!L_(698) A
!end ���������:20FDA40_BOOT
!beg �����:20FDA91AE013
        SPW_0(156557)=.false.	!L_(640) O
!end �����:20FDA91AE013
!beg ����:20FDA91AE013
        SPW_0(156569)=.false.	!L_(652) O
!end ����:20FDA91AE013
!beg ����������:20FDA40_EJECTOR
        SPW_0(156600)=.true.	!L_(683) A
!end ����������:20FDA40_EJECTOR
!beg ���������10:20FDA91AE013
        SPW_0(156568)=.false.	!L_(651) O
!end ���������10:20FDA91AE013
!beg ���������10:20FDA91AE018
        SPW_0(156583)=.false.	!L_(666) O
!end ���������10:20FDA91AE018
!beg ������:20FDA40_PUNCH
        SPW_0(156631)=.false.	!L_(714) O
!end ������:20FDA40_PUNCH
!beg ���������:20FDA40_STICK_LOADER
        SPW_0(156629)=.true.	!L_(712) A
!end ���������:20FDA40_STICK_LOADER
!beg ����:20FDA40_BOOT
        SPW_0(156611)=.false.	!L_(694) O
!end ����:20FDA40_BOOT
!beg ���������1:20FDA91AE013
        SPW_0(156567)=.false.	!L_(650) O
!end ���������1:20FDA91AE013
!beg ���������1:20FDA91AE018
        SPW_0(156582)=.false.	!L_(665) O
!end ���������1:20FDA91AE018
!beg ������:20FDA40_STICK_LOADER
        SPW_0(156624)=.false.	!L_(707) O
!end ������:20FDA40_STICK_LOADER
!beg ���������6:20FDA91AE013
        SPW_0(156562)=.false.	!L_(645) O
!end ���������6:20FDA91AE013
!beg ���������6:20FDA91AE018
        SPW_0(156577)=.false.	!L_(660) O
!end ���������6:20FDA91AE018
!beg ����������:20FDA40_BOOT
        SPW_0(156614)=.true.	!L_(697) A
!end ����������:20FDA40_BOOT
!beg ���������:20FDA40_STICK_LOADER
        SPW_0(156623)=.false.	!L_(706) O
!end ���������:20FDA40_STICK_LOADER
!beg ����������:20FDA40_MATRIX
        SPW_0(156621)=.true.	!L_(704) A
!end ����������:20FDA40_MATRIX
!beg ������:20FDA40_EJECTOR
        SPW_0(156596)=.false.	!L_(679) O
!end ������:20FDA40_EJECTOR
!beg ��������:20FDA41CU001KN01
        SPW_0(156637)=.false.	!L_(720) O
!end ��������:20FDA41CU001KN01
!beg ������:20FDA40_FEEDER
        SPW_0(156605)=.false.	!L_(688) O
!end ������:20FDA40_FEEDER
!beg ����:20FDA91AE013
        SPW_0(156558)=.false.	!L_(641) O
!end ����:20FDA91AE013
!beg ����:20FDA91AE018
        SPW_0(156573)=.false.	!L_(656) O
!end ����:20FDA91AE018
!beg ���������2:20FDA91AE013
        SPW_0(156566)=.false.	!L_(649) O
!end ���������2:20FDA91AE013
!beg ���������2:20FDA91AE018
        SPW_0(156581)=.false.	!L_(664) O
!end ���������2:20FDA91AE018
!beg ������:20FDA40_MATRIX
        SPW_0(156617)=.false.	!L_(700) O
!end ������:20FDA40_MATRIX
!beg ���������7:20FDA91AE013
        SPW_0(156561)=.false.	!L_(644) O
!end ���������7:20FDA91AE013
!beg ����:20FDA40_FEEDER
        SPW_0(156604)=.false.	!L_(687) O
!end ����:20FDA40_FEEDER
!beg ���������7:20FDA91AE018
        SPW_0(156576)=.false.	!L_(659) O
!end ���������7:20FDA91AE018
!beg ����������:20FDA40_PUNCH
        SPW_0(156635)=.true.	!L_(718) A
!end ����������:20FDA40_PUNCH
!beg ����:20FDA40_STICK_LOADER
        SPW_0(156625)=.false.	!L_(708) O
!end ����:20FDA40_STICK_LOADER
!beg �����:20FDA40_FEEDER
        SPW_0(156606)=.false.	!L_(689) O
!end �����:20FDA40_FEEDER
!beg ����������:20FDA40_STICK_LOADER
        SPW_0(156628)=.true.	!L_(711) A
!end ����������:20FDA40_STICK_LOADER
!beg �����:20FDA40_EJECTOR
        SPW_0(156599)=.false.	!L_(682) O
!end �����:20FDA40_EJECTOR
!beg ������:20FDA40_BOOT
        SPW_0(156612)=.false.	!L_(695) O
!end ������:20FDA40_BOOT
!beg ���������:20FDA40_MATRIX
        SPW_0(156622)=.true.	!L_(705) A
!end ���������:20FDA40_MATRIX
!beg ���������:20FDA41CU001KN01
        SPW_0(156638)=.false.	!L_(721) O
!end ���������:20FDA41CU001KN01
!beg �����:20FDA40_BOOT
        SPW_0(156613)=.false.	!L_(696) O
!end �����:20FDA40_BOOT
!beg ���������:20FDA40_EJECTOR
        SPW_0(156595)=.false.	!L_(678) O
!end ���������:20FDA40_EJECTOR
!beg ������:20FDA40_EJECTOR
        SPW_0(156598)=.false.	!L_(681) O
!end ������:20FDA40_EJECTOR
!beg ���������:20FDA40_BOOT
        SPW_0(156609)=.false.	!L_(692) O
!end ���������:20FDA40_BOOT
!beg ���������:20FDA40_MATRIX
        SPW_0(156616)=.false.	!L_(699) O
!end ���������:20FDA40_MATRIX
!beg ���������3:20FDA91AE013
        SPW_0(156565)=.false.	!L_(648) O
!end ���������3:20FDA91AE013
!beg ���������3:20FDA91AE018
        SPW_0(156580)=.false.	!L_(663) O
!end ���������3:20FDA91AE018
!beg ����:20FDA40_EJECTOR
        SPW_0(156597)=.false.	!L_(680) O
!end ����:20FDA40_EJECTOR
!beg ���������:20FDA40_PUNCH
        SPW_0(156636)=.true.	!L_(719) A
!end ���������:20FDA40_PUNCH
!beg ���������8:20FDA91AE013
        SPW_0(156560)=.false.	!L_(643) O
!end ���������8:20FDA91AE013
!beg ������:20FDA40_BOOT
        SPW_0(156610)=.false.	!L_(693) O
!end ������:20FDA40_BOOT
!beg ���������8:20FDA91AE018
        SPW_0(156575)=.false.	!L_(658) O
!end ���������8:20FDA91AE018
       end

       subroutine FDA50_ConInQ
       end

       subroutine FDA50_ConIn
       LOGICAL*1 SPW_0(0:163839)
       Common/SPW_0/ SPW_0 !
       integer*4 SPW_0_4(0:40959)
       equivalence(SPW_0,SPW_0_4)
       LOGICAL*1 SPW_2(0:20479)
       Common/SPW_2/ SPW_2 !
       integer*4 SPW_2_4(0:5119)
       equivalence(SPW_2,SPW_2_4)
       LOGICAL*1 SPW_4(0:16383)
       Common/SPW_4/ SPW_4 !
       integer*4 SPW_4_4(0:4095)
       equivalence(SPW_4,SPW_4_4)
       LOGICAL*1 SPW_6(0:28671)
       Common/SPW_6/ SPW_6 !
       integer*4 SPW_6_4(0:7167)
       equivalence(SPW_6,SPW_6_4)
        Call FDA50_ConInQ
!beg ���������2:20FDA52AE501KE01
        SPW_2(16293)=.false.	!L_(380) O
!end ���������2:20FDA52AE501KE01
!beg ���������2:20FDA52AE501KE02
        SPW_2(15928)=.false.	!L_(15) O
!end ���������2:20FDA52AE501KE02
!beg ���������9:20FDA52AE501KE01
        SPW_2(16286)=.false.	!L_(373) O
!end ���������9:20FDA52AE501KE01
!beg ���������9:20FDA52AE501KE02
        SPW_2(15921)=.false.	!L_(8) O
!end ���������9:20FDA52AE501KE02
!beg ���������1:20FDA52AE501KE01
        SPW_2(16294)=.false.	!L_(381) O
!end ���������1:20FDA52AE501KE01
!beg ���������1:20FDA52AE501KE02
        SPW_2(15929)=.false.	!L_(16) O
!end ���������1:20FDA52AE501KE02
!beg ���������8:20FDA52AE501KE01
        SPW_2(16287)=.false.	!L_(374) O
!end ���������8:20FDA52AE501KE01
!beg ���������8:20FDA52AE501KE02
        SPW_2(15922)=.false.	!L_(9) O
!end ���������8:20FDA52AE501KE02
!beg ���������10:20FDA52AE501KE01
        SPW_2(16295)=.false.	!L_(382) O
!end ���������10:20FDA52AE501KE01
!beg ���������10:20FDA52AE501KE02
        SPW_2(15930)=.false.	!L_(17) O
!end ���������10:20FDA52AE501KE02
!beg �����:20FDA52AE501KE01
        SPW_2(16296)=.false.	!L_(383) O
!end �����:20FDA52AE501KE01
!beg �����:20FDA52AE501KE02
        SPW_2(15931)=.false.	!L_(18) O
!end �����:20FDA52AE501KE02
!beg ���������7:20FDA52AE501KE01
        SPW_2(16288)=.false.	!L_(375) O
!end ���������7:20FDA52AE501KE01
!beg ���������7:20FDA52AE501KE02
        SPW_2(15923)=.false.	!L_(10) O
!end ���������7:20FDA52AE501KE02
!beg ��������:20FDA51CU001KN01
        SPW_2(16303)=.false.	!L_(390) O
!end ��������:20FDA51CU001KN01
!beg ������:20FDA52AE501KE01
        SPW_2(16284)=.false.	!L_(371) O
!end ������:20FDA52AE501KE01
!beg ������:20FDA52AE501KE02
        SPW_2(15919)=.false.	!L_(6) O
!end ������:20FDA52AE501KE02
!beg ���������6:20FDA52AE501KE01
        SPW_2(16289)=.false.	!L_(376) O
!end ���������6:20FDA52AE501KE01
!beg ���������6:20FDA52AE501KE02
        SPW_2(15924)=.false.	!L_(11) O
!end ���������6:20FDA52AE501KE02
!beg ���������:20FDA51CU001KN01
        SPW_2(16304)=.false.	!L_(391) O
!end ���������:20FDA51CU001KN01
!beg ����:20FDA52AE501KE01
        SPW_2(16285)=.false.	!L_(372) O
!end ����:20FDA52AE501KE01
!beg ����:20FDA52AE501KE02
        SPW_2(15920)=.false.	!L_(7) O
!end ����:20FDA52AE501KE02
!beg ���������5:20FDA52AE501KE01
        SPW_2(16290)=.false.	!L_(377) O
!end ���������5:20FDA52AE501KE01
!beg ���������5:20FDA52AE501KE02
        SPW_2(15925)=.false.	!L_(12) O
!end ���������5:20FDA52AE501KE02
!beg ���������:20FDA52AE501KE01
        SPW_2(16282)=.false.	!L_(369) O
!end ���������:20FDA52AE501KE01
!beg ���������:20FDA52AE501KE02
        SPW_2(15917)=.false.	!L_(4) O
!end ���������:20FDA52AE501KE02
!beg ���������4:20FDA52AE501KE01
        SPW_2(16291)=.false.	!L_(378) O
!end ���������4:20FDA52AE501KE01
!beg ���������4:20FDA52AE501KE02
        SPW_2(15926)=.false.	!L_(13) O
!end ���������4:20FDA52AE501KE02
!beg ���������3:20FDA52AE501KE01
        SPW_2(16292)=.false.	!L_(379) O
!end ���������3:20FDA52AE501KE01
!beg ���������3:20FDA52AE501KE02
        SPW_2(15927)=.false.	!L_(14) O
!end ���������3:20FDA52AE501KE02
!beg ������:20FDA52AE501KE01
        SPW_2(16283)=.false.	!L_(370) O
!end ������:20FDA52AE501KE01
!beg ������:20FDA52AE501KE02
        SPW_2(15918)=.false.	!L_(5) O
!end ������:20FDA52AE501KE02
       end

       subroutine FDA60_ConInQ
       end

       subroutine FDA60_ConIn
       LOGICAL*1 SPW_0(0:163839)
       Common/SPW_0/ SPW_0 !
       integer*4 SPW_0_4(0:40959)
       equivalence(SPW_0,SPW_0_4)
       LOGICAL*1 SPW_2(0:20479)
       Common/SPW_2/ SPW_2 !
       integer*4 SPW_2_4(0:5119)
       equivalence(SPW_2,SPW_2_4)
       LOGICAL*1 SPW_4(0:16383)
       Common/SPW_4/ SPW_4 !
       integer*4 SPW_4_4(0:4095)
       equivalence(SPW_4,SPW_4_4)
       LOGICAL*1 SPW_6(0:28671)
       Common/SPW_6/ SPW_6 !
       integer*4 SPW_6_4(0:7167)
       equivalence(SPW_6,SPW_6_4)
        Call FDA60_ConInQ
!beg �������:20FDA65AA206
        SPW_0(147805)=.false.	!L_(298) O
!end �������:20FDA65AA206
!beg �������:20FDA65AA207
        SPW_0(147803)=.false.	!L_(296) O
!end �������:20FDA65AA207
!beg �������:20FDA65AA208
        SPW_0(147801)=.false.	!L_(294) O
!end �������:20FDA65AA208
!beg �������:20FDA65AA209
        SPW_0(147799)=.false.	!L_(292) O
!end �������:20FDA65AA209
!beg �������:20FDA64AA133
        SPW_0(148094)=.false.	!L_(587) O
!end �������:20FDA64AA133
!beg �������:20FDA64AA134
        SPW_0(148092)=.false.	!L_(585) O
!end �������:20FDA64AA134
!beg �������:20FDA64AA135
        SPW_0(148102)=.false.	!L_(595) O
!end �������:20FDA64AA135
!beg �������:20FDA64AA136
        SPW_0(148100)=.false.	!L_(593) O
!end �������:20FDA64AA136
!beg �������:20FDA64AA137
        SPW_0(148098)=.false.	!L_(591) O
!end �������:20FDA64AA137
!beg �������:20FDA64AA138
        SPW_0(148096)=.false.	!L_(589) O
!end �������:20FDA64AA138
!beg �������:20FDA64AA143
        SPW_0(148042)=.false.	!L_(535) O
!end �������:20FDA64AA143
!beg �������:20FDA64AA144
        SPW_0(148024)=.false.	!L_(517) O
!end �������:20FDA64AA144
!beg �������:20FDA64AA145
        SPW_0(148040)=.false.	!L_(533) O
!end �������:20FDA64AA145
!beg �������:20FDA64AA150
        SPW_0(147988)=.false.	!L_(481) O
!end �������:20FDA64AA150
!beg �������:20FDA64AA151
        SPW_0(147986)=.false.	!L_(479) O
!end �������:20FDA64AA151
!beg �������:20FDA64AA152
        SPW_0(147984)=.false.	!L_(477) O
!end �������:20FDA64AA152
!beg �������:20FDA64AA153
        SPW_0(147850)=.false.	!L_(343) O
!end �������:20FDA64AA153
!beg �������:20FDA64AA154
        SPW_0(147848)=.false.	!L_(341) O
!end �������:20FDA64AA154
!beg �������:20FDA62AA206
        SPW_0(147829)=.false.	!L_(322) O
!end �������:20FDA62AA206
!beg �������:20FDA64AA155
        SPW_0(147982)=.false.	!L_(475) O
!end �������:20FDA64AA155
!beg �������:20FDA62AA207
        SPW_0(147827)=.false.	!L_(320) O
!end �������:20FDA62AA207
!beg �������:20FDA64AA156
        SPW_0(147980)=.false.	!L_(473) O
!end �������:20FDA64AA156
!beg �������:20FDA62AA208
        SPW_0(147825)=.false.	!L_(318) O
!end �������:20FDA62AA208
!beg �������:20FDA64AA157
        SPW_0(147906)=.false.	!L_(399) O
!end �������:20FDA64AA157
!beg �������:20FDA62AA209
        SPW_0(147823)=.false.	!L_(316) O
!end �������:20FDA62AA209
!beg �������:20FDA64AA158
        SPW_0(147904)=.false.	!L_(397) O
!end �������:20FDA64AA158
!beg �������:20FDA64AA159
        SPW_0(147908)=.false.	!L_(401) O
!end �������:20FDA64AA159
!beg �������:20FDA61AA133
        SPW_0(148118)=.false.	!L_(611) O
!end �������:20FDA61AA133
!beg �������:20FDA61AA134
        SPW_0(148116)=.false.	!L_(609) O
!end �������:20FDA61AA134
!beg �������:20FDA61AA135
        SPW_0(148126)=.false.	!L_(619) O
!end �������:20FDA61AA135
!beg �������:20FDA61AA136
        SPW_0(148124)=.false.	!L_(617) O
!end �������:20FDA61AA136
!beg �������:20FDA61AA137
        SPW_0(148122)=.false.	!L_(615) O
!end �������:20FDA61AA137
!beg �������:20FDA61AA138
        SPW_0(148120)=.false.	!L_(613) O
!end �������:20FDA61AA138
!beg �������:20FDA64AA160
        SPW_0(147910)=.false.	!L_(403) O
!end �������:20FDA64AA160
!beg �������:20FDA64AA161
        SPW_0(147916)=.false.	!L_(409) O
!end �������:20FDA64AA161
!beg �������:20FDA64AA162
        SPW_0(147914)=.false.	!L_(407) O
!end �������:20FDA64AA162
!beg �������:20FDA64AA163
        SPW_0(147912)=.false.	!L_(405) O
!end �������:20FDA64AA163
!beg �������:20FDA64AA164
        SPW_0(147868)=.false.	!L_(361) O
!end �������:20FDA64AA164
!beg �������:20FDA61AA143
        SPW_0(148054)=.false.	!L_(547) O
!end �������:20FDA61AA143
!beg �������:20FDA61AA144
        SPW_0(148030)=.false.	!L_(523) O
!end �������:20FDA61AA144
!beg �������:20FDA61AA145
        SPW_0(148052)=.false.	!L_(545) O
!end �������:20FDA61AA145
!beg �������:20FDA61AA150
        SPW_0(148018)=.false.	!L_(511) O
!end �������:20FDA61AA150
!beg �������:20FDA61AA151
        SPW_0(148016)=.false.	!L_(509) O
!end �������:20FDA61AA151
!beg �������:20FDA61AA152
        SPW_0(148014)=.false.	!L_(507) O
!end �������:20FDA61AA152
!beg �������:20FDA61AA153
        SPW_0(147862)=.false.	!L_(355) O
!end �������:20FDA61AA153
!beg �������:20FDA61AA154
        SPW_0(147860)=.false.	!L_(353) O
!end �������:20FDA61AA154
!beg �������:20FDA61AA155
        SPW_0(148012)=.false.	!L_(505) O
!end �������:20FDA61AA155
!beg �������:20FDA61AA156
        SPW_0(148010)=.false.	!L_(503) O
!end �������:20FDA61AA156
!beg �������:20FDA61AA157
        SPW_0(147948)=.false.	!L_(441) O
!end �������:20FDA61AA157
!beg �������:20FDA61AA158
        SPW_0(147946)=.false.	!L_(439) O
!end �������:20FDA61AA158
!beg �������:20FDA61AA159
        SPW_0(147950)=.false.	!L_(443) O
!end �������:20FDA61AA159
!beg �������:20FDA61AA160
        SPW_0(147952)=.false.	!L_(445) O
!end �������:20FDA61AA160
!beg �������:20FDA61AA161
        SPW_0(147958)=.false.	!L_(451) O
!end �������:20FDA61AA161
!beg �������:20FDA61AA162
        SPW_0(147956)=.false.	!L_(449) O
!end �������:20FDA61AA162
!beg �������:20FDA61AA163
        SPW_0(147954)=.false.	!L_(447) O
!end �������:20FDA61AA163
!beg �������:20FDA61AA164
        SPW_0(147874)=.false.	!L_(367) O
!end �������:20FDA61AA164
!beg �������:20FDA64AA133
        SPW_0(148093)=.false.	!L_(586) O
!end �������:20FDA64AA133
!beg �������:20FDA64AA134
        SPW_0(148091)=.false.	!L_(584) O
!end �������:20FDA64AA134
!beg �������:20FDA64AA135
        SPW_0(148101)=.false.	!L_(594) O
!end �������:20FDA64AA135
!beg �������:20FDA64AA136
        SPW_0(148099)=.false.	!L_(592) O
!end �������:20FDA64AA136
!beg �������:20FDA64AA137
        SPW_0(148097)=.false.	!L_(590) O
!end �������:20FDA64AA137
!beg �������:20FDA64AA138
        SPW_0(148095)=.false.	!L_(588) O
!end �������:20FDA64AA138
!beg �������:20FDA64AA143
        SPW_0(148041)=.false.	!L_(534) O
!end �������:20FDA64AA143
!beg �������:20FDA64AA144
        SPW_0(148023)=.false.	!L_(516) O
!end �������:20FDA64AA144
!beg �������:20FDA64AA145
        SPW_0(148039)=.false.	!L_(532) O
!end �������:20FDA64AA145
!beg �������:20FDA64AA150
        SPW_0(147987)=.false.	!L_(480) O
!end �������:20FDA64AA150
!beg �������:20FDA64AA151
        SPW_0(147985)=.false.	!L_(478) O
!end �������:20FDA64AA151
!beg �������:20FDA64AA152
        SPW_0(147983)=.false.	!L_(476) O
!end �������:20FDA64AA152
!beg �������:20FDA64AA153
        SPW_0(147849)=.false.	!L_(342) O
!end �������:20FDA64AA153
!beg �������:20FDA64AA154
        SPW_0(147847)=.false.	!L_(340) O
!end �������:20FDA64AA154
!beg �������:20FDA64AA155
        SPW_0(147981)=.false.	!L_(474) O
!end �������:20FDA64AA155
!beg �������:20FDA64AA156
        SPW_0(147979)=.false.	!L_(472) O
!end �������:20FDA64AA156
!beg �������:20FDA64AA157
        SPW_0(147905)=.false.	!L_(398) O
!end �������:20FDA64AA157
!beg �������:20FDA64AA158
        SPW_0(147903)=.false.	!L_(396) O
!end �������:20FDA64AA158
!beg �������:20FDA64AA159
        SPW_0(147907)=.false.	!L_(400) O
!end �������:20FDA64AA159
!beg �������:20FDA61AA133
        SPW_0(148117)=.false.	!L_(610) O
!end �������:20FDA61AA133
!beg �������:20FDA61AA134
        SPW_0(148115)=.false.	!L_(608) O
!end �������:20FDA61AA134
!beg �������:20FDA61AA135
        SPW_0(148125)=.false.	!L_(618) O
!end �������:20FDA61AA135
!beg �������:20FDA61AA136
        SPW_0(148123)=.false.	!L_(616) O
!end �������:20FDA61AA136
!beg �������:20FDA61AA137
        SPW_0(148121)=.false.	!L_(614) O
!end �������:20FDA61AA137
!beg ������:20FDA60AE408
        SPW_0(149651)=.false.	!L_(2144) O
!end ������:20FDA60AE408
!beg �������:20FDA61AA138
        SPW_0(148119)=.false.	!L_(612) O
!end �������:20FDA61AA138
!beg �������:20FDA64AA160
        SPW_0(147909)=.false.	!L_(402) O
!end �������:20FDA64AA160
!beg �������:20FDA64AA161
        SPW_0(147915)=.false.	!L_(408) O
!end �������:20FDA64AA161
!beg �������:20FDA64AA162
        SPW_0(147913)=.false.	!L_(406) O
!end �������:20FDA64AA162
!beg �������:20FDA64AA163
        SPW_0(147911)=.false.	!L_(404) O
!end �������:20FDA64AA163
!beg �������:20FDA64AA164
        SPW_0(147867)=.false.	!L_(360) O
!end �������:20FDA64AA164
!beg �����:20FDA60AE408
        SPW_0(149652)=.false.	!L_(2145) O
!end �����:20FDA60AE408
!beg ������:20FDA60AE413
        SPW_0(149647)=.false.	!L_(2140) O
!end ������:20FDA60AE413
!beg �������:20FDA61AA143
        SPW_0(148053)=.false.	!L_(546) O
!end �������:20FDA61AA143
!beg �������:20FDA61AA144
        SPW_0(148029)=.false.	!L_(522) O
!end �������:20FDA61AA144
!beg �������:20FDA61AA145
        SPW_0(148051)=.false.	!L_(544) O
!end �������:20FDA61AA145
!beg �����:20FDA60AE413
        SPW_0(149648)=.false.	!L_(2141) O
!end �����:20FDA60AE413
!beg �������:20FDA61AA150
        SPW_0(148017)=.false.	!L_(510) O
!end �������:20FDA61AA150
!beg �������:20FDA61AA151
        SPW_0(148015)=.false.	!L_(508) O
!end �������:20FDA61AA151
!beg �������:20FDA61AA152
        SPW_0(148013)=.false.	!L_(506) O
!end �������:20FDA61AA152
!beg �������:20FDA61AA153
        SPW_0(147861)=.false.	!L_(354) O
!end �������:20FDA61AA153
!beg �������:20FDA61AA154
        SPW_0(147859)=.false.	!L_(352) O
!end �������:20FDA61AA154
!beg �������:20FDA61AA155
        SPW_0(148011)=.false.	!L_(504) O
!end �������:20FDA61AA155
!beg �������:20FDA61AA156
        SPW_0(148009)=.false.	!L_(502) O
!end �������:20FDA61AA156
!beg �������:20FDA61AA157
        SPW_0(147947)=.false.	!L_(440) O
!end �������:20FDA61AA157
!beg �������:20FDA61AA158
        SPW_0(147945)=.false.	!L_(438) O
!end �������:20FDA61AA158
!beg �������:20FDA61AA159
        SPW_0(147949)=.false.	!L_(442) O
!end �������:20FDA61AA159
!beg �������:20FDA61AA160
        SPW_0(147951)=.false.	!L_(444) O
!end �������:20FDA61AA160
!beg �������:20FDA61AA161
        SPW_0(147957)=.false.	!L_(450) O
!end �������:20FDA61AA161
!beg �������:20FDA61AA162
        SPW_0(147955)=.false.	!L_(448) O
!end �������:20FDA61AA162
!beg �������:20FDA61AA163
        SPW_0(147953)=.false.	!L_(446) O
!end �������:20FDA61AA163
!beg �������:20FDA61AA164
        SPW_0(147873)=.false.	!L_(366) O
!end �������:20FDA61AA164
!beg ��������:20FDA60AE408
        SPW_0(149653)=.false.	!L_(2146) O
!end ��������:20FDA60AE408
!beg ��������:20FDA60AE413
        SPW_0(149649)=.false.	!L_(2142) O
!end ��������:20FDA60AE413
!beg �������:20FDA64AA206
        SPW_0(147814)=.false.	!L_(307) O
!end �������:20FDA64AA206
!beg �������:20FDA64AA207
        SPW_0(147812)=.false.	!L_(305) O
!end �������:20FDA64AA207
!beg �������:20FDA64AA208
        SPW_0(147810)=.false.	!L_(303) O
!end �������:20FDA64AA208
!beg �������:20FDA64AA209
        SPW_0(147808)=.false.	!L_(301) O
!end �������:20FDA64AA209
!beg �������:20FDA61AA206
        SPW_0(147838)=.false.	!L_(331) O
!end �������:20FDA61AA206
!beg �������:20FDA61AA207
        SPW_0(147836)=.false.	!L_(329) O
!end �������:20FDA61AA207
!beg �������:20FDA61AA208
        SPW_0(147834)=.false.	!L_(327) O
!end �������:20FDA61AA208
!beg �������:20FDA61AA209
        SPW_0(147832)=.false.	!L_(325) O
!end �������:20FDA61AA209
!beg �������:20FDA66AA133
        SPW_0(148058)=.false.	!L_(551) O
!end �������:20FDA66AA133
!beg �������:20FDA66AA134
        SPW_0(148056)=.false.	!L_(549) O
!end �������:20FDA66AA134
!beg �������:20FDA66AA135
        SPW_0(148066)=.false.	!L_(559) O
!end �������:20FDA66AA135
!beg �������:20FDA66AA136
        SPW_0(148064)=.false.	!L_(557) O
!end �������:20FDA66AA136
!beg �������:20FDA66AA137
        SPW_0(148062)=.false.	!L_(555) O
!end �������:20FDA66AA137
!beg �������:20FDA66AA138
        SPW_0(148060)=.false.	!L_(553) O
!end �������:20FDA66AA138
!beg �������:20FDA66AA143
        SPW_0(148034)=.false.	!L_(527) O
!end �������:20FDA66AA143
!beg �������:20FDA66AA144
        SPW_0(148020)=.false.	!L_(513) O
!end �������:20FDA66AA144
!beg �������:20FDA66AA145
        SPW_0(148032)=.false.	!L_(525) O
!end �������:20FDA66AA145
!beg �������:20FDA60AA100
        SPW_0(148160)=.false.	!L_(653) O
!end �������:20FDA60AA100
!beg �������:20FDA60AA101
        SPW_0(148158)=.false.	!L_(651) O
!end �������:20FDA60AA101
!beg �������:20FDA60AA102
        SPW_0(148156)=.false.	!L_(649) O
!end �������:20FDA60AA102
!beg �������:20FDA60AA103
        SPW_0(148154)=.false.	!L_(647) O
!end �������:20FDA60AA103
!beg �������:20FDA66AA150
        SPW_0(147968)=.false.	!L_(461) O
!end �������:20FDA66AA150
!beg �������:20FDA66AA151
        SPW_0(147966)=.false.	!L_(459) O
!end �������:20FDA66AA151
!beg �������:20FDA66AA152
        SPW_0(147964)=.false.	!L_(457) O
!end �������:20FDA66AA152
!beg �������:20FDA66AA153
        SPW_0(147842)=.false.	!L_(335) O
!end �������:20FDA66AA153
!beg �������:20FDA66AA154
        SPW_0(147840)=.false.	!L_(333) O
!end �������:20FDA66AA154
!beg �������:20FDA64AA206
        SPW_0(147813)=.false.	!L_(306) O
!end �������:20FDA64AA206
!beg �������:20FDA60AA108
        SPW_0(148152)=.false.	!L_(645) O
!end �������:20FDA60AA108
!beg �������:20FDA66AA155
        SPW_0(147962)=.false.	!L_(455) O
!end �������:20FDA66AA155
!beg �������:20FDA64AA207
        SPW_0(147811)=.false.	!L_(304) O
!end �������:20FDA64AA207
!beg �������:20FDA60AA109
        SPW_0(148150)=.false.	!L_(643) O
!end �������:20FDA60AA109
!beg �������:20FDA66AA156
        SPW_0(147960)=.false.	!L_(453) O
!end �������:20FDA66AA156
!beg �������:20FDA64AA208
        SPW_0(147809)=.false.	!L_(302) O
!end �������:20FDA64AA208
!beg �������:20FDA66AA157
        SPW_0(147878)=.false.	!L_(371) O
!end �������:20FDA66AA157
!beg �������:20FDA64AA209
        SPW_0(147807)=.false.	!L_(300) O
!end �������:20FDA64AA209
!beg �������:20FDA66AA158
        SPW_0(147876)=.false.	!L_(369) O
!end �������:20FDA66AA158
!beg �������:20FDA66AA159
        SPW_0(147880)=.false.	!L_(373) O
!end �������:20FDA66AA159
!beg �������:20FDA63AA133
        SPW_0(148082)=.false.	!L_(575) O
!end �������:20FDA63AA133
!beg �������:20FDA63AA134
        SPW_0(148080)=.false.	!L_(573) O
!end �������:20FDA63AA134
!beg �������:20FDA63AA135
        SPW_0(148090)=.false.	!L_(583) O
!end �������:20FDA63AA135
!beg �������:20FDA63AA136
        SPW_0(148088)=.false.	!L_(581) O
!end �������:20FDA63AA136
!beg �������:20FDA60AA110
        SPW_0(148148)=.false.	!L_(641) O
!end �������:20FDA60AA110
!beg �������:20FDA63AA137
        SPW_0(148086)=.false.	!L_(579) O
!end �������:20FDA63AA137
!beg �������:20FDA60AA111
        SPW_0(148146)=.false.	!L_(639) O
!end �������:20FDA60AA111
!beg �������:20FDA63AA138
        SPW_0(148084)=.false.	!L_(577) O
!end �������:20FDA63AA138
!beg �������:20FDA66AA160
        SPW_0(147882)=.false.	!L_(375) O
!end �������:20FDA66AA160
!beg �������:20FDA66AA161
        SPW_0(147888)=.false.	!L_(381) O
!end �������:20FDA66AA161
!beg �������:20FDA66AA162
        SPW_0(147886)=.false.	!L_(379) O
!end �������:20FDA66AA162
!beg �������:20FDA60AA116
        SPW_0(148134)=.false.	!L_(627) O
!end �������:20FDA60AA116
!beg �������:20FDA66AA163
        SPW_0(147884)=.false.	!L_(377) O
!end �������:20FDA66AA163
!beg �������:20FDA66AA164
        SPW_0(147864)=.false.	!L_(357) O
!end �������:20FDA66AA164
!beg �������:20FDA60AA117
        SPW_0(147790)=.false.	!L_(283) O
!end �������:20FDA60AA117
!beg �������:20FDA60AA118
        SPW_0(148132)=.false.	!L_(625) O
!end �������:20FDA60AA118
!beg �������:20FDA63AA143
        SPW_0(148046)=.false.	!L_(539) O
!end �������:20FDA63AA143
!beg �������:20FDA63AA144
        SPW_0(148026)=.false.	!L_(519) O
!end �������:20FDA63AA144
!beg �������:20FDA63AA145
        SPW_0(148044)=.false.	!L_(537) O
!end �������:20FDA63AA145
!beg �������:20FDA60AA124
        SPW_0(148130)=.false.	!L_(623) O
!end �������:20FDA60AA124
!beg �������:20FDA60AA126
        SPW_0(148128)=.false.	!L_(621) O
!end �������:20FDA60AA126
!beg �������:20FDA63AA150
        SPW_0(147998)=.false.	!L_(491) O
!end �������:20FDA63AA150
!beg �������:20FDA63AA151
        SPW_0(147996)=.false.	!L_(489) O
!end �������:20FDA63AA151
!beg �������:20FDA63AA152
        SPW_0(147994)=.false.	!L_(487) O
!end �������:20FDA63AA152
!beg �������:20FDA63AA153
        SPW_0(147854)=.false.	!L_(347) O
!end �������:20FDA63AA153
!beg �������:20FDA63AA154
        SPW_0(147852)=.false.	!L_(345) O
!end �������:20FDA63AA154
!beg �������:20FDA61AA206
        SPW_0(147837)=.false.	!L_(330) O
!end �������:20FDA61AA206
!beg �������:20FDA63AA155
        SPW_0(147992)=.false.	!L_(485) O
!end �������:20FDA63AA155
!beg �������:20FDA61AA207
        SPW_0(147835)=.false.	!L_(328) O
!end �������:20FDA61AA207
!beg �������:20FDA63AA156
        SPW_0(147990)=.false.	!L_(483) O
!end �������:20FDA63AA156
!beg �������:20FDA61AA208
        SPW_0(147833)=.false.	!L_(326) O
!end �������:20FDA61AA208
!beg �������:20FDA63AA157
        SPW_0(147920)=.false.	!L_(413) O
!end �������:20FDA63AA157
!beg �������:20FDA61AA209
        SPW_0(147831)=.false.	!L_(324) O
!end �������:20FDA61AA209
!beg �������:20FDA63AA158
        SPW_0(147918)=.false.	!L_(411) O
!end �������:20FDA63AA158
!beg �������:20FDA60AA132
        SPW_0(148140)=.false.	!L_(633) O
!end �������:20FDA60AA132
!beg �������:20FDA63AA159
        SPW_0(147922)=.false.	!L_(415) O
!end �������:20FDA63AA159
!beg �������:20FDA60AA133
        SPW_0(148138)=.false.	!L_(631) O
!end �������:20FDA60AA133
!beg �������:20FDA60AA134
        SPW_0(148144)=.false.	!L_(637) O
!end �������:20FDA60AA134
!beg �������:20FDA60AA135
        SPW_0(148142)=.false.	!L_(635) O
!end �������:20FDA60AA135
!beg �������:20FDA60AA136
        SPW_0(148136)=.false.	!L_(629) O
!end �������:20FDA60AA136
!beg �������:20FDA63AA160
        SPW_0(147924)=.false.	!L_(417) O
!end �������:20FDA63AA160
!beg �������:20FDA63AA161
        SPW_0(147930)=.false.	!L_(423) O
!end �������:20FDA63AA161
!beg �������:20FDA63AA162
        SPW_0(147928)=.false.	!L_(421) O
!end �������:20FDA63AA162
!beg �������:20FDA63AA163
        SPW_0(147926)=.false.	!L_(419) O
!end �������:20FDA63AA163
!beg �������:20FDA63AA164
        SPW_0(147870)=.false.	!L_(363) O
!end �������:20FDA63AA164
!beg �������:20FDA66AA133
        SPW_0(148057)=.false.	!L_(550) O
!end �������:20FDA66AA133
!beg �������:20FDA66AA134
        SPW_0(148055)=.false.	!L_(548) O
!end �������:20FDA66AA134
!beg �������:20FDA66AA135
        SPW_0(148065)=.false.	!L_(558) O
!end �������:20FDA66AA135
!beg �������:20FDA66AA136
        SPW_0(148063)=.false.	!L_(556) O
!end �������:20FDA66AA136
!beg �������:20FDA66AA137
        SPW_0(148061)=.false.	!L_(554) O
!end �������:20FDA66AA137
!beg �������:20FDA66AA138
        SPW_0(148059)=.false.	!L_(552) O
!end �������:20FDA66AA138
!beg �������:20FDA66AA143
        SPW_0(148033)=.false.	!L_(526) O
!end �������:20FDA66AA143
!beg �������:20FDA66AA144
        SPW_0(148019)=.false.	!L_(512) O
!end �������:20FDA66AA144
!beg �������:20FDA66AA145
        SPW_0(148031)=.false.	!L_(524) O
!end �������:20FDA66AA145
!beg �������:20FDA60AA100
        SPW_0(148159)=.false.	!L_(652) O
!end �������:20FDA60AA100
!beg �������:20FDA60AA101
        SPW_0(148157)=.false.	!L_(650) O
!end �������:20FDA60AA101
!beg �������:20FDA60AA102
        SPW_0(148155)=.false.	!L_(648) O
!end �������:20FDA60AA102
!beg �������:20FDA60AA103
        SPW_0(148153)=.false.	!L_(646) O
!end �������:20FDA60AA103
!beg �������:20FDA66AA150
        SPW_0(147967)=.false.	!L_(460) O
!end �������:20FDA66AA150
!beg �������:20FDA66AA151
        SPW_0(147965)=.false.	!L_(458) O
!end �������:20FDA66AA151
!beg �������:20FDA66AA152
        SPW_0(147963)=.false.	!L_(456) O
!end �������:20FDA66AA152
!beg �������:20FDA66AA153
        SPW_0(147841)=.false.	!L_(334) O
!end �������:20FDA66AA153
!beg �������:20FDA66AA154
        SPW_0(147839)=.false.	!L_(332) O
!end �������:20FDA66AA154
!beg �������:20FDA60AA108
        SPW_0(148151)=.false.	!L_(644) O
!end �������:20FDA60AA108
!beg �������:20FDA66AA155
        SPW_0(147961)=.false.	!L_(454) O
!end �������:20FDA66AA155
!beg �������:20FDA60AA109
        SPW_0(148149)=.false.	!L_(642) O
!end �������:20FDA60AA109
!beg �������:20FDA66AA156
        SPW_0(147959)=.false.	!L_(452) O
!end �������:20FDA66AA156
!beg �������:20FDA66AA157
        SPW_0(147877)=.false.	!L_(370) O
!end �������:20FDA66AA157
!beg �������:20FDA66AA158
        SPW_0(147875)=.false.	!L_(368) O
!end �������:20FDA66AA158
!beg �������:20FDA66AA159
        SPW_0(147879)=.false.	!L_(372) O
!end �������:20FDA66AA159
!beg �������:20FDA63AA133
        SPW_0(148081)=.false.	!L_(574) O
!end �������:20FDA63AA133
!beg �������:20FDA63AA134
        SPW_0(148079)=.false.	!L_(572) O
!end �������:20FDA63AA134
!beg �������:20FDA63AA135
        SPW_0(148089)=.false.	!L_(582) O
!end �������:20FDA63AA135
!beg �������:20FDA63AA136
        SPW_0(148087)=.false.	!L_(580) O
!end �������:20FDA63AA136
!beg �������:20FDA60AA110
        SPW_0(148147)=.false.	!L_(640) O
!end �������:20FDA60AA110
!beg �������:20FDA63AA137
        SPW_0(148085)=.false.	!L_(578) O
!end �������:20FDA63AA137
!beg �������:20FDA60AA111
        SPW_0(148145)=.false.	!L_(638) O
!end �������:20FDA60AA111
!beg �������:20FDA63AA138
        SPW_0(148083)=.false.	!L_(576) O
!end �������:20FDA63AA138
!beg �������:20FDA66AA160
        SPW_0(147881)=.false.	!L_(374) O
!end �������:20FDA66AA160
!beg �������:20FDA66AA161
        SPW_0(147887)=.false.	!L_(380) O
!end �������:20FDA66AA161
!beg �������:20FDA66AA162
        SPW_0(147885)=.false.	!L_(378) O
!end �������:20FDA66AA162
!beg �������:20FDA60AA116
        SPW_0(148133)=.false.	!L_(626) O
!end �������:20FDA60AA116
!beg �������:20FDA66AA163
        SPW_0(147883)=.false.	!L_(376) O
!end �������:20FDA66AA163
!beg �������:20FDA66AA164
        SPW_0(147863)=.false.	!L_(356) O
!end �������:20FDA66AA164
!beg �������:20FDA60AA117
        SPW_0(147789)=.false.	!L_(282) O
!end �������:20FDA60AA117
!beg �������:20FDA60AA118
        SPW_0(148131)=.false.	!L_(624) O
!end �������:20FDA60AA118
!beg �������:20FDA63AA143
        SPW_0(148045)=.false.	!L_(538) O
!end �������:20FDA63AA143
!beg �������:20FDA63AA144
        SPW_0(148025)=.false.	!L_(518) O
!end �������:20FDA63AA144
!beg �������:20FDA63AA145
        SPW_0(148043)=.false.	!L_(536) O
!end �������:20FDA63AA145
!beg �������:20FDA60AA124
        SPW_0(148129)=.false.	!L_(622) O
!end �������:20FDA60AA124
!beg �������:20FDA60AA126
        SPW_0(148127)=.false.	!L_(620) O
!end �������:20FDA60AA126
!beg �������:20FDA63AA150
        SPW_0(147997)=.false.	!L_(490) O
!end �������:20FDA63AA150
!beg �������:20FDA63AA151
        SPW_0(147995)=.false.	!L_(488) O
!end �������:20FDA63AA151
!beg �������:20FDA63AA152
        SPW_0(147993)=.false.	!L_(486) O
!end �������:20FDA63AA152
!beg �������:20FDA63AA153
        SPW_0(147853)=.false.	!L_(346) O
!end �������:20FDA63AA153
!beg �������:20FDA63AA154
        SPW_0(147851)=.false.	!L_(344) O
!end �������:20FDA63AA154
!beg �������:20FDA63AA155
        SPW_0(147991)=.false.	!L_(484) O
!end �������:20FDA63AA155
!beg �������:20FDA63AA156
        SPW_0(147989)=.false.	!L_(482) O
!end �������:20FDA63AA156
!beg �������:20FDA63AA157
        SPW_0(147919)=.false.	!L_(412) O
!end �������:20FDA63AA157
!beg �������:20FDA63AA158
        SPW_0(147917)=.false.	!L_(410) O
!end �������:20FDA63AA158
!beg �������:20FDA60AA132
        SPW_0(148139)=.false.	!L_(632) O
!end �������:20FDA60AA132
!beg �������:20FDA63AA159
        SPW_0(147921)=.false.	!L_(414) O
!end �������:20FDA63AA159
!beg �������:20FDA60AA133
        SPW_0(148137)=.false.	!L_(630) O
!end �������:20FDA60AA133
!beg �������:20FDA60AA134
        SPW_0(148143)=.false.	!L_(636) O
!end �������:20FDA60AA134
!beg �������:20FDA60AA135
        SPW_0(148141)=.false.	!L_(634) O
!end �������:20FDA60AA135
!beg �������:20FDA60AA136
        SPW_0(148135)=.false.	!L_(628) O
!end �������:20FDA60AA136
!beg �������:20FDA63AA160
        SPW_0(147923)=.false.	!L_(416) O
!end �������:20FDA63AA160
!beg �������:20FDA63AA161
        SPW_0(147929)=.false.	!L_(422) O
!end �������:20FDA63AA161
!beg �������:20FDA63AA162
        SPW_0(147927)=.false.	!L_(420) O
!end �������:20FDA63AA162
!beg �������:20FDA63AA163
        SPW_0(147925)=.false.	!L_(418) O
!end �������:20FDA63AA163
!beg �������:20FDA63AA164
        SPW_0(147869)=.false.	!L_(362) O
!end �������:20FDA63AA164
!beg �������:20FDA66AA206
        SPW_0(147798)=.false.	!L_(291) O
!end �������:20FDA66AA206
!beg �������:20FDA66AA207
        SPW_0(147796)=.false.	!L_(289) O
!end �������:20FDA66AA207
!beg �������:20FDA66AA208
        SPW_0(147794)=.false.	!L_(287) O
!end �������:20FDA66AA208
!beg �������:20FDA66AA209
        SPW_0(147792)=.false.	!L_(285) O
!end �������:20FDA66AA209
!beg �������:20FDA63AA206
        SPW_0(147822)=.false.	!L_(315) O
!end �������:20FDA63AA206
!beg �������:20FDA63AA207
        SPW_0(147820)=.false.	!L_(313) O
!end �������:20FDA63AA207
!beg �������:20FDA63AA208
        SPW_0(147818)=.false.	!L_(311) O
!end �������:20FDA63AA208
!beg �������:20FDA63AA209
        SPW_0(147816)=.false.	!L_(309) O
!end �������:20FDA63AA209
!beg �������:20FDA66AA206
        SPW_0(147797)=.false.	!L_(290) O
!end �������:20FDA66AA206
!beg �������:20FDA66AA207
        SPW_0(147795)=.false.	!L_(288) O
!end �������:20FDA66AA207
!beg �������:20FDA66AA208
        SPW_0(147793)=.false.	!L_(286) O
!end �������:20FDA66AA208
!beg �������:20FDA66AA209
        SPW_0(147791)=.false.	!L_(284) O
!end �������:20FDA66AA209
!beg �������:20FDA65AA133
        SPW_0(148070)=.false.	!L_(563) O
!end �������:20FDA65AA133
!beg �������:20FDA65AA134
        SPW_0(148068)=.false.	!L_(561) O
!end �������:20FDA65AA134
!beg �������:20FDA65AA135
        SPW_0(148078)=.false.	!L_(571) O
!end �������:20FDA65AA135
!beg �������:20FDA65AA136
        SPW_0(148076)=.false.	!L_(569) O
!end �������:20FDA65AA136
!beg �������:20FDA65AA137
        SPW_0(148074)=.false.	!L_(567) O
!end �������:20FDA65AA137
!beg �������:20FDA65AA138
        SPW_0(148072)=.false.	!L_(565) O
!end �������:20FDA65AA138
!beg �������:20FDA65AA143
        SPW_0(148038)=.false.	!L_(531) O
!end �������:20FDA65AA143
!beg �������:20FDA65AA144
        SPW_0(148022)=.false.	!L_(515) O
!end �������:20FDA65AA144
!beg �������:20FDA65AA145
        SPW_0(148036)=.false.	!L_(529) O
!end �������:20FDA65AA145
!beg �������:20FDA65AA150
        SPW_0(147978)=.false.	!L_(471) O
!end �������:20FDA65AA150
!beg �������:20FDA65AA151
        SPW_0(147976)=.false.	!L_(469) O
!end �������:20FDA65AA151
!beg �������:20FDA65AA152
        SPW_0(147974)=.false.	!L_(467) O
!end �������:20FDA65AA152
!beg �������:20FDA65AA153
        SPW_0(147846)=.false.	!L_(339) O
!end �������:20FDA65AA153
!beg �������:20FDA65AA154
        SPW_0(147844)=.false.	!L_(337) O
!end �������:20FDA65AA154
!beg �������:20FDA63AA206
        SPW_0(147821)=.false.	!L_(314) O
!end �������:20FDA63AA206
!beg �������:20FDA65AA155
        SPW_0(147972)=.false.	!L_(465) O
!end �������:20FDA65AA155
!beg �������:20FDA63AA207
        SPW_0(147819)=.false.	!L_(312) O
!end �������:20FDA63AA207
!beg �������:20FDA65AA156
        SPW_0(147970)=.false.	!L_(463) O
!end �������:20FDA65AA156
!beg �������:20FDA63AA208
        SPW_0(147817)=.false.	!L_(310) O
!end �������:20FDA63AA208
!beg �������:20FDA65AA157
        SPW_0(147892)=.false.	!L_(385) O
!end �������:20FDA65AA157
!beg �������:20FDA63AA209
        SPW_0(147815)=.false.	!L_(308) O
!end �������:20FDA63AA209
!beg �������:20FDA65AA158
        SPW_0(147890)=.false.	!L_(383) O
!end �������:20FDA65AA158
!beg �������:20FDA65AA159
        SPW_0(147894)=.false.	!L_(387) O
!end �������:20FDA65AA159
!beg �������:20FDA62AA133
        SPW_0(148106)=.false.	!L_(599) O
!end �������:20FDA62AA133
!beg �������:20FDA62AA134
        SPW_0(148104)=.false.	!L_(597) O
!end �������:20FDA62AA134
!beg �������:20FDA62AA135
        SPW_0(148114)=.false.	!L_(607) O
!end �������:20FDA62AA135
!beg �������:20FDA62AA136
        SPW_0(148112)=.false.	!L_(605) O
!end �������:20FDA62AA136
!beg �������:20FDA62AA137
        SPW_0(148110)=.false.	!L_(603) O
!end �������:20FDA62AA137
!beg �������:20FDA62AA138
        SPW_0(148108)=.false.	!L_(601) O
!end �������:20FDA62AA138
!beg �������:20FDA65AA160
        SPW_0(147896)=.false.	!L_(389) O
!end �������:20FDA65AA160
!beg �������:20FDA65AA161
        SPW_0(147902)=.false.	!L_(395) O
!end �������:20FDA65AA161
!beg �������:20FDA65AA162
        SPW_0(147900)=.false.	!L_(393) O
!end �������:20FDA65AA162
!beg �������:20FDA65AA163
        SPW_0(147898)=.false.	!L_(391) O
!end �������:20FDA65AA163
!beg �������:20FDA65AA164
        SPW_0(147866)=.false.	!L_(359) O
!end �������:20FDA65AA164
!beg �������:20FDA62AA143
        SPW_0(148050)=.false.	!L_(543) O
!end �������:20FDA62AA143
!beg �������:20FDA62AA144
        SPW_0(148028)=.false.	!L_(521) O
!end �������:20FDA62AA144
!beg �������:20FDA62AA145
        SPW_0(148048)=.false.	!L_(541) O
!end �������:20FDA62AA145
!beg �������:20FDA62AA150
        SPW_0(148008)=.false.	!L_(501) O
!end �������:20FDA62AA150
!beg �������:20FDA62AA151
        SPW_0(148006)=.false.	!L_(499) O
!end �������:20FDA62AA151
!beg �������:20FDA62AA152
        SPW_0(148004)=.false.	!L_(497) O
!end �������:20FDA62AA152
!beg �������:20FDA62AA153
        SPW_0(147858)=.false.	!L_(351) O
!end �������:20FDA62AA153
!beg �������:20FDA62AA154
        SPW_0(147856)=.false.	!L_(349) O
!end �������:20FDA62AA154
!beg �������:20FDA62AA155
        SPW_0(148002)=.false.	!L_(495) O
!end �������:20FDA62AA155
!beg �������:20FDA62AA156
        SPW_0(148000)=.false.	!L_(493) O
!end �������:20FDA62AA156
!beg �������:20FDA62AA157
        SPW_0(147934)=.false.	!L_(427) O
!end �������:20FDA62AA157
!beg �������:20FDA62AA158
        SPW_0(147932)=.false.	!L_(425) O
!end �������:20FDA62AA158
!beg �������:20FDA62AA159
        SPW_0(147936)=.false.	!L_(429) O
!end �������:20FDA62AA159
!beg �������:20FDA62AA160
        SPW_0(147938)=.false.	!L_(431) O
!end �������:20FDA62AA160
!beg �������:20FDA62AA161
        SPW_0(147944)=.false.	!L_(437) O
!end �������:20FDA62AA161
!beg �������:20FDA62AA162
        SPW_0(147942)=.false.	!L_(435) O
!end �������:20FDA62AA162
!beg �������:20FDA62AA163
        SPW_0(147940)=.false.	!L_(433) O
!end �������:20FDA62AA163
!beg �������:20FDA62AA164
        SPW_0(147872)=.false.	!L_(365) O
!end �������:20FDA62AA164
!beg �������:20FDA65AA133
        SPW_0(148069)=.false.	!L_(562) O
!end �������:20FDA65AA133
!beg �������:20FDA65AA134
        SPW_0(148067)=.false.	!L_(560) O
!end �������:20FDA65AA134
!beg �������:20FDA65AA135
        SPW_0(148077)=.false.	!L_(570) O
!end �������:20FDA65AA135
!beg �������:20FDA65AA136
        SPW_0(148075)=.false.	!L_(568) O
!end �������:20FDA65AA136
!beg �������:20FDA65AA137
        SPW_0(148073)=.false.	!L_(566) O
!end �������:20FDA65AA137
!beg �������:20FDA65AA138
        SPW_0(148071)=.false.	!L_(564) O
!end �������:20FDA65AA138
!beg �������:20FDA65AA143
        SPW_0(148037)=.false.	!L_(530) O
!end �������:20FDA65AA143
!beg �������:20FDA65AA144
        SPW_0(148021)=.false.	!L_(514) O
!end �������:20FDA65AA144
!beg �������:20FDA65AA145
        SPW_0(148035)=.false.	!L_(528) O
!end �������:20FDA65AA145
!beg �������:20FDA65AA150
        SPW_0(147977)=.false.	!L_(470) O
!end �������:20FDA65AA150
!beg �������:20FDA65AA151
        SPW_0(147975)=.false.	!L_(468) O
!end �������:20FDA65AA151
!beg �������:20FDA65AA152
        SPW_0(147973)=.false.	!L_(466) O
!end �������:20FDA65AA152
!beg �������:20FDA65AA153
        SPW_0(147845)=.false.	!L_(338) O
!end �������:20FDA65AA153
!beg �������:20FDA65AA154
        SPW_0(147843)=.false.	!L_(336) O
!end �������:20FDA65AA154
!beg �������:20FDA65AA155
        SPW_0(147971)=.false.	!L_(464) O
!end �������:20FDA65AA155
!beg �������:20FDA65AA156
        SPW_0(147969)=.false.	!L_(462) O
!end �������:20FDA65AA156
!beg �������:20FDA65AA157
        SPW_0(147891)=.false.	!L_(384) O
!end �������:20FDA65AA157
!beg �������:20FDA65AA158
        SPW_0(147889)=.false.	!L_(382) O
!end �������:20FDA65AA158
!beg �������:20FDA65AA159
        SPW_0(147893)=.false.	!L_(386) O
!end �������:20FDA65AA159
!beg �������:20FDA62AA133
        SPW_0(148105)=.false.	!L_(598) O
!end �������:20FDA62AA133
!beg �������:20FDA62AA134
        SPW_0(148103)=.false.	!L_(596) O
!end �������:20FDA62AA134
!beg �������:20FDA62AA135
        SPW_0(148113)=.false.	!L_(606) O
!end �������:20FDA62AA135
!beg �������:20FDA62AA136
        SPW_0(148111)=.false.	!L_(604) O
!end �������:20FDA62AA136
!beg �������:20FDA62AA137
        SPW_0(148109)=.false.	!L_(602) O
!end �������:20FDA62AA137
!beg �������:20FDA62AA138
        SPW_0(148107)=.false.	!L_(600) O
!end �������:20FDA62AA138
!beg �������:20FDA65AA160
        SPW_0(147895)=.false.	!L_(388) O
!end �������:20FDA65AA160
!beg �������:20FDA65AA161
        SPW_0(147901)=.false.	!L_(394) O
!end �������:20FDA65AA161
!beg �������:20FDA65AA162
        SPW_0(147899)=.false.	!L_(392) O
!end �������:20FDA65AA162
!beg �������:20FDA65AA163
        SPW_0(147897)=.false.	!L_(390) O
!end �������:20FDA65AA163
!beg �������:20FDA65AA164
        SPW_0(147865)=.false.	!L_(358) O
!end �������:20FDA65AA164
!beg ����:20FDA60AE408
        SPW_0(149650)=.false.	!L_(2143) O
!end ����:20FDA60AE408
!beg �������:20FDA62AA143
        SPW_0(148049)=.false.	!L_(542) O
!end �������:20FDA62AA143
!beg �������:20FDA62AA144
        SPW_0(148027)=.false.	!L_(520) O
!end �������:20FDA62AA144
!beg �������:20FDA62AA145
        SPW_0(148047)=.false.	!L_(540) O
!end �������:20FDA62AA145
!beg ����:20FDA60AE413
        SPW_0(149646)=.false.	!L_(2139) O
!end ����:20FDA60AE413
!beg �������:20FDA62AA150
        SPW_0(148007)=.false.	!L_(500) O
!end �������:20FDA62AA150
!beg �������:20FDA62AA151
        SPW_0(148005)=.false.	!L_(498) O
!end �������:20FDA62AA151
!beg �������:20FDA62AA152
        SPW_0(148003)=.false.	!L_(496) O
!end �������:20FDA62AA152
!beg �������:20FDA62AA153
        SPW_0(147857)=.false.	!L_(350) O
!end �������:20FDA62AA153
!beg �������:20FDA62AA154
        SPW_0(147855)=.false.	!L_(348) O
!end �������:20FDA62AA154
!beg �������:20FDA62AA155
        SPW_0(148001)=.false.	!L_(494) O
!end �������:20FDA62AA155
!beg �������:20FDA62AA156
        SPW_0(147999)=.false.	!L_(492) O
!end �������:20FDA62AA156
!beg �������:20FDA62AA157
        SPW_0(147933)=.false.	!L_(426) O
!end �������:20FDA62AA157
!beg �������:20FDA62AA158
        SPW_0(147931)=.false.	!L_(424) O
!end �������:20FDA62AA158
!beg �������:20FDA62AA159
        SPW_0(147935)=.false.	!L_(428) O
!end �������:20FDA62AA159
!beg �������:20FDA62AA160
        SPW_0(147937)=.false.	!L_(430) O
!end �������:20FDA62AA160
!beg �������:20FDA62AA161
        SPW_0(147943)=.false.	!L_(436) O
!end �������:20FDA62AA161
!beg �������:20FDA62AA162
        SPW_0(147941)=.false.	!L_(434) O
!end �������:20FDA62AA162
!beg �������:20FDA62AA163
        SPW_0(147939)=.false.	!L_(432) O
!end �������:20FDA62AA163
!beg �������:20FDA62AA164
        SPW_0(147871)=.false.	!L_(364) O
!end �������:20FDA62AA164
!beg �������:20FDA65AA206
        SPW_0(147806)=.false.	!L_(299) O
!end �������:20FDA65AA206
!beg �������:20FDA65AA207
        SPW_0(147804)=.false.	!L_(297) O
!end �������:20FDA65AA207
!beg �������:20FDA65AA208
        SPW_0(147802)=.false.	!L_(295) O
!end �������:20FDA65AA208
!beg �������:20FDA65AA209
        SPW_0(147800)=.false.	!L_(293) O
!end �������:20FDA65AA209
!beg �������:20FDA62AA206
        SPW_0(147830)=.false.	!L_(323) O
!end �������:20FDA62AA206
!beg �������:20FDA62AA207
        SPW_0(147828)=.false.	!L_(321) O
!end �������:20FDA62AA207
!beg �������:20FDA62AA208
        SPW_0(147826)=.false.	!L_(319) O
!end �������:20FDA62AA208
!beg �������:20FDA62AA209
        SPW_0(147824)=.false.	!L_(317) O
!end �������:20FDA62AA209
       end

       subroutine FDA70_ConInQ
       end

       subroutine FDA70_ConIn
       LOGICAL*1 SPW_0(0:163839)
       Common/SPW_0/ SPW_0 !
       integer*4 SPW_0_4(0:40959)
       equivalence(SPW_0,SPW_0_4)
       LOGICAL*1 SPW_2(0:20479)
       Common/SPW_2/ SPW_2 !
       integer*4 SPW_2_4(0:5119)
       equivalence(SPW_2,SPW_2_4)
       LOGICAL*1 SPW_4(0:16383)
       Common/SPW_4/ SPW_4 !
       integer*4 SPW_4_4(0:4095)
       equivalence(SPW_4,SPW_4_4)
       LOGICAL*1 SPW_6(0:28671)
       Common/SPW_6/ SPW_6 !
       integer*4 SPW_6_4(0:7167)
       equivalence(SPW_6,SPW_6_4)
        Call FDA70_ConInQ
!beg ���������3:20FDA73AE003
        SPW_4(16062)=.false.	!L_(59) O
!end ���������3:20FDA73AE003
!beg ���������8:20FDA71AE801
        SPW_4(16042)=.false.	!L_(39) O
!end ���������8:20FDA71AE801
!beg ����:20FDA71AE202
        SPW_4(16025)=.false.	!L_(22) O
!end ����:20FDA71AE202
!beg ��1-����:20FDA71AE202
        SPW_4(16036)=.false.	!L_(33) O
!end ��1-����:20FDA71AE202
!beg ���������:20FDA71CU001KN01
        SPW_4(16089)=.false.	!L_(86) O
!end ���������:20FDA71CU001KN01
!beg ���������8:20FDA73AE003
        SPW_4(16057)=.false.	!L_(54) O
!end ���������8:20FDA73AE003
!beg ���������2:20FDA71AE202
        SPW_4(16033)=.false.	!L_(30) O
!end ���������2:20FDA71AE202
!beg ���������5:20FDA74AE001
        SPW_4(16075)=.false.	!L_(72) O
!end ���������5:20FDA74AE001
!beg ���������7:20FDA71AE202
        SPW_4(16028)=.false.	!L_(25) O
!end ���������7:20FDA71AE202
!beg ���������:20FDA71AE801
        SPW_4(16037)=.false.	!L_(34) O
!end ���������:20FDA71AE801
!beg ���������4:20FDA71AE801
        SPW_4(16046)=.false.	!L_(43) O
!end ���������4:20FDA71AE801
!beg ���������-�����:20FDA73AE003
        SPW_4(16054)=.false.	!L_(51) O
!end ���������-�����:20FDA73AE003
!beg ���������:20FDA73AE003
        SPW_4(16052)=.false.	!L_(49) O
!end ���������:20FDA73AE003
!beg ���������10:20FDA74AE001
        SPW_4(16080)=.false.	!L_(77) O
!end ���������10:20FDA74AE001
!beg ���������4:20FDA73AE003
        SPW_4(16061)=.false.	!L_(58) O
!end ���������4:20FDA73AE003
!beg ���������9:20FDA71AE801
        SPW_4(16041)=.false.	!L_(38) O
!end ���������9:20FDA71AE801
!beg ������:20FDA71AE801
        SPW_4(16038)=.false.	!L_(35) O
!end ������:20FDA71AE801
!beg ��������:20FDA71AM001
        SPW_4(16020)=.false.	!L_(17) O
!end ��������:20FDA71AM001
!beg ���������1:20FDA74AE001
        SPW_4(16079)=.false.	!L_(76) O
!end ���������1:20FDA74AE001
!beg ���������9:20FDA73AE003
        SPW_4(16056)=.false.	!L_(53) O
!end ���������9:20FDA73AE003
!beg ���������3:20FDA71AE202
        SPW_4(16032)=.false.	!L_(29) O
!end ���������3:20FDA71AE202
!beg ������:20FDA73AE003
        SPW_4(16053)=.false.	!L_(50) O
!end ������:20FDA73AE003
!beg ���������6:20FDA74AE001
        SPW_4(16074)=.false.	!L_(71) O
!end ���������6:20FDA74AE001
!beg ���������8:20FDA71AE202
        SPW_4(16027)=.false.	!L_(24) O
!end ���������8:20FDA71AE202
!beg ���� � ��������������:20FDA71AE801
        SPW_4(16048)=.false.	!L_(45) O
!end ���� � ��������������:20FDA71AE801
!beg �������:20FDA71AE701KE01
        SPW_4(16083)=.false.	!L_(80) O
!end �������:20FDA71AE701KE01
!beg �������:20FDA71AE701KE02
        SPW_4(16017)=.false.	!L_(14) O
!end �������:20FDA71AE701KE02
!beg ����-������:20FDA71AE801
        SPW_4(16051)=.false.	!L_(48) O
!end ����-������:20FDA71AE801
!beg ���������5:20FDA71AE801
        SPW_4(16045)=.false.	!L_(42) O
!end ���������5:20FDA71AE801
!beg �������:20FDA71AE701KE01
        SPW_4(16082)=.false.	!L_(79) O
!end �������:20FDA71AE701KE01
!beg �������:20FDA71AE701KE02
        SPW_4(16016)=.false.	!L_(13) O
!end �������:20FDA71AE701KE02
!beg ���������5:20FDA73AE003
        SPW_4(16060)=.false.	!L_(57) O
!end ���������5:20FDA73AE003
!beg ���������-�����������:20FDA73AE003
        SPW_4(16064)=.false.	!L_(61) O
!end ���������-�����������:20FDA73AE003
!beg ����:20FDA74AE001
        SPW_4(16070)=.false.	!L_(67) O
!end ����:20FDA74AE001
!beg ���������2:20FDA74AE001
        SPW_4(16078)=.false.	!L_(75) O
!end ���������2:20FDA74AE001
!beg ���������:20FDA71AJ002
        SPW_4(16019)=.false.	!L_(16) O
!end ���������:20FDA71AJ002
!beg ���������:20FDA71AE202
        SPW_4(16022)=.false.	!L_(19) O
!end ���������:20FDA71AE202
!beg ���������4:20FDA71AE202
        SPW_4(16031)=.false.	!L_(28) O
!end ���������4:20FDA71AE202
!beg ���������7:20FDA74AE001
        SPW_4(16073)=.false.	!L_(70) O
!end ���������7:20FDA74AE001
!beg ���������10:20FDA71AE801
        SPW_4(16050)=.false.	!L_(47) O
!end ���������10:20FDA71AE801
!beg ���������9:20FDA71AE202
        SPW_4(16026)=.false.	!L_(23) O
!end ���������9:20FDA71AE202
!beg ������:20FDA71AE202
        SPW_4(16023)=SPW_0(154505)	!L_(20) O L_(414)
!end ������:20FDA71AE202
!beg ���������10:20FDA73AE003
        SPW_4(16065)=.false.	!L_(62) O
!end ���������10:20FDA73AE003
!beg ��1-�����:20FDA71AE202
        SPW_4(16024)=.false.	!L_(21) O
!end ��1-�����:20FDA71AE202
!beg ���������6:20FDA71AE801
        SPW_4(16044)=.false.	!L_(41) O
!end ���������6:20FDA71AE801
!beg ����-�����:20FDA71AE801
        SPW_4(16039)=.false.	!L_(36) O
!end ����-�����:20FDA71AE801
!beg ��������:20FDA72CU001KN01
        SPW_4(16086)=.false.	!L_(83) O
!end ��������:20FDA72CU001KN01
!beg ���������6:20FDA73AE003
        SPW_4(16059)=.false.	!L_(56) O
!end ���������6:20FDA73AE003
!beg ���� ��� �����������:20FDA71AE801
        SPW_4(16049)=.false.	!L_(46) O
!end ���� ��� �����������:20FDA71AE801
!beg ���������3:20FDA74AE001
        SPW_4(16077)=.false.	!L_(74) O
!end ���������3:20FDA74AE001
!beg ���������5:20FDA71AE202
        SPW_4(16030)=.false.	!L_(27) O
!end ���������5:20FDA71AE202
!beg ��1-�����-���:20FDA71AE202
        SPW_4(16034)=.false.	!L_(31) O
!end ��1-�����-���:20FDA71AE202
!beg ���������8:20FDA74AE001
        SPW_4(16072)=.false.	!L_(69) O
!end ���������8:20FDA74AE001
!beg ���������:20FDA72CU001KN01
        SPW_4(16087)=.false.	!L_(84) O
!end ���������:20FDA72CU001KN01
!beg ����:20FDA71AE801
        SPW_4(16040)=.false.	!L_(37) O
!end ����:20FDA71AE801
!beg ������:20FDA74AE001
        SPW_4(16069)=.false.	!L_(66) O
!end ������:20FDA74AE001
!beg ����:20FDA73AE003
        SPW_4(16055)=.false.	!L_(52) O
!end ����:20FDA73AE003
!beg �����:20FDA74AE001
        SPW_4(16081)=.false.	!L_(78) O
!end �����:20FDA74AE001
!beg ���������2:20FDA73AE003
        SPW_4(16063)=.false.	!L_(60) O
!end ���������2:20FDA73AE003
!beg ��������:20FDA71AJ002
        SPW_4(16018)=.false.	!L_(15) O
!end ��������:20FDA71AJ002
!beg ���������7:20FDA71AE801
        SPW_4(16043)=.false.	!L_(40) O
!end ���������7:20FDA71AE801
!beg ���������10:20FDA71AE202
        SPW_4(16035)=.false.	!L_(32) O
!end ���������10:20FDA71AE202
!beg ���������-����:20FDA73AE003
        SPW_4(16066)=.false.	!L_(63) O
!end ���������-����:20FDA73AE003
!beg ���������7:20FDA73AE003
        SPW_4(16058)=.false.	!L_(55) O
!end ���������7:20FDA73AE003
!beg ���������:20FDA74AE001
        SPW_4(16067)=.false.	!L_(64) O
!end ���������:20FDA74AE001
!beg �������:20FDA71AA001
        SPW_4(16090)=.false.	!L_(87) O
!end �������:20FDA71AA001
!beg �������:20FDA73AE403KA01
        SPW_4(16085)=.false.	!L_(82) O
!end �������:20FDA73AE403KA01
!beg ���������4:20FDA74AE001
        SPW_4(16076)=.false.	!L_(73) O
!end ���������4:20FDA74AE001
!beg ���������6:20FDA71AE202
        SPW_4(16029)=.false.	!L_(26) O
!end ���������6:20FDA71AE202
!beg ���������:20FDA71AM001
        SPW_4(16021)=.false.	!L_(18) O
!end ���������:20FDA71AM001
!beg ���������9:20FDA74AE001
        SPW_4(16071)=.false.	!L_(68) O
!end ���������9:20FDA74AE001
!beg ��������:20FDA71CU001KN01
        SPW_4(16088)=.false.	!L_(85) O
!end ��������:20FDA71CU001KN01
!beg ������:20FDA74AE001
        SPW_4(16068)=.false.	!L_(65) O
!end ������:20FDA74AE001
!beg �������:20FDA73AE403KA01
        SPW_4(16084)=.false.	!L_(81) O
!end �������:20FDA73AE403KA01
!beg ���������3:20FDA71AE801
        SPW_4(16047)=.false.	!L_(44) O
!end ���������3:20FDA71AE801
!beg �������:20FDA71AA001
        SPW_4(16091)=.false.	!L_(88) O
!end �������:20FDA71AA001
       end

       subroutine FDA90_ConInQ
       end

       subroutine FDA90_ConIn
       LOGICAL*1 SPW_0(0:163839)
       Common/SPW_0/ SPW_0 !
       integer*4 SPW_0_4(0:40959)
       equivalence(SPW_0,SPW_0_4)
       LOGICAL*1 SPW_2(0:20479)
       Common/SPW_2/ SPW_2 !
       integer*4 SPW_2_4(0:5119)
       equivalence(SPW_2,SPW_2_4)
       LOGICAL*1 SPW_4(0:16383)
       Common/SPW_4/ SPW_4 !
       integer*4 SPW_4_4(0:4095)
       equivalence(SPW_4,SPW_4_4)
       LOGICAL*1 SPW_6(0:28671)
       Common/SPW_6/ SPW_6 !
       integer*4 SPW_6_4(0:7167)
       equivalence(SPW_6,SPW_6_4)
        Call FDA90_ConInQ
!beg ������:20FDA91AE002KE01
        SPW_0(154282)=.false.	!L_(191) O
!end ������:20FDA91AE002KE01
!beg ��������:20FDA91CM001KN01
        SPW_0(154341)=.false.	!L_(250) O
!end ��������:20FDA91CM001KN01
!beg ����������:20FDA91AE001KE01
        SPW_0(154291)=.true.	!L_(200) A
!end ����������:20FDA91AE001KE01
!beg �����:20FDA91AE001KE01
        SPW_0(154290)=.false.	!L_(199) O
!end �����:20FDA91AE001KE01
!beg ���������:20FDA91AE003KE01
        SPW_0(154293)=.false.	!L_(202) O
!end ���������:20FDA91AE003KE01
!beg ���������:20FDA91AE002KE01
        SPW_0(154285)=.true.	!L_(194) A
!end ���������:20FDA91AE002KE01
!beg ���������:20FDA91AE006
        SPW_0(154321)=.false.	!L_(230) O
!end ���������:20FDA91AE006
!beg �����:FDA70_LIFT
        SPW_0(154254)=.false.	!L_(163) O
!end �����:FDA70_LIFT
!beg ���������:20FDA91AE007
        SPW_0(154307)=.false.	!L_(216) O
!end ���������:20FDA91AE007
!beg ���������:20FDA91AE008
        SPW_0(154300)=.false.	!L_(209) O
!end ���������:20FDA91AE008
!beg ���������:20FDA91AE009
        SPW_0(154314)=.false.	!L_(223) O
!end ���������:20FDA91AE009
!beg ����:20FDA91AE002KE01
        SPW_0(154281)=.false.	!L_(190) O
!end ����:20FDA91AE002KE01
!beg ���������:20FDA91AE012
        SPW_0(154244)=.false.	!L_(153) O
!end ���������:20FDA91AE012
!beg ���������:20FDA91AE014
        SPW_0(154328)=.false.	!L_(237) O
!end ���������:20FDA91AE014
!beg ���������:20FDA91CM001KN01
        SPW_0(154342)=.false.	!L_(251) O
!end ���������:20FDA91CM001KN01
!beg ���������:20FDA90AE999
        SPW_0(154258)=.false.	!L_(167) O
!end ���������:20FDA90AE999
!beg ������:20FDA91AE006
        SPW_0(154322)=.false.	!L_(231) O
!end ������:20FDA91AE006
!beg ������:20FDA91AE007
        SPW_0(154308)=.false.	!L_(217) O
!end ������:20FDA91AE007
!beg ������:20FDA91AE008
        SPW_0(154301)=.false.	!L_(210) O
!end ������:20FDA91AE008
!beg ������:20FDA91AE009
        SPW_0(154315)=.false.	!L_(224) O
!end ������:20FDA91AE009
!beg ������:20FDA91AE012
        SPW_0(154245)=.false.	!L_(154) O
!end ������:20FDA91AE012
!beg ������:20FDA91AE014
        SPW_0(154329)=.false.	!L_(238) O
!end ������:20FDA91AE014
!beg ��������:20FDA91CU002KN01
        SPW_0(154335)=.false.	!L_(244) O
!end ��������:20FDA91CU002KN01
!beg ������:20FDA91AE001KE01
        SPW_0(154289)=.false.	!L_(198) O
!end ������:20FDA91AE001KE01
!beg ����������:20FDA91AE005KE01
        SPW_0(154277)=.true.	!L_(186) A
!end ����������:20FDA91AE005KE01
!beg ������:20FDA90AE999
        SPW_0(154259)=.false.	!L_(168) O
!end ������:20FDA90AE999
!beg �����:20FDA91AE005KE01
        SPW_0(154276)=.false.	!L_(185) O
!end �����:20FDA91AE005KE01
!beg ���������:20FDA91AE002KE01
        SPW_0(154279)=.false.	!L_(188) O
!end ���������:20FDA91AE002KE01
!beg ����:FDA70_LIFT
        SPW_0(154255)=.false.	!L_(164) O
!end ����:FDA70_LIFT
!beg ������:20FDA91AE003KE01
        SPW_0(154294)=.false.	!L_(203) O
!end ������:20FDA91AE003KE01
!beg ���������:20FDA91AE001KE01
        SPW_0(154292)=.true.	!L_(201) A
!end ���������:20FDA91AE001KE01
!beg ����������:20FDA91AE018KE01
        SPW_0(154270)=.true.	!L_(179) A
!end ����������:20FDA91AE018KE01
!beg �����:20FDA91AE018KE01
        SPW_0(154269)=.false.	!L_(178) O
!end �����:20FDA91AE018KE01
!beg ����:20FDA91AE001KE01
        SPW_0(154288)=.false.	!L_(197) O
!end ����:20FDA91AE001KE01
!beg ���������:20FDA91CU002KN01
        SPW_0(154336)=.false.	!L_(245) O
!end ���������:20FDA91CU002KN01
!beg ��������:20FDA91CU001KN01
        SPW_0(154339)=.false.	!L_(248) O
!end ��������:20FDA91CU001KN01
!beg �����:20FDA91AE006
        SPW_0(154324)=.false.	!L_(233) O
!end �����:20FDA91AE006
!beg �����:20FDA91AE007
        SPW_0(154310)=.false.	!L_(219) O
!end �����:20FDA91AE007
!beg �����:20FDA91AE008
        SPW_0(154303)=.false.	!L_(212) O
!end �����:20FDA91AE008
!beg �����:20FDA91AE009
        SPW_0(154317)=.false.	!L_(226) O
!end �����:20FDA91AE009
!beg ����:20FDA91AE006
        SPW_0(154325)=.false.	!L_(234) O
!end ����:20FDA91AE006
!beg ����:20FDA91AE007
        SPW_0(154311)=.false.	!L_(220) O
!end ����:20FDA91AE007
!beg ����:20FDA91AE008
        SPW_0(154304)=.false.	!L_(213) O
!end ����:20FDA91AE008
!beg ������:20FDA91AE005KE01
        SPW_0(154275)=.false.	!L_(184) O
!end ������:20FDA91AE005KE01
!beg ����:20FDA91AE009
        SPW_0(154318)=.false.	!L_(227) O
!end ����:20FDA91AE009
!beg �����:20FDA91AE012
        SPW_0(154247)=.false.	!L_(156) O
!end �����:20FDA91AE012
!beg �����:20FDA91AE014
        SPW_0(154331)=.false.	!L_(240) O
!end �����:20FDA91AE014
!beg ����:20FDA91AE012
        SPW_0(154248)=.false.	!L_(157) O
!end ����:20FDA91AE012
!beg ����:20FDA91AE014
        SPW_0(154332)=.false.	!L_(241) O
!end ����:20FDA91AE014
!beg ���������:20FDA91AE001KE01
        SPW_0(154286)=.false.	!L_(195) O
!end ���������:20FDA91AE001KE01
!beg �����:20FDA90AE999
        SPW_0(154261)=.false.	!L_(170) O
!end �����:20FDA90AE999
!beg ����:20FDA90AE999
        SPW_0(154262)=.false.	!L_(171) O
!end ����:20FDA90AE999
!beg ������:20FDA91AE002KE01
        SPW_0(154280)=.false.	!L_(189) O
!end ������:20FDA91AE002KE01
!beg ������:20FDA91AE018KE01
        SPW_0(154268)=.false.	!L_(177) O
!end ������:20FDA91AE018KE01
!beg ���������:20FDA91AE005KE01
        SPW_0(154278)=.true.	!L_(187) A
!end ���������:20FDA91AE005KE01
!beg ���������:20FDA91AE006
        SPW_0(154327)=.true.	!L_(236) A
!end ���������:20FDA91AE006
!beg ���������:20FDA91AE007
        SPW_0(154313)=.true.	!L_(222) A
!end ���������:20FDA91AE007
!beg ���������:20FDA91AE008
        SPW_0(154306)=.true.	!L_(215) A
!end ���������:20FDA91AE008
!beg ���������:20FDA91AE009
        SPW_0(154320)=.true.	!L_(229) A
!end ���������:20FDA91AE009
!beg ���������:FDA70_LIFT
        SPW_0(154256)=.true.	!L_(165) A
!end ���������:FDA70_LIFT
!beg ���������:20FDA91AE012
        SPW_0(154250)=.true.	!L_(159) A
!end ���������:20FDA91AE012
!beg ���������:20FDA91AE014
        SPW_0(154334)=.true.	!L_(243) A
!end ���������:20FDA91AE014
!beg ���������:20FDA91CU001KN01
        SPW_0(154340)=.false.	!L_(249) O
!end ���������:20FDA91CU001KN01
!beg ����:20FDA91AE005KE01
        SPW_0(154274)=.false.	!L_(183) O
!end ����:20FDA91AE005KE01
!beg ���������:20FDA90AE999
        SPW_0(154264)=.true.	!L_(173) A
!end ���������:20FDA90AE999
!beg ���������:20FDA91AE018KE01
        SPW_0(154271)=.true.	!L_(180) A
!end ���������:20FDA91AE018KE01
!beg ����:20FDA91AE018KE01
        SPW_0(154267)=.false.	!L_(176) O
!end ����:20FDA91AE018KE01
!beg ����������:20FDA91AE003KE01
        SPW_0(154298)=.true.	!L_(207) A
!end ����������:20FDA91AE003KE01
!beg ������:FDA70_LIFT
        SPW_0(154252)=.false.	!L_(161) O
!end ������:FDA70_LIFT
!beg �����:20FDA91AE003KE01
        SPW_0(154297)=.false.	!L_(206) O
!end �����:20FDA91AE003KE01
!beg ���������:20FDA91AE005KE01
        SPW_0(154272)=.false.	!L_(181) O
!end ���������:20FDA91AE005KE01
!beg ������:20FDA91AE001KE01
        SPW_0(154287)=.false.	!L_(196) O
!end ������:20FDA91AE001KE01
!beg ����:20FDA91AE006
        SPW_0(154323)=.false.	!L_(232) O
!end ����:20FDA91AE006
!beg ����:20FDA91AE007
        SPW_0(154309)=.false.	!L_(218) O
!end ����:20FDA91AE007
!beg ����:20FDA91AE008
        SPW_0(154302)=.false.	!L_(211) O
!end ����:20FDA91AE008
!beg ����:20FDA91AE009
        SPW_0(154316)=.false.	!L_(225) O
!end ����:20FDA91AE009
!beg ����:20FDA91AE012
        SPW_0(154246)=.false.	!L_(155) O
!end ����:20FDA91AE012
!beg ����:20FDA91AE014
        SPW_0(154330)=.false.	!L_(239) O
!end ����:20FDA91AE014
!beg ���������:20FDA91AE018KE01
        SPW_0(154265)=.false.	!L_(174) O
!end ���������:20FDA91AE018KE01
!beg ����:20FDA90AE999
        SPW_0(154260)=.false.	!L_(169) O
!end ����:20FDA90AE999
!beg ����:FDA70_LIFT
        SPW_0(154253)=.false.	!L_(162) O
!end ����:FDA70_LIFT
!beg ���������:20FDA91AE006
        SPW_0(154326)=.true.	!L_(235) A
!end ���������:20FDA91AE006
!beg ���������:20FDA91AE007
        SPW_0(154312)=.true.	!L_(221) A
!end ���������:20FDA91AE007
!beg ���������:20FDA91AE008
        SPW_0(154305)=.true.	!L_(214) A
!end ���������:20FDA91AE008
!beg ���������:20FDA91AE009
        SPW_0(154319)=.true.	!L_(228) A
!end ���������:20FDA91AE009
!beg ���������:20FDA91AE012
        SPW_0(154249)=.true.	!L_(158) A
!end ���������:20FDA91AE012
!beg ���������:20FDA91AE014
        SPW_0(154333)=.true.	!L_(242) A
!end ���������:20FDA91AE014
!beg ���������:FDA70_LIFT
        SPW_0(154251)=.false.	!L_(160) O
!end ���������:FDA70_LIFT
!beg ������:20FDA91AE003KE01
        SPW_0(154296)=.false.	!L_(205) O
!end ������:20FDA91AE003KE01
!beg ��������:20FDA91CM002KN01
        SPW_0(154337)=.false.	!L_(246) O
!end ��������:20FDA91CM002KN01
!beg ����������:20FDA91AE002KE01
        SPW_0(154284)=.true.	!L_(193) A
!end ����������:20FDA91AE002KE01
!beg ���������:20FDA90AE999
        SPW_0(154263)=.true.	!L_(172) A
!end ���������:20FDA90AE999
!beg �����:20FDA91AE002KE01
        SPW_0(154283)=.false.	!L_(192) O
!end �����:20FDA91AE002KE01
!beg ������:20FDA91AE005KE01
        SPW_0(154273)=.false.	!L_(182) O
!end ������:20FDA91AE005KE01
!beg ���������:20FDA91AE003KE01
        SPW_0(154299)=.true.	!L_(208) A
!end ���������:20FDA91AE003KE01
!beg ����:20FDA91AE003KE01
        SPW_0(154295)=.false.	!L_(204) O
!end ����:20FDA91AE003KE01
!beg ���������:20FDA91CM002KN01
        SPW_0(154338)=.false.	!L_(247) O
!end ���������:20FDA91CM002KN01
!beg ������:20FDA91AE018KE01
        SPW_0(154266)=.false.	!L_(175) O
!end ������:20FDA91AE018KE01
!beg ���������:FDA70_LIFT
        SPW_0(154257)=.true.	!L_(166) A
!end ���������:FDA70_LIFT
       end
