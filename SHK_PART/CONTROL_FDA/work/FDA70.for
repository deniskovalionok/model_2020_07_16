      Subroutine FDA70(ext_deltat)
      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      Include 'FDA70.fh'

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      Call FDA70_ConIn
      R_(29)=R8_ibe
C FDA70_vent_log.fgi( 160, 274):pre: �������������� �����  
      R_(33)=R8_ide
C FDA70_vent_log.fgi(  62, 274):pre: �������������� �����  
      R_(21)=R8_iv
C FDA70_vent_log.fgi(  55, 192):pre: �������������� �����  
      R_(10)=R8_op
C FDA70_vent_log.fgi( 152, 192):pre: �������������� �����  
      !{
      Call ZATVOR_MAN(deltat,REAL(R_urud,4),R8_umud,I_evud
     &,I_uvud,I_avud,C8_ipud,
     & I_ovud,R_erud,R_arud,R_ukud,REAL(R_elud,4),
     & R_amud,REAL(R_imud,4),R_ilud,
     & REAL(R_ulud,4),I_otud,I_axud,I_ivud,I_itud,L_omud,L_ixud
     &,
     & L_ibaf,L_emud,L_epud,L_apud,L_uxud,
     & L_olud,L_alud,L_upud,L_adaf,L_irud,L_orud,
     & REAL(R8_arul,8),REAL(1.0,4),R8_ivul,L_(82),L_akud,L_
     &(81),L_ekud,
     & L_oxud,I_exud,L_obaf,R_etud,REAL(R_opud,4),L_ubaf,L_ikud
     &,L_edaf,
     & L_okud,L_asud,L_esud,L_isud,L_usud,L_atud,L_osud)
      !}

      if(L_atud.or.L_usud.or.L_osud.or.L_isud.or.L_esud.or.L_asud
     &) then      
                  I_utud = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_utud = z'40000000'
      endif
C FDA70_vlv.fgi(  95,  62):���� ���������� ��������,20FDA73AE403KA01
      !{
      Call SHIB_HANDLER(deltat,R8_ope,R_ali,R_eli,R_idi,R_adi
     &,
     & L_eki,R_odi,R_edi,L_iki,R_obi,
     & R_ubi,R_aki,R_oki,R_uki,R_eke,
     & R_ufi,R_ake,REAL(R_upe,4),C20_ere,I_ive,I_oxe,
     & R_ime,REAL(R_ume,4),R_ule,
     & REAL(R_eme,4),I_ese,I_ase,I_ove,I_exe,C20_are,
     & C20_ire,C8_ore,L_uke,R_oke,
     & REAL(R_ale,4),L_ile,R_ele,
     & REAL(R_ole,4),I_axe,I_ixe,I_uve,I_eve,L_ape,
     & L_abi,L_afi,L_ome,L_ame,
     & L_ipe,L_epe,L_ibi,L_ure,L_udi,L_ose,L_use,
     & REAL(R8_ike,8),REAL(1.0,4),R8_ise,L_(14),L_ife,L_(13
     &),L_efe,
     & L_ebi,I_uxe,L_efi,L_ufe,L_ofi,L_ofe,L_ate,L_ete,L_ite
     &,L_ute,
     & L_ave,L_ote)
      !}
C FDA70_vlv.fgi( 137, 193):���������� ������,20FDA71AE701KE02
      !{
      Call SHIB_HANDLER(deltat,R8_emod,R_ofud,R_ufud,R_abud
     &,R_oxod,
     & L_udud,R_ebud,R_uxod,L_afud,R_exod,
     & R_ixod,R_odud,R_efud,R_ifud,R_udod,
     & R_idud,R_odod,REAL(R_imod,4),C20_umod,I_atod,I_evod
     &,
     & R_alod,REAL(R_ilod,4),R_ikod,
     & REAL(R_ukod,4),I_upod,I_opod,I_etod,I_utod,C20_omod
     &,
     & C20_apod,C8_epod,L_ifod,R_efod,
     & REAL(R_ofod,4),L_akod,R_ufod,
     & REAL(R_ekod,4),I_otod,I_avod,I_itod,I_usod,L_olod,
     & L_ovod,L_obud,L_elod,L_okod,
     & L_amod,L_ulod,L_axod,L_ipod,L_ibud,L_erod,L_irod,
     & REAL(R8_afod,8),REAL(1.0,4),R8_arod,L_(80),L_adod,L_
     &(79),L_ubod,
     & L_uvod,I_ivod,L_ubud,L_idod,L_edud,L_edod,L_orod,L_urod
     &,L_asod,L_isod,
     & L_osod,L_esod)
      !}
C FDA70_vlv.fgi( 119, 193):���������� ������,20FDA71AE701KE01
      Call MECHANIC_PRIVOD(deltat,REAL(950,4),L_atid,L_(77
     &),
     & REAL(900,4),L_ofid,L_(68),REAL(800,4),L_ikid,L_(69
     &),
     & REAL(700,4),L_elid,L_(70),REAL(600,4),L_amid,L_(71
     &),
     & REAL(500,4),L_umid,L_(72),REAL(400,4),L_opid,L_(73
     &),
     & REAL(300,4),L_irid,L_(74),REAL(200,4),L_esid,L_(75
     &),
     & L_osid,L_efid,L_akid,L_ukid,L_olid,L_etid,L_ufid,
     & L_okid,L_ilid,L_emid,L_usid,L_ifid,L_ekid,
     & L_alid,L_ulid,L_imid,L_apid,L_omid,L_epid,L_arid,
     & L_urid,L_upid,L_orid,L_isid,L_ipid,L_erid,
     & L_asid,REAL(100,4),L_otid,L_itid,L_avid,L_utid,
     & L_(76),L_idid,L_(64),L_ixid,L_(65),R_afid,L_oxid,L_uxid
     &,
     & REAL(20,4),L_ovid,L_(78),L_evid,L_ivid,L_(67),
     & L_(66),INT(I_odid,4),L_udid,INT(I_axid,4),L_exid,
     & INT(I_adid,4),L_edid,R_uvid,REAL(4136,4),REAL(0,4)
     &,R_ibod,L_obid,
     & L_ubid,L_abod,L_ebod)
C FDA70_vlv.fgi(  40, 192):������,20FDA74AE001
      Call MECHANIC_PRIVOD(deltat,REAL(950,4),L_isu,L_(32
     &),
     & REAL(900,4),L_afu,L_(23),REAL(800,4),L_ufu,L_(24),
     & REAL(700,4),L_oku,L_(25),REAL(600,4),L_ilu,L_(26),
     & REAL(500,4),L_emu,L_(27),REAL(400,4),L_apu,L_(28),
     & REAL(300,4),L_upu,L_(29),REAL(200,4),L_oru,L_(30),
     & L_asu,L_odu,L_ifu,L_eku,L_alu,L_osu,L_efu,
     & L_aku,L_uku,L_olu,L_esu,L_udu,L_ofu,
     & L_iku,L_elu,L_ulu,L_imu,L_amu,L_omu,L_ipu,
     & L_eru,L_epu,L_aru,L_uru,L_umu,L_opu,
     & L_iru,REAL(1886,4),L_atu,L_usu,L_itu,L_etu,
     & L_(31),L_ubu,L_(19),L_uvu,L_(20),R_idu,L_axu,L_exu
     &,
     & REAL(20,4),L_avu,L_(33),L_otu,L_utu,L_(22),
     & L_(21),INT(I_adu,4),L_edu,INT(I_ivu,4),L_ovu,
     & INT(I_ibu,4),L_obu,R_evu,REAL(3136,4),REAL(636,4),R_uxu
     &,L_abu,
     & L_ebu,L_ixu,L_oxu)
C FDA70_vlv.fgi(  92, 192):������,20FDA71AE202
      Call MECHANIC_PRIVOD(deltat,REAL(950,4),L_osad,L_(47
     &),
     & REAL(900,4),L_efad,L_(38),REAL(800,4),L_akad,L_(39
     &),
     & REAL(700,4),L_ukad,L_(40),REAL(600,4),L_olad,L_(41
     &),
     & REAL(500,4),L_imad,L_(42),REAL(400,4),L_epad,L_(43
     &),
     & REAL(300,4),L_arad,L_(44),REAL(3587,4),L_urad,L_(45
     &),
     & L_esad,L_udad,L_ofad,L_ikad,L_elad,L_usad,L_ifad,
     & L_ekad,L_alad,L_ulad,L_isad,L_afad,L_ufad,
     & L_okad,L_ilad,L_amad,L_omad,L_emad,L_umad,L_opad,
     & L_irad,L_ipad,L_erad,L_asad,L_apad,L_upad,
     & L_orad,REAL(3334,4),L_etad,L_atad,L_otad,L_itad,
     & L_(46),L_adad,L_(34),L_axad,L_(35),R_odad,L_exad,L_ixad
     &,
     & REAL(20,4),L_evad,L_(48),L_utad,L_avad,L_(37),
     & L_(36),INT(I_edad,4),L_idad,INT(I_ovad,4),L_uvad,
     & INT(I_obad,4),L_ubad,R_ivad,REAL(3637,4),REAL(3284
     &,4),R_abed,L_ebad,
     & L_ibad,L_oxad,L_uxad)
C FDA70_vlv.fgi(  66, 192):������,20FDA71AE801
      Call MECHANIC_PRIVOD(deltat,REAL(950,4),L_used,L_(62
     &),
     & REAL(900,4),L_ifed,L_(53),REAL(800,4),L_eked,L_(54
     &),
     & REAL(700,4),L_aled,L_(55),REAL(600,4),L_uled,L_(56
     &),
     & REAL(500,4),L_omed,L_(57),REAL(400,4),L_iped,L_(58
     &),
     & REAL(300,4),L_ered,L_(59),REAL(200,4),L_ased,L_(60
     &),
     & L_ised,L_afed,L_ufed,L_oked,L_iled,L_ated,L_ofed,
     & L_iked,L_eled,L_amed,L_osed,L_efed,L_aked,
     & L_uked,L_oled,L_emed,L_umed,L_imed,L_aped,L_uped,
     & L_ored,L_oped,L_ired,L_esed,L_eped,L_ared,
     & L_ured,REAL(250,4),L_ited,L_eted,L_uted,L_oted,
     & L_(61),L_eded,L_(49),L_exed,L_(50),R_uded,L_ixed,L_oxed
     &,
     & REAL(20,4),L_ived,L_(63),L_aved,L_eved,L_(52),
     & L_(51),INT(I_ided,4),L_oded,INT(I_uved,4),L_axed,
     & INT(I_ubed,4),L_aded,R_oved,REAL(500,4),REAL(0,4),R_ebid
     &,L_ibed,
     & L_obed,L_uxed,L_abid)
C FDA70_vlv.fgi(  14, 192):������,20FDA73AE003
      R_(1) = 1.e-3
C FDA70_vent_log.fgi( 162,  83):��������� (RE4) (�������)
      R_esil = R8_ef * R_(1)
C FDA70_vent_log.fgi( 165,  85):����������
      !{
      Call DAT_ANA_HANDLER(deltat,R_aril,R_util,REAL(1,4)
     &,
     & REAL(R_oril,4),REAL(R_uril,4),
     & REAL(R_upil,4),REAL(R_opil,4),I_otil,
     & REAL(R_isil,4),L_osil,REAL(R_usil,4),L_atil,L_etil
     &,R_asil,
     & REAL(R_iril,4),REAL(R_eril,4),L_itil,REAL(R_esil,4
     &))
      !}
C FDA70_vlv.fgi( 182,  46):���������� �������,20FDA72CP002XQ01
      R_(2) = 1.e-3
C FDA70_vent_log.fgi(  47,  83):��������� (RE4) (�������)
      R_oxil = R8_al * R_(2)
C FDA70_vent_log.fgi(  50,  85):����������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ivil,R_edol,REAL(1,4)
     &,
     & REAL(R_axil,4),REAL(R_exil,4),
     & REAL(R_evil,4),REAL(R_avil,4),I_adol,
     & REAL(R_uxil,4),L_abol,REAL(R_ebol,4),L_ibol,L_obol
     &,R_ixil,
     & REAL(R_uvil,4),REAL(R_ovil,4),L_ubol,REAL(R_oxil,4
     &))
      !}
C FDA70_vlv.fgi( 182,  60):���������� �������,20FDA71CP002XQ01
      R_(3) = 1.e-3
C FDA70_vent_log.fgi(  47,  75):��������� (RE4) (�������)
      R_(4) = R8_e + (-R8_af)
C FDA70_vent_log.fgi(  39,  78):��������
      R_ipol = R_(4) * R_(3)
C FDA70_vent_log.fgi(  50,  77):����������
      !{
      Call DAT_ANA_HANDLER(deltat,R_emol,R_asol,REAL(1,4)
     &,
     & REAL(R_umol,4),REAL(R_apol,4),
     & REAL(R_amol,4),REAL(R_ulol,4),I_urol,
     & REAL(R_opol,4),L_upol,REAL(R_arol,4),L_erol,L_irol
     &,R_epol,
     & REAL(R_omol,4),REAL(R_imol,4),L_orol,REAL(R_ipol,4
     &))
      !}
C FDA70_vlv.fgi( 182,  87):���������� �������,20FDA71CP001XQ01
      R_(5) = 1.e-3
C FDA70_vent_log.fgi( 162,  75):��������� (RE4) (�������)
      if(R8_ar.le.R0_ad) then
         R_evuk=R0_u
      elseif(R8_ar.gt.R0_o) then
         R_evuk=R0_i
      else
         R_evuk=R0_u+(R8_ar-(R0_ad))*(R0_i-(R0_u))/(R0_o-
     &(R0_ad))
      endif
C FDA70_vent_log.fgi( 158,  43):��������������� ���������
      !{
      Call DAT_ANA_HANDLER(deltat,R_atuk,R_uxuk,REAL(1,4)
     &,
     & REAL(R_otuk,4),REAL(R_utuk,4),
     & REAL(R_usuk,4),REAL(R_osuk,4),I_oxuk,
     & REAL(R_ivuk,4),L_ovuk,REAL(R_uvuk,4),L_axuk,L_exuk
     &,R_avuk,
     & REAL(R_ituk,4),REAL(R_etuk,4),L_ixuk,REAL(R_evuk,4
     &))
      !}
C FDA70_vlv.fgi( 183,  32):���������� �������,20FDA72CW001XQ01
      R_uxal = R8_ed
C FDA70_vent_log.fgi( 157,  52):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_oval,R_idel,REAL(1,4)
     &,
     & REAL(R_exal,4),REAL(R_ixal,4),
     & REAL(R_ival,4),REAL(R_eval,4),I_edel,
     & REAL(R_abel,4),L_ebel,REAL(R_ibel,4),L_obel,L_ubel
     &,R_oxal,
     & REAL(R_axal,4),REAL(R_uval,4),L_adel,REAL(R_uxal,4
     &))
      !}
C FDA70_vlv.fgi( 120,  46):���������� �������,20FDA72CM001XQ01
      R_amal = R8_id
C FDA70_vent_log.fgi( 157,  57):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ukal,R_opal,REAL(1,4)
     &,
     & REAL(R_ilal,4),REAL(R_olal,4),
     & REAL(R_okal,4),REAL(R_ikal,4),I_ipal,
     & REAL(R_emal,4),L_imal,REAL(R_omal,4),L_umal,L_apal
     &,R_ulal,
     & REAL(R_elal,4),REAL(R_alal,4),L_epal,REAL(R_amal,4
     &))
      !}
C FDA70_vlv.fgi( 120,  18):���������� �������,20FDA72CU001XQ01
      R_idil = R8_od
C FDA70_vent_log.fgi( 157,  68):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ebil,R_akil,REAL(1,4)
     &,
     & REAL(R_ubil,4),REAL(R_adil,4),
     & REAL(R_abil,4),REAL(R_uxel,4),I_ufil,
     & REAL(R_odil,4),L_udil,REAL(R_afil,4),L_efil,L_ifil
     &,R_edil,
     & REAL(R_obil,4),REAL(R_ibil,4),L_ofil,REAL(R_idil,4
     &))
      !}
C FDA70_vlv.fgi( 151,  32):���������� �������,20FDA72CF001XQ01
      R_(6) = R8_ud + (-R8_af)
C FDA70_vent_log.fgi( 154,  78):��������
      R_akol = R_(6) * R_(5)
C FDA70_vent_log.fgi( 165,  77):����������
      !{
      Call DAT_ANA_HANDLER(deltat,R_udol,R_olol,REAL(1,4)
     &,
     & REAL(R_ifol,4),REAL(R_ofol,4),
     & REAL(R_odol,4),REAL(R_idol,4),I_ilol,
     & REAL(R_ekol,4),L_ikol,REAL(R_okol,4),L_ukol,L_alol
     &,R_ufol,
     & REAL(R_efol,4),REAL(R_afol,4),L_elol,REAL(R_akol,4
     &))
      !}
C FDA70_vlv.fgi( 182,  73):���������� �������,20FDA72CP001XQ01
      R_utol = R8_if
C FDA70_vent_log.fgi( 159,  92):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_osol,R_ixol,REAL(1,4)
     &,
     & REAL(R_etol,4),REAL(R_itol,4),
     & REAL(R_isol,4),REAL(R_esol,4),I_exol,
     & REAL(R_avol,4),L_evol,REAL(R_ivol,4),L_ovol,L_uvol
     &,R_otol,
     & REAL(R_atol,4),REAL(R_usol,4),L_axol,REAL(R_utol,4
     &))
      !}
C FDA70_vlv.fgi( 151,  60):���������� �������,20FDA72CT001XQ01
      if(R8_uv.le.R0_ek) then
         R_odal=R0_ak
      elseif(R8_uv.gt.R0_uf) then
         R_odal=R0_of
      else
         R_odal=R0_ak+(R8_uv-(R0_ek))*(R0_of-(R0_ak))/(R0_uf
     &-(R0_ek))
      endif
C FDA70_vent_log.fgi(  43,  43):��������������� ���������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ibal,R_ekal,REAL(1,4)
     &,
     & REAL(R_adal,4),REAL(R_edal,4),
     & REAL(R_ebal,4),REAL(R_abal,4),I_akal,
     & REAL(R_udal,4),L_afal,REAL(R_efal,4),L_ifal,L_ofal
     &,R_idal,
     & REAL(R_ubal,4),REAL(R_obal,4),L_ufal,REAL(R_odal,4
     &))
      !}
C FDA70_vlv.fgi( 151,  18):���������� �������,20FDA71CW001XQ01
      R_ekel = R8_ik
C FDA70_vent_log.fgi(  42,  52):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_afel,R_ulel,REAL(1,4)
     &,
     & REAL(R_ofel,4),REAL(R_ufel,4),
     & REAL(R_udel,4),REAL(R_odel,4),I_olel,
     & REAL(R_ikel,4),L_okel,REAL(R_ukel,4),L_alel,L_elel
     &,R_akel,
     & REAL(R_ifel,4),REAL(R_efel,4),L_ilel,REAL(R_ekel,4
     &))
      !}
C FDA70_vlv.fgi( 120,  60):���������� �������,20FDA71CM001XQ01
      R_isal = R8_ok
C FDA70_vent_log.fgi(  42,  57):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_eral,R_aval,REAL(1,4)
     &,
     & REAL(R_ural,4),REAL(R_asal,4),
     & REAL(R_aral,4),REAL(R_upal,4),I_utal,
     & REAL(R_osal,4),L_usal,REAL(R_atal,4),L_etal,L_ital
     &,R_esal,
     & REAL(R_oral,4),REAL(R_iral,4),L_otal,REAL(R_isal,4
     &))
      !}
C FDA70_vlv.fgi( 120,  32):���������� �������,20FDA71CU001XQ01
      R_ulil = R8_uk
C FDA70_vent_log.fgi(  42,  68):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_okil,R_ipil,REAL(1,4)
     &,
     & REAL(R_elil,4),REAL(R_ilil,4),
     & REAL(R_ikil,4),REAL(R_ekil,4),I_epil,
     & REAL(R_amil,4),L_emil,REAL(R_imil,4),L_omil,L_umil
     &,R_olil,
     & REAL(R_alil,4),REAL(R_ukil,4),L_apil,REAL(R_ulil,4
     &))
      !}
C FDA70_vlv.fgi( 151,  46):���������� �������,20FDA71CF001XQ01
      R_edul = R8_el
C FDA70_vent_log.fgi(  44,  92):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_abul,R_uful,REAL(1,4)
     &,
     & REAL(R_obul,4),REAL(R_ubul,4),
     & REAL(R_uxol,4),REAL(R_oxol,4),I_oful,
     & REAL(R_idul,4),L_odul,REAL(R_udul,4),L_aful,L_eful
     &,R_adul,
     & REAL(R_ibul,4),REAL(R_ebul,4),L_iful,REAL(R_edul,4
     &))
      !}
C FDA70_vlv.fgi( 151,  74):���������� �������,20FDA71CT001XQ01
      !��������� R_(7) = FDA70_vent_logC?? /0.0005/
      R_(7)=R0_il
C FDA70_vent_log.fgi( 129, 131):���������
      L_(1)=R8_ol.gt.R_(7)
C FDA70_vent_log.fgi( 133, 132):���������� >
      !��������� R_(9) = FDA70_vent_logC?? /0.0/
      R_(9)=R0_ul
C FDA70_vent_log.fgi( 137, 138):���������
      !��������� R_(8) = FDA70_vent_logC?? /500/
      R_(8)=R0_am
C FDA70_vent_log.fgi( 137, 136):���������
      if(L_(1)) then
         R_(17)=R_(8)
      else
         R_(17)=R_(9)
      endif
C FDA70_vent_log.fgi( 140, 136):���� RE IN LO CH7
      R_(13) = 1.0
C FDA70_vent_log.fgi( 139, 190):��������� (RE4) (�������)
      R_(14) = 0.0
C FDA70_vent_log.fgi( 139, 192):��������� (RE4) (�������)
      !��������� R_(11) = FDA70_vent_logC?? /1.3/
      R_(11)=R0_om
C FDA70_vent_log.fgi( 135, 211):���������
      L_(2)=R_evuk.gt.R_(11)
C FDA70_vent_log.fgi( 139, 212):���������� >
      L_(3) = L_(2).OR.L_em
C FDA70_vent_log.fgi( 163, 211):���
      L_ap=L_um.or.(L_ap.and..not.(L_(3)))
      L_(4)=.not.L_ap
C FDA70_vent_log.fgi( 169, 213):RS �������
      L_ep=L_ap
C FDA70_vent_log.fgi( 183, 215):������,FDA72dust_start
      if(L_ep) then
         R_(12)=R_(13)
      else
         R_(12)=R_(14)
      endif
C FDA70_vent_log.fgi( 142, 191):���� RE IN LO CH7
      R8_op=(R0_im*R_(10)+deltat*R_(12))/(R0_im+deltat)
C FDA70_vent_log.fgi( 152, 192):�������������� �����  
      R8_ip=R8_op
C FDA70_vent_log.fgi( 170, 192):������,20FDA72AN001Y01
      !��������� R_(15) = FDA70_vent_logC?? /1/
      R_(15)=R0_up
C FDA70_vent_log.fgi( 135, 156):���������
      L_(5)=R8_ar.lt.R_(15)
C FDA70_vent_log.fgi( 139, 157):���������� <
      L_ir=L_er.or.(L_ir.and..not.(L_(5)))
      L_(6)=.not.L_ir
C FDA70_vent_log.fgi( 169, 159):RS �������
      L_ur=L_ir
C FDA70_vent_log.fgi( 183, 161):������,20FDA72CW001_OUT
      !��������� R_(16) = FDA70_vent_logC?? /-50000/
      R_(16)=R0_or
C FDA70_vent_log.fgi( 165, 135):���������
      if(L_ur) then
         R8_as=R_(16)
      else
         R8_as=R_(17)
      endif
C FDA70_vent_log.fgi( 168, 135):���� RE IN LO CH7
      !��������� R_(18) = FDA70_vent_logC?? /0.0005/
      R_(18)=R0_es
C FDA70_vent_log.fgi(  32, 131):���������
      L_(7)=R8_is.gt.R_(18)
C FDA70_vent_log.fgi(  36, 132):���������� >
      !��������� R_(20) = FDA70_vent_logC?? /0.0/
      R_(20)=R0_os
C FDA70_vent_log.fgi(  40, 138):���������
      !��������� R_(19) = FDA70_vent_logC?? /500/
      R_(19)=R0_us
C FDA70_vent_log.fgi(  40, 136):���������
      if(L_(7)) then
         R_(28)=R_(19)
      else
         R_(28)=R_(20)
      endif
C FDA70_vent_log.fgi(  43, 136):���� RE IN LO CH7
      R_(24) = 1.0
C FDA70_vent_log.fgi(  42, 190):��������� (RE4) (�������)
      R_(25) = 0.0
C FDA70_vent_log.fgi(  42, 192):��������� (RE4) (�������)
      !��������� R_(22) = FDA70_vent_logC?? /1.3/
      R_(22)=R0_it
C FDA70_vent_log.fgi(  38, 211):���������
      L_(8)=R_odal.gt.R_(22)
C FDA70_vent_log.fgi(  42, 212):���������� >
      L_(9) = L_(8).OR.L_at
C FDA70_vent_log.fgi(  66, 211):���
      L_ut=L_ot.or.(L_ut.and..not.(L_(9)))
      L_(10)=.not.L_ut
C FDA70_vent_log.fgi(  72, 213):RS �������
      L_av=L_ut
C FDA70_vent_log.fgi(  86, 215):������,FDA71dust_start
      if(L_av) then
         R_(23)=R_(24)
      else
         R_(23)=R_(25)
      endif
C FDA70_vent_log.fgi(  45, 191):���� RE IN LO CH7
      R8_iv=(R0_et*R_(21)+deltat*R_(23))/(R0_et+deltat)
C FDA70_vent_log.fgi(  55, 192):�������������� �����  
      R8_ev=R8_iv
C FDA70_vent_log.fgi(  74, 192):������,20FDA71AN001Y01
      !��������� R_(26) = FDA70_vent_logC?? /1/
      R_(26)=R0_ov
C FDA70_vent_log.fgi(  38, 156):���������
      L_(11)=R8_uv.lt.R_(26)
C FDA70_vent_log.fgi(  42, 157):���������� <
      L_ex=L_ax.or.(L_ex.and..not.(L_(11)))
      L_(12)=.not.L_ex
C FDA70_vent_log.fgi(  72, 159):RS �������
      L_ox=L_ex
C FDA70_vent_log.fgi(  86, 161):������,20FDA71CW001_OUT
      !��������� R_(27) = FDA70_vent_logC?? /-50000/
      R_(27)=R0_ix
C FDA70_vent_log.fgi(  68, 135):���������
      if(L_ox) then
         R8_ux=R_(27)
      else
         R8_ux=R_(28)
      endif
C FDA70_vent_log.fgi(  71, 135):���� RE IN LO CH7
      !��������� R_(32) = FDA70_vent_logC?? /0.0/
      R_(32)=R0_obe
C FDA70_vent_log.fgi( 145, 275):���������
      !��������� R_(31) = FDA70_vent_logC?? /0.001/
      R_(31)=R0_ube
C FDA70_vent_log.fgi( 145, 273):���������
      !��������� R_(36) = FDA70_vent_logC?? /0.0/
      R_(36)=R0_ode
C FDA70_vent_log.fgi(  47, 275):���������
      !��������� R_(35) = FDA70_vent_logC?? /0.001/
      R_(35)=R0_ude
C FDA70_vent_log.fgi(  47, 273):���������
      R_olul=R_afe
C FDA70_vlv.fgi( 172, 104):������,20FDA73CW001XQ01_input
      !{
      Call DAT_ANA_HANDLER(deltat,R_ikul,R_epul,REAL(1,4)
     &,
     & REAL(R_alul,4),REAL(R_elul,4),
     & REAL(R_ekul,4),REAL(R_akul,4),I_apul,
     & REAL(R_ulul,4),L_amul,REAL(R_emul,4),L_imul,L_omul
     &,R_ilul,
     & REAL(R_ukul,4),REAL(R_okul,4),L_umul,REAL(R_olul,4
     &))
      !}
C FDA70_vlv.fgi( 150, 116):���������� �������,20FDA73CW001XQ01
      !{
      Call BVALVE_MAN(deltat,REAL(R_imaf,4),L_usaf,L_ataf
     &,R8_ikaf,L_obuk,
     & L_eduk,L_apuk,I_uraf,I_asaf,R_idaf,
     & REAL(R_udaf,4),R_ofaf,REAL(R_akaf,4),
     & R_afaf,REAL(R_ifaf,4),I_araf,I_esaf,I_oraf,I_iraf,L_ekaf
     &,
     & L_isaf,L_utaf,L_ufaf,L_efaf,
     & L_elaf,L_alaf,L_etaf,L_odaf,L_olaf,L_ivaf,L_osaf,
     & L_amaf,L_emaf,REAL(R8_arul,8),REAL(1.0,4),R8_ivul,L_ukaf
     &,
     & L_okaf,L_avaf,R_upaf,REAL(R_ilaf,4),L_evaf,L_ovaf,L_omaf
     &,
     & L_umaf,L_apaf,L_ipaf,L_opaf,L_epaf)
      !}

      if(L_opaf.or.L_ipaf.or.L_epaf.or.L_apaf.or.L_umaf.or.L_omaf
     &) then      
                  I_eraf = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_eraf = z'40000000'
      endif
C FDA70_vlv.fgi(  77,  88):���� ���������� ������ �������,20FDA72CF101KA02
      !{
      Call BVALVE_MAN(deltat,REAL(R_ufef,4),L_epef,L_ipef
     &,R8_ubef,L_obuk,
     & L_eduk,L_apuk,I_emef,I_imef,R_uvaf,
     & REAL(R_exaf,4),R_abef,REAL(R_ibef,4),
     & R_ixaf,REAL(R_uxaf,4),I_ilef,I_omef,I_amef,I_ulef,L_obef
     &,
     & L_umef,L_eref,L_ebef,L_oxaf,
     & L_odef,L_idef,L_opef,L_axaf,L_afef,L_uref,L_apef,
     & L_ifef,L_ofef,REAL(R8_arul,8),REAL(1.0,4),R8_ivul,L_edef
     &,
     & L_adef,L_iref,R_elef,REAL(R_udef,4),L_oref,L_asef,L_akef
     &,
     & L_ekef,L_ikef,L_ukef,L_alef,L_okef)
      !}

      if(L_alef.or.L_ukef.or.L_okef.or.L_ikef.or.L_ekef.or.L_akef
     &) then      
                  I_olef = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_olef = z'40000000'
      endif
C FDA70_vlv.fgi(  58,  88):���� ���������� ������ �������,20FDA71CF101KA02
      !{
      Call BVALVE_MAN(deltat,REAL(R_ebif,4),L_okif,L_ukif
     &,R8_evef,L_obuk,
     & L_eduk,L_apuk,I_ofif,I_ufif,R_esef,
     & REAL(R_osef,4),R_itef,REAL(R_utef,4),
     & R_usef,REAL(R_etef,4),I_udif,I_akif,I_ifif,I_efif,L_avef
     &,
     & L_ekif,L_olif,L_otef,L_atef,
     & L_axef,L_uvef,L_alif,L_isef,L_ixef,L_emif,L_ikif,
     & L_uxef,L_abif,REAL(R8_arul,8),REAL(1.0,4),R8_ivul,L_ovef
     &,
     & L_ivef,L_ulif,R_odif,REAL(R_exef,4),L_amif,L_imif,L_ibif
     &,
     & L_obif,L_ubif,L_edif,L_idif,L_adif)
      !}

      if(L_idif.or.L_edif.or.L_adif.or.L_ubif.or.L_obif.or.L_ibif
     &) then      
                  I_afif = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_afif = z'40000000'
      endif
C FDA70_vlv.fgi(  77,  37):���� ���������� ������ �������,20FDA72AA101
      !{
      Call BVALVE_MAN(deltat,REAL(R_otif,4),L_adof,L_edof
     &,R8_orif,L_obuk,
     & L_eduk,L_apuk,I_abof,I_ebof,R_omif,
     & REAL(R_apif,4),R_upif,REAL(R_erif,4),
     & R_epif,REAL(R_opif,4),I_exif,I_ibof,I_uxif,I_oxif,L_irif
     &,
     & L_obof,L_afof,L_arif,L_ipif,
     & L_isif,L_esif,L_idof,L_umif,L_usif,L_ofof,L_ubof,
     & L_etif,L_itif,REAL(R8_arul,8),REAL(1.0,4),R8_ivul,L_asif
     &,
     & L_urif,L_efof,R_axif,REAL(R_osif,4),L_ifof,L_ufof,L_utif
     &,
     & L_avif,L_evif,L_ovif,L_uvif,L_ivif)
      !}

      if(L_uvif.or.L_ovif.or.L_ivif.or.L_evif.or.L_avif.or.L_utif
     &) then      
                  I_ixif = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_ixif = z'40000000'
      endif
C FDA70_vlv.fgi(  58,  37):���� ���������� ������ �������,20FDA71AA104
      !{
      Call BVALVE_MAN(deltat,REAL(R_arof,4),L_ivof,L_ovof
     &,R8_amof,L_obuk,
     & L_eduk,L_apuk,I_itof,I_otof,R_akof,
     & REAL(R_ikof,4),R_elof,REAL(R_olof,4),
     & R_okof,REAL(R_alof,4),I_osof,I_utof,I_etof,I_atof,L_ulof
     &,
     & L_avof,L_ixof,L_ilof,L_ukof,
     & L_umof,L_omof,L_uvof,L_ekof,L_epof,L_abuf,L_evof,
     & L_opof,L_upof,REAL(R8_arul,8),REAL(1.0,4),R8_ivul,L_imof
     &,
     & L_emof,L_oxof,R_isof,REAL(R_apof,4),L_uxof,L_ebuf,L_erof
     &,
     & L_irof,L_orof,L_asof,L_esof,L_urof)
      !}

      if(L_esof.or.L_asof.or.L_urof.or.L_orof.or.L_irof.or.L_erof
     &) then      
                  I_usof = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_usof = z'40000000'
      endif
C FDA70_vlv.fgi(  77,  62):���� ���������� ������ �������,20FDA72AA103
      !{
      Call BVALVE_MAN(deltat,REAL(R_iluf,4),L_uruf,L_asuf
     &,R8_ifuf,L_obuk,
     & L_eduk,L_apuk,I_upuf,I_aruf,R_ibuf,
     & REAL(R_ubuf,4),R_oduf,REAL(R_afuf,4),
     & R_aduf,REAL(R_iduf,4),I_apuf,I_eruf,I_opuf,I_ipuf,L_efuf
     &,
     & L_iruf,L_usuf,L_uduf,L_eduf,
     & L_ekuf,L_akuf,L_esuf,L_obuf,L_okuf,L_ituf,L_oruf,
     & L_aluf,L_eluf,REAL(R8_arul,8),REAL(1.0,4),R8_ivul,L_ufuf
     &,
     & L_ofuf,L_atuf,R_umuf,REAL(R_ikuf,4),L_etuf,L_otuf,L_oluf
     &,
     & L_uluf,L_amuf,L_imuf,L_omuf,L_emuf)
      !}

      if(L_omuf.or.L_imuf.or.L_emuf.or.L_amuf.or.L_uluf.or.L_oluf
     &) then      
                  I_epuf = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_epuf = z'40000000'
      endif
C FDA70_vlv.fgi(  58,  62):���� ���������� ������ �������,20FDA71AA103
      !{
      Call BVALVE_MAN(deltat,REAL(R_udak,4),L_emak,L_imak
     &,R8_uxuf,L_obuk,
     & L_eduk,L_apuk,I_elak,I_ilak,R_utuf,
     & REAL(R_evuf,4),R_axuf,REAL(R_ixuf,4),
     & R_ivuf,REAL(R_uvuf,4),I_ikak,I_olak,I_alak,I_ukak,L_oxuf
     &,
     & L_ulak,L_epak,L_exuf,L_ovuf,
     & L_obak,L_ibak,L_omak,L_avuf,L_adak,L_upak,L_amak,
     & L_idak,L_odak,REAL(R8_arul,8),REAL(1.0,4),R8_ivul,L_ebak
     &,
     & L_abak,L_ipak,R_ekak,REAL(R_ubak,4),L_opak,L_arak,L_afak
     &,
     & L_efak,L_ifak,L_ufak,L_akak,L_ofak)
      !}

      if(L_akak.or.L_ufak.or.L_ofak.or.L_ifak.or.L_efak.or.L_afak
     &) then      
                  I_okak = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_okak = z'40000000'
      endif
C FDA70_vlv.fgi(  37,  37):���� ���������� ������ �������,20FDA72AA102
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_oxak,4),L_akek,L_ekek
     &,R8_etak,C30_ovak,
     & L_obuk,L_eduk,L_apuk,I_afek,I_efek,R_axak,R_uvak,
     & R_erak,REAL(R_orak,4),R_isak,
     & REAL(R_usak,4),R_urak,REAL(R_esak,4),I_edek,
     & I_ifek,I_udek,I_odek,L_atak,L_ofek,L_alek,L_osak,
     & L_asak,L_avak,L_utak,L_ikek,L_irak,L_ivak,
     & L_olek,L_ufek,L_exak,L_ixak,REAL(R8_arul,8),REAL(1.0
     &,4),R8_ivul,
     & L_otak,L_itak,L_elek,R_adek,REAL(R_evak,4),L_ilek,L_ulek
     &,
     & L_uxak,L_abek,L_ebek,L_obek,L_ubek,L_ibek)
      !}

      if(L_ubek.or.L_obek.or.L_ibek.or.L_ebek.or.L_abek.or.L_uxak
     &) then      
                  I_idek = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_idek = z'40000000'
      endif
C FDA70_vlv.fgi(  37,  62):���� ���������� �������� ��������,20FDA72AA104
      R_opel=R_uvak
C FDA70_vent_log.fgi( 180,  99):������,20FDA72CG001XQ01_input
      !{
      Call DAT_ANA_HANDLER(deltat,R_imel,R_esel,REAL(1,4)
     &,
     & REAL(R_apel,4),REAL(R_epel,4),
     & REAL(R_emel,4),REAL(R_amel,4),I_asel,
     & REAL(R_upel,4),L_arel,REAL(R_erel,4),L_irel,L_orel
     &,R_ipel,
     & REAL(R_umel,4),REAL(R_omel,4),L_urel,REAL(R_opel,4
     &))
      !}
C FDA70_vlv.fgi( 120,  74):���������� �������,20FDA72CG001XQ01
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_erik,4),L_ovik,L_uvik
     &,R8_ulik,C30_epik,
     & L_obuk,L_eduk,L_apuk,I_otik,I_utik,R_opik,R_ipik,
     & R_ufik,REAL(R_ekik,4),R_alik,
     & REAL(R_ilik,4),R_ikik,REAL(R_ukik,4),I_usik,
     & I_avik,I_itik,I_etik,L_olik,L_evik,L_oxik,L_elik,
     & L_okik,L_omik,L_imik,L_axik,L_akik,L_apik,
     & L_ebok,L_ivik,L_upik,L_arik,REAL(R8_arul,8),REAL(1.0
     &,4),R8_ivul,
     & L_emik,L_amik,L_uxik,R_osik,REAL(R_umik,4),L_abok,L_ibok
     &,
     & L_irik,L_orik,L_urik,L_esik,L_isik,L_asik)
      !}

      if(L_isik.or.L_esik.or.L_asik.or.L_urik.or.L_orik.or.L_irik
     &) then      
                  I_atik = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_atik = z'40000000'
      endif
C FDA70_vlv.fgi(  16,  62):���� ���������� �������� ��������,20FDA71AA101
      R_avel=R_ipik
C FDA70_vent_log.fgi(  66,  99):������,20FDA71CG001XQ01_input
      !{
      Call DAT_ANA_HANDLER(deltat,R_usel,R_oxel,REAL(1,4)
     &,
     & REAL(R_itel,4),REAL(R_otel,4),
     & REAL(R_osel,4),REAL(R_isel,4),I_ixel,
     & REAL(R_evel,4),L_ivel,REAL(R_ovel,4),L_uvel,L_axel
     &,R_utel,
     & REAL(R_etel,4),REAL(R_atel,4),L_exel,REAL(R_avel,4
     &))
      !}
C FDA70_vlv.fgi( 120,  87):���������� �������,20FDA71CG001XQ01
      !{
      Call BVALVE_MAN(deltat,REAL(R_olok,4),L_asok,L_esok
     &,R8_ofok,L_obuk,
     & L_eduk,L_apuk,I_arok,I_erok,R_obok,
     & REAL(R_adok,4),R_udok,REAL(R_efok,4),
     & R_edok,REAL(R_odok,4),I_epok,I_irok,I_upok,I_opok,L_ifok
     &,
     & L_orok,L_atok,L_afok,L_idok,
     & L_ikok,L_ekok,L_isok,L_ubok,L_ukok,L_otok,L_urok,
     & L_elok,L_ilok,REAL(R8_arul,8),REAL(1.0,4),R8_ivul,L_akok
     &,
     & L_ufok,L_etok,R_apok,REAL(R_okok,4),L_itok,L_utok,L_ulok
     &,
     & L_amok,L_emok,L_omok,L_umok,L_imok)
      !}

      if(L_umok.or.L_omok.or.L_imok.or.L_emok.or.L_amok.or.L_ulok
     &) then      
                  I_ipok = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_ipok = z'40000000'
      endif
C FDA70_vlv.fgi(  16,  37):���� ���������� ������ �������,20FDA71AA102
      L_(90)=I_amam.ne.0
C FDA70_logic.fgi( 223,  80):��������� 1->LO
      L_(89)=I_imam.ne.0
C FDA70_logic.fgi( 223,  84):��������� 1->LO
      L_omam=(L_(89).or.L_omam).and..not.(L_(90))
      L_umam=.not.L_omam
C FDA70_logic.fgi( 233,  82):RS �������
      L_emam=L_omam
C FDA70_logic.fgi( 254,  84):������,20FDA72AE702YU22
      L_(92)=I_epam.ne.0
C FDA70_logic.fgi( 133,  54):��������� 1->LO
      L_(91)=I_ipam.ne.0
C FDA70_logic.fgi( 133,  58):��������� 1->LO
      L_opam=(L_(91).or.L_opam).and..not.(L_(92))
      L_upam=.not.L_opam
C FDA70_logic.fgi( 143,  56):RS �������
      L_apam=L_opam
C FDA70_logic.fgi( 164,  58):������,20FDA71AE701YU21
      L_(93)=I_aram.ne.0
C FDA70_logic.fgi( 127,  68):��������� 1->LO
      L_(96)=I_eram.ne.0
C FDA70_logic.fgi( 127,  77):��������� 1->LO
      L_(100) = L_(96).OR.L_(93)
C FDA70_logic.fgi( 136,  82):���
      L_(99)=I_iram.ne.0
C FDA70_logic.fgi( 127,  86):��������� 1->LO
      L_uram=(L_(99).or.L_uram).and..not.(L_(100))
      L_(101)=.not.L_uram
C FDA70_logic.fgi( 143,  84):RS �������
      L_oram=L_uram
C FDA70_logic.fgi( 164,  86):������,20FDA71AE202YU21
      L_(94) = L_(96).OR.L_(99)
C FDA70_logic.fgi( 136,  64):���
      L_osam=(L_(93).or.L_osam).and..not.(L_(94))
      L_(95)=.not.L_osam
C FDA70_logic.fgi( 143,  66):RS �������
      L_isam=L_osam
C FDA70_logic.fgi( 164,  68):������,20FDA71AE202YU22
      L_(97) = L_(99).OR.L_(93)
C FDA70_logic.fgi( 136,  73):���
      L_esam=(L_(96).or.L_esam).and..not.(L_(97))
      L_(98)=.not.L_esam
C FDA70_logic.fgi( 143,  75):RS �������
      L_asam=L_esam
C FDA70_logic.fgi( 164,  77):������,20FDA71AE202YU23
      L_(102)=I_itam.ne.0
C FDA70_logic.fgi( 217,  94):��������� 1->LO
      L_(105)=I_utam.ne.0
C FDA70_logic.fgi( 217, 103):��������� 1->LO
      L_(109) = L_(105).OR.L_(102)
C FDA70_logic.fgi( 226, 108):���
      L_(108)=I_evam.ne.0
C FDA70_logic.fgi( 217, 112):��������� 1->LO
      L_ivam=(L_(108).or.L_ivam).and..not.(L_(109))
      L_(110)=.not.L_ivam
C FDA70_logic.fgi( 233, 110):RS �������
      L_etam=L_ivam
C FDA70_logic.fgi( 272, 112):������,20FDA72AE702YU27
      L_(103) = L_(105).OR.L_(108)
C FDA70_logic.fgi( 226,  90):���
      L_otam=(L_(102).or.L_otam).and..not.(L_(103))
      L_(104)=.not.L_otam
C FDA70_logic.fgi( 233,  92):RS �������
      L_usam=L_otam
C FDA70_logic.fgi( 272,  94):������,20FDA72AE702YU29
      L_(106) = L_(108).OR.L_(102)
C FDA70_logic.fgi( 226,  99):���
      L_avam=(L_(105).or.L_avam).and..not.(L_(106))
      L_(107)=.not.L_avam
C FDA70_logic.fgi( 233, 101):RS �������
      L_atam=L_avam
C FDA70_logic.fgi( 272, 103):������,20FDA72AE702YU28
      L_(112)=I_uvam.ne.0
C FDA70_logic.fgi( 221, 121):��������� 1->LO
      L_(111)=I_axam.ne.0
C FDA70_logic.fgi( 221, 125):��������� 1->LO
      L_exam=(L_(111).or.L_exam).and..not.(L_(112))
      L_ixam=.not.L_exam
C FDA70_logic.fgi( 231, 123):RS �������
      L_ovam=L_exam
C FDA70_logic.fgi( 252, 125):������,20FDA72AL101YU21
      L_(118)=I_oxam.ne.0
C FDA70_logic.fgi( 127,  98):��������� 1->LO
      L_(117)=I_uxam.ne.0
C FDA70_logic.fgi( 127, 107):��������� 1->LO
      L_(119)=I_edem.ne.0
C FDA70_logic.fgi( 127, 116):��������� 1->LO
      L_(123) = L_(119).OR.L_(118).OR.L_(117)
C FDA70_logic.fgi( 137, 121):���
      L_(122)=I_odem.ne.0
C FDA70_logic.fgi( 127, 125):��������� 1->LO
      L_udem=(L_(122).or.L_udem).and..not.(L_(123))
      L_(124)=.not.L_udem
C FDA70_logic.fgi( 143, 123):RS �������
      L_adem=L_udem
C FDA70_logic.fgi( 168, 125):������,20FDA71AE801YU21
      L_(120) = L_(122).OR.L_(118).OR.L_(117)
C FDA70_logic.fgi( 137, 112):���
      L_idem=(L_(119).or.L_idem).and..not.(L_(120))
      L_(121)=.not.L_idem
C FDA70_logic.fgi( 143, 114):RS �������
      L_ubem=L_idem
C FDA70_logic.fgi( 168, 116):������,20FDA71AE801YU24
      L_(115) = L_(122).OR.L_(119).OR.L_(118)
C FDA70_logic.fgi( 137, 103):���
      L_obem=(L_(117).or.L_obem).and..not.(L_(115))
      L_(116)=.not.L_obem
C FDA70_logic.fgi( 143, 105):RS �������
      L_ibem=L_obem
C FDA70_logic.fgi( 168, 107):������,20FDA71AE801YU22
      L_(113) = L_(122).OR.L_(119).OR.L_(117)
C FDA70_logic.fgi( 137,  94):���
      L_ebem=(L_(118).or.L_ebem).and..not.(L_(113))
      L_(114)=.not.L_ebem
C FDA70_logic.fgi( 143,  96):RS �������
      L_abem=L_ebem
C FDA70_logic.fgi( 168,  98):������,20FDA71AE801YU23
      L_afem=L_efem
C FDA70_logic.fgi(  82,  66):������,20FDA71AA001YA21
      !{
      Call ZATVOR_MAN(deltat,REAL(R_ixul,4),R8_etul,I_udam
     &,I_ifam,I_odam,C8_utul,
     & I_efam,R_uvul,R_ovul,R_erul,REAL(R_orul,4),
     & R_isul,REAL(R_usul,4),R_urul,
     & REAL(R_esul,4),I_edam,I_ofam,I_afam,I_adam,L_atul,L_akam
     &,
     & L_alam,L_osul,L_otul,L_itul,L_ikam,
     & L_asul,L_irul,L_evul,L_olam,L_axul,L_exul,
     & REAL(R8_arul,8),REAL(1.0,4),R8_ivul,L_(88),L_ipul,L_
     &(87),L_afem,
     & L_ekam,I_ufam,L_elam,R_ubam,REAL(R_avul,4),L_ilam,L_opul
     &,L_ulam,
     & L_upul,L_oxul,L_uxul,L_abam,L_ibam,L_obam,L_ebam)
      !}

      if(L_obam.or.L_ibam.or.L_ebam.or.L_abam.or.L_uxul.or.L_oxul
     &) then      
                  I_idam = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_idam = z'40000000'
      endif
C FDA70_vlv.fgi(  95,  88):���� ���������� ��������,20FDA71AA001
      L_(125)=I_ofem.ne.0
C FDA70_logic.fgi(  51,  75):��������� 1->LO
      L_ufem=(L_(125).or.L_ufem).and..not.(L_(125))
      L_(126)=.not.L_ufem
C FDA70_logic.fgi(  61,  73):RS �������
      L_ifem=L_ufem
C FDA70_logic.fgi(  82,  75):������,20FDA73AE002YU21
      L_(127)=I_akem.ne.0
C FDA70_logic.fgi(  47,  84):��������� 1->LO
      L_(130)=I_ekem.ne.0
C FDA70_logic.fgi(  47,  93):��������� 1->LO
      L_(134) = L_(130).OR.L_(127)
C FDA70_logic.fgi(  56,  98):���
      L_(133)=I_ikem.ne.0
C FDA70_logic.fgi(  47, 102):��������� 1->LO
      L_ukem=(L_(133).or.L_ukem).and..not.(L_(134))
      L_(135)=.not.L_ukem
C FDA70_logic.fgi(  63, 100):RS �������
      L_okem=L_ukem
C FDA70_logic.fgi(  84, 102):������,20FDA73AE003YU22
      L_(128) = L_(130).OR.L_(133)
C FDA70_logic.fgi(  56,  80):���
      L_olem=(L_(127).or.L_olem).and..not.(L_(128))
      L_(129)=.not.L_olem
C FDA70_logic.fgi(  63,  82):RS �������
      L_ilem=L_olem
C FDA70_logic.fgi(  84,  84):������,20FDA73AE003YU20
      L_(131) = L_(133).OR.L_(127)
C FDA70_logic.fgi(  56,  89):���
      L_elem=(L_(130).or.L_elem).and..not.(L_(131))
      L_(132)=.not.L_elem
C FDA70_logic.fgi(  63,  91):RS �������
      L_alem=L_elem
C FDA70_logic.fgi(  84,  93):������,20FDA73AE003YU21
      L_ulem=I_asem.eq.I0_amem
C FDA70_logic.fgi(  58, 165):���������� �������������
      L_emem=I_asem.eq.I0_imem
C FDA70_logic.fgi(  58, 171):���������� �������������
      L_(137)=I_umem.ne.0
C FDA70_logic.fgi(  51, 110):��������� 1->LO
      L_(136)=I_apem.ne.0
C FDA70_logic.fgi(  51, 114):��������� 1->LO
      L_epem=(L_(136).or.L_epem).and..not.(L_(137))
      L_(138)=.not.L_epem
C FDA70_logic.fgi(  61, 112):RS �������
      L_omem=L_epem
C FDA70_logic.fgi(  82, 114):������,20FDA71AJ002YA11
      L_(140)=I_opem.ne.0
C FDA70_logic.fgi(  51, 121):��������� 1->LO
      L_(139)=I_upem.ne.0
C FDA70_logic.fgi(  51, 125):��������� 1->LO
      L_arem=(L_(139).or.L_arem).and..not.(L_(140))
      L_erem=.not.L_arem
C FDA70_logic.fgi(  61, 123):RS �������
      L_ipem=L_arem
C FDA70_logic.fgi(  82, 125):������,20FDA71AM001YU21
      R_(37) = 20.0
C FDA70_logic.fgi(  39, 176):��������� (RE4) (�������)
      Call PUMP_HANDLER(deltat,C30_ubuk,I_ofuk,L_obuk,L_eduk
     &,L_apuk,
     & I_ufuk,I_ifuk,R_ixok,REAL(R_uxok,4),
     & R_uvok,REAL(R_exok,4),I_akuk,L_omuk,L_oluk,L_oruk,
     & L_uruk,L_uduk,L_ukuk,L_eluk,L_aluk,L_iluk,L_emuk,L_ovok
     &,
     & L_oxok,L_ivok,L_axok,L_aruk,L_(86),
     & L_eruk,L_(85),L_evok,L_avok,L_afuk,I_ekuk,R_epuk,R_ipuk
     &,
     & L_isuk,L_imuk,L_iruk,REAL(R8_arul,8),L_amuk,
     & REAL(R8_uluk,8),R_asuk,REAL(R_ikuk,4),R_okuk,REAL(R8_oduk
     &,8),R_iduk,
     & R8_ivul,R_esuk,R8_uluk,REAL(R_abuk,4),REAL(R_ebuk,4
     &))
C FDA70_vlv.fgi(  16,  89):���������� ���������� �������,20FDA71CU001KN01
C label 432  try432=try432-1
C sav1=R_esuk
C sav2=R_asuk
C sav3=L_omuk
C sav4=L_emuk
C sav5=L_oluk
C sav6=R8_uluk
C sav7=R_okuk
C sav8=I_ekuk
C sav9=I_akuk
C sav10=I_ufuk
C sav11=I_ofuk
C sav12=I_ifuk
C sav13=I_efuk
C sav14=L_afuk
C sav15=L_uduk
C sav16=R_iduk
C sav17=C30_ubuk
C sav18=L_oxok
C sav19=L_axok
      Call PUMP_HANDLER(deltat,C30_ubuk,I_ofuk,L_obuk,L_eduk
     &,L_apuk,
     & I_ufuk,I_ifuk,R_ixok,REAL(R_uxok,4),
     & R_uvok,REAL(R_exok,4),I_akuk,L_omuk,L_oluk,L_oruk,
     & L_uruk,L_uduk,L_ukuk,L_eluk,L_aluk,L_iluk,L_emuk,L_ovok
     &,
     & L_oxok,L_ivok,L_axok,L_aruk,L_(86),
     & L_eruk,L_(85),L_evok,L_avok,L_afuk,I_ekuk,R_epuk,R_ipuk
     &,
     & L_isuk,L_imuk,L_iruk,REAL(R8_arul,8),L_amuk,
     & REAL(R8_uluk,8),R_asuk,REAL(R_ikuk,4),R_okuk,REAL(R8_oduk
     &,8),R_iduk,
     & R8_ivul,R_esuk,R8_uluk,REAL(R_abuk,4),REAL(R_ebuk,4
     &))
C FDA70_vlv.fgi(  16,  89):recalc:���������� ���������� �������,20FDA71CU001KN01
C if(sav1.ne.R_esuk .and. try432.gt.0) goto 432
C if(sav2.ne.R_asuk .and. try432.gt.0) goto 432
C if(sav3.ne.L_omuk .and. try432.gt.0) goto 432
C if(sav4.ne.L_emuk .and. try432.gt.0) goto 432
C if(sav5.ne.L_oluk .and. try432.gt.0) goto 432
C if(sav6.ne.R8_uluk .and. try432.gt.0) goto 432
C if(sav7.ne.R_okuk .and. try432.gt.0) goto 432
C if(sav8.ne.I_ekuk .and. try432.gt.0) goto 432
C if(sav9.ne.I_akuk .and. try432.gt.0) goto 432
C if(sav10.ne.I_ufuk .and. try432.gt.0) goto 432
C if(sav11.ne.I_ofuk .and. try432.gt.0) goto 432
C if(sav12.ne.I_ifuk .and. try432.gt.0) goto 432
C if(sav13.ne.I_efuk .and. try432.gt.0) goto 432
C if(sav14.ne.L_afuk .and. try432.gt.0) goto 432
C if(sav15.ne.L_uduk .and. try432.gt.0) goto 432
C if(sav16.ne.R_iduk .and. try432.gt.0) goto 432
C if(sav17.ne.C30_ubuk .and. try432.gt.0) goto 432
C if(sav18.ne.L_oxok .and. try432.gt.0) goto 432
C if(sav19.ne.L_axok .and. try432.gt.0) goto 432
      if(L_emuk) then
         R_(34)=R_(35)
      else
         R_(34)=R_(36)
      endif
C FDA70_vent_log.fgi(  50, 273):���� RE IN LO CH7
      R8_ide=(R0_ade*R_(33)+deltat*R_(34))/(R0_ade+deltat
     &)
C FDA70_vent_log.fgi(  62, 274):�������������� �����  
      R8_ede=R8_ide
C FDA70_vent_log.fgi(  80, 274):������,F_FDA71CU001_G
      Call PUMP_HANDLER(deltat,C30_ulo,I_ipo,L_obuk,L_eduk
     &,L_apuk,
     & I_opo,I_epo,R_oko,REAL(R_alo,4),
     & R_ako,REAL(R_iko,4),I_upo,L_eto,L_iso,L_axo,
     & L_exo,L_omo,L_oro,L_aso,L_uro,L_eso,L_ato,L_ufo,
     & L_uko,L_ofo,L_eko,L_ivo,L_(18),
     & L_ovo,L_(17),L_ifo,L_efo,L_umo,I_aro,R_oto,R_uto,
     & L_uxo,L_imuk,L_uvo,REAL(R8_arul,8),L_uso,
     & REAL(R8_oso,8),R_ixo,REAL(R_ero,4),R_iro,REAL(R8_imo
     &,8),R_emo,
     & R8_ivul,R_oxo,R8_oso,REAL(R_elo,4),REAL(R_ilo,4))
C FDA70_vlv.fgi(  23, 151):���������� ���������� �������,20FDA71AM001
C label 440  try440=try440-1
C sav1=R_oxo
C sav2=R_ixo
C sav3=L_eto
C sav4=L_ato
C sav5=L_iso
C sav6=R8_oso
C sav7=R_iro
C sav8=I_aro
C sav9=I_upo
C sav10=I_opo
C sav11=I_ipo
C sav12=I_epo
C sav13=I_apo
C sav14=L_umo
C sav15=L_omo
C sav16=R_emo
C sav17=C30_ulo
C sav18=L_uko
C sav19=L_eko
      Call PUMP_HANDLER(deltat,C30_ulo,I_ipo,L_obuk,L_eduk
     &,L_apuk,
     & I_opo,I_epo,R_oko,REAL(R_alo,4),
     & R_ako,REAL(R_iko,4),I_upo,L_eto,L_iso,L_axo,
     & L_exo,L_omo,L_oro,L_aso,L_uro,L_eso,L_ato,L_ufo,
     & L_uko,L_ofo,L_eko,L_ivo,L_(18),
     & L_ovo,L_(17),L_ifo,L_efo,L_umo,I_aro,R_oto,R_uto,
     & L_uxo,L_imuk,L_uvo,REAL(R8_arul,8),L_uso,
     & REAL(R8_oso,8),R_ixo,REAL(R_ero,4),R_iro,REAL(R8_imo
     &,8),R_emo,
     & R8_ivul,R_oxo,R8_oso,REAL(R_elo,4),REAL(R_ilo,4))
C FDA70_vlv.fgi(  23, 151):recalc:���������� ���������� �������,20FDA71AM001
C if(sav1.ne.R_oxo .and. try440.gt.0) goto 440
C if(sav2.ne.R_ixo .and. try440.gt.0) goto 440
C if(sav3.ne.L_eto .and. try440.gt.0) goto 440
C if(sav4.ne.L_ato .and. try440.gt.0) goto 440
C if(sav5.ne.L_iso .and. try440.gt.0) goto 440
C if(sav6.ne.R8_oso .and. try440.gt.0) goto 440
C if(sav7.ne.R_iro .and. try440.gt.0) goto 440
C if(sav8.ne.I_aro .and. try440.gt.0) goto 440
C if(sav9.ne.I_upo .and. try440.gt.0) goto 440
C if(sav10.ne.I_opo .and. try440.gt.0) goto 440
C if(sav11.ne.I_ipo .and. try440.gt.0) goto 440
C if(sav12.ne.I_epo .and. try440.gt.0) goto 440
C if(sav13.ne.I_apo .and. try440.gt.0) goto 440
C if(sav14.ne.L_umo .and. try440.gt.0) goto 440
C if(sav15.ne.L_omo .and. try440.gt.0) goto 440
C if(sav16.ne.R_emo .and. try440.gt.0) goto 440
C if(sav17.ne.C30_ulo .and. try440.gt.0) goto 440
C if(sav18.ne.L_uko .and. try440.gt.0) goto 440
C if(sav19.ne.L_eko .and. try440.gt.0) goto 440
      Call PUMP_HANDLER(deltat,C30_orek,I_etek,L_obuk,L_eduk
     &,L_apuk,
     & I_itek,I_atek,R_ipek,REAL(R_upek,4),
     & R_umek,REAL(R_epek,4),I_otek,L_abik,L_exek,L_udik,
     & L_afik,L_isek,L_ivek,L_uvek,L_ovek,L_axek,L_uxek,L_omek
     &,
     & L_opek,L_imek,L_apek,L_edik,L_(84),
     & L_idik,L_(83),L_emek,L_amek,L_osek,I_utek,R_ibik,R_obik
     &,
     & L_ofik,L_imuk,L_odik,REAL(R8_arul,8),L_oxek,
     & REAL(R8_ixek,8),R_efik,REAL(R_avek,4),R_evek,REAL(R8_esek
     &,8),R_asek,
     & R8_ivul,R_ifik,R8_ixek,REAL(R_arek,4),REAL(R_erek,4
     &))
C FDA70_vlv.fgi(  37,  89):���������� ���������� �������,20FDA72CU001KN01
C label 441  try441=try441-1
C sav1=R_ifik
C sav2=R_efik
C sav3=L_abik
C sav4=L_uxek
C sav5=L_exek
C sav6=R8_ixek
C sav7=R_evek
C sav8=I_utek
C sav9=I_otek
C sav10=I_itek
C sav11=I_etek
C sav12=I_atek
C sav13=I_usek
C sav14=L_osek
C sav15=L_isek
C sav16=R_asek
C sav17=C30_orek
C sav18=L_opek
C sav19=L_apek
      Call PUMP_HANDLER(deltat,C30_orek,I_etek,L_obuk,L_eduk
     &,L_apuk,
     & I_itek,I_atek,R_ipek,REAL(R_upek,4),
     & R_umek,REAL(R_epek,4),I_otek,L_abik,L_exek,L_udik,
     & L_afik,L_isek,L_ivek,L_uvek,L_ovek,L_axek,L_uxek,L_omek
     &,
     & L_opek,L_imek,L_apek,L_edik,L_(84),
     & L_idik,L_(83),L_emek,L_amek,L_osek,I_utek,R_ibik,R_obik
     &,
     & L_ofik,L_imuk,L_odik,REAL(R8_arul,8),L_oxek,
     & REAL(R8_ixek,8),R_efik,REAL(R_avek,4),R_evek,REAL(R8_esek
     &,8),R_asek,
     & R8_ivul,R_ifik,R8_ixek,REAL(R_arek,4),REAL(R_erek,4
     &))
C FDA70_vlv.fgi(  37,  89):recalc:���������� ���������� �������,20FDA72CU001KN01
C if(sav1.ne.R_ifik .and. try441.gt.0) goto 441
C if(sav2.ne.R_efik .and. try441.gt.0) goto 441
C if(sav3.ne.L_abik .and. try441.gt.0) goto 441
C if(sav4.ne.L_uxek .and. try441.gt.0) goto 441
C if(sav5.ne.L_exek .and. try441.gt.0) goto 441
C if(sav6.ne.R8_ixek .and. try441.gt.0) goto 441
C if(sav7.ne.R_evek .and. try441.gt.0) goto 441
C if(sav8.ne.I_utek .and. try441.gt.0) goto 441
C if(sav9.ne.I_otek .and. try441.gt.0) goto 441
C if(sav10.ne.I_itek .and. try441.gt.0) goto 441
C if(sav11.ne.I_etek .and. try441.gt.0) goto 441
C if(sav12.ne.I_atek .and. try441.gt.0) goto 441
C if(sav13.ne.I_usek .and. try441.gt.0) goto 441
C if(sav14.ne.L_osek .and. try441.gt.0) goto 441
C if(sav15.ne.L_isek .and. try441.gt.0) goto 441
C if(sav16.ne.R_asek .and. try441.gt.0) goto 441
C if(sav17.ne.C30_orek .and. try441.gt.0) goto 441
C if(sav18.ne.L_opek .and. try441.gt.0) goto 441
C if(sav19.ne.L_apek .and. try441.gt.0) goto 441
      if(L_uxek) then
         R_(30)=R_(31)
      else
         R_(30)=R_(32)
      endif
C FDA70_vent_log.fgi( 148, 273):���� RE IN LO CH7
      R8_ibe=(R0_abe*R_(29)+deltat*R_(30))/(R0_abe+deltat
     &)
C FDA70_vent_log.fgi( 160, 274):�������������� �����  
      R8_ebe=R8_ibe
C FDA70_vent_log.fgi( 178, 274):������,F_FDA72CU001_G
      Call PUMP_HANDLER(deltat,C30_ari,I_osi,L_obuk,L_eduk
     &,L_apuk,
     & I_usi,I_isi,R_umi,REAL(R_epi,4),
     & R_emi,REAL(R_omi,4),I_ati,L_ixi,L_ovi,L_edo,
     & L_ido,L_uri,L_uti,L_evi,L_avi,L_ivi,L_exi,L_ami,
     & L_api,L_uli,L_imi,L_obo,L_(16),
     & L_ubo,L_(15),L_oli,L_ili,L_asi,I_eti,R_uxi,R_abo,
     & L_afo,L_imuk,L_ado,REAL(R8_arul,8),L_axi,
     & REAL(R8_uvi,8),R_odo,REAL(R_iti,4),R_oti,REAL(R8_ori
     &,8),R_iri,
     & R8_ivul,R_udo,R8_uvi,REAL(R_ipi,4),REAL(R_opi,4))
C FDA70_vlv.fgi(  44, 151):���������� ���������� �������,20FDA71AJ002
C label 449  try449=try449-1
C sav1=R_udo
C sav2=R_odo
C sav3=L_ixi
C sav4=L_exi
C sav5=L_ovi
C sav6=R8_uvi
C sav7=R_oti
C sav8=I_eti
C sav9=I_ati
C sav10=I_usi
C sav11=I_osi
C sav12=I_isi
C sav13=I_esi
C sav14=L_asi
C sav15=L_uri
C sav16=R_iri
C sav17=C30_ari
C sav18=L_api
C sav19=L_imi
      Call PUMP_HANDLER(deltat,C30_ari,I_osi,L_obuk,L_eduk
     &,L_apuk,
     & I_usi,I_isi,R_umi,REAL(R_epi,4),
     & R_emi,REAL(R_omi,4),I_ati,L_ixi,L_ovi,L_edo,
     & L_ido,L_uri,L_uti,L_evi,L_avi,L_ivi,L_exi,L_ami,
     & L_api,L_uli,L_imi,L_obo,L_(16),
     & L_ubo,L_(15),L_oli,L_ili,L_asi,I_eti,R_uxi,R_abo,
     & L_afo,L_imuk,L_ado,REAL(R8_arul,8),L_axi,
     & REAL(R8_uvi,8),R_odo,REAL(R_iti,4),R_oti,REAL(R8_ori
     &,8),R_iri,
     & R8_ivul,R_udo,R8_uvi,REAL(R_ipi,4),REAL(R_opi,4))
C FDA70_vlv.fgi(  44, 151):recalc:���������� ���������� �������,20FDA71AJ002
C if(sav1.ne.R_udo .and. try449.gt.0) goto 449
C if(sav2.ne.R_odo .and. try449.gt.0) goto 449
C if(sav3.ne.L_ixi .and. try449.gt.0) goto 449
C if(sav4.ne.L_exi .and. try449.gt.0) goto 449
C if(sav5.ne.L_ovi .and. try449.gt.0) goto 449
C if(sav6.ne.R8_uvi .and. try449.gt.0) goto 449
C if(sav7.ne.R_oti .and. try449.gt.0) goto 449
C if(sav8.ne.I_eti .and. try449.gt.0) goto 449
C if(sav9.ne.I_ati .and. try449.gt.0) goto 449
C if(sav10.ne.I_usi .and. try449.gt.0) goto 449
C if(sav11.ne.I_osi .and. try449.gt.0) goto 449
C if(sav12.ne.I_isi .and. try449.gt.0) goto 449
C if(sav13.ne.I_esi .and. try449.gt.0) goto 449
C if(sav14.ne.L_asi .and. try449.gt.0) goto 449
C if(sav15.ne.L_uri .and. try449.gt.0) goto 449
C if(sav16.ne.R_iri .and. try449.gt.0) goto 449
C if(sav17.ne.C30_ari .and. try449.gt.0) goto 449
C if(sav18.ne.L_api .and. try449.gt.0) goto 449
C if(sav19.ne.L_imi .and. try449.gt.0) goto 449
      R_esem = R_esem + R_(37)
C FDA70_logic.fgi(  42, 178):��������
C label 450  try450=try450-1
C sav1=R_esem
      R_esem = R_esem + R_(37)
C FDA70_logic.fgi(  42, 178):recalc:��������
C if(sav1.ne.R_esem .and. try450.gt.0) goto 450
      End
