      Subroutine FDC_MODE_OBJECT(ext_deltat,I_i,I_u,I_ad,I_ed
     &,I_id,I_ud,I_af,I1_of,I1_uf,I_ik,I_ok,I1_il,I_am,I_ap
     &,I1_ep)
C |I_i           |2 4 I|sel_mode_0_object|������� ������ ��������� "�������� ��������� ���� ������\n������ ������ �������"|0|
C |I_u           |2 4 O|bgl_object_mode0|���� ������ ����� ����������� ������ "������������������"||
C |I_ad          |2 4 O|bgu_object_mode0|���� ������� ����� ����������� ������ "������������������"||
C |I_ed          |2 4 O|bgl_object_mode2|���� ������ ����� ����������� ������ "���. ������."||
C |I_id          |2 4 O|bgu_object_mode2|���� ������� ����� ����������� ������ "���. ������."||
C |I_ud          |2 4 O|bgl_object_mode3|���� ������ ����� ����������� ������ "������"||
C |I_af          |2 4 O|bgu_object_mode3|���� ������� ����� ����������� ������ "������"||
C |I1_of         |2 1 O|object_mode0    |���� ���������� ������ "������������������"|0|
C |I1_uf         |2 1 O|object_mode2    |���� ���������� ������ "���. ������."|0|
C |I_ik          |2 4 K|_lcmp1          |�������� ������ �����������|0|
C |I_ok          |2 4 K|_lcmp3          |�������� ������ �����������|2|
C |I1_il         |2 1 O|object_mode3    |���� ���������� ������ "������"|0|
C |I_am          |2 4 K|_lcmp4          |�������� ������ �����������|3|
C |I_ap          |2 4 O|tbg_mode_object |���� ���� ������ ���������� ������ ������ �������||
C |I1_ep         |2 1 O|b_mode_object   |��������� ����� ������|1|

      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      LOGICAL*1 L0_e
      INTEGER*4 I_i
      LOGICAL*1 L0_o
      INTEGER*4 I_u,I_ad,I_ed,I_id,I0_od,I_ud,I_af,I0_ef,I0_if
      INTEGER*1 I1_of,I1_uf
      INTEGER*4 I0_ak
      LOGICAL*1 L0_ek
      INTEGER*4 I_ik,I_ok
      LOGICAL*1 L0_uk
      INTEGER*4 I0_al,I0_el
      INTEGER*1 I1_il
      INTEGER*4 I0_ol,I0_ul,I_am
      LOGICAL*1 L0_em
      INTEGER*4 I0_im,I0_om,I0_um,I_ap
      INTEGER*1 I1_ep

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      L0_o=.false.
C FDC_MODE_OBJECT.fmg(  27, 184):��������� ���������� (�������)
      if(I_i.ne.0) then
         I0_ol=I_i
         L0_e=I_i.ne.0
         I_i=0
      endif
C FDC_MODE_OBJECT.fmg(  35, 186):���������� ��������� ����������������
      I0_od = z'01000020'
C FDC_MODE_OBJECT.fmg(  43, 279):��������� ������������� IN (�������)
      I0_ef = z'01000005'
C FDC_MODE_OBJECT.fmg(  43, 285):��������� ������������� IN (�������)
      I0_if = z'01000023'
C FDC_MODE_OBJECT.fmg(  43, 287):��������� ������������� IN (�������)
      I0_ak = z'01000005'
C FDC_MODE_OBJECT.fmg( 164, 268):��������� ������������� IN (�������)
      I0_el = z'0100002F'
C FDC_MODE_OBJECT.fmg( 138, 269):��������� ������������� IN (�������)
      I0_om = z'01000033'
C FDC_MODE_OBJECT.fmg(  69, 270):��������� ������������� IN (�������)
      I0_um = z'01000009'
C FDC_MODE_OBJECT.fmg(  69, 272):��������� ������������� IN (�������)
      I0_ul = 1
C FDC_MODE_OBJECT.fmg(  48, 193):��������� ������������� IN (�������)
      I1_ep = (-I0_ul) + I0_ol
C FDC_MODE_OBJECT.fmg(  52, 192):��������
      L0_ek=I1_ep.eq.I_ik
C FDC_MODE_OBJECT.fmg(  69, 215):���������� �������������,1
      if(L0_ek) then
         I_ad=I0_ef
      else
         I_ad=I0_if
      endif
C FDC_MODE_OBJECT.fmg(  73, 227):���� RE IN LO CH7
      if(L0_ek) then
         I1_of=1
      else
         I1_of=0
      endif
C FDC_MODE_OBJECT.fmg(  79, 211):��������� LO->1
      if(L0_ek) then
         I_u=I0_od
      else
         I_u=I0_if
      endif
C FDC_MODE_OBJECT.fmg(  82, 221):���� RE IN LO CH7
      L0_uk=I1_ep.eq.I_ok
C FDC_MODE_OBJECT.fmg(  69, 239):���������� �������������,3
      if(L0_uk) then
         I_id=I0_ef
      else
         I_id=I0_if
      endif
C FDC_MODE_OBJECT.fmg(  73, 251):���� RE IN LO CH7
      if(L0_uk) then
         I1_uf=1
      else
         I1_uf=0
      endif
C FDC_MODE_OBJECT.fmg(  79, 235):��������� LO->1
      if(L0_uk) then
         I_ed=I0_od
      else
         I_ed=I0_if
      endif
C FDC_MODE_OBJECT.fmg(  82, 245):���� RE IN LO CH7
      L0_em=I1_ep.eq.I_am
C FDC_MODE_OBJECT.fmg(  69, 261):���������� �������������,4
      if(L0_em) then
         I0_im=I0_om
      else
         I0_im=I0_um
      endif
C FDC_MODE_OBJECT.fmg(  73, 270):���� RE IN LO CH7
      if(L0_uk) then
         I0_al=I0_el
      else
         I0_al=I0_im
      endif
C FDC_MODE_OBJECT.fmg( 141, 269):���� RE IN LO CH7
      if(L0_ek) then
         I_ap=I0_ak
      else
         I_ap=I0_al
      endif
C FDC_MODE_OBJECT.fmg( 167, 268):���� RE IN LO CH7
      if(L0_em) then
         I1_il=1
      else
         I1_il=0
      endif
C FDC_MODE_OBJECT.fmg(  79, 261):��������� LO->1
      if(L0_em) then
         I_af=I0_ef
      else
         I_af=I0_if
      endif
C FDC_MODE_OBJECT.fmg(  83, 285):���� RE IN LO CH7
      if(L0_em) then
         I_ud=I0_od
      else
         I_ud=I0_if
      endif
C FDC_MODE_OBJECT.fmg(  92, 279):���� RE IN LO CH7
      End
