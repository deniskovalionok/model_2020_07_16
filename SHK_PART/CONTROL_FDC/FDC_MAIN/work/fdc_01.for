      Subroutine fdc_01(ext_deltat)
      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      Include 'fdc_01.fh'

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      Call fdc_01_ConIn
      I_(139)=I1_o !CopyBack
C 20FDC_control01.fgi( 142, 710):pre: ������-�������: ���������� ��� �������������� ������
      R_(34)=R_af !CopyBack
C 20FDC_control01.fgi( 142, 234):pre: ������-�������: ���������� ��� �������������� ������
      R_(35)=R_ud !CopyBack
C 20FDC_control01.fgi( 131, 253):pre: ������-�������: ���������� ��� �������������� ������
      R_(36)=R_od !CopyBack
C 20FDC_control01.fgi( 132, 199):pre: ������-�������: ���������� ��� �������������� ������
      I_(146)=I1_e !CopyBack
C 20FDC_control01.fgi(  38, 698):pre: ������-�������: ���������� ��� �������������� ������
      I_(142)=I1_i !CopyBack
C 20FDC_control01.fgi(  38, 667):pre: ������-�������: ���������� ��� �������������� ������
      I_(128)=I1_u !CopyBack
C 20FDC_control01.fgi( 289, 734):pre: ������-�������: ���������� ��� �������������� ������
      I_(123)=I1_ad !CopyBack
C 20FDC_control01.fgi( 366, 734):pre: ������-�������: ���������� ��� �������������� ������
      I_(102)=I1_ed !CopyBack
C 20FDC_control01.fgi( 289, 551):pre: ������-�������: ���������� ��� �������������� ������
      I_(97)=I1_id !CopyBack
C 20FDC_control01.fgi( 366, 551):pre: ������-�������: ���������� ��� �������������� ������
      I_(6)=I2_ef !CopyBack
C 20FDC_control02.fgi(  40, 629):pre: ������-�������: ���������� ��� �������������� ������
      I_(4)=I2_if !CopyBack
C 20FDC_control02.fgi( 141, 629):pre: ������-�������: ���������� ��� �������������� ������
      I_(2)=I2_of !CopyBack
C 20FDC_control02.fgi( 221, 629):pre: ������-�������: ���������� ��� �������������� ������
      !{
      Call KLAPAN_MAN(deltat,REAL(R_akif,4),R8_ubif,I_emif
     &,C8_odif,I_omif,R_ifif,
     & R_efif,R_uvef,REAL(R_exef,4),R_abif,
     & REAL(R_ibif,4),R_ixef,REAL(R_uxef,4),I_ulif,
     & I_umif,I_imif,I_olif,L_obif,L_epif,L_erif,L_ebif,
     & L_oxef,REAL(R_udif,4),L_edif,L_adif,L_opif,
     & L_axef,L_afif,L_urif,L_ofif,L_ufif,REAL(R8_atif,8)
     &,
     & REAL(1.0,4),R8_abof,L_(41),L_avef,L_(42),L_evef,L_ipif
     &,I_apif,L_irif,
     & R_ilif,REAL(R_idif,4),L_orif,L_ivef,L_asif,L_ovef,L_ekif
     &,L_ikif,
     & L_okif,L_alif,L_elif,L_ukif)
      !}

      if(L_elif.or.L_alif.or.L_ukif.or.L_okif.or.L_ikif.or.L_ekif
     &) then      
                  I_amif = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_amif = z'40000000'
      endif
C 20FDC_control01.fgi(  20, 242):���� ���������� ��������,VE2
      !{
      Call KLAPAN_MAN(deltat,REAL(R_isud,4),R8_epud,I_ovud
     &,C8_arud,I_axud,R_urud,
     & R_orud,R_elud,REAL(R_olud,4),R_imud,
     & REAL(R_umud,4),R_ulud,REAL(R_emud,4),I_evud,
     & I_exud,I_uvud,I_avud,L_apud,L_oxud,L_obaf,L_omud,
     & L_amud,REAL(R_erud,4),L_opud,L_ipud,L_abaf,
     & L_ilud,L_irud,L_edaf,L_asud,L_esud,REAL(R8_atif,8)
     &,
     & REAL(1.0,4),R8_abof,L_(35),L_ikud,L_(36),L_okud,L_uxud
     &,I_ixud,L_ubaf,
     & R_utud,REAL(R_upud,4),L_adaf,L_ukud,L_idaf,L_alud,L_osud
     &,L_usud,
     & L_atud,L_itud,L_otud,L_etud)
      !}

      if(L_otud.or.L_itud.or.L_etud.or.L_atud.or.L_usud.or.L_osud
     &) then      
                  I_ivud = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_ivud = z'40000000'
      endif
C 20FDC_control01.fgi(  35, 218):���� ���������� ��������,PC2
      !{
      Call KLAPAN_MAN(deltat,REAL(R_opaf,4),R8_ilaf,I_usaf
     &,C8_emaf,I_etaf,R_apaf,
     & R_umaf,R_ifaf,REAL(R_ufaf,4),R_okaf,
     & REAL(R_alaf,4),R_akaf,REAL(R_ikaf,4),I_isaf,
     & I_itaf,I_ataf,I_esaf,L_elaf,L_utaf,L_uvaf,L_ukaf,
     & L_ekaf,REAL(R_imaf,4),L_ulaf,L_olaf,L_evaf,
     & L_ofaf,L_omaf,L_ixaf,L_epaf,L_ipaf,REAL(R8_atif,8)
     &,
     & REAL(1.0,4),R8_abof,L_(37),L_odaf,L_(38),L_udaf,L_avaf
     &,I_otaf,L_axaf,
     & R_asaf,REAL(R_amaf,4),L_exaf,L_afaf,L_oxaf,L_efaf,L_upaf
     &,L_araf,
     & L_eraf,L_oraf,L_uraf,L_iraf)
      !}

      if(L_uraf.or.L_oraf.or.L_iraf.or.L_eraf.or.L_araf.or.L_upaf
     &) then      
                  I_osaf = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_osaf = z'40000000'
      endif
C 20FDC_control01.fgi(  20, 218):���� ���������� ��������,PC1
      !{
      Call KLAPAN_MAN(deltat,REAL(R_ulef,4),R8_ofef,I_aref
     &,C8_ikef,I_iref,R_elef,
     & R_alef,R_obef,REAL(R_adef,4),R_udef,
     & REAL(R_efef,4),R_edef,REAL(R_odef,4),I_opef,
     & I_oref,I_eref,I_ipef,L_ifef,L_asef,L_atef,L_afef,
     & L_idef,REAL(R_okef,4),L_akef,L_ufef,L_isef,
     & L_ubef,L_ukef,L_otef,L_ilef,L_olef,REAL(R8_atif,8)
     &,
     & REAL(1.0,4),R8_abof,L_(39),L_uxaf,L_(40),L_abef,L_esef
     &,I_uref,L_etef,
     & R_epef,REAL(R_ekef,4),L_itef,L_ebef,L_utef,L_ibef,L_amef
     &,L_emef,
     & L_imef,L_umef,L_apef,L_omef)
      !}

      if(L_apef.or.L_umef.or.L_omef.or.L_imef.or.L_emef.or.L_amef
     &) then      
                  I_upef = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_upef = z'40000000'
      endif
C 20FDC_control01.fgi(  35, 242):���� ���������� ��������,VP1
      R_(1) = 10000.0
C 20FDC_vlv.fgi( 166, 231):��������� (RE4) (�������)
      if(R_(1).ge.0.0) then
         R8_uf=R8_ak/max(R_(1),1.0e-10)
      else
         R8_uf=R8_ak/min(R_(1),-1.0e-10)
      endif
C 20FDC_vlv.fgi( 171, 234):�������� ����������
      R_(2) = 10000.0
C 20FDC_vlv.fgi( 166, 240):��������� (RE4) (�������)
      if(R_(2).ge.0.0) then
         R8_ek=R8_ik/max(R_(2),1.0e-10)
      else
         R8_ek=R8_ik/min(R_(2),-1.0e-10)
      endif
C 20FDC_vlv.fgi( 171, 243):�������� ����������
      R_(3) = 100.0
C 20FDC_vlv.fgi(  48, 131):��������� (RE4) (�������)
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_es,4),L_ox,L_ux,R8_um
     &,C30_er,
     & L_ired,L_ased,L_ubid,I_ov,I_uv,R_or,R_ir,
     & R_uk,REAL(R_el,4),R_am,
     & REAL(R_im,4),R_il,REAL(R_ul,4),I_ut,
     & I_ax,I_iv,I_ev,L_om,L_ex,L_obe,L_em,
     & L_ol,L_op,L_ip,L_abe,L_al,L_ar,
     & L_ede,L_ix,L_ur,L_as,REAL(R8_atif,8),REAL(1.0,4),R8_abof
     &,
     & L_ep,L_ap,L_ube,R_ot,REAL(R_up,4),L_ade,L_ide,
     & L_is,L_os,L_us,L_et,L_it,L_at)
      !}

      if(L_it.or.L_et.or.L_at.or.L_us.or.L_os.or.L_is) then
     &      
                  I_av = ior(z'000000FF',z'04000000')    
     &                        
                else
                   I_av = z'40000000'
      endif
C 20FDC_vlv.fgi(  54, 147):���� ���������� �������� ��������,20FDC70AA006
      R_(4) = R8_um
C 20FDC_vlv.fgi(  47, 134):��������
      R_ok = R_(4) * R_(3)
C 20FDC_vlv.fgi(  54, 133):����������
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_ape,4),L_ite,L_ote,R8_oke
     &,C30_ame,
     & L_ired,L_ased,L_ubid,I_ise,I_ose,R_ime,R_eme,
     & R_ode,REAL(R_afe,4),R_ufe,
     & REAL(R_eke,4),R_efe,REAL(R_ofe,4),I_ore,
     & I_use,I_ese,I_ase,L_ike,L_ate,L_ive,L_ake,
     & L_ife,L_ile,L_ele,L_ute,L_ude,L_ule,
     & L_axe,L_ete,L_ome,L_ume,REAL(R8_atif,8),REAL(1.0,4
     &),R8_abof,
     & L_ale,L_uke,L_ove,R_ire,REAL(R_ole,4),L_uve,L_exe,
     & L_epe,L_ipe,L_ope,L_are,L_ere,L_upe)
      !}

      if(L_ere.or.L_are.or.L_upe.or.L_ope.or.L_ipe.or.L_epe
     &) then      
                  I_ure = ior(z'000000FF',z'04000000')   
     &                         
                else
                   I_ure = z'40000000'
      endif
C 20FDC_vlv.fgi(  87, 223):���� ���������� �������� ��������,20FDC70AA005
      R_(5) = 100.0
C 20FDC_vlv.fgi(  49, 171):��������� (RE4) (�������)
      R_ulad = R8_oxe
C 20FDC_vlv.fgi( 171, 249):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_ipad,4),REAL
     &(R_ulad,4),R_okad,
     & R_opad,REAL(1.0,4),REAL(R_elad,4),
     & REAL(R_ilad,4),REAL(R_ikad,4),
     & REAL(R_ekad,4),I_epad,REAL(R_amad,4),L_emad,
     & REAL(R_imad,4),L_omad,L_umad,R_olad,REAL(R_alad,4)
     &,REAL(R_ukad,4),L_apad)
      !}
C 20FDC_vlv.fgi( 170, 259):���������� ������� ��������,20FDC70CP200XQ01
      R_(7) = 100.0
C 20FDC_vlv.fgi(  50, 244):��������� (RE4) (�������)
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_ili,4),L_uri,L_asi,R8_afi
     &,C30_iki,
     & L_ired,L_ased,L_ubid,I_upi,I_ari,R_uki,R_oki,
     & R_abi,REAL(R_ibi,4),R_edi,
     & REAL(R_odi,4),R_obi,REAL(R_adi,4),I_api,
     & I_eri,I_opi,I_ipi,L_udi,L_iri,L_usi,L_idi,
     & L_ubi,L_ufi,L_ofi,L_esi,L_ebi,L_eki,
     & L_iti,L_ori,L_ali,L_eli,REAL(R8_atif,8),REAL(1.0,4
     &),R8_abof,
     & L_ifi,L_efi,L_ati,R_umi,REAL(R_aki,4),L_eti,L_oti,
     & L_oli,L_uli,L_ami,L_imi,L_omi,L_emi)
      !}

      if(L_omi.or.L_imi.or.L_emi.or.L_ami.or.L_uli.or.L_oli
     &) then      
                  I_epi = ior(z'000000FF',z'04000000')   
     &                         
                else
                   I_epi = z'40000000'
      endif
C 20FDC_vlv.fgi(  55, 187):���� ���������� �������� ��������,20FDC70AA004
      R_(6) = R8_afi
C 20FDC_vlv.fgi(  48, 174):��������
      R_ixe = R_(6) * R_(5)
C 20FDC_vlv.fgi(  55, 173):����������
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_efo,4),L_omo,L_umo,R8_uxi
     &,C30_edo,
     & L_ired,L_ased,L_ubid,I_olo,I_ulo,R_odo,R_ido,
     & R_uti,REAL(R_evi,4),R_axi,
     & REAL(R_ixi,4),R_ivi,REAL(R_uvi,4),I_uko,
     & I_amo,I_ilo,I_elo,L_oxi,L_emo,L_opo,L_exi,
     & L_ovi,L_obo,L_ibo,L_apo,L_avi,L_ado,
     & L_ero,L_imo,L_udo,L_afo,REAL(R8_atif,8),REAL(1.0,4
     &),R8_abof,
     & L_ebo,L_abo,L_upo,R_oko,REAL(R_ubo,4),L_aro,L_iro,
     & L_ifo,L_ofo,L_ufo,L_eko,L_iko,L_ako)
      !}

      if(L_iko.or.L_eko.or.L_ako.or.L_ufo.or.L_ofo.or.L_ifo
     &) then      
                  I_alo = ior(z'000000FF',z'04000000')   
     &                         
                else
                   I_alo = z'40000000'
      endif
C 20FDC_vlv.fgi(  72, 223):���� ���������� �������� ��������,20FDC70AA003
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_abu,4),L_iku,L_oku,R8_oto
     &,C30_axo,
     & L_ired,L_ased,L_ubid,I_ifu,I_ofu,R_ixo,R_exo,
     & R_oro,REAL(R_aso,4),R_uso,
     & REAL(R_eto,4),R_eso,REAL(R_oso,4),I_odu,
     & I_ufu,I_efu,I_afu,L_ito,L_aku,L_ilu,L_ato,
     & L_iso,L_ivo,L_evo,L_uku,L_uro,L_uvo,
     & L_amu,L_eku,L_oxo,L_uxo,REAL(R8_atif,8),REAL(1.0,4
     &),R8_abof,
     & L_avo,L_uto,L_olu,R_idu,REAL(R_ovo,4),L_ulu,L_emu,
     & L_ebu,L_ibu,L_obu,L_adu,L_edu,L_ubu)
      !}

      if(L_edu.or.L_adu.or.L_ubu.or.L_obu.or.L_ibu.or.L_ebu
     &) then      
                  I_udu = ior(z'000000FF',z'04000000')   
     &                         
                else
                   I_udu = z'40000000'
      endif
C 20FDC_vlv.fgi(  55, 223):���� ���������� �������� ��������,20FDC70AA008
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_utu,4),L_edad,L_idad
     &,R8_iru,C30_usu,
     & L_ired,L_ased,L_ubid,I_ebad,I_ibad,R_etu,R_atu,
     & R_imu,REAL(R_umu,4),R_opu,
     & REAL(R_aru,4),R_apu,REAL(R_ipu,4),I_ixu,
     & I_obad,I_abad,I_uxu,L_eru,L_ubad,L_efad,L_upu,
     & L_epu,L_esu,L_asu,L_odad,L_omu,L_osu,
     & L_ufad,L_adad,L_itu,L_otu,REAL(R8_atif,8),REAL(1.0
     &,4),R8_abof,
     & L_uru,L_oru,L_ifad,R_exu,REAL(R_isu,4),L_ofad,L_akad
     &,
     & L_avu,L_evu,L_ivu,L_uvu,L_axu,L_ovu)
      !}

      if(L_axu.or.L_uvu.or.L_ovu.or.L_ivu.or.L_evu.or.L_avu
     &) then      
                  I_oxu = ior(z'000000FF',z'04000000')   
     &                         
                else
                   I_oxu = z'40000000'
      endif
C 20FDC_vlv.fgi(  56, 261):���� ���������� �������� ��������,20FDC70DP003
      R_(8) = R8_iru
C 20FDC_vlv.fgi(  49, 247):��������
      R_uxe = R_(8) * R_(7)
C 20FDC_vlv.fgi(  56, 246):����������
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_ixad,4),L_ufed,L_aked
     &,R8_atad,C30_ivad,
     & L_ired,L_ased,L_ubid,I_uded,I_afed,R_uvad,R_ovad,
     & R_arad,REAL(R_irad,4),R_esad,
     & REAL(R_osad,4),R_orad,REAL(R_asad,4),I_aded,
     & I_efed,I_oded,I_ided,L_usad,L_ifed,L_uked,L_isad,
     & L_urad,L_utad,L_otad,L_eked,L_erad,L_evad,
     & L_iled,L_ofed,L_axad,L_exad,REAL(R8_atif,8),REAL(1.0
     &,4),R8_abof,
     & L_itad,L_etad,L_aled,R_ubed,REAL(R_avad,4),L_eled,L_oled
     &,
     & L_oxad,L_uxad,L_abed,L_ibed,L_obed,L_ebed)
      !}

      if(L_obed.or.L_ibed.or.L_ebed.or.L_abed.or.L_uxad.or.L_oxad
     &) then      
                  I_eded = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_eded = z'40000000'
      endif
C 20FDC_vlv.fgi(  38, 223):���� ���������� �������� ��������,20FDC70AA007
      I_(1) = 000
C 20FDC_control02.fgi( 217, 630):��������� ������������� IN (�������)
      L0_ikid=R0_ukid.ne.R0_okid
      R0_okid=R0_ukid
C 20FDC_control02.fgi( 202, 623):���������� ������������� ������
      if(L0_ikid) then
         I_(2)=I_(1)
      endif
C 20FDC_control02.fgi( 221, 629):���� � ������������� �������
      I2_of=I_(2)
C 20FDC_control02.fgi( 221, 629):������-�������: ���������� ��� �������������� ������
      I_(3) = 000
C 20FDC_control02.fgi( 137, 630):��������� ������������� IN (�������)
      L0_elid=R0_olid.ne.R0_ilid
      R0_ilid=R0_olid
C 20FDC_control02.fgi( 122, 623):���������� ������������� ������
      if(L0_elid) then
         I_(4)=I_(3)
      endif
C 20FDC_control02.fgi( 141, 629):���� � ������������� �������
      I2_if=I_(4)
C 20FDC_control02.fgi( 141, 629):������-�������: ���������� ��� �������������� ������
      I_(5) = 000
C 20FDC_control02.fgi(  36, 630):��������� ������������� IN (�������)
      L0_amid=R0_imid.ne.R0_emid
      R0_emid=R0_imid
C 20FDC_control02.fgi(  21, 623):���������� ������������� ������
      if(L0_amid) then
         I_(6)=I_(5)
      endif
C 20FDC_control02.fgi(  40, 629):���� � ������������� �������
      I2_ef=I_(6)
C 20FDC_control02.fgi(  40, 629):������-�������: ���������� ��� �������������� ������
      I_(7) = z'01000010'
C 20FDC_control02.fgi( 261, 800):��������� ������������� IN (�������)
      I_(8) = z'01000009'
C 20FDC_control02.fgi( 261, 802):��������� ������������� IN (�������)
      L_(5)=.false.
C 20FDC_control02.fgi( 236, 783):��������� ���������� (�������)
      if(L_(5)) then
         I0_ipid=0
      endif
      I1_opid=I0_ipid
      L_(4)=I0_ipid.ne.0
C 20FDC_control02.fgi( 246, 785):���������� ��������� 
      L_(3)=I1_opid.eq.I0_apid
C 20FDC_control02.fgi( 260, 792):���������� �������������
      if(L_(3)) then
         I_epid=I_(7)
      else
         I_epid=I_(8)
      endif
C 20FDC_control02.fgi( 265, 800):���� RE IN LO CH7
      I_(9) = z'01000010'
C 20FDC_control02.fgi( 151, 764):��������� ������������� IN (�������)
      I_(10) = z'01000009'
C 20FDC_control02.fgi( 151, 766):��������� ������������� IN (�������)
      I_(11) = z'01000010'
C 20FDC_control02.fgi( 151, 800):��������� ������������� IN (�������)
      I_(12) = z'01000009'
C 20FDC_control02.fgi( 151, 802):��������� ������������� IN (�������)
      L_(9)=.false.
C 20FDC_control02.fgi( 126, 747):��������� ���������� (�������)
      if(L_(9)) then
         I0_usid=0
      endif
      I1_atid=I0_usid
      L_(8)=I0_usid.ne.0
C 20FDC_control02.fgi( 136, 749):���������� ��������� 
      L_(6)=I1_atid.eq.I0_asid
C 20FDC_control02.fgi( 150, 756):���������� �������������
      if(L_(6)) then
         I_esid=I_(9)
      else
         I_esid=I_(10)
      endif
C 20FDC_control02.fgi( 155, 764):���� RE IN LO CH7
      L_(11)=.false.
C 20FDC_control02.fgi( 126, 783):��������� ���������� (�������)
      if(L_(11)) then
         I0_etid=0
      endif
      I1_itid=I0_etid
      L_(10)=I0_etid.ne.0
C 20FDC_control02.fgi( 136, 785):���������� ��������� 
      L_(7)=I1_itid.eq.I0_isid
C 20FDC_control02.fgi( 150, 792):���������� �������������
      if(L_(7)) then
         I_osid=I_(11)
      else
         I_osid=I_(12)
      endif
C 20FDC_control02.fgi( 155, 800):���� RE IN LO CH7
      R_(9) = 100.0
C 20FDC_control01.fgi( 107, 143):��������� (RE4) (�������)
      R_(14) = 1.0
C 20FDC_control01.fgi(  50, 167):��������� (RE4) (�������)
      R_(12) = 0.0
C 20FDC_control01.fgi(  54, 167):��������� (RE4) (�������)
      R_(10) = 1.0
C 20FDC_control01.fgi(  41, 149):��������� (RE4) (�������)
      R_(13) = R8_axid + R_(10)
C 20FDC_control01.fgi(  44, 151):��������
      R_(11) = -1.0
C 20FDC_control01.fgi(  41, 153):��������� (RE4) (�������)
      R_(15) = R8_axid + R_(11)
C 20FDC_control01.fgi(  44, 155):��������
      if(R8_asod.le.R_(15)) then
         R_(24)=R_(14)
      elseif(R8_asod.gt.R_(13)) then
         R_(24)=R_(12)
      else
         R_(24)=R_(14)+(R8_asod-(R_(15)))*(R_(12)-(R_(14)
     &))/(R_(13)-(R_(15)))
      endif
C 20FDC_control01.fgi(  53, 161):��������������� ��������� �����������
      R_(25) = R_(24) * R8_urod
C 20FDC_control01.fgi(  64, 147):����������
      R_(17) = 749.0
C 20FDC_control01.fgi( 144, 341):��������� (RE4) (�������)
      R_(19) = 0.0
C 20FDC_control01.fgi( 140, 341):��������� (RE4) (�������)
      R_(18) = 100000.0
C 20FDC_control01.fgi( 146, 329):��������� (RE4) (�������)
      R_(20) = 0.0
C 20FDC_control01.fgi( 142, 329):��������� (RE4) (�������)
      L0_esod=btest(I0_ixid,0)
C 20FDC_control01.fgi(  65, 132):���������� ������ � ���������
      I_(14) = z'01000000'
C 20FDC_control01.fgi(  88, 107):��������� ������������� IN (�������)
      I_(13) = z'0100000A'
C 20FDC_control01.fgi(  88, 105):��������� ������������� IN (�������)
      if(R8_erod.le.R0_upod) then
         R_(21)=R0_opod
      elseif(R8_erod.gt.R0_ipod) then
         R_(21)=R0_epod
      else
         R_(21)=R0_opod+(R8_erod-(R0_upod))*(R0_epod-(R0_opod
     &))/(R0_ipod-(R0_upod))
      endif
C 20FDC_control01.fgi(  46, 451):��������������� ���������
      R_(22) = R_(21) * R8_arod
C 20FDC_control01.fgi(  59, 449):����������
      R_(23) = 0.0
C 20FDC_control01.fgi(  73, 450):��������� (RE4) (�������)
      if(L_orod) then
         R8_irod=R_(22)
      else
         R8_irod=R_(23)
      endif
C 20FDC_control01.fgi(  79, 449):���� RE IN LO CH7
      R_(26) = 0.0
C 20FDC_control01.fgi(  78, 148):��������� (RE4) (�������)
      if(L0_esod) then
         R8_isod=R_(25)
      else
         R8_isod=R_(26)
      endif
C 20FDC_control01.fgi(  84, 147):���� RE IN LO CH7
      if(R8_urod.ge.0.0) then
         R_(16)=R8_isod/max(R8_urod,1.0e-10)
      else
         R_(16)=R8_isod/min(R8_urod,-1.0e-10)
      endif
C 20FDC_control01.fgi(  96, 142):�������� ����������
      R8_exid = R_(9) * R_(16)
C 20FDC_control01.fgi( 110, 143):����������
      R_(27) = 0.1
C 20FDC_control01.fgi( 125, 200):��������� (RE4) (�������)
      R_(29) = 0.1
C 20FDC_control01.fgi( 125, 235):��������� (RE4) (�������)
      L0_utod=R0_evod.ne.R0_avod
      R0_avod=R0_evod
C 20FDC_control01.fgi(  81, 230):���������� ������������� ������
      R_(31) = 0.1
C 20FDC_control01.fgi( 124, 254):��������� (RE4) (�������)
      L0_ivod=R0_uvod.ne.R0_ovod
      R0_ovod=R0_uvod
C 20FDC_control01.fgi(  81, 249):���������� ������������� ������
      I_(15) = z'0100002F'
C 20FDC_control01.fgi( 174, 746):��������� ������������� IN (�������)
      I_(17) = z'01000033'
C 20FDC_control01.fgi( 153, 747):��������� ������������� IN (�������)
      I_(18) = z'01000009'
C 20FDC_control01.fgi( 153, 749):��������� ������������� IN (�������)
      I_(19) = 3
C 20FDC_control01.fgi( 139, 696):��������� ������������� IN (�������)
      L_(30)=I1_edud.eq.I0_abud
C 20FDC_control01.fgi( 121, 688):���������� �������������
      L_(29)=L_(30).and..not.L0_oxod
      L0_oxod=L_(30)
C 20FDC_control01.fgi( 136, 688):������������  �� 1 ���
      if(L_(29)) then
         I_(139)=I_(19)
      endif
C 20FDC_control01.fgi( 142, 695):���� � ������������� �������
      I_(20) = 1
C 20FDC_control01.fgi( 128, 725):��������� ������������� IN (�������)
      I_(21) = 0
C 20FDC_control01.fgi( 139, 711):��������� ������������� IN (�������)
      L_(33)=I1_edud.eq.I0_ubud
C 20FDC_control01.fgi( 121, 705):���������� �������������
      L_(31) = L_(33).AND.(.NOT.L_obud)
C 20FDC_control01.fgi( 129, 704):�
      L_(32)=L_(31).and..not.L0_uxod
      L0_uxod=L_(31)
C 20FDC_control01.fgi( 136, 704):������������  �� 1 ���
      if(L_(32)) then
         I_(139)=I_(21)
      endif
C 20FDC_control01.fgi( 142, 710):���� � ������������� �������
      L0_ufud=R0_afud.ne.R0_udud
      R0_udud=R0_afud
C 20FDC_control01.fgi(  22,  92):���������� ������������� ������
      L0_ofud=R0_ifud.ne.R0_efud
      R0_efud=R0_ifud
C 20FDC_control01.fgi(  22, 104):���������� ������������� ������
      L_akud=(L0_ofud.or.L_akud).and..not.(L0_ufud)
      L_(34)=.not.L_akud
C 20FDC_control01.fgi(  43, 102):RS �������
      L_odud=L_akud
C 20FDC_control01.fgi(  60, 104):������,NV1_on
      if(L_odud) then
         I_apod=I_(13)
      else
         I_apod=I_(14)
      endif
C 20FDC_control01.fgi(  95, 105):���� RE IN LO CH7
      L_idud=L_akud
C 20FDC_control01.fgi(  60, 100):������,NV2_on
      if(L_idud) then
         I_umod=I_(13)
      else
         I_umod=I_(14)
      endif
C 20FDC_control01.fgi(  95,  94):���� RE IN LO CH7
      R_(33) = R8_ekud
C 20FDC_control01.fgi( 136, 335):��������
      if(R_(33).le.R_(20)) then
         R_efuf=R_(19)
      elseif(R_(33).gt.R_(18)) then
         R_efuf=R_(17)
      else
         R_efuf=R_(19)+(R_(33)-(R_(20)))*(R_(17)-(R_(19))
     &)/(R_(18)-(R_(20)))
      endif
C 20FDC_control01.fgi( 143, 335):��������������� ��������� �����������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_ukuf,4),REAL
     &(R_efuf,4),R_aduf,
     & R_aluf,REAL(1,4),REAL(R_oduf,4),
     & REAL(R_uduf,4),REAL(R_ubuf,4),
     & REAL(R_obuf,4),I_okuf,REAL(R_ifuf,4),L_ofuf,
     & REAL(R_ufuf,4),L_akuf,L_ekuf,R_afuf,REAL(R_iduf,4)
     &,REAL(R_eduf,4),L_ikuf)
      !}
C 20FDC_control01.fgi( 144, 314):���������� ������� ��������,p_ac301
      L_(60)=R_afuf.gt.R_obuf
C 20FDC_control01.fgi(  51, 301):���������� >
      L_(59)=R_afuf.gt.R_ubuf
C 20FDC_control01.fgi(  51, 296):���������� >
      R_apuf = R8_epof
C 20FDC_control01.fgi(  56, 401):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_uluf,R_oruf,REAL(1,4)
     &,
     & REAL(R_imuf,4),REAL(R_omuf,4),
     & REAL(R_oluf,4),REAL(R_iluf,4),I_iruf,
     & REAL(R_epuf,4),L_ipuf,REAL(R_opuf,4),L_upuf,L_aruf
     &,R_umuf,
     & REAL(R_emuf,4),REAL(R_amuf,4),L_eruf,REAL(R_apuf,4
     &))
      !}
C 20FDC_control01.fgi(  60, 391):���������� �������,t_ac301
      L_(64)=R_umuf.gt.R_iluf
C 20FDC_control01.fgi(  51, 370):���������� >
      L_(63)=R_umuf.gt.R_oluf
C 20FDC_control01.fgi(  51, 365):���������� >
      L_(62)=R_umuf.lt.R_imuf
C 20FDC_control01.fgi(  51, 346):���������� <
      L_(61)=R_umuf.lt.R_omuf
C 20FDC_control01.fgi(  51, 341):���������� <
      L0_erof=R0_opof.ne.R0_ipof
      R0_ipof=R0_opof
C 20FDC_control01.fgi( 142, 601):���������� ������������� ������
      L_(46)=.false.
C 20FDC_control01.fgi( 173, 599):��������� ���������� (�������)
      L_(45)=.false.
C 20FDC_control01.fgi( 173, 603):��������� ���������� (�������)
      if(L_(45).or.L_(46)) then
         L0_irof=(L_(45).or.L0_irof).and..not.(L_(46))
      else
         if(L0_erof.and..not.L0_arof) L0_irof=.not.L0_irof
      endif
      L0_arof=L0_erof
      L_(47)=.not.L0_irof
C 20FDC_control01.fgi( 179, 601):T �������
      if(L0_irof) then
         I_upof=1
      else
         I_upof=0
      endif
C 20FDC_control01.fgi( 194, 603):��������� LO->1
      L0_isof=R0_urof.ne.R0_orof
      R0_orof=R0_urof
C 20FDC_control01.fgi( 142, 614):���������� ������������� ������
      L_(49)=.false.
C 20FDC_control01.fgi( 173, 612):��������� ���������� (�������)
      L_(48)=.false.
C 20FDC_control01.fgi( 173, 616):��������� ���������� (�������)
      if(L_(48).or.L_(49)) then
         L0_osof=(L_(48).or.L0_osof).and..not.(L_(49))
      else
         if(L0_isof.and..not.L0_esof) L0_osof=.not.L0_osof
      endif
      L0_esof=L0_isof
      L_(50)=.not.L0_osof
C 20FDC_control01.fgi( 179, 614):T �������
      if(L0_osof) then
         I_asof=1
      else
         I_asof=0
      endif
C 20FDC_control01.fgi( 194, 616):��������� LO->1
      L0_otof=R0_atof.ne.R0_usof
      R0_usof=R0_atof
C 20FDC_control01.fgi( 137, 384):���������� ������������� ������
      L_(52)=.false.
C 20FDC_control01.fgi( 168, 382):��������� ���������� (�������)
      L_(51)=.false.
C 20FDC_control01.fgi( 168, 386):��������� ���������� (�������)
      if(L_(51).or.L_(52)) then
         L0_utof=(L_(51).or.L0_utof).and..not.(L_(52))
      else
         if(L0_otof.and..not.L0_itof) L0_utof=.not.L0_utof
      endif
      L0_itof=L0_otof
      L_(53)=.not.L0_utof
C 20FDC_control01.fgi( 174, 384):T �������
      if(L0_utof) then
         I_etof=1
      else
         I_etof=0
      endif
C 20FDC_control01.fgi( 189, 386):��������� LO->1
      L0_uvof=R0_evof.ne.R0_avof
      R0_avof=R0_evof
C 20FDC_control01.fgi( 137, 397):���������� ������������� ������
      L_(55)=.false.
C 20FDC_control01.fgi( 168, 395):��������� ���������� (�������)
      L_(54)=.false.
C 20FDC_control01.fgi( 168, 399):��������� ���������� (�������)
      if(L_(54).or.L_(55)) then
         L0_axof=(L_(54).or.L0_axof).and..not.(L_(55))
      else
         if(L0_uvof.and..not.L0_ovof) L0_axof=.not.L0_axof
      endif
      L0_ovof=L0_uvof
      L_(56)=.not.L0_axof
C 20FDC_control01.fgi( 174, 397):T �������
      if(L0_axof) then
         I_ivof=1
      else
         I_ivof=0
      endif
C 20FDC_control01.fgi( 189, 399):��������� LO->1
      I_(23) = z'01000007'
C 20FDC_control01.fgi(  71, 284):��������� ������������� IN (�������)
      I_(25) = z'01000010'
C 20FDC_control01.fgi(  52, 285):��������� ������������� IN (�������)
      I_(26) = z'01000009'
C 20FDC_control01.fgi(  52, 287):��������� ������������� IN (�������)
      I_(27) = z'01000007'
C 20FDC_control01.fgi(  71, 308):��������� ������������� IN (�������)
      I_(29) = z'01000010'
C 20FDC_control01.fgi(  52, 309):��������� ������������� IN (�������)
      I_(30) = z'01000009'
C 20FDC_control01.fgi(  52, 311):��������� ������������� IN (�������)
      if(L_(60)) then
         I_(28)=I_(29)
      else
         I_(28)=I_(30)
      endif
C 20FDC_control01.fgi(  57, 309):���� RE IN LO CH7
      if(L_(59)) then
         I_oxof=I_(27)
      else
         I_oxof=I_(28)
      endif
C 20FDC_control01.fgi(  76, 308):���� RE IN LO CH7
      I_(31) = z'01000007'
C 20FDC_control01.fgi(  71, 353):��������� ������������� IN (�������)
      I_(33) = z'01000010'
C 20FDC_control01.fgi(  52, 354):��������� ������������� IN (�������)
      I_(34) = z'01000009'
C 20FDC_control01.fgi(  52, 356):��������� ������������� IN (�������)
      if(L_(62)) then
         I_(32)=I_(33)
      else
         I_(32)=I_(34)
      endif
C 20FDC_control01.fgi(  57, 354):���� RE IN LO CH7
      if(L_(61)) then
         I_uxof=I_(31)
      else
         I_uxof=I_(32)
      endif
C 20FDC_control01.fgi(  76, 353):���� RE IN LO CH7
      I_(35) = z'01000007'
C 20FDC_control01.fgi(  71, 377):��������� ������������� IN (�������)
      I_(37) = z'01000010'
C 20FDC_control01.fgi(  52, 378):��������� ������������� IN (�������)
      I_(38) = z'01000009'
C 20FDC_control01.fgi(  52, 380):��������� ������������� IN (�������)
      if(L_(64)) then
         I_(36)=I_(37)
      else
         I_(36)=I_(38)
      endif
C 20FDC_control01.fgi(  57, 378):���� RE IN LO CH7
      if(L_(63)) then
         I_abuf=I_(35)
      else
         I_abuf=I_(36)
      endif
C 20FDC_control01.fgi(  76, 377):���� RE IN LO CH7
      L_(66)=.false.
C 20FDC_control01.fgi(  17, 321):��������� ���������� (�������)
      if(L_(66)) then
         I0_ebuf=0
      endif
      I_(39)=I0_ebuf
      L_(65)=I0_ebuf.ne.0
C 20FDC_control01.fgi(  27, 323):���������� ��������� 
      L_(68)=.false.
C 20FDC_control01.fgi(  17, 389):��������� ���������� (�������)
      if(L_(68)) then
         I0_ibuf=0
      endif
      I_(40)=I0_ibuf
      L_(67)=I0_ibuf.ne.0
C 20FDC_control01.fgi(  27, 391):���������� ��������� 
      I_(41) = z'01000007'
C 20FDC_control01.fgi(  71, 506):��������� ������������� IN (�������)
      I_(43) = z'01000010'
C 20FDC_control01.fgi(  52, 507):��������� ������������� IN (�������)
      I_(44) = z'01000009'
C 20FDC_control01.fgi(  52, 509):��������� ������������� IN (�������)
      I_(45) = z'01000007'
C 20FDC_control01.fgi(  71, 530):��������� ������������� IN (�������)
      I_(47) = z'01000010'
C 20FDC_control01.fgi(  52, 531):��������� ������������� IN (�������)
      I_(48) = z'01000009'
C 20FDC_control01.fgi(  52, 533):��������� ������������� IN (�������)
      I_(49) = z'01000007'
C 20FDC_control01.fgi(  71, 575):��������� ������������� IN (�������)
      I_(51) = z'01000010'
C 20FDC_control01.fgi(  52, 576):��������� ������������� IN (�������)
      I_(52) = z'01000009'
C 20FDC_control01.fgi(  52, 578):��������� ������������� IN (�������)
      I_(53) = z'01000007'
C 20FDC_control01.fgi(  71, 599):��������� ������������� IN (�������)
      I_(55) = z'01000010'
C 20FDC_control01.fgi(  52, 600):��������� ������������� IN (�������)
      I_(56) = z'01000009'
C 20FDC_control01.fgi(  52, 602):��������� ������������� IN (�������)
      L_(78)=.false.
C 20FDC_control01.fgi(  17, 543):��������� ���������� (�������)
      if(L_(78)) then
         I0_atuf=0
      endif
      I_(57)=I0_atuf
      L_(77)=I0_atuf.ne.0
C 20FDC_control01.fgi(  27, 545):���������� ��������� 
      L_(80)=.false.
C 20FDC_control01.fgi(  17, 611):��������� ���������� (�������)
      if(L_(80)) then
         I0_etuf=0
      endif
      I_(58)=I0_etuf
      L_(79)=I0_etuf.ne.0
C 20FDC_control01.fgi(  27, 613):���������� ��������� 
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_adak,4),REAL
     &(R_ixuf,4),R_evuf,
     & R_edak,REAL(1,4),REAL(R_uvuf,4),
     & REAL(R_axuf,4),REAL(R_avuf,4),
     & REAL(R_utuf,4),I_ubak,REAL(R_oxuf,4),L_uxuf,
     & REAL(R_abak,4),L_ebak,L_ibak,R_exuf,REAL(R_ovuf,4)
     &,REAL(R_ivuf,4),L_obak)
      !}
C 20FDC_control01.fgi(  60, 546):���������� ������� ��������,p_shkav_20FDC10-1
      L_(72)=R_exuf.gt.R_utuf
C 20FDC_control01.fgi(  51, 523):���������� >
      if(L_(72)) then
         I_(46)=I_(47)
      else
         I_(46)=I_(48)
      endif
C 20FDC_control01.fgi(  57, 531):���� RE IN LO CH7
      L_(71)=R_exuf.gt.R_avuf
C 20FDC_control01.fgi(  51, 518):���������� >
      if(L_(71)) then
         I_isuf=I_(45)
      else
         I_isuf=I_(46)
      endif
C 20FDC_control01.fgi(  76, 530):���� RE IN LO CH7
      L_(70)=R_exuf.lt.R_uvuf
C 20FDC_control01.fgi(  51, 499):���������� <
      if(L_(70)) then
         I_(42)=I_(43)
      else
         I_(42)=I_(44)
      endif
C 20FDC_control01.fgi(  57, 507):���� RE IN LO CH7
      L_(58)=R_exuf.lt.R_uvuf
C 20FDC_control01.fgi(  51, 277):���������� <
      if(L_(58)) then
         I_(24)=I_(25)
      else
         I_(24)=I_(26)
      endif
C 20FDC_control01.fgi(  57, 285):���� RE IN LO CH7
      L_(69)=R_exuf.lt.R_axuf
C 20FDC_control01.fgi(  51, 494):���������� <
      if(L_(69)) then
         I_esuf=I_(41)
      else
         I_esuf=I_(42)
      endif
C 20FDC_control01.fgi(  76, 506):���� RE IN LO CH7
      L_(57)=R_exuf.lt.R_axuf
C 20FDC_control01.fgi(  51, 272):���������� <
      if(L_(57)) then
         I_ixof=I_(23)
      else
         I_ixof=I_(24)
      endif
C 20FDC_control01.fgi(  76, 284):���� RE IN LO CH7
      !{
      Call DAT_ANA_HANDLER(deltat,R_afak,R_ulak,REAL(1,4)
     &,
     & REAL(R_ofak,4),REAL(R_ufak,4),
     & REAL(R_udak,4),REAL(R_odak,4),I_olak,
     & REAL(R_ikak,4),L_okak,REAL(R_ukak,4),L_alak,L_elak
     &,R_akak,
     & REAL(R_ifak,4),REAL(R_efak,4),L_ilak,REAL(R_ekak,4
     &))
      !}
C 20FDC_control01.fgi(  60, 613):���������� �������,t_shkav_20FDC10-1
      L_(76)=R_akak.gt.R_odak
C 20FDC_control01.fgi(  51, 592):���������� >
      if(L_(76)) then
         I_(54)=I_(55)
      else
         I_(54)=I_(56)
      endif
C 20FDC_control01.fgi(  57, 600):���� RE IN LO CH7
      L_(75)=R_akak.gt.R_udak
C 20FDC_control01.fgi(  51, 587):���������� >
      if(L_(75)) then
         I_usuf=I_(53)
      else
         I_usuf=I_(54)
      endif
C 20FDC_control01.fgi(  76, 599):���� RE IN LO CH7
      L_(74)=R_akak.lt.R_ofak
C 20FDC_control01.fgi(  51, 568):���������� <
      if(L_(74)) then
         I_(50)=I_(51)
      else
         I_(50)=I_(52)
      endif
C 20FDC_control01.fgi(  57, 576):���� RE IN LO CH7
      L_(73)=R_akak.lt.R_ufak
C 20FDC_control01.fgi(  51, 563):���������� <
      if(L_(73)) then
         I_osuf=I_(49)
      else
         I_osuf=I_(50)
      endif
C 20FDC_control01.fgi(  76, 575):���� RE IN LO CH7
      L0_umak=R0_epak.ne.R0_apak
      R0_apak=R0_epak
C 20FDC_control01.fgi( 653,  20):���������� ������������� ������
      Call FDC_MODE_SECTOR(deltat,INT(I_arak,4),I_atak,
     & I_esak,I_etak,I_isak,I_usak,
     & I_asak,I_osak,I_urak,I1_umil,
     & I_ivak,I1_orak,I1_irak,I_ovak,I_evak,I1_erak,
     & I_avak,I_opak,I1_itak)
C 20FDC_control01.fgi( 214, 790):���������� ������ �������,20FDC10
      I1_omil=I1_umil
C 20FDC_control01.fgi( 140, 782):������,b_mode_20FDC10
      L_(174)=I1_umil.eq.I0_emil
C 20FDC_control01.fgi( 138, 788):���������� �������������
      Call FDC_MODE_OBJECT(deltat,INT(I_ibod,4),I_ifod,
     & I_udod,I_efod,I_odod,I_afod,
     & I_idod,I1_edod,I1_adod,I_omod,I_imod,
     & I1_ubod,I_emod,I_abod,I1_ofod)
C 20FDC_control01.fgi( 210, 706):���������� ������ �������,20FDC10AC301
      L_(13)=I1_ubod.ne.0
C 20FDC_control01.fgi( 142, 683):��������� 1->LO
      L_(12)=I1_adod.ne.0
C 20FDC_control01.fgi( 142, 677):��������� 1->LO
      L_(14) = L_(13).OR.L_(12)
C 20FDC_control01.fgi( 154, 682):���
      if(L_(14)) then
         I1_oxid=1
      else
         I1_oxid=0
      endif
C 20FDC_control01.fgi( 165, 682):��������� LO->1
      L0_uvak=R0_exak.ne.R0_axak
      R0_axak=R0_exak
C 20FDC_control01.fgi( 637,  20):���������� ������������� ������
      I_(59) = z'01000010'
C 20FDC_control01.fgi(1057, 170):��������� ������������� IN (�������)
      I_(60) = z'01000009'
C 20FDC_control01.fgi(1057, 172):��������� ������������� IN (�������)
      L_(83)=.false.
C 20FDC_control01.fgi(1032, 153):��������� ���������� (�������)
      if(L_(83)) then
         I0_uxak=0
      endif
      I1_abek=I0_uxak
      L_(82)=I0_uxak.ne.0
C 20FDC_control01.fgi(1042, 155):���������� ��������� 
      L_(81)=I1_abek.eq.I0_ixak
C 20FDC_control01.fgi(1056, 162):���������� �������������
      if(L_(81)) then
         I_oxak=I_(59)
      else
         I_oxak=I_(60)
      endif
C 20FDC_control01.fgi(1061, 170):���� RE IN LO CH7
      I_(61) = z'01000010'
C 20FDC_control01.fgi( 927, 170):��������� ������������� IN (�������)
      I_(62) = z'01000009'
C 20FDC_control01.fgi( 927, 172):��������� ������������� IN (�������)
      L_(86)=.false.
C 20FDC_control01.fgi( 902, 153):��������� ���������� (�������)
      if(L_(86)) then
         I0_ubek=0
      endif
      I1_adek=I0_ubek
      L_(85)=I0_ubek.ne.0
C 20FDC_control01.fgi( 912, 155):���������� ��������� 
      L_(84)=I1_adek.eq.I0_ibek
C 20FDC_control01.fgi( 926, 162):���������� �������������
      if(L_(84)) then
         I_obek=I_(61)
      else
         I_obek=I_(62)
      endif
C 20FDC_control01.fgi( 931, 170):���� RE IN LO CH7
      I_(63) = z'01000010'
C 20FDC_control01.fgi( 787, 170):��������� ������������� IN (�������)
      I_(64) = z'01000009'
C 20FDC_control01.fgi( 787, 172):��������� ������������� IN (�������)
      L_(89)=.false.
C 20FDC_control01.fgi( 762, 153):��������� ���������� (�������)
      if(L_(89)) then
         I0_udek=0
      endif
      I1_afek=I0_udek
      L_(88)=I0_udek.ne.0
C 20FDC_control01.fgi( 772, 155):���������� ��������� 
      L_(87)=I1_afek.eq.I0_idek
C 20FDC_control01.fgi( 786, 162):���������� �������������
      if(L_(87)) then
         I_odek=I_(63)
      else
         I_odek=I_(64)
      endif
C 20FDC_control01.fgi( 791, 170):���� RE IN LO CH7
      I_(65) = z'01000010'
C 20FDC_control01.fgi(1057, 246):��������� ������������� IN (�������)
      I_(66) = z'01000009'
C 20FDC_control01.fgi(1057, 248):��������� ������������� IN (�������)
      L_(92)=.false.
C 20FDC_control01.fgi(1032, 229):��������� ���������� (�������)
      if(L_(92)) then
         I0_ufek=0
      endif
      I1_akek=I0_ufek
      L_(91)=I0_ufek.ne.0
C 20FDC_control01.fgi(1042, 231):���������� ��������� 
      L_(90)=I1_akek.eq.I0_ifek
C 20FDC_control01.fgi(1056, 238):���������� �������������
      if(L_(90)) then
         I_ofek=I_(65)
      else
         I_ofek=I_(66)
      endif
C 20FDC_control01.fgi(1061, 246):���� RE IN LO CH7
      I_(67) = z'01000010'
C 20FDC_control01.fgi( 927, 210):��������� ������������� IN (�������)
      I_(68) = z'01000009'
C 20FDC_control01.fgi( 927, 212):��������� ������������� IN (�������)
      L_(95)=.false.
C 20FDC_control01.fgi( 902, 193):��������� ���������� (�������)
      if(L_(95)) then
         I0_ukek=0
      endif
      I1_alek=I0_ukek
      L_(94)=I0_ukek.ne.0
C 20FDC_control01.fgi( 912, 195):���������� ��������� 
      L_(93)=I1_alek.eq.I0_ikek
C 20FDC_control01.fgi( 926, 202):���������� �������������
      if(L_(93)) then
         I_okek=I_(67)
      else
         I_okek=I_(68)
      endif
C 20FDC_control01.fgi( 931, 210):���� RE IN LO CH7
      I_(69) = z'01000010'
C 20FDC_control01.fgi( 927, 246):��������� ������������� IN (�������)
      I_(70) = z'01000009'
C 20FDC_control01.fgi( 927, 248):��������� ������������� IN (�������)
      L_(98)=.false.
C 20FDC_control01.fgi( 902, 229):��������� ���������� (�������)
      if(L_(98)) then
         I0_olek=0
      endif
      I1_ulek=I0_olek
      L_(97)=I0_olek.ne.0
C 20FDC_control01.fgi( 912, 231):���������� ��������� 
      L_(96)=I1_ulek.eq.I0_elek
C 20FDC_control01.fgi( 926, 238):���������� �������������
      if(L_(96)) then
         I_ilek=I_(69)
      else
         I_ilek=I_(70)
      endif
C 20FDC_control01.fgi( 931, 246):���� RE IN LO CH7
      I_(71) = z'01000010'
C 20FDC_control01.fgi( 787, 210):��������� ������������� IN (�������)
      I_(72) = z'01000009'
C 20FDC_control01.fgi( 787, 212):��������� ������������� IN (�������)
      L_(101)=.false.
C 20FDC_control01.fgi( 762, 193):��������� ���������� (�������)
      if(L_(101)) then
         I0_okik=0
      endif
      I1_ukik=I0_okik
      L_(100)=I0_okik.ne.0
C 20FDC_control01.fgi( 772, 195):���������� ��������� 
      L_(99)=I1_ukik.eq.I0_ekik
C 20FDC_control01.fgi( 786, 202):���������� �������������
      if(L_(99)) then
         I_ikik=I_(71)
      else
         I_ikik=I_(72)
      endif
C 20FDC_control01.fgi( 791, 210):���� RE IN LO CH7
      I_(73) = z'01000010'
C 20FDC_control01.fgi( 787, 246):��������� ������������� IN (�������)
      I_(74) = z'01000009'
C 20FDC_control01.fgi( 787, 248):��������� ������������� IN (�������)
      L_(104)=.false.
C 20FDC_control01.fgi( 762, 229):��������� ���������� (�������)
      if(L_(104)) then
         I0_ilik=0
      endif
      I1_olik=I0_ilik
      L_(103)=I0_ilik.ne.0
C 20FDC_control01.fgi( 772, 231):���������� ��������� 
      L_(102)=I1_olik.eq.I0_alik
C 20FDC_control01.fgi( 786, 238):���������� �������������
      if(L_(102)) then
         I_elik=I_(73)
      else
         I_elik=I_(74)
      endif
C 20FDC_control01.fgi( 791, 246):���� RE IN LO CH7
      I_(75) = z'01000010'
C 20FDC_control01.fgi( 677, 210):��������� ������������� IN (�������)
      I_(76) = z'01000009'
C 20FDC_control01.fgi( 677, 212):��������� ������������� IN (�������)
      I_(77) = z'01000010'
C 20FDC_control01.fgi( 677, 246):��������� ������������� IN (�������)
      I_(78) = z'01000009'
C 20FDC_control01.fgi( 677, 248):��������� ������������� IN (�������)
      L_(108)=.false.
C 20FDC_control01.fgi( 652, 193):��������� ���������� (�������)
      if(L_(108)) then
         I0_arik=0
      endif
      I1_erik=I0_arik
      L_(107)=I0_arik.ne.0
C 20FDC_control01.fgi( 662, 195):���������� ��������� 
      L_(105)=I1_erik.eq.I0_epik
C 20FDC_control01.fgi( 676, 202):���������� �������������
      if(L_(105)) then
         I_ipik=I_(75)
      else
         I_ipik=I_(76)
      endif
C 20FDC_control01.fgi( 681, 210):���� RE IN LO CH7
      L_(110)=.false.
C 20FDC_control01.fgi( 652, 229):��������� ���������� (�������)
      if(L_(110)) then
         I0_irik=0
      endif
      I1_orik=I0_irik
      L_(109)=I0_irik.ne.0
C 20FDC_control01.fgi( 662, 231):���������� ��������� 
      L_(106)=I1_orik.eq.I0_opik
C 20FDC_control01.fgi( 676, 238):���������� �������������
      if(L_(106)) then
         I_upik=I_(77)
      else
         I_upik=I_(78)
      endif
C 20FDC_control01.fgi( 681, 246):���� RE IN LO CH7
      I_(79) = z'01000010'
C 20FDC_control01.fgi( 787, 398):��������� ������������� IN (�������)
      I_(80) = z'01000009'
C 20FDC_control01.fgi( 787, 400):��������� ������������� IN (�������)
      L_(113)=.false.
C 20FDC_control01.fgi( 762, 381):��������� ���������� (�������)
      if(L_(113)) then
         I0_exik=0
      endif
      I1_ixik=I0_exik
      L_(112)=I0_exik.ne.0
C 20FDC_control01.fgi( 772, 383):���������� ��������� 
      L_(111)=I1_ixik.eq.I0_uvik
C 20FDC_control01.fgi( 786, 390):���������� �������������
      if(L_(111)) then
         I_axik=I_(79)
      else
         I_axik=I_(80)
      endif
C 20FDC_control01.fgi( 791, 398):���� RE IN LO CH7
      I_(81) = z'01000010'
C 20FDC_control01.fgi( 787, 434):��������� ������������� IN (�������)
      I_(82) = z'01000009'
C 20FDC_control01.fgi( 787, 436):��������� ������������� IN (�������)
      L_(116)=.false.
C 20FDC_control01.fgi( 762, 417):��������� ���������� (�������)
      if(L_(116)) then
         I0_abok=0
      endif
      I1_ebok=I0_abok
      L_(115)=I0_abok.ne.0
C 20FDC_control01.fgi( 772, 419):���������� ��������� 
      L_(114)=I1_ebok.eq.I0_oxik
C 20FDC_control01.fgi( 786, 426):���������� �������������
      if(L_(114)) then
         I_uxik=I_(81)
      else
         I_uxik=I_(82)
      endif
C 20FDC_control01.fgi( 791, 434):���� RE IN LO CH7
      I_(83) = z'01000010'
C 20FDC_control01.fgi( 677, 398):��������� ������������� IN (�������)
      I_(84) = z'01000009'
C 20FDC_control01.fgi( 677, 400):��������� ������������� IN (�������)
      I_(85) = z'01000010'
C 20FDC_control01.fgi( 677, 434):��������� ������������� IN (�������)
      I_(86) = z'01000009'
C 20FDC_control01.fgi( 677, 436):��������� ������������� IN (�������)
      L_(120)=.false.
C 20FDC_control01.fgi( 652, 381):��������� ���������� (�������)
      if(L_(120)) then
         I0_ofok=0
      endif
      I1_ufok=I0_ofok
      L_(119)=I0_ofok.ne.0
C 20FDC_control01.fgi( 662, 383):���������� ��������� 
      L_(117)=I1_ufok.eq.I0_udok
C 20FDC_control01.fgi( 676, 390):���������� �������������
      if(L_(117)) then
         I_afok=I_(83)
      else
         I_afok=I_(84)
      endif
C 20FDC_control01.fgi( 681, 398):���� RE IN LO CH7
      L_(122)=.false.
C 20FDC_control01.fgi( 652, 417):��������� ���������� (�������)
      if(L_(122)) then
         I0_akok=0
      endif
      I1_ekok=I0_akok
      L_(121)=I0_akok.ne.0
C 20FDC_control01.fgi( 662, 419):���������� ��������� 
      L_(118)=I1_ekok.eq.I0_efok
C 20FDC_control01.fgi( 676, 426):���������� �������������
      if(L_(118)) then
         I_ifok=I_(85)
      else
         I_ifok=I_(86)
      endif
C 20FDC_control01.fgi( 681, 434):���� RE IN LO CH7
      I_(87) = z'01000010'
C 20FDC_control01.fgi( 386,  65):��������� ������������� IN (�������)
      I_(88) = z'01000009'
C 20FDC_control01.fgi( 386,  67):��������� ������������� IN (�������)
      L_(125)=.false.
C 20FDC_control01.fgi( 361,  48):��������� ���������� (�������)
      if(L_(125)) then
         I0_emok=0
      endif
      I1_imok=I0_emok
      L_(124)=I0_emok.ne.0
C 20FDC_control01.fgi( 371,  50):���������� ��������� 
      L_(123)=I1_imok.eq.I0_ulok
C 20FDC_control01.fgi( 385,  57):���������� �������������
      if(L_(123)) then
         I_amok=I_(87)
      else
         I_amok=I_(88)
      endif
C 20FDC_control01.fgi( 390,  65):���� RE IN LO CH7
      I_(89) = z'01000010'
C 20FDC_control01.fgi( 903, 579):��������� ������������� IN (�������)
      I_(90) = z'01000009'
C 20FDC_control01.fgi( 903, 581):��������� ������������� IN (�������)
      L_(128)=.false.
C 20FDC_control01.fgi( 878, 562):��������� ���������� (�������)
      if(L_(128)) then
         I0_asok=0
      endif
      I1_esok=I0_asok
      L_(127)=I0_asok.ne.0
C 20FDC_control01.fgi( 888, 564):���������� ��������� 
      L_(126)=I1_esok.eq.I0_orok
C 20FDC_control01.fgi( 902, 571):���������� �������������
      if(L_(126)) then
         I_urok=I_(89)
      else
         I_urok=I_(90)
      endif
C 20FDC_control01.fgi( 907, 579):���� RE IN LO CH7
      I_(91) = z'01000010'
C 20FDC_control01.fgi( 903, 615):��������� ������������� IN (�������)
      I_(92) = z'01000009'
C 20FDC_control01.fgi( 903, 617):��������� ������������� IN (�������)
      L_(131)=.false.
C 20FDC_control01.fgi( 878, 598):��������� ���������� (�������)
      if(L_(131)) then
         I0_atok=0
      endif
      I1_etok=I0_atok
      L_(130)=I0_atok.ne.0
C 20FDC_control01.fgi( 888, 600):���������� ��������� 
      L_(129)=I1_etok.eq.I0_osok
C 20FDC_control01.fgi( 902, 607):���������� �������������
      if(L_(129)) then
         I_usok=I_(91)
      else
         I_usok=I_(92)
      endif
C 20FDC_control01.fgi( 907, 615):���� RE IN LO CH7
      I_(93) = 4
C 20FDC_control01.fgi( 362, 513):��������� ������������� IN (�������)
      I_(94) = 3
C 20FDC_control01.fgi( 362, 526):��������� ������������� IN (�������)
      I_(95) = 2
C 20FDC_control01.fgi( 362, 539):��������� ������������� IN (�������)
      I_(96) = 1
C 20FDC_control01.fgi( 362, 552):��������� ������������� IN (�������)
      L0_itok=R0_utok.ne.R0_otok
      R0_otok=R0_utok
C 20FDC_control01.fgi( 349, 507):���������� ������������� ������
      if(L0_itok) then
         I_(97)=I_(93)
      endif
C 20FDC_control01.fgi( 366, 512):���� � ������������� �������
      L0_avok=R0_ivok.ne.R0_evok
      R0_evok=R0_ivok
C 20FDC_control01.fgi( 349, 520):���������� ������������� ������
      if(L0_avok) then
         I_(97)=I_(94)
      endif
C 20FDC_control01.fgi( 366, 525):���� � ������������� �������
      L0_ovok=R0_axok.ne.R0_uvok
      R0_uvok=R0_axok
C 20FDC_control01.fgi( 349, 533):���������� ������������� ������
      if(L0_ovok) then
         I_(97)=I_(95)
      endif
C 20FDC_control01.fgi( 366, 538):���� � ������������� �������
      L0_exok=R0_oxok.ne.R0_ixok
      R0_ixok=R0_oxok
C 20FDC_control01.fgi( 349, 546):���������� ������������� ������
      if(L0_exok) then
         I_(97)=I_(96)
      endif
C 20FDC_control01.fgi( 366, 551):���� � ������������� �������
      I1_id=I_(97)
C 20FDC_control01.fgi( 366, 551):������-�������: ���������� ��� �������������� ������
      I_(98) = 4
C 20FDC_control01.fgi( 285, 513):��������� ������������� IN (�������)
      I_(99) = 3
C 20FDC_control01.fgi( 285, 526):��������� ������������� IN (�������)
      I_(100) = 2
C 20FDC_control01.fgi( 285, 539):��������� ������������� IN (�������)
      I_(101) = 1
C 20FDC_control01.fgi( 285, 552):��������� ������������� IN (�������)
      L0_eduk=R0_oduk.ne.R0_iduk
      R0_iduk=R0_oduk
C 20FDC_control01.fgi( 272, 507):���������� ������������� ������
      if(L0_eduk) then
         I_(102)=I_(98)
      endif
C 20FDC_control01.fgi( 289, 512):���� � ������������� �������
      L0_uduk=R0_efuk.ne.R0_afuk
      R0_afuk=R0_efuk
C 20FDC_control01.fgi( 272, 520):���������� ������������� ������
      if(L0_uduk) then
         I_(102)=I_(99)
      endif
C 20FDC_control01.fgi( 289, 525):���� � ������������� �������
      L0_ifuk=R0_ufuk.ne.R0_ofuk
      R0_ofuk=R0_ufuk
C 20FDC_control01.fgi( 272, 533):���������� ������������� ������
      if(L0_ifuk) then
         I_(102)=I_(100)
      endif
C 20FDC_control01.fgi( 289, 538):���� � ������������� �������
      L0_akuk=R0_ikuk.ne.R0_ekuk
      R0_ekuk=R0_ikuk
C 20FDC_control01.fgi( 272, 546):���������� ������������� ������
      if(L0_akuk) then
         I_(102)=I_(101)
      endif
C 20FDC_control01.fgi( 289, 551):���� � ������������� �������
      I1_ed=I_(102)
C 20FDC_control01.fgi( 289, 551):������-�������: ���������� ��� �������������� ������
      I_(103) = z'01000010'
C 20FDC_control01.fgi( 787, 579):��������� ������������� IN (�������)
      I_(104) = z'01000009'
C 20FDC_control01.fgi( 787, 581):��������� ������������� IN (�������)
      L_(134)=.false.
C 20FDC_control01.fgi( 762, 562):��������� ���������� (�������)
      if(L_(134)) then
         I0_aluk=0
      endif
      I1_eluk=I0_aluk
      L_(133)=I0_aluk.ne.0
C 20FDC_control01.fgi( 772, 564):���������� ��������� 
      L_(132)=I1_eluk.eq.I0_okuk
C 20FDC_control01.fgi( 786, 571):���������� �������������
      if(L_(132)) then
         I_ukuk=I_(103)
      else
         I_ukuk=I_(104)
      endif
C 20FDC_control01.fgi( 791, 579):���� RE IN LO CH7
      I_(105) = z'01000010'
C 20FDC_control01.fgi( 787, 615):��������� ������������� IN (�������)
      I_(106) = z'01000009'
C 20FDC_control01.fgi( 787, 617):��������� ������������� IN (�������)
      L_(137)=.false.
C 20FDC_control01.fgi( 762, 598):��������� ���������� (�������)
      if(L_(137)) then
         I0_uluk=0
      endif
      I1_amuk=I0_uluk
      L_(136)=I0_uluk.ne.0
C 20FDC_control01.fgi( 772, 600):���������� ��������� 
      L_(135)=I1_amuk.eq.I0_iluk
C 20FDC_control01.fgi( 786, 607):���������� �������������
      if(L_(135)) then
         I_oluk=I_(105)
      else
         I_oluk=I_(106)
      endif
C 20FDC_control01.fgi( 791, 615):���� RE IN LO CH7
      I_(107) = z'01000010'
C 20FDC_control01.fgi( 677, 579):��������� ������������� IN (�������)
      I_(108) = z'01000009'
C 20FDC_control01.fgi( 677, 581):��������� ������������� IN (�������)
      I_(109) = z'01000010'
C 20FDC_control01.fgi( 677, 615):��������� ������������� IN (�������)
      I_(110) = z'01000009'
C 20FDC_control01.fgi( 677, 617):��������� ������������� IN (�������)
      L_(141)=.false.
C 20FDC_control01.fgi( 652, 562):��������� ���������� (�������)
      if(L_(141)) then
         I0_evuk=0
      endif
      I1_ivuk=I0_evuk
      L_(140)=I0_evuk.ne.0
C 20FDC_control01.fgi( 662, 564):���������� ��������� 
      L_(138)=I1_ivuk.eq.I0_ituk
C 20FDC_control01.fgi( 676, 571):���������� �������������
      if(L_(138)) then
         I_otuk=I_(107)
      else
         I_otuk=I_(108)
      endif
C 20FDC_control01.fgi( 681, 579):���� RE IN LO CH7
      L_(143)=.false.
C 20FDC_control01.fgi( 652, 598):��������� ���������� (�������)
      if(L_(143)) then
         I0_ovuk=0
      endif
      I1_uvuk=I0_ovuk
      L_(142)=I0_ovuk.ne.0
C 20FDC_control01.fgi( 662, 600):���������� ��������� 
      L_(139)=I1_uvuk.eq.I0_utuk
C 20FDC_control01.fgi( 676, 607):���������� �������������
      if(L_(139)) then
         I_avuk=I_(109)
      else
         I_avuk=I_(110)
      endif
C 20FDC_control01.fgi( 681, 615):���� RE IN LO CH7
      I_(111) = z'01000010'
C 20FDC_control01.fgi( 903, 728):��������� ������������� IN (�������)
      I_(112) = z'01000009'
C 20FDC_control01.fgi( 903, 730):��������� ������������� IN (�������)
      L_(146)=.false.
C 20FDC_control01.fgi( 878, 711):��������� ���������� (�������)
      if(L_(146)) then
         I0_edal=0
      endif
      I1_idal=I0_edal
      L_(145)=I0_edal.ne.0
C 20FDC_control01.fgi( 888, 713):���������� ��������� 
      L_(144)=I1_idal.eq.I0_ubal
C 20FDC_control01.fgi( 902, 720):���������� �������������
      if(L_(144)) then
         I_adal=I_(111)
      else
         I_adal=I_(112)
      endif
C 20FDC_control01.fgi( 907, 728):���� RE IN LO CH7
      I_(113) = z'01000010'
C 20FDC_control01.fgi( 903, 762):��������� ������������� IN (�������)
      I_(114) = z'01000009'
C 20FDC_control01.fgi( 903, 764):��������� ������������� IN (�������)
      L_(149)=.false.
C 20FDC_control01.fgi( 878, 745):��������� ���������� (�������)
      if(L_(149)) then
         I0_efal=0
      endif
      I1_ifal=I0_efal
      L_(148)=I0_efal.ne.0
C 20FDC_control01.fgi( 888, 747):���������� ��������� 
      L_(147)=I1_ifal.eq.I0_udal
C 20FDC_control01.fgi( 902, 754):���������� �������������
      if(L_(147)) then
         I_afal=I_(113)
      else
         I_afal=I_(114)
      endif
C 20FDC_control01.fgi( 907, 762):���� RE IN LO CH7
      I_(115) = z'01000010'
C 20FDC_control01.fgi( 903, 798):��������� ������������� IN (�������)
      I_(116) = z'01000009'
C 20FDC_control01.fgi( 903, 800):��������� ������������� IN (�������)
      L_(152)=.false.
C 20FDC_control01.fgi( 878, 781):��������� ���������� (�������)
      if(L_(152)) then
         I0_ekal=0
      endif
      I1_ikal=I0_ekal
      L_(151)=I0_ekal.ne.0
C 20FDC_control01.fgi( 888, 783):���������� ��������� 
      L_(150)=I1_ikal.eq.I0_ufal
C 20FDC_control01.fgi( 902, 790):���������� �������������
      if(L_(150)) then
         I_akal=I_(115)
      else
         I_akal=I_(116)
      endif
C 20FDC_control01.fgi( 907, 798):���� RE IN LO CH7
      I_(117) = z'01000010'
C 20FDC_control01.fgi( 787, 728):��������� ������������� IN (�������)
      I_(118) = z'01000009'
C 20FDC_control01.fgi( 787, 730):��������� ������������� IN (�������)
      L_(155)=.false.
C 20FDC_control01.fgi( 762, 711):��������� ���������� (�������)
      if(L_(155)) then
         I0_elal=0
      endif
      I1_ilal=I0_elal
      L_(154)=I0_elal.ne.0
C 20FDC_control01.fgi( 772, 713):���������� ��������� 
      L_(153)=I1_ilal.eq.I0_ukal
C 20FDC_control01.fgi( 786, 720):���������� �������������
      if(L_(153)) then
         I_alal=I_(117)
      else
         I_alal=I_(118)
      endif
C 20FDC_control01.fgi( 791, 728):���� RE IN LO CH7
      I_(119) = 4
C 20FDC_control01.fgi( 362, 696):��������� ������������� IN (�������)
      I_(120) = 3
C 20FDC_control01.fgi( 362, 709):��������� ������������� IN (�������)
      I_(121) = 2
C 20FDC_control01.fgi( 362, 722):��������� ������������� IN (�������)
      I_(122) = 1
C 20FDC_control01.fgi( 362, 735):��������� ������������� IN (�������)
      L0_olal=R0_amal.ne.R0_ulal
      R0_ulal=R0_amal
C 20FDC_control01.fgi( 349, 690):���������� ������������� ������
      if(L0_olal) then
         I_(123)=I_(119)
      endif
C 20FDC_control01.fgi( 366, 695):���� � ������������� �������
      L0_emal=R0_omal.ne.R0_imal
      R0_imal=R0_omal
C 20FDC_control01.fgi( 349, 703):���������� ������������� ������
      if(L0_emal) then
         I_(123)=I_(120)
      endif
C 20FDC_control01.fgi( 366, 708):���� � ������������� �������
      L0_umal=R0_epal.ne.R0_apal
      R0_apal=R0_epal
C 20FDC_control01.fgi( 349, 716):���������� ������������� ������
      if(L0_umal) then
         I_(123)=I_(121)
      endif
C 20FDC_control01.fgi( 366, 721):���� � ������������� �������
      L0_ipal=R0_upal.ne.R0_opal
      R0_opal=R0_upal
C 20FDC_control01.fgi( 349, 729):���������� ������������� ������
      if(L0_ipal) then
         I_(123)=I_(122)
      endif
C 20FDC_control01.fgi( 366, 734):���� � ������������� �������
      I1_ad=I_(123)
C 20FDC_control01.fgi( 366, 734):������-�������: ���������� ��� �������������� ������
      I_(124) = 4
C 20FDC_control01.fgi( 285, 696):��������� ������������� IN (�������)
      I_(125) = 3
C 20FDC_control01.fgi( 285, 709):��������� ������������� IN (�������)
      I_(126) = 2
C 20FDC_control01.fgi( 285, 722):��������� ������������� IN (�������)
      I_(127) = 1
C 20FDC_control01.fgi( 285, 735):��������� ������������� IN (�������)
      L0_obel=R0_adel.ne.R0_ubel
      R0_ubel=R0_adel
C 20FDC_control01.fgi( 272, 690):���������� ������������� ������
      if(L0_obel) then
         I_(128)=I_(124)
      endif
C 20FDC_control01.fgi( 289, 695):���� � ������������� �������
      L0_edel=R0_odel.ne.R0_idel
      R0_idel=R0_odel
C 20FDC_control01.fgi( 272, 703):���������� ������������� ������
      if(L0_edel) then
         I_(128)=I_(125)
      endif
C 20FDC_control01.fgi( 289, 708):���� � ������������� �������
      L0_udel=R0_efel.ne.R0_afel
      R0_afel=R0_efel
C 20FDC_control01.fgi( 272, 716):���������� ������������� ������
      if(L0_udel) then
         I_(128)=I_(126)
      endif
C 20FDC_control01.fgi( 289, 721):���� � ������������� �������
      L0_ifel=R0_ufel.ne.R0_ofel
      R0_ofel=R0_ufel
C 20FDC_control01.fgi( 272, 729):���������� ������������� ������
      if(L0_ifel) then
         I_(128)=I_(127)
      endif
C 20FDC_control01.fgi( 289, 734):���� � ������������� �������
      I1_u=I_(128)
C 20FDC_control01.fgi( 289, 734):������-�������: ���������� ��� �������������� ������
      I_(129) = z'01000010'
C 20FDC_control01.fgi( 787, 762):��������� ������������� IN (�������)
      I_(130) = z'01000009'
C 20FDC_control01.fgi( 787, 764):��������� ������������� IN (�������)
      L_(158)=.false.
C 20FDC_control01.fgi( 762, 745):��������� ���������� (�������)
      if(L_(158)) then
         I0_ikel=0
      endif
      I1_okel=I0_ikel
      L_(157)=I0_ikel.ne.0
C 20FDC_control01.fgi( 772, 747):���������� ��������� 
      L_(156)=I1_okel.eq.I0_akel
C 20FDC_control01.fgi( 786, 754):���������� �������������
      if(L_(156)) then
         I_ekel=I_(129)
      else
         I_ekel=I_(130)
      endif
C 20FDC_control01.fgi( 791, 762):���� RE IN LO CH7
      I_(131) = z'01000010'
C 20FDC_control01.fgi( 787, 798):��������� ������������� IN (�������)
      I_(132) = z'01000009'
C 20FDC_control01.fgi( 787, 800):��������� ������������� IN (�������)
      L_(161)=.false.
C 20FDC_control01.fgi( 762, 781):��������� ���������� (�������)
      if(L_(161)) then
         I0_elel=0
      endif
      I1_ilel=I0_elel
      L_(160)=I0_elel.ne.0
C 20FDC_control01.fgi( 772, 783):���������� ��������� 
      L_(159)=I1_ilel.eq.I0_ukel
C 20FDC_control01.fgi( 786, 790):���������� �������������
      if(L_(159)) then
         I_alel=I_(131)
      else
         I_alel=I_(132)
      endif
C 20FDC_control01.fgi( 791, 798):���� RE IN LO CH7
      I_(133) = z'01000010'
C 20FDC_control01.fgi( 677, 728):��������� ������������� IN (�������)
      I_(134) = z'01000009'
C 20FDC_control01.fgi( 677, 730):��������� ������������� IN (�������)
      L_(164)=.false.
C 20FDC_control01.fgi( 652, 711):��������� ���������� (�������)
      if(L_(164)) then
         I0_esel=0
      endif
      I1_isel=I0_esel
      L_(163)=I0_esel.ne.0
C 20FDC_control01.fgi( 662, 713):���������� ��������� 
      L_(162)=I1_isel.eq.I0_urel
C 20FDC_control01.fgi( 676, 720):���������� �������������
      if(L_(162)) then
         I_asel=I_(133)
      else
         I_asel=I_(134)
      endif
C 20FDC_control01.fgi( 681, 728):���� RE IN LO CH7
      I_(135) = z'01000010'
C 20FDC_control01.fgi( 677, 762):��������� ������������� IN (�������)
      I_(136) = z'01000009'
C 20FDC_control01.fgi( 677, 764):��������� ������������� IN (�������)
      I_(137) = z'01000010'
C 20FDC_control01.fgi( 677, 798):��������� ������������� IN (�������)
      I_(138) = z'01000009'
C 20FDC_control01.fgi( 677, 800):��������� ������������� IN (�������)
      L_(168)=.false.
C 20FDC_control01.fgi( 652, 745):��������� ���������� (�������)
      if(L_(168)) then
         I0_ovel=0
      endif
      I1_uvel=I0_ovel
      L_(167)=I0_ovel.ne.0
C 20FDC_control01.fgi( 662, 747):���������� ��������� 
      L_(165)=I1_uvel.eq.I0_utel
C 20FDC_control01.fgi( 676, 754):���������� �������������
      if(L_(165)) then
         I_avel=I_(135)
      else
         I_avel=I_(136)
      endif
C 20FDC_control01.fgi( 681, 762):���� RE IN LO CH7
      L_(170)=.false.
C 20FDC_control01.fgi( 652, 781):��������� ���������� (�������)
      if(L_(170)) then
         I0_axel=0
      endif
      I1_exel=I0_axel
      L_(169)=I0_axel.ne.0
C 20FDC_control01.fgi( 662, 783):���������� ��������� 
      L_(166)=I1_exel.eq.I0_evel
C 20FDC_control01.fgi( 676, 790):���������� �������������
      if(L_(166)) then
         I_ivel=I_(137)
      else
         I_ivel=I_(138)
      endif
C 20FDC_control01.fgi( 681, 798):���� RE IN LO CH7
      I_(141) = 4
C 20FDC_control01.fgi(  34, 655):��������� ������������� IN (�������)
      I_(143) = 3
C 20FDC_control01.fgi(  34, 668):��������� ������������� IN (�������)
      I_(144) = 2
C 20FDC_control01.fgi(  34, 686):��������� ������������� IN (�������)
      I_(145) = 1
C 20FDC_control01.fgi(  34, 699):��������� ������������� IN (�������)
      L0_odil=R0_afil.ne.R0_udil
      R0_udil=R0_afil
C 20FDC_control01.fgi(  21, 649):���������� ������������� ������
      if(L0_odil) then
         I_(142)=I_(141)
      endif
C 20FDC_control01.fgi(  38, 654):���� � ������������� �������
      L0_efil=R0_ofil.ne.R0_ifil
      R0_ifil=R0_ofil
C 20FDC_control01.fgi(  21, 662):���������� ������������� ������
      if(L0_efil) then
         I_(142)=I_(143)
      endif
C 20FDC_control01.fgi(  38, 667):���� � ������������� �������
      I1_i=I_(142)
C 20FDC_control01.fgi(  38, 667):������-�������: ���������� ��� �������������� ������
      L0_ufil=R0_ekil.ne.R0_akil
      R0_akil=R0_ekil
C 20FDC_control01.fgi(  21, 680):���������� ������������� ������
      if(L0_ufil) then
         I_(146)=I_(144)
      endif
C 20FDC_control01.fgi(  38, 685):���� � ������������� �������
      L0_ikil=R0_ukil.ne.R0_okil
      R0_okil=R0_ukil
C 20FDC_control01.fgi(  21, 693):���������� ������������� ������
      if(L0_ikil) then
         I_(146)=I_(145)
      endif
C 20FDC_control01.fgi(  38, 698):���� � ������������� �������
      I1_e=I_(146)
C 20FDC_control01.fgi(  38, 698):������-�������: ���������� ��� �������������� ������
      I_(148) = z'01000033'
C 20FDC_control01.fgi( 141, 798):��������� ������������� IN (�������)
      I_(147) = z'01000009'
C 20FDC_control01.fgi( 141, 796):��������� ������������� IN (�������)
      if(L_(174)) then
         I_imil=I_(147)
      else
         I_imil=I_(148)
      endif
C 20FDC_control01.fgi( 145, 796):���� RE IN LO CH7
      L_(176)=.false.
C 20FDC_control01.fgi( 100, 798):��������� ���������� (�������)
      if(L_(176)) then
         I0_apil=0
      endif
      I_(149)=I0_apil
      L_(175)=I0_apil.ne.0
C 20FDC_control01.fgi( 110, 800):���������� ��������� 
      L_(21)=R8_ovif.lt.R0_etod
C 20FDC_control01.fgi(  93, 240):���������� <
C label 1081  try1081=try1081-1
      R_(28) = R_(27) + R_od
C 20FDC_control01.fgi( 128, 200):��������
      L_(24)=R8_ovif.gt.R0_otod
C 20FDC_control01.fgi(  93, 190):���������� >
      L_(20) = L0_utod.AND.L_(24)
C 20FDC_control01.fgi( 104, 191):�
      if(L_(20)) then
         R_(36)=R_(28)
      endif
C 20FDC_control01.fgi( 132, 199):���� � ������������� �������
      R_od=R_(36)
C 20FDC_control01.fgi( 132, 199):������-�������: ���������� ��� �������������� ������
C sav1=R_(28)
      R_(28) = R_(27) + R_od
C 20FDC_control01.fgi( 128, 200):recalc:��������
C if(sav1.ne.R_(28) .and. try1084.gt.0) goto 1084
      R_(32) = R_(31) + R_ud
C 20FDC_control01.fgi( 127, 254):��������
      L_(25) = L_(21).AND.L0_utod
C 20FDC_control01.fgi( 104, 231):�
      L_(26) = L0_ivod.OR.L_(25)
C 20FDC_control01.fgi( 113, 248):���
      if(L_(26)) then
         R_(35)=R_(32)
      endif
C 20FDC_control01.fgi( 131, 253):���� � ������������� �������
      R_ud=R_(35)
C 20FDC_control01.fgi( 131, 253):������-�������: ���������� ��� �������������� ������
C sav1=R_(32)
      R_(32) = R_(31) + R_ud
C 20FDC_control01.fgi( 127, 254):recalc:��������
C if(sav1.ne.R_(32) .and. try1097.gt.0) goto 1097
      R_(30) = R_(29) + R_af
C 20FDC_control01.fgi( 128, 235):��������
      L_(23) = L_(24).AND.L0_itod
C 20FDC_control01.fgi( 129, 227):�
      L_(17) = L_(21).AND.L0_usod
C 20FDC_control01.fgi( 129, 215):�
      L_(22) = L_(23).OR.L_(17)
C 20FDC_control01.fgi( 137, 226):���
      if(L_(22)) then
         R_(34)=R_(30)
      endif
C 20FDC_control01.fgi( 142, 234):���� � ������������� �������
      R_af=R_(34)
C 20FDC_control01.fgi( 142, 234):������-�������: ���������� ��� �������������� ������
C sav1=R_(30)
      R_(30) = R_(29) + R_af
C 20FDC_control01.fgi( 128, 235):recalc:��������
C if(sav1.ne.R_(30) .and. try1109.gt.0) goto 1109
      !{
      Call KLAPAN_MAN(deltat,REAL(R_adof,4),R8_ovif,I_ekof
     &,C8_ixif,I_okof,R_ibof,
     & R_ebof,R_etif,REAL(R_af,4),R_avif,
     & REAL(R_od,4),R_otif,REAL(R_ud,4),I_ufof,
     & I_ukof,I_ikof,I_ofof,L_ivif,L_elof,L_emof,L_evif,
     & L_utif,REAL(R_oxif,4),L_axif,L_uvif,L_olof,
     & L_itif,L_uxif,L_umof,L_obof,L_ubof,REAL(R8_atif,8)
     &,
     & REAL(1.0,4),R8_abof,L_(43),L_esif,L_(44),L_isif,L_ilof
     &,I_alof,L_imof,
     & R_ifof,REAL(R_exif,4),L_omof,L_osif,L_apof,L_usif,L_edof
     &,L_idof,
     & L_odof,L_afof,L_efof,L_udof)
      !}

      if(L_efof.or.L_afof.or.L_udof.or.L_odof.or.L_idof.or.L_edof
     &) then      
                  I_akof = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_akof = z'40000000'
      endif
C 20FDC_control01.fgi( 183, 243):���� ���������� ��������,VE1
      L_(18)=L_(23).and..not.L0_atod
      L0_atod=L_(23)
C 20FDC_control01.fgi( 112, 222):������������  �� 1 ���
      L0_itod=(L_(25).or.L0_itod).and..not.(L_(18))
      L_(19)=.not.L0_itod
C 20FDC_control01.fgi( 121, 224):RS �������
C sav1=L_(23)
      L_(23) = L_(24).AND.L0_itod
C 20FDC_control01.fgi( 129, 227):recalc:�
C if(sav1.ne.L_(23) .and. try1111.gt.0) goto 1111
      L_(15)=L_(17).and..not.L0_osod
      L0_osod=L_(17)
C 20FDC_control01.fgi( 112, 210):������������  �� 1 ���
      L0_usod=(L_(20).or.L0_usod).and..not.(L_(15))
      L_(16)=.not.L0_usod
C 20FDC_control01.fgi( 121, 212):RS �������
C sav1=L_(17)
      L_(17) = L_(21).AND.L0_usod
C 20FDC_control01.fgi( 129, 215):recalc:�
C if(sav1.ne.L_(17) .and. try1114.gt.0) goto 1114
      L_(173)=.NOT.L_adud.and..not.L0_ebud
      L0_ebud=.NOT.L_adud
C 20FDC_control01.fgi( 107, 719):������������  �� 1 ���
C label 1137  try1137=try1137-1
      if(L_(173)) then
         I0_idil=0
      endif
      I_(140)=I0_idil
      L_(172)=I0_idil.ne.0
C 20FDC_control01.fgi( 118, 721):���������� ��������� 
      L_adud=L_(172).and..not.L0_ibud
      L0_ibud=L_(172)
C 20FDC_control01.fgi( 133, 719):������������  �� 1 ���
      I_(22) = (-I_(20)) + I_(140)
C 20FDC_control01.fgi( 131, 724):��������
      if(L_adud) then
         I_(139)=I_(22)
      endif
C 20FDC_control01.fgi( 142, 723):���� � ������������� �������
      I1_o=I_(139)
C 20FDC_control01.fgi( 142, 710):������-�������: ���������� ��� �������������� ������
      L_(28)=I1_o.eq.I0_exod
C 20FDC_control01.fgi( 152, 730):���������� �������������
      L_(171)=I1_o.eq.I0_edil
C 20FDC_control01.fgi( 152, 737):���������� �������������
      L_(27) = L_(171).OR.L_(28)
C 20FDC_control01.fgi( 164, 736):���
      if(L_(27)) then
         I1_axod=1
      else
         I1_axod=0
      endif
C 20FDC_control01.fgi( 173, 736):��������� LO->1
      if(L_(171)) then
         I_(16)=I_(17)
      else
         I_(16)=I_(18)
      endif
C 20FDC_control01.fgi( 157, 747):���� RE IN LO CH7
      if(L_(28)) then
         I_ixod=I_(15)
      else
         I_ixod=I_(16)
      endif
C 20FDC_control01.fgi( 177, 746):���� RE IN LO CH7
      Call PUMP_HANDLER(deltat,C30_ored,I_ited,L_ired,L_ased
     &,L_ubid,
     & I_oted,I_eted,R_eped,REAL(R_oped,4),
     & R_omed,REAL(R_aped,4),I_uted,L_ibid,L_ixed,L_ifid,
     & L_ofid,L_osed,L_oved,L_axed,L_uved,L_exed,L_abid,L_imed
     &,
     & L_iped,L_emed,L_umed,L_udid,L_(2),
     & L_afid,L_(1),L_amed,L_uled,L_used,I_aved,R_adid,R_edid
     &,
     & L_ekid,L_ebid,L_efid,REAL(R8_atif,8),L_uxed,
     & REAL(R8_oxed,8),R_ufid,REAL(R_eved,4),R_ived,REAL(R8_ised
     &,8),R_esed,
     & R8_abof,R_akid,R8_oxed,REAL(R_uped,4),REAL(R_ared,4
     &))
C 20FDC_vlv.fgi( 172, 207):���������� ���������� �������,20FDC70AN001
C label 1173  try1173=try1173-1
C sav1=R_akid
C sav2=R_ufid
C sav3=L_ibid
C sav4=L_abid
C sav5=L_ixed
C sav6=R8_oxed
C sav7=R_ived
C sav8=I_aved
C sav9=I_uted
C sav10=I_oted
C sav11=I_ited
C sav12=I_eted
C sav13=I_ated
C sav14=L_used
C sav15=L_osed
C sav16=R_esed
C sav17=C30_ored
C sav18=L_iped
C sav19=L_umed
      Call PUMP_HANDLER(deltat,C30_ored,I_ited,L_ired,L_ased
     &,L_ubid,
     & I_oted,I_eted,R_eped,REAL(R_oped,4),
     & R_omed,REAL(R_aped,4),I_uted,L_ibid,L_ixed,L_ifid,
     & L_ofid,L_osed,L_oved,L_axed,L_uved,L_exed,L_abid,L_imed
     &,
     & L_iped,L_emed,L_umed,L_udid,L_(2),
     & L_afid,L_(1),L_amed,L_uled,L_used,I_aved,R_adid,R_edid
     &,
     & L_ekid,L_ebid,L_efid,REAL(R8_atif,8),L_uxed,
     & REAL(R8_oxed,8),R_ufid,REAL(R_eved,4),R_ived,REAL(R8_ised
     &,8),R_esed,
     & R8_abof,R_akid,R8_oxed,REAL(R_uped,4),REAL(R_ared,4
     &))
C 20FDC_vlv.fgi( 172, 207):recalc:���������� ���������� �������,20FDC70AN001
C if(sav1.ne.R_akid .and. try1173.gt.0) goto 1173
C if(sav2.ne.R_ufid .and. try1173.gt.0) goto 1173
C if(sav3.ne.L_ibid .and. try1173.gt.0) goto 1173
C if(sav4.ne.L_abid .and. try1173.gt.0) goto 1173
C if(sav5.ne.L_ixed .and. try1173.gt.0) goto 1173
C if(sav6.ne.R8_oxed .and. try1173.gt.0) goto 1173
C if(sav7.ne.R_ived .and. try1173.gt.0) goto 1173
C if(sav8.ne.I_aved .and. try1173.gt.0) goto 1173
C if(sav9.ne.I_uted .and. try1173.gt.0) goto 1173
C if(sav10.ne.I_oted .and. try1173.gt.0) goto 1173
C if(sav11.ne.I_ited .and. try1173.gt.0) goto 1173
C if(sav12.ne.I_eted .and. try1173.gt.0) goto 1173
C if(sav13.ne.I_ated .and. try1173.gt.0) goto 1173
C if(sav14.ne.L_used .and. try1173.gt.0) goto 1173
C if(sav15.ne.L_osed .and. try1173.gt.0) goto 1173
C if(sav16.ne.R_esed .and. try1173.gt.0) goto 1173
C if(sav17.ne.C30_ored .and. try1173.gt.0) goto 1173
C if(sav18.ne.L_iped .and. try1173.gt.0) goto 1173
C if(sav19.ne.L_umed .and. try1173.gt.0) goto 1173
      End
