      Subroutine MECHANIC_PRIVOD(ext_deltat,R_u,L_af,L_ef
     &,R_ak,L_el,L_il,R_em,L_ip,L_op,R_ir,L_os,L_us,R_ot,L_uv
     &,L_ax,R_ux,L_ade,L_ede,R_afe,L_eke,L_ike,R_ele,L_ime
     &,L_ome,R_ipe,L_ore,L_ure,L_ase,L_ese,L_ise,L_ose,L_use
     &,L_ate,L_ete,L_ite,L_ote,L_ute,L_ave,L_eve,L_ive,L_ove
     &,L_uve,L_axe,L_exe,L_ixe,L_oxe,L_uxe,L_abi,L_ebi,L_ibi
     &,L_obi,L_ubi,L_adi,L_edi,R_afi,L_ifi,L_ufi,L_aki,L_uki
     &,L_ali,L_eli,L_ili,L_oli,L_uli,R_ami,L_emi,L_imi,R_ipi
     &,L_opi,L_upi,L_ari,L_eri,L_iri,L_ori,I_esi,L_isi,I_iti
     &,L_oti,I_uti,L_avi,R_evi,R_oxi,R_ibo,R_ofo,L_ufo,L_ako
     &,L_iko,L_elo)
C |R_u           |4 4 I|pos10_x         |���������� ��������� 10||
C |L_af          |1 1 O|POS10_READY     |�������� ��������� 5||
C |L_ef          |1 1 I|POS10C          |������� ������� � ��������� 5||
C |R_ak          |4 4 I|pos9_x          |���������� ��������� 9||
C |L_el          |1 1 O|POS9_READY      |�������� ��������� 4||
C |L_il          |1 1 I|POS9C           |������� ������� � ��������� 4||
C |R_em          |4 4 I|pos8_x          |���������� ��������� 8||
C |L_ip          |1 1 O|POS8_READY      |�������� ��������� 3||
C |L_op          |1 1 I|POS8C           |������� ������� � ��������� 3||
C |R_ir          |4 4 I|pos7_x          |���������� ��������� 7||
C |L_os          |1 1 O|POS7_READY      |�������� ��������� 2||
C |L_us          |1 1 I|POS7C           |������� ������� � ��������� 2||
C |R_ot          |4 4 I|pos6_x          |���������� ��������� 6||
C |L_uv          |1 1 O|POS6_READY      |�������� ��������� 1||
C |L_ax          |1 1 I|POS6C           |������� ������� � ��������� 1||
C |R_ux          |4 4 I|pos5_x          |���������� ��������� 5||
C |L_ade         |1 1 O|POS5_READY      |�������� ��������� 5||
C |L_ede         |1 1 I|POS5C           |������� ������� � ��������� 5||
C |R_afe         |4 4 I|pos4_x          |���������� ��������� 4||
C |L_eke         |1 1 O|POS4_READY      |�������� ��������� 4||
C |L_ike         |1 1 I|POS4C           |������� ������� � ��������� 4||
C |R_ele         |4 4 I|pos3_x          |���������� ��������� 3||
C |L_ime         |1 1 O|POS3_READY      |�������� ��������� 3||
C |L_ome         |1 1 I|POS3C           |������� ������� � ��������� 3||
C |R_ipe         |4 4 I|pos2_x          |���������� ��������� 2||
C |L_ore         |1 1 O|POS2_READY      |�������� ��������� 2||
C |L_ure         |1 1 I|POS2C           |������� ������� � ��������� 2||
C |L_ase         |1 1 O|POS10_UP        |������� ������||
C |L_ese         |1 1 O|POS9_UP         |������� ������||
C |L_ise         |1 1 O|POS8_UP         |������� ������||
C |L_ose         |1 1 O|POS7_UP         |������� ������||
C |L_use         |1 1 O|POS6_UP         |������� ������||
C |L_ate         |1 1 O|POS10_DOWN      |������� �����||
C |L_ete         |1 1 O|POS9_DOWN       |������� �����||
C |L_ite         |1 1 O|POS8_DOWN       |������� �����||
C |L_ote         |1 1 O|POS7_DOWN       |������� �����||
C |L_ute         |1 1 O|POS6_DOWN       |������� �����||
C |L_ave         |1 1 O|POS10_STOP      |������� ����||
C |L_eve         |1 1 O|POS9_STOP       |������� ����||
C |L_ive         |1 1 O|POS8_STOP       |������� ����||
C |L_ove         |1 1 O|POS7_STOP       |������� ����||
C |L_uve         |1 1 O|POS6_STOP       |������� ����||
C |L_axe         |1 1 O|POS5_UP         |������� ������||
C |L_exe         |1 1 O|POS5_DOWN       |������� �����||
C |L_ixe         |1 1 O|POS5_STOP       |������� ����||
C |L_oxe         |1 1 O|POS4_UP         |������� ������||
C |L_uxe         |1 1 O|POS3_UP         |������� ������||
C |L_abi         |1 1 O|POS2_UP         |������� ������||
C |L_ebi         |1 1 O|POS4_DOWN       |������� �����||
C |L_ibi         |1 1 O|POS3_DOWN       |������� �����||
C |L_obi         |1 1 O|POS2_DOWN       |������� �����||
C |L_ubi         |1 1 O|POS4_STOP       |������� ����||
C |L_adi         |1 1 O|POS3_STOP       |������� ����||
C |L_edi         |1 1 O|POS2_STOP       |������� ����||
C |R_afi         |4 4 I|pos1_x          |���������� ��������� 1||
C |L_ifi         |1 1 O|POS1_STOP       |������� ����||
C |L_ufi         |1 1 O|POS1_UP         |������� ������||
C |L_aki         |1 1 O|POS1_DOWN       |������� �����||
C |L_uki         |1 1 O|POS1_READY      |�������� ��������� 1||
C |L_ali         |1 1 I|POS1C           |������� ������� � ��������� 1||
C |L_eli         |1 1 O|UNCATCH         |��������� �������|F|
C |L_ili         |1 1 I|YA31            |������� ��������� �������|F|
C |L_oli         |1 1 O|CATCH           |������ �������|F|
C |L_uli         |1 1 I|YA30            |������� ������ �������|F|
C |R_ami         |4 4 O|SPEED           |�������� ������� [��/�]||
C |L_emi         |1 1 S|_splsJ202*      |[TF]���������� ��������� ������������� |F|
C |L_imi         |1 1 S|_splsJ197*      |[TF]���������� ��������� ������������� |F|
C |R_ipi         |4 4 I|vel             |�������� ������� [��/�]||
C |L_opi         |1 1 O|POS_DOWN_GLOB   |||
C |L_upi         |1 1 I|DOWNC           |������ �����. ���������� �������||
C |L_ari         |1 1 O|POS_UP_GLOB     |||
C |L_eri         |1 1 O|POS_STOP_GLOB   |||
C |L_iri         |1 1 I|STOPC           |������ ����. ���������� �������||
C |L_ori         |1 1 I|UPC             |������ ������. ���������� �������||
C |I_esi         |2 4 I|STOP_USER       |������ ����. ������� �� ���������||
C |L_isi         |1 1 I|STOP            |������ ����. ������� �� ����������||
C |I_iti         |2 4 I|DOWN_USER       |������ �����. ������� �� ���������||
C |L_oti         |1 1 I|DOWN            |������ �����. ������� �� ����������||
C |I_uti         |2 4 I|UP_USER         |������ ������. ������� �� ���������||
C |L_avi         |1 1 I|UP              |������ ������. ������� �� ����������||
C |R_evi         |4 4 O|POS             |��������� ������� [��]||
C |R_oxi         |4 4 I|max             |����. ��������� ������� [��]||
C |R_ibo         |4 4 I|min             |���. ��������� ������� [��]||
C |R_ofo         |4 4 S|_ointJ59*       |����� ����������� |0.0|
C |L_ufo         |1 1 O|XH52            |������ � MIN||
C |L_ako         |1 1 O|XH51            |������ � MAX||
C |L_iko         |1 1 S|_qffJ43*        |�������� ������ Q RS-��������  |F|
C |L_elo         |1 1 S|_qffJ42*        |�������� ������ Q RS-��������  |F|

      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      REAL*4 R0_e,R0_i,R0_o,R_u,R0_ad
      LOGICAL*1 L0_ed,L0_id,L0_od,L0_ud,L_af,L_ef
      REAL*4 R0_if,R0_of,R0_uf,R_ak,R0_ek
      LOGICAL*1 L0_ik,L0_ok,L0_uk,L0_al,L_el,L_il
      REAL*4 R0_ol,R0_ul,R0_am,R_em,R0_im
      LOGICAL*1 L0_om,L0_um,L0_ap,L0_ep,L_ip,L_op
      REAL*4 R0_up,R0_ar,R0_er,R_ir,R0_or
      LOGICAL*1 L0_ur,L0_as,L0_es,L0_is,L_os,L_us
      REAL*4 R0_at,R0_et,R0_it,R_ot,R0_ut
      LOGICAL*1 L0_av,L0_ev,L0_iv,L0_ov,L_uv,L_ax
      REAL*4 R0_ex,R0_ix,R0_ox,R_ux,R0_abe
      LOGICAL*1 L0_ebe,L0_ibe,L0_obe,L0_ube,L_ade,L_ede
      REAL*4 R0_ide,R0_ode,R0_ude,R_afe,R0_efe
      LOGICAL*1 L0_ife,L0_ofe,L0_ufe,L0_ake,L_eke,L_ike
      REAL*4 R0_oke,R0_uke,R0_ale,R_ele,R0_ile
      LOGICAL*1 L0_ole,L0_ule,L0_ame,L0_eme,L_ime,L_ome
      REAL*4 R0_ume,R0_ape,R0_epe,R_ipe,R0_ope
      LOGICAL*1 L0_upe,L0_are,L0_ere,L0_ire,L_ore,L_ure,L_ase
     &,L_ese,L_ise,L_ose,L_use,L_ate,L_ete,L_ite,L_ote,L_ute
     &,L_ave,L_eve,L_ive,L_ove,L_uve,L_axe
      LOGICAL*1 L_exe,L_ixe,L_oxe,L_uxe,L_abi,L_ebi,L_ibi
     &,L_obi,L_ubi,L_adi,L_edi
      REAL*4 R0_idi,R0_odi,R0_udi,R_afi,R0_efi
      LOGICAL*1 L_ifi,L0_ofi,L_ufi,L_aki,L0_eki,L0_iki,L0_oki
     &,L_uki,L_ali,L_eli,L_ili,L_oli,L_uli
      REAL*4 R_ami
      LOGICAL*1 L_emi,L_imi,L0_omi,L0_umi,L0_api
      REAL*4 R0_epi,R_ipi
      LOGICAL*1 L_opi,L_upi,L_ari,L_eri,L_iri,L_ori,L0_uri
     &,L0_asi
      INTEGER*4 I_esi
      LOGICAL*1 L_isi,L0_osi,L0_usi,L0_ati,L0_eti
      INTEGER*4 I_iti
      LOGICAL*1 L_oti
      INTEGER*4 I_uti
      LOGICAL*1 L_avi
      REAL*4 R_evi,R0_ivi,R0_ovi,R0_uvi,R0_axi
      LOGICAL*1 L0_exi,L0_ixi
      REAL*4 R_oxi,R0_uxi
      LOGICAL*1 L0_abo,L0_ebo
      REAL*4 R_ibo,R0_obo
      LOGICAL*1 L0_ubo,L0_ado,L0_edo,L0_ido,L0_odo,L0_udo
     &,L0_afo,L0_efo,L0_ifo
      REAL*4 R_ofo
      LOGICAL*1 L_ufo,L_ako
      REAL*4 R0_eko
      LOGICAL*1 L_iko
      REAL*4 R0_oko,R0_uko,R0_alo
      LOGICAL*1 L_elo
      REAL*4 R0_ilo,R0_olo,R0_ulo

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      R0_e = 10.0
C MECHANIC_PRIVOD.fmg( 451, 210):��������� (RE4) (�������)
      R0_i = R_u + (-R0_e)
C MECHANIC_PRIVOD.fmg( 454, 212):��������
      R0_o = 10.0
C MECHANIC_PRIVOD.fmg( 457, 221):��������� (RE4) (�������)
      R0_ad = R_u + R0_o
C MECHANIC_PRIVOD.fmg( 460, 223):��������
      R0_if = 10.0
C MECHANIC_PRIVOD.fmg( 451, 250):��������� (RE4) (�������)
      R0_of = R_ak + (-R0_if)
C MECHANIC_PRIVOD.fmg( 454, 252):��������
      R0_uf = 10.0
C MECHANIC_PRIVOD.fmg( 457, 261):��������� (RE4) (�������)
      R0_ek = R_ak + R0_uf
C MECHANIC_PRIVOD.fmg( 460, 263):��������
      R0_ol = 10.0
C MECHANIC_PRIVOD.fmg( 451, 289):��������� (RE4) (�������)
      R0_ul = R_em + (-R0_ol)
C MECHANIC_PRIVOD.fmg( 454, 291):��������
      R0_am = 10.0
C MECHANIC_PRIVOD.fmg( 457, 300):��������� (RE4) (�������)
      R0_im = R_em + R0_am
C MECHANIC_PRIVOD.fmg( 460, 302):��������
      R0_up = 10.0
C MECHANIC_PRIVOD.fmg( 451, 327):��������� (RE4) (�������)
      R0_ar = R_ir + (-R0_up)
C MECHANIC_PRIVOD.fmg( 454, 329):��������
      R0_er = 10.0
C MECHANIC_PRIVOD.fmg( 457, 338):��������� (RE4) (�������)
      R0_or = R_ir + R0_er
C MECHANIC_PRIVOD.fmg( 460, 340):��������
      R0_at = 10.0
C MECHANIC_PRIVOD.fmg( 451, 366):��������� (RE4) (�������)
      R0_et = R_ot + (-R0_at)
C MECHANIC_PRIVOD.fmg( 454, 368):��������
      R0_it = 10.0
C MECHANIC_PRIVOD.fmg( 457, 377):��������� (RE4) (�������)
      R0_ut = R_ot + R0_it
C MECHANIC_PRIVOD.fmg( 460, 379):��������
      R0_ex = 10.0
C MECHANIC_PRIVOD.fmg( 326, 210):��������� (RE4) (�������)
      R0_ix = R_ux + (-R0_ex)
C MECHANIC_PRIVOD.fmg( 329, 212):��������
      R0_ox = 10.0
C MECHANIC_PRIVOD.fmg( 332, 221):��������� (RE4) (�������)
      R0_abe = R_ux + R0_ox
C MECHANIC_PRIVOD.fmg( 335, 223):��������
      R0_ide = 10.0
C MECHANIC_PRIVOD.fmg( 326, 250):��������� (RE4) (�������)
      R0_ode = R_afe + (-R0_ide)
C MECHANIC_PRIVOD.fmg( 329, 252):��������
      R0_ude = 10.0
C MECHANIC_PRIVOD.fmg( 332, 261):��������� (RE4) (�������)
      R0_efe = R_afe + R0_ude
C MECHANIC_PRIVOD.fmg( 335, 263):��������
      R0_oke = 10.0
C MECHANIC_PRIVOD.fmg( 326, 289):��������� (RE4) (�������)
      R0_uke = R_ele + (-R0_oke)
C MECHANIC_PRIVOD.fmg( 329, 291):��������
      R0_ale = 10.0
C MECHANIC_PRIVOD.fmg( 332, 300):��������� (RE4) (�������)
      R0_ile = R_ele + R0_ale
C MECHANIC_PRIVOD.fmg( 335, 302):��������
      R0_ume = 10.0
C MECHANIC_PRIVOD.fmg( 326, 327):��������� (RE4) (�������)
      R0_ape = R_ipe + (-R0_ume)
C MECHANIC_PRIVOD.fmg( 329, 329):��������
      R0_epe = 10.0
C MECHANIC_PRIVOD.fmg( 332, 338):��������� (RE4) (�������)
      R0_ope = R_ipe + R0_epe
C MECHANIC_PRIVOD.fmg( 335, 340):��������
      R0_idi = 10.0
C MECHANIC_PRIVOD.fmg( 326, 366):��������� (RE4) (�������)
      R0_odi = R_afi + (-R0_idi)
C MECHANIC_PRIVOD.fmg( 329, 368):��������
      R0_udi = 10.0
C MECHANIC_PRIVOD.fmg( 332, 377):��������� (RE4) (�������)
      R0_efi = R_afi + R0_udi
C MECHANIC_PRIVOD.fmg( 335, 379):��������
      L_eli=L_ili
C MECHANIC_PRIVOD.fmg(  43, 340):������,UNCATCH
      L_oli=L_uli
C MECHANIC_PRIVOD.fmg(  43, 345):������,CATCH
      R0_epi = 1.0
C MECHANIC_PRIVOD.fmg( 143, 280):��������� (RE4) (�������)
      if(R_ipi.ge.0.0) then
         R0_uxi=R0_epi/max(R_ipi,1.0e-10)
      else
         R0_uxi=R0_epi/min(R_ipi,-1.0e-10)
      endif
C MECHANIC_PRIVOD.fmg( 141, 275):�������� ����������
      R0_obo = 0
C MECHANIC_PRIVOD.fmg( 143, 269):��������� (RE4) (�������)
      L0_ati=I_esi.ne.0
C MECHANIC_PRIVOD.fmg(  63, 236):��������� 1->LO
      L0_exi=I_iti.ne.0
C MECHANIC_PRIVOD.fmg(  63, 216):��������� 1->LO
      L0_ubo=I_uti.ne.0
C MECHANIC_PRIVOD.fmg(  63, 254):��������� 1->LO
      R0_ivi = 0.01
C MECHANIC_PRIVOD.fmg( 207, 273):��������� (RE4) (�������)
      R0_ovi = (-R0_ivi) + R_oxi
C MECHANIC_PRIVOD.fmg( 210, 273):��������
      R0_uvi = 0.01
C MECHANIC_PRIVOD.fmg( 207, 250):��������� (RE4) (�������)
      R0_axi = R0_uvi + R_ibo
C MECHANIC_PRIVOD.fmg( 210, 250):��������
      L0_ebo=.false.
C MECHANIC_PRIVOD.fmg( 143, 256):��������� ���������� (�������)
      L0_abo=.false.
C MECHANIC_PRIVOD.fmg( 141, 256):��������� ���������� (�������)
      R0_alo = 0.0
C MECHANIC_PRIVOD.fmg( 121, 258):��������� (RE4) (�������)
      R0_uko = -1.0
C MECHANIC_PRIVOD.fmg( 121, 256):��������� (RE4) (�������)
      R0_ulo = 0.0
C MECHANIC_PRIVOD.fmg( 121, 271):��������� (RE4) (�������)
      R0_olo = 1.0
C MECHANIC_PRIVOD.fmg( 121, 269):��������� (RE4) (�������)
      L0_eti = L_avi.OR.L_ori.OR.L_ari
C MECHANIC_PRIVOD.fmg(  47, 267):���
C label 113  try113=try113-1
      L0_oki=R_evi.lt.R0_efi
C MECHANIC_PRIVOD.fmg( 357, 377):���������� <
      L0_iki=R_evi.gt.R0_odi
C MECHANIC_PRIVOD.fmg( 357, 369):���������� >
      L_uki = L0_oki.AND.L0_iki.AND.L_ali
C MECHANIC_PRIVOD.fmg( 368, 372):�
      L_ifi=L_uki
C MECHANIC_PRIVOD.fmg( 395, 372):������,POS1_STOP
      L0_ire=R_evi.lt.R0_ope
C MECHANIC_PRIVOD.fmg( 357, 338):���������� <
      L0_ere=R_evi.gt.R0_ape
C MECHANIC_PRIVOD.fmg( 357, 330):���������� >
      L_ore = L0_ire.AND.L0_ere.AND.L_ure
C MECHANIC_PRIVOD.fmg( 368, 333):�
      L_edi=L_ore
C MECHANIC_PRIVOD.fmg( 395, 333):������,POS2_STOP
      L0_eme=R_evi.lt.R0_ile
C MECHANIC_PRIVOD.fmg( 357, 300):���������� <
      L0_ame=R_evi.gt.R0_uke
C MECHANIC_PRIVOD.fmg( 357, 292):���������� >
      L_ime = L0_eme.AND.L0_ame.AND.L_ome
C MECHANIC_PRIVOD.fmg( 368, 295):�
      L_adi=L_ime
C MECHANIC_PRIVOD.fmg( 395, 295):������,POS3_STOP
      L0_ake=R_evi.lt.R0_efe
C MECHANIC_PRIVOD.fmg( 357, 261):���������� <
      L0_ufe=R_evi.gt.R0_ode
C MECHANIC_PRIVOD.fmg( 357, 253):���������� >
      L_eke = L0_ake.AND.L0_ufe.AND.L_ike
C MECHANIC_PRIVOD.fmg( 368, 256):�
      L_ubi=L_eke
C MECHANIC_PRIVOD.fmg( 395, 256):������,POS4_STOP
      L0_ube=R_evi.lt.R0_abe
C MECHANIC_PRIVOD.fmg( 357, 221):���������� <
      L0_obe=R_evi.gt.R0_ix
C MECHANIC_PRIVOD.fmg( 357, 213):���������� >
      L_ade = L0_ube.AND.L0_obe.AND.L_ede
C MECHANIC_PRIVOD.fmg( 368, 216):�
      L_ixe=L_ade
C MECHANIC_PRIVOD.fmg( 395, 216):������,POS5_STOP
      L0_ov=R_evi.lt.R0_ut
C MECHANIC_PRIVOD.fmg( 482, 377):���������� <
      L0_iv=R_evi.gt.R0_et
C MECHANIC_PRIVOD.fmg( 482, 369):���������� >
      L_uv = L0_ov.AND.L0_iv.AND.L_ax
C MECHANIC_PRIVOD.fmg( 493, 372):�
      L_uve=L_uv
C MECHANIC_PRIVOD.fmg( 520, 372):������,POS6_STOP
      L0_is=R_evi.lt.R0_or
C MECHANIC_PRIVOD.fmg( 482, 338):���������� <
      L0_es=R_evi.gt.R0_ar
C MECHANIC_PRIVOD.fmg( 482, 330):���������� >
      L_os = L0_is.AND.L0_es.AND.L_us
C MECHANIC_PRIVOD.fmg( 493, 333):�
      L_ove=L_os
C MECHANIC_PRIVOD.fmg( 520, 333):������,POS7_STOP
      L0_ep=R_evi.lt.R0_im
C MECHANIC_PRIVOD.fmg( 482, 300):���������� <
      L0_ap=R_evi.gt.R0_ul
C MECHANIC_PRIVOD.fmg( 482, 292):���������� >
      L_ip = L0_ep.AND.L0_ap.AND.L_op
C MECHANIC_PRIVOD.fmg( 493, 295):�
      L_ive=L_ip
C MECHANIC_PRIVOD.fmg( 520, 295):������,POS8_STOP
      L0_al=R_evi.lt.R0_ek
C MECHANIC_PRIVOD.fmg( 482, 261):���������� <
      L0_uk=R_evi.gt.R0_of
C MECHANIC_PRIVOD.fmg( 482, 253):���������� >
      L_el = L0_al.AND.L0_uk.AND.L_il
C MECHANIC_PRIVOD.fmg( 493, 256):�
      L_eve=L_el
C MECHANIC_PRIVOD.fmg( 520, 256):������,POS9_STOP
      L0_ud=R_evi.lt.R0_ad
C MECHANIC_PRIVOD.fmg( 482, 221):���������� <
      L0_od=R_evi.gt.R0_i
C MECHANIC_PRIVOD.fmg( 482, 213):���������� >
      L_af = L0_ud.AND.L0_od.AND.L_ef
C MECHANIC_PRIVOD.fmg( 493, 216):�
      L_ave=L_af
C MECHANIC_PRIVOD.fmg( 520, 216):������,POS10_STOP
      L_eri = L_ifi.OR.L_edi.OR.L_adi.OR.L_ubi.OR.L_ixe.OR.L_uve.OR.L_ov
     &e.OR.L_ive.OR.L_eve.OR.L_ave
C MECHANIC_PRIVOD.fmg( 238, 375):���
      L0_asi = L_isi.OR.L_iri.OR.L_eri
C MECHANIC_PRIVOD.fmg(  47, 248):���
      L0_usi = (.NOT.L0_ubo).AND.L0_asi.AND.(.NOT.L0_exi)
C MECHANIC_PRIVOD.fmg(  73, 248):�
      L_ufo=R_ofo.lt.R0_axi
C MECHANIC_PRIVOD.fmg( 215, 251):���������� <
      L_ako=R_ofo.gt.R0_ovi
C MECHANIC_PRIVOD.fmg( 215, 274):���������� >
      L0_edo = L_ufo.OR.L_ako
C MECHANIC_PRIVOD.fmg( 165, 220):���
      L0_osi = L0_usi.OR.L0_ati.OR.L0_edo
C MECHANIC_PRIVOD.fmg(  86, 250):���
      L0_api=L_elo.and..not.L_imi
      L_imi=L_elo
C MECHANIC_PRIVOD.fmg(  94, 229):������������  �� 1 ���
      L0_umi = L_elo.AND.L_iko
C MECHANIC_PRIVOD.fmg( 114, 246):�
      L0_odo = L0_api.OR.L0_osi.OR.L0_umi
C MECHANIC_PRIVOD.fmg( 100, 227):���
      L0_eki=R_evi.gt.R0_efi
C MECHANIC_PRIVOD.fmg( 357, 383):���������� >
      L_aki = L0_eki.AND.L_ali
C MECHANIC_PRIVOD.fmg( 368, 382):�
      L0_are=R_evi.gt.R0_ope
C MECHANIC_PRIVOD.fmg( 357, 344):���������� >
      L_obi = L0_are.AND.L_ure
C MECHANIC_PRIVOD.fmg( 368, 343):�
      L0_ule=R_evi.gt.R0_ile
C MECHANIC_PRIVOD.fmg( 357, 306):���������� >
      L_ibi = L0_ule.AND.L_ome
C MECHANIC_PRIVOD.fmg( 368, 305):�
      L0_ofe=R_evi.gt.R0_efe
C MECHANIC_PRIVOD.fmg( 357, 267):���������� >
      L_ebi = L0_ofe.AND.L_ike
C MECHANIC_PRIVOD.fmg( 368, 266):�
      L0_ibe=R_evi.gt.R0_abe
C MECHANIC_PRIVOD.fmg( 357, 227):���������� >
      L_exe = L0_ibe.AND.L_ede
C MECHANIC_PRIVOD.fmg( 368, 226):�
      L0_ev=R_evi.gt.R0_ut
C MECHANIC_PRIVOD.fmg( 482, 383):���������� >
      L_ute = L0_ev.AND.L_ax
C MECHANIC_PRIVOD.fmg( 493, 382):�
      L0_as=R_evi.gt.R0_or
C MECHANIC_PRIVOD.fmg( 482, 344):���������� >
      L_ote = L0_as.AND.L_us
C MECHANIC_PRIVOD.fmg( 493, 343):�
      L0_um=R_evi.gt.R0_im
C MECHANIC_PRIVOD.fmg( 482, 306):���������� >
      L_ite = L0_um.AND.L_op
C MECHANIC_PRIVOD.fmg( 493, 305):�
      L0_ok=R_evi.gt.R0_ek
C MECHANIC_PRIVOD.fmg( 482, 267):���������� >
      L_ete = L0_ok.AND.L_il
C MECHANIC_PRIVOD.fmg( 493, 266):�
      L0_id=R_evi.gt.R0_ad
C MECHANIC_PRIVOD.fmg( 482, 227):���������� >
      L_ate = L0_id.AND.L_ef
C MECHANIC_PRIVOD.fmg( 493, 226):�
      L_opi = L_aki.OR.L_obi.OR.L_ibi.OR.L_ebi.OR.L_exe.OR.L_ute.OR.L_ot
     &e.OR.L_ite.OR.L_ete.OR.L_ate
C MECHANIC_PRIVOD.fmg( 238, 348):���
      L0_uri = L_oti.OR.L_upi.OR.L_opi
C MECHANIC_PRIVOD.fmg(  47, 228):���
      L0_ixi = (.NOT.L0_usi).AND.(.NOT.L0_ati).AND.(.NOT.L0_ubo
     &).AND.L0_uri
C MECHANIC_PRIVOD.fmg(  73, 231):�
      L0_ido = L0_ixi.OR.L0_exi
C MECHANIC_PRIVOD.fmg(  86, 232):���
      L_iko=L0_ido.or.(L_iko.and..not.(L0_odo))
      L0_udo=.not.L_iko
C MECHANIC_PRIVOD.fmg( 107, 230):RS �������
      L0_omi=L_iko.and..not.L_emi
      L_emi=L_iko
C MECHANIC_PRIVOD.fmg(  94, 258):������������  �� 1 ���
      L0_efo = L0_osi.OR.L0_omi.OR.L0_umi
C MECHANIC_PRIVOD.fmg( 100, 258):���
      L0_ado = L0_eti.AND.(.NOT.L0_ati).AND.(.NOT.L0_exi).AND.
     &(.NOT.L0_usi)
C MECHANIC_PRIVOD.fmg(  73, 264):�
      L0_afo = L0_ado.OR.L0_ubo
C MECHANIC_PRIVOD.fmg(  86, 263):���
      L_elo=L0_afo.or.(L_elo.and..not.(L0_efo))
      L0_ifo=.not.L_elo
C MECHANIC_PRIVOD.fmg( 107, 261):RS �������
C sav1=L0_umi
      L0_umi = L_elo.AND.L_iko
C MECHANIC_PRIVOD.fmg( 114, 246):recalc:�
C if(sav1.ne.L0_umi .and. try255.gt.0) goto 255
      if(L_elo) then
         R0_ilo=R0_olo
      else
         R0_ilo=R0_ulo
      endif
C MECHANIC_PRIVOD.fmg( 125, 270):���� RE IN LO CH7
      if(L_iko) then
         R0_oko=R0_uko
      else
         R0_oko=R0_alo
      endif
C MECHANIC_PRIVOD.fmg( 125, 257):���� RE IN LO CH7
      R0_eko = R0_ilo + R0_oko
C MECHANIC_PRIVOD.fmg( 133, 263):��������
      if(L0_abo) then
         R_ofo=R0_obo
      elseif(L0_ebo) then
         R_ofo=R_ofo
      else
         R_ofo=R_ofo+deltat/R0_uxi*R0_eko
      endif
      if(R_ofo.gt.R_oxi) then
         R_ofo=R_oxi
      elseif(R_ofo.lt.R_ibo) then
         R_ofo=R_ibo
      endif
C MECHANIC_PRIVOD.fmg( 146, 263):����������
      R_evi=R_ofo
C MECHANIC_PRIVOD.fmg( 242, 263):������,POS
      L0_ofi=R_evi.lt.R0_odi
C MECHANIC_PRIVOD.fmg( 357, 362):���������� <
      L_ufi = L0_ofi.AND.L_ali
C MECHANIC_PRIVOD.fmg( 368, 361):�
      L0_upe=R_evi.lt.R0_ape
C MECHANIC_PRIVOD.fmg( 357, 323):���������� <
      L_abi = L0_upe.AND.L_ure
C MECHANIC_PRIVOD.fmg( 368, 322):�
      L0_ole=R_evi.lt.R0_uke
C MECHANIC_PRIVOD.fmg( 357, 285):���������� <
      L_uxe = L0_ole.AND.L_ome
C MECHANIC_PRIVOD.fmg( 368, 284):�
      L0_ife=R_evi.lt.R0_ode
C MECHANIC_PRIVOD.fmg( 357, 246):���������� <
      L_oxe = L0_ife.AND.L_ike
C MECHANIC_PRIVOD.fmg( 368, 245):�
      L0_ebe=R_evi.lt.R0_ix
C MECHANIC_PRIVOD.fmg( 357, 206):���������� <
      L_axe = L0_ebe.AND.L_ede
C MECHANIC_PRIVOD.fmg( 368, 205):�
      L0_av=R_evi.lt.R0_et
C MECHANIC_PRIVOD.fmg( 482, 362):���������� <
      L_use = L0_av.AND.L_ax
C MECHANIC_PRIVOD.fmg( 493, 361):�
      L0_ur=R_evi.lt.R0_ar
C MECHANIC_PRIVOD.fmg( 482, 323):���������� <
      L_ose = L0_ur.AND.L_us
C MECHANIC_PRIVOD.fmg( 493, 322):�
      L0_om=R_evi.lt.R0_ul
C MECHANIC_PRIVOD.fmg( 482, 285):���������� <
      L_ise = L0_om.AND.L_op
C MECHANIC_PRIVOD.fmg( 493, 284):�
      L0_ik=R_evi.lt.R0_of
C MECHANIC_PRIVOD.fmg( 482, 246):���������� <
      L_ese = L0_ik.AND.L_il
C MECHANIC_PRIVOD.fmg( 493, 245):�
      L0_ed=R_evi.lt.R0_i
C MECHANIC_PRIVOD.fmg( 482, 206):���������� <
      L_ase = L0_ed.AND.L_ef
C MECHANIC_PRIVOD.fmg( 493, 205):�
      L_ari = L_ufi.OR.L_abi.OR.L_uxe.OR.L_oxe.OR.L_axe.OR.L_use.OR.L_os
     &e.OR.L_ise.OR.L_ese.OR.L_ase
C MECHANIC_PRIVOD.fmg( 238, 319):���
C sav1=L0_eti
      L0_eti = L_avi.OR.L_ori.OR.L_ari
C MECHANIC_PRIVOD.fmg(  47, 267):recalc:���
C if(sav1.ne.L0_eti .and. try113.gt.0) goto 113
      R_ami = R0_eko * R_ipi
C MECHANIC_PRIVOD.fmg( 129, 286):����������
      End
