      Interface
      Subroutine MECHANIC_PRIVOD(ext_deltat,R_u,L_af,L_ef
     &,R_ak,L_el,L_il,R_em,L_ip,L_op,R_ir,L_os,L_us,R_ot,L_uv
     &,L_ax,R_ux,L_ade,L_ede,R_afe,L_eke,L_ike,R_ele,L_ime
     &,L_ome,R_ipe,L_ore,L_ure,L_ase,L_ese,L_ise,L_ose,L_use
     &,L_ate,L_ete,L_ite,L_ote,L_ute,L_ave,L_eve,L_ive,L_ove
     &,L_uve,L_axe,L_exe,L_ixe,L_oxe,L_uxe,L_abi,L_ebi,L_ibi
     &,L_obi,L_ubi,L_adi,L_edi,R_afi,L_ifi,L_ufi,L_aki,L_uki
     &,L_ali,L_eli,L_ili,L_oli,L_uli,R_ami,L_emi,L_imi,R_ipi
     &,L_opi,L_upi,L_ari,L_eri,L_iri,L_ori,I_esi,L_isi,I_iti
     &,L_oti,I_uti,L_avi,R_evi,R_oxi,R_ibo,R_ofo,L_ufo,L_ako
     &,L_iko,L_elo)
C |R_u           |4 4 I|pos10_x         |���������� ��������� 10||
C |L_af          |1 1 O|POS10_READY     |�������� ��������� 5||
C |L_ef          |1 1 I|POS10C          |������� ������� � ��������� 5||
C |R_ak          |4 4 I|pos9_x          |���������� ��������� 9||
C |L_el          |1 1 O|POS9_READY      |�������� ��������� 4||
C |L_il          |1 1 I|POS9C           |������� ������� � ��������� 4||
C |R_em          |4 4 I|pos8_x          |���������� ��������� 8||
C |L_ip          |1 1 O|POS8_READY      |�������� ��������� 3||
C |L_op          |1 1 I|POS8C           |������� ������� � ��������� 3||
C |R_ir          |4 4 I|pos7_x          |���������� ��������� 7||
C |L_os          |1 1 O|POS7_READY      |�������� ��������� 2||
C |L_us          |1 1 I|POS7C           |������� ������� � ��������� 2||
C |R_ot          |4 4 I|pos6_x          |���������� ��������� 6||
C |L_uv          |1 1 O|POS6_READY      |�������� ��������� 1||
C |L_ax          |1 1 I|POS6C           |������� ������� � ��������� 1||
C |R_ux          |4 4 I|pos5_x          |���������� ��������� 5||
C |L_ade         |1 1 O|POS5_READY      |�������� ��������� 5||
C |L_ede         |1 1 I|POS5C           |������� ������� � ��������� 5||
C |R_afe         |4 4 I|pos4_x          |���������� ��������� 4||
C |L_eke         |1 1 O|POS4_READY      |�������� ��������� 4||
C |L_ike         |1 1 I|POS4C           |������� ������� � ��������� 4||
C |R_ele         |4 4 I|pos3_x          |���������� ��������� 3||
C |L_ime         |1 1 O|POS3_READY      |�������� ��������� 3||
C |L_ome         |1 1 I|POS3C           |������� ������� � ��������� 3||
C |R_ipe         |4 4 I|pos2_x          |���������� ��������� 2||
C |L_ore         |1 1 O|POS2_READY      |�������� ��������� 2||
C |L_ure         |1 1 I|POS2C           |������� ������� � ��������� 2||
C |L_ase         |1 1 O|POS10_UP        |������� ������||
C |L_ese         |1 1 O|POS9_UP         |������� ������||
C |L_ise         |1 1 O|POS8_UP         |������� ������||
C |L_ose         |1 1 O|POS7_UP         |������� ������||
C |L_use         |1 1 O|POS6_UP         |������� ������||
C |L_ate         |1 1 O|POS10_DOWN      |������� �����||
C |L_ete         |1 1 O|POS9_DOWN       |������� �����||
C |L_ite         |1 1 O|POS8_DOWN       |������� �����||
C |L_ote         |1 1 O|POS7_DOWN       |������� �����||
C |L_ute         |1 1 O|POS6_DOWN       |������� �����||
C |L_ave         |1 1 O|POS10_STOP      |������� ����||
C |L_eve         |1 1 O|POS9_STOP       |������� ����||
C |L_ive         |1 1 O|POS8_STOP       |������� ����||
C |L_ove         |1 1 O|POS7_STOP       |������� ����||
C |L_uve         |1 1 O|POS6_STOP       |������� ����||
C |L_axe         |1 1 O|POS5_UP         |������� ������||
C |L_exe         |1 1 O|POS5_DOWN       |������� �����||
C |L_ixe         |1 1 O|POS5_STOP       |������� ����||
C |L_oxe         |1 1 O|POS4_UP         |������� ������||
C |L_uxe         |1 1 O|POS3_UP         |������� ������||
C |L_abi         |1 1 O|POS2_UP         |������� ������||
C |L_ebi         |1 1 O|POS4_DOWN       |������� �����||
C |L_ibi         |1 1 O|POS3_DOWN       |������� �����||
C |L_obi         |1 1 O|POS2_DOWN       |������� �����||
C |L_ubi         |1 1 O|POS4_STOP       |������� ����||
C |L_adi         |1 1 O|POS3_STOP       |������� ����||
C |L_edi         |1 1 O|POS2_STOP       |������� ����||
C |R_afi         |4 4 I|pos1_x          |���������� ��������� 1||
C |L_ifi         |1 1 O|POS1_STOP       |������� ����||
C |L_ufi         |1 1 O|POS1_UP         |������� ������||
C |L_aki         |1 1 O|POS1_DOWN       |������� �����||
C |L_uki         |1 1 O|POS1_READY      |�������� ��������� 1||
C |L_ali         |1 1 I|POS1C           |������� ������� � ��������� 1||
C |L_eli         |1 1 O|UNCATCH         |��������� �������|F|
C |L_ili         |1 1 I|YA31            |������� ��������� �������|F|
C |L_oli         |1 1 O|CATCH           |������ �������|F|
C |L_uli         |1 1 I|YA30            |������� ������ �������|F|
C |R_ami         |4 4 O|SPEED           |�������� ������� [��/�]||
C |L_emi         |1 1 S|_splsJ202*      |[TF]���������� ��������� ������������� |F|
C |L_imi         |1 1 S|_splsJ197*      |[TF]���������� ��������� ������������� |F|
C |R_ipi         |4 4 I|vel             |�������� ������� [��/�]||
C |L_opi         |1 1 O|POS_DOWN_GLOB   |||
C |L_upi         |1 1 I|DOWNC           |������ �����. ���������� �������||
C |L_ari         |1 1 O|POS_UP_GLOB     |||
C |L_eri         |1 1 O|POS_STOP_GLOB   |||
C |L_iri         |1 1 I|STOPC           |������ ����. ���������� �������||
C |L_ori         |1 1 I|UPC             |������ ������. ���������� �������||
C |I_esi         |2 4 I|STOP_USER       |������ ����. ������� �� ���������||
C |L_isi         |1 1 I|STOP            |������ ����. ������� �� ����������||
C |I_iti         |2 4 I|DOWN_USER       |������ �����. ������� �� ���������||
C |L_oti         |1 1 I|DOWN            |������ �����. ������� �� ����������||
C |I_uti         |2 4 I|UP_USER         |������ ������. ������� �� ���������||
C |L_avi         |1 1 I|UP              |������ ������. ������� �� ����������||
C |R_evi         |4 4 O|POS             |��������� ������� [��]||
C |R_oxi         |4 4 I|max             |����. ��������� ������� [��]||
C |R_ibo         |4 4 I|min             |���. ��������� ������� [��]||
C |R_ofo         |4 4 S|_ointJ59*       |����� ����������� |0.0|
C |L_ufo         |1 1 O|XH52            |������ � MIN||
C |L_ako         |1 1 O|XH51            |������ � MAX||
C |L_iko         |1 1 S|_qffJ43*        |�������� ������ Q RS-��������  |F|
C |L_elo         |1 1 S|_qffJ42*        |�������� ������ Q RS-��������  |F|

      IMPLICIT NONE
      REAL*4 ext_deltat
      REAL*4 R_u
      LOGICAL*1 L_af,L_ef
      REAL*4 R_ak
      LOGICAL*1 L_el,L_il
      REAL*4 R_em
      LOGICAL*1 L_ip,L_op
      REAL*4 R_ir
      LOGICAL*1 L_os,L_us
      REAL*4 R_ot
      LOGICAL*1 L_uv,L_ax
      REAL*4 R_ux
      LOGICAL*1 L_ade,L_ede
      REAL*4 R_afe
      LOGICAL*1 L_eke,L_ike
      REAL*4 R_ele
      LOGICAL*1 L_ime,L_ome
      REAL*4 R_ipe
      LOGICAL*1 L_ore,L_ure,L_ase,L_ese,L_ise,L_ose,L_use
     &,L_ate,L_ete,L_ite,L_ote,L_ute,L_ave,L_eve,L_ive,L_ove
     &,L_uve,L_axe,L_exe,L_ixe,L_oxe,L_uxe
      LOGICAL*1 L_abi,L_ebi,L_ibi,L_obi,L_ubi,L_adi,L_edi
      REAL*4 R_afi
      LOGICAL*1 L_ifi,L_ufi,L_aki,L_uki,L_ali,L_eli,L_ili
     &,L_oli,L_uli
      REAL*4 R_ami
      LOGICAL*1 L_emi,L_imi
      REAL*4 R_ipi
      LOGICAL*1 L_opi,L_upi,L_ari,L_eri,L_iri,L_ori
      INTEGER*4 I_esi
      LOGICAL*1 L_isi
      INTEGER*4 I_iti
      LOGICAL*1 L_oti
      INTEGER*4 I_uti
      LOGICAL*1 L_avi
      REAL*4 R_evi,R_oxi,R_ibo,R_ofo
      LOGICAL*1 L_ufo,L_ako,L_iko,L_elo
      End subroutine MECHANIC_PRIVOD
      End interface
