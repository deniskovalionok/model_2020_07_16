      Interface
      Subroutine BVALVE_MAN(ext_deltat,R_odi,L_edi,L_ixe,R8_ere
     &,L_e,L_ad,L_ed,I_af,I_ef,R_uf,R_ak,R_ek,R_ik,R_ok,R_uk
     &,I_al,I_ol,I_em,I_ap,L_ep,L_op,L_up,L_ar,L_er,L_or,L_ur
     &,L_os,L_us,L_it,L_ot,L_av,L_ev,L_ov,R8_ax,R_ux,R8_ibe
     &,L_obe,L_ade,L_ope,R_ore,R_ase,L_axe,L_obi,L_udi,L_afi
     &,L_efi,L_ifi,L_ofi,L_ufi)
C |R_odi         |4 4 I|11 mlfpar19     |��������� ������������|0.6|
C |L_edi         |1 1 O|13 cbo          |�� ������� (���)|F|
C |L_ixe         |1 1 O|14 cbc          |�� ������� (���)|F|
C |R8_ere        |4 8 O|16 value        |��� �������� ��������|0.5|
C |L_e           |1 1 I|tech_mode       |||
C |L_ad          |1 1 I|ruch_mode       |||
C |L_ed          |1 1 I|avt_mode        |||
C |I_af          |2 4 O|LM              |�����||
C |I_ef          |2 4 O|LF              |����� �������������||
C |R_uf          |4 4 S|vmstop_button_ST*|��������� ������ "������� ���� �� ���������" |0.0|
C |R_ak          |4 4 I|vmstop_button   |������� ������ ������ "������� ���� �� ���������" |0.0|
C |R_ek          |4 4 S|vmclose_button_ST*|��������� ������ "������� ������� �� ���������" |0.0|
C |R_ik          |4 4 I|vmclose_button  |������� ������ ������ "������� ������� �� ���������" |0.0|
C |R_ok          |4 4 S|vmopen_button_ST*|��������� ������ "������� ������� �� ���������" |0.0|
C |R_uk          |4 4 I|vmopen_button   |������� ������ ������ "������� ������� �� ���������" |0.0|
C |I_al          |2 4 O|LST             |����� ����||
C |I_ol          |2 4 O|LCL             |����� �������||
C |I_em          |2 4 O|LOP             |����� �������||
C |I_ap          |2 4 O|LS              |��������� �������||
C |L_ep          |1 1 I|vlv_kvit        |||
C |L_op          |1 1 I|instr_fault     |||
C |L_up          |1 1 S|_qffJ453*       |�������� ������ Q RS-��������  |F|
C |L_ar          |1 1 O|vmclose_button_CMD*|[TF]����� ������ ������� ������� �� ���������|F|
C |L_er          |1 1 O|vmopen_button_CMD*|[TF]����� ������ ������� ������� �� ���������|F|
C |L_or          |1 1 I|ulubclose       |���������� ������� �� ���������|F|
C |L_ur          |1 1 I|ulubopen        |���������� ������� �� ���������|F|
C |L_os          |1 1 O|block           |||
C |L_us          |1 1 O|vmstop_button_CMD*|[TF]����� ������ ������� ���� �� ���������|F|
C |L_it          |1 1 O|STOP            |�������||
C |L_ot          |1 1 S|_qff10*         |�������� ������ Q RS-��������  |F|
C |L_av          |1 1 O|fault           |�������������||
C |L_ev          |1 1 O|norm            |�����||
C |L_ov          |1 1 O|nopower         |��� ����������||
C |R8_ax         |4 8 I|voltage         |[��]���������� �� ������||
C |R_ux          |4 4 I|power           |�������� ��������||
C |R8_ibe        |4 8 O|sn_power        |�������� ���� ��|3.0|
C |L_obe         |1 1 I|uluclose        |������� ������� �� ����������|F|
C |L_ade         |1 1 I|uluopen         |������� ������� �� ����������|F|
C |L_ope         |1 1 S|_qff6*          |�������� ������ Q RS-��������  |F|
C |R_ore         |4 4 O|mf_value        |��� ��������� �����|0.5|
C |R_ase         |4 4 I|tcl_top         |����� ����|30.0|
C |L_axe         |1 1 S|_qff2*          |�������� ������ Q RS-��������  |F|
C |L_obi         |1 1 S|_qff1*          |�������� ������ Q RS-��������  |F|
C |L_udi         |1 1 I|mlf24           |��� ���������� � ����������||
C |L_afi         |1 1 I|mlf23           |������� ������� �����||
C |L_efi         |1 1 I|mlf22           |����� ����� ��������||
C |L_ifi         |1 1 I|mlf04           |�������� �� ��������||
C |L_ofi         |1 1 I|mlf03           |�������� �� ��������||
C |L_ufi         |1 1 I|mlf19           |���� ������������||

      IMPLICIT NONE
      REAL*4 ext_deltat
      LOGICAL*1 L_e,L_ad,L_ed
      INTEGER*4 I_af,I_ef
      REAL*4 R_uf,R_ak,R_ek,R_ik,R_ok,R_uk
      INTEGER*4 I_al,I_ol,I_em,I_ap
      LOGICAL*1 L_ep,L_op,L_up,L_ar,L_er,L_or,L_ur,L_os,L_us
     &,L_it,L_ot,L_av,L_ev,L_ov
      REAL*8 R8_ax
      REAL*4 R_ux
      REAL*8 R8_ibe
      LOGICAL*1 L_obe,L_ade,L_ope
      REAL*8 R8_ere
      REAL*4 R_ore,R_ase
      LOGICAL*1 L_axe,L_ixe,L_obi,L_edi
      REAL*4 R_odi
      LOGICAL*1 L_udi,L_afi,L_efi,L_ifi,L_ofi,L_ufi
      End subroutine BVALVE_MAN
      End interface
