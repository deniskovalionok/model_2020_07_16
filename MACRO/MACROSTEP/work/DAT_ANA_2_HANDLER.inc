      Interface
      Subroutine DAT_ANA_2_HANDLER(ext_deltat,R_e,R_i,R_ed
     &,R_id,I_al,R_im,R_um,R_ap,R_ep,R_os,L_us,R_et,L_it,L_ot
     &,R_av,R_ux,R_ebe,L_obe)
C |R_e           |4 4 O|nom             |||
C |R_i           |4 4 K|_cJ1505         |�������� ��������� ����������|0.3|
C |R_ed          |4 4 I|koeff_units     |����������� �����������||
C |R_id          |4 4 I|input           |����||
C |I_al          |2 4 O|dat_color       |���� ��������||
C |R_im          |4 4 I|LOWER_warning_setpoint|||
C |R_um          |4 4 I|LOWER_alarm_setpoint|||
C |R_ap          |4 4 I|UPPER_alarm_setpoint|||
C |R_ep          |4 4 I|UPPER_warning_setpoint|||
C |R_os          |4 4 I|GM05V           |���������� ����������� �������||
C |L_us          |1 1 I|GM05            |���������� ����������� �������||
C |R_et          |4 4 I|GM04V           |������ �������� ���������� ��������||
C |L_it          |1 1 I|GM04            |������ �������� ���������� ��������||
C |L_ot          |1 1 I|GM02            |����� �� ������������ ������ ���������||
C |R_av          |4 4 O|KKS             |�����||
C |R_ux          |4 4 I|MAXIMUM         |||
C |R_ebe         |4 4 I|MINIMUM         |||
C |L_obe         |1 1 I|GM01            |����� �� ����������� ������ ���������||

      IMPLICIT NONE
      REAL*4 ext_deltat
      REAL*4 R_e,R_i,R_ed,R_id
      INTEGER*4 I_al
      REAL*4 R_im,R_um,R_ap,R_ep,R_os
      LOGICAL*1 L_us
      REAL*4 R_et
      LOGICAL*1 L_it,L_ot
      REAL*4 R_av,R_ux,R_ebe
      LOGICAL*1 L_obe
      End subroutine DAT_ANA_2_HANDLER
      End interface
