      Interface
      Subroutine pid_tuned(ext_deltat,L_e,R8_id,R_ef,R_ak
     &,R_ok,R_uk,R_al,R_el,R_il,R_ol,R_ul,R_ep,R_ar)
C |L_e           |1 1 I|flagmod         |����� ������� �����||
C |R8_id         |4 8 O|out             |����� ��������� ��������||
C |R_ef          |4 4 I|k4              |�-� ����������� ��||
C |R_ak          |4 4 I|t3              |���������� ������� ���������������||
C |R_ok          |4 4 S|_sdif1*         |���������� ��������� |0.0|
C |R_uk          |4 4 I|k3              |�-� ���������������||
C |R_al          |4 4 I|u1              |����������� �����������||
C |R_el          |4 4 I|k2              |�-� ������������||
C |R_il          |4 4 O|dlt             |������� ��������� ����������||
C |R_ol          |4 4 I|k1              |�-� ����������������||
C |R_ul          |4 4 O|_odif1*         |�������� ������ |0.0|
C |R_ep          |4 4 S|_oint1*         |����� ����������� |0.0|
C |R_ar          |4 4 I|delta           |������� ��������� ����������||

      IMPLICIT NONE
      REAL*4 ext_deltat
      LOGICAL*1 L_e
      REAL*8 R8_id
      REAL*4 R_ef,R_ak,R_ok,R_uk,R_al,R_el,R_il,R_ol,R_ul
     &,R_ep,R_ar
      End subroutine pid_tuned
      End interface
