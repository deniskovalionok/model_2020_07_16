      Subroutine KLAPAN_MAN(ext_deltat,R_aki,R8_ose,I_e,C8_ef
     &,I_if,R_ek,R_ok,R_uk,R_al,R_el,R_il,R_ol,R_ul,I_am,I_om
     &,I_ep,I_up,L_er,L_or,L_ur,L_as,L_es,R_os,L_us,L_at,L_ut
     &,L_av,L_ov,L_uv,L_ex,L_ox,R8_abe,R_ube,R8_ide,L_ode
     &,L_ude,L_efe,L_ife,L_uke,I_ale,L_ase,R_ate,R_axe,L_ibi
     &,L_ubi,L_afi,L_ofi,L_eki,L_iki,L_oki,L_uki,L_ali,L_eli
     &)
C |R_aki         |4 4 I|11 mlfpar19     |��������� ������������|0.6|
C |R8_ose        |4 8 O|16 value        |��� �������� ��������|0.5|
C |I_e           |2 4 O|LREADY          |����� ����������||
C |C8_ef         |3 8 O|TEXT            |�������� ���������||
C |I_if          |2 4 O|LF              |����� �������������||
C |R_ek          |4 4 O|POS_CL          |��������||
C |R_ok          |4 4 O|POS_OP          |��������||
C |R_uk          |4 4 S|vmstop_button_ST*|��������� ������ "������� ���� �� ���������" |0.0|
C |R_al          |4 4 I|vmstop_button   |������� ������ ������ "������� ���� �� ���������" |0.0|
C |R_el          |4 4 S|vmclose_button_ST*|��������� ������ "������� ������� �� ���������" |0.0|
C |R_il          |4 4 I|vmclose_button  |������� ������ ������ "������� ������� �� ���������" |0.0|
C |R_ol          |4 4 S|vmopen_button_ST*|��������� ������ "������� ������� �� ���������" |0.0|
C |R_ul          |4 4 I|vmopen_button   |������� ������ ������ "������� ������� �� ���������" |0.0|
C |I_am          |2 4 O|LST             |����� ����||
C |I_om          |2 4 O|LCL             |����� �������||
C |I_ep          |2 4 O|LOP             |����� �������||
C |I_up          |2 4 O|LZM             |������ "�������"||
C |L_er          |1 1 I|vlv_kvit        |||
C |L_or          |1 1 I|instr_fault     |||
C |L_ur          |1 1 S|_qffJ453*       |�������� ������ Q RS-��������  |F|
C |L_as          |1 1 O|vmclose_button_CMD*|[TF]����� ������ ������� ������� �� ���������|F|
C |L_es          |1 1 O|vmopen_button_CMD*|[TF]����� ������ ������� ������� �� ���������|F|
C |R_os          |4 4 I|tcl             |����� ��������|30.0|
C |L_us          |1 1 I|ulubclose       |���������� ������� �� ���������|F|
C |L_at          |1 1 I|ulubopen        |���������� ������� �� ���������|F|
C |L_ut          |1 1 O|block           |||
C |L_av          |1 1 O|vmstop_button_CMD*|[TF]����� ������ ������� ���� �� ���������|F|
C |L_ov          |1 1 O|STOP            |�������||
C |L_uv          |1 1 S|_qff10*         |�������� ������ Q RS-��������  |F|
C |L_ex          |1 1 O|norm            |�����||
C |L_ox          |1 1 O|nopower         |��� ����������||
C |R8_abe        |4 8 I|voltage         |[��]���������� �� ������||
C |R_ube         |4 4 I|power           |�������� ��������||
C |R8_ide        |4 8 O|sn_power        |�������� ���� ��|3.0|
C |L_ode         |1 1 I|YA22C           |������� ������� �� ����������|F|
C |L_ude         |1 1 I|YA22            |������� ������� �� ����������|F|
C |L_efe         |1 1 I|YA21C           |������� ������� �� ����������|F|
C |L_ife         |1 1 I|YA21            |������� ������� �� ����������|F|
C |L_uke         |1 1 O|fault           |�������������||
C |I_ale         |2 4 O|LAM             |������ "�������"||
C |L_ase         |1 1 S|_qff6*          |�������� ������ Q RS-��������  |F|
C |R_ate         |4 4 O|mf_value        |��� ��������� �����|0.5|
C |R_axe         |4 4 I|top             |����� ��������|30.0|
C |L_ibi         |1 1 S|_qff2*          |�������� ������ Q RS-��������  |F|
C |L_ubi         |1 1 O|XH52            |�� ������� (���)|F|
C |L_afi         |1 1 S|_qff1*          |�������� ������ Q RS-��������  |F|
C |L_ofi         |1 1 O|XH51            |�� ������� (���)|F|
C |L_eki         |1 1 I|mlf24           |��� ���������� � ����������||
C |L_iki         |1 1 I|mlf23           |������� ������� �����||
C |L_oki         |1 1 I|mlf22           |����� ����� ��������||
C |L_uki         |1 1 I|mlf04           |�������� �� ��������||
C |L_ali         |1 1 I|mlf03           |�������� �� ��������||
C |L_eli         |1 1 I|mlf19           |���� ������������||

      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      INTEGER*4 I_e,I0_i,I0_o
      LOGICAL*1 L0_u
      CHARACTER*8 C8_ad,C8_ed,C8_id,C8_od,C8_ud,C8_af,C8_ef
      INTEGER*4 I_if,I0_of,I0_uf
      REAL*4 R0_ak,R_ek,R0_ik,R_ok,R_uk,R_al,R_el,R_il,R_ol
     &,R_ul
      INTEGER*4 I_am,I0_em,I0_im,I_om,I0_um,I0_ap,I_ep,I0_ip
     &,I0_op,I_up,I0_ar
      LOGICAL*1 L_er,L0_ir,L_or,L_ur,L_as,L_es
      REAL*4 R0_is,R_os
      LOGICAL*1 L_us,L_at,L0_et,L0_it,L0_ot,L_ut,L_av,L0_ev
     &,L0_iv,L_ov,L_uv,L0_ax,L_ex,L0_ix,L_ox
      REAL*4 R0_ux
      REAL*8 R8_abe
      LOGICAL*1 L0_ebe,L0_ibe
      REAL*4 R0_obe,R_ube,R0_ade,R0_ede
      REAL*8 R8_ide
      LOGICAL*1 L_ode,L_ude,L0_afe,L_efe,L_ife,L0_ofe
      INTEGER*4 I0_ufe,I0_ake,I0_eke,I0_ike,I0_oke
      LOGICAL*1 L_uke
      INTEGER*4 I_ale,I0_ele,I0_ile
      LOGICAL*1 L0_ole,L0_ule
      REAL*4 R0_ame,R0_eme
      LOGICAL*1 L0_ime
      REAL*4 R0_ome,R0_ume,R0_ape,R0_epe,R0_ipe,R0_ope
      LOGICAL*1 L0_upe
      REAL*4 R0_are,R0_ere,R0_ire,R0_ore
      LOGICAL*1 L0_ure,L_ase
      REAL*4 R0_ese,R0_ise
      REAL*8 R8_ose
      REAL*4 R0_use,R_ate,R0_ete,R0_ite,R0_ote,R0_ute,R0_ave
     &,R0_eve,R0_ive,R0_ove,R0_uve,R_axe
      LOGICAL*1 L0_exe,L0_ixe,L0_oxe,L0_uxe,L0_abi,L0_ebi
     &,L_ibi,L0_obi,L_ubi,L0_adi,L0_edi,L0_idi,L0_odi,L0_udi
     &,L_afi,L0_efi,L0_ifi,L_ofi,L0_ufi
      REAL*4 R_aki
      LOGICAL*1 L_eki,L_iki,L_oki,L_uki,L_ali,L_eli

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      I0_o = z'01000003'
C KLAPAN_MAN.fmg( 328, 204):��������� ������������� IN (�������)
      I0_i = z'0100000A'
C KLAPAN_MAN.fmg( 328, 202):��������� ������������� IN (�������)
      I0_ap = z'01000003'
C KLAPAN_MAN.fmg( 114, 171):��������� ������������� IN (�������)
      I0_um = z'0100000A'
C KLAPAN_MAN.fmg( 114, 169):��������� ������������� IN (�������)
      C8_ad = '������'
C KLAPAN_MAN.fmg( 205, 224):��������� ���������� CH8 (�������)
      C8_id = '������'
C KLAPAN_MAN.fmg( 180, 226):��������� ���������� CH8 (�������)
      C8_ud = '����'
C KLAPAN_MAN.fmg( 165, 226):��������� ���������� CH8 (�������)
      C8_af = '����'
C KLAPAN_MAN.fmg( 165, 228):��������� ���������� CH8 (�������)
      I0_ufe = z'01000009'
C KLAPAN_MAN.fmg( 152, 207):��������� ������������� IN (�������)
      I0_ake = z'01000003'
C KLAPAN_MAN.fmg( 152, 209):��������� ������������� IN (�������)
      I0_ar = z'0100000E'
C KLAPAN_MAN.fmg( 188, 201):��������� ������������� IN (�������)
      I0_of = z'01000007'
C KLAPAN_MAN.fmg( 148, 180):��������� ������������� IN (�������)
      I0_uf = z'01000003'
C KLAPAN_MAN.fmg( 148, 182):��������� ������������� IN (�������)
      R0_ak = 100
C KLAPAN_MAN.fmg( 378, 273):��������� (RE4) (�������)
      R0_ik = 100
C KLAPAN_MAN.fmg( 365, 267):��������� (RE4) (�������)
      L_av=R_al.ne.R_uk
      R_uk=R_al
C KLAPAN_MAN.fmg(  22, 208):���������� ������������� ������
      L_as=R_il.ne.R_el
      R_el=R_il
C KLAPAN_MAN.fmg(  21, 194):���������� ������������� ������
      L0_afe = (.NOT.L_us).AND.L_as
C KLAPAN_MAN.fmg(  65, 194):�
      L0_adi = L0_afe.OR.L_ude.OR.L_ode
C KLAPAN_MAN.fmg(  69, 191):���
      L_es=R_ul.ne.R_ol
      R_ol=R_ul
C KLAPAN_MAN.fmg(  22, 238):���������� ������������� ������
      L0_ofe = L_es.AND.(.NOT.L_at)
C KLAPAN_MAN.fmg(  65, 236):�
      L0_ifi = L0_ofe.OR.L_ife.OR.L_efe
C KLAPAN_MAN.fmg(  69, 233):���
      L0_ev = L0_ifi.OR.L0_adi
C KLAPAN_MAN.fmg(  74, 203):���
      L_uv=(L_av.or.L_uv).and..not.(L0_ev)
      L0_iv=.not.L_uv
C KLAPAN_MAN.fmg( 104, 205):RS �������,10
      L_ov=L_uv
C KLAPAN_MAN.fmg( 132, 207):������,STOP
      L0_oxe = L_uv.OR.L_eki
C KLAPAN_MAN.fmg(  96, 214):���
      I0_em = z'01000007'
C KLAPAN_MAN.fmg( 112, 214):��������� ������������� IN (�������)
      I0_im = z'01000003'
C KLAPAN_MAN.fmg( 112, 216):��������� ������������� IN (�������)
      if(L_uv) then
         I_am=I0_em
      else
         I_am=I0_im
      endif
C KLAPAN_MAN.fmg( 116, 214):���� RE IN LO CH7
      I0_op = z'01000003'
C KLAPAN_MAN.fmg( 120, 265):��������� ������������� IN (�������)
      I0_ip = z'0100000A'
C KLAPAN_MAN.fmg( 120, 263):��������� ������������� IN (�������)
      I0_oke = z'01000009'
C KLAPAN_MAN.fmg( 152, 264):��������� ������������� IN (�������)
      I0_ike = z'01000003'
C KLAPAN_MAN.fmg( 152, 262):��������� ������������� IN (�������)
      I0_ele = z'0100000E'
C KLAPAN_MAN.fmg( 166, 256):��������� ������������� IN (�������)
      L_ur=(L_or.or.L_ur).and..not.(L_er)
      L0_ir=.not.L_ur
C KLAPAN_MAN.fmg( 326, 178):RS �������
      L0_ot=.false.
C KLAPAN_MAN.fmg(  63, 217):��������� ���������� (�������)
      L0_it=.false.
C KLAPAN_MAN.fmg(  63, 215):��������� ���������� (�������)
      L0_et=.false.
C KLAPAN_MAN.fmg(  63, 213):��������� ���������� (�������)
      L_ut = L0_ot.OR.L0_it.OR.L0_et.OR.L_at.OR.L_us
C KLAPAN_MAN.fmg(  67, 213):���
      R0_is = DeltaT
C KLAPAN_MAN.fmg( 250, 254):��������� (RE4) (�������)
      if(R_os.ge.0.0) then
         R0_ute=R0_is/max(R_os,1.0e-10)
      else
         R0_ute=R0_is/min(R_os,-1.0e-10)
      endif
C KLAPAN_MAN.fmg( 259, 252):�������� ����������
      L0_ix =.NOT.(L_ali.OR.L_uki.OR.L_eli.OR.L_oki.OR.L_iki.OR.L_eki
     &)
C KLAPAN_MAN.fmg( 319, 191):���
      L0_ax =.NOT.(L0_ix)
C KLAPAN_MAN.fmg( 328, 185):���
      L_uke = L0_ax.OR.L_ur
C KLAPAN_MAN.fmg( 332, 184):���
      if(L_uke) then
         I_if=I0_of
      else
         I_if=I0_uf
      endif
C KLAPAN_MAN.fmg( 152, 181):���� RE IN LO CH7
      L_ex = L0_ix.OR.L_or
C KLAPAN_MAN.fmg( 328, 190):���
      if(L_ex) then
         I_e=I0_i
      else
         I_e=I0_o
      endif
C KLAPAN_MAN.fmg( 331, 202):���� RE IN LO CH7
      R0_ux = 0.1
C KLAPAN_MAN.fmg( 254, 160):��������� (RE4) (�������)
      L_ox=R8_abe.lt.R0_ux
C KLAPAN_MAN.fmg( 259, 162):���������� <
      R0_obe = 0.0
C KLAPAN_MAN.fmg( 266, 182):��������� (RE4) (�������)
      R0_ipe = 0.000001
C KLAPAN_MAN.fmg( 354, 208):��������� (RE4) (�������)
      R0_ope = 0.999999
C KLAPAN_MAN.fmg( 354, 224):��������� (RE4) (�������)
      R0_ame = 0.0
C KLAPAN_MAN.fmg( 296, 244):��������� (RE4) (�������)
      R0_eme = 0.0
C KLAPAN_MAN.fmg( 370, 242):��������� (RE4) (�������)
      L0_ime = L_iki.OR.L_oki
C KLAPAN_MAN.fmg( 363, 237):���
      R0_ume = 1.0
C KLAPAN_MAN.fmg( 348, 252):��������� (RE4) (�������)
      R0_ape = 0.0
C KLAPAN_MAN.fmg( 340, 252):��������� (RE4) (�������)
      R0_ere = 1.0e-10
C KLAPAN_MAN.fmg( 318, 228):��������� (RE4) (�������)
      R0_ese = 0.0
C KLAPAN_MAN.fmg( 328, 242):��������� (RE4) (�������)
      R0_ite = DeltaT
C KLAPAN_MAN.fmg( 250, 264):��������� (RE4) (�������)
      if(R_axe.ge.0.0) then
         R0_ave=R0_ite/max(R_axe,1.0e-10)
      else
         R0_ave=R0_ite/min(R_axe,-1.0e-10)
      endif
C KLAPAN_MAN.fmg( 259, 262):�������� ����������
      R0_uve = 0.0
C KLAPAN_MAN.fmg( 282, 244):��������� (RE4) (�������)
      L0_uxe = (.NOT.L_ov).AND.L_ubi
C KLAPAN_MAN.fmg( 102, 166):�
C label 133  try133=try133-1
      if(L_oki) then
         R0_ome=R0_eme
      else
         R0_ome=R8_ose
      endif
C KLAPAN_MAN.fmg( 373, 242):���� RE IN LO CH7
      if(.NOT.L0_ime) then
         R_ate=R8_ose
      endif
C KLAPAN_MAN.fmg( 384, 252):���� � ������������� �������
      L0_idi = (.NOT.L_ov).AND.L_ofi
C KLAPAN_MAN.fmg( 104, 260):�
      L0_udi = L_ofi.OR.L0_oxe.OR.L0_adi
C KLAPAN_MAN.fmg( 102, 229):���
      L0_ufi = (.NOT.L_ofi).AND.L0_ifi
C KLAPAN_MAN.fmg( 102, 235):�
      L_afi=(L0_ufi.or.L_afi).and..not.(L0_udi)
      L0_efi=.not.L_afi
C KLAPAN_MAN.fmg( 110, 233):RS �������,1
      L0_odi = (.NOT.L0_idi).AND.L_afi
C KLAPAN_MAN.fmg( 128, 236):�
      L0_ule = L0_odi.AND.(.NOT.L_uki)
C KLAPAN_MAN.fmg( 252, 242):�
      L0_ixe = L0_ule.OR.L_ali
C KLAPAN_MAN.fmg( 265, 241):���
      if(L0_ixe) then
         R0_ive=R0_ave
      else
         R0_ive=R0_uve
      endif
C KLAPAN_MAN.fmg( 287, 254):���� RE IN LO CH7
      L0_ebi = L0_oxe.OR.L_ubi.OR.L0_ifi
C KLAPAN_MAN.fmg( 102, 185):���
      L0_edi = L0_adi.AND.(.NOT.L_ubi)
C KLAPAN_MAN.fmg( 102, 191):�
      L_ibi=(L0_edi.or.L_ibi).and..not.(L0_ebi)
      L0_obi=.not.L_ibi
C KLAPAN_MAN.fmg( 110, 189):RS �������,2
      L0_abi = L_ibi.AND.(.NOT.L0_uxe)
C KLAPAN_MAN.fmg( 128, 190):�
      L0_ole = L0_abi.AND.(.NOT.L_ali)
C KLAPAN_MAN.fmg( 252, 228):�
      L0_exe = L0_ole.OR.L_uki
C KLAPAN_MAN.fmg( 265, 227):���
      if(L0_exe) then
         R0_eve=R0_ute
      else
         R0_eve=R0_uve
      endif
C KLAPAN_MAN.fmg( 287, 236):���� RE IN LO CH7
      R0_ove = R0_ive + (-R0_eve)
C KLAPAN_MAN.fmg( 293, 246):��������
      if(L_eki) then
         R0_ise=R0_ame
      else
         R0_ise=R0_ove
      endif
C KLAPAN_MAN.fmg( 300, 244):���� RE IN LO CH7
      R0_ore = R_ate + (-R_aki)
C KLAPAN_MAN.fmg( 308, 235):��������
      R0_are = R0_ise + R0_ore
C KLAPAN_MAN.fmg( 314, 236):��������
      R0_ire = R0_are * R0_ore
C KLAPAN_MAN.fmg( 318, 235):����������
      L0_ure=R0_ire.lt.R0_ere
C KLAPAN_MAN.fmg( 323, 234):���������� <
      L_ase=(L0_ure.or.L_ase).and..not.(.NOT.L_eli)
      L0_upe=.not.L_ase
C KLAPAN_MAN.fmg( 330, 232):RS �������,6
      if(L_ase) then
         R_ate=R_aki
      endif
C KLAPAN_MAN.fmg( 324, 252):���� � ������������� �������
      if(L_ase) then
         R0_use=R0_ese
      else
         R0_use=R0_ise
      endif
C KLAPAN_MAN.fmg( 332, 244):���� RE IN LO CH7
      R0_ete = R_ate + R0_use
C KLAPAN_MAN.fmg( 338, 245):��������
      R0_epe=MAX(R0_ape,R0_ete)
C KLAPAN_MAN.fmg( 346, 246):��������
      R0_ote=MIN(R0_ume,R0_epe)
C KLAPAN_MAN.fmg( 354, 247):�������
      L_ubi=R0_ote.lt.R0_ipe
C KLAPAN_MAN.fmg( 363, 210):���������� <
C sav1=L0_ebi
      L0_ebi = L0_oxe.OR.L_ubi.OR.L0_ifi
C KLAPAN_MAN.fmg( 102, 185):recalc:���
C if(sav1.ne.L0_ebi .and. try172.gt.0) goto 172
C sav1=L0_edi
      L0_edi = L0_adi.AND.(.NOT.L_ubi)
C KLAPAN_MAN.fmg( 102, 191):recalc:�
C if(sav1.ne.L0_edi .and. try174.gt.0) goto 174
C sav1=L0_uxe
      L0_uxe = (.NOT.L_ov).AND.L_ubi
C KLAPAN_MAN.fmg( 102, 166):recalc:�
C if(sav1.ne.L0_uxe .and. try133.gt.0) goto 133
      if(L0_ime) then
         R8_ose=R0_ome
      else
         R8_ose=R0_ote
      endif
C KLAPAN_MAN.fmg( 377, 246):���� RE IN LO CH7
      R_ok = R0_ik * R8_ose
C KLAPAN_MAN.fmg( 368, 266):����������
      R_ek = R0_ak + (-R_ok)
C KLAPAN_MAN.fmg( 382, 272):��������
      L_ofi=R0_ote.gt.R0_ope
C KLAPAN_MAN.fmg( 363, 225):���������� >
C sav1=L0_udi
      L0_udi = L_ofi.OR.L0_oxe.OR.L0_adi
C KLAPAN_MAN.fmg( 102, 229):recalc:���
C if(sav1.ne.L0_udi .and. try153.gt.0) goto 153
C sav1=L0_ufi
      L0_ufi = (.NOT.L_ofi).AND.L0_ifi
C KLAPAN_MAN.fmg( 102, 235):recalc:�
C if(sav1.ne.L0_ufi .and. try155.gt.0) goto 155
C sav1=L0_idi
      L0_idi = (.NOT.L_ov).AND.L_ofi
C KLAPAN_MAN.fmg( 104, 260):recalc:�
C if(sav1.ne.L0_idi .and. try148.gt.0) goto 148
      L0_u = (.NOT.L_ofi).AND.(.NOT.L_ubi)
C KLAPAN_MAN.fmg( 206, 218):�
      if(L_ofi) then
         C8_od=C8_ud
      else
         C8_od=C8_af
      endif
C KLAPAN_MAN.fmg( 169, 227):���� RE IN LO CH7
      if(L_uke) then
         C8_ed=C8_id
      else
         C8_ed=C8_od
      endif
C KLAPAN_MAN.fmg( 184, 226):���� RE IN LO CH7
      if(L0_u) then
         C8_ef=C8_ad
      else
         C8_ef=C8_ed
      endif
C KLAPAN_MAN.fmg( 209, 225):���� RE IN LO CH7
      if(L0_ime) then
         R_ate=R0_ote
      endif
C KLAPAN_MAN.fmg( 366, 252):���� � ������������� �������
C sav1=R0_ore
      R0_ore = R_ate + (-R_aki)
C KLAPAN_MAN.fmg( 308, 235):recalc:��������
C if(sav1.ne.R0_ore .and. try196.gt.0) goto 196
C sav1=R0_ete
      R0_ete = R_ate + R0_use
C KLAPAN_MAN.fmg( 338, 245):recalc:��������
C if(sav1.ne.R0_ete .and. try213.gt.0) goto 213
      L0_ebe = L0_odi.OR.L0_abi
C KLAPAN_MAN.fmg( 251, 174):���
      L0_ibe = L0_ebe.AND.(.NOT.L_ox)
C KLAPAN_MAN.fmg( 266, 173):�
      if(L0_ibe) then
         R0_ede=R_ube
      else
         R0_ede=R0_obe
      endif
C KLAPAN_MAN.fmg( 269, 180):���� RE IN LO CH7
      if(L0_idi) then
         I0_eke=I0_ufe
      else
         I0_eke=I0_ake
      endif
C KLAPAN_MAN.fmg( 155, 208):���� RE IN LO CH7
      if(L_uke) then
         I_up=I0_ar
      else
         I_up=I0_eke
      endif
C KLAPAN_MAN.fmg( 191, 206):���� RE IN LO CH7
      if(L0_idi) then
         I_ep=I0_ip
      else
         I_ep=I0_op
      endif
C KLAPAN_MAN.fmg( 124, 264):���� RE IN LO CH7
      if(L0_uxe) then
         I_om=I0_um
      else
         I_om=I0_ap
      endif
C KLAPAN_MAN.fmg( 117, 170):���� RE IN LO CH7
      if(L0_uxe) then
         I0_ile=I0_ike
      else
         I0_ile=I0_oke
      endif
C KLAPAN_MAN.fmg( 155, 262):���� RE IN LO CH7
      if(L_uke) then
         I_ale=I0_ele
      else
         I_ale=I0_ile
      endif
C KLAPAN_MAN.fmg( 170, 262):���� RE IN LO CH7
      R0_ade = R8_ide
C KLAPAN_MAN.fmg( 264, 190):��������
C label 293  try293=try293-1
      R8_ide = R0_ede + R0_ade
C KLAPAN_MAN.fmg( 275, 189):��������
C sav1=R0_ade
      R0_ade = R8_ide
C KLAPAN_MAN.fmg( 264, 190):recalc:��������
C if(sav1.ne.R0_ade .and. try293.gt.0) goto 293
      End
