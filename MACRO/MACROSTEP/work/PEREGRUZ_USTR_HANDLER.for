      Subroutine PEREGRUZ_USTR_HANDLER(ext_deltat,L_e,L_i
     &,L_o,L_u,R_ed,R_id,R_ud,R_af,L_ef,R_of,R_uf,L_ak,L_ul
     &,L_om,L_um,R_ap,R_ep,R_ip,R_op,R_up,L_er,L_or,R_as,R_es
     &,R_ot,R_ut,R_av,R_ev,L_iv,R_ov,R_uv,L_ax,L_ex,L_ix,L_ux
     &,L_abe,L_ebe,L_obe,L_ode,L_ife,L_ufe)
C |L_e           |1 1 O|UNCATCH         |��������� �������|F|
C |L_i           |1 1 I|YA31            |������� ��������� �������|F|
C |L_o           |1 1 O|CATCH           |������ �������|F|
C |L_u           |1 1 I|YA30            |������� ������ �������|F|
C |R_ed          |4 4 K|_cJ3237         |�������� ��������� ����������|`max_c`|
C |R_id          |4 4 K|_cJ3236         |�������� ��������� ����������|`min_c`|
C |R_ud          |4 4 S|_simpJ3201*     |[���]���������� ��������� ������������� |0.0|
C |R_af          |4 4 K|_timpJ3201      |[���]������������ �������� �������������|0.4|
C |L_ef          |1 1 S|_limpJ3201*     |[TF]���������� ��������� ������������� |F|
C |R_of          |4 4 S|_simpJ3198*     |[���]���������� ��������� ������������� |0.0|
C |R_uf          |4 4 K|_timpJ3198      |[���]������������ �������� �������������|0.4|
C |L_ak          |1 1 S|_limpJ3198*     |[TF]���������� ��������� ������������� |F|
C |L_ul          |1 1 I|stop            |������� �� ��������� ����||
C |L_om          |1 1 I|YA27            |������� ���� �� ����������|F|
C |L_um          |1 1 I|down            |������� �� ��������� ����||
C |R_ap          |4 4 K|_uintV_INT_PU   |����������� ������ ����������� ������|`max_c`|
C |R_ep          |4 4 K|_tintV_INT_PU   |[���]�������� T �����������|1|
C |R_ip          |4 4 K|_lintV_INT_PU   |����������� ������ ����������� �����|`min_c`|
C |R_op          |4 4 O|POS             |��������� ��||
C |R_up          |4 4 O|_ointV_INT_PU*  |�������� ������ ����������� |`state_c`|
C |L_er          |1 1 I|mlf03           |�������� �� ��������||
C |L_or          |1 1 I|mlf04           |�������� �� ��������||
C |R_as          |4 4 O|VZ01            |�������� ����������� ��||
C |R_es          |4 4 I|vel             |����� ����|20.0|
C |R_ot          |4 4 S|USER_DOWN_ST*   |��������� ������ "������� ���� �� ���������" |0.0|
C |R_ut          |4 4 I|USER_DOWN       |������� ������ ������ "������� ���� �� ���������" |0.0|
C |R_av          |4 4 S|USER_UP_ST*     |��������� ������ "������� ����� �� ���������" |0.0|
C |R_ev          |4 4 I|USER_UP         |������� ������ ������ "������� ����� �� ���������" |0.0|
C |L_iv          |1 1 O|USER_STOP_CMD*  |[TF]����� ������ ������� ���� �� ���������|F|
C |R_ov          |4 4 S|USER_STOP_ST*   |��������� ������ "������� ���� �� ���������" |0.0|
C |R_uv          |4 4 I|USER_STOP       |������� ������ ������ "������� ���� �� ���������" |0.0|
C |L_ax          |1 1 O|USER_UP_CMD*    |[TF]����� ������ ������� ����� �� ���������|F|
C |L_ex          |1 1 I|up              |������� �� ��������� �����||
C |L_ix          |1 1 O|USER_DOWN_CMD*  |[TF]����� ������ ������� ���� �� ���������|F|
C |L_ux          |1 1 O|stop_inner      |�������||
C |L_abe         |1 1 S|_qffJ3299*      |�������� ������ Q RS-��������  |F|
C |L_ebe         |1 1 I|YA25            |������� ���� �� ����������|F|
C |L_obe         |1 1 I|YA26            |������� ����� �� ����������|F|
C |L_ode         |1 1 S|_qff2*          |�������� ������ Q RS-��������  |F|
C |L_ife         |1 1 S|_qff1*          |�������� ������ Q RS-��������  |F|
C |L_ufe         |1 1 I|mlf24           |��� ���������� � ����������||

      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      LOGICAL*1 L_e,L_i,L_o,L_u,L0_ad
      REAL*4 R_ed,R_id,R0_od,R_ud,R_af
      LOGICAL*1 L_ef
      REAL*4 R0_if,R_of,R_uf
      LOGICAL*1 L_ak,L0_ek
      REAL*4 R0_ik,R0_ok,R0_uk
      LOGICAL*1 L0_al
      REAL*4 R0_el,R0_il,R0_ol
      LOGICAL*1 L_ul,L0_am,L0_em,L0_im,L_om,L_um
      REAL*4 R_ap,R_ep,R_ip,R_op,R_up
      LOGICAL*1 L0_ar,L_er,L0_ir,L_or
      REAL*4 R0_ur,R_as,R_es,R0_is,R0_os,R0_us,R0_at
      LOGICAL*1 L0_et,L0_it
      REAL*4 R_ot,R_ut,R_av,R_ev
      LOGICAL*1 L_iv
      REAL*4 R_ov,R_uv
      LOGICAL*1 L_ax,L_ex,L_ix,L0_ox,L_ux,L_abe,L_ebe,L0_ibe
     &,L_obe,L0_ube,L0_ade,L0_ede,L0_ide,L_ode,L0_ude,L0_afe
     &,L0_efe,L_ife,L0_ofe,L_ufe

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      R0_od=R_ud
C PEREGRUZ_USTR_HANDLER.fmg( 374, 215):pre: ������������  �� T
      R0_if=R_of
C PEREGRUZ_USTR_HANDLER.fmg( 374, 225):pre: ������������  �� T
      L_e=L_i
C PEREGRUZ_USTR_HANDLER.fmg( 192, 254):������,UNCATCH
      L_o=L_u
C PEREGRUZ_USTR_HANDLER.fmg( 192, 260):������,CATCH
      !��������� R0_ok = PEREGRUZ_USTR_HANDLERC?? /`max_c`
C /
      R0_ok=R_ed
C PEREGRUZ_USTR_HANDLER.fmg( 344, 215):���������
      !��������� R0_il = PEREGRUZ_USTR_HANDLERC?? /`min_c`
C /
      R0_il=R_id
C PEREGRUZ_USTR_HANDLER.fmg( 344, 225):���������
      R0_ik = 0.01
C PEREGRUZ_USTR_HANDLER.fmg( 344, 212):��������� (RE4) (�������)
      R0_uk = R0_ok + (-R0_ik)
C PEREGRUZ_USTR_HANDLER.fmg( 348, 214):��������
      R0_el = 0.01
C PEREGRUZ_USTR_HANDLER.fmg( 344, 222):��������� (RE4) (�������)
      R0_ol = R0_il + R0_el
C PEREGRUZ_USTR_HANDLER.fmg( 348, 224):��������
      R0_ur = 0.0
C PEREGRUZ_USTR_HANDLER.fmg( 314, 246):��������� (RE4) (�������)
      R0_at = 0.0
C PEREGRUZ_USTR_HANDLER.fmg( 298, 246):��������� (RE4) (�������)
      L_ix=R_ut.ne.R_ot
      R_ot=R_ut
C PEREGRUZ_USTR_HANDLER.fmg(  14, 190):���������� ������������� ������
      L0_ibe = L_ix.OR.L_um
C PEREGRUZ_USTR_HANDLER.fmg(  65, 188):���
      L0_ede = L0_ibe.OR.L_ebe
C PEREGRUZ_USTR_HANDLER.fmg(  70, 188):���
      L_ax=R_ev.ne.R_av
      R_av=R_ev
C PEREGRUZ_USTR_HANDLER.fmg(  16, 249):���������� ������������� ������
      L0_ube = L_ex.OR.L_ax
C PEREGRUZ_USTR_HANDLER.fmg(  66, 238):���
      L0_afe = L0_ube.OR.L_obe
C PEREGRUZ_USTR_HANDLER.fmg(  70, 234):���
      L0_ox = L0_afe.OR.L0_ede
C PEREGRUZ_USTR_HANDLER.fmg(  76, 203):���
      L_iv=R_uv.ne.R_ov
      R_ov=R_uv
C PEREGRUZ_USTR_HANDLER.fmg(  14, 214):���������� ������������� ������
      L0_ade = L_abe.OR.L_ufe
C PEREGRUZ_USTR_HANDLER.fmg(  98, 214):���
C label 38  try38=try38-1
      L0_efe = L_ufe.OR.L0_ade.OR.L0_ede
C PEREGRUZ_USTR_HANDLER.fmg( 104, 230):���
      L_ife=(L0_afe.or.L_ife).and..not.(L0_efe)
      L0_ofe=.not.L_ife
C PEREGRUZ_USTR_HANDLER.fmg( 111, 232):RS �������,1
      L0_ir = L_ife.AND.(.NOT.L_or)
C PEREGRUZ_USTR_HANDLER.fmg( 268, 245):�
      L0_it = L0_ir.OR.L_er
C PEREGRUZ_USTR_HANDLER.fmg( 281, 244):���
      if(L0_it) then
         R0_os=R_es
      else
         R0_os=R0_at
      endif
C PEREGRUZ_USTR_HANDLER.fmg( 303, 256):���� RE IN LO CH7
      L0_ide = L0_ade.OR.L0_afe
C PEREGRUZ_USTR_HANDLER.fmg( 104, 180):���
      L_ode=(L0_ede.or.L_ode).and..not.(L0_ide)
      L0_ude=.not.L_ode
C PEREGRUZ_USTR_HANDLER.fmg( 111, 186):RS �������,2
      L0_ar = L_ode.AND.(.NOT.L_er)
C PEREGRUZ_USTR_HANDLER.fmg( 268, 231):�
      L0_et = L0_ar.OR.L_or
C PEREGRUZ_USTR_HANDLER.fmg( 281, 230):���
      if(L0_et) then
         R0_is=R_es
      else
         R0_is=R0_at
      endif
C PEREGRUZ_USTR_HANDLER.fmg( 303, 238):���� RE IN LO CH7
      R0_us = R0_os + (-R0_is)
C PEREGRUZ_USTR_HANDLER.fmg( 309, 249):��������
      if(L_ufe) then
         R_as=R0_ur
      else
         R_as=R0_us
      endif
C PEREGRUZ_USTR_HANDLER.fmg( 318, 248):���� RE IN LO CH7
      R_up=R_up+deltat/R_ep*R_as
      if(R_up.gt.R_ap) then
         R_up=R_ap
      elseif(R_up.lt.R_ip) then
         R_up=R_ip
      endif
C PEREGRUZ_USTR_HANDLER.fmg( 332, 232):����������,V_INT_PU
      L0_al=R_up.lt.R0_ol
C PEREGRUZ_USTR_HANDLER.fmg( 356, 225):���������� <
      if(L0_al.and..not.L_ak) then
         R_of=R_uf
      else
         R_of=max(R0_if-deltat,0.0)
      endif
      L0_em=R_of.gt.0.0
      L_ak=L0_al
C PEREGRUZ_USTR_HANDLER.fmg( 374, 225):������������  �� T
      L0_ek=R_up.gt.R0_uk
C PEREGRUZ_USTR_HANDLER.fmg( 356, 215):���������� >
      if(L0_ek.and..not.L_ef) then
         R_ud=R_af
      else
         R_ud=max(R0_od-deltat,0.0)
      endif
      L0_am=R_ud.gt.0.0
      L_ef=L0_ek
C PEREGRUZ_USTR_HANDLER.fmg( 374, 215):������������  �� T
      L0_im = L_iv.OR.L_om.OR.L0_em.OR.L0_am.OR.L_ul
C PEREGRUZ_USTR_HANDLER.fmg(  59, 207):���
      L_abe=L0_im.or.(L_abe.and..not.(L0_ox))
      L0_ad=.not.L_abe
C PEREGRUZ_USTR_HANDLER.fmg( 106, 205):RS �������
C sav1=L0_ade
      L0_ade = L_abe.OR.L_ufe
C PEREGRUZ_USTR_HANDLER.fmg(  98, 214):recalc:���
C if(sav1.ne.L0_ade .and. try38.gt.0) goto 38
      L_ux=L_abe
C PEREGRUZ_USTR_HANDLER.fmg( 133, 207):������,stop_inner
      R_op=R_up
C PEREGRUZ_USTR_HANDLER.fmg( 390, 234):������,POS
      End
