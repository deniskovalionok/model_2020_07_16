      Interface
      Subroutine REG_RAS_MAN(ext_deltat,L_e,L_u,R_of,R_ik
     &,R_al,R_el,R_il,R_ol,R_um,R_ip,R_op,R_up,R_ar,R_er,R_ir
     &,L_ur,R_as,R_es,L_os,L_at,L_et,I_iv,R_uv,R_ax,R_ex,R8_ix
     &,R_ox,R_ebe,R_ibe,R_obe,R_ube,R_ade)
C |L_e           |1 1 I|feedback        |||
C |L_u           |1 1 I|flagmod         |����� ������� �����||
C |R_of          |4 4 I|k4              |�-� ����������� ��|0|
C |R_ik          |4 4 I|t3              |���������� ������� ���������������|1.0|
C |R_al          |4 4 S|_sdif1*         |���������� ��������� |0.0|
C |R_el          |4 4 I|u1              |����������� �����������|1.0|
C |R_il          |4 4 O|dlt             |������� ��������� ����������||
C |R_ol          |4 4 O|_odif1*         |�������� ������ |0.0|
C |R_um          |4 4 S|_oint1*         |����� ����������� |0.0|
C |R_ip          |4 4 S|turn_on_ST*     |��������� ������ "--" |0.0|
C |R_op          |4 4 I|turn_on         |������� ������ ������ "--" |0.0|
C |R_up          |4 4 S|vmopen_ST*      |��������� ������ "--" |0.0|
C |R_ar          |4 4 I|vmopen          |������� ������ ������ "--" |0.0|
C |R_er          |4 4 S|vmclose_ST*     |��������� ������ "--" |0.0|
C |R_ir          |4 4 I|vmclose         |������� ������ ������ "--" |0.0|
C |L_ur          |1 1 O|turn_on_CMD*    |[TF]����� ������ --|F|
C |R_as          |4 4 K|_cJ898          |�������� ��������� ����������|1.0|
C |R_es          |4 4 K|_cJ897          |�������� ��������� ����������|0.0|
C |L_os          |1 1 S|_qffJ901*       |�������� ������ Q RS-��������  |F|
C |L_at          |1 1 O|vmopen_CMD*     |[TF]����� ������ --|F|
C |L_et          |1 1 O|vmclose_CMD*    |[TF]����� ������ --|F|
C |I_iv          |2 4 O|LS              |||
C |R_uv          |4 4 K|_cJ733          |�������� ��������� ����������|10.0|
C |R_ax          |4 4 O|out             |����� ��������� ��������||
C |R_ex          |4 4 O|_oaprJ896*      |�������� ������ ���������. ����� |0.0|
C |R8_ix         |4 8 O|pos             |||
C |R_ox          |4 4 I|norma           |||
C |R_ebe         |4 4 I|CP              |||
C |R_ibe         |4 4 I|setpoint        |||
C |R_obe         |4 4 I|Kd              |||
C |R_ube         |4 4 I|Ki              ||0.1|
C |R_ade         |4 4 I|Kp              ||1.0|

      IMPLICIT NONE
      REAL*4 ext_deltat
      LOGICAL*1 L_e,L_u
      REAL*4 R_of,R_ik,R_al,R_el,R_il,R_ol,R_um,R_ip,R_op
     &,R_up,R_ar,R_er,R_ir
      LOGICAL*1 L_ur
      REAL*4 R_as,R_es
      LOGICAL*1 L_os,L_at,L_et
      INTEGER*4 I_iv
      REAL*4 R_uv,R_ax,R_ex
      REAL*8 R8_ix
      REAL*4 R_ox,R_ebe,R_ibe,R_obe,R_ube,R_ade
      End subroutine REG_RAS_MAN
      End interface
