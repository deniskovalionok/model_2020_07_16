      Subroutine LODOCHKA_HANDLER(ext_deltat,L_e,R_ad,L_af
     &,L_uk,L_al,L_ol,L_em,L_op,R_ir,R_or,L_ur,L_ot,L_av,L_ev
     &,R_uv,R_ax,L_ex,L_ox,R_ede,R_ide,L_ode,I_ofe,I_ike,I_ele
     &,I_ame,I_ume,I_ape,L_upe,L_ite,L_ave,L_ove,L_exe,L_uxe
     &,L_ibi,L_adi,L_efi,L_aki,L_uki,I_ili,I_oli,R_omi,R_opi
     &,R_upi,R_uri,R_isi,R_eti,R_ovi,R_ixi,R_uxi,R_abo,R_ubo
     &,R_edo,R_ido,L_odo,L_oko,L_uko,R_elo,R_amo,R_omo,R_ipo
     &,R_ero,R_aso,R_eso,R_uto,R_avo,R_axo,R_uxo,R_ebu,R_ofu
     &,R_ufu,L_aku,R_uku,R_ulu,R_umu,L_eru,R_iru,R_esu,R_atu
     &,R_etu,R_uvu,R_axu,L_exu,R_abad,R_adad,R_afad,R_ekad
     &,R_alad,R_ulad,R_amad,L_ipad,L_upad,L_erad,R_irad,L_orad
     &,R_urad,R_itad,R_evad,R_axad,R_exad,L_obed,L_eded,L_oded
     &,R_uded,L_afed,R_efed,R_aked,R_uled,R_umed,R_aped,R_eped
     &,L_ored,L_esed,L_ised,L_used,L_oted,L_aved,R_exed,R_ixed
     &,R_ibid,R_obid,L_adid,R_afid,I_etid,R_avid,R_evid,R_ivid
     &,R_uvid,R_ixid,R_abod,R_ibod,C20_obod)
C |L_e           |1 1 I|LODOCHKA_FREE_STATE|��������� ������� � ��������� "������ �������"||
C |R_ad          |4 4 I|FDA40_BOAT_MASS |����� ����� � �����������||
C |L_af          |1 1 I|FDA60_LOD_INPUT_GATEWAY_FROM|������� �� �������� �����||
C |L_uk          |1 1 I|FDA60_LOD_TRANSFER_TABLE|������� �� ������������ �������||
C |L_al          |1 1 I|FDA60_LOD_MAIN_TROLLEY|������� �� �������||
C |L_ol          |1 1 I|FDA60_LOD_INPUT_GATEWAY|������� �� �������� �����||
C |L_em          |1 1 I|FDA60_LOD_VZV   |������� �� ������������||
C |L_op          |1 1 I|FDA60_LOD_LIFT  |������� ����������� �� ����� ||
C |R_ir          |4 4 S|_simpJ10082*    |[���]���������� ��������� ������������� |0.0|
C |R_or          |4 4 K|_timpJ10082     |[���]������������ �������� �������������|1.0|
C |L_ur          |1 1 S|_limpJ10082*    |[TF]���������� ��������� ������������� |F|
C |L_ot          |1 1 I|FDA40_START     |������ ������� �����������||
C |L_av          |1 1 I|FDA91EC001_INIT_COORD|������������� ���������� ������� ��� ��. �����������||
C |L_ev          |1 1 I|FDA91EC003_INIT_COORD|������������� ���������� ������� ��� ��. ���||
C |R_uv          |4 4 S|_simpJ10030*    |[���]���������� ��������� ������������� |0.0|
C |R_ax          |4 4 K|_timpJ10030     |[���]������������ �������� �������������|1.0|
C |L_ex          |1 1 S|_limpJ10030*    |[TF]���������� ��������� ������������� |F|
C |L_ox          |1 1 I|FDA91EC005_INIT_COORD|������������� ���������� ������� ��� ��. ���������||
C |R_ede         |4 4 S|_simpJ10001*    |[���]���������� ��������� ������������� |0.0|
C |R_ide         |4 4 K|_timpJ10001     |[���]������������ �������� �������������|1.0|
C |L_ode         |1 1 S|_limpJ10001*    |[TF]���������� ��������� ������������� |F|
C |I_ofe         |2 4 K|_lcmpJ9990      |�������� ������ �����������|5|
C |I_ike         |2 4 K|_lcmpJ9978      |�������� ������ �����������|4|
C |I_ele         |2 4 K|_lcmpJ9966      |�������� ������ �����������|3|
C |I_ame         |2 4 K|_lcmpJ9954      |�������� ������ �����������|2|
C |I_ume         |2 4 K|_lcmpJ9942      |�������� ������ �����������|1|
C |I_ape         |2 4 I|FDA60AE408_NUM_BOAT|����� ��������� ������� �� �������||
C |L_upe         |1 1 I|FDA60AE408_CATCH|������ ������� 5��� �������� 20FDA60AE408||
C |L_ite         |1 1 I|FDA61AE401_UNLOAD5|������ ������� ����� 20FDA61AE401||
C |L_ave         |1 1 I|FDA61AE401_UNLOAD4|������ ������� ����� 20FDA61AE401||
C |L_ove         |1 1 I|FDA61AE401_UNLOAD3|������ ������� ����� 20FDA61AE401||
C |L_exe         |1 1 I|FDA61AE401_UNLOAD2|������ ������� ����� 20FDA61AE401||
C |L_uxe         |1 1 I|FDA61AE401_UNLOAD1|������ ������� ����� 20FDA61AE401||
C |L_ibi         |1 1 I|FDA61AE401_CATCH5|������ ������� ����� 20FDA61AE401||
C |L_adi         |1 1 I|FDA61AE401_CATCH4|������ ������� ����� 20FDA61AE401||
C |L_efi         |1 1 I|FDA61AE401_CATCH3|������ ������� ����� 20FDA61AE401||
C |L_aki         |1 1 I|FDA61AE401_CATCH2|������ ������� ����� 20FDA61AE401||
C |L_uki         |1 1 I|FDA61AE401_CATCH1|������ ������� ����� 20FDA61AE401||
C |I_ili         |2 4 O|CR              |�������������� � �������������||
C |I_oli         |2 4 O|LP              |����� �� ������� ����������� �������||
C |R_omi         |4 4 K|_lcmpJ9186      |[]�������� ������ �����������|20|
C |R_opi         |4 4 I|FDA60AE500AVY01 |�������� ��������� 20FDA60AE500A||
C |R_upi         |4 4 K|_lcmpJ9173      |[]�������� ������ �����������|20|
C |R_uri         |4 4 I|FDA60AE500BVY01 |�������� ��������� 20FDA60AE500B||
C |R_isi         |4 4 I|FDA60AE500�VY01 |�������� ��������� 20FDA60AE500�||
C |R_eti         |4 4 K|_lcmpJ9156      |[]�������� ������ �����������|20|
C |R_ovi         |4 4 K|_lcmpJ9127      |[]�������� ������ �����������|20|
C |R_ixi         |4 4 K|_lcmpJ9057      |[]�������� ������ �����������|2|
C |R_uxi         |4 4 I|FDA60AE403_VZ01 |�������� �������������� �������||
C |R_abo         |4 4 K|_lcmpJ9051      |[]�������� ������ �����������|20|
C |R_ubo         |4 4 I|FDA60AE403_POS  |��������� ������� �� ��� X||
C |R_edo         |4 4 S|_simpJ9036*     |[���]���������� ��������� ������������� |0.0|
C |R_ido         |4 4 K|_timpJ9036      |[���]������������ �������� �������������|1.0|
C |L_odo         |1 1 S|_limpJ9036*     |[TF]���������� ��������� ������������� |F|
C |L_oko         |1 1 S|_splsJ8856*     |[TF]���������� ��������� ������������� |F|
C |L_uko         |1 1 I|FDA40_HAVE_DELTA_MASS|���� ��������� ����� �������||
C |R_elo         |4 4 I|FDA40_DELTA_MASS|��������� ����� �������||
C |R_amo         |4 4 O|MASS            |����� ����� ����������� � �������||
C |R_omo         |4 4 K|_lcmpJ8516      |[]�������� ������ �����������|20|
C |R_ipo         |4 4 K|_lcmpJ8503      |[]�������� ������ �����������|20|
C |R_ero         |4 4 K|_lcmpJ8495      |[]�������� ������ �����������|20|
C |R_aso         |4 4 I|FDA91AE001KE01_POS|��������� �1||
C |R_eso         |4 4 K|_lcmpJ8485      |[]�������� ������ �����������|20|
C |R_uto         |4 4 I|FDA91AE001KE01_V01|������� �1 �� ��� X||
C |R_avo         |4 4 I|FDA91AE003KE01_V01|�������� 20FDA91AE003KE01||
C |R_axo         |4 4 K|_lcmpJ8465      |[]�������� ������ �����������|20|
C |R_uxo         |4 4 I|FDA91AE003KE01_POS|��������� �� 20FDA91AE003KE01||
C |R_ebu         |4 4 K|_lcmpJ8453      |[]�������� ������ �����������|20|
C |R_ofu         |4 4 S|_simpJ8368*     |[���]���������� ��������� ������������� |0.0|
C |R_ufu         |4 4 K|_timpJ8368      |[���]������������ �������� �������������|1.0|
C |L_aku         |1 1 S|_limpJ8368*     |[TF]���������� ��������� ������������� |F|
C |R_uku         |4 4 K|_lcmpJ8358      |[]�������� ������ �����������|20|
C |R_ulu         |4 4 K|_lcmpJ8350      |[]�������� ������ �����������|20|
C |R_umu         |4 4 K|_lcmpJ8342      |[]�������� ������ �����������|20|
C |L_eru         |1 1 O|FDA91AE012_CATCHED|������� ��������� �� 20FDA91AE012||
C |R_iru         |4 4 K|_lcmpJ8317      |[]�������� ������ �����������|20|
C |R_esu         |4 4 K|_lcmpJ8308      |[]�������� ������ �����������|20|
C |R_atu         |4 4 I|FDA91AE012_POS  |��������� �4||
C |R_etu         |4 4 K|_lcmpJ8297      |[]�������� ������ �����������|20|
C |R_uvu         |4 4 S|_simpJ7966*     |[���]���������� ��������� ������������� |0.0|
C |R_axu         |4 4 K|_timpJ7966      |[���]������������ �������� �������������|1.0|
C |L_exu         |1 1 S|_limpJ7966*     |[TF]���������� ��������� ������������� |F|
C |R_abad        |4 4 K|_lcmpJ7929      |[]�������� ������ �����������|20|
C |R_adad        |4 4 K|_lcmpJ7921      |[]�������� ������ �����������|20|
C |R_afad        |4 4 K|_lcmpJ7913      |[]�������� ������ �����������|20|
C |R_ekad        |4 4 K|_lcmpJ7509      |[]�������� ������ �����������|20|
C |R_alad        |4 4 K|_lcmpJ7500      |[]�������� ������ �����������|20|
C |R_ulad        |4 4 I|FDA91AE007_POS  |��������� �4||
C |R_amad        |4 4 K|_lcmpJ7487      |[]�������� ������ �����������|20|
C |L_ipad        |1 1 O|FDA91AE007_CATCHED|������� ��������� �4||
C |L_upad        |1 1 S|_qffJ7476*      |�������� ������ Q RS-��������  |F|
C |L_erad        |1 1 I|FDA91AE007_UNCATCH|��������� ������� �4||
C |R_irad        |4 4 I|FDA91AE007_VZ01 |������� �4 �� ��� Z||
C |L_orad        |1 1 I|FDA91AE007_CATCH|������ ������� �4||
C |R_urad        |4 4 K|_lcmpJ7170      |[]�������� ������ �����������|20|
C |R_itad        |4 4 K|_lcmpJ6848      |[]�������� ������ �����������|20|
C |R_evad        |4 4 K|_lcmpJ6839      |[]�������� ������ �����������|20|
C |R_axad        |4 4 I|FDA91AE006_POS  |��������� �� 20FDA91AE006||
C |R_exad        |4 4 K|_lcmpJ6826      |[]�������� ������ �����������|20|
C |L_obed        |1 1 O|FDA91AE006_CATCHED|������� ��������� �� 20FDA91AE006||
C |L_eded        |1 1 S|_qffJ6815*      |�������� ������ Q RS-��������  |F|
C |L_oded        |1 1 I|FDA91AE006_UNCATCH|��������� ������� �� 20FDA91AE006||
C |R_uded        |4 4 I|FDA91AE006_VZ01 |������� �� �� ��� Z||
C |L_afed        |1 1 I|FDA91AE006_CATCH|������ ������� �� 20FDA91AE006||
C |R_efed        |4 4 K|_lcmpJ6805      |[]�������� ������ �����������|20|
C |R_aked        |4 4 K|_lcmpJ6796      |[]�������� ������ �����������|20|
C |R_uled        |4 4 K|_lcmpJ6476      |[]�������� ������ �����������|20|
C |R_umed        |4 4 K|_lcmpJ6463      |[]�������� ������ �����������|1000|
C |R_aped        |4 4 I|FDA91AE014_POS  |��������� �� 20FDA91AE014||
C |R_eped        |4 4 K|_lcmpJ6181      |[]�������� ������ �����������|20|
C |L_ored        |1 1 O|FDA91AE014_CATCHED|������� ��������� �� 20FDA91AE014||
C |L_esed        |1 1 I|FDA91AE012_UNCATCH|��������� ������� �� 20FDA91AE012||
C |L_ised        |1 1 I|FDA91AE012_CATCH|������ ������� �� 20FDA91AE012||
C |L_used        |1 1 S|_qffJ6163*      |�������� ������ Q RS-��������  |F|
C |L_oted        |1 1 S|_qffJ6151*      |�������� ������ Q RS-��������  |F|
C |L_aved        |1 1 I|FDA91AE014_UNCATCH|��������� ������� �� 20FDA91AE014||
C |R_exed        |4 4 O|VZ01            |��������� ������� �� ��� Z||
C |R_ixed        |4 4 S|_ointJ6126*     |����� ����������� |0.0|
C |R_ibid        |4 4 I|FDA91AE012_VZ01 |������� �� �� ��� Z||
C |R_obid        |4 4 I|FDA91AE014_VZ01 |������� �� �� ��� Z||
C |L_adid        |1 1 I|FDA91AE014_CATCH|������ ������� �� 20FDA91AE014||
C |R_afid        |4 4 S|_ointJ5958*     |����� ����������� |`p_LOAD_Y`|
C |I_etid        |2 4 O|LINE_POS        |����� �� ������� ����������� �������||
C |R_avid        |4 4 O|VX01            |��������� ������� �� ��� X||
C |R_evid        |4 4 S|_ointJ5909*     |����� ����������� |`p_LOAD_X`|
C |R_ivid        |4 4 K|_lcmpJ2655      |[]�������� ������ �����������|4456|
C |R_uvid        |4 4 I|FDA60AE413VX02  |�������� �������� �������||
C |R_ixid        |4 4 I|FDA60AE408VX02  |�������� �������� �������||
C |R_abod        |4 4 K|_lcmpJ871       |[]�������� ������ �����������|1.0|
C |R_ibod        |4 4 O|VY01            |��������� ������� �� ��� Y||
C |C20_obod      |3 20 O|SECTION         |������� ������� �������||

      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      LOGICAL*1 L_e
      REAL*4 R0_i
      INTEGER*4 I0_o
      LOGICAL*1 L0_u
      REAL*4 R_ad
      LOGICAL*1 L0_ed
      INTEGER*4 I0_id,I0_od
      LOGICAL*1 L0_ud,L_af,L0_ef
      INTEGER*4 I0_if
      LOGICAL*1 L0_of
      INTEGER*4 I0_uf
      LOGICAL*1 L0_ak
      INTEGER*4 I0_ek
      LOGICAL*1 L0_ik
      INTEGER*4 I0_ok
      LOGICAL*1 L_uk,L_al
      INTEGER*4 I0_el
      LOGICAL*1 L0_il,L_ol
      INTEGER*4 I0_ul
      LOGICAL*1 L0_am,L_em
      INTEGER*4 I0_im
      LOGICAL*1 L0_om,L0_um
      INTEGER*4 I0_ap,I0_ep
      LOGICAL*1 L0_ip,L_op,L0_up
      INTEGER*4 I0_ar
      REAL*4 R0_er,R_ir,R_or
      LOGICAL*1 L_ur
      INTEGER*4 I0_as
      REAL*4 R0_es,R0_is
      LOGICAL*1 L0_os
      REAL*4 R0_us
      LOGICAL*1 L0_at,L0_et
      INTEGER*4 I0_it
      LOGICAL*1 L_ot
      INTEGER*4 I0_ut
      LOGICAL*1 L_av,L_ev,L0_iv
      REAL*4 R0_ov,R_uv,R_ax
      LOGICAL*1 L_ex,L0_ix,L_ox,L0_ux
      INTEGER*4 I0_abe
      REAL*4 R0_ebe,R0_ibe
      LOGICAL*1 L0_obe
      REAL*4 R0_ube,R0_ade,R_ede,R_ide
      LOGICAL*1 L_ode,L0_ude
      INTEGER*4 I0_afe
      LOGICAL*1 L0_efe,L0_ife
      INTEGER*4 I_ofe,I0_ufe
      LOGICAL*1 L0_ake,L0_eke
      INTEGER*4 I_ike,I0_oke
      LOGICAL*1 L0_uke,L0_ale
      INTEGER*4 I_ele,I0_ile
      LOGICAL*1 L0_ole,L0_ule
      INTEGER*4 I_ame,I0_eme
      LOGICAL*1 L0_ime,L0_ome
      INTEGER*4 I_ume,I_ape
      LOGICAL*1 L0_epe,L0_ipe
      INTEGER*4 I0_ope
      LOGICAL*1 L_upe
      INTEGER*4 I0_are
      LOGICAL*1 L0_ere
      INTEGER*4 I0_ire
      LOGICAL*1 L0_ore
      INTEGER*4 I0_ure
      LOGICAL*1 L0_ase
      INTEGER*4 I0_ese
      LOGICAL*1 L0_ise,L0_ose
      INTEGER*4 I0_use
      LOGICAL*1 L0_ate
      INTEGER*4 I0_ete
      LOGICAL*1 L_ite,L0_ote
      INTEGER*4 I0_ute
      LOGICAL*1 L_ave,L0_eve
      INTEGER*4 I0_ive
      LOGICAL*1 L_ove,L0_uve
      INTEGER*4 I0_axe
      LOGICAL*1 L_exe,L0_ixe
      INTEGER*4 I0_oxe
      LOGICAL*1 L_uxe,L0_abi
      INTEGER*4 I0_ebi
      LOGICAL*1 L_ibi,L0_obi
      INTEGER*4 I0_ubi
      LOGICAL*1 L_adi,L0_edi,L0_idi,L0_odi,L0_udi
      INTEGER*4 I0_afi
      LOGICAL*1 L_efi,L0_ifi,L0_ofi
      INTEGER*4 I0_ufi
      LOGICAL*1 L_aki,L0_eki,L0_iki
      INTEGER*4 I0_oki
      LOGICAL*1 L_uki,L0_ali
      INTEGER*4 I0_eli,I_ili,I_oli
      LOGICAL*1 L0_uli
      INTEGER*4 I0_ami
      LOGICAL*1 L0_emi
      INTEGER*4 I0_imi
      REAL*4 R_omi,R0_umi,R0_api,R0_epi
      LOGICAL*1 L0_ipi
      REAL*4 R_opi,R_upi,R0_ari,R0_eri,R0_iri
      LOGICAL*1 L0_ori
      REAL*4 R_uri,R0_asi,R0_esi,R_isi,R0_osi,R0_usi
      LOGICAL*1 L0_ati
      REAL*4 R_eti,R0_iti,R0_oti,R0_uti,R0_avi,R0_evi
      LOGICAL*1 L0_ivi
      REAL*4 R_ovi,R0_uvi,R0_axi,R0_exi,R_ixi,R0_oxi,R_uxi
     &,R_abo,R0_ebo,R0_ibo,R0_obo,R_ubo,R0_ado,R_edo,R_ido
      LOGICAL*1 L_odo
      INTEGER*4 I0_udo
      REAL*4 R0_afo
      LOGICAL*1 L0_efo
      REAL*4 R0_ifo
      LOGICAL*1 L0_ofo,L0_ufo,L0_ako,L0_eko
      INTEGER*4 I0_iko
      LOGICAL*1 L_oko,L_uko,L0_alo
      REAL*4 R_elo,R0_ilo
      LOGICAL*1 L0_olo
      REAL*4 R0_ulo,R_amo
      LOGICAL*1 L0_emo
      INTEGER*4 I0_imo
      REAL*4 R_omo,R0_umo,R0_apo,R0_epo,R_ipo,R0_opo,R0_upo
     &,R0_aro,R_ero,R0_iro,R0_oro,R0_uro,R_aso,R_eso,R0_iso
     &,R0_oso
      LOGICAL*1 L0_uso,L0_ato,L0_eto
      REAL*4 R0_ito
      LOGICAL*1 L0_oto
      REAL*4 R_uto,R_avo,R0_evo
      LOGICAL*1 L0_ivo,L0_ovo,L0_uvo
      REAL*4 R_axo,R0_exo,R0_ixo,R0_oxo,R_uxo
      LOGICAL*1 L0_abu
      REAL*4 R_ebu,R0_ibu,R0_obu,R0_ubu
      LOGICAL*1 L0_adu
      INTEGER*4 I0_edu
      REAL*4 R0_idu,R0_odu
      LOGICAL*1 L0_udu
      INTEGER*4 I0_afu,I0_efu
      REAL*4 R0_ifu,R_ofu,R_ufu
      LOGICAL*1 L_aku,L0_eku,L0_iku,L0_oku
      REAL*4 R_uku,R0_alu,R0_elu,R0_ilu
      LOGICAL*1 L0_olu
      REAL*4 R_ulu,R0_amu,R0_emu,R0_imu
      LOGICAL*1 L0_omu
      REAL*4 R_umu,R0_apu,R0_epu,R0_ipu
      LOGICAL*1 L0_opu
      INTEGER*4 I0_upu
      LOGICAL*1 L0_aru,L_eru
      REAL*4 R_iru,R0_oru,R0_uru,R0_asu,R_esu,R0_isu,R0_osu
     &,R0_usu,R_atu,R_etu,R0_itu,R0_otu
      LOGICAL*1 L0_utu,L0_avu,L0_evu
      INTEGER*4 I0_ivu
      REAL*4 R0_ovu,R_uvu,R_axu
      LOGICAL*1 L_exu,L0_ixu,L0_oxu,L0_uxu
      REAL*4 R_abad,R0_ebad,R0_ibad,R0_obad
      LOGICAL*1 L0_ubad
      REAL*4 R_adad,R0_edad,R0_idad,R0_odad
      LOGICAL*1 L0_udad
      REAL*4 R_afad,R0_efad,R0_ifad,R0_ofad
      LOGICAL*1 L0_ufad
      INTEGER*4 I0_akad
      REAL*4 R_ekad,R0_ikad,R0_okad,R0_ukad,R_alad,R0_elad
     &,R0_ilad,R0_olad,R_ulad,R_amad,R0_emad,R0_imad
      LOGICAL*1 L0_omad,L0_umad,L0_apad,L0_epad,L_ipad,L0_opad
     &,L_upad,L0_arad,L_erad
      REAL*4 R_irad
      LOGICAL*1 L_orad
      REAL*4 R_urad,R0_asad,R0_esad,R0_isad,R0_osad
      LOGICAL*1 L0_usad
      REAL*4 R0_atad,R0_etad,R_itad,R0_otad,R0_utad,R0_avad
     &,R_evad,R0_ivad,R0_ovad,R0_uvad,R_axad,R_exad,R0_ixad
     &,R0_oxad
      LOGICAL*1 L0_uxad,L0_abed,L0_ebed,L0_ibed,L_obed
      REAL*4 R0_ubed
      LOGICAL*1 L0_aded,L_eded,L0_ided,L_oded
      REAL*4 R_uded
      LOGICAL*1 L_afed
      REAL*4 R_efed,R0_ifed,R0_ofed,R0_ufed,R_aked,R0_eked
     &,R0_iked,R0_oked,R0_uked,R0_aled,R0_eled
      LOGICAL*1 L0_iled,L0_oled
      REAL*4 R_uled,R0_amed,R0_emed,R0_imed
      LOGICAL*1 L0_omed
      REAL*4 R_umed,R_aped,R_eped,R0_iped,R0_oped
      LOGICAL*1 L0_uped,L0_ared,L0_ered,L0_ired,L_ored,L0_ured
     &,L0_ased,L_esed,L_ised
      REAL*4 R0_osed
      LOGICAL*1 L_used
      REAL*4 R0_ated,R0_eted
      LOGICAL*1 L0_ited,L_oted,L0_uted,L_aved,L0_eved
      REAL*4 R0_ived,R0_oved,R0_uved,R0_axed,R_exed,R_ixed
      INTEGER*4 I0_oxed
      LOGICAL*1 L0_uxed
      REAL*4 R0_abid,R0_ebid,R_ibid,R_obid,R0_ubid
      LOGICAL*1 L_adid
      REAL*4 R0_edid
      LOGICAL*1 L0_idid
      REAL*4 R0_odid,R0_udid,R_afid,R0_efid
      LOGICAL*1 L0_ifid,L0_ofid
      REAL*4 R0_ufid,R0_akid
      LOGICAL*1 L0_ekid
      REAL*4 R0_ikid,R0_okid
      LOGICAL*1 L0_ukid
      REAL*4 R0_alid,R0_elid,R0_ilid
      INTEGER*4 I0_olid,I0_ulid
      LOGICAL*1 L0_amid
      REAL*4 R0_emid,R0_imid
      LOGICAL*1 L0_omid
      REAL*4 R0_umid,R0_apid,R0_epid
      INTEGER*4 I0_ipid,I0_opid
      LOGICAL*1 L0_upid
      INTEGER*4 I0_arid
      REAL*4 R0_erid,R0_irid
      LOGICAL*1 L0_orid
      REAL*4 R0_urid,R0_asid
      INTEGER*4 I0_esid
      LOGICAL*1 L0_isid
      REAL*4 R0_osid,R0_usid
      INTEGER*4 I0_atid,I_etid
      REAL*4 R0_itid,R0_otid,R0_utid,R_avid,R_evid,R_ivid
      LOGICAL*1 L0_ovid
      REAL*4 R_uvid,R0_axid,R0_exid,R_ixid,R0_oxid
      LOGICAL*1 L0_uxid
      REAL*4 R_abod,R0_ebod,R_ibod
      CHARACTER*20 C20_obod

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      R0_ado=R_edo
C LODOCHKA_HANDLER.fmg( 209,1329):pre: ������������  �� T
      R0_ifu=R_ofu
C LODOCHKA_HANDLER.fmg( 213,1378):pre: ������������  �� T
      R0_ade=R_ede
C LODOCHKA_HANDLER.fmg( 690,1931):pre: ������������  �� T
      R0_ov=R_uv
C LODOCHKA_HANDLER.fmg( 690,1940):pre: ������������  �� T
      R0_er=R_ir
C LODOCHKA_HANDLER.fmg( 690,1914):pre: ������������  �� T
      R0_ovu=R_uvu
C LODOCHKA_HANDLER.fmg( 213,1424):pre: ������������  �� T
      R0_i = 0.0
C LODOCHKA_HANDLER.fmg( 505,1969):��������� (RE4) (�������)
      if(L_e) then
         R_amo=R0_i
      endif
C LODOCHKA_HANDLER.fmg( 510,1968):���� � ������������� �������
      I0_o = 4001
C LODOCHKA_HANDLER.fmg( 336,1964):��������� ������������� IN (�������)
      I0_id = 0010
C LODOCHKA_HANDLER.fmg( 654,1792):��������� ������������� IN (�������)
      I0_od = 00011
C LODOCHKA_HANDLER.fmg( 696,1796):��������� ������������� IN (�������)
      I0_if = 0005
C LODOCHKA_HANDLER.fmg( 654,1802):��������� ������������� IN (�������)
      I0_uf = 0006
C LODOCHKA_HANDLER.fmg( 654,1812):��������� ������������� IN (�������)
      I0_ek = 0002
C LODOCHKA_HANDLER.fmg( 654,1822):��������� ������������� IN (�������)
      I0_ok = 0001
C LODOCHKA_HANDLER.fmg( 654,1832):��������� ������������� IN (�������)
      I0_el = 00010
C LODOCHKA_HANDLER.fmg( 696,1806):��������� ������������� IN (�������)
      I0_ul = 0005
C LODOCHKA_HANDLER.fmg( 696,1816):��������� ������������� IN (�������)
      I0_im = 0006
C LODOCHKA_HANDLER.fmg( 696,1826):��������� ������������� IN (�������)
      I0_ap = 0002
C LODOCHKA_HANDLER.fmg( 696,1838):��������� ������������� IN (�������)
      I0_ep = 0001
C LODOCHKA_HANDLER.fmg( 696,1852):��������� ������������� IN (�������)
      I0_ar = 9096
C LODOCHKA_HANDLER.fmg( 654,1847):��������� ������������� IN (�������)
      I0_as = 40
C LODOCHKA_HANDLER.fmg( 695,1906):��������� ������������� IN (�������)
      R0_es = 0
C LODOCHKA_HANDLER.fmg( 696,1894):��������� (RE4) (�������)
      R0_is = 0
C LODOCHKA_HANDLER.fmg( 696,1881):��������� (RE4) (�������)
      R0_us = 0
C LODOCHKA_HANDLER.fmg( 696,1868):��������� (RE4) (�������)
      I0_it = 94
C LODOCHKA_HANDLER.fmg( 654,1906):��������� ������������� IN (�������)
      I0_ut = 91
C LODOCHKA_HANDLER.fmg( 695,1984):��������� ������������� IN (�������)
      L0_iv = L_av.OR.L_ev.OR.L_ox
C LODOCHKA_HANDLER.fmg( 665,1936):���
      I0_abe = 95
C LODOCHKA_HANDLER.fmg( 654,1926):��������� ������������� IN (�������)
      R0_ebe = 1500.0
C LODOCHKA_HANDLER.fmg( 696,1971):��������� (RE4) (�������)
      R0_ibe = 2682.0
C LODOCHKA_HANDLER.fmg( 696,1958):��������� (RE4) (�������)
      R0_ube = 16310.0
C LODOCHKA_HANDLER.fmg( 696,1946):��������� (RE4) (�������)
      I0_afe = 0016
C LODOCHKA_HANDLER.fmg( 680,1611):��������� ������������� IN (�������)
      L0_ife=I_ape.eq.I_ofe
C LODOCHKA_HANDLER.fmg( 660,1600):���������� �������������
      I0_ufe = 0015
C LODOCHKA_HANDLER.fmg( 680,1624):��������� ������������� IN (�������)
      L0_eke=I_ape.eq.I_ike
C LODOCHKA_HANDLER.fmg( 660,1612):���������� �������������
      I0_oke = 0014
C LODOCHKA_HANDLER.fmg( 680,1637):��������� ������������� IN (�������)
      L0_ale=I_ape.eq.I_ele
C LODOCHKA_HANDLER.fmg( 660,1626):���������� �������������
      I0_ile = 0013
C LODOCHKA_HANDLER.fmg( 680,1650):��������� ������������� IN (�������)
      L0_ule=I_ape.eq.I_ame
C LODOCHKA_HANDLER.fmg( 660,1638):���������� �������������
      I0_eme = 0012
C LODOCHKA_HANDLER.fmg( 680,1663):��������� ������������� IN (�������)
      L0_ome=I_ape.eq.I_ume
C LODOCHKA_HANDLER.fmg( 660,1652):���������� �������������
      I0_ope = 0011
C LODOCHKA_HANDLER.fmg( 654,1662):��������� ������������� IN (�������)
      I0_are = 0027
C LODOCHKA_HANDLER.fmg( 678,1679):��������� ������������� IN (�������)
      I0_ire = 0026
C LODOCHKA_HANDLER.fmg( 678,1688):��������� ������������� IN (�������)
      I0_ure = 0025
C LODOCHKA_HANDLER.fmg( 678,1698):��������� ������������� IN (�������)
      I0_ese = 0024
C LODOCHKA_HANDLER.fmg( 678,1708):��������� ������������� IN (�������)
      I0_use = 0023
C LODOCHKA_HANDLER.fmg( 678,1718):��������� ������������� IN (�������)
      I0_ete = 0028
C LODOCHKA_HANDLER.fmg( 654,1674):��������� ������������� IN (�������)
      I0_ute = 0028
C LODOCHKA_HANDLER.fmg( 654,1684):��������� ������������� IN (�������)
      I0_ive = 0028
C LODOCHKA_HANDLER.fmg( 654,1694):��������� ������������� IN (�������)
      I0_axe = 0028
C LODOCHKA_HANDLER.fmg( 654,1703):��������� ������������� IN (�������)
      I0_oxe = 0028
C LODOCHKA_HANDLER.fmg( 654,1712):��������� ������������� IN (�������)
      I0_ebi = 0042
C LODOCHKA_HANDLER.fmg( 654,1725):��������� ������������� IN (�������)
      I0_ubi = 0043
C LODOCHKA_HANDLER.fmg( 654,1734):��������� ������������� IN (�������)
      I0_afi = 0044
C LODOCHKA_HANDLER.fmg( 654,1744):��������� ������������� IN (�������)
      I0_ufi = 0045
C LODOCHKA_HANDLER.fmg( 654,1754):��������� ������������� IN (�������)
      I0_oki = 0046
C LODOCHKA_HANDLER.fmg( 654,1764):��������� ������������� IN (�������)
      I0_eli = 0028
C LODOCHKA_HANDLER.fmg( 686,1770):��������� ������������� IN (�������)
      I0_ami = 60
C LODOCHKA_HANDLER.fmg( 288,1566):��������� ������������� IN (�������)
      I0_imi = 60
C LODOCHKA_HANDLER.fmg( 148,1558):��������� ������������� IN (�������)
      R0_umi = 3140
C LODOCHKA_HANDLER.fmg(  36,1856):��������� (RE4) (�������)
      R0_ari = 2530
C LODOCHKA_HANDLER.fmg(  36,1841):��������� (RE4) (�������)
      R0_esi = 0.0
C LODOCHKA_HANDLER.fmg(  70,1862):��������� (RE4) (�������)
      R0_iti = 2240
C LODOCHKA_HANDLER.fmg(  36,1822):��������� (RE4) (�������)
      R0_avi = 0.0
C LODOCHKA_HANDLER.fmg(  70,1914):��������� (RE4) (�������)
      R0_uvi = 15200
C LODOCHKA_HANDLER.fmg(  36,1904):��������� (RE4) (�������)
      R0_oxi=abs(R_uxi)
C LODOCHKA_HANDLER.fmg( 169,1316):���������� ��������
      L0_ofo=R0_oxi.gt.R_ixi
C LODOCHKA_HANDLER.fmg( 180,1316):���������� >
      R0_obo = 3140.0
C LODOCHKA_HANDLER.fmg( 156,1326):��������� (RE4) (�������)
      R0_ibo = R0_obo + (-R_ubo)
C LODOCHKA_HANDLER.fmg( 163,1324):��������
      R0_ebo=abs(R0_ibo)
C LODOCHKA_HANDLER.fmg( 169,1324):���������� ��������
      L0_ufo=R0_ebo.lt.R_abo
C LODOCHKA_HANDLER.fmg( 180,1324):���������� <
      I0_udo = 60
C LODOCHKA_HANDLER.fmg( 214,1324):��������� ������������� IN (�������)
      R0_afo = 15200
C LODOCHKA_HANDLER.fmg( 215,1312):��������� (RE4) (�������)
      R0_ifo = 3140
C LODOCHKA_HANDLER.fmg( 215,1298):��������� (RE4) (�������)
      I0_iko = 96
C LODOCHKA_HANDLER.fmg( 173,1330):��������� ������������� IN (�������)
      L0_alo=L_uko.and..not.L_oko
      L_oko=L_uko
C LODOCHKA_HANDLER.fmg( 472,1415):������������  �� 1 ���
      I0_imo = 40
C LODOCHKA_HANDLER.fmg( 455,1420):��������� ������������� IN (�������)
      R0_umo = 600.0
C LODOCHKA_HANDLER.fmg( 158,1724):��������� (RE4) (�������)
      R0_ito = 0.0
C LODOCHKA_HANDLER.fmg( 206,1683):��������� (RE4) (�������)
      R0_aro = 100.0
C LODOCHKA_HANDLER.fmg( 158,1652):��������� (RE4) (�������)
      R0_uro = 788.0
C LODOCHKA_HANDLER.fmg( 158,1660):��������� (RE4) (�������)
      R0_evo = 0.0
C LODOCHKA_HANDLER.fmg( 206,1748):��������� (RE4) (�������)
      R0_exo = 16310
C LODOCHKA_HANDLER.fmg( 158,1715):��������� (RE4) (�������)
      R0_uked = 40.0
C LODOCHKA_HANDLER.fmg( 311,1748):��������� (RE4) (�������)
      I0_edu = 40
C LODOCHKA_HANDLER.fmg( 148,1568):��������� ������������� IN (�������)
      I0_afu = 40
C LODOCHKA_HANDLER.fmg( 288,1574):��������� ������������� IN (�������)
      R0_atad = 40.0
C LODOCHKA_HANDLER.fmg( 310,1682):��������� (RE4) (�������)
      I0_efu = 91
C LODOCHKA_HANDLER.fmg( 183,1358):��������� ������������� IN (�������)
      R0_ilu = 7668.0
C LODOCHKA_HANDLER.fmg( 160,1391):��������� (RE4) (�������)
      R0_imu = 700.0
C LODOCHKA_HANDLER.fmg( 160,1374):��������� (RE4) (�������)
      R0_ipu = 790.0
C LODOCHKA_HANDLER.fmg( 160,1382):��������� (RE4) (�������)
      I0_upu = 94
C LODOCHKA_HANDLER.fmg( 219,1389):��������� ������������� IN (�������)
      L0_aru=.true.
C LODOCHKA_HANDLER.fmg( 214,1966):��������� ���������� (�������)
      R0_asu = 789.0
C LODOCHKA_HANDLER.fmg( 158,1942):��������� (RE4) (�������)
      R0_usu = 7668.0
C LODOCHKA_HANDLER.fmg( 158,1951):��������� (RE4) (�������)
      I0_ivu = 91
C LODOCHKA_HANDLER.fmg( 183,1404):��������� ������������� IN (�������)
      R0_obad = 19709.0
C LODOCHKA_HANDLER.fmg( 160,1436):��������� (RE4) (�������)
      R0_odad = 500.0
C LODOCHKA_HANDLER.fmg( 160,1420):��������� (RE4) (�������)
      R0_ofad = 790.0
C LODOCHKA_HANDLER.fmg( 160,1428):��������� (RE4) (�������)
      I0_akad = 96
C LODOCHKA_HANDLER.fmg( 219,1434):��������� ������������� IN (�������)
      R0_ukad = 789.0
C LODOCHKA_HANDLER.fmg( 158,1842):��������� (RE4) (�������)
      R0_olad = 19709.0
C LODOCHKA_HANDLER.fmg( 158,1851):��������� (RE4) (�������)
      L0_epad=.true.
C LODOCHKA_HANDLER.fmg( 214,1866):��������� ���������� (�������)
      R0_osad = 0.0
C LODOCHKA_HANDLER.fmg( 358,1684):��������� (RE4) (�������)
      R0_isad = 0.0
C LODOCHKA_HANDLER.fmg( 310,1676):��������� (RE4) (�������)
      R0_avad = 789.0
C LODOCHKA_HANDLER.fmg( 158,1888):��������� (RE4) (�������)
      R0_uvad = 16310.0
C LODOCHKA_HANDLER.fmg( 158,1896):��������� (RE4) (�������)
      L0_ibed=.true.
C LODOCHKA_HANDLER.fmg( 214,1912):��������� ���������� (�������)
      R0_ufed = 2682.0
C LODOCHKA_HANDLER.fmg( 158,1790):��������� (RE4) (�������)
      R0_oked = 16310.0
C LODOCHKA_HANDLER.fmg( 158,1798):��������� (RE4) (�������)
      R0_aled = 0.0
C LODOCHKA_HANDLER.fmg( 359,1750):��������� (RE4) (�������)
      R0_amed = 1000
C LODOCHKA_HANDLER.fmg( 311,1716):��������� (RE4) (�������)
      L0_ired=.true.
C LODOCHKA_HANDLER.fmg( 214,1814):��������� ���������� (�������)
      R0_eted = 0.0
C LODOCHKA_HANDLER.fmg( 206,1978):��������� (RE4) (�������)
      R0_uved = 999999.0
C LODOCHKA_HANDLER.fmg( 508,1504):��������� (RE4) (�������)
      L0_eved=.false.
C LODOCHKA_HANDLER.fmg( 474,1266):��������� ���������� (�������)
      R0_oved = 1.0
C LODOCHKA_HANDLER.fmg( 472,1282):��������� (RE4) (�������)
      R0_axed = 0.0
C LODOCHKA_HANDLER.fmg( 478,1282):��������� (RE4) (�������)
      R0_abid = 0.0
C LODOCHKA_HANDLER.fmg( 497,1588):��������� (RE4) (�������)
      I0_oxed = 91
C LODOCHKA_HANDLER.fmg( 428,1582):��������� ������������� IN (�������)
      L0_idid=.false.
C LODOCHKA_HANDLER.fmg( 474,1302):��������� ���������� (�������)
      R0_edid = 1.0
C LODOCHKA_HANDLER.fmg( 472,1318):��������� (RE4) (�������)
      R0_odid = 0.0
C LODOCHKA_HANDLER.fmg( 478,1318):��������� (RE4) (�������)
      L0_ofid=.false.
C LODOCHKA_HANDLER.fmg( 474,1337):��������� ���������� (�������)
      R0_efid = 1.0
C LODOCHKA_HANDLER.fmg( 472,1352):��������� (RE4) (�������)
      R0_ufid = 0.0
C LODOCHKA_HANDLER.fmg( 478,1352):��������� (RE4) (�������)
      R0_okid = 999999.0
C LODOCHKA_HANDLER.fmg( 358,1504):��������� (RE4) (�������)
      R0_elid = 6000.0
C LODOCHKA_HANDLER.fmg( 359,1518):��������� (RE4) (�������)
      R0_ilid = 999999.0
C LODOCHKA_HANDLER.fmg( 359,1520):��������� (RE4) (�������)
      I0_olid = 91
C LODOCHKA_HANDLER.fmg( 290,1499):��������� ������������� IN (�������)
      I0_ulid = 65
C LODOCHKA_HANDLER.fmg( 290,1508):��������� ������������� IN (�������)
      R0_imid = 999999.0
C LODOCHKA_HANDLER.fmg( 216,1502):��������� (RE4) (�������)
      R0_apid = 18000.0
C LODOCHKA_HANDLER.fmg( 216,1516):��������� (RE4) (�������)
      R0_epid = 999999.0
C LODOCHKA_HANDLER.fmg( 216,1518):��������� (RE4) (�������)
      I0_ipid = 91
C LODOCHKA_HANDLER.fmg( 148,1497):��������� ������������� IN (�������)
      I0_opid = 65
C LODOCHKA_HANDLER.fmg( 148,1506):��������� ������������� IN (�������)
      I0_arid = 91
C LODOCHKA_HANDLER.fmg( 288,1582):��������� ������������� IN (�������)
      R0_irid = 0.0
C LODOCHKA_HANDLER.fmg( 357,1588):��������� (RE4) (�������)
      I0_esid = 91
C LODOCHKA_HANDLER.fmg( 148,1576):��������� ������������� IN (�������)
      R0_usid = 0.0
C LODOCHKA_HANDLER.fmg( 218,1597):��������� (RE4) (�������)
      I0_atid = 65
C LODOCHKA_HANDLER.fmg( 148,1586):��������� ������������� IN (�������)
      R0_axid = 0.0
C LODOCHKA_HANDLER.fmg(  61,1946):��������� (RE4) (�������)
      R0_oxid = 0.0
C LODOCHKA_HANDLER.fmg(  61,1965):��������� (RE4) (�������)
      C20_obod = 'FDA60'
C LODOCHKA_HANDLER.fmg(  22,1973):��������� ���������� CH20 (CH30) (�������)
      R0_exi = R0_uvi + (-R_evid)
C LODOCHKA_HANDLER.fmg(  43,1902):��������
C label 265  try265=try265-1
      R0_elu = R0_ilu + (-R_evid)
C LODOCHKA_HANDLER.fmg( 168,1389):��������
      R0_alu=abs(R0_elu)
C LODOCHKA_HANDLER.fmg( 174,1389):���������� ��������
      L0_oku=R0_alu.lt.R_uku
C LODOCHKA_HANDLER.fmg( 182,1389):���������� <
      R0_ibad = R0_obad + (-R_evid)
C LODOCHKA_HANDLER.fmg( 168,1434):��������
      R0_ebad=abs(R0_ibad)
C LODOCHKA_HANDLER.fmg( 174,1434):���������� ��������
      L0_uxu=R0_ebad.lt.R_abad
C LODOCHKA_HANDLER.fmg( 182,1434):���������� <
      R0_ifad = R0_ofad + (-R_afid)
C LODOCHKA_HANDLER.fmg( 168,1426):��������
      R0_efad=abs(R0_ifad)
C LODOCHKA_HANDLER.fmg( 174,1426):���������� ��������
      L0_udad=R0_efad.lt.R_afad
C LODOCHKA_HANDLER.fmg( 182,1426):���������� <
      R0_otu = R_atu + (-R_ixed)
C LODOCHKA_HANDLER.fmg( 166,1958):��������
      R0_itu=abs(R0_otu)
C LODOCHKA_HANDLER.fmg( 172,1958):���������� ��������
      L0_evu=R0_itu.lt.R_etu
C LODOCHKA_HANDLER.fmg( 180,1958):���������� <
      R0_osu = R0_usu + (-R_evid)
C LODOCHKA_HANDLER.fmg( 166,1949):��������
      R0_isu=abs(R0_osu)
C LODOCHKA_HANDLER.fmg( 172,1949):���������� ��������
      L0_avu=R0_isu.lt.R_esu
C LODOCHKA_HANDLER.fmg( 180,1949):���������� <
      R0_uru = R0_asu + (-R_afid)
C LODOCHKA_HANDLER.fmg( 166,1940):��������
      R0_oru=abs(R0_uru)
C LODOCHKA_HANDLER.fmg( 172,1940):���������� ��������
      L0_utu=R0_oru.lt.R_iru
C LODOCHKA_HANDLER.fmg( 180,1940):���������� <
      L0_ured = L_ised.AND.L0_evu.AND.L0_avu.AND.L0_utu
C LODOCHKA_HANDLER.fmg( 191,1962):�
      L_used=L0_ured.or.(L_used.and..not.(L_esed))
      L0_ased=.not.L_used
C LODOCHKA_HANDLER.fmg( 201,1960):RS �������
      if(L_used) then
         R0_ated=R_ibid
      else
         R0_ated=R0_eted
      endif
C LODOCHKA_HANDLER.fmg( 209,1976):���� RE IN LO CH7
      R0_oxad = R_axad + (-R_ixed)
C LODOCHKA_HANDLER.fmg( 166,1903):��������
      R0_ixad=abs(R0_oxad)
C LODOCHKA_HANDLER.fmg( 172,1903):���������� ��������
      L0_ebed=R0_ixad.lt.R_exad
C LODOCHKA_HANDLER.fmg( 180,1903):���������� <
      R0_ovad = R0_uvad + (-R_evid)
C LODOCHKA_HANDLER.fmg( 166,1894):��������
      R0_ivad=abs(R0_ovad)
C LODOCHKA_HANDLER.fmg( 172,1894):���������� ��������
      L0_abed=R0_ivad.lt.R_evad
C LODOCHKA_HANDLER.fmg( 180,1894):���������� <
      R0_utad = R0_avad + (-R_afid)
C LODOCHKA_HANDLER.fmg( 166,1886):��������
      R0_otad=abs(R0_utad)
C LODOCHKA_HANDLER.fmg( 172,1886):���������� ��������
      L0_uxad=R0_otad.lt.R_itad
C LODOCHKA_HANDLER.fmg( 180,1886):���������� <
      L0_aded = L_afed.AND.L0_ebed.AND.L0_abed.AND.L0_uxad
C LODOCHKA_HANDLER.fmg( 191,1908):�
      L_eded=L0_aded.or.(L_eded.and..not.(L_oded))
      L0_ided=.not.L_eded
C LODOCHKA_HANDLER.fmg( 200,1906):RS �������
      if(L_eded) then
         R0_ubed=R_uded
      else
         R0_ubed=R0_ated
      endif
C LODOCHKA_HANDLER.fmg( 209,1917):���� RE IN LO CH7
      R0_imad = R_ulad + (-R_ixed)
C LODOCHKA_HANDLER.fmg( 166,1858):��������
      R0_emad=abs(R0_imad)
C LODOCHKA_HANDLER.fmg( 172,1858):���������� ��������
      L0_apad=R0_emad.lt.R_amad
C LODOCHKA_HANDLER.fmg( 180,1858):���������� <
      R0_ilad = R0_olad + (-R_evid)
C LODOCHKA_HANDLER.fmg( 166,1849):��������
      R0_elad=abs(R0_ilad)
C LODOCHKA_HANDLER.fmg( 172,1849):���������� ��������
      L0_umad=R0_elad.lt.R_alad
C LODOCHKA_HANDLER.fmg( 180,1849):���������� <
      R0_okad = R0_ukad + (-R_afid)
C LODOCHKA_HANDLER.fmg( 166,1840):��������
      R0_ikad=abs(R0_okad)
C LODOCHKA_HANDLER.fmg( 172,1840):���������� ��������
      L0_omad=R0_ikad.lt.R_ekad
C LODOCHKA_HANDLER.fmg( 180,1840):���������� <
      L0_opad = L_orad.AND.L0_apad.AND.L0_umad.AND.L0_omad
C LODOCHKA_HANDLER.fmg( 191,1862):�
      L_upad=L0_opad.or.(L_upad.and..not.(L_erad))
      L0_arad=.not.L_upad
C LODOCHKA_HANDLER.fmg( 200,1860):RS �������
      if(L_upad) then
         R0_osed=R_irad
      else
         R0_osed=R0_ubed
      endif
C LODOCHKA_HANDLER.fmg( 209,1872):���� RE IN LO CH7
      R0_oped = R_aped + (-R_ixed)
C LODOCHKA_HANDLER.fmg( 166,1805):��������
      R0_iped=abs(R0_oped)
C LODOCHKA_HANDLER.fmg( 172,1805):���������� ��������
      L0_ered=R0_iped.lt.R_eped
C LODOCHKA_HANDLER.fmg( 180,1805):���������� <
      R0_iked = R0_oked + (-R_evid)
C LODOCHKA_HANDLER.fmg( 166,1796):��������
      R0_eked=abs(R0_iked)
C LODOCHKA_HANDLER.fmg( 172,1796):���������� ��������
      L0_ared=R0_eked.lt.R_aked
C LODOCHKA_HANDLER.fmg( 180,1796):���������� <
      R0_ofed = R0_ufed + (-R_afid)
C LODOCHKA_HANDLER.fmg( 166,1788):��������
      R0_ifed=abs(R0_ofed)
C LODOCHKA_HANDLER.fmg( 172,1788):���������� ��������
      L0_uped=R0_ifed.lt.R_efed
C LODOCHKA_HANDLER.fmg( 180,1788):���������� <
      L0_ited = L_adid.AND.L0_ered.AND.L0_ared.AND.L0_uped
C LODOCHKA_HANDLER.fmg( 191,1810):�
      L_oted=L0_ited.or.(L_oted.and..not.(L_aved))
      L0_uted=.not.L_oted
C LODOCHKA_HANDLER.fmg( 200,1808):RS �������
      if(L_oted) then
         R0_ubid=R_obid
      else
         R0_ubid=R0_osed
      endif
C LODOCHKA_HANDLER.fmg( 209,1819):���� RE IN LO CH7
      L0_uxed=I_etid.eq.I0_oxed
C LODOCHKA_HANDLER.fmg( 452,1582):���������� �������������
      if(L0_uxed) then
         R0_ebid=R0_ubid
      else
         R0_ebid=R0_abid
      endif
C LODOCHKA_HANDLER.fmg( 500,1586):���� RE IN LO CH7
      R0_idad = R0_odad + (-R_ixed)
C LODOCHKA_HANDLER.fmg( 168,1418):��������
      R0_edad=abs(R0_idad)
C LODOCHKA_HANDLER.fmg( 174,1418):���������� ��������
      L0_ubad=R0_edad.lt.R_adad
C LODOCHKA_HANDLER.fmg( 182,1418):���������� <
      L0_ixu=I_etid.eq.I0_ivu
C LODOCHKA_HANDLER.fmg( 189,1405):���������� �������������
      L0_oxu = L0_uxu.AND.L0_udad.AND.L0_ubad.AND.L_erad.AND.L0_ixu
C LODOCHKA_HANDLER.fmg( 200,1424):�
      if(L0_oxu.and..not.L_exu) then
         R_uvu=R_axu
      else
         R_uvu=max(R0_ovu-deltat,0.0)
      endif
      L0_ufad=R_uvu.gt.0.0
      L_exu=L0_oxu
C LODOCHKA_HANDLER.fmg( 213,1424):������������  �� T
      if(L0_ufad) then
         I_etid=I0_akad
      endif
C LODOCHKA_HANDLER.fmg( 225,1434):���� � ������������� �������
      L0_et=I_etid.eq.I0_it
C LODOCHKA_HANDLER.fmg( 660,1906):���������� �������������
      L0_at = L_ot.AND.L0_et
C LODOCHKA_HANDLER.fmg( 672,1914):�
      if(L0_at.and..not.L_ur) then
         R_ir=R_or
      else
         R_ir=max(R0_er-deltat,0.0)
      endif
      L0_os=R_ir.gt.0.0
      L_ur=L0_at
C LODOCHKA_HANDLER.fmg( 690,1914):������������  �� T
      if(L0_os) then
         I_etid=I0_as
      endif
C LODOCHKA_HANDLER.fmg( 701,1906):���� � ������������� �������
      L0_ux=I_etid.eq.I0_abe
C LODOCHKA_HANDLER.fmg( 660,1927):���������� �������������
      L0_ix = L0_iv.AND.L0_ux
C LODOCHKA_HANDLER.fmg( 672,1932):�
      if(L0_ix.and..not.L_ex) then
         R_uv=R_ax
      else
         R_uv=max(R0_ov-deltat,0.0)
      endif
      L0_obe=R_uv.gt.0.0
      L_ex=L0_ix
C LODOCHKA_HANDLER.fmg( 690,1940):������������  �� T
      if(L0_obe) then
         R0_udid=R0_ibe
      endif
C LODOCHKA_HANDLER.fmg( 701,1958):���� � ������������� �������
      if(L0_os) then
         R0_udid=R0_is
      endif
C LODOCHKA_HANDLER.fmg( 701,1880):���� � ������������� �������
      R0_obu = R_uxo + (-R_afid)
C LODOCHKA_HANDLER.fmg( 166,1732):��������
      R0_ibu=abs(R0_obu)
C LODOCHKA_HANDLER.fmg( 172,1732):���������� ��������
      L0_abu=R0_ibu.lt.R_ebu
C LODOCHKA_HANDLER.fmg( 180,1732):���������� <
      if(L0_obe) then
         R0_ived=R0_ebe
      endif
C LODOCHKA_HANDLER.fmg( 701,1970):���� � ������������� �������
      if(L0_os) then
         R0_ived=R0_es
      endif
C LODOCHKA_HANDLER.fmg( 701,1894):���� � ������������� �������
      L0_ude = L0_ix.OR.L0_at
C LODOCHKA_HANDLER.fmg( 681,1931):���
      if(L0_ude.and..not.L_ode) then
         R_ede=R_ide
      else
         R_ede=max(R0_ade-deltat,0.0)
      endif
      L0_ifid=R_ede.gt.0.0
      L_ode=L0_ude
C LODOCHKA_HANDLER.fmg( 690,1931):������������  �� T
      if(L0_ifid) then
         R_ixed=R0_ived
      elseif(L0_eved) then
         R_ixed=R_ixed
      else
         R_ixed=R_ixed+deltat/R0_oved*R0_ebid
      endif
      if(R_ixed.gt.R0_uved) then
         R_ixed=R0_uved
      elseif(R_ixed.lt.R0_axed) then
         R_ixed=R0_axed
      endif
C LODOCHKA_HANDLER.fmg( 476,1273):����������
C sav1=R0_otu
      R0_otu = R_atu + (-R_ixed)
C LODOCHKA_HANDLER.fmg( 166,1958):recalc:��������
C if(sav1.ne.R0_otu .and. try281.gt.0) goto 281
C sav1=R0_imad
      R0_imad = R_ulad + (-R_ixed)
C LODOCHKA_HANDLER.fmg( 166,1858):recalc:��������
C if(sav1.ne.R0_imad .and. try325.gt.0) goto 325
C sav1=R0_oxad
      R0_oxad = R_axad + (-R_ixed)
C LODOCHKA_HANDLER.fmg( 166,1903):recalc:��������
C if(sav1.ne.R0_oxad .and. try303.gt.0) goto 303
C sav1=R0_oped
      R0_oped = R_aped + (-R_ixed)
C LODOCHKA_HANDLER.fmg( 166,1805):recalc:��������
C if(sav1.ne.R0_oped .and. try347.gt.0) goto 347
C sav1=R0_idad
      R0_idad = R0_odad + (-R_ixed)
C LODOCHKA_HANDLER.fmg( 168,1418):recalc:��������
C if(sav1.ne.R0_idad .and. try379.gt.0) goto 379
      R0_epo = R0_umo + (-R_ixed)
C LODOCHKA_HANDLER.fmg( 166,1722):��������
      R0_apo=abs(R0_epo)
C LODOCHKA_HANDLER.fmg( 172,1722):���������� ��������
      L0_ivo=R0_apo.lt.R_omo
C LODOCHKA_HANDLER.fmg( 180,1722):���������� <
      R0_oxo = R0_exo + (-R_evid)
C LODOCHKA_HANDLER.fmg( 166,1712):��������
      R0_ixo=abs(R0_oxo)
C LODOCHKA_HANDLER.fmg( 172,1712):���������� ��������
      L0_uvo=R0_ixo.lt.R_axo
C LODOCHKA_HANDLER.fmg( 180,1712):���������� <
      L0_ovo = L0_abu.AND.L0_ivo.AND.L0_uvo
C LODOCHKA_HANDLER.fmg( 188,1730):�
      if(L0_ovo) then
         R0_erid=R_avo
      else
         R0_erid=R0_evo
      endif
C LODOCHKA_HANDLER.fmg( 210,1747):���� RE IN LO CH7
      if(L0_obe) then
         I_etid=I0_ut
      endif
C LODOCHKA_HANDLER.fmg( 701,1983):���� � ������������� �������
      L0_upid=I_etid.eq.I0_arid
C LODOCHKA_HANDLER.fmg( 312,1582):���������� �������������
      if(L0_upid) then
         R0_odu=R0_erid
      else
         R0_odu=R0_irid
      endif
C LODOCHKA_HANDLER.fmg( 360,1586):���� RE IN LO CH7
      L0_omed=R_afid.lt.R_umed
C LODOCHKA_HANDLER.fmg( 333,1734):���������� <
      R0_imed = R0_amed + (-R_evid)
C LODOCHKA_HANDLER.fmg( 318,1714):��������
      R0_emed=abs(R0_imed)
C LODOCHKA_HANDLER.fmg( 324,1714):���������� ��������
      L0_oled=R0_emed.lt.R_uled
C LODOCHKA_HANDLER.fmg( 333,1714):���������� <
      L0_iled = L0_omed.AND.L0_oled
C LODOCHKA_HANDLER.fmg( 340,1733):�
      if(L0_iled) then
         R0_eled=R0_uked
      else
         R0_eled=R0_aled
      endif
C LODOCHKA_HANDLER.fmg( 362,1748):���� RE IN LO CH7
      L0_udu=I_etid.eq.I0_afu
C LODOCHKA_HANDLER.fmg( 312,1575):���������� �������������
      if(L0_udu) then
         R0_idu=R0_eled
      else
         R0_idu=R0_odu
      endif
C LODOCHKA_HANDLER.fmg( 360,1578):���� RE IN LO CH7
      R0_axi=abs(R0_exi)
C LODOCHKA_HANDLER.fmg(  49,1902):���������� ��������
      L0_ivi=R0_axi.lt.R_ovi
C LODOCHKA_HANDLER.fmg(  58,1902):���������� <
      if(L0_ivi) then
         R0_evi=R_uxi
      else
         R0_evi=R0_avi
      endif
C LODOCHKA_HANDLER.fmg(  74,1912):���� RE IN LO CH7
      L0_uli=I_etid.eq.I0_ami
C LODOCHKA_HANDLER.fmg( 312,1566):���������� �������������
      if(L0_uli) then
         R0_utid=R0_evi
      else
         R0_utid=R0_idu
      endif
C LODOCHKA_HANDLER.fmg( 360,1570):���� RE IN LO CH7
      L0_ukid=I_etid.eq.I0_ulid
C LODOCHKA_HANDLER.fmg( 314,1510):���������� �������������
      if(L0_ukid) then
         R0_alid=R0_elid
      else
         R0_alid=R0_ilid
      endif
C LODOCHKA_HANDLER.fmg( 362,1518):���� RE IN LO CH7
      L0_ekid=I_etid.eq.I0_olid
C LODOCHKA_HANDLER.fmg( 314,1500):���������� �������������
      if(L0_ekid) then
         R0_ikid=R0_okid
      else
         R0_ikid=R0_alid
      endif
C LODOCHKA_HANDLER.fmg( 362,1504):���� RE IN LO CH7
      R0_epu = R0_ipu + (-R_afid)
C LODOCHKA_HANDLER.fmg( 168,1380):��������
      R0_apu=abs(R0_epu)
C LODOCHKA_HANDLER.fmg( 174,1380):���������� ��������
      L0_omu=R0_apu.lt.R_umu
C LODOCHKA_HANDLER.fmg( 182,1380):���������� <
      R0_emu = R0_imu + (-R_ixed)
C LODOCHKA_HANDLER.fmg( 168,1372):��������
      R0_amu=abs(R0_emu)
C LODOCHKA_HANDLER.fmg( 174,1372):���������� ��������
      L0_olu=R0_amu.lt.R_ulu
C LODOCHKA_HANDLER.fmg( 182,1372):���������� <
      L0_eku=I_etid.eq.I0_efu
C LODOCHKA_HANDLER.fmg( 189,1360):���������� �������������
      L0_iku = L0_oku.AND.L0_omu.AND.L0_olu.AND.L_esed.AND.L0_eku
C LODOCHKA_HANDLER.fmg( 200,1378):�
      if(L0_iku.and..not.L_aku) then
         R_ofu=R_ufu
      else
         R_ofu=max(R0_ifu-deltat,0.0)
      endif
      L0_opu=R_ofu.gt.0.0
      L_aku=L0_iku
C LODOCHKA_HANDLER.fmg( 213,1378):������������  �� T
      if(L0_opu) then
         I_etid=I0_upu
      endif
C LODOCHKA_HANDLER.fmg( 225,1388):���� � ������������� �������
      L0_eko=I_etid.eq.I0_iko
C LODOCHKA_HANDLER.fmg( 179,1331):���������� �������������
      L0_ako = L0_eko.AND.L0_ufo.AND.L0_ofo
C LODOCHKA_HANDLER.fmg( 191,1329):�
      if(L0_ako.and..not.L_odo) then
         R_edo=R_ido
      else
         R_edo=max(R0_ado-deltat,0.0)
      endif
      L0_efo=R_edo.gt.0.0
      L_odo=L0_ako
C LODOCHKA_HANDLER.fmg( 209,1329):������������  �� T
      if(L0_efo) then
         R0_akid=R0_afo
      endif
C LODOCHKA_HANDLER.fmg( 220,1311):���� � ������������� �������
      if(L0_obe) then
         R0_akid=R0_ube
      endif
C LODOCHKA_HANDLER.fmg( 701,1945):���� � ������������� �������
      if(L0_os) then
         R0_akid=R0_us
      endif
C LODOCHKA_HANDLER.fmg( 701,1868):���� � ������������� �������
      if(L0_efo) then
         R0_udid=R0_ifo
      endif
C LODOCHKA_HANDLER.fmg( 220,1298):���� � ������������� �������
      if(L0_ifid) then
         R_afid=R0_udid
      elseif(L0_idid) then
         R_afid=R_afid
      else
         R_afid=R_afid+deltat/R0_edid*R0_utid
      endif
      if(R_afid.gt.R0_ikid) then
         R_afid=R0_ikid
      elseif(R_afid.lt.R0_odid) then
         R_afid=R0_odid
      endif
C LODOCHKA_HANDLER.fmg( 476,1309):����������
C sav1=R0_uru
      R0_uru = R0_asu + (-R_afid)
C LODOCHKA_HANDLER.fmg( 166,1940):recalc:��������
C if(sav1.ne.R0_uru .and. try291.gt.0) goto 291
C sav1=R0_epu
      R0_epu = R0_ipu + (-R_afid)
C LODOCHKA_HANDLER.fmg( 168,1380):recalc:��������
C if(sav1.ne.R0_epu .and. try581.gt.0) goto 581
C sav1=R0_obu
      R0_obu = R_uxo + (-R_afid)
C LODOCHKA_HANDLER.fmg( 166,1732):recalc:��������
C if(sav1.ne.R0_obu .and. try464.gt.0) goto 464
C sav1=R0_utad
      R0_utad = R0_avad + (-R_afid)
C LODOCHKA_HANDLER.fmg( 166,1886):recalc:��������
C if(sav1.ne.R0_utad .and. try313.gt.0) goto 313
C sav1=R0_okad
      R0_okad = R0_ukad + (-R_afid)
C LODOCHKA_HANDLER.fmg( 166,1840):recalc:��������
C if(sav1.ne.R0_okad .and. try335.gt.0) goto 335
C sav1=R0_ofed
      R0_ofed = R0_ufed + (-R_afid)
C LODOCHKA_HANDLER.fmg( 166,1788):recalc:��������
C if(sav1.ne.R0_ofed .and. try357.gt.0) goto 357
C sav1=R0_ifad
      R0_ifad = R0_ofad + (-R_afid)
C LODOCHKA_HANDLER.fmg( 168,1426):recalc:��������
C if(sav1.ne.R0_ifad .and. try276.gt.0) goto 276
      R_ibod=R_afid
C LODOCHKA_HANDLER.fmg( 506,1309):������,VY01
      L0_uxid=R_ibod.lt.R_abod
C LODOCHKA_HANDLER.fmg(  54,1958):���������� <
      if(L0_uxid) then
         R0_ebod=R_ixid
      else
         R0_ebod=R0_oxid
      endif
C LODOCHKA_HANDLER.fmg(  64,1964):���� RE IN LO CH7
      L0_ovid=R_ibod.gt.R_ivid
C LODOCHKA_HANDLER.fmg(  54,1939):���������� >
      if(L0_ovid) then
         R0_exid=R_uvid
      else
         R0_exid=R0_axid
      endif
C LODOCHKA_HANDLER.fmg(  64,1945):���� RE IN LO CH7
      R0_otid = R0_ebod + R0_exid
C LODOCHKA_HANDLER.fmg( 212,1595):��������
      if(L0_efo) then
         I_etid=I0_udo
      endif
C LODOCHKA_HANDLER.fmg( 220,1324):���� � ������������� �������
      L0_isid=I_etid.eq.I0_atid
C LODOCHKA_HANDLER.fmg( 172,1588):���������� �������������
      if(L0_isid) then
         R0_osid=R0_otid
      else
         R0_osid=R0_usid
      endif
C LODOCHKA_HANDLER.fmg( 221,1596):���� RE IN LO CH7
      R0_oso = R_aso + (-R_evid)
C LODOCHKA_HANDLER.fmg( 166,1671):��������
      R0_iso=abs(R0_oso)
C LODOCHKA_HANDLER.fmg( 172,1671):���������� ��������
      L0_eto=R0_iso.lt.R_eso
C LODOCHKA_HANDLER.fmg( 180,1671):���������� <
      R0_oro = R0_uro + (-R_afid)
C LODOCHKA_HANDLER.fmg( 166,1658):��������
      R0_iro=abs(R0_oro)
C LODOCHKA_HANDLER.fmg( 172,1658):���������� ��������
      L0_ato=R0_iro.lt.R_ero
C LODOCHKA_HANDLER.fmg( 180,1658):���������� <
      R0_upo = R0_aro + (-R_ixed)
C LODOCHKA_HANDLER.fmg( 166,1650):��������
      R0_opo=abs(R0_upo)
C LODOCHKA_HANDLER.fmg( 172,1650):���������� ��������
      L0_uso=R0_opo.lt.R_ipo
C LODOCHKA_HANDLER.fmg( 180,1650):���������� <
      L0_oto = L0_eto.AND.L0_ato.AND.L0_uso
C LODOCHKA_HANDLER.fmg( 191,1669):�
      if(L0_oto) then
         R0_asid=R_uto
      else
         R0_asid=R0_ito
      endif
C LODOCHKA_HANDLER.fmg( 209,1682):���� RE IN LO CH7
      L0_orid=I_etid.eq.I0_esid
C LODOCHKA_HANDLER.fmg( 172,1578):���������� �������������
      if(L0_orid) then
         R0_urid=R0_asid
      else
         R0_urid=R0_osid
      endif
C LODOCHKA_HANDLER.fmg( 220,1582):���� RE IN LO CH7
      R0_esad = R0_isad + (-R_afid)
C LODOCHKA_HANDLER.fmg( 317,1674):��������
      R0_asad=abs(R0_esad)
C LODOCHKA_HANDLER.fmg( 323,1674):���������� ��������
      L0_usad=R0_asad.lt.R_urad
C LODOCHKA_HANDLER.fmg( 332,1674):���������� <
      if(L0_usad) then
         R0_etad=R0_atad
      else
         R0_etad=R0_osad
      endif
C LODOCHKA_HANDLER.fmg( 362,1683):���� RE IN LO CH7
      L0_adu=I_etid.eq.I0_edu
C LODOCHKA_HANDLER.fmg( 172,1568):���������� �������������
      if(L0_adu) then
         R0_ubu=R0_etad
      else
         R0_ubu=R0_urid
      endif
C LODOCHKA_HANDLER.fmg( 220,1572):���� RE IN LO CH7
      R0_epi = R0_umi + (-R_afid)
C LODOCHKA_HANDLER.fmg(  43,1853):��������
      R0_api=abs(R0_epi)
C LODOCHKA_HANDLER.fmg(  49,1853):���������� ��������
      L0_ipi=R0_api.lt.R_omi
C LODOCHKA_HANDLER.fmg(  58,1853):���������� <
      if(L0_ipi) then
         R0_asi=R_opi
      else
         R0_asi=R0_esi
      endif
C LODOCHKA_HANDLER.fmg(  74,1860):���� RE IN LO CH7
      R0_iri = R0_ari + (-R_afid)
C LODOCHKA_HANDLER.fmg(  43,1838):��������
      R0_eri=abs(R0_iri)
C LODOCHKA_HANDLER.fmg(  49,1838):���������� ��������
      L0_ori=R0_eri.lt.R_upi
C LODOCHKA_HANDLER.fmg(  58,1838):���������� <
      if(L0_ori) then
         R0_osi=R_uri
      else
         R0_osi=R0_asi
      endif
C LODOCHKA_HANDLER.fmg(  74,1846):���� RE IN LO CH7
      R0_uti = R0_iti + (-R_afid)
C LODOCHKA_HANDLER.fmg(  43,1820):��������
      R0_oti=abs(R0_uti)
C LODOCHKA_HANDLER.fmg(  49,1820):���������� ��������
      L0_ati=R0_oti.lt.R_eti
C LODOCHKA_HANDLER.fmg(  58,1820):���������� <
      if(L0_ati) then
         R0_usi=R_isi
      else
         R0_usi=R0_osi
      endif
C LODOCHKA_HANDLER.fmg(  74,1830):���� RE IN LO CH7
      L0_emi=I_etid.eq.I0_imi
C LODOCHKA_HANDLER.fmg( 172,1560):���������� �������������
      if(L0_emi) then
         R0_itid=R0_usi
      else
         R0_itid=R0_ubu
      endif
C LODOCHKA_HANDLER.fmg( 220,1563):���� RE IN LO CH7
      L0_omid=I_etid.eq.I0_opid
C LODOCHKA_HANDLER.fmg( 172,1508):���������� �������������
      if(L0_omid) then
         R0_umid=R0_apid
      else
         R0_umid=R0_epid
      endif
C LODOCHKA_HANDLER.fmg( 220,1516):���� RE IN LO CH7
      L0_amid=I_etid.eq.I0_ipid
C LODOCHKA_HANDLER.fmg( 172,1498):���������� �������������
      if(L0_amid) then
         R0_emid=R0_imid
      else
         R0_emid=R0_umid
      endif
C LODOCHKA_HANDLER.fmg( 220,1502):���� RE IN LO CH7
      if(L0_ifid) then
         R_evid=R0_akid
      elseif(L0_ofid) then
         R_evid=R_evid
      else
         R_evid=R_evid+deltat/R0_efid*R0_itid
      endif
      if(R_evid.gt.R0_emid) then
         R_evid=R0_emid
      elseif(R_evid.lt.R0_ufid) then
         R_evid=R0_ufid
      endif
C LODOCHKA_HANDLER.fmg( 476,1344):����������
C sav1=R0_oso
      R0_oso = R_aso + (-R_evid)
C LODOCHKA_HANDLER.fmg( 166,1671):recalc:��������
C if(sav1.ne.R0_oso .and. try673.gt.0) goto 673
C sav1=R0_elu
      R0_elu = R0_ilu + (-R_evid)
C LODOCHKA_HANDLER.fmg( 168,1389):recalc:��������
C if(sav1.ne.R0_elu .and. try266.gt.0) goto 266
C sav1=R0_ovad
      R0_ovad = R0_uvad + (-R_evid)
C LODOCHKA_HANDLER.fmg( 166,1894):recalc:��������
C if(sav1.ne.R0_ovad .and. try308.gt.0) goto 308
C sav1=R0_osu
      R0_osu = R0_usu + (-R_evid)
C LODOCHKA_HANDLER.fmg( 166,1949):recalc:��������
C if(sav1.ne.R0_osu .and. try286.gt.0) goto 286
C sav1=R0_exi
      R0_exi = R0_uvi + (-R_evid)
C LODOCHKA_HANDLER.fmg(  43,1902):recalc:��������
C if(sav1.ne.R0_exi .and. try265.gt.0) goto 265
C sav1=R0_imed
      R0_imed = R0_amed + (-R_evid)
C LODOCHKA_HANDLER.fmg( 318,1714):recalc:��������
C if(sav1.ne.R0_imed .and. try537.gt.0) goto 537
C sav1=R0_ibad
      R0_ibad = R0_obad + (-R_evid)
C LODOCHKA_HANDLER.fmg( 168,1434):recalc:��������
C if(sav1.ne.R0_ibad .and. try271.gt.0) goto 271
C sav1=R0_iked
      R0_iked = R0_oked + (-R_evid)
C LODOCHKA_HANDLER.fmg( 166,1796):recalc:��������
C if(sav1.ne.R0_iked .and. try352.gt.0) goto 352
C sav1=R0_ilad
      R0_ilad = R0_olad + (-R_evid)
C LODOCHKA_HANDLER.fmg( 166,1849):recalc:��������
C if(sav1.ne.R0_ilad .and. try330.gt.0) goto 330
C sav1=R0_oxo
      R0_oxo = R0_exo + (-R_evid)
C LODOCHKA_HANDLER.fmg( 166,1712):recalc:��������
C if(sav1.ne.R0_oxo .and. try517.gt.0) goto 517
      R_avid=R_evid
C LODOCHKA_HANDLER.fmg( 506,1344):������,VX01
      L0_emo=I_etid.eq.I0_imo
C LODOCHKA_HANDLER.fmg( 478,1420):���������� �������������
      L0_olo = L0_emo.AND.L0_alo
C LODOCHKA_HANDLER.fmg( 484,1420):�
      I_oli=I_etid
C LODOCHKA_HANDLER.fmg( 497,1248):������,LP
      R_exed=R_ixed
C LODOCHKA_HANDLER.fmg( 506,1273):������,VZ01
      if(L_oted) then
         L_ored=L0_ired
      endif
C LODOCHKA_HANDLER.fmg( 218,1813):���� � ������������� �������
      if(L_upad) then
         L_ipad=L0_epad
      endif
C LODOCHKA_HANDLER.fmg( 218,1866):���� � ������������� �������
      if(L_eded) then
         L_obed=L0_ibed
      endif
C LODOCHKA_HANDLER.fmg( 218,1911):���� � ������������� �������
      if(L_used) then
         L_eru=L0_aru
      endif
C LODOCHKA_HANDLER.fmg( 218,1966):���� � ������������� �������
      L0_up=I_ili.eq.I0_ar
C LODOCHKA_HANDLER.fmg( 660,1848):���������� �������������
C label 800  try800=try800-1
      L0_ip = L0_up.AND.L_op
C LODOCHKA_HANDLER.fmg( 673,1847):�
      if(L0_ip) then
         I_ili=I0_ep
      endif
C LODOCHKA_HANDLER.fmg( 701,1852):���� � ������������� �������
      L0_iki=I_ili.eq.I0_oki
C LODOCHKA_HANDLER.fmg( 660,1764):���������� �������������
      L0_eki = L0_iki.AND.L_uki
C LODOCHKA_HANDLER.fmg( 674,1764):�
      L0_ofi=I_ili.eq.I0_ufi
C LODOCHKA_HANDLER.fmg( 660,1755):���������� �������������
      L0_ifi = L0_ofi.AND.L_aki
C LODOCHKA_HANDLER.fmg( 674,1754):�
      L0_udi=I_ili.eq.I0_afi
C LODOCHKA_HANDLER.fmg( 660,1746):���������� �������������
      L0_odi = L0_udi.AND.L_efi
C LODOCHKA_HANDLER.fmg( 674,1744):�
      L0_obi=I_ili.eq.I0_ubi
C LODOCHKA_HANDLER.fmg( 660,1736):���������� �������������
      L0_idi = L0_obi.AND.L_adi
C LODOCHKA_HANDLER.fmg( 674,1734):�
      L0_abi=I_ili.eq.I0_ebi
C LODOCHKA_HANDLER.fmg( 660,1726):���������� �������������
      L0_edi = L0_abi.AND.L_ibi
C LODOCHKA_HANDLER.fmg( 674,1725):�
      L0_ali = L0_eki.OR.L0_ifi.OR.L0_odi.OR.L0_idi.OR.L0_edi
C LODOCHKA_HANDLER.fmg( 688,1760):���
      if(L0_ali) then
         I_ili=I0_eli
      endif
C LODOCHKA_HANDLER.fmg( 692,1769):���� � ������������� �������
      L0_ixe=I_ili.eq.I0_oxe
C LODOCHKA_HANDLER.fmg( 660,1714):���������� �������������
      L0_ose = L0_ixe.AND.L_uxe
C LODOCHKA_HANDLER.fmg( 668,1712):�
      if(L0_ose) then
         I_ili=I0_use
      endif
C LODOCHKA_HANDLER.fmg( 683,1717):���� � ������������� �������
      L0_ed=I_ili.eq.I0_id
C LODOCHKA_HANDLER.fmg( 660,1793):���������� �������������
      L0_ud = L0_ed.AND.L_af
C LODOCHKA_HANDLER.fmg( 673,1792):�
      if(L0_ud) then
         I_ili=I0_od
      endif
C LODOCHKA_HANDLER.fmg( 700,1796):���� � ������������� �������
      L0_ate=I_ili.eq.I0_ete
C LODOCHKA_HANDLER.fmg( 660,1675):���������� �������������
      L0_ere = L0_ate.AND.L_ite
C LODOCHKA_HANDLER.fmg( 668,1674):�
      if(L0_ere) then
         I_ili=I0_are
      endif
C LODOCHKA_HANDLER.fmg( 683,1678):���� � ������������� �������
      L0_ik=I_ili.eq.I0_ok
C LODOCHKA_HANDLER.fmg( 660,1834):���������� �������������
      L0_um = L0_ik.AND.L_uk
C LODOCHKA_HANDLER.fmg( 673,1832):�
      if(L0_um) then
         I_ili=I0_ap
      endif
C LODOCHKA_HANDLER.fmg( 701,1838):���� � ������������� �������
      L0_ak=I_ili.eq.I0_ek
C LODOCHKA_HANDLER.fmg( 660,1822):���������� �������������
      L0_om = L0_ak.AND.L_al
C LODOCHKA_HANDLER.fmg( 673,1822):�
      if(L0_om) then
         I_ili=I0_im
      endif
C LODOCHKA_HANDLER.fmg( 701,1825):���� � ������������� �������
      L0_ote=I_ili.eq.I0_ute
C LODOCHKA_HANDLER.fmg( 660,1684):���������� �������������
      L0_ore = L0_ote.AND.L_ave
C LODOCHKA_HANDLER.fmg( 668,1684):�
      if(L0_ore) then
         I_ili=I0_ire
      endif
C LODOCHKA_HANDLER.fmg( 683,1688):���� � ������������� �������
      L0_eve=I_ili.eq.I0_ive
C LODOCHKA_HANDLER.fmg( 660,1694):���������� �������������
      L0_ase = L0_eve.AND.L_ove
C LODOCHKA_HANDLER.fmg( 668,1694):�
      if(L0_ase) then
         I_ili=I0_ure
      endif
C LODOCHKA_HANDLER.fmg( 683,1698):���� � ������������� �������
      L0_of=I_ili.eq.I0_uf
C LODOCHKA_HANDLER.fmg( 660,1812):���������� �������������
      L0_am = L0_of.AND.L_em
C LODOCHKA_HANDLER.fmg( 673,1812):�
      if(L0_am) then
         I_ili=I0_ul
      endif
C LODOCHKA_HANDLER.fmg( 701,1815):���� � ������������� �������
      L0_ef=I_ili.eq.I0_if
C LODOCHKA_HANDLER.fmg( 660,1802):���������� �������������
      L0_il = L0_ef.AND.L_ol
C LODOCHKA_HANDLER.fmg( 673,1802):�
      if(L0_il) then
         I_ili=I0_el
      endif
C LODOCHKA_HANDLER.fmg( 701,1805):���� � ������������� �������
      L0_uve=I_ili.eq.I0_axe
C LODOCHKA_HANDLER.fmg( 660,1704):���������� �������������
      L0_ise = L0_uve.AND.L_exe
C LODOCHKA_HANDLER.fmg( 668,1703):�
      if(L0_ise) then
         I_ili=I0_ese
      endif
C LODOCHKA_HANDLER.fmg( 683,1708):���� � ������������� �������
      L0_ipe=I_ili.eq.I0_ope
C LODOCHKA_HANDLER.fmg( 660,1664):���������� �������������
      L0_epe = L0_ipe.AND.L_upe
C LODOCHKA_HANDLER.fmg( 668,1662):�
      L0_ime = L0_epe.AND.L0_ome
C LODOCHKA_HANDLER.fmg( 674,1658):�
      if(L0_ime) then
         I_ili=I0_eme
      endif
C LODOCHKA_HANDLER.fmg( 685,1662):���� � ������������� �������
      L0_ake = L0_epe.AND.L0_eke
C LODOCHKA_HANDLER.fmg( 674,1618):�
      if(L0_ake) then
         I_ili=I0_ufe
      endif
C LODOCHKA_HANDLER.fmg( 685,1624):���� � ������������� �������
      L0_efe = L0_epe.AND.L0_ife
C LODOCHKA_HANDLER.fmg( 674,1606):�
      if(L0_efe) then
         I_ili=I0_afe
      endif
C LODOCHKA_HANDLER.fmg( 685,1610):���� � ������������� �������
      L0_ole = L0_epe.AND.L0_ule
C LODOCHKA_HANDLER.fmg( 674,1644):�
      if(L0_ole) then
         I_ili=I0_ile
      endif
C LODOCHKA_HANDLER.fmg( 685,1650):���� � ������������� �������
      L0_uke = L0_epe.AND.L0_ale
C LODOCHKA_HANDLER.fmg( 674,1632):�
      if(L0_uke) then
         I_ili=I0_oke
      endif
C LODOCHKA_HANDLER.fmg( 685,1636):���� � ������������� �������
      L0_u=I_ili.eq.I0_o
C LODOCHKA_HANDLER.fmg( 342,1965):���������� �������������
      if(L0_u) then
         R_amo=R_ad
      endif
C LODOCHKA_HANDLER.fmg( 373,1970):���� � ������������� �������
      R0_ilo = R_amo
C LODOCHKA_HANDLER.fmg( 468,1432):��������
C label 986  try986=try986-1
      R0_ulo = R0_ilo + R_elo
C LODOCHKA_HANDLER.fmg( 478,1431):��������
      if(L0_olo) then
         R_amo=R0_ulo
      endif
C LODOCHKA_HANDLER.fmg( 488,1430):���� � ������������� �������
C sav1=R0_ilo
      R0_ilo = R_amo
C LODOCHKA_HANDLER.fmg( 468,1432):recalc:��������
C if(sav1.ne.R0_ilo .and. try986.gt.0) goto 986
      End
