      Interface
      Subroutine LODOCHKA_HANDLER_IT_MAIN(ext_deltat,R_utid
     &,R_ed,L_ef,L_al,L_el,L_ul,L_im,L_up,R_or,R_ur,L_as,L_ut
     &,L_ev,L_iv,R_ax,R_ex,L_ix,L_ux,R_ide,R_ode,L_ude,I_ufe
     &,I_oke,I_ile,I_eme,I_ape,I_epe,L_are,L_ote,L_eve,L_uve
     &,L_ixe,L_abi,L_obi,L_edi,L_ifi,L_eki,L_ali,I_oli,I_uli
     &,R_umi,R_upi,R_ari,R_asi,R_osi,R_iti,R_uvi,R_oxi,R_abo
     &,R_ebo,R_ado,R_ido,R_odo,L_udo,L_uko,L_alo,R_ilo,R_emo
     &,R_umo,R_opo,R_iro,R_eso,R_iso,R_avo,R_evo,R_exo,R_abu
     &,R_ibu,R_aku,R_eku,L_iku,R_elu,R_emu,R_epu,L_oru,R_uru
     &,R_osu,R_itu,R_otu,R_exu,R_ixu,L_oxu,R_ibad,R_idad,R_ifad
     &,R_okad,R_ilad,R_emad,R_imad,L_upad,L_erad,L_orad,R_urad
     &,L_asad,R_esad,R_utad,R_ovad,R_ixad,R_oxad,L_aded,L_oded
     &,L_afed,R_efed,L_ifed,R_ofed,R_iked,R_emed,R_eped,R_iped
     &,R_oped,L_ased,L_osed,L_used,L_eted,L_aved,L_ived,R_oxed
     &,R_uxed,R_ubid,R_adid,L_idid,R_ifid,I_usid,R_itid,R_otid
     &,R_ivid,R_uvid,C20_axid)
C |R_utid        |4 4 I|20FDB50AE400VX02|�������� �������� �������||
C |R_ed          |4 4 I|FDB40_BOAT_MASS |����� ����� � �����������||
C |L_ef          |1 1 I|FDB60_LOD_INPUT_GATEWAY_FROM|������� �� �������� �����||
C |L_al          |1 1 I|FDB60_LOD_TRANSFER_TABLE|������� �� ������������ �������||
C |L_el          |1 1 I|FDB60_LOD_MAIN_TROLLEY|������� �� �������||
C |L_ul          |1 1 I|FDB60_LOD_INPUT_GATEWAY|������� �� �������� �����||
C |L_im          |1 1 I|FDB60_LOD_VZV   |������� �� ������������||
C |L_up          |1 1 I|FDB60_LOD_VZVESH|������� �� ������������ �������������� �����||
C |R_or          |4 4 S|_simpJ10082*    |[���]���������� ��������� ������������� |0.0|
C |R_ur          |4 4 K|_timpJ10082     |[���]������������ �������� �������������|1.0|
C |L_as          |1 1 S|_limpJ10082*    |[TF]���������� ��������� ������������� |F|
C |L_ut          |1 1 I|FDB40_START     |������ ������� �����������||
C |L_ev          |1 1 I|FDB91EC001_INIT_COORD|������������� ���������� ������� ��� ��. �����������||
C |L_iv          |1 1 I|FDB91EC003_INIT_COORD|������������� ���������� ������� ��� ��. ���||
C |R_ax          |4 4 S|_simpJ10030*    |[���]���������� ��������� ������������� |0.0|
C |R_ex          |4 4 K|_timpJ10030     |[���]������������ �������� �������������|1.0|
C |L_ix          |1 1 S|_limpJ10030*    |[TF]���������� ��������� ������������� |F|
C |L_ux          |1 1 I|FDB91EC005_INIT_COORD|������������� ���������� ������� ��� ��. ���������||
C |R_ide         |4 4 S|_simpJ10001*    |[���]���������� ��������� ������������� |0.0|
C |R_ode         |4 4 K|_timpJ10001     |[���]������������ �������� �������������|1.0|
C |L_ude         |1 1 S|_limpJ10001*    |[TF]���������� ��������� ������������� |F|
C |I_ufe         |2 4 K|_lcmpJ9990      |�������� ������ �����������|5|
C |I_oke         |2 4 K|_lcmpJ9978      |�������� ������ �����������|4|
C |I_ile         |2 4 K|_lcmpJ9966      |�������� ������ �����������|3|
C |I_eme         |2 4 K|_lcmpJ9954      |�������� ������ �����������|2|
C |I_ape         |2 4 K|_lcmpJ9942      |�������� ������ �����������|1|
C |I_epe         |2 4 I|FDB60AE408_NUM_BOAT|����� ��������� ������� �� �������||
C |L_are         |1 1 I|FDB60AE408_CATCH|������ ������� 5��� �������� 20FDB60AE408||
C |L_ote         |1 1 I|FDB61AE401_UNLOAD5|������ ������� ����� 20FDB61AE401||
C |L_eve         |1 1 I|FDB61AE401_UNLOAD4|������ ������� ����� 20FDB61AE401||
C |L_uve         |1 1 I|FDB61AE401_UNLOAD3|������ ������� ����� 20FDB61AE401||
C |L_ixe         |1 1 I|FDB61AE401_UNLOAD2|������ ������� ����� 20FDB61AE401||
C |L_abi         |1 1 I|FDB61AE401_UNLOAD1|������ ������� ����� 20FDB61AE401||
C |L_obi         |1 1 I|FDB61AE401_CATCH5|������ ������� ����� 20FDB61AE401||
C |L_edi         |1 1 I|FDB61AE401_CATCH4|������ ������� ����� 20FDB61AE401||
C |L_ifi         |1 1 I|FDB61AE401_CATCH3|������ ������� ����� 20FDB61AE401||
C |L_eki         |1 1 I|FDB61AE401_CATCH2|������ ������� ����� 20FDB61AE401||
C |L_ali         |1 1 I|FDB61AE401_CATCH1|������ ������� ����� 20FDB61AE401||
C |I_oli         |2 4 O|CR              |�������������� � �������������||
C |I_uli         |2 4 O|LP              |����� �� ������� ����������� �������||
C |R_umi         |4 4 K|_lcmpJ9186      |[]�������� ������ �����������|20|
C |R_upi         |4 4 I|FDB60AE500AVY01 |�������� ��������� 20FDB60AE500A||
C |R_ari         |4 4 K|_lcmpJ9173      |[]�������� ������ �����������|20|
C |R_asi         |4 4 I|FDB60AE500BVY01 |�������� ��������� 20FDB60AE500B||
C |R_osi         |4 4 I|FDB60AE500�VY01 |�������� ��������� 20FDB60AE500�||
C |R_iti         |4 4 K|_lcmpJ9156      |[]�������� ������ �����������|20|
C |R_uvi         |4 4 K|_lcmpJ9127      |[]�������� ������ �����������|20|
C |R_oxi         |4 4 K|_lcmpJ9057      |[]�������� ������ �����������|2|
C |R_abo         |4 4 I|FDB60AE403_VZ01 |�������� �������������� �������||
C |R_ebo         |4 4 K|_lcmpJ9051      |[]�������� ������ �����������|20|
C |R_ado         |4 4 I|FDB60AE403_POS  |��������� ������� �� ��� X||
C |R_ido         |4 4 S|_simpJ9036*     |[���]���������� ��������� ������������� |0.0|
C |R_odo         |4 4 K|_timpJ9036      |[���]������������ �������� �������������|1.0|
C |L_udo         |1 1 S|_limpJ9036*     |[TF]���������� ��������� ������������� |F|
C |L_uko         |1 1 S|_splsJ8856*     |[TF]���������� ��������� ������������� |F|
C |L_alo         |1 1 I|FDB40_HAVE_DELTA_MASS|���� ��������� ����� �������||
C |R_ilo         |4 4 I|FDB40_DELTA_MASS|��������� ����� �������||
C |R_emo         |4 4 O|MASS            |����� ����� ����������� � �������||
C |R_umo         |4 4 K|_lcmpJ8516      |[]�������� ������ �����������|20|
C |R_opo         |4 4 K|_lcmpJ8503      |[]�������� ������ �����������|20|
C |R_iro         |4 4 K|_lcmpJ8495      |[]�������� ������ �����������|20|
C |R_eso         |4 4 I|FDB91AE001KE01_POS|��������� �1||
C |R_iso         |4 4 K|_lcmpJ8485      |[]�������� ������ �����������|20|
C |R_avo         |4 4 I|FDB91AE001KE01_V01|������� �1 �� ��� X||
C |R_evo         |4 4 I|FDB91AE003KE01_V01|�������� 20FDB91AE003KE01||
C |R_exo         |4 4 K|_lcmpJ8465      |[]�������� ������ �����������|20|
C |R_abu         |4 4 I|FDB91AE003KE01_POS|��������� �� 20FDB91AE003KE01||
C |R_ibu         |4 4 K|_lcmpJ8453      |[]�������� ������ �����������|20|
C |R_aku         |4 4 S|_simpJ8368*     |[���]���������� ��������� ������������� |0.0|
C |R_eku         |4 4 K|_timpJ8368      |[���]������������ �������� �������������|1.0|
C |L_iku         |1 1 S|_limpJ8368*     |[TF]���������� ��������� ������������� |F|
C |R_elu         |4 4 K|_lcmpJ8358      |[]�������� ������ �����������|20|
C |R_emu         |4 4 K|_lcmpJ8350      |[]�������� ������ �����������|20|
C |R_epu         |4 4 K|_lcmpJ8342      |[]�������� ������ �����������|20|
C |L_oru         |1 1 O|FDB91AE012_CATCHED|������� ��������� �� 20FDB91AE012||
C |R_uru         |4 4 K|_lcmpJ8317      |[]�������� ������ �����������|20|
C |R_osu         |4 4 K|_lcmpJ8308      |[]�������� ������ �����������|20|
C |R_itu         |4 4 I|FDB91AE012_POS  |��������� �4||
C |R_otu         |4 4 K|_lcmpJ8297      |[]�������� ������ �����������|20|
C |R_exu         |4 4 S|_simpJ7966*     |[���]���������� ��������� ������������� |0.0|
C |R_ixu         |4 4 K|_timpJ7966      |[���]������������ �������� �������������|1.0|
C |L_oxu         |1 1 S|_limpJ7966*     |[TF]���������� ��������� ������������� |F|
C |R_ibad        |4 4 K|_lcmpJ7929      |[]�������� ������ �����������|20|
C |R_idad        |4 4 K|_lcmpJ7921      |[]�������� ������ �����������|20|
C |R_ifad        |4 4 K|_lcmpJ7913      |[]�������� ������ �����������|20|
C |R_okad        |4 4 K|_lcmpJ7509      |[]�������� ������ �����������|20|
C |R_ilad        |4 4 K|_lcmpJ7500      |[]�������� ������ �����������|20|
C |R_emad        |4 4 I|FDB91AE007_POS  |��������� �4||
C |R_imad        |4 4 K|_lcmpJ7487      |[]�������� ������ �����������|20|
C |L_upad        |1 1 O|FDB91AE007_CATCHED|������� ��������� �4||
C |L_erad        |1 1 S|_qffJ7476*      |�������� ������ Q RS-��������  |F|
C |L_orad        |1 1 I|FDB91AE007_UNCATCH|��������� ������� �4||
C |R_urad        |4 4 I|FDB91AE007_VZ01 |������� �4 �� ��� Z||
C |L_asad        |1 1 I|FDB91AE007_CATCH|������ ������� �4||
C |R_esad        |4 4 K|_lcmpJ7170      |[]�������� ������ �����������|20|
C |R_utad        |4 4 K|_lcmpJ6848      |[]�������� ������ �����������|20|
C |R_ovad        |4 4 K|_lcmpJ6839      |[]�������� ������ �����������|20|
C |R_ixad        |4 4 I|FDB91AE006_POS  |��������� �� 20FDB91AE006||
C |R_oxad        |4 4 K|_lcmpJ6826      |[]�������� ������ �����������|20|
C |L_aded        |1 1 O|FDB91AE006_CATCHED|������� ��������� �� 20FDB91AE006||
C |L_oded        |1 1 S|_qffJ6815*      |�������� ������ Q RS-��������  |F|
C |L_afed        |1 1 I|FDB91AE006_UNCATCH|��������� ������� �� 20FDB91AE006||
C |R_efed        |4 4 I|FDB91AE006_VZ01 |������� �� �� ��� Z||
C |L_ifed        |1 1 I|FDB91AE006_CATCH|������ ������� �� 20FDB91AE006||
C |R_ofed        |4 4 K|_lcmpJ6805      |[]�������� ������ �����������|20|
C |R_iked        |4 4 K|_lcmpJ6796      |[]�������� ������ �����������|20|
C |R_emed        |4 4 K|_lcmpJ6476      |[]�������� ������ �����������|20|
C |R_eped        |4 4 K|_lcmpJ6463      |[]�������� ������ �����������|1000|
C |R_iped        |4 4 I|FDB91AE014_POS  |��������� �� 20FDB91AE014||
C |R_oped        |4 4 K|_lcmpJ6181      |[]�������� ������ �����������|20|
C |L_ased        |1 1 O|FDB91AE014_CATCHED|������� ��������� �� 20FDB91AE014||
C |L_osed        |1 1 I|FDB91AE012_UNCATCH|��������� ������� �� 20FDB91AE012||
C |L_used        |1 1 I|FDB91AE012_CATCH|������ ������� �� 20FDB91AE012||
C |L_eted        |1 1 S|_qffJ6163*      |�������� ������ Q RS-��������  |F|
C |L_aved        |1 1 S|_qffJ6151*      |�������� ������ Q RS-��������  |F|
C |L_ived        |1 1 I|FDB91AE014_UNCATCH|��������� ������� �� 20FDB91AE014||
C |R_oxed        |4 4 O|VZ01            |��������� ������� �� ��� Z||
C |R_uxed        |4 4 S|_ointJ6126*     |����� ����������� |0.0|
C |R_ubid        |4 4 I|FDB91AE012_VZ01 |������� �� �� ��� Z||
C |R_adid        |4 4 I|FDB91AE014_VZ01 |������� �� �� ��� Z||
C |L_idid        |1 1 I|FDB91AE014_CATCH|������ ������� �� 20FDB91AE014||
C |R_ifid        |4 4 S|_ointJ5958*     |����� ����������� |`p_LOAD_Y`|
C |I_usid        |2 4 O|LINE_POS        |����� �� ������� ����������� �������||
C |R_itid        |4 4 O|VX01            |��������� ������� �� ��� X||
C |R_otid        |4 4 S|_ointJ5909*     |����� ����������� |`p_LOAD_X`|
C |R_ivid        |4 4 K|_lcmpJ871       |[]�������� ������ �����������|1.0|
C |R_uvid        |4 4 O|VY01            |��������� ������� �� ��� Y||
C |C20_axid      |3 20 O|SECTION         |������� ������� �������||

      IMPLICIT NONE
      REAL*4 ext_deltat
      REAL*4 R_ed
      LOGICAL*1 L_ef,L_al,L_el,L_ul,L_im,L_up
      REAL*4 R_or,R_ur
      LOGICAL*1 L_as,L_ut,L_ev,L_iv
      REAL*4 R_ax,R_ex
      LOGICAL*1 L_ix,L_ux
      REAL*4 R_ide,R_ode
      LOGICAL*1 L_ude
      INTEGER*4 I_ufe,I_oke,I_ile,I_eme,I_ape,I_epe
      LOGICAL*1 L_are,L_ote,L_eve,L_uve,L_ixe,L_abi,L_obi
     &,L_edi,L_ifi,L_eki,L_ali
      INTEGER*4 I_oli,I_uli
      REAL*4 R_umi,R_upi,R_ari,R_asi,R_osi,R_iti,R_uvi,R_oxi
     &,R_abo,R_ebo,R_ado,R_ido,R_odo
      LOGICAL*1 L_udo,L_uko,L_alo
      REAL*4 R_ilo,R_emo,R_umo,R_opo,R_iro,R_eso,R_iso,R_avo
     &,R_evo,R_exo,R_abu,R_ibu,R_aku,R_eku
      LOGICAL*1 L_iku
      REAL*4 R_elu,R_emu,R_epu
      LOGICAL*1 L_oru
      REAL*4 R_uru,R_osu,R_itu,R_otu,R_exu,R_ixu
      LOGICAL*1 L_oxu
      REAL*4 R_ibad,R_idad,R_ifad,R_okad,R_ilad,R_emad,R_imad
      LOGICAL*1 L_upad,L_erad,L_orad
      REAL*4 R_urad
      LOGICAL*1 L_asad
      REAL*4 R_esad,R_utad,R_ovad,R_ixad,R_oxad
      LOGICAL*1 L_aded,L_oded,L_afed
      REAL*4 R_efed
      LOGICAL*1 L_ifed
      REAL*4 R_ofed,R_iked,R_emed,R_eped,R_iped,R_oped
      LOGICAL*1 L_ased,L_osed,L_used,L_eted,L_aved,L_ived
      REAL*4 R_oxed,R_uxed,R_ubid,R_adid
      LOGICAL*1 L_idid
      REAL*4 R_ifid
      INTEGER*4 I_usid
      REAL*4 R_itid,R_otid,R_utid,R_ivid,R_uvid
      CHARACTER*20 C20_axid
      End subroutine LODOCHKA_HANDLER_IT_MAIN
      End interface
