      Interface
      Subroutine TOLKATEL_OY_HANDLER(ext_deltat,R_abi,R_e
     &,R_ed,R_ef,R_of,R_uf,R_ak,R_ek,L_up,R_as,R_ex,L_ix,R_ux
     &,L_abe,L_ede,L_ude,R_uke,R_ole,L_ame,R_ime,L_ume,R_ape
     &,R_epe,L_ope,L_ore,L_ase,L_ese,L_ose,L_ete,L_ute,L_eve
     &,R_ebi,L_ubi,L_edi,L_odi,L_udi,L_ifi,L_ofi,C20_uli,L_ami
     &,L_imi,L_omi,L_epi,L_ipi,L_opi,L_upi,L_ari,L_eri,L_iri
     &,L_ori,L_uri,L_asi,L_esi,L_isi,L_osi,L_usi,L_ati,L_eti
     &,L_iti,R_oti,R_uti,R_avi,R_evi,R_ivi,I_ovi,R_ixi,R_uxi
     &,R_abo,L_obo,R_ado,C20_iko,C20_ilo,I_emo,I_umo,R_apo
     &,R_epo,R_ipo,R_opo,L_upo,L_aro,I_ero,I_uro,I_uso,I_ito
     &,C20_ivo,C8_ixo,R_afu,R_efu,L_ifu,R_ofu,R_ufu,I_aku
     &,L_oku,L_uku,I_elu,I_ulu,L_umu,L_epu,L_ipu,L_opu,L_upu
     &,L_aru,L_asu,L_atu,L_itu,L_otu,R8_avu,R_uvu,R8_ixu,L_abad
     &,L_obad,I_ubad,L_idad,L_odad,L_ofad,L_elad,L_emad)
C |R_abi         |4 4 I|11 mlfpar19     ||0.6|
C |R_e           |4 4 S|_slpbJ3665*     |���������� ��������� ||
C |R_ed          |4 4 I|VX01            |||
C |R_ef          |4 4 I|p_TOLKATEL_XH54 |||
C |R_of          |4 4 S|_slpbJ3618*     |���������� ��������� ||
C |R_uf          |4 4 S|_slpbJ3617*     |���������� ��������� ||
C |R_ak          |4 4 S|_slpbJ3616*     |���������� ��������� ||
C |R_ek          |4 4 S|_slpbJ3610*     |���������� ��������� ||
C |L_up          |1 1 S|_qffJ3566*      |�������� ������ Q RS-��������  |F|
C |R_as          |4 4 I|mlfpar19        |��������� ������������|0.6|
C |R_ex          |4 4 S|_sdelnv2dyn$100Dasha*|[���]���������� ��������� |0|
C |L_ix          |1 1 S|_idelnv2dyn$100Dasha*|[TF]���������� ��������� �������� ������ |f|
C |R_ux          |4 4 S|_sdelnv2dyn$99Dasha*|[���]���������� ��������� |0|
C |L_abe         |1 1 S|_idelnv2dyn$99Dasha*|[TF]���������� ��������� �������� ������ |f|
C |L_ede         |1 1 S|_qff156*        |�������� ������ Q RS-��������  |F|
C |L_ude         |1 1 S|_qff155*        |�������� ������ Q RS-��������  |F|
C |R_uke         |4 4 I|value_pos       |��� �������� ��������|0.5|
C |R_ole         |4 4 S|_sdelnv2dyn$83Dasha*|[���]���������� ��������� |0|
C |L_ame         |1 1 S|_idelnv2dyn$83Dasha*|[TF]���������� ��������� �������� ������ |f|
C |R_ime         |4 4 S|_sdelnv2dyn$82Dasha*|[���]���������� ��������� |0|
C |L_ume         |1 1 S|_idelnv2dyn$82Dasha*|[TF]���������� ��������� �������� ������ |f|
C |R_ape         |4 4 I|tpos1           |����� ���� � ��������� 1|30.0|
C |R_epe         |4 4 I|tpos2           |����� ���� � ��������� 2|30.0|
C |L_ope         |1 1 S|_qffnv2dyn$106Dasha*|�������� ������ Q RS-��������  |F|
C |L_ore         |1 1 S|_qffnv2dyn$104Dasha*|�������� ������ Q RS-��������  |F|
C |L_ase         |1 1 S|_qffnv2dyn$105Dasha*|�������� ������ Q RS-��������  |F|
C |L_ese         |1 1 I|OUTC            |�����||
C |L_ose         |1 1 S|_qffnv2dyn$103Dasha*|�������� ������ Q RS-��������  |F|
C |L_ete         |1 1 S|_qffnv2dyn$102Dasha*|�������� ������ Q RS-��������  |F|
C |L_ute         |1 1 S|_qffnv2dyn$101Dasha*|�������� ������ Q RS-��������  |F|
C |L_eve         |1 1 I|OUTO            |������||
C |R_ebi         |4 4 O|mf_value        |��� ��������� �����|0.5|
C |L_ubi         |1 1 S|_qffnv2dyn$87Dasha*|�������� ������ Q RS-��������  |F|
C |L_edi         |1 1 S|_qffnv2dyn$86Dasha*|�������� ������ Q RS-��������  |F|
C |L_odi         |1 1 S|_qffnv2dyn$96Dasha*|�������� ������ Q RS-��������  |F|
C |L_udi         |1 1 S|_qffnv2dyn$95Dasha*|�������� ������ Q RS-��������  |F|
C |L_ifi         |1 1 O|limit_switch_error|||
C |L_ofi         |1 1 O|flag_mlf19      |||
C |C20_uli       |3 20 O|task_state      |���������||
C |L_ami         |1 1 I|vlv_kvit        |||
C |L_imi         |1 1 I|instr_fault     |||
C |L_omi         |1 1 S|_qffJ3139*      |�������� ������ Q RS-��������  |F|
C |L_epi         |1 1 O|norm            |�����||
C |L_ipi         |1 1 I|mlf17           |������ ���������������� ����� �����||
C |L_opi         |1 1 I|mlf16           |������ ������������ ����� �����||
C |L_upi         |1 1 I|mlf15           |������ ���������������� ����� ������||
C |L_ari         |1 1 I|mlf14           |������ ������������ ����� ������||
C |L_eri         |1 1 I|mlf07           |���������� ���� �������||
C |L_iri         |1 1 I|mlf28           |������ ���������������� ��������� �����||
C |L_ori         |1 1 I|mlf09           |����� ��������� �����||
C |L_uri         |1 1 I|mlf08           |����� ��������� ������||
C |L_asi         |1 1 I|mlf26           |������ ���������������� ��������� ������||
C |L_esi         |1 1 I|mlf27           |������ ������������ ��������� �����||
C |L_isi         |1 1 I|mlf25           |������ ������������ ��������� ������||
C |L_osi         |1 1 I|mlf23           |������� ������� �����||
C |L_usi         |1 1 I|mlf22           |����� ����� ��������||
C |L_ati         |1 1 I|mlf19           |���� ������������||
C |L_eti         |1 1 I|YA26C           |������� ������� �� ���������� (��)|F|
C |L_iti         |1 1 I|YA25C           |������� ������� �� ����������|F|
C |R_oti         |4 4 I|tcl_top         |�������� �������|20|
C |R_uti         |4 4 K|_uintT_INT_1    |����������� ������ ����������� ������|`p_TOLKATEL_UP`|
C |R_avi         |4 4 K|_tintT_INT_1    |[���]�������� T �����������|1|
C |R_evi         |4 4 O|_ointT_INT_1*   |�������� ������ ����������� |0|
C |R_ivi         |4 4 K|_lintT_INT_1    |����������� ������ ����������� �����|0.0|
C |I_ovi         |2 4 O|LWORK           |����� �������||
C |R_ixi         |4 4 K|_lcmpJ2850      |[]�������� ������ �����������|`p_TOLKATEL_XH54`|
C |R_uxi         |4 4 K|_lcmpJ2849      |[]�������� ������ �����������|1.0|
C |R_abo         |4 4 O|VY01            |��������� ��������� �� ��� X||
C |L_obo         |1 1 I|mlf04           |���������������� �������� �����||
C |R_ado         |4 4 O|VY02            |�������� ����������� ���������||
C |C20_iko       |3 20 O|task_name3      |������� ��������� ��� ������� ������������ ��������||
C |C20_ilo       |3 20 O|task_name2      |������� ��������� ��� ������� ������������ ��������||
C |I_emo         |2 4 O|LREADY          |����� ����������||
C |I_umo         |2 4 O|LBUSY           |����� �����||
C |R_apo         |4 4 S|vminit_button_ST*|��������� ������ "������� � �������� �� ���������" |0.0|
C |R_epo         |4 4 I|vminit_button   |������� ������ ������ "������� � �������� �� ���������" |0.0|
C |R_ipo         |4 4 S|vmwork_button_ST*|��������� ������ "������� � ������� �� ���������" |0.0|
C |R_opo         |4 4 I|vmwork_button   |������� ������ ������ "������� � ������� �� ���������" |0.0|
C |L_upo         |1 1 I|YA25            |������� ������� �� ����������|F|
C |L_aro         |1 1 I|YA26            |������� ������� �� ����������|F|
C |I_ero         |2 4 O|state1          |��������� 1||
C |I_uro         |2 4 O|state2          |��������� 2||
C |I_uso         |2 4 O|LWORKO          |����� � �������||
C |I_ito         |2 4 O|LINITC          |����� � ��������||
C |C20_ivo       |3 20 O|task_name       |������� ���������||
C |C8_ixo        |3 8 I|task            |������� ���������||
C |R_afu         |4 4 S|vmstop_button_ST*|��������� ������ "������� ���� �� ���������" |0.0|
C |R_efu         |4 4 I|vmstop_button   |������� ������ ������ "������� ���� �� ���������" |0.0|
C |L_ifu         |1 1 O|vmstart_button_CMD*|[TF]����� ������ ������� �������� ��������� �� ���������|F|
C |R_ofu         |4 4 S|vmstart_button_ST*|��������� ������ "������� �������� ��������� �� ���������" |0.0|
C |R_ufu         |4 4 I|vmstart_button  |������� ������ ������ "������� �������� ��������� �� ���������" |0.0|
C |I_aku         |2 4 O|LERROR          |����� �������������||
C |L_oku         |1 1 I|stop_avt        |������� ���� �� ����������|F|
C |L_uku         |1 1 O|vmstop_button_CMD*|[TF]����� ������ ������� ���� �� ���������|F|
C |I_elu         |2 4 O|LINIT           |����� ��������||
C |I_ulu         |2 4 O|LZM             |������ "�������"||
C |L_umu         |1 1 O|vminit_button_CMD*|[TF]����� ������ ������� � �������� �� ���������|F|
C |L_epu         |1 1 O|vmwork_button_CMD*|[TF]����� ������ ������� � ������� �� ���������|F|
C |L_ipu         |1 1 I|mlf06           |������������� ������� "�����"||
C |L_opu         |1 1 I|mlf05           |������������� ������� "������"||
C |L_upu         |1 1 I|ulubopen        |���������� ������� �� ���������|F|
C |L_aru         |1 1 I|ulubclose       |���������� ������� �� ���������|F|
C |L_asu         |1 1 O|block           |||
C |L_atu         |1 1 S|_qff10*         |�������� ������ Q RS-��������  |F|
C |L_itu         |1 1 O|STOP            |�������||
C |L_otu         |1 1 O|nopower         |��� ����������||
C |R8_avu        |4 8 I|voltage         |[��]���������� �� ������||
C |R_uvu         |4 4 I|power           |�������� ��������||
C |R8_ixu        |4 8 O|sn_power        |�������� ���� ��|3.0|
C |L_abad        |1 1 I|mlf03           |���������������� �������� ������||
C |L_obad        |1 1 O|fault           |�������������||
C |I_ubad        |2 4 O|LAM             |������ "�������"||
C |L_idad        |1 1 O|XH53            |�� ����� (���)|F|
C |L_odad        |1 1 O|XH54            |�� ������ (���)|F|
C |L_ofad        |1 1 S|_qff2*          |�������� ������ Q RS-��������  |F|
C |L_elad        |1 1 S|_qff1*          |�������� ������ Q RS-��������  |F|
C |L_emad        |1 1 I|mlf24           |��� ���������� � ����������||

      IMPLICIT NONE
      REAL*4 ext_deltat
      REAL*4 R_e,R_ed,R_ef,R_of,R_uf,R_ak,R_ek
      LOGICAL*1 L_up
      REAL*4 R_as,R_ex
      LOGICAL*1 L_ix
      REAL*4 R_ux
      LOGICAL*1 L_abe,L_ede,L_ude
      REAL*4 R_uke,R_ole
      LOGICAL*1 L_ame
      REAL*4 R_ime
      LOGICAL*1 L_ume
      REAL*4 R_ape,R_epe
      LOGICAL*1 L_ope,L_ore,L_ase,L_ese,L_ose,L_ete,L_ute
     &,L_eve
      REAL*4 R_abi,R_ebi
      LOGICAL*1 L_ubi,L_edi,L_odi,L_udi,L_ifi,L_ofi
      CHARACTER*20 C20_uli
      LOGICAL*1 L_ami,L_imi,L_omi,L_epi,L_ipi,L_opi,L_upi
     &,L_ari,L_eri,L_iri,L_ori,L_uri,L_asi,L_esi,L_isi,L_osi
     &,L_usi,L_ati,L_eti,L_iti
      REAL*4 R_oti,R_uti,R_avi,R_evi,R_ivi
      INTEGER*4 I_ovi
      REAL*4 R_ixi,R_uxi,R_abo
      LOGICAL*1 L_obo
      REAL*4 R_ado
      CHARACTER*20 C20_iko,C20_ilo
      INTEGER*4 I_emo,I_umo
      REAL*4 R_apo,R_epo,R_ipo,R_opo
      LOGICAL*1 L_upo,L_aro
      INTEGER*4 I_ero,I_uro,I_uso,I_ito
      CHARACTER*20 C20_ivo
      CHARACTER*8 C8_ixo
      REAL*4 R_afu,R_efu
      LOGICAL*1 L_ifu
      REAL*4 R_ofu,R_ufu
      INTEGER*4 I_aku
      LOGICAL*1 L_oku,L_uku
      INTEGER*4 I_elu,I_ulu
      LOGICAL*1 L_umu,L_epu,L_ipu,L_opu,L_upu,L_aru,L_asu
     &,L_atu,L_itu,L_otu
      REAL*8 R8_avu
      REAL*4 R_uvu
      REAL*8 R8_ixu
      LOGICAL*1 L_abad,L_obad
      INTEGER*4 I_ubad
      LOGICAL*1 L_idad,L_odad,L_ofad,L_elad,L_emad
      End subroutine TOLKATEL_OY_HANDLER
      End interface
