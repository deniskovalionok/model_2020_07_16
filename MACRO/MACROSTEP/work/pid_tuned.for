      Subroutine pid_tuned(ext_deltat,L_e,R8_id,R_ef,R_ak
     &,R_ok,R_uk,R_al,R_el,R_il,R_ol,R_ul,R_ep,R_ar)
C |L_e           |1 1 I|flagmod         |����� ������� �����||
C |R8_id         |4 8 O|out             |����� ��������� ��������||
C |R_ef          |4 4 I|k4              |�-� ����������� ��||
C |R_ak          |4 4 I|t3              |���������� ������� ���������������||
C |R_ok          |4 4 S|_sdif1*         |���������� ��������� |0.0|
C |R_uk          |4 4 I|k3              |�-� ���������������||
C |R_al          |4 4 I|u1              |����������� �����������||
C |R_el          |4 4 I|k2              |�-� ������������||
C |R_il          |4 4 O|dlt             |������� ��������� ����������||
C |R_ol          |4 4 I|k1              |�-� ����������������||
C |R_ul          |4 4 O|_odif1*         |�������� ������ |0.0|
C |R_ep          |4 4 S|_oint1*         |����� ����������� |0.0|
C |R_ar          |4 4 I|delta           |������� ��������� ����������||

      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      LOGICAL*1 L_e,L0_i
      REAL*4 R0_o
      LOGICAL*1 L0_u
      REAL*4 R0_ad,R0_ed
      REAL*8 R8_id
      REAL*4 R0_od,R0_ud,R0_af,R_ef,R0_if,R0_of,R0_uf,R_ak
      LOGICAL*1 L0_ek
      REAL*4 R0_ik,R_ok,R_uk,R_al,R_el,R_il,R_ol,R_ul,R0_am
     &,R0_em,R0_im,R0_om
      LOGICAL*1 L0_um,L0_ap
      REAL*4 R_ep,R0_ip,R0_op,R0_up,R_ar

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      R0_ik=R_ok
C pid_tuned.fmg( 243, 164):pre: ���������������� ����� ,1
      R0_im = 100
C pid_tuned.fmg( 233, 210):��������� (RE4) (�������)
      R0_ip = -(R0_im)
C pid_tuned.fmg( 244, 203):��������
      R0_o = 0.001
C pid_tuned.fmg( 273, 178):��������� (RE4) (�������)
      R0_ad = 0.999
C pid_tuned.fmg( 273, 182):��������� (RE4) (�������)
      R0_ed = 0.000001
C pid_tuned.fmg( 200, 200):��������� (RE4) (�������)
      L0_ek=.false.
C pid_tuned.fmg( 237, 159):��������� ���������� (�������)
      L0_um=.false.
C pid_tuned.fmg( 236, 187):��������� ���������� (�������)
      R0_od = 1
C pid_tuned.fmg( 261, 193):��������� (RE4) (�������)
      R0_ud = 0
C pid_tuned.fmg( 261, 187):��������� (RE4) (�������)
      R0_up = 0
C pid_tuned.fmg( 237, 203):��������� (RE4) (�������)
      R0_om = 1
C pid_tuned.fmg( 235, 198):��������� (RE4) (�������)
      R0_am = R_ol * R_il
C pid_tuned.fmg( 200, 219):����������
C label 19  try19=try19-1
      if(R_il.gt.R0_ed) then
         R0_uf=R_il-R0_ed
      elseif(R_il.le.-R0_ed) then
         R0_uf=R_il+R0_ed
      else
         R0_uf=0.0
      endif
C pid_tuned.fmg( 203, 193):���� ������������������
      R0_op = R_el * R0_uf
C pid_tuned.fmg( 221, 192):����������
      if(L0_ek) then
         R_ul=0.0
         R_ok=R_il
      else
         R_ul=(R_uk*(R_il-R0_ik)+R_ak*R_ul)/(R_ak+deltat)
         R_ok=R_il
      endif
C pid_tuned.fmg( 243, 164):���������������� ����� ,1
      R0_em = R0_am + R_ep + R_ul
C pid_tuned.fmg( 256, 192):��������
      R0_af=MIN(R0_od,R0_em)
C pid_tuned.fmg( 266, 193):�������
      R0_if=MAX(R0_af,R0_ud)
C pid_tuned.fmg( 266, 189):��������
      R0_of = R_ef * R0_if
C pid_tuned.fmg( 163, 185):����������
      R_il = R_ar + (-R0_of)
C pid_tuned.fmg( 169, 193):��������
C sav1=R_ul
      if(L0_ek) then
         R_ul=0.0
         R_ok=R_il
      else
         R_ul=(R_uk*(R_il-R0_ik)+R_ak*R_ul)/(R_ak+deltat)
         R_ok=R_il
      endif
C pid_tuned.fmg( 243, 164):recalc:���������������� ����� ,1
C if(sav1.ne.R_ul .and. try25.gt.0) goto 25
C sav1=R0_am
      R0_am = R_ol * R_il
C pid_tuned.fmg( 200, 219):recalc:����������
C if(sav1.ne.R0_am .and. try19.gt.0) goto 19
      L0_u=R0_if.gt.R0_ad
C pid_tuned.fmg( 277, 184):���������� >
      L0_i=R0_if.lt.R0_o
C pid_tuned.fmg( 277, 180):���������� <
      L0_ap = L0_u.OR.L0_i
C pid_tuned.fmg( 283, 181):���
      if(L0_um) then
         R_ep=R0_up
      elseif(L0_ap) then
         R_ep=R_ep
      else
         R_ep=R_ep+deltat/R0_om*R0_op
      endif
      if(R_ep.gt.R0_im) then
         R_ep=R0_im
      elseif(R_ep.lt.R0_ip) then
         R_ep=R0_ip
      endif
C pid_tuned.fmg( 243, 192):����������,1
C sav1=R0_em
      R0_em = R0_am + R_ep + R_ul
C pid_tuned.fmg( 256, 192):recalc:��������
C if(sav1.ne.R0_em .and. try27.gt.0) goto 27
      if(L_e) then
         R8_id=R8_id
      else
         R8_id=R0_if
      endif
C pid_tuned.fmg( 306, 187):���� RE IN LO CH7
C label 52  try52=try52-1
      End
