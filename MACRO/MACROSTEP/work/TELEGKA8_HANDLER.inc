      Interface
      Subroutine TELEGKA8_HANDLER(ext_deltat,R_ubi,R_i,R_o
     &,L_u,R_ad,R_af,R_ak,R_ik,R_ok,R_uk,R_al,L_or,R_us,R_uv
     &,R_ax,L_ex,R_ox,R_ux,L_abe,R_ibe,L_obe,R_ade,L_ede,L_ife
     &,L_ake,R_ame,R_ume,L_epe,R_ope,L_are,L_ire,L_ise,L_use
     &,L_ate,L_ite,L_ave,L_ove,L_axe,R_adi,L_odi,L_afi,L_ifi
     &,L_ofi,L_eki,L_iki,L_uli,L_emi,C20_api,L_epi,L_opi,L_upi
     &,L_iri,L_ori,L_uri,L_asi,L_esi,L_isi,L_osi,L_usi,L_ati
     &,L_eti,L_iti,L_oti,L_uti,L_avi,L_evi,L_ivi,R_axi,R_exi
     &,L_odo,R_udo,R_afo,R_efo,R_ifo,R_ofo,R_ufo,R_oko,R_emo
     &,R_imo,R_omo,R_umo,L_apo,R_epo,R_ipo,L_ero,L_iro,L_oro
     &,L_uro,L_aso,L_eso,L_oso,L_uso,L_ato,L_eto,L_uto,L_avo
     &,L_exo,L_abu,L_ibu)
C |R_ubi         |4 4 I|11 mlfpar19     ||0.6|
C |R_i           |4 4 S|_simpJ3895*     |[���]���������� ��������� ������������� |0.0|
C |R_o           |4 4 K|_timpJ3895      |[���]������������ �������� �������������|1.0|
C |L_u           |1 1 S|_limpJ3895*     |[TF]���������� ��������� ������������� |F|
C |R_ad          |4 4 S|_slpbJ3888*     |���������� ��������� ||
C |R_af          |4 4 I|VX01            |||
C |R_ak          |4 4 I|max_c           |||
C |R_ik          |4 4 S|_slpbJ3841*     |���������� ��������� ||
C |R_ok          |4 4 S|_slpbJ3840*     |���������� ��������� ||
C |R_uk          |4 4 S|_slpbJ3839*     |���������� ��������� ||
C |R_al          |4 4 S|_slpbJ3833*     |���������� ��������� ||
C |L_or          |1 1 S|_qffJ3789*      |�������� ������ Q RS-��������  |F|
C |R_us          |4 4 I|mlfpar19        |��������� ������������|0.6|
C |R_uv          |4 4 S|_simpJ3631*     |[���]���������� ��������� ������������� |0.0|
C |R_ax          |4 4 K|_timpJ3631      |[���]������������ �������� �������������|0.4|
C |L_ex          |1 1 S|_limpJ3631*     |[TF]���������� ��������� ������������� |F|
C |R_ox          |4 4 S|_simpJ3629*     |[���]���������� ��������� ������������� |0.0|
C |R_ux          |4 4 K|_timpJ3629      |[���]������������ �������� �������������|0.4|
C |L_abe         |1 1 S|_limpJ3629*     |[TF]���������� ��������� ������������� |F|
C |R_ibe         |4 4 S|_sdelnv2dyn$100Dasha*|[���]���������� ��������� |0|
C |L_obe         |1 1 S|_idelnv2dyn$100Dasha*|[TF]���������� ��������� �������� ������ |f|
C |R_ade         |4 4 S|_sdelnv2dyn$99Dasha*|[���]���������� ��������� |0|
C |L_ede         |1 1 S|_idelnv2dyn$99Dasha*|[TF]���������� ��������� �������� ������ |f|
C |L_ife         |1 1 S|_qff156*        |�������� ������ Q RS-��������  |F|
C |L_ake         |1 1 S|_qff155*        |�������� ������ Q RS-��������  |F|
C |R_ame         |4 4 I|value_pos       |��� �������� ��������|0.5|
C |R_ume         |4 4 S|_sdelnv2dyn$83Dasha*|[���]���������� ��������� |0|
C |L_epe         |1 1 S|_idelnv2dyn$83Dasha*|[TF]���������� ��������� �������� ������ |f|
C |R_ope         |4 4 S|_sdelnv2dyn$82Dasha*|[���]���������� ��������� |0|
C |L_are         |1 1 S|_idelnv2dyn$82Dasha*|[TF]���������� ��������� �������� ������ |f|
C |L_ire         |1 1 S|_qffnv2dyn$106Dasha*|�������� ������ Q RS-��������  |F|
C |L_ise         |1 1 S|_qffnv2dyn$104Dasha*|�������� ������ Q RS-��������  |F|
C |L_use         |1 1 S|_qffnv2dyn$105Dasha*|�������� ������ Q RS-��������  |F|
C |L_ate         |1 1 I|OUTC            |�����||
C |L_ite         |1 1 S|_qffnv2dyn$103Dasha*|�������� ������ Q RS-��������  |F|
C |L_ave         |1 1 S|_qffnv2dyn$102Dasha*|�������� ������ Q RS-��������  |F|
C |L_ove         |1 1 S|_qffnv2dyn$101Dasha*|�������� ������ Q RS-��������  |F|
C |L_axe         |1 1 I|OUTO            |������||
C |R_adi         |4 4 O|mf_value        |��� ��������� �����|0.5|
C |L_odi         |1 1 S|_qffnv2dyn$87Dasha*|�������� ������ Q RS-��������  |F|
C |L_afi         |1 1 S|_qffnv2dyn$86Dasha*|�������� ������ Q RS-��������  |F|
C |L_ifi         |1 1 S|_qffnv2dyn$96Dasha*|�������� ������ Q RS-��������  |F|
C |L_ofi         |1 1 S|_qffnv2dyn$95Dasha*|�������� ������ Q RS-��������  |F|
C |L_eki         |1 1 O|limit_switch_error|||
C |L_iki         |1 1 O|flag_mlf19      |||
C |L_uli         |1 1 O|XH53            |�� ������ (���)|F|
C |L_emi         |1 1 O|XH54            |�� ����� (���)|F|
C |C20_api       |3 20 O|task_state      |���������||
C |L_epi         |1 1 I|vlv_kvit        |||
C |L_opi         |1 1 I|instr_fault     |||
C |L_upi         |1 1 S|_qffJ3361*      |�������� ������ Q RS-��������  |F|
C |L_iri         |1 1 O|fault           |�������������||
C |L_ori         |1 1 O|norm            |�����||
C |L_uri         |1 1 I|mlf17           |������ ���������������� ����� �����||
C |L_asi         |1 1 I|mlf16           |������ ������������ ����� �����||
C |L_esi         |1 1 I|mlf15           |������ ���������������� ����� ������||
C |L_isi         |1 1 I|mlf14           |������ ������������ ����� ������||
C |L_osi         |1 1 I|mlf07           |���������� ���� �������||
C |L_usi         |1 1 I|mlf28           |������ ���������������� ��������� �����||
C |L_ati         |1 1 I|mlf09           |����� ��������� �����||
C |L_eti         |1 1 I|mlf08           |����� ��������� ������||
C |L_iti         |1 1 I|mlf26           |������ ���������������� ��������� ������||
C |L_oti         |1 1 I|mlf27           |������ ������������ ��������� �����||
C |L_uti         |1 1 I|mlf25           |������ ������������ ��������� ������||
C |L_avi         |1 1 I|mlf23           |������� ������� �����||
C |L_evi         |1 1 I|mlf22           |����� ����� ��������||
C |L_ivi         |1 1 I|mlf19           |���� ������������||
C |R_axi         |4 4 K|_cJ3173         |�������� ��������� ����������|`max_c`|
C |R_exi         |4 4 K|_cJ3172         |�������� ��������� ����������|`min_c`|
C |L_odo         |1 1 I|YA27            |������� ���� �� ����������|F|
C |R_udo         |4 4 K|_uintV_INT_PU   |����������� ������ ����������� ������|`max_c`|
C |R_afo         |4 4 K|_tintV_INT_PU   |[���]�������� T �����������|1|
C |R_efo         |4 4 O|_ointV_INT_PU*  |�������� ������ ����������� |`state_c`|
C |R_ifo         |4 4 K|_lintV_INT_PU   |����������� ������ ����������� �����|`min_c`|
C |R_ofo         |4 4 I|vel             |����� ����|20.0|
C |R_ufo         |4 4 O|POS             |��������� ��||
C |R_oko         |4 4 O|VZ01            |�������� ����������� ��||
C |R_emo         |4 4 S|vmdown_ST*      |��������� ������ "������� ����� �� ���������" |0.0|
C |R_imo         |4 4 I|vmdown          |������� ������ ������ "������� ����� �� ���������" |0.0|
C |R_omo         |4 4 S|vmup_ST*        |��������� ������ "������� ������ �� ���������" |0.0|
C |R_umo         |4 4 I|vmup            |������� ������ ������ "������� ������ �� ���������" |0.0|
C |L_apo         |1 1 O|vmstop_CMD*     |[TF]����� ������ ������� ���� �� ���������|F|
C |R_epo         |4 4 S|vmstop_ST*      |��������� ������ "������� ���� �� ���������" |0.0|
C |R_ipo         |4 4 I|vmstop          |������� ������ ������ "������� ���� �� ���������" |0.0|
C |L_ero         |1 1 I|mlf06           |������������� ������� "�����"||
C |L_iro         |1 1 I|mlf05           |������������� ������� "������"||
C |L_oro         |1 1 O|vmup_CMD*       |[TF]����� ������ ������� ������ �� ���������|F|
C |L_uro         |1 1 I|ulubup          |���������� ������ �� ����������|F|
C |L_aso         |1 1 I|ulubdown        |���������� ���� �� ����������|F|
C |L_eso         |1 1 O|vmdown_CMD*     |[TF]����� ������ ������� ����� �� ���������|F|
C |L_oso         |1 1 O|stop_inner      |�������||
C |L_uso         |1 1 S|_qffJ3174*      |�������� ������ Q RS-��������  |F|
C |L_ato         |1 1 I|mlf04           |���������������� �������� �����||
C |L_eto         |1 1 I|YA25            |������� ����� �� ����������|F|
C |L_uto         |1 1 I|mlf03           |���������������� �������� ������||
C |L_avo         |1 1 I|YA26            |������� ������ �� ����������|F|
C |L_exo         |1 1 S|_qff2*          |�������� ������ Q RS-��������  |F|
C |L_abu         |1 1 S|_qff1*          |�������� ������ Q RS-��������  |F|
C |L_ibu         |1 1 I|mlf24           |��� ���������� � ����������||

      IMPLICIT NONE
      REAL*4 ext_deltat
      REAL*4 R_i,R_o
      LOGICAL*1 L_u
      REAL*4 R_ad,R_af,R_ak,R_ik,R_ok,R_uk,R_al
      LOGICAL*1 L_or
      REAL*4 R_us,R_uv,R_ax
      LOGICAL*1 L_ex
      REAL*4 R_ox,R_ux
      LOGICAL*1 L_abe
      REAL*4 R_ibe
      LOGICAL*1 L_obe
      REAL*4 R_ade
      LOGICAL*1 L_ede,L_ife,L_ake
      REAL*4 R_ame,R_ume
      LOGICAL*1 L_epe
      REAL*4 R_ope
      LOGICAL*1 L_are,L_ire,L_ise,L_use,L_ate,L_ite,L_ave
     &,L_ove,L_axe
      REAL*4 R_ubi,R_adi
      LOGICAL*1 L_odi,L_afi,L_ifi,L_ofi,L_eki,L_iki,L_uli
     &,L_emi
      CHARACTER*20 C20_api
      LOGICAL*1 L_epi,L_opi,L_upi,L_iri,L_ori,L_uri,L_asi
     &,L_esi,L_isi,L_osi,L_usi,L_ati,L_eti,L_iti,L_oti,L_uti
     &,L_avi,L_evi,L_ivi
      REAL*4 R_axi,R_exi
      LOGICAL*1 L_odo
      REAL*4 R_udo,R_afo,R_efo,R_ifo,R_ofo,R_ufo,R_oko,R_emo
     &,R_imo,R_omo,R_umo
      LOGICAL*1 L_apo
      REAL*4 R_epo,R_ipo
      LOGICAL*1 L_ero,L_iro,L_oro,L_uro,L_aso,L_eso,L_oso
     &,L_uso,L_ato,L_eto,L_uto,L_avo,L_exo,L_abu,L_ibu
      End subroutine TELEGKA8_HANDLER
      End interface
