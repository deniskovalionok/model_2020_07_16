      Interface
      Subroutine LIFT_HANDLER(ext_deltat,R_uvi,R8_ili,I_o
     &,I_id,I_af,L_of,I_uf,R_ak,R_ek,R_ik,R_ok,I_uk,I_il,I_im
     &,I_ap,C20_ar,C20_as,C8_at,R_uv,R_ax,L_ex,R_ix,R_ox,I_ux
     &,R_obe,R_ade,I_ede,I_ude,I_ife,L_ake,L_ike,L_oke,L_ale
     &,L_ile,L_ole,L_ule,R_eme,L_epe,L_upe,L_ire,L_ore,L_ase
     &,L_ise,R8_use,R_ote,R8_eve,L_ive,L_axe,L_oxe,I_uxe,L_uki
     &,R_uli,R_upi,L_esi,L_osi,L_uti,L_ivi,L_axi,L_exi,L_ixi
     &,L_oxi,L_uxi,L_abo)
C |R_uvi         |4 4 I|11 mlfpar19     |��������� ������������|0.6|
C |R8_ili        |4 8 O|16 value        |��� �������� ��������|0.5|
C |I_o           |2 4 O|state2          |����������||
C |I_id          |2 4 O|LREADY          |����� ����������||
C |I_af          |2 4 O|LBUSY           |����� �����||
C |L_of          |1 1 I|peregruz        |����������|F|
C |I_uf          |2 4 O|LPEREGR         |����� ����������||
C |R_ak          |4 4 S|vmdown_button_ST*|��������� ������ "������� ���� �� ���������" |0.0|
C |R_ek          |4 4 I|vmdown_button   |������� ������ ������ "������� ���� �� ���������" |0.0|
C |R_ik          |4 4 S|vmup_button_ST* |��������� ������ "������� ����� �� ���������" |0.0|
C |R_ok          |4 4 I|vmup_button     |������� ������ ������ "������� ����� �� ���������" |0.0|
C |I_uk          |2 4 O|state1          |��������� 1||
C |I_il          |2 4 O|state3          |��������� 2||
C |I_im          |2 4 O|LUPO            |����� �����||
C |I_ap          |2 4 O|LDOWNC          |����� ����||
C |C20_ar        |3 20 O|task_state      |���������||
C |C20_as        |3 20 O|task_name       |������� ���������||
C |C8_at         |3 8 I|task            |������� ���������||
C |R_uv          |4 4 S|vmstop_button_ST*|��������� ������ "������� ���� �� ���������" |0.0|
C |R_ax          |4 4 I|vmstop_button   |������� ������ ������ "������� ���� �� ���������" |0.0|
C |L_ex          |1 1 O|vmstart_button_CMD*|[TF]����� ������ ������� �������� ��������� �� ���������|F|
C |R_ix          |4 4 S|vmstart_button_ST*|��������� ������ "������� �������� ��������� �� ���������" |0.0|
C |R_ox          |4 4 I|vmstart_button  |������� ������ ������ "������� �������� ��������� �� ���������" |0.0|
C |I_ux          |2 4 O|LERROR          |����� �������������||
C |R_obe         |4 4 O|POS_CL          |��������||
C |R_ade         |4 4 O|POS_OP          |��������||
C |I_ede         |2 4 O|LDOWN           |����� �����||
C |I_ude         |2 4 O|LUP             |����� ������||
C |I_ife         |2 4 O|LZM             |������ "�������"||
C |L_ake         |1 1 I|vlv_kvit        |||
C |L_ike         |1 1 I|instr_fault     |||
C |L_oke         |1 1 S|_qffJ1618*      |�������� ������ Q RS-��������  |F|
C |L_ale         |1 1 O|vmdown_button_CMD*|[TF]����� ������ ������� ���� �� ���������|F|
C |L_ile         |1 1 O|vmup_button_CMD*|[TF]����� ������ ������� ����� �� ���������|F|
C |L_ole         |1 1 I|ulubopen        |���������� ������� �� ���������|F|
C |L_ule         |1 1 I|ulubclose       |���������� ������� �� ���������|F|
C |R_eme         |4 4 I|tdown           |����� ���� ����|30.0|
C |L_epe         |1 1 O|block           |||
C |L_upe         |1 1 O|vmstop_button_CMD*|[TF]����� ������ ������� ���� �� ���������|F|
C |L_ire         |1 1 O|STOP            |�������||
C |L_ore         |1 1 S|_qff10*         |�������� ������ Q RS-��������  |F|
C |L_ase         |1 1 O|norm            |�����||
C |L_ise         |1 1 O|nopower         |��� ����������||
C |R8_use        |4 8 I|voltage         |[��]���������� �� ������||
C |R_ote         |4 4 I|power           |�������� ��������||
C |R8_eve        |4 8 O|sn_power        |�������� ���� ��|3.0|
C |L_ive         |1 1 I|YA23            |������� ������� �� ����������|F|
C |L_axe         |1 1 I|YA24            |������� ������� �� ����������|F|
C |L_oxe         |1 1 O|fault           |�������������||
C |I_uxe         |2 4 O|LAM             |������ "�������"||
C |L_uki         |1 1 S|_qff6*          |�������� ������ Q RS-��������  |F|
C |R_uli         |4 4 O|mf_value        |��� ��������� �����|0.5|
C |R_upi         |4 4 I|tup             |����� ���� �����|30.0|
C |L_esi         |1 1 S|_qff2*          |�������� ������ Q RS-��������  |F|
C |L_osi         |1 1 O|XH53            |�� ������� (���)|F|
C |L_uti         |1 1 S|_qff1*          |�������� ������ Q RS-��������  |F|
C |L_ivi         |1 1 O|XH54            |�� ������� (���)|F|
C |L_axi         |1 1 I|mlf24           |��� ���������� � ����������||
C |L_exi         |1 1 I|mlf23           |������� ������� �����||
C |L_ixi         |1 1 I|mlf22           |����� ����� ��������||
C |L_oxi         |1 1 I|mlf04           |�������� �� ��������||
C |L_uxi         |1 1 I|mlf03           |�������� �� ��������||
C |L_abo         |1 1 I|mlf19           |���� ������������||

      IMPLICIT NONE
      REAL*4 ext_deltat
      INTEGER*4 I_o,I_id,I_af
      LOGICAL*1 L_of
      INTEGER*4 I_uf
      REAL*4 R_ak,R_ek,R_ik,R_ok
      INTEGER*4 I_uk,I_il,I_im,I_ap
      CHARACTER*20 C20_ar,C20_as
      CHARACTER*8 C8_at
      REAL*4 R_uv,R_ax
      LOGICAL*1 L_ex
      REAL*4 R_ix,R_ox
      INTEGER*4 I_ux
      REAL*4 R_obe,R_ade
      INTEGER*4 I_ede,I_ude,I_ife
      LOGICAL*1 L_ake,L_ike,L_oke,L_ale,L_ile,L_ole,L_ule
      REAL*4 R_eme
      LOGICAL*1 L_epe,L_upe,L_ire,L_ore,L_ase,L_ise
      REAL*8 R8_use
      REAL*4 R_ote
      REAL*8 R8_eve
      LOGICAL*1 L_ive,L_axe,L_oxe
      INTEGER*4 I_uxe
      LOGICAL*1 L_uki
      REAL*8 R8_ili
      REAL*4 R_uli,R_upi
      LOGICAL*1 L_esi,L_osi,L_uti,L_ivi
      REAL*4 R_uvi
      LOGICAL*1 L_axi,L_exi,L_ixi,L_oxi,L_uxi,L_abo
      End subroutine LIFT_HANDLER
      End interface
