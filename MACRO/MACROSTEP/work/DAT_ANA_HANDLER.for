      Subroutine DAT_ANA_HANDLER(ext_deltat,R_e,R_i,R_ed,R_el
     &,R_ol,R_em,R_om,I_op,R_es,L_is,R_us,L_at,L_et,R_ot,R_ix
     &,R_ux,L_ebe,R_ibe)
C |R_e           |4 4 O|nom             |||
C |R_i           |4 4 K|_cJ1450         |�������� ��������� ����������|0.3|
C |R_ed          |4 4 I|koeff_units     |����������� �����������||
C |R_el          |4 4 I|LOWER_warning_setpoint|||
C |R_ol          |4 4 I|LOWER_alarm_setpoint|||
C |R_em          |4 4 I|UPPER_alarm_setpoint|||
C |R_om          |4 4 I|UPPER_warning_setpoint|||
C |I_op          |2 4 O|dat_color       |���� ��������||
C |R_es          |4 4 I|GM05V           |���������� ����������� �������||
C |L_is          |1 1 I|GM05            |���������� ����������� �������||
C |R_us          |4 4 I|GM04V           |������ �������� ���������� ��������||
C |L_at          |1 1 I|GM04            |������ �������� ���������� ��������||
C |L_et          |1 1 I|GM02            |����� �� ������������ ������ ���������||
C |R_ot          |4 4 O|KKS             |�����||
C |R_ix          |4 4 I|MAXIMUM         |||
C |R_ux          |4 4 I|MINIMUM         |||
C |L_ebe         |1 1 I|GM01            |����� �� ����������� ������ ���������||
C |R_ibe         |4 4 I|input           |����||

      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      REAL*4 R_e,R_i,R0_o,R0_u,R0_ad,R_ed
      LOGICAL*1 L0_id,L0_od,L0_ud,L0_af,L0_ef,L0_if,L0_of
     &,L0_uf,L0_ak,L0_ek,L0_ik,L0_ok,L0_uk,L0_al
      REAL*4 R_el
      LOGICAL*1 L0_il
      REAL*4 R_ol
      LOGICAL*1 L0_ul,L0_am
      REAL*4 R_em
      LOGICAL*1 L0_im
      REAL*4 R_om
      LOGICAL*1 L0_um,L0_ap,L0_ep
      INTEGER*4 I0_ip,I_op
      LOGICAL*1 L0_up,L0_ar,L0_er
      INTEGER*4 I0_ir,I0_or,I0_ur
      REAL*4 R0_as,R_es
      LOGICAL*1 L_is
      REAL*4 R0_os,R_us
      LOGICAL*1 L_at,L_et
      REAL*4 R0_it,R_ot,R0_ut,R0_av,R0_ev
      LOGICAL*1 L0_iv,L0_ov
      REAL*4 R0_uv
      LOGICAL*1 L0_ax
      REAL*4 R0_ex,R_ix,R0_ox,R_ux,R0_abe
      LOGICAL*1 L_ebe
      REAL*4 R_ibe

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      !��������� R0_o = DAT_ANA_HANDLERC?? /0.3/
      R0_o=R_i
C DAT_ANA_HANDLER.fmg( 329, 207):���������
      R0_ad = R_ix + (-R_ux)
C DAT_ANA_HANDLER.fmg( 322, 209):��������
      R0_u = R0_ad * R0_o
C DAT_ANA_HANDLER.fmg( 332, 208):����������
      R_e = R0_u + R_ux
C DAT_ANA_HANDLER.fmg( 344, 207):��������
      R0_abe = R_ibe * R_ed
C DAT_ANA_HANDLER.fmg(  44, 254):����������
      if(L_ebe) then
         R0_ox=R_ux
      else
         R0_ox=R0_abe
      endif
C DAT_ANA_HANDLER.fmg( 130, 252):���� RE IN LO CH7
      if(L_et) then
         R0_it=R_ix
      else
         R0_it=R0_ox
      endif
C DAT_ANA_HANDLER.fmg( 141, 251):���� RE IN LO CH7
      if(L_at) then
         R0_os=R_us
      else
         R0_os=R0_it
      endif
C DAT_ANA_HANDLER.fmg( 151, 250):���� RE IN LO CH7
      L0_il=R0_abe.lt.R_el
C DAT_ANA_HANDLER.fmg( 248, 266):���������� <
      L0_am=R0_abe.gt.R_em
C DAT_ANA_HANDLER.fmg( 248, 258):���������� >
      L0_ul=R0_abe.lt.R_ol
C DAT_ANA_HANDLER.fmg( 248, 251):���������� <
      L0_um = L0_am.OR.L0_ul
C DAT_ANA_HANDLER.fmg( 254, 257):���
      L0_im=R0_abe.gt.R_om
C DAT_ANA_HANDLER.fmg( 248, 273):���������� >
      L0_ap = L0_im.OR.L0_il
C DAT_ANA_HANDLER.fmg( 254, 272):���
      L0_uf=R_us.gt.R_el
C DAT_ANA_HANDLER.fmg( 286, 202):���������� >
      L0_ak=R_us.gt.R_ol
C DAT_ANA_HANDLER.fmg( 286, 207):���������� >
      L0_ek=R_us.lt.R_em
C DAT_ANA_HANDLER.fmg( 286, 212):���������� <
      L0_ik=R_us.lt.R_om
C DAT_ANA_HANDLER.fmg( 286, 218):���������� <
      L0_ok=R_us.lt.R_ix
C DAT_ANA_HANDLER.fmg( 286, 225):���������� <
      L0_al=R_us.gt.R_ux
C DAT_ANA_HANDLER.fmg( 286, 234):���������� >
      L0_uk = L0_al.AND.L0_ok.AND.L0_ik.AND.L0_ek.AND.L0_ak.AND.L0_uf
C DAT_ANA_HANDLER.fmg( 302, 230):�
      L0_ar = L_at.AND.L0_uk
C DAT_ANA_HANDLER.fmg( 194, 231):�
      I0_ip = z'000000FF'
C DAT_ANA_HANDLER.fmg( 299, 278):��������� ������������� IN (�������)
      I0_or = z'0000FFFF'
C DAT_ANA_HANDLER.fmg( 214, 232):��������� ������������� IN (�������)
      I0_ur = z'0000FF00'
C DAT_ANA_HANDLER.fmg( 214, 234):��������� ������������� IN (�������)
      R0_ut = 0.01
C DAT_ANA_HANDLER.fmg(  92, 260):��������� (RE4) (�������)
      R0_ev = R_ix + (-R_ux)
C DAT_ANA_HANDLER.fmg(  86, 262):��������
      R0_av = R0_ev * R0_ut
C DAT_ANA_HANDLER.fmg(  95, 262):����������
      R0_ex = R_ix + R0_av
C DAT_ANA_HANDLER.fmg( 101, 272):��������
      L0_ax=R0_abe.gt.R0_ex
C DAT_ANA_HANDLER.fmg( 108, 274):���������� >
      R0_uv = R_ux + (-R0_av)
C DAT_ANA_HANDLER.fmg( 101, 266):��������
      L0_ov=R0_abe.lt.R0_uv
C DAT_ANA_HANDLER.fmg( 108, 268):���������� <
      L0_iv = L0_ax.OR.L0_ov
C DAT_ANA_HANDLER.fmg( 116, 270):���
      L0_ep = L0_ap.OR.L0_um.OR.L0_iv.OR.L_ebe.OR.L_et
C DAT_ANA_HANDLER.fmg( 299, 268):���
      R0_as = R_es + R_ot
C DAT_ANA_HANDLER.fmg( 148, 214):��������
C label 78  try78=try78-1
      if(L_is) then
         R_ot=R0_as
      else
         R_ot=R0_os
      endif
C DAT_ANA_HANDLER.fmg( 158, 249):���� RE IN LO CH7
C sav1=R0_as
      R0_as = R_es + R_ot
C DAT_ANA_HANDLER.fmg( 148, 214):recalc:��������
C if(sav1.ne.R0_as .and. try78.gt.0) goto 78
      L0_if=R_ot.gt.R_ux
C DAT_ANA_HANDLER.fmg( 286, 192):���������� >
      L0_ef=R_ot.lt.R_ix
C DAT_ANA_HANDLER.fmg( 286, 182):���������� <
      L0_ud=R_ot.lt.R_em
C DAT_ANA_HANDLER.fmg( 286, 169):���������� <
      L0_id=R_ot.gt.R_el
C DAT_ANA_HANDLER.fmg( 286, 159):���������� >
      L0_od=R_ot.gt.R_ol
C DAT_ANA_HANDLER.fmg( 286, 164):���������� >
      L0_af=R_ot.lt.R_om
C DAT_ANA_HANDLER.fmg( 286, 174):���������� <
      L0_of = L0_if.AND.L0_ef.AND.L0_af.AND.L0_ud.AND.L0_od.AND.L0_id
C DAT_ANA_HANDLER.fmg( 302, 186):�
      L0_up = L_is.AND.L0_of
C DAT_ANA_HANDLER.fmg( 194, 224):�
      L0_er = L0_ar.OR.L0_up
C DAT_ANA_HANDLER.fmg( 203, 229):���
      if(L0_er) then
         I0_ir=I0_or
      else
         I0_ir=I0_ur
      endif
C DAT_ANA_HANDLER.fmg( 217, 232):���� RE IN LO CH7
      if(L0_ep) then
         I_op=I0_ip
      else
         I_op=I0_ir
      endif
C DAT_ANA_HANDLER.fmg( 304, 278):���� RE IN LO CH7
      End
