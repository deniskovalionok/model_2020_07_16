      Subroutine TOLKATEL_HANDLER_dpro(ext_deltat,L_e,L_i
     &,R_o,R_u,R_ad,R_ed,R_id,I_od,R_ef,R_if,R_ek,R_ik,C20_om
     &,C20_op,I_ir,I_as,R_es,R_is,R_os,R_us,L_at,L_et,I_it
     &,I_av,I_ax,I_ox,C20_obe,C20_ode,C8_ofe,R_ile,R_ole,L_ule
     &,R_ame,R_eme,I_ime,L_ape,L_epe,L_ipe,I_upe,I_ire,L_ase
     &,L_ise,L_ose,L_ate,L_ite,L_ote,L_ute,L_uve,L_uxe,L_ebi
     &,L_obi,L_adi,R8_idi,R_efi,R8_ufi,L_uki,I_ali,L_imi,L_umi
     &,L_ari,L_ori,L_asi,L_esi,L_isi,L_osi,L_usi,L_ati)
C |L_e           |1 1 I|YA25C           |������� ������� �� ����������|F|
C |L_i           |1 1 I|YA26C           |������� ������� �� ���������� (��)|F|
C |R_o           |4 4 K|_uintT_INT      |����������� ������ ����������� ������|`p_UP`|
C |R_u           |4 4 K|_tintT_INT      |[���]�������� T �����������|1|
C |R_ad          |4 4 K|_lintT_INT      |����������� ������ ����������� �����|`p_LOW`|
C |R_ed          |4 4 K|_lcmpJ2903      |[]�������� ������ �����������|`p_TOLKATEL_XH54`|
C |R_id          |4 4 K|_lcmpJ2873      |[]�������� ������ �����������|`p_TOLKATEL_XH53`|
C |I_od          |2 4 O|LWORK           |����� �������||
C |R_ef          |4 4 O|VX01            |��������� ��������� �� ��� X||
C |R_if          |4 4 O|_ointT_INT*     |�������� ������ ����������� |`p_TOLKATEL_XH54`|
C |R_ek          |4 4 O|VX02            |�������� ����������� ���������||
C |R_ik          |4 4 I|tcl_top         |�������� �������|20|
C |C20_om        |3 20 O|task_name3      |������� ��������� ��� ������� ������������ ��������||
C |C20_op        |3 20 O|task_name2      |������� ��������� ��� ������� ������������ ��������||
C |I_ir          |2 4 O|LREADY          |����� ����������||
C |I_as          |2 4 O|LBUSY           |����� �����||
C |R_es          |4 4 S|vminit_button_ST*|��������� ������ "������� � �������� �� ���������" |0.0|
C |R_is          |4 4 I|vminit_button   |������� ������ ������ "������� � �������� �� ���������" |0.0|
C |R_os          |4 4 S|vmwork_button_ST*|��������� ������ "������� � ������� �� ���������" |0.0|
C |R_us          |4 4 I|vmwork_button   |������� ������ ������ "������� � ������� �� ���������" |0.0|
C |L_at          |1 1 I|YA25            |������� ������� �� ����������|F|
C |L_et          |1 1 I|YA26            |������� ������� �� ����������|F|
C |I_it          |2 4 O|state1          |��������� 1||
C |I_av          |2 4 O|state2          |��������� 2||
C |I_ax          |2 4 O|LWORKO          |����� � �������||
C |I_ox          |2 4 O|LINITC          |����� � ��������||
C |C20_obe       |3 20 O|task_state      |���������||
C |C20_ode       |3 20 O|task_name       |������� ���������||
C |C8_ofe        |3 8 I|task            |������� ���������||
C |R_ile         |4 4 S|vmstop_button_ST*|��������� ������ "������� ���� �� ���������" |0.0|
C |R_ole         |4 4 I|vmstop_button   |������� ������ ������ "������� ���� �� ���������" |0.0|
C |L_ule         |1 1 O|vmstart_button_CMD*|[TF]����� ������ ������� �������� ��������� �� ���������|F|
C |R_ame         |4 4 S|vmstart_button_ST*|��������� ������ "������� �������� ��������� �� ���������" |0.0|
C |R_eme         |4 4 I|vmstart_button  |������� ������ ������ "������� �������� ��������� �� ���������" |0.0|
C |I_ime         |2 4 O|LERROR          |����� �������������||
C |L_ape         |1 1 I|YA27C           |������� ���� �� ���������� (��)|F|
C |L_epe         |1 1 I|stop_avt        |������� ���� �� ����������|F|
C |L_ipe         |1 1 O|vmstop_button_CMD*|[TF]����� ������ ������� ���� �� ���������|F|
C |I_upe         |2 4 O|LINIT           |����� ��������||
C |I_ire         |2 4 O|LZM             |������ "�������"||
C |L_ase         |1 1 I|vlv_kvit        |||
C |L_ise         |1 1 I|instr_fault     |||
C |L_ose         |1 1 S|_qffJ1618*      |�������� ������ Q RS-��������  |F|
C |L_ate         |1 1 O|vminit_button_CMD*|[TF]����� ������ ������� � �������� �� ���������|F|
C |L_ite         |1 1 O|vmwork_button_CMD*|[TF]����� ������ ������� � ������� �� ���������|F|
C |L_ote         |1 1 I|ulubopen        |���������� ������� �� ���������|F|
C |L_ute         |1 1 I|ulubclose       |���������� ������� �� ���������|F|
C |L_uve         |1 1 O|block           |||
C |L_uxe         |1 1 S|_qff10*         |�������� ������ Q RS-��������  |F|
C |L_ebi         |1 1 O|STOP            |�������||
C |L_obi         |1 1 O|norm            |�����||
C |L_adi         |1 1 O|nopower         |��� ����������||
C |R8_idi        |4 8 I|voltage         |[��]���������� �� ������||
C |R_efi         |4 4 I|power           |�������� ��������||
C |R8_ufi        |4 8 O|sn_power        |�������� ���� ��|3.0|
C |L_uki         |1 1 O|fault           |�������������||
C |I_ali         |2 4 O|LAM             |������ "�������"||
C |L_imi         |1 1 S|_qff2*          |�������� ������ Q RS-��������  |F|
C |L_umi         |1 1 O|XH53            |�� ������� (���)|F|
C |L_ari         |1 1 S|_qff1*          |�������� ������ Q RS-��������  |F|
C |L_ori         |1 1 O|XH54            |�� ������� (���)|F|
C |L_asi         |1 1 I|mlf24           |��� ���������� � ����������||
C |L_esi         |1 1 I|mlf23           |������� ������� �����||
C |L_isi         |1 1 I|mlf22           |����� ����� ��������||
C |L_osi         |1 1 I|mlf04           |�������� �� ��������||
C |L_usi         |1 1 I|mlf03           |�������� �� ��������||
C |L_ati         |1 1 I|mlf19           |���� ������������||

      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      LOGICAL*1 L_e,L_i
      REAL*4 R_o,R_u,R_ad,R_ed,R_id
      INTEGER*4 I_od,I0_ud,I0_af
      REAL*4 R_ef,R_if
      LOGICAL*1 L0_of,L0_uf
      REAL*4 R0_ak,R_ek,R_ik,R0_ok,R0_uk,R0_al,R0_el
      LOGICAL*1 L0_il,L0_ol
      CHARACTER*20 C20_ul,C20_am,C20_em,C20_im,C20_om,C20_um
     &,C20_ap,C20_ep,C20_ip,C20_op
      INTEGER*4 I0_up,I0_ar
      LOGICAL*1 L0_er
      INTEGER*4 I_ir,I0_or,I0_ur,I_as
      REAL*4 R_es,R_is,R_os,R_us
      LOGICAL*1 L_at,L_et
      INTEGER*4 I_it,I0_ot,I0_ut,I_av,I0_ev,I0_iv,I0_ov,I0_uv
     &,I_ax,I0_ex,I0_ix,I_ox
      CHARACTER*20 C20_ux,C20_abe,C20_ebe,C20_ibe,C20_obe
     &,C20_ube,C20_ade,C20_ede,C20_ide,C20_ode
      CHARACTER*8 C8_ude
      LOGICAL*1 L0_afe
      CHARACTER*8 C8_efe
      LOGICAL*1 L0_ife
      CHARACTER*8 C8_ofe
      INTEGER*4 I0_ufe,I0_ake,I0_eke,I0_ike,I0_oke,I0_uke
     &,I0_ale,I0_ele
      REAL*4 R_ile,R_ole
      LOGICAL*1 L_ule
      REAL*4 R_ame,R_eme
      INTEGER*4 I_ime,I0_ome,I0_ume
      LOGICAL*1 L_ape,L_epe,L_ipe,L0_ope
      INTEGER*4 I_upe,I0_are,I0_ere,I_ire,I0_ore,I0_ure
      LOGICAL*1 L_ase,L0_ese,L_ise,L_ose,L0_use,L_ate,L0_ete
     &,L_ite,L_ote,L_ute,L0_ave,L0_eve,L0_ive,L0_ove,L_uve
     &,L0_axe,L0_exe,L0_ixe,L0_oxe,L_uxe,L0_abi,L_ebi
      LOGICAL*1 L0_ibi,L_obi,L0_ubi,L_adi
      REAL*4 R0_edi
      REAL*8 R8_idi
      LOGICAL*1 L0_odi,L0_udi
      REAL*4 R0_afi,R_efi,R0_ifi,R0_ofi
      REAL*8 R8_ufi
      LOGICAL*1 L0_aki,L0_eki,L0_iki,L0_oki,L_uki
      INTEGER*4 I_ali,I0_eli,I0_ili
      LOGICAL*1 L0_oli,L0_uli,L0_ami,L0_emi,L_imi,L0_omi,L_umi
     &,L0_api,L0_epi,L0_ipi,L0_opi,L0_upi,L_ari,L0_eri,L0_iri
     &,L_ori,L0_uri,L_asi,L_esi,L_isi,L_osi,L_usi
      LOGICAL*1 L_ati

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      L0_aki = L_at.OR.L_e
C TOLKATEL_HANDLER_dpro.fmg(  66, 186):���
      L0_iki = L_et.OR.L_i
C TOLKATEL_HANDLER_dpro.fmg(  63, 229):���
      I0_af = z'01000003'
C TOLKATEL_HANDLER_dpro.fmg( 122, 268):��������� ������������� IN (�������)
      I0_ud = z'0100000A'
C TOLKATEL_HANDLER_dpro.fmg( 122, 266):��������� ������������� IN (�������)
      R0_ak = 0.0
C TOLKATEL_HANDLER_dpro.fmg( 312, 246):��������� (RE4) (�������)
      R0_el = 0.0
C TOLKATEL_HANDLER_dpro.fmg( 298, 246):��������� (RE4) (�������)
      C20_am = ''
C TOLKATEL_HANDLER_dpro.fmg( 168, 156):��������� ���������� CH20 (CH30) (�������)
      C20_ul = '�������'
C TOLKATEL_HANDLER_dpro.fmg( 168, 154):��������� ���������� CH20 (CH30) (�������)
      C20_em = '������'
C TOLKATEL_HANDLER_dpro.fmg( 189, 157):��������� ���������� CH20 (CH30) (�������)
      C20_ap = ''
C TOLKATEL_HANDLER_dpro.fmg(  74, 158):��������� ���������� CH20 (CH30) (�������)
      C20_um = '� ��������� 2'
C TOLKATEL_HANDLER_dpro.fmg(  74, 156):��������� ���������� CH20 (CH30) (�������)
      C20_ep = '� ��������� 1'
C TOLKATEL_HANDLER_dpro.fmg(  96, 158):��������� ���������� CH20 (CH30) (�������)
      I0_up = z'0100000A'
C TOLKATEL_HANDLER_dpro.fmg( 220, 268):��������� ������������� IN (�������)
      I0_ar = z'01000003'
C TOLKATEL_HANDLER_dpro.fmg( 220, 270):��������� ������������� IN (�������)
      I0_ur = z'0100000A'
C TOLKATEL_HANDLER_dpro.fmg( 220, 290):��������� ������������� IN (�������)
      I0_or = z'01000003'
C TOLKATEL_HANDLER_dpro.fmg( 220, 288):��������� ������������� IN (�������)
      L_ate=R_is.ne.R_es
      R_es=R_is
C TOLKATEL_HANDLER_dpro.fmg(  14, 196):���������� ������������� ������
      L_ite=R_us.ne.R_os
      R_os=R_us
C TOLKATEL_HANDLER_dpro.fmg(  19, 249):���������� ������������� ������
      I0_ut = z'01000003'
C TOLKATEL_HANDLER_dpro.fmg( 138, 162):��������� ������������� IN (�������)
      I0_ot = z'01000010'
C TOLKATEL_HANDLER_dpro.fmg( 138, 160):��������� ������������� IN (�������)
      I0_iv = z'01000003'
C TOLKATEL_HANDLER_dpro.fmg( 117, 252):��������� ������������� IN (�������)
      I0_ev = z'01000010'
C TOLKATEL_HANDLER_dpro.fmg( 117, 250):��������� ������������� IN (�������)
      I0_uv = z'01000003'
C TOLKATEL_HANDLER_dpro.fmg( 185, 229):��������� ������������� IN (�������)
      I0_ov = z'0100000A'
C TOLKATEL_HANDLER_dpro.fmg( 185, 227):��������� ������������� IN (�������)
      I0_ix = z'01000003'
C TOLKATEL_HANDLER_dpro.fmg( 193, 179):��������� ������������� IN (�������)
      I0_ex = z'0100000A'
C TOLKATEL_HANDLER_dpro.fmg( 193, 177):��������� ������������� IN (�������)
      I0_ere = z'01000003'
C TOLKATEL_HANDLER_dpro.fmg( 115, 175):��������� ������������� IN (�������)
      I0_are = z'0100000A'
C TOLKATEL_HANDLER_dpro.fmg( 115, 173):��������� ������������� IN (�������)
      C20_ebe = '�������'
C TOLKATEL_HANDLER_dpro.fmg(  29, 160):��������� ���������� CH20 (CH30) (�������)
      C20_ibe = '������� ���'
C TOLKATEL_HANDLER_dpro.fmg(  29, 162):��������� ���������� CH20 (CH30) (�������)
      C20_ux = '��������'
C TOLKATEL_HANDLER_dpro.fmg(  44, 160):��������� ���������� CH20 (CH30) (�������)
      C20_ube = '� ��������'
C TOLKATEL_HANDLER_dpro.fmg(  50, 171):��������� ���������� CH20 (CH30) (�������)
      C20_ede = '� �������'
C TOLKATEL_HANDLER_dpro.fmg(  35, 172):��������� ���������� CH20 (CH30) (�������)
      C20_ide = ''
C TOLKATEL_HANDLER_dpro.fmg(  35, 174):��������� ���������� CH20 (CH30) (�������)
      C8_ude = 'init'
C TOLKATEL_HANDLER_dpro.fmg(  18, 180):��������� ���������� CH8 (�������)
      call chcomp(C8_ofe,C8_ude,L0_afe)
C TOLKATEL_HANDLER_dpro.fmg(  23, 184):���������� ���������
      C8_efe = 'work'
C TOLKATEL_HANDLER_dpro.fmg(  18, 222):��������� ���������� CH8 (�������)
      call chcomp(C8_ofe,C8_efe,L0_ife)
C TOLKATEL_HANDLER_dpro.fmg(  23, 226):���������� ���������
      if(L0_ife) then
         C20_ip=C20_um
      else
         C20_ip=C20_ap
      endif
C TOLKATEL_HANDLER_dpro.fmg(  78, 157):���� RE IN LO CH20
      if(L0_afe) then
         C20_op=C20_ep
      else
         C20_op=C20_ip
      endif
C TOLKATEL_HANDLER_dpro.fmg( 100, 158):���� RE IN LO CH20
      if(L0_ife) then
         C20_im=C20_ul
      else
         C20_im=C20_am
      endif
C TOLKATEL_HANDLER_dpro.fmg( 172, 155):���� RE IN LO CH20
      if(L0_afe) then
         C20_om=C20_em
      else
         C20_om=C20_im
      endif
C TOLKATEL_HANDLER_dpro.fmg( 194, 158):���� RE IN LO CH20
      if(L0_ife) then
         C20_ade=C20_ede
      else
         C20_ade=C20_ide
      endif
C TOLKATEL_HANDLER_dpro.fmg(  39, 172):���� RE IN LO CH20
      if(L0_afe) then
         C20_ode=C20_ube
      else
         C20_ode=C20_ade
      endif
C TOLKATEL_HANDLER_dpro.fmg(  55, 172):���� RE IN LO CH20
      I0_ufe = z'0100008E'
C TOLKATEL_HANDLER_dpro.fmg( 170, 194):��������� ������������� IN (�������)
      I0_ake = z'01000086'
C TOLKATEL_HANDLER_dpro.fmg( 164, 248):��������� ������������� IN (�������)
      I0_ike = z'01000003'
C TOLKATEL_HANDLER_dpro.fmg( 152, 262):��������� ������������� IN (�������)
      I0_oke = z'01000010'
C TOLKATEL_HANDLER_dpro.fmg( 152, 264):��������� ������������� IN (�������)
      I0_ele = z'01000003'
C TOLKATEL_HANDLER_dpro.fmg( 159, 209):��������� ������������� IN (�������)
      I0_ale = z'01000010'
C TOLKATEL_HANDLER_dpro.fmg( 159, 207):��������� ������������� IN (�������)
      L_ipe=R_ole.ne.R_ile
      R_ile=R_ole
C TOLKATEL_HANDLER_dpro.fmg(  14, 213):���������� ������������� ������
      L0_ope = L_ipe.OR.L_epe.OR.L_ape
C TOLKATEL_HANDLER_dpro.fmg(  48, 210):���
      L_ule=R_eme.ne.R_ame
      R_ame=R_eme
C TOLKATEL_HANDLER_dpro.fmg(  19, 236):���������� ������������� ������
      L0_ete = L_ule.AND.L0_ife
C TOLKATEL_HANDLER_dpro.fmg(  30, 234):�
      L0_exe = L_ite.OR.L0_ete
C TOLKATEL_HANDLER_dpro.fmg(  52, 235):���
      L0_use = L_ule.AND.L0_afe
C TOLKATEL_HANDLER_dpro.fmg(  29, 192):�
      L0_axe = L_ate.OR.L0_use
C TOLKATEL_HANDLER_dpro.fmg(  52, 193):���
      I0_ore = z'0100000E'
C TOLKATEL_HANDLER_dpro.fmg( 189, 200):��������� ������������� IN (�������)
      I0_ome = z'01000007'
C TOLKATEL_HANDLER_dpro.fmg( 150, 180):��������� ������������� IN (�������)
      I0_ume = z'01000003'
C TOLKATEL_HANDLER_dpro.fmg( 150, 182):��������� ������������� IN (�������)
      I0_eli = z'0100000E'
C TOLKATEL_HANDLER_dpro.fmg( 182, 255):��������� ������������� IN (�������)
      L_ose=(L_ise.or.L_ose).and..not.(L_ase)
      L0_ese=.not.L_ose
C TOLKATEL_HANDLER_dpro.fmg( 326, 178):RS �������
      L0_ove=.false.
C TOLKATEL_HANDLER_dpro.fmg(  64, 215):��������� ���������� (�������)
      L0_ive=.false.
C TOLKATEL_HANDLER_dpro.fmg(  64, 213):��������� ���������� (�������)
      L0_ubi =.NOT.(L_usi.OR.L_osi.OR.L_ati.OR.L_isi.OR.L_esi.OR.L_asi
     &)
C TOLKATEL_HANDLER_dpro.fmg( 319, 191):���
      L0_ibi =.NOT.(L0_ubi)
C TOLKATEL_HANDLER_dpro.fmg( 328, 185):���
      L_uki = L0_ibi.OR.L_ose
C TOLKATEL_HANDLER_dpro.fmg( 332, 184):���
      L0_eve = L_uki.OR.L_ote
C TOLKATEL_HANDLER_dpro.fmg(  58, 243):���
      L0_oki = L0_exe.AND.(.NOT.L0_eve)
C TOLKATEL_HANDLER_dpro.fmg(  66, 236):�
      L0_iri = L0_oki.OR.L0_iki
C TOLKATEL_HANDLER_dpro.fmg(  80, 234):���
      L0_ave = L_uki.OR.L_ute
C TOLKATEL_HANDLER_dpro.fmg(  55, 201):���
      L_uve = L_uki.OR.L0_ove.OR.L0_ive.OR.L0_eve.OR.L0_ave
C TOLKATEL_HANDLER_dpro.fmg(  68, 213):���
      if(L_uve) then
         I_as=I0_up
      else
         I_as=I0_ar
      endif
C TOLKATEL_HANDLER_dpro.fmg( 224, 269):���� RE IN LO CH7
      L0_er = L_uki.OR.L_uve
C TOLKATEL_HANDLER_dpro.fmg( 218, 280):���
      if(L0_er) then
         I_ir=I0_or
      else
         I_ir=I0_ur
      endif
C TOLKATEL_HANDLER_dpro.fmg( 224, 288):���� RE IN LO CH7
      L0_eki = (.NOT.L0_ave).AND.L0_axe
C TOLKATEL_HANDLER_dpro.fmg(  66, 194):�
      L0_api = L0_eki.OR.L0_aki
C TOLKATEL_HANDLER_dpro.fmg(  80, 192):���
      L0_oxe = L0_iri.OR.L0_api
C TOLKATEL_HANDLER_dpro.fmg(  86, 203):���
      if(L_uki) then
         I_ime=I0_ome
      else
         I_ime=I0_ume
      endif
C TOLKATEL_HANDLER_dpro.fmg( 153, 181):���� RE IN LO CH7
      L_obi = L0_ubi.OR.L_ise
C TOLKATEL_HANDLER_dpro.fmg( 328, 190):���
      R0_edi = 0.1
C TOLKATEL_HANDLER_dpro.fmg( 254, 160):��������� (RE4) (�������)
      L_adi=R8_idi.lt.R0_edi
C TOLKATEL_HANDLER_dpro.fmg( 259, 162):���������� <
      R0_afi = 0.0
C TOLKATEL_HANDLER_dpro.fmg( 266, 182):��������� (RE4) (�������)
      L0_ixe = L_umi.OR.L0_ope.OR.L_ori
C TOLKATEL_HANDLER_dpro.fmg(  60, 207):���
C label 187  try187=try187-1
      L_uxe=(L0_ixe.or.L_uxe).and..not.(L0_oxe)
      L0_abi=.not.L_uxe
C TOLKATEL_HANDLER_dpro.fmg( 106, 205):RS �������,10
      L_ebi = L_epe.OR.L_uxe
C TOLKATEL_HANDLER_dpro.fmg( 117, 208):���
      L_ori=R_if.gt.R_ed
C TOLKATEL_HANDLER_dpro.fmg( 351, 239):���������� >
      L0_ipi = (.NOT.L_ebi).AND.L_ori
C TOLKATEL_HANDLER_dpro.fmg( 105, 260):�
      L0_oli = L_ebi.OR.L_asi
C TOLKATEL_HANDLER_dpro.fmg(  98, 214):���
      L0_upi = L_ori.OR.L0_oli.OR.L0_api
C TOLKATEL_HANDLER_dpro.fmg( 104, 229):���
      L0_uri = (.NOT.L_ori).AND.L0_iri
C TOLKATEL_HANDLER_dpro.fmg( 104, 235):�
      L_ari=(L0_uri.or.L_ari).and..not.(L0_upi)
      L0_eri=.not.L_ari
C TOLKATEL_HANDLER_dpro.fmg( 111, 233):RS �������,1
      L0_opi = (.NOT.L0_ipi).AND.L_ari
C TOLKATEL_HANDLER_dpro.fmg( 130, 236):�
      L0_uf = L0_opi.AND.(.NOT.L_osi)
C TOLKATEL_HANDLER_dpro.fmg( 268, 245):�
      L0_ol = L0_uf.OR.L_usi
C TOLKATEL_HANDLER_dpro.fmg( 281, 244):���
      if(L0_ol) then
         R0_uk=R_ik
      else
         R0_uk=R0_el
      endif
C TOLKATEL_HANDLER_dpro.fmg( 303, 256):���� RE IN LO CH7
      L0_emi = L0_oli.OR.L_umi.OR.L0_iri
C TOLKATEL_HANDLER_dpro.fmg( 104, 185):���
      L0_epi = L0_api.AND.(.NOT.L_umi)
C TOLKATEL_HANDLER_dpro.fmg( 104, 191):�
      L_imi=(L0_epi.or.L_imi).and..not.(L0_emi)
      L0_omi=.not.L_imi
C TOLKATEL_HANDLER_dpro.fmg( 111, 189):RS �������,2
      L0_uli = (.NOT.L_ebi).AND.L_umi
C TOLKATEL_HANDLER_dpro.fmg( 103, 166):�
      L0_ami = L_imi.AND.(.NOT.L0_uli)
C TOLKATEL_HANDLER_dpro.fmg( 130, 190):�
      L0_of = L0_ami.AND.(.NOT.L_usi)
C TOLKATEL_HANDLER_dpro.fmg( 268, 231):�
      L0_il = L0_of.OR.L_osi
C TOLKATEL_HANDLER_dpro.fmg( 281, 230):���
      if(L0_il) then
         R0_ok=R_ik
      else
         R0_ok=R0_el
      endif
C TOLKATEL_HANDLER_dpro.fmg( 303, 238):���� RE IN LO CH7
      R0_al = R0_uk + (-R0_ok)
C TOLKATEL_HANDLER_dpro.fmg( 309, 249):��������
      if(L_asi) then
         R_ek=R0_ak
      else
         R_ek=R0_al
      endif
C TOLKATEL_HANDLER_dpro.fmg( 316, 248):���� RE IN LO CH7
      R_if=R_if+deltat/R_u*R_ek
      if(R_if.gt.R_o) then
         R_if=R_o
      elseif(R_if.lt.R_ad) then
         R_if=R_ad
      endif
C TOLKATEL_HANDLER_dpro.fmg( 332, 232):����������,T_INT
      L_umi=R_if.lt.R_id
C TOLKATEL_HANDLER_dpro.fmg( 351, 227):���������� <
C sav1=L0_ixe
      L0_ixe = L_umi.OR.L0_ope.OR.L_ori
C TOLKATEL_HANDLER_dpro.fmg(  60, 207):recalc:���
C if(sav1.ne.L0_ixe .and. try187.gt.0) goto 187
C sav1=L0_emi
      L0_emi = L0_oli.OR.L_umi.OR.L0_iri
C TOLKATEL_HANDLER_dpro.fmg( 104, 185):recalc:���
C if(sav1.ne.L0_emi .and. try243.gt.0) goto 243
C sav1=L0_epi
      L0_epi = L0_api.AND.(.NOT.L_umi)
C TOLKATEL_HANDLER_dpro.fmg( 104, 191):recalc:�
C if(sav1.ne.L0_epi .and. try245.gt.0) goto 245
C sav1=L0_uli
      L0_uli = (.NOT.L_ebi).AND.L_umi
C TOLKATEL_HANDLER_dpro.fmg( 103, 166):recalc:�
C if(sav1.ne.L0_uli .and. try249.gt.0) goto 249
      if(L_umi) then
         I_upe=I0_are
      else
         I_upe=I0_ere
      endif
C TOLKATEL_HANDLER_dpro.fmg( 118, 174):���� RE IN LO CH7
      if(L_umi) then
         I_it=I0_ot
      else
         I_it=I0_ut
      endif
C TOLKATEL_HANDLER_dpro.fmg( 142, 160):���� RE IN LO CH7
      if(L_umi) then
         I0_eke=I0_ike
      else
         I0_eke=I0_oke
      endif
C TOLKATEL_HANDLER_dpro.fmg( 156, 262):���� RE IN LO CH7
      if(L0_opi) then
         I0_ili=I0_ake
      else
         I0_ili=I0_eke
      endif
C TOLKATEL_HANDLER_dpro.fmg( 168, 262):���� RE IN LO CH7
      if(L_uki) then
         I_ali=I0_eli
      else
         I_ali=I0_ili
      endif
C TOLKATEL_HANDLER_dpro.fmg( 186, 260):���� RE IN LO CH7
      R_ef=R_if
C TOLKATEL_HANDLER_dpro.fmg( 365, 234):������,VX01
      if(L0_ami) then
         I_ox=I0_ex
      else
         I_ox=I0_ix
      endif
C TOLKATEL_HANDLER_dpro.fmg( 196, 178):���� RE IN LO CH7
      L0_odi = L0_opi.OR.L0_ami
C TOLKATEL_HANDLER_dpro.fmg( 251, 174):���
      L0_udi = L0_odi.AND.(.NOT.L_adi)
C TOLKATEL_HANDLER_dpro.fmg( 266, 173):�
      if(L0_udi) then
         R0_ofi=R_efi
      else
         R0_ofi=R0_afi
      endif
C TOLKATEL_HANDLER_dpro.fmg( 269, 180):���� RE IN LO CH7
      if(L0_opi) then
         I_ax=I0_ov
      else
         I_ax=I0_uv
      endif
C TOLKATEL_HANDLER_dpro.fmg( 188, 228):���� RE IN LO CH7
      if(L_ori) then
         I_od=I0_ud
      else
         I_od=I0_af
      endif
C TOLKATEL_HANDLER_dpro.fmg( 126, 267):���� RE IN LO CH7
      if(L_ori) then
         I_av=I0_ev
      else
         I_av=I0_iv
      endif
C TOLKATEL_HANDLER_dpro.fmg( 120, 250):���� RE IN LO CH7
      if(L_ori) then
         C20_abe=C20_ebe
      else
         C20_abe=C20_ibe
      endif
C TOLKATEL_HANDLER_dpro.fmg(  33, 161):���� RE IN LO CH20
      if(L_umi) then
         C20_obe=C20_ux
      else
         C20_obe=C20_abe
      endif
C TOLKATEL_HANDLER_dpro.fmg(  49, 160):���� RE IN LO CH20
      if(L_ori) then
         I0_uke=I0_ale
      else
         I0_uke=I0_ele
      endif
C TOLKATEL_HANDLER_dpro.fmg( 162, 208):���� RE IN LO CH7
      if(L0_ami) then
         I0_ure=I0_ufe
      else
         I0_ure=I0_uke
      endif
C TOLKATEL_HANDLER_dpro.fmg( 173, 206):���� RE IN LO CH7
      if(L_uki) then
         I_ire=I0_ore
      else
         I_ire=I0_ure
      endif
C TOLKATEL_HANDLER_dpro.fmg( 192, 206):���� RE IN LO CH7
      R0_ifi = R8_ufi
C TOLKATEL_HANDLER_dpro.fmg( 264, 190):��������
C label 340  try340=try340-1
      R8_ufi = R0_ofi + R0_ifi
C TOLKATEL_HANDLER_dpro.fmg( 275, 189):��������
C sav1=R0_ifi
      R0_ifi = R8_ufi
C TOLKATEL_HANDLER_dpro.fmg( 264, 190):recalc:��������
C if(sav1.ne.R0_ifi .and. try340.gt.0) goto 340
      End
