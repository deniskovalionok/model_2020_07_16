      Subroutine TIAGA_HANDLER(ext_deltat,R_ati,R8_ofi,I_u
     &,I_id,R_od,R_ud,R_af,R_ef,I_if,I_ak,I_al,I_ol,C20_om
     &,C20_op,C8_or,R_it,R_ot,L_ut,R_av,R_ev,I_iv,R_ex,R_ox
     &,I_ux,I_ibe,I_ade,L_ode,L_afe,L_efe,L_ofe,L_ake,L_ike
     &,L_oke,R_ale,L_ame,L_ime,L_ape,L_epe,L_ope,L_are,R8_ire
     &,R_ese,R8_use,L_ate,L_ite,L_ute,I_ave,L_afi,R_aki,R_ami
     &,L_ipi,L_upi,L_asi,L_osi,L_eti,L_iti,L_oti,L_uti,L_avi
     &,L_evi)
C |R_ati         |4 4 I|11 mlfpar19     |��������� ������������|0.6|
C |R8_ofi        |4 8 O|16 value        |��� �������� ��������|0.5|
C |I_u           |2 4 O|LREADY          |����� ����������||
C |I_id          |2 4 O|LBUSY           |����� �����||
C |R_od          |4 4 S|vminit_button_ST*|��������� ������ "������� � �������� �� ���������" |0.0|
C |R_ud          |4 4 I|vminit_button   |������� ������ ������ "������� � �������� �� ���������" |0.0|
C |R_af          |4 4 S|vmwork_button_ST*|��������� ������ "������� � ������� �� ���������" |0.0|
C |R_ef          |4 4 I|vmwork_button   |������� ������ ������ "������� � ������� �� ���������" |0.0|
C |I_if          |2 4 O|state1          |��������� 1||
C |I_ak          |2 4 O|state2          |��������� 2||
C |I_al          |2 4 O|LWORKO          |����� � �������||
C |I_ol          |2 4 O|LINITC          |����� � ��������||
C |C20_om        |3 20 O|task_state      |���������||
C |C20_op        |3 20 O|task_name       |������� ���������||
C |C8_or         |3 8 I|task            |������� ���������||
C |R_it          |4 4 S|vmstop_button_ST*|��������� ������ "������� ���� �� ���������" |0.0|
C |R_ot          |4 4 I|vmstop_button   |������� ������ ������ "������� ���� �� ���������" |0.0|
C |L_ut          |1 1 O|vmstart_button_CMD*|[TF]����� ������ ������� �������� ��������� �� ���������|F|
C |R_av          |4 4 S|vmstart_button_ST*|��������� ������ "������� �������� ��������� �� ���������" |0.0|
C |R_ev          |4 4 I|vmstart_button  |������� ������ ������ "������� �������� ��������� �� ���������" |0.0|
C |I_iv          |2 4 O|LERROR          |����� �������������||
C |R_ex          |4 4 O|POS_CL          |��������||
C |R_ox          |4 4 O|POS_OP          |��������||
C |I_ux          |2 4 O|LINIT           |����� ��������||
C |I_ibe         |2 4 O|LWORK           |����� �������||
C |I_ade         |2 4 O|LZM             |������ "�������"||
C |L_ode         |1 1 I|vlv_kvit        |||
C |L_afe         |1 1 I|instr_fault     |||
C |L_efe         |1 1 S|_qffJ1618*      |�������� ������ Q RS-��������  |F|
C |L_ofe         |1 1 O|vminit_button_CMD*|[TF]����� ������ ������� � �������� �� ���������|F|
C |L_ake         |1 1 O|vmwork_button_CMD*|[TF]����� ������ ������� � ������� �� ���������|F|
C |L_ike         |1 1 I|ulubopen        |���������� ������� �� ���������|F|
C |L_oke         |1 1 I|ulubclose       |���������� ������� �� ���������|F|
C |R_ale         |4 4 I|tinit           |����� ���� � ��������|30.0|
C |L_ame         |1 1 O|block           |||
C |L_ime         |1 1 O|vmstop_button_CMD*|[TF]����� ������ ������� ���� �� ���������|F|
C |L_ape         |1 1 O|STOP            |�������||
C |L_epe         |1 1 S|_qff10*         |�������� ������ Q RS-��������  |F|
C |L_ope         |1 1 O|norm            |�����||
C |L_are         |1 1 O|nopower         |��� ����������||
C |R8_ire        |4 8 I|voltage         |[��]���������� �� ������||
C |R_ese         |4 4 I|power           |�������� ��������||
C |R8_use        |4 8 O|sn_power        |�������� ���� ��|3.0|
C |L_ate         |1 1 I|YA25            |������� ������� �� ����������|F|
C |L_ite         |1 1 I|YA26            |������� ������� �� ����������|F|
C |L_ute         |1 1 O|fault           |�������������||
C |I_ave         |2 4 O|LAM             |������ "�������"||
C |L_afi         |1 1 S|_qff6*          |�������� ������ Q RS-��������  |F|
C |R_aki         |4 4 O|mf_value        |��� ��������� �����|0.5|
C |R_ami         |4 4 I|twork           |����� ���� � �������|30.0|
C |L_ipi         |1 1 S|_qff2*          |�������� ������ Q RS-��������  |F|
C |L_upi         |1 1 O|XH53            |�� ������� (���)|F|
C |L_asi         |1 1 S|_qff1*          |�������� ������ Q RS-��������  |F|
C |L_osi         |1 1 O|XH54            |�� ������� (���)|F|
C |L_eti         |1 1 I|mlf24           |��� ���������� � ����������||
C |L_iti         |1 1 I|mlf23           |������� ������� �����||
C |L_oti         |1 1 I|mlf22           |����� ����� ��������||
C |L_uti         |1 1 I|mlf04           |�������� �� ��������||
C |L_avi         |1 1 I|mlf03           |�������� �� ��������||
C |L_evi         |1 1 I|mlf19           |���� ������������||

      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      INTEGER*4 I0_e,I0_i
      LOGICAL*1 L0_o
      INTEGER*4 I_u,I0_ad,I0_ed,I_id
      REAL*4 R_od,R_ud,R_af,R_ef
      INTEGER*4 I_if,I0_of,I0_uf,I_ak,I0_ek,I0_ik,I0_ok,I0_uk
     &,I_al,I0_el,I0_il,I_ol
      CHARACTER*20 C20_ul,C20_am,C20_em,C20_im,C20_om,C20_um
     &,C20_ap,C20_ep,C20_ip,C20_op
      CHARACTER*8 C8_up
      LOGICAL*1 L0_ar
      CHARACTER*8 C8_er
      LOGICAL*1 L0_ir
      CHARACTER*8 C8_or
      INTEGER*4 I0_ur,I0_as,I0_es,I0_is,I0_os,I0_us,I0_at
     &,I0_et
      REAL*4 R_it,R_ot
      LOGICAL*1 L_ut
      REAL*4 R_av,R_ev
      INTEGER*4 I_iv,I0_ov,I0_uv
      REAL*4 R0_ax,R_ex,R0_ix,R_ox
      INTEGER*4 I_ux,I0_abe,I0_ebe,I_ibe,I0_obe,I0_ube,I_ade
     &,I0_ede,I0_ide
      LOGICAL*1 L_ode,L0_ude,L_afe,L_efe,L0_ife,L_ofe,L0_ufe
     &,L_ake,L0_eke,L_ike,L_oke
      REAL*4 R0_uke,R_ale
      LOGICAL*1 L0_ele,L0_ile,L0_ole,L0_ule,L_ame,L0_eme,L_ime
     &,L0_ome,L0_ume,L_ape,L_epe,L0_ipe,L_ope,L0_upe,L_are
      REAL*4 R0_ere
      REAL*8 R8_ire
      LOGICAL*1 L0_ore,L0_ure
      REAL*4 R0_ase,R_ese,R0_ise,R0_ose
      REAL*8 R8_use
      LOGICAL*1 L_ate,L0_ete,L_ite,L0_ote,L_ute
      INTEGER*4 I_ave,I0_eve,I0_ive
      LOGICAL*1 L0_ove,L0_uve
      REAL*4 R0_axe,R0_exe
      LOGICAL*1 L0_ixe
      REAL*4 R0_oxe,R0_uxe,R0_abi,R0_ebi,R0_ibi,R0_obi
      LOGICAL*1 L0_ubi
      REAL*4 R0_adi,R0_edi,R0_idi,R0_odi
      LOGICAL*1 L0_udi,L_afi
      REAL*4 R0_efi,R0_ifi
      REAL*8 R8_ofi
      REAL*4 R0_ufi,R_aki,R0_eki,R0_iki,R0_oki,R0_uki,R0_ali
     &,R0_eli,R0_ili,R0_oli,R0_uli,R_ami
      LOGICAL*1 L0_emi,L0_imi,L0_omi,L0_umi,L0_api,L0_epi
     &,L_ipi,L0_opi,L_upi,L0_ari,L0_eri,L0_iri,L0_ori,L0_uri
     &,L_asi,L0_esi,L0_isi,L_osi,L0_usi
      REAL*4 R_ati
      LOGICAL*1 L_eti,L_iti,L_oti,L_uti,L_avi,L_evi

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      I0_e = z'0100000A'
C TIAGA_HANDLER.fmg( 220, 268):��������� ������������� IN (�������)
      I0_i = z'01000003'
C TIAGA_HANDLER.fmg( 220, 270):��������� ������������� IN (�������)
      I0_ed = z'0100000A'
C TIAGA_HANDLER.fmg( 220, 290):��������� ������������� IN (�������)
      I0_ad = z'01000003'
C TIAGA_HANDLER.fmg( 220, 288):��������� ������������� IN (�������)
      L_ofe=R_ud.ne.R_od
      R_od=R_ud
C TIAGA_HANDLER.fmg(  14, 193):���������� ������������� ������
      L_ake=R_ef.ne.R_af
      R_af=R_ef
C TIAGA_HANDLER.fmg(  19, 249):���������� ������������� ������
      I0_uf = z'01000003'
C TIAGA_HANDLER.fmg( 138, 162):��������� ������������� IN (�������)
      I0_of = z'01000010'
C TIAGA_HANDLER.fmg( 138, 160):��������� ������������� IN (�������)
      I0_ik = z'01000003'
C TIAGA_HANDLER.fmg( 117, 252):��������� ������������� IN (�������)
      I0_ek = z'01000010'
C TIAGA_HANDLER.fmg( 117, 250):��������� ������������� IN (�������)
      I0_uk = z'01000003'
C TIAGA_HANDLER.fmg( 185, 229):��������� ������������� IN (�������)
      I0_ok = z'0100000A'
C TIAGA_HANDLER.fmg( 185, 227):��������� ������������� IN (�������)
      I0_il = z'01000003'
C TIAGA_HANDLER.fmg( 193, 179):��������� ������������� IN (�������)
      I0_el = z'0100000A'
C TIAGA_HANDLER.fmg( 193, 177):��������� ������������� IN (�������)
      I0_ebe = z'01000003'
C TIAGA_HANDLER.fmg( 115, 171):��������� ������������� IN (�������)
      I0_abe = z'0100000A'
C TIAGA_HANDLER.fmg( 115, 169):��������� ������������� IN (�������)
      C20_em = '�������'
C TIAGA_HANDLER.fmg(  29, 160):��������� ���������� CH20 (CH30) (�������)
      C20_im = '������� ���'
C TIAGA_HANDLER.fmg(  29, 162):��������� ���������� CH20 (CH30) (�������)
      C20_ul = '��������'
C TIAGA_HANDLER.fmg(  44, 160):��������� ���������� CH20 (CH30) (�������)
      C20_um = '� ��������'
C TIAGA_HANDLER.fmg(  50, 171):��������� ���������� CH20 (CH30) (�������)
      C20_ep = '� �������'
C TIAGA_HANDLER.fmg(  35, 172):��������� ���������� CH20 (CH30) (�������)
      C20_ip = ''
C TIAGA_HANDLER.fmg(  35, 174):��������� ���������� CH20 (CH30) (�������)
      C8_up = 'init'
C TIAGA_HANDLER.fmg(  18, 180):��������� ���������� CH8 (�������)
      call chcomp(C8_or,C8_up,L0_ar)
C TIAGA_HANDLER.fmg(  23, 184):���������� ���������
      C8_er = 'work'
C TIAGA_HANDLER.fmg(  18, 222):��������� ���������� CH8 (�������)
      call chcomp(C8_or,C8_er,L0_ir)
C TIAGA_HANDLER.fmg(  23, 226):���������� ���������
      if(L0_ir) then
         C20_ap=C20_ep
      else
         C20_ap=C20_ip
      endif
C TIAGA_HANDLER.fmg(  39, 172):���� RE IN LO CH20
      if(L0_ar) then
         C20_op=C20_um
      else
         C20_op=C20_ap
      endif
C TIAGA_HANDLER.fmg(  55, 172):���� RE IN LO CH20
      I0_ur = z'0100008E'
C TIAGA_HANDLER.fmg( 170, 194):��������� ������������� IN (�������)
      I0_as = z'01000086'
C TIAGA_HANDLER.fmg( 164, 248):��������� ������������� IN (�������)
      I0_is = z'01000003'
C TIAGA_HANDLER.fmg( 152, 262):��������� ������������� IN (�������)
      I0_os = z'01000010'
C TIAGA_HANDLER.fmg( 152, 264):��������� ������������� IN (�������)
      I0_et = z'01000003'
C TIAGA_HANDLER.fmg( 159, 209):��������� ������������� IN (�������)
      I0_at = z'01000010'
C TIAGA_HANDLER.fmg( 159, 207):��������� ������������� IN (�������)
      L_ime=R_ot.ne.R_it
      R_it=R_ot
C TIAGA_HANDLER.fmg(  14, 208):���������� ������������� ������
      L_ut=R_ev.ne.R_av
      R_av=R_ev
C TIAGA_HANDLER.fmg(  19, 236):���������� ������������� ������
      L0_ufe = L_ut.AND.L0_ir
C TIAGA_HANDLER.fmg(  30, 234):�
      L0_eme = L_ake.OR.L0_ufe
C TIAGA_HANDLER.fmg(  52, 235):���
      L0_ife = L_ut.AND.L0_ar
C TIAGA_HANDLER.fmg(  29, 192):�
      L0_eke = L_ofe.OR.L0_ife
C TIAGA_HANDLER.fmg(  52, 193):���
      I0_ede = z'0100000E'
C TIAGA_HANDLER.fmg( 189, 200):��������� ������������� IN (�������)
      I0_ov = z'01000007'
C TIAGA_HANDLER.fmg( 150, 180):��������� ������������� IN (�������)
      I0_uv = z'01000003'
C TIAGA_HANDLER.fmg( 150, 182):��������� ������������� IN (�������)
      R0_ax = 100
C TIAGA_HANDLER.fmg( 378, 273):��������� (RE4) (�������)
      R0_ix = 100
C TIAGA_HANDLER.fmg( 365, 267):��������� (RE4) (�������)
      I0_ube = z'01000003'
C TIAGA_HANDLER.fmg( 122, 265):��������� ������������� IN (�������)
      I0_obe = z'0100000A'
C TIAGA_HANDLER.fmg( 122, 263):��������� ������������� IN (�������)
      I0_eve = z'0100000E'
C TIAGA_HANDLER.fmg( 182, 255):��������� ������������� IN (�������)
      L_efe=(L_afe.or.L_efe).and..not.(L_ode)
      L0_ude=.not.L_efe
C TIAGA_HANDLER.fmg( 326, 178):RS �������
      L0_ule=.false.
C TIAGA_HANDLER.fmg(  64, 215):��������� ���������� (�������)
      L0_ole=.false.
C TIAGA_HANDLER.fmg(  64, 213):��������� ���������� (�������)
      R0_uke = DeltaT
C TIAGA_HANDLER.fmg( 250, 254):��������� (RE4) (�������)
      if(R_ale.ge.0.0) then
         R0_uki=R0_uke/max(R_ale,1.0e-10)
      else
         R0_uki=R0_uke/min(R_ale,-1.0e-10)
      endif
C TIAGA_HANDLER.fmg( 259, 252):�������� ����������
      L0_upe =.NOT.(L_avi.OR.L_uti.OR.L_evi.OR.L_oti.OR.L_iti.OR.L_eti
     &)
C TIAGA_HANDLER.fmg( 319, 191):���
      L0_ipe =.NOT.(L0_upe)
C TIAGA_HANDLER.fmg( 328, 185):���
      L_ute = L0_ipe.OR.L_efe
C TIAGA_HANDLER.fmg( 332, 184):���
      L0_ile = L_ute.OR.L_ike
C TIAGA_HANDLER.fmg(  58, 243):���
      L0_ote = L0_eme.AND.(.NOT.L0_ile)
C TIAGA_HANDLER.fmg(  66, 236):�
      L0_isi = L0_ote.OR.L_ite
C TIAGA_HANDLER.fmg(  70, 234):���
      L0_ele = L_ute.OR.L_oke
C TIAGA_HANDLER.fmg(  55, 201):���
      L_ame = L_ute.OR.L0_ule.OR.L0_ole.OR.L0_ile.OR.L0_ele
C TIAGA_HANDLER.fmg(  68, 213):���
      if(L_ame) then
         I_id=I0_e
      else
         I_id=I0_i
      endif
C TIAGA_HANDLER.fmg( 224, 269):���� RE IN LO CH7
      L0_o = L_ute.OR.L_ame
C TIAGA_HANDLER.fmg( 218, 280):���
      if(L0_o) then
         I_u=I0_ad
      else
         I_u=I0_ed
      endif
C TIAGA_HANDLER.fmg( 224, 288):���� RE IN LO CH7
      L0_ete = (.NOT.L0_ele).AND.L0_eke
C TIAGA_HANDLER.fmg(  66, 194):�
      L0_ari = L0_ete.OR.L_ate
C TIAGA_HANDLER.fmg(  70, 192):���
      L0_ome = L0_isi.OR.L0_ari
C TIAGA_HANDLER.fmg(  76, 203):���
      L_epe=(L_ime.or.L_epe).and..not.(L0_ome)
      L0_ume=.not.L_epe
C TIAGA_HANDLER.fmg( 106, 205):RS �������,10
      L_ape=L_epe
C TIAGA_HANDLER.fmg( 125, 207):������,STOP
      L0_omi = L_epe.OR.L_eti
C TIAGA_HANDLER.fmg(  98, 214):���
      if(L_ute) then
         I_iv=I0_ov
      else
         I_iv=I0_uv
      endif
C TIAGA_HANDLER.fmg( 153, 181):���� RE IN LO CH7
      L_ope = L0_upe.OR.L_afe
C TIAGA_HANDLER.fmg( 328, 190):���
      R0_ere = 0.1
C TIAGA_HANDLER.fmg( 254, 160):��������� (RE4) (�������)
      L_are=R8_ire.lt.R0_ere
C TIAGA_HANDLER.fmg( 259, 162):���������� <
      R0_ase = 0.0
C TIAGA_HANDLER.fmg( 266, 182):��������� (RE4) (�������)
      R0_ibi = 0.000001
C TIAGA_HANDLER.fmg( 354, 208):��������� (RE4) (�������)
      R0_obi = 0.999999
C TIAGA_HANDLER.fmg( 354, 224):��������� (RE4) (�������)
      R0_axe = 0.0
C TIAGA_HANDLER.fmg( 296, 244):��������� (RE4) (�������)
      R0_exe = 0.0
C TIAGA_HANDLER.fmg( 370, 242):��������� (RE4) (�������)
      L0_ixe = L_iti.OR.L_oti
C TIAGA_HANDLER.fmg( 363, 237):���
      R0_uxe = 1.0
C TIAGA_HANDLER.fmg( 348, 252):��������� (RE4) (�������)
      R0_abi = 0.0
C TIAGA_HANDLER.fmg( 340, 252):��������� (RE4) (�������)
      R0_edi = 1.0e-10
C TIAGA_HANDLER.fmg( 318, 228):��������� (RE4) (�������)
      R0_efi = 0.0
C TIAGA_HANDLER.fmg( 328, 242):��������� (RE4) (�������)
      R0_iki = DeltaT
C TIAGA_HANDLER.fmg( 250, 264):��������� (RE4) (�������)
      if(R_ami.ge.0.0) then
         R0_ali=R0_iki/max(R_ami,1.0e-10)
      else
         R0_ali=R0_iki/min(R_ami,-1.0e-10)
      endif
C TIAGA_HANDLER.fmg( 259, 262):�������� ����������
      R0_uli = 0.0
C TIAGA_HANDLER.fmg( 282, 244):��������� (RE4) (�������)
      L0_umi = (.NOT.L_ape).AND.L_upi
C TIAGA_HANDLER.fmg( 103, 166):�
C label 196  try196=try196-1
      if(L_oti) then
         R0_oxe=R0_exe
      else
         R0_oxe=R8_ofi
      endif
C TIAGA_HANDLER.fmg( 373, 242):���� RE IN LO CH7
      if(.NOT.L0_ixe) then
         R_aki=R8_ofi
      endif
C TIAGA_HANDLER.fmg( 384, 252):���� � ������������� �������
      L0_iri = (.NOT.L_ape).AND.L_osi
C TIAGA_HANDLER.fmg( 105, 260):�
      L0_uri = L_osi.OR.L0_omi.OR.L0_ari
C TIAGA_HANDLER.fmg( 104, 229):���
      L0_usi = (.NOT.L_osi).AND.L0_isi
C TIAGA_HANDLER.fmg( 104, 235):�
      L_asi=(L0_usi.or.L_asi).and..not.(L0_uri)
      L0_esi=.not.L_asi
C TIAGA_HANDLER.fmg( 111, 233):RS �������,1
      L0_ori = (.NOT.L0_iri).AND.L_asi
C TIAGA_HANDLER.fmg( 130, 236):�
      L0_uve = L0_ori.AND.(.NOT.L_uti)
C TIAGA_HANDLER.fmg( 252, 242):�
      L0_imi = L0_uve.OR.L_avi
C TIAGA_HANDLER.fmg( 265, 241):���
      if(L0_imi) then
         R0_ili=R0_ali
      else
         R0_ili=R0_uli
      endif
C TIAGA_HANDLER.fmg( 287, 254):���� RE IN LO CH7
      L0_epi = L0_omi.OR.L_upi.OR.L0_isi
C TIAGA_HANDLER.fmg( 104, 185):���
      L0_eri = L0_ari.AND.(.NOT.L_upi)
C TIAGA_HANDLER.fmg( 104, 191):�
      L_ipi=(L0_eri.or.L_ipi).and..not.(L0_epi)
      L0_opi=.not.L_ipi
C TIAGA_HANDLER.fmg( 111, 189):RS �������,2
      L0_api = L_ipi.AND.(.NOT.L0_umi)
C TIAGA_HANDLER.fmg( 130, 190):�
      L0_ove = L0_api.AND.(.NOT.L_avi)
C TIAGA_HANDLER.fmg( 252, 228):�
      L0_emi = L0_ove.OR.L_uti
C TIAGA_HANDLER.fmg( 265, 227):���
      if(L0_emi) then
         R0_eli=R0_uki
      else
         R0_eli=R0_uli
      endif
C TIAGA_HANDLER.fmg( 287, 236):���� RE IN LO CH7
      R0_oli = R0_ili + (-R0_eli)
C TIAGA_HANDLER.fmg( 293, 246):��������
      if(L_eti) then
         R0_ifi=R0_axe
      else
         R0_ifi=R0_oli
      endif
C TIAGA_HANDLER.fmg( 300, 244):���� RE IN LO CH7
      R0_odi = R_aki + (-R_ati)
C TIAGA_HANDLER.fmg( 308, 235):��������
      R0_adi = R0_ifi + R0_odi
C TIAGA_HANDLER.fmg( 314, 236):��������
      R0_idi = R0_adi * R0_odi
C TIAGA_HANDLER.fmg( 318, 235):����������
      L0_udi=R0_idi.lt.R0_edi
C TIAGA_HANDLER.fmg( 323, 234):���������� <
      L_afi=(L0_udi.or.L_afi).and..not.(.NOT.L_evi)
      L0_ubi=.not.L_afi
C TIAGA_HANDLER.fmg( 330, 232):RS �������,6
      if(L_afi) then
         R_aki=R_ati
      endif
C TIAGA_HANDLER.fmg( 324, 252):���� � ������������� �������
      if(L_afi) then
         R0_ufi=R0_efi
      else
         R0_ufi=R0_ifi
      endif
C TIAGA_HANDLER.fmg( 332, 244):���� RE IN LO CH7
      R0_eki = R_aki + R0_ufi
C TIAGA_HANDLER.fmg( 338, 245):��������
      R0_ebi=MAX(R0_abi,R0_eki)
C TIAGA_HANDLER.fmg( 346, 246):��������
      R0_oki=MIN(R0_uxe,R0_ebi)
C TIAGA_HANDLER.fmg( 354, 247):�������
      L_upi=R0_oki.lt.R0_ibi
C TIAGA_HANDLER.fmg( 363, 210):���������� <
C sav1=L0_epi
      L0_epi = L0_omi.OR.L_upi.OR.L0_isi
C TIAGA_HANDLER.fmg( 104, 185):recalc:���
C if(sav1.ne.L0_epi .and. try238.gt.0) goto 238
C sav1=L0_eri
      L0_eri = L0_ari.AND.(.NOT.L_upi)
C TIAGA_HANDLER.fmg( 104, 191):recalc:�
C if(sav1.ne.L0_eri .and. try240.gt.0) goto 240
C sav1=L0_umi
      L0_umi = (.NOT.L_ape).AND.L_upi
C TIAGA_HANDLER.fmg( 103, 166):recalc:�
C if(sav1.ne.L0_umi .and. try196.gt.0) goto 196
      if(L_upi) then
         I0_es=I0_is
      else
         I0_es=I0_os
      endif
C TIAGA_HANDLER.fmg( 156, 262):���� RE IN LO CH7
      if(L0_ori) then
         I0_ive=I0_as
      else
         I0_ive=I0_es
      endif
C TIAGA_HANDLER.fmg( 168, 262):���� RE IN LO CH7
      if(L_ute) then
         I_ave=I0_eve
      else
         I_ave=I0_ive
      endif
C TIAGA_HANDLER.fmg( 186, 260):���� RE IN LO CH7
      if(L0_ixe) then
         R8_ofi=R0_oxe
      else
         R8_ofi=R0_oki
      endif
C TIAGA_HANDLER.fmg( 377, 246):���� RE IN LO CH7
      R_ox = R0_ix * R8_ofi
C TIAGA_HANDLER.fmg( 368, 266):����������
      R_ex = R0_ax + (-R_ox)
C TIAGA_HANDLER.fmg( 382, 272):��������
      L_osi=R0_oki.gt.R0_obi
C TIAGA_HANDLER.fmg( 363, 225):���������� >
C sav1=L0_uri
      L0_uri = L_osi.OR.L0_omi.OR.L0_ari
C TIAGA_HANDLER.fmg( 104, 229):recalc:���
C if(sav1.ne.L0_uri .and. try217.gt.0) goto 217
C sav1=L0_usi
      L0_usi = (.NOT.L_osi).AND.L0_isi
C TIAGA_HANDLER.fmg( 104, 235):recalc:�
C if(sav1.ne.L0_usi .and. try219.gt.0) goto 219
C sav1=L0_iri
      L0_iri = (.NOT.L_ape).AND.L_osi
C TIAGA_HANDLER.fmg( 105, 260):recalc:�
C if(sav1.ne.L0_iri .and. try212.gt.0) goto 212
      if(L_osi) then
         C20_am=C20_em
      else
         C20_am=C20_im
      endif
C TIAGA_HANDLER.fmg(  33, 161):���� RE IN LO CH20
      if(L_upi) then
         C20_om=C20_ul
      else
         C20_om=C20_am
      endif
C TIAGA_HANDLER.fmg(  49, 160):���� RE IN LO CH20
      if(L_osi) then
         I0_us=I0_at
      else
         I0_us=I0_et
      endif
C TIAGA_HANDLER.fmg( 162, 208):���� RE IN LO CH7
      if(L0_api) then
         I0_ide=I0_ur
      else
         I0_ide=I0_us
      endif
C TIAGA_HANDLER.fmg( 173, 206):���� RE IN LO CH7
      if(L_ute) then
         I_ade=I0_ede
      else
         I_ade=I0_ide
      endif
C TIAGA_HANDLER.fmg( 192, 206):���� RE IN LO CH7
      if(L0_ixe) then
         R_aki=R0_oki
      endif
C TIAGA_HANDLER.fmg( 366, 252):���� � ������������� �������
C sav1=R0_odi
      R0_odi = R_aki + (-R_ati)
C TIAGA_HANDLER.fmg( 308, 235):recalc:��������
C if(sav1.ne.R0_odi .and. try264.gt.0) goto 264
C sav1=R0_eki
      R0_eki = R_aki + R0_ufi
C TIAGA_HANDLER.fmg( 338, 245):recalc:��������
C if(sav1.ne.R0_eki .and. try281.gt.0) goto 281
      if(L0_api) then
         I_ol=I0_el
      else
         I_ol=I0_il
      endif
C TIAGA_HANDLER.fmg( 196, 178):���� RE IN LO CH7
      L0_ore = L0_ori.OR.L0_api
C TIAGA_HANDLER.fmg( 251, 174):���
      L0_ure = L0_ore.AND.(.NOT.L_are)
C TIAGA_HANDLER.fmg( 266, 173):�
      if(L0_ure) then
         R0_ose=R_ese
      else
         R0_ose=R0_ase
      endif
C TIAGA_HANDLER.fmg( 269, 180):���� RE IN LO CH7
      if(L0_ori) then
         I_al=I0_ok
      else
         I_al=I0_uk
      endif
C TIAGA_HANDLER.fmg( 188, 228):���� RE IN LO CH7
      if(L0_iri) then
         I_ibe=I0_obe
      else
         I_ibe=I0_ube
      endif
C TIAGA_HANDLER.fmg( 126, 264):���� RE IN LO CH7
      if(L0_iri) then
         I_ak=I0_ek
      else
         I_ak=I0_ik
      endif
C TIAGA_HANDLER.fmg( 120, 250):���� RE IN LO CH7
      if(L0_umi) then
         I_ux=I0_abe
      else
         I_ux=I0_ebe
      endif
C TIAGA_HANDLER.fmg( 118, 170):���� RE IN LO CH7
      if(L0_umi) then
         I_if=I0_of
      else
         I_if=I0_uf
      endif
C TIAGA_HANDLER.fmg( 142, 160):���� RE IN LO CH7
      R0_ise = R8_use
C TIAGA_HANDLER.fmg( 264, 190):��������
C label 372  try372=try372-1
      R8_use = R0_ose + R0_ise
C TIAGA_HANDLER.fmg( 275, 189):��������
C sav1=R0_ise
      R0_ise = R8_use
C TIAGA_HANDLER.fmg( 264, 190):recalc:��������
C if(sav1.ne.R0_ise .and. try372.gt.0) goto 372
      End
