      Interface
      Subroutine LODOCHKA_HANDLER(ext_deltat,L_e,R_ad,L_af
     &,L_uk,L_al,L_ol,L_em,L_op,R_ir,R_or,L_ur,L_ot,L_av,L_ev
     &,R_uv,R_ax,L_ex,L_ox,R_ede,R_ide,L_ode,I_ofe,I_ike,I_ele
     &,I_ame,I_ume,I_ape,L_upe,L_ite,L_ave,L_ove,L_exe,L_uxe
     &,L_ibi,L_adi,L_efi,L_aki,L_uki,I_ili,I_oli,R_omi,R_opi
     &,R_upi,R_uri,R_isi,R_eti,R_ovi,R_ixi,R_uxi,R_abo,R_ubo
     &,R_edo,R_ido,L_odo,L_oko,L_uko,R_elo,R_amo,R_omo,R_ipo
     &,R_ero,R_aso,R_eso,R_uto,R_avo,R_axo,R_uxo,R_ebu,R_ofu
     &,R_ufu,L_aku,R_uku,R_ulu,R_umu,L_eru,R_iru,R_esu,R_atu
     &,R_etu,R_uvu,R_axu,L_exu,R_abad,R_adad,R_afad,R_ekad
     &,R_alad,R_ulad,R_amad,L_ipad,L_upad,L_erad,R_irad,L_orad
     &,R_urad,R_itad,R_evad,R_axad,R_exad,L_obed,L_eded,L_oded
     &,R_uded,L_afed,R_efed,R_aked,R_uled,R_umed,R_aped,R_eped
     &,L_ored,L_esed,L_ised,L_used,L_oted,L_aved,R_exed,R_ixed
     &,R_ibid,R_obid,L_adid,R_afid,I_etid,R_avid,R_evid,R_ivid
     &,R_uvid,R_ixid,R_abod,R_ibod,C20_obod)
C |L_e           |1 1 I|LODOCHKA_FREE_STATE|��������� ������� � ��������� "������ �������"||
C |R_ad          |4 4 I|FDA40_BOAT_MASS |����� ����� � �����������||
C |L_af          |1 1 I|FDA60_LOD_INPUT_GATEWAY_FROM|������� �� �������� �����||
C |L_uk          |1 1 I|FDA60_LOD_TRANSFER_TABLE|������� �� ������������ �������||
C |L_al          |1 1 I|FDA60_LOD_MAIN_TROLLEY|������� �� �������||
C |L_ol          |1 1 I|FDA60_LOD_INPUT_GATEWAY|������� �� �������� �����||
C |L_em          |1 1 I|FDA60_LOD_VZV   |������� �� ������������||
C |L_op          |1 1 I|FDA60_LOD_LIFT  |������� ����������� �� ����� ||
C |R_ir          |4 4 S|_simpJ10082*    |[���]���������� ��������� ������������� |0.0|
C |R_or          |4 4 K|_timpJ10082     |[���]������������ �������� �������������|1.0|
C |L_ur          |1 1 S|_limpJ10082*    |[TF]���������� ��������� ������������� |F|
C |L_ot          |1 1 I|FDA40_START     |������ ������� �����������||
C |L_av          |1 1 I|FDA91EC001_INIT_COORD|������������� ���������� ������� ��� ��. �����������||
C |L_ev          |1 1 I|FDA91EC003_INIT_COORD|������������� ���������� ������� ��� ��. ���||
C |R_uv          |4 4 S|_simpJ10030*    |[���]���������� ��������� ������������� |0.0|
C |R_ax          |4 4 K|_timpJ10030     |[���]������������ �������� �������������|1.0|
C |L_ex          |1 1 S|_limpJ10030*    |[TF]���������� ��������� ������������� |F|
C |L_ox          |1 1 I|FDA91EC005_INIT_COORD|������������� ���������� ������� ��� ��. ���������||
C |R_ede         |4 4 S|_simpJ10001*    |[���]���������� ��������� ������������� |0.0|
C |R_ide         |4 4 K|_timpJ10001     |[���]������������ �������� �������������|1.0|
C |L_ode         |1 1 S|_limpJ10001*    |[TF]���������� ��������� ������������� |F|
C |I_ofe         |2 4 K|_lcmpJ9990      |�������� ������ �����������|5|
C |I_ike         |2 4 K|_lcmpJ9978      |�������� ������ �����������|4|
C |I_ele         |2 4 K|_lcmpJ9966      |�������� ������ �����������|3|
C |I_ame         |2 4 K|_lcmpJ9954      |�������� ������ �����������|2|
C |I_ume         |2 4 K|_lcmpJ9942      |�������� ������ �����������|1|
C |I_ape         |2 4 I|FDA60AE408_NUM_BOAT|����� ��������� ������� �� �������||
C |L_upe         |1 1 I|FDA60AE408_CATCH|������ ������� 5��� �������� 20FDA60AE408||
C |L_ite         |1 1 I|FDA61AE401_UNLOAD5|������ ������� ����� 20FDA61AE401||
C |L_ave         |1 1 I|FDA61AE401_UNLOAD4|������ ������� ����� 20FDA61AE401||
C |L_ove         |1 1 I|FDA61AE401_UNLOAD3|������ ������� ����� 20FDA61AE401||
C |L_exe         |1 1 I|FDA61AE401_UNLOAD2|������ ������� ����� 20FDA61AE401||
C |L_uxe         |1 1 I|FDA61AE401_UNLOAD1|������ ������� ����� 20FDA61AE401||
C |L_ibi         |1 1 I|FDA61AE401_CATCH5|������ ������� ����� 20FDA61AE401||
C |L_adi         |1 1 I|FDA61AE401_CATCH4|������ ������� ����� 20FDA61AE401||
C |L_efi         |1 1 I|FDA61AE401_CATCH3|������ ������� ����� 20FDA61AE401||
C |L_aki         |1 1 I|FDA61AE401_CATCH2|������ ������� ����� 20FDA61AE401||
C |L_uki         |1 1 I|FDA61AE401_CATCH1|������ ������� ����� 20FDA61AE401||
C |I_ili         |2 4 O|CR              |�������������� � �������������||
C |I_oli         |2 4 O|LP              |����� �� ������� ����������� �������||
C |R_omi         |4 4 K|_lcmpJ9186      |[]�������� ������ �����������|20|
C |R_opi         |4 4 I|FDA60AE500AVY01 |�������� ��������� 20FDA60AE500A||
C |R_upi         |4 4 K|_lcmpJ9173      |[]�������� ������ �����������|20|
C |R_uri         |4 4 I|FDA60AE500BVY01 |�������� ��������� 20FDA60AE500B||
C |R_isi         |4 4 I|FDA60AE500�VY01 |�������� ��������� 20FDA60AE500�||
C |R_eti         |4 4 K|_lcmpJ9156      |[]�������� ������ �����������|20|
C |R_ovi         |4 4 K|_lcmpJ9127      |[]�������� ������ �����������|20|
C |R_ixi         |4 4 K|_lcmpJ9057      |[]�������� ������ �����������|2|
C |R_uxi         |4 4 I|FDA60AE403_VZ01 |�������� �������������� �������||
C |R_abo         |4 4 K|_lcmpJ9051      |[]�������� ������ �����������|20|
C |R_ubo         |4 4 I|FDA60AE403_POS  |��������� ������� �� ��� X||
C |R_edo         |4 4 S|_simpJ9036*     |[���]���������� ��������� ������������� |0.0|
C |R_ido         |4 4 K|_timpJ9036      |[���]������������ �������� �������������|1.0|
C |L_odo         |1 1 S|_limpJ9036*     |[TF]���������� ��������� ������������� |F|
C |L_oko         |1 1 S|_splsJ8856*     |[TF]���������� ��������� ������������� |F|
C |L_uko         |1 1 I|FDA40_HAVE_DELTA_MASS|���� ��������� ����� �������||
C |R_elo         |4 4 I|FDA40_DELTA_MASS|��������� ����� �������||
C |R_amo         |4 4 O|MASS            |����� ����� ����������� � �������||
C |R_omo         |4 4 K|_lcmpJ8516      |[]�������� ������ �����������|20|
C |R_ipo         |4 4 K|_lcmpJ8503      |[]�������� ������ �����������|20|
C |R_ero         |4 4 K|_lcmpJ8495      |[]�������� ������ �����������|20|
C |R_aso         |4 4 I|FDA91AE001KE01_POS|��������� �1||
C |R_eso         |4 4 K|_lcmpJ8485      |[]�������� ������ �����������|20|
C |R_uto         |4 4 I|FDA91AE001KE01_V01|������� �1 �� ��� X||
C |R_avo         |4 4 I|FDA91AE003KE01_V01|�������� 20FDA91AE003KE01||
C |R_axo         |4 4 K|_lcmpJ8465      |[]�������� ������ �����������|20|
C |R_uxo         |4 4 I|FDA91AE003KE01_POS|��������� �� 20FDA91AE003KE01||
C |R_ebu         |4 4 K|_lcmpJ8453      |[]�������� ������ �����������|20|
C |R_ofu         |4 4 S|_simpJ8368*     |[���]���������� ��������� ������������� |0.0|
C |R_ufu         |4 4 K|_timpJ8368      |[���]������������ �������� �������������|1.0|
C |L_aku         |1 1 S|_limpJ8368*     |[TF]���������� ��������� ������������� |F|
C |R_uku         |4 4 K|_lcmpJ8358      |[]�������� ������ �����������|20|
C |R_ulu         |4 4 K|_lcmpJ8350      |[]�������� ������ �����������|20|
C |R_umu         |4 4 K|_lcmpJ8342      |[]�������� ������ �����������|20|
C |L_eru         |1 1 O|FDA91AE012_CATCHED|������� ��������� �� 20FDA91AE012||
C |R_iru         |4 4 K|_lcmpJ8317      |[]�������� ������ �����������|20|
C |R_esu         |4 4 K|_lcmpJ8308      |[]�������� ������ �����������|20|
C |R_atu         |4 4 I|FDA91AE012_POS  |��������� �4||
C |R_etu         |4 4 K|_lcmpJ8297      |[]�������� ������ �����������|20|
C |R_uvu         |4 4 S|_simpJ7966*     |[���]���������� ��������� ������������� |0.0|
C |R_axu         |4 4 K|_timpJ7966      |[���]������������ �������� �������������|1.0|
C |L_exu         |1 1 S|_limpJ7966*     |[TF]���������� ��������� ������������� |F|
C |R_abad        |4 4 K|_lcmpJ7929      |[]�������� ������ �����������|20|
C |R_adad        |4 4 K|_lcmpJ7921      |[]�������� ������ �����������|20|
C |R_afad        |4 4 K|_lcmpJ7913      |[]�������� ������ �����������|20|
C |R_ekad        |4 4 K|_lcmpJ7509      |[]�������� ������ �����������|20|
C |R_alad        |4 4 K|_lcmpJ7500      |[]�������� ������ �����������|20|
C |R_ulad        |4 4 I|FDA91AE007_POS  |��������� �4||
C |R_amad        |4 4 K|_lcmpJ7487      |[]�������� ������ �����������|20|
C |L_ipad        |1 1 O|FDA91AE007_CATCHED|������� ��������� �4||
C |L_upad        |1 1 S|_qffJ7476*      |�������� ������ Q RS-��������  |F|
C |L_erad        |1 1 I|FDA91AE007_UNCATCH|��������� ������� �4||
C |R_irad        |4 4 I|FDA91AE007_VZ01 |������� �4 �� ��� Z||
C |L_orad        |1 1 I|FDA91AE007_CATCH|������ ������� �4||
C |R_urad        |4 4 K|_lcmpJ7170      |[]�������� ������ �����������|20|
C |R_itad        |4 4 K|_lcmpJ6848      |[]�������� ������ �����������|20|
C |R_evad        |4 4 K|_lcmpJ6839      |[]�������� ������ �����������|20|
C |R_axad        |4 4 I|FDA91AE006_POS  |��������� �� 20FDA91AE006||
C |R_exad        |4 4 K|_lcmpJ6826      |[]�������� ������ �����������|20|
C |L_obed        |1 1 O|FDA91AE006_CATCHED|������� ��������� �� 20FDA91AE006||
C |L_eded        |1 1 S|_qffJ6815*      |�������� ������ Q RS-��������  |F|
C |L_oded        |1 1 I|FDA91AE006_UNCATCH|��������� ������� �� 20FDA91AE006||
C |R_uded        |4 4 I|FDA91AE006_VZ01 |������� �� �� ��� Z||
C |L_afed        |1 1 I|FDA91AE006_CATCH|������ ������� �� 20FDA91AE006||
C |R_efed        |4 4 K|_lcmpJ6805      |[]�������� ������ �����������|20|
C |R_aked        |4 4 K|_lcmpJ6796      |[]�������� ������ �����������|20|
C |R_uled        |4 4 K|_lcmpJ6476      |[]�������� ������ �����������|20|
C |R_umed        |4 4 K|_lcmpJ6463      |[]�������� ������ �����������|1000|
C |R_aped        |4 4 I|FDA91AE014_POS  |��������� �� 20FDA91AE014||
C |R_eped        |4 4 K|_lcmpJ6181      |[]�������� ������ �����������|20|
C |L_ored        |1 1 O|FDA91AE014_CATCHED|������� ��������� �� 20FDA91AE014||
C |L_esed        |1 1 I|FDA91AE012_UNCATCH|��������� ������� �� 20FDA91AE012||
C |L_ised        |1 1 I|FDA91AE012_CATCH|������ ������� �� 20FDA91AE012||
C |L_used        |1 1 S|_qffJ6163*      |�������� ������ Q RS-��������  |F|
C |L_oted        |1 1 S|_qffJ6151*      |�������� ������ Q RS-��������  |F|
C |L_aved        |1 1 I|FDA91AE014_UNCATCH|��������� ������� �� 20FDA91AE014||
C |R_exed        |4 4 O|VZ01            |��������� ������� �� ��� Z||
C |R_ixed        |4 4 S|_ointJ6126*     |����� ����������� |0.0|
C |R_ibid        |4 4 I|FDA91AE012_VZ01 |������� �� �� ��� Z||
C |R_obid        |4 4 I|FDA91AE014_VZ01 |������� �� �� ��� Z||
C |L_adid        |1 1 I|FDA91AE014_CATCH|������ ������� �� 20FDA91AE014||
C |R_afid        |4 4 S|_ointJ5958*     |����� ����������� |`p_LOAD_Y`|
C |I_etid        |2 4 O|LINE_POS        |����� �� ������� ����������� �������||
C |R_avid        |4 4 O|VX01            |��������� ������� �� ��� X||
C |R_evid        |4 4 S|_ointJ5909*     |����� ����������� |`p_LOAD_X`|
C |R_ivid        |4 4 K|_lcmpJ2655      |[]�������� ������ �����������|4456|
C |R_uvid        |4 4 I|FDA60AE413VX02  |�������� �������� �������||
C |R_ixid        |4 4 I|FDA60AE408VX02  |�������� �������� �������||
C |R_abod        |4 4 K|_lcmpJ871       |[]�������� ������ �����������|1.0|
C |R_ibod        |4 4 O|VY01            |��������� ������� �� ��� Y||
C |C20_obod      |3 20 O|SECTION         |������� ������� �������||

      IMPLICIT NONE
      REAL*4 ext_deltat
      LOGICAL*1 L_e
      REAL*4 R_ad
      LOGICAL*1 L_af,L_uk,L_al,L_ol,L_em,L_op
      REAL*4 R_ir,R_or
      LOGICAL*1 L_ur,L_ot,L_av,L_ev
      REAL*4 R_uv,R_ax
      LOGICAL*1 L_ex,L_ox
      REAL*4 R_ede,R_ide
      LOGICAL*1 L_ode
      INTEGER*4 I_ofe,I_ike,I_ele,I_ame,I_ume,I_ape
      LOGICAL*1 L_upe,L_ite,L_ave,L_ove,L_exe,L_uxe,L_ibi
     &,L_adi,L_efi,L_aki,L_uki
      INTEGER*4 I_ili,I_oli
      REAL*4 R_omi,R_opi,R_upi,R_uri,R_isi,R_eti,R_ovi,R_ixi
     &,R_uxi,R_abo,R_ubo,R_edo,R_ido
      LOGICAL*1 L_odo,L_oko,L_uko
      REAL*4 R_elo,R_amo,R_omo,R_ipo,R_ero,R_aso,R_eso,R_uto
     &,R_avo,R_axo,R_uxo,R_ebu,R_ofu,R_ufu
      LOGICAL*1 L_aku
      REAL*4 R_uku,R_ulu,R_umu
      LOGICAL*1 L_eru
      REAL*4 R_iru,R_esu,R_atu,R_etu,R_uvu,R_axu
      LOGICAL*1 L_exu
      REAL*4 R_abad,R_adad,R_afad,R_ekad,R_alad,R_ulad,R_amad
      LOGICAL*1 L_ipad,L_upad,L_erad
      REAL*4 R_irad
      LOGICAL*1 L_orad
      REAL*4 R_urad,R_itad,R_evad,R_axad,R_exad
      LOGICAL*1 L_obed,L_eded,L_oded
      REAL*4 R_uded
      LOGICAL*1 L_afed
      REAL*4 R_efed,R_aked,R_uled,R_umed,R_aped,R_eped
      LOGICAL*1 L_ored,L_esed,L_ised,L_used,L_oted,L_aved
      REAL*4 R_exed,R_ixed,R_ibid,R_obid
      LOGICAL*1 L_adid
      REAL*4 R_afid
      INTEGER*4 I_etid
      REAL*4 R_avid,R_evid,R_ivid,R_uvid,R_ixid,R_abod,R_ibod
      CHARACTER*20 C20_obod
      End subroutine LODOCHKA_HANDLER
      End interface
