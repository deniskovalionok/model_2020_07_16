      Subroutine TVS_HANDLER(ext_deltat,I_u,I_od,R_ef,R_of
     &,R_ek,R_ok,R_el,R_ol,R_em,R_om,R_ep,R_op,R_er,R_or,R_es
     &,R_os,I_us,R_et,R_it,I_ev,R_ov,R_uv,R_ox,R_ux,I_ebe
     &,I_ade,I_ede,I_ude,I_afe,I_ofe,I_ufe,R_uke,R_ele,I_ule
     &,R_ime,R_ume,R_epe,I_are,R_ire,I_ase,R_ose,R_ate,R_ite
     &,I_eve,R_ove,I_exe,R_uxe,R_ebi,R_obi,I_idi,R_udi,I_ifi
     &,R_aki,R_iki,R_uki,I_oli,R_ami,R_omi,I_umi,R_epi,R_upi
     &,R_eri,R_uri,I_asi,R_isi,R_ati,R_iti,R_avi,I_evi,R_ovi
     &,R_uvi,R_axi,R_exi,R_ixi,R_oxi,R_uxi,R_abo,R_ebo,R_ibo
     &,R_obo,R_ubo,R_ado,R_edo,R_ido,R_odo,R_udo,R_afo,R_efo
     &,I_ifo,I_ofo,I_ufo,I_ako,I_eko,I_iko,I_oko,I_uko,I_alo
     &,I_elo,I_ilo,I_olo,I_ulo,I_amo,R_emo,R_imo,R_omo,R_umo
     &,R_apo,R_epo,R_ipo,R_opo,R_upo,R_aro,R_ero,R_ato,L_oto
     &,I_uto,L_evo,R_uvo,R_axo,R_exo,R_ixo,R_oxo,R_uxo,R_ubu
     &,I_adu,L_idu,R_afu,R_efu,R_ifu,R_ofu,R_ufu,R_aku,R_alu
     &,I_elu,L_olu,R_emu,R_imu,R_omu,R_umu,R_apu,R_epu,R_eru
     &,I_iru,L_uru,R_isu,R_osu,R_usu,R_atu,R_etu,R_itu,R_ivu
     &,I_ovu,L_axu,R_oxu,R_uxu,R_abad,R_ebad,R_ibad,R_obad
     &,R_odad,I_udad,L_efad,R_ufad,R_akad,R_ekad,R_ikad,R_okad
     &,R_ukad,R_ulad,I_amad,L_imad,R_apad,R_epad,R_ipad,R_opad
     &,R_upad,R_arad,R_asad,I_esad,L_osad,R_etad,R_itad,R_otad
     &,R_utad,R_avad,R_evad,R_exad,I_ixad,L_uxad,R_ibed,R_obed
     &,R_ubed,R_aded,R_eded,R_ided,R_ifed,I_ofed,L_aked,R_oked
     &,R_uked,R_aled,R_eled,R_iled,R_oled,R_omed,I_umed,L_eped
     &,R_uped,R_ared,R_ered,R_ired,R_ored,R_ured,R_ised,I_osed
     &,L_ated,R_oted,R_uted,R_aved,R_eved,R_ived,R_oved,R_exed
     &,I_ixed,L_uxed,R_ibid,R_obid,R_ubid,R_adid,R_edid,R_idid
     &,R_afid,I_efid,L_ofid,R_ekid,R_ikid,R_okid,R_ukid,R_alid
     &,R_elid,R_ulid,I_amid,L_imid,R_epid,R_ipid,R_opid,R_upid
     &,R_arid,R_erid,I_irid,I_asid,R_esid)
C |I_u           |2 4 O|FDD10AX004POSTL |��������� �����||
C |I_od          |2 4 O|FDD10AX004POSTR |��������� ������||
C |R_ef          |4 4 K|_lcmpJ3321      |[]�������� ������ �����������|90|
C |R_of          |4 4 K|_lcmpJ3320      |[]�������� ������ �����������|75|
C |R_ek          |4 4 K|_lcmpJ3310      |[]�������� ������ �����������|50|
C |R_ok          |4 4 K|_lcmpJ3309      |[]�������� ������ �����������|35|
C |R_el          |4 4 K|_lcmpJ3304      |[]�������� ������ �����������|20|
C |R_ol          |4 4 K|_lcmpJ3303      |[]�������� ������ �����������|5|
C |R_em          |4 4 K|_lcmpJ3171      |[]�������� ������ �����������|100|
C |R_om          |4 4 K|_lcmpJ3170      |[]�������� ������ �����������|90|
C |R_ep          |4 4 K|_lcmpJ3164      |[]�������� ������ �����������|65|
C |R_op          |4 4 K|_lcmpJ3163      |[]�������� ������ �����������|50|
C |R_er          |4 4 K|_lcmpJ3143      |[]�������� ������ �����������|35|
C |R_or          |4 4 K|_lcmpJ3142      |[]�������� ������ �����������|20|
C |R_es          |4 4 K|_lcmpJ3137      |[]�������� ������ �����������|2|
C |R_os          |4 4 K|_lcmpJ3136      |[]�������� ������ �����������|0|
C |I_us          |2 4 K|_cJ2724         |�������� ��������� �������������|0|
C |R_et          |4 4 K|_lcmpJ2722      |[]�������� ������ �����������|99|
C |R_it          |4 4 K|_lcmpJ2717      |[]�������� ������ �����������|0.1|
C |I_ev          |2 4 K|_cJ2710         |�������� ��������� �������������|0|
C |R_ov          |4 4 K|_lcmpJ2703      |[]�������� ������ �����������|0.1|
C |R_uv          |4 4 K|_lcmpJ2702      |[]�������� ������ �����������|99|
C |R_ox          |4 4 K|_lcmpJ2691      |[]�������� ������ �����������|0.1|
C |R_ux          |4 4 K|_lcmpJ2690      |[]�������� ������ �����������|99|
C |I_ebe         |2 4 K|_cJ2684         |�������� ��������� �������������|0|
C |I_ade         |2 4 K|_cJ2612         |�������� ��������� �������������|1|
C |I_ede         |2 4 K|_cJ2611         |�������� ��������� �������������|2|
C |I_ude         |2 4 K|_cJ2589         |�������� ��������� �������������|1|
C |I_afe         |2 4 K|_cJ2588         |�������� ��������� �������������|2|
C |I_ofe         |2 4 K|_cJ2568         |�������� ��������� �������������|1|
C |I_ufe         |2 4 K|_cJ2566         |�������� ��������� �������������|2|
C |R_uke         |4 4 K|_lcmpJ2529      |[]�������� ������ �����������|100|
C |R_ele         |4 4 K|_lcmpJ2528      |[]�������� ������ �����������|30|
C |I_ule         |2 4 O|FDD10AX008POSCLOSE|?||
C |R_ime         |4 4 K|_lcmpJ2427      |[]�������� ������ �����������|100|
C |R_ume         |4 4 K|_lcmpJ2426      |[]�������� ������ �����������|50|
C |R_epe         |4 4 K|_lcmpJ2419      |[]�������� ������ �����������|50|
C |I_are         |2 4 O|FDD10AX008POSOPEN|��������� �� 90 ��������||
C |R_ire         |4 4 K|_lcmpJ2411      |[]�������� ������ �����������|0|
C |I_ase         |2 4 O|FDD10AX007POSCLOSE|������� ������ �������� � �����������||
C |R_ose         |4 4 K|_lcmpJ2394      |[]�������� ������ �����������|100|
C |R_ate         |4 4 K|_lcmpJ2393      |[]�������� ������ �����������|50|
C |R_ite         |4 4 K|_lcmpJ2386      |[]�������� ������ �����������|50|
C |I_eve         |2 4 O|FDD10AX007POSOPEN|������� ��� �� ����������� � ���������||
C |R_ove         |4 4 K|_lcmpJ2378      |[]�������� ������ �����������|0|
C |I_exe         |2 4 O|FDD10AX006POSCLOSE|?||
C |R_uxe         |4 4 K|_lcmpJ2361      |[]�������� ������ �����������|100|
C |R_ebi         |4 4 K|_lcmpJ2360      |[]�������� ������ �����������|15|
C |R_obi         |4 4 K|_lcmpJ2353      |[]�������� ������ �����������|15|
C |I_idi         |2 4 O|FDD10AX006POSOPEN|����� ����� � ��������� ����������������||
C |R_udi         |4 4 K|_lcmpJ2345      |[]�������� ������ �����������|0|
C |I_ifi         |2 4 O|FDD10AX005POSCLOSE|?||
C |R_aki         |4 4 K|_lcmpJ2328      |[]�������� ������ �����������|100|
C |R_iki         |4 4 K|_lcmpJ2327      |[]�������� ������ �����������|15|
C |R_uki         |4 4 K|_lcmpJ2320      |[]�������� ������ �����������|15|
C |I_oli         |2 4 O|FDD10AX005POSOPEN|����� ����� � ��������� ����������������||
C |R_ami         |4 4 K|_lcmpJ2312      |[]�������� ������ �����������|0|
C |R_omi         |4 4 K|_lcmpJ2254      |[]�������� ������ �����������|30|
C |I_umi         |2 4 O|FDD10AX003POSOPEN|�������� ��� ������ ����||
C |R_epi         |4 4 K|_lcmpJ2247      |[]�������� ������ �����������|0|
C |R_upi         |4 4 K|_lcmpJ2231      |[]�������� ������ �����������|100|
C |R_eri         |4 4 K|_lcmpJ2230      |[]�������� ������ �����������|50|
C |R_uri         |4 4 K|_lcmpJ2223      |[]�������� ������ �����������|50|
C |I_asi         |2 4 O|FDD10AX002POSOPEN|����� ����������� ||
C |R_isi         |4 4 K|_lcmpJ2216      |[]�������� ������ �����������|0|
C |R_ati         |4 4 K|_lcmpJ2200      |[]�������� ������ �����������|100|
C |R_iti         |4 4 K|_lcmpJ2199      |[]�������� ������ �����������|50|
C |R_avi         |4 4 K|_lcmpJ2192      |[]�������� ������ �����������|50|
C |I_evi         |2 4 O|FDD10AX001POSOPEN|����� ���� �������� ����������� ||
C |R_ovi         |4 4 K|_lcmpJ2185      |[]�������� ������ �����������|0|
C |R_uvi         |4 4 K|_cJ1913         |�������� ��������� ����������|10|
C |R_axi         |4 4 K|_cJ1911         |�������� ��������� ����������|10|
C |R_exi         |4 4 K|_cJ1909         |�������� ��������� ����������|10|
C |R_ixi         |4 4 K|_cJ1907         |�������� ��������� ����������|10|
C |R_oxi         |4 4 K|_cJ1905         |�������� ��������� ����������|10|
C |R_uxi         |4 4 K|_cJ1903         |�������� ��������� ����������|10|
C |R_abo         |4 4 K|_cJ1894         |�������� ��������� ����������|0|
C |R_ebo         |4 4 K|_cJ1889         |�������� ��������� ����������|0|
C |R_ibo         |4 4 K|_cJ1885         |�������� ��������� ����������|0|
C |R_obo         |4 4 K|_cJ1852         |�������� ��������� ����������|10|
C |R_ubo         |4 4 K|_cJ1850         |�������� ��������� ����������|10|
C |R_ado         |4 4 K|_cJ1848         |�������� ��������� ����������|10|
C |R_edo         |4 4 K|_cJ1846         |�������� ��������� ����������|10|
C |R_ido         |4 4 K|_cJ1844         |�������� ��������� ����������|10|
C |R_odo         |4 4 K|_cJ1842         |�������� ��������� ����������|10|
C |R_udo         |4 4 K|_cJ1840         |�������� ��������� ����������|10|
C |R_afo         |4 4 K|_cJ1838         |�������� ��������� ����������|10|
C |R_efo         |4 4 K|_cJ1836         |�������� ��������� ����������|10|
C |I_ifo         |2 4 K|_cJ1811         |�������� ��������� �������������|15|
C |I_ofo         |2 4 K|_cJ1807         |�������� ��������� �������������|14|
C |I_ufo         |2 4 K|_cJ1803         |�������� ��������� �������������|13|
C |I_ako         |2 4 K|_cJ1799         |�������� ��������� �������������|12|
C |I_eko         |2 4 K|_cJ1795         |�������� ��������� �������������|11|
C |I_iko         |2 4 K|_cJ1791         |�������� ��������� �������������|10|
C |I_oko         |2 4 K|_cJ1787         |�������� ��������� �������������|9|
C |I_uko         |2 4 K|_cJ1783         |�������� ��������� �������������|8|
C |I_alo         |2 4 K|_cJ1779         |�������� ��������� �������������|7|
C |I_elo         |2 4 K|_cJ1775         |�������� ��������� �������������|6|
C |I_ilo         |2 4 K|_cJ1771         |�������� ��������� �������������|5|
C |I_olo         |2 4 K|_cJ1767         |�������� ��������� �������������|4|
C |I_ulo         |2 4 K|_cJ1763         |�������� ��������� �������������|3|
C |I_amo         |2 4 K|_cJ1749         |�������� ��������� �������������|2|
C |R_emo         |4 4 K|_cJ1684         |�������� ��������� ����������|0|
C |R_imo         |4 4 K|_cJ1678         |�������� ��������� ����������|0|
C |R_omo         |4 4 K|_cJ1666         |�������� ��������� ����������|0|
C |R_umo         |4 4 K|_cJ1656         |�������� ��������� ����������|0|
C |R_apo         |4 4 K|_cJ1646         |�������� ��������� ����������|0|
C |R_epo         |4 4 K|_cJ1635         |�������� ��������� ����������|0|
C |R_ipo         |4 4 K|_cJ1626         |�������� ��������� ����������|0|
C |R_opo         |4 4 K|_cJ1615         |�������� ��������� ����������|0|
C |R_upo         |4 4 K|_cJ1606         |�������� ��������� ����������|0|
C |R_aro         |4 4 K|_cJ1598         |�������� ��������� ����������|0|
C |R_ero         |4 4 K|_cJ1592         |�������� ��������� ����������|0|
C |R_ato         |4 4 K|_cJ1531         |�������� ��������� ����������|0|
C |L_oto         |1 1 I|FDD10AX001START |||
C |I_uto         |2 4 K|_cJ1090         |�������� ��������� �������������|16|
C |L_evo         |1 1 I|FDD10AX016START |||
C |R_uvo         |4 4 K|_lcmpJ1084      |[]�������� ������ �����������|99|
C |R_axo         |4 4 K|_uintJ1075      |����������� ������ ����������� ������|100|
C |R_exo         |4 4 K|_tintJ1075      |[���]�������� T �����������|10.0|
C |R_ixo         |4 4 O|_ointJ1075*     |�������� ������ ����������� |0.0|
C |R_oxo         |4 4 K|_lintJ1075      |����������� ������ ����������� �����|0|
C |R_uxo         |4 4 O|FDD10AX015SPEED |�������� �� ��������� 15||
C |R_ubu         |4 4 O|FDD10AX015POS   |��������� �� ��������� 15||
C |I_adu         |2 4 K|_cJ1044         |�������� ��������� �������������|15|
C |L_idu         |1 1 I|FDD10AX015START |||
C |R_afu         |4 4 K|_lcmpJ1038      |[]�������� ������ �����������|99|
C |R_efu         |4 4 K|_uintJ1029      |����������� ������ ����������� ������|100|
C |R_ifu         |4 4 K|_tintJ1029      |[���]�������� T �����������|10.0|
C |R_ofu         |4 4 O|_ointJ1029*     |�������� ������ ����������� |0.0|
C |R_ufu         |4 4 K|_lintJ1029      |����������� ������ ����������� �����|0|
C |R_aku         |4 4 O|FDD10AX014SPEED |�������� �� ��������� 14||
C |R_alu         |4 4 O|FDD10AX014POS   |��������� �� ��������� 14||
C |I_elu         |2 4 K|_cJ1014         |�������� ��������� �������������|14|
C |L_olu         |1 1 I|FDD10AX014START |||
C |R_emu         |4 4 K|_lcmpJ1008      |[]�������� ������ �����������|99|
C |R_imu         |4 4 K|_uintJ1000      |����������� ������ ����������� ������|100|
C |R_omu         |4 4 K|_tintJ1000      |[���]�������� T �����������|10.0|
C |R_umu         |4 4 O|_ointJ1000*     |�������� ������ ����������� |0.0|
C |R_apu         |4 4 K|_lintJ1000      |����������� ������ ����������� �����|0|
C |R_epu         |4 4 O|FDD10AX013SPEED |�������� �� ��������� 13||
C |R_eru         |4 4 O|FDD10AX013POS   |��������� �� ��������� 13||
C |I_iru         |2 4 K|_cJ987          |�������� ��������� �������������|13|
C |L_uru         |1 1 I|FDD10AX013START |||
C |R_isu         |4 4 K|_lcmpJ981       |[]�������� ������ �����������|99|
C |R_osu         |4 4 K|_uintJ973       |����������� ������ ����������� ������|100|
C |R_usu         |4 4 K|_tintJ973       |[���]�������� T �����������|10.0|
C |R_atu         |4 4 O|_ointJ973*      |�������� ������ ����������� |0.0|
C |R_etu         |4 4 K|_lintJ973       |����������� ������ ����������� �����|0|
C |R_itu         |4 4 O|FDD10AX012SPEED |�������� �� ��������� 12||
C |R_ivu         |4 4 O|FDD10AX012POS   |��������� �� ��������� 12||
C |I_ovu         |2 4 K|_cJ958          |�������� ��������� �������������|12|
C |L_axu         |1 1 I|FDD10AX012START |||
C |R_oxu         |4 4 K|_lcmpJ952       |[]�������� ������ �����������|99|
C |R_uxu         |4 4 K|_uintJ944       |����������� ������ ����������� ������|100|
C |R_abad        |4 4 K|_tintJ944       |[���]�������� T �����������|10.0|
C |R_ebad        |4 4 O|_ointJ944*      |�������� ������ ����������� |0.0|
C |R_ibad        |4 4 K|_lintJ944       |����������� ������ ����������� �����|0|
C |R_obad        |4 4 O|FDD10AX011SPEED |�������� �� ��������� 11||
C |R_odad        |4 4 O|FDD10AX011POS   |��������� �� ��������� 11||
C |I_udad        |2 4 K|_cJ930          |�������� ��������� �������������|11|
C |L_efad        |1 1 I|FDD10AX011START |||
C |R_ufad        |4 4 K|_lcmpJ924       |[]�������� ������ �����������|99|
C |R_akad        |4 4 K|_uintJ916       |����������� ������ ����������� ������|100|
C |R_ekad        |4 4 K|_tintJ916       |[���]�������� T �����������|10.0|
C |R_ikad        |4 4 O|_ointJ916*      |�������� ������ ����������� |0.0|
C |R_okad        |4 4 K|_lintJ916       |����������� ������ ����������� �����|0|
C |R_ukad        |4 4 O|FDD10AX010SPEED |�������� �� ��������� 10||
C |R_ulad        |4 4 O|FDD10AX010POS   |��������� �� ��������� 10||
C |I_amad        |2 4 K|_cJ901          |�������� ��������� �������������|10|
C |L_imad        |1 1 I|FDD10AX010START |||
C |R_apad        |4 4 K|_lcmpJ895       |[]�������� ������ �����������|99|
C |R_epad        |4 4 K|_uintJ887       |����������� ������ ����������� ������|100|
C |R_ipad        |4 4 K|_tintJ887       |[���]�������� T �����������|10.0|
C |R_opad        |4 4 O|_ointJ887*      |�������� ������ ����������� |0.0|
C |R_upad        |4 4 K|_lintJ887       |����������� ������ ����������� �����|0|
C |R_arad        |4 4 O|FDD10AX009SPEED |�������� �� ��������� 9||
C |R_asad        |4 4 O|FDD10AX009POS   |��������� �� ��������� 9||
C |I_esad        |2 4 K|_cJ873          |�������� ��������� �������������|9|
C |L_osad        |1 1 I|FDD10AX009START |||
C |R_etad        |4 4 K|_lcmpJ867       |[]�������� ������ �����������|99|
C |R_itad        |4 4 K|_uintJ858       |����������� ������ ����������� ������|100|
C |R_otad        |4 4 K|_tintJ858       |[���]�������� T �����������|10.0|
C |R_utad        |4 4 O|_ointJ858*      |�������� ������ ����������� |0.0|
C |R_avad        |4 4 K|_lintJ858       |����������� ������ ����������� �����|0|
C |R_evad        |4 4 O|FDD10AX008SPEED |�������� �� ��������� 8||
C |R_exad        |4 4 O|FDD10AX008POS   |��������� �� ��������� 8||
C |I_ixad        |2 4 K|_cJ420          |�������� ��������� �������������|8|
C |L_uxad        |1 1 I|FDD10AX008START |||
C |R_ibed        |4 4 K|_lcmpJ414       |[]�������� ������ �����������|99|
C |R_obed        |4 4 K|_uintJ405       |����������� ������ ����������� ������|100|
C |R_ubed        |4 4 K|_tintJ405       |[���]�������� T �����������|10.0|
C |R_aded        |4 4 O|_ointJ405*      |�������� ������ ����������� |0.0|
C |R_eded        |4 4 K|_lintJ405       |����������� ������ ����������� �����|0|
C |R_ided        |4 4 O|FDD10AX007SPEED |�������� �� ��������� 7||
C |R_ifed        |4 4 O|FDD10AX007POS   |��������� �� ��������� 7||
C |I_ofed        |2 4 K|_cJ386          |�������� ��������� �������������|7|
C |L_aked        |1 1 I|FDD10AX007START |||
C |R_oked        |4 4 K|_lcmpJ380       |[]�������� ������ �����������|99|
C |R_uked        |4 4 K|_uintJ372       |����������� ������ ����������� ������|100|
C |R_aled        |4 4 K|_tintJ372       |[���]�������� T �����������|10.0|
C |R_eled        |4 4 O|_ointJ372*      |�������� ������ ����������� |0.0|
C |R_iled        |4 4 K|_lintJ372       |����������� ������ ����������� �����|0|
C |R_oled        |4 4 O|FDD10AX006SPEED |�������� �� ��������� 6||
C |R_omed        |4 4 O|FDD10AX006POS   |��������� �� ��������� 6||
C |I_umed        |2 4 K|_cJ358          |�������� ��������� �������������|6|
C |L_eped        |1 1 I|FDD10AX006START |||
C |R_uped        |4 4 K|_lcmpJ352       |[]�������� ������ �����������|99|
C |R_ared        |4 4 K|_uintJ343       |����������� ������ ����������� ������|100|
C |R_ered        |4 4 K|_tintJ343       |[���]�������� T �����������|10.0|
C |R_ired        |4 4 O|_ointJ343*      |�������� ������ ����������� |0.0|
C |R_ored        |4 4 K|_lintJ343       |����������� ������ ����������� �����|0|
C |R_ured        |4 4 O|FDD10AX005SPEED |�������� �� ��������� 5||
C |R_ised        |4 4 O|FDD10AX005POS   |��������� �� ��������� 5||
C |I_osed        |2 4 K|_cJ200          |�������� ��������� �������������|5|
C |L_ated        |1 1 I|FDD10AX005START |||
C |R_oted        |4 4 K|_lcmpJ194       |[]�������� ������ �����������|99|
C |R_uted        |4 4 K|_uintJ186       |����������� ������ ����������� ������|100|
C |R_aved        |4 4 K|_tintJ186       |[���]�������� T �����������|10.0|
C |R_eved        |4 4 O|_ointJ186*      |�������� ������ ����������� |0.0|
C |R_ived        |4 4 K|_lintJ186       |����������� ������ ����������� �����|0|
C |R_oved        |4 4 O|FDD10AX004SPEED |�������� �� ��������� 4||
C |R_exed        |4 4 O|FDD10AX004POS   |��������� �� ��������� 4||
C |I_ixed        |2 4 K|_cJ172          |�������� ��������� �������������|4|
C |L_uxed        |1 1 I|FDD10AX004START |||
C |R_ibid        |4 4 K|_lcmpJ166       |[]�������� ������ �����������|99|
C |R_obid        |4 4 K|_uintJ157       |����������� ������ ����������� ������|100|
C |R_ubid        |4 4 K|_tintJ157       |[���]�������� T �����������|10.0|
C |R_adid        |4 4 O|_ointJ157*      |�������� ������ ����������� |0.0|
C |R_edid        |4 4 K|_lintJ157       |����������� ������ ����������� �����|0|
C |R_idid        |4 4 O|FDD10AX003SPEED |������� �� ��������� 3||
C |R_afid        |4 4 O|FDD10AX003POS   |��������� �� ��������� 3||
C |I_efid        |2 4 K|_cJ126          |�������� ��������� �������������|3|
C |L_ofid        |1 1 I|FDD10AX003START |||
C |R_ekid        |4 4 K|_lcmpJ120       |[]�������� ������ �����������|99|
C |R_ikid        |4 4 K|_uintJ111       |����������� ������ ����������� ������|100|
C |R_okid        |4 4 K|_tintJ111       |[���]�������� T �����������|10.0|
C |R_ukid        |4 4 O|_ointJ111*      |�������� ������ ����������� |0.0|
C |R_alid        |4 4 K|_lintJ111       |����������� ������ ����������� �����|0|
C |R_elid        |4 4 O|FDD10AX002SPEED |��������� �� ��������� 2||
C |R_ulid        |4 4 O|FDD10AX002POS   |��������� �� ��������� 2||
C |I_amid        |2 4 K|_cJ35           |�������� ��������� �������������|2|
C |L_imid        |1 1 I|FDD10AX002START |||
C |R_epid        |4 4 K|_lcmpJ29        |[]�������� ������ �����������|99|
C |R_ipid        |4 4 K|_uintJ11        |����������� ������ ����������� ������|100|
C |R_opid        |4 4 K|_tintJ11        |[���]�������� T �����������|10.0|
C |R_upid        |4 4 O|_ointJ11*       |�������� ������ ����������� |0.0|
C |R_arid        |4 4 K|_lintJ11        |����������� ������ ����������� �����|0|
C |R_erid        |4 4 O|FDD10AX001SPEED |�������� �� ��������� 1||
C |I_irid        |2 4 K|_cJ7            |�������� ��������� �������������|1|
C |I_asid        |2 4 O|POS_LINE        |��������� �� ����� (������)||
C |R_esid        |4 4 O|FDD10AX001POS   |��������� �� ��������� 1||

      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      LOGICAL*1 L0_e
      INTEGER*4 I0_i,I0_o,I_u
      LOGICAL*1 L0_ad
      INTEGER*4 I0_ed,I0_id,I_od
      LOGICAL*1 L0_ud,L0_af
      REAL*4 R_ef
      LOGICAL*1 L0_if
      REAL*4 R_of
      LOGICAL*1 L0_uf,L0_ak
      REAL*4 R_ek
      LOGICAL*1 L0_ik
      REAL*4 R_ok
      LOGICAL*1 L0_uk,L0_al
      REAL*4 R_el
      LOGICAL*1 L0_il
      REAL*4 R_ol
      LOGICAL*1 L0_ul,L0_am
      REAL*4 R_em
      LOGICAL*1 L0_im
      REAL*4 R_om
      LOGICAL*1 L0_um,L0_ap
      REAL*4 R_ep
      LOGICAL*1 L0_ip
      REAL*4 R_op
      LOGICAL*1 L0_up,L0_ar
      REAL*4 R_er
      LOGICAL*1 L0_ir
      REAL*4 R_or
      LOGICAL*1 L0_ur,L0_as
      REAL*4 R_es
      LOGICAL*1 L0_is
      REAL*4 R_os
      INTEGER*4 I_us,I0_at
      REAL*4 R_et,R_it
      LOGICAL*1 L0_ot,L0_ut,L0_av
      INTEGER*4 I_ev,I0_iv
      REAL*4 R_ov,R_uv
      LOGICAL*1 L0_ax,L0_ex,L0_ix
      REAL*4 R_ox,R_ux
      INTEGER*4 I0_abe,I_ebe
      LOGICAL*1 L0_ibe,L0_obe,L0_ube
      INTEGER*4 I_ade,I_ede,I0_ide,I0_ode,I_ude,I_afe,I0_efe
     &,I0_ife,I_ofe,I_ufe,I0_ake,I0_eke
      LOGICAL*1 L0_ike,L0_oke
      REAL*4 R_uke
      LOGICAL*1 L0_ale
      REAL*4 R_ele
      INTEGER*4 I0_ile,I0_ole,I_ule
      LOGICAL*1 L0_ame,L0_eme
      REAL*4 R_ime
      LOGICAL*1 L0_ome
      REAL*4 R_ume
      LOGICAL*1 L0_ape
      REAL*4 R_epe
      LOGICAL*1 L0_ipe
      INTEGER*4 I0_ope,I0_upe,I_are
      LOGICAL*1 L0_ere
      REAL*4 R_ire
      INTEGER*4 I0_ore,I0_ure,I_ase
      LOGICAL*1 L0_ese,L0_ise
      REAL*4 R_ose
      LOGICAL*1 L0_use
      REAL*4 R_ate
      LOGICAL*1 L0_ete
      REAL*4 R_ite
      LOGICAL*1 L0_ote
      INTEGER*4 I0_ute,I0_ave,I_eve
      LOGICAL*1 L0_ive
      REAL*4 R_ove
      INTEGER*4 I0_uve,I0_axe,I_exe
      LOGICAL*1 L0_ixe,L0_oxe
      REAL*4 R_uxe
      LOGICAL*1 L0_abi
      REAL*4 R_ebi
      LOGICAL*1 L0_ibi
      REAL*4 R_obi
      LOGICAL*1 L0_ubi
      INTEGER*4 I0_adi,I0_edi,I_idi
      LOGICAL*1 L0_odi
      REAL*4 R_udi
      INTEGER*4 I0_afi,I0_efi,I_ifi
      LOGICAL*1 L0_ofi,L0_ufi
      REAL*4 R_aki
      LOGICAL*1 L0_eki
      REAL*4 R_iki
      LOGICAL*1 L0_oki
      REAL*4 R_uki
      LOGICAL*1 L0_ali
      INTEGER*4 I0_eli,I0_ili,I_oli
      LOGICAL*1 L0_uli
      REAL*4 R_ami
      LOGICAL*1 L0_emi,L0_imi
      REAL*4 R_omi
      INTEGER*4 I_umi
      LOGICAL*1 L0_api
      REAL*4 R_epi
      LOGICAL*1 L0_ipi,L0_opi
      REAL*4 R_upi
      LOGICAL*1 L0_ari
      REAL*4 R_eri
      LOGICAL*1 L0_iri,L0_ori
      REAL*4 R_uri
      INTEGER*4 I_asi
      LOGICAL*1 L0_esi
      REAL*4 R_isi
      LOGICAL*1 L0_osi,L0_usi
      REAL*4 R_ati
      LOGICAL*1 L0_eti
      REAL*4 R_iti
      LOGICAL*1 L0_oti,L0_uti
      REAL*4 R_avi
      INTEGER*4 I_evi
      LOGICAL*1 L0_ivi
      REAL*4 R_ovi,R_uvi,R_axi,R_exi,R_ixi,R_oxi,R_uxi,R_abo
     &,R_ebo,R_ibo,R_obo,R_ubo,R_ado,R_edo,R_ido,R_odo,R_udo
     &,R_afo,R_efo
      INTEGER*4 I_ifo,I_ofo,I_ufo,I_ako,I_eko,I_iko,I_oko
     &,I_uko,I_alo,I_elo,I_ilo,I_olo,I_ulo,I_amo
      REAL*4 R_emo,R_imo,R_omo,R_umo,R_apo,R_epo,R_ipo,R_opo
     &,R_upo,R_aro,R_ero,R0_iro,R0_oro,R0_uro,R0_aso,R0_eso
     &,R0_iso,R0_oso,R0_uso,R_ato,R0_eto,R0_ito
      LOGICAL*1 L_oto
      INTEGER*4 I_uto,I0_avo
      LOGICAL*1 L_evo,L0_ivo,L0_ovo
      REAL*4 R_uvo,R_axo,R_exo,R_ixo,R_oxo,R_uxo,R0_abu,R0_ebu
      LOGICAL*1 L0_ibu
      INTEGER*4 I0_obu
      REAL*4 R_ubu
      INTEGER*4 I_adu,I0_edu
      LOGICAL*1 L_idu,L0_odu,L0_udu
      REAL*4 R_afu,R_efu,R_ifu,R_ofu,R_ufu,R_aku,R0_eku,R0_iku
      LOGICAL*1 L0_oku
      INTEGER*4 I0_uku
      REAL*4 R_alu
      INTEGER*4 I_elu,I0_ilu
      LOGICAL*1 L_olu,L0_ulu,L0_amu
      REAL*4 R_emu,R_imu,R_omu,R_umu,R_apu,R_epu,R0_ipu,R0_opu
      LOGICAL*1 L0_upu
      INTEGER*4 I0_aru
      REAL*4 R_eru
      INTEGER*4 I_iru,I0_oru
      LOGICAL*1 L_uru,L0_asu,L0_esu
      REAL*4 R_isu,R_osu,R_usu,R_atu,R_etu,R_itu,R0_otu,R0_utu
      LOGICAL*1 L0_avu
      INTEGER*4 I0_evu
      REAL*4 R_ivu
      INTEGER*4 I_ovu,I0_uvu
      LOGICAL*1 L_axu,L0_exu,L0_ixu
      REAL*4 R_oxu,R_uxu,R_abad,R_ebad,R_ibad,R_obad,R0_ubad
     &,R0_adad
      LOGICAL*1 L0_edad
      INTEGER*4 I0_idad
      REAL*4 R_odad
      INTEGER*4 I_udad,I0_afad
      LOGICAL*1 L_efad,L0_ifad,L0_ofad
      REAL*4 R_ufad,R_akad,R_ekad,R_ikad,R_okad,R_ukad,R0_alad
     &,R0_elad
      LOGICAL*1 L0_ilad
      INTEGER*4 I0_olad
      REAL*4 R_ulad
      INTEGER*4 I_amad,I0_emad
      LOGICAL*1 L_imad,L0_omad,L0_umad
      REAL*4 R_apad,R_epad,R_ipad,R_opad,R_upad,R_arad,R0_erad
     &,R0_irad
      LOGICAL*1 L0_orad
      INTEGER*4 I0_urad
      REAL*4 R_asad
      INTEGER*4 I_esad,I0_isad
      LOGICAL*1 L_osad,L0_usad,L0_atad
      REAL*4 R_etad,R_itad,R_otad,R_utad,R_avad,R_evad,R0_ivad
     &,R0_ovad
      LOGICAL*1 L0_uvad
      INTEGER*4 I0_axad
      REAL*4 R_exad
      INTEGER*4 I_ixad,I0_oxad
      LOGICAL*1 L_uxad,L0_abed,L0_ebed
      REAL*4 R_ibed,R_obed,R_ubed,R_aded,R_eded,R_ided,R0_oded
     &,R0_uded
      LOGICAL*1 L0_afed
      INTEGER*4 I0_efed
      REAL*4 R_ifed
      INTEGER*4 I_ofed,I0_ufed
      LOGICAL*1 L_aked,L0_eked,L0_iked
      REAL*4 R_oked,R_uked,R_aled,R_eled,R_iled,R_oled,R0_uled
     &,R0_amed
      LOGICAL*1 L0_emed
      INTEGER*4 I0_imed
      REAL*4 R_omed
      INTEGER*4 I_umed,I0_aped
      LOGICAL*1 L_eped,L0_iped,L0_oped
      REAL*4 R_uped,R_ared,R_ered,R_ired,R_ored,R_ured
      LOGICAL*1 L0_ased
      INTEGER*4 I0_esed
      REAL*4 R_ised
      INTEGER*4 I_osed,I0_used
      LOGICAL*1 L_ated,L0_eted,L0_ited
      REAL*4 R_oted,R_uted,R_aved,R_eved,R_ived,R_oved
      LOGICAL*1 L0_uved
      INTEGER*4 I0_axed
      REAL*4 R_exed
      INTEGER*4 I_ixed,I0_oxed
      LOGICAL*1 L_uxed,L0_abid,L0_ebid
      REAL*4 R_ibid,R_obid,R_ubid,R_adid,R_edid,R_idid
      LOGICAL*1 L0_odid
      INTEGER*4 I0_udid
      REAL*4 R_afid
      INTEGER*4 I_efid,I0_ifid
      LOGICAL*1 L_ofid,L0_ufid,L0_akid
      REAL*4 R_ekid,R_ikid,R_okid,R_ukid,R_alid,R_elid
      LOGICAL*1 L0_ilid
      INTEGER*4 I0_olid
      REAL*4 R_ulid
      INTEGER*4 I_amid,I0_emid
      LOGICAL*1 L_imid,L0_omid,L0_umid,L0_apid
      REAL*4 R_epid,R_ipid,R_opid,R_upid,R_arid,R_erid
      INTEGER*4 I_irid
      LOGICAL*1 L0_orid
      INTEGER*4 I0_urid,I_asid
      REAL*4 R_esid

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      I0_i = z'01000010'
C TVS_HANDLER.fmg( 563,1040):��������� ������������� IN (�������)
      I0_o = z'01000003'
C TVS_HANDLER.fmg( 563,1042):��������� ������������� IN (�������)
      I0_ed = z'01000010'
C TVS_HANDLER.fmg( 563,1113):��������� ������������� IN (�������)
      I0_id = z'01000003'
C TVS_HANDLER.fmg( 563,1115):��������� ������������� IN (�������)
      !��������� I0_at = TVS_HANDLERC?? /0/
      I0_at=I_us
C TVS_HANDLER.fmg( 259,1094):��������� ������������� IN
      !��������� I0_iv = TVS_HANDLERC?? /0/
      I0_iv=I_ev
C TVS_HANDLER.fmg( 258,1135):��������� ������������� IN
      !��������� I0_abe = TVS_HANDLERC?? /0/
      I0_abe=I_ebe
C TVS_HANDLER.fmg( 260,1176):��������� ������������� IN
      !��������� I0_ide = TVS_HANDLERC?? /1/
      I0_ide=I_ade
C TVS_HANDLER.fmg( 260,1167):��������� ������������� IN
      !��������� I0_ode = TVS_HANDLERC?? /2/
      I0_ode=I_ede
C TVS_HANDLER.fmg( 260,1190):��������� ������������� IN
      !��������� I0_efe = TVS_HANDLERC?? /1/
      I0_efe=I_ude
C TVS_HANDLER.fmg( 260,1126):��������� ������������� IN
      !��������� I0_ife = TVS_HANDLERC?? /2/
      I0_ife=I_afe
C TVS_HANDLER.fmg( 258,1149):��������� ������������� IN
      !��������� I0_eke = TVS_HANDLERC?? /1/
      I0_eke=I_ofe
C TVS_HANDLER.fmg( 261,1083):��������� ������������� IN
      !��������� I0_ake = TVS_HANDLERC?? /2/
      I0_ake=I_ufe
C TVS_HANDLER.fmg( 261,1109):��������� ������������� IN
      I0_ile = z'01000010'
C TVS_HANDLER.fmg( 251, 876):��������� ������������� IN (�������)
      I0_ole = z'01000003'
C TVS_HANDLER.fmg( 251, 878):��������� ������������� IN (�������)
      I0_ope = z'01000010'
C TVS_HANDLER.fmg( 250, 895):��������� ������������� IN (�������)
      I0_upe = z'01000003'
C TVS_HANDLER.fmg( 250, 897):��������� ������������� IN (�������)
      I0_ore = z'01000010'
C TVS_HANDLER.fmg( 251, 917):��������� ������������� IN (�������)
      I0_ure = z'01000003'
C TVS_HANDLER.fmg( 251, 919):��������� ������������� IN (�������)
      I0_ute = z'01000010'
C TVS_HANDLER.fmg( 250, 936):��������� ������������� IN (�������)
      I0_ave = z'01000003'
C TVS_HANDLER.fmg( 250, 938):��������� ������������� IN (�������)
      I0_uve = z'01000010'
C TVS_HANDLER.fmg( 252, 956):��������� ������������� IN (�������)
      I0_axe = z'01000003'
C TVS_HANDLER.fmg( 252, 958):��������� ������������� IN (�������)
      I0_adi = z'01000010'
C TVS_HANDLER.fmg( 251, 975):��������� ������������� IN (�������)
      I0_edi = z'01000003'
C TVS_HANDLER.fmg( 251, 977):��������� ������������� IN (�������)
      I0_afi = z'01000010'
C TVS_HANDLER.fmg( 253, 998):��������� ������������� IN (�������)
      I0_efi = z'01000003'
C TVS_HANDLER.fmg( 253,1000):��������� ������������� IN (�������)
      I0_eli = z'01000010'
C TVS_HANDLER.fmg( 252,1017):��������� ������������� IN (�������)
      I0_ili = z'01000003'
C TVS_HANDLER.fmg( 252,1019):��������� ������������� IN (�������)
      !��������� R0_uled = TVS_HANDLERC?? /10/
      R0_uled=R_uvi
C TVS_HANDLER.fmg(  60, 990):���������
      !��������� R0_iro = TVS_HANDLERC?? /10/
      R0_iro=R_axi
C TVS_HANDLER.fmg(  62,1028):���������
      !��������� R0_uro = TVS_HANDLERC?? /10/
      R0_uro=R_exi
C TVS_HANDLER.fmg(  58,1068):���������
      !��������� R0_eso = TVS_HANDLERC?? /10/
      R0_eso=R_ixi
C TVS_HANDLER.fmg(  59,1104):���������
      !��������� R0_oso = TVS_HANDLERC?? /10/
      R0_oso=R_oxi
C TVS_HANDLER.fmg(  55,1140):���������
      !��������� R0_eto = TVS_HANDLERC?? /10/
      R0_eto=R_uxi
C TVS_HANDLER.fmg(  66,1176):���������
      !��������� R0_aso = TVS_HANDLERC?? /0/
      R0_aso=R_abo
C TVS_HANDLER.fmg(  64,1070):���������
      !��������� R0_iso = TVS_HANDLERC?? /0/
      R0_iso=R_ebo
C TVS_HANDLER.fmg(  63,1106):���������
      !��������� R0_uso = TVS_HANDLERC?? /0/
      R0_uso=R_ibo
C TVS_HANDLER.fmg(  62,1142):���������
      !��������� R0_abu = TVS_HANDLERC?? /10/
      R0_abu=R_obo
C TVS_HANDLER.fmg(  62, 662):���������
      !��������� R0_eku = TVS_HANDLERC?? /10/
      R0_eku=R_ubo
C TVS_HANDLER.fmg(  61, 697):���������
      !��������� R0_ipu = TVS_HANDLERC?? /10/
      R0_ipu=R_ado
C TVS_HANDLER.fmg(  58, 734):���������
      !��������� R0_otu = TVS_HANDLERC?? /10/
      R0_otu=R_edo
C TVS_HANDLER.fmg(  58, 772):���������
      !��������� R0_ubad = TVS_HANDLERC?? /10/
      R0_ubad=R_ido
C TVS_HANDLER.fmg(  58, 812):���������
      !��������� R0_alad = TVS_HANDLERC?? /10/
      R0_alad=R_odo
C TVS_HANDLER.fmg(  58, 848):���������
      !��������� R0_erad = TVS_HANDLERC?? /10/
      R0_erad=R_udo
C TVS_HANDLER.fmg(  61, 884):���������
      !��������� R0_ivad = TVS_HANDLERC?? /10/
      R0_ivad=R_afo
C TVS_HANDLER.fmg(  59, 919):���������
      !��������� R0_oded = TVS_HANDLERC?? /10/
      R0_oded=R_efo
C TVS_HANDLER.fmg(  62, 953):���������
      !��������� I0_obu = TVS_HANDLERC?? /15/
      I0_obu=I_ifo
C TVS_HANDLER.fmg(  53, 656):��������� ������������� IN
      !��������� I0_uku = TVS_HANDLERC?? /14/
      I0_uku=I_ofo
C TVS_HANDLER.fmg(  53, 691):��������� ������������� IN
      !��������� I0_aru = TVS_HANDLERC?? /13/
      I0_aru=I_ufo
C TVS_HANDLER.fmg(  53, 728):��������� ������������� IN
      !��������� I0_evu = TVS_HANDLERC?? /12/
      I0_evu=I_ako
C TVS_HANDLER.fmg(  53, 766):��������� ������������� IN
      !��������� I0_idad = TVS_HANDLERC?? /11/
      I0_idad=I_eko
C TVS_HANDLER.fmg(  53, 806):��������� ������������� IN
      !��������� I0_olad = TVS_HANDLERC?? /10/
      I0_olad=I_iko
C TVS_HANDLER.fmg(  53, 842):��������� ������������� IN
      !��������� I0_urad = TVS_HANDLERC?? /9/
      I0_urad=I_oko
C TVS_HANDLER.fmg(  53, 878):��������� ������������� IN
      !��������� I0_axad = TVS_HANDLERC?? /8/
      I0_axad=I_uko
C TVS_HANDLER.fmg(  53, 913):��������� ������������� IN
      !��������� I0_efed = TVS_HANDLERC?? /7/
      I0_efed=I_alo
C TVS_HANDLER.fmg(  52, 947):��������� ������������� IN
      !��������� I0_imed = TVS_HANDLERC?? /6/
      I0_imed=I_elo
C TVS_HANDLER.fmg(  53, 984):��������� ������������� IN
      !��������� I0_esed = TVS_HANDLERC?? /5/
      I0_esed=I_ilo
C TVS_HANDLER.fmg(  52,1022):��������� ������������� IN
      !��������� I0_axed = TVS_HANDLERC?? /4/
      I0_axed=I_olo
C TVS_HANDLER.fmg(  51,1062):��������� ������������� IN
      !��������� I0_udid = TVS_HANDLERC?? /3/
      I0_udid=I_ulo
C TVS_HANDLER.fmg(  51,1098):��������� ������������� IN
      !��������� I0_olid = TVS_HANDLERC?? /2/
      I0_olid=I_amo
C TVS_HANDLER.fmg(  57,1127):��������� ������������� IN
      !��������� R0_ebu = TVS_HANDLERC?? /0/
      R0_ebu=R_emo
C TVS_HANDLER.fmg(  65, 664):���������
      !��������� R0_iku = TVS_HANDLERC?? /0/
      R0_iku=R_imo
C TVS_HANDLER.fmg(  64, 699):���������
      !��������� R0_opu = TVS_HANDLERC?? /0/
      R0_opu=R_omo
C TVS_HANDLER.fmg(  60, 736):���������
      !��������� R0_utu = TVS_HANDLERC?? /0/
      R0_utu=R_umo
C TVS_HANDLER.fmg(  61, 774):���������
      !��������� R0_adad = TVS_HANDLERC?? /0/
      R0_adad=R_apo
C TVS_HANDLER.fmg(  61, 814):���������
      !��������� R0_elad = TVS_HANDLERC?? /0/
      R0_elad=R_epo
C TVS_HANDLER.fmg(  61, 850):���������
      !��������� R0_irad = TVS_HANDLERC?? /0/
      R0_irad=R_ipo
C TVS_HANDLER.fmg(  65, 886):���������
      !��������� R0_ovad = TVS_HANDLERC?? /0/
      R0_ovad=R_opo
C TVS_HANDLER.fmg(  62, 921):���������
      !��������� R0_uded = TVS_HANDLERC?? /0/
      R0_uded=R_upo
C TVS_HANDLER.fmg(  65, 955):���������
      !��������� R0_amed = TVS_HANDLERC?? /0/
      R0_amed=R_aro
C TVS_HANDLER.fmg(  65, 992):���������
      !��������� R0_oro = TVS_HANDLERC?? /0/
      R0_oro=R_ero
C TVS_HANDLER.fmg(  67,1030):���������
      !��������� R0_ito = TVS_HANDLERC?? /0/
      R0_ito=R_ato
C TVS_HANDLER.fmg(  68,1178):���������
      !��������� I0_avo = TVS_HANDLERC?? /16/
      I0_avo=I_uto
C TVS_HANDLER.fmg( 119, 647):��������� ������������� IN
      !��������� I0_edu = TVS_HANDLERC?? /15/
      I0_edu=I_adu
C TVS_HANDLER.fmg( 119, 682):��������� ������������� IN
      !��������� I0_ilu = TVS_HANDLERC?? /14/
      I0_ilu=I_elu
C TVS_HANDLER.fmg( 119, 719):��������� ������������� IN
      !��������� I0_oru = TVS_HANDLERC?? /13/
      I0_oru=I_iru
C TVS_HANDLER.fmg( 119, 757):��������� ������������� IN
      !��������� I0_uvu = TVS_HANDLERC?? /12/
      I0_uvu=I_ovu
C TVS_HANDLER.fmg( 119, 797):��������� ������������� IN
      !��������� I0_afad = TVS_HANDLERC?? /11/
      I0_afad=I_udad
C TVS_HANDLER.fmg( 119, 833):��������� ������������� IN
      !��������� I0_emad = TVS_HANDLERC?? /10/
      I0_emad=I_amad
C TVS_HANDLER.fmg( 119, 869):��������� ������������� IN
      !��������� I0_isad = TVS_HANDLERC?? /9/
      I0_isad=I_esad
C TVS_HANDLER.fmg( 119, 904):��������� ������������� IN
      !��������� I0_oxad = TVS_HANDLERC?? /8/
      I0_oxad=I_ixad
C TVS_HANDLER.fmg( 119, 938):��������� ������������� IN
      !��������� I0_ufed = TVS_HANDLERC?? /7/
      I0_ufed=I_ofed
C TVS_HANDLER.fmg( 119, 975):��������� ������������� IN
      !��������� I0_aped = TVS_HANDLERC?? /6/
      I0_aped=I_umed
C TVS_HANDLER.fmg( 119,1013):��������� ������������� IN
      !��������� I0_used = TVS_HANDLERC?? /5/
      I0_used=I_osed
C TVS_HANDLER.fmg( 119,1053):��������� ������������� IN
      !��������� I0_oxed = TVS_HANDLERC?? /4/
      I0_oxed=I_ixed
C TVS_HANDLER.fmg( 119,1089):��������� ������������� IN
      !��������� I0_ifid = TVS_HANDLERC?? /3/
      I0_ifid=I_efid
C TVS_HANDLER.fmg( 119,1125):��������� ������������� IN
      !��������� I0_emid = TVS_HANDLERC?? /2/
      I0_emid=I_amid
C TVS_HANDLER.fmg( 119,1160):��������� ������������� IN
      !��������� I0_urid = TVS_HANDLERC?? /1/
      I0_urid=I_irid
C TVS_HANDLER.fmg(  58,1168):��������� ������������� IN
      L0_odid=I0_udid.eq.I_asid
C TVS_HANDLER.fmg(  62,1097):���������� �������������
C label 266  try266=try266-1
      if(L0_odid) then
         R_idid=R0_eso
      else
         R_idid=R0_iso
      endif
C TVS_HANDLER.fmg(  75,1104):���� RE IN LO CH7
      R_adid=R_adid+deltat/R_ubid*R_idid
      if(R_adid.gt.R_obid) then
         R_adid=R_obid
      elseif(R_adid.lt.R_edid) then
         R_adid=R_edid
      endif
C TVS_HANDLER.fmg(  86,1103):����������
      L0_ebid=R_adid.gt.R_ibid
C TVS_HANDLER.fmg( 111,1085):���������� >
      L0_abid = L0_ebid.AND.L0_odid.AND.L_uxed
C TVS_HANDLER.fmg( 119,1083):�
      if(L0_abid) then
         I_asid=I0_oxed
      endif
C TVS_HANDLER.fmg( 125,1088):���� � ������������� �������
      L0_uved=I0_axed.eq.I_asid
C TVS_HANDLER.fmg(  62,1061):���������� �������������
      if(L0_uved) then
         R_oved=R0_uro
      else
         R_oved=R0_aso
      endif
C TVS_HANDLER.fmg(  75,1068):���� RE IN LO CH7
      R_eved=R_eved+deltat/R_aved*R_oved
      if(R_eved.gt.R_uted) then
         R_eved=R_uted
      elseif(R_eved.lt.R_ived) then
         R_eved=R_ived
      endif
C TVS_HANDLER.fmg(  86,1067):����������
      L0_ited=R_eved.gt.R_oted
C TVS_HANDLER.fmg( 111,1049):���������� >
      L0_eted = L0_ited.AND.L0_uved.AND.L_ated
C TVS_HANDLER.fmg( 119,1047):�
      if(L0_eted) then
         I_asid=I0_used
      endif
C TVS_HANDLER.fmg( 125,1052):���� � ������������� �������
      L0_ased=I0_esed.eq.I_asid
C TVS_HANDLER.fmg(  62,1021):���������� �������������
      if(L0_ased) then
         R_ured=R0_iro
      else
         R_ured=R0_oro
      endif
C TVS_HANDLER.fmg(  75,1028):���� RE IN LO CH7
      R_ired=R_ired+deltat/R_ered*R_ured
      if(R_ired.gt.R_ared) then
         R_ired=R_ared
      elseif(R_ired.lt.R_ored) then
         R_ired=R_ored
      endif
C TVS_HANDLER.fmg(  86,1027):����������
      L0_oped=R_ired.gt.R_uped
C TVS_HANDLER.fmg( 111,1009):���������� >
      L0_iped = L0_oped.AND.L0_ased.AND.L_eped
C TVS_HANDLER.fmg( 119,1007):�
      if(L0_iped) then
         I_asid=I0_aped
      endif
C TVS_HANDLER.fmg( 125,1012):���� � ������������� �������
      L0_ilid=I_asid.eq.I0_olid
C TVS_HANDLER.fmg(  67,1128):���������� �������������
      if(L0_ilid) then
         R_elid=R0_oso
      else
         R_elid=R0_uso
      endif
C TVS_HANDLER.fmg(  73,1140):���� RE IN LO CH7
      R_ukid=R_ukid+deltat/R_okid*R_elid
      if(R_ukid.gt.R_ikid) then
         R_ukid=R_ikid
      elseif(R_ukid.lt.R_alid) then
         R_ukid=R_alid
      endif
C TVS_HANDLER.fmg(  86,1139):����������
      L0_akid=R_ukid.gt.R_ekid
C TVS_HANDLER.fmg( 111,1121):���������� >
      L0_ufid = L0_akid.AND.L0_ilid.AND.L_ofid
C TVS_HANDLER.fmg( 119,1119):�
      if(L0_ufid) then
         I_asid=I0_ifid
      endif
C TVS_HANDLER.fmg( 125,1124):���� � ������������� �������
      L0_orid=I_asid.eq.I0_urid
C TVS_HANDLER.fmg(  63,1169):���������� �������������
      L0_omid = L0_orid.AND.L_oto
C TVS_HANDLER.fmg(  71,1168):�
      if(L0_omid) then
         R_erid=R0_eto
      else
         R_erid=R0_ito
      endif
C TVS_HANDLER.fmg(  75,1176):���� RE IN LO CH7
      R_upid=R_upid+deltat/R_opid*R_erid
      if(R_upid.gt.R_ipid) then
         R_upid=R_ipid
      elseif(R_upid.lt.R_arid) then
         R_upid=R_arid
      endif
C TVS_HANDLER.fmg(  96,1175):����������
      L0_apid=R_upid.gt.R_epid
C TVS_HANDLER.fmg( 111,1156):���������� >
      L0_umid = L0_apid.AND.L0_omid.AND.L_imid
C TVS_HANDLER.fmg( 119,1154):�
      if(L0_umid) then
         I_asid=I0_emid
      endif
C TVS_HANDLER.fmg( 125,1159):���� � ������������� �������
      L0_oku=I0_uku.eq.I_asid
C TVS_HANDLER.fmg(  62, 690):���������� �������������
      if(L0_oku) then
         R_aku=R0_eku
      else
         R_aku=R0_iku
      endif
C TVS_HANDLER.fmg(  75, 697):���� RE IN LO CH7
      R_ofu=R_ofu+deltat/R_ifu*R_aku
      if(R_ofu.gt.R_efu) then
         R_ofu=R_efu
      elseif(R_ofu.lt.R_ufu) then
         R_ofu=R_ufu
      endif
C TVS_HANDLER.fmg(  86, 696):����������
      L0_udu=R_ofu.gt.R_afu
C TVS_HANDLER.fmg( 111, 678):���������� >
      L0_odu = L0_udu.AND.L0_oku.AND.L_idu
C TVS_HANDLER.fmg( 119, 676):�
      if(L0_odu) then
         I_asid=I0_edu
      endif
C TVS_HANDLER.fmg( 125, 681):���� � ������������� �������
      L0_ibu=I0_obu.eq.I_asid
C TVS_HANDLER.fmg(  62, 655):���������� �������������
      if(L0_ibu) then
         R_uxo=R0_abu
      else
         R_uxo=R0_ebu
      endif
C TVS_HANDLER.fmg(  75, 662):���� RE IN LO CH7
      R_ixo=R_ixo+deltat/R_exo*R_uxo
      if(R_ixo.gt.R_axo) then
         R_ixo=R_axo
      elseif(R_ixo.lt.R_oxo) then
         R_ixo=R_oxo
      endif
C TVS_HANDLER.fmg(  86, 661):����������
      L0_ovo=R_ixo.gt.R_uvo
C TVS_HANDLER.fmg( 111, 643):���������� >
      L0_ivo = L0_ovo.AND.L0_ibu.AND.L_evo
C TVS_HANDLER.fmg( 119, 641):�
      if(L0_ivo) then
         I_asid=I0_avo
      endif
C TVS_HANDLER.fmg( 125, 646):���� � ������������� �������
      L0_emed=I0_imed.eq.I_asid
C TVS_HANDLER.fmg(  62, 983):���������� �������������
      if(L0_emed) then
         R_oled=R0_uled
      else
         R_oled=R0_amed
      endif
C TVS_HANDLER.fmg(  75, 990):���� RE IN LO CH7
      R_eled=R_eled+deltat/R_aled*R_oled
      if(R_eled.gt.R_uked) then
         R_eled=R_uked
      elseif(R_eled.lt.R_iled) then
         R_eled=R_iled
      endif
C TVS_HANDLER.fmg(  86, 989):����������
      L0_iked=R_eled.gt.R_oked
C TVS_HANDLER.fmg( 111, 971):���������� >
      L0_eked = L0_iked.AND.L0_emed.AND.L_aked
C TVS_HANDLER.fmg( 119, 969):�
      if(L0_eked) then
         I_asid=I0_ufed
      endif
C TVS_HANDLER.fmg( 125, 974):���� � ������������� �������
      L0_upu=I0_aru.eq.I_asid
C TVS_HANDLER.fmg(  62, 727):���������� �������������
      if(L0_upu) then
         R_epu=R0_ipu
      else
         R_epu=R0_opu
      endif
C TVS_HANDLER.fmg(  75, 734):���� RE IN LO CH7
      R_umu=R_umu+deltat/R_omu*R_epu
      if(R_umu.gt.R_imu) then
         R_umu=R_imu
      elseif(R_umu.lt.R_apu) then
         R_umu=R_apu
      endif
C TVS_HANDLER.fmg(  86, 733):����������
      L0_amu=R_umu.gt.R_emu
C TVS_HANDLER.fmg( 111, 715):���������� >
      L0_ulu = L0_amu.AND.L0_upu.AND.L_olu
C TVS_HANDLER.fmg( 119, 713):�
      if(L0_ulu) then
         I_asid=I0_ilu
      endif
C TVS_HANDLER.fmg( 125, 718):���� � ������������� �������
      L0_avu=I0_evu.eq.I_asid
C TVS_HANDLER.fmg(  62, 765):���������� �������������
      if(L0_avu) then
         R_itu=R0_otu
      else
         R_itu=R0_utu
      endif
C TVS_HANDLER.fmg(  75, 772):���� RE IN LO CH7
      R_atu=R_atu+deltat/R_usu*R_itu
      if(R_atu.gt.R_osu) then
         R_atu=R_osu
      elseif(R_atu.lt.R_etu) then
         R_atu=R_etu
      endif
C TVS_HANDLER.fmg(  86, 771):����������
      L0_esu=R_atu.gt.R_isu
C TVS_HANDLER.fmg( 111, 753):���������� >
      L0_asu = L0_esu.AND.L0_avu.AND.L_uru
C TVS_HANDLER.fmg( 119, 751):�
      if(L0_asu) then
         I_asid=I0_oru
      endif
C TVS_HANDLER.fmg( 125, 756):���� � ������������� �������
      L0_afed=I0_efed.eq.I_asid
C TVS_HANDLER.fmg(  62, 946):���������� �������������
      if(L0_afed) then
         R_ided=R0_oded
      else
         R_ided=R0_uded
      endif
C TVS_HANDLER.fmg(  75, 953):���� RE IN LO CH7
      R_aded=R_aded+deltat/R_ubed*R_ided
      if(R_aded.gt.R_obed) then
         R_aded=R_obed
      elseif(R_aded.lt.R_eded) then
         R_aded=R_eded
      endif
C TVS_HANDLER.fmg(  86, 952):����������
      L0_ebed=R_aded.gt.R_ibed
C TVS_HANDLER.fmg( 111, 934):���������� >
      L0_abed = L0_ebed.AND.L0_afed.AND.L_uxad
C TVS_HANDLER.fmg( 119, 932):�
      if(L0_abed) then
         I_asid=I0_oxad
      endif
C TVS_HANDLER.fmg( 125, 937):���� � ������������� �������
      L0_uvad=I0_axad.eq.I_asid
C TVS_HANDLER.fmg(  62, 912):���������� �������������
      if(L0_uvad) then
         R_evad=R0_ivad
      else
         R_evad=R0_ovad
      endif
C TVS_HANDLER.fmg(  75, 919):���� RE IN LO CH7
      R_utad=R_utad+deltat/R_otad*R_evad
      if(R_utad.gt.R_itad) then
         R_utad=R_itad
      elseif(R_utad.lt.R_avad) then
         R_utad=R_avad
      endif
C TVS_HANDLER.fmg(  86, 918):����������
      L0_atad=R_utad.gt.R_etad
C TVS_HANDLER.fmg( 111, 900):���������� >
      L0_usad = L0_atad.AND.L0_uvad.AND.L_osad
C TVS_HANDLER.fmg( 119, 898):�
      if(L0_usad) then
         I_asid=I0_isad
      endif
C TVS_HANDLER.fmg( 125, 903):���� � ������������� �������
      L0_orad=I0_urad.eq.I_asid
C TVS_HANDLER.fmg(  62, 877):���������� �������������
      if(L0_orad) then
         R_arad=R0_erad
      else
         R_arad=R0_irad
      endif
C TVS_HANDLER.fmg(  75, 884):���� RE IN LO CH7
      R_opad=R_opad+deltat/R_ipad*R_arad
      if(R_opad.gt.R_epad) then
         R_opad=R_epad
      elseif(R_opad.lt.R_upad) then
         R_opad=R_upad
      endif
C TVS_HANDLER.fmg(  86, 883):����������
      L0_umad=R_opad.gt.R_apad
C TVS_HANDLER.fmg( 111, 865):���������� >
      L0_omad = L0_umad.AND.L0_orad.AND.L_imad
C TVS_HANDLER.fmg( 119, 863):�
      if(L0_omad) then
         I_asid=I0_emad
      endif
C TVS_HANDLER.fmg( 125, 868):���� � ������������� �������
      L0_ilad=I0_olad.eq.I_asid
C TVS_HANDLER.fmg(  62, 841):���������� �������������
      if(L0_ilad) then
         R_ukad=R0_alad
      else
         R_ukad=R0_elad
      endif
C TVS_HANDLER.fmg(  75, 848):���� RE IN LO CH7
      R_ikad=R_ikad+deltat/R_ekad*R_ukad
      if(R_ikad.gt.R_akad) then
         R_ikad=R_akad
      elseif(R_ikad.lt.R_okad) then
         R_ikad=R_okad
      endif
C TVS_HANDLER.fmg(  86, 847):����������
      L0_ofad=R_ikad.gt.R_ufad
C TVS_HANDLER.fmg( 111, 829):���������� >
      L0_ifad = L0_ofad.AND.L0_ilad.AND.L_efad
C TVS_HANDLER.fmg( 119, 827):�
      if(L0_ifad) then
         I_asid=I0_afad
      endif
C TVS_HANDLER.fmg( 125, 832):���� � ������������� �������
      L0_edad=I0_idad.eq.I_asid
C TVS_HANDLER.fmg(  62, 805):���������� �������������
      if(L0_edad) then
         R_obad=R0_ubad
      else
         R_obad=R0_adad
      endif
C TVS_HANDLER.fmg(  75, 812):���� RE IN LO CH7
      R_ebad=R_ebad+deltat/R_abad*R_obad
      if(R_ebad.gt.R_uxu) then
         R_ebad=R_uxu
      elseif(R_ebad.lt.R_ibad) then
         R_ebad=R_ibad
      endif
C TVS_HANDLER.fmg(  86, 811):����������
      L0_ixu=R_ebad.gt.R_oxu
C TVS_HANDLER.fmg( 111, 793):���������� >
      L0_exu = L0_ixu.AND.L0_edad.AND.L_axu
C TVS_HANDLER.fmg( 119, 791):�
      if(L0_exu) then
         I_asid=I0_uvu
      endif
C TVS_HANDLER.fmg( 125, 796):���� � ������������� �������
      if(L0_edad) then
         R_odad=R_ebad
      endif
C TVS_HANDLER.fmg( 125, 812):���� � ������������� �������
      if(L0_ilad) then
         R_ulad=R_ikad
      endif
C TVS_HANDLER.fmg( 125, 848):���� � ������������� �������
      if(L0_orad) then
         R_asad=R_opad
      endif
C TVS_HANDLER.fmg( 125, 884):���� � ������������� �������
      if(L0_uvad) then
         R_exad=R_utad
      endif
C TVS_HANDLER.fmg( 125, 919):���� � ������������� �������
      L0_eme=R_exad.lt.R_ime
C TVS_HANDLER.fmg( 214, 867):���������� <
      L0_ome=R_exad.gt.R_ume
C TVS_HANDLER.fmg( 214, 876):���������� >
      L0_ame = L0_ome.AND.L0_eme
C TVS_HANDLER.fmg( 223, 873):�
      if(L0_ame) then
         I_ule=I0_ile
      else
         I_ule=I0_ole
      endif
C TVS_HANDLER.fmg( 254, 876):���� RE IN LO CH7
      L0_ape=R_exad.lt.R_epe
C TVS_HANDLER.fmg( 214, 886):���������� <
      L0_ere=R_exad.gt.R_ire
C TVS_HANDLER.fmg( 214, 895):���������� >
      L0_ipe = L0_ere.AND.L0_ape
C TVS_HANDLER.fmg( 223, 892):�
      if(L0_ipe) then
         I_are=I0_ope
      else
         I_are=I0_upe
      endif
C TVS_HANDLER.fmg( 253, 895):���� RE IN LO CH7
      if(L0_afed) then
         R_ifed=R_aded
      endif
C TVS_HANDLER.fmg( 125, 953):���� � ������������� �������
      L0_use=R_ifed.gt.R_ate
C TVS_HANDLER.fmg( 214, 917):���������� >
      L0_ise=R_ifed.lt.R_ose
C TVS_HANDLER.fmg( 214, 908):���������� <
      L0_ese = L0_use.AND.L0_ise
C TVS_HANDLER.fmg( 223, 914):�
      if(L0_ese) then
         I_ase=I0_ore
      else
         I_ase=I0_ure
      endif
C TVS_HANDLER.fmg( 254, 917):���� RE IN LO CH7
      L0_ete=R_ifed.lt.R_ite
C TVS_HANDLER.fmg( 214, 927):���������� <
      L0_ive=R_ifed.gt.R_ove
C TVS_HANDLER.fmg( 214, 936):���������� >
      L0_ote = L0_ive.AND.L0_ete
C TVS_HANDLER.fmg( 223, 933):�
      if(L0_ote) then
         I_eve=I0_ute
      else
         I_eve=I0_ave
      endif
C TVS_HANDLER.fmg( 253, 936):���� RE IN LO CH7
      if(L0_avu) then
         R_ivu=R_atu
      endif
C TVS_HANDLER.fmg( 125, 772):���� � ������������� �������
      if(L0_upu) then
         R_eru=R_umu
      endif
C TVS_HANDLER.fmg( 125, 734):���� � ������������� �������
      if(L0_emed) then
         R_omed=R_eled
      endif
C TVS_HANDLER.fmg( 125, 990):���� � ������������� �������
      L0_oxe=R_omed.lt.R_uxe
C TVS_HANDLER.fmg( 215, 947):���������� <
      L0_abi=R_omed.gt.R_ebi
C TVS_HANDLER.fmg( 215, 956):���������� >
      L0_ixe = L0_abi.AND.L0_oxe
C TVS_HANDLER.fmg( 224, 953):�
      if(L0_ixe) then
         I_exe=I0_uve
      else
         I_exe=I0_axe
      endif
C TVS_HANDLER.fmg( 255, 956):���� RE IN LO CH7
      L0_ibi=R_omed.lt.R_obi
C TVS_HANDLER.fmg( 215, 966):���������� <
      L0_odi=R_omed.gt.R_udi
C TVS_HANDLER.fmg( 215, 975):���������� >
      L0_ubi = L0_odi.AND.L0_ibi
C TVS_HANDLER.fmg( 224, 972):�
      if(L0_ubi) then
         I_idi=I0_adi
      else
         I_idi=I0_edi
      endif
C TVS_HANDLER.fmg( 254, 975):���� RE IN LO CH7
      if(L0_ibu) then
         R_ubu=R_ixo
      endif
C TVS_HANDLER.fmg( 125, 662):���� � ������������� �������
      if(L0_oku) then
         R_alu=R_ofu
      endif
C TVS_HANDLER.fmg( 125, 697):���� � ������������� �������
      if(L0_omid) then
         R_esid=R_upid
      endif
C TVS_HANDLER.fmg( 125,1176):���� � ������������� �������
      L0_ivi=R_esid.gt.R_ovi
C TVS_HANDLER.fmg( 219,1183):���������� >
      L0_uti=R_esid.lt.R_avi
C TVS_HANDLER.fmg( 219,1174):���������� <
      L0_oti = L0_ivi.AND.L0_uti
C TVS_HANDLER.fmg( 228,1180):�
      if(L0_oti) then
         I_evi=I0_ode
      endif
C TVS_HANDLER.fmg( 267,1189):���� � ������������� �������
      L0_obe=R_esid.lt.R_ox
C TVS_HANDLER.fmg( 240,1174):���������� <
      L0_ibe=R_esid.gt.R_ux
C TVS_HANDLER.fmg( 240,1167):���������� >
      L0_ube = L0_obe.OR.L0_ibe
C TVS_HANDLER.fmg( 250,1171):���
      if(L0_ube) then
         I_evi=I0_abe
      endif
C TVS_HANDLER.fmg( 267,1175):���� � ������������� �������
      L0_eti=R_esid.gt.R_iti
C TVS_HANDLER.fmg( 219,1164):���������� >
      L0_usi=R_esid.lt.R_ati
C TVS_HANDLER.fmg( 219,1155):���������� <
      L0_osi = L0_eti.AND.L0_usi
C TVS_HANDLER.fmg( 228,1161):�
      if(L0_osi) then
         I_evi=I0_ide
      endif
C TVS_HANDLER.fmg( 268,1166):���� � ������������� �������
      if(L0_ilid) then
         R_ulid=R_ukid
      endif
C TVS_HANDLER.fmg( 125,1140):���� � ������������� �������
      L0_opi=R_ulid.lt.R_upi
C TVS_HANDLER.fmg( 218,1114):���������� <
      L0_ari=R_ulid.gt.R_eri
C TVS_HANDLER.fmg( 218,1123):���������� >
      L0_ipi = L0_ari.AND.L0_opi
C TVS_HANDLER.fmg( 227,1120):�
      if(L0_ipi) then
         I_asi=I0_efe
      endif
C TVS_HANDLER.fmg( 266,1125):���� � ������������� �������
      L0_ex=R_ulid.lt.R_ov
C TVS_HANDLER.fmg( 241,1133):���������� <
      L0_ax=R_ulid.gt.R_uv
C TVS_HANDLER.fmg( 241,1126):���������� >
      L0_ix = L0_ex.OR.L0_ax
C TVS_HANDLER.fmg( 251,1130):���
      if(L0_ix) then
         I_asi=I0_iv
      endif
C TVS_HANDLER.fmg( 264,1134):���� � ������������� �������
      L0_ori=R_ulid.lt.R_uri
C TVS_HANDLER.fmg( 218,1133):���������� <
      L0_esi=R_ulid.gt.R_isi
C TVS_HANDLER.fmg( 218,1142):���������� >
      L0_iri = L0_esi.AND.L0_ori
C TVS_HANDLER.fmg( 227,1139):�
      if(L0_iri) then
         I_asi=I0_ife
      endif
C TVS_HANDLER.fmg( 265,1148):���� � ������������� �������
      if(L0_ased) then
         R_ised=R_ired
      endif
C TVS_HANDLER.fmg( 125,1028):���� � ������������� �������
      L0_eki=R_ised.gt.R_iki
C TVS_HANDLER.fmg( 216, 998):���������� >
      L0_ufi=R_ised.lt.R_aki
C TVS_HANDLER.fmg( 216, 989):���������� <
      L0_ofi = L0_eki.AND.L0_ufi
C TVS_HANDLER.fmg( 225, 995):�
      if(L0_ofi) then
         I_ifi=I0_afi
      else
         I_ifi=I0_efi
      endif
C TVS_HANDLER.fmg( 256, 998):���� RE IN LO CH7
      L0_oki=R_ised.lt.R_uki
C TVS_HANDLER.fmg( 216,1008):���������� <
      L0_uli=R_ised.gt.R_ami
C TVS_HANDLER.fmg( 216,1017):���������� >
      L0_ali = L0_uli.AND.L0_oki
C TVS_HANDLER.fmg( 225,1014):�
      if(L0_ali) then
         I_oli=I0_eli
      else
         I_oli=I0_ili
      endif
C TVS_HANDLER.fmg( 255,1017):���� RE IN LO CH7
      if(L0_uved) then
         R_exed=R_eved
      endif
C TVS_HANDLER.fmg( 125,1068):���� � ������������� �������
      L0_im=R_exed.gt.R_om
C TVS_HANDLER.fmg( 516,1058):���������� >
      L0_am=R_exed.lt.R_em
C TVS_HANDLER.fmg( 516,1049):���������� <
      L0_ul = L0_im.AND.L0_am
C TVS_HANDLER.fmg( 525,1055):�
      L0_ir=R_exed.gt.R_or
C TVS_HANDLER.fmg( 517,1093):���������� >
      L0_ar=R_exed.lt.R_er
C TVS_HANDLER.fmg( 517,1084):���������� <
      L0_up = L0_ir.AND.L0_ar
C TVS_HANDLER.fmg( 526,1090):�
      L0_is=R_exed.gt.R_os
C TVS_HANDLER.fmg( 517,1113):���������� >
      L0_as=R_exed.lt.R_es
C TVS_HANDLER.fmg( 517,1104):���������� <
      L0_ur = L0_is.AND.L0_as
C TVS_HANDLER.fmg( 526,1110):�
      L0_ip=R_exed.gt.R_op
C TVS_HANDLER.fmg( 516,1076):���������� >
      L0_ap=R_exed.lt.R_ep
C TVS_HANDLER.fmg( 516,1067):���������� <
      L0_um = L0_ip.AND.L0_ap
C TVS_HANDLER.fmg( 525,1073):�
      L0_ad = L0_ur.OR.L0_up.OR.L0_um.OR.L0_ul
C TVS_HANDLER.fmg( 552,1093):���
      if(L0_ad) then
         I_od=I0_ed
      else
         I_od=I0_id
      endif
C TVS_HANDLER.fmg( 566,1113):���� RE IN LO CH7
      L0_ik=R_exed.gt.R_ok
C TVS_HANDLER.fmg( 515,1020):���������� >
      L0_ak=R_exed.lt.R_ek
C TVS_HANDLER.fmg( 515,1011):���������� <
      L0_uf = L0_ik.AND.L0_ak
C TVS_HANDLER.fmg( 524,1017):�
      L0_il=R_exed.gt.R_ol
C TVS_HANDLER.fmg( 515,1040):���������� >
      L0_al=R_exed.lt.R_el
C TVS_HANDLER.fmg( 515,1031):���������� <
      L0_uk = L0_il.AND.L0_al
C TVS_HANDLER.fmg( 524,1037):�
      L0_if=R_exed.gt.R_of
C TVS_HANDLER.fmg( 514,1003):���������� >
      L0_af=R_exed.lt.R_ef
C TVS_HANDLER.fmg( 514, 994):���������� <
      L0_ud = L0_if.AND.L0_af
C TVS_HANDLER.fmg( 523,1000):�
      L0_e = L0_uk.OR.L0_uf.OR.L0_ud
C TVS_HANDLER.fmg( 549,1017):���
      if(L0_e) then
         I_u=I0_i
      else
         I_u=I0_o
      endif
C TVS_HANDLER.fmg( 566,1040):���� RE IN LO CH7
      if(L0_odid) then
         R_afid=R_adid
      endif
C TVS_HANDLER.fmg( 125,1104):���� � ������������� �������
      L0_imi=R_afid.lt.R_omi
C TVS_HANDLER.fmg( 217,1092):���������� <
      L0_api=R_afid.gt.R_epi
C TVS_HANDLER.fmg( 217,1101):���������� >
      L0_emi = L0_api.AND.L0_imi
C TVS_HANDLER.fmg( 226,1098):�
      if(L0_emi) then
         I_umi=I0_ake
      endif
C TVS_HANDLER.fmg( 267,1108):���� � ������������� �������
      L0_ut=R_afid.lt.R_it
C TVS_HANDLER.fmg( 241,1092):���������� <
      L0_ot=R_afid.gt.R_et
C TVS_HANDLER.fmg( 241,1085):���������� >
      L0_av = L0_ut.OR.L0_ot
C TVS_HANDLER.fmg( 251,1089):���
      if(L0_av) then
         I_umi=I0_at
      endif
C TVS_HANDLER.fmg( 265,1093):���� � ������������� �������
      L0_oke=R_afid.lt.R_uke
C TVS_HANDLER.fmg( 217,1072):���������� <
      L0_ale=R_afid.gt.R_ele
C TVS_HANDLER.fmg( 217,1081):���������� >
      L0_ike = L0_ale.AND.L0_oke
C TVS_HANDLER.fmg( 226,1078):�
      if(L0_ike) then
         I_umi=I0_eke
      endif
C TVS_HANDLER.fmg( 267,1082):���� � ������������� �������
      End
