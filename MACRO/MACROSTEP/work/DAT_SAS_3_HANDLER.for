      Subroutine DAT_SAS_3_HANDLER(ext_deltat,R_od,R_ud,L_af
     &,R_uf,R_ak,I_al,R_el,R_il,R_ap,R_ep,L_ip,R_er,R_ir,I_is
     &,R_os,R_us,R_iv,R_ov,L_uv,R_ox,R_ux,I_ube,R_ade,R_ede
     &,R_ufe,R_ake,L_eke,R_ale,R_ele,I_eme,R_ime,R_ome)
C |R_od          |4 4 I|LOWER_warning_setpoint4|||
C |R_ud          |4 4 I|LOWER_alarm_setpoint4|||
C |L_af          |1 1 I|fault4          |�����4||
C |R_uf          |4 4 I|UPPER_alarm_setpoint4|||
C |R_ak          |4 4 I|UPPER_warning_setpoint4|||
C |I_al          |2 4 O|dat4_color      |���� ��������4||
C |R_el          |4 4 O|KKS4            |�����4||
C |R_il          |4 4 I|input4          |����4||
C |R_ap          |4 4 I|LOWER_warning_setpoint3|||
C |R_ep          |4 4 I|LOWER_alarm_setpoint3|||
C |L_ip          |1 1 I|fault3          |�����3||
C |R_er          |4 4 I|UPPER_alarm_setpoint3|||
C |R_ir          |4 4 I|UPPER_warning_setpoint3|||
C |I_is          |2 4 O|dat3_color      |���� ��������3||
C |R_os          |4 4 O|KKS3            |�����3||
C |R_us          |4 4 I|input3          |����3||
C |R_iv          |4 4 I|LOWER_warning_setpoint2|||
C |R_ov          |4 4 I|LOWER_alarm_setpoint2|||
C |L_uv          |1 1 I|fault2          |�����2||
C |R_ox          |4 4 I|UPPER_alarm_setpoint2|||
C |R_ux          |4 4 I|UPPER_warning_setpoint2|||
C |I_ube         |2 4 O|dat2_color      |���� ��������2||
C |R_ade         |4 4 O|KKS2            |�����2||
C |R_ede         |4 4 I|input2          |����2||
C |R_ufe         |4 4 I|LOWER_warning_setpoint1|||
C |R_ake         |4 4 I|LOWER_alarm_setpoint1|||
C |L_eke         |1 1 I|fault1          |�����1||
C |R_ale         |4 4 I|UPPER_alarm_setpoint1|||
C |R_ele         |4 4 I|UPPER_warning_setpoint1|||
C |I_eme         |2 4 O|dat1_color      |���� ��������1||
C |R_ime         |4 4 O|KKS1            |�����1||
C |R_ome         |4 4 I|input1          |����1||

      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      INTEGER*4 I0_e,I0_i,I0_o,I0_u,I0_ad
      LOGICAL*1 L0_ed,L0_id
      REAL*4 R_od,R_ud
      LOGICAL*1 L_af
      INTEGER*4 I0_ef,I0_if
      LOGICAL*1 L0_of
      REAL*4 R_uf,R_ak
      LOGICAL*1 L0_ek
      INTEGER*4 I0_ik,I0_ok,I0_uk,I_al
      REAL*4 R_el,R_il
      INTEGER*4 I0_ol,I0_ul,I0_am,I0_em,I0_im
      LOGICAL*1 L0_om,L0_um
      REAL*4 R_ap,R_ep
      LOGICAL*1 L_ip
      INTEGER*4 I0_op,I0_up
      LOGICAL*1 L0_ar
      REAL*4 R_er,R_ir
      LOGICAL*1 L0_or
      INTEGER*4 I0_ur,I0_as,I0_es,I_is
      REAL*4 R_os,R_us
      INTEGER*4 I0_at,I0_et,I0_it,I0_ot,I0_ut
      LOGICAL*1 L0_av,L0_ev
      REAL*4 R_iv,R_ov
      LOGICAL*1 L_uv
      INTEGER*4 I0_ax,I0_ex
      LOGICAL*1 L0_ix
      REAL*4 R_ox,R_ux
      LOGICAL*1 L0_abe
      INTEGER*4 I0_ebe,I0_ibe,I0_obe,I_ube
      REAL*4 R_ade,R_ede
      INTEGER*4 I0_ide,I0_ode,I0_ude,I0_afe,I0_efe
      LOGICAL*1 L0_ife,L0_ofe
      REAL*4 R_ufe,R_ake
      LOGICAL*1 L_eke
      INTEGER*4 I0_ike,I0_oke
      LOGICAL*1 L0_uke
      REAL*4 R_ale,R_ele
      LOGICAL*1 L0_ile
      INTEGER*4 I0_ole,I0_ule,I0_ame,I_eme
      REAL*4 R_ime,R_ome

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      I0_e = z'01000015'
C DAT_SAS_3_HANDLER.fmg(  92, 195):��������� ������������� IN (�������)
      I0_o = z'01000017'
C DAT_SAS_3_HANDLER.fmg(  74, 196):��������� ������������� IN (�������)
      I0_ad = z'01000028'
C DAT_SAS_3_HANDLER.fmg(  58, 197):��������� ������������� IN (�������)
      I0_ok = z'01000010'
C DAT_SAS_3_HANDLER.fmg(  40, 198):��������� ������������� IN (�������)
      I0_uk = z'01000021'
C DAT_SAS_3_HANDLER.fmg(  40, 200):��������� ������������� IN (�������)
      I0_ef = z'01000054'
C DAT_SAS_3_HANDLER.fmg( 114, 194):��������� ������������� IN (�������)
      L0_ed=R_il.lt.R_od
C DAT_SAS_3_HANDLER.fmg(  74, 178):���������� <
      L0_id=R_il.lt.R_ud
C DAT_SAS_3_HANDLER.fmg(  92, 172):���������� <
      L0_of=R_il.gt.R_uf
C DAT_SAS_3_HANDLER.fmg(  54, 184):���������� >
      L0_ek=R_il.gt.R_ak
C DAT_SAS_3_HANDLER.fmg(  38, 190):���������� >
      if(L0_ek) then
         I0_ik=I0_ok
      else
         I0_ik=I0_uk
      endif
C DAT_SAS_3_HANDLER.fmg(  44, 198):���� RE IN LO CH7
      if(L0_of) then
         I0_u=I0_ad
      else
         I0_u=I0_ik
      endif
C DAT_SAS_3_HANDLER.fmg(  61, 198):���� RE IN LO CH7
      if(L0_ed) then
         I0_i=I0_o
      else
         I0_i=I0_u
      endif
C DAT_SAS_3_HANDLER.fmg(  78, 196):���� RE IN LO CH7
      if(L0_id) then
         I0_if=I0_e
      else
         I0_if=I0_i
      endif
C DAT_SAS_3_HANDLER.fmg(  96, 196):���� RE IN LO CH7
      if(L_af) then
         I_al=I0_ef
      else
         I_al=I0_if
      endif
C DAT_SAS_3_HANDLER.fmg( 118, 194):���� RE IN LO CH7
      R_el=R_il
C DAT_SAS_3_HANDLER.fmg(  66, 212):������,KKS4
      I0_ol = z'01000015'
C DAT_SAS_3_HANDLER.fmg( 357, 252):��������� ������������� IN (�������)
      I0_am = z'01000017'
C DAT_SAS_3_HANDLER.fmg( 339, 254):��������� ������������� IN (�������)
      I0_im = z'01000028'
C DAT_SAS_3_HANDLER.fmg( 322, 254):��������� ������������� IN (�������)
      I0_as = z'01000010'
C DAT_SAS_3_HANDLER.fmg( 306, 256):��������� ������������� IN (�������)
      I0_es = z'01000021'
C DAT_SAS_3_HANDLER.fmg( 306, 258):��������� ������������� IN (�������)
      I0_op = z'01000054'
C DAT_SAS_3_HANDLER.fmg( 379, 252):��������� ������������� IN (�������)
      L0_om=R_us.lt.R_ap
C DAT_SAS_3_HANDLER.fmg( 338, 236):���������� <
      L0_um=R_us.lt.R_ep
C DAT_SAS_3_HANDLER.fmg( 357, 230):���������� <
      L0_ar=R_us.gt.R_er
C DAT_SAS_3_HANDLER.fmg( 319, 242):���������� >
      L0_or=R_us.gt.R_ir
C DAT_SAS_3_HANDLER.fmg( 302, 248):���������� >
      if(L0_or) then
         I0_ur=I0_as
      else
         I0_ur=I0_es
      endif
C DAT_SAS_3_HANDLER.fmg( 309, 256):���� RE IN LO CH7
      if(L0_ar) then
         I0_em=I0_im
      else
         I0_em=I0_ur
      endif
C DAT_SAS_3_HANDLER.fmg( 326, 255):���� RE IN LO CH7
      if(L0_om) then
         I0_ul=I0_am
      else
         I0_ul=I0_em
      endif
C DAT_SAS_3_HANDLER.fmg( 342, 254):���� RE IN LO CH7
      if(L0_um) then
         I0_up=I0_ol
      else
         I0_up=I0_ul
      endif
C DAT_SAS_3_HANDLER.fmg( 360, 253):���� RE IN LO CH7
      if(L_ip) then
         I_is=I0_op
      else
         I_is=I0_up
      endif
C DAT_SAS_3_HANDLER.fmg( 382, 252):���� RE IN LO CH7
      R_os=R_us
C DAT_SAS_3_HANDLER.fmg( 331, 270):������,KKS3
      I0_at = z'01000015'
C DAT_SAS_3_HANDLER.fmg( 225, 252):��������� ������������� IN (�������)
      I0_it = z'01000017'
C DAT_SAS_3_HANDLER.fmg( 207, 254):��������� ������������� IN (�������)
      I0_ut = z'01000028'
C DAT_SAS_3_HANDLER.fmg( 190, 254):��������� ������������� IN (�������)
      I0_ibe = z'01000010'
C DAT_SAS_3_HANDLER.fmg( 174, 256):��������� ������������� IN (�������)
      I0_obe = z'01000021'
C DAT_SAS_3_HANDLER.fmg( 174, 258):��������� ������������� IN (�������)
      I0_ax = z'01000054'
C DAT_SAS_3_HANDLER.fmg( 247, 252):��������� ������������� IN (�������)
      L0_av=R_ede.lt.R_iv
C DAT_SAS_3_HANDLER.fmg( 206, 236):���������� <
      L0_ev=R_ede.lt.R_ov
C DAT_SAS_3_HANDLER.fmg( 225, 230):���������� <
      L0_ix=R_ede.gt.R_ox
C DAT_SAS_3_HANDLER.fmg( 187, 242):���������� >
      L0_abe=R_ede.gt.R_ux
C DAT_SAS_3_HANDLER.fmg( 170, 248):���������� >
      if(L0_abe) then
         I0_ebe=I0_ibe
      else
         I0_ebe=I0_obe
      endif
C DAT_SAS_3_HANDLER.fmg( 177, 256):���� RE IN LO CH7
      if(L0_ix) then
         I0_ot=I0_ut
      else
         I0_ot=I0_ebe
      endif
C DAT_SAS_3_HANDLER.fmg( 194, 255):���� RE IN LO CH7
      if(L0_av) then
         I0_et=I0_it
      else
         I0_et=I0_ot
      endif
C DAT_SAS_3_HANDLER.fmg( 210, 254):���� RE IN LO CH7
      if(L0_ev) then
         I0_ex=I0_at
      else
         I0_ex=I0_et
      endif
C DAT_SAS_3_HANDLER.fmg( 228, 253):���� RE IN LO CH7
      if(L_uv) then
         I_ube=I0_ax
      else
         I_ube=I0_ex
      endif
C DAT_SAS_3_HANDLER.fmg( 250, 252):���� RE IN LO CH7
      R_ade=R_ede
C DAT_SAS_3_HANDLER.fmg( 199, 270):������,KKS2
      I0_ide = z'01000015'
C DAT_SAS_3_HANDLER.fmg(  92, 252):��������� ������������� IN (�������)
      I0_ude = z'01000017'
C DAT_SAS_3_HANDLER.fmg(  74, 254):��������� ������������� IN (�������)
      I0_efe = z'01000028'
C DAT_SAS_3_HANDLER.fmg(  58, 254):��������� ������������� IN (�������)
      I0_ule = z'01000010'
C DAT_SAS_3_HANDLER.fmg(  40, 256):��������� ������������� IN (�������)
      I0_ame = z'01000021'
C DAT_SAS_3_HANDLER.fmg(  40, 258):��������� ������������� IN (�������)
      I0_ike = z'01000054'
C DAT_SAS_3_HANDLER.fmg( 114, 252):��������� ������������� IN (�������)
      L0_ife=R_ome.lt.R_ufe
C DAT_SAS_3_HANDLER.fmg(  74, 236):���������� <
      L0_ofe=R_ome.lt.R_ake
C DAT_SAS_3_HANDLER.fmg(  92, 230):���������� <
      L0_uke=R_ome.gt.R_ale
C DAT_SAS_3_HANDLER.fmg(  54, 242):���������� >
      L0_ile=R_ome.gt.R_ele
C DAT_SAS_3_HANDLER.fmg(  38, 248):���������� >
      if(L0_ile) then
         I0_ole=I0_ule
      else
         I0_ole=I0_ame
      endif
C DAT_SAS_3_HANDLER.fmg(  44, 256):���� RE IN LO CH7
      if(L0_uke) then
         I0_afe=I0_efe
      else
         I0_afe=I0_ole
      endif
C DAT_SAS_3_HANDLER.fmg(  61, 255):���� RE IN LO CH7
      if(L0_ife) then
         I0_ode=I0_ude
      else
         I0_ode=I0_afe
      endif
C DAT_SAS_3_HANDLER.fmg(  78, 254):���� RE IN LO CH7
      if(L0_ofe) then
         I0_oke=I0_ide
      else
         I0_oke=I0_ode
      endif
C DAT_SAS_3_HANDLER.fmg(  96, 253):���� RE IN LO CH7
      if(L_eke) then
         I_eme=I0_ike
      else
         I_eme=I0_oke
      endif
C DAT_SAS_3_HANDLER.fmg( 118, 252):���� RE IN LO CH7
      R_ime=R_ome
C DAT_SAS_3_HANDLER.fmg(  66, 270):������,KKS1
      End
