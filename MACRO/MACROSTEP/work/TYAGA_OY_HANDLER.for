      Subroutine TYAGA_OY_HANDLER(ext_deltat,R_udi,R_e,R_ad
     &,R_ud,R_ok,R_al,R_el,R_il,R_ol,L_es,R_it,R_ade,L_ede
     &,R_ode,L_ude,L_ake,L_oke,R_ome,R_ipe,L_upe,R_ere,L_ore
     &,R_ure,R_ase,L_ise,L_ite,L_ute,L_ave,L_ive,L_axe,L_oxe
     &,L_abi,R_afi,L_ofi,L_aki,L_iki,L_oki,L_eli,L_ili,C20_opi
     &,L_upi,L_eri,L_iri,L_asi,L_esi,L_isi,L_osi,L_usi,L_ati
     &,L_eti,L_iti,L_oti,L_uti,L_avi,L_evi,L_ivi,L_ovi,L_uvi
     &,L_axi,L_exi,R_ixi,R_oxi,R_uxi,R_abo,R_ebo,I_ibo,R_ado
     &,L_odo,R_afo,C20_ilo,C20_imo,I_epo,I_upo,R_aro,R_ero
     &,R_iro,R_oro,L_uro,L_aso,I_eso,I_uso,I_uto,I_ivo,C20_ixo
     &,C8_ibu,R_aku,R_eku,L_iku,R_oku,R_uku,I_alu,L_olu,L_ulu
     &,L_amu,I_imu,I_apu,L_aru,L_iru,L_oru,L_uru,L_asu,L_esu
     &,L_etu,L_evu,L_ovu,L_uvu,R8_exu,R_abad,R8_obad,L_edad
     &,L_udad,I_afad,L_ofad,L_ufad,L_ukad,L_imad,L_ipad)
C |R_udi         |4 4 I|11 mlfpar19     ||0.6|
C |R_e           |4 4 K|_cJ3901         |�������� ��������� ����������|`p_UP`|
C |R_ad          |4 4 K|_cJ3896         |�������� ��������� ����������|`p_LOW`|
C |R_ud          |4 4 S|_slpbJ3889*     |���������� ��������� ||
C |R_ok          |4 4 I|p_TOLKATEL_XH54 |||
C |R_al          |4 4 S|_slpbJ3850*     |���������� ��������� ||
C |R_el          |4 4 S|_slpbJ3849*     |���������� ��������� ||
C |R_il          |4 4 S|_slpbJ3848*     |���������� ��������� ||
C |R_ol          |4 4 S|_slpbJ3842*     |���������� ��������� ||
C |L_es          |1 1 S|_qffJ3798*      |�������� ������ Q RS-��������  |F|
C |R_it          |4 4 I|mlfpar19        |��������� ������������|0.6|
C |R_ade         |4 4 S|_sdelnv2dyn$100Dasha*|[���]���������� ��������� |0|
C |L_ede         |1 1 S|_idelnv2dyn$100Dasha*|[TF]���������� ��������� �������� ������ |f|
C |R_ode         |4 4 S|_sdelnv2dyn$99Dasha*|[���]���������� ��������� |0|
C |L_ude         |1 1 S|_idelnv2dyn$99Dasha*|[TF]���������� ��������� �������� ������ |f|
C |L_ake         |1 1 S|_qff156*        |�������� ������ Q RS-��������  |F|
C |L_oke         |1 1 S|_qff155*        |�������� ������ Q RS-��������  |F|
C |R_ome         |4 4 I|value_pos       |��� �������� ��������|0.5|
C |R_ipe         |4 4 S|_sdelnv2dyn$83Dasha*|[���]���������� ��������� |0|
C |L_upe         |1 1 S|_idelnv2dyn$83Dasha*|[TF]���������� ��������� �������� ������ |f|
C |R_ere         |4 4 S|_sdelnv2dyn$82Dasha*|[���]���������� ��������� |0|
C |L_ore         |1 1 S|_idelnv2dyn$82Dasha*|[TF]���������� ��������� �������� ������ |f|
C |R_ure         |4 4 I|tpos1           |����� ���� � ��������� 1|30.0|
C |R_ase         |4 4 I|tpos2           |����� ���� � ��������� 2|30.0|
C |L_ise         |1 1 S|_qffnv2dyn$106Dasha*|�������� ������ Q RS-��������  |F|
C |L_ite         |1 1 S|_qffnv2dyn$104Dasha*|�������� ������ Q RS-��������  |F|
C |L_ute         |1 1 S|_qffnv2dyn$105Dasha*|�������� ������ Q RS-��������  |F|
C |L_ave         |1 1 I|OUTC            |�����||
C |L_ive         |1 1 S|_qffnv2dyn$103Dasha*|�������� ������ Q RS-��������  |F|
C |L_axe         |1 1 S|_qffnv2dyn$102Dasha*|�������� ������ Q RS-��������  |F|
C |L_oxe         |1 1 S|_qffnv2dyn$101Dasha*|�������� ������ Q RS-��������  |F|
C |L_abi         |1 1 I|OUTO            |������||
C |R_afi         |4 4 O|mf_value        |��� ��������� �����|0.5|
C |L_ofi         |1 1 S|_qffnv2dyn$87Dasha*|�������� ������ Q RS-��������  |F|
C |L_aki         |1 1 S|_qffnv2dyn$86Dasha*|�������� ������ Q RS-��������  |F|
C |L_iki         |1 1 S|_qffnv2dyn$96Dasha*|�������� ������ Q RS-��������  |F|
C |L_oki         |1 1 S|_qffnv2dyn$95Dasha*|�������� ������ Q RS-��������  |F|
C |L_eli         |1 1 O|limit_switch_error|||
C |L_ili         |1 1 O|flag_mlf19      |||
C |C20_opi       |3 20 O|task_state      |���������||
C |L_upi         |1 1 I|vlv_kvit        |||
C |L_eri         |1 1 I|instr_fault     |||
C |L_iri         |1 1 S|_qffJ3427*      |�������� ������ Q RS-��������  |F|
C |L_asi         |1 1 O|norm            |�����||
C |L_esi         |1 1 I|mlf17           |������ ���������������� ����� �����||
C |L_isi         |1 1 I|mlf16           |������ ������������ ����� �����||
C |L_osi         |1 1 I|mlf15           |������ ���������������� ����� ������||
C |L_usi         |1 1 I|mlf14           |������ ������������ ����� ������||
C |L_ati         |1 1 I|mlf07           |���������� ���� �������||
C |L_eti         |1 1 I|mlf28           |������ ���������������� ��������� �����||
C |L_iti         |1 1 I|mlf09           |����� ��������� �����||
C |L_oti         |1 1 I|mlf08           |����� ��������� ������||
C |L_uti         |1 1 I|mlf26           |������ ���������������� ��������� ������||
C |L_avi         |1 1 I|mlf27           |������ ������������ ��������� �����||
C |L_evi         |1 1 I|mlf25           |������ ������������ ��������� ������||
C |L_ivi         |1 1 I|mlf23           |������� ������� �����||
C |L_ovi         |1 1 I|mlf22           |����� ����� ��������||
C |L_uvi         |1 1 I|mlf19           |���� ������������||
C |L_axi         |1 1 I|YA25C           |������� ������� �� ����������|F|
C |L_exi         |1 1 I|YA26C           |������� ������� �� ���������� (��)|F|
C |R_ixi         |4 4 I|tcl_top         |�������� �������|20|
C |R_oxi         |4 4 K|_uintT_INT      |����������� ������ ����������� ������|`p_UP`|
C |R_uxi         |4 4 K|_tintT_INT      |[���]�������� T �����������|1|
C |R_abo         |4 4 O|_ointT_INT*     |�������� ������ ����������� |`p_START`|
C |R_ebo         |4 4 K|_lintT_INT      |����������� ������ ����������� �����|`p_LOW`|
C |I_ibo         |2 4 O|LWORK           |����� �������||
C |R_ado         |4 4 O|VX01            |��������� ��������� �� ��� X||
C |L_odo         |1 1 I|mlf04           |���������������� �������� �����||
C |R_afo         |4 4 O|VX02            |�������� ����������� ���������||
C |C20_ilo       |3 20 O|task_name3      |������� ��������� ��� ������� ������������ ��������||
C |C20_imo       |3 20 O|task_name2      |������� ��������� ��� ������� ������������ ��������||
C |I_epo         |2 4 O|LREADY          |����� ����������||
C |I_upo         |2 4 O|LBUSY           |����� �����||
C |R_aro         |4 4 S|vminit_button_ST*|��������� ������ "������� � �������� �� ���������" |0.0|
C |R_ero         |4 4 I|vminit_button   |������� ������ ������ "������� � �������� �� ���������" |0.0|
C |R_iro         |4 4 S|vmwork_button_ST*|��������� ������ "������� � ������� �� ���������" |0.0|
C |R_oro         |4 4 I|vmwork_button   |������� ������ ������ "������� � ������� �� ���������" |0.0|
C |L_uro         |1 1 I|YA25            |������� ������� �� ����������|F|
C |L_aso         |1 1 I|YA26            |������� ������� �� ����������|F|
C |I_eso         |2 4 O|state1          |��������� 1||
C |I_uso         |2 4 O|state2          |��������� 2||
C |I_uto         |2 4 O|LWORKO          |����� � �������||
C |I_ivo         |2 4 O|LINITC          |����� � ��������||
C |C20_ixo       |3 20 O|task_name       |������� ���������||
C |C8_ibu        |3 8 I|task            |������� ���������||
C |R_aku         |4 4 S|vmstop_button_ST*|��������� ������ "������� ���� �� ���������" |0.0|
C |R_eku         |4 4 I|vmstop_button   |������� ������ ������ "������� ���� �� ���������" |0.0|
C |L_iku         |1 1 O|vmstart_button_CMD*|[TF]����� ������ ������� �������� ��������� �� ���������|F|
C |R_oku         |4 4 S|vmstart_button_ST*|��������� ������ "������� �������� ��������� �� ���������" |0.0|
C |R_uku         |4 4 I|vmstart_button  |������� ������ ������ "������� �������� ��������� �� ���������" |0.0|
C |I_alu         |2 4 O|LERROR          |����� �������������||
C |L_olu         |1 1 I|YA27C           |������� ���� �� ���������� (��)|F|
C |L_ulu         |1 1 I|stop_avt        |������� ���� �� ����������|F|
C |L_amu         |1 1 O|vmstop_button_CMD*|[TF]����� ������ ������� ���� �� ���������|F|
C |I_imu         |2 4 O|LINIT           |����� ��������||
C |I_apu         |2 4 O|LZM             |������ "�������"||
C |L_aru         |1 1 O|vminit_button_CMD*|[TF]����� ������ ������� � �������� �� ���������|F|
C |L_iru         |1 1 O|vmwork_button_CMD*|[TF]����� ������ ������� � ������� �� ���������|F|
C |L_oru         |1 1 I|mlf06           |������������� ������� "�����"||
C |L_uru         |1 1 I|mlf05           |������������� ������� "������"||
C |L_asu         |1 1 I|ulubopen        |���������� ������� �� ���������|F|
C |L_esu         |1 1 I|ulubclose       |���������� ������� �� ���������|F|
C |L_etu         |1 1 O|block           |||
C |L_evu         |1 1 S|_qff10*         |�������� ������ Q RS-��������  |F|
C |L_ovu         |1 1 O|STOP            |�������||
C |L_uvu         |1 1 O|nopower         |��� ����������||
C |R8_exu        |4 8 I|voltage         |[��]���������� �� ������||
C |R_abad        |4 4 I|power           |�������� ��������||
C |R8_obad       |4 8 O|sn_power        |�������� ���� ��|3.0|
C |L_edad        |1 1 I|mlf03           |���������������� �������� ������||
C |L_udad        |1 1 O|fault           |�������������||
C |I_afad        |2 4 O|LAM             |������ "�������"||
C |L_ofad        |1 1 O|XH53            |�� ����� (���)|F|
C |L_ufad        |1 1 O|XH54            |�� ������ (���)|F|
C |L_ukad        |1 1 S|_qff2*          |�������� ������ Q RS-��������  |F|
C |L_imad        |1 1 S|_qff1*          |�������� ������ Q RS-��������  |F|
C |L_ipad        |1 1 I|mlf24           |��� ���������� � ����������||

      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      REAL*4 R_e,R0_i,R0_o,R0_u,R_ad,R0_ed,R0_id,R0_od,R_ud
     &,R0_af,R0_ef,R0_if,R0_of
      LOGICAL*1 L0_uf,L0_ak,L0_ek,L0_ik
      REAL*4 R_ok,R0_uk,R_al,R_el,R_il,R_ol,R0_ul,R0_am,R0_em
     &,R0_im,R0_om,R0_um,R0_ap,R0_ep,R0_ip,R0_op
      LOGICAL*1 L0_up
      REAL*4 R0_ar,R0_er,R0_ir,R0_or,R0_ur
      LOGICAL*1 L0_as,L_es
      REAL*4 R0_is,R0_os,R0_us,R0_at,R0_et,R_it,R0_ot,R0_ut
     &,R0_av
      LOGICAL*1 L0_ev,L0_iv
      REAL*4 R0_ov
      LOGICAL*1 L0_uv,L0_ax
      REAL*4 R0_ex,R0_ix
      LOGICAL*1 L0_ox,L0_ux,L0_abe,L0_ebe,L0_ibe,L0_obe
      REAL*4 R0_ube,R_ade
      LOGICAL*1 L_ede
      REAL*4 R0_ide,R_ode
      LOGICAL*1 L_ude,L0_afe,L0_efe,L0_ife,L0_ofe,L0_ufe,L_ake
     &,L0_eke,L0_ike,L_oke,L0_uke,L0_ale,L0_ele
      REAL*4 R0_ile,R0_ole
      LOGICAL*1 L0_ule,L0_ame
      REAL*4 R0_eme,R0_ime,R_ome
      LOGICAL*1 L0_ume,L0_ape
      REAL*4 R0_epe,R_ipe
      LOGICAL*1 L0_ope,L_upe
      REAL*4 R0_are,R_ere
      LOGICAL*1 L0_ire,L_ore
      REAL*4 R_ure,R_ase
      LOGICAL*1 L0_ese,L_ise,L0_ose,L0_use,L0_ate,L0_ete,L_ite
     &,L0_ote,L_ute,L_ave,L0_eve,L_ive,L0_ove,L0_uve,L_axe
     &,L0_exe,L0_ixe,L_oxe,L0_uxe,L_abi,L0_ebi
      REAL*4 R0_ibi,R0_obi,R0_ubi
      LOGICAL*1 L0_adi
      REAL*4 R0_edi
      LOGICAL*1 L0_idi,L0_odi
      REAL*4 R_udi,R_afi
      LOGICAL*1 L0_efi,L0_ifi,L_ofi,L0_ufi,L_aki,L0_eki,L_iki
     &,L_oki,L0_uki,L0_ali,L_eli,L_ili,L0_oli,L0_uli,L0_ami
     &,L0_emi,L0_imi,L0_omi
      CHARACTER*20 C20_umi,C20_api,C20_epi,C20_ipi,C20_opi
      LOGICAL*1 L_upi,L0_ari,L_eri,L_iri,L0_ori,L0_uri,L_asi
     &,L_esi,L_isi,L_osi,L_usi,L_ati,L_eti,L_iti,L_oti,L_uti
     &,L_avi,L_evi,L_ivi,L_ovi,L_uvi,L_axi
      LOGICAL*1 L_exi
      REAL*4 R_ixi,R_oxi,R_uxi,R_abo,R_ebo
      INTEGER*4 I_ibo,I0_obo,I0_ubo
      REAL*4 R_ado
      LOGICAL*1 L0_edo,L0_ido,L_odo
      REAL*4 R0_udo,R_afo,R0_efo,R0_ifo,R0_ofo,R0_ufo,R0_ako
      LOGICAL*1 L0_eko,L0_iko
      CHARACTER*20 C20_oko,C20_uko,C20_alo,C20_elo,C20_ilo
     &,C20_olo,C20_ulo,C20_amo,C20_emo,C20_imo
      INTEGER*4 I0_omo,I0_umo
      LOGICAL*1 L0_apo
      INTEGER*4 I_epo,I0_ipo,I0_opo,I_upo
      REAL*4 R_aro,R_ero,R_iro,R_oro
      LOGICAL*1 L_uro,L_aso
      INTEGER*4 I_eso,I0_iso,I0_oso,I_uso,I0_ato,I0_eto,I0_ito
     &,I0_oto,I_uto,I0_avo,I0_evo,I_ivo
      CHARACTER*20 C20_ovo,C20_uvo,C20_axo,C20_exo,C20_ixo
      CHARACTER*8 C8_oxo
      LOGICAL*1 L0_uxo
      CHARACTER*8 C8_abu
      LOGICAL*1 L0_ebu
      CHARACTER*8 C8_ibu
      LOGICAL*1 L0_obu
      INTEGER*4 I0_ubu
      LOGICAL*1 L0_adu
      INTEGER*4 I0_edu
      LOGICAL*1 L0_idu
      INTEGER*4 I0_odu,I0_udu,I0_afu
      LOGICAL*1 L0_efu
      INTEGER*4 I0_ifu,I0_ofu,I0_ufu
      REAL*4 R_aku,R_eku
      LOGICAL*1 L_iku
      REAL*4 R_oku,R_uku
      INTEGER*4 I_alu,I0_elu,I0_ilu
      LOGICAL*1 L_olu,L_ulu,L_amu,L0_emu
      INTEGER*4 I_imu,I0_omu,I0_umu,I_apu,I0_epu,I0_ipu
      LOGICAL*1 L0_opu,L0_upu,L_aru,L0_eru,L_iru,L_oru,L_uru
     &,L_asu,L_esu,L0_isu,L0_osu,L0_usu,L0_atu,L_etu,L0_itu
     &,L0_otu,L0_utu,L0_avu,L_evu,L0_ivu,L_ovu,L_uvu
      REAL*4 R0_axu
      REAL*8 R8_exu
      LOGICAL*1 L0_ixu,L0_oxu
      REAL*4 R0_uxu,R_abad,R0_ebad,R0_ibad
      REAL*8 R8_obad
      LOGICAL*1 L0_ubad,L0_adad,L_edad,L0_idad,L0_odad,L_udad
      INTEGER*4 I_afad,I0_efad,I0_ifad
      LOGICAL*1 L_ofad,L_ufad,L0_akad,L0_ekad,L0_ikad,L0_okad
     &,L_ukad,L0_alad,L0_elad,L0_ilad,L0_olad,L0_ulad,L0_amad
     &,L0_emad,L_imad,L0_omad,L0_umad,L0_apad,L0_epad,L_ipad

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      R0_ix=R_ud
C TYAGA_OY_HANDLER.fmg( 349, 461):pre: ����������� ��
      R0_im=R_el
C TYAGA_OY_HANDLER.fmg( 394, 406):pre: ����������� ��
      R0_um=R_al
C TYAGA_OY_HANDLER.fmg( 386, 412):pre: ����������� ��
      R0_us=R_il
C TYAGA_OY_HANDLER.fmg( 349, 414):pre: ����������� ��
      R0_or=R_ol
C TYAGA_OY_HANDLER.fmg( 319, 403):pre: ����������� ��
      R0_ide=R_ode
C TYAGA_OY_HANDLER.fmg( 260, 346):pre: �������� ��������� ������,nv2dyn$99Dasha
      R0_ube=R_ade
C TYAGA_OY_HANDLER.fmg( 260, 334):pre: �������� ��������� ������,nv2dyn$100Dasha
      R0_are=R_ere
C TYAGA_OY_HANDLER.fmg( 245, 310):pre: �������� ��������� ������,nv2dyn$82Dasha
      R0_epe=R_ipe
C TYAGA_OY_HANDLER.fmg( 245, 296):pre: �������� ��������� ������,nv2dyn$83Dasha
      !��������� R0_o = TYAGA_OY_HANDLERC?? /`p_UP`/
      R0_o=R_e
C TYAGA_OY_HANDLER.fmg( 374, 460):���������
      R0_i = 0.01
C TYAGA_OY_HANDLER.fmg( 374, 457):��������� (RE4) (�������)
      R0_u = R0_o + (-R0_i)
C TYAGA_OY_HANDLER.fmg( 377, 458):��������
      !��������� R0_id = TYAGA_OY_HANDLERC?? /`p_LOW`/
      R0_id=R_ad
C TYAGA_OY_HANDLER.fmg( 372, 439):���������
      R0_ed = 0.01
C TYAGA_OY_HANDLER.fmg( 372, 436):��������� (RE4) (�������)
      R0_od = R0_id + R0_ed
C TYAGA_OY_HANDLER.fmg( 375, 438):��������
      R0_af = 1.0
C TYAGA_OY_HANDLER.fmg( 360, 422):��������� (RE4) (�������)
      R0_ef = 1.0
C TYAGA_OY_HANDLER.fmg( 360, 428):��������� (RE4) (�������)
      R0_uk = R_it * R_ok
C TYAGA_OY_HANDLER.fmg( 352, 442):����������
      R0_if = R0_uk + (-R0_af)
C TYAGA_OY_HANDLER.fmg( 363, 424):��������
      R0_of = R0_uk + R0_ef
C TYAGA_OY_HANDLER.fmg( 363, 430):��������
      R0_ul = 0.0
C TYAGA_OY_HANDLER.fmg( 428, 414):��������� (RE4) (�������),nv2dyn$57Dasha
      R0_em = 0.99
C TYAGA_OY_HANDLER.fmg( 410, 416):��������� (RE4) (�������),nv2dyn$60Dasha
      if(.NOT.L_ivi) then
         R0_op=R0_im
      endif
C TYAGA_OY_HANDLER.fmg( 398, 412):���� � ������������� �������
      R0_ap = 1.0
C TYAGA_OY_HANDLER.fmg( 363, 412):��������� (RE4) (�������)
      R0_ep = 0.0
C TYAGA_OY_HANDLER.fmg( 355, 412):��������� (RE4) (�������)
      R0_er = 1.0e-10
C TYAGA_OY_HANDLER.fmg( 333, 390):��������� (RE4) (�������)
      R0_is = 0.0
C TYAGA_OY_HANDLER.fmg( 343, 404):��������� (RE4) (�������)
      R0_ur = R0_or + (-R_it)
C TYAGA_OY_HANDLER.fmg( 322, 396):��������
      R0_ot = 0.33
C TYAGA_OY_HANDLER.fmg( 284, 473):��������� (RE4) (�������),nv2dyn$59Dasha
      R0_ut = R_ixi * R0_ot
C TYAGA_OY_HANDLER.fmg( 290, 474):����������
      if(L_ati) then
         R0_efo=R0_ut
      else
         R0_efo=R_ixi
      endif
C TYAGA_OY_HANDLER.fmg( 296, 476):���� RE IN LO CH7,nv2dyn$40Dasha
      R0_av = 0.999999
C TYAGA_OY_HANDLER.fmg( 146, 476):��������� (RE4) (�������),nv2dyn$60Dasha
      L0_ev=R_ome.gt.R0_av
C TYAGA_OY_HANDLER.fmg( 154, 477):���������� >,nv2dyn$45Dasha
      L0_iv = L_uti.AND.L0_ev
C TYAGA_OY_HANDLER.fmg( 164, 483):�
      R0_ov = 0.000001
C TYAGA_OY_HANDLER.fmg( 151, 426):��������� (RE4) (�������),nv2dyn$61Dasha
      L0_uv=R_ome.lt.R0_ov
C TYAGA_OY_HANDLER.fmg( 158, 428):���������� <,nv2dyn$46Dasha
      L0_ax = L_eti.AND.L0_uv
C TYAGA_OY_HANDLER.fmg( 164, 432):�
      R0_ile = 0.000001
C TYAGA_OY_HANDLER.fmg( 218, 258):��������� (RE4) (�������),nv2dyn$61Dasha
      R0_ole = 0.999999
C TYAGA_OY_HANDLER.fmg( 217, 264):��������� (RE4) (�������),nv2dyn$60Dasha
      R0_eme = 0.000001
C TYAGA_OY_HANDLER.fmg( 218, 275):��������� (RE4) (�������),nv2dyn$61Dasha
      L0_ume=R_ome.lt.R0_eme
C TYAGA_OY_HANDLER.fmg( 222, 276):���������� <,nv2dyn$46Dasha
      L0_oli = L_eti.AND.L0_ume
C TYAGA_OY_HANDLER.fmg( 254, 274):�
      R0_ime = 0.999999
C TYAGA_OY_HANDLER.fmg( 216, 281):��������� (RE4) (�������),nv2dyn$60Dasha
      L0_ape=R_ome.gt.R0_ime
C TYAGA_OY_HANDLER.fmg( 222, 282):���������� >,nv2dyn$45Dasha
      L0_uli = L_uti.AND.L0_ape
C TYAGA_OY_HANDLER.fmg( 254, 286):�
      if(.not.L_ati) then
         R_ipe=0.0
      elseif(.not.L_upe) then
         R_ipe=R_ure
      else
         R_ipe=max(R0_epe-deltat,0.0)
      endif
      L0_ope=L_ati.and.R_ipe.le.0.0
      L_upe=L_ati
C TYAGA_OY_HANDLER.fmg( 245, 296):�������� ��������� ������,nv2dyn$83Dasha
      if(.not.L_ati) then
         R_ere=0.0
      elseif(.not.L_ore) then
         R_ere=R_ase
      else
         R_ere=max(R0_are-deltat,0.0)
      endif
      L0_ire=L_ati.and.R_ere.le.0.0
      L_ore=L_ati
C TYAGA_OY_HANDLER.fmg( 245, 310):�������� ��������� ������,nv2dyn$82Dasha
      R0_ibi = 0.01
C TYAGA_OY_HANDLER.fmg(  36, 282):��������� (RE4) (�������),nv2dyn$74Dasha
      R0_obi = R0_ibi + R_udi
C TYAGA_OY_HANDLER.fmg(  40, 281):��������
      R0_ubi = 0.01
C TYAGA_OY_HANDLER.fmg(  36, 288):��������� (RE4) (�������),nv2dyn$74Dasha
      R0_edi = (-R0_ubi) + R_udi
C TYAGA_OY_HANDLER.fmg(  40, 288):��������
      C20_epi = '��������� 2'
C TYAGA_OY_HANDLER.fmg(  28, 312):��������� ���������� CH20 (CH30) (�������)
      C20_ipi = '������� ���'
C TYAGA_OY_HANDLER.fmg(  28, 314):��������� ���������� CH20 (CH30) (�������)
      C20_umi = '��������� 1'
C TYAGA_OY_HANDLER.fmg(  44, 310):��������� ���������� CH20 (CH30) (�������)
      L_iri=(L_eri.or.L_iri).and..not.(L_upi)
      L0_ari=.not.L_iri
C TYAGA_OY_HANDLER.fmg( 296, 274):RS �������
      L0_ubad = L_uro.OR.L_axi.OR.L_odo
C TYAGA_OY_HANDLER.fmg(  74, 387):���
      L0_idad = L_aso.OR.L_exi
C TYAGA_OY_HANDLER.fmg(  71, 432):���
      I0_ubo = z'01000003'
C TYAGA_OY_HANDLER.fmg( 130, 471):��������� ������������� IN (�������)
      I0_obo = z'0100000A'
C TYAGA_OY_HANDLER.fmg( 130, 469):��������� ������������� IN (�������)
      R0_udo = 0.0
C TYAGA_OY_HANDLER.fmg( 320, 467):��������� (RE4) (�������)
      R0_ako = 0.0
C TYAGA_OY_HANDLER.fmg( 306, 467):��������� (RE4) (�������)
      C20_uko = ''
C TYAGA_OY_HANDLER.fmg( 176, 359):��������� ���������� CH20 (CH30) (�������)
      C20_oko = '�������'
C TYAGA_OY_HANDLER.fmg( 176, 357):��������� ���������� CH20 (CH30) (�������)
      C20_alo = '������'
C TYAGA_OY_HANDLER.fmg( 197, 360):��������� ���������� CH20 (CH30) (�������)
      C20_ulo = ''
C TYAGA_OY_HANDLER.fmg(  82, 361):��������� ���������� CH20 (CH30) (�������)
      C20_olo = '� ��������� 2'
C TYAGA_OY_HANDLER.fmg(  82, 359):��������� ���������� CH20 (CH30) (�������)
      C20_amo = '� ��������� 1'
C TYAGA_OY_HANDLER.fmg( 104, 360):��������� ���������� CH20 (CH30) (�������)
      I0_omo = z'0100000A'
C TYAGA_OY_HANDLER.fmg( 228, 471):��������� ������������� IN (�������)
      I0_umo = z'01000003'
C TYAGA_OY_HANDLER.fmg( 228, 473):��������� ������������� IN (�������)
      I0_opo = z'0100000A'
C TYAGA_OY_HANDLER.fmg( 228, 492):��������� ������������� IN (�������)
      I0_ipo = z'01000003'
C TYAGA_OY_HANDLER.fmg( 228, 490):��������� ������������� IN (�������)
      L_aru=R_ero.ne.R_aro
      R_aro=R_ero
C TYAGA_OY_HANDLER.fmg(  22, 396):���������� ������������� ������
      L_iru=R_oro.ne.R_iro
      R_iro=R_oro
C TYAGA_OY_HANDLER.fmg(  27, 452):���������� ������������� ������
      I0_oso = z'01000003'
C TYAGA_OY_HANDLER.fmg( 146, 364):��������� ������������� IN (�������)
      I0_iso = z'01000010'
C TYAGA_OY_HANDLER.fmg( 146, 362):��������� ������������� IN (�������)
      I0_eto = z'01000003'
C TYAGA_OY_HANDLER.fmg( 125, 454):��������� ������������� IN (�������)
      I0_ato = z'01000010'
C TYAGA_OY_HANDLER.fmg( 125, 452):��������� ������������� IN (�������)
      I0_oto = z'01000003'
C TYAGA_OY_HANDLER.fmg( 193, 432):��������� ������������� IN (�������)
      I0_ito = z'0100000A'
C TYAGA_OY_HANDLER.fmg( 193, 430):��������� ������������� IN (�������)
      I0_evo = z'01000003'
C TYAGA_OY_HANDLER.fmg( 201, 382):��������� ������������� IN (�������)
      I0_avo = z'0100000A'
C TYAGA_OY_HANDLER.fmg( 201, 380):��������� ������������� IN (�������)
      I0_umu = z'01000003'
C TYAGA_OY_HANDLER.fmg( 123, 378):��������� ������������� IN (�������)
      I0_omu = z'0100000A'
C TYAGA_OY_HANDLER.fmg( 123, 376):��������� ������������� IN (�������)
      C20_ovo = '� ��������'
C TYAGA_OY_HANDLER.fmg(  58, 374):��������� ���������� CH20 (CH30) (�������)
      C20_axo = '� �������'
C TYAGA_OY_HANDLER.fmg(  43, 374):��������� ���������� CH20 (CH30) (�������)
      C20_exo = ''
C TYAGA_OY_HANDLER.fmg(  43, 376):��������� ���������� CH20 (CH30) (�������)
      C8_oxo = 'init'
C TYAGA_OY_HANDLER.fmg(  26, 383):��������� ���������� CH8 (�������)
      call chcomp(C8_ibu,C8_oxo,L0_uxo)
C TYAGA_OY_HANDLER.fmg(  31, 386):���������� ���������
      C8_abu = 'work'
C TYAGA_OY_HANDLER.fmg(  26, 425):��������� ���������� CH8 (�������)
      call chcomp(C8_ibu,C8_abu,L0_ebu)
C TYAGA_OY_HANDLER.fmg(  31, 428):���������� ���������
      if(L0_ebu) then
         C20_emo=C20_olo
      else
         C20_emo=C20_ulo
      endif
C TYAGA_OY_HANDLER.fmg(  86, 360):���� RE IN LO CH20
      if(L0_uxo) then
         C20_imo=C20_amo
      else
         C20_imo=C20_emo
      endif
C TYAGA_OY_HANDLER.fmg( 108, 360):���� RE IN LO CH20
      if(L0_ebu) then
         C20_elo=C20_oko
      else
         C20_elo=C20_uko
      endif
C TYAGA_OY_HANDLER.fmg( 180, 358):���� RE IN LO CH20
      if(L0_uxo) then
         C20_ilo=C20_alo
      else
         C20_ilo=C20_elo
      endif
C TYAGA_OY_HANDLER.fmg( 202, 360):���� RE IN LO CH20
      if(L0_ebu) then
         C20_uvo=C20_axo
      else
         C20_uvo=C20_exo
      endif
C TYAGA_OY_HANDLER.fmg(  47, 375):���� RE IN LO CH20
      if(L0_uxo) then
         C20_ixo=C20_ovo
      else
         C20_ixo=C20_uvo
      endif
C TYAGA_OY_HANDLER.fmg(  63, 374):���� RE IN LO CH20
      I0_ubu = z'0100008E'
C TYAGA_OY_HANDLER.fmg( 178, 398):��������� ������������� IN (�������)
      I0_edu = z'01000086'
C TYAGA_OY_HANDLER.fmg( 172, 451):��������� ������������� IN (�������)
      I0_udu = z'01000003'
C TYAGA_OY_HANDLER.fmg( 160, 464):��������� ������������� IN (�������)
      I0_afu = z'01000010'
C TYAGA_OY_HANDLER.fmg( 160, 466):��������� ������������� IN (�������)
      I0_ufu = z'01000003'
C TYAGA_OY_HANDLER.fmg( 167, 412):��������� ������������� IN (�������)
      I0_ofu = z'01000010'
C TYAGA_OY_HANDLER.fmg( 167, 410):��������� ������������� IN (�������)
      L_amu=R_eku.ne.R_aku
      R_aku=R_eku
C TYAGA_OY_HANDLER.fmg(  22, 416):���������� ������������� ������
      L0_emu = L_amu.OR.L_ulu.OR.L_olu
C TYAGA_OY_HANDLER.fmg(  56, 413):���
      L_iku=R_uku.ne.R_oku
      R_oku=R_uku
C TYAGA_OY_HANDLER.fmg(  27, 438):���������� ������������� ������
      L0_eru = L_iku.AND.L0_ebu
C TYAGA_OY_HANDLER.fmg(  38, 436):�
      L0_otu = L_iru.OR.L0_eru
C TYAGA_OY_HANDLER.fmg(  60, 438):���
      L0_ele = L_uru.AND.L0_otu
C TYAGA_OY_HANDLER.fmg( 240, 346):�
      L0_ufe = L0_otu.OR.(.NOT.L_oru)
C TYAGA_OY_HANDLER.fmg( 244, 330):���
      L0_upu = L_iku.AND.L0_uxo
C TYAGA_OY_HANDLER.fmg(  37, 394):�
      L0_itu = L_aru.OR.L0_upu
C TYAGA_OY_HANDLER.fmg(  60, 396):���
      L0_ale = L_oru.AND.L0_itu
C TYAGA_OY_HANDLER.fmg( 240, 334):�
      L_ake=(L0_ale.or.L_ake).and..not.(L0_ufe)
      L0_eke=.not.L_ake
C TYAGA_OY_HANDLER.fmg( 251, 332):RS �������,156
      if(.not.L_ake) then
         R_ade=0.0
      elseif(.not.L_ede) then
         R_ade=R_ure
      else
         R_ade=max(R0_ube-deltat,0.0)
      endif
      L0_afe=L_ake.and.R_ade.le.0.0
      L_ede=L_ake
C TYAGA_OY_HANDLER.fmg( 260, 334):�������� ��������� ������,nv2dyn$100Dasha
      L_iki=(L0_afe.or.L_iki).and..not.(L_abi)
      L0_efe=.not.L_iki
C TYAGA_OY_HANDLER.fmg( 276, 332):RS �������,nv2dyn$96Dasha
      L0_ike = (.NOT.L_uru).OR.L0_itu
C TYAGA_OY_HANDLER.fmg( 246, 342):���
      L_oke=(L0_ele.or.L_oke).and..not.(L0_ike)
      L0_uke=.not.L_oke
C TYAGA_OY_HANDLER.fmg( 251, 344):RS �������,155
      if(.not.L_oke) then
         R_ode=0.0
      elseif(.not.L_ude) then
         R_ode=R_ase
      else
         R_ode=max(R0_ide-deltat,0.0)
      endif
      L0_ife=L_oke.and.R_ode.le.0.0
      L_ude=L_oke
C TYAGA_OY_HANDLER.fmg( 260, 346):�������� ��������� ������,nv2dyn$99Dasha
      L_oki=(L0_ife.or.L_oki).and..not.(L_ave)
      L0_ofe=.not.L_oki
C TYAGA_OY_HANDLER.fmg( 276, 344):RS �������,nv2dyn$95Dasha
      I0_epu = z'0100000E'
C TYAGA_OY_HANDLER.fmg( 197, 402):��������� ������������� IN (�������)
      I0_elu = z'01000007'
C TYAGA_OY_HANDLER.fmg( 154, 383):��������� ������������� IN (�������)
      I0_ilu = z'01000003'
C TYAGA_OY_HANDLER.fmg( 154, 385):��������� ������������� IN (�������)
      I0_efad = z'0100000E'
C TYAGA_OY_HANDLER.fmg( 190, 458):��������� ������������� IN (�������)
      L0_atu=.false.
C TYAGA_OY_HANDLER.fmg(  72, 418):��������� ���������� (�������)
      L0_usu=.false.
C TYAGA_OY_HANDLER.fmg(  72, 416):��������� ���������� (�������)
      R0_axu = 0.1
C TYAGA_OY_HANDLER.fmg( 254, 366):��������� (RE4) (�������)
      L_uvu=R8_exu.lt.R0_axu
C TYAGA_OY_HANDLER.fmg( 258, 367):���������� <
      R0_uxu = 0.0
C TYAGA_OY_HANDLER.fmg( 265, 386):��������� (RE4) (�������)
      L0_ove = (.NOT.L_uvi).AND.L_ive
C TYAGA_OY_HANDLER.fmg( 111, 277):�
C label 246  try246=try246-1
      L0_uve = L_ave.AND.L0_ove
C TYAGA_OY_HANDLER.fmg( 115, 278):�
      L_ive=(L_uvi.or.L_ive).and..not.(L0_uve)
      L0_eve=.not.L_ive
C TYAGA_OY_HANDLER.fmg( 106, 274):RS �������,nv2dyn$103Dasha
C sav1=L0_ove
      L0_ove = (.NOT.L_uvi).AND.L_ive
C TYAGA_OY_HANDLER.fmg( 111, 277):recalc:�
C if(sav1.ne.L0_ove .and. try246.gt.0) goto 246
      L0_ose = (.NOT.L_uvi).AND.L_ise
C TYAGA_OY_HANDLER.fmg( 116, 263):�
C label 250  try250=try250-1
      L0_use = L_abi.AND.L0_ose
C TYAGA_OY_HANDLER.fmg( 120, 264):�
      L_ise=(L_uvi.or.L_ise).and..not.(L0_use)
      L0_ese=.not.L_ise
C TYAGA_OY_HANDLER.fmg( 112, 260):RS �������,nv2dyn$106Dasha
C sav1=L0_ose
      L0_ose = (.NOT.L_uvi).AND.L_ise
C TYAGA_OY_HANDLER.fmg( 116, 263):recalc:�
C if(sav1.ne.L0_ose .and. try250.gt.0) goto 250
      L0_adi=R_afi.gt.R0_edi
C TYAGA_OY_HANDLER.fmg(  45, 292):���������� >,nv2dyn$84Dasha
C label 255  try255=try255-1
      R_abo=R_abo+deltat/R_uxi*R_afo
      if(R_abo.gt.R_oxi) then
         R_abo=R_oxi
      elseif(R_abo.lt.R_ebo) then
         R_abo=R_ebo
      endif
C TYAGA_OY_HANDLER.fmg( 338, 455):����������,T_INT
      if(L_ivi) then
         R0_ex=R0_ix
      else
         R0_ex=R_abo
      endif
C TYAGA_OY_HANDLER.fmg( 348, 455):���� RE IN LO CH7
      L0_ak=R_ado.lt.R0_of
C TYAGA_OY_HANDLER.fmg( 343, 430):���������� <
      L0_uf=R_ado.gt.R0_if
C TYAGA_OY_HANDLER.fmg( 343, 424):���������� >
      L0_ek = L0_ak.AND.L0_uf
C TYAGA_OY_HANDLER.fmg( 350, 428):�
      L0_ik = L_uvi.AND.L0_ek
C TYAGA_OY_HANDLER.fmg( 354, 434):�
      if(L0_ik) then
         R_ado=R0_uk
      else
         R_ado=R0_ex
      endif
C TYAGA_OY_HANDLER.fmg( 358, 454):���� RE IN LO CH7
      L0_ibe=R_ado.lt.R0_od
C TYAGA_OY_HANDLER.fmg( 384, 439):���������� <
      L0_ebe = L0_ibe.AND.(.NOT.L_eti)
C TYAGA_OY_HANDLER.fmg( 426, 438):�
      L_ofad = L_iti.OR.L0_ebe.OR.L_avi
C TYAGA_OY_HANDLER.fmg( 430, 438):���
      L0_omi = L_oti.AND.L_ofad
C TYAGA_OY_HANDLER.fmg( 262, 320):�
      L0_obe=R_ado.gt.R0_u
C TYAGA_OY_HANDLER.fmg( 386, 460):���������� >
      L0_abe = L0_obe.AND.(.NOT.L_uti)
C TYAGA_OY_HANDLER.fmg( 423, 458):�
      L_ufad = L_oti.OR.L0_abe.OR.L_evi
C TYAGA_OY_HANDLER.fmg( 429, 458):���
      L0_imi = L_iti.AND.L_ufad
C TYAGA_OY_HANDLER.fmg( 262, 316):�
      L0_emi = L0_ire.AND.(.NOT.L_ufad)
C TYAGA_OY_HANDLER.fmg( 254, 308):�
      L0_ami = L0_ope.AND.(.NOT.L_ofad)
C TYAGA_OY_HANDLER.fmg( 250, 295):�
      L0_ebi=R_afi.lt.R0_obi
C TYAGA_OY_HANDLER.fmg(  47, 286):���������� <,nv2dyn$85Dasha
      L0_idi = L0_adi.AND.L0_ebi
C TYAGA_OY_HANDLER.fmg(  52, 291):�
      L0_odi = L0_idi.AND.L_uvi
C TYAGA_OY_HANDLER.fmg(  64, 283):�
      L_oxe=(L_abi.or.L_oxe).and..not.(L_ofad)
      L0_uxe=.not.L_oxe
C TYAGA_OY_HANDLER.fmg(  64, 276):RS �������,nv2dyn$101Dasha
      L0_ixe = L0_odi.AND.L_oxe
C TYAGA_OY_HANDLER.fmg(  83, 282):�
      L_axe=(L0_ixe.or.L_axe).and..not.(L0_uve)
      L0_exe=.not.L_axe
C TYAGA_OY_HANDLER.fmg( 120, 280):RS �������,nv2dyn$102Dasha
      L_ite=(L_ave.or.L_ite).and..not.(L_ufad)
      L0_ete=.not.L_ite
C TYAGA_OY_HANDLER.fmg(  70, 265):RS �������,nv2dyn$104Dasha
      L0_ote = L0_odi.AND.L_ite
C TYAGA_OY_HANDLER.fmg(  83, 268):�
      L_ute=(L0_ote.or.L_ute).and..not.(L0_use)
      L0_ate=.not.L_ute
C TYAGA_OY_HANDLER.fmg( 128, 266):RS �������,nv2dyn$105Dasha
      L_ili = L_axe.OR.L_ute
C TYAGA_OY_HANDLER.fmg( 142, 281):���
      L_aki=(L_ufad.or.L_aki).and..not.(L_ofad)
      L0_ifi=.not.L_aki
C TYAGA_OY_HANDLER.fmg( 117, 304):RS �������,nv2dyn$86Dasha
      L0_eki = L_evi.AND.(.NOT.L_aki)
C TYAGA_OY_HANDLER.fmg( 126, 307):�
      L_ofi=(L_ofad.or.L_ofi).and..not.(L_ufad)
      L0_efi=.not.L_ofi
C TYAGA_OY_HANDLER.fmg( 116, 293):RS �������,nv2dyn$87Dasha
      L0_ufi = L_avi.AND.(.NOT.L_ofi)
C TYAGA_OY_HANDLER.fmg( 126, 298):�
      L_eli = L0_eki.OR.L0_ufi
C TYAGA_OY_HANDLER.fmg( 132, 306):���
      L0_ame=R_afi.gt.R0_ole
C TYAGA_OY_HANDLER.fmg( 222, 265):���������� >,nv2dyn$45Dasha
      L0_ali = L_osi.AND.L0_ame
C TYAGA_OY_HANDLER.fmg( 254, 268):�
      L0_ule=R_afi.lt.R0_ile
C TYAGA_OY_HANDLER.fmg( 222, 259):���������� <,nv2dyn$46Dasha
      L0_uki = L_esi.AND.L0_ule
C TYAGA_OY_HANDLER.fmg( 254, 257):�
      L0_ori =.NOT.(L_edad.OR.L_odo.OR.L0_omi.OR.L0_imi.OR.L0_emi.OR.L0_
     &ami.OR.L0_uli.OR.L0_oli.OR.L_ivi.OR.L_usi.OR.L_isi.OR.L_ili.OR.L_e
     &li.OR.L0_ali.OR.L0_uki.OR.L_oki.OR.L_iki)
C TYAGA_OY_HANDLER.fmg( 288, 308):���
      L0_uri =.NOT.(L0_ori)
C TYAGA_OY_HANDLER.fmg( 298, 280):���
      L_udad = L0_uri.OR.L_iri
C TYAGA_OY_HANDLER.fmg( 302, 280):���
      L0_osu = L_udad.OR.L_asu
C TYAGA_OY_HANDLER.fmg(  66, 450):���
      L0_odad = L0_otu.AND.(.NOT.L0_osu).AND.(.NOT.L_uru)
C TYAGA_OY_HANDLER.fmg(  74, 443):�
      L0_umad = L0_odad.OR.L0_idad.OR.L_edad
C TYAGA_OY_HANDLER.fmg(  78, 436):���
      L0_isu = L_udad.OR.L_esu
C TYAGA_OY_HANDLER.fmg(  63, 404):���
      L0_adad = (.NOT.L0_isu).AND.L0_itu.AND.(.NOT.L_oru)
C TYAGA_OY_HANDLER.fmg(  74, 396):�
      L0_ilad = L0_adad.OR.L0_ubad
C TYAGA_OY_HANDLER.fmg(  78, 394):���
      L0_avu = L0_umad.OR.L0_ilad
C TYAGA_OY_HANDLER.fmg(  84, 406):���
      L0_utu = L_ofad.OR.L0_emu.OR.L_ufad
C TYAGA_OY_HANDLER.fmg(  68, 410):���
      L_evu=(L0_utu.or.L_evu).and..not.(L0_avu)
      L0_ivu=.not.L_evu
C TYAGA_OY_HANDLER.fmg( 114, 408):RS �������,10
      L_ovu = L_ulu.OR.L_evu
C TYAGA_OY_HANDLER.fmg( 125, 410):���
      L0_apad = L_ufad.AND.(.NOT.L_oti)
C TYAGA_OY_HANDLER.fmg(  82, 462):�
      L0_ulad = (.NOT.L_ovu).AND.L0_apad
C TYAGA_OY_HANDLER.fmg( 113, 462):�
      L0_akad = L_ovu.OR.L_ipad
C TYAGA_OY_HANDLER.fmg( 106, 416):���
      L0_ux = L_ufad.AND.L_isi
C TYAGA_OY_HANDLER.fmg(  86, 348):�
      L0_ox = L_ofad.AND.L_usi
C TYAGA_OY_HANDLER.fmg(  86, 336):�
      L0_opu = L0_ux.OR.L0_ox
C TYAGA_OY_HANDLER.fmg(  94, 346):���
      L0_emad = L0_apad.OR.L0_akad.OR.L0_ilad.OR.L0_opu
C TYAGA_OY_HANDLER.fmg( 112, 430):���
      L0_epad = (.NOT.L0_apad).AND.L0_umad
C TYAGA_OY_HANDLER.fmg( 112, 438):�
      L_imad=(L0_epad.or.L_imad).and..not.(L0_emad)
      L0_omad=.not.L_imad
C TYAGA_OY_HANDLER.fmg( 119, 436):RS �������,1
      L0_amad = (.NOT.L0_ulad).AND.L_imad
C TYAGA_OY_HANDLER.fmg( 138, 438):�
      L0_ido = L0_amad.AND.(.NOT.L_odo)
C TYAGA_OY_HANDLER.fmg( 276, 466):�
      L0_iko = L0_ido.OR.L_edad
C TYAGA_OY_HANDLER.fmg( 289, 464):���
      if(L0_iko) then
         R0_ofo=R0_efo
      else
         R0_ofo=R0_ako
      endif
C TYAGA_OY_HANDLER.fmg( 311, 477):���� RE IN LO CH7
      L0_elad = L_ofad.AND.(.NOT.L_iti)
C TYAGA_OY_HANDLER.fmg(  54, 365):�
      L0_okad = L0_akad.OR.L0_elad.OR.L0_umad.OR.L0_opu
C TYAGA_OY_HANDLER.fmg( 112, 386):���
      L0_olad = L0_ilad.AND.(.NOT.L0_elad)
C TYAGA_OY_HANDLER.fmg( 112, 394):�
      L_ukad=(L0_olad.or.L_ukad).and..not.(L0_okad)
      L0_alad=.not.L_ukad
C TYAGA_OY_HANDLER.fmg( 119, 392):RS �������,2
      L0_ekad = (.NOT.L_ovu).AND.L0_elad
C TYAGA_OY_HANDLER.fmg( 111, 368):�
      L0_ikad = L_ukad.AND.(.NOT.L0_ekad)
C TYAGA_OY_HANDLER.fmg( 138, 392):�
      L0_edo = L0_ikad.AND.(.NOT.L_edad)
C TYAGA_OY_HANDLER.fmg( 276, 452):�
      L0_eko = L0_edo.OR.L_odo
C TYAGA_OY_HANDLER.fmg( 289, 450):���
      if(L0_eko) then
         R0_ifo=R0_efo
      else
         R0_ifo=R0_ako
      endif
C TYAGA_OY_HANDLER.fmg( 311, 459):���� RE IN LO CH7
      R0_ufo = R0_ofo + (-R0_ifo)
C TYAGA_OY_HANDLER.fmg( 317, 470):��������
      if(L_ipad) then
         R_afo=R0_udo
      else
         R_afo=R0_ufo
      endif
C TYAGA_OY_HANDLER.fmg( 324, 468):���� RE IN LO CH7
      R0_ar = R_afo + R0_ur
C TYAGA_OY_HANDLER.fmg( 328, 397):��������
      R0_ir = R0_ar * R0_ur
C TYAGA_OY_HANDLER.fmg( 332, 396):����������
      L0_as=R0_ir.lt.R0_er
C TYAGA_OY_HANDLER.fmg( 338, 395):���������� <
      L_es=(L0_as.or.L_es).and..not.(.NOT.L_uvi)
      L0_up=.not.L_es
C TYAGA_OY_HANDLER.fmg( 344, 393):RS �������
      if(L_es) then
         R0_os=R0_is
      else
         R0_os=R_afo
      endif
C TYAGA_OY_HANDLER.fmg( 346, 404):���� RE IN LO CH7
      R0_at = R0_us + R0_os
C TYAGA_OY_HANDLER.fmg( 352, 406):��������
      R0_ip=MAX(R0_ep,R0_at)
C TYAGA_OY_HANDLER.fmg( 360, 407):��������
      R0_et=MIN(R0_ap,R0_ip)
C TYAGA_OY_HANDLER.fmg( 368, 408):�������
      if(L_ivi) then
         R0_op=R0_et
      endif
C TYAGA_OY_HANDLER.fmg( 380, 412):���� � ������������� �������
      if(L_es) then
         R0_op=R_it
      endif
C TYAGA_OY_HANDLER.fmg( 338, 412):���� � ������������� �������
      if(L_usi) then
         R0_am=R0_em
      else
         R0_am=R0_op
      endif
C TYAGA_OY_HANDLER.fmg( 414, 416):���� RE IN LO CH7,nv2dyn$56Dasha
      if(L_isi) then
         R_afi=R0_ul
      else
         R_afi=R0_am
      endif
C TYAGA_OY_HANDLER.fmg( 432, 416):���� RE IN LO CH7,nv2dyn$56Dasha
      R_ol=R0_op
C TYAGA_OY_HANDLER.fmg( 319, 403):����������� ��
      R_il=R0_op
C TYAGA_OY_HANDLER.fmg( 349, 414):����������� ��
      if(L_ivi) then
         R0_om=R0_um
      else
         R0_om=R0_et
      endif
C TYAGA_OY_HANDLER.fmg( 387, 406):���� RE IN LO CH7
      R_al=R0_om
C TYAGA_OY_HANDLER.fmg( 386, 412):����������� ��
      R_el=R0_om
C TYAGA_OY_HANDLER.fmg( 394, 406):����������� ��
      if(L0_ikad) then
         I_ivo=I0_avo
      else
         I_ivo=I0_evo
      endif
C TYAGA_OY_HANDLER.fmg( 204, 380):���� RE IN LO CH7
      L0_obu = L0_ax.OR.L0_ikad
C TYAGA_OY_HANDLER.fmg( 182, 396):���
      L0_ixu = L0_amad.OR.L0_ikad
C TYAGA_OY_HANDLER.fmg( 250, 379):���
      L0_oxu = L0_ixu.AND.(.NOT.L_uvu)
C TYAGA_OY_HANDLER.fmg( 266, 378):�
      if(L0_oxu) then
         R0_ibad=R_abad
      else
         R0_ibad=R0_uxu
      endif
C TYAGA_OY_HANDLER.fmg( 268, 386):���� RE IN LO CH7
      if(L0_elad) then
         I_eso=I0_iso
      else
         I_eso=I0_oso
      endif
C TYAGA_OY_HANDLER.fmg( 150, 363):���� RE IN LO CH7
      if(L0_elad) then
         I_imu=I0_omu
      else
         I_imu=I0_umu
      endif
C TYAGA_OY_HANDLER.fmg( 126, 376):���� RE IN LO CH7
      if(L0_amad) then
         I_uto=I0_ito
      else
         I_uto=I0_oto
      endif
C TYAGA_OY_HANDLER.fmg( 196, 430):���� RE IN LO CH7
      L0_adu = L0_iv.OR.L0_amad
C TYAGA_OY_HANDLER.fmg( 174, 444):���
      if(L0_apad) then
         I_ibo=I0_obo
      else
         I_ibo=I0_ubo
      endif
C TYAGA_OY_HANDLER.fmg( 134, 470):���� RE IN LO CH7
      if(L0_apad) then
         I_uso=I0_ato
      else
         I_uso=I0_eto
      endif
C TYAGA_OY_HANDLER.fmg( 128, 452):���� RE IN LO CH7
      L_etu = L_udad.OR.L0_atu.OR.L0_usu.OR.L0_osu.OR.L0_isu
C TYAGA_OY_HANDLER.fmg(  76, 416):���
      L0_apo = L_udad.OR.L_etu
C TYAGA_OY_HANDLER.fmg( 226, 483):���
      if(L0_apo) then
         I_epo=I0_ipo
      else
         I_epo=I0_opo
      endif
C TYAGA_OY_HANDLER.fmg( 232, 491):���� RE IN LO CH7
      if(L_etu) then
         I_upo=I0_omo
      else
         I_upo=I0_umo
      endif
C TYAGA_OY_HANDLER.fmg( 232, 472):���� RE IN LO CH7
      if(L_udad) then
         I_alu=I0_elu
      else
         I_alu=I0_ilu
      endif
C TYAGA_OY_HANDLER.fmg( 158, 384):���� RE IN LO CH7
      L_asi = L0_ori.OR.L_eri
C TYAGA_OY_HANDLER.fmg( 298, 286):���
      if(L_ufad) then
         C20_api=C20_epi
      else
         C20_api=C20_ipi
      endif
C TYAGA_OY_HANDLER.fmg(  32, 312):���� RE IN LO CH20
      if(L_ofad) then
         C20_opi=C20_umi
      else
         C20_opi=C20_api
      endif
C TYAGA_OY_HANDLER.fmg(  48, 311):���� RE IN LO CH20
      L0_efu = (.NOT.L0_iv).AND.L_ufad
C TYAGA_OY_HANDLER.fmg( 165, 404):�
      if(L0_efu) then
         I0_ifu=I0_ofu
      else
         I0_ifu=I0_ufu
      endif
C TYAGA_OY_HANDLER.fmg( 170, 410):���� RE IN LO CH7
      if(L0_obu) then
         I0_ipu=I0_ubu
      else
         I0_ipu=I0_ifu
      endif
C TYAGA_OY_HANDLER.fmg( 181, 409):���� RE IN LO CH7
      if(L_udad) then
         I_apu=I0_epu
      else
         I_apu=I0_ipu
      endif
C TYAGA_OY_HANDLER.fmg( 200, 408):���� RE IN LO CH7
      L0_idu = L_ofad.AND.(.NOT.L0_ax)
C TYAGA_OY_HANDLER.fmg( 158, 458):�
      if(L0_idu) then
         I0_odu=I0_udu
      else
         I0_odu=I0_afu
      endif
C TYAGA_OY_HANDLER.fmg( 164, 465):���� RE IN LO CH7
      if(L0_adu) then
         I0_ifad=I0_edu
      else
         I0_ifad=I0_odu
      endif
C TYAGA_OY_HANDLER.fmg( 176, 464):���� RE IN LO CH7
      if(L_udad) then
         I_afad=I0_efad
      else
         I_afad=I0_ifad
      endif
C TYAGA_OY_HANDLER.fmg( 194, 463):���� RE IN LO CH7
      R_ud=R0_ex
C TYAGA_OY_HANDLER.fmg( 349, 461):����������� ��
      R0_ebad = R8_obad
C TYAGA_OY_HANDLER.fmg( 264, 395):��������
C label 642  try642=try642-1
      R8_obad = R0_ibad + R0_ebad
C TYAGA_OY_HANDLER.fmg( 274, 394):��������
C sav1=R0_ebad
      R0_ebad = R8_obad
C TYAGA_OY_HANDLER.fmg( 264, 395):recalc:��������
C if(sav1.ne.R0_ebad .and. try642.gt.0) goto 642
      End
