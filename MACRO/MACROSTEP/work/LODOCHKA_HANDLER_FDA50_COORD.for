      Subroutine LODOCHKA_HANDLER_FDA50_COORD(ext_deltat,L_e
     &,L_o,L_ad,I_af,I_if,I_ek,I_ok,I_il,I_ul,I_om,I_ap,I_up
     &,I_er,I_as,I_is,I_et,I_ot,I_iv,I_uv,I_ax,I_ex,I_abe
     &,I_ibe,I_ede,I_ode,I_ife,I_ufe,I_oke,I_ale,I_ule,I_eme
     &,I_ape,I_ipe,I_ere,I_ore,I_ise,I_use,I_ote,I_ave,I_uve
     &,I_exe,I_abi,I_ibi,I_edi,I_odi,I_ifi,I_ufi,I_oki,I_ali
     &,I_uli,I_emi,I_api,I_ipi,I_eri,I_ori,I_isi,I_usi,I_oti
     &,I_avi,I_uvi,I_exi,I_ebo,I_edo,I_afo,I_ifo,I_eko,I_oko
     &,I_ilo,I_ulo,I_omo,I_apo,I_upo,I_ero,I_aso,I_iso,I_eto
     &,I_oto,I_ivo,I_uvo,I_oxo,I_abu,I_ubu,I_edu,I_afu,I_ifu
     &,I_eku,I_oku,I_ilu,I_ulu,I_omu,I_apu,I_upu,I_eru,I_asu
     &,I_isu,I_etu,I_otu,I_ivu,I_uvu,I_oxu,I_abad,I_ubad,I_edad
     &,I_afad,I_ifad,I_ekad,I_okad,I_ilad,I_ulad,I_omad,I_apad
     &,I_upad,I_erad,I_asad,I_isad,I_etad,I_otad,I_ivad,I_uvad
     &,I_oxad,I_abed,I_ubed,I_eded,I_afed,I_ifed,I_eked,I_oked
     &,I_iled,I_uled,I_omed,I_aped,I_uped,I_ered,I_ased,I_ised
     &,I_eted,I_oted,I_ived,I_uved,I_oxed,I_abid,I_ubid,I_edid
     &,I_afid,I_ifid,I_ekid,I_okid,I_ilid,I_ulid,I_omid,I_apid
     &,I_upid,I_erid,I_asid,I_isid,I_etid,I_otid,I_ivid,I_uvid
     &,I_oxid,I_abod,I_ubod,I_edod,I_idod,I_afod,I_efod,I_ofod
     &,I_ufod,I_akod,I_ekod,L_ikod,L_akik,L_ekik,L_atik,L_etik
     &,L_itik,L_otik,L_utik,L_avik,L_evik,L_ivik,L_ovik,L_uvik
     &,L_axik,L_exik,L_ixik,L_oxik,L_uxik,L_abok,L_ebok,L_ibok
     &,L_odok,I_ofok)
C |L_e           |1 1 I|LODOCHKA_FREE_STATE|��������� ������� � ��������� "������ �������"||
C |L_o           |1 1 I|LODOCHKA_KTS_STATE|��������� ������� � ��������� "��������� �����"||
C |L_ad          |1 1 I|LODOCHKA_PRESS_STATE|��������� ������� � ��������� "������������ �����"||
C |I_af          |2 4 O|CIL_FDA50_80    |�������������� � �������������||
C |I_if          |2 4 K|_lcmpJ25649     |�������� ������ �����������|5080|
C |I_ek          |2 4 O|CIL_FDA50_79    |�������������� � �������������||
C |I_ok          |2 4 K|_lcmpJ25622     |�������� ������ �����������|5079|
C |I_il          |2 4 O|CIL_FDA50_78    |�������������� � �������������||
C |I_ul          |2 4 K|_lcmpJ25595     |�������� ������ �����������|5078|
C |I_om          |2 4 O|CIL_FDA50_77    |�������������� � �������������||
C |I_ap          |2 4 K|_lcmpJ25568     |�������� ������ �����������|5077|
C |I_up          |2 4 O|CIL_FDA50_76    |�������������� � �������������||
C |I_er          |2 4 K|_lcmpJ25541     |�������� ������ �����������|5076|
C |I_as          |2 4 O|CIL_FDA50_75    |�������������� � �������������||
C |I_is          |2 4 K|_lcmpJ25514     |�������� ������ �����������|5075|
C |I_et          |2 4 O|CIL_FDA50_74    |�������������� � �������������||
C |I_ot          |2 4 K|_lcmpJ25487     |�������� ������ �����������|5074|
C |I_iv          |2 4 O|CIL_FDA50_73    |�������������� � �������������||
C |I_uv          |2 4 K|_lcmpJ25460     |�������� ������ �����������|5073|
C |I_ax          |2 4 K|_lcmpJ25303     |�������� ������ �����������|5029|
C |I_ex          |2 4 K|_lcmpJ25302     |�������� ������ �����������|5028|
C |I_abe         |2 4 O|CIL_FDA50_49    |�������������� � �������������||
C |I_ibe         |2 4 K|_lcmpJ25242     |�������� ������ �����������|5049|
C |I_ede         |2 4 O|CIL_FDA50_48    |�������������� � �������������||
C |I_ode         |2 4 K|_lcmpJ25215     |�������� ������ �����������|5048|
C |I_ife         |2 4 O|CIL_FDA50_47    |�������������� � �������������||
C |I_ufe         |2 4 K|_lcmpJ25188     |�������� ������ �����������|5047|
C |I_oke         |2 4 O|CIL_FDA50_46    |�������������� � �������������||
C |I_ale         |2 4 K|_lcmpJ25161     |�������� ������ �����������|5046|
C |I_ule         |2 4 O|CIL_FDA50_45    |�������������� � �������������||
C |I_eme         |2 4 K|_lcmpJ25134     |�������� ������ �����������|5045|
C |I_ape         |2 4 O|CIL_FDA50_44    |�������������� � �������������||
C |I_ipe         |2 4 K|_lcmpJ25107     |�������� ������ �����������|5044|
C |I_ere         |2 4 O|CIL_FDA50_43    |�������������� � �������������||
C |I_ore         |2 4 K|_lcmpJ25080     |�������� ������ �����������|5043|
C |I_ise         |2 4 O|CIL_FDA50_42    |�������������� � �������������||
C |I_use         |2 4 K|_lcmpJ25053     |�������� ������ �����������|5042|
C |I_ote         |2 4 O|CIL_FDA50_41    |�������������� � �������������||
C |I_ave         |2 4 K|_lcmpJ25026     |�������� ������ �����������|5041|
C |I_uve         |2 4 O|CIL_FDA50_40    |�������������� � �������������||
C |I_exe         |2 4 K|_lcmpJ24999     |�������� ������ �����������|5040|
C |I_abi         |2 4 O|CIL_FDA50_39    |�������������� � �������������||
C |I_ibi         |2 4 K|_lcmpJ24972     |�������� ������ �����������|5039|
C |I_edi         |2 4 O|CIL_FDA50_38    |�������������� � �������������||
C |I_odi         |2 4 K|_lcmpJ24945     |�������� ������ �����������|5038|
C |I_ifi         |2 4 O|CIL_FDA50_37    |�������������� � �������������||
C |I_ufi         |2 4 K|_lcmpJ24918     |�������� ������ �����������|5037|
C |I_oki         |2 4 O|CIL_FDA50_36    |�������������� � �������������||
C |I_ali         |2 4 K|_lcmpJ24891     |�������� ������ �����������|5036|
C |I_uli         |2 4 O|CIL_FDA50_35    |�������������� � �������������||
C |I_emi         |2 4 K|_lcmpJ24864     |�������� ������ �����������|5035|
C |I_api         |2 4 O|CIL_FDA50_34    |�������������� � �������������||
C |I_ipi         |2 4 K|_lcmpJ24837     |�������� ������ �����������|5034|
C |I_eri         |2 4 O|CIL_FDA50_33    |�������������� � �������������||
C |I_ori         |2 4 K|_lcmpJ24810     |�������� ������ �����������|5033|
C |I_isi         |2 4 O|CIL_FDA50_32    |�������������� � �������������||
C |I_usi         |2 4 K|_lcmpJ24783     |�������� ������ �����������|5032|
C |I_oti         |2 4 O|CIL_FDA50_31    |�������������� � �������������||
C |I_avi         |2 4 K|_lcmpJ24756     |�������� ������ �����������|5031|
C |I_uvi         |2 4 O|CIL_FDA50_30    |�������������� � �������������||
C |I_exi         |2 4 K|_lcmpJ24729     |�������� ������ �����������|5030|
C |I_ebo         |2 4 O|CIL_FDA50_29    |�������������� � �������������||
C |I_edo         |2 4 O|CIL_FDA50_28    |�������������� � �������������||
C |I_afo         |2 4 O|CIL_FDA50_27    |�������������� � �������������||
C |I_ifo         |2 4 K|_lcmpJ24648     |�������� ������ �����������|5027|
C |I_eko         |2 4 O|CIL_FDA50_26    |�������������� � �������������||
C |I_oko         |2 4 K|_lcmpJ24621     |�������� ������ �����������|5026|
C |I_ilo         |2 4 O|CIL_FDA50_53    |�������������� � �������������||
C |I_ulo         |2 4 K|_lcmpJ24567     |�������� ������ �����������|5053|
C |I_omo         |2 4 O|CIL_FDA50_52    |�������������� � �������������||
C |I_apo         |2 4 K|_lcmpJ24540     |�������� ������ �����������|5052|
C |I_upo         |2 4 O|CIL_FDA50_51    |�������������� � �������������||
C |I_ero         |2 4 K|_lcmpJ24513     |�������� ������ �����������|5051|
C |I_aso         |2 4 O|CIL_FDA50_50    |�������������� � �������������||
C |I_iso         |2 4 K|_lcmpJ24486     |�������� ������ �����������|5050|
C |I_eto         |2 4 O|CIL_FDA50_72    |�������������� � �������������||
C |I_oto         |2 4 K|_lcmpJ24459     |�������� ������ �����������|5072|
C |I_ivo         |2 4 O|CIL_FDA50_71    |�������������� � �������������||
C |I_uvo         |2 4 K|_lcmpJ24432     |�������� ������ �����������|5071|
C |I_oxo         |2 4 O|CIL_FDA50_70    |�������������� � �������������||
C |I_abu         |2 4 K|_lcmpJ24405     |�������� ������ �����������|5070|
C |I_ubu         |2 4 O|CIL_FDA50_69    |�������������� � �������������||
C |I_edu         |2 4 K|_lcmpJ24378     |�������� ������ �����������|5069|
C |I_afu         |2 4 O|CIL_FDA50_68    |�������������� � �������������||
C |I_ifu         |2 4 K|_lcmpJ24351     |�������� ������ �����������|5068|
C |I_eku         |2 4 O|CIL_FDA50_67    |�������������� � �������������||
C |I_oku         |2 4 K|_lcmpJ24324     |�������� ������ �����������|5067|
C |I_ilu         |2 4 O|CIL_FDA50_66    |�������������� � �������������||
C |I_ulu         |2 4 K|_lcmpJ24297     |�������� ������ �����������|5066|
C |I_omu         |2 4 O|CIL_FDA50_65    |�������������� � �������������||
C |I_apu         |2 4 K|_lcmpJ24270     |�������� ������ �����������|5065|
C |I_upu         |2 4 O|CIL_FDA50_64    |�������������� � �������������||
C |I_eru         |2 4 K|_lcmpJ24243     |�������� ������ �����������|5064|
C |I_asu         |2 4 O|CIL_FDA50_63    |�������������� � �������������||
C |I_isu         |2 4 K|_lcmpJ24216     |�������� ������ �����������|5063|
C |I_etu         |2 4 O|CIL_FDA50_62    |�������������� � �������������||
C |I_otu         |2 4 K|_lcmpJ24189     |�������� ������ �����������|5062|
C |I_ivu         |2 4 O|CIL_FDA50_61    |�������������� � �������������||
C |I_uvu         |2 4 K|_lcmpJ24162     |�������� ������ �����������|5061|
C |I_oxu         |2 4 O|CIL_FDA50_60    |�������������� � �������������||
C |I_abad        |2 4 K|_lcmpJ24135     |�������� ������ �����������|5060|
C |I_ubad        |2 4 O|CIL_FDA50_59    |�������������� � �������������||
C |I_edad        |2 4 K|_lcmpJ24108     |�������� ������ �����������|5059|
C |I_afad        |2 4 O|CIL_FDA50_58    |�������������� � �������������||
C |I_ifad        |2 4 K|_lcmpJ24081     |�������� ������ �����������|5058|
C |I_ekad        |2 4 O|CIL_FDA50_57    |�������������� � �������������||
C |I_okad        |2 4 K|_lcmpJ24054     |�������� ������ �����������|5057|
C |I_ilad        |2 4 O|CIL_FDA50_56    |�������������� � �������������||
C |I_ulad        |2 4 K|_lcmpJ24027     |�������� ������ �����������|5056|
C |I_omad        |2 4 O|CIL_FDA50_55    |�������������� � �������������||
C |I_apad        |2 4 K|_lcmpJ24000     |�������� ������ �����������|5055|
C |I_upad        |2 4 O|CIL_FDA50_54    |�������������� � �������������||
C |I_erad        |2 4 K|_lcmpJ23973     |�������� ������ �����������|5054|
C |I_asad        |2 4 O|CIL_FDA50_25    |�������������� � �������������||
C |I_isad        |2 4 K|_lcmpJ23946     |�������� ������ �����������|5025|
C |I_etad        |2 4 O|CIL_FDA50_24    |�������������� � �������������||
C |I_otad        |2 4 K|_lcmpJ23919     |�������� ������ �����������|5024|
C |I_ivad        |2 4 O|CIL_FDA50_23    |�������������� � �������������||
C |I_uvad        |2 4 K|_lcmpJ23892     |�������� ������ �����������|5023|
C |I_oxad        |2 4 O|CIL_FDA50_22    |�������������� � �������������||
C |I_abed        |2 4 K|_lcmpJ23865     |�������� ������ �����������|5022|
C |I_ubed        |2 4 O|CIL_FDA50_21    |�������������� � �������������||
C |I_eded        |2 4 K|_lcmpJ23838     |�������� ������ �����������|5021|
C |I_afed        |2 4 O|CIL_FDA50_20    |�������������� � �������������||
C |I_ifed        |2 4 K|_lcmpJ23811     |�������� ������ �����������|5020|
C |I_eked        |2 4 O|CIL_FDA50_19    |�������������� � �������������||
C |I_oked        |2 4 K|_lcmpJ23784     |�������� ������ �����������|5019|
C |I_iled        |2 4 O|CIL_FDA50_18    |�������������� � �������������||
C |I_uled        |2 4 K|_lcmpJ23757     |�������� ������ �����������|5018|
C |I_omed        |2 4 O|CIL_FDA50_17    |�������������� � �������������||
C |I_aped        |2 4 K|_lcmpJ23730     |�������� ������ �����������|5017|
C |I_uped        |2 4 O|CIL_FDA50_16    |�������������� � �������������||
C |I_ered        |2 4 K|_lcmpJ23703     |�������� ������ �����������|5016|
C |I_ased        |2 4 O|CIL_FDA50_15    |�������������� � �������������||
C |I_ised        |2 4 K|_lcmpJ23676     |�������� ������ �����������|5015|
C |I_eted        |2 4 O|CIL_FDA50_14    |�������������� � �������������||
C |I_oted        |2 4 K|_lcmpJ23649     |�������� ������ �����������|5014|
C |I_ived        |2 4 O|CIL_FDA50_13    |�������������� � �������������||
C |I_uved        |2 4 K|_lcmpJ23622     |�������� ������ �����������|5013|
C |I_oxed        |2 4 O|CIL_FDA50_12    |�������������� � �������������||
C |I_abid        |2 4 K|_lcmpJ23595     |�������� ������ �����������|5012|
C |I_ubid        |2 4 O|CIL_FDA50_11    |�������������� � �������������||
C |I_edid        |2 4 K|_lcmpJ23568     |�������� ������ �����������|5011|
C |I_afid        |2 4 O|CIL_FDA50_10    |�������������� � �������������||
C |I_ifid        |2 4 K|_lcmpJ23541     |�������� ������ �����������|5010|
C |I_ekid        |2 4 O|CIL_FDA50_9     |�������������� � �������������||
C |I_okid        |2 4 K|_lcmpJ23514     |�������� ������ �����������|5009|
C |I_ilid        |2 4 O|CIL_FDA50_8     |�������������� � �������������||
C |I_ulid        |2 4 K|_lcmpJ23487     |�������� ������ �����������|5008|
C |I_omid        |2 4 O|CIL_FDA50_7     |�������������� � �������������||
C |I_apid        |2 4 K|_lcmpJ23460     |�������� ������ �����������|5007|
C |I_upid        |2 4 O|CIL_FDA50_6     |�������������� � �������������||
C |I_erid        |2 4 K|_lcmpJ23433     |�������� ������ �����������|5006|
C |I_asid        |2 4 O|CIL_FDA50_5     |�������������� � �������������||
C |I_isid        |2 4 K|_lcmpJ23406     |�������� ������ �����������|5005|
C |I_etid        |2 4 O|CIL_FDA50_4     |�������������� � �������������||
C |I_otid        |2 4 K|_lcmpJ23379     |�������� ������ �����������|5004|
C |I_ivid        |2 4 O|CIL_FDA50_3     |�������������� � �������������||
C |I_uvid        |2 4 K|_lcmpJ23324     |�������� ������ �����������|5003|
C |I_oxid        |2 4 O|CIL_FDA50_2     |�������������� � �������������||
C |I_abod        |2 4 K|_lcmpJ23297     |�������� ������ �����������|5002|
C |I_ubod        |2 4 K|_lcmpJ23239     |�������� ������ �����������|2|
C |I_edod        |2 4 K|_lcmpJ23238     |�������� ������ �����������|1|
C |I_idod        |2 4 K|_lcmpJ23236     |�������� ������ �����������|0|
C |I_afod        |2 4 O|CIL_FDA50_1     |�������������� � �������������||
C |I_efod        |2 4 O|STATE           |��������� �������||
C |I_ofod        |2 4 K|_lcmpJ23124     |�������� ������ �����������|5001|
C |I_ufod        |2 4 K|_colJ20190*     |�������� ��������� ����� |16#0100000A|
C |I_akod        |2 4 K|_colJ20189*     |�������� ��������� ����� |16#01000007|
C |I_ekod        |2 4 K|_colJ20188*     |�������� ��������� ����� |16#01000010|
C |L_ikod        |1 1 I|FDA52AE401_CATCH2|������� ��������� �� (������ ��������)||
C |L_akik        |1 1 I|FDA52AE501KE01_pos9|�������, ������� 9||
C |L_ekik        |1 1 I|FDA52AE501KE01_pos10|�������, ������� 10||
C |L_atik        |1 1 I|FDA50_ST_CATCH  |������� �� �������� � ����||
C |L_etik        |1 1 I|FDA52AE501KE01_pos8|�������, ������� 8||
C |L_itik        |1 1 I|FDA52AE501KE01_pos7|�������, ������� 7||
C |L_otik        |1 1 I|FDA52AE501KE02_pos1|�����������, ������� 1||
C |L_utik        |1 1 I|FDA52AE501KE02_pos8|�����������, ������� 8||
C |L_avik        |1 1 I|FDA52AE501KE02_pos7|�����������, ������� 7||
C |L_evik        |1 1 I|FDA52AE501KE02_pos6|�����������, ������� 6||
C |L_ivik        |1 1 I|FDA52AE501KE02_pos5|�����������, ������� 5||
C |L_ovik        |1 1 I|FDA52AE501KE02_pos4|�����������, ������� 4||
C |L_uvik        |1 1 I|FDA52AE501KE02_pos3|�����������, ������� 3||
C |L_axik        |1 1 I|FDA52AE501KE02_pos2|�����������, ������� 2||
C |L_exik        |1 1 I|FDA52AE501KE01_pos6|�������, ������� 6||
C |L_ixik        |1 1 I|FDA52AE501KE01_pos5|�������, ������� 5||
C |L_oxik        |1 1 I|FDA52AE501KE01_pos4|�������, ������� 4||
C |L_uxik        |1 1 I|FDA52AE501KE01_pos3|�������, ������� 3||
C |L_abok        |1 1 I|FDA52AE501KE01_pos2|�������, ������� 2||
C |L_ebok        |1 1 I|FDA52AE501KE01_pos1|�������, ������� 1||
C |L_ibok        |1 1 I|FDA52AE401_CATCH|������� ��������� ��||
C |L_odok        |1 1 I|FDA52AE501_CATCH|������� ��������� �������������||
C |I_ofok        |2 4 O|CR              |�������������� � �������������||

      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      LOGICAL*1 L_e
      INTEGER*4 I0_i
      LOGICAL*1 L_o
      INTEGER*4 I0_u
      LOGICAL*1 L_ad
      INTEGER*4 I0_ed
      LOGICAL*1 L0_id,L0_od,L0_ud
      INTEGER*4 I_af
      LOGICAL*1 L0_ef
      INTEGER*4 I_if
      LOGICAL*1 L0_of,L0_uf,L0_ak
      INTEGER*4 I_ek
      LOGICAL*1 L0_ik
      INTEGER*4 I_ok
      LOGICAL*1 L0_uk,L0_al,L0_el
      INTEGER*4 I_il
      LOGICAL*1 L0_ol
      INTEGER*4 I_ul
      LOGICAL*1 L0_am,L0_em,L0_im
      INTEGER*4 I_om
      LOGICAL*1 L0_um
      INTEGER*4 I_ap
      LOGICAL*1 L0_ep,L0_ip,L0_op
      INTEGER*4 I_up
      LOGICAL*1 L0_ar
      INTEGER*4 I_er
      LOGICAL*1 L0_ir,L0_or,L0_ur
      INTEGER*4 I_as
      LOGICAL*1 L0_es
      INTEGER*4 I_is
      LOGICAL*1 L0_os,L0_us,L0_at
      INTEGER*4 I_et
      LOGICAL*1 L0_it
      INTEGER*4 I_ot
      LOGICAL*1 L0_ut,L0_av,L0_ev
      INTEGER*4 I_iv
      LOGICAL*1 L0_ov
      INTEGER*4 I_uv,I_ax,I_ex
      LOGICAL*1 L0_ix,L0_ox,L0_ux
      INTEGER*4 I_abe
      LOGICAL*1 L0_ebe
      INTEGER*4 I_ibe
      LOGICAL*1 L0_obe,L0_ube,L0_ade
      INTEGER*4 I_ede
      LOGICAL*1 L0_ide
      INTEGER*4 I_ode
      LOGICAL*1 L0_ude,L0_afe,L0_efe
      INTEGER*4 I_ife
      LOGICAL*1 L0_ofe
      INTEGER*4 I_ufe
      LOGICAL*1 L0_ake,L0_eke,L0_ike
      INTEGER*4 I_oke
      LOGICAL*1 L0_uke
      INTEGER*4 I_ale
      LOGICAL*1 L0_ele,L0_ile,L0_ole
      INTEGER*4 I_ule
      LOGICAL*1 L0_ame
      INTEGER*4 I_eme
      LOGICAL*1 L0_ime,L0_ome,L0_ume
      INTEGER*4 I_ape
      LOGICAL*1 L0_epe
      INTEGER*4 I_ipe
      LOGICAL*1 L0_ope,L0_upe,L0_are
      INTEGER*4 I_ere
      LOGICAL*1 L0_ire
      INTEGER*4 I_ore
      LOGICAL*1 L0_ure,L0_ase,L0_ese
      INTEGER*4 I_ise
      LOGICAL*1 L0_ose
      INTEGER*4 I_use
      LOGICAL*1 L0_ate,L0_ete,L0_ite
      INTEGER*4 I_ote
      LOGICAL*1 L0_ute
      INTEGER*4 I_ave
      LOGICAL*1 L0_eve,L0_ive,L0_ove
      INTEGER*4 I_uve
      LOGICAL*1 L0_axe
      INTEGER*4 I_exe
      LOGICAL*1 L0_ixe,L0_oxe,L0_uxe
      INTEGER*4 I_abi
      LOGICAL*1 L0_ebi
      INTEGER*4 I_ibi
      LOGICAL*1 L0_obi,L0_ubi,L0_adi
      INTEGER*4 I_edi
      LOGICAL*1 L0_idi
      INTEGER*4 I_odi
      LOGICAL*1 L0_udi,L0_afi,L0_efi
      INTEGER*4 I_ifi
      LOGICAL*1 L0_ofi
      INTEGER*4 I_ufi
      LOGICAL*1 L0_aki,L0_eki,L0_iki
      INTEGER*4 I_oki
      LOGICAL*1 L0_uki
      INTEGER*4 I_ali
      LOGICAL*1 L0_eli,L0_ili,L0_oli
      INTEGER*4 I_uli
      LOGICAL*1 L0_ami
      INTEGER*4 I_emi
      LOGICAL*1 L0_imi,L0_omi,L0_umi
      INTEGER*4 I_api
      LOGICAL*1 L0_epi
      INTEGER*4 I_ipi
      LOGICAL*1 L0_opi,L0_upi,L0_ari
      INTEGER*4 I_eri
      LOGICAL*1 L0_iri
      INTEGER*4 I_ori
      LOGICAL*1 L0_uri,L0_asi,L0_esi
      INTEGER*4 I_isi
      LOGICAL*1 L0_osi
      INTEGER*4 I_usi
      LOGICAL*1 L0_ati,L0_eti,L0_iti
      INTEGER*4 I_oti
      LOGICAL*1 L0_uti
      INTEGER*4 I_avi
      LOGICAL*1 L0_evi,L0_ivi,L0_ovi
      INTEGER*4 I_uvi
      LOGICAL*1 L0_axi
      INTEGER*4 I_exi
      LOGICAL*1 L0_ixi,L0_oxi,L0_uxi,L0_abo
      INTEGER*4 I_ebo
      LOGICAL*1 L0_ibo,L0_obo,L0_ubo,L0_ado
      INTEGER*4 I_edo
      LOGICAL*1 L0_ido,L0_odo,L0_udo
      INTEGER*4 I_afo
      LOGICAL*1 L0_efo
      INTEGER*4 I_ifo
      LOGICAL*1 L0_ofo,L0_ufo,L0_ako
      INTEGER*4 I_eko
      LOGICAL*1 L0_iko
      INTEGER*4 I_oko
      LOGICAL*1 L0_uko,L0_alo,L0_elo
      INTEGER*4 I_ilo
      LOGICAL*1 L0_olo
      INTEGER*4 I_ulo
      LOGICAL*1 L0_amo,L0_emo,L0_imo
      INTEGER*4 I_omo
      LOGICAL*1 L0_umo
      INTEGER*4 I_apo
      LOGICAL*1 L0_epo,L0_ipo,L0_opo
      INTEGER*4 I_upo
      LOGICAL*1 L0_aro
      INTEGER*4 I_ero
      LOGICAL*1 L0_iro,L0_oro,L0_uro
      INTEGER*4 I_aso
      LOGICAL*1 L0_eso
      INTEGER*4 I_iso
      LOGICAL*1 L0_oso,L0_uso,L0_ato
      INTEGER*4 I_eto
      LOGICAL*1 L0_ito
      INTEGER*4 I_oto
      LOGICAL*1 L0_uto,L0_avo,L0_evo
      INTEGER*4 I_ivo
      LOGICAL*1 L0_ovo
      INTEGER*4 I_uvo
      LOGICAL*1 L0_axo,L0_exo,L0_ixo
      INTEGER*4 I_oxo
      LOGICAL*1 L0_uxo
      INTEGER*4 I_abu
      LOGICAL*1 L0_ebu,L0_ibu,L0_obu
      INTEGER*4 I_ubu
      LOGICAL*1 L0_adu
      INTEGER*4 I_edu
      LOGICAL*1 L0_idu,L0_odu,L0_udu
      INTEGER*4 I_afu
      LOGICAL*1 L0_efu
      INTEGER*4 I_ifu
      LOGICAL*1 L0_ofu,L0_ufu,L0_aku
      INTEGER*4 I_eku
      LOGICAL*1 L0_iku
      INTEGER*4 I_oku
      LOGICAL*1 L0_uku,L0_alu,L0_elu
      INTEGER*4 I_ilu
      LOGICAL*1 L0_olu
      INTEGER*4 I_ulu
      LOGICAL*1 L0_amu,L0_emu,L0_imu
      INTEGER*4 I_omu
      LOGICAL*1 L0_umu
      INTEGER*4 I_apu
      LOGICAL*1 L0_epu,L0_ipu,L0_opu
      INTEGER*4 I_upu
      LOGICAL*1 L0_aru
      INTEGER*4 I_eru
      LOGICAL*1 L0_iru,L0_oru,L0_uru
      INTEGER*4 I_asu
      LOGICAL*1 L0_esu
      INTEGER*4 I_isu
      LOGICAL*1 L0_osu,L0_usu,L0_atu
      INTEGER*4 I_etu
      LOGICAL*1 L0_itu
      INTEGER*4 I_otu
      LOGICAL*1 L0_utu,L0_avu,L0_evu
      INTEGER*4 I_ivu
      LOGICAL*1 L0_ovu
      INTEGER*4 I_uvu
      LOGICAL*1 L0_axu,L0_exu,L0_ixu
      INTEGER*4 I_oxu
      LOGICAL*1 L0_uxu
      INTEGER*4 I_abad
      LOGICAL*1 L0_ebad,L0_ibad,L0_obad
      INTEGER*4 I_ubad
      LOGICAL*1 L0_adad
      INTEGER*4 I_edad
      LOGICAL*1 L0_idad,L0_odad,L0_udad
      INTEGER*4 I_afad
      LOGICAL*1 L0_efad
      INTEGER*4 I_ifad
      LOGICAL*1 L0_ofad,L0_ufad,L0_akad
      INTEGER*4 I_ekad
      LOGICAL*1 L0_ikad
      INTEGER*4 I_okad
      LOGICAL*1 L0_ukad,L0_alad,L0_elad
      INTEGER*4 I_ilad
      LOGICAL*1 L0_olad
      INTEGER*4 I_ulad
      LOGICAL*1 L0_amad,L0_emad,L0_imad
      INTEGER*4 I_omad
      LOGICAL*1 L0_umad
      INTEGER*4 I_apad
      LOGICAL*1 L0_epad,L0_ipad,L0_opad
      INTEGER*4 I_upad
      LOGICAL*1 L0_arad
      INTEGER*4 I_erad
      LOGICAL*1 L0_irad,L0_orad,L0_urad
      INTEGER*4 I_asad
      LOGICAL*1 L0_esad
      INTEGER*4 I_isad
      LOGICAL*1 L0_osad,L0_usad,L0_atad
      INTEGER*4 I_etad
      LOGICAL*1 L0_itad
      INTEGER*4 I_otad
      LOGICAL*1 L0_utad,L0_avad,L0_evad
      INTEGER*4 I_ivad
      LOGICAL*1 L0_ovad
      INTEGER*4 I_uvad
      LOGICAL*1 L0_axad,L0_exad,L0_ixad
      INTEGER*4 I_oxad
      LOGICAL*1 L0_uxad
      INTEGER*4 I_abed
      LOGICAL*1 L0_ebed,L0_ibed,L0_obed
      INTEGER*4 I_ubed
      LOGICAL*1 L0_aded
      INTEGER*4 I_eded
      LOGICAL*1 L0_ided,L0_oded,L0_uded
      INTEGER*4 I_afed
      LOGICAL*1 L0_efed
      INTEGER*4 I_ifed
      LOGICAL*1 L0_ofed,L0_ufed,L0_aked
      INTEGER*4 I_eked
      LOGICAL*1 L0_iked
      INTEGER*4 I_oked
      LOGICAL*1 L0_uked,L0_aled,L0_eled
      INTEGER*4 I_iled
      LOGICAL*1 L0_oled
      INTEGER*4 I_uled
      LOGICAL*1 L0_amed,L0_emed,L0_imed
      INTEGER*4 I_omed
      LOGICAL*1 L0_umed
      INTEGER*4 I_aped
      LOGICAL*1 L0_eped,L0_iped,L0_oped
      INTEGER*4 I_uped
      LOGICAL*1 L0_ared
      INTEGER*4 I_ered
      LOGICAL*1 L0_ired,L0_ored,L0_ured
      INTEGER*4 I_ased
      LOGICAL*1 L0_esed
      INTEGER*4 I_ised
      LOGICAL*1 L0_osed,L0_used,L0_ated
      INTEGER*4 I_eted
      LOGICAL*1 L0_ited
      INTEGER*4 I_oted
      LOGICAL*1 L0_uted,L0_aved,L0_eved
      INTEGER*4 I_ived
      LOGICAL*1 L0_oved
      INTEGER*4 I_uved
      LOGICAL*1 L0_axed,L0_exed,L0_ixed
      INTEGER*4 I_oxed
      LOGICAL*1 L0_uxed
      INTEGER*4 I_abid
      LOGICAL*1 L0_ebid,L0_ibid,L0_obid
      INTEGER*4 I_ubid
      LOGICAL*1 L0_adid
      INTEGER*4 I_edid
      LOGICAL*1 L0_idid,L0_odid,L0_udid
      INTEGER*4 I_afid
      LOGICAL*1 L0_efid
      INTEGER*4 I_ifid
      LOGICAL*1 L0_ofid,L0_ufid,L0_akid
      INTEGER*4 I_ekid
      LOGICAL*1 L0_ikid
      INTEGER*4 I_okid
      LOGICAL*1 L0_ukid,L0_alid,L0_elid
      INTEGER*4 I_ilid
      LOGICAL*1 L0_olid
      INTEGER*4 I_ulid
      LOGICAL*1 L0_amid,L0_emid,L0_imid
      INTEGER*4 I_omid
      LOGICAL*1 L0_umid
      INTEGER*4 I_apid
      LOGICAL*1 L0_epid,L0_ipid,L0_opid
      INTEGER*4 I_upid
      LOGICAL*1 L0_arid
      INTEGER*4 I_erid
      LOGICAL*1 L0_irid,L0_orid,L0_urid
      INTEGER*4 I_asid
      LOGICAL*1 L0_esid
      INTEGER*4 I_isid
      LOGICAL*1 L0_osid,L0_usid,L0_atid
      INTEGER*4 I_etid
      LOGICAL*1 L0_itid
      INTEGER*4 I_otid
      LOGICAL*1 L0_utid,L0_avid,L0_evid
      INTEGER*4 I_ivid
      LOGICAL*1 L0_ovid
      INTEGER*4 I_uvid
      LOGICAL*1 L0_axid,L0_exid,L0_ixid
      INTEGER*4 I_oxid
      LOGICAL*1 L0_uxid
      INTEGER*4 I_abod
      LOGICAL*1 L0_ebod,L0_ibod,L0_obod
      INTEGER*4 I_ubod
      LOGICAL*1 L0_adod
      INTEGER*4 I_edod,I_idod
      LOGICAL*1 L0_odod,L0_udod
      INTEGER*4 I_afod,I_efod
      LOGICAL*1 L0_ifod
      INTEGER*4 I_ofod,I_ufod,I_akod,I_ekod
      LOGICAL*1 L_ikod,L0_okod,L0_ukod,L0_alod,L0_elod,L0_ilod
     &,L0_olod,L0_ulod,L0_amod,L0_emod,L0_imod,L0_omod,L0_umod
     &,L0_apod,L0_epod,L0_ipod,L0_opod,L0_upod,L0_arod,L0_erod
     &,L0_irod,L0_orod
      LOGICAL*1 L0_urod,L0_asod,L0_esod,L0_isod,L0_osod,L0_usod
     &,L0_atod,L0_etod,L0_itod,L0_otod,L0_utod,L0_avod,L0_evod
     &,L0_ivod,L0_ovod,L0_uvod,L0_axod,L0_exod,L0_ixod,L0_oxod
     &,L0_uxod,L0_abud
      LOGICAL*1 L0_ebud,L0_ibud,L0_obud,L0_ubud,L0_adud,L0_edud
     &,L0_idud,L0_odud,L0_udud,L0_afud,L0_efud,L0_ifud,L0_ofud
     &,L0_ufud,L0_akud,L0_ekud,L0_ikud,L0_okud,L0_ukud,L0_alud
     &,L0_elud,L0_ilud
      LOGICAL*1 L0_olud,L0_ulud,L0_amud,L0_emud,L0_imud,L0_omud
     &,L0_umud,L0_apud,L0_epud,L0_ipud,L0_opud,L0_upud,L0_arud
     &,L0_erud,L0_irud,L0_orud,L0_urud,L0_asud,L0_esud
      INTEGER*4 I0_isud
      LOGICAL*1 L0_osud,L0_usud
      INTEGER*4 I0_atud
      LOGICAL*1 L0_etud,L0_itud
      INTEGER*4 I0_otud
      LOGICAL*1 L0_utud,L0_avud
      INTEGER*4 I0_evud
      LOGICAL*1 L0_ivud,L0_ovud
      INTEGER*4 I0_uvud
      LOGICAL*1 L0_axud,L0_exud,L0_ixud,L0_oxud,L0_uxud,L0_abaf
     &,L0_ebaf,L0_ibaf,L0_obaf
      INTEGER*4 I0_ubaf
      LOGICAL*1 L0_adaf,L0_edaf,L0_idaf
      INTEGER*4 I0_odaf
      LOGICAL*1 L0_udaf,L0_afaf,L0_efaf,L0_ifaf
      INTEGER*4 I0_ofaf
      LOGICAL*1 L0_ufaf,L0_akaf,L0_ekaf
      INTEGER*4 I0_ikaf
      LOGICAL*1 L0_okaf,L0_ukaf
      INTEGER*4 I0_alaf
      LOGICAL*1 L0_elaf
      INTEGER*4 I0_ilaf
      LOGICAL*1 L0_olaf
      INTEGER*4 I0_ulaf
      LOGICAL*1 L0_amaf,L0_emaf,L0_imaf
      INTEGER*4 I0_omaf
      LOGICAL*1 L0_umaf,L0_apaf
      INTEGER*4 I0_epaf
      LOGICAL*1 L0_ipaf,L0_opaf,L0_upaf,L0_araf
      INTEGER*4 I0_eraf
      LOGICAL*1 L0_iraf,L0_oraf,L0_uraf
      INTEGER*4 I0_asaf
      LOGICAL*1 L0_esaf,L0_isaf,L0_osaf
      INTEGER*4 I0_usaf
      LOGICAL*1 L0_ataf,L0_etaf,L0_itaf
      INTEGER*4 I0_otaf
      LOGICAL*1 L0_utaf,L0_avaf,L0_evaf
      INTEGER*4 I0_ivaf
      LOGICAL*1 L0_ovaf,L0_uvaf
      INTEGER*4 I0_axaf
      LOGICAL*1 L0_exaf,L0_ixaf,L0_oxaf,L0_uxaf
      INTEGER*4 I0_abef
      LOGICAL*1 L0_ebef,L0_ibef,L0_obef
      INTEGER*4 I0_ubef
      LOGICAL*1 L0_adef,L0_edef,L0_idef
      INTEGER*4 I0_odef
      LOGICAL*1 L0_udef
      INTEGER*4 I0_afef
      LOGICAL*1 L0_efef
      INTEGER*4 I0_ifef
      LOGICAL*1 L0_ofef
      INTEGER*4 I0_ufef
      LOGICAL*1 L0_akef
      INTEGER*4 I0_ekef
      LOGICAL*1 L0_ikef
      INTEGER*4 I0_okef
      LOGICAL*1 L0_ukef
      INTEGER*4 I0_alef
      LOGICAL*1 L0_elef
      INTEGER*4 I0_ilef
      LOGICAL*1 L0_olef
      INTEGER*4 I0_ulef
      LOGICAL*1 L0_amef
      INTEGER*4 I0_emef
      LOGICAL*1 L0_imef
      INTEGER*4 I0_omef
      LOGICAL*1 L0_umef
      INTEGER*4 I0_apef
      LOGICAL*1 L0_epef
      INTEGER*4 I0_ipef
      LOGICAL*1 L0_opef,L0_upef
      INTEGER*4 I0_aref
      LOGICAL*1 L0_eref,L0_iref,L0_oref,L0_uref
      INTEGER*4 I0_asef
      LOGICAL*1 L0_esef,L0_isef,L0_osef
      INTEGER*4 I0_usef
      LOGICAL*1 L0_atef,L0_etef,L0_itef
      INTEGER*4 I0_otef
      LOGICAL*1 L0_utef,L0_avef,L0_evef
      INTEGER*4 I0_ivef
      LOGICAL*1 L0_ovef
      INTEGER*4 I0_uvef
      LOGICAL*1 L0_axef
      INTEGER*4 I0_exef
      LOGICAL*1 L0_ixef
      INTEGER*4 I0_oxef
      LOGICAL*1 L0_uxef
      INTEGER*4 I0_abif
      LOGICAL*1 L0_ebif
      INTEGER*4 I0_ibif
      LOGICAL*1 L0_obif
      INTEGER*4 I0_ubif
      LOGICAL*1 L0_adif,L0_edif,L0_idif
      INTEGER*4 I0_odif
      LOGICAL*1 L0_udif,L0_afif
      INTEGER*4 I0_efif
      LOGICAL*1 L0_ifif,L0_ofif
      INTEGER*4 I0_ufif
      LOGICAL*1 L0_akif,L0_ekif
      INTEGER*4 I0_ikif
      LOGICAL*1 L0_okif,L0_ukif
      INTEGER*4 I0_alif
      LOGICAL*1 L0_elif,L0_ilif,L0_olif
      INTEGER*4 I0_ulif
      LOGICAL*1 L0_amif,L0_emif
      INTEGER*4 I0_imif
      LOGICAL*1 L0_omif,L0_umif,L0_apif
      INTEGER*4 I0_epif
      LOGICAL*1 L0_ipif,L0_opif,L0_upif,L0_arif
      INTEGER*4 I0_erif
      LOGICAL*1 L0_irif
      INTEGER*4 I0_orif
      LOGICAL*1 L0_urif
      INTEGER*4 I0_asif
      LOGICAL*1 L0_esif
      INTEGER*4 I0_isif
      LOGICAL*1 L0_osif
      INTEGER*4 I0_usif
      LOGICAL*1 L0_atif
      INTEGER*4 I0_etif
      LOGICAL*1 L0_itif
      INTEGER*4 I0_otif
      LOGICAL*1 L0_utif
      INTEGER*4 I0_avif
      LOGICAL*1 L0_evif
      INTEGER*4 I0_ivif
      LOGICAL*1 L0_ovif
      INTEGER*4 I0_uvif
      LOGICAL*1 L0_axif
      INTEGER*4 I0_exif
      LOGICAL*1 L0_ixif,L0_oxif,L0_uxif
      INTEGER*4 I0_abof
      LOGICAL*1 L0_ebof,L0_ibof
      INTEGER*4 I0_obof
      LOGICAL*1 L0_ubof,L0_adof,L0_edof,L0_idof
      INTEGER*4 I0_odof
      LOGICAL*1 L0_udof,L0_afof,L0_efof
      INTEGER*4 I0_ifof
      LOGICAL*1 L0_ofof,L0_ufof,L0_akof
      INTEGER*4 I0_ekof
      LOGICAL*1 L0_ikof,L0_okof,L0_ukof
      INTEGER*4 I0_alof
      LOGICAL*1 L0_elof,L0_ilof,L0_olof
      INTEGER*4 I0_ulof
      LOGICAL*1 L0_amof,L0_emof
      INTEGER*4 I0_imof
      LOGICAL*1 L0_omof,L0_umof,L0_apof
      INTEGER*4 I0_epof
      LOGICAL*1 L0_ipof,L0_opof,L0_upof,L0_arof
      INTEGER*4 I0_erof
      LOGICAL*1 L0_irof,L0_orof,L0_urof
      INTEGER*4 I0_asof
      LOGICAL*1 L0_esof
      INTEGER*4 I0_isof
      LOGICAL*1 L0_osof
      INTEGER*4 I0_usof
      LOGICAL*1 L0_atof
      INTEGER*4 I0_etof
      LOGICAL*1 L0_itof
      INTEGER*4 I0_otof
      LOGICAL*1 L0_utof
      INTEGER*4 I0_avof
      LOGICAL*1 L0_evof
      INTEGER*4 I0_ivof
      LOGICAL*1 L0_ovof
      INTEGER*4 I0_uvof
      LOGICAL*1 L0_axof
      INTEGER*4 I0_exof
      LOGICAL*1 L0_ixof
      INTEGER*4 I0_oxof
      LOGICAL*1 L0_uxof
      INTEGER*4 I0_abuf
      LOGICAL*1 L0_ebuf
      INTEGER*4 I0_ibuf
      LOGICAL*1 L0_obuf
      INTEGER*4 I0_ubuf
      LOGICAL*1 L0_aduf,L0_eduf
      INTEGER*4 I0_iduf
      LOGICAL*1 L0_oduf,L0_uduf,L0_afuf,L0_efuf
      INTEGER*4 I0_ifuf
      LOGICAL*1 L0_ofuf,L0_ufuf,L0_akuf
      INTEGER*4 I0_ekuf
      LOGICAL*1 L0_ikuf,L0_okuf,L0_ukuf
      INTEGER*4 I0_aluf
      LOGICAL*1 L0_eluf,L0_iluf,L0_oluf
      INTEGER*4 I0_uluf
      LOGICAL*1 L0_amuf,L0_emuf
      INTEGER*4 I0_imuf
      LOGICAL*1 L0_omuf,L0_umuf
      INTEGER*4 I0_apuf
      LOGICAL*1 L0_epuf
      INTEGER*4 I0_ipuf
      LOGICAL*1 L0_opuf,L0_upuf,L0_aruf
      INTEGER*4 I0_eruf
      LOGICAL*1 L0_iruf
      INTEGER*4 I0_oruf
      LOGICAL*1 L0_uruf
      INTEGER*4 I0_asuf
      LOGICAL*1 L0_esuf
      INTEGER*4 I0_isuf
      LOGICAL*1 L0_osuf
      INTEGER*4 I0_usuf
      LOGICAL*1 L0_atuf
      INTEGER*4 I0_etuf
      LOGICAL*1 L0_ituf
      INTEGER*4 I0_otuf
      LOGICAL*1 L0_utuf
      INTEGER*4 I0_avuf
      LOGICAL*1 L0_evuf
      INTEGER*4 I0_ivuf
      LOGICAL*1 L0_ovuf
      INTEGER*4 I0_uvuf
      LOGICAL*1 L0_axuf
      INTEGER*4 I0_exuf
      LOGICAL*1 L0_ixuf,L0_oxuf
      INTEGER*4 I0_uxuf
      LOGICAL*1 L0_abak
      INTEGER*4 I0_ebak
      LOGICAL*1 L0_ibak,L0_obak,L0_ubak
      INTEGER*4 I0_adak
      LOGICAL*1 L0_edak,L0_idak
      INTEGER*4 I0_odak
      LOGICAL*1 L0_udak,L0_afak
      INTEGER*4 I0_efak
      LOGICAL*1 L0_ifak,L0_ofak
      INTEGER*4 I0_ufak
      LOGICAL*1 L0_akak,L0_ekak
      INTEGER*4 I0_ikak
      LOGICAL*1 L0_okak
      INTEGER*4 I0_ukak
      LOGICAL*1 L0_alak,L0_elak
      INTEGER*4 I0_ilak
      LOGICAL*1 L0_olak,L0_ulak,L0_amak
      INTEGER*4 I0_emak
      LOGICAL*1 L0_imak,L0_omak
      INTEGER*4 I0_umak
      LOGICAL*1 L0_apak
      INTEGER*4 I0_epak
      LOGICAL*1 L0_ipak
      INTEGER*4 I0_opak
      LOGICAL*1 L0_upak
      INTEGER*4 I0_arak
      LOGICAL*1 L0_erak
      INTEGER*4 I0_irak
      LOGICAL*1 L0_orak
      INTEGER*4 I0_urak
      LOGICAL*1 L0_asak
      INTEGER*4 I0_esak
      LOGICAL*1 L0_isak
      INTEGER*4 I0_osak
      LOGICAL*1 L0_usak
      INTEGER*4 I0_atak
      LOGICAL*1 L0_etak
      INTEGER*4 I0_itak
      LOGICAL*1 L0_otak
      INTEGER*4 I0_utak
      LOGICAL*1 L0_avak
      INTEGER*4 I0_evak
      LOGICAL*1 L0_ivak
      INTEGER*4 I0_ovak
      LOGICAL*1 L0_uvak,L0_axak
      INTEGER*4 I0_exak
      LOGICAL*1 L0_ixak
      INTEGER*4 I0_oxak
      LOGICAL*1 L0_uxak
      INTEGER*4 I0_abek
      LOGICAL*1 L0_ebek,L0_ibek
      INTEGER*4 I0_obek
      LOGICAL*1 L0_ubek
      INTEGER*4 I0_adek
      LOGICAL*1 L0_edek
      INTEGER*4 I0_idek
      LOGICAL*1 L0_odek
      INTEGER*4 I0_udek
      LOGICAL*1 L0_afek
      INTEGER*4 I0_efek
      LOGICAL*1 L0_ifek
      INTEGER*4 I0_ofek
      LOGICAL*1 L0_ufek,L0_akek
      INTEGER*4 I0_ekek
      LOGICAL*1 L0_ikek
      INTEGER*4 I0_okek
      LOGICAL*1 L0_ukek
      INTEGER*4 I0_alek
      LOGICAL*1 L0_elek
      INTEGER*4 I0_ilek
      LOGICAL*1 L0_olek,L0_ulek,L0_amek,L0_emek,L0_imek,L0_omek
     &,L0_umek,L0_apek,L0_epek,L0_ipek,L0_opek,L0_upek,L0_arek
     &,L0_erek,L0_irek,L0_orek,L0_urek,L0_asek,L0_esek,L0_isek
     &,L0_osek,L0_usek
      LOGICAL*1 L0_atek,L0_etek,L0_itek,L0_otek,L0_utek,L0_avek
     &,L0_evek,L0_ivek,L0_ovek,L0_uvek,L0_axek
      INTEGER*4 I0_exek
      LOGICAL*1 L0_ixek
      INTEGER*4 I0_oxek
      LOGICAL*1 L0_uxek
      INTEGER*4 I0_abik
      LOGICAL*1 L0_ebik
      INTEGER*4 I0_ibik
      LOGICAL*1 L0_obik,L0_ubik,L0_adik,L0_edik,L0_idik,L0_odik
     &,L0_udik,L0_afik,L0_efik,L0_ifik,L0_ofik,L0_ufik,L_akik
     &,L_ekik,L0_ikik
      INTEGER*4 I0_okik
      LOGICAL*1 L0_ukik
      INTEGER*4 I0_alik
      LOGICAL*1 L0_elik
      INTEGER*4 I0_ilik
      LOGICAL*1 L0_olik
      INTEGER*4 I0_ulik
      LOGICAL*1 L0_amik
      INTEGER*4 I0_emik
      LOGICAL*1 L0_imik
      INTEGER*4 I0_omik
      LOGICAL*1 L0_umik
      INTEGER*4 I0_apik
      LOGICAL*1 L0_epik
      INTEGER*4 I0_ipik
      LOGICAL*1 L0_opik
      INTEGER*4 I0_upik
      LOGICAL*1 L0_arik
      INTEGER*4 I0_erik
      LOGICAL*1 L0_irik
      INTEGER*4 I0_orik
      LOGICAL*1 L0_urik
      INTEGER*4 I0_asik
      LOGICAL*1 L0_esik
      INTEGER*4 I0_isik
      LOGICAL*1 L0_osik
      INTEGER*4 I0_usik
      LOGICAL*1 L_atik,L_etik,L_itik,L_otik,L_utik,L_avik
     &,L_evik,L_ivik,L_ovik,L_uvik,L_axik,L_exik,L_ixik,L_oxik
     &,L_uxik,L_abok,L_ebok,L_ibok
      INTEGER*4 I0_obok,I0_ubok
      LOGICAL*1 L0_adok,L0_edok,L0_idok,L_odok,L0_udok
      INTEGER*4 I0_afok
      LOGICAL*1 L0_efok
      INTEGER*4 I0_ifok,I_ofok

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      I0_i = 0
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 882,2390):��������� ������������� IN (�������)
      if(L_e) then
         I_efod=I0_i
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 890,2390):���� � ������������� �������
      I0_u = 2
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 882,2408):��������� ������������� IN (�������)
      if(L_o) then
         I_efod=I0_u
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 890,2407):���� � ������������� �������
      I0_ed = 1
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 882,2426):��������� ������������� IN (�������)
      if(L_ad) then
         I_efod=I0_ed
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 891,2426):���� � ������������� �������
      L0_odod=I_efod.eq.I_idod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 475,2438):���������� �������������
      L0_obod=I_efod.eq.I_ubod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 475,2424):���������� �������������
      L0_adod=I_efod.eq.I_edod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 475,2431):���������� �������������
      !�����. �������� I_ufod = LODOCHKA_HANDLER_FDA50_COORDC??
C  /z'0100000A'/
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 445,2448):��������� �����
      !�����. �������� I_akod = LODOCHKA_HANDLER_FDA50_COORDC??
C  /z'01000007'/
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 445,2451):��������� �����
      !�����. �������� I_ekod = LODOCHKA_HANDLER_FDA50_COORDC??
C  /z'01000010'/
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 445,2454):��������� �����
      L0_adok = L_ibok.OR.L_ikod
C LODOCHKA_HANDLER_FDA50_COORD.fmg(  52,2423):���
      L0_okod = L_utik.AND.L_ekik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 118,1446):�
      L0_ifaf = L0_okod.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 139,1444):�
      L0_ukod = L_utik.AND.L_akik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 118,1458):�
      L0_efaf = L0_ukod.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 139,1456):�
      L0_alod = L_utik.AND.L_etik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 118,1470):�
      L0_afaf = L0_alod.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 139,1468):�
      L0_elod = L_utik.AND.L_itik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 118,1482):�
      L0_udaf = L0_elod.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 139,1480):�
      L0_ilod = L_utik.AND.L_exik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 118,1494):�
      L0_ipaf = L0_ilod.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 139,1492):�
      L0_olod = L_utik.AND.L_ixik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 118,1506):�
      L0_opaf = L0_olod.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 139,1504):�
      L0_ulod = L_utik.AND.L_oxik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 118,1518):�
      L0_iraf = L0_ulod.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 139,1516):�
      L0_amod = L_utik.AND.L_uxik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 118,1530):�
      L0_esaf = L0_amod.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 139,1528):�
      L0_emod = L_utik.AND.L_abok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 118,1542):�
      L0_ataf = L0_emod.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 139,1540):�
      L0_imod = L_utik.AND.L_ebok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 118,1554):�
      L0_utaf = L0_imod.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 139,1552):�
      L0_omod = L_avik.AND.L_ekik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 118,1566):�
      L0_exaf = L0_omod.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 139,1564):�
      L0_umod = L_avik.AND.L_akik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 118,1578):�
      L0_ixaf = L0_umod.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 139,1576):�
      L0_apod = L_avik.AND.L_etik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 118,1590):�
      L0_ebef = L0_apod.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 139,1588):�
      L0_epod = L_avik.AND.L_itik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 118,1602):�
      L0_adef = L0_epod.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 139,1600):�
      L0_ipod = L_avik.AND.L_exik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 118,1614):�
      L0_amaf = L0_ipod.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 139,1612):�
      L0_opod = L_avik.AND.L_ixik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 118,1626):�
      L0_eref = L0_opod.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 139,1624):�
      L0_upod = L_avik.AND.L_oxik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 118,1638):�
      L0_iref = L0_upod.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 139,1636):�
      L0_arod = L_avik.AND.L_uxik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 118,1650):�
      L0_esef = L0_arod.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 139,1648):�
      L0_erod = L_avik.AND.L_abok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 118,1662):�
      L0_atef = L0_erod.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 139,1660):�
      L0_irod = L_avik.AND.L_ebok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 118,1674):�
      L0_utef = L0_irod.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 139,1672):�
      L0_orod = L_evik.AND.L_ekik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 118,1686):�
      L0_abaf = L0_orod.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 139,1684):�
      L0_urod = L_evik.AND.L_akik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 118,1698):�
      L0_ebaf = L0_urod.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 139,1696):�
      L0_asod = L_evik.AND.L_etik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 118,1712):�
      L0_udif = L0_asod.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 139,1710):�
      L0_esod = L_evik.AND.L_itik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 118,1724):�
      L0_edif = L0_esod.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 139,1722):�
      L0_isod = L_evik.AND.L_exik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 118,1736):�
      L0_ifif = L0_isod.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 139,1734):�
      L0_osod = L_evik.AND.L_ixik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 118,1748):�
      L0_akif = L0_osod.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 139,1746):�
      L0_usod = L_evik.AND.L_oxik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 118,1760):�
      L0_okif = L0_usod.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 139,1758):�
      L0_atod = L_evik.AND.L_uxik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 118,1772):�
      L0_elif = L0_atod.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 139,1770):�
      L0_etod = L_evik.AND.L_abok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 118,1784):�
      L0_omif = L0_etod.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 139,1782):�
      L0_itod = L_evik.AND.L_ebok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 118,1796):�
      L0_ipif = L0_itod.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 139,1794):�
      L0_otod = L_ivik.AND.L_ekik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 118,1809):�
      L0_opif = L0_otod.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 139,1807):�
      L0_utod = L_ivik.AND.L_akik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 118,1821):�
      L0_adaf = L0_utod.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 139,1819):�
      L0_avod = L_ivik.AND.L_etik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 118,1833):�
      L0_ubof = L0_avod.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 139,1831):�
      L0_evod = L_ivik.AND.L_itik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 118,1845):�
      L0_adof = L0_evod.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 139,1843):�
      L0_ivod = L_ivik.AND.L_exik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 118,1857):�
      L0_udof = L0_ivod.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 139,1855):�
      L0_ovod = L_ivik.AND.L_ixik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 118,1869):�
      L0_ofof = L0_ovod.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 139,1867):�
      L0_uvod = L_ivik.AND.L_oxik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 117,1881):�
      L0_ikof = L0_uvod.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 138,1879):�
      L0_axod = L_ivik.AND.L_uxik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 117,1893):�
      L0_elof = L0_axod.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 138,1891):�
      L0_exod = L_ivik.AND.L_abok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 117,1906):�
      L0_omof = L0_exod.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 138,1904):�
      L0_ixod = L_ivik.AND.L_ebok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 117,1918):�
      L0_ipof = L0_ixod.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 138,1916):�
      L0_oxod = L_ovik.AND.L_ekik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 117,1930):�
      L0_opof = L0_oxod.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 138,1928):�
      L0_uxod = L_uvik.AND.L_ebok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 118,2161):�
      L0_akak = L0_uxod.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 140,2159):�
      L0_abud = L_ovik.AND.L_ebok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 118,2040):�
      L0_amuf = L0_abud.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 140,2038):�
      L0_ebud = L_uvik.AND.L_ekik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 118,2052):�
      L0_omuf = L0_ebud.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 140,2050):�
      L0_ibud = L_uvik.AND.L_akik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 118,2064):�
      L0_opuf = L0_ibud.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 140,2062):�
      L0_obud = L_uvik.AND.L_etik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 118,2076):�
      L0_upuf = L0_obud.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 140,2074):�
      L0_ubud = L_uvik.AND.L_itik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 118,2088):�
      L0_okaf = L0_ubud.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 140,2086):�
      L0_adud = L_uvik.AND.L_exik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 118,2101):�
      L0_ibak = L0_adud.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 140,2099):�
      L0_edud = L_uvik.AND.L_ixik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 118,2113):�
      L0_obak = L0_edud.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 140,2111):�
      L0_idud = L_uvik.AND.L_oxik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 118,2125):�
      L0_edak = L0_idud.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 140,2123):�
      L0_odud = L_uvik.AND.L_uxik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 118,2137):�
      L0_udak = L0_odud.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 140,2135):�
      L0_udud = L_uvik.AND.L_abok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 118,2149):�
      L0_ifak = L0_udud.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 140,2147):�
      L0_afud = L_axik.AND.L_ekik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 118,2173):�
      L0_alak = L0_afud.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 140,2171):�
      L0_efud = L_axik.AND.L_akik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 118,2185):�
      L0_olak = L0_efud.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 140,2183):�
      L0_ifud = L_axik.AND.L_etik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 118,2198):�
      L0_ulak = L0_ifud.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 140,2196):�
      L0_ofud = L_axik.AND.L_itik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 118,2210):�
      L0_imak = L0_ofud.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 140,2208):�
      L0_ufud = L_axik.AND.L_exik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 118,2222):�
      L0_ixuf = L0_ufud.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 140,2220):�
      L0_akud = L_axik.AND.L_ixik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 118,2234):�
      L0_uvak = L0_akud.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 140,2232):�
      L0_ekud = L_axik.AND.L_oxik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 118,2246):�
      L0_obik = L0_ekud.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 140,2244):�
      L0_ikud = L_axik.AND.L_uxik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 118,2258):�
      L0_ubik = L0_ikud.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 140,2256):�
      L0_okud = L_axik.AND.L_abok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 118,2270):�
      L0_adik = L0_okud.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 140,2268):�
      L0_ukud = L_axik.AND.L_ebok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 118,2282):�
      L0_ebek = L0_ukud.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 140,2280):�
      L0_alud = L_otik.AND.L_ekik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 118,2296):�
      L0_edik = L0_alud.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 140,2294):�
      L0_elud = L_otik.AND.L_akik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 118,2308):�
      L0_idik = L0_elud.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 140,2306):�
      L0_ilud = L_ovik.AND.L_oxik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 118,2004):�
      L0_ofuf = L0_ilud.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 139,2002):�
      L0_olud = L_ovik.AND.L_uxik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 118,2016):�
      L0_ikuf = L0_olud.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 140,2014):�
      L0_ulud = L_ovik.AND.L_abok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 118,2028):�
      L0_eluf = L0_ulud.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 140,2026):�
      L0_amud = L_ovik.AND.L_akik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 117,1942):�
      L0_irof = L0_amud.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 138,1940):�
      L0_emud = L_ovik.AND.L_etik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 117,1954):�
      L0_ixif = L0_emud.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 138,1952):�
      L0_imud = L_ovik.AND.L_itik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 117,1966):�
      L0_ufaf = L0_imud.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 138,1964):�
      L0_omud = L_ovik.AND.L_exik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 118,1978):�
      L0_oduf = L0_omud.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 139,1976):�
      L0_umud = L_ovik.AND.L_ixik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 118,1990):�
      L0_uduf = L0_umud.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 139,1988):�
      L0_apud = L_otik.AND.L_etik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 118,2320):�
      L0_odik = L0_apud.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 140,2318):�
      L0_epud = L_otik.AND.L_itik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 118,2332):�
      L0_udik = L0_epud.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 140,2330):�
      L0_ipud = L_otik.AND.L_exik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 118,2344):�
      L0_afik = L0_ipud.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 140,2342):�
      L0_opud = L_otik.AND.L_ixik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 118,2356):�
      L0_ufek = L0_opud.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 139,2354):�
      L0_upud = L_otik.AND.L_oxik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 118,2368):�
      L0_efik = L0_upud.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 139,2366):�
      L0_arud = L_otik.AND.L_uxik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 118,2380):�
      L0_ifik = L0_arud.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 139,2378):�
      L0_erud = L_otik.AND.L_abok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 118,2392):�
      L0_ofik = L0_erud.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 140,2390):�
      L0_irud = L_otik.AND.L_ebok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 118,2404):�
      L0_ufik = L0_irud.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 140,2402):�
      I0_isud = 5080
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 309,1455):��������� ������������� IN (�������)
      I0_atud = 5079
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 309,1467):��������� ������������� IN (�������)
      I0_otud = 5078
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 309,1479):��������� ������������� IN (�������)
      I0_evud = 5077
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 309,1490):��������� ������������� IN (�������)
      I0_uvud = 5060
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 309,1696):��������� ������������� IN (�������)
      I0_ubaf = 5059
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 308,1708):��������� ������������� IN (�������)
      I0_odaf = 5049
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 308,1830):��������� ������������� IN (�������)
      I0_ofaf = 5060
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 202,1696):��������� ������������� IN (�������)
      I0_ikaf = 5037
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 306,1975):��������� ������������� IN (�������)
      I0_alaf = 5027
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 305,2096):��������� ������������� IN (�������)
      I0_ilaf = 5080
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 202,1454):��������� ������������� IN (�������)
      I0_ulaf = 5079
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 202,1467):��������� ������������� IN (�������)
      I0_omaf = 5066
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 308,1624):��������� ������������� IN (�������)
      I0_epaf = 5076
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 309,1503):��������� ������������� IN (�������)
      I0_eraf = 5075
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 309,1514):��������� ������������� IN (�������)
      I0_asaf = 5074
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 309,1526):��������� ������������� IN (�������)
      I0_usaf = 5073
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 309,1538):��������� ������������� IN (�������)
      I0_otaf = 5072
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 309,1550):��������� ������������� IN (�������)
      I0_ivaf = 5071
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 308,1562):��������� ������������� IN (�������)
      I0_axaf = 5070
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 308,1574):��������� ������������� IN (�������)
      I0_abef = 5069
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 308,1586):��������� ������������� IN (�������)
      I0_ubef = 5068
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 308,1598):��������� ������������� IN (�������)
      I0_odef = 5067
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 308,1612):��������� ������������� IN (�������)
      I0_afef = 5078
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 202,1480):��������� ������������� IN (�������)
      I0_ifef = 5077
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 202,1492):��������� ������������� IN (�������)
      I0_ufef = 5076
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 202,1504):��������� ������������� IN (�������)
      I0_ekef = 5075
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 202,1516):��������� ������������� IN (�������)
      I0_okef = 5074
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 202,1528):��������� ������������� IN (�������)
      I0_alef = 5073
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 202,1540):��������� ������������� IN (�������)
      I0_ilef = 5072
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 202,1552):��������� ������������� IN (�������)
      I0_ulef = 5071
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 202,1564):��������� ������������� IN (�������)
      I0_emef = 5070
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 202,1576):��������� ������������� IN (�������)
      I0_omef = 5069
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 202,1588):��������� ������������� IN (�������)
      I0_apef = 5068
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 202,1600):��������� ������������� IN (�������)
      I0_ipef = 5067
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 202,1612):��������� ������������� IN (�������)
      I0_aref = 5065
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 308,1636):��������� ������������� IN (�������)
      I0_asef = 5064
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 308,1648):��������� ������������� IN (�������)
      I0_usef = 5063
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 308,1660):��������� ������������� IN (�������)
      I0_otef = 5062
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 308,1672):��������� ������������� IN (�������)
      I0_ivef = 5061
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 308,1684):��������� ������������� IN (�������)
      I0_uvef = 5066
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 202,1624):��������� ������������� IN (�������)
      I0_exef = 5065
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 202,1636):��������� ������������� IN (�������)
      I0_oxef = 5064
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 202,1648):��������� ������������� IN (�������)
      I0_abif = 5063
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 202,1660):��������� ������������� IN (�������)
      I0_ibif = 5062
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 202,1672):��������� ������������� IN (�������)
      I0_ubif = 5061
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 202,1684):��������� ������������� IN (�������)
      I0_odif = 5058
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 308,1722):��������� ������������� IN (�������)
      I0_efif = 5057
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 308,1733):��������� ������������� IN (�������)
      I0_ufif = 5056
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 308,1745):��������� ������������� IN (�������)
      I0_ikif = 5055
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 308,1757):��������� ������������� IN (�������)
      I0_alif = 5054
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 308,1769):��������� ������������� IN (�������)
      I0_ulif = 5053
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 307,1781):��������� ������������� IN (�������)
      I0_imif = 5052
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 307,1793):��������� ������������� IN (�������)
      I0_epif = 5051
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 307,1805):��������� ������������� IN (�������)
      I0_erif = 5050
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 307,1818):��������� ������������� IN (�������)
      I0_orif = 5059
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 201,1708):��������� ������������� IN (�������)
      I0_asif = 5058
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 201,1722):��������� ������������� IN (�������)
      I0_isif = 5057
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 201,1734):��������� ������������� IN (�������)
      I0_usif = 5056
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 201,1746):��������� ������������� IN (�������)
      I0_etif = 5055
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 201,1758):��������� ������������� IN (�������)
      I0_otif = 5054
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 201,1770):��������� ������������� IN (�������)
      I0_avif = 5053
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 201,1782):��������� ������������� IN (�������)
      I0_ivif = 5052
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 201,1794):��������� ������������� IN (�������)
      I0_uvif = 5051
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 201,1806):��������� ������������� IN (�������)
      I0_exif = 5050
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 201,1818):��������� ������������� IN (�������)
      I0_abof = 5038
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 306,1962):��������� ������������� IN (�������)
      I0_obof = 5048
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 308,1842):��������� ������������� IN (�������)
      I0_odof = 5047
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 308,1854):��������� ������������� IN (�������)
      I0_ifof = 5046
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 308,1866):��������� ������������� IN (�������)
      I0_ekof = 5045
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 308,1878):��������� ������������� IN (�������)
      I0_alof = 5044
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 308,1890):��������� ������������� IN (�������)
      I0_ulof = 5043
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 306,1902):��������� ������������� IN (�������)
      I0_imof = 5042
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 306,1916):��������� ������������� IN (�������)
      I0_epof = 5041
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 306,1928):��������� ������������� IN (�������)
      I0_erof = 5040
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 307,1940):��������� ������������� IN (�������)
      I0_asof = 5039
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 307,1950):��������� ������������� IN (�������)
      I0_isof = 5049
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 201,1831):��������� ������������� IN (�������)
      I0_usof = 5048
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 201,1843):��������� ������������� IN (�������)
      I0_etof = 5047
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 201,1855):��������� ������������� IN (�������)
      I0_otof = 5046
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 201,1867):��������� ������������� IN (�������)
      I0_avof = 5045
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 201,1879):��������� ������������� IN (�������)
      I0_ivof = 5044
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 201,1891):��������� ������������� IN (�������)
      I0_uvof = 5043
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 201,1903):��������� ������������� IN (�������)
      I0_exof = 5042
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 201,1916):��������� ������������� IN (�������)
      I0_oxof = 5041
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 201,1928):��������� ������������� IN (�������)
      I0_abuf = 5040
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 201,1940):��������� ������������� IN (�������)
      I0_ibuf = 5039
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 201,1952):��������� ������������� IN (�������)
      I0_ubuf = 5038
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 201,1964):��������� ������������� IN (�������)
      I0_iduf = 5036
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 306,1988):��������� ������������� IN (�������)
      I0_ifuf = 5035
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 306,2000):��������� ������������� IN (�������)
      I0_ekuf = 5034
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 306,2012):��������� ������������� IN (�������)
      I0_aluf = 5033
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 306,2024):��������� ������������� IN (�������)
      I0_uluf = 5032
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 306,2036):��������� ������������� IN (�������)
      I0_imuf = 5031
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 304,2048):��������� ������������� IN (�������)
      I0_apuf = 5030
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 304,2060):��������� ������������� IN (�������)
      I0_ipuf = 5029
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 304,2072):��������� ������������� IN (�������)
      I0_eruf = 5028
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 304,2084):��������� ������������� IN (�������)
      I0_oruf = 5037
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 200,1976):��������� ������������� IN (�������)
      I0_asuf = 5036
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 200,1988):��������� ������������� IN (�������)
      I0_isuf = 5035
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 200,2000):��������� ������������� IN (�������)
      I0_usuf = 5034
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 200,2014):��������� ������������� IN (�������)
      I0_etuf = 5033
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 200,2026):��������� ������������� IN (�������)
      I0_otuf = 5032
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 200,2038):��������� ������������� IN (�������)
      I0_avuf = 5031
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 200,2050):��������� ������������� IN (�������)
      I0_ivuf = 5030
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 200,2062):��������� ������������� IN (�������)
      I0_uvuf = 5029
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 200,2074):��������� ������������� IN (�������)
      I0_exuf = 5028
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 200,2085):��������� ������������� IN (�������)
      I0_uxuf = 5016
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 304,2231):��������� ������������� IN (�������)
      I0_ebak = 5026
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 305,2110):��������� ������������� IN (�������)
      I0_adak = 5025
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 305,2122):��������� ������������� IN (�������)
      I0_odak = 5024
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 305,2134):��������� ������������� IN (�������)
      I0_efak = 5023
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 305,2146):��������� ������������� IN (�������)
      I0_ufak = 5022
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 305,2158):��������� ������������� IN (�������)
      I0_ikak = 5021
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 304,2170):��������� ������������� IN (�������)
      I0_ukak = 5020
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 304,2182):��������� ������������� IN (�������)
      I0_ilak = 5019
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 304,2194):��������� ������������� IN (�������)
      I0_emak = 5018
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 304,2207):��������� ������������� IN (�������)
      I0_umak = 5017
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 304,2219):��������� ������������� IN (�������)
      I0_epak = 5027
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 200,2098):��������� ������������� IN (�������)
      I0_opak = 5026
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 200,2111):��������� ������������� IN (�������)
      I0_arak = 5025
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 200,2123):��������� ������������� IN (�������)
      I0_irak = 5024
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 200,2135):��������� ������������� IN (�������)
      I0_urak = 5023
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 200,2147):��������� ������������� IN (�������)
      I0_esak = 5022
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 200,2159):��������� ������������� IN (�������)
      I0_osak = 5021
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 198,2171):��������� ������������� IN (�������)
      I0_atak = 5020
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 198,2183):��������� ������������� IN (�������)
      I0_itak = 5019
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 198,2195):��������� ������������� IN (�������)
      I0_utak = 5018
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 198,2208):��������� ������������� IN (�������)
      I0_evak = 5017
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 198,2219):��������� ������������� IN (�������)
      I0_ovak = 5015
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 304,2244):��������� ������������� IN (�������)
      I0_exak = 5014
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 304,2255):��������� ������������� IN (�������)
      I0_oxak = 5013
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 304,2267):��������� ������������� IN (�������)
      I0_abek = 5012
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 304,2279):��������� ������������� IN (�������)
      I0_obek = 5011
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 304,2291):��������� ������������� IN (�������)
      I0_adek = 5010
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 304,2304):��������� ������������� IN (�������)
      I0_idek = 5009
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 304,2316):��������� ������������� IN (�������)
      I0_udek = 5008
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 304,2328):��������� ������������� IN (�������)
      I0_efek = 5007
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 304,2340):��������� ������������� IN (�������)
      I0_ofek = 5006
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 304,2352):��������� ������������� IN (�������)
      I0_ekek = 5005
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 304,2364):��������� ������������� IN (�������)
      I0_okek = 5004
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 304,2376):��������� ������������� IN (�������)
      I0_alek = 5003
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 304,2388):��������� ������������� IN (�������)
      I0_ilek = 5002
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 304,2400):��������� ������������� IN (�������)
      I0_exek = 5081
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 372,2414):��������� ������������� IN (�������)
      I0_oxek = 5001
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 304,2417):��������� ������������� IN (�������)
      I0_abik = 5016
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 198,2232):��������� ������������� IN (�������)
      I0_ibik = 5015
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 198,2244):��������� ������������� IN (�������)
      I0_okik = 5014
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 198,2256):��������� ������������� IN (�������)
      I0_alik = 5013
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 198,2268):��������� ������������� IN (�������)
      I0_ilik = 5012
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 198,2280):��������� ������������� IN (�������)
      I0_ulik = 5011
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 198,2292):��������� ������������� IN (�������)
      I0_emik = 5010
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 199,2306):��������� ������������� IN (�������)
      I0_omik = 5009
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 199,2318):��������� ������������� IN (�������)
      I0_apik = 5008
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 199,2330):��������� ������������� IN (�������)
      I0_ipik = 5007
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 199,2342):��������� ������������� IN (�������)
      I0_upik = 5006
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 199,2354):��������� ������������� IN (�������)
      I0_erik = 5005
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 199,2366):��������� ������������� IN (�������)
      I0_orik = 5004
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 199,2378):��������� ������������� IN (�������)
      I0_asik = 5003
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 199,2390):��������� ������������� IN (�������)
      I0_isik = 5002
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 198,2402):��������� ������������� IN (�������)
      I0_usik = 5001
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 198,2414):��������� ������������� IN (�������)
      I0_obok = 5081
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 198,2433):��������� ������������� IN (�������)
      I0_ubok = 5082
C LODOCHKA_HANDLER_FDA50_COORD.fmg(  63,2430):��������� ������������� IN (�������)
      I0_afok = 5081
C LODOCHKA_HANDLER_FDA50_COORD.fmg(  63,2447):��������� ������������� IN (�������)
      I0_ifok = 5082
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 198,2449):��������� ������������� IN (�������)
      L0_udok=I_ofok.eq.I0_afok
C LODOCHKA_HANDLER_FDA50_COORD.fmg(  70,2448):���������� �������������
C label 1673  try1673=try1673-1
      L0_efok = L0_udok.AND.L_odok
C LODOCHKA_HANDLER_FDA50_COORD.fmg(  96,2440):�
      if(L0_efok) then
         I_ofok=I0_ifok
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 204,2448):���� � ������������� �������
      L0_ixek=I_ofok.eq.I0_oxek
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 312,2418):���������� �������������
      L0_uvek = L0_ixek.AND.L0_ufik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 320,2406):�
      L0_elek=I_ofok.eq.I0_ilek
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 312,2402):���������� �������������
      L0_ivek = L0_elek.AND.L0_ofik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 320,2394):�
      L0_ukek=I_ofok.eq.I0_alek
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 312,2390):���������� �������������
      L0_evek = L0_ukek.AND.L0_ifik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 320,2382):�
      L0_ikek=I_ofok.eq.I0_okek
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 312,2378):���������� �������������
      L0_avek = L0_ikek.AND.L0_efik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 320,2370):�
      L0_akek=I_ofok.eq.I0_ekek
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 312,2366):���������� �������������
      L0_utek = L0_akek.AND.L0_ufek
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 320,2358):�
      L0_ifek=I_ofok.eq.I0_ofek
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 312,2354):���������� �������������
      L0_otek = L0_ifek.AND.L0_afik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 320,2346):�
      L0_afek=I_ofok.eq.I0_efek
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 312,2342):���������� �������������
      L0_itek = L0_afek.AND.L0_udik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 320,2334):�
      L0_odek=I_ofok.eq.I0_udek
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 312,2330):���������� �������������
      L0_etek = L0_odek.AND.L0_odik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 320,2322):�
      L0_edek=I_ofok.eq.I0_idek
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 312,2318):���������� �������������
      L0_atek = L0_edek.AND.L0_idik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 320,2310):�
      L0_ubek=I_ofok.eq.I0_adek
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 312,2306):���������� �������������
      L0_usek = L0_ubek.AND.L0_edik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 320,2298):�
      L0_ibek=I_ofok.eq.I0_obek
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 312,2292):���������� �������������
      L0_osek = L0_ibek.AND.L0_ebek
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 320,2284):�
      L0_uxak=I_ofok.eq.I0_abek
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 312,2280):���������� �������������
      L0_isek = L0_uxak.AND.L0_adik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 320,2272):�
      L0_ixak=I_ofok.eq.I0_oxak
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 312,2268):���������� �������������
      L0_esek = L0_ixak.AND.L0_ubik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 320,2260):�
      L0_axak=I_ofok.eq.I0_exak
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 312,2256):���������� �������������
      L0_asek = L0_axak.AND.L0_obik
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 320,2248):�
      L0_ivak=I_ofok.eq.I0_ovak
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 312,2244):���������� �������������
      L0_urek = L0_ivak.AND.L0_uvak
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 320,2236):�
      L0_oxuf=I_ofok.eq.I0_uxuf
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 312,2232):���������� �������������
      L0_orek = L0_oxuf.AND.L0_ixuf
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 320,2224):�
      L0_omak=I_ofok.eq.I0_umak
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 312,2220):���������� �������������
      L0_irek = L0_omak.AND.L0_imak
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 320,2212):�
      L0_amak=I_ofok.eq.I0_emak
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 311,2208):���������� �������������
      L0_erek = L0_amak.AND.L0_ulak
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 320,2200):�
      L0_elak=I_ofok.eq.I0_ilak
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 311,2195):���������� �������������
      L0_arek = L0_elak.AND.L0_olak
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 320,2187):�
      L0_okak=I_ofok.eq.I0_ukak
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 311,2183):���������� �������������
      L0_upek = L0_okak.AND.L0_alak
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 320,2175):�
      L0_ekak=I_ofok.eq.I0_ikak
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 311,2171):���������� �������������
      L0_opek = L0_ekak.AND.L0_akak
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 320,2163):�
      L0_ofak=I_ofok.eq.I0_ufak
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 312,2159):���������� �������������
      L0_ipek = L0_ofak.AND.L0_ifak
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 321,2151):�
      L0_afak=I_ofok.eq.I0_efak
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 312,2147):���������� �������������
      L0_epek = L0_afak.AND.L0_udak
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 321,2139):�
      L0_idak=I_ofok.eq.I0_odak
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 312,2135):���������� �������������
      L0_apek = L0_idak.AND.L0_edak
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 321,2127):�
      L0_ubak=I_ofok.eq.I0_adak
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 312,2123):���������� �������������
      L0_umek = L0_ubak.AND.L0_obak
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 321,2115):�
      L0_abak=I_ofok.eq.I0_ebak
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 312,2112):���������� �������������
      L0_omek = L0_abak.AND.L0_ibak
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 321,2103):�
      L0_ukaf=I_ofok.eq.I0_alaf
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 312,2098):���������� �������������
      L0_imek = L0_ukaf.AND.L0_okaf
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 321,2090):�
      L0_aruf=I_ofok.eq.I0_eruf
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 312,2086):���������� �������������
      L0_emek = L0_aruf.AND.L0_upuf
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 320,2078):�
      L0_epuf=I_ofok.eq.I0_ipuf
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 312,2074):���������� �������������
      L0_amek = L0_epuf.AND.L0_opuf
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 320,2066):�
      L0_umuf=I_ofok.eq.I0_apuf
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 312,2060):���������� �������������
      L0_ulek = L0_umuf.AND.L0_omuf
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 320,2054):�
      L0_emuf=I_ofok.eq.I0_imuf
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 312,2048):���������� �������������
      L0_olek = L0_emuf.AND.L0_amuf
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 320,2042):�
      L0_ovek = L0_uvek.OR.L0_ivek.OR.L0_evek.OR.L0_avek.OR.L0_utek.OR.L
     &0_otek.OR.L0_itek.OR.L0_etek.OR.L0_atek.OR.L0_usek.OR.L0_osek.OR.L
     &0_isek.OR.L0_esek.OR.L0_asek.OR.L0_urek.OR.L0_orek.OR.L0_irek.OR.L
     &0_erek.OR.L0_arek.OR.L0_upek.OR.L0_opek.OR.L0_ipek.OR.L0_epek.OR.L
     &0_apek.OR.L0_umek.OR.L0_omek.OR.L0_imek.OR.L0_emek.OR.L0_amek.OR.L
     &0_ulek.OR.L0_olek
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 358,2376):���
      L0_oluf=I_ofok.eq.I0_uluf
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 314,2036):���������� �������������
      L0_iluf = L0_oluf.AND.L0_eluf
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 322,2030):�
      L0_ukuf=I_ofok.eq.I0_aluf
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 314,2024):���������� �������������
      L0_okuf = L0_ukuf.AND.L0_ikuf
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 322,2018):�
      L0_akuf=I_ofok.eq.I0_ekuf
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 314,2012):���������� �������������
      L0_ufuf = L0_akuf.AND.L0_ofuf
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 322,2006):�
      L0_efuf=I_ofok.eq.I0_ifuf
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 314,2000):���������� �������������
      L0_afuf = L0_efuf.AND.L0_uduf
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 322,1992):�
      L0_eduf=I_ofok.eq.I0_iduf
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 314,1989):���������� �������������
      L0_aduf = L0_eduf.AND.L0_oduf
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 322,1980):�
      L0_ekaf=I_ofok.eq.I0_ikaf
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 314,1976):���������� �������������
      L0_akaf = L0_ekaf.AND.L0_ufaf
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 322,1968):�
      L0_uxif=I_ofok.eq.I0_abof
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 314,1964):���������� �������������
      L0_oxif = L0_uxif.AND.L0_ixif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 322,1956):�
      L0_urof=I_ofok.eq.I0_asof
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 314,1952):���������� �������������
      L0_orof = L0_urof.AND.L0_irof
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 322,1944):�
      L0_arof=I_ofok.eq.I0_erof
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 314,1940):���������� �������������
      L0_upof = L0_arof.AND.L0_opof
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 322,1932):�
      L0_apof=I_ofok.eq.I0_epof
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 314,1928):���������� �������������
      L0_umof = L0_apof.AND.L0_ipof
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 322,1920):�
      L0_emof=I_ofok.eq.I0_imof
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 314,1916):���������� �������������
      L0_amof = L0_emof.AND.L0_omof
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 322,1908):�
      L0_olof=I_ofok.eq.I0_ulof
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 314,1903):���������� �������������
      L0_ilof = L0_olof.AND.L0_elof
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 322,1895):�
      L0_ukof=I_ofok.eq.I0_alof
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 315,1891):���������� �������������
      L0_okof = L0_ukof.AND.L0_ikof
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 324,1883):�
      L0_akof=I_ofok.eq.I0_ekof
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 315,1879):���������� �������������
      L0_ufof = L0_akof.AND.L0_ofof
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 324,1871):�
      L0_efof=I_ofok.eq.I0_ifof
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 315,1867):���������� �������������
      L0_afof = L0_efof.AND.L0_udof
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 324,1859):�
      L0_idof=I_ofok.eq.I0_odof
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 315,1855):���������� �������������
      L0_edof = L0_idof.AND.L0_adof
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 324,1847):�
      L0_ibof=I_ofok.eq.I0_obof
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 315,1844):���������� �������������
      L0_ebof = L0_ibof.AND.L0_ubof
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 324,1835):�
      L0_idaf=I_ofok.eq.I0_odaf
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 315,1831):���������� �������������
      L0_edaf = L0_idaf.AND.L0_adaf
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 324,1823):�
      L0_arif=I_ofok.eq.I0_erif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 314,1819):���������� �������������
      L0_upif = L0_arif.AND.L0_opif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 323,1811):�
      L0_apif=I_ofok.eq.I0_epif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 314,1806):���������� �������������
      L0_umif = L0_apif.AND.L0_ipif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 323,1798):�
      L0_emif=I_ofok.eq.I0_imif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 314,1794):���������� �������������
      L0_amif = L0_emif.AND.L0_omif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 323,1786):�
      L0_olif=I_ofok.eq.I0_ulif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 314,1782):���������� �������������
      L0_ilif = L0_olif.AND.L0_elif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 323,1774):�
      L0_ukif=I_ofok.eq.I0_alif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 316,1770):���������� �������������
      L0_uxud = L0_ukif.AND.L0_okif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 324,1762):�
      L0_ekif=I_ofok.eq.I0_ikif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 316,1758):���������� �������������
      L0_oxud = L0_ekif.AND.L0_akif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 324,1750):�
      L0_ofif=I_ofok.eq.I0_ufif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 316,1746):���������� �������������
      L0_ixud = L0_ofif.AND.L0_ifif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 324,1738):�
      L0_afif=I_ofok.eq.I0_efif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 316,1734):���������� �������������
      L0_exud = L0_afif.AND.L0_edif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 324,1726):�
      L0_idif=I_ofok.eq.I0_odif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 316,1722):���������� �������������
      L0_axud = L0_idif.AND.L0_udif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 324,1714):�
      L0_obaf=I_ofok.eq.I0_ubaf
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 316,1708):���������� �������������
      L0_ibaf = L0_obaf.AND.L0_ebaf
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 324,1700):�
      L0_ovud=I_ofok.eq.I0_uvud
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 316,1696):���������� �������������
      L0_ivud = L0_ovud.AND.L0_abaf
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 324,1688):�
      L0_evef=I_ofok.eq.I0_ivef
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 316,1684):���������� �������������
      L0_avef = L0_evef.AND.L0_utef
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 324,1676):�
      L0_itef=I_ofok.eq.I0_otef
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 316,1672):���������� �������������
      L0_etef = L0_itef.AND.L0_atef
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 324,1664):�
      L0_urud = L0_iluf.OR.L0_okuf.OR.L0_ufuf.OR.L0_afuf.OR.L0_aduf.OR.L
     &0_akaf.OR.L0_oxif.OR.L0_orof.OR.L0_upof.OR.L0_umof.OR.L0_amof.OR.L
     &0_ilof.OR.L0_okof.OR.L0_ufof.OR.L0_afof.OR.L0_edof.OR.L0_ebof.OR.L
     &0_edaf.OR.L0_upif.OR.L0_umif.OR.L0_amif.OR.L0_ilif.OR.L0_uxud.OR.L
     &0_oxud.OR.L0_ixud.OR.L0_exud.OR.L0_axud.OR.L0_ibaf.OR.L0_ivud.OR.L
     &0_avef.OR.L0_etef
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 360,2000):���
      L0_osef=I_ofok.eq.I0_usef
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 316,1660):���������� �������������
      L0_isef = L0_osef.AND.L0_esef
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 324,1652):�
      L0_uref=I_ofok.eq.I0_asef
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 316,1648):���������� �������������
      L0_oref = L0_uref.AND.L0_iref
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 324,1640):�
      L0_upef=I_ofok.eq.I0_aref
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 316,1637):���������� �������������
      L0_opef = L0_upef.AND.L0_eref
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 324,1628):�
      L0_imaf=I_ofok.eq.I0_omaf
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 316,1624):���������� �������������
      L0_emaf = L0_imaf.AND.L0_amaf
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 324,1616):�
      L0_idef=I_ofok.eq.I0_odef
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 316,1612):���������� �������������
      L0_edef = L0_idef.AND.L0_adef
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 324,1604):�
      L0_obef=I_ofok.eq.I0_ubef
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 315,1600):���������� �������������
      L0_ibef = L0_obef.AND.L0_ebef
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 324,1592):�
      L0_uxaf=I_ofok.eq.I0_abef
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 315,1588):���������� �������������
      L0_oxaf = L0_uxaf.AND.L0_ixaf
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 324,1580):�
      L0_uvaf=I_ofok.eq.I0_axaf
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 315,1576):���������� �������������
      L0_ovaf = L0_uvaf.AND.L0_exaf
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 324,1568):�
      L0_evaf=I_ofok.eq.I0_ivaf
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 315,1564):���������� �������������
      L0_avaf = L0_evaf.AND.L0_utaf
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 324,1556):�
      L0_itaf=I_ofok.eq.I0_otaf
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 316,1552):���������� �������������
      L0_etaf = L0_itaf.AND.L0_ataf
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 324,1544):�
      L0_osaf=I_ofok.eq.I0_usaf
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 316,1540):���������� �������������
      L0_isaf = L0_osaf.AND.L0_esaf
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 324,1532):�
      L0_uraf=I_ofok.eq.I0_asaf
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 316,1528):���������� �������������
      L0_oraf = L0_uraf.AND.L0_iraf
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 324,1520):�
      L0_araf=I_ofok.eq.I0_eraf
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 316,1516):���������� �������������
      L0_upaf = L0_araf.AND.L0_opaf
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 325,1508):�
      L0_apaf=I_ofok.eq.I0_epaf
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 316,1504):���������� �������������
      L0_umaf = L0_apaf.AND.L0_ipaf
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 325,1496):�
      L0_avud=I_ofok.eq.I0_evud
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 316,1492):���������� �������������
      L0_utud = L0_avud.AND.L0_udaf
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 325,1484):�
      L0_itud=I_ofok.eq.I0_otud
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 316,1480):���������� �������������
      L0_etud = L0_itud.AND.L0_afaf
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 325,1472):�
      L0_usud=I_ofok.eq.I0_atud
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 316,1468):���������� �������������
      L0_osud = L0_usud.AND.L0_efaf
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 325,1460):�
      L0_esud=I_ofok.eq.I0_isud
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 316,1456):���������� �������������
      L0_asud = L0_esud.AND.L0_ifaf
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 325,1448):�
      L0_orud = L0_isef.OR.L0_oref.OR.L0_opef.OR.L0_emaf.OR.L0_edef.OR.L
     &0_ibef.OR.L0_oxaf.OR.L0_ovaf.OR.L0_avaf.OR.L0_etaf.OR.L0_isaf.OR.L
     &0_oraf.OR.L0_upaf.OR.L0_umaf.OR.L0_utud.OR.L0_etud.OR.L0_osud.OR.L
     &0_asud
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 361,1636):���
      L0_axek = L0_ovek.OR.L0_urud.OR.L0_orud
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 373,2404):���
      if(L0_axek) then
         I_ofok=I0_exek
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 378,2413):���� � ������������� �������
      L0_edok=I_ofok.eq.I0_ubok
C LODOCHKA_HANDLER_FDA50_COORD.fmg(  70,2431):���������� �������������
      L0_adif = L0_edok.AND.L_atik.AND.L0_orod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,1690):�
      if(L0_adif) then
         I_ofok=I0_ofaf
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 208,1696):���� � ������������� �������
      L0_elaf = L0_edok.AND.L_atik.AND.L0_okod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,1448):�
      if(L0_elaf) then
         I_ofok=I0_ilaf
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 209,1454):���� � ������������� �������
      L0_olaf = L0_edok.AND.L_atik.AND.L0_ukod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,1460):�
      if(L0_olaf) then
         I_ofok=I0_ulaf
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 209,1466):���� � ������������� �������
      L0_udef = L0_edok.AND.L_atik.AND.L0_alod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,1472):�
      if(L0_udef) then
         I_ofok=I0_afef
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 209,1479):���� � ������������� �������
      L0_efef = L0_edok.AND.L_atik.AND.L0_elod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,1484):�
      if(L0_efef) then
         I_ofok=I0_ifef
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 209,1491):���� � ������������� �������
      L0_ofef = L0_edok.AND.L_atik.AND.L0_ilod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,1496):�
      if(L0_ofef) then
         I_ofok=I0_ufef
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 209,1503):���� � ������������� �������
      L0_akef = L0_edok.AND.L_atik.AND.L0_olod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,1508):�
      if(L0_akef) then
         I_ofok=I0_ekef
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 209,1515):���� � ������������� �������
      L0_ikef = L0_edok.AND.L_atik.AND.L0_ulod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,1522):�
      if(L0_ikef) then
         I_ofok=I0_okef
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 209,1528):���� � ������������� �������
      L0_ukef = L0_edok.AND.L_atik.AND.L0_amod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,1534):�
      if(L0_ukef) then
         I_ofok=I0_alef
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 209,1540):���� � ������������� �������
      L0_elef = L0_edok.AND.L_atik.AND.L0_emod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,1546):�
      if(L0_elef) then
         I_ofok=I0_ilef
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 209,1552):���� � ������������� �������
      L0_olef = L0_edok.AND.L_atik.AND.L0_imod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,1558):�
      if(L0_olef) then
         I_ofok=I0_ulef
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 209,1564):���� � ������������� �������
      L0_amef = L0_edok.AND.L_atik.AND.L0_omod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,1570):�
      if(L0_amef) then
         I_ofok=I0_emef
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 209,1576):���� � ������������� �������
      L0_imef = L0_edok.AND.L_atik.AND.L0_umod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,1582):�
      if(L0_imef) then
         I_ofok=I0_omef
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 209,1588):���� � ������������� �������
      L0_umef = L0_edok.AND.L_atik.AND.L0_apod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,1594):�
      if(L0_umef) then
         I_ofok=I0_apef
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 209,1599):���� � ������������� �������
      L0_epef = L0_edok.AND.L_atik.AND.L0_epod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,1606):�
      if(L0_epef) then
         I_ofok=I0_ipef
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 209,1612):���� � ������������� �������
      L0_ovef = L0_edok.AND.L_atik.AND.L0_ipod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,1618):�
      if(L0_ovef) then
         I_ofok=I0_uvef
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 209,1624):���� � ������������� �������
      L0_axef = L0_edok.AND.L_atik.AND.L0_opod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,1630):�
      if(L0_axef) then
         I_ofok=I0_exef
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 209,1636):���� � ������������� �������
      L0_ixef = L0_edok.AND.L_atik.AND.L0_upod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,1642):�
      if(L0_ixef) then
         I_ofok=I0_oxef
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 209,1648):���� � ������������� �������
      L0_uxef = L0_edok.AND.L_atik.AND.L0_arod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,1654):�
      if(L0_uxef) then
         I_ofok=I0_abif
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 209,1660):���� � ������������� �������
      L0_ebif = L0_edok.AND.L_atik.AND.L0_erod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,1666):�
      if(L0_ebif) then
         I_ofok=I0_ibif
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 209,1672):���� � ������������� �������
      L0_obif = L0_edok.AND.L_atik.AND.L0_irod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,1678):�
      if(L0_obif) then
         I_ofok=I0_ubif
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 209,1684):���� � ������������� �������
      L0_irif = L0_edok.AND.L_atik.AND.L0_urod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,1702):�
      if(L0_irif) then
         I_ofok=I0_orif
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 208,1708):���� � ������������� �������
      L0_urif = L0_edok.AND.L_atik.AND.L0_asod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,1715):�
      if(L0_urif) then
         I_ofok=I0_asif
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 208,1722):���� � ������������� �������
      L0_esif = L0_edok.AND.L_atik.AND.L0_esod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,1727):�
      if(L0_esif) then
         I_ofok=I0_isif
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 208,1734):���� � ������������� �������
      L0_osif = L0_edok.AND.L_atik.AND.L0_isod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,1739):�
      if(L0_osif) then
         I_ofok=I0_usif
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 208,1746):���� � ������������� �������
      L0_atif = L0_edok.AND.L_atik.AND.L0_osod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,1751):�
      if(L0_atif) then
         I_ofok=I0_etif
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 208,1758):���� � ������������� �������
      L0_itif = L0_edok.AND.L_atik.AND.L0_usod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,1763):�
      if(L0_itif) then
         I_ofok=I0_otif
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 208,1770):���� � ������������� �������
      L0_utif = L0_edok.AND.L_atik.AND.L0_atod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,1775):�
      if(L0_utif) then
         I_ofok=I0_avif
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 208,1782):���� � ������������� �������
      L0_evif = L0_edok.AND.L_atik.AND.L0_etod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,1787):�
      if(L0_evif) then
         I_ofok=I0_ivif
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 208,1794):���� � ������������� �������
      L0_ovif = L0_edok.AND.L_atik.AND.L0_itod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,1799):�
      if(L0_ovif) then
         I_ofok=I0_uvif
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 208,1806):���� � ������������� �������
      L0_axif = L0_edok.AND.L_atik.AND.L0_otod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,1812):�
      if(L0_axif) then
         I_ofok=I0_exif
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 208,1818):���� � ������������� �������
      L0_esof = L0_edok.AND.L_atik.AND.L0_utod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,1824):�
      if(L0_esof) then
         I_ofok=I0_isof
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 208,1830):���� � ������������� �������
      L0_osof = L0_edok.AND.L_atik.AND.L0_avod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,1836):�
      if(L0_osof) then
         I_ofok=I0_usof
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 208,1842):���� � ������������� �������
      L0_atof = L0_edok.AND.L_atik.AND.L0_evod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,1848):�
      if(L0_atof) then
         I_ofok=I0_etof
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 208,1854):���� � ������������� �������
      L0_itof = L0_edok.AND.L_atik.AND.L0_ivod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,1860):�
      if(L0_itof) then
         I_ofok=I0_otof
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 208,1866):���� � ������������� �������
      L0_utof = L0_edok.AND.L_atik.AND.L0_ovod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,1872):�
      if(L0_utof) then
         I_ofok=I0_avof
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 208,1878):���� � ������������� �������
      L0_evof = L0_edok.AND.L_atik.AND.L0_uvod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,1884):�
      if(L0_evof) then
         I_ofok=I0_ivof
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 208,1890):���� � ������������� �������
      L0_ovof = L0_edok.AND.L_atik.AND.L0_axod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,1896):�
      if(L0_ovof) then
         I_ofok=I0_uvof
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 208,1902):���� � ������������� �������
      L0_axof = L0_edok.AND.L_atik.AND.L0_exod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,1910):�
      if(L0_axof) then
         I_ofok=I0_exof
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 208,1916):���� � ������������� �������
      L0_ixof = L0_edok.AND.L_atik.AND.L0_ixod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,1922):�
      if(L0_ixof) then
         I_ofok=I0_oxof
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 208,1928):���� � ������������� �������
      L0_uxof = L0_edok.AND.L_atik.AND.L0_oxod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,1934):�
      if(L0_uxof) then
         I_ofok=I0_abuf
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 208,1940):���� � ������������� �������
      L0_ebuf = L0_edok.AND.L_atik.AND.L0_amud
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,1946):�
      if(L0_ebuf) then
         I_ofok=I0_ibuf
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 208,1951):���� � ������������� �������
      L0_obuf = L0_edok.AND.L_atik.AND.L0_emud
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,1958):�
      if(L0_obuf) then
         I_ofok=I0_ubuf
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 208,1964):���� � ������������� �������
      L0_iruf = L0_edok.AND.L_atik.AND.L0_imud
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,1970):�
      if(L0_iruf) then
         I_ofok=I0_oruf
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 207,1976):���� � ������������� �������
      L0_uruf = L0_edok.AND.L_atik.AND.L0_omud
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,1982):�
      if(L0_uruf) then
         I_ofok=I0_asuf
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 207,1988):���� � ������������� �������
      L0_esuf = L0_edok.AND.L_atik.AND.L0_umud
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,1994):�
      if(L0_esuf) then
         I_ofok=I0_isuf
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 207,2000):���� � ������������� �������
      L0_osuf = L0_edok.AND.L_atik.AND.L0_ilud
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,2006):�
      if(L0_osuf) then
         I_ofok=I0_usuf
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 207,2013):���� � ������������� �������
      L0_atuf = L0_edok.AND.L_atik.AND.L0_olud
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,2018):�
      if(L0_atuf) then
         I_ofok=I0_etuf
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 207,2025):���� � ������������� �������
      L0_ituf = L0_edok.AND.L_atik.AND.L0_ulud
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,2030):�
      if(L0_ituf) then
         I_ofok=I0_otuf
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 207,2037):���� � ������������� �������
      L0_utuf = L0_edok.AND.L_atik.AND.L0_abud
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,2042):�
      if(L0_utuf) then
         I_ofok=I0_avuf
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 206,2049):���� � ������������� �������
      L0_evuf = L0_edok.AND.L_atik.AND.L0_ebud
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,2054):�
      if(L0_evuf) then
         I_ofok=I0_ivuf
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 206,2061):���� � ������������� �������
      L0_ovuf = L0_edok.AND.L_atik.AND.L0_ibud
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,2066):�
      if(L0_ovuf) then
         I_ofok=I0_uvuf
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 206,2073):���� � ������������� �������
      L0_axuf = L0_edok.AND.L_atik.AND.L0_obud
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,2078):�
      if(L0_axuf) then
         I_ofok=I0_exuf
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 206,2084):���� � ������������� �������
      L0_apak = L0_edok.AND.L_atik.AND.L0_ubud
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,2090):�
      if(L0_apak) then
         I_ofok=I0_epak
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 206,2097):���� � ������������� �������
      L0_ipak = L0_edok.AND.L_atik.AND.L0_adud
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,2104):�
      if(L0_ipak) then
         I_ofok=I0_opak
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 206,2110):���� � ������������� �������
      L0_upak = L0_edok.AND.L_atik.AND.L0_edud
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,2116):�
      if(L0_upak) then
         I_ofok=I0_arak
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 206,2122):���� � ������������� �������
      L0_erak = L0_edok.AND.L_atik.AND.L0_idud
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,2128):�
      if(L0_erak) then
         I_ofok=I0_irak
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 206,2134):���� � ������������� �������
      L0_orak = L0_edok.AND.L_atik.AND.L0_odud
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,2140):�
      if(L0_orak) then
         I_ofok=I0_urak
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 206,2146):���� � ������������� �������
      L0_asak = L0_edok.AND.L_atik.AND.L0_udud
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,2152):�
      if(L0_asak) then
         I_ofok=I0_esak
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 206,2158):���� � ������������� �������
      L0_isak = L0_edok.AND.L_atik.AND.L0_uxod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,2164):�
      if(L0_isak) then
         I_ofok=I0_osak
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 205,2170):���� � ������������� �������
      L0_usak = L0_edok.AND.L_atik.AND.L0_afud
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,2176):�
      if(L0_usak) then
         I_ofok=I0_atak
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 205,2182):���� � ������������� �������
      L0_etak = L0_edok.AND.L_atik.AND.L0_efud
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,2188):�
      if(L0_etak) then
         I_ofok=I0_itak
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 205,2194):���� � ������������� �������
      L0_otak = L0_edok.AND.L_atik.AND.L0_ifud
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,2201):�
      if(L0_otak) then
         I_ofok=I0_utak
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 205,2207):���� � ������������� �������
      L0_avak = L0_edok.AND.L_atik.AND.L0_ofud
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,2213):�
      if(L0_avak) then
         I_ofok=I0_evak
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 205,2218):���� � ������������� �������
      L0_uxek = L0_edok.AND.L_atik.AND.L0_ufud
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,2225):�
      if(L0_uxek) then
         I_ofok=I0_abik
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 205,2232):���� � ������������� �������
      L0_ebik = L0_edok.AND.L_atik.AND.L0_akud
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,2237):�
      if(L0_ebik) then
         I_ofok=I0_ibik
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 205,2244):���� � ������������� �������
      L0_ikik = L0_edok.AND.L_atik.AND.L0_ekud
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,2249):�
      if(L0_ikik) then
         I_ofok=I0_okik
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 205,2256):���� � ������������� �������
      L0_ukik = L0_edok.AND.L_atik.AND.L0_ikud
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,2261):�
      if(L0_ukik) then
         I_ofok=I0_alik
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 205,2268):���� � ������������� �������
      L0_elik = L0_edok.AND.L_atik.AND.L0_okud
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,2273):�
      if(L0_elik) then
         I_ofok=I0_ilik
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 205,2280):���� � ������������� �������
      L0_olik = L0_edok.AND.L_atik.AND.L0_ukud
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,2285):�
      if(L0_olik) then
         I_ofok=I0_ulik
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 205,2292):���� � ������������� �������
      L0_amik = L0_edok.AND.L_atik.AND.L0_alud
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,2298):�
      if(L0_amik) then
         I_ofok=I0_emik
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 206,2305):���� � ������������� �������
      L0_imik = L0_edok.AND.L_atik.AND.L0_elud
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,2310):�
      if(L0_imik) then
         I_ofok=I0_omik
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 206,2317):���� � ������������� �������
      L0_umik = L0_edok.AND.L_atik.AND.L0_apud
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,2322):�
      if(L0_umik) then
         I_ofok=I0_apik
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 206,2329):���� � ������������� �������
      L0_epik = L0_edok.AND.L_atik.AND.L0_epud
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,2334):�
      if(L0_epik) then
         I_ofok=I0_ipik
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 206,2341):���� � ������������� �������
      L0_opik = L0_edok.AND.L_atik.AND.L0_ipud
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,2346):�
      if(L0_opik) then
         I_ofok=I0_upik
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 206,2353):���� � ������������� �������
      L0_arik = L0_edok.AND.L_atik.AND.L0_opud
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,2358):�
      if(L0_arik) then
         I_ofok=I0_erik
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 206,2365):���� � ������������� �������
      L0_irik = L0_edok.AND.L_atik.AND.L0_upud
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,2370):�
      if(L0_irik) then
         I_ofok=I0_orik
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 206,2377):���� � ������������� �������
      L0_urik = L0_edok.AND.L_atik.AND.L0_arud
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,2382):�
      if(L0_urik) then
         I_ofok=I0_asik
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 206,2389):���� � ������������� �������
      L0_esik = L0_edok.AND.L_atik.AND.L0_erud
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,2394):�
      if(L0_esik) then
         I_ofok=I0_isik
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 205,2401):���� � ������������� �������
      L0_osik = L0_edok.AND.L_atik.AND.L0_irud
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 132,2406):�
      if(L0_osik) then
         I_ofok=I0_usik
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 205,2414):���� � ������������� �������
      L0_idok = L0_edok.AND.L0_adok
C LODOCHKA_HANDLER_FDA50_COORD.fmg(  97,2424):�
      if(L0_idok) then
         I_ofok=I0_obok
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 205,2432):���� � ������������� �������
      L0_ifod=I_ofok.eq.I_ofod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 475,2416):���������� �������������
      L0_udod = L0_ifod.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,2415):�
      if(L0_udod) then
         I_afod=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,2418):���� � ������������� �������
      L0_ebod = L0_ifod.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,2402):�
      if(L0_ebod) then
         I_afod=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,2406):���� � ������������� �������
      L0_ibod = L0_ifod.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,2408):�
      if(L0_ibod) then
         I_afod=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,2412):���� � ������������� �������
      L0_ef=I_ofok.eq.I_if
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 641,1857):���������� �������������
      L0_ud = L0_ef.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,1856):�
      if(L0_ud) then
         I_af=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,1860):���� � ������������� �������
      L0_id = L0_ef.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,1843):�
      if(L0_id) then
         I_af=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,1846):���� � ������������� �������
      L0_od = L0_ef.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,1850):�
      if(L0_od) then
         I_af=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,1853):���� � ������������� �������
      L0_ik=I_ofok.eq.I_ok
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 641,1878):���������� �������������
      L0_ak = L0_ik.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,1878):�
      if(L0_ak) then
         I_ek=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,1881):���� � ������������� �������
      L0_of = L0_ik.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,1864):�
      if(L0_of) then
         I_ek=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,1868):���� � ������������� �������
      L0_uf = L0_ik.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,1871):�
      if(L0_uf) then
         I_ek=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,1874):���� � ������������� �������
      L0_ol=I_ofok.eq.I_ul
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 641,1900):���������� �������������
      L0_el = L0_ol.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,1899):�
      if(L0_el) then
         I_il=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,1902):���� � ������������� �������
      L0_uk = L0_ol.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,1886):�
      if(L0_uk) then
         I_il=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,1890):���� � ������������� �������
      L0_al = L0_ol.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,1892):�
      if(L0_al) then
         I_il=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,1896):���� � ������������� �������
      L0_ov=I_ofok.eq.I_uv
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 641,2008):���������� �������������
      L0_ev = L0_ov.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,2006):�
      if(L0_ev) then
         I_iv=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,2010):���� � ������������� �������
      L0_ut = L0_ov.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,1994):�
      if(L0_ut) then
         I_iv=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,1997):���� � ������������� �������
      L0_av = L0_ov.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,2000):�
      if(L0_av) then
         I_iv=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,2004):���� � ������������� �������
      L0_it=I_ofok.eq.I_ot
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 641,1986):���������� �������������
      L0_at = L0_it.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,1985):�
      if(L0_at) then
         I_et=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,1988):���� � ������������� �������
      L0_os = L0_it.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,1972):�
      if(L0_os) then
         I_et=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,1976):���� � ������������� �������
      L0_us = L0_it.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,1978):�
      if(L0_us) then
         I_et=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,1982):���� � ������������� �������
      L0_es=I_ofok.eq.I_is
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 641,1964):���������� �������������
      L0_ur = L0_es.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,1964):�
      if(L0_ur) then
         I_as=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,1967):���� � ������������� �������
      L0_ir = L0_es.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,1950):�
      if(L0_ir) then
         I_as=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,1954):���� � ������������� �������
      L0_or = L0_es.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,1957):�
      if(L0_or) then
         I_as=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,1960):���� � ������������� �������
      L0_ar=I_ofok.eq.I_er
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 641,1943):���������� �������������
      L0_op = L0_ar.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,1942):�
      if(L0_op) then
         I_up=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,1946):���� � ������������� �������
      L0_ep = L0_ar.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,1929):�
      if(L0_ep) then
         I_up=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,1932):���� � ������������� �������
      L0_ip = L0_ar.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,1936):�
      if(L0_ip) then
         I_up=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,1939):���� � ������������� �������
      L0_umad=I_ofok.eq.I_apad
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 641,2394):���������� �������������
      L0_imad = L0_umad.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,2394):�
      if(L0_imad) then
         I_omad=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,2397):���� � ������������� �������
      L0_amad = L0_umad.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,2380):�
      if(L0_amad) then
         I_omad=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,2384):���� � ������������� �������
      L0_emad = L0_umad.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,2387):�
      if(L0_emad) then
         I_omad=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,2390):���� � ������������� �������
      L0_olad=I_ofok.eq.I_ulad
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 641,2373):���������� �������������
      L0_elad = L0_olad.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,2372):�
      if(L0_elad) then
         I_ilad=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,2376):���� � ������������� �������
      L0_ukad = L0_olad.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,2359):�
      if(L0_ukad) then
         I_ilad=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,2362):���� � ������������� �������
      L0_alad = L0_olad.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,2366):�
      if(L0_alad) then
         I_ilad=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,2369):���� � ������������� �������
      L0_ikad=I_ofok.eq.I_okad
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 641,2352):���������� �������������
      L0_akad = L0_ikad.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,2350):�
      if(L0_akad) then
         I_ekad=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,2354):���� � ������������� �������
      L0_ofad = L0_ikad.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,2338):�
      if(L0_ofad) then
         I_ekad=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,2341):���� � ������������� �������
      L0_ufad = L0_ikad.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,2344):�
      if(L0_ufad) then
         I_ekad=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,2348):���� � ������������� �������
      L0_efad=I_ofok.eq.I_ifad
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 641,2330):���������� �������������
      L0_udad = L0_efad.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,2329):�
      if(L0_udad) then
         I_afad=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,2332):���� � ������������� �������
      L0_idad = L0_efad.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,2316):�
      if(L0_idad) then
         I_afad=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,2320):���� � ������������� �������
      L0_odad = L0_efad.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,2322):�
      if(L0_odad) then
         I_afad=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,2326):���� � ������������� �������
      L0_adad=I_ofok.eq.I_edad
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 641,2308):���������� �������������
      L0_obad = L0_adad.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,2308):�
      if(L0_obad) then
         I_ubad=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,2311):���� � ������������� �������
      L0_ebad = L0_adad.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,2294):�
      if(L0_ebad) then
         I_ubad=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,2298):���� � ������������� �������
      L0_ibad = L0_adad.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,2301):�
      if(L0_ibad) then
         I_ubad=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,2304):���� � ������������� �������
      L0_uxu=I_ofok.eq.I_abad
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 641,2287):���������� �������������
      L0_ixu = L0_uxu.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,2286):�
      if(L0_ixu) then
         I_oxu=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,2290):���� � ������������� �������
      L0_axu = L0_uxu.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,2273):�
      if(L0_axu) then
         I_oxu=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,2276):���� � ������������� �������
      L0_exu = L0_uxu.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,2280):�
      if(L0_exu) then
         I_oxu=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,2283):���� � ������������� �������
      L0_ovu=I_ofok.eq.I_uvu
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 641,2266):���������� �������������
      L0_evu = L0_ovu.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,2264):�
      if(L0_evu) then
         I_ivu=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,2268):���� � ������������� �������
      L0_utu = L0_ovu.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,2252):�
      if(L0_utu) then
         I_ivu=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,2255):���� � ������������� �������
      L0_avu = L0_ovu.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,2258):�
      if(L0_avu) then
         I_ivu=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,2262):���� � ������������� �������
      L0_itu=I_ofok.eq.I_otu
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 641,2244):���������� �������������
      L0_atu = L0_itu.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,2243):�
      if(L0_atu) then
         I_etu=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,2246):���� � ������������� �������
      L0_osu = L0_itu.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,2230):�
      if(L0_osu) then
         I_etu=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,2234):���� � ������������� �������
      L0_usu = L0_itu.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,2236):�
      if(L0_usu) then
         I_etu=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,2240):���� � ������������� �������
      L0_esu=I_ofok.eq.I_isu
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 641,2222):���������� �������������
      L0_uru = L0_esu.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,2222):�
      if(L0_uru) then
         I_asu=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,2225):���� � ������������� �������
      L0_iru = L0_esu.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,2208):�
      if(L0_iru) then
         I_asu=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,2212):���� � ������������� �������
      L0_oru = L0_esu.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,2215):�
      if(L0_oru) then
         I_asu=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,2218):���� � ������������� �������
      L0_aru=I_ofok.eq.I_eru
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 641,2201):���������� �������������
      L0_opu = L0_aru.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,2200):�
      if(L0_opu) then
         I_upu=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,2204):���� � ������������� �������
      L0_epu = L0_aru.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,2187):�
      if(L0_epu) then
         I_upu=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,2190):���� � ������������� �������
      L0_ipu = L0_aru.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,2194):�
      if(L0_ipu) then
         I_upu=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,2197):���� � ������������� �������
      L0_umu=I_ofok.eq.I_apu
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 641,2180):���������� �������������
      L0_imu = L0_umu.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,2178):�
      if(L0_imu) then
         I_omu=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,2182):���� � ������������� �������
      L0_amu = L0_umu.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,2166):�
      if(L0_amu) then
         I_omu=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,2169):���� � ������������� �������
      L0_emu = L0_umu.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,2172):�
      if(L0_emu) then
         I_omu=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,2176):���� � ������������� �������
      L0_olu=I_ofok.eq.I_ulu
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 641,2158):���������� �������������
      L0_elu = L0_olu.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,2157):�
      if(L0_elu) then
         I_ilu=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,2160):���� � ������������� �������
      L0_uku = L0_olu.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,2144):�
      if(L0_uku) then
         I_ilu=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,2148):���� � ������������� �������
      L0_alu = L0_olu.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,2150):�
      if(L0_alu) then
         I_ilu=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,2154):���� � ������������� �������
      L0_iku=I_ofok.eq.I_oku
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 641,2136):���������� �������������
      L0_aku = L0_iku.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,2136):�
      if(L0_aku) then
         I_eku=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,2139):���� � ������������� �������
      L0_ofu = L0_iku.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,2122):�
      if(L0_ofu) then
         I_eku=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,2126):���� � ������������� �������
      L0_ufu = L0_iku.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,2129):�
      if(L0_ufu) then
         I_eku=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,2132):���� � ������������� �������
      L0_efu=I_ofok.eq.I_ifu
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 641,2115):���������� �������������
      L0_udu = L0_efu.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,2114):�
      if(L0_udu) then
         I_afu=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,2118):���� � ������������� �������
      L0_idu = L0_efu.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,2101):�
      if(L0_idu) then
         I_afu=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,2104):���� � ������������� �������
      L0_odu = L0_efu.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,2108):�
      if(L0_odu) then
         I_afu=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,2111):���� � ������������� �������
      L0_adu=I_ofok.eq.I_edu
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 641,2094):���������� �������������
      L0_obu = L0_adu.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,2092):�
      if(L0_obu) then
         I_ubu=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,2096):���� � ������������� �������
      L0_ebu = L0_adu.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,2080):�
      if(L0_ebu) then
         I_ubu=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,2083):���� � ������������� �������
      L0_ibu = L0_adu.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,2086):�
      if(L0_ibu) then
         I_ubu=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,2090):���� � ������������� �������
      L0_uxo=I_ofok.eq.I_abu
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 641,2072):���������� �������������
      L0_ixo = L0_uxo.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,2071):�
      if(L0_ixo) then
         I_oxo=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,2074):���� � ������������� �������
      L0_axo = L0_uxo.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,2058):�
      if(L0_axo) then
         I_oxo=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,2062):���� � ������������� �������
      L0_exo = L0_uxo.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,2064):�
      if(L0_exo) then
         I_oxo=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,2068):���� � ������������� �������
      L0_ovo=I_ofok.eq.I_uvo
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 641,2050):���������� �������������
      L0_evo = L0_ovo.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,2050):�
      if(L0_evo) then
         I_ivo=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,2053):���� � ������������� �������
      L0_uto = L0_ovo.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,2036):�
      if(L0_uto) then
         I_ivo=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,2040):���� � ������������� �������
      L0_avo = L0_ovo.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,2043):�
      if(L0_avo) then
         I_ivo=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,2046):���� � ������������� �������
      L0_ito=I_ofok.eq.I_oto
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 641,2029):���������� �������������
      L0_ato = L0_ito.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,2028):�
      if(L0_ato) then
         I_eto=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,2032):���� � ������������� �������
      L0_oso = L0_ito.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,2015):�
      if(L0_oso) then
         I_eto=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,2018):���� � ������������� �������
      L0_uso = L0_ito.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,2022):�
      if(L0_uso) then
         I_eto=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,2025):���� � ������������� �������
      L0_um=I_ofok.eq.I_ap
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 641,1922):���������� �������������
      L0_im = L0_um.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,1920):�
      if(L0_im) then
         I_om=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,1924):���� � ������������� �������
      L0_am = L0_um.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,1908):�
      if(L0_am) then
         I_om=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,1911):���� � ������������� �������
      L0_em = L0_um.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,1914):�
      if(L0_em) then
         I_om=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,1918):���� � ������������� �������
      L0_arad=I_ofok.eq.I_erad
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 641,2416):���������� �������������
      L0_opad = L0_arad.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,2415):�
      if(L0_opad) then
         I_upad=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,2418):���� � ������������� �������
      L0_epad = L0_arad.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,2402):�
      if(L0_epad) then
         I_upad=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,2406):���� � ������������� �������
      L0_ipad = L0_arad.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 681,2408):�
      if(L0_ipad) then
         I_upad=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 712,2412):���� � ������������� �������
      L0_uxid=I_ofok.eq.I_abod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 475,2394):���������� �������������
      L0_ixid = L0_uxid.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,2394):�
      if(L0_ixid) then
         I_oxid=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,2397):���� � ������������� �������
      L0_axid = L0_uxid.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,2380):�
      if(L0_axid) then
         I_oxid=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,2384):���� � ������������� �������
      L0_exid = L0_uxid.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,2387):�
      if(L0_exid) then
         I_oxid=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,2390):���� � ������������� �������
      L0_ovid=I_ofok.eq.I_uvid
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 475,2373):���������� �������������
      L0_evid = L0_ovid.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,2372):�
      if(L0_evid) then
         I_ivid=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,2376):���� � ������������� �������
      L0_utid = L0_ovid.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,2359):�
      if(L0_utid) then
         I_ivid=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,2362):���� � ������������� �������
      L0_avid = L0_ovid.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,2366):�
      if(L0_avid) then
         I_ivid=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,2369):���� � ������������� �������
      L0_itid=I_ofok.eq.I_otid
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 475,2352):���������� �������������
      L0_atid = L0_itid.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,2350):�
      if(L0_atid) then
         I_etid=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,2354):���� � ������������� �������
      L0_osid = L0_itid.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,2338):�
      if(L0_osid) then
         I_etid=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,2341):���� � ������������� �������
      L0_usid = L0_itid.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,2344):�
      if(L0_usid) then
         I_etid=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,2348):���� � ������������� �������
      L0_esid=I_ofok.eq.I_isid
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 475,2330):���������� �������������
      L0_urid = L0_esid.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,2329):�
      if(L0_urid) then
         I_asid=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,2332):���� � ������������� �������
      L0_irid = L0_esid.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,2316):�
      if(L0_irid) then
         I_asid=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,2320):���� � ������������� �������
      L0_orid = L0_esid.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,2322):�
      if(L0_orid) then
         I_asid=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,2326):���� � ������������� �������
      L0_arid=I_ofok.eq.I_erid
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 475,2308):���������� �������������
      L0_opid = L0_arid.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,2308):�
      if(L0_opid) then
         I_upid=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,2311):���� � ������������� �������
      L0_epid = L0_arid.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,2294):�
      if(L0_epid) then
         I_upid=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,2298):���� � ������������� �������
      L0_ipid = L0_arid.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,2301):�
      if(L0_ipid) then
         I_upid=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,2304):���� � ������������� �������
      L0_umid=I_ofok.eq.I_apid
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 475,2287):���������� �������������
      L0_imid = L0_umid.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,2286):�
      if(L0_imid) then
         I_omid=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,2290):���� � ������������� �������
      L0_amid = L0_umid.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,2273):�
      if(L0_amid) then
         I_omid=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,2276):���� � ������������� �������
      L0_emid = L0_umid.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,2280):�
      if(L0_emid) then
         I_omid=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,2283):���� � ������������� �������
      L0_olid=I_ofok.eq.I_ulid
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 475,2266):���������� �������������
      L0_elid = L0_olid.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,2264):�
      if(L0_elid) then
         I_ilid=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,2268):���� � ������������� �������
      L0_ukid = L0_olid.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,2252):�
      if(L0_ukid) then
         I_ilid=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,2255):���� � ������������� �������
      L0_alid = L0_olid.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,2258):�
      if(L0_alid) then
         I_ilid=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,2262):���� � ������������� �������
      L0_ikid=I_ofok.eq.I_okid
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 475,2244):���������� �������������
      L0_akid = L0_ikid.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,2243):�
      if(L0_akid) then
         I_ekid=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,2246):���� � ������������� �������
      L0_ofid = L0_ikid.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,2230):�
      if(L0_ofid) then
         I_ekid=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,2234):���� � ������������� �������
      L0_ufid = L0_ikid.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,2236):�
      if(L0_ufid) then
         I_ekid=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,2240):���� � ������������� �������
      L0_efid=I_ofok.eq.I_ifid
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 475,2222):���������� �������������
      L0_udid = L0_efid.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,2222):�
      if(L0_udid) then
         I_afid=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,2225):���� � ������������� �������
      L0_idid = L0_efid.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,2208):�
      if(L0_idid) then
         I_afid=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,2212):���� � ������������� �������
      L0_odid = L0_efid.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,2215):�
      if(L0_odid) then
         I_afid=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,2218):���� � ������������� �������
      L0_adid=I_ofok.eq.I_edid
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 475,2201):���������� �������������
      L0_obid = L0_adid.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,2200):�
      if(L0_obid) then
         I_ubid=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,2204):���� � ������������� �������
      L0_ebid = L0_adid.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,2187):�
      if(L0_ebid) then
         I_ubid=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,2190):���� � ������������� �������
      L0_ibid = L0_adid.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,2194):�
      if(L0_ibid) then
         I_ubid=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,2197):���� � ������������� �������
      L0_uxed=I_ofok.eq.I_abid
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 475,2180):���������� �������������
      L0_ixed = L0_uxed.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,2178):�
      if(L0_ixed) then
         I_oxed=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,2182):���� � ������������� �������
      L0_axed = L0_uxed.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,2166):�
      if(L0_axed) then
         I_oxed=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,2169):���� � ������������� �������
      L0_exed = L0_uxed.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,2172):�
      if(L0_exed) then
         I_oxed=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,2176):���� � ������������� �������
      L0_oved=I_ofok.eq.I_uved
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 475,2158):���������� �������������
      L0_eved = L0_oved.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,2157):�
      if(L0_eved) then
         I_ived=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,2160):���� � ������������� �������
      L0_uted = L0_oved.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,2144):�
      if(L0_uted) then
         I_ived=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,2148):���� � ������������� �������
      L0_aved = L0_oved.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,2150):�
      if(L0_aved) then
         I_ived=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,2154):���� � ������������� �������
      L0_ited=I_ofok.eq.I_oted
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 475,2136):���������� �������������
      L0_ated = L0_ited.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,2136):�
      if(L0_ated) then
         I_eted=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,2139):���� � ������������� �������
      L0_osed = L0_ited.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,2122):�
      if(L0_osed) then
         I_eted=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,2126):���� � ������������� �������
      L0_used = L0_ited.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,2129):�
      if(L0_used) then
         I_eted=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,2132):���� � ������������� �������
      L0_esed=I_ofok.eq.I_ised
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 475,2115):���������� �������������
      L0_ured = L0_esed.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,2114):�
      if(L0_ured) then
         I_ased=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,2118):���� � ������������� �������
      L0_ired = L0_esed.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,2101):�
      if(L0_ired) then
         I_ased=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,2104):���� � ������������� �������
      L0_ored = L0_esed.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,2108):�
      if(L0_ored) then
         I_ased=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,2111):���� � ������������� �������
      L0_ared=I_ofok.eq.I_ered
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 475,2094):���������� �������������
      L0_oped = L0_ared.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,2092):�
      if(L0_oped) then
         I_uped=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,2096):���� � ������������� �������
      L0_eped = L0_ared.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,2080):�
      if(L0_eped) then
         I_uped=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,2083):���� � ������������� �������
      L0_iped = L0_ared.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,2086):�
      if(L0_iped) then
         I_uped=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,2090):���� � ������������� �������
      L0_umed=I_ofok.eq.I_aped
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 475,2072):���������� �������������
      L0_imed = L0_umed.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,2071):�
      if(L0_imed) then
         I_omed=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,2074):���� � ������������� �������
      L0_amed = L0_umed.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,2058):�
      if(L0_amed) then
         I_omed=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,2062):���� � ������������� �������
      L0_emed = L0_umed.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,2064):�
      if(L0_emed) then
         I_omed=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,2068):���� � ������������� �������
      L0_oled=I_ofok.eq.I_uled
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 475,2050):���������� �������������
      L0_eled = L0_oled.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,2050):�
      if(L0_eled) then
         I_iled=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,2053):���� � ������������� �������
      L0_uked = L0_oled.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,2036):�
      if(L0_uked) then
         I_iled=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,2040):���� � ������������� �������
      L0_aled = L0_oled.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,2043):�
      if(L0_aled) then
         I_iled=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,2046):���� � ������������� �������
      L0_iked=I_ofok.eq.I_oked
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 475,2029):���������� �������������
      L0_aked = L0_iked.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,2028):�
      if(L0_aked) then
         I_eked=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,2032):���� � ������������� �������
      L0_ofed = L0_iked.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,2015):�
      if(L0_ofed) then
         I_eked=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,2018):���� � ������������� �������
      L0_ufed = L0_iked.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,2022):�
      if(L0_ufed) then
         I_eked=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,2025):���� � ������������� �������
      L0_efed=I_ofok.eq.I_ifed
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 475,2008):���������� �������������
      L0_uded = L0_efed.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,2006):�
      if(L0_uded) then
         I_afed=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,2010):���� � ������������� �������
      L0_ided = L0_efed.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1994):�
      if(L0_ided) then
         I_afed=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1997):���� � ������������� �������
      L0_oded = L0_efed.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,2000):�
      if(L0_oded) then
         I_afed=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,2004):���� � ������������� �������
      L0_aded=I_ofok.eq.I_eded
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 475,1986):���������� �������������
      L0_obed = L0_aded.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1985):�
      if(L0_obed) then
         I_ubed=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1988):���� � ������������� �������
      L0_ebed = L0_aded.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1972):�
      if(L0_ebed) then
         I_ubed=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1976):���� � ������������� �������
      L0_ibed = L0_aded.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1978):�
      if(L0_ibed) then
         I_ubed=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1982):���� � ������������� �������
      L0_uxad=I_ofok.eq.I_abed
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 475,1964):���������� �������������
      L0_ixad = L0_uxad.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1964):�
      if(L0_ixad) then
         I_oxad=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1967):���� � ������������� �������
      L0_axad = L0_uxad.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1950):�
      if(L0_axad) then
         I_oxad=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1954):���� � ������������� �������
      L0_exad = L0_uxad.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1957):�
      if(L0_exad) then
         I_oxad=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1960):���� � ������������� �������
      L0_ovad=I_ofok.eq.I_uvad
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 475,1943):���������� �������������
      L0_evad = L0_ovad.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1942):�
      if(L0_evad) then
         I_ivad=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1946):���� � ������������� �������
      L0_utad = L0_ovad.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1929):�
      if(L0_utad) then
         I_ivad=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1932):���� � ������������� �������
      L0_avad = L0_ovad.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1936):�
      if(L0_avad) then
         I_ivad=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1939):���� � ������������� �������
      L0_itad=I_ofok.eq.I_otad
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 475,1922):���������� �������������
      L0_atad = L0_itad.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1920):�
      if(L0_atad) then
         I_etad=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1924):���� � ������������� �������
      L0_osad = L0_itad.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1908):�
      if(L0_osad) then
         I_etad=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1911):���� � ������������� �������
      L0_usad = L0_itad.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1914):�
      if(L0_usad) then
         I_etad=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1918):���� � ������������� �������
      L0_esad=I_ofok.eq.I_isad
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 475,1900):���������� �������������
      L0_urad = L0_esad.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1899):�
      if(L0_urad) then
         I_asad=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1902):���� � ������������� �������
      L0_irad = L0_esad.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1886):�
      if(L0_irad) then
         I_asad=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1890):���� � ������������� �������
      L0_orad = L0_esad.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1892):�
      if(L0_orad) then
         I_asad=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1896):���� � ������������� �������
      L0_iko=I_ofok.eq.I_oko
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 475,1878):���������� �������������
      L0_ako = L0_iko.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1878):�
      if(L0_ako) then
         I_eko=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1881):���� � ������������� �������
      L0_ofo = L0_iko.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1864):�
      if(L0_ofo) then
         I_eko=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1868):���� � ������������� �������
      L0_ufo = L0_iko.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1871):�
      if(L0_ufo) then
         I_eko=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1874):���� � ������������� �������
      L0_efo=I_ofok.eq.I_ifo
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 475,1857):���������� �������������
      L0_udo = L0_efo.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1856):�
      if(L0_udo) then
         I_afo=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1860):���� � ������������� �������
      L0_ido = L0_efo.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1843):�
      if(L0_ido) then
         I_afo=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1846):���� � ������������� �������
      L0_odo = L0_efo.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1850):�
      if(L0_odo) then
         I_afo=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1853):���� � ������������� �������
      L0_ubo=I_ofok.eq.I_ex
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 475,1836):���������� �������������
      L0_ado = L0_ubo.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1834):�
      if(L0_ado) then
         I_edo=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1838):���� � ������������� �������
      L0_ibo = L0_ubo.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1822):�
      if(L0_ibo) then
         I_edo=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1825):���� � ������������� �������
      L0_obo = L0_ubo.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1828):�
      if(L0_obo) then
         I_edo=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1832):���� � ������������� �������
      L0_uxi=I_ofok.eq.I_ax
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 475,1814):���������� �������������
      L0_abo = L0_uxi.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1813):�
      if(L0_abo) then
         I_ebo=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1816):���� � ������������� �������
      L0_ixi = L0_uxi.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1800):�
      if(L0_ixi) then
         I_ebo=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1804):���� � ������������� �������
      L0_oxi = L0_uxi.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1806):�
      if(L0_oxi) then
         I_ebo=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1810):���� � ������������� �������
      L0_axi=I_ofok.eq.I_exi
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 475,1792):���������� �������������
      L0_ovi = L0_axi.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1792):�
      if(L0_ovi) then
         I_uvi=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1795):���� � ������������� �������
      L0_evi = L0_axi.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1778):�
      if(L0_evi) then
         I_uvi=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1782):���� � ������������� �������
      L0_ivi = L0_axi.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1785):�
      if(L0_ivi) then
         I_uvi=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1788):���� � ������������� �������
      L0_uti=I_ofok.eq.I_avi
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 475,1771):���������� �������������
      L0_iti = L0_uti.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1770):�
      if(L0_iti) then
         I_oti=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1774):���� � ������������� �������
      L0_ati = L0_uti.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1757):�
      if(L0_ati) then
         I_oti=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1760):���� � ������������� �������
      L0_eti = L0_uti.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1764):�
      if(L0_eti) then
         I_oti=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1767):���� � ������������� �������
      L0_osi=I_ofok.eq.I_usi
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 475,1750):���������� �������������
      L0_esi = L0_osi.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1748):�
      if(L0_esi) then
         I_isi=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1752):���� � ������������� �������
      L0_uri = L0_osi.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1736):�
      if(L0_uri) then
         I_isi=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1739):���� � ������������� �������
      L0_asi = L0_osi.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1742):�
      if(L0_asi) then
         I_isi=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1746):���� � ������������� �������
      L0_iri=I_ofok.eq.I_ori
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 475,1728):���������� �������������
      L0_ari = L0_iri.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1727):�
      if(L0_ari) then
         I_eri=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1730):���� � ������������� �������
      L0_opi = L0_iri.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1714):�
      if(L0_opi) then
         I_eri=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1718):���� � ������������� �������
      L0_upi = L0_iri.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1720):�
      if(L0_upi) then
         I_eri=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1724):���� � ������������� �������
      L0_epi=I_ofok.eq.I_ipi
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 475,1706):���������� �������������
      L0_umi = L0_epi.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1706):�
      if(L0_umi) then
         I_api=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1709):���� � ������������� �������
      L0_imi = L0_epi.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1692):�
      if(L0_imi) then
         I_api=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1696):���� � ������������� �������
      L0_omi = L0_epi.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1699):�
      if(L0_omi) then
         I_api=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1702):���� � ������������� �������
      L0_ami=I_ofok.eq.I_emi
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 475,1685):���������� �������������
      L0_oli = L0_ami.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1684):�
      if(L0_oli) then
         I_uli=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1688):���� � ������������� �������
      L0_eli = L0_ami.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1671):�
      if(L0_eli) then
         I_uli=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1674):���� � ������������� �������
      L0_ili = L0_ami.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1678):�
      if(L0_ili) then
         I_uli=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1681):���� � ������������� �������
      L0_uki=I_ofok.eq.I_ali
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 475,1664):���������� �������������
      L0_iki = L0_uki.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1662):�
      if(L0_iki) then
         I_oki=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1666):���� � ������������� �������
      L0_aki = L0_uki.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1650):�
      if(L0_aki) then
         I_oki=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1653):���� � ������������� �������
      L0_eki = L0_uki.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1656):�
      if(L0_eki) then
         I_oki=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1660):���� � ������������� �������
      L0_ofi=I_ofok.eq.I_ufi
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 475,1642):���������� �������������
      L0_efi = L0_ofi.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1641):�
      if(L0_efi) then
         I_ifi=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1644):���� � ������������� �������
      L0_udi = L0_ofi.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1628):�
      if(L0_udi) then
         I_ifi=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1632):���� � ������������� �������
      L0_afi = L0_ofi.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1634):�
      if(L0_afi) then
         I_ifi=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1638):���� � ������������� �������
      L0_idi=I_ofok.eq.I_odi
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 475,1620):���������� �������������
      L0_adi = L0_idi.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1620):�
      if(L0_adi) then
         I_edi=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1623):���� � ������������� �������
      L0_obi = L0_idi.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1606):�
      if(L0_obi) then
         I_edi=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1610):���� � ������������� �������
      L0_ubi = L0_idi.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1613):�
      if(L0_ubi) then
         I_edi=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1616):���� � ������������� �������
      L0_ebi=I_ofok.eq.I_ibi
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 475,1599):���������� �������������
      L0_uxe = L0_ebi.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1598):�
      if(L0_uxe) then
         I_abi=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1602):���� � ������������� �������
      L0_ixe = L0_ebi.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1585):�
      if(L0_ixe) then
         I_abi=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1588):���� � ������������� �������
      L0_oxe = L0_ebi.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1592):�
      if(L0_oxe) then
         I_abi=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1595):���� � ������������� �������
      L0_axe=I_ofok.eq.I_exe
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 475,1578):���������� �������������
      L0_ove = L0_axe.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1576):�
      if(L0_ove) then
         I_uve=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1580):���� � ������������� �������
      L0_eve = L0_axe.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1564):�
      if(L0_eve) then
         I_uve=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1567):���� � ������������� �������
      L0_ive = L0_axe.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1570):�
      if(L0_ive) then
         I_uve=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1574):���� � ������������� �������
      L0_ute=I_ofok.eq.I_ave
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 475,1556):���������� �������������
      L0_ite = L0_ute.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1555):�
      if(L0_ite) then
         I_ote=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1558):���� � ������������� �������
      L0_ate = L0_ute.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1542):�
      if(L0_ate) then
         I_ote=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1546):���� � ������������� �������
      L0_ete = L0_ute.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1548):�
      if(L0_ete) then
         I_ote=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1552):���� � ������������� �������
      L0_ose=I_ofok.eq.I_use
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 475,1534):���������� �������������
      L0_ese = L0_ose.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1534):�
      if(L0_ese) then
         I_ise=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1537):���� � ������������� �������
      L0_ure = L0_ose.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1520):�
      if(L0_ure) then
         I_ise=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1524):���� � ������������� �������
      L0_ase = L0_ose.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1527):�
      if(L0_ase) then
         I_ise=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1530):���� � ������������� �������
      L0_ire=I_ofok.eq.I_ore
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 475,1513):���������� �������������
      L0_are = L0_ire.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1512):�
      if(L0_are) then
         I_ere=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1516):���� � ������������� �������
      L0_ope = L0_ire.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1499):�
      if(L0_ope) then
         I_ere=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1502):���� � ������������� �������
      L0_upe = L0_ire.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1506):�
      if(L0_upe) then
         I_ere=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1509):���� � ������������� �������
      L0_epe=I_ofok.eq.I_ipe
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 475,1492):���������� �������������
      L0_ume = L0_epe.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1490):�
      if(L0_ume) then
         I_ape=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1494):���� � ������������� �������
      L0_ime = L0_epe.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1478):�
      if(L0_ime) then
         I_ape=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1481):���� � ������������� �������
      L0_ome = L0_epe.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1484):�
      if(L0_ome) then
         I_ape=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1488):���� � ������������� �������
      L0_ame=I_ofok.eq.I_eme
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 475,1470):���������� �������������
      L0_ole = L0_ame.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1469):�
      if(L0_ole) then
         I_ule=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1472):���� � ������������� �������
      L0_ele = L0_ame.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1456):�
      if(L0_ele) then
         I_ule=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1460):���� � ������������� �������
      L0_ile = L0_ame.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1462):�
      if(L0_ile) then
         I_ule=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1466):���� � ������������� �������
      L0_uke=I_ofok.eq.I_ale
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 475,1448):���������� �������������
      L0_ike = L0_uke.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1448):�
      if(L0_ike) then
         I_oke=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1451):���� � ������������� �������
      L0_ake = L0_uke.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1434):�
      if(L0_ake) then
         I_oke=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1438):���� � ������������� �������
      L0_eke = L0_uke.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1441):�
      if(L0_eke) then
         I_oke=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1444):���� � ������������� �������
      L0_ofe=I_ofok.eq.I_ufe
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 475,1427):���������� �������������
      L0_efe = L0_ofe.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1426):�
      if(L0_efe) then
         I_ife=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1430):���� � ������������� �������
      L0_ude = L0_ofe.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1413):�
      if(L0_ude) then
         I_ife=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1416):���� � ������������� �������
      L0_afe = L0_ofe.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1420):�
      if(L0_afe) then
         I_ife=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1423):���� � ������������� �������
      L0_ide=I_ofok.eq.I_ode
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 475,1406):���������� �������������
      L0_ade = L0_ide.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1404):�
      if(L0_ade) then
         I_ede=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1408):���� � ������������� �������
      L0_obe = L0_ide.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1392):�
      if(L0_obe) then
         I_ede=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1395):���� � ������������� �������
      L0_ube = L0_ide.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1398):�
      if(L0_ube) then
         I_ede=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1402):���� � ������������� �������
      L0_ebe=I_ofok.eq.I_ibe
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 475,1384):���������� �������������
      L0_ux = L0_ebe.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1383):�
      if(L0_ux) then
         I_abe=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1386):���� � ������������� �������
      L0_ix = L0_ebe.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1370):�
      if(L0_ix) then
         I_abe=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1374):���� � ������������� �������
      L0_ox = L0_ebe.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1376):�
      if(L0_ox) then
         I_abe=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1380):���� � ������������� �������
      L0_eso=I_ofok.eq.I_iso
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 475,1362):���������� �������������
      L0_uro = L0_eso.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1362):�
      if(L0_uro) then
         I_aso=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1365):���� � ������������� �������
      L0_iro = L0_eso.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1348):�
      if(L0_iro) then
         I_aso=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1352):���� � ������������� �������
      L0_oro = L0_eso.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1355):�
      if(L0_oro) then
         I_aso=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1358):���� � ������������� �������
      L0_aro=I_ofok.eq.I_ero
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 475,1341):���������� �������������
      L0_opo = L0_aro.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1340):�
      if(L0_opo) then
         I_upo=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1344):���� � ������������� �������
      L0_epo = L0_aro.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1327):�
      if(L0_epo) then
         I_upo=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1330):���� � ������������� �������
      L0_ipo = L0_aro.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1334):�
      if(L0_ipo) then
         I_upo=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1337):���� � ������������� �������
      L0_umo=I_ofok.eq.I_apo
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 475,1320):���������� �������������
      L0_imo = L0_umo.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1318):�
      if(L0_imo) then
         I_omo=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1322):���� � ������������� �������
      L0_amo = L0_umo.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1306):�
      if(L0_amo) then
         I_omo=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1309):���� � ������������� �������
      L0_emo = L0_umo.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1312):�
      if(L0_emo) then
         I_omo=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1316):���� � ������������� �������
      L0_olo=I_ofok.eq.I_ulo
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 475,1298):���������� �������������
      L0_elo = L0_olo.AND.L0_odod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1297):�
      if(L0_elo) then
         I_ilo=I_ekod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1300):���� � ������������� �������
      L0_uko = L0_olo.AND.L0_obod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1284):�
      if(L0_uko) then
         I_ilo=I_ufod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1288):���� � ������������� �������
      L0_alo = L0_olo.AND.L0_adod
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 515,1290):�
      if(L0_alo) then
         I_ilo=I_akod
      endif
C LODOCHKA_HANDLER_FDA50_COORD.fmg( 546,1294):���� � ������������� �������
      End
