      Subroutine TOLKATEL_OY_HANDLER_dpro(ext_deltat,L_e,L_i
     &,R_o,R_u,R_ad,I_ed,R_ud,R_af,R_ef,R_if,R_ek,R_ik,C20_om
     &,C20_op,I_ir,I_as,R_es,R_is,R_os,R_us,L_at,L_et,I_it
     &,I_av,I_ax,I_ox,C20_obe,C20_ode,C8_ofe,R_ile,R_ole,L_ule
     &,R_ame,R_eme,I_ime,L_ape,L_epe,I_ope,I_ere,L_ure,L_ese
     &,L_ise,L_use,L_ete,L_ite,L_ote,L_ove,L_oxe,L_abi,L_ibi
     &,L_ubi,R8_edi,R_afi,R8_ofi,L_oki,I_uki,L_emi,L_omi,L_upi
     &,L_iri,L_uri,L_asi,L_esi,L_isi,L_osi,L_usi)
C |L_e           |1 1 I|YA26C           |������� ������� �� ���������� (��)|F|
C |L_i           |1 1 I|YA25C           |������� ������� �� ����������|F|
C |R_o           |4 4 K|_uintT_INT_1    |����������� ������ ����������� ������|`p_TOLKATEL_UP`|
C |R_u           |4 4 K|_tintT_INT_1    |[���]�������� T �����������|1|
C |R_ad          |4 4 K|_lintT_INT_1    |����������� ������ ����������� �����|0.0|
C |I_ed          |2 4 O|LWORK           |����� �������||
C |R_ud          |4 4 K|_lcmpJ2850      |[]�������� ������ �����������|`p_TOLKATEL_XH54`|
C |R_af          |4 4 K|_lcmpJ2849      |[]�������� ������ �����������|1.0|
C |R_ef          |4 4 O|VY01            |��������� ��������� �� ��� X||
C |R_if          |4 4 O|_ointT_INT_1*   |�������� ������ ����������� |0|
C |R_ek          |4 4 O|VY02            |�������� ����������� ���������||
C |R_ik          |4 4 I|tcl_top         |�������� �������|20|
C |C20_om        |3 20 O|task_name3      |������� ��������� ��� ������� ������������ ��������||
C |C20_op        |3 20 O|task_name2      |������� ��������� ��� ������� ������������ ��������||
C |I_ir          |2 4 O|LREADY          |����� ����������||
C |I_as          |2 4 O|LBUSY           |����� �����||
C |R_es          |4 4 S|vminit_button_ST*|��������� ������ "������� � �������� �� ���������" |0.0|
C |R_is          |4 4 I|vminit_button   |������� ������ ������ "������� � �������� �� ���������" |0.0|
C |R_os          |4 4 S|vmwork_button_ST*|��������� ������ "������� � ������� �� ���������" |0.0|
C |R_us          |4 4 I|vmwork_button   |������� ������ ������ "������� � ������� �� ���������" |0.0|
C |L_at          |1 1 I|YA25            |������� ������� �� ����������|F|
C |L_et          |1 1 I|YA26            |������� ������� �� ����������|F|
C |I_it          |2 4 O|state1          |��������� 1||
C |I_av          |2 4 O|state2          |��������� 2||
C |I_ax          |2 4 O|LWORKO          |����� � �������||
C |I_ox          |2 4 O|LINITC          |����� � ��������||
C |C20_obe       |3 20 O|task_state      |���������||
C |C20_ode       |3 20 O|task_name       |������� ���������||
C |C8_ofe        |3 8 I|task            |������� ���������||
C |R_ile         |4 4 S|vmstop_button_ST*|��������� ������ "������� ���� �� ���������" |0.0|
C |R_ole         |4 4 I|vmstop_button   |������� ������ ������ "������� ���� �� ���������" |0.0|
C |L_ule         |1 1 O|vmstart_button_CMD*|[TF]����� ������ ������� �������� ��������� �� ���������|F|
C |R_ame         |4 4 S|vmstart_button_ST*|��������� ������ "������� �������� ��������� �� ���������" |0.0|
C |R_eme         |4 4 I|vmstart_button  |������� ������ ������ "������� �������� ��������� �� ���������" |0.0|
C |I_ime         |2 4 O|LERROR          |����� �������������||
C |L_ape         |1 1 I|stop_avt        |������� ���� �� ����������|F|
C |L_epe         |1 1 O|vmstop_button_CMD*|[TF]����� ������ ������� ���� �� ���������|F|
C |I_ope         |2 4 O|LINIT           |����� ��������||
C |I_ere         |2 4 O|LZM             |������ "�������"||
C |L_ure         |1 1 I|vlv_kvit        |||
C |L_ese         |1 1 I|instr_fault     |||
C |L_ise         |1 1 S|_qffJ1618*      |�������� ������ Q RS-��������  |F|
C |L_use         |1 1 O|vminit_button_CMD*|[TF]����� ������ ������� � �������� �� ���������|F|
C |L_ete         |1 1 O|vmwork_button_CMD*|[TF]����� ������ ������� � ������� �� ���������|F|
C |L_ite         |1 1 I|ulubopen        |���������� ������� �� ���������|F|
C |L_ote         |1 1 I|ulubclose       |���������� ������� �� ���������|F|
C |L_ove         |1 1 O|block           |||
C |L_oxe         |1 1 S|_qff10*         |�������� ������ Q RS-��������  |F|
C |L_abi         |1 1 O|STOP            |�������||
C |L_ibi         |1 1 O|norm            |�����||
C |L_ubi         |1 1 O|nopower         |��� ����������||
C |R8_edi        |4 8 I|voltage         |[��]���������� �� ������||
C |R_afi         |4 4 I|power           |�������� ��������||
C |R8_ofi        |4 8 O|sn_power        |�������� ���� ��|3.0|
C |L_oki         |1 1 O|fault           |�������������||
C |I_uki         |2 4 O|LAM             |������ "�������"||
C |L_emi         |1 1 S|_qff2*          |�������� ������ Q RS-��������  |F|
C |L_omi         |1 1 O|XH53            |�� ������� (���)|F|
C |L_upi         |1 1 S|_qff1*          |�������� ������ Q RS-��������  |F|
C |L_iri         |1 1 O|XH54            |�� ������� (���)|F|
C |L_uri         |1 1 I|mlf24           |��� ���������� � ����������||
C |L_asi         |1 1 I|mlf23           |������� ������� �����||
C |L_esi         |1 1 I|mlf22           |����� ����� ��������||
C |L_isi         |1 1 I|mlf04           |�������� �� ��������||
C |L_osi         |1 1 I|mlf03           |�������� �� ��������||
C |L_usi         |1 1 I|mlf19           |���� ������������||

      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      LOGICAL*1 L_e,L_i
      REAL*4 R_o,R_u,R_ad
      INTEGER*4 I_ed,I0_id,I0_od
      REAL*4 R_ud,R_af,R_ef,R_if
      LOGICAL*1 L0_of,L0_uf
      REAL*4 R0_ak,R_ek,R_ik,R0_ok,R0_uk,R0_al,R0_el
      LOGICAL*1 L0_il,L0_ol
      CHARACTER*20 C20_ul,C20_am,C20_em,C20_im,C20_om,C20_um
     &,C20_ap,C20_ep,C20_ip,C20_op
      INTEGER*4 I0_up,I0_ar
      LOGICAL*1 L0_er
      INTEGER*4 I_ir,I0_or,I0_ur,I_as
      REAL*4 R_es,R_is,R_os,R_us
      LOGICAL*1 L_at,L_et
      INTEGER*4 I_it,I0_ot,I0_ut,I_av,I0_ev,I0_iv,I0_ov,I0_uv
     &,I_ax,I0_ex,I0_ix,I_ox
      CHARACTER*20 C20_ux,C20_abe,C20_ebe,C20_ibe,C20_obe
     &,C20_ube,C20_ade,C20_ede,C20_ide,C20_ode
      CHARACTER*8 C8_ude
      LOGICAL*1 L0_afe
      CHARACTER*8 C8_efe
      LOGICAL*1 L0_ife
      CHARACTER*8 C8_ofe
      INTEGER*4 I0_ufe,I0_ake,I0_eke,I0_ike,I0_oke,I0_uke
     &,I0_ale,I0_ele
      REAL*4 R_ile,R_ole
      LOGICAL*1 L_ule
      REAL*4 R_ame,R_eme
      INTEGER*4 I_ime,I0_ome,I0_ume
      LOGICAL*1 L_ape,L_epe,L0_ipe
      INTEGER*4 I_ope,I0_upe,I0_are,I_ere,I0_ire,I0_ore
      LOGICAL*1 L_ure,L0_ase,L_ese,L_ise,L0_ose,L_use,L0_ate
     &,L_ete,L_ite,L_ote,L0_ute,L0_ave,L0_eve,L0_ive,L_ove
     &,L0_uve,L0_axe,L0_exe,L0_ixe,L_oxe,L0_uxe,L_abi
      LOGICAL*1 L0_ebi,L_ibi,L0_obi,L_ubi
      REAL*4 R0_adi
      REAL*8 R8_edi
      LOGICAL*1 L0_idi,L0_odi
      REAL*4 R0_udi,R_afi,R0_efi,R0_ifi
      REAL*8 R8_ofi
      LOGICAL*1 L0_ufi,L0_aki,L0_eki,L0_iki,L_oki
      INTEGER*4 I_uki,I0_ali,I0_eli
      LOGICAL*1 L0_ili,L0_oli,L0_uli,L0_ami,L_emi,L0_imi,L_omi
     &,L0_umi,L0_api,L0_epi,L0_ipi,L0_opi,L_upi,L0_ari,L0_eri
     &,L_iri,L0_ori,L_uri,L_asi,L_esi,L_isi,L_osi
      LOGICAL*1 L_usi

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      L0_eki = L_et.OR.L_e
C TOLKATEL_OY_HANDLER_dpro.fmg(  68, 229):���
      L0_ufi = L_at.OR.L_i
C TOLKATEL_OY_HANDLER_dpro.fmg(  66, 187):���
      I0_od = z'01000003'
C TOLKATEL_OY_HANDLER_dpro.fmg( 122, 270):��������� ������������� IN (�������)
      I0_id = z'0100000A'
C TOLKATEL_OY_HANDLER_dpro.fmg( 122, 268):��������� ������������� IN (�������)
      R0_ak = 0.0
C TOLKATEL_OY_HANDLER_dpro.fmg( 312, 246):��������� (RE4) (�������)
      R0_el = 0.0
C TOLKATEL_OY_HANDLER_dpro.fmg( 298, 246):��������� (RE4) (�������)
      C20_am = ''
C TOLKATEL_OY_HANDLER_dpro.fmg( 168, 156):��������� ���������� CH20 (CH30) (�������)
      C20_ul = '�������'
C TOLKATEL_OY_HANDLER_dpro.fmg( 168, 154):��������� ���������� CH20 (CH30) (�������)
      C20_em = '������'
C TOLKATEL_OY_HANDLER_dpro.fmg( 189, 157):��������� ���������� CH20 (CH30) (�������)
      C20_ap = ''
C TOLKATEL_OY_HANDLER_dpro.fmg(  74, 158):��������� ���������� CH20 (CH30) (�������)
      C20_um = '� ��������� 2'
C TOLKATEL_OY_HANDLER_dpro.fmg(  74, 156):��������� ���������� CH20 (CH30) (�������)
      C20_ep = '� ��������� 1'
C TOLKATEL_OY_HANDLER_dpro.fmg(  96, 158):��������� ���������� CH20 (CH30) (�������)
      I0_up = z'0100000A'
C TOLKATEL_OY_HANDLER_dpro.fmg( 220, 268):��������� ������������� IN (�������)
      I0_ar = z'01000003'
C TOLKATEL_OY_HANDLER_dpro.fmg( 220, 270):��������� ������������� IN (�������)
      I0_ur = z'0100000A'
C TOLKATEL_OY_HANDLER_dpro.fmg( 220, 290):��������� ������������� IN (�������)
      I0_or = z'01000003'
C TOLKATEL_OY_HANDLER_dpro.fmg( 220, 288):��������� ������������� IN (�������)
      L_use=R_is.ne.R_es
      R_es=R_is
C TOLKATEL_OY_HANDLER_dpro.fmg(  14, 193):���������� ������������� ������
      L_ete=R_us.ne.R_os
      R_os=R_us
C TOLKATEL_OY_HANDLER_dpro.fmg(  19, 249):���������� ������������� ������
      I0_ut = z'01000003'
C TOLKATEL_OY_HANDLER_dpro.fmg( 138, 162):��������� ������������� IN (�������)
      I0_ot = z'01000010'
C TOLKATEL_OY_HANDLER_dpro.fmg( 138, 160):��������� ������������� IN (�������)
      I0_iv = z'01000003'
C TOLKATEL_OY_HANDLER_dpro.fmg( 117, 252):��������� ������������� IN (�������)
      I0_ev = z'01000010'
C TOLKATEL_OY_HANDLER_dpro.fmg( 117, 250):��������� ������������� IN (�������)
      I0_uv = z'01000003'
C TOLKATEL_OY_HANDLER_dpro.fmg( 185, 229):��������� ������������� IN (�������)
      I0_ov = z'0100000A'
C TOLKATEL_OY_HANDLER_dpro.fmg( 185, 227):��������� ������������� IN (�������)
      I0_ix = z'01000003'
C TOLKATEL_OY_HANDLER_dpro.fmg( 193, 179):��������� ������������� IN (�������)
      I0_ex = z'0100000A'
C TOLKATEL_OY_HANDLER_dpro.fmg( 193, 177):��������� ������������� IN (�������)
      I0_are = z'01000003'
C TOLKATEL_OY_HANDLER_dpro.fmg( 115, 175):��������� ������������� IN (�������)
      I0_upe = z'0100000A'
C TOLKATEL_OY_HANDLER_dpro.fmg( 115, 173):��������� ������������� IN (�������)
      C20_ebe = '�������'
C TOLKATEL_OY_HANDLER_dpro.fmg(  29, 160):��������� ���������� CH20 (CH30) (�������)
      C20_ibe = '������� ���'
C TOLKATEL_OY_HANDLER_dpro.fmg(  29, 162):��������� ���������� CH20 (CH30) (�������)
      C20_ux = '��������'
C TOLKATEL_OY_HANDLER_dpro.fmg(  44, 160):��������� ���������� CH20 (CH30) (�������)
      C20_ube = '� ��������'
C TOLKATEL_OY_HANDLER_dpro.fmg(  50, 171):��������� ���������� CH20 (CH30) (�������)
      C20_ede = '� �������'
C TOLKATEL_OY_HANDLER_dpro.fmg(  35, 172):��������� ���������� CH20 (CH30) (�������)
      C20_ide = ''
C TOLKATEL_OY_HANDLER_dpro.fmg(  35, 174):��������� ���������� CH20 (CH30) (�������)
      C8_ude = 'init'
C TOLKATEL_OY_HANDLER_dpro.fmg(  18, 180):��������� ���������� CH8 (�������)
      call chcomp(C8_ofe,C8_ude,L0_afe)
C TOLKATEL_OY_HANDLER_dpro.fmg(  23, 184):���������� ���������
      C8_efe = 'work'
C TOLKATEL_OY_HANDLER_dpro.fmg(  18, 222):��������� ���������� CH8 (�������)
      call chcomp(C8_ofe,C8_efe,L0_ife)
C TOLKATEL_OY_HANDLER_dpro.fmg(  23, 226):���������� ���������
      if(L0_ife) then
         C20_ip=C20_um
      else
         C20_ip=C20_ap
      endif
C TOLKATEL_OY_HANDLER_dpro.fmg(  78, 157):���� RE IN LO CH20
      if(L0_afe) then
         C20_op=C20_ep
      else
         C20_op=C20_ip
      endif
C TOLKATEL_OY_HANDLER_dpro.fmg( 100, 158):���� RE IN LO CH20
      if(L0_ife) then
         C20_im=C20_ul
      else
         C20_im=C20_am
      endif
C TOLKATEL_OY_HANDLER_dpro.fmg( 172, 155):���� RE IN LO CH20
      if(L0_afe) then
         C20_om=C20_em
      else
         C20_om=C20_im
      endif
C TOLKATEL_OY_HANDLER_dpro.fmg( 194, 158):���� RE IN LO CH20
      if(L0_ife) then
         C20_ade=C20_ede
      else
         C20_ade=C20_ide
      endif
C TOLKATEL_OY_HANDLER_dpro.fmg(  39, 172):���� RE IN LO CH20
      if(L0_afe) then
         C20_ode=C20_ube
      else
         C20_ode=C20_ade
      endif
C TOLKATEL_OY_HANDLER_dpro.fmg(  55, 172):���� RE IN LO CH20
      I0_ufe = z'0100008E'
C TOLKATEL_OY_HANDLER_dpro.fmg( 170, 194):��������� ������������� IN (�������)
      I0_ake = z'01000086'
C TOLKATEL_OY_HANDLER_dpro.fmg( 164, 248):��������� ������������� IN (�������)
      I0_ike = z'01000003'
C TOLKATEL_OY_HANDLER_dpro.fmg( 152, 262):��������� ������������� IN (�������)
      I0_oke = z'01000010'
C TOLKATEL_OY_HANDLER_dpro.fmg( 152, 264):��������� ������������� IN (�������)
      I0_ele = z'01000003'
C TOLKATEL_OY_HANDLER_dpro.fmg( 159, 209):��������� ������������� IN (�������)
      I0_ale = z'01000010'
C TOLKATEL_OY_HANDLER_dpro.fmg( 159, 207):��������� ������������� IN (�������)
      L_epe=R_ole.ne.R_ile
      R_ile=R_ole
C TOLKATEL_OY_HANDLER_dpro.fmg(  14, 213):���������� ������������� ������
      L0_ipe = L_epe.OR.L_ape
C TOLKATEL_OY_HANDLER_dpro.fmg(  48, 212):���
      L_ule=R_eme.ne.R_ame
      R_ame=R_eme
C TOLKATEL_OY_HANDLER_dpro.fmg(  19, 236):���������� ������������� ������
      L0_ate = L_ule.AND.L0_ife
C TOLKATEL_OY_HANDLER_dpro.fmg(  30, 234):�
      L0_axe = L_ete.OR.L0_ate
C TOLKATEL_OY_HANDLER_dpro.fmg(  52, 235):���
      L0_ose = L_ule.AND.L0_afe
C TOLKATEL_OY_HANDLER_dpro.fmg(  29, 192):�
      L0_uve = L_use.OR.L0_ose
C TOLKATEL_OY_HANDLER_dpro.fmg(  52, 193):���
      I0_ire = z'0100000E'
C TOLKATEL_OY_HANDLER_dpro.fmg( 189, 200):��������� ������������� IN (�������)
      I0_ome = z'01000007'
C TOLKATEL_OY_HANDLER_dpro.fmg( 150, 180):��������� ������������� IN (�������)
      I0_ume = z'01000003'
C TOLKATEL_OY_HANDLER_dpro.fmg( 150, 182):��������� ������������� IN (�������)
      I0_ali = z'0100000E'
C TOLKATEL_OY_HANDLER_dpro.fmg( 182, 255):��������� ������������� IN (�������)
      L_ise=(L_ese.or.L_ise).and..not.(L_ure)
      L0_ase=.not.L_ise
C TOLKATEL_OY_HANDLER_dpro.fmg( 326, 178):RS �������
      L0_ive=.false.
C TOLKATEL_OY_HANDLER_dpro.fmg(  64, 215):��������� ���������� (�������)
      L0_eve=.false.
C TOLKATEL_OY_HANDLER_dpro.fmg(  64, 213):��������� ���������� (�������)
      L0_obi =.NOT.(L_osi.OR.L_isi.OR.L_usi.OR.L_esi.OR.L_asi.OR.L_uri
     &)
C TOLKATEL_OY_HANDLER_dpro.fmg( 319, 191):���
      L0_ebi =.NOT.(L0_obi)
C TOLKATEL_OY_HANDLER_dpro.fmg( 328, 185):���
      L_oki = L0_ebi.OR.L_ise
C TOLKATEL_OY_HANDLER_dpro.fmg( 332, 184):���
      L0_ute = L_oki.OR.L_ote
C TOLKATEL_OY_HANDLER_dpro.fmg(  55, 201):���
      L0_aki = (.NOT.L0_ute).AND.L0_uve
C TOLKATEL_OY_HANDLER_dpro.fmg(  66, 194):�
      L0_umi = L0_aki.OR.L0_ufi
C TOLKATEL_OY_HANDLER_dpro.fmg(  70, 192):���
      L0_ave = L_oki.OR.L_ite
C TOLKATEL_OY_HANDLER_dpro.fmg(  58, 243):���
      L_ove = L_oki.OR.L0_ive.OR.L0_eve.OR.L0_ave.OR.L0_ute
C TOLKATEL_OY_HANDLER_dpro.fmg(  68, 213):���
      L0_er = L_oki.OR.L_ove
C TOLKATEL_OY_HANDLER_dpro.fmg( 218, 280):���
      if(L0_er) then
         I_ir=I0_or
      else
         I_ir=I0_ur
      endif
C TOLKATEL_OY_HANDLER_dpro.fmg( 224, 288):���� RE IN LO CH7
      if(L_ove) then
         I_as=I0_up
      else
         I_as=I0_ar
      endif
C TOLKATEL_OY_HANDLER_dpro.fmg( 224, 269):���� RE IN LO CH7
      L0_iki = L0_axe.AND.(.NOT.L0_ave)
C TOLKATEL_OY_HANDLER_dpro.fmg(  66, 236):�
      L0_eri = L0_iki.OR.L0_eki
C TOLKATEL_OY_HANDLER_dpro.fmg(  78, 234):���
      L0_ixe = L0_eri.OR.L0_umi
C TOLKATEL_OY_HANDLER_dpro.fmg(  84, 203):���
      if(L_oki) then
         I_ime=I0_ome
      else
         I_ime=I0_ume
      endif
C TOLKATEL_OY_HANDLER_dpro.fmg( 153, 181):���� RE IN LO CH7
      L_ibi = L0_obi.OR.L_ese
C TOLKATEL_OY_HANDLER_dpro.fmg( 328, 190):���
      R0_adi = 0.1
C TOLKATEL_OY_HANDLER_dpro.fmg( 254, 160):��������� (RE4) (�������)
      L_ubi=R8_edi.lt.R0_adi
C TOLKATEL_OY_HANDLER_dpro.fmg( 259, 162):���������� <
      R0_udi = 0.0
C TOLKATEL_OY_HANDLER_dpro.fmg( 266, 182):��������� (RE4) (�������)
      L0_exe = L_omi.OR.L0_ipe.OR.L_iri
C TOLKATEL_OY_HANDLER_dpro.fmg(  60, 207):���
C label 186  try186=try186-1
      L_oxe=(L0_exe.or.L_oxe).and..not.(L0_ixe)
      L0_uxe=.not.L_oxe
C TOLKATEL_OY_HANDLER_dpro.fmg( 106, 205):RS �������,10
      L_abi = L_ape.OR.L_oxe
C TOLKATEL_OY_HANDLER_dpro.fmg( 117, 208):���
      L_iri=R_if.gt.R_ud
C TOLKATEL_OY_HANDLER_dpro.fmg( 351, 239):���������� >
      L0_epi = (.NOT.L_abi).AND.L_iri
C TOLKATEL_OY_HANDLER_dpro.fmg( 105, 260):�
      L0_ili = L_abi.OR.L_uri
C TOLKATEL_OY_HANDLER_dpro.fmg(  98, 214):���
      L0_opi = L_iri.OR.L0_ili.OR.L0_umi
C TOLKATEL_OY_HANDLER_dpro.fmg( 104, 229):���
      L0_ori = (.NOT.L_iri).AND.L0_eri
C TOLKATEL_OY_HANDLER_dpro.fmg( 104, 235):�
      L_upi=(L0_ori.or.L_upi).and..not.(L0_opi)
      L0_ari=.not.L_upi
C TOLKATEL_OY_HANDLER_dpro.fmg( 111, 233):RS �������,1
      L0_ipi = (.NOT.L0_epi).AND.L_upi
C TOLKATEL_OY_HANDLER_dpro.fmg( 130, 236):�
      L0_uf = L0_ipi.AND.(.NOT.L_isi)
C TOLKATEL_OY_HANDLER_dpro.fmg( 268, 245):�
      L0_ol = L0_uf.OR.L_osi
C TOLKATEL_OY_HANDLER_dpro.fmg( 281, 244):���
      if(L0_ol) then
         R0_uk=R_ik
      else
         R0_uk=R0_el
      endif
C TOLKATEL_OY_HANDLER_dpro.fmg( 303, 256):���� RE IN LO CH7
      L0_ami = L0_ili.OR.L_omi.OR.L0_eri
C TOLKATEL_OY_HANDLER_dpro.fmg( 104, 185):���
      L0_api = L0_umi.AND.(.NOT.L_omi)
C TOLKATEL_OY_HANDLER_dpro.fmg( 104, 191):�
      L_emi=(L0_api.or.L_emi).and..not.(L0_ami)
      L0_imi=.not.L_emi
C TOLKATEL_OY_HANDLER_dpro.fmg( 111, 189):RS �������,2
      L0_oli = (.NOT.L_abi).AND.L_omi
C TOLKATEL_OY_HANDLER_dpro.fmg( 103, 166):�
      L0_uli = L_emi.AND.(.NOT.L0_oli)
C TOLKATEL_OY_HANDLER_dpro.fmg( 130, 190):�
      L0_of = L0_uli.AND.(.NOT.L_osi)
C TOLKATEL_OY_HANDLER_dpro.fmg( 268, 231):�
      L0_il = L0_of.OR.L_isi
C TOLKATEL_OY_HANDLER_dpro.fmg( 281, 230):���
      if(L0_il) then
         R0_ok=R_ik
      else
         R0_ok=R0_el
      endif
C TOLKATEL_OY_HANDLER_dpro.fmg( 303, 238):���� RE IN LO CH7
      R0_al = R0_uk + (-R0_ok)
C TOLKATEL_OY_HANDLER_dpro.fmg( 309, 249):��������
      if(L_uri) then
         R_ek=R0_ak
      else
         R_ek=R0_al
      endif
C TOLKATEL_OY_HANDLER_dpro.fmg( 316, 248):���� RE IN LO CH7
      R_if=R_if+deltat/R_u*R_ek
      if(R_if.gt.R_o) then
         R_if=R_o
      elseif(R_if.lt.R_ad) then
         R_if=R_ad
      endif
C TOLKATEL_OY_HANDLER_dpro.fmg( 332, 232):����������,T_INT_1
      L_omi=R_if.lt.R_af
C TOLKATEL_OY_HANDLER_dpro.fmg( 351, 227):���������� <
C sav1=L0_exe
      L0_exe = L_omi.OR.L0_ipe.OR.L_iri
C TOLKATEL_OY_HANDLER_dpro.fmg(  60, 207):recalc:���
C if(sav1.ne.L0_exe .and. try186.gt.0) goto 186
C sav1=L0_ami
      L0_ami = L0_ili.OR.L_omi.OR.L0_eri
C TOLKATEL_OY_HANDLER_dpro.fmg( 104, 185):recalc:���
C if(sav1.ne.L0_ami .and. try242.gt.0) goto 242
C sav1=L0_api
      L0_api = L0_umi.AND.(.NOT.L_omi)
C TOLKATEL_OY_HANDLER_dpro.fmg( 104, 191):recalc:�
C if(sav1.ne.L0_api .and. try244.gt.0) goto 244
C sav1=L0_oli
      L0_oli = (.NOT.L_abi).AND.L_omi
C TOLKATEL_OY_HANDLER_dpro.fmg( 103, 166):recalc:�
C if(sav1.ne.L0_oli .and. try248.gt.0) goto 248
      if(L_omi) then
         I_ope=I0_upe
      else
         I_ope=I0_are
      endif
C TOLKATEL_OY_HANDLER_dpro.fmg( 118, 174):���� RE IN LO CH7
      if(L_omi) then
         I_it=I0_ot
      else
         I_it=I0_ut
      endif
C TOLKATEL_OY_HANDLER_dpro.fmg( 142, 160):���� RE IN LO CH7
      if(L_omi) then
         I0_eke=I0_ike
      else
         I0_eke=I0_oke
      endif
C TOLKATEL_OY_HANDLER_dpro.fmg( 156, 262):���� RE IN LO CH7
      if(L0_ipi) then
         I0_eli=I0_ake
      else
         I0_eli=I0_eke
      endif
C TOLKATEL_OY_HANDLER_dpro.fmg( 168, 262):���� RE IN LO CH7
      if(L_oki) then
         I_uki=I0_ali
      else
         I_uki=I0_eli
      endif
C TOLKATEL_OY_HANDLER_dpro.fmg( 186, 260):���� RE IN LO CH7
      R_ef=R_if
C TOLKATEL_OY_HANDLER_dpro.fmg( 365, 234):������,VY01
      if(L0_uli) then
         I_ox=I0_ex
      else
         I_ox=I0_ix
      endif
C TOLKATEL_OY_HANDLER_dpro.fmg( 196, 178):���� RE IN LO CH7
      L0_idi = L0_ipi.OR.L0_uli
C TOLKATEL_OY_HANDLER_dpro.fmg( 251, 174):���
      L0_odi = L0_idi.AND.(.NOT.L_ubi)
C TOLKATEL_OY_HANDLER_dpro.fmg( 266, 173):�
      if(L0_odi) then
         R0_ifi=R_afi
      else
         R0_ifi=R0_udi
      endif
C TOLKATEL_OY_HANDLER_dpro.fmg( 269, 180):���� RE IN LO CH7
      if(L0_ipi) then
         I_ax=I0_ov
      else
         I_ax=I0_uv
      endif
C TOLKATEL_OY_HANDLER_dpro.fmg( 188, 228):���� RE IN LO CH7
      if(L_iri) then
         I_ed=I0_id
      else
         I_ed=I0_od
      endif
C TOLKATEL_OY_HANDLER_dpro.fmg( 126, 268):���� RE IN LO CH7
      if(L_iri) then
         I_av=I0_ev
      else
         I_av=I0_iv
      endif
C TOLKATEL_OY_HANDLER_dpro.fmg( 120, 250):���� RE IN LO CH7
      if(L_iri) then
         I0_uke=I0_ale
      else
         I0_uke=I0_ele
      endif
C TOLKATEL_OY_HANDLER_dpro.fmg( 162, 208):���� RE IN LO CH7
      if(L0_uli) then
         I0_ore=I0_ufe
      else
         I0_ore=I0_uke
      endif
C TOLKATEL_OY_HANDLER_dpro.fmg( 173, 206):���� RE IN LO CH7
      if(L_oki) then
         I_ere=I0_ire
      else
         I_ere=I0_ore
      endif
C TOLKATEL_OY_HANDLER_dpro.fmg( 192, 206):���� RE IN LO CH7
      if(L_iri) then
         C20_abe=C20_ebe
      else
         C20_abe=C20_ibe
      endif
C TOLKATEL_OY_HANDLER_dpro.fmg(  33, 161):���� RE IN LO CH20
      if(L_omi) then
         C20_obe=C20_ux
      else
         C20_obe=C20_abe
      endif
C TOLKATEL_OY_HANDLER_dpro.fmg(  49, 160):���� RE IN LO CH20
      R0_efi = R8_ofi
C TOLKATEL_OY_HANDLER_dpro.fmg( 264, 190):��������
C label 339  try339=try339-1
      R8_ofi = R0_ifi + R0_efi
C TOLKATEL_OY_HANDLER_dpro.fmg( 275, 189):��������
C sav1=R0_efi
      R0_efi = R8_ofi
C TOLKATEL_OY_HANDLER_dpro.fmg( 264, 190):recalc:��������
C if(sav1.ne.R0_efi .and. try339.gt.0) goto 339
      End
