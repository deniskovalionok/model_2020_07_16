      Subroutine trans_pH(ext_deltat,R8_o,R8_af,R8_ok)
C |R8_o          |4 8 I|Ww02            |||
C |R8_af         |4 8 O|Out             |||
C |R8_ok         |4 8 I|Ww01            |||

      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      REAL*4 R0_e,R0_i
      REAL*8 R8_o
      REAL*4 R0_u,R0_ad,R0_ed,R0_id,R0_od,R0_ud
      REAL*8 R8_af
      REAL*4 R0_ef,R0_if,R0_of,R0_uf,R0_ak,R0_ek,R0_ik
      REAL*8 R8_ok

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      R0_e = 1
C trans_pH.fmg( 235, 222):��������� (RE4) (�������)
      R0_ad = R0_e * R8_ok
C trans_pH.fmg( 238, 222):����������
      R0_i = 1
C trans_pH.fmg( 235, 227):��������� (RE4) (�������)
      R0_ik = R8_o * R0_i
C trans_pH.fmg( 238, 229):����������
      R0_u = 1e-7
C trans_pH.fmg( 245, 219):��������� (RE4) (�������)
      R0_ed=MAX(R0_ad,R0_u)
C trans_pH.fmg( 250, 221):��������
      if(R0_ed.gt.1.0E-34) then
         R0_of=log10(R0_ed)
      else
         R0_of=-34
      endif
C trans_pH.fmg( 260, 221):�������� ����������
      R0_id = 1e-7
C trans_pH.fmg( 245, 226):��������� (RE4) (�������)
      R0_od=MAX(R0_ik,R0_id)
C trans_pH.fmg( 250, 228):��������
      if(R0_od.gt.1.0E-34) then
         R0_ak=log10(R0_od)
      else
         R0_ak=-34
      endif
C trans_pH.fmg( 260, 228):�������� ����������
      R0_ud = 0
C trans_pH.fmg( 283, 221):��������� (RE4) (�������)
      R0_ef = 14
C trans_pH.fmg( 283, 227):��������� (RE4) (�������)
      R0_uf = 7
C trans_pH.fmg( 269, 225):��������� (RE4) (�������)
      R0_ek = (-R0_ak) + R0_uf + R0_of
C trans_pH.fmg( 272, 226):��������
      R0_if=MIN(R0_ef,R0_ek)
C trans_pH.fmg( 288, 227):�������
      R8_af=MAX(R0_if,R0_ud)
C trans_pH.fmg( 288, 223):��������
      End
