      Subroutine BVALVE_MAN(ext_deltat,R_odi,L_edi,L_ixe,R8_ere
     &,L_e,L_ad,L_ed,I_af,I_ef,R_uf,R_ak,R_ek,R_ik,R_ok,R_uk
     &,I_al,I_ol,I_em,I_ap,L_ep,L_op,L_up,L_ar,L_er,L_or,L_ur
     &,L_os,L_us,L_it,L_ot,L_av,L_ev,L_ov,R8_ax,R_ux,R8_ibe
     &,L_obe,L_ade,L_ope,R_ore,R_ase,L_axe,L_obi,L_udi,L_afi
     &,L_efi,L_ifi,L_ofi,L_ufi)
C |R_odi         |4 4 I|11 mlfpar19     |��������� ������������|0.6|
C |L_edi         |1 1 O|13 cbo          |�� ������� (���)|F|
C |L_ixe         |1 1 O|14 cbc          |�� ������� (���)|F|
C |R8_ere        |4 8 O|16 value        |��� �������� ��������|0.5|
C |L_e           |1 1 I|tech_mode       |||
C |L_ad          |1 1 I|ruch_mode       |||
C |L_ed          |1 1 I|avt_mode        |||
C |I_af          |2 4 O|LM              |�����||
C |I_ef          |2 4 O|LF              |����� �������������||
C |R_uf          |4 4 S|vmstop_button_ST*|��������� ������ "������� ���� �� ���������" |0.0|
C |R_ak          |4 4 I|vmstop_button   |������� ������ ������ "������� ���� �� ���������" |0.0|
C |R_ek          |4 4 S|vmclose_button_ST*|��������� ������ "������� ������� �� ���������" |0.0|
C |R_ik          |4 4 I|vmclose_button  |������� ������ ������ "������� ������� �� ���������" |0.0|
C |R_ok          |4 4 S|vmopen_button_ST*|��������� ������ "������� ������� �� ���������" |0.0|
C |R_uk          |4 4 I|vmopen_button   |������� ������ ������ "������� ������� �� ���������" |0.0|
C |I_al          |2 4 O|LST             |����� ����||
C |I_ol          |2 4 O|LCL             |����� �������||
C |I_em          |2 4 O|LOP             |����� �������||
C |I_ap          |2 4 O|LS              |��������� �������||
C |L_ep          |1 1 I|vlv_kvit        |||
C |L_op          |1 1 I|instr_fault     |||
C |L_up          |1 1 S|_qffJ453*       |�������� ������ Q RS-��������  |F|
C |L_ar          |1 1 O|vmclose_button_CMD*|[TF]����� ������ ������� ������� �� ���������|F|
C |L_er          |1 1 O|vmopen_button_CMD*|[TF]����� ������ ������� ������� �� ���������|F|
C |L_or          |1 1 I|ulubclose       |���������� ������� �� ���������|F|
C |L_ur          |1 1 I|ulubopen        |���������� ������� �� ���������|F|
C |L_os          |1 1 O|block           |||
C |L_us          |1 1 O|vmstop_button_CMD*|[TF]����� ������ ������� ���� �� ���������|F|
C |L_it          |1 1 O|STOP            |�������||
C |L_ot          |1 1 S|_qff10*         |�������� ������ Q RS-��������  |F|
C |L_av          |1 1 O|fault           |�������������||
C |L_ev          |1 1 O|norm            |�����||
C |L_ov          |1 1 O|nopower         |��� ����������||
C |R8_ax         |4 8 I|voltage         |[��]���������� �� ������||
C |R_ux          |4 4 I|power           |�������� ��������||
C |R8_ibe        |4 8 O|sn_power        |�������� ���� ��|3.0|
C |L_obe         |1 1 I|uluclose        |������� ������� �� ����������|F|
C |L_ade         |1 1 I|uluopen         |������� ������� �� ����������|F|
C |L_ope         |1 1 S|_qff6*          |�������� ������ Q RS-��������  |F|
C |R_ore         |4 4 O|mf_value        |��� ��������� �����|0.5|
C |R_ase         |4 4 I|tcl_top         |����� ����|30.0|
C |L_axe         |1 1 S|_qff2*          |�������� ������ Q RS-��������  |F|
C |L_obi         |1 1 S|_qff1*          |�������� ������ Q RS-��������  |F|
C |L_udi         |1 1 I|mlf24           |��� ���������� � ����������||
C |L_afi         |1 1 I|mlf23           |������� ������� �����||
C |L_efi         |1 1 I|mlf22           |����� ����� ��������||
C |L_ifi         |1 1 I|mlf04           |�������� �� ��������||
C |L_ofi         |1 1 I|mlf03           |�������� �� ��������||
C |L_ufi         |1 1 I|mlf19           |���� ������������||

      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      LOGICAL*1 L_e
      INTEGER*4 I0_i,I0_o,I0_u
      LOGICAL*1 L_ad,L_ed
      INTEGER*4 I0_id,I0_od,I0_ud,I_af,I_ef,I0_if,I0_of
      REAL*4 R_uf,R_ak,R_ek,R_ik,R_ok,R_uk
      INTEGER*4 I_al,I0_el,I0_il,I_ol,I0_ul,I0_am,I_em,I0_im
     &,I0_om,I0_um,I_ap
      LOGICAL*1 L_ep,L0_ip,L_op,L_up,L_ar,L_er
      REAL*4 R0_ir
      LOGICAL*1 L_or,L_ur,L0_as,L0_es,L0_is,L_os,L_us,L0_at
     &,L0_et,L_it,L_ot,L0_ut,L_av,L_ev,L0_iv,L_ov
      REAL*4 R0_uv
      REAL*8 R8_ax
      LOGICAL*1 L0_ex,L0_ix
      REAL*4 R0_ox,R_ux,R0_abe,R0_ebe
      REAL*8 R8_ibe
      LOGICAL*1 L_obe,L0_ube,L_ade,L0_ede
      INTEGER*4 I0_ide,I0_ode,I0_ude,I0_afe,I0_efe,I0_ife
     &,I0_ofe,I0_ufe,I0_ake
      LOGICAL*1 L0_eke,L0_ike
      REAL*4 R0_oke,R0_uke
      LOGICAL*1 L0_ale
      REAL*4 R0_ele,R0_ile,R0_ole,R0_ule,R0_ame,R0_eme
      LOGICAL*1 L0_ime
      REAL*4 R0_ome,R0_ume,R0_ape,R0_epe
      LOGICAL*1 L0_ipe,L_ope
      REAL*4 R0_upe,R0_are
      REAL*8 R8_ere
      REAL*4 R0_ire,R_ore,R0_ure,R_ase,R0_ese,R0_ise,R0_ose
     &,R0_use,R0_ate,R0_ete,R0_ite,R0_ote
      LOGICAL*1 L0_ute,L0_ave,L0_eve,L0_ive,L0_ove,L0_uve
     &,L_axe,L0_exe,L_ixe,L0_oxe,L0_uxe,L0_abi,L0_ebi,L0_ibi
     &,L_obi,L0_ubi,L0_adi,L_edi,L0_idi
      REAL*4 R_odi
      LOGICAL*1 L_udi,L_afi,L_efi,L_ifi,L_ofi,L_ufi

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      I0_i = z'01000010'
C BVALVE_MAN.fmg( 201, 160):��������� ������������� IN (�������)
      I0_od = z'01000022'
C BVALVE_MAN.fmg( 163, 162):��������� ������������� IN (�������)
      I0_u = z'0100000A'
C BVALVE_MAN.fmg( 185, 161):��������� ������������� IN (�������)
      I0_ud = z'0100000A'
C BVALVE_MAN.fmg( 163, 164):��������� ������������� IN (�������)
      if(L_ed) then
         I0_id=I0_od
      else
         I0_id=I0_ud
      endif
C BVALVE_MAN.fmg( 170, 162):���� RE IN LO CH7
      if(L_ad) then
         I0_o=I0_u
      else
         I0_o=I0_id
      endif
C BVALVE_MAN.fmg( 188, 162):���� RE IN LO CH7
      if(L_e) then
         I_af=I0_i
      else
         I_af=I0_o
      endif
C BVALVE_MAN.fmg( 204, 160):���� RE IN LO CH7
      I0_if = z'01000007'
C BVALVE_MAN.fmg( 148, 180):��������� ������������� IN (�������)
      I0_of = z'01000003'
C BVALVE_MAN.fmg( 148, 182):��������� ������������� IN (�������)
      L_us=R_ak.ne.R_uf
      R_uf=R_ak
C BVALVE_MAN.fmg(  22, 208):���������� ������������� ������
      L_ar=R_ik.ne.R_ek
      R_ek=R_ik
C BVALVE_MAN.fmg(  20, 194):���������� ������������� ������
      L0_ube = (.NOT.L_or).AND.L_ar
C BVALVE_MAN.fmg(  65, 194):�
      L0_oxe = L0_ube.OR.L_obe
C BVALVE_MAN.fmg(  69, 192):���
      L_er=R_uk.ne.R_ok
      R_ok=R_uk
C BVALVE_MAN.fmg(  22, 238):���������� ������������� ������
      L0_ede = L_er.AND.(.NOT.L_ur)
C BVALVE_MAN.fmg(  65, 236):�
      L0_adi = L0_ede.OR.L_ade
C BVALVE_MAN.fmg(  69, 234):���
      L0_at = L0_adi.OR.L0_oxe
C BVALVE_MAN.fmg(  74, 203):���
      L_ot=(L_us.or.L_ot).and..not.(L0_at)
      L0_et=.not.L_ot
C BVALVE_MAN.fmg( 104, 205):RS �������,10
      L_it=L_ot
C BVALVE_MAN.fmg( 132, 207):������,STOP
      L0_eve = L_ot.OR.L_udi
C BVALVE_MAN.fmg(  96, 214):���
      I0_el = z'01000007'
C BVALVE_MAN.fmg( 112, 214):��������� ������������� IN (�������)
      I0_il = z'01000003'
C BVALVE_MAN.fmg( 112, 216):��������� ������������� IN (�������)
      if(L_ot) then
         I_al=I0_el
      else
         I_al=I0_il
      endif
C BVALVE_MAN.fmg( 116, 214):���� RE IN LO CH7
      I0_ul = z'01000010'
C BVALVE_MAN.fmg( 114, 169):��������� ������������� IN (�������)
      I0_am = z'01000003'
C BVALVE_MAN.fmg( 114, 171):��������� ������������� IN (�������)
      I0_om = z'01000003'
C BVALVE_MAN.fmg( 120, 265):��������� ������������� IN (�������)
      I0_im = z'0100000A'
C BVALVE_MAN.fmg( 120, 263):��������� ������������� IN (�������)
      I0_um = z'01000097'
C BVALVE_MAN.fmg( 188, 200):��������� ������������� IN (�������)
      I0_ife = z'0100000A'
C BVALVE_MAN.fmg( 152, 264):��������� ������������� IN (�������)
      I0_efe = z'01000010'
C BVALVE_MAN.fmg( 152, 262):��������� ������������� IN (�������)
      I0_ide = z'0100000A'
C BVALVE_MAN.fmg( 152, 207):��������� ������������� IN (�������)
      I0_ufe = z'01000087'
C BVALVE_MAN.fmg( 166, 256):��������� ������������� IN (�������)
      I0_ude = z'01000086'
C BVALVE_MAN.fmg( 166, 201):��������� ������������� IN (�������)
      L_up=(L_op.or.L_up).and..not.(L_ep)
      L0_ip=.not.L_up
C BVALVE_MAN.fmg( 326, 178):RS �������
      L0_is=.false.
C BVALVE_MAN.fmg(  63, 217):��������� ���������� (�������)
      L0_es=.false.
C BVALVE_MAN.fmg(  63, 215):��������� ���������� (�������)
      L0_as=.false.
C BVALVE_MAN.fmg(  63, 213):��������� ���������� (�������)
      L_os = L0_is.OR.L0_es.OR.L0_as.OR.L_ur.OR.L_or
C BVALVE_MAN.fmg(  67, 213):���
      R0_ir = DeltaT
C BVALVE_MAN.fmg( 250, 254):��������� (RE4) (�������)
      if(R_ase.ge.0.0) then
         R0_ose=R0_ir/max(R_ase,1.0e-10)
      else
         R0_ose=R0_ir/min(R_ase,-1.0e-10)
      endif
C BVALVE_MAN.fmg( 259, 252):�������� ����������
      L0_iv =.NOT.(L_ofi.OR.L_ifi.OR.L_ufi.OR.L_efi.OR.L_afi.OR.L_udi
     &)
C BVALVE_MAN.fmg( 319, 191):���
      L0_ut =.NOT.(L0_iv)
C BVALVE_MAN.fmg( 328, 185):���
      L_av = L0_ut.OR.L_up
C BVALVE_MAN.fmg( 332, 184):���
      if(L_av) then
         I_ef=I0_if
      else
         I_ef=I0_of
      endif
C BVALVE_MAN.fmg( 152, 181):���� RE IN LO CH7
      L_ev = L0_iv.OR.L_op
C BVALVE_MAN.fmg( 328, 190):���
      R0_uv = 0.1
C BVALVE_MAN.fmg( 254, 160):��������� (RE4) (�������)
      L_ov=R8_ax.lt.R0_uv
C BVALVE_MAN.fmg( 259, 162):���������� <
      R0_ox = 0.0
C BVALVE_MAN.fmg( 266, 182):��������� (RE4) (�������)
      R0_ame = 0.000001
C BVALVE_MAN.fmg( 354, 208):��������� (RE4) (�������)
      R0_eme = 0.999999
C BVALVE_MAN.fmg( 354, 224):��������� (RE4) (�������)
      R0_oke = 0.0
C BVALVE_MAN.fmg( 296, 244):��������� (RE4) (�������)
      R0_uke = 0.0
C BVALVE_MAN.fmg( 370, 242):��������� (RE4) (�������)
      L0_ale = L_afi.OR.L_efi
C BVALVE_MAN.fmg( 363, 237):���
      R0_ile = 1.0
C BVALVE_MAN.fmg( 348, 252):��������� (RE4) (�������)
      R0_ole = 0.0
C BVALVE_MAN.fmg( 340, 252):��������� (RE4) (�������)
      R0_ume = 1.0e-10
C BVALVE_MAN.fmg( 318, 228):��������� (RE4) (�������)
      R0_upe = 0.0
C BVALVE_MAN.fmg( 328, 242):��������� (RE4) (�������)
      R0_ese = DeltaT
C BVALVE_MAN.fmg( 250, 264):��������� (RE4) (�������)
      if(R_ase.ge.0.0) then
         R0_use=R0_ese/max(R_ase,1.0e-10)
      else
         R0_use=R0_ese/min(R_ase,-1.0e-10)
      endif
C BVALVE_MAN.fmg( 259, 262):�������� ����������
      R0_ote = 0.0
C BVALVE_MAN.fmg( 282, 244):��������� (RE4) (�������)
      L0_ive = (.NOT.L_it).AND.L_ixe
C BVALVE_MAN.fmg( 102, 166):�
C label 131  try131=try131-1
      if(L_efi) then
         R0_ele=R0_uke
      else
         R0_ele=R8_ere
      endif
C BVALVE_MAN.fmg( 373, 242):���� RE IN LO CH7
      if(.NOT.L0_ale) then
         R_ore=R8_ere
      endif
C BVALVE_MAN.fmg( 384, 252):���� � ������������� �������
      L0_abi = (.NOT.L_it).AND.L_edi
C BVALVE_MAN.fmg( 104, 260):�
      L0_ibi = L_edi.OR.L0_eve.OR.L0_oxe
C BVALVE_MAN.fmg( 102, 229):���
      L0_idi = (.NOT.L_edi).AND.L0_adi
C BVALVE_MAN.fmg( 102, 235):�
      L_obi=(L0_idi.or.L_obi).and..not.(L0_ibi)
      L0_ubi=.not.L_obi
C BVALVE_MAN.fmg( 110, 233):RS �������,1
      L0_ebi = (.NOT.L0_abi).AND.L_obi
C BVALVE_MAN.fmg( 128, 236):�
      L0_ike = L0_ebi.AND.(.NOT.L_ifi)
C BVALVE_MAN.fmg( 252, 242):�
      L0_ave = L0_ike.OR.L_ofi
C BVALVE_MAN.fmg( 265, 241):���
      if(L0_ave) then
         R0_ete=R0_use
      else
         R0_ete=R0_ote
      endif
C BVALVE_MAN.fmg( 287, 254):���� RE IN LO CH7
      L0_uve = L0_eve.OR.L_ixe.OR.L0_adi
C BVALVE_MAN.fmg( 102, 185):���
      L0_uxe = L0_oxe.AND.(.NOT.L_ixe)
C BVALVE_MAN.fmg( 102, 191):�
      L_axe=(L0_uxe.or.L_axe).and..not.(L0_uve)
      L0_exe=.not.L_axe
C BVALVE_MAN.fmg( 110, 189):RS �������,2
      L0_ove = L_axe.AND.(.NOT.L0_ive)
C BVALVE_MAN.fmg( 128, 190):�
      L0_eke = L0_ove.AND.(.NOT.L_ofi)
C BVALVE_MAN.fmg( 252, 228):�
      L0_ute = L0_eke.OR.L_ifi
C BVALVE_MAN.fmg( 265, 227):���
      if(L0_ute) then
         R0_ate=R0_ose
      else
         R0_ate=R0_ote
      endif
C BVALVE_MAN.fmg( 287, 236):���� RE IN LO CH7
      R0_ite = R0_ete + (-R0_ate)
C BVALVE_MAN.fmg( 293, 246):��������
      if(L_udi) then
         R0_are=R0_oke
      else
         R0_are=R0_ite
      endif
C BVALVE_MAN.fmg( 300, 244):���� RE IN LO CH7
      R0_epe = R_ore + (-R_odi)
C BVALVE_MAN.fmg( 308, 235):��������
      R0_ome = R0_are + R0_epe
C BVALVE_MAN.fmg( 314, 236):��������
      R0_ape = R0_ome * R0_epe
C BVALVE_MAN.fmg( 318, 235):����������
      L0_ipe=R0_ape.lt.R0_ume
C BVALVE_MAN.fmg( 323, 234):���������� <
      L_ope=(L0_ipe.or.L_ope).and..not.(.NOT.L_ufi)
      L0_ime=.not.L_ope
C BVALVE_MAN.fmg( 330, 232):RS �������,6
      if(L_ope) then
         R_ore=R_odi
      endif
C BVALVE_MAN.fmg( 324, 252):���� � ������������� �������
      if(L_ope) then
         R0_ire=R0_upe
      else
         R0_ire=R0_are
      endif
C BVALVE_MAN.fmg( 332, 244):���� RE IN LO CH7
      R0_ure = R_ore + R0_ire
C BVALVE_MAN.fmg( 338, 245):��������
      R0_ule=MAX(R0_ole,R0_ure)
C BVALVE_MAN.fmg( 346, 246):��������
      R0_ise=MIN(R0_ile,R0_ule)
C BVALVE_MAN.fmg( 354, 247):�������
      L_ixe=R0_ise.lt.R0_ame
C BVALVE_MAN.fmg( 363, 210):���������� <
C sav1=L0_uve
      L0_uve = L0_eve.OR.L_ixe.OR.L0_adi
C BVALVE_MAN.fmg( 102, 185):recalc:���
C if(sav1.ne.L0_uve .and. try171.gt.0) goto 171
C sav1=L0_uxe
      L0_uxe = L0_oxe.AND.(.NOT.L_ixe)
C BVALVE_MAN.fmg( 102, 191):recalc:�
C if(sav1.ne.L0_uxe .and. try173.gt.0) goto 173
C sav1=L0_ive
      L0_ive = (.NOT.L_it).AND.L_ixe
C BVALVE_MAN.fmg( 102, 166):recalc:�
C if(sav1.ne.L0_ive .and. try131.gt.0) goto 131
      if(L0_ale) then
         R8_ere=R0_ele
      else
         R8_ere=R0_ise
      endif
C BVALVE_MAN.fmg( 377, 246):���� RE IN LO CH7
      L_edi=R0_ise.gt.R0_eme
C BVALVE_MAN.fmg( 363, 225):���������� >
C sav1=L0_ibi
      L0_ibi = L_edi.OR.L0_eve.OR.L0_oxe
C BVALVE_MAN.fmg( 102, 229):recalc:���
C if(sav1.ne.L0_ibi .and. try151.gt.0) goto 151
C sav1=L0_idi
      L0_idi = (.NOT.L_edi).AND.L0_adi
C BVALVE_MAN.fmg( 102, 235):recalc:�
C if(sav1.ne.L0_idi .and. try153.gt.0) goto 153
C sav1=L0_abi
      L0_abi = (.NOT.L_it).AND.L_edi
C BVALVE_MAN.fmg( 104, 260):recalc:�
C if(sav1.ne.L0_abi .and. try146.gt.0) goto 146
      if(L0_ale) then
         R_ore=R0_ise
      endif
C BVALVE_MAN.fmg( 366, 252):���� � ������������� �������
C sav1=R0_epe
      R0_epe = R_ore + (-R_odi)
C BVALVE_MAN.fmg( 308, 235):recalc:��������
C if(sav1.ne.R0_epe .and. try196.gt.0) goto 196
C sav1=R0_ure
      R0_ure = R_ore + R0_ire
C BVALVE_MAN.fmg( 338, 245):recalc:��������
C if(sav1.ne.R0_ure .and. try213.gt.0) goto 213
      L0_ex = L0_ebi.OR.L0_ove
C BVALVE_MAN.fmg( 251, 174):���
      L0_ix = L0_ex.AND.(.NOT.L_ov)
C BVALVE_MAN.fmg( 266, 173):�
      if(L0_ix) then
         R0_ebe=R_ux
      else
         R0_ebe=R0_ox
      endif
C BVALVE_MAN.fmg( 269, 180):���� RE IN LO CH7
      if(L0_abi) then
         I_em=I0_im
      else
         I_em=I0_om
      endif
C BVALVE_MAN.fmg( 124, 264):���� RE IN LO CH7
      if(L0_ive) then
         I_ol=I0_ul
      else
         I_ol=I0_am
      endif
C BVALVE_MAN.fmg( 117, 170):���� RE IN LO CH7
      if(L0_ive) then
         I0_ake=I0_efe
      else
         I0_ake=I0_ife
      endif
C BVALVE_MAN.fmg( 155, 262):���� RE IN LO CH7
      if(L0_ebi) then
         I0_ofe=I0_ufe
      else
         I0_ofe=I0_ake
      endif
C BVALVE_MAN.fmg( 170, 262):���� RE IN LO CH7
      if(L0_abi) then
         I0_afe=I0_ide
      else
         I0_afe=I0_ofe
      endif
C BVALVE_MAN.fmg( 155, 208):���� RE IN LO CH7
      if(L0_ove) then
         I0_ode=I0_ude
      else
         I0_ode=I0_afe
      endif
C BVALVE_MAN.fmg( 170, 206):���� RE IN LO CH7
      if(L_av) then
         I_ap=I0_um
      else
         I_ap=I0_ode
      endif
C BVALVE_MAN.fmg( 191, 206):���� RE IN LO CH7
      R0_abe = R8_ibe
C BVALVE_MAN.fmg( 264, 190):��������
C label 269  try269=try269-1
      R8_ibe = R0_ebe + R0_abe
C BVALVE_MAN.fmg( 275, 189):��������
C sav1=R0_abe
      R0_abe = R8_ibe
C BVALVE_MAN.fmg( 264, 190):recalc:��������
C if(sav1.ne.R0_abe .and. try269.gt.0) goto 269
      End
