      Interface
      Subroutine DAT_ANA_PATM_HANDLER(ext_deltat,R_e,R_i,R_o
     &,R_u,R_od,R_ul,R_em,R_um,R_ep,I_er,R_us,L_at,R_it,L_ot
     &,L_ut,R_ev,R_abe,R_ibe,L_ube)
C |R_e           |4 4 I|ATMOSPHERE_ABS  |���. ��������||
C |R_i           |4 4 I|input           |����||
C |R_o           |4 4 O|nom             |||
C |R_u           |4 4 K|_cJ1739         |�������� ��������� ����������|0.3|
C |R_od          |4 4 I|koeff_units     |����������� �����������||
C |R_ul          |4 4 I|LOWER_warning_setpoint|||
C |R_em          |4 4 I|LOWER_alarm_setpoint|||
C |R_um          |4 4 I|UPPER_alarm_setpoint|||
C |R_ep          |4 4 I|UPPER_warning_setpoint|||
C |I_er          |2 4 O|dat_color       |���� ��������||
C |R_us          |4 4 I|GM05V           |���������� ����������� �������||
C |L_at          |1 1 I|GM05            |���������� ����������� �������||
C |R_it          |4 4 I|GM04V           |������ �������� ���������� ��������||
C |L_ot          |1 1 I|GM04            |������ �������� ���������� ��������||
C |L_ut          |1 1 I|GM02            |����� �� ������������ ������ ���������||
C |R_ev          |4 4 O|KKS             |�����||
C |R_abe         |4 4 I|MAXIMUM         |||
C |R_ibe         |4 4 I|MINIMUM         |||
C |L_ube         |1 1 I|GM01            |����� �� ����������� ������ ���������||

      IMPLICIT NONE
      REAL*4 ext_deltat
      REAL*4 R_e,R_i,R_o,R_u,R_od,R_ul,R_em,R_um,R_ep
      INTEGER*4 I_er
      REAL*4 R_us
      LOGICAL*1 L_at
      REAL*4 R_it
      LOGICAL*1 L_ot,L_ut
      REAL*4 R_ev,R_abe,R_ibe
      LOGICAL*1 L_ube
      End subroutine DAT_ANA_PATM_HANDLER
      End interface
