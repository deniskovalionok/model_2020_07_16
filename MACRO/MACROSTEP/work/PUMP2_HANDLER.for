      Subroutine PUMP2_HANDLER(ext_deltat,I_o,I_u,R_id,R_od
     &,R_ud,R_af,L_al,L_el,L_il,L_ol,L_ul,L_am,L_em,L_im,L_om
     &,L_um,L_ap,L_ep,L_ip,L_op,L_up,L_ir,L_as,L_es,L_is,I_et
     &,R_av,R_iv,L_ov,L_ex,L_ix,R8_ebe,L_ibe,R8_obe,R_afe
     &,R_ofe,R_ike,R8_oke,R_ale,R8_ele,R_ule,R8_ame,R_ime
     &,R_ome)
C |I_o           |2 4 O|LOFF            |����� ��������||
C |I_u           |2 4 O|LON             |����� �������||
C |R_id          |4 4 S|turn_off_button_ST*|��������� ������ "������� ������� �� ���������" |0.0|
C |R_od          |4 4 I|turn_off_button |������� ������ ������ "������� ������� �� ���������" |0.0|
C |R_ud          |4 4 S|turn_on_button_ST*|��������� ������ "������� �������� �� ���������" |0.0|
C |R_af          |4 4 I|turn_on_button  |������� ������ ������ "������� �������� �� ���������" |0.0|
C |L_al          |1 1 O|block           |||
C |L_el          |1 1 O|fault           |�������������||
C |L_il          |1 1 S|_qff156*        |�������� ������ Q RS-��������  |F|
C |L_ol          |1 1 S|_qff155*        |�������� ������ Q RS-��������  |F|
C |L_ul          |1 1 O|norm            |�����||
C |L_am          |1 1 I|gm14            |������������� ������� ���������||
C |L_em          |1 1 I|gm12            |������������� ������� ��������||
C |L_im          |1 1 I|gm13            |���������������� ����������||
C |L_om          |1 1 I|gm11            |���������������� ���������||
C |L_um          |1 1 O|cb_out          |��������� �������� ����.||
C |L_ap          |1 1 I|uluboff         |���������� ���������� �� ���������|F|
C |L_ep          |1 1 O|turn_off_button_CMD*|[TF]����� ������ ������� ������� �� ���������|F|
C |L_ip          |1 1 I|ulubon          |���������� ��������� �� ���������|F|
C |L_op          |1 1 O|turn_on_button_CMD*|[TF]����� ������ ������� �������� �� ���������|F|
C |L_up          |1 1 S|_qffJ1545*      |�������� ������ Q RS-��������  |F|
C |L_ir          |1 1 S|_qffJ1541*      |�������� ������ Q RS-��������  |F|
C |L_as          |1 1 I|uluoff          |������� ��������� �� ����������|F|
C |L_es          |1 1 I|uluon           |������� �������� �� ����������|F|
C |L_is          |1 1 O|lstate          |��������� ����������� ���������||
C |I_et          |2 4 O|LBM             |||
C |R_av          |4 4 K|_tdel1          |[]�������� ������� �������� ������|2|
C |R_iv          |4 4 S|_sdel1*         |[���]���������� ��������� �������� ������ |0.0|
C |L_ov          |1 1 S|_idel1*         |[TF]���������� ��������� �������� ������ |f|
C |L_ex          |1 1 I|block_ele       |���������� ������������� �������||
C |L_ix          |1 1 S|_qff3*          |�������� ������ Q RS-��������  |F|
C |R8_ebe        |4 8 I|voltage         |[��]���������� �� ������||
C |L_ibe         |1 1 I|el_use          |����� ������||
C |R8_obe        |4 8 I|estate          |������������� �������� EN_CAD|0.0|
C |R_afe         |4 4 O|_oapr5*         |�������� ������ ���������. ����� |0.0|
C |R_ofe         |4 4 I|kratn           |��������� ��������� ����|3.0|
C |R_ike         |4 4 O|idvig           |[�] ��� ��������� |3.0|
C |R8_oke        |4 8 I|pnagr           |[��] �������� �������� ||
C |R_ale         |4 4 O|power           |�������� ���������|3.0|
C |R8_ele        |4 8 O|sn_power        |�������� ���� ��|3.0|
C |R_ule         |4 4 O|_oapr1*         |�������� ������ ���������. ����� |0.0|
C |R8_ame        |4 8 O|fizstate        |������������� ��������|0.0|
C |R_ime         |4 4 I|timeon          |���������� ������� ���������|3.0|
C |R_ome         |4 4 I|timeoff         |���������� ������� ����������|3.0|

      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      INTEGER*4 I0_e,I0_i,I_o,I_u,I0_ad,I0_ed
      REAL*4 R_id,R_od,R_ud,R_af
      INTEGER*4 I0_ef
      LOGICAL*1 L0_if,L0_of,L0_uf,L0_ak,L0_ek,L0_ik,L0_ok
     &,L0_uk,L_al,L_el,L_il,L_ol,L_ul,L_am,L_em,L_im,L_om
     &,L_um,L_ap,L_ep,L_ip,L_op
      LOGICAL*1 L_up,L0_ar,L0_er,L_ir,L0_or,L0_ur,L_as,L_es
     &,L_is,L0_os,L0_us,L0_at
      INTEGER*4 I_et,I0_it,I0_ot,I0_ut
      REAL*4 R_av,R0_ev,R_iv
      LOGICAL*1 L_ov,L0_uv,L0_ax,L_ex,L_ix
      REAL*4 R0_ox
      LOGICAL*1 L0_ux
      REAL*4 R0_abe
      REAL*8 R8_ebe
      LOGICAL*1 L_ibe
      REAL*8 R8_obe
      LOGICAL*1 L0_ube
      REAL*4 R0_ade,R0_ede,R0_ide,R0_ode
      LOGICAL*1 L0_ude
      REAL*4 R_afe,R0_efe,R0_ife,R_ofe,R0_ufe,R0_ake,R0_eke
     &,R_ike
      REAL*8 R8_oke
      REAL*4 R0_uke,R_ale
      REAL*8 R8_ele
      LOGICAL*1 L0_ile
      REAL*4 R0_ole,R_ule
      REAL*8 R8_ame
      REAL*4 R0_eme,R_ime,R_ome
      LOGICAL*1 L0_ume
      REAL*4 R0_ape,R0_epe,R0_ipe

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      R0_efe=R_afe
C PUMP2_HANDLER.fmg( 310, 202):pre: �������������� �����  ,5
      R0_ole=R_ule
C PUMP2_HANDLER.fmg( 248, 249):pre: �������������� �����  ,1
      R0_ev=R_iv
C PUMP2_HANDLER.fmg( 298, 176):pre: �������� ��������� ������,1
      I0_e = z'0100000A'
C PUMP2_HANDLER.fmg( 199, 235):��������� ������������� IN (�������)
      I0_i = z'01000003'
C PUMP2_HANDLER.fmg( 199, 237):��������� ������������� IN (�������)
      I0_ot = z'01000009'
C PUMP2_HANDLER.fmg( 141, 272):��������� ������������� IN (�������)
      I0_ut = z'01000003'
C PUMP2_HANDLER.fmg( 141, 274):��������� ������������� IN (�������)
      I0_ef = z'0100000E'
C PUMP2_HANDLER.fmg( 163, 271):��������� ������������� IN (�������)
      I0_ad = z'0100000A'
C PUMP2_HANDLER.fmg( 189, 226):��������� ������������� IN (�������)
      I0_ed = z'01000003'
C PUMP2_HANDLER.fmg( 189, 228):��������� ������������� IN (�������)
      L_ep=R_od.ne.R_id
      R_id=R_od
C PUMP2_HANDLER.fmg(  70, 221):���������� ������������� ������
      L0_ar = L_ep.AND.(.NOT.L_ap)
C PUMP2_HANDLER.fmg( 130, 220):�
      L0_ek = L_am.AND.L_ep
C PUMP2_HANDLER.fmg( 132, 167):�
      L0_uf = (.NOT.L_em).OR.L_ep
C PUMP2_HANDLER.fmg( 138, 170):���
      L_op=R_af.ne.R_ud
      R_ud=R_af
C PUMP2_HANDLER.fmg(  70, 245):���������� ������������� ������
      L0_or = L_op.AND.(.NOT.L_ip)
C PUMP2_HANDLER.fmg( 130, 244):�
      L0_ik = L_em.AND.L_op
C PUMP2_HANDLER.fmg( 132, 174):�
      L_ol=(L0_ik.or.L_ol).and..not.(L0_uf)
      L0_ak=.not.L_ol
C PUMP2_HANDLER.fmg( 144, 172):RS �������,155
      L0_if = L_op.OR.(.NOT.L_am)
C PUMP2_HANDLER.fmg( 137, 163):���
      L_il=(L0_ek.or.L_il).and..not.(L0_if)
      L0_of=.not.L_il
C PUMP2_HANDLER.fmg( 144, 165):RS �������,156
      L_ul =.NOT.(L_om.OR.L_im.OR.L_ol.OR.L_il)
C PUMP2_HANDLER.fmg( 155, 186):���
      L_el =.NOT.(L_ul)
C PUMP2_HANDLER.fmg( 164, 181):���
      L_up=(L_im.or.L_up).and..not.(.NOT.L_im)
      L0_ok=.not.L_up
C PUMP2_HANDLER.fmg( 123, 202):RS �������
      L0_er = L0_ar.OR.L_as.OR.L_up
C PUMP2_HANDLER.fmg( 137, 218):���
      L0_us = L0_er.AND.(.NOT.L_am)
C PUMP2_HANDLER.fmg( 142, 217):�
      L_ir=(L_om.or.L_ir).and..not.(.NOT.L_om)
      L0_uk=.not.L_ir
C PUMP2_HANDLER.fmg( 121, 228):RS �������
      L0_ur = L0_or.OR.L_es.OR.L_ir
C PUMP2_HANDLER.fmg( 137, 242):���
      L0_os = L0_ur.AND.(.NOT.L_em)
C PUMP2_HANDLER.fmg( 142, 241):�
      L_ix=(L0_os.or.L_ix).and..not.(L0_us)
      L0_at=.not.L_ix
C PUMP2_HANDLER.fmg( 148, 239):RS �������,3
      L_is=L_ix
C PUMP2_HANDLER.fmg( 176, 241):������,lstate
      L_um=L_ix
C PUMP2_HANDLER.fmg( 178, 235):������,cb_out
      if(L_ix) then
         I_u=I0_ad
      else
         I_u=I0_ed
      endif
C PUMP2_HANDLER.fmg( 192, 226):���� RE IN LO CH7
      if(.NOT.L_ix) then
         I_o=I0_e
      else
         I_o=I0_i
      endif
C PUMP2_HANDLER.fmg( 202, 235):���� RE IN LO CH7
      if(L_ix) then
         I0_it=I0_ot
      else
         I0_it=I0_ut
      endif
C PUMP2_HANDLER.fmg( 144, 272):���� RE IN LO CH7
      if(L_el) then
         I_et=I0_ef
      else
         I_et=I0_it
      endif
C PUMP2_HANDLER.fmg( 166, 271):���� RE IN LO CH7
      L_al = L_ip.OR.L_ap
C PUMP2_HANDLER.fmg( 153, 207):���
      R0_ox = 1000
C PUMP2_HANDLER.fmg( 305, 183):��������� (RE4) (�������)
      R0_ufe = R0_ox * R8_ebe
C PUMP2_HANDLER.fmg( 308, 183):����������
      R0_abe = 0.1
C PUMP2_HANDLER.fmg( 276, 174):��������� (RE4) (�������)
      L0_ux=R8_ebe.lt.R0_abe
C PUMP2_HANDLER.fmg( 280, 176):���������� <
      L0_ume = L_ix.AND.(.NOT.L0_ux)
C PUMP2_HANDLER.fmg( 219, 219):�
      if(L0_ume) then
         R0_eme=R_ime
      else
         R0_eme=R_ome
      endif
C PUMP2_HANDLER.fmg( 224, 260):���� RE IN LO CH7
      L0_ube = (.NOT.L_ibe).AND.L0_ume
C PUMP2_HANDLER.fmg( 324, 195):�
      if(.not.L0_ux) then
         R_iv=0.0
      elseif(.not.L_ov) then
         R_iv=R_av
      else
         R_iv=max(R0_ev-deltat,0.0)
      endif
      L0_uv=L0_ux.and.R_iv.le.0.0
      L_ov=L0_ux
C PUMP2_HANDLER.fmg( 298, 176):�������� ��������� ������,1
      L0_ax = L0_uv.AND.(.NOT.L_ex)
C PUMP2_HANDLER.fmg( 325, 175):�
      R0_ide = R8_oke * R_ofe
C PUMP2_HANDLER.fmg( 274, 194):����������
      R0_ode = R0_ide + (-R8_oke)
C PUMP2_HANDLER.fmg( 280, 201):��������
      R0_ade = 0
C PUMP2_HANDLER.fmg( 321, 206):��������� (RE4) (�������)
      L0_ude=.false.
C PUMP2_HANDLER.fmg( 304, 197):��������� ���������� (�������)
      R0_ake = 1.73
C PUMP2_HANDLER.fmg( 323, 184):��������� (RE4) (�������)
      R0_eke = R0_ake * R0_ufe
C PUMP2_HANDLER.fmg( 326, 184):����������
      L0_ile=.false.
C PUMP2_HANDLER.fmg( 242, 244):��������� ���������� (�������)
      R0_ipe = 0
C PUMP2_HANDLER.fmg( 216, 249):��������� (RE4) (�������)
      R0_epe = 1
C PUMP2_HANDLER.fmg( 216, 247):��������� (RE4) (�������)
      if(L0_ume) then
         R0_ape=R0_epe
      else
         R0_ape=R0_ipe
      endif
C PUMP2_HANDLER.fmg( 219, 248):���� RE IN LO CH7
      if(L0_ile) then
         R_ule=R0_ape
      else
         R_ule=(R0_eme*R0_ole+deltat*R0_ape)/(R0_eme+deltat
     &)
      endif
C PUMP2_HANDLER.fmg( 248, 249):�������������� �����  ,1
      if(L_ibe) then
         R8_ame=R8_obe
      else
         R8_ame=R_ule
      endif
C PUMP2_HANDLER.fmg( 340, 247):���� RE IN LO CH7
      R0_ife = R_ule * R0_ode
C PUMP2_HANDLER.fmg( 286, 202):����������
      if(L0_ude) then
         R_afe=R0_ife
      else
         R_afe=(R0_eme*R0_efe+deltat*R0_ife)/(R0_eme+deltat
     &)
      endif
C PUMP2_HANDLER.fmg( 310, 202):�������������� �����  ,5
      R0_ede = R0_ide + (-R_afe)
C PUMP2_HANDLER.fmg( 318, 201):��������
      if(L0_ube) then
         R_ale=R0_ede
      else
         R_ale=R0_ade
      endif
C PUMP2_HANDLER.fmg( 324, 201):���� RE IN LO CH7
      if(R0_eke.ge.0.0) then
         R_ike=R_ale/max(R0_eke,1.0e-10)
      else
         R_ike=R_ale/min(R0_eke,-1.0e-10)
      endif
C PUMP2_HANDLER.fmg( 353, 186):�������� ����������
      R0_uke = R8_ele
C PUMP2_HANDLER.fmg( 343, 215):��������
C label 146  try146=try146-1
      R8_ele = R_ale + R0_uke
C PUMP2_HANDLER.fmg( 349, 214):��������
C sav1=R0_uke
      R0_uke = R8_ele
C PUMP2_HANDLER.fmg( 343, 215):recalc:��������
C if(sav1.ne.R0_uke .and. try146.gt.0) goto 146
      End
