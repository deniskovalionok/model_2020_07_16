      Interface
      Subroutine LODOCHKA_HANDLER_FDA60_COORD(ext_deltat,R_e
     &,I_i,I_u,I_ed,R_od,I_af,R_ef,I_if,I_uf,I_ak,I_ek,R_ik
     &,I_ok,R_uk,I_al,R_el,I_il,R_ol,I_ul,R_am,I_em,R_im,I_om
     &,R_um,I_ap,R_ep,I_op,I_up,R_ar,I_ir,R_or,I_as,I_es,R_is
     &,I_us,I_at,R_et,I_ot,I_ut,R_av,I_iv,I_ov,R_uv,I_ex,I_ix
     &,R_ox,I_abe,I_ebe,R_ibe,I_ube,I_ade,R_ede,I_ode,I_ude
     &,I_afe,I_efe,I_ife,I_ofe,I_ufe,I_ake,I_eke,I_ike,I_oke
     &,I_uke,I_ale,I_ele,I_ile,I_ole,I_ule,I_ame,I_eme,I_ime
     &,I_ome,I_ume,I_ape,I_epe,I_ipe,I_ope,I_upe,I_are,I_ere
     &,I_ire,I_ore,I_ure,I_ase,I_ese,I_ise,I_ose,I_use,I_ate
     &,I_ete,I_ite,I_ote,I_ute,I_ave,I_eve,I_ive,I_ove,I_uve
     &,I_axe,I_exe,I_ixe,I_oxe,I_uxe,I_abi,I_ebi,I_ibi,I_obi
     &,I_ubi,I_adi,I_edi,I_idi,I_odi,I_udi,I_afi,I_efi,I_ifi
     &,I_ofi,I_ufi,I_aki,I_eki,I_iki,I_oki,I_uki,R_ali,R_eli
     &,R_ili,R_oli,R_uli,R_ami,R_emi,R_imi,R_omi,R_umi,R_api
     &,R_epi,R_ipi,R_opi,R_upi,R_ari,R_eri,R_iri,R_ori,R_uri
     &,R_asi,R_esi,R_isi,R_osi,R_usi,R_ati,R_eti,R_iti,R_oti
     &,R_uti,R_avi,R_evi,R_ivi,R_ovi,R_uvi,R_axi,R_exi,R_ixi
     &,R_oxi,R_uxi,R_abo,R_ebo,R_ibo,R_obo,R_ubo,R_ado,R_edo
     &,R_ido,R_odo,R_udo,R_afo,R_efo,R_ifo,R_ofo,R_ufo,R_ako
     &,R_eko,R_iko,R_oko,R_uko,R_alo,R_elo,R_ilo,R_olo,R_ulo
     &,R_amo,R_emo,R_imo,R_omo,R_umo,R_apo,L_aso,L_eso,L_iso
     &,L_oso,L_uso,L_ato,L_eto,L_ito,L_oto,L_uto,L_avo,L_evo
     &,I_ibu,I_obu,I_ubu,I_adu,I_edu,I_idu,I_odu,I_udu,I_afu
     &,I_efu,I_ilu,I_olu,I_ulu,I_amu,I_emu,I_imu,I_omu,I_umu
     &,I_apu,I_epu,I_isu,I_osu,I_usu,I_atu,I_etu,I_itu,I_otu
     &,I_utu,I_avu,I_evu,I_ibad,I_obad,I_ubad,I_adad,I_edad
     &,I_idad,I_odad,I_udad,I_afad,I_efad,I_ilad,I_olad,I_ulad
     &,I_amad,I_emad,I_imad,I_omad,I_umad,I_apad,I_epad,I_osad
     &,I_usad,I_atad,I_etad,I_itad,I_otad,I_utad,I_avad,I_evad
     &,I_ivad,I_ovad,I_uvad,I_axad,I_exad,I_ixad,I_oxad,I_uxad
     &,I_abed,I_ebed,I_ibed,I_obed)
C |R_e           |4 4 O|CIL_20FDA70_MASS|||
C |I_i           |2 4 K|_lcmpJ10261     |�������� ������ �����������|7001|
C |I_u           |2 4 O|CIL_20FDA70_N   |||
C |I_ed          |2 4 K|_lcmpJ10245     |�������� ������ �����������|5|
C |R_od          |4 4 O|FDA60CW100ZQ01  |||
C |I_af          |2 4 O|FDA60GG006ZV01  |||
C |R_ef          |4 4 O|CIL_20FDA40_MASS|||
C |I_if          |2 4 K|_lcmpJ10222     |�������� ������ �����������|4001|
C |I_uf          |2 4 O|CIL_20FDA40_N   |||
C |I_ak          |2 4 K|_lcmpJ10126     |�������� ������ �����������|6|
C |I_ek          |2 4 O|CIL_20FDA60AE502_N7|||
C |R_ik          |4 4 O|CIL_20FDA60AE502_MASS7|||
C |I_ok          |2 4 O|CIL_20FDA60AE502_N6|||
C |R_uk          |4 4 O|CIL_20FDA60AE502_MASS6|||
C |I_al          |2 4 O|CIL_20FDA60AE502_N5|||
C |R_el          |4 4 O|CIL_20FDA60AE502_MASS5|||
C |I_il          |2 4 O|CIL_20FDA60AE502_N4|||
C |R_ol          |4 4 O|CIL_20FDA60AE502_MASS4|||
C |I_ul          |2 4 O|CIL_20FDA60AE502_N3|||
C |R_am          |4 4 O|CIL_20FDA60AE502_MASS3|||
C |I_em          |2 4 O|CIL_20FDA60AE502_N2|||
C |R_im          |4 4 O|CIL_20FDA60AE502_MASS2|||
C |I_om          |2 4 O|CIL_20FDA60AE502_N1|||
C |R_um          |4 4 O|CIL_20FDA60AE502_MASS1|||
C |I_ap          |2 4 K|_lcmpJ9970      |�������� ������ �����������|35|
C |R_ep          |4 4 O|CIL_20FDA60AE515_MASS|||
C |I_op          |2 4 O|CIL_20FDA60AE515_N|||
C |I_up          |2 4 K|_lcmpJ9956      |�������� ������ �����������|11|
C |R_ar          |4 4 O|CIL_20FDA60AE509_MASS|||
C |I_ir          |2 4 O|CIL_20FDA60AE509_N|||
C |R_or          |4 4 O|CIL_20FDA60AE403_MASS|||
C |I_as          |2 4 O|CIL_20FDA60AE403_N|||
C |I_es          |2 4 K|_lcmpJ9928      |�������� ������ �����������|5|
C |R_is          |4 4 O|CIL_20FDA60AE500_MASS2|||
C |I_us          |2 4 O|CIL_20FDA60AE500_N2|||
C |I_at          |2 4 K|_lcmpJ9914      |�������� ������ �����������|4|
C |R_et          |4 4 O|CIL_20FDA60AE500_MASS1|||
C |I_ot          |2 4 O|CIL_20FDA60AE500_N1|||
C |I_ut          |2 4 K|_lcmpJ9900      |�������� ������ �����������|101|
C |R_av          |4 4 O|CIL_20FDA60AE402_MASS|||
C |I_iv          |2 4 O|CIL_20FDA60AE402_N|||
C |I_ov          |2 4 K|_lcmpJ9886      |�������� ������ �����������|100|
C |R_uv          |4 4 O|CIL_20FDA60AE201_MASS|||
C |I_ex          |2 4 O|CIL_20FDA60AE201_N|||
C |I_ix          |2 4 K|_lcmpJ9872      |�������� ������ �����������|99|
C |R_ox          |4 4 O|CIL_20FDA60AE401_MASS|||
C |I_abe         |2 4 O|CIL_20FDA60AE401_N|||
C |I_ebe         |2 4 K|_lcmpJ9858      |�������� ������ �����������|98|
C |R_ibe         |4 4 O|CIL_20FDA60AE200_MASS|||
C |I_ube         |2 4 O|CIL_20FDA60AE200_N|||
C |I_ade         |2 4 K|_lcmpJ9844      |�������� ������ �����������|97|
C |R_ede         |4 4 O|CIL_20FDA60AE400_MASS|||
C |I_ode         |2 4 O|CIL_20FDA60AE400_N|||
C |I_ude         |2 4 K|_lcmpJ9767      |�������� ������ �����������|27|
C |I_afe         |2 4 K|_lcmpJ9766      |�������� ������ �����������|26|
C |I_efe         |2 4 K|_lcmpJ9765      |�������� ������ �����������|25|
C |I_ife         |2 4 K|_lcmpJ9764      |�������� ������ �����������|24|
C |I_ofe         |2 4 K|_lcmpJ9763      |�������� ������ �����������|23|
C |I_ufe         |2 4 K|_lcmpJ9757      |�������� ������ �����������|92|
C |I_ake         |2 4 K|_lcmpJ9756      |�������� ������ �����������|96|
C |I_eke         |2 4 K|_lcmpJ9755      |�������� ������ �����������|95|
C |I_ike         |2 4 K|_lcmpJ9754      |�������� ������ �����������|94|
C |I_oke         |2 4 K|_lcmpJ9753      |�������� ������ �����������|93|
C |I_uke         |2 4 K|_lcmpJ9751      |�������� ������ �����������|87|
C |I_ale         |2 4 K|_lcmpJ9749      |�������� ������ �����������|91|
C |I_ele         |2 4 K|_lcmpJ9747      |�������� ������ �����������|90|
C |I_ile         |2 4 K|_lcmpJ9745      |�������� ������ �����������|89|
C |I_ole         |2 4 K|_lcmpJ9744      |�������� ������ �����������|88|
C |I_ule         |2 4 K|_lcmpJ9738      |�������� ������ �����������|82|
C |I_ame         |2 4 K|_lcmpJ9737      |�������� ������ �����������|86|
C |I_eme         |2 4 K|_lcmpJ9736      |�������� ������ �����������|85|
C |I_ime         |2 4 K|_lcmpJ9735      |�������� ������ �����������|84|
C |I_ome         |2 4 K|_lcmpJ9734      |�������� ������ �����������|83|
C |I_ume         |2 4 K|_lcmpJ9732      |�������� ������ �����������|77|
C |I_ape         |2 4 K|_lcmpJ9730      |�������� ������ �����������|81|
C |I_epe         |2 4 K|_lcmpJ9728      |�������� ������ �����������|80|
C |I_ipe         |2 4 K|_lcmpJ9726      |�������� ������ �����������|79|
C |I_ope         |2 4 K|_lcmpJ9724      |�������� ������ �����������|78|
C |I_upe         |2 4 K|_lcmpJ9717      |�������� ������ �����������|62|
C |I_are         |2 4 K|_lcmpJ9716      |�������� ������ �����������|66|
C |I_ere         |2 4 K|_lcmpJ9715      |�������� ������ �����������|65|
C |I_ire         |2 4 K|_lcmpJ9714      |�������� ������ �����������|64|
C |I_ore         |2 4 K|_lcmpJ9713      |�������� ������ �����������|63|
C |I_ure         |2 4 K|_lcmpJ9711      |�������� ������ �����������|57|
C |I_ase         |2 4 K|_lcmpJ9709      |�������� ������ �����������|61|
C |I_ese         |2 4 K|_lcmpJ9707      |�������� ������ �����������|60|
C |I_ise         |2 4 K|_lcmpJ9705      |�������� ������ �����������|59|
C |I_ose         |2 4 K|_lcmpJ9703      |�������� ������ �����������|58|
C |I_use         |2 4 K|_lcmpJ9696      |�������� ������ �����������|52|
C |I_ate         |2 4 K|_lcmpJ9695      |�������� ������ �����������|56|
C |I_ete         |2 4 K|_lcmpJ9694      |�������� ������ �����������|55|
C |I_ite         |2 4 K|_lcmpJ9693      |�������� ������ �����������|54|
C |I_ote         |2 4 K|_lcmpJ9692      |�������� ������ �����������|53|
C |I_ute         |2 4 K|_lcmpJ9690      |�������� ������ �����������|47|
C |I_ave         |2 4 K|_lcmpJ9688      |�������� ������ �����������|51|
C |I_eve         |2 4 K|_lcmpJ9686      |�������� ������ �����������|50|
C |I_ive         |2 4 K|_lcmpJ9684      |�������� ������ �����������|49|
C |I_ove         |2 4 K|_lcmpJ9682      |�������� ������ �����������|48|
C |I_uve         |2 4 K|_lcmpJ9653      |�������� ������ �����������|72|
C |I_axe         |2 4 K|_lcmpJ9652      |�������� ������ �����������|76|
C |I_exe         |2 4 K|_lcmpJ9651      |�������� ������ �����������|75|
C |I_ixe         |2 4 K|_lcmpJ9650      |�������� ������ �����������|74|
C |I_oxe         |2 4 K|_lcmpJ9649      |�������� ������ �����������|73|
C |I_uxe         |2 4 K|_lcmpJ9647      |�������� ������ �����������|67|
C |I_abi         |2 4 K|_lcmpJ9645      |�������� ������ �����������|71|
C |I_ebi         |2 4 K|_lcmpJ9643      |�������� ������ �����������|70|
C |I_ibi         |2 4 K|_lcmpJ9641      |�������� ������ �����������|69|
C |I_obi         |2 4 K|_lcmpJ9640      |�������� ������ �����������|68|
C |I_ubi         |2 4 K|_lcmpJ9634      |�������� ������ �����������|42|
C |I_adi         |2 4 K|_lcmpJ9633      |�������� ������ �����������|46|
C |I_edi         |2 4 K|_lcmpJ9632      |�������� ������ �����������|45|
C |I_idi         |2 4 K|_lcmpJ9631      |�������� ������ �����������|44|
C |I_odi         |2 4 K|_lcmpJ9630      |�������� ������ �����������|43|
C |I_udi         |2 4 K|_lcmpJ9614      |�������� ������ �����������|16|
C |I_afi         |2 4 K|_lcmpJ9613      |�������� ������ �����������|15|
C |I_efi         |2 4 K|_lcmpJ9612      |�������� ������ �����������|14|
C |I_ifi         |2 4 K|_lcmpJ9611      |�������� ������ �����������|13|
C |I_ofi         |2 4 K|_lcmpJ9609      |�������� ������ �����������|12|
C |I_ufi         |2 4 K|_lcmpJ9605      |�������� ������ �����������|37|
C |I_aki         |2 4 K|_lcmpJ9602      |�������� ������ �����������|41|
C |I_eki         |2 4 K|_lcmpJ9600      |�������� ������ �����������|40|
C |I_iki         |2 4 K|_lcmpJ9598      |�������� ������ �����������|39|
C |I_oki         |2 4 K|_lcmpJ9595      |�������� ������ �����������|38|
C |I_uki         |2 4 I|CR              |������ ������������||
C |R_ali         |4 4 O|CIL_PKS6_MASS1  |||
C |R_eli         |4 4 O|CIL_PKS6_MASS2  |||
C |R_ili         |4 4 O|CIL_PKS6_MASS3  |||
C |R_oli         |4 4 O|CIL_PKS6_MASS4  |||
C |R_uli         |4 4 O|CIL_PKS6_MASS5  |||
C |R_ami         |4 4 O|CIL_KO6_MASS1   |||
C |R_emi         |4 4 O|CIL_KO6_MASS2   |||
C |R_imi         |4 4 O|CIL_KO6_MASS3   |||
C |R_omi         |4 4 O|CIL_KO6_MASS4   |||
C |R_umi         |4 4 O|CIL_KO6_MASS5   |||
C |R_api         |4 4 O|CIL_KO5_MASS5   |||
C |R_epi         |4 4 O|CIL_KO5_MASS4   |||
C |R_ipi         |4 4 O|CIL_KO5_MASS3   |||
C |R_opi         |4 4 O|CIL_KO5_MASS2   |||
C |R_upi         |4 4 O|CIL_KO5_MASS1   |||
C |R_ari         |4 4 O|CIL_PKS5_MASS5  |||
C |R_eri         |4 4 O|CIL_PKS5_MASS4  |||
C |R_iri         |4 4 O|CIL_PKS5_MASS3  |||
C |R_ori         |4 4 O|CIL_PKS5_MASS2  |||
C |R_uri         |4 4 O|CIL_PKS5_MASS1  |||
C |R_asi         |4 4 O|CIL_KO4_MASS5   |||
C |R_esi         |4 4 O|CIL_KO4_MASS4   |||
C |R_isi         |4 4 O|CIL_KO4_MASS3   |||
C |R_osi         |4 4 O|CIL_KO4_MASS2   |||
C |R_usi         |4 4 O|CIL_KO4_MASS1   |||
C |R_ati         |4 4 O|CIL_PKS4_MASS5  |||
C |R_eti         |4 4 O|CIL_PKS4_MASS4  |||
C |R_iti         |4 4 O|CIL_PKS4_MASS3  |||
C |R_oti         |4 4 O|CIL_PKS4_MASS2  |||
C |R_uti         |4 4 O|CIL_PKS4_MASS1  |||
C |R_avi         |4 4 O|CIL_20FDA60AE408_MASS1|||
C |R_evi         |4 4 O|CIL_20FDA60AE408_MASS2|||
C |R_ivi         |4 4 O|CIL_20FDA60AE408_MASS3|||
C |R_ovi         |4 4 O|CIL_20FDA60AE408_MASS4|||
C |R_uvi         |4 4 O|CIL_20FDA60AE408_MASS5|||
C |R_axi         |4 4 O|CIL_20FDA60AE413_MASS1|||
C |R_exi         |4 4 O|CIL_20FDA60AE413_MASS2|||
C |R_ixi         |4 4 O|CIL_20FDA60AE413_MASS3|||
C |R_oxi         |4 4 O|CIL_20FDA60AE413_MASS4|||
C |R_uxi         |4 4 O|CIL_20FDA60AE413_MASS5|||
C |R_abo         |4 4 O|CIL_KO3_MASS5   |||
C |R_ebo         |4 4 O|CIL_KO3_MASS4   |||
C |R_ibo         |4 4 O|CIL_KO3_MASS3   |||
C |R_obo         |4 4 O|CIL_KO3_MASS2   |||
C |R_ubo         |4 4 O|CIL_KO3_MASS1   |||
C |R_ado         |4 4 O|CIL_PKS3_MASS5  |||
C |R_edo         |4 4 O|CIL_PKS3_MASS4  |||
C |R_ido         |4 4 O|CIL_PKS3_MASS3  |||
C |R_odo         |4 4 O|CIL_PKS3_MASS2  |||
C |R_udo         |4 4 O|CIL_PKS3_MASS1  |||
C |R_afo         |4 4 O|CIL_PKS2_MASS1  |||
C |R_efo         |4 4 O|CIL_PKS2_MASS2  |||
C |R_ifo         |4 4 O|CIL_PKS2_MASS3  |||
C |R_ofo         |4 4 O|CIL_PKS2_MASS4  |||
C |R_ufo         |4 4 O|CIL_PKS2_MASS5  |||
C |R_ako         |4 4 O|CIL_KO2_MASS1   |||
C |R_eko         |4 4 O|CIL_KO2_MASS2   |||
C |R_iko         |4 4 O|CIL_KO2_MASS3   |||
C |R_oko         |4 4 O|CIL_KO2_MASS4   |||
C |R_uko         |4 4 O|CIL_KO2_MASS5   |||
C |R_alo         |4 4 O|CIL_KO1_MASS5   |||
C |R_elo         |4 4 O|CIL_KO1_MASS4   |||
C |R_ilo         |4 4 O|CIL_KO1_MASS3   |||
C |R_olo         |4 4 O|CIL_KO1_MASS2   |||
C |R_ulo         |4 4 O|CIL_KO1_MASS1   |||
C |R_amo         |4 4 O|CIL_PKS1_MASS5  |||
C |R_emo         |4 4 O|CIL_PKS1_MASS4  |||
C |R_imo         |4 4 O|CIL_PKS1_MASS3  |||
C |R_omo         |4 4 O|CIL_PKS1_MASS2  |||
C |R_umo         |4 4 I|MASS            |����� �������||
C |R_apo         |4 4 O|CIL_PKS1_MASS1  |||
C |L_aso         |1 1 O|IN_KO4          |||
C |L_eso         |1 1 O|IN_KO5          |||
C |L_iso         |1 1 O|IN_KO6          |||
C |L_oso         |1 1 O|IN_KO3          |||
C |L_uso         |1 1 O|IN_KO2          |||
C |L_ato         |1 1 O|IN_KO1          |||
C |L_eto         |1 1 O|IN_PKS4         |||
C |L_ito         |1 1 O|IN_PKS5         |||
C |L_oto         |1 1 O|IN_PKS6         |||
C |L_uto         |1 1 O|IN_PKS3         |||
C |L_avo         |1 1 O|IN_PKS2         |||
C |L_evo         |1 1 O|IN_PKS1         |||
C |I_ibu         |2 4 O|CIL_PKS6_N1     |||
C |I_obu         |2 4 O|CIL_PKS6_N2     |||
C |I_ubu         |2 4 O|CIL_KO6_N5      |||
C |I_adu         |2 4 O|CIL_KO6_N4      |||
C |I_edu         |2 4 O|CIL_KO6_N3      |||
C |I_idu         |2 4 O|CIL_KO6_N2      |||
C |I_odu         |2 4 O|CIL_KO6_N1      |||
C |I_udu         |2 4 O|CIL_PKS6_N5     |||
C |I_afu         |2 4 O|CIL_PKS6_N4     |||
C |I_efu         |2 4 O|CIL_PKS6_N3     |||
C |I_ilu         |2 4 O|CIL_PKS5_N1     |||
C |I_olu         |2 4 O|CIL_PKS5_N2     |||
C |I_ulu         |2 4 O|CIL_KO5_N5      |||
C |I_amu         |2 4 O|CIL_KO5_N4      |||
C |I_emu         |2 4 O|CIL_KO5_N3      |||
C |I_imu         |2 4 O|CIL_KO5_N2      |||
C |I_omu         |2 4 O|CIL_KO5_N1      |||
C |I_umu         |2 4 O|CIL_PKS5_N5     |||
C |I_apu         |2 4 O|CIL_PKS5_N4     |||
C |I_epu         |2 4 O|CIL_PKS5_N3     |||
C |I_isu         |2 4 O|CIL_PKS4_N1     |||
C |I_osu         |2 4 O|CIL_PKS4_N2     |||
C |I_usu         |2 4 O|CIL_KO4_N5      |||
C |I_atu         |2 4 O|CIL_KO4_N4      |||
C |I_etu         |2 4 O|CIL_KO4_N3      |||
C |I_itu         |2 4 O|CIL_KO4_N2      |||
C |I_otu         |2 4 O|CIL_KO4_N1      |||
C |I_utu         |2 4 O|CIL_PKS4_N5     |||
C |I_avu         |2 4 O|CIL_PKS4_N4     |||
C |I_evu         |2 4 O|CIL_PKS4_N3     |||
C |I_ibad        |2 4 O|CIL_PKS3_N1     |||
C |I_obad        |2 4 O|CIL_PKS3_N2     |||
C |I_ubad        |2 4 O|CIL_KO3_N5      |||
C |I_adad        |2 4 O|CIL_KO3_N4      |||
C |I_edad        |2 4 O|CIL_KO3_N3      |||
C |I_idad        |2 4 O|CIL_KO3_N2      |||
C |I_odad        |2 4 O|CIL_KO3_N1      |||
C |I_udad        |2 4 O|CIL_PKS3_N5     |||
C |I_afad        |2 4 O|CIL_PKS3_N4     |||
C |I_efad        |2 4 O|CIL_PKS3_N3     |||
C |I_ilad        |2 4 O|CIL_PKS2_N1     |||
C |I_olad        |2 4 O|CIL_PKS2_N2     |||
C |I_ulad        |2 4 O|CIL_KO2_N5      |||
C |I_amad        |2 4 O|CIL_KO2_N4      |||
C |I_emad        |2 4 O|CIL_KO2_N3      |||
C |I_imad        |2 4 O|CIL_KO2_N2      |||
C |I_omad        |2 4 O|CIL_KO2_N1      |||
C |I_umad        |2 4 O|CIL_PKS2_N5     |||
C |I_apad        |2 4 O|CIL_PKS2_N4     |||
C |I_epad        |2 4 O|CIL_PKS2_N3     |||
C |I_osad        |2 4 O|CIL_PKS1_N1     |||
C |I_usad        |2 4 O|CIL_PKS1_N2     |||
C |I_atad        |2 4 O|CIL_KO1_N5      |||
C |I_etad        |2 4 O|CIL_KO1_N4      |||
C |I_itad        |2 4 O|CIL_KO1_N3      |||
C |I_otad        |2 4 O|CIL_KO1_N2      |||
C |I_utad        |2 4 O|CIL_KO1_N1      |||
C |I_avad        |2 4 O|CIL_PKS1_N5     |||
C |I_evad        |2 4 O|CIL_PKS1_N4     |||
C |I_ivad        |2 4 O|CIL_PKS1_N3     |||
C |I_ovad        |2 4 O|CIL_20FDA60AE413_N5|||
C |I_uvad        |2 4 O|CIL_20FDA60AE413_N4|||
C |I_axad        |2 4 O|CIL_20FDA60AE413_N3|||
C |I_exad        |2 4 O|CIL_20FDA60AE413_N2|||
C |I_ixad        |2 4 O|CIL_20FDA60AE413_N1|||
C |I_oxad        |2 4 P|num_boat        |����� �������||
C |I_uxad        |2 4 O|CIL_20FDA60AE408_N5|||
C |I_abed        |2 4 O|CIL_20FDA60AE408_N4|||
C |I_ebed        |2 4 O|CIL_20FDA60AE408_N3|||
C |I_ibed        |2 4 O|CIL_20FDA60AE408_N2|||
C |I_obed        |2 4 O|CIL_20FDA60AE408_N1|||

      IMPLICIT NONE
      REAL*4 ext_deltat
      REAL*4 R_e
      INTEGER*4 I_i,I_u,I_ed
      REAL*4 R_od
      INTEGER*4 I_af
      REAL*4 R_ef
      INTEGER*4 I_if,I_uf,I_ak,I_ek
      REAL*4 R_ik
      INTEGER*4 I_ok
      REAL*4 R_uk
      INTEGER*4 I_al
      REAL*4 R_el
      INTEGER*4 I_il
      REAL*4 R_ol
      INTEGER*4 I_ul
      REAL*4 R_am
      INTEGER*4 I_em
      REAL*4 R_im
      INTEGER*4 I_om
      REAL*4 R_um
      INTEGER*4 I_ap
      REAL*4 R_ep
      INTEGER*4 I_op,I_up
      REAL*4 R_ar
      INTEGER*4 I_ir
      REAL*4 R_or
      INTEGER*4 I_as,I_es
      REAL*4 R_is
      INTEGER*4 I_us,I_at
      REAL*4 R_et
      INTEGER*4 I_ot,I_ut
      REAL*4 R_av
      INTEGER*4 I_iv,I_ov
      REAL*4 R_uv
      INTEGER*4 I_ex,I_ix
      REAL*4 R_ox
      INTEGER*4 I_abe,I_ebe
      REAL*4 R_ibe
      INTEGER*4 I_ube,I_ade
      REAL*4 R_ede
      INTEGER*4 I_ode,I_ude,I_afe,I_efe,I_ife,I_ofe,I_ufe
     &,I_ake,I_eke,I_ike,I_oke,I_uke,I_ale,I_ele,I_ile,I_ole
     &,I_ule,I_ame,I_eme,I_ime,I_ome,I_ume
      INTEGER*4 I_ape,I_epe,I_ipe,I_ope,I_upe,I_are,I_ere
     &,I_ire,I_ore,I_ure,I_ase,I_ese,I_ise,I_ose,I_use,I_ate
     &,I_ete,I_ite,I_ote,I_ute,I_ave,I_eve
      INTEGER*4 I_ive,I_ove,I_uve,I_axe,I_exe,I_ixe,I_oxe
     &,I_uxe,I_abi,I_ebi,I_ibi,I_obi,I_ubi,I_adi,I_edi,I_idi
     &,I_odi,I_udi,I_afi,I_efi,I_ifi,I_ofi
      INTEGER*4 I_ufi,I_aki,I_eki,I_iki,I_oki,I_uki
      REAL*4 R_ali,R_eli,R_ili,R_oli,R_uli,R_ami,R_emi,R_imi
     &,R_omi,R_umi,R_api,R_epi,R_ipi,R_opi,R_upi,R_ari,R_eri
     &,R_iri,R_ori,R_uri,R_asi,R_esi
      REAL*4 R_isi,R_osi,R_usi,R_ati,R_eti,R_iti,R_oti,R_uti
     &,R_avi,R_evi,R_ivi,R_ovi,R_uvi,R_axi,R_exi,R_ixi,R_oxi
     &,R_uxi,R_abo,R_ebo,R_ibo,R_obo
      REAL*4 R_ubo,R_ado,R_edo,R_ido,R_odo,R_udo,R_afo,R_efo
     &,R_ifo,R_ofo,R_ufo,R_ako,R_eko,R_iko,R_oko,R_uko,R_alo
     &,R_elo,R_ilo,R_olo,R_ulo,R_amo
      REAL*4 R_emo,R_imo,R_omo,R_umo,R_apo
      LOGICAL*1 L_aso,L_eso,L_iso,L_oso,L_uso,L_ato,L_eto
     &,L_ito,L_oto,L_uto,L_avo,L_evo
      INTEGER*4 I_ibu,I_obu,I_ubu,I_adu,I_edu,I_idu,I_odu
     &,I_udu,I_afu,I_efu,I_ilu,I_olu,I_ulu,I_amu,I_emu,I_imu
     &,I_omu,I_umu,I_apu,I_epu,I_isu,I_osu
      INTEGER*4 I_usu,I_atu,I_etu,I_itu,I_otu,I_utu,I_avu
     &,I_evu,I_ibad,I_obad,I_ubad,I_adad,I_edad,I_idad,I_odad
     &,I_udad,I_afad,I_efad,I_ilad,I_olad,I_ulad,I_amad
      INTEGER*4 I_emad,I_imad,I_omad,I_umad,I_apad,I_epad
     &,I_osad,I_usad,I_atad,I_etad,I_itad,I_otad,I_utad,I_avad
     &,I_evad,I_ivad,I_ovad,I_uvad,I_axad,I_exad,I_ixad,I_oxad
      INTEGER*4 I_uxad,I_abed,I_ebed,I_ibed,I_obed
      End subroutine LODOCHKA_HANDLER_FDA60_COORD
      End interface
