      Subroutine TRANSPORTER_dpro(ext_deltat,R_i,R_o,L_u,R_ed
     &,R_id,L_od,R_af,R_ef,L_if,R_uf,R_ak,L_ek,L_ar,L_er,L_or
     &,L_ur,L_as,R_at,R_et,L_it,L_uv,R_ex,R_ix,L_ox,R_abe
     &,R_ebe,L_ibe,R_ube,R_ade,L_ede,R_ode,R_ude,L_afe,L_ome
     &,L_ume,L_ape,L_ope,L_upe,R_ure,R_ase,L_ese,L_ose,L_use
     &,L_ate,L_ete,L_ite,L_ote,L_ute,L_ave,L_eve,L_ive,L_ove
     &,L_uve,L_axe,L_ixe,L_uxe,L_ebi,L_ibi,L_obi,L_ubi,L_adi
     &,L_edi,L_idi,L_afi,L_efi,L_ifi,L_ofi,L_ufi,L_aki,L_eki
     &,R_iki,R_ali,R_oli,R_uli,R_ami,R_emi,R_imi,R_omi,R_umi
     &,R_api,L_epi,L_ipi,L_opi,L_upi,L_ari,L_eri,L_iri,L_ori
     &,L_uri,L_asi,L_esi,L_isi,L_osi,L_usi,L_ati,L_eti,L_iti
     &,L_oti,L_uti,L_avi,L_evi,L_ivi,L_ovi,L_uvi,L_axi,L_exi
     &,L_ixi,L_oxi,L_uxi,L_abo,L_ibo,L_ubo,L_edo,L_ido,L_odo
     &,L_udo,L_afo,L_efo,L_ifo,L_ofo,L_ufo,L_ako,L_eko,L_iko
     &,L_oko,L_uko,L_alo,L_elo,L_ilo,L_olo)
C |R_i           |4 4 S|_simpinitimpv4* |[���]���������� ��������� ������������� |0.0|
C |R_o           |4 4 K|_timpinitimpv4  |[���]������������ �������� �������������|0.4|
C |L_u           |1 1 S|_limpinitimpv4* |[TF]���������� ��������� ������������� |F|
C |R_ed          |4 4 S|_simpinitimpv3* |[���]���������� ��������� ������������� |0.0|
C |R_id          |4 4 K|_timpinitimpv3  |[���]������������ �������� �������������|0.4|
C |L_od          |1 1 S|_limpinitimpv3* |[TF]���������� ��������� ������������� |F|
C |R_af          |4 4 S|_simpinitimpv2* |[���]���������� ��������� ������������� |0.0|
C |R_ef          |4 4 K|_timpinitimpv2  |[���]������������ �������� �������������|0.4|
C |L_if          |1 1 S|_limpinitimpv2* |[TF]���������� ��������� ������������� |F|
C |R_uf          |4 4 S|_simpinitimpv1* |[���]���������� ��������� ������������� |0.0|
C |R_ak          |4 4 K|_timpinitimpv1  |[���]������������ �������� �������������|0.4|
C |L_ek          |1 1 S|_limpinitimpv1* |[TF]���������� ��������� ������������� |F|
C |L_ar          |1 1 O|C7_stop_avt     |����||
C |L_er          |1 1 O|C8_stop_avt     |����||
C |L_or          |1 1 O|C6_stop_avt     |����||
C |L_ur          |1 1 S|_qfftrinitv*    |�������� ������ Q RS-��������  |F|
C |L_as          |1 1 O|VINITCMPLT      |||
C |R_at          |4 4 I|miny            |||
C |R_et          |4 4 I|maxy            |||
C |L_it          |1 1 I|VINIT           |������� � �������� �� ���������||
C |L_uv          |1 1 I|DOWN            |������� ����||
C |R_ex          |4 4 S|_simpinitimph4* |[���]���������� ��������� ������������� |0.0|
C |R_ix          |4 4 K|_timpinitimph4  |[���]������������ �������� �������������|0.4|
C |L_ox          |1 1 S|_limpinitimph4* |[TF]���������� ��������� ������������� |F|
C |R_abe         |4 4 S|_simpinitimph3* |[���]���������� ��������� ������������� |0.0|
C |R_ebe         |4 4 K|_timpinitimph3  |[���]������������ �������� �������������|0.4|
C |L_ibe         |1 1 S|_limpinitimph3* |[TF]���������� ��������� ������������� |F|
C |R_ube         |4 4 S|_simpinitimph2* |[���]���������� ��������� ������������� |0.0|
C |R_ade         |4 4 K|_timpinitimph2  |[���]������������ �������� �������������|0.4|
C |L_ede         |1 1 S|_limpinitimph2* |[TF]���������� ��������� ������������� |F|
C |R_ode         |4 4 S|_simpinitimph1* |[���]���������� ��������� ������������� |0.0|
C |R_ude         |4 4 K|_timpinitimph1  |[���]������������ �������� �������������|0.4|
C |L_afe         |1 1 S|_limpinitimph1* |[TF]���������� ��������� ������������� |F|
C |L_ome         |1 1 O|C5_stop_avt     |����||
C |L_ume         |1 1 O|C4_stop_avt     |����||
C |L_ape         |1 1 O|C3_stop_avt     |����||
C |L_ope         |1 1 S|_qfftrinith*    |�������� ������ Q RS-��������  |F|
C |L_upe         |1 1 O|INITCMPLT       |||
C |R_ure         |4 4 I|minx            |||
C |R_ase         |4 4 I|maxx            |||
C |L_ese         |1 1 I|INIT            |INIT �� �����������||
C |L_ose         |1 1 O|C7YA26          |������� �����||
C |L_use         |1 1 O|C6YA25          |������� �����||
C |L_ate         |1 1 O|C8YA25          |������� � �������||
C |L_ete         |1 1 I|C7YA26_B        |������� �����||
C |L_ite         |1 1 I|C6YA25_B        |������� �����||
C |L_ote         |1 1 I|C8YA25_B        |������� � �������||
C |L_ute         |1 1 I|UP              |������� �����||
C |L_ave         |1 1 O|C6YA26          |������� ����||
C |L_eve         |1 1 O|C7YA25          |������� ����||
C |L_ive         |1 1 O|C8YA26          |������� � �������||
C |L_ove         |1 1 I|C7YA25_B        |������� ����||
C |L_uve         |1 1 I|C6YA26_B        |������� ����||
C |L_axe         |1 1 I|C8YA26_B        |������� � �������||
C |L_ixe         |1 1 O|C5YA26          |������� � �������||
C |L_uxe         |1 1 O|C4YA25          |������� � �������||
C |L_ebi         |1 1 O|C3YA25          |������� � �������||
C |L_ibi         |1 1 I|C5YA26_B        |������� � �������||
C |L_obi         |1 1 I|C4YA25_B        |������� � �������||
C |L_ubi         |1 1 I|C3YA25_B        |������� � �������||
C |L_adi         |1 1 I|PREV            |������� � ���� ����||
C |L_edi         |1 1 O|VZZ02           |��������� ������� �����||
C |L_idi         |1 1 O|VZZ01           |��������� ������� ������||
C |L_afi         |1 1 O|L11             |������ ������||
C |L_efi         |1 1 O|L12             |��������� ����������||
C |L_ifi         |1 1 O|C11_uluop       |������� ������||
C |L_ofi         |1 1 O|C11_ulucl       |������� ������||
C |L_ufi         |1 1 I|C11_CBC         |������ ������||
C |L_aki         |1 1 I|C11B            |�������/������� ������||
C |L_eki         |1 1 I|C11_CBO         |������ ������||
C |R_iki         |4 4 O|VS01            |��������� �����������||
C |R_ali         |4 4 O|VZ01            |��������� ������������||
C |R_oli         |4 4 O|VX01            |��������� ������������||
C |R_uli         |4 4 I|C10_VZ01        |��������� �����������||
C |R_ami         |4 4 I|C8VX01          |��������� �������||
C |R_emi         |4 4 I|C7VX01          |��������� �������||
C |R_imi         |4 4 I|C6VX01          |��������� �������||
C |R_omi         |4 4 I|C5VX01          |��������� �������||
C |R_umi         |4 4 I|C4VX01          |��������� �������||
C |R_api         |4 4 I|C3VX01          |��������� �������||
C |L_epi         |1 1 O|L9              |���� ���� - �����||
C |L_ipi         |1 1 O|L10             |���� ���� - �����||
C |L_opi         |1 1 O|L8              |������ ������||
C |L_upi         |1 1 O|L7              |��������� ������||
C |L_ari         |1 1 O|L6_input        |���� ���� - �������||
C |L_eri         |1 1 O|L4              |��������� ����||
C |L_iri         |1 1 O|L5              |���� ���� - �����||
C |L_ori         |1 1 O|L3              |�������� ���������||
C |L_uri         |1 1 O|L1_input        |���������� ����||
C |L_asi         |1 1 O|L2              |���������� ����||
C |L_esi         |1 1 O|L10_input       |���� ���� - �����||
C |L_isi         |1 1 O|L9_input        |���� ���� - �����||
C |L_osi         |1 1 O|L6              |���� ���� - �������||
C |L_usi         |1 1 O|L5_input        |���� ���� - �����||
C |L_ati         |1 1 O|L1              |���������� ����||
C |L_eti         |1 1 O|L4_input        |��������� ����||
C |L_iti         |1 1 O|L3_input        |�������� ���������||
C |L_oti         |1 1 O|L2_input        |���������� ����||
C |L_uti         |1 1 I|C6XH54          |������� ��������||
C |L_avi         |1 1 I|C5XH53          |������ ��������||
C |L_evi         |1 1 I|C4XH54          |����� ��������||
C |L_ivi         |1 1 I|C3XH54          |����� ��������||
C |L_ovi         |1 1 I|C8XH53          |������ ��������||
C |L_uvi         |1 1 I|C7XH54          |������� ��������||
C |L_axi         |1 1 I|C6XH53          |������ ��������||
C |L_exi         |1 1 I|C5XH54          |����� ��������||
C |L_ixi         |1 1 I|C4XH53          |������ ��������||
C |L_oxi         |1 1 I|C3XH53          |������ ��������||
C |L_uxi         |1 1 O|C2_uluop        |������� ������||
C |L_abo         |1 1 O|C2_ulucl        |������� ������||
C |L_ibo         |1 1 O|C5YA25          |������� � �������||
C |L_ubo         |1 1 O|C4YA26          |������� � �������||
C |L_edo         |1 1 O|C3YA26          |������� � �������||
C |L_ido         |1 1 O|C1_uluop        |������� ����||
C |L_odo         |1 1 O|C1_ulucl        |������� ����||
C |L_udo         |1 1 O|C10_YA26        |��������� ������� ����||
C |L_afo         |1 1 O|C10_YA25        |��������� ������� �����||
C |L_efo         |1 1 I|C10B            |��������� ����������||
C |L_ifo         |1 1 I|C5YA25_B        |������� � �������||
C |L_ofo         |1 1 I|C4YA26_B        |������� � �������||
C |L_ufo         |1 1 I|NEXT            |������� � ���� ����||
C |L_ako         |1 1 I|C3YA26_B        |������� � �������||
C |L_eko         |1 1 I|C2_CBC          |������ ������||
C |L_iko         |1 1 I|C2B             |�������/������� ������||
C |L_oko         |1 1 I|C2_CBO          |������ ������||
C |L_uko         |1 1 I|C1_CBC          |���� ������||
C |L_alo         |1 1 I|C1B             |�������/������� ����||
C |L_elo         |1 1 I|C1_CBO          |���� ������||
C |L_ilo         |1 1 I|C10XH53         |��������� �� ����������||
C |L_olo         |1 1 I|C10XH54         |��������� ����������||

      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      REAL*4 R0_e,R_i,R_o
      LOGICAL*1 L_u
      REAL*4 R0_ad,R_ed,R_id
      LOGICAL*1 L_od
      REAL*4 R0_ud,R_af,R_ef
      LOGICAL*1 L_if
      REAL*4 R0_of,R_uf,R_ak
      LOGICAL*1 L_ek,L0_ik,L0_ok,L0_uk,L0_al
      REAL*4 R0_el
      LOGICAL*1 L0_il
      REAL*4 R0_ol
      LOGICAL*1 L0_ul
      REAL*4 R0_am
      LOGICAL*1 L0_em
      REAL*4 R0_im
      LOGICAL*1 L0_om
      REAL*4 R0_um
      LOGICAL*1 L0_ap
      REAL*4 R0_ep,R0_ip,R0_op,R0_up
      LOGICAL*1 L_ar,L_er,L0_ir,L_or,L_ur,L_as
      REAL*4 R0_es,R0_is,R0_os,R0_us,R_at,R_et
      LOGICAL*1 L_it,L0_ot,L0_ut,L0_av,L0_ev,L0_iv,L0_ov,L_uv
      REAL*4 R0_ax,R_ex,R_ix
      LOGICAL*1 L_ox
      REAL*4 R0_ux,R_abe,R_ebe
      LOGICAL*1 L_ibe
      REAL*4 R0_obe,R_ube,R_ade
      LOGICAL*1 L_ede
      REAL*4 R0_ide,R_ode,R_ude
      LOGICAL*1 L_afe,L0_efe,L0_ife,L0_ofe
      REAL*4 R0_ufe
      LOGICAL*1 L0_ake
      REAL*4 R0_eke
      LOGICAL*1 L0_ike
      REAL*4 R0_oke
      LOGICAL*1 L0_uke
      REAL*4 R0_ale
      LOGICAL*1 L0_ele
      REAL*4 R0_ile
      LOGICAL*1 L0_ole
      REAL*4 R0_ule,R0_ame,R0_eme,R0_ime
      LOGICAL*1 L_ome,L_ume,L_ape,L0_epe,L0_ipe,L_ope,L_upe
      REAL*4 R0_are,R0_ere,R0_ire,R0_ore,R_ure,R_ase
      LOGICAL*1 L_ese,L0_ise,L_ose,L_use,L_ate,L_ete,L_ite
     &,L_ote,L_ute,L_ave,L_eve,L_ive,L_ove,L_uve,L_axe,L0_exe
     &,L_ixe,L0_oxe,L_uxe,L0_abi,L_ebi,L_ibi
      LOGICAL*1 L_obi,L_ubi,L_adi,L_edi,L_idi,L0_odi,L0_udi
     &,L_afi,L_efi,L_ifi,L_ofi,L_ufi,L_aki,L_eki
      REAL*4 R_iki,R0_oki,R0_uki,R_ali,R0_eli,R0_ili,R_oli
     &,R_uli,R_ami,R_emi,R_imi,R_omi,R_umi,R_api
      LOGICAL*1 L_epi,L_ipi,L_opi,L_upi,L_ari,L_eri,L_iri
     &,L_ori,L_uri,L_asi,L_esi,L_isi,L_osi,L_usi,L_ati,L_eti
     &,L_iti,L_oti,L_uti,L_avi,L_evi,L_ivi
      LOGICAL*1 L_ovi,L_uvi,L_axi,L_exi,L_ixi,L_oxi,L_uxi
     &,L_abo,L0_ebo,L_ibo,L0_obo,L_ubo,L0_ado,L_edo,L_ido
     &,L_odo,L_udo,L_afo,L_efo,L_ifo,L_ofo,L_ufo
      LOGICAL*1 L_ako,L_eko,L_iko,L_oko,L_uko,L_alo,L_elo
     &,L_ilo,L_olo

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      R0_e=R_i
C TRANSPORTER_dpro.fmg( 213, 206):pre: ������������  �� T,initimpv4
      R0_ud=R_af
C TRANSPORTER_dpro.fmg( 213, 222):pre: ������������  �� T,initimpv2
      R0_ad=R_ed
C TRANSPORTER_dpro.fmg( 213, 214):pre: ������������  �� T,initimpv3
      R0_obe=R_ube
C TRANSPORTER_dpro.fmg(  98, 222):pre: ������������  �� T,initimph2
      R0_ax=R_ex
C TRANSPORTER_dpro.fmg(  98, 206):pre: ������������  �� T,initimph4
      R0_ux=R_abe
C TRANSPORTER_dpro.fmg(  98, 214):pre: ������������  �� T,initimph3
      R0_ide=R_ode
C TRANSPORTER_dpro.fmg(  36, 235):pre: ������������  �� T,initimph1
      R0_of=R_uf
C TRANSPORTER_dpro.fmg( 161, 235):pre: ������������  �� T,initimpv1
      if(L_it.and..not.L_ek) then
         R_uf=R_ak
      else
         R_uf=max(R0_of-deltat,0.0)
      endif
      L0_ir=R_uf.gt.0.0
      L_ek=L_it
C TRANSPORTER_dpro.fmg( 161, 235):������������  �� T,initimpv1
      R0_el = -10
C TRANSPORTER_dpro.fmg( 181, 202):��������� (RE4) (�������)
      R0_ol = 10
C TRANSPORTER_dpro.fmg( 181, 206):��������� (RE4) (�������)
      R0_am = -10
C TRANSPORTER_dpro.fmg( 181, 210):��������� (RE4) (�������)
      R0_im = 10
C TRANSPORTER_dpro.fmg( 181, 214):��������� (RE4) (�������)
      R0_um = -10
C TRANSPORTER_dpro.fmg( 181, 218):��������� (RE4) (�������)
      R0_ep = 10
C TRANSPORTER_dpro.fmg( 181, 222):��������� (RE4) (�������)
      R0_is = 0.5
C TRANSPORTER_dpro.fmg( 158, 222):��������� (RE4) (�������)
      R0_us = (-R_at) + R_et
C TRANSPORTER_dpro.fmg( 154, 224):��������
      R0_os = R0_us * R0_is
C TRANSPORTER_dpro.fmg( 162, 224):����������
      R0_es = R_at + R0_os
C TRANSPORTER_dpro.fmg( 167, 224):��������
      R0_up = (-R0_es) + R_ami
C TRANSPORTER_dpro.fmg( 174, 220):��������
      L0_om=R0_up.lt.R0_um
C TRANSPORTER_dpro.fmg( 186, 220):���������� <
      L0_ap=R0_up.gt.R0_ep
C TRANSPORTER_dpro.fmg( 186, 224):���������� >
      R0_ip = (-R0_es) + R_emi
C TRANSPORTER_dpro.fmg( 174, 204):��������
      L0_al=R0_ip.lt.R0_el
C TRANSPORTER_dpro.fmg( 186, 204):���������� <
      L0_il=R0_ip.gt.R0_ol
C TRANSPORTER_dpro.fmg( 186, 208):���������� >
      R0_op = (-R0_es) + R_imi
C TRANSPORTER_dpro.fmg( 174, 212):��������
      L0_ul=R0_op.lt.R0_am
C TRANSPORTER_dpro.fmg( 186, 212):���������� <
      L0_em=R0_op.gt.R0_im
C TRANSPORTER_dpro.fmg( 186, 216):���������� >
      if(L_ese.and..not.L_afe) then
         R_ode=R_ude
      else
         R_ode=max(R0_ide-deltat,0.0)
      endif
      L0_epe=R_ode.gt.0.0
      L_afe=L_ese
C TRANSPORTER_dpro.fmg(  36, 235):������������  �� T,initimph1
      R0_ufe = -10
C TRANSPORTER_dpro.fmg(  62, 202):��������� (RE4) (�������)
      R0_eke = 10
C TRANSPORTER_dpro.fmg(  62, 206):��������� (RE4) (�������)
      R0_oke = -10
C TRANSPORTER_dpro.fmg(  62, 210):��������� (RE4) (�������)
      R0_ale = 10
C TRANSPORTER_dpro.fmg(  62, 214):��������� (RE4) (�������)
      R0_ile = -10
C TRANSPORTER_dpro.fmg(  62, 218):��������� (RE4) (�������)
      R0_ule = 10
C TRANSPORTER_dpro.fmg(  62, 222):��������� (RE4) (�������)
      R0_ere = 0.5
C TRANSPORTER_dpro.fmg(  39, 222):��������� (RE4) (�������)
      R0_ore = (-R_ure) + R_ase
C TRANSPORTER_dpro.fmg(  34, 224):��������
      R0_ire = R0_ore * R0_ere
C TRANSPORTER_dpro.fmg(  42, 224):����������
      R0_are = R_ure + R0_ire
C TRANSPORTER_dpro.fmg(  48, 224):��������
      R0_ime = (-R0_are) + R_api
C TRANSPORTER_dpro.fmg(  54, 220):��������
      L0_ele=R0_ime.lt.R0_ile
C TRANSPORTER_dpro.fmg(  66, 220):���������� <
      L0_ole=R0_ime.gt.R0_ule
C TRANSPORTER_dpro.fmg(  66, 224):���������� >
      R0_ame = (-R0_are) + R_omi
C TRANSPORTER_dpro.fmg(  54, 204):��������
      L0_ofe=R0_ame.lt.R0_ufe
C TRANSPORTER_dpro.fmg(  66, 204):���������� <
      L0_ake=R0_ame.gt.R0_eke
C TRANSPORTER_dpro.fmg(  66, 208):���������� >
      R0_eme = (-R0_are) + R_umi
C TRANSPORTER_dpro.fmg(  54, 212):��������
      L0_ike=R0_eme.lt.R0_oke
C TRANSPORTER_dpro.fmg(  66, 212):���������� <
      L0_uke=R0_eme.gt.R0_ale
C TRANSPORTER_dpro.fmg(  66, 216):���������� >
      L0_ise = L_axi.OR.L_uvi
C TRANSPORTER_dpro.fmg( 300, 214):���
      L_ifi = L_aki.AND.L_ufi
C TRANSPORTER_dpro.fmg(  44, 157):�
      L_ofi = L_eki.AND.L_aki
C TRANSPORTER_dpro.fmg(  44, 163):�
      L_uxi = L_iko.AND.L_eko
C TRANSPORTER_dpro.fmg(  44, 172):�
      L_abo = L_oko.AND.L_iko
C TRANSPORTER_dpro.fmg(  44, 178):�
      R0_oki = 0.0
C TRANSPORTER_dpro.fmg( 373, 242):��������� (RE4) (�������)
      R0_uki = 0.0
C TRANSPORTER_dpro.fmg( 373, 244):��������� (RE4) (�������)
      R_ali = R_imi + R0_uki + R0_oki
C TRANSPORTER_dpro.fmg( 376, 245):��������
      R0_eli = 0.0
C TRANSPORTER_dpro.fmg( 373, 274):��������� (RE4) (�������)
      R0_ili = 0.0
C TRANSPORTER_dpro.fmg( 373, 276):��������� (RE4) (�������)
      R_oli = R_api + R0_ili + R0_eli
C TRANSPORTER_dpro.fmg( 376, 276):��������
      L_edi=L_uti
C TRANSPORTER_dpro.fmg( 392, 184):������,VZZ02
      L_idi=L_axi
C TRANSPORTER_dpro.fmg( 392, 188):������,VZZ01
      L0_odi = L_axi.OR.L_uvi
C TRANSPORTER_dpro.fmg( 270, 239):���
      L0_udi = L_axi.OR.L_uvi
C TRANSPORTER_dpro.fmg( 300, 262):���
      L_afi=L_eki
C TRANSPORTER_dpro.fmg( 326, 156):������,L11
      L_efi=L_olo
C TRANSPORTER_dpro.fmg( 326, 162):������,L12
      R_iki=R_uli
C TRANSPORTER_dpro.fmg( 392, 204):������,VS01
      L_opi=L_oko
C TRANSPORTER_dpro.fmg( 326, 167):������,L8
      L_upi=L_elo
C TRANSPORTER_dpro.fmg( 326, 174):������,L7
      L_eti = L_ivi.AND.L_evi.AND.L_avi
C TRANSPORTER_dpro.fmg( 307, 196):�
      L_eri=L_eti
C TRANSPORTER_dpro.fmg( 326, 196):������,L4
      L_usi = L_axi.AND.L_eti
C TRANSPORTER_dpro.fmg( 311, 212):�
      L_iri=L_usi
C TRANSPORTER_dpro.fmg( 326, 212):������,L5
      L_isi = L_ovi.AND.L_usi
C TRANSPORTER_dpro.fmg( 316, 220):�
      L_epi=L_isi
C TRANSPORTER_dpro.fmg( 329, 222):������,L9
      L_osi = L_eti.AND.L_uti
C TRANSPORTER_dpro.fmg( 311, 182):�
      L_ari=L_osi
C TRANSPORTER_dpro.fmg( 326, 186):������,L6_input
      L_iti =.NOT.(L_oxi.OR.L_ivi.OR.L_axi.OR.L_uti)
C TRANSPORTER_dpro.fmg( 303, 243):���
      L_ori=L_iti
C TRANSPORTER_dpro.fmg( 325, 243):������,L3
      L_oti = L_oxi.AND.L_ixi.AND.L_exi
C TRANSPORTER_dpro.fmg( 306, 278):�
      L_asi=L_oti
C TRANSPORTER_dpro.fmg( 325, 278):������,L2
      L_ati = L_oti.AND.L_axi
C TRANSPORTER_dpro.fmg( 310, 262):�
      L_uri=L_ati
C TRANSPORTER_dpro.fmg( 325, 268):������,L1_input
      L_esi = L_ati.AND.L_ovi
C TRANSPORTER_dpro.fmg( 316, 256):�
      L_ipi=L_esi
C TRANSPORTER_dpro.fmg( 328, 258):������,L10
      L_ido = L_alo.AND.L_uko
C TRANSPORTER_dpro.fmg(  44, 185):�
      L_odo = L_elo.AND.L_alo
C TRANSPORTER_dpro.fmg(  44, 191):�
      L_udo = L_efo.AND.L_ilo
C TRANSPORTER_dpro.fmg( 378, 158):�
      L_afo = L_olo.AND.L_efo
C TRANSPORTER_dpro.fmg( 378, 166):�
      L0_abi = L_ope.AND.L0_ole
C TRANSPORTER_dpro.fmg(  74, 224):�
C label 166  try166=try166-1
      L0_ado = L_ope.AND.L0_ele
C TRANSPORTER_dpro.fmg(  74, 220):�
      L0_ipe = (.NOT.L0_abi).AND.L_ope.AND.(.NOT.L0_ado)
C TRANSPORTER_dpro.fmg(  86, 222):�
      L_ope=(L0_epe.or.L_ope).and..not.(L0_ipe)
      L_upe=.not.L_ope
C TRANSPORTER_dpro.fmg(  50, 233):RS �������,trinith
C sav1=L0_ado
      L0_ado = L_ope.AND.L0_ele
C TRANSPORTER_dpro.fmg(  74, 220):recalc:�
C if(sav1.ne.L0_ado .and. try169.gt.0) goto 169
C sav1=L0_abi
      L0_abi = L_ope.AND.L0_ole
C TRANSPORTER_dpro.fmg(  74, 224):recalc:�
C if(sav1.ne.L0_abi .and. try166.gt.0) goto 166
C sav1=L0_ipe
      L0_ipe = (.NOT.L0_abi).AND.L_ope.AND.(.NOT.L0_ado)
C TRANSPORTER_dpro.fmg(  86, 222):recalc:�
C if(sav1.ne.L0_ipe .and. try172.gt.0) goto 172
      L0_exe = L_ope.AND.L0_ofe
C TRANSPORTER_dpro.fmg(  74, 204):�
      L_ixe = L_adi.OR.L_ibi.OR.L0_exe
C TRANSPORTER_dpro.fmg(  93, 240):���
      L0_oxe = L_ope.AND.L0_uke
C TRANSPORTER_dpro.fmg(  74, 216):�
      L_uxe = L_adi.OR.L_obi.OR.L0_oxe
C TRANSPORTER_dpro.fmg(  93, 246):���
      L0_obo = L_ope.AND.L0_ike
C TRANSPORTER_dpro.fmg(  74, 212):�
      L_ubo = L_ufo.OR.L_ofo.OR.L0_obo
C TRANSPORTER_dpro.fmg(  93, 266):���
      L0_ife = (.NOT.L0_oxe).AND.L_ope.AND.(.NOT.L0_obo)
C TRANSPORTER_dpro.fmg(  86, 214):�
      if(L0_ife.and..not.L_ibe) then
         R_abe=R_ebe
      else
         R_abe=max(R0_ux-deltat,0.0)
      endif
      L_ume=R_abe.gt.0.0
      L_ibe=L0_ife
C TRANSPORTER_dpro.fmg(  98, 214):������������  �� T,initimph3
      L0_ebo = L_ope.AND.L0_ake
C TRANSPORTER_dpro.fmg(  74, 208):�
      L_ibo = L_ufo.OR.L_ifo.OR.L0_ebo
C TRANSPORTER_dpro.fmg(  93, 260):���
      L0_efe = (.NOT.L0_ebo).AND.L_ope.AND.(.NOT.L0_exe)
C TRANSPORTER_dpro.fmg(  86, 206):�
      if(L0_efe.and..not.L_ox) then
         R_ex=R_ix
      else
         R_ex=max(R0_ax-deltat,0.0)
      endif
      L_ome=R_ex.gt.0.0
      L_ox=L0_efe
C TRANSPORTER_dpro.fmg(  98, 206):������������  �� T,initimph4
      if(L0_ipe.and..not.L_ede) then
         R_ube=R_ade
      else
         R_ube=max(R0_obe-deltat,0.0)
      endif
      L_ape=R_ube.gt.0.0
      L_ede=L0_ipe
C TRANSPORTER_dpro.fmg(  98, 222):������������  �� T,initimph2
      L_edo = L_ufo.OR.L_ako.OR.L0_ado
C TRANSPORTER_dpro.fmg(  93, 272):���
      L_ebi = L_adi.OR.L_ubi.OR.L0_abi
C TRANSPORTER_dpro.fmg(  93, 252):���
      L_ur=(L0_ir.or.L_ur).and..not.(L_or)
      L_as=.not.L_ur
C TRANSPORTER_dpro.fmg( 170, 233):RS �������,trinitv
C label 229  try229=try229-1
      L0_ut = L_ur.AND.L0_em
C TRANSPORTER_dpro.fmg( 194, 216):�
      L0_iv = L_ur.AND.L0_ul
C TRANSPORTER_dpro.fmg( 194, 212):�
      L0_ok = (.NOT.L0_ut).AND.L_ur.AND.(.NOT.L0_iv)
C TRANSPORTER_dpro.fmg( 205, 214):�
      if(L0_ok.and..not.L_od) then
         R_ed=R_id
      else
         R_ed=max(R0_ad-deltat,0.0)
      endif
      L_or=R_ed.gt.0.0
      L_od=L0_ok
C TRANSPORTER_dpro.fmg( 213, 214):������������  �� T,initimpv3
      L_ave = L_uv.OR.L_uve.OR.L0_iv
C TRANSPORTER_dpro.fmg( 212, 266):���
      L_use = L_ute.OR.L_ite.OR.L0_ut
C TRANSPORTER_dpro.fmg( 212, 246):���
      L0_ot = L_ur.AND.L0_al
C TRANSPORTER_dpro.fmg( 194, 204):�
      L_ose = L_ute.OR.L_ete.OR.L0_ot
C TRANSPORTER_dpro.fmg( 212, 240):���
      L0_av = L_ur.AND.L0_ap
C TRANSPORTER_dpro.fmg( 194, 224):�
      L_ate = L_ute.OR.L_ote.OR.L0_av
C TRANSPORTER_dpro.fmg( 212, 252):���
      L0_ov = L_ur.AND.L0_om
C TRANSPORTER_dpro.fmg( 194, 220):�
      L_ive = L_uv.OR.L_axe.OR.L0_ov
C TRANSPORTER_dpro.fmg( 212, 272):���
      L0_uk = (.NOT.L0_av).AND.L_ur.AND.(.NOT.L0_ov)
C TRANSPORTER_dpro.fmg( 205, 222):�
      if(L0_uk.and..not.L_if) then
         R_af=R_ef
      else
         R_af=max(R0_ud-deltat,0.0)
      endif
      L_er=R_af.gt.0.0
      L_if=L0_uk
C TRANSPORTER_dpro.fmg( 213, 222):������������  �� T,initimpv2
      L0_ev = L_ur.AND.L0_il
C TRANSPORTER_dpro.fmg( 194, 208):�
      L_eve = L_uv.OR.L_ove.OR.L0_ev
C TRANSPORTER_dpro.fmg( 212, 260):���
      L0_ik = (.NOT.L0_ev).AND.L_ur.AND.(.NOT.L0_ot)
C TRANSPORTER_dpro.fmg( 205, 206):�
      if(L0_ik.and..not.L_u) then
         R_i=R_o
      else
         R_i=max(R0_e-deltat,0.0)
      endif
      L_ar=R_i.gt.0.0
      L_u=L0_ik
C TRANSPORTER_dpro.fmg( 213, 206):������������  �� T,initimpv4
      End
