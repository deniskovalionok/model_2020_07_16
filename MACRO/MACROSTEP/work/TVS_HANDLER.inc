      Interface
      Subroutine TVS_HANDLER(ext_deltat,I_u,I_od,R_ef,R_of
     &,R_ek,R_ok,R_el,R_ol,R_em,R_om,R_ep,R_op,R_er,R_or,R_es
     &,R_os,I_us,R_et,R_it,I_ev,R_ov,R_uv,R_ox,R_ux,I_ebe
     &,I_ade,I_ede,I_ude,I_afe,I_ofe,I_ufe,R_uke,R_ele,I_ule
     &,R_ime,R_ume,R_epe,I_are,R_ire,I_ase,R_ose,R_ate,R_ite
     &,I_eve,R_ove,I_exe,R_uxe,R_ebi,R_obi,I_idi,R_udi,I_ifi
     &,R_aki,R_iki,R_uki,I_oli,R_ami,R_omi,I_umi,R_epi,R_upi
     &,R_eri,R_uri,I_asi,R_isi,R_ati,R_iti,R_avi,I_evi,R_ovi
     &,R_uvi,R_axi,R_exi,R_ixi,R_oxi,R_uxi,R_abo,R_ebo,R_ibo
     &,R_obo,R_ubo,R_ado,R_edo,R_ido,R_odo,R_udo,R_afo,R_efo
     &,I_ifo,I_ofo,I_ufo,I_ako,I_eko,I_iko,I_oko,I_uko,I_alo
     &,I_elo,I_ilo,I_olo,I_ulo,I_amo,R_emo,R_imo,R_omo,R_umo
     &,R_apo,R_epo,R_ipo,R_opo,R_upo,R_aro,R_ero,R_ato,L_oto
     &,I_uto,L_evo,R_uvo,R_axo,R_exo,R_ixo,R_oxo,R_uxo,R_ubu
     &,I_adu,L_idu,R_afu,R_efu,R_ifu,R_ofu,R_ufu,R_aku,R_alu
     &,I_elu,L_olu,R_emu,R_imu,R_omu,R_umu,R_apu,R_epu,R_eru
     &,I_iru,L_uru,R_isu,R_osu,R_usu,R_atu,R_etu,R_itu,R_ivu
     &,I_ovu,L_axu,R_oxu,R_uxu,R_abad,R_ebad,R_ibad,R_obad
     &,R_odad,I_udad,L_efad,R_ufad,R_akad,R_ekad,R_ikad,R_okad
     &,R_ukad,R_ulad,I_amad,L_imad,R_apad,R_epad,R_ipad,R_opad
     &,R_upad,R_arad,R_asad,I_esad,L_osad,R_etad,R_itad,R_otad
     &,R_utad,R_avad,R_evad,R_exad,I_ixad,L_uxad,R_ibed,R_obed
     &,R_ubed,R_aded,R_eded,R_ided,R_ifed,I_ofed,L_aked,R_oked
     &,R_uked,R_aled,R_eled,R_iled,R_oled,R_omed,I_umed,L_eped
     &,R_uped,R_ared,R_ered,R_ired,R_ored,R_ured,R_ised,I_osed
     &,L_ated,R_oted,R_uted,R_aved,R_eved,R_ived,R_oved,R_exed
     &,I_ixed,L_uxed,R_ibid,R_obid,R_ubid,R_adid,R_edid,R_idid
     &,R_afid,I_efid,L_ofid,R_ekid,R_ikid,R_okid,R_ukid,R_alid
     &,R_elid,R_ulid,I_amid,L_imid,R_epid,R_ipid,R_opid,R_upid
     &,R_arid,R_erid,I_irid,I_asid,R_esid)
C |I_u           |2 4 O|FDD10AX004POSTL |��������� �����||
C |I_od          |2 4 O|FDD10AX004POSTR |��������� ������||
C |R_ef          |4 4 K|_lcmpJ3321      |[]�������� ������ �����������|90|
C |R_of          |4 4 K|_lcmpJ3320      |[]�������� ������ �����������|75|
C |R_ek          |4 4 K|_lcmpJ3310      |[]�������� ������ �����������|50|
C |R_ok          |4 4 K|_lcmpJ3309      |[]�������� ������ �����������|35|
C |R_el          |4 4 K|_lcmpJ3304      |[]�������� ������ �����������|20|
C |R_ol          |4 4 K|_lcmpJ3303      |[]�������� ������ �����������|5|
C |R_em          |4 4 K|_lcmpJ3171      |[]�������� ������ �����������|100|
C |R_om          |4 4 K|_lcmpJ3170      |[]�������� ������ �����������|90|
C |R_ep          |4 4 K|_lcmpJ3164      |[]�������� ������ �����������|65|
C |R_op          |4 4 K|_lcmpJ3163      |[]�������� ������ �����������|50|
C |R_er          |4 4 K|_lcmpJ3143      |[]�������� ������ �����������|35|
C |R_or          |4 4 K|_lcmpJ3142      |[]�������� ������ �����������|20|
C |R_es          |4 4 K|_lcmpJ3137      |[]�������� ������ �����������|2|
C |R_os          |4 4 K|_lcmpJ3136      |[]�������� ������ �����������|0|
C |I_us          |2 4 K|_cJ2724         |�������� ��������� �������������|0|
C |R_et          |4 4 K|_lcmpJ2722      |[]�������� ������ �����������|99|
C |R_it          |4 4 K|_lcmpJ2717      |[]�������� ������ �����������|0.1|
C |I_ev          |2 4 K|_cJ2710         |�������� ��������� �������������|0|
C |R_ov          |4 4 K|_lcmpJ2703      |[]�������� ������ �����������|0.1|
C |R_uv          |4 4 K|_lcmpJ2702      |[]�������� ������ �����������|99|
C |R_ox          |4 4 K|_lcmpJ2691      |[]�������� ������ �����������|0.1|
C |R_ux          |4 4 K|_lcmpJ2690      |[]�������� ������ �����������|99|
C |I_ebe         |2 4 K|_cJ2684         |�������� ��������� �������������|0|
C |I_ade         |2 4 K|_cJ2612         |�������� ��������� �������������|1|
C |I_ede         |2 4 K|_cJ2611         |�������� ��������� �������������|2|
C |I_ude         |2 4 K|_cJ2589         |�������� ��������� �������������|1|
C |I_afe         |2 4 K|_cJ2588         |�������� ��������� �������������|2|
C |I_ofe         |2 4 K|_cJ2568         |�������� ��������� �������������|1|
C |I_ufe         |2 4 K|_cJ2566         |�������� ��������� �������������|2|
C |R_uke         |4 4 K|_lcmpJ2529      |[]�������� ������ �����������|100|
C |R_ele         |4 4 K|_lcmpJ2528      |[]�������� ������ �����������|30|
C |I_ule         |2 4 O|FDD10AX008POSCLOSE|?||
C |R_ime         |4 4 K|_lcmpJ2427      |[]�������� ������ �����������|100|
C |R_ume         |4 4 K|_lcmpJ2426      |[]�������� ������ �����������|50|
C |R_epe         |4 4 K|_lcmpJ2419      |[]�������� ������ �����������|50|
C |I_are         |2 4 O|FDD10AX008POSOPEN|��������� �� 90 ��������||
C |R_ire         |4 4 K|_lcmpJ2411      |[]�������� ������ �����������|0|
C |I_ase         |2 4 O|FDD10AX007POSCLOSE|������� ������ �������� � �����������||
C |R_ose         |4 4 K|_lcmpJ2394      |[]�������� ������ �����������|100|
C |R_ate         |4 4 K|_lcmpJ2393      |[]�������� ������ �����������|50|
C |R_ite         |4 4 K|_lcmpJ2386      |[]�������� ������ �����������|50|
C |I_eve         |2 4 O|FDD10AX007POSOPEN|������� ��� �� ����������� � ���������||
C |R_ove         |4 4 K|_lcmpJ2378      |[]�������� ������ �����������|0|
C |I_exe         |2 4 O|FDD10AX006POSCLOSE|?||
C |R_uxe         |4 4 K|_lcmpJ2361      |[]�������� ������ �����������|100|
C |R_ebi         |4 4 K|_lcmpJ2360      |[]�������� ������ �����������|15|
C |R_obi         |4 4 K|_lcmpJ2353      |[]�������� ������ �����������|15|
C |I_idi         |2 4 O|FDD10AX006POSOPEN|����� ����� � ��������� ����������������||
C |R_udi         |4 4 K|_lcmpJ2345      |[]�������� ������ �����������|0|
C |I_ifi         |2 4 O|FDD10AX005POSCLOSE|?||
C |R_aki         |4 4 K|_lcmpJ2328      |[]�������� ������ �����������|100|
C |R_iki         |4 4 K|_lcmpJ2327      |[]�������� ������ �����������|15|
C |R_uki         |4 4 K|_lcmpJ2320      |[]�������� ������ �����������|15|
C |I_oli         |2 4 O|FDD10AX005POSOPEN|����� ����� � ��������� ����������������||
C |R_ami         |4 4 K|_lcmpJ2312      |[]�������� ������ �����������|0|
C |R_omi         |4 4 K|_lcmpJ2254      |[]�������� ������ �����������|30|
C |I_umi         |2 4 O|FDD10AX003POSOPEN|�������� ��� ������ ����||
C |R_epi         |4 4 K|_lcmpJ2247      |[]�������� ������ �����������|0|
C |R_upi         |4 4 K|_lcmpJ2231      |[]�������� ������ �����������|100|
C |R_eri         |4 4 K|_lcmpJ2230      |[]�������� ������ �����������|50|
C |R_uri         |4 4 K|_lcmpJ2223      |[]�������� ������ �����������|50|
C |I_asi         |2 4 O|FDD10AX002POSOPEN|����� ����������� ||
C |R_isi         |4 4 K|_lcmpJ2216      |[]�������� ������ �����������|0|
C |R_ati         |4 4 K|_lcmpJ2200      |[]�������� ������ �����������|100|
C |R_iti         |4 4 K|_lcmpJ2199      |[]�������� ������ �����������|50|
C |R_avi         |4 4 K|_lcmpJ2192      |[]�������� ������ �����������|50|
C |I_evi         |2 4 O|FDD10AX001POSOPEN|����� ���� �������� ����������� ||
C |R_ovi         |4 4 K|_lcmpJ2185      |[]�������� ������ �����������|0|
C |R_uvi         |4 4 K|_cJ1913         |�������� ��������� ����������|10|
C |R_axi         |4 4 K|_cJ1911         |�������� ��������� ����������|10|
C |R_exi         |4 4 K|_cJ1909         |�������� ��������� ����������|10|
C |R_ixi         |4 4 K|_cJ1907         |�������� ��������� ����������|10|
C |R_oxi         |4 4 K|_cJ1905         |�������� ��������� ����������|10|
C |R_uxi         |4 4 K|_cJ1903         |�������� ��������� ����������|10|
C |R_abo         |4 4 K|_cJ1894         |�������� ��������� ����������|0|
C |R_ebo         |4 4 K|_cJ1889         |�������� ��������� ����������|0|
C |R_ibo         |4 4 K|_cJ1885         |�������� ��������� ����������|0|
C |R_obo         |4 4 K|_cJ1852         |�������� ��������� ����������|10|
C |R_ubo         |4 4 K|_cJ1850         |�������� ��������� ����������|10|
C |R_ado         |4 4 K|_cJ1848         |�������� ��������� ����������|10|
C |R_edo         |4 4 K|_cJ1846         |�������� ��������� ����������|10|
C |R_ido         |4 4 K|_cJ1844         |�������� ��������� ����������|10|
C |R_odo         |4 4 K|_cJ1842         |�������� ��������� ����������|10|
C |R_udo         |4 4 K|_cJ1840         |�������� ��������� ����������|10|
C |R_afo         |4 4 K|_cJ1838         |�������� ��������� ����������|10|
C |R_efo         |4 4 K|_cJ1836         |�������� ��������� ����������|10|
C |I_ifo         |2 4 K|_cJ1811         |�������� ��������� �������������|15|
C |I_ofo         |2 4 K|_cJ1807         |�������� ��������� �������������|14|
C |I_ufo         |2 4 K|_cJ1803         |�������� ��������� �������������|13|
C |I_ako         |2 4 K|_cJ1799         |�������� ��������� �������������|12|
C |I_eko         |2 4 K|_cJ1795         |�������� ��������� �������������|11|
C |I_iko         |2 4 K|_cJ1791         |�������� ��������� �������������|10|
C |I_oko         |2 4 K|_cJ1787         |�������� ��������� �������������|9|
C |I_uko         |2 4 K|_cJ1783         |�������� ��������� �������������|8|
C |I_alo         |2 4 K|_cJ1779         |�������� ��������� �������������|7|
C |I_elo         |2 4 K|_cJ1775         |�������� ��������� �������������|6|
C |I_ilo         |2 4 K|_cJ1771         |�������� ��������� �������������|5|
C |I_olo         |2 4 K|_cJ1767         |�������� ��������� �������������|4|
C |I_ulo         |2 4 K|_cJ1763         |�������� ��������� �������������|3|
C |I_amo         |2 4 K|_cJ1749         |�������� ��������� �������������|2|
C |R_emo         |4 4 K|_cJ1684         |�������� ��������� ����������|0|
C |R_imo         |4 4 K|_cJ1678         |�������� ��������� ����������|0|
C |R_omo         |4 4 K|_cJ1666         |�������� ��������� ����������|0|
C |R_umo         |4 4 K|_cJ1656         |�������� ��������� ����������|0|
C |R_apo         |4 4 K|_cJ1646         |�������� ��������� ����������|0|
C |R_epo         |4 4 K|_cJ1635         |�������� ��������� ����������|0|
C |R_ipo         |4 4 K|_cJ1626         |�������� ��������� ����������|0|
C |R_opo         |4 4 K|_cJ1615         |�������� ��������� ����������|0|
C |R_upo         |4 4 K|_cJ1606         |�������� ��������� ����������|0|
C |R_aro         |4 4 K|_cJ1598         |�������� ��������� ����������|0|
C |R_ero         |4 4 K|_cJ1592         |�������� ��������� ����������|0|
C |R_ato         |4 4 K|_cJ1531         |�������� ��������� ����������|0|
C |L_oto         |1 1 I|FDD10AX001START |||
C |I_uto         |2 4 K|_cJ1090         |�������� ��������� �������������|16|
C |L_evo         |1 1 I|FDD10AX016START |||
C |R_uvo         |4 4 K|_lcmpJ1084      |[]�������� ������ �����������|99|
C |R_axo         |4 4 K|_uintJ1075      |����������� ������ ����������� ������|100|
C |R_exo         |4 4 K|_tintJ1075      |[���]�������� T �����������|10.0|
C |R_ixo         |4 4 O|_ointJ1075*     |�������� ������ ����������� |0.0|
C |R_oxo         |4 4 K|_lintJ1075      |����������� ������ ����������� �����|0|
C |R_uxo         |4 4 O|FDD10AX015SPEED |�������� �� ��������� 15||
C |R_ubu         |4 4 O|FDD10AX015POS   |��������� �� ��������� 15||
C |I_adu         |2 4 K|_cJ1044         |�������� ��������� �������������|15|
C |L_idu         |1 1 I|FDD10AX015START |||
C |R_afu         |4 4 K|_lcmpJ1038      |[]�������� ������ �����������|99|
C |R_efu         |4 4 K|_uintJ1029      |����������� ������ ����������� ������|100|
C |R_ifu         |4 4 K|_tintJ1029      |[���]�������� T �����������|10.0|
C |R_ofu         |4 4 O|_ointJ1029*     |�������� ������ ����������� |0.0|
C |R_ufu         |4 4 K|_lintJ1029      |����������� ������ ����������� �����|0|
C |R_aku         |4 4 O|FDD10AX014SPEED |�������� �� ��������� 14||
C |R_alu         |4 4 O|FDD10AX014POS   |��������� �� ��������� 14||
C |I_elu         |2 4 K|_cJ1014         |�������� ��������� �������������|14|
C |L_olu         |1 1 I|FDD10AX014START |||
C |R_emu         |4 4 K|_lcmpJ1008      |[]�������� ������ �����������|99|
C |R_imu         |4 4 K|_uintJ1000      |����������� ������ ����������� ������|100|
C |R_omu         |4 4 K|_tintJ1000      |[���]�������� T �����������|10.0|
C |R_umu         |4 4 O|_ointJ1000*     |�������� ������ ����������� |0.0|
C |R_apu         |4 4 K|_lintJ1000      |����������� ������ ����������� �����|0|
C |R_epu         |4 4 O|FDD10AX013SPEED |�������� �� ��������� 13||
C |R_eru         |4 4 O|FDD10AX013POS   |��������� �� ��������� 13||
C |I_iru         |2 4 K|_cJ987          |�������� ��������� �������������|13|
C |L_uru         |1 1 I|FDD10AX013START |||
C |R_isu         |4 4 K|_lcmpJ981       |[]�������� ������ �����������|99|
C |R_osu         |4 4 K|_uintJ973       |����������� ������ ����������� ������|100|
C |R_usu         |4 4 K|_tintJ973       |[���]�������� T �����������|10.0|
C |R_atu         |4 4 O|_ointJ973*      |�������� ������ ����������� |0.0|
C |R_etu         |4 4 K|_lintJ973       |����������� ������ ����������� �����|0|
C |R_itu         |4 4 O|FDD10AX012SPEED |�������� �� ��������� 12||
C |R_ivu         |4 4 O|FDD10AX012POS   |��������� �� ��������� 12||
C |I_ovu         |2 4 K|_cJ958          |�������� ��������� �������������|12|
C |L_axu         |1 1 I|FDD10AX012START |||
C |R_oxu         |4 4 K|_lcmpJ952       |[]�������� ������ �����������|99|
C |R_uxu         |4 4 K|_uintJ944       |����������� ������ ����������� ������|100|
C |R_abad        |4 4 K|_tintJ944       |[���]�������� T �����������|10.0|
C |R_ebad        |4 4 O|_ointJ944*      |�������� ������ ����������� |0.0|
C |R_ibad        |4 4 K|_lintJ944       |����������� ������ ����������� �����|0|
C |R_obad        |4 4 O|FDD10AX011SPEED |�������� �� ��������� 11||
C |R_odad        |4 4 O|FDD10AX011POS   |��������� �� ��������� 11||
C |I_udad        |2 4 K|_cJ930          |�������� ��������� �������������|11|
C |L_efad        |1 1 I|FDD10AX011START |||
C |R_ufad        |4 4 K|_lcmpJ924       |[]�������� ������ �����������|99|
C |R_akad        |4 4 K|_uintJ916       |����������� ������ ����������� ������|100|
C |R_ekad        |4 4 K|_tintJ916       |[���]�������� T �����������|10.0|
C |R_ikad        |4 4 O|_ointJ916*      |�������� ������ ����������� |0.0|
C |R_okad        |4 4 K|_lintJ916       |����������� ������ ����������� �����|0|
C |R_ukad        |4 4 O|FDD10AX010SPEED |�������� �� ��������� 10||
C |R_ulad        |4 4 O|FDD10AX010POS   |��������� �� ��������� 10||
C |I_amad        |2 4 K|_cJ901          |�������� ��������� �������������|10|
C |L_imad        |1 1 I|FDD10AX010START |||
C |R_apad        |4 4 K|_lcmpJ895       |[]�������� ������ �����������|99|
C |R_epad        |4 4 K|_uintJ887       |����������� ������ ����������� ������|100|
C |R_ipad        |4 4 K|_tintJ887       |[���]�������� T �����������|10.0|
C |R_opad        |4 4 O|_ointJ887*      |�������� ������ ����������� |0.0|
C |R_upad        |4 4 K|_lintJ887       |����������� ������ ����������� �����|0|
C |R_arad        |4 4 O|FDD10AX009SPEED |�������� �� ��������� 9||
C |R_asad        |4 4 O|FDD10AX009POS   |��������� �� ��������� 9||
C |I_esad        |2 4 K|_cJ873          |�������� ��������� �������������|9|
C |L_osad        |1 1 I|FDD10AX009START |||
C |R_etad        |4 4 K|_lcmpJ867       |[]�������� ������ �����������|99|
C |R_itad        |4 4 K|_uintJ858       |����������� ������ ����������� ������|100|
C |R_otad        |4 4 K|_tintJ858       |[���]�������� T �����������|10.0|
C |R_utad        |4 4 O|_ointJ858*      |�������� ������ ����������� |0.0|
C |R_avad        |4 4 K|_lintJ858       |����������� ������ ����������� �����|0|
C |R_evad        |4 4 O|FDD10AX008SPEED |�������� �� ��������� 8||
C |R_exad        |4 4 O|FDD10AX008POS   |��������� �� ��������� 8||
C |I_ixad        |2 4 K|_cJ420          |�������� ��������� �������������|8|
C |L_uxad        |1 1 I|FDD10AX008START |||
C |R_ibed        |4 4 K|_lcmpJ414       |[]�������� ������ �����������|99|
C |R_obed        |4 4 K|_uintJ405       |����������� ������ ����������� ������|100|
C |R_ubed        |4 4 K|_tintJ405       |[���]�������� T �����������|10.0|
C |R_aded        |4 4 O|_ointJ405*      |�������� ������ ����������� |0.0|
C |R_eded        |4 4 K|_lintJ405       |����������� ������ ����������� �����|0|
C |R_ided        |4 4 O|FDD10AX007SPEED |�������� �� ��������� 7||
C |R_ifed        |4 4 O|FDD10AX007POS   |��������� �� ��������� 7||
C |I_ofed        |2 4 K|_cJ386          |�������� ��������� �������������|7|
C |L_aked        |1 1 I|FDD10AX007START |||
C |R_oked        |4 4 K|_lcmpJ380       |[]�������� ������ �����������|99|
C |R_uked        |4 4 K|_uintJ372       |����������� ������ ����������� ������|100|
C |R_aled        |4 4 K|_tintJ372       |[���]�������� T �����������|10.0|
C |R_eled        |4 4 O|_ointJ372*      |�������� ������ ����������� |0.0|
C |R_iled        |4 4 K|_lintJ372       |����������� ������ ����������� �����|0|
C |R_oled        |4 4 O|FDD10AX006SPEED |�������� �� ��������� 6||
C |R_omed        |4 4 O|FDD10AX006POS   |��������� �� ��������� 6||
C |I_umed        |2 4 K|_cJ358          |�������� ��������� �������������|6|
C |L_eped        |1 1 I|FDD10AX006START |||
C |R_uped        |4 4 K|_lcmpJ352       |[]�������� ������ �����������|99|
C |R_ared        |4 4 K|_uintJ343       |����������� ������ ����������� ������|100|
C |R_ered        |4 4 K|_tintJ343       |[���]�������� T �����������|10.0|
C |R_ired        |4 4 O|_ointJ343*      |�������� ������ ����������� |0.0|
C |R_ored        |4 4 K|_lintJ343       |����������� ������ ����������� �����|0|
C |R_ured        |4 4 O|FDD10AX005SPEED |�������� �� ��������� 5||
C |R_ised        |4 4 O|FDD10AX005POS   |��������� �� ��������� 5||
C |I_osed        |2 4 K|_cJ200          |�������� ��������� �������������|5|
C |L_ated        |1 1 I|FDD10AX005START |||
C |R_oted        |4 4 K|_lcmpJ194       |[]�������� ������ �����������|99|
C |R_uted        |4 4 K|_uintJ186       |����������� ������ ����������� ������|100|
C |R_aved        |4 4 K|_tintJ186       |[���]�������� T �����������|10.0|
C |R_eved        |4 4 O|_ointJ186*      |�������� ������ ����������� |0.0|
C |R_ived        |4 4 K|_lintJ186       |����������� ������ ����������� �����|0|
C |R_oved        |4 4 O|FDD10AX004SPEED |�������� �� ��������� 4||
C |R_exed        |4 4 O|FDD10AX004POS   |��������� �� ��������� 4||
C |I_ixed        |2 4 K|_cJ172          |�������� ��������� �������������|4|
C |L_uxed        |1 1 I|FDD10AX004START |||
C |R_ibid        |4 4 K|_lcmpJ166       |[]�������� ������ �����������|99|
C |R_obid        |4 4 K|_uintJ157       |����������� ������ ����������� ������|100|
C |R_ubid        |4 4 K|_tintJ157       |[���]�������� T �����������|10.0|
C |R_adid        |4 4 O|_ointJ157*      |�������� ������ ����������� |0.0|
C |R_edid        |4 4 K|_lintJ157       |����������� ������ ����������� �����|0|
C |R_idid        |4 4 O|FDD10AX003SPEED |������� �� ��������� 3||
C |R_afid        |4 4 O|FDD10AX003POS   |��������� �� ��������� 3||
C |I_efid        |2 4 K|_cJ126          |�������� ��������� �������������|3|
C |L_ofid        |1 1 I|FDD10AX003START |||
C |R_ekid        |4 4 K|_lcmpJ120       |[]�������� ������ �����������|99|
C |R_ikid        |4 4 K|_uintJ111       |����������� ������ ����������� ������|100|
C |R_okid        |4 4 K|_tintJ111       |[���]�������� T �����������|10.0|
C |R_ukid        |4 4 O|_ointJ111*      |�������� ������ ����������� |0.0|
C |R_alid        |4 4 K|_lintJ111       |����������� ������ ����������� �����|0|
C |R_elid        |4 4 O|FDD10AX002SPEED |��������� �� ��������� 2||
C |R_ulid        |4 4 O|FDD10AX002POS   |��������� �� ��������� 2||
C |I_amid        |2 4 K|_cJ35           |�������� ��������� �������������|2|
C |L_imid        |1 1 I|FDD10AX002START |||
C |R_epid        |4 4 K|_lcmpJ29        |[]�������� ������ �����������|99|
C |R_ipid        |4 4 K|_uintJ11        |����������� ������ ����������� ������|100|
C |R_opid        |4 4 K|_tintJ11        |[���]�������� T �����������|10.0|
C |R_upid        |4 4 O|_ointJ11*       |�������� ������ ����������� |0.0|
C |R_arid        |4 4 K|_lintJ11        |����������� ������ ����������� �����|0|
C |R_erid        |4 4 O|FDD10AX001SPEED |�������� �� ��������� 1||
C |I_irid        |2 4 K|_cJ7            |�������� ��������� �������������|1|
C |I_asid        |2 4 O|POS_LINE        |��������� �� ����� (������)||
C |R_esid        |4 4 O|FDD10AX001POS   |��������� �� ��������� 1||

      IMPLICIT NONE
      REAL*4 ext_deltat
      INTEGER*4 I_u,I_od
      REAL*4 R_ef,R_of,R_ek,R_ok,R_el,R_ol,R_em,R_om,R_ep
     &,R_op,R_er,R_or,R_es,R_os
      INTEGER*4 I_us
      REAL*4 R_et,R_it
      INTEGER*4 I_ev
      REAL*4 R_ov,R_uv,R_ox,R_ux
      INTEGER*4 I_ebe,I_ade,I_ede,I_ude,I_afe,I_ofe,I_ufe
      REAL*4 R_uke,R_ele
      INTEGER*4 I_ule
      REAL*4 R_ime,R_ume,R_epe
      INTEGER*4 I_are
      REAL*4 R_ire
      INTEGER*4 I_ase
      REAL*4 R_ose,R_ate,R_ite
      INTEGER*4 I_eve
      REAL*4 R_ove
      INTEGER*4 I_exe
      REAL*4 R_uxe,R_ebi,R_obi
      INTEGER*4 I_idi
      REAL*4 R_udi
      INTEGER*4 I_ifi
      REAL*4 R_aki,R_iki,R_uki
      INTEGER*4 I_oli
      REAL*4 R_ami,R_omi
      INTEGER*4 I_umi
      REAL*4 R_epi,R_upi,R_eri,R_uri
      INTEGER*4 I_asi
      REAL*4 R_isi,R_ati,R_iti,R_avi
      INTEGER*4 I_evi
      REAL*4 R_ovi,R_uvi,R_axi,R_exi,R_ixi,R_oxi,R_uxi,R_abo
     &,R_ebo,R_ibo,R_obo,R_ubo,R_ado,R_edo,R_ido,R_odo,R_udo
     &,R_afo,R_efo
      INTEGER*4 I_ifo,I_ofo,I_ufo,I_ako,I_eko,I_iko,I_oko
     &,I_uko,I_alo,I_elo,I_ilo,I_olo,I_ulo,I_amo
      REAL*4 R_emo,R_imo,R_omo,R_umo,R_apo,R_epo,R_ipo,R_opo
     &,R_upo,R_aro,R_ero,R_ato
      LOGICAL*1 L_oto
      INTEGER*4 I_uto
      LOGICAL*1 L_evo
      REAL*4 R_uvo,R_axo,R_exo,R_ixo,R_oxo,R_uxo,R_ubu
      INTEGER*4 I_adu
      LOGICAL*1 L_idu
      REAL*4 R_afu,R_efu,R_ifu,R_ofu,R_ufu,R_aku,R_alu
      INTEGER*4 I_elu
      LOGICAL*1 L_olu
      REAL*4 R_emu,R_imu,R_omu,R_umu,R_apu,R_epu,R_eru
      INTEGER*4 I_iru
      LOGICAL*1 L_uru
      REAL*4 R_isu,R_osu,R_usu,R_atu,R_etu,R_itu,R_ivu
      INTEGER*4 I_ovu
      LOGICAL*1 L_axu
      REAL*4 R_oxu,R_uxu,R_abad,R_ebad,R_ibad,R_obad,R_odad
      INTEGER*4 I_udad
      LOGICAL*1 L_efad
      REAL*4 R_ufad,R_akad,R_ekad,R_ikad,R_okad,R_ukad,R_ulad
      INTEGER*4 I_amad
      LOGICAL*1 L_imad
      REAL*4 R_apad,R_epad,R_ipad,R_opad,R_upad,R_arad,R_asad
      INTEGER*4 I_esad
      LOGICAL*1 L_osad
      REAL*4 R_etad,R_itad,R_otad,R_utad,R_avad,R_evad,R_exad
      INTEGER*4 I_ixad
      LOGICAL*1 L_uxad
      REAL*4 R_ibed,R_obed,R_ubed,R_aded,R_eded,R_ided,R_ifed
      INTEGER*4 I_ofed
      LOGICAL*1 L_aked
      REAL*4 R_oked,R_uked,R_aled,R_eled,R_iled,R_oled,R_omed
      INTEGER*4 I_umed
      LOGICAL*1 L_eped
      REAL*4 R_uped,R_ared,R_ered,R_ired,R_ored,R_ured,R_ised
      INTEGER*4 I_osed
      LOGICAL*1 L_ated
      REAL*4 R_oted,R_uted,R_aved,R_eved,R_ived,R_oved,R_exed
      INTEGER*4 I_ixed
      LOGICAL*1 L_uxed
      REAL*4 R_ibid,R_obid,R_ubid,R_adid,R_edid,R_idid,R_afid
      INTEGER*4 I_efid
      LOGICAL*1 L_ofid
      REAL*4 R_ekid,R_ikid,R_okid,R_ukid,R_alid,R_elid,R_ulid
      INTEGER*4 I_amid
      LOGICAL*1 L_imid
      REAL*4 R_epid,R_ipid,R_opid,R_upid,R_arid,R_erid
      INTEGER*4 I_irid,I_asid
      REAL*4 R_esid
      End subroutine TVS_HANDLER
      End interface
