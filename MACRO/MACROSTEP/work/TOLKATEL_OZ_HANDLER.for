      Subroutine TOLKATEL_OZ_HANDLER(ext_deltat,R_obi,R_e
     &,R_ed,R_ef,R_of,R_uf,R_ak,R_ek,L_up,R_as,R_ox,L_ux,R_ibe
     &,L_obe,L_ude,L_ife,R_ile,R_ime,L_ume,R_ipe,L_upe,L_ere
     &,L_ese,L_ose,L_use,L_ete,L_ute,L_ive,L_uve,R_ubi,L_idi
     &,L_udi,L_efi,L_ifi,L_aki,L_eki,C20_imi,L_omi,L_api,L_epi
     &,L_upi,L_ari,L_eri,L_iri,L_ori,L_uri,L_asi,L_esi,L_isi
     &,L_osi,L_usi,L_ati,L_eti,L_iti,L_oti,R_uti,R_avi,R_evi
     &,R_ivi,R_uvi,R_exi,I_ixi,R_abo,R_ubo,C20_eko,C20_elo
     &,I_amo,I_omo,R_umo,R_apo,R_epo,R_ipo,I_opo,I_ero,I_eso
     &,I_uso,C20_uto,C8_uvo,R_idu,R_odu,L_udu,R_afu,R_efu
     &,I_ifu,L_aku,L_eku,I_oku,I_elu,L_emu,L_omu,L_umu,L_apu
     &,L_epu,L_ipu,L_iru,L_isu,L_usu,L_atu,R8_itu,R_evu,R8_uvu
     &,L_axu,L_exu,L_oxu,L_uxu,L_ebad,I_ibad,L_adad,L_edad
     &,L_efad,L_ukad,L_ulad)
C |R_obi         |4 4 I|11 mlfpar19     ||0.6|
C |R_e           |4 4 S|_slpbJ3525*     |���������� ��������� ||
C |R_ed          |4 4 I|VX01            |||
C |R_ef          |4 4 I|p_TOLKATEL_XH54 |||
C |R_of          |4 4 S|_slpbJ3478*     |���������� ��������� ||
C |R_uf          |4 4 S|_slpbJ3477*     |���������� ��������� ||
C |R_ak          |4 4 S|_slpbJ3476*     |���������� ��������� ||
C |R_ek          |4 4 S|_slpbJ3470*     |���������� ��������� ||
C |L_up          |1 1 S|_qffJ3426*      |�������� ������ Q RS-��������  |F|
C |R_as          |4 4 I|mlfpar19        |��������� ������������|0.6|
C |R_ox          |4 4 S|_sdelnv2dyn$100Dasha*|[���]���������� ��������� |0|
C |L_ux          |1 1 S|_idelnv2dyn$100Dasha*|[TF]���������� ��������� �������� ������ |f|
C |R_ibe         |4 4 S|_sdelnv2dyn$99Dasha*|[���]���������� ��������� |0|
C |L_obe         |1 1 S|_idelnv2dyn$99Dasha*|[TF]���������� ��������� �������� ������ |f|
C |L_ude         |1 1 S|_qff156*        |�������� ������ Q RS-��������  |F|
C |L_ife         |1 1 S|_qff155*        |�������� ������ Q RS-��������  |F|
C |R_ile         |4 4 I|value_pos       |��� �������� ��������|0.5|
C |R_ime         |4 4 S|_sdelnv2dyn$83Dasha*|[���]���������� ��������� |0|
C |L_ume         |1 1 S|_idelnv2dyn$83Dasha*|[TF]���������� ��������� �������� ������ |f|
C |R_ipe         |4 4 S|_sdelnv2dyn$82Dasha*|[���]���������� ��������� |0|
C |L_upe         |1 1 S|_idelnv2dyn$82Dasha*|[TF]���������� ��������� �������� ������ |f|
C |L_ere         |1 1 S|_qffnv2dyn$106Dasha*|�������� ������ Q RS-��������  |F|
C |L_ese         |1 1 S|_qffnv2dyn$104Dasha*|�������� ������ Q RS-��������  |F|
C |L_ose         |1 1 S|_qffnv2dyn$105Dasha*|�������� ������ Q RS-��������  |F|
C |L_use         |1 1 I|OUTC            |�����||
C |L_ete         |1 1 S|_qffnv2dyn$103Dasha*|�������� ������ Q RS-��������  |F|
C |L_ute         |1 1 S|_qffnv2dyn$102Dasha*|�������� ������ Q RS-��������  |F|
C |L_ive         |1 1 S|_qffnv2dyn$101Dasha*|�������� ������ Q RS-��������  |F|
C |L_uve         |1 1 I|OUTO            |������||
C |R_ubi         |4 4 O|mf_value        |��� ��������� �����|0.5|
C |L_idi         |1 1 S|_qffnv2dyn$87Dasha*|�������� ������ Q RS-��������  |F|
C |L_udi         |1 1 S|_qffnv2dyn$86Dasha*|�������� ������ Q RS-��������  |F|
C |L_efi         |1 1 S|_qffnv2dyn$96Dasha*|�������� ������ Q RS-��������  |F|
C |L_ifi         |1 1 S|_qffnv2dyn$95Dasha*|�������� ������ Q RS-��������  |F|
C |L_aki         |1 1 O|limit_switch_error|||
C |L_eki         |1 1 O|flag_mlf19      |||
C |C20_imi       |3 20 O|task_state      |���������||
C |L_omi         |1 1 I|vlv_kvit        |||
C |L_api         |1 1 I|instr_fault     |||
C |L_epi         |1 1 S|_qffJ2999*      |�������� ������ Q RS-��������  |F|
C |L_upi         |1 1 O|norm            |�����||
C |L_ari         |1 1 I|mlf17           |������ ���������������� ����� �����||
C |L_eri         |1 1 I|mlf16           |������ ������������ ����� �����||
C |L_iri         |1 1 I|mlf15           |������ ���������������� ����� ������||
C |L_ori         |1 1 I|mlf14           |������ ������������ ����� ������||
C |L_uri         |1 1 I|mlf07           |���������� ���� �������||
C |L_asi         |1 1 I|mlf28           |������ ���������������� ��������� �����||
C |L_esi         |1 1 I|mlf09           |����� ��������� �����||
C |L_isi         |1 1 I|mlf08           |����� ��������� ������||
C |L_osi         |1 1 I|mlf26           |������ ���������������� ��������� ������||
C |L_usi         |1 1 I|mlf27           |������ ������������ ��������� �����||
C |L_ati         |1 1 I|mlf25           |������ ������������ ��������� ������||
C |L_eti         |1 1 I|mlf23           |������� ������� �����||
C |L_iti         |1 1 I|mlf22           |����� ����� ��������||
C |L_oti         |1 1 I|mlf19           |���� ������������||
C |R_uti         |4 4 K|_uintT_INT      |����������� ������ ����������� ������|10000|
C |R_avi         |4 4 K|_tintT_INT      |[���]�������� T �����������|1|
C |R_evi         |4 4 O|_ointT_INT*     |�������� ������ ����������� |`p_TOLKATEL_XH54`|
C |R_ivi         |4 4 K|_lintT_INT      |����������� ������ ����������� �����|0.0|
C |R_uvi         |4 4 K|_lcmpJ2903      |[]�������� ������ �����������|`p_TOLKATEL_XH54`|
C |R_exi         |4 4 K|_lcmpJ2873      |[]�������� ������ �����������|5.0|
C |I_ixi         |2 4 O|LWORK           |����� �������||
C |R_abo         |4 4 O|VZ01            |��������� ��������� �� ��� Z||
C |R_ubo         |4 4 O|VZ02            |�������� ����������� ���������||
C |C20_eko       |3 20 O|task_name3      |������� ��������� ��� ������� ������������ ��������||
C |C20_elo       |3 20 O|task_name2      |������� ��������� ��� ������� ������������ ��������||
C |I_amo         |2 4 O|LREADY          |����� ����������||
C |I_omo         |2 4 O|LBUSY           |����� �����||
C |R_umo         |4 4 S|vminit_button_ST*|��������� ������ "������� � �������� �� ���������" |0.0|
C |R_apo         |4 4 I|vminit_button   |������� ������ ������ "������� � �������� �� ���������" |0.0|
C |R_epo         |4 4 S|vmwork_button_ST*|��������� ������ "������� � ������� �� ���������" |0.0|
C |R_ipo         |4 4 I|vmwork_button   |������� ������ ������ "������� � ������� �� ���������" |0.0|
C |I_opo         |2 4 O|state1          |��������� 1||
C |I_ero         |2 4 O|state2          |��������� 2||
C |I_eso         |2 4 O|LWORKO          |����� � �������||
C |I_uso         |2 4 O|LINITC          |����� � ��������||
C |C20_uto       |3 20 O|task_name       |������� ���������||
C |C8_uvo        |3 8 I|task            |������� ���������||
C |R_idu         |4 4 S|vmstop_button_ST*|��������� ������ "������� ���� �� ���������" |0.0|
C |R_odu         |4 4 I|vmstop_button   |������� ������ ������ "������� ���� �� ���������" |0.0|
C |L_udu         |1 1 O|vmstart_button_CMD*|[TF]����� ������ ������� �������� ��������� �� ���������|F|
C |R_afu         |4 4 S|vmstart_button_ST*|��������� ������ "������� �������� ��������� �� ���������" |0.0|
C |R_efu         |4 4 I|vmstart_button  |������� ������ ������ "������� �������� ��������� �� ���������" |0.0|
C |I_ifu         |2 4 O|LERROR          |����� �������������||
C |L_aku         |1 1 I|stop_avt        |������� ���� �� ����������|F|
C |L_eku         |1 1 O|vmstop_button_CMD*|[TF]����� ������ ������� ���� �� ���������|F|
C |I_oku         |2 4 O|LINIT           |����� ��������||
C |I_elu         |2 4 O|LZM             |������ "�������"||
C |L_emu         |1 1 O|vminit_button_CMD*|[TF]����� ������ ������� � �������� �� ���������|F|
C |L_omu         |1 1 O|vmwork_button_CMD*|[TF]����� ������ ������� � ������� �� ���������|F|
C |L_umu         |1 1 I|mlf06           |������������� ������� "�����"||
C |L_apu         |1 1 I|mlf05           |������������� ������� "������"||
C |L_epu         |1 1 I|ulubopen        |���������� ������� �� ���������|F|
C |L_ipu         |1 1 I|ulubclose       |���������� ������� �� ���������|F|
C |L_iru         |1 1 O|block           |||
C |L_isu         |1 1 S|_qff10*         |�������� ������ Q RS-��������  |F|
C |L_usu         |1 1 O|STOP            |�������||
C |L_atu         |1 1 O|nopower         |��� ����������||
C |R8_itu        |4 8 I|voltage         |[��]���������� �� ������||
C |R_evu         |4 4 I|power           |�������� ��������||
C |R8_uvu        |4 8 O|sn_power        |�������� ���� ��|3.0|
C |L_axu         |1 1 I|mlf04           |���������������� �������� �����||
C |L_exu         |1 1 I|YA25            |������� ������� �� ����������|F|
C |L_oxu         |1 1 I|mlf03           |���������������� �������� ������||
C |L_uxu         |1 1 I|YA26            |������� ������� �� ����������|F|
C |L_ebad        |1 1 O|fault           |�������������||
C |I_ibad        |2 4 O|LAM             |������ "�������"||
C |L_adad        |1 1 O|XH53            |�� ����� (���)|F|
C |L_edad        |1 1 O|XH54            |�� ������ (���)|F|
C |L_efad        |1 1 S|_qff2*          |�������� ������ Q RS-��������  |F|
C |L_ukad        |1 1 S|_qff1*          |�������� ������ Q RS-��������  |F|
C |L_ulad        |1 1 I|mlf24           |��� ���������� � ����������||

      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      REAL*4 R_e,R0_i,R0_o,R0_u,R0_ad,R_ed
      LOGICAL*1 L0_id,L0_od,L0_ud,L0_af
      REAL*4 R_ef,R0_if,R_of,R_uf,R_ak,R_ek,R0_ik,R0_ok,R0_uk
     &,R0_al,R0_el,R0_il,R0_ol,R0_ul,R0_am,R0_em
      LOGICAL*1 L0_im
      REAL*4 R0_om,R0_um,R0_ap,R0_ep,R0_ip
      LOGICAL*1 L0_op,L_up
      REAL*4 R0_ar,R0_er,R0_ir,R0_or,R0_ur,R_as,R0_es,R0_is
     &,R0_os,R0_us,R0_at,R0_et
      LOGICAL*1 L0_it,L0_ot
      REAL*4 R0_ut
      LOGICAL*1 L0_av,L0_ev,L0_iv,L0_ov,L0_uv,L0_ax
      REAL*4 R0_ex,R0_ix,R_ox
      LOGICAL*1 L_ux
      REAL*4 R0_abe,R0_ebe,R_ibe
      LOGICAL*1 L_obe,L0_ube,L0_ade,L0_ede,L0_ide,L0_ode,L_ude
     &,L0_afe,L0_efe,L_ife,L0_ofe,L0_ufe,L0_ake
      REAL*4 R0_eke,R0_ike
      LOGICAL*1 L0_oke,L0_uke
      REAL*4 R0_ale,R0_ele,R_ile
      LOGICAL*1 L0_ole,L0_ule
      REAL*4 R0_ame,R0_eme,R_ime
      LOGICAL*1 L0_ome,L_ume
      REAL*4 R0_ape,R0_epe,R_ipe
      LOGICAL*1 L0_ope,L_upe,L0_are,L_ere,L0_ire,L0_ore,L0_ure
     &,L0_ase,L_ese,L0_ise,L_ose,L_use,L0_ate,L_ete,L0_ite
     &,L0_ote,L_ute,L0_ave,L0_eve,L_ive,L0_ove,L_uve
      LOGICAL*1 L0_axe
      REAL*4 R0_exe,R0_ixe,R0_oxe
      LOGICAL*1 L0_uxe
      REAL*4 R0_abi
      LOGICAL*1 L0_ebi,L0_ibi
      REAL*4 R_obi,R_ubi
      LOGICAL*1 L0_adi,L0_edi,L_idi,L0_odi,L_udi,L0_afi,L_efi
     &,L_ifi,L0_ofi,L0_ufi,L_aki,L_eki,L0_iki,L0_oki,L0_uki
     &,L0_ali,L0_eli,L0_ili
      CHARACTER*20 C20_oli,C20_uli,C20_ami,C20_emi,C20_imi
      LOGICAL*1 L_omi,L0_umi,L_api,L_epi,L0_ipi,L0_opi,L_upi
     &,L_ari,L_eri,L_iri,L_ori,L_uri,L_asi,L_esi,L_isi,L_osi
     &,L_usi,L_ati,L_eti,L_iti,L_oti
      REAL*4 R_uti,R_avi,R_evi,R_ivi
      LOGICAL*1 L0_ovi
      REAL*4 R_uvi
      LOGICAL*1 L0_axi
      REAL*4 R_exi
      INTEGER*4 I_ixi,I0_oxi,I0_uxi
      REAL*4 R_abo
      LOGICAL*1 L0_ebo,L0_ibo
      REAL*4 R0_obo,R_ubo,R0_ado,R0_edo,R0_ido,R0_odo,R0_udo
      LOGICAL*1 L0_afo,L0_efo
      CHARACTER*20 C20_ifo,C20_ofo,C20_ufo,C20_ako,C20_eko
     &,C20_iko,C20_oko,C20_uko,C20_alo,C20_elo
      INTEGER*4 I0_ilo,I0_olo
      LOGICAL*1 L0_ulo
      INTEGER*4 I_amo,I0_emo,I0_imo,I_omo
      REAL*4 R_umo,R_apo,R_epo,R_ipo
      INTEGER*4 I_opo,I0_upo,I0_aro,I_ero,I0_iro,I0_oro,I0_uro
     &,I0_aso,I_eso,I0_iso,I0_oso,I_uso
      CHARACTER*20 C20_ato,C20_eto,C20_ito,C20_oto,C20_uto
      CHARACTER*8 C8_avo
      LOGICAL*1 L0_evo
      CHARACTER*8 C8_ivo
      LOGICAL*1 L0_ovo
      CHARACTER*8 C8_uvo
      LOGICAL*1 L0_axo
      INTEGER*4 I0_exo
      LOGICAL*1 L0_ixo
      INTEGER*4 I0_oxo
      LOGICAL*1 L0_uxo
      INTEGER*4 I0_abu,I0_ebu,I0_ibu
      LOGICAL*1 L0_obu
      INTEGER*4 I0_ubu,I0_adu,I0_edu
      REAL*4 R_idu,R_odu
      LOGICAL*1 L_udu
      REAL*4 R_afu,R_efu
      INTEGER*4 I_ifu,I0_ofu,I0_ufu
      LOGICAL*1 L_aku,L_eku,L0_iku
      INTEGER*4 I_oku,I0_uku,I0_alu,I_elu,I0_ilu,I0_olu
      LOGICAL*1 L0_ulu,L0_amu,L_emu,L0_imu,L_omu,L_umu,L_apu
     &,L_epu,L_ipu,L0_opu,L0_upu,L0_aru,L0_eru,L_iru,L0_oru
     &,L0_uru,L0_asu,L0_esu,L_isu,L0_osu,L_usu,L_atu
      REAL*4 R0_etu
      REAL*8 R8_itu
      LOGICAL*1 L0_otu,L0_utu
      REAL*4 R0_avu,R_evu,R0_ivu,R0_ovu
      REAL*8 R8_uvu
      LOGICAL*1 L_axu,L_exu,L0_ixu,L_oxu,L_uxu,L0_abad,L_ebad
      INTEGER*4 I_ibad,I0_obad,I0_ubad
      LOGICAL*1 L_adad,L_edad,L0_idad,L0_odad,L0_udad,L0_afad
     &,L_efad,L0_ifad,L0_ofad,L0_ufad,L0_akad,L0_ekad,L0_ikad
     &,L0_okad,L_ukad,L0_alad,L0_elad,L0_ilad,L0_olad,L_ulad

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      R0_at=R_e
C TOLKATEL_OZ_HANDLER.fmg( 355, 456):pre: ����������� ��
      R0_al=R_uf
C TOLKATEL_OZ_HANDLER.fmg( 399, 397):pre: ����������� ��
      R0_il=R_of
C TOLKATEL_OZ_HANDLER.fmg( 391, 402):pre: ����������� ��
      R0_ir=R_ak
C TOLKATEL_OZ_HANDLER.fmg( 354, 404):pre: ����������� ��
      R0_ep=R_ek
C TOLKATEL_OZ_HANDLER.fmg( 324, 394):pre: ����������� ��
      R0_ebe=R_ibe
C TOLKATEL_OZ_HANDLER.fmg( 251, 344):pre: �������� ��������� ������,nv2dyn$99Dasha
      R0_ix=R_ox
C TOLKATEL_OZ_HANDLER.fmg( 251, 332):pre: �������� ��������� ������,nv2dyn$100Dasha
      R0_epe=R_ipe
C TOLKATEL_OZ_HANDLER.fmg( 236, 308):pre: �������� ��������� ������,nv2dyn$82Dasha
      R0_eme=R_ime
C TOLKATEL_OZ_HANDLER.fmg( 236, 294):pre: �������� ��������� ������,nv2dyn$83Dasha
      R0_i = 1.0
C TOLKATEL_OZ_HANDLER.fmg( 368, 417):��������� (RE4) (�������)
      R0_o = 1.0
C TOLKATEL_OZ_HANDLER.fmg( 368, 422):��������� (RE4) (�������)
      R0_if = R_as * R_ef
C TOLKATEL_OZ_HANDLER.fmg( 361, 436):����������
      R0_u = R0_if + (-R0_i)
C TOLKATEL_OZ_HANDLER.fmg( 372, 418):��������
      L0_id=R_ed.gt.R0_u
C TOLKATEL_OZ_HANDLER.fmg( 352, 418):���������� >
      R0_ad = R0_if + R0_o
C TOLKATEL_OZ_HANDLER.fmg( 372, 424):��������
      L0_od=R_ed.lt.R0_ad
C TOLKATEL_OZ_HANDLER.fmg( 352, 424):���������� <
      L0_ud = L0_od.AND.L0_id
C TOLKATEL_OZ_HANDLER.fmg( 358, 423):�
      L0_af = L_oti.AND.L0_ud
C TOLKATEL_OZ_HANDLER.fmg( 362, 428):�
      R0_ik = 0.0
C TOLKATEL_OZ_HANDLER.fmg( 433, 405):��������� (RE4) (�������),nv2dyn$57Dasha
      R0_uk = 0.99
C TOLKATEL_OZ_HANDLER.fmg( 415, 406):��������� (RE4) (�������),nv2dyn$60Dasha
      if(.NOT.L_eti) then
         R0_em=R0_al
      endif
C TOLKATEL_OZ_HANDLER.fmg( 404, 403):���� � ������������� �������
      R0_ol = 1.0
C TOLKATEL_OZ_HANDLER.fmg( 368, 403):��������� (RE4) (�������)
      R0_ul = 0.0
C TOLKATEL_OZ_HANDLER.fmg( 360, 403):��������� (RE4) (�������)
      R0_um = 1.0e-10
C TOLKATEL_OZ_HANDLER.fmg( 338, 380):��������� (RE4) (�������)
      R0_ar = 0.0
C TOLKATEL_OZ_HANDLER.fmg( 348, 394):��������� (RE4) (�������)
      R0_ip = R0_ep + (-R_as)
C TOLKATEL_OZ_HANDLER.fmg( 328, 386):��������
      R0_es = 0.33
C TOLKATEL_OZ_HANDLER.fmg( 282, 468):��������� (RE4) (�������),nv2dyn$59Dasha
      R0_os = 20
C TOLKATEL_OZ_HANDLER.fmg( 280, 472):��������� (RE4) (�������)
      R0_is = R0_os * R0_es
C TOLKATEL_OZ_HANDLER.fmg( 288, 470):����������
      if(L_uri) then
         R0_ado=R0_is
      else
         R0_ado=R0_os
      endif
C TOLKATEL_OZ_HANDLER.fmg( 293, 472):���� RE IN LO CH7,nv2dyn$40Dasha
      R0_et = 0.000001
C TOLKATEL_OZ_HANDLER.fmg( 148, 422):��������� (RE4) (�������),nv2dyn$61Dasha
      L0_it=R_ile.lt.R0_et
C TOLKATEL_OZ_HANDLER.fmg( 154, 424):���������� <,nv2dyn$46Dasha
      L0_ot = L_asi.AND.L0_it
C TOLKATEL_OZ_HANDLER.fmg( 161, 429):�
      R0_ut = 0.999999
C TOLKATEL_OZ_HANDLER.fmg( 142, 474):��������� (RE4) (�������),nv2dyn$60Dasha
      L0_av=R_ile.gt.R0_ut
C TOLKATEL_OZ_HANDLER.fmg( 150, 475):���������� >,nv2dyn$45Dasha
      L0_ev = L_osi.AND.L0_av
C TOLKATEL_OZ_HANDLER.fmg( 160, 481):�
      R0_ame = 20
C TOLKATEL_OZ_HANDLER.fmg( 228, 298):��������� (RE4) (�������)
      if(.not.L_uri) then
         R_ime=0.0
      elseif(.not.L_ume) then
         R_ime=R0_ame
      else
         R_ime=max(R0_eme-deltat,0.0)
      endif
      L0_ome=L_uri.and.R_ime.le.0.0
      L_ume=L_uri
C TOLKATEL_OZ_HANDLER.fmg( 236, 294):�������� ��������� ������,nv2dyn$83Dasha
      R0_ape = 20
C TOLKATEL_OZ_HANDLER.fmg( 228, 311):��������� (RE4) (�������)
      if(.not.L_uri) then
         R_ipe=0.0
      elseif(.not.L_upe) then
         R_ipe=R0_ape
      else
         R_ipe=max(R0_epe-deltat,0.0)
      endif
      L0_ope=L_uri.and.R_ipe.le.0.0
      L_upe=L_uri
C TOLKATEL_OZ_HANDLER.fmg( 236, 308):�������� ��������� ������,nv2dyn$82Dasha
      R0_ex = 20
C TOLKATEL_OZ_HANDLER.fmg( 245, 335):��������� (RE4) (�������)
      R0_abe = 20
C TOLKATEL_OZ_HANDLER.fmg( 246, 349):��������� (RE4) (�������)
      R0_eke = 0.000001
C TOLKATEL_OZ_HANDLER.fmg( 208, 256):��������� (RE4) (�������),nv2dyn$61Dasha
      R0_ike = 0.999999
C TOLKATEL_OZ_HANDLER.fmg( 208, 262):��������� (RE4) (�������),nv2dyn$60Dasha
      R0_ale = 0.000001
C TOLKATEL_OZ_HANDLER.fmg( 208, 273):��������� (RE4) (�������),nv2dyn$61Dasha
      L0_ole=R_ile.lt.R0_ale
C TOLKATEL_OZ_HANDLER.fmg( 214, 274):���������� <,nv2dyn$46Dasha
      L0_iki = L_asi.AND.L0_ole
C TOLKATEL_OZ_HANDLER.fmg( 245, 272):�
      R0_ele = 0.999999
C TOLKATEL_OZ_HANDLER.fmg( 208, 279):��������� (RE4) (�������),nv2dyn$60Dasha
      L0_ule=R_ile.gt.R0_ele
C TOLKATEL_OZ_HANDLER.fmg( 212, 280):���������� >,nv2dyn$45Dasha
      L0_oki = L_osi.AND.L0_ule
C TOLKATEL_OZ_HANDLER.fmg( 245, 284):�
      R0_exe = 0.01
C TOLKATEL_OZ_HANDLER.fmg(  34, 282):��������� (RE4) (�������),nv2dyn$74Dasha
      R0_ixe = R0_exe + R_obi
C TOLKATEL_OZ_HANDLER.fmg(  38, 281):��������
      R0_oxe = 0.01
C TOLKATEL_OZ_HANDLER.fmg(  34, 288):��������� (RE4) (�������),nv2dyn$74Dasha
      R0_abi = (-R0_oxe) + R_obi
C TOLKATEL_OZ_HANDLER.fmg(  38, 288):��������
      C20_ami = '��������� 2'
C TOLKATEL_OZ_HANDLER.fmg(  36, 306):��������� ���������� CH20 (CH30) (�������)
      C20_emi = '������� ���'
C TOLKATEL_OZ_HANDLER.fmg(  36, 308):��������� ���������� CH20 (CH30) (�������)
      C20_oli = '��������� 1'
C TOLKATEL_OZ_HANDLER.fmg(  52, 306):��������� ���������� CH20 (CH30) (�������)
      L_epi=(L_api.or.L_epi).and..not.(L_omi)
      L0_umi=.not.L_epi
C TOLKATEL_OZ_HANDLER.fmg( 287, 272):RS �������
      I0_uxi = z'01000003'
C TOLKATEL_OZ_HANDLER.fmg( 128, 469):��������� ������������� IN (�������)
      I0_oxi = z'0100000A'
C TOLKATEL_OZ_HANDLER.fmg( 128, 467):��������� ������������� IN (�������)
      R0_obo = 0.0
C TOLKATEL_OZ_HANDLER.fmg( 318, 462):��������� (RE4) (�������)
      R0_udo = 0.0
C TOLKATEL_OZ_HANDLER.fmg( 304, 462):��������� (RE4) (�������)
      C20_ofo = ''
C TOLKATEL_OZ_HANDLER.fmg( 174, 357):��������� ���������� CH20 (CH30) (�������)
      C20_ifo = '�������'
C TOLKATEL_OZ_HANDLER.fmg( 174, 355):��������� ���������� CH20 (CH30) (�������)
      C20_ufo = '������'
C TOLKATEL_OZ_HANDLER.fmg( 194, 358):��������� ���������� CH20 (CH30) (�������)
      C20_oko = ''
C TOLKATEL_OZ_HANDLER.fmg(  80, 359):��������� ���������� CH20 (CH30) (�������)
      C20_iko = '� ��������� 2'
C TOLKATEL_OZ_HANDLER.fmg(  80, 357):��������� ���������� CH20 (CH30) (�������)
      C20_uko = '� ��������� 1'
C TOLKATEL_OZ_HANDLER.fmg( 101, 358):��������� ���������� CH20 (CH30) (�������)
      I0_ilo = z'0100000A'
C TOLKATEL_OZ_HANDLER.fmg( 226, 469):��������� ������������� IN (�������)
      I0_olo = z'01000003'
C TOLKATEL_OZ_HANDLER.fmg( 226, 471):��������� ������������� IN (�������)
      I0_imo = z'0100000A'
C TOLKATEL_OZ_HANDLER.fmg( 226, 490):��������� ������������� IN (�������)
      I0_emo = z'01000003'
C TOLKATEL_OZ_HANDLER.fmg( 226, 488):��������� ������������� IN (�������)
      L_emu=R_apo.ne.R_umo
      R_umo=R_apo
C TOLKATEL_OZ_HANDLER.fmg(  20, 394):���������� ������������� ������
      L_omu=R_ipo.ne.R_epo
      R_epo=R_ipo
C TOLKATEL_OZ_HANDLER.fmg(  24, 450):���������� ������������� ������
      I0_aro = z'01000003'
C TOLKATEL_OZ_HANDLER.fmg( 144, 362):��������� ������������� IN (�������)
      I0_upo = z'01000010'
C TOLKATEL_OZ_HANDLER.fmg( 144, 360):��������� ������������� IN (�������)
      I0_oro = z'01000003'
C TOLKATEL_OZ_HANDLER.fmg( 122, 452):��������� ������������� IN (�������)
      I0_iro = z'01000010'
C TOLKATEL_OZ_HANDLER.fmg( 122, 450):��������� ������������� IN (�������)
      I0_aso = z'01000003'
C TOLKATEL_OZ_HANDLER.fmg( 190, 430):��������� ������������� IN (�������)
      I0_uro = z'0100000A'
C TOLKATEL_OZ_HANDLER.fmg( 190, 428):��������� ������������� IN (�������)
      I0_oso = z'01000003'
C TOLKATEL_OZ_HANDLER.fmg( 198, 380):��������� ������������� IN (�������)
      I0_iso = z'0100000A'
C TOLKATEL_OZ_HANDLER.fmg( 198, 378):��������� ������������� IN (�������)
      I0_alu = z'01000003'
C TOLKATEL_OZ_HANDLER.fmg( 120, 376):��������� ������������� IN (�������)
      I0_uku = z'0100000A'
C TOLKATEL_OZ_HANDLER.fmg( 120, 374):��������� ������������� IN (�������)
      C20_ato = '� ��������'
C TOLKATEL_OZ_HANDLER.fmg(  56, 372):��������� ���������� CH20 (CH30) (�������)
      C20_ito = '� �������'
C TOLKATEL_OZ_HANDLER.fmg(  40, 372):��������� ���������� CH20 (CH30) (�������)
      C20_oto = ''
C TOLKATEL_OZ_HANDLER.fmg(  40, 374):��������� ���������� CH20 (CH30) (�������)
      C8_avo = 'init'
C TOLKATEL_OZ_HANDLER.fmg(  24, 381):��������� ���������� CH8 (�������)
      call chcomp(C8_uvo,C8_avo,L0_evo)
C TOLKATEL_OZ_HANDLER.fmg(  28, 384):���������� ���������
      C8_ivo = 'work'
C TOLKATEL_OZ_HANDLER.fmg(  24, 423):��������� ���������� CH8 (�������)
      call chcomp(C8_uvo,C8_ivo,L0_ovo)
C TOLKATEL_OZ_HANDLER.fmg(  28, 426):���������� ���������
      if(L0_ovo) then
         C20_alo=C20_iko
      else
         C20_alo=C20_oko
      endif
C TOLKATEL_OZ_HANDLER.fmg(  84, 358):���� RE IN LO CH20
      if(L0_evo) then
         C20_elo=C20_uko
      else
         C20_elo=C20_alo
      endif
C TOLKATEL_OZ_HANDLER.fmg( 106, 358):���� RE IN LO CH20
      if(L0_ovo) then
         C20_ako=C20_ifo
      else
         C20_ako=C20_ofo
      endif
C TOLKATEL_OZ_HANDLER.fmg( 178, 356):���� RE IN LO CH20
      if(L0_evo) then
         C20_eko=C20_ufo
      else
         C20_eko=C20_ako
      endif
C TOLKATEL_OZ_HANDLER.fmg( 199, 358):���� RE IN LO CH20
      if(L0_ovo) then
         C20_eto=C20_ito
      else
         C20_eto=C20_oto
      endif
C TOLKATEL_OZ_HANDLER.fmg(  44, 373):���� RE IN LO CH20
      if(L0_evo) then
         C20_uto=C20_ato
      else
         C20_uto=C20_eto
      endif
C TOLKATEL_OZ_HANDLER.fmg(  60, 372):���� RE IN LO CH20
      I0_exo = z'0100008E'
C TOLKATEL_OZ_HANDLER.fmg( 175, 396):��������� ������������� IN (�������)
      I0_oxo = z'01000086'
C TOLKATEL_OZ_HANDLER.fmg( 170, 451):��������� ������������� IN (�������)
      I0_ebu = z'01000003'
C TOLKATEL_OZ_HANDLER.fmg( 158, 462):��������� ������������� IN (�������)
      I0_ibu = z'01000010'
C TOLKATEL_OZ_HANDLER.fmg( 158, 464):��������� ������������� IN (�������)
      I0_edu = z'01000003'
C TOLKATEL_OZ_HANDLER.fmg( 164, 410):��������� ������������� IN (�������)
      I0_adu = z'01000010'
C TOLKATEL_OZ_HANDLER.fmg( 164, 408):��������� ������������� IN (�������)
      L_eku=R_odu.ne.R_idu
      R_idu=R_odu
C TOLKATEL_OZ_HANDLER.fmg(  20, 414):���������� ������������� ������
      L0_iku = L_eku.OR.L_aku
C TOLKATEL_OZ_HANDLER.fmg(  54, 412):���
      L_udu=R_efu.ne.R_afu
      R_afu=R_efu
C TOLKATEL_OZ_HANDLER.fmg(  24, 436):���������� ������������� ������
      L0_imu = L_udu.AND.L0_ovo
C TOLKATEL_OZ_HANDLER.fmg(  36, 434):�
      L0_uru = L_omu.OR.L0_imu
C TOLKATEL_OZ_HANDLER.fmg(  57, 436):���
      L0_ake = L_apu.AND.L0_uru
C TOLKATEL_OZ_HANDLER.fmg( 230, 344):�
      L0_ode = L0_uru.OR.(.NOT.L_umu)
C TOLKATEL_OZ_HANDLER.fmg( 236, 328):���
      L0_amu = L_udu.AND.L0_evo
C TOLKATEL_OZ_HANDLER.fmg(  34, 392):�
      L0_oru = L_emu.OR.L0_amu
C TOLKATEL_OZ_HANDLER.fmg(  58, 394):���
      L0_ufe = L_umu.AND.L0_oru
C TOLKATEL_OZ_HANDLER.fmg( 230, 332):�
      L_ude=(L0_ufe.or.L_ude).and..not.(L0_ode)
      L0_afe=.not.L_ude
C TOLKATEL_OZ_HANDLER.fmg( 242, 330):RS �������,156
      if(.not.L_ude) then
         R_ox=0.0
      elseif(.not.L_ux) then
         R_ox=R0_ex
      else
         R_ox=max(R0_ix-deltat,0.0)
      endif
      L0_ube=L_ude.and.R_ox.le.0.0
      L_ux=L_ude
C TOLKATEL_OZ_HANDLER.fmg( 251, 332):�������� ��������� ������,nv2dyn$100Dasha
      L_efi=(L0_ube.or.L_efi).and..not.(L_uve)
      L0_ade=.not.L_efi
C TOLKATEL_OZ_HANDLER.fmg( 268, 330):RS �������,nv2dyn$96Dasha
      L0_efe = (.NOT.L_apu).OR.L0_oru
C TOLKATEL_OZ_HANDLER.fmg( 236, 340):���
      L_ife=(L0_ake.or.L_ife).and..not.(L0_efe)
      L0_ofe=.not.L_ife
C TOLKATEL_OZ_HANDLER.fmg( 242, 342):RS �������,155
      if(.not.L_ife) then
         R_ibe=0.0
      elseif(.not.L_obe) then
         R_ibe=R0_abe
      else
         R_ibe=max(R0_ebe-deltat,0.0)
      endif
      L0_ede=L_ife.and.R_ibe.le.0.0
      L_obe=L_ife
C TOLKATEL_OZ_HANDLER.fmg( 251, 344):�������� ��������� ������,nv2dyn$99Dasha
      L_ifi=(L0_ede.or.L_ifi).and..not.(L_use)
      L0_ide=.not.L_ifi
C TOLKATEL_OZ_HANDLER.fmg( 268, 342):RS �������,nv2dyn$95Dasha
      I0_ilu = z'0100000E'
C TOLKATEL_OZ_HANDLER.fmg( 194, 400):��������� ������������� IN (�������)
      I0_ofu = z'01000007'
C TOLKATEL_OZ_HANDLER.fmg( 150, 384):��������� ������������� IN (�������)
      I0_ufu = z'01000003'
C TOLKATEL_OZ_HANDLER.fmg( 150, 386):��������� ������������� IN (�������)
      I0_obad = z'0100000E'
C TOLKATEL_OZ_HANDLER.fmg( 188, 456):��������� ������������� IN (�������)
      L0_eru=.false.
C TOLKATEL_OZ_HANDLER.fmg(  70, 416):��������� ���������� (�������)
      L0_aru=.false.
C TOLKATEL_OZ_HANDLER.fmg(  70, 414):��������� ���������� (�������)
      R0_etu = 0.1
C TOLKATEL_OZ_HANDLER.fmg( 260, 361):��������� (RE4) (�������)
      L_atu=R8_itu.lt.R0_etu
C TOLKATEL_OZ_HANDLER.fmg( 264, 362):���������� <
      R0_avu = 0.0
C TOLKATEL_OZ_HANDLER.fmg( 271, 382):��������� (RE4) (�������)
      L0_ite = (.NOT.L_oti).AND.L_ete
C TOLKATEL_OZ_HANDLER.fmg( 109, 277):�
C label 252  try252=try252-1
      L0_ote = L_use.AND.L0_ite
C TOLKATEL_OZ_HANDLER.fmg( 113, 278):�
      L_ete=(L_oti.or.L_ete).and..not.(L0_ote)
      L0_ate=.not.L_ete
C TOLKATEL_OZ_HANDLER.fmg( 104, 274):RS �������,nv2dyn$103Dasha
C sav1=L0_ite
      L0_ite = (.NOT.L_oti).AND.L_ete
C TOLKATEL_OZ_HANDLER.fmg( 109, 277):recalc:�
C if(sav1.ne.L0_ite .and. try252.gt.0) goto 252
      L0_ire = (.NOT.L_oti).AND.L_ere
C TOLKATEL_OZ_HANDLER.fmg( 114, 263):�
C label 256  try256=try256-1
      L0_ore = L_uve.AND.L0_ire
C TOLKATEL_OZ_HANDLER.fmg( 118, 264):�
      L_ere=(L_oti.or.L_ere).and..not.(L0_ore)
      L0_are=.not.L_ere
C TOLKATEL_OZ_HANDLER.fmg( 110, 260):RS �������,nv2dyn$106Dasha
C sav1=L0_ire
      L0_ire = (.NOT.L_oti).AND.L_ere
C TOLKATEL_OZ_HANDLER.fmg( 114, 263):recalc:�
C if(sav1.ne.L0_ire .and. try256.gt.0) goto 256
      L0_uxe=R_ubi.gt.R0_abi
C TOLKATEL_OZ_HANDLER.fmg(  43, 292):���������� >,nv2dyn$84Dasha
C label 261  try261=try261-1
      R_evi=R_evi+deltat/R_avi*R_ubo
      if(R_evi.gt.R_uti) then
         R_evi=R_uti
      elseif(R_evi.lt.R_ivi) then
         R_evi=R_ivi
      endif
C TOLKATEL_OZ_HANDLER.fmg( 346, 450):����������,T_INT
      if(L_eti) then
         R0_us=R0_at
      else
         R0_us=R_evi
      endif
C TOLKATEL_OZ_HANDLER.fmg( 354, 450):���� RE IN LO CH7
      if(L0_af) then
         R_abo=R0_if
      else
         R_abo=R0_us
      endif
C TOLKATEL_OZ_HANDLER.fmg( 366, 448):���� RE IN LO CH7
      L0_axi=R_abo.lt.R_exi
C TOLKATEL_OZ_HANDLER.fmg( 384, 436):���������� <
      L0_uv = L0_axi.AND.(.NOT.L_asi)
C TOLKATEL_OZ_HANDLER.fmg( 428, 435):�
      L_adad = L_esi.OR.L0_uv.OR.L_usi
C TOLKATEL_OZ_HANDLER.fmg( 432, 435):���
      L0_ili = L_isi.AND.L_adad
C TOLKATEL_OZ_HANDLER.fmg( 252, 318):�
      L0_ovi=R_abo.gt.R_uvi
C TOLKATEL_OZ_HANDLER.fmg( 384, 454):���������� >
      L0_ax = L0_ovi.AND.(.NOT.L_osi)
C TOLKATEL_OZ_HANDLER.fmg( 426, 453):�
      L_edad = L_isi.OR.L0_ax.OR.L_ati
C TOLKATEL_OZ_HANDLER.fmg( 432, 453):���
      L0_eli = L_esi.AND.L_edad
C TOLKATEL_OZ_HANDLER.fmg( 252, 314):�
      L0_ali = L0_ope.AND.(.NOT.L_edad)
C TOLKATEL_OZ_HANDLER.fmg( 244, 306):�
      L0_uki = L0_ome.AND.(.NOT.L_adad)
C TOLKATEL_OZ_HANDLER.fmg( 242, 293):�
      L0_axe=R_ubi.lt.R0_ixe
C TOLKATEL_OZ_HANDLER.fmg(  45, 286):���������� <,nv2dyn$85Dasha
      L0_ebi = L0_uxe.AND.L0_axe
C TOLKATEL_OZ_HANDLER.fmg(  50, 291):�
      L0_ibi = L0_ebi.AND.L_oti
C TOLKATEL_OZ_HANDLER.fmg(  62, 283):�
      L_ive=(L_uve.or.L_ive).and..not.(L_adad)
      L0_ove=.not.L_ive
C TOLKATEL_OZ_HANDLER.fmg(  62, 276):RS �������,nv2dyn$101Dasha
      L0_eve = L0_ibi.AND.L_ive
C TOLKATEL_OZ_HANDLER.fmg(  81, 282):�
      L_ute=(L0_eve.or.L_ute).and..not.(L0_ote)
      L0_ave=.not.L_ute
C TOLKATEL_OZ_HANDLER.fmg( 118, 280):RS �������,nv2dyn$102Dasha
      L_ese=(L_use.or.L_ese).and..not.(L_edad)
      L0_ase=.not.L_ese
C TOLKATEL_OZ_HANDLER.fmg(  68, 265):RS �������,nv2dyn$104Dasha
      L0_ise = L0_ibi.AND.L_ese
C TOLKATEL_OZ_HANDLER.fmg(  81, 268):�
      L_ose=(L0_ise.or.L_ose).and..not.(L0_ore)
      L0_ure=.not.L_ose
C TOLKATEL_OZ_HANDLER.fmg( 126, 266):RS �������,nv2dyn$105Dasha
      L_eki = L_ute.OR.L_ose
C TOLKATEL_OZ_HANDLER.fmg( 140, 281):���
      L_udi=(L_edad.or.L_udi).and..not.(L_adad)
      L0_edi=.not.L_udi
C TOLKATEL_OZ_HANDLER.fmg( 115, 304):RS �������,nv2dyn$86Dasha
      L0_afi = L_ati.AND.(.NOT.L_udi)
C TOLKATEL_OZ_HANDLER.fmg( 124, 307):�
      L_idi=(L_adad.or.L_idi).and..not.(L_edad)
      L0_adi=.not.L_idi
C TOLKATEL_OZ_HANDLER.fmg( 114, 293):RS �������,nv2dyn$87Dasha
      L0_odi = L_usi.AND.(.NOT.L_idi)
C TOLKATEL_OZ_HANDLER.fmg( 124, 298):�
      L_aki = L0_afi.OR.L0_odi
C TOLKATEL_OZ_HANDLER.fmg( 130, 306):���
      L0_uke=R_ubi.gt.R0_ike
C TOLKATEL_OZ_HANDLER.fmg( 212, 263):���������� >,nv2dyn$45Dasha
      L0_ufi = L_iri.AND.L0_uke
C TOLKATEL_OZ_HANDLER.fmg( 245, 266):�
      L0_oke=R_ubi.lt.R0_eke
C TOLKATEL_OZ_HANDLER.fmg( 214, 257):���������� <,nv2dyn$46Dasha
      L0_ofi = L_ari.AND.L0_oke
C TOLKATEL_OZ_HANDLER.fmg( 245, 255):�
      L0_ipi =.NOT.(L_oxu.OR.L_axu.OR.L0_ili.OR.L0_eli.OR.L0_ali.OR.L0_u
     &ki.OR.L0_oki.OR.L0_iki.OR.L_eti.OR.L_ori.OR.L_eri.OR.L_eki.OR.L_ak
     &i.OR.L0_ufi.OR.L0_ofi.OR.L_ifi.OR.L_efi)
C TOLKATEL_OZ_HANDLER.fmg( 279, 306):���
      L0_opi =.NOT.(L0_ipi)
C TOLKATEL_OZ_HANDLER.fmg( 290, 278):���
      L_ebad = L0_opi.OR.L_epi
C TOLKATEL_OZ_HANDLER.fmg( 294, 278):���
      L0_upu = L_ebad.OR.L_epu
C TOLKATEL_OZ_HANDLER.fmg(  64, 448):���
      L0_abad = L0_uru.AND.(.NOT.L0_upu).AND.(.NOT.L_apu)
C TOLKATEL_OZ_HANDLER.fmg(  72, 441):�
      L0_elad = L0_abad.OR.L_uxu.OR.L_oxu
C TOLKATEL_OZ_HANDLER.fmg(  76, 434):���
      L0_opu = L_ebad.OR.L_ipu
C TOLKATEL_OZ_HANDLER.fmg(  60, 402):���
      L0_ixu = (.NOT.L0_opu).AND.L0_oru.AND.(.NOT.L_umu)
C TOLKATEL_OZ_HANDLER.fmg(  72, 396):�
      L0_ufad = L0_ixu.OR.L_exu.OR.L_axu
C TOLKATEL_OZ_HANDLER.fmg(  76, 392):���
      L0_esu = L0_elad.OR.L0_ufad
C TOLKATEL_OZ_HANDLER.fmg(  81, 404):���
      L0_asu = L_adad.OR.L0_iku.OR.L_edad
C TOLKATEL_OZ_HANDLER.fmg(  66, 408):���
      L_isu=(L0_asu.or.L_isu).and..not.(L0_esu)
      L0_osu=.not.L_isu
C TOLKATEL_OZ_HANDLER.fmg( 112, 406):RS �������,10
      L_usu = L_aku.OR.L_isu
C TOLKATEL_OZ_HANDLER.fmg( 122, 408):���
      L0_ilad = L_edad.AND.(.NOT.L_isi)
C TOLKATEL_OZ_HANDLER.fmg(  78, 468):�
      L0_ekad = (.NOT.L_usu).AND.L0_ilad
C TOLKATEL_OZ_HANDLER.fmg( 110, 460):�
      L0_idad = L_usu.OR.L_ulad
C TOLKATEL_OZ_HANDLER.fmg( 103, 414):���
      L0_ov = L_edad.AND.L_eri
C TOLKATEL_OZ_HANDLER.fmg(  60, 346):�
      L0_iv = L_adad.AND.L_ori
C TOLKATEL_OZ_HANDLER.fmg(  60, 334):�
      L0_ulu = L0_ov.OR.L0_iv
C TOLKATEL_OZ_HANDLER.fmg(  68, 344):���
      L0_okad = L0_ilad.OR.L0_idad.OR.L0_ufad.OR.L0_ulu
C TOLKATEL_OZ_HANDLER.fmg( 109, 428):���
      L0_olad = (.NOT.L0_ilad).AND.L0_elad
C TOLKATEL_OZ_HANDLER.fmg( 109, 436):�
      L_ukad=(L0_olad.or.L_ukad).and..not.(L0_okad)
      L0_alad=.not.L_ukad
C TOLKATEL_OZ_HANDLER.fmg( 116, 434):RS �������,1
      L0_ikad = (.NOT.L0_ekad).AND.L_ukad
C TOLKATEL_OZ_HANDLER.fmg( 135, 436):�
      L0_ibo = L0_ikad.AND.(.NOT.L_axu)
C TOLKATEL_OZ_HANDLER.fmg( 274, 461):�
      L0_efo = L0_ibo.OR.L_oxu
C TOLKATEL_OZ_HANDLER.fmg( 286, 460):���
      if(L0_efo) then
         R0_ido=R0_ado
      else
         R0_ido=R0_udo
      endif
C TOLKATEL_OZ_HANDLER.fmg( 308, 472):���� RE IN LO CH7
      L0_ofad = L_adad.AND.(.NOT.L_esi)
C TOLKATEL_OZ_HANDLER.fmg(  82, 366):�
      L0_afad = L0_idad.OR.L0_ofad.OR.L0_elad.OR.L0_ulu
C TOLKATEL_OZ_HANDLER.fmg( 109, 384):���
      L0_akad = L0_ufad.AND.(.NOT.L0_ofad)
C TOLKATEL_OZ_HANDLER.fmg( 109, 392):�
      L_efad=(L0_akad.or.L_efad).and..not.(L0_afad)
      L0_ifad=.not.L_efad
C TOLKATEL_OZ_HANDLER.fmg( 116, 390):RS �������,2
      L0_odad = (.NOT.L_usu).AND.L0_ofad
C TOLKATEL_OZ_HANDLER.fmg( 108, 366):�
      L0_udad = L_efad.AND.(.NOT.L0_odad)
C TOLKATEL_OZ_HANDLER.fmg( 135, 390):�
      L0_ebo = L0_udad.AND.(.NOT.L_oxu)
C TOLKATEL_OZ_HANDLER.fmg( 274, 447):�
      L0_afo = L0_ebo.OR.L_axu
C TOLKATEL_OZ_HANDLER.fmg( 286, 446):���
      if(L0_afo) then
         R0_edo=R0_ado
      else
         R0_edo=R0_udo
      endif
C TOLKATEL_OZ_HANDLER.fmg( 308, 454):���� RE IN LO CH7
      R0_odo = R0_ido + (-R0_edo)
C TOLKATEL_OZ_HANDLER.fmg( 314, 465):��������
      if(L_ulad) then
         R_ubo=R0_obo
      else
         R_ubo=R0_odo
      endif
C TOLKATEL_OZ_HANDLER.fmg( 322, 464):���� RE IN LO CH7
      R0_om = R_ubo + R0_ip
C TOLKATEL_OZ_HANDLER.fmg( 334, 388):��������
      R0_ap = R0_om * R0_ip
C TOLKATEL_OZ_HANDLER.fmg( 338, 386):����������
      L0_op=R0_ap.lt.R0_um
C TOLKATEL_OZ_HANDLER.fmg( 342, 386):���������� <
      L_up=(L0_op.or.L_up).and..not.(.NOT.L_oti)
      L0_im=.not.L_up
C TOLKATEL_OZ_HANDLER.fmg( 349, 384):RS �������
      if(L_up) then
         R0_er=R0_ar
      else
         R0_er=R_ubo
      endif
C TOLKATEL_OZ_HANDLER.fmg( 352, 395):���� RE IN LO CH7
      R0_or = R0_ir + R0_er
C TOLKATEL_OZ_HANDLER.fmg( 358, 396):��������
      R0_am=MAX(R0_ul,R0_or)
C TOLKATEL_OZ_HANDLER.fmg( 365, 398):��������
      R0_ur=MIN(R0_ol,R0_am)
C TOLKATEL_OZ_HANDLER.fmg( 373, 398):�������
      if(L_eti) then
         R0_em=R0_ur
      endif
C TOLKATEL_OZ_HANDLER.fmg( 386, 403):���� � ������������� �������
      if(L_up) then
         R0_em=R_as
      endif
C TOLKATEL_OZ_HANDLER.fmg( 344, 403):���� � ������������� �������
      if(L_ori) then
         R0_ok=R0_uk
      else
         R0_ok=R0_em
      endif
C TOLKATEL_OZ_HANDLER.fmg( 418, 407):���� RE IN LO CH7,nv2dyn$56Dasha
      if(L_eri) then
         R_ubi=R0_ik
      else
         R_ubi=R0_ok
      endif
C TOLKATEL_OZ_HANDLER.fmg( 438, 406):���� RE IN LO CH7,nv2dyn$56Dasha
      R_ek=R0_em
C TOLKATEL_OZ_HANDLER.fmg( 324, 394):����������� ��
      R_ak=R0_em
C TOLKATEL_OZ_HANDLER.fmg( 354, 404):����������� ��
      if(L_eti) then
         R0_el=R0_il
      else
         R0_el=R0_ur
      endif
C TOLKATEL_OZ_HANDLER.fmg( 392, 397):���� RE IN LO CH7
      R_of=R0_el
C TOLKATEL_OZ_HANDLER.fmg( 391, 402):����������� ��
      R_uf=R0_el
C TOLKATEL_OZ_HANDLER.fmg( 399, 397):����������� ��
      if(L0_udad) then
         I_uso=I0_iso
      else
         I_uso=I0_oso
      endif
C TOLKATEL_OZ_HANDLER.fmg( 202, 378):���� RE IN LO CH7
      L0_axo = L0_ot.OR.L0_udad
C TOLKATEL_OZ_HANDLER.fmg( 179, 393):���
      L0_otu = L0_ikad.OR.L0_udad
C TOLKATEL_OZ_HANDLER.fmg( 256, 374):���
      L0_utu = L0_otu.AND.(.NOT.L_atu)
C TOLKATEL_OZ_HANDLER.fmg( 272, 374):�
      if(L0_utu) then
         R0_ovu=R_evu
      else
         R0_ovu=R0_avu
      endif
C TOLKATEL_OZ_HANDLER.fmg( 274, 381):���� RE IN LO CH7
      if(L0_ofad) then
         I_opo=I0_upo
      else
         I_opo=I0_aro
      endif
C TOLKATEL_OZ_HANDLER.fmg( 148, 361):���� RE IN LO CH7
      if(L0_ofad) then
         I_oku=I0_uku
      else
         I_oku=I0_alu
      endif
C TOLKATEL_OZ_HANDLER.fmg( 124, 374):���� RE IN LO CH7
      if(L0_ikad) then
         I_eso=I0_uro
      else
         I_eso=I0_aso
      endif
C TOLKATEL_OZ_HANDLER.fmg( 194, 428):���� RE IN LO CH7
      L0_ixo = L0_ev.OR.L0_ikad
C TOLKATEL_OZ_HANDLER.fmg( 170, 442):���
      if(L0_ilad) then
         I_ixi=I0_oxi
      else
         I_ixi=I0_uxi
      endif
C TOLKATEL_OZ_HANDLER.fmg( 131, 468):���� RE IN LO CH7
      if(L0_ilad) then
         I_ero=I0_iro
      else
         I_ero=I0_oro
      endif
C TOLKATEL_OZ_HANDLER.fmg( 126, 450):���� RE IN LO CH7
      L_iru = L_ebad.OR.L0_eru.OR.L0_aru.OR.L0_upu.OR.L0_opu
C TOLKATEL_OZ_HANDLER.fmg(  74, 414):���
      L0_ulo = L_ebad.OR.L_iru
C TOLKATEL_OZ_HANDLER.fmg( 224, 481):���
      if(L0_ulo) then
         I_amo=I0_emo
      else
         I_amo=I0_imo
      endif
C TOLKATEL_OZ_HANDLER.fmg( 229, 489):���� RE IN LO CH7
      if(L_iru) then
         I_omo=I0_ilo
      else
         I_omo=I0_olo
      endif
C TOLKATEL_OZ_HANDLER.fmg( 229, 470):���� RE IN LO CH7
      if(L_ebad) then
         I_ifu=I0_ofu
      else
         I_ifu=I0_ufu
      endif
C TOLKATEL_OZ_HANDLER.fmg( 154, 384):���� RE IN LO CH7
      L_upi = L0_ipi.OR.L_api
C TOLKATEL_OZ_HANDLER.fmg( 290, 284):���
      L0_obu = (.NOT.L0_ev).AND.L_edad
C TOLKATEL_OZ_HANDLER.fmg( 161, 402):�
      if(L0_obu) then
         I0_ubu=I0_adu
      else
         I0_ubu=I0_edu
      endif
C TOLKATEL_OZ_HANDLER.fmg( 168, 408):���� RE IN LO CH7
      if(L0_axo) then
         I0_olu=I0_exo
      else
         I0_olu=I0_ubu
      endif
C TOLKATEL_OZ_HANDLER.fmg( 178, 407):���� RE IN LO CH7
      if(L_ebad) then
         I_elu=I0_ilu
      else
         I_elu=I0_olu
      endif
C TOLKATEL_OZ_HANDLER.fmg( 198, 406):���� RE IN LO CH7
      if(L_edad) then
         C20_uli=C20_ami
      else
         C20_uli=C20_emi
      endif
C TOLKATEL_OZ_HANDLER.fmg(  40, 307):���� RE IN LO CH20
      if(L_adad) then
         C20_imi=C20_oli
      else
         C20_imi=C20_uli
      endif
C TOLKATEL_OZ_HANDLER.fmg(  56, 306):���� RE IN LO CH20
      L0_uxo = L_adad.AND.(.NOT.L0_ot)
C TOLKATEL_OZ_HANDLER.fmg( 155, 456):�
      if(L0_uxo) then
         I0_abu=I0_ebu
      else
         I0_abu=I0_ibu
      endif
C TOLKATEL_OZ_HANDLER.fmg( 161, 463):���� RE IN LO CH7
      if(L0_ixo) then
         I0_ubad=I0_oxo
      else
         I0_ubad=I0_abu
      endif
C TOLKATEL_OZ_HANDLER.fmg( 174, 462):���� RE IN LO CH7
      if(L_ebad) then
         I_ibad=I0_obad
      else
         I_ibad=I0_ubad
      endif
C TOLKATEL_OZ_HANDLER.fmg( 192, 461):���� RE IN LO CH7
      R_e=R0_us
C TOLKATEL_OZ_HANDLER.fmg( 355, 456):����������� ��
      R0_ivu = R8_uvu
C TOLKATEL_OZ_HANDLER.fmg( 270, 390):��������
C label 639  try639=try639-1
      R8_uvu = R0_ovu + R0_ivu
C TOLKATEL_OZ_HANDLER.fmg( 280, 390):��������
C sav1=R0_ivu
      R0_ivu = R8_uvu
C TOLKATEL_OZ_HANDLER.fmg( 270, 390):recalc:��������
C if(sav1.ne.R0_ivu .and. try639.gt.0) goto 639
      End
