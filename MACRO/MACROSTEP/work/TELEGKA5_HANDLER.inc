      Interface
      Subroutine TELEGKA5_HANDLER(ext_deltat,R_imi,I_e,L_i
     &,L_o,L_u,L_ad,I_od,R_ud,R_al,R_el,R_il,R_ol,L_es,R_it
     &,R_ube,L_ade,R_ode,L_ude,L_ake,L_oke,R_ome,R_ope,L_are
     &,R_ore,L_ase,L_ese,L_ise,L_eve,L_ove,L_uve,L_ixe,L_uxe
     &,L_ubi,L_edi,L_idi,L_odi,L_afi,L_ofi,L_eki,L_oki,R_omi
     &,L_epi,L_opi,L_ari,C20_asi,L_esi,L_isi,L_osi,L_usi,L_ati
     &,L_eti,L_iti,L_oti,L_uti,L_avi,L_evi,L_ivi,L_ovi,L_uvi
     &,R_exi,R_oxi,R_uxi,R_abo,R_ebo,R_ibo,R_obo,R_odo,I_uko
     &,I_alo,I_elo,I_amo,I_emo,I_imo,I_epo,I_ipo,I_opo,I_iro
     &,I_oro,I_uro,I_aso,I_uso,I_ato,I_uto,I_ivo,R_ovo,R_uvo
     &,R_axo,R_exo,I_ixo,I_abu,I_adu,I_odu,C20_ofu,C8_oku
     &,R_epu,R_ipu,L_opu,R_upu,R_aru,I_eru,L_uru,L_asu,L_esu
     &,I_osu,I_etu,I_utu,L_ovu,L_axu,L_ixu,L_uxu,L_abad,L_ebad
     &,L_idad,L_afad,L_ifad,L_ofad,R8_akad,R_ukad,R8_ilad
     &,L_olad,L_ulad,L_emad,L_imad,L_umad,I_apad,L_opad,L_upad
     &,L_urad,L_itad,L_ivad)
C |R_imi         |4 4 I|11 mlfpar19     ||0.6|
C |I_e           |2 4 O|NUM_BOAT        |�������� ������� �������||
C |L_i           |1 1 S|_splsJ4028*     |[TF]���������� ��������� ������������� |F|
C |L_o           |1 1 S|_splsJ4025*     |[TF]���������� ��������� ������������� |F|
C |L_u           |1 1 I|RESET_BOAT      |�������� ������� �������||
C |L_ad          |1 1 I|PREPARE_BOAT    |����������� ������� � �������� �������||
C |I_od          |2 4 S|_COUNTER_OUTCOUNTER*|||
C |R_ud          |4 4 S|_slpbJ3978*     |���������� ��������� ||
C |R_al          |4 4 S|_slpbJ3934*     |���������� ��������� ||
C |R_el          |4 4 S|_slpbJ3933*     |���������� ��������� ||
C |R_il          |4 4 S|_slpbJ3932*     |���������� ��������� ||
C |R_ol          |4 4 S|_slpbJ3926*     |���������� ��������� ||
C |L_es          |1 1 S|_qffJ3882*      |�������� ������ Q RS-��������  |F|
C |R_it          |4 4 I|mlfpar19        |��������� ������������|0.6|
C |R_ube         |4 4 S|_sdelnv2dyn$100Dasha*|[���]���������� ��������� |0|
C |L_ade         |1 1 S|_idelnv2dyn$100Dasha*|[TF]���������� ��������� �������� ������ |f|
C |R_ode         |4 4 S|_sdelnv2dyn$99Dasha*|[���]���������� ��������� |0|
C |L_ude         |1 1 S|_idelnv2dyn$99Dasha*|[TF]���������� ��������� �������� ������ |f|
C |L_ake         |1 1 S|_qff156*        |�������� ������ Q RS-��������  |F|
C |L_oke         |1 1 S|_qff155*        |�������� ������ Q RS-��������  |F|
C |R_ome         |4 4 I|value_pos       |��� �������� ��������|0.5|
C |R_ope         |4 4 S|_sdelnv2dyn$83Dasha*|[���]���������� ��������� |0|
C |L_are         |1 1 S|_idelnv2dyn$83Dasha*|[TF]���������� ��������� �������� ������ |f|
C |R_ore         |4 4 S|_sdelnv2dyn$82Dasha*|[���]���������� ��������� |0|
C |L_ase         |1 1 S|_idelnv2dyn$82Dasha*|[TF]���������� ��������� �������� ������ |f|
C |L_ese         |1 1 S|_qffnv2dyn$96Dasha*|�������� ������ Q RS-��������  |F|
C |L_ise         |1 1 S|_qffnv2dyn$95Dasha*|�������� ������ Q RS-��������  |F|
C |L_eve         |1 1 I|vlv_kvit        |||
C |L_ove         |1 1 I|instr_fault     |||
C |L_uve         |1 1 S|_qffJ3417*      |�������� ������ Q RS-��������  |F|
C |L_ixe         |1 1 O|norm            |�����||
C |L_uxe         |1 1 S|_qffnv2dyn$106Dasha*|�������� ������ Q RS-��������  |F|
C |L_ubi         |1 1 S|_qffnv2dyn$104Dasha*|�������� ������ Q RS-��������  |F|
C |L_edi         |1 1 S|_qffnv2dyn$105Dasha*|�������� ������ Q RS-��������  |F|
C |L_idi         |1 1 O|flag_mlf19      |||
C |L_odi         |1 1 I|OUTC            |�����||
C |L_afi         |1 1 S|_qffnv2dyn$103Dasha*|�������� ������ Q RS-��������  |F|
C |L_ofi         |1 1 S|_qffnv2dyn$102Dasha*|�������� ������ Q RS-��������  |F|
C |L_eki         |1 1 S|_qffnv2dyn$101Dasha*|�������� ������ Q RS-��������  |F|
C |L_oki         |1 1 I|OUTO            |������||
C |R_omi         |4 4 O|mf_value        |��� ��������� �����|0.5|
C |L_epi         |1 1 S|_qffnv2dyn$87Dasha*|�������� ������ Q RS-��������  |F|
C |L_opi         |1 1 S|_qffnv2dyn$86Dasha*|�������� ������ Q RS-��������  |F|
C |L_ari         |1 1 O|limit_switch_error|||
C |C20_asi       |3 20 O|task_state      |���������||
C |L_esi         |1 1 I|mlf23           |������� ������� �����||
C |L_isi         |1 1 I|mlf22           |����� ����� ��������||
C |L_osi         |1 1 I|mlf19           |���� ������������||
C |L_usi         |1 1 I|mlf17           |������ ���������������� ����� �����||
C |L_ati         |1 1 I|mlf16           |������ ������������ ����� �����||
C |L_eti         |1 1 I|mlf15           |������ ���������������� ����� ������||
C |L_iti         |1 1 I|mlf14           |������ ������������ ����� ������||
C |L_oti         |1 1 I|mlf07           |���������� ���� �������||
C |L_uti         |1 1 I|mlf28           |������ ���������������� ��������� �����||
C |L_avi         |1 1 I|mlf09           |����� ��������� �����||
C |L_evi         |1 1 I|mlf08           |����� ��������� ������||
C |L_ivi         |1 1 I|mlf26           |������ ���������������� ��������� ������||
C |L_ovi         |1 1 I|mlf27           |������ ������������ ��������� �����||
C |L_uvi         |1 1 I|mlf25           |������ ������������ ��������� ������||
C |R_exi         |4 4 K|_lcmpJ3295      |[]�������� ������ �����������|0.001|
C |R_oxi         |4 4 K|_lcmpJ3291      |[]�������� ������ �����������|15299|
C |R_uxi         |4 4 K|_uintV_INT_     |����������� ������ ����������� ������|15300|
C |R_abo         |4 4 K|_tintV_INT_     |[���]�������� T �����������|1|
C |R_ebo         |4 4 O|_ointV_INT_*    |�������� ������ ����������� |15300|
C |R_ibo         |4 4 K|_lintV_INT_     |����������� ������ ����������� �����|0.0|
C |R_obo         |4 4 O|VX01            |��������� ������� �� ��� X||
C |R_odo         |4 4 O|VX02            |�������� ����������� �������||
C |I_uko         |2 4 K|_lcmpJ2719      |�������� ������ �����������|0|
C |I_alo         |2 4 I|N5              |����� ������� � ������ 5||
C |I_elo         |2 4 O|LS5             |����� ������� ������� � ������ 5||
C |I_amo         |2 4 K|_lcmpJ2708      |�������� ������ �����������|0|
C |I_emo         |2 4 I|N4              |����� ������� � ������ 4||
C |I_imo         |2 4 O|LS4             |����� ������� ������� � ������ 4||
C |I_epo         |2 4 K|_lcmpJ2697      |�������� ������ �����������|0|
C |I_ipo         |2 4 I|N3              |����� ������� � ������ 3||
C |I_opo         |2 4 O|LS3             |����� ������� ������� � ������ 3||
C |I_iro         |2 4 K|_lcmpJ2682      |�������� ������ �����������|0|
C |I_oro         |2 4 I|N2              |����� ������� � ������ 2||
C |I_uro         |2 4 O|LS2             |����� ������� ������� � ������ 2||
C |I_aso         |2 4 O|LS1             |����� ������� ������� � ������ 1||
C |I_uso         |2 4 K|_lcmpJ2670      |�������� ������ �����������|0|
C |I_ato         |2 4 I|N1              |����� ������� � ������ 1||
C |I_uto         |2 4 O|LREADY          |����� ����������||
C |I_ivo         |2 4 O|LBUSY           |����� �����||
C |R_ovo         |4 4 S|vmpos1_button_ST*|��������� ������ "������� � ��������� 1 �� ���������" |0.0|
C |R_uvo         |4 4 I|vmpos1_button   |������� ������ ������ "������� � ��������� 1 �� ���������" |0.0|
C |R_axo         |4 4 S|vmpos2_button_ST*|��������� ������ "������� � ��������� 2 �� ���������" |0.0|
C |R_exo         |4 4 I|vmpos2_button   |������� ������ ������ "������� � ��������� 2 �� ���������" |0.0|
C |I_ixo         |2 4 O|state1          |��������� 1||
C |I_abu         |2 4 O|state2          |��������� 2||
C |I_adu         |2 4 O|LPOS2O          |����� � ��������� 2||
C |I_odu         |2 4 O|LPOS1C          |����� � ��������� 1||
C |C20_ofu       |3 20 O|task_name       |������� ���������||
C |C8_oku        |3 8 I|task            |������� ���������||
C |R_epu         |4 4 S|vmstop_button_ST*|��������� ������ "������� ���� �� ���������" |0.0|
C |R_ipu         |4 4 I|vmstop_button   |������� ������ ������ "������� ���� �� ���������" |0.0|
C |L_opu         |1 1 O|vmstart_button_CMD*|[TF]����� ������ ������� �������� ��������� �� ���������|F|
C |R_upu         |4 4 S|vmstart_button_ST*|��������� ������ "������� �������� ��������� �� ���������" |0.0|
C |R_aru         |4 4 I|vmstart_button  |������� ������ ������ "������� �������� ��������� �� ���������" |0.0|
C |I_eru         |2 4 O|LERROR          |����� �������������||
C |L_uru         |1 1 I|YA27            |������� ������� �� ����������|F|
C |L_asu         |1 1 I|stop_avt        |������� ���� �� ����������|F|
C |L_esu         |1 1 O|vmstop_button_CMD*|[TF]����� ������ ������� ���� �� ���������|F|
C |I_osu         |2 4 O|LPOS1           |����� ��������� 1||
C |I_etu         |2 4 O|LPOS2           |����� ��������� 2||
C |I_utu         |2 4 O|LZM             |������ "�������"||
C |L_ovu         |1 1 O|vmpos1_button_CMD*|[TF]����� ������ ������� � ��������� 1 �� ���������|F|
C |L_axu         |1 1 O|vmpos2_button_CMD*|[TF]����� ������ ������� � ��������� 2 �� ���������|F|
C |L_ixu         |1 1 I|mlf06           |������������� ������� "�����"||
C |L_uxu         |1 1 I|mlf05           |������������� ������� "������"||
C |L_abad        |1 1 I|ulubopen        |���������� ������� �� ���������|F|
C |L_ebad        |1 1 I|ulubclose       |���������� ������� �� ���������|F|
C |L_idad        |1 1 O|block           |||
C |L_afad        |1 1 S|_qff10*         |�������� ������ Q RS-��������  |F|
C |L_ifad        |1 1 O|STOP            |�������||
C |L_ofad        |1 1 O|nopower         |��� ����������||
C |R8_akad       |4 8 I|voltage         |[��]���������� �� ������||
C |R_ukad        |4 4 I|power           |�������� ��������||
C |R8_ilad       |4 8 O|sn_power        |�������� ���� ��|3.0|
C |L_olad        |1 1 I|mlf04           |���������������� �������� �����||
C |L_ulad        |1 1 I|YA25            |������� ������� �� ����������|F|
C |L_emad        |1 1 I|mlf03           |���������������� �������� ������||
C |L_imad        |1 1 I|YA26            |������� ������� �� ����������|F|
C |L_umad        |1 1 O|fault           |�������������||
C |I_apad        |2 4 O|LAM             |������ "�������"||
C |L_opad        |1 1 O|XH53            |�� ����� (���)|F|
C |L_upad        |1 1 O|XH54            |�� ������ (���)|F|
C |L_urad        |1 1 S|_qff2*          |�������� ������ Q RS-��������  |F|
C |L_itad        |1 1 S|_qff1*          |�������� ������ Q RS-��������  |F|
C |L_ivad        |1 1 I|mlf24           |��� ���������� � ����������||

      IMPLICIT NONE
      REAL*4 ext_deltat
      INTEGER*4 I_e
      LOGICAL*1 L_i,L_o,L_u,L_ad
      INTEGER*4 I_od
      REAL*4 R_ud,R_al,R_el,R_il,R_ol
      LOGICAL*1 L_es
      REAL*4 R_it,R_ube
      LOGICAL*1 L_ade
      REAL*4 R_ode
      LOGICAL*1 L_ude,L_ake,L_oke
      REAL*4 R_ome,R_ope
      LOGICAL*1 L_are
      REAL*4 R_ore
      LOGICAL*1 L_ase,L_ese,L_ise,L_eve,L_ove,L_uve,L_ixe
     &,L_uxe,L_ubi,L_edi,L_idi,L_odi,L_afi,L_ofi,L_eki,L_oki
      REAL*4 R_imi,R_omi
      LOGICAL*1 L_epi,L_opi,L_ari
      CHARACTER*20 C20_asi
      LOGICAL*1 L_esi,L_isi,L_osi,L_usi,L_ati,L_eti,L_iti
     &,L_oti,L_uti,L_avi,L_evi,L_ivi,L_ovi,L_uvi
      REAL*4 R_exi,R_oxi,R_uxi,R_abo,R_ebo,R_ibo,R_obo,R_odo
      INTEGER*4 I_uko,I_alo,I_elo,I_amo,I_emo,I_imo,I_epo
     &,I_ipo,I_opo,I_iro,I_oro,I_uro,I_aso,I_uso,I_ato,I_uto
     &,I_ivo
      REAL*4 R_ovo,R_uvo,R_axo,R_exo
      INTEGER*4 I_ixo,I_abu,I_adu,I_odu
      CHARACTER*20 C20_ofu
      CHARACTER*8 C8_oku
      REAL*4 R_epu,R_ipu
      LOGICAL*1 L_opu
      REAL*4 R_upu,R_aru
      INTEGER*4 I_eru
      LOGICAL*1 L_uru,L_asu,L_esu
      INTEGER*4 I_osu,I_etu,I_utu
      LOGICAL*1 L_ovu,L_axu,L_ixu,L_uxu,L_abad,L_ebad,L_idad
     &,L_afad,L_ifad,L_ofad
      REAL*8 R8_akad
      REAL*4 R_ukad
      REAL*8 R8_ilad
      LOGICAL*1 L_olad,L_ulad,L_emad,L_imad,L_umad
      INTEGER*4 I_apad
      LOGICAL*1 L_opad,L_upad,L_urad,L_itad,L_ivad
      End subroutine TELEGKA5_HANDLER
      End interface
