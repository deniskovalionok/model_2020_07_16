      Interface
      Subroutine TRANSPORTER_dpro(ext_deltat,R_i,R_o,L_u,R_ed
     &,R_id,L_od,R_af,R_ef,L_if,R_uf,R_ak,L_ek,L_ar,L_er,L_or
     &,L_ur,L_as,R_at,R_et,L_it,L_uv,R_ex,R_ix,L_ox,R_abe
     &,R_ebe,L_ibe,R_ube,R_ade,L_ede,R_ode,R_ude,L_afe,L_ome
     &,L_ume,L_ape,L_ope,L_upe,R_ure,R_ase,L_ese,L_ose,L_use
     &,L_ate,L_ete,L_ite,L_ote,L_ute,L_ave,L_eve,L_ive,L_ove
     &,L_uve,L_axe,L_ixe,L_uxe,L_ebi,L_ibi,L_obi,L_ubi,L_adi
     &,L_edi,L_idi,L_afi,L_efi,L_ifi,L_ofi,L_ufi,L_aki,L_eki
     &,R_iki,R_ali,R_oli,R_uli,R_ami,R_emi,R_imi,R_omi,R_umi
     &,R_api,L_epi,L_ipi,L_opi,L_upi,L_ari,L_eri,L_iri,L_ori
     &,L_uri,L_asi,L_esi,L_isi,L_osi,L_usi,L_ati,L_eti,L_iti
     &,L_oti,L_uti,L_avi,L_evi,L_ivi,L_ovi,L_uvi,L_axi,L_exi
     &,L_ixi,L_oxi,L_uxi,L_abo,L_ibo,L_ubo,L_edo,L_ido,L_odo
     &,L_udo,L_afo,L_efo,L_ifo,L_ofo,L_ufo,L_ako,L_eko,L_iko
     &,L_oko,L_uko,L_alo,L_elo,L_ilo,L_olo)
C |R_i           |4 4 S|_simpinitimpv4* |[���]���������� ��������� ������������� |0.0|
C |R_o           |4 4 K|_timpinitimpv4  |[���]������������ �������� �������������|0.4|
C |L_u           |1 1 S|_limpinitimpv4* |[TF]���������� ��������� ������������� |F|
C |R_ed          |4 4 S|_simpinitimpv3* |[���]���������� ��������� ������������� |0.0|
C |R_id          |4 4 K|_timpinitimpv3  |[���]������������ �������� �������������|0.4|
C |L_od          |1 1 S|_limpinitimpv3* |[TF]���������� ��������� ������������� |F|
C |R_af          |4 4 S|_simpinitimpv2* |[���]���������� ��������� ������������� |0.0|
C |R_ef          |4 4 K|_timpinitimpv2  |[���]������������ �������� �������������|0.4|
C |L_if          |1 1 S|_limpinitimpv2* |[TF]���������� ��������� ������������� |F|
C |R_uf          |4 4 S|_simpinitimpv1* |[���]���������� ��������� ������������� |0.0|
C |R_ak          |4 4 K|_timpinitimpv1  |[���]������������ �������� �������������|0.4|
C |L_ek          |1 1 S|_limpinitimpv1* |[TF]���������� ��������� ������������� |F|
C |L_ar          |1 1 O|C7_stop_avt     |����||
C |L_er          |1 1 O|C8_stop_avt     |����||
C |L_or          |1 1 O|C6_stop_avt     |����||
C |L_ur          |1 1 S|_qfftrinitv*    |�������� ������ Q RS-��������  |F|
C |L_as          |1 1 O|VINITCMPLT      |||
C |R_at          |4 4 I|miny            |||
C |R_et          |4 4 I|maxy            |||
C |L_it          |1 1 I|VINIT           |������� � �������� �� ���������||
C |L_uv          |1 1 I|DOWN            |������� ����||
C |R_ex          |4 4 S|_simpinitimph4* |[���]���������� ��������� ������������� |0.0|
C |R_ix          |4 4 K|_timpinitimph4  |[���]������������ �������� �������������|0.4|
C |L_ox          |1 1 S|_limpinitimph4* |[TF]���������� ��������� ������������� |F|
C |R_abe         |4 4 S|_simpinitimph3* |[���]���������� ��������� ������������� |0.0|
C |R_ebe         |4 4 K|_timpinitimph3  |[���]������������ �������� �������������|0.4|
C |L_ibe         |1 1 S|_limpinitimph3* |[TF]���������� ��������� ������������� |F|
C |R_ube         |4 4 S|_simpinitimph2* |[���]���������� ��������� ������������� |0.0|
C |R_ade         |4 4 K|_timpinitimph2  |[���]������������ �������� �������������|0.4|
C |L_ede         |1 1 S|_limpinitimph2* |[TF]���������� ��������� ������������� |F|
C |R_ode         |4 4 S|_simpinitimph1* |[���]���������� ��������� ������������� |0.0|
C |R_ude         |4 4 K|_timpinitimph1  |[���]������������ �������� �������������|0.4|
C |L_afe         |1 1 S|_limpinitimph1* |[TF]���������� ��������� ������������� |F|
C |L_ome         |1 1 O|C5_stop_avt     |����||
C |L_ume         |1 1 O|C4_stop_avt     |����||
C |L_ape         |1 1 O|C3_stop_avt     |����||
C |L_ope         |1 1 S|_qfftrinith*    |�������� ������ Q RS-��������  |F|
C |L_upe         |1 1 O|INITCMPLT       |||
C |R_ure         |4 4 I|minx            |||
C |R_ase         |4 4 I|maxx            |||
C |L_ese         |1 1 I|INIT            |INIT �� �����������||
C |L_ose         |1 1 O|C7YA26          |������� �����||
C |L_use         |1 1 O|C6YA25          |������� �����||
C |L_ate         |1 1 O|C8YA25          |������� � �������||
C |L_ete         |1 1 I|C7YA26_B        |������� �����||
C |L_ite         |1 1 I|C6YA25_B        |������� �����||
C |L_ote         |1 1 I|C8YA25_B        |������� � �������||
C |L_ute         |1 1 I|UP              |������� �����||
C |L_ave         |1 1 O|C6YA26          |������� ����||
C |L_eve         |1 1 O|C7YA25          |������� ����||
C |L_ive         |1 1 O|C8YA26          |������� � �������||
C |L_ove         |1 1 I|C7YA25_B        |������� ����||
C |L_uve         |1 1 I|C6YA26_B        |������� ����||
C |L_axe         |1 1 I|C8YA26_B        |������� � �������||
C |L_ixe         |1 1 O|C5YA26          |������� � �������||
C |L_uxe         |1 1 O|C4YA25          |������� � �������||
C |L_ebi         |1 1 O|C3YA25          |������� � �������||
C |L_ibi         |1 1 I|C5YA26_B        |������� � �������||
C |L_obi         |1 1 I|C4YA25_B        |������� � �������||
C |L_ubi         |1 1 I|C3YA25_B        |������� � �������||
C |L_adi         |1 1 I|PREV            |������� � ���� ����||
C |L_edi         |1 1 O|VZZ02           |��������� ������� �����||
C |L_idi         |1 1 O|VZZ01           |��������� ������� ������||
C |L_afi         |1 1 O|L11             |������ ������||
C |L_efi         |1 1 O|L12             |��������� ����������||
C |L_ifi         |1 1 O|C11_uluop       |������� ������||
C |L_ofi         |1 1 O|C11_ulucl       |������� ������||
C |L_ufi         |1 1 I|C11_CBC         |������ ������||
C |L_aki         |1 1 I|C11B            |�������/������� ������||
C |L_eki         |1 1 I|C11_CBO         |������ ������||
C |R_iki         |4 4 O|VS01            |��������� �����������||
C |R_ali         |4 4 O|VZ01            |��������� ������������||
C |R_oli         |4 4 O|VX01            |��������� ������������||
C |R_uli         |4 4 I|C10_VZ01        |��������� �����������||
C |R_ami         |4 4 I|C8VX01          |��������� �������||
C |R_emi         |4 4 I|C7VX01          |��������� �������||
C |R_imi         |4 4 I|C6VX01          |��������� �������||
C |R_omi         |4 4 I|C5VX01          |��������� �������||
C |R_umi         |4 4 I|C4VX01          |��������� �������||
C |R_api         |4 4 I|C3VX01          |��������� �������||
C |L_epi         |1 1 O|L9              |���� ���� - �����||
C |L_ipi         |1 1 O|L10             |���� ���� - �����||
C |L_opi         |1 1 O|L8              |������ ������||
C |L_upi         |1 1 O|L7              |��������� ������||
C |L_ari         |1 1 O|L6_input        |���� ���� - �������||
C |L_eri         |1 1 O|L4              |��������� ����||
C |L_iri         |1 1 O|L5              |���� ���� - �����||
C |L_ori         |1 1 O|L3              |�������� ���������||
C |L_uri         |1 1 O|L1_input        |���������� ����||
C |L_asi         |1 1 O|L2              |���������� ����||
C |L_esi         |1 1 O|L10_input       |���� ���� - �����||
C |L_isi         |1 1 O|L9_input        |���� ���� - �����||
C |L_osi         |1 1 O|L6              |���� ���� - �������||
C |L_usi         |1 1 O|L5_input        |���� ���� - �����||
C |L_ati         |1 1 O|L1              |���������� ����||
C |L_eti         |1 1 O|L4_input        |��������� ����||
C |L_iti         |1 1 O|L3_input        |�������� ���������||
C |L_oti         |1 1 O|L2_input        |���������� ����||
C |L_uti         |1 1 I|C6XH54          |������� ��������||
C |L_avi         |1 1 I|C5XH53          |������ ��������||
C |L_evi         |1 1 I|C4XH54          |����� ��������||
C |L_ivi         |1 1 I|C3XH54          |����� ��������||
C |L_ovi         |1 1 I|C8XH53          |������ ��������||
C |L_uvi         |1 1 I|C7XH54          |������� ��������||
C |L_axi         |1 1 I|C6XH53          |������ ��������||
C |L_exi         |1 1 I|C5XH54          |����� ��������||
C |L_ixi         |1 1 I|C4XH53          |������ ��������||
C |L_oxi         |1 1 I|C3XH53          |������ ��������||
C |L_uxi         |1 1 O|C2_uluop        |������� ������||
C |L_abo         |1 1 O|C2_ulucl        |������� ������||
C |L_ibo         |1 1 O|C5YA25          |������� � �������||
C |L_ubo         |1 1 O|C4YA26          |������� � �������||
C |L_edo         |1 1 O|C3YA26          |������� � �������||
C |L_ido         |1 1 O|C1_uluop        |������� ����||
C |L_odo         |1 1 O|C1_ulucl        |������� ����||
C |L_udo         |1 1 O|C10_YA26        |��������� ������� ����||
C |L_afo         |1 1 O|C10_YA25        |��������� ������� �����||
C |L_efo         |1 1 I|C10B            |��������� ����������||
C |L_ifo         |1 1 I|C5YA25_B        |������� � �������||
C |L_ofo         |1 1 I|C4YA26_B        |������� � �������||
C |L_ufo         |1 1 I|NEXT            |������� � ���� ����||
C |L_ako         |1 1 I|C3YA26_B        |������� � �������||
C |L_eko         |1 1 I|C2_CBC          |������ ������||
C |L_iko         |1 1 I|C2B             |�������/������� ������||
C |L_oko         |1 1 I|C2_CBO          |������ ������||
C |L_uko         |1 1 I|C1_CBC          |���� ������||
C |L_alo         |1 1 I|C1B             |�������/������� ����||
C |L_elo         |1 1 I|C1_CBO          |���� ������||
C |L_ilo         |1 1 I|C10XH53         |��������� �� ����������||
C |L_olo         |1 1 I|C10XH54         |��������� ����������||

      IMPLICIT NONE
      REAL*4 ext_deltat
      REAL*4 R_i,R_o
      LOGICAL*1 L_u
      REAL*4 R_ed,R_id
      LOGICAL*1 L_od
      REAL*4 R_af,R_ef
      LOGICAL*1 L_if
      REAL*4 R_uf,R_ak
      LOGICAL*1 L_ek,L_ar,L_er,L_or,L_ur,L_as
      REAL*4 R_at,R_et
      LOGICAL*1 L_it,L_uv
      REAL*4 R_ex,R_ix
      LOGICAL*1 L_ox
      REAL*4 R_abe,R_ebe
      LOGICAL*1 L_ibe
      REAL*4 R_ube,R_ade
      LOGICAL*1 L_ede
      REAL*4 R_ode,R_ude
      LOGICAL*1 L_afe,L_ome,L_ume,L_ape,L_ope,L_upe
      REAL*4 R_ure,R_ase
      LOGICAL*1 L_ese,L_ose,L_use,L_ate,L_ete,L_ite,L_ote
     &,L_ute,L_ave,L_eve,L_ive,L_ove,L_uve,L_axe,L_ixe,L_uxe
     &,L_ebi,L_ibi,L_obi,L_ubi,L_adi,L_edi
      LOGICAL*1 L_idi,L_afi,L_efi,L_ifi,L_ofi,L_ufi,L_aki
     &,L_eki
      REAL*4 R_iki,R_ali,R_oli,R_uli,R_ami,R_emi,R_imi,R_omi
     &,R_umi,R_api
      LOGICAL*1 L_epi,L_ipi,L_opi,L_upi,L_ari,L_eri,L_iri
     &,L_ori,L_uri,L_asi,L_esi,L_isi,L_osi,L_usi,L_ati,L_eti
     &,L_iti,L_oti,L_uti,L_avi,L_evi,L_ivi
      LOGICAL*1 L_ovi,L_uvi,L_axi,L_exi,L_ixi,L_oxi,L_uxi
     &,L_abo,L_ibo,L_ubo,L_edo,L_ido,L_odo,L_udo,L_afo,L_efo
     &,L_ifo,L_ofo,L_ufo,L_ako,L_eko,L_iko
      LOGICAL*1 L_oko,L_uko,L_alo,L_elo,L_ilo,L_olo
      End subroutine TRANSPORTER_dpro
      End interface
