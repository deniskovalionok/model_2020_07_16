      Subroutine K2_VLVA(ext_deltat,L_e,L_i,L_if,L_of,R_ek
     &,R_ok,L_uk,R_el,R8_ap,L_ep,R8_ir,I_es,R_at,R_et,R_ot
     &,R_ut,R_uv,R_ax,L_ex,L_ix,R_ox,R_abe,R_ebe,R_efe,R_ole
     &,R_ule,R_eme,R8_ape,R_ipe,R_use,L_ate,R_ave,L_ove,L_uve
     &,L_axe,I_abi,I_ibi,L_edi,L_odi,L_udi,I_afi,I_efi,L_ifi
     &,L_ofi,L_aki,L_oki,R_ali,R_eli,L_ili,R_uli,L_ami,L_emi
     &,L_omi,L_umi,L_epi,L_ipi,L_opi,L_upi,L_asi,L_isi,L_osi
     &,L_ati,L_iti,L_oti,L_uti,I_ovi,I_uvi,I_axi,I_exi,L_ixi
     &,R_oxi,L_uxi,I1_ebo,L_ibo,L_obo,L_ubo,L_ado,L_edo,L_odo
     &,L_udo,L_afo,L_ifo,L_ofo,R_eko,L_iko,L_oko,L_uko,L_alo
     &,L_elo,L_amo,L_emo,L_omo,L_iso,L_oso,L_ato,L_uvo,L_axo
     &,L_exo,L_ixo,L_oxo,L_uxo,L_abu,L_ebu,L_ibu,L_obu,L_ubu
     &,L_adu,L_edu,L_idu,L_odu,L_udu)
C |L_e           |1 1 O|cl_cmd          |������� �������|F|
C |L_i           |1 1 O|op_cmd          |������� �������|F|
C |L_if          |1 1 I|cl_key          |������� �������|F|
C |L_of          |1 1 I|op_key          |������� �������|F|
C |R_ek          |4 4 K|_tdel1          |[]�������� ������� �������� ������|2.0|
C |R_ok          |4 4 S|_sdel1*         |[���]���������� ��������� �������� ������ |0.0|
C |L_uk          |1 1 S|_idel1*         |[TF]���������� ��������� �������� ������ |f|
C |R_el          |4 4 K|_c02            |�������� ��������� ����������|`power_c`|
C |R8_ap         |4 8 I|voltage         |[�]���������� ������ �������� �������||
C |L_ep          |1 1 O|novoltage       |��� ���������� �������||
C |R8_ir         |4 8 O|sn_power        |�������� ���� ��|3.0|
C |I_es          |2 4 I|SF1SWT          |������� ������� �������||
C |R_at          |4 4 O|isu_pif_value   |����� ����� ���� �� ��� ��� ���||
C |R_et          |4 4 O|isu_value       |�����. �����. ���������� �� ���||
C |R_ot          |4 4 O|isu_pif_eps     |��������� ����� ���� �� ��� ��� ���||
C |R_ut          |4 4 O|isu_eps         |��������� �����. ���������� �� ���||
C |R_uv          |4 4 O|pif_eps         |��������� ���������� ��� ���||
C |R_ax          |4 4 I|eps             |��������� ����������||
C |L_ex          |1 1 I|GM146           |����� ������� �������� ��21 "����.�� ����"||
C |L_ix          |1 1 I|GM145           |����� ������� �������� ��21 "����.�� ����"||
C |R_ox          |4 4 O|pif_value       |���.��������� ����� ��� ���|0.5|
C |R_abe         |4 4 O|out_value_prc   |���.��������� ����� � %|50|
C |R_ebe         |4 4 S|_slpb1*         |���������� ��������� ||
C |R_efe         |4 4 I|tau             |���������� ������� �������|0.0|
C |R_ole         |4 4 I|tclose          |������ ����� ��������|30.0|
C |R_ule         |4 4 I|topen           |������ ����� ��������|30.0|
C |R_eme         |4 4 O|_oapr1*         |�������� ������ ���������. ����� |0.0|
C |R8_ape        |4 8 O|value           |���.�������� ��������|0.5|
C |R_ipe         |4 4 O|out_value       |���.��������� �����|0.5|
C |R_use         |4 4 I|GM19P           |��������� ������������|0.6|
C |L_ate         |1 1 S|_qff3*          |�������� ������ Q RS-��������  |F|
C |R_ave         |4 4 O|posfbst         |��������� ���.��|0.5|
C |L_ove         |1 1 S|_spls3*         |[TF]���������� ��������� ������������� |F|
C |L_uve         |1 1 I|ISU_DIST        |������� "����" �� ���||
C |L_axe         |1 1 I|ISU_AVT         |������� "���" �� ���||
C |I_abi         |2 4 S|_sdly1*         |���������� ��������� �������� ||
C |I_ibi         |2 4 O|_ocoder1*       |����� ���������|0|
C |L_edi         |1 1 I|GM143           |����� ������� ����� ��21 "�"||
C |L_odi         |1 1 I|pif_sah3        |���� ����� "�"||
C |L_udi         |1 1 I|ISU_ON          |������� ������� �� ���||
C |I_afi         |2 4 I|ISU_NUM         |����� ���������� �� ��� ����������||
C |I_efi         |2 4 I|DISUN           ||-1|
C |L_ifi         |1 1 I|ISU_OFF         |������� ������� �� ���||
C |L_ofi         |1 1 O|isu_sign        |������� ���������� �� ���|F|
C |L_aki         |1 1 I|gk_close        |������� �� ����� ��.���.�������||
C |L_oki         |1 1 I|gk_open         |������� �� ����� ��.���.�������||
C |R_ali         |4 4 I|SA1             |������� ������ �� ������ "���� ������������ ������\n��� ������ ���� ����������" |0.0|
C |R_eli         |4 4 S|SA1_ST*         |���������� ��������� "���� ������������ ������\n��� ������ ���� ����������" |0.0|
C |L_ili         |1 1 S|SA1_STL*        |���������� ��������� "���� ������������ ������\n��� ������ ���� ����������" |F|
C |R_uli         |4 4 O|SA1_ANGLE*      |���� �������� ����� "���� ������������ ������\n��� ������ ���� ����������" |0.0|
C |L_ami         |1 1 O|SA1_CMDL*       |[TF]������� ����� ���� ������������ ������\n��� ������ ���� ���������� - �����|F|
C |L_emi         |1 1 O|SA1_CMDR*       |[TF]������� ����� ���� ������������ ������\n��� ������ ���� ���������� - ������|F|
C |L_omi         |1 1 I|pif_sa12        |���� ������ �������||
C |L_umi         |1 1 I|pif_sa11        |���� ������ �������||
C |L_epi         |1 1 S|_spls2*         |[TF]���������� ��������� ������������� |F|
C |L_ipi         |1 1 S|_spls1*         |[TF]���������� ��������� ������������� |F|
C |L_opi         |1 1 I|setar           |��������� ������ ���������� (���)||
C |L_upi         |1 1 I|breakar         |���������� ������ ���������� (���)||
C |L_asi         |1 1 I|GM141           |����� ������� ����� ��21 "�"||
C |L_isi         |1 1 I|pif_sah1        |���� ����� "�"||
C |L_osi         |1 1 I|GM142           |����� ������� ����� ��21 "�"||
C |L_ati         |1 1 I|pif_sah2        |���� ����� "�"||
C |L_iti         |1 1 O|sah3            |���� � "�"||
C |L_oti         |1 1 O|sah2            |���� � "�"||
C |L_uti         |1 1 O|sah1            |���� � "�"||
C |I_ovi         |2 4 S|SAH_POS0*       |��������� ������ �������������|2|
C |I_uvi         |2 4 O|SAH_POS         |��������� ��������� �����|2|
C |I_axi         |2 4 S|SAH_4OLD*       |��������� ����� "���� ������ ������ ��21\n��� ������ ���� ����������" |0|
C |I_exi(1:4)    |2 4 I|SAH(1:4)        |������� ������ ����� "���� ������ ������ ��21\n��� ������ ���� ����������" |4*0|
C |L_ixi         |1 1 S|SAH_FLAG*       |����� ��������� � ���� |F|
C |R_oxi         |4 4 O|SAH_ANGLE*      |���� �������� ����� "���� ������ ������ ��21\n��� ������ ���� ����������" |0.0|
C |L_uxi         |1 1 I|blkdist         |����.����.���||
C |I1_ebo(1:10)  |2 1 O|UBK(1:10)       |���������� �����||
C |L_ibo         |1 1 I|GM152           |����� ������� �������� ��� "�������"||
C |L_obo         |1 1 I|GM151           |����� ������� �������� ��� "����"||
C |L_ubo         |1 1 I|GM47            |��� ��������� ���������||
C |L_ado         |1 1 O|rlamp           |��������� ������� ��������||
C |L_edo         |1 1 O|glamp           |��������� ������� ��������||
C |L_odo         |1 1 I|blockreg        |���������� ���� �����������|F|
C |L_udo         |1 1 I|uluclose        |������� ������� (���)||
C |L_afo         |1 1 I|uluopen         |������� ������� (���)||
C |L_ifo         |1 1 I|resmode         |��������� � �������||
C |L_ofo         |1 1 S|_qff10*         |�������� ������ Q RS-��������  |`arstate_c`|
C |R_eko         |4 4 I|tslow           |����������� ���������� �������|1|
C |L_iko         |1 1 I|blockblk        |���������� ���� ����������|F|
C |L_oko         |1 1 O|cbc             |�� ������� (���)|F|
C |L_uko         |1 1 O|cbo             |�� ������� (���)|F|
C |L_alo         |1 1 O|cbcm            |������� 0.001|F|
C |L_elo         |1 1 O|outc            |��� ������� �������|F|
C |L_amo         |1 1 O|RASP            |��� �������� � ����.�������|F|
C |L_emo         |1 1 O|outo            |��� ������� �������|F|
C |L_omo         |1 1 O|cbom            |������� 0.999|F|
C |L_iso         |1 1 O|cmdcl           |������� ������� (���)||
C |L_oso         |1 1 O|arstate         |��������� ����������|F|
C |L_ato         |1 1 O|cmdop           |������� ������� (���)||
C |L_uvo         |1 1 I|tzclose         |������� ������� (��)|F|
C |L_axo         |1 1 I|blkclose        |���� �������� (��)||
C |L_exo         |1 1 I|blkopen         |����. �������� (��)||
C |L_ixo         |1 1 I|tzopen          |������� ������� (��)|F|
C |L_oxo         |1 1 I|GM45            |����. ������ �� ���||
C |L_uxo         |1 1 I|GM03            |������� � ��������||
C |L_abu         |1 1 I|GM04            |������� � ��������||
C |L_ebu         |1 1 I|GM19            |���� ������������||
C |L_ibu         |1 1 I|GM08            |������� �� "�������"||
C |L_obu         |1 1 I|GM09            |������� �� "�������"||
C |L_ubu         |1 1 I|GM22            |����� ����� ��������||
C |L_adu         |1 1 I|GM23            |������� ������� �����||
C |L_edu         |1 1 I|GM24            |��� ���������� �� � ����������||
C |L_idu         |1 1 I|GM46            |�� �����������||
C |L_odu         |1 1 I|GM121           |����� ������� ������ ��� �������||
C |L_udu         |1 1 I|GM122           |����� ������� ������ ��� �������||

      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      LOGICAL*1 L_e,L_i
      REAL*4 R0_o,R0_u,R0_ad,R0_ed,R0_id,R0_od,R0_ud,R0_af
     &,R0_ef
      LOGICAL*1 L_if,L_of
      INTEGER*4 I0_uf,I0_ak
      REAL*4 R_ek,R0_ik,R_ok
      LOGICAL*1 L_uk,L0_al
      REAL*4 R_el,R0_il,R0_ol,R0_ul,R0_am,R0_em,R0_im
      LOGICAL*1 L0_om
      REAL*4 R0_um
      REAL*8 R8_ap
      LOGICAL*1 L_ep,L0_ip
      REAL*4 R0_op,R0_up,R0_ar,R0_er
      REAL*8 R8_ir
      LOGICAL*1 L0_or
      INTEGER*4 I0_ur
      LOGICAL*1 L0_as
      INTEGER*4 I_es
      REAL*4 R0_is,R0_os,R0_us,R_at,R_et,R0_it,R_ot,R_ut,R0_av
     &,R0_ev,R0_iv,R0_ov,R_uv,R_ax
      LOGICAL*1 L_ex,L_ix
      REAL*4 R_ox,R0_ux,R_abe,R_ebe,R0_ibe,R0_obe,R0_ube,R0_ade
     &,R0_ede,R0_ide,R0_ode,R0_ude,R0_afe,R_efe
      LOGICAL*1 L0_ife
      REAL*4 R0_ofe,R0_ufe,R0_ake,R0_eke,R0_ike,R0_oke,R0_uke
     &,R0_ale,R0_ele,R0_ile,R_ole,R_ule,R0_ame,R_eme,R0_ime
     &,R0_ome
      LOGICAL*1 L0_ume
      REAL*8 R8_ape
      REAL*4 R0_epe,R_ipe,R0_ope,R0_upe,R0_are,R0_ere,R0_ire
      LOGICAL*1 L0_ore
      REAL*4 R0_ure,R0_ase,R0_ese,R0_ise
      LOGICAL*1 L0_ose
      REAL*4 R_use
      LOGICAL*1 L_ate
      REAL*4 R0_ete,R0_ite,R0_ote,R0_ute,R_ave,R0_eve,R0_ive
      LOGICAL*1 L_ove,L_uve,L_axe,L0_exe,L0_ixe,L0_oxe,L0_uxe
      INTEGER*4 I_abi,I0_ebi,I_ibi
      LOGICAL*1 L0_obi,L0_ubi,L0_adi,L_edi,L0_idi,L_odi,L_udi
      INTEGER*4 I_afi,I_efi
      LOGICAL*1 L_ifi,L_ofi,L0_ufi,L_aki,L0_eki,L0_iki,L_oki
     &,L0_uki
      REAL*4 R_ali,R_eli
      LOGICAL*1 L_ili,L0_oli
      REAL*4 R_uli
      LOGICAL*1 L_ami,L_emi,L0_imi,L_omi,L_umi,L0_api,L_epi
     &,L_ipi,L_opi,L_upi,L0_ari,L0_eri,L0_iri,L0_ori,L0_uri
     &,L_asi,L0_esi,L_isi,L_osi,L0_usi,L_ati,L0_eti
      LOGICAL*1 L_iti,L_oti,L_uti,L0_avi,L0_evi,L0_ivi
      INTEGER*4 I_ovi,I_uvi,I_axi,I_exi(1:4)
      LOGICAL*1 L_ixi
      REAL*4 R_oxi
      LOGICAL*1 L_uxi,L0_abo
      INTEGER*1 I1_ebo(1:10)
      LOGICAL*1 L_ibo,L_obo,L_ubo,L_ado,L_edo,L0_ido,L_odo
     &,L_udo,L_afo,L0_efo,L_ifo,L_ofo
      REAL*4 R0_ufo,R0_ako,R_eko
      LOGICAL*1 L_iko,L_oko,L_uko,L_alo,L_elo,L0_ilo,L0_olo
     &,L0_ulo,L_amo,L_emo,L0_imo,L_omo,L0_umo,L0_apo,L0_epo
     &,L0_ipo,L0_opo,L0_upo,L0_aro,L0_ero,L0_iro,L0_oro
      LOGICAL*1 L0_uro,L0_aso,L0_eso,L_iso,L_oso,L0_uso,L_ato
     &,L0_eto,L0_ito,L0_oto,L0_uto,L0_avo,L0_evo,L0_ivo,L0_ovo
     &,L_uvo,L_axo,L_exo,L_ixo,L_oxo,L_uxo,L_abu
      LOGICAL*1 L_ebu,L_ibu,L_obu,L_ubu,L_adu,L_edu,L_idu
     &,L_odu,L_udu

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      R0_ik=R_ok
C K2_VLVA.fmg( 130, 283):pre: �������� ��������� ������,1
      R0_ome=R_ebe
C K2_VLVA.fmg( 455, 453):pre: ����������� ��,1
      R0_ofe=R_eme
C K2_VLVA.fmg( 340, 451):pre: �������������� �����  ,1
      I0_ebi=I_abi
C K2_VLVA.fmg( 266, 335):pre: �������� �� ���,1
      R0_o = 30
C K2_VLVA.fmg( 357, 403):��������� (RE4) (�������)
      R0_u = 30
C K2_VLVA.fmg( 357, 415):��������� (RE4) (�������)
      R0_ad = DeltaT
C K2_VLVA.fmg( 357, 419):��������� (RE4) (�������)
      if(R0_u.ge.0.0) then
         R0_id=R0_ad/max(R0_u,1.0e-10)
      else
         R0_id=R0_ad/min(R0_u,-1.0e-10)
      endif
C K2_VLVA.fmg( 365, 418):�������� ����������
      if(R0_o.ge.0.0) then
         R0_ed=R0_ad/max(R0_o,1.0e-10)
      else
         R0_ed=R0_ad/min(R0_o,-1.0e-10)
      endif
C K2_VLVA.fmg( 365, 406):�������� ����������
      R0_ef = 0.0
C K2_VLVA.fmg( 371, 412):��������� (RE4) (�������)
      if(L_if) then
         R0_od=R0_ed
      else
         R0_od=R0_ef
      endif
C K2_VLVA.fmg( 375, 406):���� RE IN LO CH7
      if(L_of) then
         R0_ud=R0_id
      else
         R0_ud=R0_ef
      endif
C K2_VLVA.fmg( 375, 418):���� RE IN LO CH7
      R0_af = R0_ud + (-R0_od)
C K2_VLVA.fmg( 381, 418):��������
      I0_ak = 1
C K2_VLVA.fmg( 132, 279):��������� ������������� IN (�������)
      L0_om = L_edu.OR.L_adu.OR.L_ubu
C K2_VLVA.fmg( 106, 328):���
      !��������� R0_il = K2_VLVAC02 /`power_c`/
      R0_il=R_el
C K2_VLVA.fmg(  83, 337):���������,02
      R0_em = 3
C K2_VLVA.fmg( 116, 331):��������� (RE4) (�������)
      R0_ol = 0.2
C K2_VLVA.fmg( 108, 332):��������� (RE4) (�������)
      R0_ul = 1
C K2_VLVA.fmg( 108, 334):��������� (RE4) (�������)
      if(L0_om) then
         R0_im=R0_ol
      else
         R0_im=R0_ul
      endif
C K2_VLVA.fmg( 111, 333):���� RE IN LO CH7
      R0_um = 95.0
C K2_VLVA.fmg( 146, 261):��������� (RE4) (�������)
      L_ep=R8_ap.lt.R0_um
C K2_VLVA.fmg( 150, 263):���������� <
      R0_up = 0.0
C K2_VLVA.fmg( 138, 335):��������� (RE4) (�������)
      I0_ur = 1
C K2_VLVA.fmg( 146, 278):��������� ������������� IN (�������)
      L0_or=I0_ur.eq.I_es
C K2_VLVA.fmg( 150, 277):���������� �������������
      L_amo = L0_or.OR.L_ep
C K2_VLVA.fmg( 158, 276):���
      L_e = L_if.AND.L_amo
C K2_VLVA.fmg( 383, 400):�
      L_i = L_of.AND.L_amo
C K2_VLVA.fmg( 383, 410):�
      R0_is = 100.0
C K2_VLVA.fmg( 391, 358):��������� (RE4) (�������)
      R0_os = 0.0
C K2_VLVA.fmg( 365, 345):��������� (RE4) (�������)
      R0_us = 0.0
C K2_VLVA.fmg( 365, 382):��������� (RE4) (�������)
      if(L_amo) then
         R0_ev=R0_us
      else
         R0_ev=R_ax
      endif
C K2_VLVA.fmg( 369, 383):���� RE IN LO CH7
      R0_av = 25.0
C K2_VLVA.fmg( 392, 381):��������� (RE4) (�������)
      R0_ov = R0_ev + R0_av
C K2_VLVA.fmg( 395, 383):��������
      R0_iv = 0.02
C K2_VLVA.fmg( 403, 380):��������� (RE4) (�������)
      R_uv = R0_ov * R0_iv
C K2_VLVA.fmg( 406, 382):����������
      R0_ux = 100.0
C K2_VLVA.fmg( 458, 477):��������� (RE4) (�������)
      R0_ibe = 0.0
C K2_VLVA.fmg( 447, 471):��������� (RE4) (�������)
      R0_obe = 0.1
C K2_VLVA.fmg( 426, 417):��������� (RE4) (�������)
      if(L_obu) then
         R0_ube=1
      else
         R0_ube=0
      endif
C K2_VLVA.fmg( 423, 414):��������� LO->1
      R0_ade = R0_obe * R0_ube
C K2_VLVA.fmg( 429, 415):����������
      R0_ede = 0.000001
C K2_VLVA.fmg( 426, 419):��������� (RE4) (�������)
      R0_ere = R0_ede + R0_ade
C K2_VLVA.fmg( 434, 419):��������
      R0_ide = 0.1
C K2_VLVA.fmg( 426, 427):��������� (RE4) (�������)
      if(L_ibu) then
         R0_ode=1
      else
         R0_ode=0
      endif
C K2_VLVA.fmg( 423, 424):��������� LO->1
      R0_ude = R0_ide * R0_ode
C K2_VLVA.fmg( 429, 425):����������
      R0_afe = 0.999999
C K2_VLVA.fmg( 426, 429):��������� (RE4) (�������)
      R0_ire = R0_afe + R0_ude
C K2_VLVA.fmg( 434, 429):��������
      R0_eke = DeltaT
C K2_VLVA.fmg( 309, 456):��������� (RE4) (�������)
      R0_ile = 0.0
C K2_VLVA.fmg( 321, 447):��������� (RE4) (�������)
      R0_ame = 0.0
C K2_VLVA.fmg( 347, 448):��������� (RE4) (�������)
      R0_ime = 0.0
C K2_VLVA.fmg( 450, 446):��������� (RE4) (�������)
      if(L_ubu) then
         R0_epe=R0_ime
      else
         R0_epe=R0_ome
      endif
C K2_VLVA.fmg( 453, 447):���� RE IN LO CH7
      L0_ume = L_adu.OR.L_ubu
C K2_VLVA.fmg( 443, 442):���
      R0_ope = 1.0
C K2_VLVA.fmg( 427, 456):��������� (RE4) (�������)
      R0_upe = 0.0
C K2_VLVA.fmg( 419, 456):��������� (RE4) (�������)
      R0_ase = 1.0e-10
C K2_VLVA.fmg( 372, 434):��������� (RE4) (�������)
      R0_ete = 0.0
C K2_VLVA.fmg( 383, 447):��������� (RE4) (�������)
      L0_exe=I_efi.eq.I_afi
C K2_VLVA.fmg( 222, 341):���������� �������������
      L0_oxe = L_axe.AND.L0_exe
C K2_VLVA.fmg( 230, 351):�
      L0_ixe = L_uve.AND.L0_exe
C K2_VLVA.fmg( 230, 347):�
      L0_adi=.false.
C K2_VLVA.fmg( 249, 322):��������� ���������� (�������)
      L0_idi = L_odi.AND.(.NOT.L_edi)
C K2_VLVA.fmg( 224, 318):�
      L_ofi=I_efi.eq.I_afi
C K2_VLVA.fmg(  54, 388):���������� �������������
      L0_iki = L_udi.AND.L_ofi
C K2_VLVA.fmg(  62, 401):�
      L0_ufi = L_ifi.AND.L_ofi
C K2_VLVA.fmg(  62, 405):�
      if(L_ofi) then
         R_ot=R_uv
      endif
C K2_VLVA.fmg( 415, 376):���� � ������������� �������
      if(L_ofi) then
         R_ut=R0_ev
      endif
C K2_VLVA.fmg( 401, 370):���� � ������������� �������
      L_ami=R_ali.lt.R_eli
      L_emi=R_ali.gt.R_eli
      R_eli=R_ali
      if(L_ami) then
         R_uli=45.0
      elseif(L_emi) then
         R_uli=-(45.0)
      else
         R_uli=0.0
      endif
      !DEC$ UNDEFINE FLAG
      !DEC$ IF DEFINED(FLAG)
      L0_oli=L_ami.or.L_emi
      if(L0_oli.and..not.L_ili) then
         call spw_playsound('SWITCH',1)
      endif
      L_ili=L0_oli
      !DEC$ ENDIF
      !DEC$ UNDEFINE FLAG
C K2_VLVA.fmg(  44, 430):���������� ����� 3-���. ��� ����.
      L0_imi = L_omi.AND.(.NOT.L_odu)
C K2_VLVA.fmg(  57, 448):�
      L0_eki = L0_imi.OR.L_aki.OR.L_ami.OR.L0_ufi
C K2_VLVA.fmg(  74, 433):���
      L0_eso = (.NOT.L_uxi).AND.L0_eki
C K2_VLVA.fmg(  84, 434):�
      L0_api = L_umi.AND.(.NOT.L_udu)
C K2_VLVA.fmg(  56, 413):�
      L0_uki = L_emi.OR.L_oki.OR.L0_api.OR.L0_iki
C K2_VLVA.fmg(  74, 425):���
      L0_uso = (.NOT.L_uxi).AND.L0_uki
C K2_VLVA.fmg(  84, 426):�
      L0_ari = L_opi.AND.(.NOT.L_oxo)
C K2_VLVA.fmg( 252, 285):�
      L0_ori = L_upi.AND.(.NOT.L_oxo)
C K2_VLVA.fmg( 252, 278):�
      L0_esi = L_isi.AND.(.NOT.L_asi)
C K2_VLVA.fmg( 224, 332):�
      L0_ubi = L0_oxe.OR.L0_esi
C K2_VLVA.fmg( 241, 333):���
      L0_usi = L_ati.AND.(.NOT.L_osi)
C K2_VLVA.fmg( 224, 325):�
      L0_obi = L0_ixe.OR.L0_usi
C K2_VLVA.fmg( 241, 326):���
      if(L0_ubi) then
         I_ibi=1
      elseif(L0_obi) then
         I_ibi=2
      elseif(L0_idi) then
         I_ibi=3
      elseif(L0_adi) then
         I_ibi=4
      endif
C K2_VLVA.fmg( 255, 325):�������� �� 4 (��� ���),1
      I_abi=I_ibi
C K2_VLVA.fmg( 266, 335):�������� �� ���,1
      L0_uxe=I0_ebi.ne.I_ibi
C K2_VLVA.fmg( 272, 334):���������� �������������
      if(L0_uxe) then
         I_uvi=I_ibi
      endif
C K2_VLVA.fmg( 273, 324):���� � ������������� �������
      !DEC$ DEFINE FLAG
      if(iand(I_exi(4),z'ff00').ne.iand(I_axi,z'ff00')) then
         L_ixi=.true.
      elseif(I_uvi.eq.I_exi(2)) then
         L_ixi=.false.
      endif
      if(L_ixi) then
         iv1=I_exi(2)
         if(iv1.ne.I_uvi) then
            if(iv1.ge.1.and.iv1.lt.I_uvi) then
               I_uvi=I_uvi-1
            elseif(iv1.le.3.and.iv1.gt.I_uvi) then
               I_uvi=I_uvi+1
            endif
         endif
      endif
      if(I_uvi.ne.I_ovi) then
      !DEC$ IF DEFINED(FLAG)
          call spw_playsound('SWITCH',1)
      !DEC$ ENDIF
      endif
      R_oxi=30.0-(I_uvi-1)*30.0
      I_axi=I_exi(4)
      I_ovi=I_uvi
      !DEC$ UNDEFINE FLAG
C K2_VLVA.fmg( 204, 298):���������� ��������� ������������� -2 
      L_uti=I_uvi.eq.1
      L_oti=I_uvi.eq.2
      L_iti=I_uvi.eq.3
      L0_eti=I_uvi.eq.4
C K2_VLVA.fmg( 217, 298):���������� �� 4
      L0_iri=L_iti.and..not.L_ove
      L_ove=L_iti
C K2_VLVA.fmg( 255, 293):������������  �� 1 ���,3
      L0_uri=L_oti.and..not.L_epi
      L_epi=L_oti
C K2_VLVA.fmg( 255, 297):������������  �� 1 ���,2
      L0_evi = L0_uri.OR.L0_ori.OR.L0_iri
C K2_VLVA.fmg( 265, 295):���
      L0_eri=L_uti.and..not.L_ipi
      L_ipi=L_uti
C K2_VLVA.fmg( 255, 301):������������  �� 1 ���,1
      L0_avi = L0_eri.OR.L0_ari
C K2_VLVA.fmg( 265, 300):���
      L_ofo=(L0_avi.or.L_ofo).and..not.(L0_evi)
      L0_ivi=.not.L_ofo
C K2_VLVA.fmg( 274, 298):RS �������,10
      L_oso=L_ofo
C K2_VLVA.fmg( 298, 300):������,arstate
      L_ato = L0_uso.AND.(.NOT.L_oso)
C K2_VLVA.fmg( 144, 441):�
      L0_aro = L_ato.AND.(.NOT.L_idu)
C K2_VLVA.fmg( 152, 440):�
      L_iso = L0_eso.AND.(.NOT.L_oso)
C K2_VLVA.fmg( 144, 413):�
      L0_iro = (.NOT.L_idu).AND.L_iso
C K2_VLVA.fmg( 151, 414):�
      L_ado = L_ofo
C K2_VLVA.fmg( 289, 288):���
      L0_ido = L_ofo.AND.L_ifo
C K2_VLVA.fmg( 283, 275):�
      L_edo = (.NOT.L_ofo).OR.L0_ido
C K2_VLVA.fmg( 289, 276):���
      L0_efo = L_ofo.AND.(.NOT.L_ifo)
C K2_VLVA.fmg( 283, 261):�
      L0_ito = L_afo.AND.L0_efo
C K2_VLVA.fmg( 130, 448):�
      L0_uro = L_udo.AND.L0_efo
C K2_VLVA.fmg( 130, 399):�
      L0_abo=.false.
C K2_VLVA.fmg(  11, 345):��������� ���������� (�������)
      I1_ebo(0*16+1)=(((((I1_ebo(0*16+1).xor.L0_abo).and.1
     &)*2).or.I1_ebo(0*16+1)).and.2).or.(L0_abo.and.1)
C K2_VLVA.fmg(  36, 345):������� �������� ���������� � ������������� (������+������������ ���������)
      I1_ebo(0*16+6)=(((((I1_ebo(0*16+6).xor.L_axo).and.1
     &)*2).or.I1_ebo(0*16+6)).and.2).or.(L_axo.and.1)
C K2_VLVA.fmg(  36, 325):������� �������� ���������� � ������������� (������+������������ ���������)
      I1_ebo(0*16+5)=(((((I1_ebo(0*16+5).xor.L_exo).and.1
     &)*2).or.I1_ebo(0*16+5)).and.2).or.(L_exo.and.1)
C K2_VLVA.fmg(  36, 329):������� �������� ���������� � ������������� (������+������������ ���������)
      I1_ebo(0*16+4)=(((((I1_ebo(0*16+4).xor.L_udo).and.1
     &)*2).or.I1_ebo(0*16+4)).and.2).or.(L_udo.and.1)
C K2_VLVA.fmg(  36, 333):������� �������� ���������� � ������������� (������+������������ ���������)
      I1_ebo(0*16+3)=(((((I1_ebo(0*16+3).xor.L_afo).and.1
     &)*2).or.I1_ebo(0*16+3)).and.2).or.(L_afo.and.1)
C K2_VLVA.fmg(  36, 337):������� �������� ���������� � ������������� (������+������������ ���������)
      I1_ebo(0*16+2)=(((((I1_ebo(0*16+2).xor.L_uxi).and.1
     &)*2).or.I1_ebo(0*16+2)).and.2).or.(L_uxi.and.1)
C K2_VLVA.fmg(  36, 341):������� �������� ���������� � ������������� (������+������������ ���������)
      L0_eto = L_idu.OR.L_odo.OR.L_oxo
C K2_VLVA.fmg( 124, 424):���
      L0_oto = L0_ito.AND.(.NOT.L0_eto)
C K2_VLVA.fmg( 144, 447):�
      L0_oro = L0_oto.AND.(.NOT.L0_iro)
C K2_VLVA.fmg( 172, 446):�
      L0_upo = L0_oro.OR.L0_aro
C K2_VLVA.fmg( 178, 445):���
      L0_aso = (.NOT.L0_eto).AND.L0_uro
C K2_VLVA.fmg( 144, 400):�
      L0_ero = (.NOT.L0_aro).AND.L0_aso
C K2_VLVA.fmg( 172, 401):�
      L0_opo = L0_iro.OR.L0_ero
C K2_VLVA.fmg( 178, 402):���
      L0_ivo = L_iko.OR.L_idu
C K2_VLVA.fmg( 124, 436):���
      L0_ovo = L_ixo.AND.(.NOT.L0_ivo)
C K2_VLVA.fmg( 144, 465):�
      L0_avo = L_uxo.OR.L0_ovo
C K2_VLVA.fmg( 175, 466):���
      L0_uto = (.NOT.L_abu).AND.L0_avo
C K2_VLVA.fmg( 188, 467):�
      L0_evo = (.NOT.L0_ivo).AND.L_exo
C K2_VLVA.fmg( 144, 456):�
      L0_ipo = L_axo.AND.(.NOT.L0_ivo)
C K2_VLVA.fmg( 144, 389):�
      L0_ulo = (.NOT.L0_avo).AND.L0_opo.AND.(.NOT.L0_ipo)
C K2_VLVA.fmg( 192, 402):�
      L0_epo = (.NOT.L0_ivo).AND.L_uvo
C K2_VLVA.fmg( 144, 384):�
      L0_umo = L0_epo.OR.L_abu
C K2_VLVA.fmg( 175, 383):���
      L0_apo = (.NOT.L0_evo).AND.L0_upo.AND.(.NOT.L0_umo)
C K2_VLVA.fmg( 192, 445):�
      L0_imo = L0_uto.OR.L0_apo
C K2_VLVA.fmg( 199, 446):���
      L0_ilo = L0_umo.AND.(.NOT.L_uxo)
C K2_VLVA.fmg( 188, 382):�
      L0_olo = L0_ulo.OR.L0_ilo
C K2_VLVA.fmg( 199, 401):���
      R0_ufo = 1.0
C K2_VLVA.fmg( 283, 269):��������� (RE4) (�������)
      if(L_ofo) then
         R0_ako=R_eko
      else
         R0_ako=R0_ufo
      endif
C K2_VLVA.fmg( 287, 268):���� RE IN LO CH7
      R0_ufe = R_ole * R0_ako
C K2_VLVA.fmg( 309, 433):����������
      if(R0_ufe.ge.0.0) then
         R0_ike=R0_eke/max(R0_ufe,1.0e-10)
      else
         R0_ike=R0_eke/min(R0_ufe,-1.0e-10)
      endif
C K2_VLVA.fmg( 317, 435):�������� ����������
      R0_ake = R_ule * R0_ako
C K2_VLVA.fmg( 309, 453):����������
      if(R0_ake.ge.0.0) then
         R0_oke=R0_eke/max(R0_ake,1.0e-10)
      else
         R0_oke=R0_eke/min(R0_ake,-1.0e-10)
      endif
C K2_VLVA.fmg( 317, 455):�������� ����������
      L_emo = (.NOT.L_omo).AND.L0_imo.AND.(.NOT.L_amo)
C K2_VLVA.fmg( 228, 446):�
C label 337  try337=try337-1
      if(.NOT.L0_ume) then
         R_ave=R8_ape
      endif
C K2_VLVA.fmg( 464, 456):���� � ������������� �������
      if(L_emo) then
         R0_ale=R0_oke
      else
         R0_ale=R0_ile
      endif
C K2_VLVA.fmg( 325, 455):���� RE IN LO CH7
      L_elo = L0_olo.AND.(.NOT.L_alo).AND.(.NOT.L_amo)
C K2_VLVA.fmg( 228, 399):�
      if(L_elo) then
         R0_uke=R0_ike
      else
         R0_uke=R0_ile
      endif
C K2_VLVA.fmg( 325, 435):���� RE IN LO CH7
      R0_ele = R0_ale + (-R0_uke)
C K2_VLVA.fmg( 331, 451):��������
      L0_ife =.NOT.(L_emo.OR.L_elo)
C K2_VLVA.fmg( 333, 442):���
      if(L0_ife) then
         R_eme=R0_ele
      else
         R_eme=(R_efe*R0_ofe+deltat*R0_ele)/(R_efe+deltat
     &)
      endif
C K2_VLVA.fmg( 340, 451):�������������� �����  ,1
      if(L_edu) then
         R0_ite=R0_ame
      else
         R0_ite=R_eme
      endif
C K2_VLVA.fmg( 350, 449):���� RE IN LO CH7
      R0_ise = R_ave + (-R_use)
C K2_VLVA.fmg( 361, 440):��������
      R0_ure = R0_ite + R0_ise
C K2_VLVA.fmg( 367, 441):��������
      R0_ese = R0_ure * R0_ise
C K2_VLVA.fmg( 371, 440):����������
      L0_ose=R0_ese.lt.R0_ase
C K2_VLVA.fmg( 376, 439):���������� <
      L_ate=(L0_ose.or.L_ate).and..not.(.NOT.L_ebu)
      L0_ore=.not.L_ate
C K2_VLVA.fmg( 383, 437):RS �������,3
      if(L_ate) then
         R_ave=R_use
      endif
C K2_VLVA.fmg( 378, 456):���� � ������������� �������
      if(L_ate) then
         R0_ote=R0_ete
      else
         R0_ote=R0_ite
      endif
C K2_VLVA.fmg( 386, 448):���� RE IN LO CH7
      if(L_amo) then
         R0_ute=R0_af
      else
         R0_ute=R0_ote
      endif
C K2_VLVA.fmg( 401, 447):���� RE IN LO CH7
      R0_eve = R_ave + R0_ute
C K2_VLVA.fmg( 416, 450):��������
      R0_are=MAX(R0_upe,R0_eve)
C K2_VLVA.fmg( 424, 451):��������
      R0_ive=MIN(R0_ope,R0_are)
C K2_VLVA.fmg( 432, 452):�������
      L_omo=R0_ive.gt.R0_ire
C K2_VLVA.fmg( 443, 430):���������� >
C sav1=L_emo
      L_emo = (.NOT.L_omo).AND.L0_imo.AND.(.NOT.L_amo)
C K2_VLVA.fmg( 228, 446):recalc:�
C if(sav1.ne.L_emo .and. try337.gt.0) goto 337
      if(L0_ume) then
         R8_ape=R0_epe
      else
         R8_ape=R0_ive
      endif
C K2_VLVA.fmg( 457, 450):���� RE IN LO CH7
      R_ebe=R8_ape
C K2_VLVA.fmg( 455, 453):����������� ��,1
      if(L_ubo) then
         R_ipe=R0_ibe
      else
         R_ipe=R0_ive
      endif
C K2_VLVA.fmg( 450, 472):���� RE IN LO CH7
      R_abe = R0_ux * R_ipe
C K2_VLVA.fmg( 461, 477):����������
      if(L_amo) then
         R_ox=R0_os
      else
         R_ox=R_ipe
      endif
C K2_VLVA.fmg( 369, 346):���� RE IN LO CH7
      R0_it = R0_is * R_ox
C K2_VLVA.fmg( 394, 358):����������
      if(L_ofi) then
         R_et=R0_it
      endif
C K2_VLVA.fmg( 401, 359):���� � ������������� �������
      if(L_ofi) then
         R_at=R_ox
      endif
C K2_VLVA.fmg( 415, 352):���� � ������������� �������
      L_alo=R0_ive.lt.R0_ere
C K2_VLVA.fmg( 443, 420):���������� <
C sav1=L_elo
      L_elo = L0_olo.AND.(.NOT.L_alo).AND.(.NOT.L_amo)
C K2_VLVA.fmg( 228, 399):recalc:�
C if(sav1.ne.L_elo .and. try357.gt.0) goto 357
      L_oko = (.NOT.L_omo).AND.L_alo.AND.(.NOT.L_amo)
C K2_VLVA.fmg( 228, 374):�
      L_uko = L_omo.AND.(.NOT.L_alo).AND.(.NOT.L_amo)
C K2_VLVA.fmg( 228, 473):�
      if(L0_ume) then
         R_ave=R0_ive
      endif
C K2_VLVA.fmg( 446, 456):���� � ������������� �������
C sav1=R0_ise
      R0_ise = R_ave + (-R_use)
C K2_VLVA.fmg( 361, 440):recalc:��������
C if(sav1.ne.R0_ise .and. try378.gt.0) goto 378
C sav1=R0_eve
      R0_eve = R_ave + R0_ute
C K2_VLVA.fmg( 416, 450):recalc:��������
C if(sav1.ne.R0_eve .and. try401.gt.0) goto 401
      if(L_ate) then
         R0_am=R0_em
      else
         R0_am=R0_im
      endif
C K2_VLVA.fmg( 119, 332):���� RE IN LO CH7
      R0_op = R0_il * R0_am
C K2_VLVA.fmg( 124, 334):����������
      L0_ip = L_emo.OR.L_elo
C K2_VLVA.fmg( 101, 310):���
      if(L0_ip) then
         R0_er=R0_op
      else
         R0_er=R0_up
      endif
C K2_VLVA.fmg( 141, 334):���� RE IN LO CH7
      L0_al = L_ate.AND.L0_ip
C K2_VLVA.fmg( 122, 283):�
      if(.not.L0_al) then
         R_ok=0.0
      elseif(.not.L_uk) then
         R_ok=R_ek
      else
         R_ok=max(R0_ik-deltat,0.0)
      endif
      L0_as=L0_al.and.R_ok.le.0.0
      L_uk=L0_al
C K2_VLVA.fmg( 130, 283):�������� ��������� ������,1
      if(L0_as) then
         I0_uf=I0_ak
      endif
C K2_VLVA.fmg( 135, 280):���� � ������������� �������
      R0_ar = R8_ir
C K2_VLVA.fmg( 136, 344):��������
C label 478  try478=try478-1
      R8_ir = R0_er + R0_ar
C K2_VLVA.fmg( 147, 343):��������
C sav1=R0_ar
      R0_ar = R8_ir
C K2_VLVA.fmg( 136, 344):recalc:��������
C if(sav1.ne.R0_ar .and. try478.gt.0) goto 478
      End
