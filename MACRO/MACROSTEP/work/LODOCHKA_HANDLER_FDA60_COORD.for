      Subroutine LODOCHKA_HANDLER_FDA60_COORD(ext_deltat,R_e
     &,I_i,I_u,I_ed,R_od,I_af,R_ef,I_if,I_uf,I_ak,I_ek,R_ik
     &,I_ok,R_uk,I_al,R_el,I_il,R_ol,I_ul,R_am,I_em,R_im,I_om
     &,R_um,I_ap,R_ep,I_op,I_up,R_ar,I_ir,R_or,I_as,I_es,R_is
     &,I_us,I_at,R_et,I_ot,I_ut,R_av,I_iv,I_ov,R_uv,I_ex,I_ix
     &,R_ox,I_abe,I_ebe,R_ibe,I_ube,I_ade,R_ede,I_ode,I_ude
     &,I_afe,I_efe,I_ife,I_ofe,I_ufe,I_ake,I_eke,I_ike,I_oke
     &,I_uke,I_ale,I_ele,I_ile,I_ole,I_ule,I_ame,I_eme,I_ime
     &,I_ome,I_ume,I_ape,I_epe,I_ipe,I_ope,I_upe,I_are,I_ere
     &,I_ire,I_ore,I_ure,I_ase,I_ese,I_ise,I_ose,I_use,I_ate
     &,I_ete,I_ite,I_ote,I_ute,I_ave,I_eve,I_ive,I_ove,I_uve
     &,I_axe,I_exe,I_ixe,I_oxe,I_uxe,I_abi,I_ebi,I_ibi,I_obi
     &,I_ubi,I_adi,I_edi,I_idi,I_odi,I_udi,I_afi,I_efi,I_ifi
     &,I_ofi,I_ufi,I_aki,I_eki,I_iki,I_oki,I_uki,R_ali,R_eli
     &,R_ili,R_oli,R_uli,R_ami,R_emi,R_imi,R_omi,R_umi,R_api
     &,R_epi,R_ipi,R_opi,R_upi,R_ari,R_eri,R_iri,R_ori,R_uri
     &,R_asi,R_esi,R_isi,R_osi,R_usi,R_ati,R_eti,R_iti,R_oti
     &,R_uti,R_avi,R_evi,R_ivi,R_ovi,R_uvi,R_axi,R_exi,R_ixi
     &,R_oxi,R_uxi,R_abo,R_ebo,R_ibo,R_obo,R_ubo,R_ado,R_edo
     &,R_ido,R_odo,R_udo,R_afo,R_efo,R_ifo,R_ofo,R_ufo,R_ako
     &,R_eko,R_iko,R_oko,R_uko,R_alo,R_elo,R_ilo,R_olo,R_ulo
     &,R_amo,R_emo,R_imo,R_omo,R_umo,R_apo,L_aso,L_eso,L_iso
     &,L_oso,L_uso,L_ato,L_eto,L_ito,L_oto,L_uto,L_avo,L_evo
     &,I_ibu,I_obu,I_ubu,I_adu,I_edu,I_idu,I_odu,I_udu,I_afu
     &,I_efu,I_ilu,I_olu,I_ulu,I_amu,I_emu,I_imu,I_omu,I_umu
     &,I_apu,I_epu,I_isu,I_osu,I_usu,I_atu,I_etu,I_itu,I_otu
     &,I_utu,I_avu,I_evu,I_ibad,I_obad,I_ubad,I_adad,I_edad
     &,I_idad,I_odad,I_udad,I_afad,I_efad,I_ilad,I_olad,I_ulad
     &,I_amad,I_emad,I_imad,I_omad,I_umad,I_apad,I_epad,I_osad
     &,I_usad,I_atad,I_etad,I_itad,I_otad,I_utad,I_avad,I_evad
     &,I_ivad,I_ovad,I_uvad,I_axad,I_exad,I_ixad,I_oxad,I_uxad
     &,I_abed,I_ebed,I_ibed,I_obed)
C |R_e           |4 4 O|CIL_20FDA70_MASS|||
C |I_i           |2 4 K|_lcmpJ10261     |�������� ������ �����������|7001|
C |I_u           |2 4 O|CIL_20FDA70_N   |||
C |I_ed          |2 4 K|_lcmpJ10245     |�������� ������ �����������|5|
C |R_od          |4 4 O|FDA60CW100ZQ01  |||
C |I_af          |2 4 O|FDA60GG006ZV01  |||
C |R_ef          |4 4 O|CIL_20FDA40_MASS|||
C |I_if          |2 4 K|_lcmpJ10222     |�������� ������ �����������|4001|
C |I_uf          |2 4 O|CIL_20FDA40_N   |||
C |I_ak          |2 4 K|_lcmpJ10126     |�������� ������ �����������|6|
C |I_ek          |2 4 O|CIL_20FDA60AE502_N7|||
C |R_ik          |4 4 O|CIL_20FDA60AE502_MASS7|||
C |I_ok          |2 4 O|CIL_20FDA60AE502_N6|||
C |R_uk          |4 4 O|CIL_20FDA60AE502_MASS6|||
C |I_al          |2 4 O|CIL_20FDA60AE502_N5|||
C |R_el          |4 4 O|CIL_20FDA60AE502_MASS5|||
C |I_il          |2 4 O|CIL_20FDA60AE502_N4|||
C |R_ol          |4 4 O|CIL_20FDA60AE502_MASS4|||
C |I_ul          |2 4 O|CIL_20FDA60AE502_N3|||
C |R_am          |4 4 O|CIL_20FDA60AE502_MASS3|||
C |I_em          |2 4 O|CIL_20FDA60AE502_N2|||
C |R_im          |4 4 O|CIL_20FDA60AE502_MASS2|||
C |I_om          |2 4 O|CIL_20FDA60AE502_N1|||
C |R_um          |4 4 O|CIL_20FDA60AE502_MASS1|||
C |I_ap          |2 4 K|_lcmpJ9970      |�������� ������ �����������|35|
C |R_ep          |4 4 O|CIL_20FDA60AE515_MASS|||
C |I_op          |2 4 O|CIL_20FDA60AE515_N|||
C |I_up          |2 4 K|_lcmpJ9956      |�������� ������ �����������|11|
C |R_ar          |4 4 O|CIL_20FDA60AE509_MASS|||
C |I_ir          |2 4 O|CIL_20FDA60AE509_N|||
C |R_or          |4 4 O|CIL_20FDA60AE403_MASS|||
C |I_as          |2 4 O|CIL_20FDA60AE403_N|||
C |I_es          |2 4 K|_lcmpJ9928      |�������� ������ �����������|5|
C |R_is          |4 4 O|CIL_20FDA60AE500_MASS2|||
C |I_us          |2 4 O|CIL_20FDA60AE500_N2|||
C |I_at          |2 4 K|_lcmpJ9914      |�������� ������ �����������|4|
C |R_et          |4 4 O|CIL_20FDA60AE500_MASS1|||
C |I_ot          |2 4 O|CIL_20FDA60AE500_N1|||
C |I_ut          |2 4 K|_lcmpJ9900      |�������� ������ �����������|101|
C |R_av          |4 4 O|CIL_20FDA60AE402_MASS|||
C |I_iv          |2 4 O|CIL_20FDA60AE402_N|||
C |I_ov          |2 4 K|_lcmpJ9886      |�������� ������ �����������|100|
C |R_uv          |4 4 O|CIL_20FDA60AE201_MASS|||
C |I_ex          |2 4 O|CIL_20FDA60AE201_N|||
C |I_ix          |2 4 K|_lcmpJ9872      |�������� ������ �����������|99|
C |R_ox          |4 4 O|CIL_20FDA60AE401_MASS|||
C |I_abe         |2 4 O|CIL_20FDA60AE401_N|||
C |I_ebe         |2 4 K|_lcmpJ9858      |�������� ������ �����������|98|
C |R_ibe         |4 4 O|CIL_20FDA60AE200_MASS|||
C |I_ube         |2 4 O|CIL_20FDA60AE200_N|||
C |I_ade         |2 4 K|_lcmpJ9844      |�������� ������ �����������|97|
C |R_ede         |4 4 O|CIL_20FDA60AE400_MASS|||
C |I_ode         |2 4 O|CIL_20FDA60AE400_N|||
C |I_ude         |2 4 K|_lcmpJ9767      |�������� ������ �����������|27|
C |I_afe         |2 4 K|_lcmpJ9766      |�������� ������ �����������|26|
C |I_efe         |2 4 K|_lcmpJ9765      |�������� ������ �����������|25|
C |I_ife         |2 4 K|_lcmpJ9764      |�������� ������ �����������|24|
C |I_ofe         |2 4 K|_lcmpJ9763      |�������� ������ �����������|23|
C |I_ufe         |2 4 K|_lcmpJ9757      |�������� ������ �����������|92|
C |I_ake         |2 4 K|_lcmpJ9756      |�������� ������ �����������|96|
C |I_eke         |2 4 K|_lcmpJ9755      |�������� ������ �����������|95|
C |I_ike         |2 4 K|_lcmpJ9754      |�������� ������ �����������|94|
C |I_oke         |2 4 K|_lcmpJ9753      |�������� ������ �����������|93|
C |I_uke         |2 4 K|_lcmpJ9751      |�������� ������ �����������|87|
C |I_ale         |2 4 K|_lcmpJ9749      |�������� ������ �����������|91|
C |I_ele         |2 4 K|_lcmpJ9747      |�������� ������ �����������|90|
C |I_ile         |2 4 K|_lcmpJ9745      |�������� ������ �����������|89|
C |I_ole         |2 4 K|_lcmpJ9744      |�������� ������ �����������|88|
C |I_ule         |2 4 K|_lcmpJ9738      |�������� ������ �����������|82|
C |I_ame         |2 4 K|_lcmpJ9737      |�������� ������ �����������|86|
C |I_eme         |2 4 K|_lcmpJ9736      |�������� ������ �����������|85|
C |I_ime         |2 4 K|_lcmpJ9735      |�������� ������ �����������|84|
C |I_ome         |2 4 K|_lcmpJ9734      |�������� ������ �����������|83|
C |I_ume         |2 4 K|_lcmpJ9732      |�������� ������ �����������|77|
C |I_ape         |2 4 K|_lcmpJ9730      |�������� ������ �����������|81|
C |I_epe         |2 4 K|_lcmpJ9728      |�������� ������ �����������|80|
C |I_ipe         |2 4 K|_lcmpJ9726      |�������� ������ �����������|79|
C |I_ope         |2 4 K|_lcmpJ9724      |�������� ������ �����������|78|
C |I_upe         |2 4 K|_lcmpJ9717      |�������� ������ �����������|62|
C |I_are         |2 4 K|_lcmpJ9716      |�������� ������ �����������|66|
C |I_ere         |2 4 K|_lcmpJ9715      |�������� ������ �����������|65|
C |I_ire         |2 4 K|_lcmpJ9714      |�������� ������ �����������|64|
C |I_ore         |2 4 K|_lcmpJ9713      |�������� ������ �����������|63|
C |I_ure         |2 4 K|_lcmpJ9711      |�������� ������ �����������|57|
C |I_ase         |2 4 K|_lcmpJ9709      |�������� ������ �����������|61|
C |I_ese         |2 4 K|_lcmpJ9707      |�������� ������ �����������|60|
C |I_ise         |2 4 K|_lcmpJ9705      |�������� ������ �����������|59|
C |I_ose         |2 4 K|_lcmpJ9703      |�������� ������ �����������|58|
C |I_use         |2 4 K|_lcmpJ9696      |�������� ������ �����������|52|
C |I_ate         |2 4 K|_lcmpJ9695      |�������� ������ �����������|56|
C |I_ete         |2 4 K|_lcmpJ9694      |�������� ������ �����������|55|
C |I_ite         |2 4 K|_lcmpJ9693      |�������� ������ �����������|54|
C |I_ote         |2 4 K|_lcmpJ9692      |�������� ������ �����������|53|
C |I_ute         |2 4 K|_lcmpJ9690      |�������� ������ �����������|47|
C |I_ave         |2 4 K|_lcmpJ9688      |�������� ������ �����������|51|
C |I_eve         |2 4 K|_lcmpJ9686      |�������� ������ �����������|50|
C |I_ive         |2 4 K|_lcmpJ9684      |�������� ������ �����������|49|
C |I_ove         |2 4 K|_lcmpJ9682      |�������� ������ �����������|48|
C |I_uve         |2 4 K|_lcmpJ9653      |�������� ������ �����������|72|
C |I_axe         |2 4 K|_lcmpJ9652      |�������� ������ �����������|76|
C |I_exe         |2 4 K|_lcmpJ9651      |�������� ������ �����������|75|
C |I_ixe         |2 4 K|_lcmpJ9650      |�������� ������ �����������|74|
C |I_oxe         |2 4 K|_lcmpJ9649      |�������� ������ �����������|73|
C |I_uxe         |2 4 K|_lcmpJ9647      |�������� ������ �����������|67|
C |I_abi         |2 4 K|_lcmpJ9645      |�������� ������ �����������|71|
C |I_ebi         |2 4 K|_lcmpJ9643      |�������� ������ �����������|70|
C |I_ibi         |2 4 K|_lcmpJ9641      |�������� ������ �����������|69|
C |I_obi         |2 4 K|_lcmpJ9640      |�������� ������ �����������|68|
C |I_ubi         |2 4 K|_lcmpJ9634      |�������� ������ �����������|42|
C |I_adi         |2 4 K|_lcmpJ9633      |�������� ������ �����������|46|
C |I_edi         |2 4 K|_lcmpJ9632      |�������� ������ �����������|45|
C |I_idi         |2 4 K|_lcmpJ9631      |�������� ������ �����������|44|
C |I_odi         |2 4 K|_lcmpJ9630      |�������� ������ �����������|43|
C |I_udi         |2 4 K|_lcmpJ9614      |�������� ������ �����������|16|
C |I_afi         |2 4 K|_lcmpJ9613      |�������� ������ �����������|15|
C |I_efi         |2 4 K|_lcmpJ9612      |�������� ������ �����������|14|
C |I_ifi         |2 4 K|_lcmpJ9611      |�������� ������ �����������|13|
C |I_ofi         |2 4 K|_lcmpJ9609      |�������� ������ �����������|12|
C |I_ufi         |2 4 K|_lcmpJ9605      |�������� ������ �����������|37|
C |I_aki         |2 4 K|_lcmpJ9602      |�������� ������ �����������|41|
C |I_eki         |2 4 K|_lcmpJ9600      |�������� ������ �����������|40|
C |I_iki         |2 4 K|_lcmpJ9598      |�������� ������ �����������|39|
C |I_oki         |2 4 K|_lcmpJ9595      |�������� ������ �����������|38|
C |I_uki         |2 4 I|CR              |������ ������������||
C |R_ali         |4 4 O|CIL_PKS6_MASS1  |||
C |R_eli         |4 4 O|CIL_PKS6_MASS2  |||
C |R_ili         |4 4 O|CIL_PKS6_MASS3  |||
C |R_oli         |4 4 O|CIL_PKS6_MASS4  |||
C |R_uli         |4 4 O|CIL_PKS6_MASS5  |||
C |R_ami         |4 4 O|CIL_KO6_MASS1   |||
C |R_emi         |4 4 O|CIL_KO6_MASS2   |||
C |R_imi         |4 4 O|CIL_KO6_MASS3   |||
C |R_omi         |4 4 O|CIL_KO6_MASS4   |||
C |R_umi         |4 4 O|CIL_KO6_MASS5   |||
C |R_api         |4 4 O|CIL_KO5_MASS5   |||
C |R_epi         |4 4 O|CIL_KO5_MASS4   |||
C |R_ipi         |4 4 O|CIL_KO5_MASS3   |||
C |R_opi         |4 4 O|CIL_KO5_MASS2   |||
C |R_upi         |4 4 O|CIL_KO5_MASS1   |||
C |R_ari         |4 4 O|CIL_PKS5_MASS5  |||
C |R_eri         |4 4 O|CIL_PKS5_MASS4  |||
C |R_iri         |4 4 O|CIL_PKS5_MASS3  |||
C |R_ori         |4 4 O|CIL_PKS5_MASS2  |||
C |R_uri         |4 4 O|CIL_PKS5_MASS1  |||
C |R_asi         |4 4 O|CIL_KO4_MASS5   |||
C |R_esi         |4 4 O|CIL_KO4_MASS4   |||
C |R_isi         |4 4 O|CIL_KO4_MASS3   |||
C |R_osi         |4 4 O|CIL_KO4_MASS2   |||
C |R_usi         |4 4 O|CIL_KO4_MASS1   |||
C |R_ati         |4 4 O|CIL_PKS4_MASS5  |||
C |R_eti         |4 4 O|CIL_PKS4_MASS4  |||
C |R_iti         |4 4 O|CIL_PKS4_MASS3  |||
C |R_oti         |4 4 O|CIL_PKS4_MASS2  |||
C |R_uti         |4 4 O|CIL_PKS4_MASS1  |||
C |R_avi         |4 4 O|CIL_20FDA60AE408_MASS1|||
C |R_evi         |4 4 O|CIL_20FDA60AE408_MASS2|||
C |R_ivi         |4 4 O|CIL_20FDA60AE408_MASS3|||
C |R_ovi         |4 4 O|CIL_20FDA60AE408_MASS4|||
C |R_uvi         |4 4 O|CIL_20FDA60AE408_MASS5|||
C |R_axi         |4 4 O|CIL_20FDA60AE413_MASS1|||
C |R_exi         |4 4 O|CIL_20FDA60AE413_MASS2|||
C |R_ixi         |4 4 O|CIL_20FDA60AE413_MASS3|||
C |R_oxi         |4 4 O|CIL_20FDA60AE413_MASS4|||
C |R_uxi         |4 4 O|CIL_20FDA60AE413_MASS5|||
C |R_abo         |4 4 O|CIL_KO3_MASS5   |||
C |R_ebo         |4 4 O|CIL_KO3_MASS4   |||
C |R_ibo         |4 4 O|CIL_KO3_MASS3   |||
C |R_obo         |4 4 O|CIL_KO3_MASS2   |||
C |R_ubo         |4 4 O|CIL_KO3_MASS1   |||
C |R_ado         |4 4 O|CIL_PKS3_MASS5  |||
C |R_edo         |4 4 O|CIL_PKS3_MASS4  |||
C |R_ido         |4 4 O|CIL_PKS3_MASS3  |||
C |R_odo         |4 4 O|CIL_PKS3_MASS2  |||
C |R_udo         |4 4 O|CIL_PKS3_MASS1  |||
C |R_afo         |4 4 O|CIL_PKS2_MASS1  |||
C |R_efo         |4 4 O|CIL_PKS2_MASS2  |||
C |R_ifo         |4 4 O|CIL_PKS2_MASS3  |||
C |R_ofo         |4 4 O|CIL_PKS2_MASS4  |||
C |R_ufo         |4 4 O|CIL_PKS2_MASS5  |||
C |R_ako         |4 4 O|CIL_KO2_MASS1   |||
C |R_eko         |4 4 O|CIL_KO2_MASS2   |||
C |R_iko         |4 4 O|CIL_KO2_MASS3   |||
C |R_oko         |4 4 O|CIL_KO2_MASS4   |||
C |R_uko         |4 4 O|CIL_KO2_MASS5   |||
C |R_alo         |4 4 O|CIL_KO1_MASS5   |||
C |R_elo         |4 4 O|CIL_KO1_MASS4   |||
C |R_ilo         |4 4 O|CIL_KO1_MASS3   |||
C |R_olo         |4 4 O|CIL_KO1_MASS2   |||
C |R_ulo         |4 4 O|CIL_KO1_MASS1   |||
C |R_amo         |4 4 O|CIL_PKS1_MASS5  |||
C |R_emo         |4 4 O|CIL_PKS1_MASS4  |||
C |R_imo         |4 4 O|CIL_PKS1_MASS3  |||
C |R_omo         |4 4 O|CIL_PKS1_MASS2  |||
C |R_umo         |4 4 I|MASS            |����� �������||
C |R_apo         |4 4 O|CIL_PKS1_MASS1  |||
C |L_aso         |1 1 O|IN_KO4          |||
C |L_eso         |1 1 O|IN_KO5          |||
C |L_iso         |1 1 O|IN_KO6          |||
C |L_oso         |1 1 O|IN_KO3          |||
C |L_uso         |1 1 O|IN_KO2          |||
C |L_ato         |1 1 O|IN_KO1          |||
C |L_eto         |1 1 O|IN_PKS4         |||
C |L_ito         |1 1 O|IN_PKS5         |||
C |L_oto         |1 1 O|IN_PKS6         |||
C |L_uto         |1 1 O|IN_PKS3         |||
C |L_avo         |1 1 O|IN_PKS2         |||
C |L_evo         |1 1 O|IN_PKS1         |||
C |I_ibu         |2 4 O|CIL_PKS6_N1     |||
C |I_obu         |2 4 O|CIL_PKS6_N2     |||
C |I_ubu         |2 4 O|CIL_KO6_N5      |||
C |I_adu         |2 4 O|CIL_KO6_N4      |||
C |I_edu         |2 4 O|CIL_KO6_N3      |||
C |I_idu         |2 4 O|CIL_KO6_N2      |||
C |I_odu         |2 4 O|CIL_KO6_N1      |||
C |I_udu         |2 4 O|CIL_PKS6_N5     |||
C |I_afu         |2 4 O|CIL_PKS6_N4     |||
C |I_efu         |2 4 O|CIL_PKS6_N3     |||
C |I_ilu         |2 4 O|CIL_PKS5_N1     |||
C |I_olu         |2 4 O|CIL_PKS5_N2     |||
C |I_ulu         |2 4 O|CIL_KO5_N5      |||
C |I_amu         |2 4 O|CIL_KO5_N4      |||
C |I_emu         |2 4 O|CIL_KO5_N3      |||
C |I_imu         |2 4 O|CIL_KO5_N2      |||
C |I_omu         |2 4 O|CIL_KO5_N1      |||
C |I_umu         |2 4 O|CIL_PKS5_N5     |||
C |I_apu         |2 4 O|CIL_PKS5_N4     |||
C |I_epu         |2 4 O|CIL_PKS5_N3     |||
C |I_isu         |2 4 O|CIL_PKS4_N1     |||
C |I_osu         |2 4 O|CIL_PKS4_N2     |||
C |I_usu         |2 4 O|CIL_KO4_N5      |||
C |I_atu         |2 4 O|CIL_KO4_N4      |||
C |I_etu         |2 4 O|CIL_KO4_N3      |||
C |I_itu         |2 4 O|CIL_KO4_N2      |||
C |I_otu         |2 4 O|CIL_KO4_N1      |||
C |I_utu         |2 4 O|CIL_PKS4_N5     |||
C |I_avu         |2 4 O|CIL_PKS4_N4     |||
C |I_evu         |2 4 O|CIL_PKS4_N3     |||
C |I_ibad        |2 4 O|CIL_PKS3_N1     |||
C |I_obad        |2 4 O|CIL_PKS3_N2     |||
C |I_ubad        |2 4 O|CIL_KO3_N5      |||
C |I_adad        |2 4 O|CIL_KO3_N4      |||
C |I_edad        |2 4 O|CIL_KO3_N3      |||
C |I_idad        |2 4 O|CIL_KO3_N2      |||
C |I_odad        |2 4 O|CIL_KO3_N1      |||
C |I_udad        |2 4 O|CIL_PKS3_N5     |||
C |I_afad        |2 4 O|CIL_PKS3_N4     |||
C |I_efad        |2 4 O|CIL_PKS3_N3     |||
C |I_ilad        |2 4 O|CIL_PKS2_N1     |||
C |I_olad        |2 4 O|CIL_PKS2_N2     |||
C |I_ulad        |2 4 O|CIL_KO2_N5      |||
C |I_amad        |2 4 O|CIL_KO2_N4      |||
C |I_emad        |2 4 O|CIL_KO2_N3      |||
C |I_imad        |2 4 O|CIL_KO2_N2      |||
C |I_omad        |2 4 O|CIL_KO2_N1      |||
C |I_umad        |2 4 O|CIL_PKS2_N5     |||
C |I_apad        |2 4 O|CIL_PKS2_N4     |||
C |I_epad        |2 4 O|CIL_PKS2_N3     |||
C |I_osad        |2 4 O|CIL_PKS1_N1     |||
C |I_usad        |2 4 O|CIL_PKS1_N2     |||
C |I_atad        |2 4 O|CIL_KO1_N5      |||
C |I_etad        |2 4 O|CIL_KO1_N4      |||
C |I_itad        |2 4 O|CIL_KO1_N3      |||
C |I_otad        |2 4 O|CIL_KO1_N2      |||
C |I_utad        |2 4 O|CIL_KO1_N1      |||
C |I_avad        |2 4 O|CIL_PKS1_N5     |||
C |I_evad        |2 4 O|CIL_PKS1_N4     |||
C |I_ivad        |2 4 O|CIL_PKS1_N3     |||
C |I_ovad        |2 4 O|CIL_20FDA60AE413_N5|||
C |I_uvad        |2 4 O|CIL_20FDA60AE413_N4|||
C |I_axad        |2 4 O|CIL_20FDA60AE413_N3|||
C |I_exad        |2 4 O|CIL_20FDA60AE413_N2|||
C |I_ixad        |2 4 O|CIL_20FDA60AE413_N1|||
C |I_oxad        |2 4 P|num_boat        |����� �������||
C |I_uxad        |2 4 O|CIL_20FDA60AE408_N5|||
C |I_abed        |2 4 O|CIL_20FDA60AE408_N4|||
C |I_ebed        |2 4 O|CIL_20FDA60AE408_N3|||
C |I_ibed        |2 4 O|CIL_20FDA60AE408_N2|||
C |I_obed        |2 4 O|CIL_20FDA60AE408_N1|||

      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      REAL*4 R_e
      INTEGER*4 I_i
      LOGICAL*1 L0_o
      INTEGER*4 I_u
      REAL*4 R0_ad
      INTEGER*4 I_ed
      REAL*4 R0_id,R_od
      LOGICAL*1 L0_ud
      INTEGER*4 I_af
      REAL*4 R_ef
      INTEGER*4 I_if
      LOGICAL*1 L0_of
      INTEGER*4 I_uf,I_ak,I_ek
      REAL*4 R_ik
      INTEGER*4 I_ok
      REAL*4 R_uk
      INTEGER*4 I_al
      REAL*4 R_el
      INTEGER*4 I_il
      REAL*4 R_ol
      INTEGER*4 I_ul
      REAL*4 R_am
      INTEGER*4 I_em
      REAL*4 R_im
      INTEGER*4 I_om
      REAL*4 R_um
      INTEGER*4 I_ap
      REAL*4 R_ep
      LOGICAL*1 L0_ip
      INTEGER*4 I_op,I_up
      REAL*4 R_ar
      LOGICAL*1 L0_er
      INTEGER*4 I_ir
      REAL*4 R_or
      LOGICAL*1 L0_ur
      INTEGER*4 I_as,I_es
      REAL*4 R_is
      LOGICAL*1 L0_os
      INTEGER*4 I_us,I_at
      REAL*4 R_et
      LOGICAL*1 L0_it
      INTEGER*4 I_ot,I_ut
      REAL*4 R_av
      LOGICAL*1 L0_ev
      INTEGER*4 I_iv,I_ov
      REAL*4 R_uv
      LOGICAL*1 L0_ax
      INTEGER*4 I_ex,I_ix
      REAL*4 R_ox
      LOGICAL*1 L0_ux
      INTEGER*4 I_abe,I_ebe
      REAL*4 R_ibe
      LOGICAL*1 L0_obe
      INTEGER*4 I_ube,I_ade
      REAL*4 R_ede
      LOGICAL*1 L0_ide
      INTEGER*4 I_ode,I_ude,I_afe,I_efe,I_ife,I_ofe,I_ufe
     &,I_ake,I_eke,I_ike,I_oke,I_uke,I_ale,I_ele,I_ile,I_ole
     &,I_ule,I_ame,I_eme,I_ime,I_ome,I_ume
      INTEGER*4 I_ape,I_epe,I_ipe,I_ope,I_upe,I_are,I_ere
     &,I_ire,I_ore,I_ure,I_ase,I_ese,I_ise,I_ose,I_use,I_ate
     &,I_ete,I_ite,I_ote,I_ute,I_ave,I_eve
      INTEGER*4 I_ive,I_ove,I_uve,I_axe,I_exe,I_ixe,I_oxe
     &,I_uxe,I_abi,I_ebi,I_ibi,I_obi,I_ubi,I_adi,I_edi,I_idi
     &,I_odi,I_udi,I_afi,I_efi,I_ifi,I_ofi
      INTEGER*4 I_ufi,I_aki,I_eki,I_iki,I_oki,I_uki
      REAL*4 R_ali,R_eli,R_ili,R_oli,R_uli,R_ami,R_emi,R_imi
     &,R_omi,R_umi,R_api,R_epi,R_ipi,R_opi,R_upi,R_ari,R_eri
     &,R_iri,R_ori,R_uri,R_asi,R_esi
      REAL*4 R_isi,R_osi,R_usi,R_ati,R_eti,R_iti,R_oti,R_uti
     &,R_avi,R_evi,R_ivi,R_ovi,R_uvi,R_axi,R_exi,R_ixi,R_oxi
     &,R_uxi,R_abo,R_ebo,R_ibo,R_obo
      REAL*4 R_ubo,R_ado,R_edo,R_ido,R_odo,R_udo,R_afo,R_efo
     &,R_ifo,R_ofo,R_ufo,R_ako,R_eko,R_iko,R_oko,R_uko,R_alo
     &,R_elo,R_ilo,R_olo,R_ulo,R_amo
      REAL*4 R_emo,R_imo,R_omo,R_umo,R_apo
      LOGICAL*1 L0_epo,L0_ipo,L0_opo,L0_upo,L0_aro,L0_ero
     &,L0_iro,L0_oro,L0_uro,L_aso,L_eso,L_iso,L_oso,L_uso
     &,L_ato,L_eto,L_ito,L_oto,L_uto,L_avo,L_evo,L0_ivo
      LOGICAL*1 L0_ovo,L0_uvo,L0_axo,L0_exo,L0_ixo,L0_oxo
     &,L0_uxo,L0_abu,L0_ebu
      INTEGER*4 I_ibu,I_obu,I_ubu,I_adu,I_edu,I_idu,I_odu
     &,I_udu,I_afu,I_efu
      LOGICAL*1 L0_ifu,L0_ofu,L0_ufu,L0_aku,L0_eku,L0_iku
     &,L0_oku,L0_uku,L0_alu,L0_elu
      INTEGER*4 I_ilu,I_olu,I_ulu,I_amu,I_emu,I_imu,I_omu
     &,I_umu,I_apu,I_epu
      LOGICAL*1 L0_ipu,L0_opu,L0_upu,L0_aru,L0_eru,L0_iru
     &,L0_oru,L0_uru,L0_asu,L0_esu
      INTEGER*4 I_isu,I_osu,I_usu,I_atu,I_etu,I_itu,I_otu
     &,I_utu,I_avu,I_evu
      LOGICAL*1 L0_ivu,L0_ovu,L0_uvu,L0_axu,L0_exu,L0_ixu
     &,L0_oxu,L0_uxu,L0_abad,L0_ebad
      INTEGER*4 I_ibad,I_obad,I_ubad,I_adad,I_edad,I_idad
     &,I_odad,I_udad,I_afad,I_efad
      LOGICAL*1 L0_ifad,L0_ofad,L0_ufad,L0_akad,L0_ekad,L0_ikad
     &,L0_okad,L0_ukad,L0_alad,L0_elad
      INTEGER*4 I_ilad,I_olad,I_ulad,I_amad,I_emad,I_imad
     &,I_omad,I_umad,I_apad,I_epad
      LOGICAL*1 L0_ipad,L0_opad,L0_upad,L0_arad,L0_erad,L0_irad
     &,L0_orad,L0_urad,L0_asad,L0_esad,L0_isad
      INTEGER*4 I_osad,I_usad,I_atad,I_etad,I_itad,I_otad
     &,I_utad,I_avad,I_evad,I_ivad,I_ovad,I_uvad,I_axad,I_exad
     &,I_ixad,I_oxad,I_uxad,I_abed,I_ebed,I_ibed,I_obed

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      L0_o=I_uki.eq.I_i
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 424,1718):���������� �������������
      if(L0_o) then
         I_u=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 447,1724):���� � ������������� �������
      if(L0_o) then
         R_e=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 447,1730):���� � ������������� �������
      R0_ad = 0.001
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 102,1387):��������� (RE4) (�������)
      L0_ud=I_uki.eq.I_ed
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  43,1374):���������� �������������
      if(L0_ud) then
         I_af=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  96,1380):���� � ������������� �������
      if(L0_ud) then
         R0_id=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  96,1384):���� � ������������� �������
      R_od = R0_ad * R0_id
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 106,1386):����������
      L0_of=I_uki.eq.I_if
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 424,1750):���������� �������������
      if(L0_of) then
         I_uf=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 447,1756):���� � ������������� �������
      if(L0_of) then
         R_ef=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 447,1762):���� � ������������� �������
      L0_ur=I_uki.eq.I_ak
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  43,1428):���������� �������������
      if(L0_ur) then
         I_as=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  96,1434):���� � ������������� �������
      if(L0_ur) then
         R_or=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  96,1439):���� � ������������� �������
      I_ek = 0
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 102,1306):��������� ������������� IN (�������)
      I_ok = 0
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 102,1316):��������� ������������� IN (�������)
      I_al = 0
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 102,1326):��������� ������������� IN (�������)
      I_il = 0
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 102,1336):��������� ������������� IN (�������)
      I_ul = 0
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 102,1346):��������� ������������� IN (�������)
      I_em = 0
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 101,1355):��������� ������������� IN (�������)
      I_om = 0
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 101,1364):��������� ������������� IN (�������)
      R_ik = 0.0
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 102,1301):��������� (RE4) (�������)
      R_uk = 0.0
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 102,1312):��������� (RE4) (�������)
      R_el = 0.0
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 102,1321):��������� (RE4) (�������)
      R_ol = 0.0
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 102,1331):��������� (RE4) (�������)
      R_am = 0.0
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 102,1340):��������� (RE4) (�������)
      R_im = 0.0
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 101,1350):��������� (RE4) (�������)
      R_um = 0.0
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 101,1360):��������� (RE4) (�������)
      L0_ip=I_uki.eq.I_ap
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  43,1392):���������� �������������
      if(L0_ip) then
         I_op=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  96,1397):���� � ������������� �������
      if(L0_ip) then
         R_ep=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  96,1402):���� � ������������� �������
      L0_er=I_uki.eq.I_up
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  43,1410):���������� �������������
      if(L0_er) then
         I_ir=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  96,1416):���� � ������������� �������
      if(L0_er) then
         R_ar=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  96,1421):���� � ������������� �������
      L0_os=I_uki.eq.I_es
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  43,1446):���������� �������������
      if(L0_os) then
         I_us=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  96,1451):���� � ������������� �������
      if(L0_os) then
         R_is=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  96,1456):���� � ������������� �������
      L0_it=I_uki.eq.I_at
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  43,1462):���������� �������������
      if(L0_it) then
         I_ot=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  96,1468):���� � ������������� �������
      if(L0_it) then
         R_et=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  96,1473):���� � ������������� �������
      L0_ev=I_uki.eq.I_ut
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  43,1480):���������� �������������
      if(L0_ev) then
         I_iv=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  96,1485):���� � ������������� �������
      if(L0_ev) then
         R_av=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  96,1490):���� � ������������� �������
      L0_ax=I_uki.eq.I_ov
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  43,1497):���������� �������������
      if(L0_ax) then
         I_ex=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  96,1502):���� � ������������� �������
      if(L0_ax) then
         R_uv=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  96,1508):���� � ������������� �������
      L0_ux=I_uki.eq.I_ix
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  43,1514):���������� �������������
      if(L0_ux) then
         I_abe=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  96,1520):���� � ������������� �������
      if(L0_ux) then
         R_ox=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  96,1524):���� � ������������� �������
      L0_obe=I_uki.eq.I_ebe
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  43,1532):���������� �������������
      if(L0_obe) then
         I_ube=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  96,1538):���� � ������������� �������
      if(L0_obe) then
         R_ibe=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  96,1543):���� � ������������� �������
      L0_ide=I_uki.eq.I_ade
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  43,1552):���������� �������������
      if(L0_ide) then
         I_ode=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  96,1557):���� � ������������� �������
      if(L0_ide) then
         R_ede=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  96,1562):���� � ������������� �������
      L0_epo=I_uki.eq.I_ude
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 424,1787):���������� �������������
      if(L0_epo) then
         I_ovad=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 447,1794):���� � ������������� �������
      if(L0_epo) then
         R_uxi=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 447,1799):���� � ������������� �������
      L0_ipo=I_uki.eq.I_afe
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 424,1807):���������� �������������
      if(L0_ipo) then
         I_uvad=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 447,1814):���� � ������������� �������
      if(L0_ipo) then
         R_oxi=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 447,1819):���� � ������������� �������
      L0_opo=I_uki.eq.I_efe
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 424,1828):���������� �������������
      if(L0_opo) then
         I_axad=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 447,1835):���� � ������������� �������
      if(L0_opo) then
         R_ixi=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 447,1840):���� � ������������� �������
      L0_upo=I_uki.eq.I_ife
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 424,1848):���������� �������������
      if(L0_upo) then
         I_exad=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 447,1856):���� � ������������� �������
      if(L0_upo) then
         R_exi=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 447,1860):���� � ������������� �������
      L0_aro=I_uki.eq.I_ofe
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 424,1870):���������� �������������
      if(L0_aro) then
         I_ixad=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 447,1878):���� � ������������� �������
      if(L0_aro) then
         R_axi=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 447,1882):���� � ������������� �������
      L0_exo=I_uki.eq.I_ufe
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 292,1650):���������� �������������
      if(L0_exo) then
         I_odu=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 318,1656):���� � ������������� �������
      if(L0_exo) then
         R_ami=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 318,1661):���� � ������������� �������
      L0_ivo=I_uki.eq.I_ake
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 292,1573):���������� �������������
      if(L0_ivo) then
         I_ubu=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 317,1578):���� � ������������� �������
      if(L0_ivo) then
         R_umi=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 317,1584):���� � ������������� �������
      L0_ovo=I_uki.eq.I_eke
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 292,1592):���������� �������������
      if(L0_ovo) then
         I_adu=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 318,1598):���� � ������������� �������
      if(L0_ovo) then
         R_omi=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 318,1603):���� � ������������� �������
      L0_uvo=I_uki.eq.I_ike
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 292,1612):���������� �������������
      if(L0_uvo) then
         I_edu=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 318,1617):���� � ������������� �������
      if(L0_uvo) then
         R_imi=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 318,1622):���� � ������������� �������
      L0_axo=I_uki.eq.I_oke
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 292,1631):���������� �������������
      if(L0_axo) then
         I_idu=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 318,1636):���� � ������������� �������
      if(L0_axo) then
         R_emi=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 318,1642):���� � ������������� �������
      L_iso = L0_exo.OR.L0_axo.OR.L0_uvo.OR.L0_ovo.OR.L0_ivo
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 354,1612):���
      L0_ebu=I_uki.eq.I_uke
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 292,1754):���������� �������������
      if(L0_ebu) then
         I_ibu=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 317,1760):���� � ������������� �������
      if(L0_ebu) then
         R_ali=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 317,1764):���� � ������������� �������
      L0_ixo=I_uki.eq.I_ale
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 292,1670):���������� �������������
      if(L0_ixo) then
         I_udu=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 318,1676):���� � ������������� �������
      if(L0_ixo) then
         R_uli=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 318,1680):���� � ������������� �������
      L0_oxo=I_uki.eq.I_ele
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 292,1690):���������� �������������
      if(L0_oxo) then
         I_afu=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 318,1696):���� � ������������� �������
      if(L0_oxo) then
         R_oli=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 318,1700):���� � ������������� �������
      L0_uxo=I_uki.eq.I_ile
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 292,1711):���������� �������������
      if(L0_uxo) then
         I_efu=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 318,1716):���� � ������������� �������
      if(L0_uxo) then
         R_ili=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 318,1721):���� � ������������� �������
      L0_abu=I_uki.eq.I_ole
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 292,1732):���������� �������������
      if(L0_abu) then
         I_obu=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 318,1736):���� � ������������� �������
      if(L0_abu) then
         R_eli=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 318,1742):���� � ������������� �������
      L_oto = L0_ebu.OR.L0_abu.OR.L0_uxo.OR.L0_oxo.OR.L0_ixo
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 353,1711):���
      L0_eku=I_uki.eq.I_ule
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 178,1650):���������� �������������
      if(L0_eku) then
         I_omu=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 205,1656):���� � ������������� �������
      if(L0_eku) then
         R_upi=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 205,1661):���� � ������������� �������
      L0_ifu=I_uki.eq.I_ame
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 178,1573):���������� �������������
      if(L0_ifu) then
         I_ulu=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 205,1578):���� � ������������� �������
      if(L0_ifu) then
         R_api=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 205,1584):���� � ������������� �������
      L0_ofu=I_uki.eq.I_eme
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 179,1592):���������� �������������
      if(L0_ofu) then
         I_amu=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 205,1598):���� � ������������� �������
      if(L0_ofu) then
         R_epi=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 205,1603):���� � ������������� �������
      L0_ufu=I_uki.eq.I_ime
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 179,1612):���������� �������������
      if(L0_ufu) then
         I_emu=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 205,1617):���� � ������������� �������
      if(L0_ufu) then
         R_ipi=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 205,1622):���� � ������������� �������
      L0_aku=I_uki.eq.I_ome
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 179,1631):���������� �������������
      if(L0_aku) then
         I_imu=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 205,1636):���� � ������������� �������
      if(L0_aku) then
         R_opi=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 205,1642):���� � ������������� �������
      L_eso = L0_eku.OR.L0_aku.OR.L0_ufu.OR.L0_ofu.OR.L0_ifu
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 240,1612):���
      L0_elu=I_uki.eq.I_ume
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 178,1754):���������� �������������
      if(L0_elu) then
         I_ilu=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 204,1760):���� � ������������� �������
      if(L0_elu) then
         R_uri=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 204,1764):���� � ������������� �������
      L0_iku=I_uki.eq.I_ape
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 178,1670):���������� �������������
      if(L0_iku) then
         I_umu=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 205,1676):���� � ������������� �������
      if(L0_iku) then
         R_ari=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 205,1680):���� � ������������� �������
      L0_oku=I_uki.eq.I_epe
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 178,1690):���������� �������������
      if(L0_oku) then
         I_apu=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 205,1696):���� � ������������� �������
      if(L0_oku) then
         R_eri=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 205,1700):���� � ������������� �������
      L0_uku=I_uki.eq.I_ipe
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 178,1711):���������� �������������
      if(L0_uku) then
         I_epu=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 204,1716):���� � ������������� �������
      if(L0_uku) then
         R_iri=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 204,1721):���� � ������������� �������
      L0_alu=I_uki.eq.I_ope
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 178,1732):���������� �������������
      if(L0_alu) then
         I_olu=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 204,1736):���� � ������������� �������
      if(L0_alu) then
         R_ori=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 204,1742):���� � ������������� �������
      L_ito = L0_elu.OR.L0_alu.OR.L0_uku.OR.L0_oku.OR.L0_iku
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 240,1711):���
      L0_exu=I_uki.eq.I_upe
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 292,1872):���������� �������������
      if(L0_exu) then
         I_odad=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 318,1878):���� � ������������� �������
      if(L0_exu) then
         R_ubo=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 318,1883):���� � ������������� �������
      L0_ivu=I_uki.eq.I_are
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 292,1795):���������� �������������
      if(L0_ivu) then
         I_ubad=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 317,1800):���� � ������������� �������
      if(L0_ivu) then
         R_abo=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 317,1806):���� � ������������� �������
      L0_ovu=I_uki.eq.I_ere
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 292,1814):���������� �������������
      if(L0_ovu) then
         I_adad=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 318,1820):���� � ������������� �������
      if(L0_ovu) then
         R_ebo=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 318,1825):���� � ������������� �������
      L0_uvu=I_uki.eq.I_ire
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 292,1834):���������� �������������
      if(L0_uvu) then
         I_edad=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 318,1839):���� � ������������� �������
      if(L0_uvu) then
         R_ibo=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 318,1844):���� � ������������� �������
      L0_axu=I_uki.eq.I_ore
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 292,1853):���������� �������������
      if(L0_axu) then
         I_idad=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 318,1858):���� � ������������� �������
      if(L0_axu) then
         R_obo=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 318,1864):���� � ������������� �������
      L_oso = L0_exu.OR.L0_axu.OR.L0_uvu.OR.L0_ovu.OR.L0_ivu
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 354,1834):���
      L0_ebad=I_uki.eq.I_ure
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 292,1976):���������� �������������
      if(L0_ebad) then
         I_ibad=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 317,1982):���� � ������������� �������
      if(L0_ebad) then
         R_udo=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 317,1986):���� � ������������� �������
      L0_ixu=I_uki.eq.I_ase
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 292,1892):���������� �������������
      if(L0_ixu) then
         I_udad=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 318,1898):���� � ������������� �������
      if(L0_ixu) then
         R_ado=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 318,1902):���� � ������������� �������
      L0_oxu=I_uki.eq.I_ese
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 292,1912):���������� �������������
      if(L0_oxu) then
         I_afad=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 318,1918):���� � ������������� �������
      if(L0_oxu) then
         R_edo=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 318,1922):���� � ������������� �������
      L0_uxu=I_uki.eq.I_ise
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 292,1933):���������� �������������
      if(L0_uxu) then
         I_efad=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 318,1938):���� � ������������� �������
      if(L0_uxu) then
         R_ido=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 318,1943):���� � ������������� �������
      L0_abad=I_uki.eq.I_ose
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 292,1954):���������� �������������
      if(L0_abad) then
         I_obad=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 318,1958):���� � ������������� �������
      if(L0_abad) then
         R_odo=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 318,1964):���� � ������������� �������
      L_uto = L0_ebad.OR.L0_abad.OR.L0_uxu.OR.L0_oxu.OR.L0_ixu
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 353,1933):���
      L0_ekad=I_uki.eq.I_use
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 178,1872):���������� �������������
      if(L0_ekad) then
         I_omad=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 204,1878):���� � ������������� �������
      if(L0_ekad) then
         R_ako=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 204,1883):���� � ������������� �������
      L0_ifad=I_uki.eq.I_ate
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 178,1795):���������� �������������
      if(L0_ifad) then
         I_ulad=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 204,1800):���� � ������������� �������
      if(L0_ifad) then
         R_uko=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 204,1806):���� � ������������� �������
      L0_ofad=I_uki.eq.I_ete
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 179,1814):���������� �������������
      if(L0_ofad) then
         I_amad=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 204,1820):���� � ������������� �������
      if(L0_ofad) then
         R_oko=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 204,1825):���� � ������������� �������
      L0_ufad=I_uki.eq.I_ite
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 179,1834):���������� �������������
      if(L0_ufad) then
         I_emad=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 205,1839):���� � ������������� �������
      if(L0_ufad) then
         R_iko=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 205,1844):���� � ������������� �������
      L0_akad=I_uki.eq.I_ote
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 179,1853):���������� �������������
      if(L0_akad) then
         I_imad=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 204,1858):���� � ������������� �������
      if(L0_akad) then
         R_eko=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 204,1864):���� � ������������� �������
      L_uso = L0_ekad.OR.L0_akad.OR.L0_ufad.OR.L0_ofad.OR.L0_ifad
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 240,1834):���
      L0_elad=I_uki.eq.I_ute
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 178,1976):���������� �������������
      if(L0_elad) then
         I_ilad=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 204,1982):���� � ������������� �������
      if(L0_elad) then
         R_afo=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 204,1986):���� � ������������� �������
      L0_ikad=I_uki.eq.I_ave
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 178,1892):���������� �������������
      if(L0_ikad) then
         I_umad=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 205,1898):���� � ������������� �������
      if(L0_ikad) then
         R_ufo=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 205,1902):���� � ������������� �������
      L0_okad=I_uki.eq.I_eve
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 178,1912):���������� �������������
      if(L0_okad) then
         I_apad=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 205,1918):���� � ������������� �������
      if(L0_okad) then
         R_ofo=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 205,1922):���� � ������������� �������
      L0_ukad=I_uki.eq.I_ive
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 178,1933):���������� �������������
      if(L0_ukad) then
         I_epad=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 204,1938):���� � ������������� �������
      if(L0_ukad) then
         R_ifo=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 204,1943):���� � ������������� �������
      L0_alad=I_uki.eq.I_ove
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 178,1954):���������� �������������
      if(L0_alad) then
         I_olad=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 204,1958):���� � ������������� �������
      if(L0_alad) then
         R_efo=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 204,1964):���� � ������������� �������
      L_avo = L0_elad.OR.L0_alad.OR.L0_ukad.OR.L0_okad.OR.L0_ikad
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 240,1933):���
      L0_eru=I_uki.eq.I_uve
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  43,1651):���������� �������������
      if(L0_eru) then
         I_otu=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  98,1656):���� � ������������� �������
      if(L0_eru) then
         R_usi=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  98,1662):���� � ������������� �������
      L0_ipu=I_uki.eq.I_axe
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  43,1574):���������� �������������
      if(L0_ipu) then
         I_usu=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  98,1579):���� � ������������� �������
      if(L0_ipu) then
         R_asi=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  98,1584):���� � ������������� �������
      L0_opu=I_uki.eq.I_exe
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  44,1593):���������� �������������
      if(L0_opu) then
         I_atu=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  98,1598):���� � ������������� �������
      if(L0_opu) then
         R_esi=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  98,1604):���� � ������������� �������
      L0_upu=I_uki.eq.I_ixe
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  43,1612):���������� �������������
      if(L0_upu) then
         I_etu=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  98,1618):���� � ������������� �������
      if(L0_upu) then
         R_isi=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  98,1622):���� � ������������� �������
      L0_aru=I_uki.eq.I_oxe
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  43,1632):���������� �������������
      if(L0_aru) then
         I_itu=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  98,1637):���� � ������������� �������
      if(L0_aru) then
         R_osi=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  98,1642):���� � ������������� �������
      L_aso = L0_eru.OR.L0_aru.OR.L0_upu.OR.L0_opu.OR.L0_ipu
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 134,1612):���
      L0_esu=I_uki.eq.I_uxe
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  44,1754):���������� �������������
      if(L0_esu) then
         I_isu=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  96,1760):���� � ������������� �������
      if(L0_esu) then
         R_uti=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  96,1765):���� � ������������� �������
      L0_iru=I_uki.eq.I_abi
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  43,1670):���������� �������������
      if(L0_iru) then
         I_utu=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  98,1676):���� � ������������� �������
      if(L0_iru) then
         R_ati=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  98,1681):���� � ������������� �������
      L0_oru=I_uki.eq.I_ebi
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  43,1690):���������� �������������
      if(L0_oru) then
         I_avu=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  98,1696):���� � ������������� �������
      if(L0_oru) then
         R_eti=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  98,1701):���� � ������������� �������
      L0_uru=I_uki.eq.I_ibi
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  43,1712):���������� �������������
      if(L0_uru) then
         I_evu=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  98,1716):���� � ������������� �������
      if(L0_uru) then
         R_iti=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  98,1722):���� � ������������� �������
      L0_asu=I_uki.eq.I_obi
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  43,1732):���������� �������������
      if(L0_asu) then
         I_osu=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  97,1737):���� � ������������� �������
      if(L0_asu) then
         R_oti=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  97,1742):���� � ������������� �������
      L_eto = L0_esu.OR.L0_asu.OR.L0_uru.OR.L0_oru.OR.L0_iru
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 132,1712):���
      L0_erad=I_uki.eq.I_ubi
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  43,1873):���������� �������������
      if(L0_erad) then
         I_utad=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  97,1878):���� � ������������� �������
      if(L0_erad) then
         R_ulo=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  97,1884):���� � ������������� �������
      L0_ipad=I_uki.eq.I_adi
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  43,1796):���������� �������������
      if(L0_ipad) then
         I_atad=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  96,1801):���� � ������������� �������
      if(L0_ipad) then
         R_alo=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  96,1806):���� � ������������� �������
      L0_opad=I_uki.eq.I_edi
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  44,1815):���������� �������������
      if(L0_opad) then
         I_etad=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  98,1820):���� � ������������� �������
      if(L0_opad) then
         R_elo=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  98,1826):���� � ������������� �������
      L0_upad=I_uki.eq.I_idi
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  44,1834):���������� �������������
      if(L0_upad) then
         I_itad=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  98,1840):���� � ������������� �������
      if(L0_upad) then
         R_ilo=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  98,1844):���� � ������������� �������
      L0_arad=I_uki.eq.I_odi
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  44,1854):���������� �������������
      if(L0_arad) then
         I_otad=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  96,1859):���� � ������������� �������
      if(L0_arad) then
         R_olo=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  96,1864):���� � ������������� �������
      L_ato = L0_erad.OR.L0_arad.OR.L0_upad.OR.L0_opad.OR.L0_ipad
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 133,1834):���
      L0_ero=I_uki.eq.I_udi
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 424,1891):���������� �������������
      if(L0_ero) then
         I_uxad=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 447,1898):���� � ������������� �������
      if(L0_ero) then
         R_uvi=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 447,1903):���� � ������������� �������
      L0_iro=I_uki.eq.I_afi
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 424,1911):���������� �������������
      if(L0_iro) then
         I_abed=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 447,1918):���� � ������������� �������
      if(L0_iro) then
         R_ovi=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 447,1923):���� � ������������� �������
      L0_oro=I_uki.eq.I_efi
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 424,1932):���������� �������������
      if(L0_oro) then
         I_ebed=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 447,1939):���� � ������������� �������
      if(L0_oro) then
         R_ivi=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 447,1944):���� � ������������� �������
      L0_uro=I_uki.eq.I_ifi
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 424,1952):���������� �������������
      if(L0_uro) then
         I_ibed=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 447,1960):���� � ������������� �������
      if(L0_uro) then
         R_evi=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 447,1964):���� � ������������� �������
      L0_isad=I_uki.eq.I_ofi
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 424,1974):���������� �������������
      if(L0_isad) then
         I_obed=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 447,1982):���� � ������������� �������
      if(L0_isad) then
         R_avi=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 447,1986):���� � ������������� �������
      L0_esad=I_uki.eq.I_ufi
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  44,1978):���������� �������������
      if(L0_esad) then
         I_osad=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  96,1982):���� � ������������� �������
      if(L0_esad) then
         R_apo=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  96,1987):���� � ������������� �������
      L0_irad=I_uki.eq.I_aki
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  43,1892):���������� �������������
      if(L0_irad) then
         I_avad=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  98,1898):���� � ������������� �������
      if(L0_irad) then
         R_amo=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  98,1903):���� � ������������� �������
      L0_orad=I_uki.eq.I_eki
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  43,1912):���������� �������������
      if(L0_orad) then
         I_evad=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  98,1918):���� � ������������� �������
      if(L0_orad) then
         R_emo=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  98,1923):���� � ������������� �������
      L0_urad=I_uki.eq.I_iki
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  43,1934):���������� �������������
      if(L0_urad) then
         I_ivad=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  97,1938):���� � ������������� �������
      if(L0_urad) then
         R_imo=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  97,1944):���� � ������������� �������
      L0_asad=I_uki.eq.I_oki
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  44,1954):���������� �������������
      if(L0_asad) then
         I_usad=I_oxad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  96,1959):���� � ������������� �������
      if(L0_asad) then
         R_omo=R_umo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  96,1964):���� � ������������� �������
      L_evo = L0_esad.OR.L0_asad.OR.L0_urad.OR.L0_orad.OR.L0_irad
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 132,1934):���
      End
