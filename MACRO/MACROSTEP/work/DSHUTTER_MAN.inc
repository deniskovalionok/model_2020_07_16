      Interface
      Subroutine DSHUTTER_MAN(ext_deltat,R_eli,L_uki,L_afi
     &,R8_ute,C30_ed,L_af,L_uf,L_ak,I_uk,I_al,R_ul,R_em,R_im
     &,R_om,R_um,R_ap,R_ep,R_ip,I_op,I_er,I_ur,I_os,L_us,L_et
     &,L_it,L_ot,L_ut,L_ev,L_iv,L_ex,L_ix,L_abe,L_ebe,L_obe
     &,L_ube,L_ede,R8_ode,R_ife,R8_ake,L_eke,L_oke,L_ete,R_eve
     &,R_ove,L_odi,L_eki,L_ili,L_oli,L_uli,L_ami,L_emi,L_imi
     &)
C |R_eli         |4 4 I|11 mlfpar19     |��������� ������������|0.6|
C |L_uki         |1 1 O|13 cbo          |�� ������� (���)|F|
C |L_afi         |1 1 O|14 cbc          |�� ������� (���)|F|
C |R8_ute        |4 8 O|16 value        |��� �������� ��������|0.5|
C |C30_ed        |3 30 O|state           |||
C |L_af          |1 1 I|tech_mode       |||
C |L_uf          |1 1 I|ruch_mode       |||
C |L_ak          |1 1 I|avt_mode        |||
C |I_uk          |2 4 O|LM              |�����||
C |I_al          |2 4 O|LF              |����� �������������||
C |R_ul          |4 4 O|POS_CL          |��������||
C |R_em          |4 4 O|POS_OP          |��������||
C |R_im          |4 4 S|vmstop_button_ST*|��������� ������ "������� ���� �� ���������" |0.0|
C |R_om          |4 4 I|vmstop_button   |������� ������ ������ "������� ���� �� ���������" |0.0|
C |R_um          |4 4 S|vmclose_button_ST*|��������� ������ "������� ������� �� ���������" |0.0|
C |R_ap          |4 4 I|vmclose_button  |������� ������ ������ "������� ������� �� ���������" |0.0|
C |R_ep          |4 4 S|vmopen_button_ST*|��������� ������ "������� ������� �� ���������" |0.0|
C |R_ip          |4 4 I|vmopen_button   |������� ������ ������ "������� ������� �� ���������" |0.0|
C |I_op          |2 4 O|LST             |����� ����||
C |I_er          |2 4 O|LCL             |����� �������||
C |I_ur          |2 4 O|LOP             |����� �������||
C |I_os          |2 4 O|LS              |��������� �������||
C |L_us          |1 1 I|vlv_kvit        |||
C |L_et          |1 1 I|instr_fault     |||
C |L_it          |1 1 S|_qffJ453*       |�������� ������ Q RS-��������  |F|
C |L_ot          |1 1 O|vmclose_button_CMD*|[TF]����� ������ ������� ������� �� ���������|F|
C |L_ut          |1 1 O|vmopen_button_CMD*|[TF]����� ������ ������� ������� �� ���������|F|
C |L_ev          |1 1 I|ulubclose       |���������� ������� �� ���������|F|
C |L_iv          |1 1 I|ulubopen        |���������� ������� �� ���������|F|
C |L_ex          |1 1 O|block           |||
C |L_ix          |1 1 O|vmstop_button_CMD*|[TF]����� ������ ������� ���� �� ���������|F|
C |L_abe         |1 1 O|STOP            |�������||
C |L_ebe         |1 1 S|_qff10*         |�������� ������ Q RS-��������  |F|
C |L_obe         |1 1 O|fault           |�������������||
C |L_ube         |1 1 O|norm            |�����||
C |L_ede         |1 1 O|nopower         |��� ����������||
C |R8_ode        |4 8 I|voltage         |[��]���������� �� ������||
C |R_ife         |4 4 I|power           |�������� ��������||
C |R8_ake        |4 8 O|sn_power        |�������� ���� ��|3.0|
C |L_eke         |1 1 I|uluclose        |������� ������� �� ����������|F|
C |L_oke         |1 1 I|uluopen         |������� ������� �� ����������|F|
C |L_ete         |1 1 S|_qff6*          |�������� ������ Q RS-��������  |F|
C |R_eve         |4 4 O|mf_value        |��� ��������� �����|0.5|
C |R_ove         |4 4 I|tcl_top         |����� ����|30.0|
C |L_odi         |1 1 S|_qff2*          |�������� ������ Q RS-��������  |F|
C |L_eki         |1 1 S|_qff1*          |�������� ������ Q RS-��������  |F|
C |L_ili         |1 1 I|mlf24           |��� ���������� � ����������||
C |L_oli         |1 1 I|mlf23           |������� ������� �����||
C |L_uli         |1 1 I|mlf22           |����� ����� ��������||
C |L_ami         |1 1 I|mlf04           |�������� �� ��������||
C |L_emi         |1 1 I|mlf03           |�������� �� ��������||
C |L_imi         |1 1 I|mlf19           |���� ������������||

      IMPLICIT NONE
      REAL*4 ext_deltat
      CHARACTER*30 C30_ed
      LOGICAL*1 L_af,L_uf,L_ak
      INTEGER*4 I_uk,I_al
      REAL*4 R_ul,R_em,R_im,R_om,R_um,R_ap,R_ep,R_ip
      INTEGER*4 I_op,I_er,I_ur,I_os
      LOGICAL*1 L_us,L_et,L_it,L_ot,L_ut,L_ev,L_iv,L_ex,L_ix
     &,L_abe,L_ebe,L_obe,L_ube,L_ede
      REAL*8 R8_ode
      REAL*4 R_ife
      REAL*8 R8_ake
      LOGICAL*1 L_eke,L_oke,L_ete
      REAL*8 R8_ute
      REAL*4 R_eve,R_ove
      LOGICAL*1 L_odi,L_afi,L_eki,L_uki
      REAL*4 R_eli
      LOGICAL*1 L_ili,L_oli,L_uli,L_ami,L_emi,L_imi
      End subroutine DSHUTTER_MAN
      End interface
