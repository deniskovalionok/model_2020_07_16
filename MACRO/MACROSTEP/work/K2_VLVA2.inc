      Interface
      Subroutine K2_VLVA2(ext_deltat,R_o,R_ad,L_ed,R_od,R8_ik
     &,R8_ul,I_om,R_ap,R_ep,R_es,R_uv,R_ax,R_ix,R8_ebe,R_obe
     &,R_ake,L_eke,R_ale,L_ole,L_ule,L_eme,L_ime,L_ome,L_ume
     &,L_ape,L_epe,L_ipe,L_ere,L_ire,L_ure,L_ove,L_uve,L_exe
     &,L_adi,L_edi,L_idi,L_odi,L_udi,L_afi,L_efi,L_ifi,L_ofi
     &,L_ufi,L_aki,L_eki,L_iki,L_oki)
C |R_o           |4 4 K|_tdel1          |[]�������� ������� �������� ������|2.0|
C |R_ad          |4 4 S|_sdel1*         |[���]���������� ��������� �������� ������ |0.0|
C |L_ed          |1 1 S|_idel1*         |[TF]���������� ��������� �������� ������ |f|
C |R_od          |4 4 K|_c02            |�������� ��������� ����������|`power_c`|
C |R8_ik         |4 8 I|voltage         |[�]���������� ������ �������� �������||
C |R8_ul         |4 8 O|sn_power        |�������� ���� ��|3.0|
C |I_om          |2 4 I|SF1SWT          |������� ������� �������||
C |R_ap          |4 4 O|out_value_prc   |���.��������� ����� � %|50|
C |R_ep          |4 4 S|_slpb1*         |���������� ��������� ||
C |R_es          |4 4 I|tau             |���������� ������� �������|0.0|
C |R_uv          |4 4 I|tclose          |������ ����� ��������|30.0|
C |R_ax          |4 4 I|topen           |������ ����� ��������|30.0|
C |R_ix          |4 4 O|_oapr1*         |�������� ������ ���������. ����� |0.0|
C |R8_ebe        |4 8 O|value           |���.�������� ��������|0.5|
C |R_obe         |4 4 O|out_value       |���.��������� �����|0.5|
C |R_ake         |4 4 I|GM19P           |��������� ������������|0.6|
C |L_eke         |1 1 S|_qff3*          |�������� ������ Q RS-��������  |F|
C |R_ale         |4 4 O|posfbst         |��������� ���.��|0.5|
C |L_ole         |1 1 I|GM47            |��� ��������� ���������||
C |L_ule         |1 1 I|blockreg        |���������� ���� �����������|F|
C |L_eme         |1 1 I|uluclose        |������� ������� (���)||
C |L_ime         |1 1 I|uluopen         |������� ������� (���)||
C |L_ome         |1 1 I|blockblk        |���������� ���� ����������|F|
C |L_ume         |1 1 O|cbc             |�� ������� (���)|F|
C |L_ape         |1 1 O|cbo             |�� ������� (���)|F|
C |L_epe         |1 1 O|cbcm            |������� 0.001|F|
C |L_ipe         |1 1 O|outc            |��� ������� �������|F|
C |L_ere         |1 1 O|RASP            |��� �������� � ����.�������|F|
C |L_ire         |1 1 O|outo            |��� ������� �������|F|
C |L_ure         |1 1 O|cbom            |������� 0.999|F|
C |L_ove         |1 1 O|cmdcl           |������� ������� (���)||
C |L_uve         |1 1 I|arstate         |��������� ����������|F|
C |L_exe         |1 1 O|cmdop           |������� ������� (���)||
C |L_adi         |1 1 I|tzclose         |������� ������� (��)|F|
C |L_edi         |1 1 I|blkclose        |���� �������� (��)||
C |L_idi         |1 1 I|blkopen         |����. �������� (��)||
C |L_odi         |1 1 I|tzopen          |������� ������� (��)|F|
C |L_udi         |1 1 I|GM45            |����. ������ �� ���||
C |L_afi         |1 1 I|GM03            |������� � ��������||
C |L_efi         |1 1 I|GM04            |������� � ��������||
C |L_ifi         |1 1 I|GM19            |���� ������������||
C |L_ofi         |1 1 I|GM08            |������� �� "�������"||
C |L_ufi         |1 1 I|GM09            |������� �� "�������"||
C |L_aki         |1 1 I|GM22            |����� ����� ��������||
C |L_eki         |1 1 I|GM23            |������� ������� �����||
C |L_iki         |1 1 I|GM24            |��� ���������� �� � ����������||
C |L_oki         |1 1 I|GM46            |�� �����������||

      IMPLICIT NONE
      REAL*4 ext_deltat
      REAL*4 R_o,R_ad
      LOGICAL*1 L_ed
      REAL*4 R_od
      REAL*8 R8_ik,R8_ul
      INTEGER*4 I_om
      REAL*4 R_ap,R_ep,R_es,R_uv,R_ax,R_ix
      REAL*8 R8_ebe
      REAL*4 R_obe,R_ake
      LOGICAL*1 L_eke
      REAL*4 R_ale
      LOGICAL*1 L_ole,L_ule,L_eme,L_ime,L_ome,L_ume,L_ape
     &,L_epe,L_ipe,L_ere,L_ire,L_ure,L_ove,L_uve,L_exe,L_adi
     &,L_edi,L_idi,L_odi,L_udi,L_afi,L_efi
      LOGICAL*1 L_ifi,L_ofi,L_ufi,L_aki,L_eki,L_iki,L_oki
      End subroutine K2_VLVA2
      End interface
