      Interface
      Subroutine REG_MAN(ext_deltat,R_ixe,R8_ime,R_i,R_o,L_u
     &,R_id,R_od,L_ud,R_if,R_uf,R_ak,R_ek,R_ik,R_ok,R_uk,R_al
     &,I_el,L_ol,L_am,L_em,L_im,L_om,L_ap,L_ep,L_ar,L_er,L_ur
     &,L_as,L_is,L_us,R8_et,R_av,R8_ov,L_uv,L_ex,L_obe,I_ube
     &,L_ule,R_ume,R_epe,L_ite,L_axe,L_oxe,L_uxe,L_abi,L_ebi
     &,L_ibi,L_obi)
C |R_ixe         |4 4 I|11 mlfpar19     |��������� ������������|0.6|
C |R8_ime        |4 8 O|16 value        |��� �������� ��������|0.5|
C |R_i           |4 4 S|_simpJ1231*     |[���]���������� ��������� ������������� |0.0|
C |R_o           |4 4 K|_timpJ1231      |[���]������������ �������� �������������|0.5|
C |L_u           |1 1 S|_limpJ1231*     |[TF]���������� ��������� ������������� |F|
C |R_id          |4 4 S|_simpJ1227*     |[���]���������� ��������� ������������� |0.0|
C |R_od          |4 4 K|_timpJ1227      |[���]������������ �������� �������������|0.5|
C |L_ud          |1 1 S|_limpJ1227*     |[TF]���������� ��������� ������������� |F|
C |R_if          |4 4 O|POS_CL          |��������||
C |R_uf          |4 4 O|POS_OP          |��������||
C |R_ak          |4 4 S|vmstop_button_ST*|��������� ������ "������� ���� �� ���������" |0.0|
C |R_ek          |4 4 I|vmstop_button   |������� ������ ������ "������� ���� �� ���������" |0.0|
C |R_ik          |4 4 S|vmclose_button_ST*|��������� ������ "������� ������� �� ���������" |0.0|
C |R_ok          |4 4 I|vmclose_button  |������� ������ ������ "������� ������� �� ���������" |0.0|
C |R_uk          |4 4 S|vmopen_button_ST*|��������� ������ "������� ������� �� ���������" |0.0|
C |R_al          |4 4 I|vmopen_button   |������� ������ ������ "������� ������� �� ���������" |0.0|
C |I_el          |2 4 O|LZM             |������ "�������"||
C |L_ol          |1 1 I|vlv_kvit        |||
C |L_am          |1 1 I|instr_fault     |||
C |L_em          |1 1 S|_qffJ453*       |�������� ������ Q RS-��������  |F|
C |L_im          |1 1 O|vmclose_button_CMD*|[TF]����� ������ ������� ������� �� ���������|F|
C |L_om          |1 1 O|vmopen_button_CMD*|[TF]����� ������ ������� ������� �� ���������|F|
C |L_ap          |1 1 I|ulubclose       |���������� ������� �� ���������|F|
C |L_ep          |1 1 I|ulubopen        |���������� ������� �� ���������|F|
C |L_ar          |1 1 O|block           |||
C |L_er          |1 1 O|vmstop_button_CMD*|[TF]����� ������ ������� ���� �� ���������|F|
C |L_ur          |1 1 O|STOP            |�������||
C |L_as          |1 1 S|_qff10*         |�������� ������ Q RS-��������  |F|
C |L_is          |1 1 O|norm            |�����||
C |L_us          |1 1 O|nopower         |��� ����������||
C |R8_et         |4 8 I|voltage         |[��]���������� �� ������||
C |R_av          |4 4 I|power           |�������� ��������||
C |R8_ov         |4 8 O|sn_power        |�������� ���� ��|3.0|
C |L_uv          |1 1 I|YA22            |������� ������� �� ����������|F|
C |L_ex          |1 1 I|YA21            |������� ������� �� ����������|F|
C |L_obe         |1 1 O|fault           |�������������||
C |I_ube         |2 4 O|LAM             |������ "�������"||
C |L_ule         |1 1 S|_qff6*          |�������� ������ Q RS-��������  |F|
C |R_ume         |4 4 O|mf_value        |��� ��������� �����|0.5|
C |R_epe         |4 4 I|tcl_top         |����� ����|30.0|
C |L_ite         |1 1 O|XH52            |�� ������� (���)|F|
C |L_axe         |1 1 O|XH51            |�� ������� (���)|F|
C |L_oxe         |1 1 I|mlf24           |��� ���������� � ����������||
C |L_uxe         |1 1 I|mlf23           |������� ������� �����||
C |L_abi         |1 1 I|mlf22           |����� ����� ��������||
C |L_ebi         |1 1 I|mlf04           |�������� �� ��������||
C |L_ibi         |1 1 I|mlf03           |�������� �� ��������||
C |L_obi         |1 1 I|mlf19           |���� ������������||

      IMPLICIT NONE
      REAL*4 ext_deltat
      REAL*4 R_i,R_o
      LOGICAL*1 L_u
      REAL*4 R_id,R_od
      LOGICAL*1 L_ud
      REAL*4 R_if,R_uf,R_ak,R_ek,R_ik,R_ok,R_uk,R_al
      INTEGER*4 I_el
      LOGICAL*1 L_ol,L_am,L_em,L_im,L_om,L_ap,L_ep,L_ar,L_er
     &,L_ur,L_as,L_is,L_us
      REAL*8 R8_et
      REAL*4 R_av
      REAL*8 R8_ov
      LOGICAL*1 L_uv,L_ex,L_obe
      INTEGER*4 I_ube
      LOGICAL*1 L_ule
      REAL*8 R8_ime
      REAL*4 R_ume,R_epe
      LOGICAL*1 L_ite,L_axe
      REAL*4 R_ixe
      LOGICAL*1 L_oxe,L_uxe,L_abi,L_ebi,L_ibi,L_obi
      End subroutine REG_MAN
      End interface
