      Interface
      Subroutine BASE_STORE_HANDLER(ext_deltat,R_aro,R8_obo
     &,I_id,I_ud,L_ef,I_uf,I_ak,I_ek,I_al,I_ol,I_em,I_um,I_ip
     &,I_ar,I_or,I_ov,I_ex,I_ix,R_ox,R_ux,R_abe,R_ebe,I_ube
     &,I_ide,C20_ife,L_ake,C20_uke,C8_ule,R_ope,R_upe,L_are
     &,R_ere,R_ire,I_ore,R_ise,R_use,I_ate,I_ote,I_eve,L_uve
     &,L_exe,L_ixe,L_oxe,L_abi,L_ibi,R_ubi,L_edi,L_idi,L_ifi
     &,L_iki,L_ali,L_eli,L_oli,L_ami,R8_imi,R_epi,R8_upi,L_ari
     &,L_iri,L_uri,I_asi,L_abo,R_ado,R_ako,L_ilo,L_ulo,L_apo
     &,L_opo,L_ero,L_iro,L_oro,L_uro,L_aso,L_eso)
C |R_aro         |4 4 I|11 mlfpar19     |��������� ������������|0.6|
C |R8_obo        |4 8 O|16 value        |��� �������� ��������|0.5|
C |I_id          |2 4 K|_lcmpJ3357      |�������� ������ �����������|1|
C |I_ud          |2 4 K|_lcmpJ3354      |�������� ������ �����������|7|
C |L_ef          |1 1 S|_qffJ3425*      |�������� ������ Q RS-��������  |F|
C |I_uf          |2 4 I|POS             |�������� �������||
C |I_ak          |2 4 K|_lcmpJ3108      |�������� ������ �����������|7|
C |I_ek          |2 4 K|_lcmpJ3104      |�������� ������ �����������|1|
C |I_al          |2 4 O|LREADY          |����� ����������||
C |I_ol          |2 4 O|LLEVEL7         |����� ������� 7||
C |I_em          |2 4 O|LLEVEL6         |����� ������� 6||
C |I_um          |2 4 O|LLEVEL5         |����� ������� 5||
C |I_ip          |2 4 O|LLEVEL4         |����� ������� 4||
C |I_ar          |2 4 O|LLEVEL3         |����� ������� 3||
C |I_or          |2 4 O|LLEVEL2         |����� ������� 2||
C |I_ov          |2 4 O|LLEVEL1         |����� ������� 1||
C |I_ex          |2 4 O|count_state     ||1|
C |I_ix          |2 4 O|LBUSY           |����� �����||
C |R_ox          |4 4 S|vmdown_button_ST*|��������� ������ "������� ���� �� ���������" |0.0|
C |R_ux          |4 4 I|vmdown_button   |������� ������ ������ "������� ���� �� ���������" |0.0|
C |R_abe         |4 4 S|vmup_button_ST* |��������� ������ "������� ����� �� ���������" |0.0|
C |R_ebe         |4 4 I|vmup_button     |������� ������ ������ "������� ����� �� ���������" |0.0|
C |I_ube         |2 4 O|LUPO            |����� �����||
C |I_ide         |2 4 O|LDOWNC          |����� ����||
C |C20_ife       |3 20 O|task_state      |���������||
C |L_ake         |1 1 S|_qffJ3391*      |�������� ������ Q RS-��������  |F|
C |C20_uke       |3 20 O|task_name       |������� ���������||
C |C8_ule        |3 8 I|task            |������� ���������||
C |R_ope         |4 4 S|vmstop_button_ST*|��������� ������ "������� ���� �� ���������" |0.0|
C |R_upe         |4 4 I|vmstop_button   |������� ������ ������ "������� ���� �� ���������" |0.0|
C |L_are         |1 1 O|vmstart_button_CMD*|[TF]����� ������ ������� �������� ��������� �� ���������|F|
C |R_ere         |4 4 S|vmstart_button_ST*|��������� ������ "������� �������� ��������� �� ���������" |0.0|
C |R_ire         |4 4 I|vmstart_button  |������� ������ ������ "������� �������� ��������� �� ���������" |0.0|
C |I_ore         |2 4 O|LERROR          |����� �������������||
C |R_ise         |4 4 O|POS_CL          |��������||
C |R_use         |4 4 O|POS_OP          |��������||
C |I_ate         |2 4 O|LDOWN           |����� �����||
C |I_ote         |2 4 O|LUP             |����� ������||
C |I_eve         |2 4 O|LZM             |������ "�������"||
C |L_uve         |1 1 I|vlv_kvit        |||
C |L_exe         |1 1 I|instr_fault     |||
C |L_ixe         |1 1 S|_qffJ1618*      |�������� ������ Q RS-��������  |F|
C |L_oxe         |1 1 I|FHDOS_M31_8     |���������� ����. ������. �� OS||
C |L_abi         |1 1 O|vmdown_button_CMD*|[TF]����� ������ ������� ���� �� ���������|F|
C |L_ibi         |1 1 O|vmup_button_CMD*|[TF]����� ������ ������� ����� �� ���������|F|
C |R_ubi         |4 4 I|tdown           |����� ���� ����|30.0|
C |L_edi         |1 1 I|ulubclose       |���������� ������� �� ���������|F|
C |L_idi         |1 1 I|ulubopen        |���������� ������� �� ���������|F|
C |L_ifi         |1 1 O|block           |||
C |L_iki         |1 1 O|vmstop_button_CMD*|[TF]����� ������ ������� ���� �� ���������|F|
C |L_ali         |1 1 O|STOP            |�������||
C |L_eli         |1 1 S|_qff10*         |�������� ������ Q RS-��������  |F|
C |L_oli         |1 1 O|norm            |�����||
C |L_ami         |1 1 O|nopower         |��� ����������||
C |R8_imi        |4 8 I|voltage         |[��]���������� �� ������||
C |R_epi         |4 4 I|power           |�������� ��������||
C |R8_upi        |4 8 O|sn_power        |�������� ���� ��|3.0|
C |L_ari         |1 1 I|YA25            |������� ������� �� ����������|F|
C |L_iri         |1 1 I|YA26            |������� ������� �� ����������|F|
C |L_uri         |1 1 O|fault           |�������������||
C |I_asi         |2 4 O|LAM             |������ "�������"||
C |L_abo         |1 1 S|_qff6*          |�������� ������ Q RS-��������  |F|
C |R_ado         |4 4 O|mf_value        |��� ��������� �����|0.5|
C |R_ako         |4 4 I|tup             |����� ���� �����|30.0|
C |L_ilo         |1 1 S|_qff2*          |�������� ������ Q RS-��������  |F|
C |L_ulo         |1 1 O|XH53            |�� ������� (���)|F|
C |L_apo         |1 1 S|_qff1*          |�������� ������ Q RS-��������  |F|
C |L_opo         |1 1 O|XH54            |�� ������� (���)|F|
C |L_ero         |1 1 I|mlf24           |��� ���������� � ����������||
C |L_iro         |1 1 I|mlf23           |������� ������� �����||
C |L_oro         |1 1 I|mlf22           |����� ����� ��������||
C |L_uro         |1 1 I|mlf04           |�������� �� ��������||
C |L_aso         |1 1 I|mlf03           |�������� �� ��������||
C |L_eso         |1 1 I|mlf19           |���� ������������||

      IMPLICIT NONE
      REAL*4 ext_deltat
      INTEGER*4 I_id,I_ud
      LOGICAL*1 L_ef
      INTEGER*4 I_uf,I_ak,I_ek,I_al,I_ol,I_em,I_um,I_ip,I_ar
     &,I_or,I_ov,I_ex,I_ix
      REAL*4 R_ox,R_ux,R_abe,R_ebe
      INTEGER*4 I_ube,I_ide
      CHARACTER*20 C20_ife
      LOGICAL*1 L_ake
      CHARACTER*20 C20_uke
      CHARACTER*8 C8_ule
      REAL*4 R_ope,R_upe
      LOGICAL*1 L_are
      REAL*4 R_ere,R_ire
      INTEGER*4 I_ore
      REAL*4 R_ise,R_use
      INTEGER*4 I_ate,I_ote,I_eve
      LOGICAL*1 L_uve,L_exe,L_ixe,L_oxe,L_abi,L_ibi
      REAL*4 R_ubi
      LOGICAL*1 L_edi,L_idi,L_ifi,L_iki,L_ali,L_eli,L_oli
     &,L_ami
      REAL*8 R8_imi
      REAL*4 R_epi
      REAL*8 R8_upi
      LOGICAL*1 L_ari,L_iri,L_uri
      INTEGER*4 I_asi
      LOGICAL*1 L_abo
      REAL*8 R8_obo
      REAL*4 R_ado,R_ako
      LOGICAL*1 L_ilo,L_ulo,L_apo,L_opo
      REAL*4 R_aro
      LOGICAL*1 L_ero,L_iro,L_oro,L_uro,L_aso,L_eso
      End subroutine BASE_STORE_HANDLER
      End interface
