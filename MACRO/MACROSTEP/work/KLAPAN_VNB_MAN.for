      Subroutine KLAPAN_VNB_MAN(ext_deltat,R_oki,R8_ete,I_e
     &,I_u,I_id,C8_ik,I_ok,R_il,R_ul,R_am,R_em,R_im,R_om,R_um
     &,R_ap,I_ep,I_up,I_ir,I_as,L_is,L_us,L_at,L_et,L_it,L_ut
     &,L_av,L_uv,L_ax,L_ox,L_ux,L_ebe,L_obe,R8_ade,R_ude,R8_ife
     &,L_ofe,L_ake,L_ile,I_ole,L_ose,R_ote,R_oxe,L_adi,L_idi
     &,L_ofi,L_eki,L_uki,L_ali,L_eli,L_ili,L_oli,L_uli)
C |R_oki         |4 4 I|11 mlfpar19     |��������� ������������|0.6|
C |R8_ete        |4 8 O|16 value        |��� �������� ��������|0.5|
C |I_e           |2 4 O|LOPO            |����� �����������||
C |I_u           |2 4 O|LCLC            |����� �����������||
C |I_id          |2 4 O|LREADY          |����� ����������||
C |C8_ik         |3 8 O|TEXT            |�������� ���������||
C |I_ok          |2 4 O|LF              |����� �������������||
C |R_il          |4 4 O|POS_CL          |��������||
C |R_ul          |4 4 O|POS_OP          |��������||
C |R_am          |4 4 S|vmstop_button_ST*|��������� ������ "������� ���� �� ���������" |0.0|
C |R_em          |4 4 I|vmstop_button   |������� ������ ������ "������� ���� �� ���������" |0.0|
C |R_im          |4 4 S|vmclose_button_ST*|��������� ������ "������� ������� �� ���������" |0.0|
C |R_om          |4 4 I|vmclose_button  |������� ������ ������ "������� ������� �� ���������" |0.0|
C |R_um          |4 4 S|vmopen_button_ST*|��������� ������ "������� ������� �� ���������" |0.0|
C |R_ap          |4 4 I|vmopen_button   |������� ������ ������ "������� ������� �� ���������" |0.0|
C |I_ep          |2 4 O|LST             |����� ����||
C |I_up          |2 4 O|LCL             |����� �������||
C |I_ir          |2 4 O|LOP             |����� �������||
C |I_as          |2 4 O|LZM             |������ "�������"||
C |L_is          |1 1 I|vlv_kvit        |||
C |L_us          |1 1 I|instr_fault     |||
C |L_at          |1 1 S|_qffJ453*       |�������� ������ Q RS-��������  |F|
C |L_et          |1 1 O|vmclose_button_CMD*|[TF]����� ������ ������� ������� �� ���������|F|
C |L_it          |1 1 O|vmopen_button_CMD*|[TF]����� ������ ������� ������� �� ���������|F|
C |L_ut          |1 1 I|ulubclose       |���������� ������� �� ���������|F|
C |L_av          |1 1 I|ulubopen        |���������� ������� �� ���������|F|
C |L_uv          |1 1 O|block           |||
C |L_ax          |1 1 O|vmstop_button_CMD*|[TF]����� ������ ������� ���� �� ���������|F|
C |L_ox          |1 1 O|STOP            |�������||
C |L_ux          |1 1 S|_qff10*         |�������� ������ Q RS-��������  |F|
C |L_ebe         |1 1 O|norm            |�����||
C |L_obe         |1 1 O|nopower         |��� ����������||
C |R8_ade        |4 8 I|voltage         |[��]���������� �� ������||
C |R_ude         |4 4 I|power           |�������� ��������||
C |R8_ife        |4 8 O|sn_power        |�������� ���� ��|3.0|
C |L_ofe         |1 1 I|YA22            |������� ������� �� ����������|F|
C |L_ake         |1 1 I|YA21            |������� ������� �� ����������|F|
C |L_ile         |1 1 O|fault           |�������������||
C |I_ole         |2 4 O|LAM             |������ "�������"||
C |L_ose         |1 1 S|_qff6*          |�������� ������ Q RS-��������  |F|
C |R_ote         |4 4 O|mf_value        |��� ��������� �����|0.5|
C |R_oxe         |4 4 I|tcl_top         |����� ����|30.0|
C |L_adi         |1 1 S|_qff2*          |�������� ������ Q RS-��������  |F|
C |L_idi         |1 1 O|XH52            |�� ������� (���)|F|
C |L_ofi         |1 1 S|_qff1*          |�������� ������ Q RS-��������  |F|
C |L_eki         |1 1 O|XH51            |�� ������� (���)|F|
C |L_uki         |1 1 I|mlf24           |��� ���������� � ����������||
C |L_ali         |1 1 I|mlf23           |������� ������� �����||
C |L_eli         |1 1 I|mlf22           |����� ����� ��������||
C |L_ili         |1 1 I|mlf04           |�������� �� ��������||
C |L_oli         |1 1 I|mlf03           |�������� �� ��������||
C |L_uli         |1 1 I|mlf19           |���� ������������||

      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      INTEGER*4 I_e,I0_i,I0_o,I_u,I0_ad,I0_ed,I_id,I0_od,I0_ud
      LOGICAL*1 L0_af
      CHARACTER*8 C8_ef,C8_if,C8_of,C8_uf,C8_ak,C8_ek,C8_ik
      INTEGER*4 I_ok,I0_uk,I0_al
      REAL*4 R0_el,R_il,R0_ol,R_ul,R_am,R_em,R_im,R_om,R_um
     &,R_ap
      INTEGER*4 I_ep,I0_ip,I0_op,I_up,I0_ar,I0_er,I_ir,I0_or
     &,I0_ur,I_as,I0_es
      LOGICAL*1 L_is,L0_os,L_us,L_at,L_et,L_it
      REAL*4 R0_ot
      LOGICAL*1 L_ut,L_av,L0_ev,L0_iv,L0_ov,L_uv,L_ax,L0_ex
     &,L0_ix,L_ox,L_ux,L0_abe,L_ebe,L0_ibe,L_obe
      REAL*4 R0_ube
      REAL*8 R8_ade
      LOGICAL*1 L0_ede,L0_ide
      REAL*4 R0_ode,R_ude,R0_afe,R0_efe
      REAL*8 R8_ife
      LOGICAL*1 L_ofe,L0_ufe,L_ake,L0_eke
      INTEGER*4 I0_ike,I0_oke,I0_uke,I0_ale,I0_ele
      LOGICAL*1 L_ile
      INTEGER*4 I_ole,I0_ule,I0_ame
      LOGICAL*1 L0_eme,L0_ime
      REAL*4 R0_ome,R0_ume
      LOGICAL*1 L0_ape
      REAL*4 R0_epe,R0_ipe,R0_ope,R0_upe,R0_are,R0_ere
      LOGICAL*1 L0_ire
      REAL*4 R0_ore,R0_ure,R0_ase,R0_ese
      LOGICAL*1 L0_ise,L_ose
      REAL*4 R0_use,R0_ate
      REAL*8 R8_ete
      REAL*4 R0_ite,R_ote,R0_ute,R0_ave,R0_eve,R0_ive,R0_ove
     &,R0_uve,R0_axe,R0_exe,R0_ixe,R_oxe
      LOGICAL*1 L0_uxe,L0_abi,L0_ebi,L0_ibi,L0_obi,L0_ubi
     &,L_adi,L0_edi,L_idi,L0_odi,L0_udi,L0_afi,L0_efi,L0_ifi
     &,L_ofi,L0_ufi,L0_aki,L_eki,L0_iki
      REAL*4 R_oki
      LOGICAL*1 L_uki,L_ali,L_eli,L_ili,L_oli,L_uli

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      I0_o = z'01000003'
C KLAPAN_VNB_MAN.fmg( 170, 230):��������� ������������� IN (�������)
      I0_i = z'0100000A'
C KLAPAN_VNB_MAN.fmg( 170, 228):��������� ������������� IN (�������)
      I0_ed = z'01000003'
C KLAPAN_VNB_MAN.fmg( 192, 181):��������� ������������� IN (�������)
      I0_ad = z'0100000A'
C KLAPAN_VNB_MAN.fmg( 192, 179):��������� ������������� IN (�������)
      I0_er = z'01000003'
C KLAPAN_VNB_MAN.fmg( 114, 171):��������� ������������� IN (�������)
      I0_ar = z'0100000A'
C KLAPAN_VNB_MAN.fmg( 114, 169):��������� ������������� IN (�������)
      I0_ud = z'01000003'
C KLAPAN_VNB_MAN.fmg( 328, 204):��������� ������������� IN (�������)
      I0_od = z'0100000A'
C KLAPAN_VNB_MAN.fmg( 328, 202):��������� ������������� IN (�������)
      C8_ef = '������'
C KLAPAN_VNB_MAN.fmg( 234, 280):��������� ���������� CH8 (�������)
      C8_of = '������'
C KLAPAN_VNB_MAN.fmg( 210, 281):��������� ���������� CH8 (�������)
      C8_ak = '����'
C KLAPAN_VNB_MAN.fmg( 194, 282):��������� ���������� CH8 (�������)
      C8_ek = '����'
C KLAPAN_VNB_MAN.fmg( 194, 284):��������� ���������� CH8 (�������)
      I0_ike = z'0100000A'
C KLAPAN_VNB_MAN.fmg( 152, 207):��������� ������������� IN (�������)
      I0_oke = z'01000003'
C KLAPAN_VNB_MAN.fmg( 152, 209):��������� ������������� IN (�������)
      I0_es = z'0100000E'
C KLAPAN_VNB_MAN.fmg( 188, 201):��������� ������������� IN (�������)
      I0_uk = z'01000007'
C KLAPAN_VNB_MAN.fmg( 148, 180):��������� ������������� IN (�������)
      I0_al = z'01000003'
C KLAPAN_VNB_MAN.fmg( 148, 182):��������� ������������� IN (�������)
      R0_el = 100
C KLAPAN_VNB_MAN.fmg( 378, 273):��������� (RE4) (�������)
      R0_ol = 100
C KLAPAN_VNB_MAN.fmg( 365, 267):��������� (RE4) (�������)
      L_ax=R_em.ne.R_am
      R_am=R_em
C KLAPAN_VNB_MAN.fmg(  22, 208):���������� ������������� ������
      L_et=R_om.ne.R_im
      R_im=R_om
C KLAPAN_VNB_MAN.fmg(  21, 194):���������� ������������� ������
      L0_ufe = (.NOT.L_ut).AND.L_et
C KLAPAN_VNB_MAN.fmg(  65, 194):�
      L0_odi = L0_ufe.OR.L_ofe
C KLAPAN_VNB_MAN.fmg(  69, 192):���
      L_it=R_ap.ne.R_um
      R_um=R_ap
C KLAPAN_VNB_MAN.fmg(  22, 238):���������� ������������� ������
      L0_eke = L_it.AND.(.NOT.L_av)
C KLAPAN_VNB_MAN.fmg(  65, 236):�
      L0_aki = L0_eke.OR.L_ake
C KLAPAN_VNB_MAN.fmg(  69, 234):���
      L0_ex = L0_aki.OR.L0_odi
C KLAPAN_VNB_MAN.fmg(  74, 203):���
      L_ux=(L_ax.or.L_ux).and..not.(L0_ex)
      L0_ix=.not.L_ux
C KLAPAN_VNB_MAN.fmg( 104, 205):RS �������,10
      L_ox=L_ux
C KLAPAN_VNB_MAN.fmg( 132, 207):������,STOP
      L0_ebi = L_ux.OR.L_uki
C KLAPAN_VNB_MAN.fmg(  96, 214):���
      I0_ip = z'01000007'
C KLAPAN_VNB_MAN.fmg( 112, 214):��������� ������������� IN (�������)
      I0_op = z'01000003'
C KLAPAN_VNB_MAN.fmg( 112, 216):��������� ������������� IN (�������)
      if(L_ux) then
         I_ep=I0_ip
      else
         I_ep=I0_op
      endif
C KLAPAN_VNB_MAN.fmg( 116, 214):���� RE IN LO CH7
      I0_ur = z'01000003'
C KLAPAN_VNB_MAN.fmg( 120, 265):��������� ������������� IN (�������)
      I0_or = z'0100000A'
C KLAPAN_VNB_MAN.fmg( 120, 263):��������� ������������� IN (�������)
      I0_ele = z'0100000A'
C KLAPAN_VNB_MAN.fmg( 152, 264):��������� ������������� IN (�������)
      I0_ale = z'01000003'
C KLAPAN_VNB_MAN.fmg( 152, 262):��������� ������������� IN (�������)
      I0_ule = z'0100000E'
C KLAPAN_VNB_MAN.fmg( 166, 256):��������� ������������� IN (�������)
      L_at=(L_us.or.L_at).and..not.(L_is)
      L0_os=.not.L_at
C KLAPAN_VNB_MAN.fmg( 326, 178):RS �������
      L0_ov=.false.
C KLAPAN_VNB_MAN.fmg(  63, 217):��������� ���������� (�������)
      L0_iv=.false.
C KLAPAN_VNB_MAN.fmg(  63, 215):��������� ���������� (�������)
      L0_ev=.false.
C KLAPAN_VNB_MAN.fmg(  63, 213):��������� ���������� (�������)
      L_uv = L0_ov.OR.L0_iv.OR.L0_ev.OR.L_av.OR.L_ut
C KLAPAN_VNB_MAN.fmg(  67, 213):���
      R0_ot = DeltaT
C KLAPAN_VNB_MAN.fmg( 250, 254):��������� (RE4) (�������)
      if(R_oxe.ge.0.0) then
         R0_ive=R0_ot/max(R_oxe,1.0e-10)
      else
         R0_ive=R0_ot/min(R_oxe,-1.0e-10)
      endif
C KLAPAN_VNB_MAN.fmg( 259, 252):�������� ����������
      L0_ibe =.NOT.(L_oli.OR.L_ili.OR.L_uli.OR.L_eli.OR.L_ali.OR.L_uki
     &)
C KLAPAN_VNB_MAN.fmg( 319, 191):���
      L0_abe =.NOT.(L0_ibe)
C KLAPAN_VNB_MAN.fmg( 328, 185):���
      L_ile = L0_abe.OR.L_at
C KLAPAN_VNB_MAN.fmg( 332, 184):���
      if(L_ile) then
         I_ok=I0_uk
      else
         I_ok=I0_al
      endif
C KLAPAN_VNB_MAN.fmg( 152, 181):���� RE IN LO CH7
      L_ebe = L0_ibe.OR.L_us
C KLAPAN_VNB_MAN.fmg( 328, 190):���
      if(L_ebe) then
         I_id=I0_od
      else
         I_id=I0_ud
      endif
C KLAPAN_VNB_MAN.fmg( 331, 202):���� RE IN LO CH7
      R0_ube = 0.1
C KLAPAN_VNB_MAN.fmg( 254, 160):��������� (RE4) (�������)
      L_obe=R8_ade.lt.R0_ube
C KLAPAN_VNB_MAN.fmg( 259, 162):���������� <
      R0_ode = 0.0
C KLAPAN_VNB_MAN.fmg( 266, 182):��������� (RE4) (�������)
      R0_are = 0.000001
C KLAPAN_VNB_MAN.fmg( 354, 208):��������� (RE4) (�������)
      R0_ere = 0.999999
C KLAPAN_VNB_MAN.fmg( 354, 224):��������� (RE4) (�������)
      R0_ome = 0.0
C KLAPAN_VNB_MAN.fmg( 296, 244):��������� (RE4) (�������)
      R0_ume = 0.0
C KLAPAN_VNB_MAN.fmg( 370, 242):��������� (RE4) (�������)
      L0_ape = L_ali.OR.L_eli
C KLAPAN_VNB_MAN.fmg( 363, 237):���
      R0_ipe = 1.0
C KLAPAN_VNB_MAN.fmg( 348, 252):��������� (RE4) (�������)
      R0_ope = 0.0
C KLAPAN_VNB_MAN.fmg( 340, 252):��������� (RE4) (�������)
      R0_ure = 1.0e-10
C KLAPAN_VNB_MAN.fmg( 318, 228):��������� (RE4) (�������)
      R0_use = 0.0
C KLAPAN_VNB_MAN.fmg( 328, 242):��������� (RE4) (�������)
      R0_ave = DeltaT
C KLAPAN_VNB_MAN.fmg( 250, 264):��������� (RE4) (�������)
      if(R_oxe.ge.0.0) then
         R0_ove=R0_ave/max(R_oxe,1.0e-10)
      else
         R0_ove=R0_ave/min(R_oxe,-1.0e-10)
      endif
C KLAPAN_VNB_MAN.fmg( 259, 262):�������� ����������
      R0_ixe = 0.0
C KLAPAN_VNB_MAN.fmg( 282, 244):��������� (RE4) (�������)
      L0_ibi = (.NOT.L_ox).AND.L_idi
C KLAPAN_VNB_MAN.fmg( 102, 166):�
C label 137  try137=try137-1
      if(L_eli) then
         R0_epe=R0_ume
      else
         R0_epe=R8_ete
      endif
C KLAPAN_VNB_MAN.fmg( 373, 242):���� RE IN LO CH7
      if(.NOT.L0_ape) then
         R_ote=R8_ete
      endif
C KLAPAN_VNB_MAN.fmg( 384, 252):���� � ������������� �������
      L0_afi = (.NOT.L_ox).AND.L_eki
C KLAPAN_VNB_MAN.fmg( 104, 260):�
      L0_ifi = L_eki.OR.L0_ebi.OR.L0_odi
C KLAPAN_VNB_MAN.fmg( 102, 229):���
      L0_iki = (.NOT.L_eki).AND.L0_aki
C KLAPAN_VNB_MAN.fmg( 102, 235):�
      L_ofi=(L0_iki.or.L_ofi).and..not.(L0_ifi)
      L0_ufi=.not.L_ofi
C KLAPAN_VNB_MAN.fmg( 110, 233):RS �������,1
      L0_efi = (.NOT.L0_afi).AND.L_ofi
C KLAPAN_VNB_MAN.fmg( 128, 236):�
      L0_ime = L0_efi.AND.(.NOT.L_ili)
C KLAPAN_VNB_MAN.fmg( 252, 242):�
      L0_abi = L0_ime.OR.L_oli
C KLAPAN_VNB_MAN.fmg( 265, 241):���
      if(L0_abi) then
         R0_axe=R0_ove
      else
         R0_axe=R0_ixe
      endif
C KLAPAN_VNB_MAN.fmg( 287, 254):���� RE IN LO CH7
      L0_ubi = L0_ebi.OR.L_idi.OR.L0_aki
C KLAPAN_VNB_MAN.fmg( 102, 185):���
      L0_udi = L0_odi.AND.(.NOT.L_idi)
C KLAPAN_VNB_MAN.fmg( 102, 191):�
      L_adi=(L0_udi.or.L_adi).and..not.(L0_ubi)
      L0_edi=.not.L_adi
C KLAPAN_VNB_MAN.fmg( 110, 189):RS �������,2
      L0_obi = L_adi.AND.(.NOT.L0_ibi)
C KLAPAN_VNB_MAN.fmg( 128, 190):�
      L0_eme = L0_obi.AND.(.NOT.L_oli)
C KLAPAN_VNB_MAN.fmg( 252, 228):�
      L0_uxe = L0_eme.OR.L_ili
C KLAPAN_VNB_MAN.fmg( 265, 227):���
      if(L0_uxe) then
         R0_uve=R0_ive
      else
         R0_uve=R0_ixe
      endif
C KLAPAN_VNB_MAN.fmg( 287, 236):���� RE IN LO CH7
      R0_exe = R0_axe + (-R0_uve)
C KLAPAN_VNB_MAN.fmg( 293, 246):��������
      if(L_uki) then
         R0_ate=R0_ome
      else
         R0_ate=R0_exe
      endif
C KLAPAN_VNB_MAN.fmg( 300, 244):���� RE IN LO CH7
      R0_ese = R_ote + (-R_oki)
C KLAPAN_VNB_MAN.fmg( 308, 235):��������
      R0_ore = R0_ate + R0_ese
C KLAPAN_VNB_MAN.fmg( 314, 236):��������
      R0_ase = R0_ore * R0_ese
C KLAPAN_VNB_MAN.fmg( 318, 235):����������
      L0_ise=R0_ase.lt.R0_ure
C KLAPAN_VNB_MAN.fmg( 323, 234):���������� <
      L_ose=(L0_ise.or.L_ose).and..not.(.NOT.L_uli)
      L0_ire=.not.L_ose
C KLAPAN_VNB_MAN.fmg( 330, 232):RS �������,6
      if(L_ose) then
         R_ote=R_oki
      endif
C KLAPAN_VNB_MAN.fmg( 324, 252):���� � ������������� �������
      if(L_ose) then
         R0_ite=R0_use
      else
         R0_ite=R0_ate
      endif
C KLAPAN_VNB_MAN.fmg( 332, 244):���� RE IN LO CH7
      R0_ute = R_ote + R0_ite
C KLAPAN_VNB_MAN.fmg( 338, 245):��������
      R0_upe=MAX(R0_ope,R0_ute)
C KLAPAN_VNB_MAN.fmg( 346, 246):��������
      R0_eve=MIN(R0_ipe,R0_upe)
C KLAPAN_VNB_MAN.fmg( 354, 247):�������
      L_idi=R0_eve.lt.R0_are
C KLAPAN_VNB_MAN.fmg( 363, 210):���������� <
C sav1=L0_ubi
      L0_ubi = L0_ebi.OR.L_idi.OR.L0_aki
C KLAPAN_VNB_MAN.fmg( 102, 185):recalc:���
C if(sav1.ne.L0_ubi .and. try177.gt.0) goto 177
C sav1=L0_udi
      L0_udi = L0_odi.AND.(.NOT.L_idi)
C KLAPAN_VNB_MAN.fmg( 102, 191):recalc:�
C if(sav1.ne.L0_udi .and. try179.gt.0) goto 179
C sav1=L0_ibi
      L0_ibi = (.NOT.L_ox).AND.L_idi
C KLAPAN_VNB_MAN.fmg( 102, 166):recalc:�
C if(sav1.ne.L0_ibi .and. try137.gt.0) goto 137
      if(L0_ape) then
         R8_ete=R0_epe
      else
         R8_ete=R0_eve
      endif
C KLAPAN_VNB_MAN.fmg( 377, 246):���� RE IN LO CH7
      R_ul = R0_ol * R8_ete
C KLAPAN_VNB_MAN.fmg( 368, 266):����������
      R_il = R0_el + (-R_ul)
C KLAPAN_VNB_MAN.fmg( 382, 272):��������
      L_eki=R0_eve.gt.R0_ere
C KLAPAN_VNB_MAN.fmg( 363, 225):���������� >
C sav1=L0_ifi
      L0_ifi = L_eki.OR.L0_ebi.OR.L0_odi
C KLAPAN_VNB_MAN.fmg( 102, 229):recalc:���
C if(sav1.ne.L0_ifi .and. try157.gt.0) goto 157
C sav1=L0_iki
      L0_iki = (.NOT.L_eki).AND.L0_aki
C KLAPAN_VNB_MAN.fmg( 102, 235):recalc:�
C if(sav1.ne.L0_iki .and. try159.gt.0) goto 159
C sav1=L0_afi
      L0_afi = (.NOT.L_ox).AND.L_eki
C KLAPAN_VNB_MAN.fmg( 104, 260):recalc:�
C if(sav1.ne.L0_afi .and. try152.gt.0) goto 152
      L0_af = (.NOT.L_eki).AND.(.NOT.L_idi)
C KLAPAN_VNB_MAN.fmg( 236, 274):�
      if(L_eki) then
         C8_uf=C8_ak
      else
         C8_uf=C8_ek
      endif
C KLAPAN_VNB_MAN.fmg( 198, 282):���� RE IN LO CH7
      if(L_ile) then
         C8_if=C8_of
      else
         C8_if=C8_uf
      endif
C KLAPAN_VNB_MAN.fmg( 214, 282):���� RE IN LO CH7
      if(L0_af) then
         C8_ik=C8_ef
      else
         C8_ik=C8_if
      endif
C KLAPAN_VNB_MAN.fmg( 238, 280):���� RE IN LO CH7
      if(L0_ape) then
         R_ote=R0_eve
      endif
C KLAPAN_VNB_MAN.fmg( 366, 252):���� � ������������� �������
C sav1=R0_ese
      R0_ese = R_ote + (-R_oki)
C KLAPAN_VNB_MAN.fmg( 308, 235):recalc:��������
C if(sav1.ne.R0_ese .and. try204.gt.0) goto 204
C sav1=R0_ute
      R0_ute = R_ote + R0_ite
C KLAPAN_VNB_MAN.fmg( 338, 245):recalc:��������
C if(sav1.ne.R0_ute .and. try221.gt.0) goto 221
      if(L0_obi) then
         I_u=I0_ad
      else
         I_u=I0_ed
      endif
C KLAPAN_VNB_MAN.fmg( 196, 180):���� RE IN LO CH7
      L0_ede = L0_efi.OR.L0_obi
C KLAPAN_VNB_MAN.fmg( 251, 174):���
      L0_ide = L0_ede.AND.(.NOT.L_obe)
C KLAPAN_VNB_MAN.fmg( 266, 173):�
      if(L0_ide) then
         R0_efe=R_ude
      else
         R0_efe=R0_ode
      endif
C KLAPAN_VNB_MAN.fmg( 269, 180):���� RE IN LO CH7
      if(L0_efi) then
         I_e=I0_i
      else
         I_e=I0_o
      endif
C KLAPAN_VNB_MAN.fmg( 173, 228):���� RE IN LO CH7
      if(L0_afi) then
         I0_uke=I0_ike
      else
         I0_uke=I0_oke
      endif
C KLAPAN_VNB_MAN.fmg( 155, 208):���� RE IN LO CH7
      if(L_ile) then
         I_as=I0_es
      else
         I_as=I0_uke
      endif
C KLAPAN_VNB_MAN.fmg( 191, 206):���� RE IN LO CH7
      if(L0_afi) then
         I_ir=I0_or
      else
         I_ir=I0_ur
      endif
C KLAPAN_VNB_MAN.fmg( 124, 264):���� RE IN LO CH7
      if(L0_ibi) then
         I_up=I0_ar
      else
         I_up=I0_er
      endif
C KLAPAN_VNB_MAN.fmg( 117, 170):���� RE IN LO CH7
      if(L0_ibi) then
         I0_ame=I0_ale
      else
         I0_ame=I0_ele
      endif
C KLAPAN_VNB_MAN.fmg( 155, 262):���� RE IN LO CH7
      if(L_ile) then
         I_ole=I0_ule
      else
         I_ole=I0_ame
      endif
C KLAPAN_VNB_MAN.fmg( 170, 262):���� RE IN LO CH7
      R0_afe = R8_ife
C KLAPAN_VNB_MAN.fmg( 264, 190):��������
C label 307  try307=try307-1
      R8_ife = R0_efe + R0_afe
C KLAPAN_VNB_MAN.fmg( 275, 189):��������
C sav1=R0_afe
      R0_afe = R8_ife
C KLAPAN_VNB_MAN.fmg( 264, 190):recalc:��������
C if(sav1.ne.R0_afe .and. try307.gt.0) goto 307
      End
