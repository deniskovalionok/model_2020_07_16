      Subroutine TELEGKA_HANDLER(ext_deltat,R_ake,R8_ufu,R_u
     &,L_ad,R_id,L_od,L_uf,L_ik,R_er,L_or,R_as,L_is,L_ot,L_ov
     &,L_ax,L_ex,L_ox,L_ebe,L_ube,L_ede,L_oke,L_ale,L_epe
     &,L_ipe,L_are,L_ise,L_ose,L_use,L_ate,L_ete,L_ite,L_ote
     &,L_ute,L_ave,L_eve,L_ive,L_ove,L_uve,R_ebi,R_obi,I_idi
     &,I_afi,R_efi,R_ifi,R_ofi,R_ufi,I_aki,I_oki,I_oli,I_emi
     &,C20_epi,C20_eri,C8_esi,R_uvi,R_axi,L_exi,R_ixi,R_oxi
     &,I_uxi,R_obo,R_ado,I_edo,I_udo,I_ifo,L_ako,L_iko,L_oko
     &,L_elo,L_olo,L_ulo,L_emo,L_omo,L_umo,R_ipo,L_iro,L_oro
     &,L_eso,L_iso,L_ato,L_eto,R8_oto,R_ivo,R8_axo,L_exo,L_ixo
     &,L_ibu,I_obu,L_edu,L_idu,R_ifu,L_umu,R_oru,R_usu,L_evu
     &,L_exu,L_abad,R_adad,L_edad,L_idad,L_odad,L_udad,L_afad
     &,L_efad)
C |R_ake         |4 4 I|11 mlfpar19     ||0.6|
C |R8_ufu        |4 8 O|16 value        |��� �������� ��������|0.5|
C |R_u           |4 4 S|_sdelnv2dyn$100Dasha*|[���]���������� ��������� |0|
C |L_ad          |1 1 S|_idelnv2dyn$100Dasha*|[TF]���������� ��������� �������� ������ |f|
C |R_id          |4 4 S|_sdelnv2dyn$99Dasha*|[���]���������� ��������� |0|
C |L_od          |1 1 S|_idelnv2dyn$99Dasha*|[TF]���������� ��������� �������� ������ |f|
C |L_uf          |1 1 S|_qff156*        |�������� ������ Q RS-��������  |F|
C |L_ik          |1 1 S|_qff155*        |�������� ������ Q RS-��������  |F|
C |R_er          |4 4 S|_sdelnv2dyn$83Dasha*|[���]���������� ��������� |0|
C |L_or          |1 1 S|_idelnv2dyn$83Dasha*|[TF]���������� ��������� �������� ������ |f|
C |R_as          |4 4 S|_sdelnv2dyn$82Dasha*|[���]���������� ��������� |0|
C |L_is          |1 1 S|_idelnv2dyn$82Dasha*|[TF]���������� ��������� �������� ������ |f|
C |L_ot          |1 1 S|_qffnv2dyn$106Dasha*|�������� ������ Q RS-��������  |F|
C |L_ov          |1 1 S|_qffnv2dyn$104Dasha*|�������� ������ Q RS-��������  |F|
C |L_ax          |1 1 S|_qffnv2dyn$105Dasha*|�������� ������ Q RS-��������  |F|
C |L_ex          |1 1 O|OUTC            |�����||
C |L_ox          |1 1 S|_qffnv2dyn$103Dasha*|�������� ������ Q RS-��������  |F|
C |L_ebe         |1 1 S|_qffnv2dyn$102Dasha*|�������� ������ Q RS-��������  |F|
C |L_ube         |1 1 S|_qffnv2dyn$101Dasha*|�������� ������ Q RS-��������  |F|
C |L_ede         |1 1 O|OUTO            |������||
C |L_oke         |1 1 S|_qffnv2dyn$87Dasha*|�������� ������ Q RS-��������  |F|
C |L_ale         |1 1 S|_qffnv2dyn$86Dasha*|�������� ������ Q RS-��������  |F|
C |L_epe         |1 1 S|_qffnv2dyn$96Dasha*|�������� ������ Q RS-��������  |F|
C |L_ipe         |1 1 S|_qffnv2dyn$95Dasha*|�������� ������ Q RS-��������  |F|
C |L_are         |1 1 O|flag_mlf19      |||
C |L_ise         |1 1 I|mlf17           |������ ���������������� ����� �����||
C |L_ose         |1 1 I|mlf16           |������ ������������ ����� �����||
C |L_use         |1 1 I|mlf15           |������ ���������������� ����� ������||
C |L_ate         |1 1 I|mlf14           |������ ������������ ����� ������||
C |L_ete         |1 1 I|mlf07           |���������� ���� �������||
C |L_ite         |1 1 I|mlf28           |������ ���������������� ��������� �����||
C |L_ote         |1 1 I|mlf09           |����� ��������� �����||
C |L_ute         |1 1 I|mlf08           |����� ��������� ������||
C |L_ave         |1 1 I|mlf26           |������ ���������������� ��������� ������||
C |L_eve         |1 1 I|mlf27           |������ ������������ ��������� �����||
C |L_ive         |1 1 I|mlf25           |������ ������������ ��������� ������||
C |L_ove         |1 1 I|YA26�           |������� ������||
C |L_uve         |1 1 I|YA25�           |�������� �����||
C |R_ebi         |4 4 O|VX02            |�������� ����������� �������||
C |R_obi         |4 4 O|VX01            |��������� �������||
C |I_idi         |2 4 O|LREADY          |����� ����������||
C |I_afi         |2 4 O|LBUSY           |����� �����||
C |R_efi         |4 4 S|vmpos1_button_ST*|��������� ������ "������� � ��������� 1 �� ���������" |0.0|
C |R_ifi         |4 4 I|vmpos1_button   |������� ������ ������ "������� � ��������� 1 �� ���������" |0.0|
C |R_ofi         |4 4 S|vmpos2_button_ST*|��������� ������ "������� � ��������� 2 �� ���������" |0.0|
C |R_ufi         |4 4 I|vmpos2_button   |������� ������ ������ "������� � ��������� 2 �� ���������" |0.0|
C |I_aki         |2 4 O|state1          |��������� 1||
C |I_oki         |2 4 O|state2          |��������� 2||
C |I_oli         |2 4 O|LPOS2O          |����� � ��������� 2||
C |I_emi         |2 4 O|LPOS1C          |����� � ��������� 1||
C |C20_epi       |3 20 O|task_state      |���������||
C |C20_eri       |3 20 O|task_name       |������� ���������||
C |C8_esi        |3 8 I|task            |������� ���������||
C |R_uvi         |4 4 S|vmstop_button_ST*|��������� ������ "������� ���� �� ���������" |0.0|
C |R_axi         |4 4 I|vmstop_button   |������� ������ ������ "������� ���� �� ���������" |0.0|
C |L_exi         |1 1 O|vmstart_button_CMD*|[TF]����� ������ ������� �������� ��������� �� ���������|F|
C |R_ixi         |4 4 S|vmstart_button_ST*|��������� ������ "������� �������� ��������� �� ���������" |0.0|
C |R_oxi         |4 4 I|vmstart_button  |������� ������ ������ "������� �������� ��������� �� ���������" |0.0|
C |I_uxi         |2 4 O|LERROR          |����� �������������||
C |R_obo         |4 4 O|POS_CL          |��������||
C |R_ado         |4 4 O|POS_OP          |��������||
C |I_edo         |2 4 O|LPOS1           |����� ��������� 1||
C |I_udo         |2 4 O|LPOS2           |����� ��������� 2||
C |I_ifo         |2 4 O|LZM             |������ "�������"||
C |L_ako         |1 1 I|vlv_kvit        |||
C |L_iko         |1 1 I|instr_fault     |||
C |L_oko         |1 1 S|_qffJ1618*      |�������� ������ Q RS-��������  |F|
C |L_elo         |1 1 O|vmpos1_button_CMD*|[TF]����� ������ ������� � ��������� 1 �� ���������|F|
C |L_olo         |1 1 O|vmpos2_button_CMD*|[TF]����� ������ ������� � ��������� 2 �� ���������|F|
C |L_ulo         |1 1 I|mlf06           |������������� ������� "�����"||
C |L_emo         |1 1 I|mlf05           |������������� ������� "������"||
C |L_omo         |1 1 I|ulubopen        |���������� ������� �� ���������|F|
C |L_umo         |1 1 I|ulubclose       |���������� ������� �� ���������|F|
C |R_ipo         |4 4 I|tpos1           |����� ���� � ��������� 1|30.0|
C |L_iro         |1 1 O|block           |||
C |L_oro         |1 1 O|vmstop_button_CMD*|[TF]����� ������ ������� ���� �� ���������|F|
C |L_eso         |1 1 O|STOP            |�������||
C |L_iso         |1 1 S|_qff10*         |�������� ������ Q RS-��������  |F|
C |L_ato         |1 1 O|norm            |�����||
C |L_eto         |1 1 O|nopower         |��� ����������||
C |R8_oto        |4 8 I|voltage         |[��]���������� �� ������||
C |R_ivo         |4 4 I|power           |�������� ��������||
C |R8_axo        |4 8 O|sn_power        |�������� ���� ��|3.0|
C |L_exo         |1 1 I|YA25            |������� ������� �� ����������|F|
C |L_ixo         |1 1 I|YA26            |������� ������� �� ����������|F|
C |L_ibu         |1 1 O|fault           |�������������||
C |I_obu         |2 4 O|LAM             |������ "�������"||
C |L_edu         |1 1 O|XH53            |�� ����� (���)|F|
C |L_idu         |1 1 O|XH54            |�� ������ (���)|F|
C |R_ifu         |4 4 O|mf_value        |��� ��������� �����|0.5|
C |L_umu         |1 1 S|_qff6*          |�������� ������ Q RS-��������  |F|
C |R_oru         |4 4 O|value_pos       |��� �������� ��������|0.5|
C |R_usu         |4 4 I|tpos2           |����� ���� � ��������� 2|30.0|
C |L_evu         |1 1 S|_qff2*          |�������� ������ Q RS-��������  |F|
C |L_exu         |1 1 O|limit_switch_error|||
C |L_abad        |1 1 S|_qff1*          |�������� ������ Q RS-��������  |F|
C |R_adad        |4 4 I|mlfpar19        |��������� ������������|0.6|
C |L_edad        |1 1 I|mlf24           |��� ���������� � ����������||
C |L_idad        |1 1 I|mlf23           |������� ������� �����||
C |L_odad        |1 1 I|mlf22           |����� ����� ��������||
C |L_udad        |1 1 I|mlf04           |���������������� �������� �����||
C |L_afad        |1 1 I|mlf03           |���������������� �������� ������||
C |L_efad        |1 1 I|mlf19           |���� ������������||

      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      LOGICAL*1 L0_e,L0_i
      REAL*4 R0_o,R_u
      LOGICAL*1 L_ad
      REAL*4 R0_ed,R_id
      LOGICAL*1 L_od,L0_ud,L0_af,L0_ef,L0_if,L0_of,L_uf,L0_ak
     &,L0_ek,L_ik,L0_ok,L0_uk,L0_al
      REAL*4 R0_el,R0_il
      LOGICAL*1 L0_ol,L0_ul
      REAL*4 R0_am
      LOGICAL*1 L0_em,L0_im
      REAL*4 R0_om
      LOGICAL*1 L0_um,L0_ap
      REAL*4 R0_ep,R0_ip
      LOGICAL*1 L0_op,L0_up
      REAL*4 R0_ar,R_er
      LOGICAL*1 L0_ir,L_or
      REAL*4 R0_ur,R_as
      LOGICAL*1 L0_es,L_is,L0_os
      INTEGER*4 I0_us,I0_at
      LOGICAL*1 L0_et,L0_it,L_ot,L0_ut,L0_av,L0_ev,L0_iv,L_ov
     &,L0_uv,L_ax,L_ex,L0_ix,L_ox,L0_ux,L0_abe,L_ebe,L0_ibe
     &,L0_obe,L_ube,L0_ade,L_ede,L0_ide
      REAL*4 R0_ode,R0_ude,R0_afe
      LOGICAL*1 L0_efe
      REAL*4 R0_ife
      LOGICAL*1 L0_ofe,L0_ufe
      REAL*4 R_ake
      LOGICAL*1 L0_eke,L0_ike,L_oke,L0_uke,L_ale,L0_ele
      REAL*4 R0_ile,R0_ole,R0_ule,R0_ame
      LOGICAL*1 L0_eme,L0_ime
      REAL*4 R0_ome,R0_ume,R0_ape
      LOGICAL*1 L_epe,L_ipe,L0_ope,L0_upe,L_are,L0_ere,L0_ire
     &,L0_ore,L0_ure,L0_ase,L0_ese,L_ise,L_ose,L_use,L_ate
     &,L_ete,L_ite,L_ote,L_ute,L_ave,L_eve,L_ive
      LOGICAL*1 L_ove,L_uve
      REAL*4 R0_axe,R0_exe,R0_ixe,R0_oxe,R0_uxe,R0_abi,R_ebi
     &,R0_ibi,R_obi
      INTEGER*4 I0_ubi,I0_adi
      LOGICAL*1 L0_edi
      INTEGER*4 I_idi,I0_odi,I0_udi,I_afi
      REAL*4 R_efi,R_ifi,R_ofi,R_ufi
      INTEGER*4 I_aki,I0_eki,I0_iki,I_oki,I0_uki,I0_ali,I0_eli
     &,I0_ili,I_oli,I0_uli,I0_ami,I_emi
      CHARACTER*20 C20_imi,C20_omi,C20_umi,C20_api,C20_epi
     &,C20_ipi,C20_opi,C20_upi,C20_ari,C20_eri
      CHARACTER*8 C8_iri
      LOGICAL*1 L0_ori
      CHARACTER*8 C8_uri
      LOGICAL*1 L0_asi
      CHARACTER*8 C8_esi
      LOGICAL*1 L0_isi
      INTEGER*4 I0_osi
      LOGICAL*1 L0_usi
      INTEGER*4 I0_ati
      LOGICAL*1 L0_eti
      INTEGER*4 I0_iti,I0_oti,I0_uti
      LOGICAL*1 L0_avi
      INTEGER*4 I0_evi,I0_ivi,I0_ovi
      REAL*4 R_uvi,R_axi
      LOGICAL*1 L_exi
      REAL*4 R_ixi,R_oxi
      INTEGER*4 I_uxi,I0_abo,I0_ebo
      REAL*4 R0_ibo,R_obo,R0_ubo,R_ado
      INTEGER*4 I_edo,I0_ido,I0_odo,I_udo,I0_afo,I0_efo,I_ifo
     &,I0_ofo,I0_ufo
      LOGICAL*1 L_ako,L0_eko,L_iko,L_oko,L0_uko,L0_alo,L_elo
     &,L0_ilo,L_olo,L_ulo,L0_amo,L_emo,L0_imo,L_omo,L_umo
      REAL*4 R0_apo,R0_epo,R_ipo
      LOGICAL*1 L0_opo,L0_upo,L0_aro,L0_ero,L_iro,L_oro,L0_uro
     &,L0_aso,L_eso,L_iso,L0_oso,L0_uso,L_ato,L_eto
      REAL*4 R0_ito
      REAL*8 R8_oto
      LOGICAL*1 L0_uto,L0_avo
      REAL*4 R0_evo,R_ivo,R0_ovo,R0_uvo
      REAL*8 R8_axo
      LOGICAL*1 L_exo,L_ixo,L0_oxo,L0_uxo,L0_abu,L0_ebu,L_ibu
      INTEGER*4 I_obu,I0_ubu,I0_adu
      LOGICAL*1 L_edu,L_idu,L0_odu,L0_udu
      REAL*4 R0_afu,R0_efu,R_ifu
      LOGICAL*1 L0_ofu
      REAL*8 R8_ufu
      REAL*4 R0_aku,R0_eku,R0_iku,R0_oku
      LOGICAL*1 L0_uku
      REAL*4 R0_alu
      LOGICAL*1 L0_elu
      REAL*4 R0_ilu
      LOGICAL*1 L0_olu
      REAL*4 R0_ulu,R0_amu,R0_emu,R0_imu
      LOGICAL*1 L0_omu,L_umu
      REAL*4 R0_apu,R0_epu,R0_ipu,R0_opu,R0_upu,R0_aru,R0_eru
     &,R0_iru,R_oru,R0_uru,R0_asu,R0_esu,R0_isu,R0_osu,R_usu
      LOGICAL*1 L0_atu,L0_etu,L0_itu,L0_otu,L0_utu,L0_avu
     &,L_evu,L0_ivu,L0_ovu,L0_uvu,L0_axu,L_exu,L0_ixu,L0_oxu
     &,L0_uxu,L_abad,L0_ebad,L0_ibad,L0_obad,L0_ubad
      REAL*4 R_adad
      LOGICAL*1 L_edad,L_idad,L_odad,L_udad,L_afad,L_efad

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      R0_ed=R_id
C TELEGKA_HANDLER.fmg( 220, 344):pre: �������� ��������� ������,nv2dyn$99Dasha
      R0_o=R_u
C TELEGKA_HANDLER.fmg( 220, 332):pre: �������� ��������� ������,nv2dyn$100Dasha
      R0_ur=R_as
C TELEGKA_HANDLER.fmg( 206, 308):pre: �������� ��������� ������,nv2dyn$82Dasha
      R0_ar=R_er
C TELEGKA_HANDLER.fmg( 206, 295):pre: �������� ��������� ������,nv2dyn$83Dasha
      R0_el = 0.000001
C TELEGKA_HANDLER.fmg( 178, 256):��������� (RE4) (�������),nv2dyn$61Dasha
      R0_il = 0.999999
C TELEGKA_HANDLER.fmg( 178, 262):��������� (RE4) (�������),nv2dyn$60Dasha
      R0_am = 0.999999
C TELEGKA_HANDLER.fmg( 138, 468):��������� (RE4) (�������),nv2dyn$60Dasha
      R0_om = 0.000001
C TELEGKA_HANDLER.fmg( 146, 414):��������� (RE4) (�������),nv2dyn$61Dasha
      R0_ep = 0.000001
C TELEGKA_HANDLER.fmg( 178, 274):��������� (RE4) (�������),nv2dyn$61Dasha
      R0_ip = 0.999999
C TELEGKA_HANDLER.fmg( 177, 280):��������� (RE4) (�������),nv2dyn$60Dasha
      if(.not.L_ete) then
         R_er=0.0
      elseif(.not.L_or) then
         R_er=R_ipo
      else
         R_er=max(R0_ar-deltat,0.0)
      endif
      L0_ir=L_ete.and.R_er.le.0.0
      L_or=L_ete
C TELEGKA_HANDLER.fmg( 206, 295):�������� ��������� ������,nv2dyn$83Dasha
      if(.not.L_ete) then
         R_as=0.0
      elseif(.not.L_is) then
         R_as=R_usu
      else
         R_as=max(R0_ur-deltat,0.0)
      endif
      L0_es=L_ete.and.R_as.le.0.0
      L_is=L_ete
C TELEGKA_HANDLER.fmg( 206, 308):�������� ��������� ������,nv2dyn$82Dasha
      I0_us = z'0100008E'
C TELEGKA_HANDLER.fmg( 184, 363):��������� ������������� IN (�������)
      I0_at = z'01000086'
C TELEGKA_HANDLER.fmg( 195, 443):��������� ������������� IN (�������)
      R0_ode = 0.01
C TELEGKA_HANDLER.fmg(  24, 293):��������� (RE4) (�������),nv2dyn$74Dasha
      R0_ude = R0_ode + R_ake
C TELEGKA_HANDLER.fmg(  28, 292):��������
      R0_afe = 0.01
C TELEGKA_HANDLER.fmg(  24, 300):��������� (RE4) (�������),nv2dyn$74Dasha
      R0_ife = (-R0_afe) + R_ake
C TELEGKA_HANDLER.fmg(  28, 300):��������
      R0_ile = 0.33
C TELEGKA_HANDLER.fmg( 265, 444):��������� (RE4) (�������),nv2dyn$59Dasha
      R0_ule = 0.33
C TELEGKA_HANDLER.fmg( 265, 454):��������� (RE4) (�������),nv2dyn$59Dasha
      R0_ome = 0.0
C TELEGKA_HANDLER.fmg( 414, 456):��������� (RE4) (�������),nv2dyn$57Dasha
      R0_ape = 0.99
C TELEGKA_HANDLER.fmg( 396, 458):��������� (RE4) (�������),nv2dyn$60Dasha
      L0_abu = L_ixo.OR.L_ove
C TELEGKA_HANDLER.fmg(  60, 427):���
      L0_oxo = L_exo.OR.L_uve.OR.L_udad
C TELEGKA_HANDLER.fmg(  64, 382):���
      R0_uxe = 17000
C TELEGKA_HANDLER.fmg( 298, 474):��������� (RE4) (�������)
      if(R_usu.ge.0.0) then
         R0_abi=R0_uxe/max(R_usu,1.0e-10)
      else
         R0_abi=R0_uxe/min(R_usu,-1.0e-10)
      endif
C TELEGKA_HANDLER.fmg( 308, 473):�������� ����������
      R0_axe = -1
C TELEGKA_HANDLER.fmg( 330, 472):��������� (RE4) (�������)
      R0_oxe = 0.0
C TELEGKA_HANDLER.fmg( 319, 474):��������� (RE4) (�������)
      R0_ibi = 17000
C TELEGKA_HANDLER.fmg( 392, 446):��������� (RE4) (�������)
      I0_ubi = z'0100000A'
C TELEGKA_HANDLER.fmg( 220, 466):��������� ������������� IN (�������)
      I0_adi = z'01000003'
C TELEGKA_HANDLER.fmg( 220, 468):��������� ������������� IN (�������)
      I0_udi = z'0100000A'
C TELEGKA_HANDLER.fmg( 220, 488):��������� ������������� IN (�������)
      I0_odi = z'01000003'
C TELEGKA_HANDLER.fmg( 220, 486):��������� ������������� IN (�������)
      L_elo=R_ifi.ne.R_efi
      R_efi=R_ifi
C TELEGKA_HANDLER.fmg(  14, 398):���������� ������������� ������
      L_olo=R_ufi.ne.R_ofi
      R_ofi=R_ufi
C TELEGKA_HANDLER.fmg(  18, 447):���������� ������������� ������
      I0_iki = z'01000003'
C TELEGKA_HANDLER.fmg( 138, 360):��������� ������������� IN (�������)
      I0_eki = z'01000010'
C TELEGKA_HANDLER.fmg( 138, 358):��������� ������������� IN (�������)
      I0_ali = z'01000003'
C TELEGKA_HANDLER.fmg( 116, 450):��������� ������������� IN (�������)
      I0_uki = z'01000010'
C TELEGKA_HANDLER.fmg( 116, 448):��������� ������������� IN (�������)
      I0_ili = z'01000003'
C TELEGKA_HANDLER.fmg( 184, 430):��������� ������������� IN (�������)
      I0_eli = z'0100000A'
C TELEGKA_HANDLER.fmg( 184, 428):��������� ������������� IN (�������)
      I0_ami = z'01000003'
C TELEGKA_HANDLER.fmg( 192, 377):��������� ������������� IN (�������)
      I0_uli = z'0100000A'
C TELEGKA_HANDLER.fmg( 192, 375):��������� ������������� IN (�������)
      I0_odo = z'01000003'
C TELEGKA_HANDLER.fmg( 114, 369):��������� ������������� IN (�������)
      I0_ido = z'0100000A'
C TELEGKA_HANDLER.fmg( 114, 367):��������� ������������� IN (�������)
      C20_umi = '��������� 2'
C TELEGKA_HANDLER.fmg(  16, 323):��������� ���������� CH20 (CH30) (�������)
      C20_api = '������� ���'
C TELEGKA_HANDLER.fmg(  16, 325):��������� ���������� CH20 (CH30) (�������)
      C20_imi = '��������� 1'
C TELEGKA_HANDLER.fmg(  32, 322):��������� ���������� CH20 (CH30) (�������)
      C20_ipi = '� ��������� 1'
C TELEGKA_HANDLER.fmg(  50, 369):��������� ���������� CH20 (CH30) (�������)
      C20_upi = '� ��������� 2'
C TELEGKA_HANDLER.fmg(  34, 370):��������� ���������� CH20 (CH30) (�������)
      C20_ari = ''
C TELEGKA_HANDLER.fmg(  34, 372):��������� ���������� CH20 (CH30) (�������)
      C8_iri = 'pos1'
C TELEGKA_HANDLER.fmg(  18, 378):��������� ���������� CH8 (�������)
      call chcomp(C8_esi,C8_iri,L0_ori)
C TELEGKA_HANDLER.fmg(  22, 382):���������� ���������
      C8_uri = 'pos2'
C TELEGKA_HANDLER.fmg(  18, 420):��������� ���������� CH8 (�������)
      call chcomp(C8_esi,C8_uri,L0_asi)
C TELEGKA_HANDLER.fmg(  22, 424):���������� ���������
      if(L0_asi) then
         C20_opi=C20_upi
      else
         C20_opi=C20_ari
      endif
C TELEGKA_HANDLER.fmg(  38, 370):���� RE IN LO CH20
      if(L0_ori) then
         C20_eri=C20_ipi
      else
         C20_eri=C20_opi
      endif
C TELEGKA_HANDLER.fmg(  54, 370):���� RE IN LO CH20
      I0_osi = z'0100008E'
C TELEGKA_HANDLER.fmg( 169, 401):��������� ������������� IN (�������)
      I0_ati = z'01000086'
C TELEGKA_HANDLER.fmg( 164, 447):��������� ������������� IN (�������)
      I0_oti = z'01000003'
C TELEGKA_HANDLER.fmg( 152, 460):��������� ������������� IN (�������)
      I0_uti = z'01000010'
C TELEGKA_HANDLER.fmg( 152, 462):��������� ������������� IN (�������)
      I0_ovi = z'01000003'
C TELEGKA_HANDLER.fmg( 154, 407):��������� ������������� IN (�������)
      I0_ivi = z'01000010'
C TELEGKA_HANDLER.fmg( 154, 405):��������� ������������� IN (�������)
      L_oro=R_axi.ne.R_uvi
      R_uvi=R_axi
C TELEGKA_HANDLER.fmg(  14, 410):���������� ������������� ������
      L_exi=R_oxi.ne.R_ixi
      R_ixi=R_oxi
C TELEGKA_HANDLER.fmg(  10, 434):���������� ������������� ������
      L0_ilo = L_exi.AND.L0_asi
C TELEGKA_HANDLER.fmg(  30, 432):�
      L0_imo = L_olo.OR.L0_ilo
C TELEGKA_HANDLER.fmg(  51, 436):���
      L0_al = L_emo.AND.L0_imo
C TELEGKA_HANDLER.fmg( 200, 344):�
      L0_of = L0_imo.OR.(.NOT.L_ulo)
C TELEGKA_HANDLER.fmg( 205, 328):���
      L0_alo = L_exi.AND.L0_ori
C TELEGKA_HANDLER.fmg(  28, 389):�
      L0_amo = L_elo.OR.L0_alo
C TELEGKA_HANDLER.fmg(  52, 391):���
      L0_uk = L_ulo.AND.L0_amo
C TELEGKA_HANDLER.fmg( 200, 332):�
      L_uf=(L0_uk.or.L_uf).and..not.(L0_of)
      L0_ak=.not.L_uf
C TELEGKA_HANDLER.fmg( 212, 330):RS �������,156
      if(.not.L_uf) then
         R_u=0.0
      elseif(.not.L_ad) then
         R_u=R_ipo
      else
         R_u=max(R0_o-deltat,0.0)
      endif
      L0_ud=L_uf.and.R_u.le.0.0
      L_ad=L_uf
C TELEGKA_HANDLER.fmg( 220, 332):�������� ��������� ������,nv2dyn$100Dasha
      L0_ek = (.NOT.L_emo).OR.L0_amo
C TELEGKA_HANDLER.fmg( 206, 340):���
      L_ik=(L0_al.or.L_ik).and..not.(L0_ek)
      L0_ok=.not.L_ik
C TELEGKA_HANDLER.fmg( 212, 342):RS �������,155
      if(.not.L_ik) then
         R_id=0.0
      elseif(.not.L_od) then
         R_id=R_usu
      else
         R_id=max(R0_ed-deltat,0.0)
      endif
      L0_ef=L_ik.and.R_id.le.0.0
      L_od=L_ik
C TELEGKA_HANDLER.fmg( 220, 344):�������� ��������� ������,nv2dyn$99Dasha
      I0_ofo = z'0100000E'
C TELEGKA_HANDLER.fmg( 188, 398):��������� ������������� IN (�������)
      I0_abo = z'01000007'
C TELEGKA_HANDLER.fmg( 140, 380):��������� ������������� IN (�������)
      I0_ebo = z'01000003'
C TELEGKA_HANDLER.fmg( 140, 382):��������� ������������� IN (�������)
      R0_ibo = 100
C TELEGKA_HANDLER.fmg( 378, 471):��������� (RE4) (�������)
      R0_ubo = 100
C TELEGKA_HANDLER.fmg( 364, 465):��������� (RE4) (�������)
      I0_efo = z'01000003'
C TELEGKA_HANDLER.fmg( 122, 463):��������� ������������� IN (�������)
      I0_afo = z'0100000A'
C TELEGKA_HANDLER.fmg( 122, 461):��������� ������������� IN (�������)
      I0_ubu = z'0100000E'
C TELEGKA_HANDLER.fmg( 182, 453):��������� ������������� IN (�������)
      L_oko=(L_iko.or.L_oko).and..not.(L_ako)
      L0_eko=.not.L_oko
C TELEGKA_HANDLER.fmg( 256, 272):RS �������
      L0_ero=.false.
C TELEGKA_HANDLER.fmg(  64, 413):��������� ���������� (�������)
      L0_aro=.false.
C TELEGKA_HANDLER.fmg(  64, 411):��������� ���������� (�������)
      R0_apo = DeltaT
C TELEGKA_HANDLER.fmg( 250, 452):��������� (RE4) (�������)
      if(R_ipo.ge.0.0) then
         R0_epo=R0_apo/max(R_ipo,1.0e-10)
      else
         R0_epo=R0_apo/min(R_ipo,-1.0e-10)
      endif
C TELEGKA_HANDLER.fmg( 258, 450):�������� ����������
      R0_ole = R0_epo * R0_ile
C TELEGKA_HANDLER.fmg( 268, 445):����������
      if(L_ete) then
         R0_uru=R0_ole
      else
         R0_uru=R0_epo
      endif
C TELEGKA_HANDLER.fmg( 276, 446):���� RE IN LO CH7,nv2dyn$40Dasha
      R0_ito = 0.1
C TELEGKA_HANDLER.fmg( 237, 372):��������� (RE4) (�������)
      L_eto=R8_oto.lt.R0_ito
C TELEGKA_HANDLER.fmg( 242, 373):���������� <
      R0_evo = 0.0
C TELEGKA_HANDLER.fmg( 248, 392):��������� (RE4) (�������)
      R0_alu = 0.000001
C TELEGKA_HANDLER.fmg( 354, 406):��������� (RE4) (�������)
      R0_ilu = 0.999999
C TELEGKA_HANDLER.fmg( 354, 422):��������� (RE4) (�������)
      R0_afu = 0.0
C TELEGKA_HANDLER.fmg( 296, 442):��������� (RE4) (�������)
      R0_efu = 0.0
C TELEGKA_HANDLER.fmg( 369, 440):��������� (RE4) (�������)
      L0_ofu = L_idad.OR.L_odad
C TELEGKA_HANDLER.fmg( 362, 435):���
      R0_eku = 1.0
C TELEGKA_HANDLER.fmg( 348, 450):��������� (RE4) (�������)
      R0_iku = 0.0
C TELEGKA_HANDLER.fmg( 340, 450):��������� (RE4) (�������)
      R0_amu = 1.0e-10
C TELEGKA_HANDLER.fmg( 318, 426):��������� (RE4) (�������)
      R0_apu = 0.0
C TELEGKA_HANDLER.fmg( 328, 440):��������� (RE4) (�������)
      R0_aru = DeltaT
C TELEGKA_HANDLER.fmg( 250, 462):��������� (RE4) (�������)
      if(R_usu.ge.0.0) then
         R0_eru=R0_aru/max(R_usu,1.0e-10)
      else
         R0_eru=R0_aru/min(R_usu,-1.0e-10)
      endif
C TELEGKA_HANDLER.fmg( 258, 460):�������� ����������
      R0_ame = R0_eru * R0_ule
C TELEGKA_HANDLER.fmg( 268, 455):����������
      if(L_ete) then
         R0_iru=R0_ame
      else
         R0_iru=R0_eru
      endif
C TELEGKA_HANDLER.fmg( 276, 456):���� RE IN LO CH7,nv2dyn$40Dasha
      R0_osu = 0.0
C TELEGKA_HANDLER.fmg( 282, 442):��������� (RE4) (�������)
      L0_efe=R_ifu.gt.R0_ife
C TELEGKA_HANDLER.fmg(  33, 304):���������� >,nv2dyn$84Dasha
C label 201  try201=try201-1
      if(L0_ofu) then
         R0_opu=R_oru
      endif
C TELEGKA_HANDLER.fmg( 366, 450):���� � ������������� �������
      L0_uku=R_oru.lt.R0_alu
C TELEGKA_HANDLER.fmg( 362, 408):���������� <
      L0_eme = L0_uku.AND.(.NOT.L_ite)
C TELEGKA_HANDLER.fmg( 398, 407):�
      L_edu = L_ote.OR.L0_eme.OR.L_eve
C TELEGKA_HANDLER.fmg( 402, 407):���
      L0_ese = L_ute.AND.L_edu
C TELEGKA_HANDLER.fmg( 222, 320):�
      L0_elu=R_oru.gt.R0_ilu
C TELEGKA_HANDLER.fmg( 362, 423):���������� >
      L0_ime = L0_elu.AND.(.NOT.L_ave)
C TELEGKA_HANDLER.fmg( 396, 422):�
      L_idu = L_ute.OR.L0_ime.OR.L_ive
C TELEGKA_HANDLER.fmg( 402, 422):���
      L0_ase = L_ote.AND.L_idu
C TELEGKA_HANDLER.fmg( 222, 314):�
      L0_ure = L0_es.AND.(.NOT.L_idu)
C TELEGKA_HANDLER.fmg( 214, 308):�
      L0_ore = L0_ir.AND.(.NOT.L_edu)
C TELEGKA_HANDLER.fmg( 211, 294):�
      L0_up=R_oru.gt.R0_ip
C TELEGKA_HANDLER.fmg( 182, 282):���������� >,nv2dyn$45Dasha
      L0_ire = L_ave.AND.L0_up
C TELEGKA_HANDLER.fmg( 214, 284):�
      L0_op=R_oru.lt.R0_ep
C TELEGKA_HANDLER.fmg( 183, 276):���������� <,nv2dyn$46Dasha
      L0_ere = L_ite.AND.L0_op
C TELEGKA_HANDLER.fmg( 214, 274):�
      L0_em=R_oru.gt.R0_am
C TELEGKA_HANDLER.fmg( 147, 470):���������� >,nv2dyn$45Dasha
      L0_im = L_ave.AND.L0_em
C TELEGKA_HANDLER.fmg( 157, 476):�
      L0_avi = (.NOT.L0_im).AND.L_idu
C TELEGKA_HANDLER.fmg( 152, 400):�
      if(L0_avi) then
         I0_evi=I0_ivi
      else
         I0_evi=I0_ovi
      endif
C TELEGKA_HANDLER.fmg( 158, 406):���� RE IN LO CH7
      L0_um=R_oru.lt.R0_om
C TELEGKA_HANDLER.fmg( 153, 416):���������� <,nv2dyn$46Dasha
      L0_ap = L_ite.AND.L0_um
C TELEGKA_HANDLER.fmg( 160, 420):�
      L0_itu = L_iso.OR.L_edad
C TELEGKA_HANDLER.fmg(  97, 412):���
      L0_ovu = L_edu.AND.(.NOT.L_ote)
C TELEGKA_HANDLER.fmg(  96, 360):�
      L0_i = L_idu.AND.L_ose
C TELEGKA_HANDLER.fmg(  50, 359):�
      L0_e = L_edu.AND.L_ate
C TELEGKA_HANDLER.fmg(  50, 347):�
      L0_uko = L0_i.OR.L0_e
C TELEGKA_HANDLER.fmg(  58, 358):���
      L0_opo = L_ibu.OR.L_umo
C TELEGKA_HANDLER.fmg(  54, 399):���
      L0_uxo = (.NOT.L0_opo).AND.L0_amo.AND.(.NOT.L_ulo)
C TELEGKA_HANDLER.fmg(  66, 391):�
      L0_uvu = L0_uxo.OR.L0_oxo
C TELEGKA_HANDLER.fmg(  70, 390):���
      L0_axu = L0_uvu.AND.(.NOT.L0_ovu)
C TELEGKA_HANDLER.fmg( 103, 389):�
      L0_otu = (.NOT.L_eso).AND.L0_ovu
C TELEGKA_HANDLER.fmg( 102, 364):�
      L_ale=(L_idu.or.L_ale).and..not.(L_edu)
      L0_ike=.not.L_ale
C TELEGKA_HANDLER.fmg( 105, 316):RS �������,nv2dyn$86Dasha
      L0_ele = L_ive.AND.(.NOT.L_ale)
C TELEGKA_HANDLER.fmg( 114, 318):�
      L_oke=(L_edu.or.L_oke).and..not.(L_idu)
      L0_eke=.not.L_oke
C TELEGKA_HANDLER.fmg( 104, 304):RS �������,nv2dyn$87Dasha
      L0_uke = L_eve.AND.(.NOT.L_oke)
C TELEGKA_HANDLER.fmg( 114, 310):�
      L_exu = L0_ele.OR.L0_uke
C TELEGKA_HANDLER.fmg( 120, 318):���
      L0_utu = L_evu.AND.(.NOT.L0_otu).AND.(.NOT.L_exu)
C TELEGKA_HANDLER.fmg( 129, 387):�
      L0_isi = L0_ap.OR.L0_utu
C TELEGKA_HANDLER.fmg( 170, 392):���
      if(L0_isi) then
         I0_ufo=I0_osi
      else
         I0_ufo=I0_evi
      endif
C TELEGKA_HANDLER.fmg( 172, 404):���� RE IN LO CH7
      if(L_ibu) then
         I_ifo=I0_ofo
      else
         I_ifo=I0_ufo
      endif
C TELEGKA_HANDLER.fmg( 192, 404):���� RE IN LO CH7
      L0_os=I_ifo.eq.I0_us
C TELEGKA_HANDLER.fmg( 189, 364):���������� �������������,nv2dyn$77Dasha
      L_ex = L0_os.AND.L0_utu
C TELEGKA_HANDLER.fmg( 194, 363):�
      L0_ux = (.NOT.L_efad).AND.L_ox
C TELEGKA_HANDLER.fmg(  99, 288):�
      L0_abe = L_ex.AND.L0_ux
C TELEGKA_HANDLER.fmg( 103, 290):�
      L0_ide=R_ifu.lt.R0_ude
C TELEGKA_HANDLER.fmg(  35, 298):���������� <,nv2dyn$85Dasha
      L0_ofe = L0_efe.AND.L0_ide
C TELEGKA_HANDLER.fmg(  40, 302):�
      L0_ufe = L0_ofe.AND.L_efad
C TELEGKA_HANDLER.fmg(  52, 294):�
      L0_eti = L_edu.AND.(.NOT.L0_ap)
C TELEGKA_HANDLER.fmg( 151, 453):�
      if(L0_eti) then
         I0_iti=I0_oti
      else
         I0_iti=I0_uti
      endif
C TELEGKA_HANDLER.fmg( 155, 460):���� RE IN LO CH7
      L0_et=I_obu.eq.I0_at
C TELEGKA_HANDLER.fmg( 200, 444):���������� �������������,nv2dyn$75Dasha
      L_ube=(L_ede.or.L_ube).and..not.(L_edu)
      L0_ade=.not.L_ube
C TELEGKA_HANDLER.fmg(  52, 288):RS �������,nv2dyn$101Dasha
      L0_obe = L0_ufe.AND.L_ube
C TELEGKA_HANDLER.fmg(  71, 294):�
      L_ebe=(L0_obe.or.L_ebe).and..not.(L0_abe)
      L0_ibe=.not.L_ebe
C TELEGKA_HANDLER.fmg( 108, 292):RS �������,nv2dyn$102Dasha
      L0_ut = (.NOT.L_efad).AND.L_ot
C TELEGKA_HANDLER.fmg( 104, 274):�
      L0_av = L_ede.AND.L0_ut
C TELEGKA_HANDLER.fmg( 108, 276):�
      L_ov=(L_ex.or.L_ov).and..not.(L_idu)
      L0_iv=.not.L_ov
C TELEGKA_HANDLER.fmg(  58, 276):RS �������,nv2dyn$104Dasha
      L0_uv = L0_ufe.AND.L_ov
C TELEGKA_HANDLER.fmg(  71, 280):�
      L_ax=(L0_uv.or.L_ax).and..not.(L0_av)
      L0_ev=.not.L_ax
C TELEGKA_HANDLER.fmg( 116, 278):RS �������,nv2dyn$105Dasha
      L_are = L_ebe.OR.L_ax
C TELEGKA_HANDLER.fmg( 130, 292):���
      L0_ul=R_ifu.gt.R0_il
C TELEGKA_HANDLER.fmg( 182, 264):���������� >,nv2dyn$45Dasha
      L0_upe = L_use.AND.L0_ul
C TELEGKA_HANDLER.fmg( 214, 267):�
      L0_ol=R_ifu.lt.R0_el
C TELEGKA_HANDLER.fmg( 183, 258):���������� <,nv2dyn$46Dasha
      L0_ope = L_ise.AND.L0_ol
C TELEGKA_HANDLER.fmg( 214, 256):�
      L_ipe=(L0_ef.or.L_ipe).and..not.(L_ex)
      L0_if=.not.L_ipe
C TELEGKA_HANDLER.fmg( 237, 342):RS �������,nv2dyn$95Dasha
      L_epe=(L0_ud.or.L_epe).and..not.(L_ede)
      L0_af=.not.L_epe
C TELEGKA_HANDLER.fmg( 237, 330):RS �������,nv2dyn$96Dasha
      L0_oso =.NOT.(L_afad.OR.L_udad.OR.L0_ese.OR.L0_ase.OR.L0_ure.OR.L0
     &_ore.OR.L0_ire.OR.L0_ere.OR.L_idad.OR.L_ate.OR.L_ose.OR.L_are.OR.L
     &_exu.OR.L0_upe.OR.L0_ope.OR.L_ipe.OR.L_epe)
C TELEGKA_HANDLER.fmg( 248, 308):���
      L0_uso =.NOT.(L0_oso)
C TELEGKA_HANDLER.fmg( 259, 280):���
      L_ibu = L0_uso.OR.L_oko
C TELEGKA_HANDLER.fmg( 263, 278):���
C sav1=L0_opo
      L0_opo = L_ibu.OR.L_umo
C TELEGKA_HANDLER.fmg(  54, 399):recalc:���
C if(sav1.ne.L0_opo .and. try308.gt.0) goto 308
      L0_upo = L_ibu.OR.L_omo
C TELEGKA_HANDLER.fmg(  56, 450):���
      L0_ebu = L0_imo.AND.(.NOT.L0_upo).AND.(.NOT.L_emo)
C TELEGKA_HANDLER.fmg(  66, 436):�
      L0_ibad = L0_ebu.OR.L0_abu.OR.L_afad
C TELEGKA_HANDLER.fmg(  70, 432):���
      L0_uro = L0_ibad.OR.L0_uvu
C TELEGKA_HANDLER.fmg(  75, 401):���
      L_iso=(L_oro.or.L_iso).and..not.(L0_uro)
      L0_aso=.not.L_iso
C TELEGKA_HANDLER.fmg( 106, 403):RS �������,10
C sav1=L0_itu
      L0_itu = L_iso.OR.L_edad
C TELEGKA_HANDLER.fmg(  97, 412):recalc:���
C if(sav1.ne.L0_itu .and. try291.gt.0) goto 291
      L_eso=L_iso
C TELEGKA_HANDLER.fmg( 120, 405):������,STOP
C sav1=L0_otu
      L0_otu = (.NOT.L_eso).AND.L0_ovu
C TELEGKA_HANDLER.fmg( 102, 364):recalc:�
C if(sav1.ne.L0_otu .and. try320.gt.0) goto 320
      L0_obad = L_idu.AND.(.NOT.L_ute)
C TELEGKA_HANDLER.fmg(  81, 457):�
      L0_ixu = (.NOT.L_eso).AND.L0_obad
C TELEGKA_HANDLER.fmg( 104, 458):�
      L0_uxu = L0_obad.OR.L0_itu.OR.L0_uvu.OR.L0_uko
C TELEGKA_HANDLER.fmg( 103, 426):���
      L0_ubad = (.NOT.L0_obad).AND.L0_ibad
C TELEGKA_HANDLER.fmg( 103, 433):�
      L_abad=(L0_ubad.or.L_abad).and..not.(L0_uxu)
      L0_ebad=.not.L_abad
C TELEGKA_HANDLER.fmg( 110, 431):RS �������,1
      L0_oxu = (.NOT.L0_ixu).AND.L_abad.AND.(.NOT.L_exu)
C TELEGKA_HANDLER.fmg( 129, 433):�
      L0_udu = L0_oxu.AND.(.NOT.L_udad)
C TELEGKA_HANDLER.fmg( 252, 440):�
      L0_etu = L0_udu.OR.L_afad
C TELEGKA_HANDLER.fmg( 264, 439):���
      if(L0_etu) then
         R0_esu=R0_iru
      else
         R0_esu=R0_osu
      endif
C TELEGKA_HANDLER.fmg( 286, 452):���� RE IN LO CH7
      L0_odu = L0_utu.AND.(.NOT.L_afad)
C TELEGKA_HANDLER.fmg( 252, 426):�
      L0_atu = L0_odu.OR.L_udad
C TELEGKA_HANDLER.fmg( 264, 425):���
      if(L0_atu) then
         R0_asu=R0_uru
      else
         R0_asu=R0_osu
      endif
C TELEGKA_HANDLER.fmg( 286, 434):���� RE IN LO CH7
      R0_isu = R0_esu + (-R0_asu)
C TELEGKA_HANDLER.fmg( 292, 444):��������
      if(L_edad) then
         R0_epu=R0_afu
      else
         R0_epu=R0_isu
      endif
C TELEGKA_HANDLER.fmg( 300, 442):���� RE IN LO CH7
      if(L_umu) then
         R0_opu=R_adad
      endif
C TELEGKA_HANDLER.fmg( 324, 450):���� � ������������� �������
      if(L_umu) then
         R0_ipu=R0_apu
      else
         R0_ipu=R0_epu
      endif
C TELEGKA_HANDLER.fmg( 332, 442):���� RE IN LO CH7
      if(L_odad) then
         R0_aku=R0_efu
      else
         R0_aku=R8_ufu
      endif
C TELEGKA_HANDLER.fmg( 372, 440):���� RE IN LO CH7
      if(L0_ofu) then
         R8_ufu=R0_aku
      else
         R8_ufu=R_oru
      endif
C TELEGKA_HANDLER.fmg( 376, 444):���� RE IN LO CH7
      if(.NOT.L0_ofu) then
         R0_opu=R8_ufu
      endif
C TELEGKA_HANDLER.fmg( 384, 450):���� � ������������� �������
      if(L_ate) then
         R0_ume=R0_ape
      else
         R0_ume=R0_opu
      endif
C TELEGKA_HANDLER.fmg( 400, 458):���� RE IN LO CH7,nv2dyn$56Dasha
      if(L_ose) then
         R_ifu=R0_ome
      else
         R_ifu=R0_ume
      endif
C TELEGKA_HANDLER.fmg( 419, 458):���� RE IN LO CH7,nv2dyn$56Dasha
      L0_avu = L0_itu.OR.L0_ovu.OR.L0_ibad.OR.L0_uko
C TELEGKA_HANDLER.fmg( 103, 382):���
      L_evu=(L0_axu.or.L_evu).and..not.(L0_avu)
      L0_ivu=.not.L_evu
C TELEGKA_HANDLER.fmg( 110, 387):RS �������,2
C sav1=L0_utu
      L0_utu = L_evu.AND.(.NOT.L0_otu).AND.(.NOT.L_exu)
C TELEGKA_HANDLER.fmg( 129, 387):recalc:�
C if(sav1.ne.L0_utu .and. try344.gt.0) goto 344
      L0_usi = L0_im.OR.L0_oxu
C TELEGKA_HANDLER.fmg( 166, 442):���
      if(L0_usi) then
         I0_adu=I0_ati
      else
         I0_adu=I0_iti
      endif
C TELEGKA_HANDLER.fmg( 168, 460):���� RE IN LO CH7
      R0_imu = R0_opu + (-R_adad)
C TELEGKA_HANDLER.fmg( 308, 433):��������
      R0_ulu = R0_epu + R0_imu
C TELEGKA_HANDLER.fmg( 314, 434):��������
      R0_emu = R0_ulu * R0_imu
C TELEGKA_HANDLER.fmg( 318, 433):����������
      L0_omu=R0_emu.lt.R0_amu
C TELEGKA_HANDLER.fmg( 322, 432):���������� <
      R0_upu = R0_opu + R0_ipu
C TELEGKA_HANDLER.fmg( 338, 443):��������
      R0_oku=MAX(R0_iku,R0_upu)
C TELEGKA_HANDLER.fmg( 345, 444):��������
      R_oru=MIN(R0_eku,R0_oku)
C TELEGKA_HANDLER.fmg( 353, 445):�������
      L_umu=(L0_omu.or.L_umu).and..not.(.NOT.L_efad)
      L0_olu=.not.L_umu
C TELEGKA_HANDLER.fmg( 329, 430):RS �������,6
      if(L_ibu) then
         I_obu=I0_ubu
      else
         I_obu=I0_adu
      endif
C TELEGKA_HANDLER.fmg( 186, 458):���� RE IN LO CH7
      R_ado = R0_ubo * R8_ufu
C TELEGKA_HANDLER.fmg( 368, 464):����������
      R_obo = R0_ibo + (-R_ado)
C TELEGKA_HANDLER.fmg( 382, 470):��������
      R_obi = R0_ibi * R8_ufu
C TELEGKA_HANDLER.fmg( 396, 445):����������
      if(L0_oxu) then
         I_oli=I0_eli
      else
         I_oli=I0_ili
      endif
C TELEGKA_HANDLER.fmg( 188, 428):���� RE IN LO CH7
      L_ede = L0_et.AND.L0_oxu
C TELEGKA_HANDLER.fmg( 204, 443):�
C sav1=L0_av
      L0_av = L_ede.AND.L0_ut
C TELEGKA_HANDLER.fmg( 108, 276):recalc:�
C if(sav1.ne.L0_av .and. try399.gt.0) goto 399
      if(L0_oxu) then
         R0_ixe=R0_abi
      else
         R0_ixe=R0_oxe
      endif
C TELEGKA_HANDLER.fmg( 322, 474):���� RE IN LO CH7
      R0_exe = R0_axe * R0_ixe
C TELEGKA_HANDLER.fmg( 334, 471):����������
      if(L0_utu) then
         R_ebi=R0_exe
      else
         R_ebi=R0_ixe
      endif
C TELEGKA_HANDLER.fmg( 340, 472):���� RE IN LO CH7
      L0_uto = L0_oxu.OR.L0_utu
C TELEGKA_HANDLER.fmg( 234, 385):���
      L0_avo = L0_uto.AND.(.NOT.L_eto)
C TELEGKA_HANDLER.fmg( 248, 384):�
      if(L0_avo) then
         R0_uvo=R_ivo
      else
         R0_uvo=R0_evo
      endif
C TELEGKA_HANDLER.fmg( 252, 392):���� RE IN LO CH7
      if(L0_ixu) then
         I_udo=I0_afo
      else
         I_udo=I0_efo
      endif
C TELEGKA_HANDLER.fmg( 125, 462):���� RE IN LO CH7
      if(L0_ixu) then
         I_oki=I0_uki
      else
         I_oki=I0_ali
      endif
C TELEGKA_HANDLER.fmg( 120, 448):���� RE IN LO CH7
      L_iro = L_ibu.OR.L0_ero.OR.L0_aro.OR.L0_upo.OR.L0_opo
C TELEGKA_HANDLER.fmg(  68, 411):���
      if(L_iro) then
         I_afi=I0_ubi
      else
         I_afi=I0_adi
      endif
C TELEGKA_HANDLER.fmg( 223, 467):���� RE IN LO CH7
      L0_edi = L_ibu.OR.L_iro
C TELEGKA_HANDLER.fmg( 218, 478):���
      if(L0_edi) then
         I_idi=I0_odi
      else
         I_idi=I0_udi
      endif
C TELEGKA_HANDLER.fmg( 223, 486):���� RE IN LO CH7
      if(L_ibu) then
         I_uxi=I0_abo
      else
         I_uxi=I0_ebo
      endif
C TELEGKA_HANDLER.fmg( 144, 380):���� RE IN LO CH7
      L_ato = L0_oso.OR.L_iko
C TELEGKA_HANDLER.fmg( 259, 284):���
      L_ot=(L_efad.or.L_ot).and..not.(L0_av)
      L0_it=.not.L_ot
C TELEGKA_HANDLER.fmg( 100, 272):RS �������,nv2dyn$106Dasha
C sav1=L0_ut
      L0_ut = (.NOT.L_efad).AND.L_ot
C TELEGKA_HANDLER.fmg( 104, 274):recalc:�
C if(sav1.ne.L0_ut .and. try398.gt.0) goto 398
      L_ox=(L_efad.or.L_ox).and..not.(L0_abe)
      L0_ix=.not.L_ox
C TELEGKA_HANDLER.fmg(  94, 286):RS �������,nv2dyn$103Dasha
C sav1=L0_ux
      L0_ux = (.NOT.L_efad).AND.L_ox
C TELEGKA_HANDLER.fmg(  99, 288):recalc:�
C if(sav1.ne.L0_ux .and. try377.gt.0) goto 377
      if(L0_utu) then
         I_emi=I0_uli
      else
         I_emi=I0_ami
      endif
C TELEGKA_HANDLER.fmg( 196, 376):���� RE IN LO CH7
      if(L0_otu) then
         I_edo=I0_ido
      else
         I_edo=I0_odo
      endif
C TELEGKA_HANDLER.fmg( 118, 368):���� RE IN LO CH7
      if(L0_otu) then
         I_aki=I0_eki
      else
         I_aki=I0_iki
      endif
C TELEGKA_HANDLER.fmg( 142, 358):���� RE IN LO CH7
      if(L_idu) then
         C20_omi=C20_umi
      else
         C20_omi=C20_api
      endif
C TELEGKA_HANDLER.fmg(  20, 324):���� RE IN LO CH20
      if(L_edu) then
         C20_epi=C20_imi
      else
         C20_epi=C20_omi
      endif
C TELEGKA_HANDLER.fmg(  36, 322):���� RE IN LO CH20
      R0_ovo = R8_axo
C TELEGKA_HANDLER.fmg( 246, 401):��������
C label 666  try666=try666-1
      R8_axo = R0_uvo + R0_ovo
C TELEGKA_HANDLER.fmg( 258, 400):��������
C sav1=R0_ovo
      R0_ovo = R8_axo
C TELEGKA_HANDLER.fmg( 246, 401):recalc:��������
C if(sav1.ne.R0_ovo .and. try666.gt.0) goto 666
      End
