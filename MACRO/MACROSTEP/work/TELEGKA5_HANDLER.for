      Subroutine TELEGKA5_HANDLER(ext_deltat,R_imi,I_e,L_i
     &,L_o,L_u,L_ad,I_od,R_ud,R_al,R_el,R_il,R_ol,L_es,R_it
     &,R_ube,L_ade,R_ode,L_ude,L_ake,L_oke,R_ome,R_ope,L_are
     &,R_ore,L_ase,L_ese,L_ise,L_eve,L_ove,L_uve,L_ixe,L_uxe
     &,L_ubi,L_edi,L_idi,L_odi,L_afi,L_ofi,L_eki,L_oki,R_omi
     &,L_epi,L_opi,L_ari,C20_asi,L_esi,L_isi,L_osi,L_usi,L_ati
     &,L_eti,L_iti,L_oti,L_uti,L_avi,L_evi,L_ivi,L_ovi,L_uvi
     &,R_exi,R_oxi,R_uxi,R_abo,R_ebo,R_ibo,R_obo,R_odo,I_uko
     &,I_alo,I_elo,I_amo,I_emo,I_imo,I_epo,I_ipo,I_opo,I_iro
     &,I_oro,I_uro,I_aso,I_uso,I_ato,I_uto,I_ivo,R_ovo,R_uvo
     &,R_axo,R_exo,I_ixo,I_abu,I_adu,I_odu,C20_ofu,C8_oku
     &,R_epu,R_ipu,L_opu,R_upu,R_aru,I_eru,L_uru,L_asu,L_esu
     &,I_osu,I_etu,I_utu,L_ovu,L_axu,L_ixu,L_uxu,L_abad,L_ebad
     &,L_idad,L_afad,L_ifad,L_ofad,R8_akad,R_ukad,R8_ilad
     &,L_olad,L_ulad,L_emad,L_imad,L_umad,I_apad,L_opad,L_upad
     &,L_urad,L_itad,L_ivad)
C |R_imi         |4 4 I|11 mlfpar19     ||0.6|
C |I_e           |2 4 O|NUM_BOAT        |�������� ������� �������||
C |L_i           |1 1 S|_splsJ4028*     |[TF]���������� ��������� ������������� |F|
C |L_o           |1 1 S|_splsJ4025*     |[TF]���������� ��������� ������������� |F|
C |L_u           |1 1 I|RESET_BOAT      |�������� ������� �������||
C |L_ad          |1 1 I|PREPARE_BOAT    |����������� ������� � �������� �������||
C |I_od          |2 4 S|_COUNTER_OUTCOUNTER*|||
C |R_ud          |4 4 S|_slpbJ3978*     |���������� ��������� ||
C |R_al          |4 4 S|_slpbJ3934*     |���������� ��������� ||
C |R_el          |4 4 S|_slpbJ3933*     |���������� ��������� ||
C |R_il          |4 4 S|_slpbJ3932*     |���������� ��������� ||
C |R_ol          |4 4 S|_slpbJ3926*     |���������� ��������� ||
C |L_es          |1 1 S|_qffJ3882*      |�������� ������ Q RS-��������  |F|
C |R_it          |4 4 I|mlfpar19        |��������� ������������|0.6|
C |R_ube         |4 4 S|_sdelnv2dyn$100Dasha*|[���]���������� ��������� |0|
C |L_ade         |1 1 S|_idelnv2dyn$100Dasha*|[TF]���������� ��������� �������� ������ |f|
C |R_ode         |4 4 S|_sdelnv2dyn$99Dasha*|[���]���������� ��������� |0|
C |L_ude         |1 1 S|_idelnv2dyn$99Dasha*|[TF]���������� ��������� �������� ������ |f|
C |L_ake         |1 1 S|_qff156*        |�������� ������ Q RS-��������  |F|
C |L_oke         |1 1 S|_qff155*        |�������� ������ Q RS-��������  |F|
C |R_ome         |4 4 I|value_pos       |��� �������� ��������|0.5|
C |R_ope         |4 4 S|_sdelnv2dyn$83Dasha*|[���]���������� ��������� |0|
C |L_are         |1 1 S|_idelnv2dyn$83Dasha*|[TF]���������� ��������� �������� ������ |f|
C |R_ore         |4 4 S|_sdelnv2dyn$82Dasha*|[���]���������� ��������� |0|
C |L_ase         |1 1 S|_idelnv2dyn$82Dasha*|[TF]���������� ��������� �������� ������ |f|
C |L_ese         |1 1 S|_qffnv2dyn$96Dasha*|�������� ������ Q RS-��������  |F|
C |L_ise         |1 1 S|_qffnv2dyn$95Dasha*|�������� ������ Q RS-��������  |F|
C |L_eve         |1 1 I|vlv_kvit        |||
C |L_ove         |1 1 I|instr_fault     |||
C |L_uve         |1 1 S|_qffJ3417*      |�������� ������ Q RS-��������  |F|
C |L_ixe         |1 1 O|norm            |�����||
C |L_uxe         |1 1 S|_qffnv2dyn$106Dasha*|�������� ������ Q RS-��������  |F|
C |L_ubi         |1 1 S|_qffnv2dyn$104Dasha*|�������� ������ Q RS-��������  |F|
C |L_edi         |1 1 S|_qffnv2dyn$105Dasha*|�������� ������ Q RS-��������  |F|
C |L_idi         |1 1 O|flag_mlf19      |||
C |L_odi         |1 1 I|OUTC            |�����||
C |L_afi         |1 1 S|_qffnv2dyn$103Dasha*|�������� ������ Q RS-��������  |F|
C |L_ofi         |1 1 S|_qffnv2dyn$102Dasha*|�������� ������ Q RS-��������  |F|
C |L_eki         |1 1 S|_qffnv2dyn$101Dasha*|�������� ������ Q RS-��������  |F|
C |L_oki         |1 1 I|OUTO            |������||
C |R_omi         |4 4 O|mf_value        |��� ��������� �����|0.5|
C |L_epi         |1 1 S|_qffnv2dyn$87Dasha*|�������� ������ Q RS-��������  |F|
C |L_opi         |1 1 S|_qffnv2dyn$86Dasha*|�������� ������ Q RS-��������  |F|
C |L_ari         |1 1 O|limit_switch_error|||
C |C20_asi       |3 20 O|task_state      |���������||
C |L_esi         |1 1 I|mlf23           |������� ������� �����||
C |L_isi         |1 1 I|mlf22           |����� ����� ��������||
C |L_osi         |1 1 I|mlf19           |���� ������������||
C |L_usi         |1 1 I|mlf17           |������ ���������������� ����� �����||
C |L_ati         |1 1 I|mlf16           |������ ������������ ����� �����||
C |L_eti         |1 1 I|mlf15           |������ ���������������� ����� ������||
C |L_iti         |1 1 I|mlf14           |������ ������������ ����� ������||
C |L_oti         |1 1 I|mlf07           |���������� ���� �������||
C |L_uti         |1 1 I|mlf28           |������ ���������������� ��������� �����||
C |L_avi         |1 1 I|mlf09           |����� ��������� �����||
C |L_evi         |1 1 I|mlf08           |����� ��������� ������||
C |L_ivi         |1 1 I|mlf26           |������ ���������������� ��������� ������||
C |L_ovi         |1 1 I|mlf27           |������ ������������ ��������� �����||
C |L_uvi         |1 1 I|mlf25           |������ ������������ ��������� ������||
C |R_exi         |4 4 K|_lcmpJ3295      |[]�������� ������ �����������|0.001|
C |R_oxi         |4 4 K|_lcmpJ3291      |[]�������� ������ �����������|15299|
C |R_uxi         |4 4 K|_uintV_INT_     |����������� ������ ����������� ������|15300|
C |R_abo         |4 4 K|_tintV_INT_     |[���]�������� T �����������|1|
C |R_ebo         |4 4 O|_ointV_INT_*    |�������� ������ ����������� |15300|
C |R_ibo         |4 4 K|_lintV_INT_     |����������� ������ ����������� �����|0.0|
C |R_obo         |4 4 O|VX01            |��������� ������� �� ��� X||
C |R_odo         |4 4 O|VX02            |�������� ����������� �������||
C |I_uko         |2 4 K|_lcmpJ2719      |�������� ������ �����������|0|
C |I_alo         |2 4 I|N5              |����� ������� � ������ 5||
C |I_elo         |2 4 O|LS5             |����� ������� ������� � ������ 5||
C |I_amo         |2 4 K|_lcmpJ2708      |�������� ������ �����������|0|
C |I_emo         |2 4 I|N4              |����� ������� � ������ 4||
C |I_imo         |2 4 O|LS4             |����� ������� ������� � ������ 4||
C |I_epo         |2 4 K|_lcmpJ2697      |�������� ������ �����������|0|
C |I_ipo         |2 4 I|N3              |����� ������� � ������ 3||
C |I_opo         |2 4 O|LS3             |����� ������� ������� � ������ 3||
C |I_iro         |2 4 K|_lcmpJ2682      |�������� ������ �����������|0|
C |I_oro         |2 4 I|N2              |����� ������� � ������ 2||
C |I_uro         |2 4 O|LS2             |����� ������� ������� � ������ 2||
C |I_aso         |2 4 O|LS1             |����� ������� ������� � ������ 1||
C |I_uso         |2 4 K|_lcmpJ2670      |�������� ������ �����������|0|
C |I_ato         |2 4 I|N1              |����� ������� � ������ 1||
C |I_uto         |2 4 O|LREADY          |����� ����������||
C |I_ivo         |2 4 O|LBUSY           |����� �����||
C |R_ovo         |4 4 S|vmpos1_button_ST*|��������� ������ "������� � ��������� 1 �� ���������" |0.0|
C |R_uvo         |4 4 I|vmpos1_button   |������� ������ ������ "������� � ��������� 1 �� ���������" |0.0|
C |R_axo         |4 4 S|vmpos2_button_ST*|��������� ������ "������� � ��������� 2 �� ���������" |0.0|
C |R_exo         |4 4 I|vmpos2_button   |������� ������ ������ "������� � ��������� 2 �� ���������" |0.0|
C |I_ixo         |2 4 O|state1          |��������� 1||
C |I_abu         |2 4 O|state2          |��������� 2||
C |I_adu         |2 4 O|LPOS2O          |����� � ��������� 2||
C |I_odu         |2 4 O|LPOS1C          |����� � ��������� 1||
C |C20_ofu       |3 20 O|task_name       |������� ���������||
C |C8_oku        |3 8 I|task            |������� ���������||
C |R_epu         |4 4 S|vmstop_button_ST*|��������� ������ "������� ���� �� ���������" |0.0|
C |R_ipu         |4 4 I|vmstop_button   |������� ������ ������ "������� ���� �� ���������" |0.0|
C |L_opu         |1 1 O|vmstart_button_CMD*|[TF]����� ������ ������� �������� ��������� �� ���������|F|
C |R_upu         |4 4 S|vmstart_button_ST*|��������� ������ "������� �������� ��������� �� ���������" |0.0|
C |R_aru         |4 4 I|vmstart_button  |������� ������ ������ "������� �������� ��������� �� ���������" |0.0|
C |I_eru         |2 4 O|LERROR          |����� �������������||
C |L_uru         |1 1 I|YA27            |������� ������� �� ����������|F|
C |L_asu         |1 1 I|stop_avt        |������� ���� �� ����������|F|
C |L_esu         |1 1 O|vmstop_button_CMD*|[TF]����� ������ ������� ���� �� ���������|F|
C |I_osu         |2 4 O|LPOS1           |����� ��������� 1||
C |I_etu         |2 4 O|LPOS2           |����� ��������� 2||
C |I_utu         |2 4 O|LZM             |������ "�������"||
C |L_ovu         |1 1 O|vmpos1_button_CMD*|[TF]����� ������ ������� � ��������� 1 �� ���������|F|
C |L_axu         |1 1 O|vmpos2_button_CMD*|[TF]����� ������ ������� � ��������� 2 �� ���������|F|
C |L_ixu         |1 1 I|mlf06           |������������� ������� "�����"||
C |L_uxu         |1 1 I|mlf05           |������������� ������� "������"||
C |L_abad        |1 1 I|ulubopen        |���������� ������� �� ���������|F|
C |L_ebad        |1 1 I|ulubclose       |���������� ������� �� ���������|F|
C |L_idad        |1 1 O|block           |||
C |L_afad        |1 1 S|_qff10*         |�������� ������ Q RS-��������  |F|
C |L_ifad        |1 1 O|STOP            |�������||
C |L_ofad        |1 1 O|nopower         |��� ����������||
C |R8_akad       |4 8 I|voltage         |[��]���������� �� ������||
C |R_ukad        |4 4 I|power           |�������� ��������||
C |R8_ilad       |4 8 O|sn_power        |�������� ���� ��|3.0|
C |L_olad        |1 1 I|mlf04           |���������������� �������� �����||
C |L_ulad        |1 1 I|YA25            |������� ������� �� ����������|F|
C |L_emad        |1 1 I|mlf03           |���������������� �������� ������||
C |L_imad        |1 1 I|YA26            |������� ������� �� ����������|F|
C |L_umad        |1 1 O|fault           |�������������||
C |I_apad        |2 4 O|LAM             |������ "�������"||
C |L_opad        |1 1 O|XH53            |�� ����� (���)|F|
C |L_upad        |1 1 O|XH54            |�� ������ (���)|F|
C |L_urad        |1 1 S|_qff2*          |�������� ������ Q RS-��������  |F|
C |L_itad        |1 1 S|_qff1*          |�������� ������ Q RS-��������  |F|
C |L_ivad        |1 1 I|mlf24           |��� ���������� � ����������||

      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      INTEGER*4 I_e
      LOGICAL*1 L_i,L_o,L_u,L_ad,L_ed,L_id
      INTEGER*4 I_od
      REAL*4 R_ud,R0_af,R0_ef,R0_if,R0_of
      LOGICAL*1 L0_uf,L0_ak,L0_ek,L0_ik
      REAL*4 R0_ok,R0_uk,R_al,R_el,R_il,R_ol,R0_ul,R0_am,R0_em
     &,R0_im,R0_om,R0_um,R0_ap,R0_ep,R0_ip,R0_op
      LOGICAL*1 L0_up
      REAL*4 R0_ar,R0_er,R0_ir,R0_or,R0_ur
      LOGICAL*1 L0_as,L_es
      REAL*4 R0_is,R0_os,R0_us,R0_at,R0_et,R_it,R0_ot,R0_ut
     &,R0_av,R0_ev,R0_iv
      LOGICAL*1 L0_ov,L0_uv
      REAL*4 R0_ax
      LOGICAL*1 L0_ex,L0_ix,L0_ox,L0_ux,L0_abe,L0_ebe
      REAL*4 R0_ibe,R0_obe,R_ube
      LOGICAL*1 L_ade
      REAL*4 R0_ede,R0_ide,R_ode
      LOGICAL*1 L_ude,L0_afe,L0_efe,L0_ife,L0_ofe,L0_ufe,L_ake
     &,L0_eke,L0_ike,L_oke,L0_uke,L0_ale,L0_ele
      REAL*4 R0_ile,R0_ole
      LOGICAL*1 L0_ule,L0_ame
      REAL*4 R0_eme,R0_ime,R_ome
      LOGICAL*1 L0_ume,L0_ape
      REAL*4 R0_epe,R0_ipe,R_ope
      LOGICAL*1 L0_upe,L_are
      REAL*4 R0_ere,R0_ire,R_ore
      LOGICAL*1 L0_ure,L_ase,L_ese,L_ise,L0_ose,L0_use,L0_ate
     &,L0_ete,L0_ite,L0_ote,L0_ute,L0_ave,L_eve,L0_ive,L_ove
     &,L_uve,L0_axe,L0_exe,L_ixe,L0_oxe,L_uxe,L0_abi
      LOGICAL*1 L0_ebi,L0_ibi,L0_obi,L_ubi,L0_adi,L_edi,L_idi
     &,L_odi,L0_udi,L_afi,L0_efi,L0_ifi,L_ofi,L0_ufi,L0_aki
     &,L_eki,L0_iki,L_oki,L0_uki
      REAL*4 R0_ali,R0_eli,R0_ili
      LOGICAL*1 L0_oli
      REAL*4 R0_uli
      LOGICAL*1 L0_ami,L0_emi
      REAL*4 R_imi,R_omi
      LOGICAL*1 L0_umi,L0_api,L_epi,L0_ipi,L_opi,L0_upi,L_ari
      CHARACTER*20 C20_eri,C20_iri,C20_ori,C20_uri,C20_asi
      LOGICAL*1 L_esi,L_isi,L_osi,L_usi,L_ati,L_eti,L_iti
     &,L_oti,L_uti,L_avi,L_evi,L_ivi,L_ovi,L_uvi,L0_axi
      REAL*4 R_exi
      LOGICAL*1 L0_ixi
      REAL*4 R_oxi,R_uxi,R_abo,R_ebo,R_ibo,R_obo,R0_ubo
      LOGICAL*1 L0_ado,L0_edo
      REAL*4 R0_ido,R_odo,R0_udo,R0_afo,R0_efo,R0_ifo,R0_ofo
      LOGICAL*1 L0_ufo,L0_ako
      INTEGER*4 I0_eko,I0_iko
      LOGICAL*1 L0_oko
      INTEGER*4 I_uko,I_alo,I_elo,I0_ilo,I0_olo
      LOGICAL*1 L0_ulo
      INTEGER*4 I_amo,I_emo,I_imo,I0_omo,I0_umo
      LOGICAL*1 L0_apo
      INTEGER*4 I_epo,I_ipo,I_opo,I0_upo,I0_aro
      LOGICAL*1 L0_ero
      INTEGER*4 I_iro,I_oro,I_uro,I_aso,I0_eso,I0_iso
      LOGICAL*1 L0_oso
      INTEGER*4 I_uso,I_ato,I0_eto,I0_ito
      LOGICAL*1 L0_oto
      INTEGER*4 I_uto,I0_avo,I0_evo,I_ivo
      REAL*4 R_ovo,R_uvo,R_axo,R_exo
      INTEGER*4 I_ixo,I0_oxo,I0_uxo,I_abu,I0_ebu,I0_ibu,I0_obu
     &,I0_ubu,I_adu,I0_edu,I0_idu,I_odu
      CHARACTER*20 C20_udu,C20_afu,C20_efu,C20_ifu,C20_ofu
      CHARACTER*8 C8_ufu
      LOGICAL*1 L0_aku
      CHARACTER*8 C8_eku
      LOGICAL*1 L0_iku
      CHARACTER*8 C8_oku
      LOGICAL*1 L0_uku
      INTEGER*4 I0_alu
      LOGICAL*1 L0_elu
      INTEGER*4 I0_ilu
      LOGICAL*1 L0_olu
      INTEGER*4 I0_ulu,I0_amu,I0_emu
      LOGICAL*1 L0_imu
      INTEGER*4 I0_omu,I0_umu,I0_apu
      REAL*4 R_epu,R_ipu
      LOGICAL*1 L_opu
      REAL*4 R_upu,R_aru
      INTEGER*4 I_eru,I0_iru,I0_oru
      LOGICAL*1 L_uru,L_asu,L_esu,L0_isu
      INTEGER*4 I_osu,I0_usu,I0_atu,I_etu,I0_itu,I0_otu,I_utu
     &,I0_avu,I0_evu
      LOGICAL*1 L0_ivu,L_ovu,L0_uvu,L_axu,L0_exu,L_ixu,L0_oxu
     &,L_uxu,L_abad,L_ebad,L0_ibad,L0_obad,L0_ubad,L0_adad
     &,L0_edad,L_idad,L0_odad,L0_udad,L_afad,L0_efad,L_ifad
     &,L_ofad
      REAL*4 R0_ufad
      REAL*8 R8_akad
      LOGICAL*1 L0_ekad,L0_ikad
      REAL*4 R0_okad,R_ukad,R0_alad,R0_elad
      REAL*8 R8_ilad
      LOGICAL*1 L_olad,L_ulad,L0_amad,L_emad,L_imad,L0_omad
     &,L_umad
      INTEGER*4 I_apad,I0_epad,I0_ipad
      LOGICAL*1 L_opad,L_upad,L0_arad,L0_erad,L0_irad,L0_orad
     &,L_urad,L0_asad,L0_esad,L0_isad,L0_osad,L0_usad,L0_atad
     &,L0_etad,L_itad,L0_otad,L0_utad,L0_avad,L0_evad,L_ivad

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      R0_im=R_el
C TELEGKA5_HANDLER.fmg( 394, 406):pre: ����������� ��
      R0_um=R_al
C TELEGKA5_HANDLER.fmg( 386, 412):pre: ����������� ��
      R0_us=R_il
C TELEGKA5_HANDLER.fmg( 349, 414):pre: ����������� ��
      R0_or=R_ol
C TELEGKA5_HANDLER.fmg( 319, 403):pre: ����������� ��
      R0_ev=R_ud
C TELEGKA5_HANDLER.fmg( 384, 461):pre: ����������� ��
      R0_ide=R_ode
C TELEGKA5_HANDLER.fmg( 249, 347):pre: �������� ��������� ������,nv2dyn$99Dasha
      R0_obe=R_ube
C TELEGKA5_HANDLER.fmg( 249, 335):pre: �������� ��������� ������,nv2dyn$100Dasha
      R0_ire=R_ore
C TELEGKA5_HANDLER.fmg( 234, 311):pre: �������� ��������� ������,nv2dyn$82Dasha
      R0_ipe=R_ope
C TELEGKA5_HANDLER.fmg( 234, 298):pre: �������� ��������� ������,nv2dyn$83Dasha
      L_id=L_u.and..not.L_i
      L_i=L_u
C TELEGKA5_HANDLER.fmg( 387, 286):������������  �� 1 ���
      L_ed=L_ad.and..not.L_o
      L_o=L_ad
C TELEGKA5_HANDLER.fmg( 387, 296):������������  �� 1 ���
      if (.not.L_id.and.L_ed) then
          I_od = I_od+1
      endif
      if (L_id) then
          I_od = 0
      endif
C TELEGKA5_HANDLER.fmg( 398, 286):�������,COUNTER
      I_e=I_od
C TELEGKA5_HANDLER.fmg( 415, 276):������,NUM_BOAT
      R0_ok = 15300
C TELEGKA5_HANDLER.fmg( 382, 439):��������� (RE4) (�������)
      R0_uk = R_it * R0_ok
C TELEGKA5_HANDLER.fmg( 387, 442):����������
      R0_af = 1.0
C TELEGKA5_HANDLER.fmg( 394, 424):��������� (RE4) (�������)
      R0_if = R0_uk + (-R0_af)
C TELEGKA5_HANDLER.fmg( 398, 425):��������
      R0_ef = 1.0
C TELEGKA5_HANDLER.fmg( 394, 429):��������� (RE4) (�������)
      R0_of = R0_uk + R0_ef
C TELEGKA5_HANDLER.fmg( 398, 430):��������
      R0_ul = 0.0
C TELEGKA5_HANDLER.fmg( 428, 414):��������� (RE4) (�������),nv2dyn$57Dasha
      R0_em = 0.99
C TELEGKA5_HANDLER.fmg( 410, 416):��������� (RE4) (�������),nv2dyn$60Dasha
      if(.NOT.L_esi) then
         R0_op=R0_im
      endif
C TELEGKA5_HANDLER.fmg( 398, 412):���� � ������������� �������
      R0_ap = 1.0
C TELEGKA5_HANDLER.fmg( 363, 412):��������� (RE4) (�������)
      R0_ep = 0.0
C TELEGKA5_HANDLER.fmg( 355, 412):��������� (RE4) (�������)
      R0_er = 1.0e-10
C TELEGKA5_HANDLER.fmg( 333, 390):��������� (RE4) (�������)
      R0_is = 0.0
C TELEGKA5_HANDLER.fmg( 343, 404):��������� (RE4) (�������)
      R0_ur = R0_or + (-R_it)
C TELEGKA5_HANDLER.fmg( 322, 396):��������
      R0_ot = 0.33
C TELEGKA5_HANDLER.fmg( 324, 475):��������� (RE4) (�������),nv2dyn$59Dasha
      R0_iv = 0.000001
C TELEGKA5_HANDLER.fmg( 138, 412):��������� (RE4) (�������),nv2dyn$61Dasha
      L0_ov=R_ome.lt.R0_iv
C TELEGKA5_HANDLER.fmg( 145, 414):���������� <,nv2dyn$46Dasha
      L0_uv = L_uti.AND.L0_ov
C TELEGKA5_HANDLER.fmg( 152, 418):�
      R0_ax = 0.999999
C TELEGKA5_HANDLER.fmg( 133, 460):��������� (RE4) (�������),nv2dyn$60Dasha
      L0_ex=R_ome.gt.R0_ax
C TELEGKA5_HANDLER.fmg( 142, 462):���������� >,nv2dyn$45Dasha
      L0_ix = L_ivi.AND.L0_ex
C TELEGKA5_HANDLER.fmg( 152, 468):�
      R0_epe = 20
C TELEGKA5_HANDLER.fmg( 226, 301):��������� (RE4) (�������)
      if(.not.L_oti) then
         R_ope=0.0
      elseif(.not.L_are) then
         R_ope=R0_epe
      else
         R_ope=max(R0_ipe-deltat,0.0)
      endif
      L0_upe=L_oti.and.R_ope.le.0.0
      L_are=L_oti
C TELEGKA5_HANDLER.fmg( 234, 298):�������� ��������� ������,nv2dyn$83Dasha
      R0_ere = 20
C TELEGKA5_HANDLER.fmg( 226, 314):��������� (RE4) (�������)
      if(.not.L_oti) then
         R_ore=0.0
      elseif(.not.L_ase) then
         R_ore=R0_ere
      else
         R_ore=max(R0_ire-deltat,0.0)
      endif
      L0_ure=L_oti.and.R_ore.le.0.0
      L_ase=L_oti
C TELEGKA5_HANDLER.fmg( 234, 311):�������� ��������� ������,nv2dyn$82Dasha
      R0_ibe = 20
C TELEGKA5_HANDLER.fmg( 243, 338):��������� (RE4) (�������)
      R0_ede = 20
C TELEGKA5_HANDLER.fmg( 244, 352):��������� (RE4) (�������)
      R0_ile = 0.000001
C TELEGKA5_HANDLER.fmg( 206, 259):��������� (RE4) (�������),nv2dyn$61Dasha
      R0_ole = 0.999999
C TELEGKA5_HANDLER.fmg( 206, 265):��������� (RE4) (�������),nv2dyn$60Dasha
      R0_eme = 0.000001
C TELEGKA5_HANDLER.fmg( 206, 276):��������� (RE4) (�������),nv2dyn$61Dasha
      L0_ume=R_ome.lt.R0_eme
C TELEGKA5_HANDLER.fmg( 212, 278):���������� <,nv2dyn$46Dasha
      L0_ate = L_uti.AND.L0_ume
C TELEGKA5_HANDLER.fmg( 243, 276):�
      R0_ime = 0.999999
C TELEGKA5_HANDLER.fmg( 206, 282):��������� (RE4) (�������),nv2dyn$60Dasha
      L0_ape=R_ome.gt.R0_ime
C TELEGKA5_HANDLER.fmg( 210, 284):���������� >,nv2dyn$45Dasha
      L0_ete = L_ivi.AND.L0_ape
C TELEGKA5_HANDLER.fmg( 243, 287):�
      L_uve=(L_ove.or.L_uve).and..not.(L_eve)
      L0_ive=.not.L_uve
C TELEGKA5_HANDLER.fmg( 285, 275):RS �������
      R0_ali = 0.01
C TELEGKA5_HANDLER.fmg(  26, 282):��������� (RE4) (�������),nv2dyn$74Dasha
      R0_eli = R0_ali + R_imi
C TELEGKA5_HANDLER.fmg(  29, 282):��������
      R0_ili = 0.01
C TELEGKA5_HANDLER.fmg(  26, 290):��������� (RE4) (�������),nv2dyn$74Dasha
      R0_uli = (-R0_ili) + R_imi
C TELEGKA5_HANDLER.fmg(  29, 289):��������
      C20_ori = '��������� 2'
C TELEGKA5_HANDLER.fmg(  17, 309):��������� ���������� CH20 (CH30) (�������)
      C20_uri = '������� ���'
C TELEGKA5_HANDLER.fmg(  17, 311):��������� ���������� CH20 (CH30) (�������)
      C20_eri = '��������� 1'
C TELEGKA5_HANDLER.fmg(  32, 308):��������� ���������� CH20 (CH30) (�������)
      R0_ubo = 20
C TELEGKA5_HANDLER.fmg( 322, 479):��������� (RE4) (�������)
      R0_ut = R0_ubo * R0_ot
C TELEGKA5_HANDLER.fmg( 330, 476):����������
      if(L_oti) then
         R0_udo=R0_ut
      else
         R0_udo=R0_ubo
      endif
C TELEGKA5_HANDLER.fmg( 336, 478):���� RE IN LO CH7,nv2dyn$40Dasha
      R0_ido = 0.0
C TELEGKA5_HANDLER.fmg( 358, 469):��������� (RE4) (�������)
      R0_ofo = 0.0
C TELEGKA5_HANDLER.fmg( 344, 469):��������� (RE4) (�������)
      I0_iko = z'01000004'
C TELEGKA5_HANDLER.fmg( 194, 468):��������� ������������� IN (�������)
      I0_eko = z'0100000B'
C TELEGKA5_HANDLER.fmg( 194, 466):��������� ������������� IN (�������)
      L0_oko=I_alo.gt.I_uko
C TELEGKA5_HANDLER.fmg( 195, 457):���������� �������������
      if(L0_oko) then
         I_elo=I0_eko
      else
         I_elo=I0_iko
      endif
C TELEGKA5_HANDLER.fmg( 198, 466):���� RE IN LO CH7
      I0_olo = z'01000004'
C TELEGKA5_HANDLER.fmg( 162, 486):��������� ������������� IN (�������)
      I0_ilo = z'0100000B'
C TELEGKA5_HANDLER.fmg( 162, 484):��������� ������������� IN (�������)
      L0_ulo=I_emo.gt.I_amo
C TELEGKA5_HANDLER.fmg( 162, 474):���������� �������������
      if(L0_ulo) then
         I_imo=I0_ilo
      else
         I_imo=I0_olo
      endif
C TELEGKA5_HANDLER.fmg( 166, 484):���� RE IN LO CH7
      I0_umo = z'01000004'
C TELEGKA5_HANDLER.fmg( 118, 486):��������� ������������� IN (�������)
      I0_omo = z'0100000B'
C TELEGKA5_HANDLER.fmg( 118, 484):��������� ������������� IN (�������)
      L0_apo=I_ipo.gt.I_epo
C TELEGKA5_HANDLER.fmg( 117, 474):���������� �������������
      if(L0_apo) then
         I_opo=I0_omo
      else
         I_opo=I0_umo
      endif
C TELEGKA5_HANDLER.fmg( 122, 484):���� RE IN LO CH7
      I0_aro = z'01000004'
C TELEGKA5_HANDLER.fmg(  74, 486):��������� ������������� IN (�������)
      I0_upo = z'0100000B'
C TELEGKA5_HANDLER.fmg(  74, 484):��������� ������������� IN (�������)
      L0_ero=I_oro.gt.I_iro
C TELEGKA5_HANDLER.fmg(  73, 474):���������� �������������
      if(L0_ero) then
         I_uro=I0_upo
      else
         I_uro=I0_aro
      endif
C TELEGKA5_HANDLER.fmg(  78, 484):���� RE IN LO CH7
      I0_iso = z'01000004'
C TELEGKA5_HANDLER.fmg(  30, 486):��������� ������������� IN (�������)
      I0_eso = z'0100000B'
C TELEGKA5_HANDLER.fmg(  30, 484):��������� ������������� IN (�������)
      L0_oso=I_ato.gt.I_uso
C TELEGKA5_HANDLER.fmg(  30, 474):���������� �������������
      if(L0_oso) then
         I_aso=I0_eso
      else
         I_aso=I0_iso
      endif
C TELEGKA5_HANDLER.fmg(  34, 484):���� RE IN LO CH7
      I0_eto = z'0100000A'
C TELEGKA5_HANDLER.fmg( 236, 480):��������� ������������� IN (�������)
      I0_ito = z'01000003'
C TELEGKA5_HANDLER.fmg( 236, 482):��������� ������������� IN (�������)
      I0_evo = z'0100000A'
C TELEGKA5_HANDLER.fmg( 272, 482):��������� ������������� IN (�������)
      I0_avo = z'01000003'
C TELEGKA5_HANDLER.fmg( 272, 480):��������� ������������� IN (�������)
      L_ovu=R_uvo.ne.R_ovo
      R_ovo=R_uvo
C TELEGKA5_HANDLER.fmg(  16, 384):���������� ������������� ������
      L_axu=R_exo.ne.R_axo
      R_axo=R_exo
C TELEGKA5_HANDLER.fmg(  20, 439):���������� ������������� ������
      I0_uxo = z'01000003'
C TELEGKA5_HANDLER.fmg( 140, 352):��������� ������������� IN (�������)
      I0_oxo = z'01000010'
C TELEGKA5_HANDLER.fmg( 140, 350):��������� ������������� IN (�������)
      I0_ibu = z'01000003'
C TELEGKA5_HANDLER.fmg( 118, 442):��������� ������������� IN (�������)
      I0_ebu = z'01000010'
C TELEGKA5_HANDLER.fmg( 118, 440):��������� ������������� IN (�������)
      I0_ubu = z'01000003'
C TELEGKA5_HANDLER.fmg( 186, 419):��������� ������������� IN (�������)
      I0_obu = z'0100000A'
C TELEGKA5_HANDLER.fmg( 186, 417):��������� ������������� IN (�������)
      I0_idu = z'01000003'
C TELEGKA5_HANDLER.fmg( 194, 369):��������� ������������� IN (�������)
      I0_edu = z'0100000A'
C TELEGKA5_HANDLER.fmg( 194, 367):��������� ������������� IN (�������)
      I0_atu = z'01000003'
C TELEGKA5_HANDLER.fmg( 116, 366):��������� ������������� IN (�������)
      I0_usu = z'0100000A'
C TELEGKA5_HANDLER.fmg( 116, 364):��������� ������������� IN (�������)
      C20_udu = '� ��������� 1'
C TELEGKA5_HANDLER.fmg(  52, 361):��������� ���������� CH20 (CH30) (�������)
      C20_efu = '� ��������� 2'
C TELEGKA5_HANDLER.fmg(  36, 362):��������� ���������� CH20 (CH30) (�������)
      C20_ifu = ''
C TELEGKA5_HANDLER.fmg(  36, 364):��������� ���������� CH20 (CH30) (�������)
      C8_ufu = 'pos1'
C TELEGKA5_HANDLER.fmg(  19, 370):��������� ���������� CH8 (�������)
      call chcomp(C8_oku,C8_ufu,L0_aku)
C TELEGKA5_HANDLER.fmg(  24, 374):���������� ���������
      C8_eku = 'pos2'
C TELEGKA5_HANDLER.fmg(  19, 412):��������� ���������� CH8 (�������)
      call chcomp(C8_oku,C8_eku,L0_iku)
C TELEGKA5_HANDLER.fmg(  24, 416):���������� ���������
      if(L0_iku) then
         C20_afu=C20_efu
      else
         C20_afu=C20_ifu
      endif
C TELEGKA5_HANDLER.fmg(  40, 362):���� RE IN LO CH20
      if(L0_aku) then
         C20_ofu=C20_udu
      else
         C20_ofu=C20_afu
      endif
C TELEGKA5_HANDLER.fmg(  56, 362):���� RE IN LO CH20
      I0_alu = z'0100008E'
C TELEGKA5_HANDLER.fmg( 170, 387):��������� ������������� IN (�������)
      I0_ilu = z'01000086'
C TELEGKA5_HANDLER.fmg( 166, 442):��������� ������������� IN (�������)
      I0_amu = z'01000003'
C TELEGKA5_HANDLER.fmg( 153, 452):��������� ������������� IN (�������)
      I0_emu = z'01000010'
C TELEGKA5_HANDLER.fmg( 153, 454):��������� ������������� IN (�������)
      I0_apu = z'01000003'
C TELEGKA5_HANDLER.fmg( 160, 399):��������� ������������� IN (�������)
      I0_umu = z'01000010'
C TELEGKA5_HANDLER.fmg( 160, 397):��������� ������������� IN (�������)
      L_esu=R_ipu.ne.R_epu
      R_epu=R_ipu
C TELEGKA5_HANDLER.fmg(  16, 402):���������� ������������� ������
      L0_isu = L_esu.OR.L_asu.OR.L_uru
C TELEGKA5_HANDLER.fmg(  55, 400):���
      L_opu=R_aru.ne.R_upu
      R_upu=R_aru
C TELEGKA5_HANDLER.fmg(  20, 427):���������� ������������� ������
      L0_exu = L_opu.AND.L0_iku
C TELEGKA5_HANDLER.fmg(  32, 426):�
      L0_ibad = L0_exu.OR.L_axu
C TELEGKA5_HANDLER.fmg(  52, 424):���
      L0_ele = L_uxu.AND.L0_ibad
C TELEGKA5_HANDLER.fmg( 228, 347):�
      L0_ufe = L0_ibad.OR.(.NOT.L_ixu)
C TELEGKA5_HANDLER.fmg( 234, 331):���
      L0_uvu = L_opu.AND.L0_aku
C TELEGKA5_HANDLER.fmg(  30, 382):�
      L0_oxu = L0_uvu.OR.L_ovu
C TELEGKA5_HANDLER.fmg(  54, 381):���
      L0_ale = L_ixu.AND.L0_oxu
C TELEGKA5_HANDLER.fmg( 228, 335):�
      L_ake=(L0_ale.or.L_ake).and..not.(L0_ufe)
      L0_eke=.not.L_ake
C TELEGKA5_HANDLER.fmg( 240, 333):RS �������,156
      if(.not.L_ake) then
         R_ube=0.0
      elseif(.not.L_ade) then
         R_ube=R0_ibe
      else
         R_ube=max(R0_obe-deltat,0.0)
      endif
      L0_afe=L_ake.and.R_ube.le.0.0
      L_ade=L_ake
C TELEGKA5_HANDLER.fmg( 249, 335):�������� ��������� ������,nv2dyn$100Dasha
      L_ese=(L0_afe.or.L_ese).and..not.(L_oki)
      L0_efe=.not.L_ese
C TELEGKA5_HANDLER.fmg( 266, 333):RS �������,nv2dyn$96Dasha
      L0_ike = (.NOT.L_uxu).OR.L0_oxu
C TELEGKA5_HANDLER.fmg( 234, 343):���
      L_oke=(L0_ele.or.L_oke).and..not.(L0_ike)
      L0_uke=.not.L_oke
C TELEGKA5_HANDLER.fmg( 240, 345):RS �������,155
      if(.not.L_oke) then
         R_ode=0.0
      elseif(.not.L_ude) then
         R_ode=R0_ede
      else
         R_ode=max(R0_ide-deltat,0.0)
      endif
      L0_ife=L_oke.and.R_ode.le.0.0
      L_ude=L_oke
C TELEGKA5_HANDLER.fmg( 249, 347):�������� ��������� ������,nv2dyn$99Dasha
      L_ise=(L0_ife.or.L_ise).and..not.(L_odi)
      L0_ofe=.not.L_ise
C TELEGKA5_HANDLER.fmg( 266, 345):RS �������,nv2dyn$95Dasha
      I0_avu = z'0100000E'
C TELEGKA5_HANDLER.fmg( 190, 390):��������� ������������� IN (�������)
      I0_iru = z'01000007'
C TELEGKA5_HANDLER.fmg( 146, 372):��������� ������������� IN (�������)
      I0_oru = z'01000003'
C TELEGKA5_HANDLER.fmg( 146, 374):��������� ������������� IN (�������)
      I0_otu = z'01000003'
C TELEGKA5_HANDLER.fmg( 123, 455):��������� ������������� IN (�������)
      I0_itu = z'0100000A'
C TELEGKA5_HANDLER.fmg( 123, 453):��������� ������������� IN (�������)
      I0_epad = z'0100000E'
C TELEGKA5_HANDLER.fmg( 184, 445):��������� ������������� IN (�������)
      L0_edad=.false.
C TELEGKA5_HANDLER.fmg(  66, 405):��������� ���������� (�������)
      L0_adad=.false.
C TELEGKA5_HANDLER.fmg(  66, 403):��������� ���������� (�������)
      R0_ufad = 0.1
C TELEGKA5_HANDLER.fmg( 252, 379):��������� (RE4) (�������)
      L_ofad=R8_akad.lt.R0_ufad
C TELEGKA5_HANDLER.fmg( 257, 380):���������� <
      R0_okad = 0.0
C TELEGKA5_HANDLER.fmg( 264, 400):��������� (RE4) (�������)
      L0_efi = (.NOT.L_osi).AND.L_afi
C TELEGKA5_HANDLER.fmg( 100, 278):�
C label 267  try267=try267-1
      L0_ifi = L_odi.AND.L0_efi
C TELEGKA5_HANDLER.fmg( 104, 279):�
      L_afi=(L_osi.or.L_afi).and..not.(L0_ifi)
      L0_udi=.not.L_afi
C TELEGKA5_HANDLER.fmg(  96, 275):RS �������,nv2dyn$103Dasha
C sav1=L0_efi
      L0_efi = (.NOT.L_osi).AND.L_afi
C TELEGKA5_HANDLER.fmg( 100, 278):recalc:�
C if(sav1.ne.L0_efi .and. try267.gt.0) goto 267
      L0_abi = (.NOT.L_osi).AND.L_uxe
C TELEGKA5_HANDLER.fmg( 105, 264):�
C label 271  try271=try271-1
      L0_ebi = L_oki.AND.L0_abi
C TELEGKA5_HANDLER.fmg( 109, 265):�
      L_uxe=(L_osi.or.L_uxe).and..not.(L0_ebi)
      L0_oxe=.not.L_uxe
C TELEGKA5_HANDLER.fmg( 100, 261):RS �������,nv2dyn$106Dasha
C sav1=L0_abi
      L0_abi = (.NOT.L_osi).AND.L_uxe
C TELEGKA5_HANDLER.fmg( 105, 264):recalc:�
C if(sav1.ne.L0_abi .and. try271.gt.0) goto 271
      L0_ux = L_upad.AND.L_ati
C TELEGKA5_HANDLER.fmg(  28, 352):�
C label 276  try276=try276-1
      L0_axi=R_obo.lt.R_exi
C TELEGKA5_HANDLER.fmg( 404, 446):���������� <
      L0_abe = L0_axi.AND.(.NOT.L_uti)
C TELEGKA5_HANDLER.fmg( 440, 444):�
      L_opad = L_avi.OR.L0_abe.OR.L_ovi
C TELEGKA5_HANDLER.fmg( 444, 444):���
      L0_ave = L_evi.AND.L_opad
C TELEGKA5_HANDLER.fmg( 250, 322):�
      L0_ute = L_avi.AND.L_upad
C TELEGKA5_HANDLER.fmg( 250, 317):�
      L0_ote = L0_ure.AND.(.NOT.L_upad)
C TELEGKA5_HANDLER.fmg( 242, 310):�
      L0_ite = L0_upe.AND.(.NOT.L_opad)
C TELEGKA5_HANDLER.fmg( 240, 296):�
      R0_ar = R_odo + R0_ur
C TELEGKA5_HANDLER.fmg( 328, 397):��������
      R0_ir = R0_ar * R0_ur
C TELEGKA5_HANDLER.fmg( 332, 396):����������
      L0_as=R0_ir.lt.R0_er
C TELEGKA5_HANDLER.fmg( 338, 395):���������� <
      L_es=(L0_as.or.L_es).and..not.(.NOT.L_osi)
      L0_up=.not.L_es
C TELEGKA5_HANDLER.fmg( 344, 393):RS �������
      if(L_es) then
         R0_os=R0_is
      else
         R0_os=R_odo
      endif
C TELEGKA5_HANDLER.fmg( 346, 404):���� RE IN LO CH7
      R0_at = R0_us + R0_os
C TELEGKA5_HANDLER.fmg( 352, 406):��������
      R0_ip=MAX(R0_ep,R0_at)
C TELEGKA5_HANDLER.fmg( 360, 407):��������
      R0_et=MIN(R0_ap,R0_ip)
C TELEGKA5_HANDLER.fmg( 368, 408):�������
      if(L_esi) then
         R0_op=R0_et
      endif
C TELEGKA5_HANDLER.fmg( 380, 412):���� � ������������� �������
      if(L_es) then
         R0_op=R_it
      endif
C TELEGKA5_HANDLER.fmg( 338, 412):���� � ������������� �������
      if(L_iti) then
         R0_am=R0_em
      else
         R0_am=R0_op
      endif
C TELEGKA5_HANDLER.fmg( 414, 416):���� RE IN LO CH7,nv2dyn$56Dasha
      if(L_ati) then
         R_omi=R0_ul
      else
         R_omi=R0_am
      endif
C TELEGKA5_HANDLER.fmg( 432, 416):���� RE IN LO CH7,nv2dyn$56Dasha
      L0_oli=R_omi.gt.R0_uli
C TELEGKA5_HANDLER.fmg(  34, 293):���������� >,nv2dyn$84Dasha
      L0_uki=R_omi.lt.R0_eli
C TELEGKA5_HANDLER.fmg(  36, 287):���������� <,nv2dyn$85Dasha
      L0_ami = L0_oli.AND.L0_uki
C TELEGKA5_HANDLER.fmg(  41, 292):�
      L0_emi = L0_ami.AND.L_osi
C TELEGKA5_HANDLER.fmg(  53, 284):�
      L_eki=(L_oki.or.L_eki).and..not.(L_opad)
      L0_iki=.not.L_eki
C TELEGKA5_HANDLER.fmg(  54, 277):RS �������,nv2dyn$101Dasha
      L0_aki = L0_emi.AND.L_eki
C TELEGKA5_HANDLER.fmg(  72, 283):�
      L_ofi=(L0_aki.or.L_ofi).and..not.(L0_ifi)
      L0_ufi=.not.L_ofi
C TELEGKA5_HANDLER.fmg( 110, 281):RS �������,nv2dyn$102Dasha
      L_ubi=(L_odi.or.L_ubi).and..not.(L_upad)
      L0_obi=.not.L_ubi
C TELEGKA5_HANDLER.fmg(  58, 266):RS �������,nv2dyn$104Dasha
      L0_adi = L0_emi.AND.L_ubi
C TELEGKA5_HANDLER.fmg(  72, 269):�
      L_edi=(L0_adi.or.L_edi).and..not.(L0_ebi)
      L0_ibi=.not.L_edi
C TELEGKA5_HANDLER.fmg( 118, 267):RS �������,nv2dyn$105Dasha
      L_idi = L_ofi.OR.L_edi
C TELEGKA5_HANDLER.fmg( 131, 282):���
      L_opi=(L_upad.or.L_opi).and..not.(L_opad)
      L0_api=.not.L_opi
C TELEGKA5_HANDLER.fmg( 106, 305):RS �������,nv2dyn$86Dasha
      L0_upi = L_uvi.AND.(.NOT.L_opi)
C TELEGKA5_HANDLER.fmg( 116, 308):�
      L_epi=(L_opad.or.L_epi).and..not.(L_upad)
      L0_umi=.not.L_epi
C TELEGKA5_HANDLER.fmg( 105, 294):RS �������,nv2dyn$87Dasha
      L0_ipi = L_ovi.AND.(.NOT.L_epi)
C TELEGKA5_HANDLER.fmg( 114, 299):�
      L_ari = L0_upi.OR.L0_ipi
C TELEGKA5_HANDLER.fmg( 122, 307):���
      L0_ame=R_omi.gt.R0_ole
C TELEGKA5_HANDLER.fmg( 210, 266):���������� >,nv2dyn$45Dasha
      L0_use = L_eti.AND.L0_ame
C TELEGKA5_HANDLER.fmg( 243, 270):�
      L0_ule=R_omi.lt.R0_ile
C TELEGKA5_HANDLER.fmg( 212, 260):���������� <,nv2dyn$46Dasha
      L0_ose = L_usi.AND.L0_ule
C TELEGKA5_HANDLER.fmg( 243, 258):�
      L0_axe =.NOT.(L_emad.OR.L_olad.OR.L0_ave.OR.L0_ute.OR.L0_ote.OR.L0
     &_ite.OR.L0_ete.OR.L0_ate.OR.L_esi.OR.L_iti.OR.L_ati.OR.L_idi.OR.L_
     &ari.OR.L0_use.OR.L0_ose.OR.L_ise.OR.L_ese)
C TELEGKA5_HANDLER.fmg( 277, 310):���
      L0_exe =.NOT.(L0_axe)
C TELEGKA5_HANDLER.fmg( 288, 282):���
      L_umad = L0_exe.OR.L_uve
C TELEGKA5_HANDLER.fmg( 292, 281):���
      L0_ubad = L_umad.OR.L_abad
C TELEGKA5_HANDLER.fmg(  58, 436):���
      L0_omad = L0_ibad.AND.(.NOT.L0_ubad).AND.(.NOT.L_uxu
     &)
C TELEGKA5_HANDLER.fmg(  68, 430):�
      L0_utad = L0_omad.OR.L_imad.OR.L_emad
C TELEGKA5_HANDLER.fmg(  72, 424):���
      L0_obad = L_umad.OR.L_ebad
C TELEGKA5_HANDLER.fmg(  56, 391):���
      L0_amad = (.NOT.L0_obad).AND.L0_oxu.AND.(.NOT.L_ixu
     &)
C TELEGKA5_HANDLER.fmg(  68, 386):�
      L0_isad = L0_amad.OR.L_ulad.OR.L_olad
C TELEGKA5_HANDLER.fmg(  72, 382):���
      L0_udad = L0_utad.OR.L0_isad
C TELEGKA5_HANDLER.fmg(  76, 393):���
      L0_odad = L_opad.OR.L0_isu.OR.L_upad
C TELEGKA5_HANDLER.fmg(  62, 397):���
      L_afad=(L0_odad.or.L_afad).and..not.(L0_udad)
      L0_efad=.not.L_afad
C TELEGKA5_HANDLER.fmg( 107, 395):RS �������,10
      L_ifad = L_asu.OR.L_afad
C TELEGKA5_HANDLER.fmg( 118, 398):���
      L0_avad = L_upad.AND.(.NOT.L_evi)
C TELEGKA5_HANDLER.fmg(  74, 446):�
      L0_usad = (.NOT.L_ifad).AND.L0_avad
C TELEGKA5_HANDLER.fmg( 106, 447):�
      L0_arad = L_ifad.OR.L_ivad
C TELEGKA5_HANDLER.fmg(  98, 404):���
      L0_ox = L_opad.AND.L_iti
C TELEGKA5_HANDLER.fmg(  28, 340):�
      L0_ivu = L0_ux.OR.L0_ox
C TELEGKA5_HANDLER.fmg(  36, 350):���
      L0_etad = L0_avad.OR.L0_arad.OR.L0_isad.OR.L0_ivu
C TELEGKA5_HANDLER.fmg( 104, 418):���
      L0_evad = (.NOT.L0_avad).AND.L0_utad
C TELEGKA5_HANDLER.fmg( 104, 425):�
      L_itad=(L0_evad.or.L_itad).and..not.(L0_etad)
      L0_otad=.not.L_itad
C TELEGKA5_HANDLER.fmg( 112, 423):RS �������,1
      L0_atad = (.NOT.L0_usad).AND.L_itad
C TELEGKA5_HANDLER.fmg( 130, 426):�
      L0_edo = L0_atad.AND.(.NOT.L_olad)
C TELEGKA5_HANDLER.fmg( 314, 468):�
      L0_ako = L0_edo.OR.L_emad
C TELEGKA5_HANDLER.fmg( 326, 466):���
      if(L0_ako) then
         R0_efo=R0_udo
      else
         R0_efo=R0_ofo
      endif
C TELEGKA5_HANDLER.fmg( 348, 479):���� RE IN LO CH7
      L0_esad = L_opad.AND.(.NOT.L_avi)
C TELEGKA5_HANDLER.fmg(  94, 353):�
      L0_orad = L0_arad.OR.L0_esad.OR.L0_utad.OR.L0_ivu
C TELEGKA5_HANDLER.fmg( 104, 374):���
      L0_osad = L0_isad.AND.(.NOT.L0_esad)
C TELEGKA5_HANDLER.fmg( 104, 381):�
      L_urad=(L0_osad.or.L_urad).and..not.(L0_orad)
      L0_asad=.not.L_urad
C TELEGKA5_HANDLER.fmg( 112, 379):RS �������,2
      L0_erad = (.NOT.L_ifad).AND.L0_esad
C TELEGKA5_HANDLER.fmg( 104, 354):�
      L0_irad = L_urad.AND.(.NOT.L0_erad)
C TELEGKA5_HANDLER.fmg( 130, 380):�
      L0_ado = L0_irad.AND.(.NOT.L_emad)
C TELEGKA5_HANDLER.fmg( 314, 454):�
      L0_ufo = L0_ado.OR.L_olad
C TELEGKA5_HANDLER.fmg( 326, 452):���
      if(L0_ufo) then
         R0_afo=R0_udo
      else
         R0_afo=R0_ofo
      endif
C TELEGKA5_HANDLER.fmg( 348, 461):���� RE IN LO CH7
      R0_ifo = R0_efo + (-R0_afo)
C TELEGKA5_HANDLER.fmg( 354, 472):��������
      if(L_ivad) then
         R_odo=R0_ido
      else
         R_odo=R0_ifo
      endif
C TELEGKA5_HANDLER.fmg( 362, 470):���� RE IN LO CH7
C sav1=R0_ar
      R0_ar = R_odo + R0_ur
C TELEGKA5_HANDLER.fmg( 328, 397):recalc:��������
C if(sav1.ne.R0_ar .and. try314.gt.0) goto 314
      R_ebo=R_ebo+deltat/R_abo*R_odo
      if(R_ebo.gt.R_uxi) then
         R_ebo=R_uxi
      elseif(R_ebo.lt.R_ibo) then
         R_ebo=R_ibo
      endif
C TELEGKA5_HANDLER.fmg( 376, 456):����������,V_INT_
      if(L_esi) then
         R0_av=R0_ev
      else
         R0_av=R_ebo
      endif
C TELEGKA5_HANDLER.fmg( 384, 456):���� RE IN LO CH7
      L0_ak=R_obo.lt.R0_of
C TELEGKA5_HANDLER.fmg( 378, 430):���������� <
      L0_uf=R_obo.gt.R0_if
C TELEGKA5_HANDLER.fmg( 378, 424):���������� >
      L0_ek = L0_ak.AND.L0_uf
C TELEGKA5_HANDLER.fmg( 384, 430):�
      L0_ik = L_osi.AND.L0_ek
C TELEGKA5_HANDLER.fmg( 388, 435):�
      if(L0_ik) then
         R_obo=R0_uk
      else
         R_obo=R0_av
      endif
C TELEGKA5_HANDLER.fmg( 392, 455):���� RE IN LO CH7
      L0_ixi=R_obo.gt.R_oxi
C TELEGKA5_HANDLER.fmg( 404, 462):���������� >
      L0_ebe = L0_ixi.AND.(.NOT.L_ivi)
C TELEGKA5_HANDLER.fmg( 444, 461):�
      L_upad = L_evi.OR.L0_ebe.OR.L_uvi
C TELEGKA5_HANDLER.fmg( 450, 461):���
C sav1=L0_ote
      L0_ote = L0_ure.AND.(.NOT.L_upad)
C TELEGKA5_HANDLER.fmg( 242, 310):recalc:�
C if(sav1.ne.L0_ote .and. try307.gt.0) goto 307
C sav1=L0_ux
      L0_ux = L_upad.AND.L_ati
C TELEGKA5_HANDLER.fmg(  28, 352):recalc:�
C if(sav1.ne.L0_ux .and. try276.gt.0) goto 276
C sav1=L0_ute
      L0_ute = L_avi.AND.L_upad
C TELEGKA5_HANDLER.fmg( 250, 317):recalc:�
C if(sav1.ne.L0_ute .and. try304.gt.0) goto 304
C sav1=L0_odad
      L0_odad = L_opad.OR.L0_isu.OR.L_upad
C TELEGKA5_HANDLER.fmg(  62, 397):recalc:���
C if(sav1.ne.L0_odad .and. try438.gt.0) goto 438
C sav1=L0_avad
      L0_avad = L_upad.AND.(.NOT.L_evi)
C TELEGKA5_HANDLER.fmg(  74, 446):recalc:�
C if(sav1.ne.L0_avad .and. try450.gt.0) goto 450
      L0_imu = (.NOT.L0_ix).AND.L_upad
C TELEGKA5_HANDLER.fmg( 154, 392):�
      if(L0_imu) then
         I0_omu=I0_umu
      else
         I0_omu=I0_apu
      endif
C TELEGKA5_HANDLER.fmg( 164, 398):���� RE IN LO CH7
      if(L_upad) then
         C20_iri=C20_ori
      else
         C20_iri=C20_uri
      endif
C TELEGKA5_HANDLER.fmg(  21, 310):���� RE IN LO CH20
      if(L_opad) then
         C20_asi=C20_eri
      else
         C20_asi=C20_iri
      endif
C TELEGKA5_HANDLER.fmg(  37, 308):���� RE IN LO CH20
      R_ud=R0_av
C TELEGKA5_HANDLER.fmg( 384, 461):����������� ��
      if(L0_irad) then
         I_odu=I0_edu
      else
         I_odu=I0_idu
      endif
C TELEGKA5_HANDLER.fmg( 198, 368):���� RE IN LO CH7
      L0_uku = L0_uv.OR.L0_irad
C TELEGKA5_HANDLER.fmg( 173, 384):���
      if(L0_uku) then
         I0_evu=I0_alu
      else
         I0_evu=I0_omu
      endif
C TELEGKA5_HANDLER.fmg( 174, 396):���� RE IN LO CH7
      if(L_umad) then
         I_utu=I0_avu
      else
         I_utu=I0_evu
      endif
C TELEGKA5_HANDLER.fmg( 194, 396):���� RE IN LO CH7
      L0_ekad = L0_atad.OR.L0_irad
C TELEGKA5_HANDLER.fmg( 249, 392):���
      L0_ikad = L0_ekad.AND.(.NOT.L_ofad)
C TELEGKA5_HANDLER.fmg( 264, 392):�
      if(L0_ikad) then
         R0_elad=R_ukad
      else
         R0_elad=R0_okad
      endif
C TELEGKA5_HANDLER.fmg( 267, 399):���� RE IN LO CH7
      if(L0_esad) then
         I_ixo=I0_oxo
      else
         I_ixo=I0_uxo
      endif
C TELEGKA5_HANDLER.fmg( 143, 350):���� RE IN LO CH7
      if(L0_esad) then
         I_osu=I0_usu
      else
         I_osu=I0_atu
      endif
C TELEGKA5_HANDLER.fmg( 119, 365):���� RE IN LO CH7
      if(L0_atad) then
         I_adu=I0_obu
      else
         I_adu=I0_ubu
      endif
C TELEGKA5_HANDLER.fmg( 190, 418):���� RE IN LO CH7
      L0_elu = L0_ix.OR.L0_atad
C TELEGKA5_HANDLER.fmg( 167, 435):���
      if(L0_avad) then
         I_etu=I0_itu
      else
         I_etu=I0_otu
      endif
C TELEGKA5_HANDLER.fmg( 126, 454):���� RE IN LO CH7
      if(L0_avad) then
         I_abu=I0_ebu
      else
         I_abu=I0_ibu
      endif
C TELEGKA5_HANDLER.fmg( 122, 440):���� RE IN LO CH7
      L_idad = L_umad.OR.L0_edad.OR.L0_adad.OR.L0_ubad.OR.L0_obad
C TELEGKA5_HANDLER.fmg(  70, 403):���
      if(L_idad) then
         I_ivo=I0_eto
      else
         I_ivo=I0_ito
      endif
C TELEGKA5_HANDLER.fmg( 240, 481):���� RE IN LO CH7
      L0_oto = L_umad.OR.L_idad
C TELEGKA5_HANDLER.fmg( 270, 472):���
      if(L0_oto) then
         I_uto=I0_avo
      else
         I_uto=I0_evo
      endif
C TELEGKA5_HANDLER.fmg( 275, 480):���� RE IN LO CH7
      if(L_umad) then
         I_eru=I0_iru
      else
         I_eru=I0_oru
      endif
C TELEGKA5_HANDLER.fmg( 150, 372):���� RE IN LO CH7
      L_ixe = L0_axe.OR.L_ove
C TELEGKA5_HANDLER.fmg( 288, 287):���
      R_ol=R0_op
C TELEGKA5_HANDLER.fmg( 319, 403):����������� ��
      R_il=R0_op
C TELEGKA5_HANDLER.fmg( 349, 414):����������� ��
      if(L_esi) then
         R0_om=R0_um
      else
         R0_om=R0_et
      endif
C TELEGKA5_HANDLER.fmg( 387, 406):���� RE IN LO CH7
      R_al=R0_om
C TELEGKA5_HANDLER.fmg( 386, 412):����������� ��
      R_el=R0_om
C TELEGKA5_HANDLER.fmg( 394, 406):����������� ��
      L0_olu = L_opad.AND.(.NOT.L0_uv)
C TELEGKA5_HANDLER.fmg( 153, 447):�
      if(L0_olu) then
         I0_ulu=I0_amu
      else
         I0_ulu=I0_emu
      endif
C TELEGKA5_HANDLER.fmg( 156, 452):���� RE IN LO CH7
      if(L0_elu) then
         I0_ipad=I0_ilu
      else
         I0_ipad=I0_ulu
      endif
C TELEGKA5_HANDLER.fmg( 169, 452):���� RE IN LO CH7
      if(L_umad) then
         I_apad=I0_epad
      else
         I_apad=I0_ipad
      endif
C TELEGKA5_HANDLER.fmg( 187, 450):���� RE IN LO CH7
      R0_alad = R8_ilad
C TELEGKA5_HANDLER.fmg( 262, 408):��������
C label 658  try658=try658-1
      R8_ilad = R0_elad + R0_alad
C TELEGKA5_HANDLER.fmg( 273, 408):��������
C sav1=R0_alad
      R0_alad = R8_ilad
C TELEGKA5_HANDLER.fmg( 262, 408):recalc:��������
C if(sav1.ne.R0_alad .and. try658.gt.0) goto 658
      End
