      Interface
      Subroutine SHIB_HANDLER(ext_deltat,R8_e,R_i,R_id,R_if
     &,R_of,L_uf,R_ik,R_ok,L_uk,R_il,R_ol,R_ul,R_am,R_em,R_im
     &,R_om,R_ip,R_op,C20_us,I_ot,I_ev,R_iv,R_ov,R_uv,R_ax
     &,I_ex,I_ux,I_ube,I_ide,C20_ife,C20_ike,C8_ile,L_epe
     &,R_ipe,R_ope,L_upe,R_are,R_ere,I_ire,I_ase,I_ose,I_ete
     &,L_ute,L_eve,L_ive,L_uve,L_exe,L_ixe,L_oxe,L_ibi,L_edi
     &,L_idi,L_udi,L_efi,R8_ofi,R_iki,R8_ali,L_eli,L_ili,L_uli
     &,L_ami,L_imi,I_omi,L_upi,L_eri,L_esi,L_usi,L_eti,L_iti
     &,L_oti,L_uti,L_avi,L_evi)
C |R8_e          |4 8 O|tg_pos          |��������� ������ � �.�. ��� ��||
C |R_i           |4 4 K|_cJ3086         |�������� ��������� ����������|`p_SHIBER_UP`|
C |R_id          |4 4 K|_cJ3015         |�������� ��������� ����������|`p_SHIBER_UP`|
C |R_if          |4 4 S|_simpJ3009*     |[���]���������� ��������� ������������� |0.0|
C |R_of          |4 4 K|_timpJ3009      |[���]������������ �������� �������������|0.4|
C |L_uf          |1 1 S|_limpJ3009*     |[TF]���������� ��������� ������������� |F|
C |R_ik          |4 4 S|_simpJ3007*     |[���]���������� ��������� ������������� |0.0|
C |R_ok          |4 4 K|_timpJ3007      |[���]������������ �������� �������������|0.4|
C |L_uk          |1 1 S|_limpJ3007*     |[TF]���������� ��������� ������������� |F|
C |R_il          |4 4 K|_uintV_INT_SH   |����������� ������ ����������� ������|`p_SHIBER_UP`|
C |R_ol          |4 4 K|_tintV_INT_SH   |[���]�������� T �����������|1|
C |R_ul          |4 4 K|_lintV_INT_SH   |����������� ������ ����������� �����|0.0|
C |R_am          |4 4 K|_lcmpJ2871      |[]�������� ������ �����������|0.001|
C |R_em          |4 4 K|_lcmpJ2841      |[]�������� ������ �����������|`p_SHIBER_XH54`|
C |R_im          |4 4 O|VZ01            |��������� ������ �� ��� Z||
C |R_om          |4 4 O|_ointV_INT_SH*  |�������� ������ ����������� |0|
C |R_ip          |4 4 O|VZ02            |�������� ����������� ������||
C |R_op          |4 4 I|tcl_top         |����� ����|20.0|
C |C20_us        |3 20 O|task_name2      |������� ��������� ��� ������� ������������ ��������||
C |I_ot          |2 4 O|LREADY          |����� ����������||
C |I_ev          |2 4 O|LBUSY           |����� �����||
C |R_iv          |4 4 S|vmclose_button_ST*|��������� ������ "������� ������� �� ���������" |0.0|
C |R_ov          |4 4 I|vmclose_button  |������� ������ ������ "������� ������� �� ���������" |0.0|
C |R_uv          |4 4 S|vmopen_button_ST*|��������� ������ "������� ������� �� ���������" |0.0|
C |R_ax          |4 4 I|vmopen_button   |������� ������ ������ "������� ������� �� ���������" |0.0|
C |I_ex          |2 4 O|state1          |��������� 1||
C |I_ux          |2 4 O|state2          |��������� 2||
C |I_ube         |2 4 O|LOPENO          |����� �����������||
C |I_ide         |2 4 O|LCLOSEC         |����� �����������||
C |C20_ife       |3 20 O|task_state      |���������||
C |C20_ike       |3 20 O|task_name       |������� ���������||
C |C8_ile        |3 8 I|task            |������� ���������||
C |L_epe         |1 1 O|vmstop_button_CMD*|[TF]����� ������ ������� ���� �� ���������|F|
C |R_ipe         |4 4 S|vmstop_button_ST*|��������� ������ "������� ���� �� ���������" |0.0|
C |R_ope         |4 4 I|vmstop_button   |������� ������ ������ "������� ���� �� ���������" |0.0|
C |L_upe         |1 1 O|vmstart_button_CMD*|[TF]����� ������ ������� �������� ��������� �� ���������|F|
C |R_are         |4 4 S|vmstart_button_ST*|��������� ������ "������� �������� ��������� �� ���������" |0.0|
C |R_ere         |4 4 I|vmstart_button  |������� ������ ������ "������� �������� ��������� �� ���������" |0.0|
C |I_ire         |2 4 O|LERROR          |����� �������������||
C |I_ase         |2 4 O|LCLOSE          |����� ������||
C |I_ose         |2 4 O|LOPEN           |����� ������||
C |I_ete         |2 4 O|LZM             |������ "�������"||
C |L_ute         |1 1 I|vlv_kvit        |||
C |L_eve         |1 1 I|instr_fault     |||
C |L_ive         |1 1 S|_qffJ1618*      |�������� ������ Q RS-��������  |F|
C |L_uve         |1 1 O|vmclose_button_CMD*|[TF]����� ������ ������� ������� �� ���������|F|
C |L_exe         |1 1 O|vmopen_button_CMD*|[TF]����� ������ ������� ������� �� ���������|F|
C |L_ixe         |1 1 I|ulubclose       |���������� ������� �� ���������|F|
C |L_oxe         |1 1 I|ulubopen        |���������� ������� �� ���������|F|
C |L_ibi         |1 1 O|block           |||
C |L_edi         |1 1 O|STOP            |�������||
C |L_idi         |1 1 S|_qffJ3027*      |�������� ������ Q RS-��������  |F|
C |L_udi         |1 1 O|norm            |�����||
C |L_efi         |1 1 O|nopower         |��� ����������||
C |R8_ofi        |4 8 I|voltage         |[��]���������� �� ������||
C |R_iki         |4 4 I|power           |�������� ��������||
C |R8_ali        |4 8 O|sn_power        |�������� ���� ��|3.0|
C |L_eli         |1 1 I|YA27            |������� ������ �������|F|
C |L_ili         |1 1 I|YA25            |������� ������� �� ����������|F|
C |L_uli         |1 1 I|YA28            |������� ������ �������|F|
C |L_ami         |1 1 I|YA26            |������� ������� �� ����������|F|
C |L_imi         |1 1 O|fault           |�������������||
C |I_omi         |2 4 O|LAM             |������ "�������"||
C |L_upi         |1 1 S|_qff2*          |�������� ������ Q RS-��������  |F|
C |L_eri         |1 1 O|XH53            |�� ������� (���)|F|
C |L_esi         |1 1 S|_qff1*          |�������� ������ Q RS-��������  |F|
C |L_usi         |1 1 O|XH54            |�� ������� (���)|F|
C |L_eti         |1 1 I|mlf24           |��� ���������� � ����������||
C |L_iti         |1 1 I|mlf23           |������� ������� �����||
C |L_oti         |1 1 I|mlf22           |����� ����� ��������||
C |L_uti         |1 1 I|mlf04           |�������� �� ��������||
C |L_avi         |1 1 I|mlf03           |�������� �� ��������||
C |L_evi         |1 1 I|mlf19           |���� ������������||

      IMPLICIT NONE
      REAL*4 ext_deltat
      REAL*8 R8_e
      REAL*4 R_i,R_id,R_if,R_of
      LOGICAL*1 L_uf
      REAL*4 R_ik,R_ok
      LOGICAL*1 L_uk
      REAL*4 R_il,R_ol,R_ul,R_am,R_em,R_im,R_om,R_ip,R_op
      CHARACTER*20 C20_us
      INTEGER*4 I_ot,I_ev
      REAL*4 R_iv,R_ov,R_uv,R_ax
      INTEGER*4 I_ex,I_ux,I_ube,I_ide
      CHARACTER*20 C20_ife,C20_ike
      CHARACTER*8 C8_ile
      LOGICAL*1 L_epe
      REAL*4 R_ipe,R_ope
      LOGICAL*1 L_upe
      REAL*4 R_are,R_ere
      INTEGER*4 I_ire,I_ase,I_ose,I_ete
      LOGICAL*1 L_ute,L_eve,L_ive,L_uve,L_exe,L_ixe,L_oxe
     &,L_ibi,L_edi,L_idi,L_udi,L_efi
      REAL*8 R8_ofi
      REAL*4 R_iki
      REAL*8 R8_ali
      LOGICAL*1 L_eli,L_ili,L_uli,L_ami,L_imi
      INTEGER*4 I_omi
      LOGICAL*1 L_upi,L_eri,L_esi,L_usi,L_eti,L_iti,L_oti
     &,L_uti,L_avi,L_evi
      End subroutine SHIB_HANDLER
      End interface
