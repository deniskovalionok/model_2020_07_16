      Interface
      Subroutine BOX_HANDLER(ext_deltat,L_e,R_i,R_o,L_u,R_ad
     &,R_ed,L_id,R_od,R_ud,L_af,I_ek,L_ik,L_ok,I_ol)
C |L_e           |1 1 O|high_concentration_button_CMD*|[TF]����� ������ |F|
C |R_i           |4 4 S|high_concentration_button_ST*|��������� ������ "" |0.0|
C |R_o           |4 4 I|high_concentration_button|������� ������ ������ "" |0.0|
C |L_u           |1 1 O|alarm_vent_button_CMD*|[TF]����� ������ |F|
C |R_ad          |4 4 S|alarm_vent_button_ST*|��������� ������ "" |0.0|
C |R_ed          |4 4 I|alarm_vent_button|������� ������ ������ "" |0.0|
C |L_id          |1 1 O|inert_gas_button_CMD*|[TF]����� ������ |F|
C |R_od          |4 4 S|inert_gas_button_ST*|��������� ������ "" |0.0|
C |R_ud          |4 4 I|inert_gas_button|������� ������ ������ "" |0.0|
C |L_af          |1 1 I|alarm_vent      |��������� ����������||
C |I_ek          |2 4 O|LSR             |��������� ����� �����||
C |L_ik          |1 1 I|high_concentration|���������� ������������ ���������/�����||
C |L_ok          |1 1 I|inert_gas       |�������� �������� �����||
C |I_ol          |2 4 O|LS              |��������� �����||

      IMPLICIT NONE
      REAL*4 ext_deltat
      LOGICAL*1 L_e
      REAL*4 R_i,R_o
      LOGICAL*1 L_u
      REAL*4 R_ad,R_ed
      LOGICAL*1 L_id
      REAL*4 R_od,R_ud
      LOGICAL*1 L_af
      INTEGER*4 I_ek
      LOGICAL*1 L_ik,L_ok
      INTEGER*4 I_ol
      End subroutine BOX_HANDLER
      End interface
