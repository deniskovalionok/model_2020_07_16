      Subroutine TELEGKA_TRANSP_HANDLER(ext_deltat,L_e,L_i
     &,L_o,L_u,R_ed,R_id,R_ud,R_af,L_ef,R_of,R_uf,L_ak,L_ek
     &,L_al,L_im,L_om,R_um,R_ap,R_ep,R_ip,R_op,L_ar,L_ir,R_ur
     &,R_as,R_it,R_ot,L_ut,R_av,R_ev,L_iv,R_ov,R_uv,L_ax,L_ix
     &,L_ox,L_ux,L_ebe,L_ibe,L_obe,L_ade,L_afe,L_ufe,L_eke
     &)
C |L_e           |1 1 O|UNCATCH         |��������� �������|F|
C |L_i           |1 1 I|YA31            |������� ��������� �������|F|
C |L_o           |1 1 O|CATCH           |������ �������|F|
C |L_u           |1 1 I|YA30            |������� ������ �������|F|
C |R_ed          |4 4 K|_cJ3237         |�������� ��������� ����������|`max_c`|
C |R_id          |4 4 K|_cJ3236         |�������� ��������� ����������|`min_c`|
C |R_ud          |4 4 S|_simpJ3201*     |[���]���������� ��������� ������������� |0.0|
C |R_af          |4 4 K|_timpJ3201      |[���]������������ �������� �������������|0.4|
C |L_ef          |1 1 S|_limpJ3201*     |[TF]���������� ��������� ������������� |F|
C |R_of          |4 4 S|_simpJ3198*     |[���]���������� ��������� ������������� |0.0|
C |R_uf          |4 4 K|_timpJ3198      |[���]������������ �������� �������������|0.4|
C |L_ak          |1 1 S|_limpJ3198*     |[TF]���������� ��������� ������������� |F|
C |L_ek          |1 1 O|XH54            |�������� ������� MAX (��)||
C |L_al          |1 1 O|XH53            |�������� ������� MIN (��)||
C |L_im          |1 1 I|YA27            |������� ���� �� ����������|F|
C |L_om          |1 1 I|forward         |������� �� ��������� ������||
C |R_um          |4 4 K|_uintV_INT_PU   |����������� ������ ����������� ������|`max_c`|
C |R_ap          |4 4 K|_tintV_INT_PU   |[���]�������� T �����������|1|
C |R_ep          |4 4 K|_lintV_INT_PU   |����������� ������ ����������� �����|`min_c`|
C |R_ip          |4 4 O|POS             |��������� ��||
C |R_op          |4 4 O|_ointV_INT_PU*  |�������� ������ ����������� |`state_c`|
C |L_ar          |1 1 I|mlf03           |�������� �� ��������||
C |L_ir          |1 1 I|mlf04           |�������� �� ��������||
C |R_ur          |4 4 O|V01             |�������� ����������� ��||
C |R_as          |4 4 I|vel             |����� ����|30.0|
C |R_it          |4 4 S|USER_BACK_ST*   |��������� ������ "������� ����� �� ���������" |0.0|
C |R_ot          |4 4 I|USER_BACK       |������� ������ ������ "������� ����� �� ���������" |0.0|
C |L_ut          |1 1 O|USER_FORWARD_CMD*|[TF]����� ������ ������� ������ �� ���������|F|
C |R_av          |4 4 S|USER_FORWARD_ST*|��������� ������ "������� ������ �� ���������" |0.0|
C |R_ev          |4 4 I|USER_FORWARD    |������� ������ ������ "������� ������ �� ���������" |0.0|
C |L_iv          |1 1 O|USER_STOP_CMD*  |[TF]����� ������ ������� ���� �� ���������|F|
C |R_ov          |4 4 S|USER_STOP_ST*   |��������� ������ "������� ���� �� ���������" |0.0|
C |R_uv          |4 4 I|USER_STOP       |������� ������ ������ "������� ���� �� ���������" |0.0|
C |L_ax          |1 1 I|back            |������� �� ��������� �����||
C |L_ix          |1 1 I|ulubforward     |���������� ������ �� ���������|F|
C |L_ox          |1 1 I|ulubback        |���������� ����� �� ���������|F|
C |L_ux          |1 1 O|USER_BACK_CMD*  |[TF]����� ������ ������� ����� �� ���������|F|
C |L_ebe         |1 1 O|stop_inner      |�������||
C |L_ibe         |1 1 S|_qffJ3299*      |�������� ������ Q RS-��������  |F|
C |L_obe         |1 1 I|YA25            |������� ����� �� ����������|F|
C |L_ade         |1 1 I|YA26            |������� ������ �� ����������|F|
C |L_afe         |1 1 S|_qff2*          |�������� ������ Q RS-��������  |F|
C |L_ufe         |1 1 S|_qff1*          |�������� ������ Q RS-��������  |F|
C |L_eke         |1 1 I|mlf24           |��� ���������� � ����������||

      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      LOGICAL*1 L_e,L_i,L_o,L_u,L0_ad
      REAL*4 R_ed,R_id,R0_od,R_ud,R_af
      LOGICAL*1 L_ef
      REAL*4 R0_if,R_of,R_uf
      LOGICAL*1 L_ak,L_ek
      REAL*4 R0_ik,R0_ok,R0_uk
      LOGICAL*1 L_al
      REAL*4 R0_el,R0_il,R0_ol
      LOGICAL*1 L0_ul,L0_am,L0_em,L_im,L_om
      REAL*4 R_um,R_ap,R_ep,R_ip,R_op
      LOGICAL*1 L0_up,L_ar,L0_er,L_ir
      REAL*4 R0_or,R_ur,R_as,R0_es,R0_is,R0_os,R0_us
      LOGICAL*1 L0_at,L0_et
      REAL*4 R_it,R_ot
      LOGICAL*1 L_ut
      REAL*4 R_av,R_ev
      LOGICAL*1 L_iv
      REAL*4 R_ov,R_uv
      LOGICAL*1 L_ax,L0_ex,L_ix,L_ox,L_ux,L0_abe,L_ebe,L_ibe
     &,L_obe,L0_ube,L_ade,L0_ede,L0_ide,L0_ode,L0_ude,L_afe
     &,L0_efe,L0_ife,L0_ofe,L_ufe,L0_ake,L_eke

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      R0_od=R_ud
C TELEGKA_TRANSP_HANDLER.fmg( 374, 215):pre: ������������  �� T
      R0_if=R_of
C TELEGKA_TRANSP_HANDLER.fmg( 374, 225):pre: ������������  �� T
      L_e=L_i
C TELEGKA_TRANSP_HANDLER.fmg( 192, 254):������,UNCATCH
      L_o=L_u
C TELEGKA_TRANSP_HANDLER.fmg( 192, 260):������,CATCH
      !��������� R0_ok = TELEGKA_TRANSP_HANDLERC?? /`max_c`
C /
      R0_ok=R_ed
C TELEGKA_TRANSP_HANDLER.fmg( 344, 215):���������
      !��������� R0_il = TELEGKA_TRANSP_HANDLERC?? /`min_c`
C /
      R0_il=R_id
C TELEGKA_TRANSP_HANDLER.fmg( 344, 225):���������
      R0_ik = 0.01
C TELEGKA_TRANSP_HANDLER.fmg( 344, 212):��������� (RE4) (�������)
      R0_uk = R0_ok + (-R0_ik)
C TELEGKA_TRANSP_HANDLER.fmg( 348, 214):��������
      R0_el = 0.01
C TELEGKA_TRANSP_HANDLER.fmg( 344, 222):��������� (RE4) (�������)
      R0_ol = R0_il + R0_el
C TELEGKA_TRANSP_HANDLER.fmg( 348, 224):��������
      R0_or = 0.0
C TELEGKA_TRANSP_HANDLER.fmg( 314, 246):��������� (RE4) (�������)
      R0_us = 0.0
C TELEGKA_TRANSP_HANDLER.fmg( 298, 246):��������� (RE4) (�������)
      L_ux=R_ot.ne.R_it
      R_it=R_ot
C TELEGKA_TRANSP_HANDLER.fmg(  14, 190):���������� ������������� ������
      L0_ube = (.NOT.L_ox).AND.L_ux.AND.L_ax
C TELEGKA_TRANSP_HANDLER.fmg(  66, 189):�
      L0_ode = L0_ube.OR.L_obe
C TELEGKA_TRANSP_HANDLER.fmg(  70, 188):���
      L_ut=R_ev.ne.R_av
      R_av=R_ev
C TELEGKA_TRANSP_HANDLER.fmg(  16, 249):���������� ������������� ������
      L0_ex = L_om.OR.L_ut
C TELEGKA_TRANSP_HANDLER.fmg(  56, 240):���
      L0_ede = L0_ex.AND.L_ix
C TELEGKA_TRANSP_HANDLER.fmg(  66, 236):�
      L0_ife = L0_ede.OR.L_ade
C TELEGKA_TRANSP_HANDLER.fmg(  70, 234):���
      L0_abe = L0_ife.OR.L0_ode
C TELEGKA_TRANSP_HANDLER.fmg(  76, 203):���
      L_iv=R_uv.ne.R_ov
      R_ov=R_uv
C TELEGKA_TRANSP_HANDLER.fmg(  14, 214):���������� ������������� ������
      L0_ide = L_ibe.OR.L_eke
C TELEGKA_TRANSP_HANDLER.fmg(  98, 214):���
C label 41  try41=try41-1
      L0_ofe = L_eke.OR.L0_ide.OR.L0_ode
C TELEGKA_TRANSP_HANDLER.fmg( 104, 230):���
      L_ufe=(L0_ife.or.L_ufe).and..not.(L0_ofe)
      L0_ake=.not.L_ufe
C TELEGKA_TRANSP_HANDLER.fmg( 111, 232):RS �������,1
      L0_er = L_ufe.AND.(.NOT.L_ir)
C TELEGKA_TRANSP_HANDLER.fmg( 268, 245):�
      L0_et = L0_er.OR.L_ar
C TELEGKA_TRANSP_HANDLER.fmg( 281, 244):���
      if(L0_et) then
         R0_is=R_as
      else
         R0_is=R0_us
      endif
C TELEGKA_TRANSP_HANDLER.fmg( 303, 256):���� RE IN LO CH7
      L0_ude = L0_ide.OR.L0_ife
C TELEGKA_TRANSP_HANDLER.fmg( 104, 180):���
      L_afe=(L0_ode.or.L_afe).and..not.(L0_ude)
      L0_efe=.not.L_afe
C TELEGKA_TRANSP_HANDLER.fmg( 111, 186):RS �������,2
      L0_up = L_afe.AND.(.NOT.L_ar)
C TELEGKA_TRANSP_HANDLER.fmg( 268, 231):�
      L0_at = L0_up.OR.L_ir
C TELEGKA_TRANSP_HANDLER.fmg( 281, 230):���
      if(L0_at) then
         R0_es=R_as
      else
         R0_es=R0_us
      endif
C TELEGKA_TRANSP_HANDLER.fmg( 303, 238):���� RE IN LO CH7
      R0_os = R0_is + (-R0_es)
C TELEGKA_TRANSP_HANDLER.fmg( 309, 249):��������
      if(L_eke) then
         R_ur=R0_or
      else
         R_ur=R0_os
      endif
C TELEGKA_TRANSP_HANDLER.fmg( 318, 248):���� RE IN LO CH7
      R_op=R_op+deltat/R_ap*R_ur
      if(R_op.gt.R_um) then
         R_op=R_um
      elseif(R_op.lt.R_ep) then
         R_op=R_ep
      endif
C TELEGKA_TRANSP_HANDLER.fmg( 332, 232):����������,V_INT_PU
      L_al=R_op.lt.R0_ol
C TELEGKA_TRANSP_HANDLER.fmg( 356, 225):���������� <
      if(L_al.and..not.L_ak) then
         R_of=R_uf
      else
         R_of=max(R0_if-deltat,0.0)
      endif
      L0_am=R_of.gt.0.0
      L_ak=L_al
C TELEGKA_TRANSP_HANDLER.fmg( 374, 225):������������  �� T
      L_ek=R_op.gt.R0_uk
C TELEGKA_TRANSP_HANDLER.fmg( 356, 215):���������� >
      if(L_ek.and..not.L_ef) then
         R_ud=R_af
      else
         R_ud=max(R0_od-deltat,0.0)
      endif
      L0_ul=R_ud.gt.0.0
      L_ef=L_ek
C TELEGKA_TRANSP_HANDLER.fmg( 374, 215):������������  �� T
      L0_em = L_iv.OR.L_im.OR.L0_am.OR.L0_ul
C TELEGKA_TRANSP_HANDLER.fmg(  59, 207):���
      L_ibe=L0_em.or.(L_ibe.and..not.(L0_abe))
      L0_ad=.not.L_ibe
C TELEGKA_TRANSP_HANDLER.fmg( 106, 205):RS �������
C sav1=L0_ide
      L0_ide = L_ibe.OR.L_eke
C TELEGKA_TRANSP_HANDLER.fmg(  98, 214):recalc:���
C if(sav1.ne.L0_ide .and. try41.gt.0) goto 41
      L_ebe=L_ibe
C TELEGKA_TRANSP_HANDLER.fmg( 133, 207):������,stop_inner
      R_ip=R_op
C TELEGKA_TRANSP_HANDLER.fmg( 390, 234):������,POS
      End
