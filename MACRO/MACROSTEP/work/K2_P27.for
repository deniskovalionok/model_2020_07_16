      Subroutine K2_P27(ext_deltat,R_ed,R_ud,R_uf,R_ek,R_ox
     &,R_ux,L_ube,L_ufe,L_ake,L_eke,R_ike,R_ale,R_ule,R_ime
     &,R_ume,R_are,R_ire,L_ure,R_ese,R_ise,R_use,R_ite,R_ute
     &,R_ave,R_ove,R_axe,R_exe,R_uxe,R_ibi,R_obi,R_adi)
C |R_ed          |4 4 S|_slpb1*         |���������� ��������� ||
C |R_ud          |4 4 I|xb              |[�] ���� �����������||
C |R_uf          |4 4 O|kzamedl         |����-� ����������||
C |R_ek          |4 4 O|minzone         |���. ������||
C |R_ox          |4 4 O|z20             |����� �������. ��������||
C |R_ux          |4 4 I|taudf           |[���] ���������� ������� ��������|5|
C |L_ube         |1 1 I|intset          |���������� ������ �����������|F|
C |L_ufe         |1 1 O|z12             |������ "������"|F|
C |L_ake         |1 1 O|z11             |������ "������"|F|
C |L_eke         |1 1 I|block           |������ �������� ����������|F|
C |R_ike         |4 4 I|taui            |[���] ���������� �����������|10.0|
C |R_ale         |4 4 I|ti              |[���] ������������ ������������� ��������|0.2|
C |R_ule         |4 4 I|alphap          |[���/%] �-� �������� �����|0.5|
C |R_ime         |4 4 I|zone            |[%] ���� ������������������ ��������|1.0|
C |R_ume         |4 4 O|_oapr1*         |�������� ������ ���������. ����� |0.0|
C |R_are         |4 4 O|delta           |[�] �������� ������� ������|0|
C |R_ire         |4 4 S|_oint1*         |����� ����������� |0.0|
C |L_ure         |1 1 I|difset          |���������� ���������������|T|
C |R_ese         |4 4 S|_sdif1*         |���������� ��������� |0.0|
C |R_ise         |4 4 O|_odif1*         |�������� ������ |0.0|
C |R_use         |4 4 I|taud            |[���] ���������� ������� ���-��|5|
C |R_ite         |4 4 I|x41max          |�������� ��������� ������� ��������||
C |R_ute         |4 4 O|epsil           |[%] (17)����� � (0-100%)||
C |R_ave         |4 4 I|setpoint        |[�.�.] (23)������� ������� ���������||
C |R_ove         |4 4 I|alpha4          |�-� �������� �� ����� �4|1|
C |R_axe         |4 4 I|x41             |(20)���� �41 (0-5��)||
C |R_exe         |4 4 I|alpha3          |�-� �������� �� ����� �3|1|
C |R_uxe         |4 4 I|alpha2          |�-� �������� �� ����� �2|1|
C |R_ibi         |4 4 I|x3              |(16)���� �3 (0-5��)||
C |R_obi         |4 4 I|x2              |(12)���� �2 (0-5��)||
C |R_adi         |4 4 I|x1              |(8)���� �1 (0-5��)||

      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      REAL*4 R0_e,R0_i,R0_o,R0_u,R0_ad,R_ed,R0_id,R0_od,R_ud
     &,R0_af,R0_ef,R0_if,R0_of,R_uf,R0_ak,R_ek,R0_ik,R0_ok
     &,R0_uk,R0_al,R0_el,R0_il
      REAL*4 R0_ol,R0_ul,R0_am,R0_em,R0_im,R0_om,R0_um,R0_ap
     &,R0_ep,R0_ip,R0_op,R0_up,R0_ar,R0_er,R0_ir,R0_or,R0_ur
     &,R0_as,R0_es,R0_is,R0_os,R0_us
      REAL*4 R0_at,R0_et,R0_it,R0_ot,R0_ut,R0_av,R0_ev,R0_iv
     &,R0_ov,R0_uv,R0_ax,R0_ex,R0_ix,R_ox,R_ux
      LOGICAL*1 L0_abe
      REAL*4 R0_ebe,R0_ibe,R0_obe
      LOGICAL*1 L_ube,L0_ade
      REAL*4 R0_ede,R0_ide,R0_ode,R0_ude,R0_afe,R0_efe,R0_ife
     &,R0_ofe
      LOGICAL*1 L_ufe,L_ake,L_eke
      REAL*4 R_ike,R0_oke,R0_uke,R_ale,R0_ele,R0_ile,R0_ole
     &,R_ule
      LOGICAL*1 L0_ame,L0_eme
      REAL*4 R_ime,R0_ome,R_ume,R0_ape,R0_epe,R0_ipe,R0_ope
     &,R0_upe,R_are,R0_ere,R_ire,R0_ore
      LOGICAL*1 L_ure
      REAL*4 R0_ase,R_ese,R_ise,R0_ose,R_use,R0_ate,R0_ete
     &,R_ite,R0_ote,R_ute,R_ave,R0_eve,R0_ive,R_ove,R0_uve
     &,R_axe,R_exe,R0_ixe,R0_oxe,R_uxe,R0_abi,R0_ebi
      REAL*4 R_ibi,R_obi,R0_ubi,R_adi

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      R0_epe=R_ed
C K2_P27.fmg( 301, 198):pre: ����������� ��,1
      R0_ase=R_ese
C K2_P27.fmg( 180, 262):pre: ���������������� ����� ,1
      R0_ebe=R_ume
C K2_P27.fmg( 158, 230):pre: �������������� �����  ,1
      R0_e = 1.0
C K2_P27.fmg( 282, 192):��������� (RE4) (�������)
      R0_i = 10.0
C K2_P27.fmg( 281, 170):��������� (RE4) (�������)
      R0_o = 5.0
C K2_P27.fmg( 276, 172):��������� (RE4) (�������)
      R0_id = 2.2
C K2_P27.fmg( 185, 254):��������� (RE4) (�������)
      R0_od = 10.0
C K2_P27.fmg( 112, 242):��������� (RE4) (�������)
      R0_ome = 0.0
C K2_P27.fmg( 149, 213):��������� (RE4) (�������)
      R0_af = DeltaT
C K2_P27.fmg( 254, 190):��������� (RE4) (�������)
      if(R_ale.ge.0.0) then
         R0_ef=R0_af/max(R_ale,1.0e-10)
      else
         R0_ef=R0_af/min(R_ale,-1.0e-10)
      endif
C K2_P27.fmg( 261, 189):�������� ����������
      R0_if = 1.0
C K2_P27.fmg( 270, 190):��������� (RE4) (�������)
      R0_of=MAX(R0_if,R0_ef)
C K2_P27.fmg( 276, 190):��������
      R0_us = 8.0
C K2_P27.fmg( 267, 239):��������� (RE4) (�������)
      R0_ak = 0.55
C K2_P27.fmg( 366, 223):��������� (RE4) (�������)
      R0_ik = 0.13049
C K2_P27.fmg( 361, 222):��������� (RE4) (�������)
      R0_uk = 0.56
C K2_P27.fmg( 355, 219):��������� (RE4) (�������)
      R0_il = 0.089
C K2_P27.fmg( 342, 213):��������� (RE4) (�������)
      R0_ul = 0.73
C K2_P27.fmg( 342, 207):��������� (RE4) (�������)
      R0_op = 0.666
C K2_P27.fmg( 280, 265):��������� (RE4) (�������)
      R0_um = 38.0
C K2_P27.fmg( 177, 246):��������� (RE4) (�������)
      R0_ap = 22.0
C K2_P27.fmg( 173, 245):��������� (RE4) (�������)
      R0_ep = 39.3
C K2_P27.fmg( 173, 231):��������� (RE4) (�������)
      R0_ip = -1.7
C K2_P27.fmg( 173, 233):��������� (RE4) (�������)
      R0_om=R_ux*(R0_ip)+(R0_ep)
      if(R0_om.lt.R0_ap) then
         R0_om=R0_ap
      elseif(R0_om.gt.R0_um) then
         R0_om=R0_um
      endif
C K2_P27.fmg( 177, 240):��������������� ������������� �����������
      R0_up = 0.1
C K2_P27.fmg( 210, 209):��������� (RE4) (�������)
      R0_ar = 0.13049
C K2_P27.fmg( 196, 209):��������� (RE4) (�������)
      R0_ir = 1.80070
C K2_P27.fmg( 196, 207):��������� (RE4) (�������)
      R0_or = R0_ir * R_ime
C K2_P27.fmg( 199, 207):����������
      R0_er = R0_ar + R0_or
C K2_P27.fmg( 204, 208):��������
      R0_ode=MAX(R0_up,R0_er)
C K2_P27.fmg( 214, 209):��������
      R0_ude = -(R0_ode)
C K2_P27.fmg( 240, 226):��������
      R0_ur = 18.8
C K2_P27.fmg( 212, 241):��������� (RE4) (�������)
      R0_es = 0.089
C K2_P27.fmg( 298, 217):��������� (RE4) (�������)
      R0_ot = 0.73
C K2_P27.fmg( 298, 211):��������� (RE4) (�������)
      R0_os = 36.6
C K2_P27.fmg( 212, 235):��������� (RE4) (�������)
      R0_ipe = R0_os * R0_ome
C K2_P27.fmg( 216, 235):����������
      R0_at = 0.40703
C K2_P27.fmg( 259, 259):��������� (RE4) (�������)
      R0_et = 0.13978
C K2_P27.fmg( 259, 257):��������� (RE4) (�������)
      R0_it = R0_et * R_ike
C K2_P27.fmg( 262, 257):����������
      R0_ax = R0_at + R0_it
C K2_P27.fmg( 267, 258):��������
      R0_av = 1.5
C K2_P27.fmg( 266, 208):��������� (RE4) (�������)
      R0_ele = R0_av * R_ale
C K2_P27.fmg( 269, 208):����������
      R0_ev = 0.54400
C K2_P27.fmg( 266, 219):��������� (RE4) (�������)
      R0_iv = 2.29719
C K2_P27.fmg( 266, 217):��������� (RE4) (�������)
      R0_ov = R0_iv * R_ule
C K2_P27.fmg( 269, 217):����������
      R0_uv = R0_ev + R0_ov
C K2_P27.fmg( 274, 218):��������
      if(R0_uv.ge.0.0) then
         R0_em=R0_us/max(R0_uv,1.0e-10)
      else
         R0_em=R0_us/min(R0_uv,-1.0e-10)
      endif
C K2_P27.fmg( 328, 211):�������� ����������
      R0_im = R0_em * R0_ele
C K2_P27.fmg( 338, 205):����������
      R0_am = R0_ul * R0_im
C K2_P27.fmg( 347, 206):����������
      R0_ol = R0_il * R0_em
C K2_P27.fmg( 347, 212):����������
      R0_el = R0_ol + R0_am
C K2_P27.fmg( 353, 207):��������
      R0_al = R0_uk * R0_el
C K2_P27.fmg( 359, 219):����������
      R0_ok = (-R0_ik) + R0_al
C K2_P27.fmg( 364, 220):��������
      R_ek = R0_ak * R0_ok
C K2_P27.fmg( 369, 221):����������
      R0_obe = 15.0
C K2_P27.fmg( 298, 266):��������� (RE4) (�������)
      R0_ix = 0.0
C K2_P27.fmg( 199, 254):��������� (RE4) (�������)
      L0_abe=.false.
C K2_P27.fmg( 154, 223):��������� ���������� (�������)
      L0_ade=.false.
C K2_P27.fmg( 301, 253):��������� ���������� (�������)
      R0_ide = 0.0
C K2_P27.fmg( 300, 266):��������� (RE4) (�������)
      R0_ede = -120.0
C K2_P27.fmg( 304, 266):��������� (RE4) (�������)
      R0_ibe = 120.0
C K2_P27.fmg( 306, 266):��������� (RE4) (�������)
      R0_afe = 0.0
C K2_P27.fmg( 163, 260):��������� (RE4) (�������)
      R0_ate = 10.0
C K2_P27.fmg(  99, 231):��������� (RE4) (�������)
      if(R_ite.ge.0.0) then
         R0_uve=R_axe/max(R_ite,1.0e-10)
      else
         R0_uve=R_axe/min(R_ite,-1.0e-10)
      endif
C K2_P27.fmg(  61, 200):�������� ����������
      R0_ive = R0_uve * R_ove
C K2_P27.fmg(  75, 199):����������
      if(R_ite.ge.0.0) then
         R0_ixe=R_ibi/max(R_ite,1.0e-10)
      else
         R0_ixe=R_ibi/min(R_ite,-1.0e-10)
      endif
C K2_P27.fmg(  61, 222):�������� ����������
      R0_oxe = R0_ixe * R_exe
C K2_P27.fmg(  75, 221):����������
      if(R_ite.ge.0.0) then
         R0_abi=R_obi/max(R_ite,1.0e-10)
      else
         R0_abi=R_obi/min(R_ite,-1.0e-10)
      endif
C K2_P27.fmg(  61, 241):�������� ����������
      R0_ebi = R0_abi * R_uxe
C K2_P27.fmg(  75, 240):����������
      if(R_ite.ge.0.0) then
         R0_ubi=R_adi/max(R_ite,1.0e-10)
      else
         R0_ubi=R_adi/min(R_ite,-1.0e-10)
      endif
C K2_P27.fmg(  60, 254):�������� ����������
      R0_eve = R0_ubi + R0_ebi + R0_oxe + R0_ive + (-R_ave
     &)
C K2_P27.fmg(  91, 230):��������
      R0_ete = R0_ate * R0_eve
C K2_P27.fmg( 102, 231):����������
      R0_ote = R0_ete + R_ud
C K2_P27.fmg( 109, 230):��������
      if(L0_abe) then
         R_ume=R0_ote
      else
         R_ume=(R_ux*R0_ebe+deltat*R0_ote)/(R_ux+deltat)
      endif
C K2_P27.fmg( 158, 230):�������������� �����  ,1
      R0_ope = R0_om * R_ume
C K2_P27.fmg( 188, 239):����������
      R0_ad=abs(R0_ope)
C K2_P27.fmg( 225, 182):���������� ��������
      if(R0_ode.ge.0.0) then
         R0_u=R0_ad/max(R0_ode,1.0e-10)
      else
         R0_u=R0_ad/min(R0_ode,-1.0e-10)
      endif
C K2_P27.fmg( 241, 180):�������� ����������
      if(R0_u.le.R0_o) then
         R_uf=R0_of
      elseif(R0_u.gt.R0_i) then
         R_uf=R0_e
      else
         R_uf=R0_of+(R0_u-(R0_o))*(R0_e-(R0_of))/(R0_i-(R0_o
     &))
      endif
C K2_P27.fmg( 282, 180):��������������� ��������� �����������
      if(L_ure) then
         R0_ose=R0_afe
      else
         R0_ose=R_ume
      endif
C K2_P27.fmg( 169, 261):���� RE IN LO CH7
      if(L_ure) then
         R_ise=0.0
         R_ese=R0_ose
      else
         R_ise=(R_use*(R0_ose-R0_ase)+R_use*R_ise)/(R_use
     &+deltat)
         R_ese=R0_ose
      endif
C K2_P27.fmg( 180, 262):���������������� ����� ,1
      R0_ere = R_ise * R0_id
C K2_P27.fmg( 191, 261):����������
      R_ute = R0_od * R0_ote
C K2_P27.fmg( 116, 242):����������
      R0_ore = (-R_ire) + R0_ere
C K2_P27.fmg( 197, 262):��������
C label 161  try161=try161-1
      if(R0_ax.ge.0.0) then
         R0_ex=R0_ore/max(R0_ax,1.0e-10)
      else
         R0_ex=R0_ore/min(R0_ax,-1.0e-10)
      endif
C K2_P27.fmg( 274, 260):�������� ����������
      R0_oke = R0_op * R0_ex
C K2_P27.fmg( 285, 261):����������
      if(L_ube) then
         R0_ape=R0_ix
      else
         R0_ape=R0_ore
      endif
C K2_P27.fmg( 204, 255):���� RE IN LO CH7
      R0_upe = R0_ape * R0_ur
C K2_P27.fmg( 216, 243):����������
      R_are = R0_upe + R0_ope + (-R0_ipe) + R0_epe
C K2_P27.fmg( 223, 238):��������
      L0_eme=R_are.gt.R0_ode
C K2_P27.fmg( 248, 244):���������� >
      if(L0_eme) then
         R0_ofe=1
      else
         R0_ofe=0
      endif
C K2_P27.fmg( 256, 240):��������� LO->1
      L0_ame=R_are.lt.R0_ude
C K2_P27.fmg( 248, 227):���������� <
      if(L0_ame) then
         R0_ife=1
      else
         R0_ife=0
      endif
C K2_P27.fmg( 256, 231):��������� LO->1
      R0_efe = R0_ofe + (-R0_ife)
C K2_P27.fmg( 263, 236):��������
      R_ox = R0_us * R0_efe
C K2_P27.fmg( 272, 237):����������
      if(R0_uv.ge.0.0) then
         R0_ile=R_ox/max(R0_uv,1.0e-10)
      else
         R0_ile=R_ox/min(R0_uv,-1.0e-10)
      endif
C K2_P27.fmg( 282, 232):�������� ����������
      R0_uke = R0_oke + R0_ile
C K2_P27.fmg( 294, 260):��������
      if(L_ube) then
         R_ire=R0_ide
      elseif(L0_ade) then
         R_ire=R_ire
      else
         R_ire=R_ire+deltat/R0_obe*R0_uke
      endif
      if(R_ire.gt.R0_ibe) then
         R_ire=R0_ibe
      elseif(R_ire.lt.R0_ede) then
         R_ire=R0_ede
      endif
C K2_P27.fmg( 304, 260):����������,1
C sav1=R0_ore
      R0_ore = (-R_ire) + R0_ere
C K2_P27.fmg( 197, 262):recalc:��������
C if(sav1.ne.R0_ore .and. try161.gt.0) goto 161
      R0_ole = R0_ile * R0_ele
C K2_P27.fmg( 294, 209):����������
      R0_ut = R0_ot * R0_ole
C K2_P27.fmg( 303, 210):����������
      R0_is = R0_es * R0_ile
C K2_P27.fmg( 303, 216):����������
      R0_as = R0_is + R0_ut
C K2_P27.fmg( 309, 211):��������
      R_ed=R0_as
C K2_P27.fmg( 301, 198):����������� ��,1
      L_ufe = L0_ame.AND.(.NOT.L_eke)
C K2_P27.fmg( 323, 226):�
      L_ake = L0_eme.AND.(.NOT.L_eke)
C K2_P27.fmg( 324, 243):�
      End
