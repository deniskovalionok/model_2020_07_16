      Interface
      Subroutine TOLKATEL_HANDLER(ext_deltat,R_afi,R_e,R_i
     &,R_ed,R_af,R_uk,R_el,R_il,R_ol,R_ul,L_is,R_ot,R_ede
     &,L_ide,R_ude,L_afe,L_eke,L_uke,R_ume,R_ope,L_are,R_ire
     &,L_ure,R_ase,R_ese,L_ose,L_ote,L_ave,L_eve,L_ove,L_exe
     &,L_uxe,L_ebi,R_efi,L_ufi,L_eki,L_oki,L_uki,L_ili,L_oli
     &,C20_upi,L_ari,L_iri,L_ori,L_esi,L_isi,L_osi,L_usi,L_ati
     &,L_eti,L_iti,L_oti,L_uti,L_avi,L_evi,L_ivi,L_ovi,L_uvi
     &,L_axi,L_exi,L_ixi,R_oxi,R_uxi,R_abo,R_ebo,R_ibo,I_obo
     &,R_edo,R_ido,L_afo,C20_ulo,C20_umo,I_opo,I_ero,R_iro
     &,R_oro,R_uro,R_aso,L_eso,L_iso,I_oso,I_eto,I_evo,I_uvo
     &,C20_uxo,C8_ubu,R_iku,R_oku,L_uku,R_alu,R_elu,I_ilu
     &,L_amu,L_emu,L_imu,I_umu,I_ipu,L_iru,L_uru,L_asu,L_esu
     &,L_isu,L_osu,L_otu,L_ovu,L_axu,L_exu,R8_oxu,R_ibad,R8_adad
     &,L_odad,L_efad,I_ifad,L_akad,L_ekad,L_elad,L_umad,L_upad
     &)
C |R_afi         |4 4 I|11 mlfpar19     ||0.6|
C |R_e           |4 4 I|TIMESCALE       |������� �������||
C |R_i           |4 4 K|_cJ4002         |�������� ��������� ����������|`p_UP`|
C |R_ed          |4 4 K|_cJ3997         |�������� ��������� ����������|`p_LOW`|
C |R_af          |4 4 S|_slpbJ3982*     |���������� ��������� ||
C |R_uk          |4 4 I|p_TOLKATEL_XH54 |||
C |R_el          |4 4 S|_slpbJ3940*     |���������� ��������� ||
C |R_il          |4 4 S|_slpbJ3939*     |���������� ��������� ||
C |R_ol          |4 4 S|_slpbJ3938*     |���������� ��������� ||
C |R_ul          |4 4 S|_slpbJ3932*     |���������� ��������� ||
C |L_is          |1 1 S|_qffJ3888*      |�������� ������ Q RS-��������  |F|
C |R_ot          |4 4 I|mlfpar19        |��������� ������������|0.6|
C |R_ede         |4 4 S|_sdelnv2dyn$100Dasha*|[���]���������� ��������� |0|
C |L_ide         |1 1 S|_idelnv2dyn$100Dasha*|[TF]���������� ��������� �������� ������ |f|
C |R_ude         |4 4 S|_sdelnv2dyn$99Dasha*|[���]���������� ��������� |0|
C |L_afe         |1 1 S|_idelnv2dyn$99Dasha*|[TF]���������� ��������� �������� ������ |f|
C |L_eke         |1 1 S|_qff156*        |�������� ������ Q RS-��������  |F|
C |L_uke         |1 1 S|_qff155*        |�������� ������ Q RS-��������  |F|
C |R_ume         |4 4 I|value_pos       |��� �������� ��������|0.5|
C |R_ope         |4 4 S|_sdelnv2dyn$83Dasha*|[���]���������� ��������� |0|
C |L_are         |1 1 S|_idelnv2dyn$83Dasha*|[TF]���������� ��������� �������� ������ |f|
C |R_ire         |4 4 S|_sdelnv2dyn$82Dasha*|[���]���������� ��������� |0|
C |L_ure         |1 1 S|_idelnv2dyn$82Dasha*|[TF]���������� ��������� �������� ������ |f|
C |R_ase         |4 4 I|tpos1           |����� ���� � ��������� 1|30.0|
C |R_ese         |4 4 I|tpos2           |����� ���� � ��������� 2|30.0|
C |L_ose         |1 1 S|_qffnv2dyn$106Dasha*|�������� ������ Q RS-��������  |F|
C |L_ote         |1 1 S|_qffnv2dyn$104Dasha*|�������� ������ Q RS-��������  |F|
C |L_ave         |1 1 S|_qffnv2dyn$105Dasha*|�������� ������ Q RS-��������  |F|
C |L_eve         |1 1 I|OUTC            |�����||
C |L_ove         |1 1 S|_qffnv2dyn$103Dasha*|�������� ������ Q RS-��������  |F|
C |L_exe         |1 1 S|_qffnv2dyn$102Dasha*|�������� ������ Q RS-��������  |F|
C |L_uxe         |1 1 S|_qffnv2dyn$101Dasha*|�������� ������ Q RS-��������  |F|
C |L_ebi         |1 1 I|OUTO            |������||
C |R_efi         |4 4 O|mf_value        |��� ��������� �����|0.5|
C |L_ufi         |1 1 S|_qffnv2dyn$87Dasha*|�������� ������ Q RS-��������  |F|
C |L_eki         |1 1 S|_qffnv2dyn$86Dasha*|�������� ������ Q RS-��������  |F|
C |L_oki         |1 1 S|_qffnv2dyn$96Dasha*|�������� ������ Q RS-��������  |F|
C |L_uki         |1 1 S|_qffnv2dyn$95Dasha*|�������� ������ Q RS-��������  |F|
C |L_ili         |1 1 O|limit_switch_error|||
C |L_oli         |1 1 O|flag_mlf19      |||
C |C20_upi       |3 20 O|task_state      |���������||
C |L_ari         |1 1 I|vlv_kvit        |||
C |L_iri         |1 1 I|instr_fault     |||
C |L_ori         |1 1 S|_qffJ3170*      |�������� ������ Q RS-��������  |F|
C |L_esi         |1 1 O|norm            |�����||
C |L_isi         |1 1 I|mlf17           |������ ���������������� ����� �����||
C |L_osi         |1 1 I|mlf16           |������ ������������ ����� �����||
C |L_usi         |1 1 I|mlf15           |������ ���������������� ����� ������||
C |L_ati         |1 1 I|mlf14           |������ ������������ ����� ������||
C |L_eti         |1 1 I|mlf07           |���������� ���� �������||
C |L_iti         |1 1 I|mlf28           |������ ���������������� ��������� �����||
C |L_oti         |1 1 I|mlf09           |����� ��������� �����||
C |L_uti         |1 1 I|mlf08           |����� ��������� ������||
C |L_avi         |1 1 I|mlf26           |������ ���������������� ��������� ������||
C |L_evi         |1 1 I|mlf27           |������ ������������ ��������� �����||
C |L_ivi         |1 1 I|mlf25           |������ ������������ ��������� ������||
C |L_ovi         |1 1 I|mlf23           |������� ������� �����||
C |L_uvi         |1 1 I|mlf22           |����� ����� ��������||
C |L_axi         |1 1 I|mlf19           |���� ������������||
C |L_exi         |1 1 I|YA25C           |������� ������� �� ����������|F|
C |L_ixi         |1 1 I|YA26C           |������� ������� �� ���������� (��)|F|
C |R_oxi         |4 4 I|tcl_top         |�������� �������|20|
C |R_uxi         |4 4 K|_uintT_INT      |����������� ������ ����������� ������|`p_UP`|
C |R_abo         |4 4 K|_tintT_INT      |[���]�������� T �����������|1|
C |R_ebo         |4 4 O|_ointT_INT*     |�������� ������ ����������� |`p_START`|
C |R_ibo         |4 4 K|_lintT_INT      |����������� ������ ����������� �����|`p_LOW`|
C |I_obo         |2 4 O|LWORK           |����� �������||
C |R_edo         |4 4 O|VX01            |��������� ��������� �� ��� X||
C |R_ido         |4 4 O|VX02            |�������� ����������� ���������||
C |L_afo         |1 1 I|mlf04           |���������������� �������� �����||
C |C20_ulo       |3 20 O|task_name3      |������� ��������� ��� ������� ������������ ��������||
C |C20_umo       |3 20 O|task_name2      |������� ��������� ��� ������� ������������ ��������||
C |I_opo         |2 4 O|LREADY          |����� ����������||
C |I_ero         |2 4 O|LBUSY           |����� �����||
C |R_iro         |4 4 S|vminit_button_ST*|��������� ������ "������� � �������� �� ���������" |0.0|
C |R_oro         |4 4 I|vminit_button   |������� ������ ������ "������� � �������� �� ���������" |0.0|
C |R_uro         |4 4 S|vmwork_button_ST*|��������� ������ "������� � ������� �� ���������" |0.0|
C |R_aso         |4 4 I|vmwork_button   |������� ������ ������ "������� � ������� �� ���������" |0.0|
C |L_eso         |1 1 I|YA25            |������� ������� �� ����������|F|
C |L_iso         |1 1 I|YA26            |������� ������� �� ����������|F|
C |I_oso         |2 4 O|state1          |��������� 1||
C |I_eto         |2 4 O|state2          |��������� 2||
C |I_evo         |2 4 O|LWORKO          |����� � �������||
C |I_uvo         |2 4 O|LINITC          |����� � ��������||
C |C20_uxo       |3 20 O|task_name       |������� ���������||
C |C8_ubu        |3 8 I|task            |������� ���������||
C |R_iku         |4 4 S|vmstop_button_ST*|��������� ������ "������� ���� �� ���������" |0.0|
C |R_oku         |4 4 I|vmstop_button   |������� ������ ������ "������� ���� �� ���������" |0.0|
C |L_uku         |1 1 O|vmstart_button_CMD*|[TF]����� ������ ������� �������� ��������� �� ���������|F|
C |R_alu         |4 4 S|vmstart_button_ST*|��������� ������ "������� �������� ��������� �� ���������" |0.0|
C |R_elu         |4 4 I|vmstart_button  |������� ������ ������ "������� �������� ��������� �� ���������" |0.0|
C |I_ilu         |2 4 O|LERROR          |����� �������������||
C |L_amu         |1 1 I|YA27C           |������� ���� �� ���������� (��)|F|
C |L_emu         |1 1 I|stop_avt        |������� ���� �� ����������|F|
C |L_imu         |1 1 O|vmstop_button_CMD*|[TF]����� ������ ������� ���� �� ���������|F|
C |I_umu         |2 4 O|LINIT           |����� ��������||
C |I_ipu         |2 4 O|LZM             |������ "�������"||
C |L_iru         |1 1 O|vminit_button_CMD*|[TF]����� ������ ������� � �������� �� ���������|F|
C |L_uru         |1 1 O|vmwork_button_CMD*|[TF]����� ������ ������� � ������� �� ���������|F|
C |L_asu         |1 1 I|mlf06           |������������� ������� "�����"||
C |L_esu         |1 1 I|mlf05           |������������� ������� "������"||
C |L_isu         |1 1 I|ulubopen        |���������� ������� �� ���������|F|
C |L_osu         |1 1 I|ulubclose       |���������� ������� �� ���������|F|
C |L_otu         |1 1 O|block           |||
C |L_ovu         |1 1 S|_qff10*         |�������� ������ Q RS-��������  |F|
C |L_axu         |1 1 O|STOP            |�������||
C |L_exu         |1 1 O|nopower         |��� ����������||
C |R8_oxu        |4 8 I|voltage         |[��]���������� �� ������||
C |R_ibad        |4 4 I|power           |�������� ��������||
C |R8_adad       |4 8 O|sn_power        |�������� ���� ��|3.0|
C |L_odad        |1 1 I|mlf03           |���������������� �������� ������||
C |L_efad        |1 1 O|fault           |�������������||
C |I_ifad        |2 4 O|LAM             |������ "�������"||
C |L_akad        |1 1 O|XH53            |�� ����� (���)|F|
C |L_ekad        |1 1 O|XH54            |�� ������ (���)|F|
C |L_elad        |1 1 S|_qff2*          |�������� ������ Q RS-��������  |F|
C |L_umad        |1 1 S|_qff1*          |�������� ������ Q RS-��������  |F|
C |L_upad        |1 1 I|mlf24           |��� ���������� � ����������||

      IMPLICIT NONE
      REAL*4 ext_deltat
      REAL*4 R_e,R_i,R_ed,R_af,R_uk,R_el,R_il,R_ol,R_ul
      LOGICAL*1 L_is
      REAL*4 R_ot,R_ede
      LOGICAL*1 L_ide
      REAL*4 R_ude
      LOGICAL*1 L_afe,L_eke,L_uke
      REAL*4 R_ume,R_ope
      LOGICAL*1 L_are
      REAL*4 R_ire
      LOGICAL*1 L_ure
      REAL*4 R_ase,R_ese
      LOGICAL*1 L_ose,L_ote,L_ave,L_eve,L_ove,L_exe,L_uxe
     &,L_ebi
      REAL*4 R_afi,R_efi
      LOGICAL*1 L_ufi,L_eki,L_oki,L_uki,L_ili,L_oli
      CHARACTER*20 C20_upi
      LOGICAL*1 L_ari,L_iri,L_ori,L_esi,L_isi,L_osi,L_usi
     &,L_ati,L_eti,L_iti,L_oti,L_uti,L_avi,L_evi,L_ivi,L_ovi
     &,L_uvi,L_axi,L_exi,L_ixi
      REAL*4 R_oxi,R_uxi,R_abo,R_ebo,R_ibo
      INTEGER*4 I_obo
      REAL*4 R_edo,R_ido
      LOGICAL*1 L_afo
      CHARACTER*20 C20_ulo,C20_umo
      INTEGER*4 I_opo,I_ero
      REAL*4 R_iro,R_oro,R_uro,R_aso
      LOGICAL*1 L_eso,L_iso
      INTEGER*4 I_oso,I_eto,I_evo,I_uvo
      CHARACTER*20 C20_uxo
      CHARACTER*8 C8_ubu
      REAL*4 R_iku,R_oku
      LOGICAL*1 L_uku
      REAL*4 R_alu,R_elu
      INTEGER*4 I_ilu
      LOGICAL*1 L_amu,L_emu,L_imu
      INTEGER*4 I_umu,I_ipu
      LOGICAL*1 L_iru,L_uru,L_asu,L_esu,L_isu,L_osu,L_otu
     &,L_ovu,L_axu,L_exu
      REAL*8 R8_oxu
      REAL*4 R_ibad
      REAL*8 R8_adad
      LOGICAL*1 L_odad,L_efad
      INTEGER*4 I_ifad
      LOGICAL*1 L_akad,L_ekad,L_elad,L_umad,L_upad
      End subroutine TOLKATEL_HANDLER
      End interface
