      Subroutine LIFT_HANDLER(ext_deltat,R_uvi,R8_ili,I_o
     &,I_id,I_af,L_of,I_uf,R_ak,R_ek,R_ik,R_ok,I_uk,I_il,I_im
     &,I_ap,C20_ar,C20_as,C8_at,R_uv,R_ax,L_ex,R_ix,R_ox,I_ux
     &,R_obe,R_ade,I_ede,I_ude,I_ife,L_ake,L_ike,L_oke,L_ale
     &,L_ile,L_ole,L_ule,R_eme,L_epe,L_upe,L_ire,L_ore,L_ase
     &,L_ise,R8_use,R_ote,R8_eve,L_ive,L_axe,L_oxe,I_uxe,L_uki
     &,R_uli,R_upi,L_esi,L_osi,L_uti,L_ivi,L_axi,L_exi,L_ixi
     &,L_oxi,L_uxi,L_abo)
C |R_uvi         |4 4 I|11 mlfpar19     |��������� ������������|0.6|
C |R8_ili        |4 8 O|16 value        |��� �������� ��������|0.5|
C |I_o           |2 4 O|state2          |����������||
C |I_id          |2 4 O|LREADY          |����� ����������||
C |I_af          |2 4 O|LBUSY           |����� �����||
C |L_of          |1 1 I|peregruz        |����������|F|
C |I_uf          |2 4 O|LPEREGR         |����� ����������||
C |R_ak          |4 4 S|vmdown_button_ST*|��������� ������ "������� ���� �� ���������" |0.0|
C |R_ek          |4 4 I|vmdown_button   |������� ������ ������ "������� ���� �� ���������" |0.0|
C |R_ik          |4 4 S|vmup_button_ST* |��������� ������ "������� ����� �� ���������" |0.0|
C |R_ok          |4 4 I|vmup_button     |������� ������ ������ "������� ����� �� ���������" |0.0|
C |I_uk          |2 4 O|state1          |��������� 1||
C |I_il          |2 4 O|state3          |��������� 2||
C |I_im          |2 4 O|LUPO            |����� �����||
C |I_ap          |2 4 O|LDOWNC          |����� ����||
C |C20_ar        |3 20 O|task_state      |���������||
C |C20_as        |3 20 O|task_name       |������� ���������||
C |C8_at         |3 8 I|task            |������� ���������||
C |R_uv          |4 4 S|vmstop_button_ST*|��������� ������ "������� ���� �� ���������" |0.0|
C |R_ax          |4 4 I|vmstop_button   |������� ������ ������ "������� ���� �� ���������" |0.0|
C |L_ex          |1 1 O|vmstart_button_CMD*|[TF]����� ������ ������� �������� ��������� �� ���������|F|
C |R_ix          |4 4 S|vmstart_button_ST*|��������� ������ "������� �������� ��������� �� ���������" |0.0|
C |R_ox          |4 4 I|vmstart_button  |������� ������ ������ "������� �������� ��������� �� ���������" |0.0|
C |I_ux          |2 4 O|LERROR          |����� �������������||
C |R_obe         |4 4 O|POS_CL          |��������||
C |R_ade         |4 4 O|POS_OP          |��������||
C |I_ede         |2 4 O|LDOWN           |����� �����||
C |I_ude         |2 4 O|LUP             |����� ������||
C |I_ife         |2 4 O|LZM             |������ "�������"||
C |L_ake         |1 1 I|vlv_kvit        |||
C |L_ike         |1 1 I|instr_fault     |||
C |L_oke         |1 1 S|_qffJ1618*      |�������� ������ Q RS-��������  |F|
C |L_ale         |1 1 O|vmdown_button_CMD*|[TF]����� ������ ������� ���� �� ���������|F|
C |L_ile         |1 1 O|vmup_button_CMD*|[TF]����� ������ ������� ����� �� ���������|F|
C |L_ole         |1 1 I|ulubopen        |���������� ������� �� ���������|F|
C |L_ule         |1 1 I|ulubclose       |���������� ������� �� ���������|F|
C |R_eme         |4 4 I|tdown           |����� ���� ����|30.0|
C |L_epe         |1 1 O|block           |||
C |L_upe         |1 1 O|vmstop_button_CMD*|[TF]����� ������ ������� ���� �� ���������|F|
C |L_ire         |1 1 O|STOP            |�������||
C |L_ore         |1 1 S|_qff10*         |�������� ������ Q RS-��������  |F|
C |L_ase         |1 1 O|norm            |�����||
C |L_ise         |1 1 O|nopower         |��� ����������||
C |R8_use        |4 8 I|voltage         |[��]���������� �� ������||
C |R_ote         |4 4 I|power           |�������� ��������||
C |R8_eve        |4 8 O|sn_power        |�������� ���� ��|3.0|
C |L_ive         |1 1 I|YA23            |������� ������� �� ����������|F|
C |L_axe         |1 1 I|YA24            |������� ������� �� ����������|F|
C |L_oxe         |1 1 O|fault           |�������������||
C |I_uxe         |2 4 O|LAM             |������ "�������"||
C |L_uki         |1 1 S|_qff6*          |�������� ������ Q RS-��������  |F|
C |R_uli         |4 4 O|mf_value        |��� ��������� �����|0.5|
C |R_upi         |4 4 I|tup             |����� ���� �����|30.0|
C |L_esi         |1 1 S|_qff2*          |�������� ������ Q RS-��������  |F|
C |L_osi         |1 1 O|XH53            |�� ������� (���)|F|
C |L_uti         |1 1 S|_qff1*          |�������� ������ Q RS-��������  |F|
C |L_ivi         |1 1 O|XH54            |�� ������� (���)|F|
C |L_axi         |1 1 I|mlf24           |��� ���������� � ����������||
C |L_exi         |1 1 I|mlf23           |������� ������� �����||
C |L_ixi         |1 1 I|mlf22           |����� ����� ��������||
C |L_oxi         |1 1 I|mlf04           |�������� �� ��������||
C |L_uxi         |1 1 I|mlf03           |�������� �� ��������||
C |L_abo         |1 1 I|mlf19           |���� ������������||

      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      INTEGER*4 I0_e,I0_i,I_o,I0_u,I0_ad
      LOGICAL*1 L0_ed
      INTEGER*4 I_id,I0_od,I0_ud,I_af,I0_ef,I0_if
      LOGICAL*1 L_of
      INTEGER*4 I_uf
      REAL*4 R_ak,R_ek,R_ik,R_ok
      INTEGER*4 I_uk,I0_al,I0_el,I_il,I0_ol,I0_ul,I0_am,I0_em
     &,I_im,I0_om,I0_um,I_ap
      CHARACTER*20 C20_ep,C20_ip,C20_op,C20_up,C20_ar,C20_er
     &,C20_ir,C20_or,C20_ur,C20_as
      CHARACTER*8 C8_es
      LOGICAL*1 L0_is
      CHARACTER*8 C8_os
      LOGICAL*1 L0_us
      CHARACTER*8 C8_at
      INTEGER*4 I0_et,I0_it,I0_ot,I0_ut,I0_av,I0_ev,I0_iv
     &,I0_ov
      REAL*4 R_uv,R_ax
      LOGICAL*1 L_ex
      REAL*4 R_ix,R_ox
      INTEGER*4 I_ux,I0_abe,I0_ebe
      REAL*4 R0_ibe,R_obe,R0_ube,R_ade
      INTEGER*4 I_ede,I0_ide,I0_ode,I_ude,I0_afe,I0_efe,I_ife
     &,I0_ofe,I0_ufe
      LOGICAL*1 L_ake,L0_eke,L_ike,L_oke,L0_uke,L_ale,L0_ele
     &,L_ile,L_ole,L_ule
      REAL*4 R0_ame,R_eme
      LOGICAL*1 L0_ime,L0_ome,L0_ume,L0_ape,L_epe,L0_ipe,L0_ope
     &,L_upe,L0_are,L0_ere,L_ire,L_ore,L0_ure,L_ase,L0_ese
     &,L_ise
      REAL*4 R0_ose
      REAL*8 R8_use
      LOGICAL*1 L0_ate,L0_ete
      REAL*4 R0_ite,R_ote,R0_ute,R0_ave
      REAL*8 R8_eve
      LOGICAL*1 L_ive,L0_ove,L0_uve,L_axe,L0_exe,L0_ixe,L_oxe
      INTEGER*4 I_uxe,I0_abi,I0_ebi
      LOGICAL*1 L0_ibi,L0_obi
      REAL*4 R0_ubi,R0_adi
      LOGICAL*1 L0_edi
      REAL*4 R0_idi,R0_odi,R0_udi,R0_afi,R0_efi,R0_ifi
      LOGICAL*1 L0_ofi
      REAL*4 R0_ufi,R0_aki,R0_eki,R0_iki
      LOGICAL*1 L0_oki,L_uki
      REAL*4 R0_ali,R0_eli
      REAL*8 R8_ili
      REAL*4 R0_oli,R_uli,R0_ami,R0_emi,R0_imi,R0_omi,R0_umi
     &,R0_api,R0_epi,R0_ipi,R0_opi,R_upi
      LOGICAL*1 L0_ari,L0_eri,L0_iri,L0_ori,L0_uri,L0_asi
     &,L_esi,L0_isi,L_osi,L0_usi,L0_ati,L0_eti,L0_iti,L0_oti
     &,L_uti,L0_avi,L0_evi,L_ivi,L0_ovi
      REAL*4 R_uvi
      LOGICAL*1 L_axi,L_exi,L_ixi,L_oxi,L_uxi,L_abo

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      I0_i = z'01000003'
C LIFT_HANDLER.fmg(  88, 242):��������� ������������� IN (�������)
      I0_e = z'01000010'
C LIFT_HANDLER.fmg(  88, 240):��������� ������������� IN (�������)
      if(L_of) then
         I_o=I0_e
      else
         I_o=I0_i
      endif
C LIFT_HANDLER.fmg(  92, 240):���� RE IN LO CH7
      I0_u = z'0100000A'
C LIFT_HANDLER.fmg( 218, 270):��������� ������������� IN (�������)
      I0_ad = z'01000003'
C LIFT_HANDLER.fmg( 218, 272):��������� ������������� IN (�������)
      I0_ud = z'0100000A'
C LIFT_HANDLER.fmg( 218, 292):��������� ������������� IN (�������)
      I0_od = z'01000003'
C LIFT_HANDLER.fmg( 218, 290):��������� ������������� IN (�������)
      I0_if = z'01000003'
C LIFT_HANDLER.fmg(  84, 251):��������� ������������� IN (�������)
      I0_ef = z'0100000A'
C LIFT_HANDLER.fmg(  84, 249):��������� ������������� IN (�������)
      if(L_of) then
         I_uf=I0_ef
      else
         I_uf=I0_if
      endif
C LIFT_HANDLER.fmg(  88, 250):���� RE IN LO CH7
      L_ale=R_ek.ne.R_ak
      R_ak=R_ek
C LIFT_HANDLER.fmg(  14, 193):���������� ������������� ������
      L_ile=R_ok.ne.R_ik
      R_ik=R_ok
C LIFT_HANDLER.fmg(  19, 249):���������� ������������� ������
      I0_el = z'01000003'
C LIFT_HANDLER.fmg( 138, 162):��������� ������������� IN (�������)
      I0_al = z'01000010'
C LIFT_HANDLER.fmg( 138, 160):��������� ������������� IN (�������)
      I0_ul = z'01000003'
C LIFT_HANDLER.fmg( 117, 252):��������� ������������� IN (�������)
      I0_ol = z'01000010'
C LIFT_HANDLER.fmg( 117, 250):��������� ������������� IN (�������)
      I0_em = z'01000003'
C LIFT_HANDLER.fmg( 185, 229):��������� ������������� IN (�������)
      I0_am = z'0100000A'
C LIFT_HANDLER.fmg( 185, 227):��������� ������������� IN (�������)
      I0_um = z'01000003'
C LIFT_HANDLER.fmg( 193, 179):��������� ������������� IN (�������)
      I0_om = z'0100000A'
C LIFT_HANDLER.fmg( 193, 177):��������� ������������� IN (�������)
      I0_ode = z'01000003'
C LIFT_HANDLER.fmg( 115, 171):��������� ������������� IN (�������)
      I0_ide = z'0100000A'
C LIFT_HANDLER.fmg( 115, 169):��������� ������������� IN (�������)
      C20_op = '������'
C LIFT_HANDLER.fmg(  29, 160):��������� ���������� CH20 (CH30) (�������)
      C20_up = '������� ���'
C LIFT_HANDLER.fmg(  29, 162):��������� ���������� CH20 (CH30) (�������)
      C20_ep = '�����'
C LIFT_HANDLER.fmg(  44, 160):��������� ���������� CH20 (CH30) (�������)
      C20_er = '����'
C LIFT_HANDLER.fmg(  50, 171):��������� ���������� CH20 (CH30) (�������)
      C20_or = '�����'
C LIFT_HANDLER.fmg(  35, 172):��������� ���������� CH20 (CH30) (�������)
      C20_ur = ''
C LIFT_HANDLER.fmg(  35, 174):��������� ���������� CH20 (CH30) (�������)
      C8_es = 'down'
C LIFT_HANDLER.fmg(  18, 180):��������� ���������� CH8 (�������)
      call chcomp(C8_at,C8_es,L0_is)
C LIFT_HANDLER.fmg(  23, 184):���������� ���������
      C8_os = 'up'
C LIFT_HANDLER.fmg(  18, 222):��������� ���������� CH8 (�������)
      call chcomp(C8_at,C8_os,L0_us)
C LIFT_HANDLER.fmg(  23, 226):���������� ���������
      if(L0_us) then
         C20_ir=C20_or
      else
         C20_ir=C20_ur
      endif
C LIFT_HANDLER.fmg(  39, 172):���� RE IN LO CH20
      if(L0_is) then
         C20_as=C20_er
      else
         C20_as=C20_ir
      endif
C LIFT_HANDLER.fmg(  55, 172):���� RE IN LO CH20
      I0_et = z'0100008E'
C LIFT_HANDLER.fmg( 170, 194):��������� ������������� IN (�������)
      I0_it = z'01000086'
C LIFT_HANDLER.fmg( 164, 248):��������� ������������� IN (�������)
      I0_ut = z'01000003'
C LIFT_HANDLER.fmg( 152, 262):��������� ������������� IN (�������)
      I0_av = z'01000010'
C LIFT_HANDLER.fmg( 152, 264):��������� ������������� IN (�������)
      I0_ov = z'01000003'
C LIFT_HANDLER.fmg( 159, 209):��������� ������������� IN (�������)
      I0_iv = z'01000010'
C LIFT_HANDLER.fmg( 159, 207):��������� ������������� IN (�������)
      L_upe=R_ax.ne.R_uv
      R_uv=R_ax
C LIFT_HANDLER.fmg(  14, 208):���������� ������������� ������
      L_ex=R_ox.ne.R_ix
      R_ix=R_ox
C LIFT_HANDLER.fmg(  19, 236):���������� ������������� ������
      L0_ele = L_ex.AND.L0_us
C LIFT_HANDLER.fmg(  30, 234):�
      L0_ope = L_ile.OR.L0_ele
C LIFT_HANDLER.fmg(  52, 235):���
      L0_uke = L_ex.AND.L0_is
C LIFT_HANDLER.fmg(  29, 192):�
      L0_ipe = L_ale.OR.L0_uke
C LIFT_HANDLER.fmg(  52, 193):���
      I0_ofe = z'0100000E'
C LIFT_HANDLER.fmg( 189, 200):��������� ������������� IN (�������)
      I0_abe = z'01000007'
C LIFT_HANDLER.fmg( 150, 180):��������� ������������� IN (�������)
      I0_ebe = z'01000003'
C LIFT_HANDLER.fmg( 150, 182):��������� ������������� IN (�������)
      R0_ibe = 100
C LIFT_HANDLER.fmg( 378, 273):��������� (RE4) (�������)
      R0_ube = 100
C LIFT_HANDLER.fmg( 365, 267):��������� (RE4) (�������)
      I0_efe = z'01000003'
C LIFT_HANDLER.fmg( 122, 265):��������� ������������� IN (�������)
      I0_afe = z'0100000A'
C LIFT_HANDLER.fmg( 122, 263):��������� ������������� IN (�������)
      I0_abi = z'0100000E'
C LIFT_HANDLER.fmg( 182, 255):��������� ������������� IN (�������)
      L_oke=(L_ike.or.L_oke).and..not.(L_ake)
      L0_eke=.not.L_oke
C LIFT_HANDLER.fmg( 326, 178):RS �������
      L0_ape=.false.
C LIFT_HANDLER.fmg(  64, 215):��������� ���������� (�������)
      L0_ume=.false.
C LIFT_HANDLER.fmg(  64, 213):��������� ���������� (�������)
      R0_ame = DeltaT
C LIFT_HANDLER.fmg( 250, 254):��������� (RE4) (�������)
      if(R_eme.ge.0.0) then
         R0_omi=R0_ame/max(R_eme,1.0e-10)
      else
         R0_omi=R0_ame/min(R_eme,-1.0e-10)
      endif
C LIFT_HANDLER.fmg( 259, 252):�������� ����������
      L0_ese =.NOT.(L_uxi.OR.L_oxi.OR.L_abo.OR.L_ixi.OR.L_exi.OR.L_axi
     &)
C LIFT_HANDLER.fmg( 319, 191):���
      L0_ure =.NOT.(L0_ese)
C LIFT_HANDLER.fmg( 328, 185):���
      L_oxe = L0_ure.OR.L_oke
C LIFT_HANDLER.fmg( 332, 184):���
      L0_ome = L_oxe.OR.L_ole
C LIFT_HANDLER.fmg(  56, 243):���
      L0_exe = L0_ope.AND.(.NOT.L0_ome)
C LIFT_HANDLER.fmg(  66, 236):�
      L0_ixe = L0_exe.OR.L_axe
C LIFT_HANDLER.fmg(  70, 234):���
      L0_evi = L0_ixe.AND.(.NOT.L_of)
C LIFT_HANDLER.fmg(  76, 234):�
      if(L_oxe) then
         I_ux=I0_abe
      else
         I_ux=I0_ebe
      endif
C LIFT_HANDLER.fmg( 153, 181):���� RE IN LO CH7
      L0_ime = L_oxe.OR.L_ule
C LIFT_HANDLER.fmg(  56, 201):���
      L_epe = L_oxe.OR.L0_ape.OR.L0_ume.OR.L0_ome.OR.L0_ime
C LIFT_HANDLER.fmg(  68, 213):���
      L0_ed = L_oxe.OR.L_epe
C LIFT_HANDLER.fmg( 216, 282):���
      if(L0_ed) then
         I_id=I0_od
      else
         I_id=I0_ud
      endif
C LIFT_HANDLER.fmg( 221, 290):���� RE IN LO CH7
      if(L_epe) then
         I_af=I0_u
      else
         I_af=I0_ad
      endif
C LIFT_HANDLER.fmg( 221, 271):���� RE IN LO CH7
      L0_ove = (.NOT.L0_ime).AND.L0_ipe
C LIFT_HANDLER.fmg(  66, 194):�
      L0_uve = L0_ove.OR.L_ive
C LIFT_HANDLER.fmg(  70, 192):���
      L0_usi = L0_uve.AND.(.NOT.L_of)
C LIFT_HANDLER.fmg(  76, 192):�
      L0_are = L0_evi.OR.L0_usi
C LIFT_HANDLER.fmg(  82, 203):���
      L_ore=(L_upe.or.L_ore).and..not.(L0_are)
      L0_ere=.not.L_ore
C LIFT_HANDLER.fmg( 106, 205):RS �������,10
      L_ire=L_ore
C LIFT_HANDLER.fmg( 125, 207):������,STOP
      L0_iri = L_ore.OR.L_axi
C LIFT_HANDLER.fmg(  98, 214):���
      L_ase = L0_ese.OR.L_ike
C LIFT_HANDLER.fmg( 328, 190):���
      R0_ose = 0.1
C LIFT_HANDLER.fmg( 254, 160):��������� (RE4) (�������)
      L_ise=R8_use.lt.R0_ose
C LIFT_HANDLER.fmg( 259, 162):���������� <
      R0_ite = 0.0
C LIFT_HANDLER.fmg( 266, 182):��������� (RE4) (�������)
      R0_efi = 0.000001
C LIFT_HANDLER.fmg( 354, 208):��������� (RE4) (�������)
      R0_ifi = 0.999999
C LIFT_HANDLER.fmg( 354, 224):��������� (RE4) (�������)
      R0_ubi = 0.0
C LIFT_HANDLER.fmg( 296, 244):��������� (RE4) (�������)
      R0_adi = 0.0
C LIFT_HANDLER.fmg( 370, 242):��������� (RE4) (�������)
      L0_edi = L_exi.OR.L_ixi
C LIFT_HANDLER.fmg( 363, 237):���
      R0_odi = 1.0
C LIFT_HANDLER.fmg( 348, 252):��������� (RE4) (�������)
      R0_udi = 0.0
C LIFT_HANDLER.fmg( 340, 252):��������� (RE4) (�������)
      R0_aki = 1.0e-10
C LIFT_HANDLER.fmg( 318, 228):��������� (RE4) (�������)
      R0_ali = 0.0
C LIFT_HANDLER.fmg( 328, 242):��������� (RE4) (�������)
      R0_emi = DeltaT
C LIFT_HANDLER.fmg( 250, 264):��������� (RE4) (�������)
      if(R_upi.ge.0.0) then
         R0_umi=R0_emi/max(R_upi,1.0e-10)
      else
         R0_umi=R0_emi/min(R_upi,-1.0e-10)
      endif
C LIFT_HANDLER.fmg( 259, 262):�������� ����������
      R0_opi = 0.0
C LIFT_HANDLER.fmg( 282, 244):��������� (RE4) (�������)
      L0_ori = (.NOT.L_ire).AND.L_osi
C LIFT_HANDLER.fmg( 103, 166):�
C label 211  try211=try211-1
      if(L_ixi) then
         R0_idi=R0_adi
      else
         R0_idi=R8_ili
      endif
C LIFT_HANDLER.fmg( 373, 242):���� RE IN LO CH7
      if(.NOT.L0_edi) then
         R_uli=R8_ili
      endif
C LIFT_HANDLER.fmg( 384, 252):���� � ������������� �������
      L0_eti = (.NOT.L_ire).AND.L_ivi
C LIFT_HANDLER.fmg( 105, 260):�
      L0_oti = L_ivi.OR.L0_iri.OR.L0_usi
C LIFT_HANDLER.fmg( 104, 229):���
      L0_ovi = (.NOT.L_ivi).AND.L0_evi
C LIFT_HANDLER.fmg( 104, 235):�
      L_uti=(L0_ovi.or.L_uti).and..not.(L0_oti)
      L0_avi=.not.L_uti
C LIFT_HANDLER.fmg( 111, 233):RS �������,1
      L0_iti = (.NOT.L0_eti).AND.L_uti
C LIFT_HANDLER.fmg( 130, 236):�
      L0_obi = L0_iti.AND.(.NOT.L_oxi)
C LIFT_HANDLER.fmg( 252, 242):�
      L0_eri = L0_obi.OR.L_uxi
C LIFT_HANDLER.fmg( 265, 241):���
      if(L0_eri) then
         R0_epi=R0_umi
      else
         R0_epi=R0_opi
      endif
C LIFT_HANDLER.fmg( 287, 254):���� RE IN LO CH7
      L0_asi = L0_iri.OR.L_osi.OR.L0_evi
C LIFT_HANDLER.fmg( 104, 185):���
      L0_ati = L0_usi.AND.(.NOT.L_osi)
C LIFT_HANDLER.fmg( 104, 191):�
      L_esi=(L0_ati.or.L_esi).and..not.(L0_asi)
      L0_isi=.not.L_esi
C LIFT_HANDLER.fmg( 111, 189):RS �������,2
      L0_uri = L_esi.AND.(.NOT.L0_ori)
C LIFT_HANDLER.fmg( 130, 190):�
      L0_ibi = L0_uri.AND.(.NOT.L_uxi)
C LIFT_HANDLER.fmg( 252, 228):�
      L0_ari = L0_ibi.OR.L_oxi
C LIFT_HANDLER.fmg( 265, 227):���
      if(L0_ari) then
         R0_api=R0_omi
      else
         R0_api=R0_opi
      endif
C LIFT_HANDLER.fmg( 287, 236):���� RE IN LO CH7
      R0_ipi = R0_epi + (-R0_api)
C LIFT_HANDLER.fmg( 293, 246):��������
      if(L_axi) then
         R0_eli=R0_ubi
      else
         R0_eli=R0_ipi
      endif
C LIFT_HANDLER.fmg( 300, 244):���� RE IN LO CH7
      R0_iki = R_uli + (-R_uvi)
C LIFT_HANDLER.fmg( 308, 235):��������
      R0_ufi = R0_eli + R0_iki
C LIFT_HANDLER.fmg( 314, 236):��������
      R0_eki = R0_ufi * R0_iki
C LIFT_HANDLER.fmg( 318, 235):����������
      L0_oki=R0_eki.lt.R0_aki
C LIFT_HANDLER.fmg( 323, 234):���������� <
      L_uki=(L0_oki.or.L_uki).and..not.(.NOT.L_abo)
      L0_ofi=.not.L_uki
C LIFT_HANDLER.fmg( 330, 232):RS �������,6
      if(L_uki) then
         R_uli=R_uvi
      endif
C LIFT_HANDLER.fmg( 324, 252):���� � ������������� �������
      if(L_uki) then
         R0_oli=R0_ali
      else
         R0_oli=R0_eli
      endif
C LIFT_HANDLER.fmg( 332, 244):���� RE IN LO CH7
      R0_ami = R_uli + R0_oli
C LIFT_HANDLER.fmg( 338, 245):��������
      R0_afi=MAX(R0_udi,R0_ami)
C LIFT_HANDLER.fmg( 346, 246):��������
      R0_imi=MIN(R0_odi,R0_afi)
C LIFT_HANDLER.fmg( 354, 247):�������
      L_osi=R0_imi.lt.R0_efi
C LIFT_HANDLER.fmg( 363, 210):���������� <
C sav1=L0_asi
      L0_asi = L0_iri.OR.L_osi.OR.L0_evi
C LIFT_HANDLER.fmg( 104, 185):recalc:���
C if(sav1.ne.L0_asi .and. try253.gt.0) goto 253
C sav1=L0_ati
      L0_ati = L0_usi.AND.(.NOT.L_osi)
C LIFT_HANDLER.fmg( 104, 191):recalc:�
C if(sav1.ne.L0_ati .and. try255.gt.0) goto 255
C sav1=L0_ori
      L0_ori = (.NOT.L_ire).AND.L_osi
C LIFT_HANDLER.fmg( 103, 166):recalc:�
C if(sav1.ne.L0_ori .and. try211.gt.0) goto 211
      if(L_osi) then
         I0_ot=I0_ut
      else
         I0_ot=I0_av
      endif
C LIFT_HANDLER.fmg( 156, 262):���� RE IN LO CH7
      if(L0_iti) then
         I0_ebi=I0_it
      else
         I0_ebi=I0_ot
      endif
C LIFT_HANDLER.fmg( 168, 262):���� RE IN LO CH7
      if(L_oxe) then
         I_uxe=I0_abi
      else
         I_uxe=I0_ebi
      endif
C LIFT_HANDLER.fmg( 186, 260):���� RE IN LO CH7
      if(L0_edi) then
         R8_ili=R0_idi
      else
         R8_ili=R0_imi
      endif
C LIFT_HANDLER.fmg( 377, 246):���� RE IN LO CH7
      R_ade = R0_ube * R8_ili
C LIFT_HANDLER.fmg( 368, 266):����������
      R_obe = R0_ibe + (-R_ade)
C LIFT_HANDLER.fmg( 382, 272):��������
      L_ivi=R0_imi.gt.R0_ifi
C LIFT_HANDLER.fmg( 363, 225):���������� >
C sav1=L0_oti
      L0_oti = L_ivi.OR.L0_iri.OR.L0_usi
C LIFT_HANDLER.fmg( 104, 229):recalc:���
C if(sav1.ne.L0_oti .and. try232.gt.0) goto 232
C sav1=L0_ovi
      L0_ovi = (.NOT.L_ivi).AND.L0_evi
C LIFT_HANDLER.fmg( 104, 235):recalc:�
C if(sav1.ne.L0_ovi .and. try234.gt.0) goto 234
C sav1=L0_eti
      L0_eti = (.NOT.L_ire).AND.L_ivi
C LIFT_HANDLER.fmg( 105, 260):recalc:�
C if(sav1.ne.L0_eti .and. try227.gt.0) goto 227
      if(L_ivi) then
         I0_ev=I0_iv
      else
         I0_ev=I0_ov
      endif
C LIFT_HANDLER.fmg( 162, 208):���� RE IN LO CH7
      if(L0_uri) then
         I0_ufe=I0_et
      else
         I0_ufe=I0_ev
      endif
C LIFT_HANDLER.fmg( 173, 206):���� RE IN LO CH7
      if(L_oxe) then
         I_ife=I0_ofe
      else
         I_ife=I0_ufe
      endif
C LIFT_HANDLER.fmg( 192, 206):���� RE IN LO CH7
      if(L_ivi) then
         C20_ip=C20_op
      else
         C20_ip=C20_up
      endif
C LIFT_HANDLER.fmg(  33, 161):���� RE IN LO CH20
      if(L_osi) then
         C20_ar=C20_ep
      else
         C20_ar=C20_ip
      endif
C LIFT_HANDLER.fmg(  49, 160):���� RE IN LO CH20
      if(L0_edi) then
         R_uli=R0_imi
      endif
C LIFT_HANDLER.fmg( 366, 252):���� � ������������� �������
C sav1=R0_iki
      R0_iki = R_uli + (-R_uvi)
C LIFT_HANDLER.fmg( 308, 235):recalc:��������
C if(sav1.ne.R0_iki .and. try279.gt.0) goto 279
C sav1=R0_ami
      R0_ami = R_uli + R0_oli
C LIFT_HANDLER.fmg( 338, 245):recalc:��������
C if(sav1.ne.R0_ami .and. try296.gt.0) goto 296
      if(L0_uri) then
         I_ap=I0_om
      else
         I_ap=I0_um
      endif
C LIFT_HANDLER.fmg( 196, 178):���� RE IN LO CH7
      L0_ate = L0_iti.OR.L0_uri
C LIFT_HANDLER.fmg( 251, 174):���
      L0_ete = L0_ate.AND.(.NOT.L_ise)
C LIFT_HANDLER.fmg( 266, 173):�
      if(L0_ete) then
         R0_ave=R_ote
      else
         R0_ave=R0_ite
      endif
C LIFT_HANDLER.fmg( 269, 180):���� RE IN LO CH7
      if(L0_iti) then
         I_im=I0_am
      else
         I_im=I0_em
      endif
C LIFT_HANDLER.fmg( 188, 228):���� RE IN LO CH7
      if(L0_eti) then
         I_ude=I0_afe
      else
         I_ude=I0_efe
      endif
C LIFT_HANDLER.fmg( 126, 264):���� RE IN LO CH7
      if(L0_eti) then
         I_il=I0_ol
      else
         I_il=I0_ul
      endif
C LIFT_HANDLER.fmg( 120, 250):���� RE IN LO CH7
      if(L0_ori) then
         I_ede=I0_ide
      else
         I_ede=I0_ode
      endif
C LIFT_HANDLER.fmg( 118, 170):���� RE IN LO CH7
      if(L0_ori) then
         I_uk=I0_al
      else
         I_uk=I0_el
      endif
C LIFT_HANDLER.fmg( 142, 160):���� RE IN LO CH7
      R0_ute = R8_eve
C LIFT_HANDLER.fmg( 264, 190):��������
C label 387  try387=try387-1
      R8_eve = R0_ave + R0_ute
C LIFT_HANDLER.fmg( 275, 189):��������
C sav1=R0_ute
      R0_ute = R8_eve
C LIFT_HANDLER.fmg( 264, 190):recalc:��������
C if(sav1.ne.R0_ute .and. try387.gt.0) goto 387
      End
