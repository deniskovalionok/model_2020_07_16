      Interface
      Subroutine LODOCHKA_HANDLER_FDB60_COORD(ext_deltat,I_i
     &,R_ad,L_od,L_of,L_ok,L_ol,L_om,L_op,L_or,L_os,L_ot,L_ov
     &,L_ox,L_obe,L_ode,L_ofe,I_eke,R_ike,R_oke,I_ale,I_ele
     &,L_ule,I_eme)
C |I_i           |2 4 K|_lcmpJ10599     |�������� ������ �����������|5008|
C |R_ad          |4 4 O|CIL_IT_PKS_MASS_AFTER|||
C |L_od          |1 1 I|FDB50AE403_UNCATCH_BOAT|������� �� ����������� �� ����� ��||
C |L_of          |1 1 I|FDB50AE403_WEIGHT_1|������� �� ������� �����������||
C |L_ok          |1 1 I|FDB50AE403_CATCH_BOAT_1|������� ������������ �� ������� �����������||
C |L_ol          |1 1 I|FDB50AE403_UNMOUNT|������� ��������� �� ��������.||
C |L_om          |1 1 I|FDB50AE403_CATCH_1|������� � �� �� ����� ��������||
C |L_op          |1 1 I|FDB50AE506_PUSH |������� � ��������� �� ���������� ������������.||
C |L_or          |1 1 I|FDB50AE505_PUSH |������� �� ������ ����� ����.||
C |L_os          |1 1 I|FDB50AE504_PUSH |������� �� �����. ��������� � ������ ����� ����.||
C |L_ot          |1 1 I|FDB50AE503_PUSH |������� � ����. ������� ���������||
C |L_ov          |1 1 I|FDB50AE502_PUSH |������� ���� ��� ������ ����� ����||
C |L_ox          |1 1 I|FDB50AE501_PUSH |�� ������� ����� ���||
C |L_obe         |1 1 I|FDB50AE500_PUSH |������� �� ����� ��������� � ������������� �����||
C |L_ode         |1 1 I|FDB50AE403_MOUNT_ADD|������� �� ������ ����� ���������||
C |L_ofe         |1 1 I|FDB50AE403_WEIGHT|������� �� ������� �����������||
C |I_eke         |2 4 K|_lcmpJ10325     |�������� ������ �����������|5003|
C |R_ike         |4 4 I|MASS            |����� �������||
C |R_oke         |4 4 O|CIL_IT_PKS_MASS_BEFORE|||
C |I_ale         |2 4 P|num_boat        |����� �������||
C |I_ele         |2 4 O|CIL_IT_PKS_N    |||
C |L_ule         |1 1 I|FDB50AE403_CATCH_BOAT|������� ����� ������������� ������ (��)||
C |I_eme         |2 4 O|CR              |�������������� � �������������||

      IMPLICIT NONE
      REAL*4 ext_deltat
      INTEGER*4 I_i
      REAL*4 R_ad
      LOGICAL*1 L_od,L_of,L_ok,L_ol,L_om,L_op,L_or,L_os,L_ot
     &,L_ov,L_ox,L_obe,L_ode,L_ofe
      INTEGER*4 I_eke
      REAL*4 R_ike,R_oke
      INTEGER*4 I_ale,I_ele
      LOGICAL*1 L_ule
      INTEGER*4 I_eme
      End subroutine LODOCHKA_HANDLER_FDB60_COORD
      End interface
